<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Consulta de Cartolas</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%@ page import = "datapro.eibs.master.Util,datapro.eibs.beans.*" %>

<jsp:useBean id="stmlist" class="datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id="header" class="datapro.eibs.beans.ECIF33001Message" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">
 
 var reason = "";

 function showInqStatementPrint(account, cartola) {
	var page = "<%=request.getContextPath()%>/pages/s/MISC_report_wait.jsp?URL='<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECIF330?SCREEN=500@account=" + account + "@reference=" + cartola + "'";
	CenterNamedWindow(page,'PDF',500,500,7);		
 }

</SCRIPT>

</head>

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>


<h3 align="center">Consulta de Cartolas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="statement_account_list, ECIF330"></h3>
<hr size="4">
<p>&nbsp;</p>

<form method="post">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" class="tbhead" align="center">
          <tr id=trdark> 
            <td nowrap width="10%" align="right"> 
              <div align="right">Cliente : </div>
            </td>
            <td nowrap width="10%" align="left"> <%=header.getE01ACMCUN()%> </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Nombre : </div>
            </td>
            <td nowrap width="30%"align="left"> <%= header.getE01CUSNA1()%> </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Identificación : </div>
            </td>
            <td nowrap width="30%" align="left"> <%= header.getE01CUSIDE()%> </td>
          </tr>
          <tr id=trclear> 
            <td nowrap width="10%" align="right"> Cuenta : </td>
            <td nowrap width="10%" align="left"> <%= header.getE01ACMACC()%> </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Producto : </div>
            </td>
            <td nowrap width="30%" align="left"> 
            	<div> 
            	 <%=header.getE01ACMPRO()%> - 
            	 <%=header.getE01DSCPRO()%> 
                </div>  
            </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Oficial Cuenta : </div>
            </td>
            <td nowrap width="30%" align="left"> 
            	<div> 
            	 <%=header.getE01ACMOFC()%> - 
            	 <%=header.getE01DSCOFC()%> 
                </div>  
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <BR>
  <% if (stmlist.getNoResult()) {%>
      <h3 align=center>No existen Estados de Cuenta para esta Cuenta</h3>
  <% } else { %>
  
  <table class="tableinfo">
    <tr > 
      <td nowrap>
        <table id="headTable" >
		    <tr id="trdark">  
		      <th align="center" nowrap>Numero</TH>
		      <th align="center" nowrap>Fecha Emision</TH>
		    </TR>
		</table>  
   		<div id="dataDiv1" class="scbarcolor" style="padding:0" >
    		<table id="dataTable"  >
               <%          		
          		stmlist.initRow();
   		  		while (stmlist.getNextRow()) {
          			ECIF33002Message message = (ECIF33002Message) stmlist.getRecord();
          		%>    		
          			  <tr> 
				         <td nowrap align="center"> <a HREF="javascript:showInqStatementPrint('<%= header.getE01ACMACC() %>', '<%= message.getE02STHNUM() %>')"> <%= message.getE02STHNUM() %></a></td>
				         <td nowrap align="center"> <a HREF="javascript:showInqStatementPrint('<%= header.getE01ACMACC() %>', '<%= message.getE02STHNUM() %>')"> <%= Util.formatDate(message.getE02STHRDD(), message.getE02STHRDM(), message.getE02STHRDY()) %></a></td>	
				     </tr>
				 <%
          		   }         
                %>         
		     </table>   
  		</div>	
          		 
      </td>
    </tr>
  </table>
  <% } %>

  <SCRIPT Language="Javascript">
    function resizeDoc() {
       adjustEquTables(headTable, dataTable, dataDiv1,1,false);
    }
  	resizeDoc();
  	window.onresize=resizeDoc;
  </SCRIPT>
  
  </form>
</body>
</html>
