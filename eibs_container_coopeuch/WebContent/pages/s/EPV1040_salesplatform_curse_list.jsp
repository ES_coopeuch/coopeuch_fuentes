<%@ page import="datapro.eibs.beans.EPV104001Message"%>
<%@ page import="datapro.eibs.master.Util"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<title>Plataforma de Ventas</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="EPV104001List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>
<script type="text/javascript">



                 $(function(){
					
					$("#radio_key").attr("checked", false);
                
				});



function consulta_extra(op,idxRow,custom){

		var ok = false;
	
		document.forms[0].keysel.value=idxRow;//setea el checkbox
		document.forms[0].selected_customer.value=custom;//setea el campo selected_customer que est� oculto
		document.forms[0].elements[idxRow].checked = true;

    	tbAddInfo.rows[0].cells[1].style.color="blue";   
    	tbAddInfo.rows[0].cells[1].innerHTML=document.forms[0]["TXTDATA"+idxRow].value;//extraInfo(document.forms[0]["TXTDATA"+idxRow].value,4);

    	$("input[name=key][value=" + idxRow + "]").prop('checked', true);

    	document.forms[0].SCREEN.value = op;

		if (op == '900' && idxRow){
			//redirecciona al popup de detalle de cr�dito
		dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1040?SCREEN="+document.forms[0].SCREEN.value+
				  "&key="+document.forms[0].keysel.value;	
				
				
				  		
			CenterWindow(dir,780,500,2);
		
		}
		


}



  
 function showAddInfo(idxRow, custom){
    document.forms[0].keysel.value=idxRow;
    document.forms[0].selected_customer.value=custom;
     
   tbAddInfo.rows[0].cells[1].style.color="blue";   
   tbAddInfo.rows[0].cells[1].innerHTML=document.forms[0]["TXTDATA"+idxRow].value;//extraInfo(document.forms[0]["TXTDATA"+idxRow].value,4);

   }
      
   
 function extraInfo(textfields,noField) {
	 var pos=0
	 var s= textfields;
	 for ( var i=0; i<noField ; i++ ) {
	   pos=textfields.indexOf("<br>",pos+1);
	  }
	 s=textfields.substring(0,pos);
	 return(s);
 }  

</script>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Plataforma de Venta<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_curse_list.jsp,EPV1040"></h3>

<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1040">
<input type="hidden" name="SCREEN" value="200"> 
<input type="hidden" name="keysel" value="">
<input type="hidden" name="totalRow" value="">
<input type="hidden" name="selected_customer">


<table class="tbenter" width=100% align=center>
	<tr>
		<td class=TDBKG width="35%">
			<div align="center"><a href="javascript:goAction('200')" id="linkApproval"><b>Cursar</b></a></div>
		</td>
		<td class=TDBKG width="35%">
			<div align="center"><a href="javascript:goAction('300')" id="linkReject"><b>Rechazar</b></a></div>
		</td>
		<td class=TDBKG width="30%">
		<div align="center"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
		</td>
	</tr>
</table>

<% 
 int k = 0;
	if (EPV104001List.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=90%>
	<TR>
		<TD>
		<div align="center"><font size="3"><b> No hay Operaciones pendientes de Curse. </b></font></div>
		</TD>
	</TR>
</TABLE>
<%
	} else {
%>
 <table id="mainTable" class="tableinfo" align="center">
	<tr>
		<td nowrap valign="top">

		<TABLE id="headTable" >
    		<TR id="trdark">  		
				<th align="center" nowrap></th>
				<th align="center" nowrap>N&uacute;mero</th>
				<th align="center" nowrap>Cliente</th>
				<th align="center" nowrap>Emision</th>
				<th align="center" nowrap>Monto prestamo</th>
				<th align="center" nowrap>Monto Liquido</th>

    		</TR>
       </TABLE>
<div id="dataDiv1" class="scbarcolor" >
    <table id="dataTable" >       		
			<%
				EPV104001List.initRow();
					k = 0;
					boolean firstTime = true;
					String chk = "";
					while (EPV104001List.getNextRow()) {
						if (firstTime) {
							firstTime = false;
							chk = "checked";
						} else {
							chk = "";
						}
						EPV104001Message convObj = (EPV104001Message) EPV104001List
								.getRecord();
			%>
			<tr>
				<td nowrap><input type="radio" name="key" id="radio_key"
					value="<%=k%>" onclick="showAddInfo(<%=k%>,'<%=convObj.getE01CUSNA1()%>'); //setValue('<%=convObj.getE01CUSNA1()%>');"
					<%=chk%>></td>
				<td nowrap align="center"><a href="javascript:consulta_extra('900','<%=k%>','<%=convObj.getE01CUSNA1()%>');"><%= Util.formatCell(convObj.getE01PVDNUM())%></a></td>
				<td nowrap align="left"><a href="javascript:consulta_extra('900','<%=k%>','<%=convObj.getE01CUSNA1()%>');"><%= Util.formatCell(convObj.getE01CUSNA1())%></a></td>
				<td nowrap align="center"><a href="javascript:consulta_extra('900','<%=k%>','<%=convObj.getE01CUSNA1()%>');"><%=Util.formatCell(convObj.getE01PVDODD() + "/" + convObj.getE01PVDODM() + "/"+ convObj.getE01PVDODY())%></a></td>
				<td nowrap align="right"><a href="javascript:consulta_extra('900','<%=k%>','<%=convObj.getE01CUSNA1()%>');"><%= Util.formatCell(convObj.getE01PVDOAM())%></a></td>
				<td nowrap align="right"><a href="javascript:consulta_extra('900','<%=k%>','<%=convObj.getE01CUSNA1()%>');"><%= Util.formatCell(convObj.getE01PVDNET())%></a>
				<%
					String ls = "";
							ls += Util.formatCell(convObj.getE01PVDCUN()) + "<br>";
							ls += Util.formatCell(convObj.getE01PVDUUS()) + "<br>";
							ls += Util.fcolorCCY(convObj.getE01PVMRMK()) + "<br>";
				%>
				<input type="hidden" id="TXTDATA<%=k%>"  name="TXTDATA<%=k%>" value="<%=ls%>">
				</td>
			</tr>
			<%
			k++;
				}
			%>
 </table>
   </div>
		</td>

      <TD nowrap ALIGN="RIGHT" valign="top" > 
	       <Table id="tbAddInfoH"  width="100%" >
	        <tr id="trdark">
	            <TH ALIGN=CENTER nowrap > Informati&oacute;n B&aacute;sica</TH>
	        </tr>
	      </Table>
      
			<Table id="tbAddInfo" >
		      <tr id="trclear" >
		            <TD  ALIGN="RIGHT"  valign="center" nowrap >
		              <b>Numero de Cliente : <br>
		              Usuario : <br>
		              Remark :<br> </b>		              
		            </TD>
		         <TD ALIGN="LEFT" valign="center" nowrap class="tdaddinfo">hola</TD>
		      </tr>
		     </Table>
			
	</td>
	</tr>
</table>

<script type="text/javascript">
     //showAddInfo(0);          
</script> 
<br>
<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV104001List.getShowPrev()) {
					int pos = EPV104001List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSECIF010?SCREEN=3&NameSearch="
									//+ EPV104001List.getSearchText() + "&Type="
									//+ EPV104001List.getSearchType() + "&Pos=" + pos
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV104001List.getShowNext()) {
					int pos = EPV104001List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSECIF010?SCREEN=3&NameSearch="
									//	+ cifList.getSearchText() + "&Type="
									//	+ cifList.getSearchType() + "&Pos=" + pos
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/next_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>

</form>

<SCRIPT language="JavaScript">
  document.forms[0].totalRow.value="<%= k %>";
   function resizeDoc() {
       divResize(true);
       //adjustDifTables(headTable, dataTable, dataDiv1,2,1);
	   adjustEquTables(headTable, dataTable, dataDiv1,1,false);
       
  }
  showChecked("ACCNUM");
  resizeDoc();
  tbAddInfoH.rows[0].cells[0].height = headTable.rows[0].cells[0].clientHeight;
  window.onresize=resizeDoc;
   showAddInfo(0);   
 

  function goAction(op) {
	var ok = false;
	
    for(n=0; n<document.forms[0].elements.length; n++)
     {
     	var elementName= document.forms[0].elements[n].name;
     	if(elementName == "key") 
     	{
			if (document.forms[0].elements[n].checked == true) {
      			ok = true;
      			break;
			}
     	}
     }      
     if ( ok ) 
     { 
     	if (op == '200')
          var answer = confirm ("�Desea cursar solicitud para " +document.forms[0].selected_customer.value+" ?");
         
     	else if (op == '300')
           		var answer = confirm ("�Desea rechazar solicitud para " +document.forms[0].selected_customer.value+" ?"); 
         
		document.forms[0].SCREEN.value = op;
		if (answer)
     	{
    		document.forms[0].submit();	
		}
		
     } 
     else 
		alert("Debe seleccionar una solicitud para continuar.");	   
		
	}


   
</SCRIPT>
</body>
</html>
