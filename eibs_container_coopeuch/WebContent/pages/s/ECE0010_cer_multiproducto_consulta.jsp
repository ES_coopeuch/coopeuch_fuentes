<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="datapro.eibs.master.Util"%>
<html>
<head>
<title>Certificados</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ECE0010Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<jsp:useBean id="cerBasic" class="datapro.eibs.beans.ECE001001Message" scope="session" />

<script language="JavaScript">


function goAction(op) {

	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}

</SCRIPT>  

</head>

<BODY>
<h3 align="center">Consulta Constancia Multiproducto<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cer_multiproducto_consulta.jsp,ECE0010">
</h3> 
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECE0010" >
  <input type=HIDDEN name="SCREEN" value="2">
  <input type=HIDDEN name="opt" value="1">  
  <table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			class="tbhead">
			<tr id="trdark">
				<td nowrap width="16%">
				<div align="right"><b>Cliente :</b></div>
				</td>
				<td nowrap width="20%">
				<div align="left"><input type="text" name="E01SELCUN"
					size="10" maxlength="9" value="<%=userPO.getHeader1()%>">
				</div>
				</td>
				<td nowrap width="16%">
				<div align="right"><b>Nombre :</b></div>
				</td>
				<td nowrap colspan="3">
				<div align="left"><font face="Arial"><font face="Arial"><font
					size="2"> <input type="text" name="E01CERNAM" size="45"
					maxlength="45" value="<%=userPO.getHeader2()%>">
				</font></font></font></div>
				</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="16%">
				<div align="right"><b>Identificación :</b></div>
				</td>
				<td nowrap width="20%">
				<div align="left"><input type="text" name="E01CERIDN"
					size="10" maxlength="9" value="<%=userPO.getHeader3()%>">
				</div>
				</td>
				<td nowrap width="16%">
				<div align="right"><b>Socio desde :</b></div>
				</td>
		        <TD NOWRAP width="16%">		        
		        <input type="text" name="Header4" size="3" maxlength="2" value="<%=userPO.getHeader4()%>" readonly>
		        <input type="text" name="Header5" size="3" maxlength="2" value="<%=userPO.getHeader5()%>" readonly>
		        <input type="text" name="Header6" size="5" maxlength="4" value="<%=userPO.getHeader6()%>" readonly>
		        </TD>
			</tr>			
		</table>
		</td>
	</tr>
</table>
  
  
  <%
	if ( ECE0010Help.getNoResult()) {
 %>
  
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>Cliente No cuenta con Productos</b></p>
          <table class="tbenter" width=100% align=center>
           <tr>
              <td class=TDBKG>
                <div align="center"><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
              </td>
           </tr>
         </table>
	  </div>

	  </TD>
	</TR>
    </TABLE>
	
  <%  
}
else {
%> <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }
%> 
<h4>Cuotas de Participacion</h4>  
  <TABLE  id=cfTable class="tableinfo">
 <tr>
    <td NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="20%"> 
              <div align="center">Tipo de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Numero de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Fecha Apertura</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Saldo ($) </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Estado </div>
            </th>
          </tr>
          <%
                ECE0010Help.initRow();
				int idx = 0;
				String valStyle = "";
        		while (ECE0010Help.getNextRow()) {
                datapro.eibs.beans.ECE001001Message msgList = (datapro.eibs.beans.ECE001001Message) ECE0010Help.getRecord();
                if (msgList.getE01CERACD().equals("06")){
               		 if (idx++ % 2 != 0)
						valStyle = "trdark";
					else
						valStyle = "trclear";
		    %>
		    	<TR id="<%= valStyle %>">
				<TD NOWRAP  ALIGN=CENTER width=\"20%\"><%= msgList.getD01CERTIP() %></td>
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERCTA() %></td>
		        <TD NOWRAP align="center" width=\"15%\" ><%= Util.formatDate(msgList.getE01CERSDD(),msgList.getE01CERSMM(),msgList.getE01CERSAA())%></td>				
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERMTO() %></td>
			    <TD NOWRAP  ALIGN=CENTER width=\"15%\">
				    <INPUT type="HIDDEN" name="E01CERSTS" size="1" value="<%= msgList.getE01CERSTS().trim()%>">
							<% if (msgList.getE01CERSTS().equals("A")) out.print("Activa");
								 else out.print("Inactiva");%>
	 		       </TD>
 				</TR>
		     <%
               }}
             %>
        </table>
        </td>
        </tr>
    </table> 	
<h4>Cuenta Vista</h4>
  <TABLE  id=cfTable class="tableinfo">
    <tr>
    <td NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="20%"> 
              <div align="center">Tipo de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Numero de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Fecha Apertura</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Saldo ($) </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Estado </div>
            </th>
          </tr>
          <%
                ECE0010Help.initRow();
				idx = 0;
				valStyle = "";
        		while (ECE0010Help.getNextRow()) {
                datapro.eibs.beans.ECE001001Message msgList = (datapro.eibs.beans.ECE001001Message) ECE0010Help.getRecord();
                if (msgList.getE01CERACD().equals("01")){
               		 if (idx++ % 2 != 0)
						valStyle = "trdark";
					else
						valStyle = "trclear";
		    %>
		    	<TR id="<%= valStyle %>">
				<TD NOWRAP  ALIGN=CENTER width=\"20%\"><%= msgList.getD01CERTIP() %></td>
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERCTA() %></td>
		        <TD NOWRAP align="center" width=\"15%\" ><%= Util.formatDate(msgList.getE01CERSDD(),msgList.getE01CERSMM(),msgList.getE01CERSAA())%></td>				
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERMTO() %></td>
			    <TD NOWRAP  ALIGN=CENTER width=\"15%\">
				    <INPUT type="HIDDEN" name="E01CERSTS" size="1" value="<%= msgList.getE01CERSTS().trim()%>">
							<% if (msgList.getE01CERSTS().equals("A")) out.print("Activa");
								 else out.print("Inactiva");%>
	 		       </TD>				
 				</TR>
		     <%
               }}
             %>
        </table>
        </td>
        </tr>
    </table>
<h4>Cuentas de Ahorro</h4> 
  <TABLE  id=cfTable class="tableinfo">
  <tr> 
    <td NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="20%"> 
              <div align="center">Tipo de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Numero de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Fecha Apertura</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Saldo ($) </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Estado </div>
            </th>
          </tr>
          <%
                ECE0010Help.initRow();
				idx = 0;
				valStyle = "";
        		while (ECE0010Help.getNextRow()) {
                datapro.eibs.beans.ECE001001Message msgList = (datapro.eibs.beans.ECE001001Message) ECE0010Help.getRecord();
                if (msgList.getE01CERACD().equals("04")){
               		 if (idx++ % 2 != 0)
						valStyle = "trdark";
					else
						valStyle = "trclear";
		    %>
		    	<TR id="<%= valStyle %>">
				<TD NOWRAP  ALIGN=CENTER width=\"20%\"><%= msgList.getD01CERTIP() %></td>
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERCTA() %></td>
		        <TD NOWRAP align="center" width=\"15%\" ><%= Util.formatDate(msgList.getE01CERSDD(),msgList.getE01CERSMM(),msgList.getE01CERSAA())%></td>				
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERMTO() %></td>
			    <TD NOWRAP  ALIGN=CENTER width=\"15%\">
				    <INPUT type="HIDDEN" name="E01CERSTS" size="1" value="<%= msgList.getE01CERSTS().trim()%>">
							<% if (msgList.getE01CERSTS().equals("A")) out.print("Activa");
								 else out.print("Inactiva");%>
	 		       </TD>				
 				</TR>
		     <%
               }}
             %>
        </table>
        </td>
        </tr>
    </table>
<h4>Depositos a Plazo</h4>  
  <TABLE  id=cfTable class="tableinfo">
 <tr>
    <td NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="20%"> 
              <div align="center">Tipo de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Numero de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Fecha Apertura</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Saldo ($) </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Estado </div>
            </th>
          </tr>
          <%
                ECE0010Help.initRow();
				idx = 0;
				valStyle = "";
        		while (ECE0010Help.getNextRow()) {
                datapro.eibs.beans.ECE001001Message msgList = (datapro.eibs.beans.ECE001001Message) ECE0010Help.getRecord();
                if (msgList.getE01CERACD().equals("11") || msgList.getE01CERACD().equals("12")){
               		 if (idx++ % 2 != 0)
						valStyle = "trdark";
					else
						valStyle = "trclear";
		    %>
		    	<TR id="<%= valStyle %>">
				<TD NOWRAP  ALIGN=CENTER width=\"20%\"><%= msgList.getD01CERTIP() %></td>
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERCTA() %></td>
		        <TD NOWRAP align="center" width=\"15%\" ><%= Util.formatDate(msgList.getE01CERSDD(),msgList.getE01CERSMM(),msgList.getE01CERSAA())%></td>				
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERMTO() %></td>
			    <TD NOWRAP  ALIGN=CENTER width=\"15%\">
					<INPUT type="HIDDEN" name="E01CERSTS" size="1" value="<%= msgList.getE01CERSTS().trim()%>">
							<% if (msgList.getE01CERSTS().equals("1")) out.print("Vigente");
								 else out.print("Vencido");%>
	 		       </TD>	
 				</TR>
		     <%
               }}
             %>
        </table>
        </td>
        </tr>
    </table> 
<h4>Creditos Estudios Superiores</h4>
  <TABLE  id=cfTable class="tableinfo">
 <tr>
    <td NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="20%"> 
              <div align="center">Tipo de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Numero de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Fecha Apertura</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Saldo ($) </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Estado </div>
            </th>
          </tr>
          <%
                ECE0010Help.initRow();
				idx = 0;
				valStyle = "";
        		while (ECE0010Help.getNextRow()) {
                datapro.eibs.beans.ECE001001Message msgList = (datapro.eibs.beans.ECE001001Message) ECE0010Help.getRecord();
                if (msgList.getE01CERACD().equals("10") && (msgList.getE01CERTCR().equals("2"))){
               		 if (idx++ % 2 != 0)
						valStyle = "trdark";
					else
						valStyle = "trclear";
		    %>
		    	<TR id="<%= valStyle %>">
				<TD NOWRAP  ALIGN=CENTER width=\"20%\"><%= msgList.getD01CERTIP() %></td>
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERCTA() %></td>
		        <TD NOWRAP align="center" width=\"15%\" ><%= Util.formatDate(msgList.getE01CERSDD(),msgList.getE01CERSMM(),msgList.getE01CERSAA())%></td>				
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERMTO() %></td>
			    <TD NOWRAP  ALIGN=CENTER width=\"15%\">
				    <INPUT type="HIDDEN" name="E01CERSTS" size="1" value="<%= msgList.getE01CERSTS().trim()%>">
							<% if (msgList.getE01CERSTS().equals("1")) out.print("Vigente");
								 else if (msgList.getE01CERSTS().equals("2")) out.print("Vencido");
								 else if (msgList.getE01CERSTS().equals("3")) out.print("Castigado");
             					 else if (msgList.getE01CERSTS().equals("4")) out.print("Castigado No Inf.");
			    				 else if (msgList.getE01CERSTS().equals("5")) out.print("Avenimiento"); %>
	 		    </TD> 				
	 		 </TR>
		     <%
               }}
             %>
        </table>
        </td>
        </tr>
    </table> 
    
<h4>Creditos Consumo/Comerciales</h4>
  <TABLE  id=cfTable class="tableinfo">
 <tr> 
    <td NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="20%"> 
              <div align="center">Tipo de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Numero de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Fecha Apertura</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Saldo ($) </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Estado </div>
            </th>
          </tr>
          <%
                ECE0010Help.initRow();
				idx = 0;
				valStyle = "";
        		while (ECE0010Help.getNextRow()) {
                datapro.eibs.beans.ECE001001Message msgList = (datapro.eibs.beans.ECE001001Message) ECE0010Help.getRecord();
                if (msgList.getE01CERACD().equals("10") && (!msgList.getE01CERTCR().equals("2"))){
               		 if (idx++ % 2 != 0)
						valStyle = "trdark";
					else
						valStyle = "trclear";
		    %>
		    	<TR id="<%= valStyle %>">
				<TD NOWRAP  ALIGN=CENTER width=\"20%\"><%= msgList.getD01CERTIP() %></td>
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERCTA() %></td>
		        <TD NOWRAP align="center" width=\"15%\" ><%= Util.formatDate(msgList.getE01CERSDD(),msgList.getE01CERSMM(),msgList.getE01CERSAA())%></td>				
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERMTO() %></td>
			    <TD NOWRAP  ALIGN=CENTER width=\"15%\">
				    <INPUT type="HIDDEN" name="E01CERSTS" size="1" value="<%= msgList.getE01CERSTS().trim()%>">
							<% if (msgList.getE01CERSTS().equals("1")) out.print("Vigente");
								 else if (msgList.getE01CERSTS().equals("2")) out.print("Vencido");
								 else if (msgList.getE01CERSTS().equals("3")) out.print("Castigado");
             					 else if (msgList.getE01CERSTS().equals("4")) out.print("Castigado No Inf.");
			    				 else if (msgList.getE01CERSTS().equals("5")) out.print("Avenimiento"); %>
	 		    </TD> 				
	 		 </TR>
		     <%
               }}
             %>
        </table>
        </td>
        </tr>        
 </table>     
<h4>Creditos Hipotecarios</h4>
  <TABLE  id=cfTable class="tableinfo">
 <tr> 
     <td NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="20%"> 
              <div align="center">Tipo de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Numero de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Fecha Apertura</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Saldo ($) </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Estado </div>
            </th>
          </tr>
          <%
                ECE0010Help.initRow();
				idx = 0;
				valStyle = "";
        		while (ECE0010Help.getNextRow()) {
                datapro.eibs.beans.ECE001001Message msgList = (datapro.eibs.beans.ECE001001Message) ECE0010Help.getRecord();
                if (msgList.getE01CERACD().equals("13")){
               		 if (idx++ % 2 != 0)
						valStyle = "trdark";
					else
						valStyle = "trclear";
		    %>
		    	<TR id="<%= valStyle %>">
				<TD NOWRAP  ALIGN=CENTER width=\"20%\"><%= msgList.getD01CERTIP() %></td>
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERCTA() %></td>
		        <TD NOWRAP align="center" width=\"15%\" ><%= Util.formatDate(msgList.getE01CERSDD(),msgList.getE01CERSMM(),msgList.getE01CERSAA())%></td>				
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERMTO() %></td>
			    <TD NOWRAP  ALIGN=CENTER width=\"15%\">
				    <INPUT type="HIDDEN" name="E01CERSTS" size="1" value="<%= msgList.getE01CERSTS().trim()%>">
							<% if (msgList.getE01CERSTS().equals("A")) out.print("Activo");						 
			    				 else out.print("Inactivo"); %>
	 		       </TD>
	 		    </TR>
		     <%
               }}
             %>
        </table>
        </td>
        </tr>        
    </table>     
<h4>Tarjetas de Credito</h4>
  <TABLE  id=cfTable class="tableinfo">
 <tr> 
     <td NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="20%"> 
              <div align="center">Tipo de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Numero de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Fecha Apertura</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Saldo ($) </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Estado </div>
            </th>
          </tr>
          <%
                ECE0010Help.initRow();
				idx = 0;
				valStyle = "";
        		while (ECE0010Help.getNextRow()) {
                datapro.eibs.beans.ECE001001Message msgList = (datapro.eibs.beans.ECE001001Message) ECE0010Help.getRecord();
                if (msgList.getE01CERACD().equals("94")){
               		 if (idx++ % 2 != 0)
						valStyle = "trdark";
					else
						valStyle = "trclear";
		    %>
		    	<TR id="<%= valStyle %>">
				<TD NOWRAP  ALIGN=CENTER width=\"20%\"><%= msgList.getD01CERTIP() %></td>
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERCTA() %></td>
		        <TD NOWRAP align="center" width=\"15%\" ><%= Util.formatDate(msgList.getE01CERSDD(),msgList.getE01CERSMM(),msgList.getE01CERSAA())%></td>				
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= msgList.getE01CERMTO() %></td>
			    <TD NOWRAP  ALIGN=CENTER width=\"15%\">
				    <INPUT type="HIDDEN" name="E01CERSTS" size="1" value="<%= msgList.getE01CERSTS().trim()%>">
							<% if (msgList.getE01CERSTS().equals("A")) out.print("Activa");					 
			    				 else out.print("Inactiva"); %>
	 		       </TD>
 				</TR>
		     <%
               }}
             %>
        </table>
        </td>
        </tr>        
    </table> 
<%
 }
%>    
  <table class="tbenter" width=100% align=center>
    <tr> 
      <td class=TDBKG width="33%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Imprimir<br>Certificado</b></a></div>
      </td>
      <td class=TDBKG width="33%"> 
        <div align="center"><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
      </td>
    </tr>
  </table>
<SCRIPT language="JavaScript">
  
  showChecked("CURRCODE");
  
 function tableresize() {
    adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  tableresize();
  window.onresize=tableresize;   
</SCRIPT>
</form>

</body>
</html>
