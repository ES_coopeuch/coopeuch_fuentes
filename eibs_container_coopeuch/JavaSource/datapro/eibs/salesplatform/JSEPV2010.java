package datapro.eibs.salesplatform;

/**
 * Plataforma de Ventas - ingreso / mantenimiento y simulacion de planillas
 * @author evargas       
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.coopeuch.core.tiendavirtual.ActualizaStockEntrada;
import cl.coopeuch.core.tiendavirtual.RespActualizaStock;
import cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtualSOAPProxy;
import datapro.eibs.beans.EDD009001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100500Message;
import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV100503Message;
import datapro.eibs.beans.EPV101001Message;
import datapro.eibs.beans.EPV101002Message;
import datapro.eibs.beans.EPV101004Message;
import datapro.eibs.beans.EPV101010Message;
import datapro.eibs.beans.EPV117001Message;
import datapro.eibs.beans.EPV170001Message;
import datapro.eibs.beans.EPV200500Message;
import datapro.eibs.beans.EPV200501Message;
import datapro.eibs.beans.EPV201002Message;
import datapro.eibs.beans.EPV201004Message;
import datapro.eibs.beans.EPV201013Message;

import datapro.eibs.beans.EPV201001Message;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageRecord;

public class JSEPV2010 extends JSEIBSServlet {
 
	private static final long serialVersionUID = -5013250952357918505L;
	
	protected static final int R_PLATFORM_LIST = 100;	
	protected static final int A_PLATFORM_LIST = 101;
	
	protected static final int R_PLATFORM_MAINT = 201;
	protected static final int R_PLATFORM_MAINT_BACK = 202;
	
	protected static final int A_PLATFORM_CARD_MAINTENANCE = 500;
	protected static final int A_PLATFORM_MAINTENANCE = 600;
	protected static final int A_PLATFORM_SIMULATE = 601;
	protected static final int A_PLATFORM_SIMULATE2 = 602;
	protected static final int A_PLATFORM_SIMULATE3 = 603;		
	protected static final int A_PLATFORM_LIQUIDACION = 700;
	protected static final int A_PLATFORM_DESEMBOLSO = 701;
	protected static final int A_PLATFORM_RESPUESTA_ENGINE = 900;
		
	protected static final int A_PLATFORM_PENDINGAPR = 1001;
	
	
	
	
	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
			case R_PLATFORM_LIST:
				procReqPlatformList(user, req, res, session);
				break;
			case A_PLATFORM_LIST:
				procActionPlatformList(user, req, res, session);
				break;
			case R_PLATFORM_MAINT:
				procReqPlatform(user, req, res, session);
				break;
			case R_PLATFORM_MAINT_BACK:
				procReqPlatform_Back(user, req, res, session);
				break;				
			case A_PLATFORM_CARD_MAINTENANCE:
				procActionCardMaintenance(user, req, res, session);
				break;
			case A_PLATFORM_MAINTENANCE:
				
				procActionMaintenance(user, req, res, session);
				break;
			case A_PLATFORM_LIQUIDACION:
				procActionPlatformLiquidacion(user, req, res, session);
				break;
			case A_PLATFORM_DESEMBOLSO:
				procActionDesembolso(user, req, res, session);
				break;
			case A_PLATFORM_RESPUESTA_ENGINE:
				procActionResponseEngine(user, req, res, session);
			break;			
			case A_PLATFORM_SIMULATE:
				procActionSimulation(user, req, res, session);
				break;
			case A_PLATFORM_SIMULATE2:
				procActionSimulation2(user, req, res, session);
				break;
			case A_PLATFORM_SIMULATE3:
				procActionSimulation3(user, req, res, session);
				break;		
			case A_PLATFORM_PENDINGAPR :
				procActionPendingApr(user, req, res, session);
				break;		
			default:
				forward(SuperServlet.devPage, req, res);
				break;
		}
	}

	
	/**
	 * procReqPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
		UserPos userPO = getUserPos(ses);
		userPO.setRedirect("/servlet/datapro.eibs.salesplatform.JSEPV2010?SCREEN=101");
		userPO.setHeader1("Plataforma de Ventas - Evaluación");
		ses.setAttribute("userPO", userPO);
		forward("EPV1000_client_enter.jsp", req, res);
	}
	
	
	/**
	 * procActionPlatformList: find the list of forms depending on status, the program will epvl1005
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		
		String customer = req.getParameter("E01CUN") == null ? "0" : req.getParameter("E01CUN").trim();
		
		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV2005", req);
			EPV200501Message msgList = (EPV200501Message) mp.getMessageRecord("EPV200501", user.getH01USR(), "0015");
			
			msgList.setE01SELCUN(customer);
			msgList.setE01SELSTS("2");
			// Sends message
			mp.sendMessage(msgList);

			// Receive salesPlatform list
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			//
			if (mp.hasError(msgError)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", msgError);
				forward("EPV1000_client_enter.jsp", req, res);								
			} else {
				EPV200500Message header = (EPV200500Message) list.get(0);	
				userPO.setCusNum(header.getE00CUSCUN());
				userPO.setCusType(header.getE00CUSIDN());
				userPO.setCusName(header.getE00CUSNA1());
				session.setAttribute("userPO", userPO);
				list.remove(0);
				// save customer number
				session.setAttribute("EPV200501List", list);
				
				if (list.size()==0){//then no occurrences
					if (!"S".equals(req.getParameter("fin"))){//Only show when not fin
						msgError = new ELEERRMessage();
						msgError.setERRNUM("1");
						msgError.setERNU01("0001");		//4		                
						msgError.setERDS01("No existen solicitudes pendiente para Evaluar de este Cliente");//70					
						session.setAttribute("error", msgError);						
					}
					procReqPlatformList(user,req,res,session);					
				}else if (list.size()>1){//then at least 2 occurrences
					forward("EPV1010_salesplatform_list.jsp", req, res);
				}else{
					redirect("datapro.eibs.salesplatform.JSEPV2010?SCREEN=201&E01PVMNUM="+ msgList.getE01PVMNUM(), res);	
				}
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	

	/**
	 * Process the simulation for request
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionSimulation(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;

		try {			
			mp = getMessageProcessor("EPV2010", req);
			EPV201001Message msg = (EPV201001Message) mp.getMessageRecord("EPV201001", user.getH01USR(), "0012");			

			
			// Sets message with page fields
			setMessageRecord(req, msg);

			//JLS PRICING 

			// 
			//<CONSULTA SERVICIO PRICING TASA PIZARRA>
			//

			
			msg.setE01PRIPL1("450");
			msg.setE01PRIPL2("852");
			msg.setE01PRIPL3("1212");
			msg.setE01PRIPL4("1572");
			msg.setE01PRIPL5("1932");
			msg.setE01PRIPL6("2652");
		
			msg.setE01PRIMT1("12000");  
			msg.setE01PRIMT2("30000");  
			msg.setE01PRIMT3("50000");
			msg.setE01PRIMT4("800000");  
			msg.setE01PRIMT5("100000");  
			msg.setE01PRIMT6("9999999");  

			msg.setE01PRI101("2.1000"); 
			msg.setE01PRI102("2.3800");  
			msg.setE01PRI103("2.7400");  
			msg.setE01PRI104("2.8400");  
			msg.setE01PRI105("2.9700");  
			msg.setE01PRI106("2.9700");  

			msg.setE01PRI201("1.97000"); 
			msg.setE01PRI202("2.1700");  
			msg.setE01PRI203("2.2600");  
			msg.setE01PRI204("2.3900");  
			msg.setE01PRI205("2.3900");  
			msg.setE01PRI206("2.3900");  
			
			msg.setE01PRI301("1.7400"); 
			msg.setE01PRI302("2.0100");  
			msg.setE01PRI303("2.1500");  
			msg.setE01PRI304("2.3400");  
			msg.setE01PRI305("2.3900");  
			msg.setE01PRI306("2.3900");  
			
			msg.setE01PRI401("2.1000"); 
			msg.setE01PRI402("2.3800");  
			msg.setE01PRI403("2.7400");  
			msg.setE01PRI404("2.8400");  
			msg.setE01PRI405("2.9700");  
			msg.setE01PRI406("2.9700");  

			msg.setE01PRI501("1.97000"); 
			msg.setE01PRI502("2.1700");  
			msg.setE01PRI503("2.2600");  
			msg.setE01PRI504("2.3900");  
			msg.setE01PRI505("2.3900");  
			msg.setE01PRI506("2.3900");  
			
			msg.setE01PRI601("1.7400"); 
			msg.setE01PRI602("2.0100");  
			msg.setE01PRI603("2.1500");  
			msg.setE01PRI604("2.3400");  
			msg.setE01PRI605("2.3900");  
			msg.setE01PRI606("2.3900");  		
			
			//JLS PRICING 
			// Sending message
			
	
			
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV201001Message) mp.receiveMessageRecord("EPV201001");
			if (mp.hasError(msgError)) {
				msg.setH01FLGWK2("");//si tiene error sigo simulando	
			}else{
				msg.setH01FLGWK2("X");//NO hubo error simulo
			}
			msg.setH01FLGWK3("");//desmarco si hubo simulacion2, ya que tiene que volver a presionar simular1
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("platfObj", msg);// seteamos nuevamente con la informacion obtenida
			req.setAttribute("SIMULAR", "S");
			forward("EPV2010_salesplatform_maintenance.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * Process the simulation2 for request
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionSimulation2(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		MessageProcessor mp = null;
		UserPos userPO = getUserPos(session);
		
		//JLS PRICING 
		
		String  nivE01 = "";
		String  nivE02 = "";
		String  nivE03 = "";
		String  nivE04 = "";

		double  nivT01 = 0;
		double  nivT02 = 0;
		double  nivT03 = 0;
		double  nivT04 = 0;

		//JLS PRICING 

		try {		
			mp = getMessageProcessor("EPV2010", req);
			EPV201001Message msg = (EPV201001Message) mp.getMessageRecord("EPV201001", user.getH01USR(), "0014");			

			// Sets message with page fields
			setMessageRecord(req, msg);

			// Sending message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV201001Message) mp.receiveMessageRecord("EPV201001");

			//JLS PRICING 

			if (!mp.hasError(msgError)) {

				session.setAttribute("platfObj", msg);
				
				//LLAMADA A PRICING PARA OBTENCION DE TASAS
				
				nivE01 = "Nivel Ejecutivo";
				nivE02 = "Nivel Agente";
				nivE03 = "Nivel Agente Reg.";
				nivE04 = "Nivel Gerente Suc.";

				nivT01 = 2.23;
				nivT02 = 1.78;
				nivT03 = 1.63;
				nivT04 = 1.58;
				
				session.setAttribute("E01PRITPI", "2.78");
				session.setAttribute("E01PRITMA", "2.90");				
				session.setAttribute("E01PRITSU", "2.50");
				session.setAttribute("E01PRITPR", "1.90");
				session.setAttribute("E01PRITCD", "998765");
				
				session.setAttribute("E01PRIN1E", "Nivel Ejecutivo");
				session.setAttribute("E01PRITN1", "2.23");				
				session.setAttribute("E01PRIN1H", "   0");

				session.setAttribute("E01PRIN2E", "Nivel Agente");
				session.setAttribute("E01PRITN2", "1.78");				
				session.setAttribute("E01PRIN2H", "13,786");

				session.setAttribute("E01PRIN3E", "Nivel Agente Reg.");
				session.setAttribute("E01PRITN3", "1.63");				
				session.setAttribute("E01PRIN3H", "12,768");

				session.setAttribute("E01PRIN4E", "Nivel Gerente Suc.");
				session.setAttribute("E01PRITN4", "1.58");				
				session.setAttribute("E01PRIN4H", "11,387");

				//GUARDAR LOS DATOS DE LA SIMULACION PARA GRABAR AL FINAL DEL CICLO.
				
				EPV201013Message msg13 = new EPV201013Message();
				
				
				msg13.setE13PVMNUM(msg.getE01PVMNUM());
				msg13.setE13PVMCUN(msg.getE01PVMCUN());

				msg13.setE13CODRET("0");
				msg13.setE13GLSRET("PROCESO EXITOSO");
				msg13.setE13SIRATE(msg.getE01SIRATE());
				msg13.setE13TASPIZ("2.5");
				msg13.setE13TASSUG("2.4");
				msg13.setE13TASMAX("3.0");
				msg13.setE13TASMIN("1.0");
				msg13.setE13TASMPR("2.3");
				msg13.setE13NOMMPR("MEJOR PRECIO");
				msg13.setE13TIPRES("0");
				msg13.setE13PLAZOO("365");
				msg13.setE13MONTOO("1000");
				msg13.setE13MONEDO("CLP");
				msg13.setE13ORDENO("1");
				msg13.setE13IDROLO("123456789");
				msg13.setE13DESROO("DES. 123456789");
				msg13.setE13VALORO("1.6");
				msg13.setE13HOLGUO("15000");
				
				msg13.setH13FLGWK1("P"); //pendiente de aprobación de tasa
				
				session.setAttribute("ObjEPV201013", msg13);
				
				
				
				//
				
				msg = (EPV201001Message) session.getAttribute("platfObj");
				
				if(userPO.getHeader15().equals("Y") ||  !req.getParameter("H02FLGWK3").equals("X"))
				{
					msg.setE01SIRATE("2.50");
					if(userPO.getHeader15().equals("Y"))
						userPO.setHeader15("");
				}
				
				mp.sendMessage(msg);
				
				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msg = (EPV201001Message) mp.receiveMessageRecord("EPV201001");

			
			
				double TasaCalculo = Double.parseDouble(msg.getE01SIRATE());
	
			
				if(TasaCalculo<=nivT01 && TasaCalculo>=nivT02 )
				{
					session.setAttribute("E01INDAUT", "X");
					session.setAttribute("E01MSGAUT", nivE01);				
				}
				else 
				{
					if(TasaCalculo<=nivT02 && TasaCalculo>=nivT03 )
					{
						session.setAttribute("E01INDAUT", "X");
						session.setAttribute("E01MSGAUT", nivE02);				
					}
					else 
					{
						if(TasaCalculo<=nivT03 && TasaCalculo>=nivT04 )
						{
							session.setAttribute("E01INDAUT", "X");
							session.setAttribute("E01MSGAUT", nivE03);				
						}
						else 
						{
							if(TasaCalculo<=nivT04 )
							{
								session.setAttribute("E01INDAUT", "X");
								session.setAttribute("E01MSGAUT", nivE04);			
							}
						}
					}	
			
				}	
			}
			//JLS PRICING			
			msg.setH01FLGWK2("X");//marco que la simulacion 1 esta lista.
			
			if (mp.hasError(msgError)) {
				msg.setH01FLGWK3("");//si tiene error sigo simulando	
			}else{
				msg.setH01FLGWK3("X");//NO hubo error simulo
			}

			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("platfObj", msg);// seteamos nuevamente con la informacion obtenida				
		
			req.setAttribute("SIMULAR3", "S");	
			forward("EPV2010_salesplatform_maintenance.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionSimulation3(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		MessageProcessor mp = null;

		try {		
			mp = getMessageProcessor("EPV1010", req);
			EPV101001Message msg = (EPV101001Message) mp.getMessageRecord("EPV101001", user.getH01USR(), "0008");			

			// Sets message with page fields
			setMessageRecord(req, msg);

			// Sending message
			mp.sendMessage(msg);

			// Receive list data
			JBObjList list = mp.receiveMessageRecordList("H11FLGMAS");
			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV101001Message) mp.receiveMessageRecord("EPV101001");			

			
			msg.setH01FLGWK2("X");//marco que la simulacion 1 esta lista.
			
			if (mp.hasError(msgError)) {
				msg.setH01FLGWK3("");//si tiene error sigo simulando	
			}else{
				msg.setH01FLGWK3("X");//NO hubo error simulo
			}
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("platfObj", msg);// seteamos nuevamente con la informacion obtenida				
			session.setAttribute("planPag", list);
			
			req.setAttribute("SIMULAR3", "S");	
			forward("EPV1010_salesplatform_maintenance.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	
	/**
	 * procReqPlatform: This Method show a single Sales Platform for evaluation process.
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatform(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,   
			IOException {

		MessageProcessor mp = null;
		EPV201013Message msg13 = new EPV201013Message();
		
		try {
			UserPos userPO = getUserPos(session);
			userPO.setPurpose("MAINTENANCE");
			mp = getMessageProcessor("EPV2010", req);

			// Sets the platform number for maintenance
			String dop = req.getParameter("DOP");
			dop  = (dop==null)?"":dop;
			
			String opecode = "C".equals(dop) ? "0007" : "0002"; // 0007: Maintenance credit cesante, 0002: Maintenance wherever credit 
			EPV201001Message msg = (EPV201001Message) mp.getMessageRecord("EPV201001", user.getH01USR(), opecode);				


			// Sets the client number
			msg.setE01PVMCUN(userPO.getCusNum());

			// Sets the platform number for maintenance
			if (req.getParameter("E01PVMNUM") != null) {
				msg.setE01PVMNUM(req.getParameter("E01PVMNUM"));
			}
			//JLS PRICING 

			// Carga de datos tabla pizarra. PARA EL CALCULO DEL MONTO BRUTO>>> 
			
			msg.setE01PRIPL1("450");
			msg.setE01PRIPL2("852");
			msg.setE01PRIPL3("1212");
			msg.setE01PRIPL4("1572");
			msg.setE01PRIPL5("1932");
			msg.setE01PRIPL6("2652");
		
			msg.setE01PRIMT1("12000");  
			msg.setE01PRIMT2("30000");  
			msg.setE01PRIMT3("50000");
			msg.setE01PRIMT4("800000");  
			msg.setE01PRIMT5("100000");  
			msg.setE01PRIMT6("9999999");  

			msg.setE01PRI101("2.1000"); 
			msg.setE01PRI102("2.3800");  
			msg.setE01PRI103("2.7400");  
			msg.setE01PRI104("2.8400");  
			msg.setE01PRI105("2.9700");  
			msg.setE01PRI106("2.9700");  

			msg.setE01PRI201("1.97000"); 
			msg.setE01PRI202("2.1700");  
			msg.setE01PRI203("2.2600");  
			msg.setE01PRI204("2.3900");  
			msg.setE01PRI205("2.3900");  
			msg.setE01PRI206("2.3900");  
			
			msg.setE01PRI301("1.7400"); 
			msg.setE01PRI302("2.0100");  
			msg.setE01PRI303("2.1500");  
			msg.setE01PRI304("2.3400");  
			msg.setE01PRI305("2.3900");  
			msg.setE01PRI306("2.3900");  
			
			msg.setE01PRI401("2.1000"); 
			msg.setE01PRI402("2.3800");  
			msg.setE01PRI403("2.7400");  
			msg.setE01PRI404("2.8400");  
			msg.setE01PRI405("2.9700");  
			msg.setE01PRI406("2.9700");  

			msg.setE01PRI501("1.97000"); 
			msg.setE01PRI502("2.1700");  
			msg.setE01PRI503("2.2600");  
			msg.setE01PRI504("2.3900");  
			msg.setE01PRI505("2.3900");  
			msg.setE01PRI506("2.3900");  
			
			msg.setE01PRI601("1.7400"); 
			msg.setE01PRI602("2.0100");  
			msg.setE01PRI603("2.1500");  
			msg.setE01PRI604("2.3400");  
			msg.setE01PRI605("2.3900");  
			msg.setE01PRI606("2.3900");  		
			
			session.setAttribute("E01INDAUT", "");
			session.setAttribute("E01MSGAUT", "");	
			
			//JLS PRICING 
			// Send message
			mp.sendMessage(msg);
			
			if ("C".equals(dop)) {
				// Receive list data
				JBObjList list = mp.receiveMessageRecordList("H11FLGMAS");
				session.setAttribute("planPag", list);				
			}
			
			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			MessageRecord newmessage = mp.receiveMessageRecord();
			
			//JLS PRICING 
			userPO.setHeader15("");
			if(newmessage.getField("E01PVMSTS").getString().equals("A"))
				userPO.setHeader15("Y"); //indica que debe simular primera vez con tasa sugerida prising 
			
			//JLS PRICING 
			
			
			
			if (!mp.hasError(msgError)) {
				session.setAttribute("platfObj", newmessage);// set header page
				// if there are no errors go to maintenance page
				if (newmessage instanceof EPV201001Message) {					
					msgError = buscarPromocionesDescuentos(user,mp,session,((EPV201001Message)newmessage).getE01LNTYPG(), userPO.getCusNum());
					if (mp.hasError(msgError)) {
						session.setAttribute("error", msgError);
						forward("EPV2010_salesplatform_list.jsp", req, res);
					} else {
						
						
						if(newmessage.getField("E01PVMSTS").getString().equals("A"))
						{
							msg.setH01FLGWK3("");
							forward("EPV2010_salesplatform_maintenance.jsp", req, res);
						} else {
							msg13.setE13PVMNUM(msg.getE01PVMNUM());
							msg13.setE13PVMCUN(msg.getE01PVMCUN());

							msg13.setE13TASPIZ(msg.getE01TASPIZ());
							msg13.setE13TASSUG(msg.getE01TASSUG());
							msg13.setE13TASMAX(msg.getE01TASMAX());
							msg13.setE13TASMIN(msg.getE01TASMIN());
							msg13.setE13TASMPR(msg.getE01TASMPR());
							msg13.setE13NOMMPR(msg.getE01NOMMPR());
							
							session.setAttribute("ObjEPV201013", msg13);
							
							
							procActionSimulation2PRIMA(user, req, res, session);
						}
					}
				} else {
					forward("EPV2010_salesplatform_cards_maintenance.jsp", req, res);
				}
			} else {
				// if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV2010_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}	

	protected void procReqPlatform_Back(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {
		
			MessageRecord newmessage = (MessageRecord)session.getAttribute("platfObj");				
			if (newmessage instanceof EPV201001Message) {									
				forward("EPV2010_salesplatform_maintenance.jsp", req, res);					
			} else {
				forward("EPV2010_salesplatform_cards_maintenance.jsp", req, res);
			}
	}
	
	/**
	 * metodo que busca la lista de productos y promociones existentes programa epv1005 
	 * @param user
	 * @param mp
	 * @param session
	 * @throws IOException
	 */
	private ELEERRMessage buscarPromocionesDescuentos(ESS0030DSMessage user,MessageProcessor mp, 
						HttpSession session, String type, String NumCli) throws IOException {
		
		//Buscar Listas de Promociones y Descuentos
		EPV100503Message msg1 = null;				
		msg1 = (EPV100503Message) mp.getMessageRecord("EPV100503", user.getH01USR(), "0001");
		type = ("E".equals(type)?"D":type);
		msg1.setE03PVCREC(type);
		msg1.setE03PVCCUN(NumCli);
		mp.sendMessage(msg1);
		JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");	
		if (!mp.hasError(list)) {
			session.setAttribute("EPV100503List", list);					
			return new ELEERRMessage();
		} else {
			return (ELEERRMessage) mp.getError(list);
		}
	}
	
	
	/**
	 * Process the Maintenance
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked", "null" })
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		MessageProcessor mp2 = null;
		boolean firstTime = true;
		StringBuffer myRow = null;
		ELEERRMessage msgError = null;
		
			
		
		EPV201001Message platfObj = (EPV201001Message)session.getAttribute("platfObj");
		
		try {
			mp = getMessageProcessor("EPV2010", req);
			String opecode = "C".equals(platfObj.getE01LNTYPG()) ? "0009" : "0005";
			EPV201001Message msg = (EPV201001Message) mp.getMessageRecord("EPV201001", user.getH01USR(), opecode);				

			// Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			msg.setH01FLGWK2("X");//marco que la simulacion 1 esta lista. Pcataldo
			msg.setH01FLGWK3("X");//NO hubo error simulo                  P.Cataldo
		
			mp.sendMessage(msg);
			
			//Receive error and data
			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV201001Message) mp.receiveMessageRecord("EPV201001");

			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("platfObj", msg);
			
			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and sho errors
				forward("EPV2010_salesplatform_maintenance.jsp", req, res);			
			} 
			else 
			{


					if (!"Y".equals(msg.getE01TPRFEV())){// type don't call engine			
							// if there are no errors go back to list
							redirect("datapro.eibs.salesplatform.JSEPV2010?SCREEN=700&customer_number="+ msg.getE01PVMCUN()+"&solicitud_number="+ msg.getE01PVMNUM(), res);					
					} 
					else 
					{					
						// if there are no errors  --> call desicion engine
						JMSMotorClient jms =  new JMSMotorClient();
						//choice target depending of flag 
						String target = ("Y".equals(msg.getE01LNFRNG()))?msg.getE01TPRBOR():msg.getE01TPRBOB();
						String planilla = msg.getE01PVMNUM();
						String cliente =  msg.getE01PVMCUN();
						String tieneAval=msg.getE01INDAVA();//A posee aval...
					
						ELEERRMessage msgError2 = jms.sendMessage(planilla,target,user.getH01USR());	
					
						if (!"0".equals(msgError2.getERRNUM()))
						{	
							// if there are errors go back to maintenance page and show errors
							session.setAttribute("error", msgError2);					
							forward("EPV2010_salesplatform_maintenance.jsp", req, res);		
						}
						else
						{			
							// if there are no errors, parse engine response					
							List lista = jms.parseXMLResponseJB();
							ELEERRMessage msgError3 = (ELEERRMessage)lista.get(0);
							EPV201004Message m = (EPV201004Message)lista.get(1);
						
							if (!"0".equals(msgError3.getERRNUM()))
							{ 
								// if there are errors go back to maintenance page and show errors
								session.setAttribute("error", msgError3);					
								forward("EPV2010_salesplatform_maintenance.jsp", req, res);		
							}
							else
							{
								//Motivos de rechazos comerciales
								JBList beanList = new JBList();
							
								mp2 = getMessageProcessor("EPV1700", req);
								EPV170001Message msg2 = (EPV170001Message) mp2.getMessageRecord("EPV170001", user.getH01USR(), "0001");
							
								msg2.setE01PVENUM(msg.getE01PVMNUM());
							
								mp2.sendMessage(msg2);	
							
								while (true) 
								{							
									msg2 = (EPV170001Message) mp2.receiveMessageRecord("EPV170001");
								
									if ("*".equals(msg2.getH60FLGMAS())) 
									{
										break;
									}
									else
									{
										if (firstTime) 
										{
											firstTime = false;										
										}
										myRow = new StringBuffer();
										myRow.append(msg2.getS01PVEMRE());
										beanList.addRow("", myRow.toString());
									}
								}							 
							
								session.setAttribute("EPV170001List", beanList);						
							
								//verificamos si requiere aval si lo poseemos enviamos segunda llamada motor 
								//de lo contrario enviamos error y devolvemos a la pagina de mantenimiento
								if ("S".equals(m.getE04INDAVA()))
								{
									if (!"A".equals(tieneAval))
									{//has not guarantor								
										ELEERRMessage msgError4 = new ELEERRMessage();
										msgError4.setERRNUM("1");
										msgError4.setERNU01("01");//4
										msgError4.setERDS01("Motor Requiere Aval, Favor Introducir!!!");//70	
										session.setAttribute("error", msgError4);					
										//anyway continue the flow.
										m.setE04PVMCUN(cliente);
										m.setE04PVMNUM(planilla);
										m.setE04CLIAVA("C");
									
									
										session.setAttribute("EPV201004resp", m);
										session.removeAttribute("EPV201004respAval");							
										forward("EPV2010_salesplatform_respuesta_motor.jsp", req, res);									
									}
									else
									{
										//find new target (guarantor include)
										target = msg.getE01TPRBOA();								
										ELEERRMessage msgError5 = jms.sendMessage(planilla,target,user.getH01USR());
										if (!"0".equals(msgError5.getERRNUM()))
										{ 
											// if there are errors go back to maintenance page and show errors
											session.setAttribute("error", msgError5);					
											forward("EPV2010_salesplatform_maintenance.jsp", req, res);		
										}
										else
										{
											// if there are no errors, parse engine response (guarantor)
											List listaAval = jms.parseXMLResponse();
											ELEERRMessage msgError6 = (ELEERRMessage)listaAval.get(0);
											EPV101004Message mAval = (EPV101004Message)listaAval.get(1);
										
											if (!"0".equals(msgError6.getERRNUM()))
											{ 
												// if there are errors go back to maintenance page and show errors
												session.setAttribute("error", msgError6);					
												forward("EPV2010_salesplatform_maintenance.jsp", req, res);		
											}
											else
											{
												mAval.setE04PVMCUN(cliente);
												mAval.setE04PVMNUM(planilla);
												mAval.setE04CLIAVA("A");										
												session.setAttribute("EPV201004respAval", mAval);										
												m.setE04PVMCUN(cliente);
												m.setE04PVMNUM(planilla);
												m.setE04CLIAVA("C");										
												session.setAttribute("EPV201004resp", m);										
												forward("EPV2010_salesplatform_respuesta_motor.jsp", req, res);		
											}//EoIe
										}//EoIe
									}//EoIe							
								}
								else
								{
									// if there are no errors and  not guarantor required, go show response	
									m.setE04PVMCUN(cliente);
									m.setE04PVMNUM(planilla);
									m.setE04CLIAVA("C");
									session.setAttribute("EPV201004resp", m);
									session.removeAttribute("EPV201004respAval");							
									forward("EPV2010_salesplatform_respuesta_motor.jsp", req, res);								
								}
							}	
						}
					}
			}

		} finally {
			if (mp != null)
				mp.close();			
		}
	}
	
	
	/**
	 * Process the Maintenance
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	
	protected void procActionCardMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		
		try {
			mp = getMessageProcessor("EPV1010", req);

			EPV101010Message msg = (EPV101010Message) mp.getMessageRecord("EPV101010", user.getH01USR(), "0005");

			// Sets message with page fields
			msg.setH10SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);
			
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV101010Message) mp.receiveMessageRecord("EPV101010");
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("platfObj", msg);
			
			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and sho errors
				forward("EPV1010_salesplatform_cards_maintenance.jsp", req, res);			
			} else {
				
				
				if (!"Y".equals(msg.getE10TPRFEV())){// type don't call engine
					// if there are no errors go back to list
					redirect("datapro.eibs.salesplatform.JSEPV1010?SCREEN=700&customer_number="+ msg.getE10PVMCUN()+"&solicitud_number="+ msg.getE10PVMNUM(), res);					
				} else {
					// if there are no errors  --> call desicion engine
					JMSMotorClient jms =  new JMSMotorClient();				
					String target = msg.getE10TPRBOB();	
					String planilla = msg.getE10PVMNUM();
					String cliente =  msg.getE10PVMCUN();
					
					ELEERRMessage msgError2 = jms.sendMessage(planilla,target,user.getH01USR());	
					if (!"0".equals(msgError2.getERRNUM())){	
						// if there are errors go back to maintenance page and show errors
						session.setAttribute("error", msgError2);					
						forward("EPV1010_salesplatform_cards_maintenance.jsp", req, res);		
					}else{			
						// if there are no errors, parse engine response					
						List lista = jms.parseXMLResponse();
						ELEERRMessage msgError3 = (ELEERRMessage)lista.get(0);
						EPV101004Message m = (EPV101004Message)lista.get(1);
						if (!"0".equals(msgError3.getERRNUM())){ 
							// if there are errors go back to maintenance page and sho errors
							session.setAttribute("error", msgError3);					
							forward("EPV1010_salesplatform_cards_maintenance.jsp", req, res);		
						}else{
							// if there are no errors, go show response	
							m.setE04PVMCUN(cliente);
							m.setE04PVMNUM(planilla);
							session.setAttribute("EPV201004resp", m);
							forward("EPV2010_salesplatform_respuesta_motor.jsp", req, res);	
						}
					}
					
				}
				
			}

		} finally {
			if (mp != null)
				mp.close();			
		}
	}
	
	/**
	 * procActionPlatformList: find the list of forms depending on status, the program will epvl1005
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformLiquidacion(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV2010", req);

			EPV201002Message msg = (EPV201002Message) mp.getMessageRecord("EPV201002");
			msg.setH02USERID(user.getH01USR());
			msg.setH02OPECOD("0002");
			msg.setH02TIMSYS(getTimeStamp());
			try {
				msg.setE02PVMCUN(req.getParameter("customer_number").trim());
			} catch (Exception e) {
				msg.setE02PVMCUN("0");
			}
			try {
				msg.setE02PVMNUM(req.getParameter("solicitud_number").trim());
			} catch (Exception e) {
				msg.setE02PVMNUM("0");
			}

			try {
				msg.setH02FLGWK1(req.getParameter("InVisado").trim());
			} catch (Exception e) {
				msg.setH02FLGWK1("0");
			}
			// Sends message
			mp.sendMessage(msg);



			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV201002Message) mp.receiveMessageRecord("EPV201002");
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");
			session.setAttribute("liquidacion", msg);
			if (!mp.hasError(msgError)) {
				// if there are no errors go to liquidacion page
				session.setAttribute("EPV201003List", list);
				forward("EPV2010_salesplatform_liquidacion.jsp", req, res);					
			} else {
				// if there are errors go same page
				session.setAttribute("error", msgError);
				forward("EPV2010_salesplatform_maintenance.jsp", req, res);			
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	/**
	 * Process the Maintenance
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionDesembolso(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;

		try {
			
			//JLS PRICING
			// LLAMAR AL WF y MARCAR LA SOLICITUD
			
			EPV201013Message msg13 = (EPV201013Message) session.getAttribute("ObjEPV201013");
			
			mp = getMessageProcessor("EPV2010", req);
			msg13.setH13USERID(user.getH01USR());
			msg13.setH13PROGRM("EPV2010");
			msg13.setH13TIMSYS(getTimeStamp());
			msg13.setH13OPECOD("0001");

			mp.sendMessage(msg13);

			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg13 = (EPV201013Message) mp.receiveMessageRecord("EPV201013");
			//JLS PRICING
			
			EPV201002Message msg = (EPV201002Message) mp.getMessageRecord("EPV201002", user.getH01USR(), "0005");

			// Sets message with page fields
			msg.setH02SCRCOD("01");
			
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);
			
			//Receive error and data
			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV201002Message) mp.receiveMessageRecord("EPV201002");
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");
			
			// Sets session with required data
			if (mp.hasError(msgError)) {
				// if there are no errors go to maintenance page
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				session.setAttribute("liquidacion", msg);
				session.setAttribute("EPV201003List", list);
				forward("EPV2010_salesplatform_liquidacion.jsp", req, res);
			} else {
				boolean ok = reservarProductosTiendaVirtual(req,session,user,msg.getE02PVMNUM());
				if (ok){
					// if there are no errors go back to Search
					forward("EPV1000_client_enter.jsp", req, res);					
				}else{
					// if there are no errors go to maintenance page
					session.setAttribute("error", msgError);
					session.setAttribute("userPO", userPO);
					session.setAttribute("liquidacion", msg);
					session.setAttribute("EPV201003List", list);
					forward("EPV2010_salesplatform_liquidacion.jsp", req, res);			
				}
						
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	protected void procActionResponseEngine(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV2010", req);

			EPV201004Message msg = (EPV201004Message) mp.getMessageRecord("EPV201004");
		
			EPV201004Message source = (EPV201004Message)session.getAttribute("EPV201004resp");

			
			//LLAMO AL WORFLOW.
			// 
			
			//set values properties
			populate(source,msg);
            msg.setH04USERID(user.getH01USR());
            msg.setH04PROGRM("EPV2010");
            msg.setH04TIMSYS(getTimeStamp());
            msg.setH04OPECOD("0001");
			try {
				msg.setH04FLGWK1(req.getParameter("APR").trim());
			} catch (Exception e) {
				msg.setH04FLGWK1("");
			}
			
			//Sending message
			mp.sendMessage(msg);
			
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			
			msg = (EPV201004Message) mp.receiveMessageRecord();
			
			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and sho errors
				session.setAttribute("error", msgError);								
				forward("EPV2010_salesplatform_maintenance.jsp", req, res);
			} else {
				//if guarantor is enabled then save
				EPV201004Message aval = (EPV201004Message)session.getAttribute("EPV201004respAval");
				if (aval!=null){//have data
					
					EPV201004Message msgaval = (EPV201004Message) mp.getMessageRecord("EPV201004");
																					
					//set values properties
					populate(aval, msgaval);
					msgaval.setH04USERID(user.getH01USR());
					msgaval.setH04PROGRM("EPV2010");
					msgaval.setH04TIMSYS(getTimeStamp());
					msgaval.setH04OPECOD("0001");
					try {
						msgaval.setH04FLGWK1(req.getParameter("APR"));
					} catch (Exception e) {
						msgaval.setH04FLGWK1(req.getParameter("")); //aprobada.
					}					
					//Sending message
					mp.sendMessage(msgaval);
					
					//Receive error and data
					ELEERRMessage msgError1 = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
					msgaval = (EPV201004Message) mp.receiveMessageRecord("EPV201004");
					
					if (mp.hasError(msgError1)) {
						// if there are errors go back to maintenance page and sho errors
						session.setAttribute("error", msgError);								
						forward("EPV2010_salesplatform_maintenance.jsp", req, res);
						return;
					}				
				}//EoI-->aval
				
				if ("R".equals(req.getParameter("APR"))){
					// if there are no errors go back to list
					forward("EPV1000_client_enter.jsp", req, res);					
				}else{
					// if there are no errors go back to list
					redirect("datapro.eibs.salesplatform.JSEPV2010?SCREEN=700&customer_number="+ source.getE04PVMCUN()+"&solicitud_number="+ source.getE04PVMNUM() + "&InVisado=" + req.getParameter("APR2").trim() , res);					
				}
			}
			
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	private boolean reservarProductosTiendaVirtual(HttpServletRequest req, HttpSession session,
			ESS0030DSMessage user,String nroSol) throws ServletException, IOException{
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		boolean procesado=false;		
        
		try {
			mp = getMessageProcessor("EPV1170", req);

			EPV117001Message msg = (EPV117001Message) mp.getMessageRecord("EPV117001", user.getH01USR(), "0023");//todas las solicitudes a reservar

			// Sets message with page fields
			msg.setE01PTVCUN(userPO.getCusNum());
			msg.setE01PTVNUM(nroSol);

			//Sending message
			mp.sendMessage(msg);						
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");		
			if (!list.isEmpty()){//data exists 
				list.initRow();	 
				TiendaVirtualSOAPProxy tvp = new TiendaVirtualSOAPProxy();	
				ArrayList<String[]> err = new ArrayList<String[]>();
				while (list.getNextRow()) {
					EPV117001Message prod = (EPV117001Message) list.getRecord();					
					if (!"".equals(prod.getE01PTVCAM()) && !"".equals(prod.getE01PTVPRD()) && !"".equals(prod.getE01TVIFLG())){
						//llenamos servicio tienda virtual...
						ActualizaStockEntrada ent = new ActualizaStockEntrada();
						ent.setCodCampana(Integer.valueOf(prod.getE01PTVCAM())); 
						ent.setCodProducto(Integer.valueOf(prod.getE01PTVPRD()));						
						ent.setCantidad(Integer.valueOf(prod.getE01PTVCAN()));
						ent.setTipoOperacion(prod.getE01TVIFLG());//Se envia R=rechazo reserva I:ingreso reserva
						ent.setIdEvaluacion(Integer.valueOf(prod.getE01PTVNUM()));								
						RespActualizaStock resp = tvp.actualizaStock(ent);								
						if (resp.getCodRespuesta()==1){//0=OK y 1=noOK
							String[] valores={prod.getE01PTVCAM(),prod.getE01PTVPRD(),prod.getE01PTVCAN(),prod.getE01TVIFLG()};
							err.add(valores);
						}else{						
							//nos conectamos al as4000 para informar que el registro fue reservado satisfactoriamente
							mp = getMessageProcessor("EPV1170", req);
							EPV117001Message msgResult = (EPV117001Message) mp.getMessageRecord("EPV117001");
							populate(prod,msgResult);
							msgResult.setH01USERID(user.getH01USR());
							msgResult.setH01PROGRM("EPV1170");
							msgResult.setH01TIMSYS(getTimeStamp());
							msgResult.setH01OPECOD("0025");
							//Sending message					            
							mp.sendMessage(msgResult);	
							//Receive error and data
							ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();							
							if (mp.hasError(msgError)) {
								// if there are errors go back to maintenance page and sho errors
								session.setAttribute("error", msgError);								
								return procesado;
							}							
						}							
					}else{
					}
					
				}//EoW
				
				if (err.size()>0){
					String numErr = (err.size()>10)?"10":String.valueOf(err.size());
		            ELEERRMessage msgError = new ELEERRMessage();
		            msgError.setERRNUM(numErr);
		            int fil=1;
		            for (Iterator<String[]> iterator = err.iterator(); iterator.hasNext();) {
		            	String fila = (fil>9)?""+fil:"0"+fil;
		            	String[] val = (String[]) iterator.next();
		            	String des = "Error Actualizar Stock Tienda Virtual Prod:"+val[1]+" Cant:"+val[2]+" Oper:"+val[3];//55
		            	msgError.getField("ERNU"+fila).setString(fila);//numero error
		            	msgError.getField("ERDS"+fila).setString(des);//descripcion error
					}		            
		            session.setAttribute("error", msgError);
				}else{
					//existen productos a actualizar y los proceso bien.						
					procesado=true;
				}
				
			}else{
				//no existen productos a actualizar y proceso bien.
				procesado=true;
			}
			  


		}
		catch (Exception e) {
			String className = e.getClass().getName();
			String description = e.getMessage() == null ? "Exception General" : e.getMessage();					
            ELEERRMessage msgError = new ELEERRMessage();				
			msgError.setERRNUM("3");
            msgError.setERNU01("01");//4
            msgError.setERDS01("ERROR AL ACCEDER SERVICIO DE TIENDA VIRTUAL. VERIFIQUE COMUNICACION.");
            msgError.setERNU02("02");//4
            msgError.setERDS02(className);
            msgError.setERNU03("03");//4
            msgError.setERDS03(description.length() > 70 ? description.substring(0, 70) : description);	
			session.setAttribute("error", msgError);				
		}		
		finally {
			if (mp != null)
				mp.close();
		}
		return procesado;		
	}

//JLS PRICING
	protected void procActionPendingApr(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,   
			IOException {

		MessageProcessor mp = null;
		EPV201013Message msg13 = new EPV201013Message();
		
		try {
			UserPos userPO = getUserPos(session);
			userPO.setPurpose("MAINTENANCE");
			mp = getMessageProcessor("EPV2010", req);

			EPV201001Message msg = (EPV201001Message) mp.getMessageRecord("EPV201001", user.getH01USR(), "0002");				

			msg.setE01PVMCUN(userPO.getCusNum());

			if (req.getParameter("E01PVMNUM") != null) {
				msg.setE01PVMNUM(req.getParameter("E01PVMNUM"));
			}
			
			mp.sendMessage(msg);
			

			session.setAttribute("E01INDAUT", "P");
			session.setAttribute("E01MSGAUT", "");	
			
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			MessageRecord newmessage = mp.receiveMessageRecord();
			
			userPO.setHeader15("");
			if(newmessage.getField("E01PVMSTS").getString().equals("A"))
				userPO.setHeader15("Y"); //indica que debe simular primera vez con tasa sugerida prising 
			
			
			if (!mp.hasError(msgError)) {
				session.setAttribute("platfObj", newmessage);// set header page

				msgError = buscarPromocionesDescuentos(user,mp,session,((EPV201001Message)newmessage).getE01LNTYPG(), userPO.getCusNum());
				if (mp.hasError(msgError)) {
					session.setAttribute("error", msgError);
				//VER DONDE DEBE DIRECCIONAR EL ERROR		forward("EPV2010_salesplatform_list.jsp", req, res);
				} else {

					msg13.setE13PVMNUM(msg.getE01PVMNUM());
					msg13.setE13PVMCUN(msg.getE01PVMCUN());

					msg13.setE13TASPIZ(msg.getE01TASPIZ());
					msg13.setE13TASSUG(msg.getE01TASSUG());
					msg13.setE13TASMAX(msg.getE01TASMAX());
					msg13.setE13TASMIN(msg.getE01TASMIN());
					msg13.setE13TASMPR(msg.getE01TASMPR());
					msg13.setE13NOMMPR(msg.getE01NOMMPR());
					
					session.setAttribute("ObjEPV201013", msg13);
					
					
					procActionSimulation2PRIMA(user, req, res, session);
					
					
					//forward("EPV2010_salesplatform_maintenance.jsp", req, res);					

				
				
				}
			} else {
				// if there are errors go back to list page
				session.setAttribute("error", msgError);
				//VER DONDE DEBE DIRECCIONAR EL ERROR forward("EPV2010_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	
	protected void procActionSimulation2PRIMA(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		MessageProcessor mp = null;
		UserPos userPO = getUserPos(session);
		
		//JLS PRICING 
		
		String  nivE01 = "";
		String  nivE02 = "";
		String  nivE03 = "";
		String  nivE04 = "";

		double  nivT01 = 0;
		double  nivT02 = 0;
		double  nivT03 = 0;
		double  nivT04 = 0;

		//JLS PRICING 

		try {		
			mp = getMessageProcessor("EPV2010", req);
			EPV201001Message msg = (EPV201001Message)session.getAttribute("platfObj");			

			// Sets message with page fields
			msg.setE01SIRATE("");
			

			// Sending message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV201001Message) mp.receiveMessageRecord("EPV201001");

			//JLS PRICING 

			if (!mp.hasError(msgError)) {

				session.setAttribute("platfObj", msg);
				
				//LLAMADA A PRICING PARA OBTENCION DE TASAS
				
				nivE01 = "Nivel Ejecutivo";
				nivE02 = "Nivel Agente";
				nivE03 = "Nivel Agente Reg.";
				nivE04 = "Nivel Gerente Suc.";

				nivT01 = 2.23;
				nivT02 = 1.78;
				nivT03 = 1.63;
				nivT04 = 1.58;
				
				session.setAttribute("E01PRITPI", "2.78");
				session.setAttribute("E01PRITMA", "2.90");				
				session.setAttribute("E01PRITSU", "2.50");
				session.setAttribute("E01PRITPR", "1.90");
				session.setAttribute("E01PRITCD", "998765");
				
				session.setAttribute("E01PRIN1E", "Nivel Ejecutivo");
				session.setAttribute("E01PRITN1", "2.23");				
				session.setAttribute("E01PRIN1H", "   0");

				session.setAttribute("E01PRIN2E", "Nivel Agente");
				session.setAttribute("E01PRITN2", "1.78");				
				session.setAttribute("E01PRIN2H", "13,786");

				session.setAttribute("E01PRIN3E", "Nivel Agente Reg.");
				session.setAttribute("E01PRITN3", "1.63");				
				session.setAttribute("E01PRIN3H", "12,768");

				session.setAttribute("E01PRIN4E", "Nivel Gerente Suc.");
				session.setAttribute("E01PRITN4", "1.58");				
				session.setAttribute("E01PRIN4H", "11,387");

				//GUARDAR LOS DATOS DE LA SIMULACION PARA GRABAR AL FINAL DEL CICLO.
				
				EPV201013Message msg13 = new EPV201013Message();
				
				
				msg13.setE13PVMNUM(msg.getE01PVMNUM());
				msg13.setE13PVMCUN(msg.getE01PVMCUN());

				msg13.setE13CODRET("0");
				msg13.setE13GLSRET("PROCESO EXITOSO");
				msg13.setE13SIRATE(msg.getE01SIRATE());
				msg13.setE13TASPIZ("2.5");
				msg13.setE13TASSUG("2.4");
				msg13.setE13TASMAX("3.0");
				msg13.setE13TASMIN("1.0");
				msg13.setE13TASMPR("2.3");
				msg13.setE13NOMMPR("MEJOR PRECIO");
				msg13.setE13TIPRES("0");
				msg13.setE13PLAZOO("365");
				msg13.setE13MONTOO("1000");
				msg13.setE13MONEDO("CLP");
				msg13.setE13ORDENO("1");
				msg13.setE13IDROLO("123456789");
				msg13.setE13DESROO("DES. 123456789");
				msg13.setE13VALORO("1.6");
				msg13.setE13HOLGUO("15000");
				
				msg13.setH13FLGWK1("P"); //pendiente de aprobación de tasa
				
				session.setAttribute("ObjEPV201013", msg13);
				
				
				
				//
				
				msg = (EPV201001Message) session.getAttribute("platfObj");
				
			//	if(userPO.getHeader15().equals("Y") ||  !req.getParameter("H02FLGWK3").equals("X"))
			//	{
					msg.setE01SIRATE("2.50");
					if(userPO.getHeader15().equals("Y"))
						userPO.setHeader15("");
			//	}
				
				mp.sendMessage(msg);
				
				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msg = (EPV201001Message) mp.receiveMessageRecord("EPV201001");

			
			
				double TasaCalculo = Double.parseDouble(msg.getE01SIRATE());
	
			
				if(TasaCalculo<=nivT01 && TasaCalculo>=nivT02 )
				{
					session.setAttribute("E01INDAUT", "X");
					session.setAttribute("E01MSGAUT", nivE01);				
				}
				else 
				{
					if(TasaCalculo<=nivT02 && TasaCalculo>=nivT03 )
					{
						session.setAttribute("E01INDAUT", "X");
						session.setAttribute("E01MSGAUT", nivE02);				
					}
					else 
					{
						if(TasaCalculo<=nivT03 && TasaCalculo>=nivT04 )
						{
							session.setAttribute("E01INDAUT", "X");
							session.setAttribute("E01MSGAUT", nivE03);				
						}
						else 
						{
							if(TasaCalculo<=nivT04 )
							{
								session.setAttribute("E01INDAUT", "X");
								session.setAttribute("E01MSGAUT", nivE04);			
							}
						}
					}	
			
				}	
			}
			//JLS PRICING			
			msg.setH01FLGWK2("X");//marco que la simulacion 1 esta lista.
			
			if (mp.hasError(msgError)) {
				msg.setH01FLGWK3("");//si tiene error sigo simulando	
			}else{
				msg.setH01FLGWK3("X");//NO hubo error simulo
			}

			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("platfObj", msg);// seteamos nuevamente con la informacion obtenida				
		
			req.setAttribute("SIMULAR3", "S");	
			forward("EPV2010_salesplatform_maintenance.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
//JLS PRICING
	
	
	
	
	
	
	
}
