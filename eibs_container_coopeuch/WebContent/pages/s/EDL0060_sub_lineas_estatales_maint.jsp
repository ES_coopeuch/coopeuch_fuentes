<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<html>
<head>
<title>Mantenedor de L&iacute;neas Estatales MYPE</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="msgSubLinEst" class="datapro.eibs.beans.EDL006001Message"  scope="session" />
<jsp:useBean id="msgLinEst" class="datapro.eibs.beans.EDL006001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

function cancel(op) {
	document.forms[0].SCREEN.value = 200;
	document.forms[0].opt.value = op;
	document.forms[0].submit();
}

function volver(op) {
	document.forms[0].SCREEN.value = 210;
	document.forms[0].opt.value = op;
	document.forms[0].submit();
}


function textCounter(field,maxlimit) 
{
	if (field.value.length > maxlimit) // if too long...trim it!
	{
		alert("Solo se permiten 200 caracteres en el comentario");
	 	field.value = field.value.substring(0, maxlimit);
	}

}

function validar() {

	document.forms[0].SCREEN.value = 500;
	
	var fSubLinea = new Date(document.forms[0].E01MLNTRY.value, document.forms[0].E01MLNTRM.value, document.forms[0].E01MLNTRD.value);
	var fLinea = new Date(<% out.print(msgLinEst.getE01MLNTRY().trim());%>, <% out.print(msgLinEst.getE01MLNTRM().trim());%>, <% out.print(msgLinEst.getE01MLNTRD().trim());%>);
    
	if(fSubLinea > fLinea)
	{
   	 if(confirm("Fecha de termino real de sublinea mayor a fecha de linea, Acepta Fecha?" ))
		document.forms[0].submit();
	}
	else 
		document.forms[0].submit();
}

</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "";
    String readonly2 = "";
    String readonly3 = "";

    if(msgLinEst.getE01MLNSTS().equals("A"))
    	if(!userPO.getPurpose().equals("NEW"))
    		  readonly2 = "readonly";

    if(userPO.getPurpose().equals("INQ"))
    {
		readonly = "readonly";
    	readonly2 = "readonly";
    	readonly3 = "readonly";    	
    }

    if(userPO.getPurpose().equals("MAINT"))
    	readonly3 = "readonly";
    
     
%>


<H3 align="center">Mantenedor de L&iacute;neas Estatales MYPE<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="sub_lineas_estatales_maint, EDL0060"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0060" >
  	<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="500">
  	<input type=HIDDEN name="opt"> 
  	<input type="hidden" name="E01MLNMDA" size="7"  value="<%= msgLinEst.getE01MLNMDA().trim()%>" readonly >
	<input type=HIDDEN name="CURRCODEL" value="<%=request.getParameter("CURRCODEL").trim()%>"> 
  
    
  <h4>Datos L&iacute;nea</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">L&iacute;nea :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01MLNSEQ" size="7"  value="<%= msgLinEst.getE01MLNSEQ().trim()%>" readonly >
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right"><%if(readonly3.equals("readonly")){out.print("SubL&iacute;nea :");} %>
              </div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
              <%if(readonly3.equals("readonly")){ %>
              	<input type="text" name="E01MLNIND" size="7"  value="<%= msgSubLinEst.getE01MLNIND().trim()%>" readonly >
              <%} %>	
              
              </div>
            </td>
          </tr>

           <tr id="trclear"> 
            <td nowrap width="20%"> 
              <div align="right">Garantizador :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01MLNGTZ" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNGTZ().trim()%>" readonly  style="text-align:right;">
                <input type="text" name="E01MLNDGR" size="41"  value="<%= msgLinEst.getE01MLNDGR().trim()%>" readonly  >
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Programa :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E01MLNPGM" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNPGM().trim()%>" readonly >
                <input type="text" name="E01MLNDPG" size="41"  value="<%= msgLinEst.getE01MLNDPG().trim()%>" readonly  >
              </div>
            </td>
          </tr>
          
                 <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Estado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSTS1" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSTS().trim()%>" readonly  >
                <input type="text" name="E01MLNDST1" size="41"  value="<%= msgLinEst.getE01MLNDST().trim()%>" readonly  >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Tasa :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTAS1" size="11" maxlength="10" value="<%= msgLinEst.getE01MLNTAS().trim()%>" readonly style="text-align:right;" >
              </div>
            </td>
          </tr> 

          <tr id="trclear"> 
            <td height="23"> 
              <div align="right">% Garant&iacute;a :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNGRT" size="11" maxlength="10" value="<%= msgLinEst.getE01MLNGRT().trim()%>" onkeypress="enterDecimal()" style="text-align:right;"  >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Segmento 1 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG1" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG1().trim()%>"  readonly>
                <input type="text" name="E01MLNDG1" size="41"  value="<%= msgLinEst.getE01MLNDG1().trim()%>" readonly  >
             </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Cupo L&iacute;nea :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCLI" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNCLI().trim())%>"  readonly style="text-align:right;">
             </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 2 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG2" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG2().trim()%>"  readonly>
                <input type="text" name="E01MLNDG2" size="41"  value="<%= msgLinEst.getE01MLNDG2().trim()%>" readonly  >
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Cupo L&iacute;nea Pesos :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCMN" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNCMN().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 3 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG3" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG3().trim()%>"  readonly>
                <input type="text" name="E01MLNDG3" size="41"  value="<%= msgLinEst.getE01MLNDG3().trim()%>" readonly  >
             </div>
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Saldo Disponible :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSDO" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNSDO().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 4 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG4" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG4().trim()%>"  readonly>
                <input type="text" name="E01MLNDG4" size="41"  value="<%= msgLinEst.getE01MLNDG4().trim()%>" readonly  >
            </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Monto Retenido :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
              	<input type="text" name="E01MLNSDR" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNSDR().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 5 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG5" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG5().trim()%>"  readonly>
                <input type="text" name="E01MLNDG5" size="41"  value="<%= msgLinEst.getE01MLNDG5().trim()%>" readonly  >
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Fecha de T&eacute;rmino Final :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTRD1" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNTRD().trim()%>" readonly>
                <input type="text" name="E01MLNTRM1" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNTRM().trim()%>" readonly>
                <input type="text" name="E01MLNTRY1" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNTRY().trim()%>" readonly>
             </div>
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
          </tr>
          
          
		</table>
	  </td>	
	</tr>
  </table>	  
  <h4>Datos Sub L&iacute;nea</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          

          <tr id="trclear"> 
            <td height="23"> 
              <div align="right">Motivo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCDM" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNCDM().trim()%>" readonly  >
                <input type="text" name="E01MLNMOT" size="41"  value="<%= msgSubLinEst.getE01MLNMOT().trim()%>" readonly  >
				<%if(!readonly.equals("readonly")) { %>
					<a href="javascript:GetCodeDescCNOFC('E01MLNCDM','E01MLNMOT','ML')"> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%}%>	
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left"> 
             </div>
            </td>
          </tr> 


          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Estado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSTS" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNSTS().trim()%>" readonly  >
                <input type="text" name="E01MLNDST" size="41"  value="<%= msgSubLinEst.getE01MLNDST().trim()%>" readonly  >
				<%if(!readonly.equals("readonly")) { %>
					<a href="javascript:GetCodeDescCNOFC('E01MLNSTS','E01MLNDST','VE')"> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%}%>	
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Tasa :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTAS" size="11" maxlength="10" value="<%= msgSubLinEst.getE01MLNTAS().trim()%>"  style="text-align:right;" <% out.print(readonly); %>>
             </div>
            </td>
          </tr> 

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Fecha de Inicio :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNFVD" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNFVD().trim()%>" <% out.print(readonly2); %>>
                <input type="text" name="E01MLNFVM" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNFVM().trim()%>" <% out.print(readonly2); %>>
                <input type="text" name="E01MLNFVY" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNFVY().trim()%>" <% out.print(readonly2); %>>
				<%if(!readonly.equals("readonly")) { %>
                	<a href="javascript:DatePicker(document.forms[0].E01MLNFVD,document.forms[0].E01MLNFVM,document.forms[0].E01MLNFVY)">
                	<img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
				<%} %>
             </div>
            </td>
            <td nowrap rowspan="3"> 
              <div align="right">Comentarios :</div>
            </td>
            <td nowrap rowspan="3"> 
              <div align="left"> 
                <textarea name="E01MLNGLS"  cols="80" rows="7" onKeyDown="textCounter(this,200)" onKeyUp="textCounter(this,200)" onkeypress="textCounter(this,200)" <% out.print(readonly); %>><%=msgSubLinEst.getE01MLNGLS().trim()%></textarea>
              </div>
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Fecha de T&eacute;rmino :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNFTD" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNFTD().trim()%>" <% out.print(readonly); %>>
                <input type="text" name="E01MLNFTM" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNFTM().trim()%>" <% out.print(readonly); %>>
                <input type="text" name="E01MLNFTY" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNFTY().trim()%>" <% out.print(readonly); %>>
				<%if(!readonly.equals("readonly")) { %>
                	<a href="javascript:DatePicker(document.forms[0].E01MLNFTD,document.forms[0].E01MLNFTM,document.forms[0].E01MLNFTY)">
                	<img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
				<%} %>
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Fecha de T&eacute;rmino Final :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTRD" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNTRD().trim()%>" <% out.print(readonly); %>>
                <input type="text" name="E01MLNTRM" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNTRM().trim()%>" <% out.print(readonly); %>>
                <input type="text" name="E01MLNTRY" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNTRY().trim()%>" <% out.print(readonly); %>>
				<%if(!readonly.equals("readonly")) { %>
                	<a href="javascript:DatePicker(document.forms[0].E01MLNTRD,document.forms[0].E01MLNTRM,document.forms[0].E01MLNTRY)">
                	<img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
				<%} %>
               </div>
            </td>
          </tr>
         <tr id="trclear"> 
            <td nowrap height="23"> 
            </td>
            <td nowrap> 
              <div align="left"> 
               </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>


  <div align="center">
	<%if(!readonly.equals("readonly")) { %>
		<input id="EIBSBTN" type=button name="Enviar" value="Enviar" onclick="validar()">
	    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel(3)">
	<%}else{%>
	    <input id="EIBSBTN" type="button" name="Cancel" value="Volver" onclick="cancel(3)">
	<%}%>

  </div>
  </form>
  
</body>
</html>
