<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page import="datapro.eibs.beans.EPV117001Message"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 
<html> 
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="EPV117002" class="datapro.eibs.beans.EPV117002Message"  scope="session" />
<jsp:useBean id="listaProd" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

  	function validaCantidad(nro){

	  	var cDisp = eval("document.forms[0].cant_disp"+nro+".value");
	  	var cSoli = eval("document.forms[0].cant_sol"+nro+".value");
	  	var icDisp = cDisp*1;
	  	var icSoli = cSoli*1;
		if (icSoli>icDisp){
			alert("Cantidad Solicitada ("+cSoli+") No puede ser mayor a la disponible ("+cDisp+")");
			eval("document.forms[0].cant_sol"+nro+".value = '0'");			
	      	eval("document.forms[0].cant_sol"+nro+".focus();");	      	
		}
	  	
  	}

  	function activarCantidad(nro){
  	
	  	if (eval("document.forms[0].flat"+nro+".checked")){
	  	  	eval("document.forms[0].cant_sol"+nro+".disabled = false");
	  	}else{
		  	eval("document.forms[0].cant_sol"+nro+".disabled = true");
			eval("document.forms[0].cant_sol"+nro+".value = '0'");		  	
	  	}
	  	
  	}

  	function validar(){
		var selecciono = false;
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.type == "checkbox")
	      	{	
	      		if (element.checked == true) {
		      		selecciono = true;
	      			//buscamos el numero del flat.
	      			var nro = element.name.substring(4);
	      			if (eval("document.forms[0].cant_sol"+nro+".value == '0'")){
	      				alert("Debe Colocar una cantidad de Productos a Solicitar para Enviar.!");
	      				eval("document.forms[0].cant_sol"+nro+".focus()");
	      				return false;
	      			}
				}
	      	}//EoI
	      }//EoF  
	   	
	   	if (!selecciono){
	   		alert('Debe seleccionar al menos un productos para Enviar.!');
			return false;
	   	}
	   	
		return true;
  	}
  	
  	function cerrarVentana(){
		window.open('','_parent','');
		window.close(); 
  	}
  	
//  Process according with user selection
 function goAction(op) {	
   	switch (op){
	//Cancel
	case 1:  {
		cerrarVentana();	
       	break;
		}
	}
 }
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>


 <% String emp = (String)session.getAttribute("EMPTV");
 	emp = (emp==null)?"":emp;//si es blanco viene llamado por menu, sino viene llamdo desde la pantalla EPV1010
 %>
 
 <h3 align="center">
<%if (readOnly){ %>
	CONSULTA DE ARTICULOS
<%} else if (maintenance){ %>
	MANTENIMIENTO DE ARTICULOS
<%} else { %>
	SELECCION DE  ARTICULOS
<%} %>
 <%if ("".equals(emp)){ %>
	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="tienda_virtual_maintenance_ws.jsp, EPV1170"></h3>
	<hr size="4">
<%}%>
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1170" onsubmit="return validar();">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="400">

 <% int row = 0;%>
 <%if ("".equals(emp)){ %>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap width="10%" align="right"> 
             	Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="EPV117002" property="E02PTVCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>          
             <td nowrap width="10%" align="right">
             	RUT :  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="EPV117002" property="E02RUTCLI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">
             	Nombre :  
             </td>
             <td nowrap width="30%" align="left">
	  			<eibsinput:text name="EPV117002" property="E02NMECLI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_SHORT %>" readonly="true"/>
             </td>             
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap align="right"> Convenio : 
              </td>
             <td nowrap align="left">
	  			<eibsinput:text name="EPV117002" property="E02CNVCLI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>              
             <td nowrap align="right"> Propuesta : 
               </td>
             <td nowrap align="left">
	  			<eibsinput:text name="EPV117002" property="E02PTVNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>
             </td>
             <td nowrap align="right">&nbsp;</td>
             <td nowrap  align="left">&nbsp;</td>                         
         </tr>         
       </table>
      </td>
    </tr>
  </table>
  <%}else{%>
  	<input  type="hidden" name="E02PTVCUN" value="<%=EPV117002.getE02PTVCUN()%>">
  	<input  type="hidden" name="E01PTVNUM" value="<%=EPV117002.getE02PTVNUM()%>">
  <%} %>
  
  
  <%
	row = 0;
	if (listaProd.getNoResult()) {
%>
<table id="headTable" width="100%">
	<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <th nowrap width="2%">&nbsp;</th>
            <th nowrap width="5%" align="center">Codigo<br>Campa�a</th>
            <th nowrap width="15%"  align="left">Nombre<br>Campa�a</th>
            <th nowrap width="5%" align="center">Codigo<br>Producto</th>
            <th nowrap width="15%" align="left">Nombre<br>Producto</th>
            <th nowrap width="15%" align="left">Marca</th>
            <th nowrap width="15%" align="left">Modelo</th>            
            <th nowrap width="10%" align="center">Precio<br>Unitario</th>
            <th nowrap width="10%" align="center">Costo<br>Envio</th>  
            <th nowrap width="4%" align="center">Cantidad<br>Disp.</th>                                             
            <th nowrap width="4%" align="center">Cantidad<br>Solic.</th>       
	</tr>
</table>
<br>
<table class="tbenter" width=100%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No existen Productos en la tienda virtual.</b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>	
 <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <th nowrap width="2%">&nbsp;</th>
            <th nowrap width="5%" align="center">Codigo<br>Campa�a</th>
            <th nowrap width="15%"  align="left">Nombre<br>Campa�a</th>
            <th nowrap width="5%" align="center">Codigo<br>Producto</th>
            <th nowrap width="15%" align="left">Nombre<br>Producto</th>
            <th nowrap width="15%" align="left">Marca</th>
            <th nowrap width="15%" align="left">Modelo</th>            
            <th nowrap width="10%" align="center">Precio<br>Unitario</th>
            <th nowrap width="10%" align="center">Costo<br>Envio</th>  
            <th nowrap width="4%" align="center">Cantidad<br>Disp.</th>                                             
            <th nowrap width="4%" align="center">Cantidad<br>Solic.</th>                                                         
          </tr>
 
          <%
    	   listaProd.initRow();
    	   int fil = 0;
		   while (listaProd.getNextRow()) {
				fil++;
		   		EPV117001Message prod = (EPV117001Message) listaProd.getRecord();
          %> 
	          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
	            <td nowrap>
	            	<input type="checkbox" name="flat<%=fil%>" <%=(!"0".equals(prod.getE01PTVCAN())?"checked='checked'":"") %>  onclick="activarCantidad('<%=fil%>')">
	            </td>
	            <td nowrap align="center"><%=prod.getE01PTVCAM()%></td>
	            <td nowrap align="left">  <%=prod.getE01PTVNCA() %> </td>
	            <td nowrap align="center"> <%=prod.getE01PTVPRD()%> </td>
	            <td nowrap align="left"> <%=prod.getE01PTVNME()%> </td>
	            <td nowrap align="left"> <%=prod.getE01PTVMAR()%> </td>
	            <td nowrap align="left"> <%=prod.getE01PTVMOD()%> </td>
	            <td nowrap align="right"> 
	            	<%=com.datapro.generics.Util.formatCCY(prod.getE01PTVVAU())%>
	            	<input type="hidden" name="precio<%=fil%>" value="<%=prod.getE01PTVVAU()%>">
	            </td>
	            <td nowrap align="right"> 
	            	<%=com.datapro.generics.Util.formatCCY(prod.getE01PTVCEN())%> 
					<input type="hidden" name="costo<%=fil%>" value="<%=prod.getE01PTVCEN()%>">	            	
	            </td>	            
	            <td nowrap align="center"> 
	            	<%=prod.getE01PTVSTO()%>
					<input type="hidden" name="cant_disp<%=fil%>" value="<%=prod.getE01PTVSTO()%>">
	            </td>	  
	            <td nowrap align="center">
	            	<input size="4" maxlength="3" type="text" name="cant_sol<%=fil%>" value="<%=prod.getE01PTVCAN()%>" onKeypress="enterInteger()" <%=("0".equals(prod.getE01PTVCAN())?"disabled='true'":"") %> onchange="validaCantidad('<%=fil%>')">
	            </td>	  	            	            
	          </tr> 
   		  <%
   		   }%>                                                                                                                
        </table>
        </td>
    </tr>
  </table>  

 	 <div align="center"> 
          <input id="EIBSBTN" type=submit name="Submit" value="Enviar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>
<%
	}
%>	
             
  </form>
  
 <%if ("S".equals(request.getAttribute("ACT"))){%>
 <script type="text/javascript">
 	  //recargamos la pagina que nos llamo..	  
	  window.opener.location.href =  '<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1170?SCREEN=101&E01PTVCUN=<%=userPO.getCusNum()%>&E01PTVNUM=<%=userPO.getHeader23()%>';
     <%//NOTA: solo para activar el check de la pagina integral%>
     <%  String re =(String) session.getAttribute("EMPTV");%>
	 <%  if ("S".equals(re)){%>
			window.opener.parent.setRecalculate3();	        
	 <% } %> 
  	  //cerramos la ventana
  	  cerrarVentana();	  
 </script>
 <% } %>   
   
</body>
</HTML>
