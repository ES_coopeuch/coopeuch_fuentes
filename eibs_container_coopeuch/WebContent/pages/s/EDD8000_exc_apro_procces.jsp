<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<title>Carga Masiva de Cargos y Abonos Cuentas Vistas</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ExCon" class= "datapro.eibs.beans.EDD800001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script type="text/javascript">

function goAction(op) {
	document.forms[0].SCREEN.value = op;
	document.forms[0].submit();	
}

</SCRIPT>  


</head>

<body>
 <% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>

<H3 align="center">Carga Masiva de Cargos y Abonos Cuentas Vistas</H3>
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ecx_apro_process.jsp, EDD8000"></H3>

<hr size="4">
<p>&nbsp;</p>

<br>
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD8000" target="main">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="100">

<table class="tableinfo" width="100%">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">     
		    <tr>
				<div align="center"> 					
  					<br>Se ha realizado la Carga Masiva de Cargos y Abonos para cuentas Vistas con Nro. de ID : 
  					<% out.print( " " +  ExCon.getE01IDCARGA() + " "); %>
  					<br><h3>Registros Abonos = <% out.print(ExCon.getE01TABONO()); %> / Registros Cargos = <% out.print(ExCon.getE01TCARGO()); %></h3>
					<br>Para su gesti&oacute;n ingrese a opci&oacute;n Cuentas Ctes-Vista / Gest.Aprobaci&oacute;n Carga  					
  				 </div>		    
		      <td > 
		      </td>
		    </tr>
		 </table>
	  </td>
	</tr>
</table>

<br>

<table class="tbenter" width=50% align="center">
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<th bgcolor="D9D9D9" align="Left" nowrap width="5%" height="30">&nbsp; &nbsp; RECHAZOS</th>
	</tr>
	<tr>
		<th align="Left" nowrap height="5"></th>
	</tr>
	<tr>
		<td>
		<table id="headTable" width="50%" align="left">
			<tr id="trdark">
				<th align="left" nowrap>MOTIVO</th>
				<th align="left" nowrap width="100">ABONOS</th>
				<th align="left" nowrap width="100">CARGOS</th>
			</tr>
			
			<tr>
				<td nowrap align="left">Cuenta vista no activa</td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TACTNA()%></td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TCCTNA()%></td>
			</tr>
			
			<tr>
				<td nowrap align="left">Cuenta vista no corresponde al RUT</td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TANRUT()%></td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TCNRUT()%></td>
			</tr>
			
			<tr>
				<td nowrap align="left">Cuenta vista sin saldo</td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TASISA()%></td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TCSISA()%></td>
			</tr>
			
			<tr>
				<td nowrap align="left">RUT no socio</td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TARUTN()%></td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TCRUTN()%></td>
			</tr>
			
			<tr>
				<td nowrap align="left">Monto sin valor</td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TAMONT()%></td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TCMONT()%></td>
			</tr>
			
			<tr>
				<td nowrap align="left">Abono duplicado</td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TAABOD()%></td>
				<td nowrap align="left" width="100"><%=ExCon.getE01TCCARD()%></td>
			</tr>

		</table>
		</td>
	</tr>
</table>
<br>
  <p align="center">
      <input id="EIBSBTN" type="button" onclick="javascript:goAction('100');"   name=Volver value="Volver">
  </p>
  
 </form> 
</body>
</html>
