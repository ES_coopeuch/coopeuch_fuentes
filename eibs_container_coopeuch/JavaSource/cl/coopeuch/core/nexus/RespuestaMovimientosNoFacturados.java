/**
 * RespuestaMovimientosNoFacturados.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.coopeuch.core.nexus;

public class RespuestaMovimientosNoFacturados  implements java.io.Serializable {
    private int coderror;
    private java.lang.String msgerror;
    private java.lang.String idwsst247;
    private int organizacion_s;
    private java.lang.String num_cuenta_s;
    private java.lang.String apaterno;
    private java.lang.String amaterno;
    private java.lang.String nombre_clte;
    private int ciclo_fac;
    private int codigo_fv;
    private int fec_ult_fac_cal;
    private int fec_prox_fac_cal;
    private int fec_ini_fac_act;
    private int fec_consulta;
    private int fec_ult_proc;
    private int fec_ult_trans;
    private int fec_prim_trans;
    private java.lang.String ind_presencia_datos;
    private java.lang.String ind_trans_restantes;
    private int transacciones_c;
    private cl.coopeuch.core.nexus.ListaTransacciones transacciones;
    private double monto1;
    private double monto2;
    private double monto3;
    private double monto4;
    private double monto5;
    private double monto6;
    private double monto7;
    private long numero1;
    private long numero2;
    private long numero3;
    private long numero4;
    private long numero5;
    private long numero6;
    private long numero7;
    private java.lang.String flag1;
    private java.lang.String flag2;
    private java.lang.String flag3;
    private java.lang.String flag4;
    private java.lang.String flag5;
    private java.lang.String flag6;
    private java.lang.String flag7;
    private java.lang.String string1;
    private java.lang.String string2;
    private java.lang.String string3;
    private java.lang.String string4;
    private java.lang.String string5;
    private java.lang.String string6;
    private java.lang.String string7;
    private java.lang.String filler_04;
    private cl.coopeuch.core.nexus.Error error;

    public RespuestaMovimientosNoFacturados() {
    }

    public int getCoderror() {
        return coderror;
    }

    public void setCoderror(int coderror) {
        this.coderror = coderror;
    }

    public java.lang.String getMsgerror() {
        return msgerror;
    }

    public void setMsgerror(java.lang.String msgerror) {
        this.msgerror = msgerror;
    }

    public java.lang.String getIdwsst247() {
        return idwsst247;
    }

    public void setIdwsst247(java.lang.String idwsst247) {
        this.idwsst247 = idwsst247;
    }

    public int getOrganizacion_s() {
        return organizacion_s;
    }

    public void setOrganizacion_s(int organizacion_s) {
        this.organizacion_s = organizacion_s;
    }

    public java.lang.String getNum_cuenta_s() {
        return num_cuenta_s;
    }

    public void setNum_cuenta_s(java.lang.String num_cuenta_s) {
        this.num_cuenta_s = num_cuenta_s;
    }

    public java.lang.String getApaterno() {
        return apaterno;
    }

    public void setApaterno(java.lang.String apaterno) {
        this.apaterno = apaterno;
    }

    public java.lang.String getAmaterno() {
        return amaterno;
    }

    public void setAmaterno(java.lang.String amaterno) {
        this.amaterno = amaterno;
    }

    public java.lang.String getNombre_clte() {
        return nombre_clte;
    }

    public void setNombre_clte(java.lang.String nombre_clte) {
        this.nombre_clte = nombre_clte;
    }

    public int getCiclo_fac() {
        return ciclo_fac;
    }

    public void setCiclo_fac(int ciclo_fac) {
        this.ciclo_fac = ciclo_fac;
    }

    public int getCodigo_fv() {
        return codigo_fv;
    }

    public void setCodigo_fv(int codigo_fv) {
        this.codigo_fv = codigo_fv;
    }

    public int getFec_ult_fac_cal() {
        return fec_ult_fac_cal;
    }

    public void setFec_ult_fac_cal(int fec_ult_fac_cal) {
        this.fec_ult_fac_cal = fec_ult_fac_cal;
    }

    public int getFec_prox_fac_cal() {
        return fec_prox_fac_cal;
    }

    public void setFec_prox_fac_cal(int fec_prox_fac_cal) {
        this.fec_prox_fac_cal = fec_prox_fac_cal;
    }

    public int getFec_ini_fac_act() {
        return fec_ini_fac_act;
    }

    public void setFec_ini_fac_act(int fec_ini_fac_act) {
        this.fec_ini_fac_act = fec_ini_fac_act;
    }

    public int getFec_consulta() {
        return fec_consulta;
    }

    public void setFec_consulta(int fec_consulta) {
        this.fec_consulta = fec_consulta;
    }

    public int getFec_ult_proc() {
        return fec_ult_proc;
    }

    public void setFec_ult_proc(int fec_ult_proc) {
        this.fec_ult_proc = fec_ult_proc;
    }

    public int getFec_ult_trans() {
        return fec_ult_trans;
    }

    public void setFec_ult_trans(int fec_ult_trans) {
        this.fec_ult_trans = fec_ult_trans;
    }

    public int getFec_prim_trans() {
        return fec_prim_trans;
    }

    public void setFec_prim_trans(int fec_prim_trans) {
        this.fec_prim_trans = fec_prim_trans;
    }

    public java.lang.String getInd_presencia_datos() {
        return ind_presencia_datos;
    }

    public void setInd_presencia_datos(java.lang.String ind_presencia_datos) {
        this.ind_presencia_datos = ind_presencia_datos;
    }

    public java.lang.String getInd_trans_restantes() {
        return ind_trans_restantes;
    }

    public void setInd_trans_restantes(java.lang.String ind_trans_restantes) {
        this.ind_trans_restantes = ind_trans_restantes;
    }

    public int getTransacciones_c() {
        return transacciones_c;
    }

    public void setTransacciones_c(int transacciones_c) {
        this.transacciones_c = transacciones_c;
    }

    public cl.coopeuch.core.nexus.ListaTransacciones getTransacciones() {
        return transacciones;
    }

    public void setTransacciones(cl.coopeuch.core.nexus.ListaTransacciones transacciones) {
        this.transacciones = transacciones;
    }

    public double getMonto1() {
        return monto1;
    }

    public void setMonto1(double monto1) {
        this.monto1 = monto1;
    }

    public double getMonto2() {
        return monto2;
    }

    public void setMonto2(double monto2) {
        this.monto2 = monto2;
    }

    public double getMonto3() {
        return monto3;
    }

    public void setMonto3(double monto3) {
        this.monto3 = monto3;
    }

    public double getMonto4() {
        return monto4;
    }

    public void setMonto4(double monto4) {
        this.monto4 = monto4;
    }

    public double getMonto5() {
        return monto5;
    }

    public void setMonto5(double monto5) {
        this.monto5 = monto5;
    }

    public double getMonto6() {
        return monto6;
    }

    public void setMonto6(double monto6) {
        this.monto6 = monto6;
    }

    public double getMonto7() {
        return monto7;
    }

    public void setMonto7(double monto7) {
        this.monto7 = monto7;
    }

    public long getNumero1() {
        return numero1;
    }

    public void setNumero1(long numero1) {
        this.numero1 = numero1;
    }

    public long getNumero2() {
        return numero2;
    }

    public void setNumero2(long numero2) {
        this.numero2 = numero2;
    }

    public long getNumero3() {
        return numero3;
    }

    public void setNumero3(long numero3) {
        this.numero3 = numero3;
    }

    public long getNumero4() {
        return numero4;
    }

    public void setNumero4(long numero4) {
        this.numero4 = numero4;
    }

    public long getNumero5() {
        return numero5;
    }

    public void setNumero5(long numero5) {
        this.numero5 = numero5;
    }

    public long getNumero6() {
        return numero6;
    }

    public void setNumero6(long numero6) {
        this.numero6 = numero6;
    }

    public long getNumero7() {
        return numero7;
    }

    public void setNumero7(long numero7) {
        this.numero7 = numero7;
    }

    public java.lang.String getFlag1() {
        return flag1;
    }

    public void setFlag1(java.lang.String flag1) {
        this.flag1 = flag1;
    }

    public java.lang.String getFlag2() {
        return flag2;
    }

    public void setFlag2(java.lang.String flag2) {
        this.flag2 = flag2;
    }

    public java.lang.String getFlag3() {
        return flag3;
    }

    public void setFlag3(java.lang.String flag3) {
        this.flag3 = flag3;
    }

    public java.lang.String getFlag4() {
        return flag4;
    }

    public void setFlag4(java.lang.String flag4) {
        this.flag4 = flag4;
    }

    public java.lang.String getFlag5() {
        return flag5;
    }

    public void setFlag5(java.lang.String flag5) {
        this.flag5 = flag5;
    }

    public java.lang.String getFlag6() {
        return flag6;
    }

    public void setFlag6(java.lang.String flag6) {
        this.flag6 = flag6;
    }

    public java.lang.String getFlag7() {
        return flag7;
    }

    public void setFlag7(java.lang.String flag7) {
        this.flag7 = flag7;
    }

    public java.lang.String getString1() {
        return string1;
    }

    public void setString1(java.lang.String string1) {
        this.string1 = string1;
    }

    public java.lang.String getString2() {
        return string2;
    }

    public void setString2(java.lang.String string2) {
        this.string2 = string2;
    }

    public java.lang.String getString3() {
        return string3;
    }

    public void setString3(java.lang.String string3) {
        this.string3 = string3;
    }

    public java.lang.String getString4() {
        return string4;
    }

    public void setString4(java.lang.String string4) {
        this.string4 = string4;
    }

    public java.lang.String getString5() {
        return string5;
    }

    public void setString5(java.lang.String string5) {
        this.string5 = string5;
    }

    public java.lang.String getString6() {
        return string6;
    }

    public void setString6(java.lang.String string6) {
        this.string6 = string6;
    }

    public java.lang.String getString7() {
        return string7;
    }

    public void setString7(java.lang.String string7) {
        this.string7 = string7;
    }

    public java.lang.String getFiller_04() {
        return filler_04;
    }

    public void setFiller_04(java.lang.String filler_04) {
        this.filler_04 = filler_04;
    }

    public cl.coopeuch.core.nexus.Error getError() {
        return error;
    }

    public void setError(cl.coopeuch.core.nexus.Error error) {
        this.error = error;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        RespuestaMovimientosNoFacturados other = (RespuestaMovimientosNoFacturados) obj;
        boolean _equals;
        _equals = true
            && this.coderror == other.getCoderror()
            && ((this.msgerror==null && other.getMsgerror()==null) || 
             (this.msgerror!=null &&
              this.msgerror.equals(other.getMsgerror())))
            && ((this.idwsst247==null && other.getIdwsst247()==null) || 
             (this.idwsst247!=null &&
              this.idwsst247.equals(other.getIdwsst247())))
            && this.organizacion_s == other.getOrganizacion_s()
            && ((this.num_cuenta_s==null && other.getNum_cuenta_s()==null) || 
             (this.num_cuenta_s!=null &&
              this.num_cuenta_s.equals(other.getNum_cuenta_s())))
            && ((this.apaterno==null && other.getApaterno()==null) || 
             (this.apaterno!=null &&
              this.apaterno.equals(other.getApaterno())))
            && ((this.amaterno==null && other.getAmaterno()==null) || 
             (this.amaterno!=null &&
              this.amaterno.equals(other.getAmaterno())))
            && ((this.nombre_clte==null && other.getNombre_clte()==null) || 
             (this.nombre_clte!=null &&
              this.nombre_clte.equals(other.getNombre_clte())))
            && this.ciclo_fac == other.getCiclo_fac()
            && this.codigo_fv == other.getCodigo_fv()
            && this.fec_ult_fac_cal == other.getFec_ult_fac_cal()
            && this.fec_prox_fac_cal == other.getFec_prox_fac_cal()
            && this.fec_ini_fac_act == other.getFec_ini_fac_act()
            && this.fec_consulta == other.getFec_consulta()
            && this.fec_ult_proc == other.getFec_ult_proc()
            && this.fec_ult_trans == other.getFec_ult_trans()
            && this.fec_prim_trans == other.getFec_prim_trans()
            && ((this.ind_presencia_datos==null && other.getInd_presencia_datos()==null) || 
             (this.ind_presencia_datos!=null &&
              this.ind_presencia_datos.equals(other.getInd_presencia_datos())))
            && ((this.ind_trans_restantes==null && other.getInd_trans_restantes()==null) || 
             (this.ind_trans_restantes!=null &&
              this.ind_trans_restantes.equals(other.getInd_trans_restantes())))
            && this.transacciones_c == other.getTransacciones_c()
            && this.monto1 == other.getMonto1()
            && this.monto2 == other.getMonto2()
            && this.monto3 == other.getMonto3()
            && this.monto4 == other.getMonto4()
            && this.monto5 == other.getMonto5()
            && this.monto6 == other.getMonto6()
            && this.monto7 == other.getMonto7()
            && this.numero1 == other.getNumero1()
            && this.numero2 == other.getNumero2()
            && this.numero3 == other.getNumero3()
            && this.numero4 == other.getNumero4()
            && this.numero5 == other.getNumero5()
            && this.numero6 == other.getNumero6()
            && this.numero7 == other.getNumero7()
            && ((this.flag1==null && other.getFlag1()==null) || 
             (this.flag1!=null &&
              this.flag1.equals(other.getFlag1())))
            && ((this.flag2==null && other.getFlag2()==null) || 
             (this.flag2!=null &&
              this.flag2.equals(other.getFlag2())))
            && ((this.flag3==null && other.getFlag3()==null) || 
             (this.flag3!=null &&
              this.flag3.equals(other.getFlag3())))
            && ((this.flag4==null && other.getFlag4()==null) || 
             (this.flag4!=null &&
              this.flag4.equals(other.getFlag4())))
            && ((this.flag5==null && other.getFlag5()==null) || 
             (this.flag5!=null &&
              this.flag5.equals(other.getFlag5())))
            && ((this.flag6==null && other.getFlag6()==null) || 
             (this.flag6!=null &&
              this.flag6.equals(other.getFlag6())))
            && ((this.flag7==null && other.getFlag7()==null) || 
             (this.flag7!=null &&
              this.flag7.equals(other.getFlag7())))
            && ((this.string1==null && other.getString1()==null) || 
             (this.string1!=null &&
              this.string1.equals(other.getString1())))
            && ((this.string2==null && other.getString2()==null) || 
             (this.string2!=null &&
              this.string2.equals(other.getString2())))
            && ((this.string3==null && other.getString3()==null) || 
             (this.string3!=null &&
              this.string3.equals(other.getString3())))
            && ((this.string4==null && other.getString4()==null) || 
             (this.string4!=null &&
              this.string4.equals(other.getString4())))
            && ((this.string5==null && other.getString5()==null) || 
             (this.string5!=null &&
              this.string5.equals(other.getString5())))
            && ((this.string6==null && other.getString6()==null) || 
             (this.string6!=null &&
              this.string6.equals(other.getString6())))
            && ((this.string7==null && other.getString7()==null) || 
             (this.string7!=null &&
              this.string7.equals(other.getString7())))
            && ((this.filler_04==null && other.getFiller_04()==null) || 
             (this.filler_04!=null &&
              this.filler_04.equals(other.getFiller_04())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        RespuestaMovimientosNoFacturados history = (RespuestaMovimientosNoFacturados) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.transacciones==null && other.getTransacciones()==null) || 
             (this.transacciones!=null &&
              this.transacciones.equals(other.getTransacciones())))
            && ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        RespuestaMovimientosNoFacturados history = (RespuestaMovimientosNoFacturados) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        _hashCode += getCoderror();
        if (getMsgerror() != null) {
            _hashCode += getMsgerror().hashCode();
        }
        if (getIdwsst247() != null) {
            _hashCode += getIdwsst247().hashCode();
        }
        _hashCode += getOrganizacion_s();
        if (getNum_cuenta_s() != null) {
            _hashCode += getNum_cuenta_s().hashCode();
        }
        if (getApaterno() != null) {
            _hashCode += getApaterno().hashCode();
        }
        if (getAmaterno() != null) {
            _hashCode += getAmaterno().hashCode();
        }
        if (getNombre_clte() != null) {
            _hashCode += getNombre_clte().hashCode();
        }
        _hashCode += getCiclo_fac();
        _hashCode += getCodigo_fv();
        _hashCode += getFec_ult_fac_cal();
        _hashCode += getFec_prox_fac_cal();
        _hashCode += getFec_ini_fac_act();
        _hashCode += getFec_consulta();
        _hashCode += getFec_ult_proc();
        _hashCode += getFec_ult_trans();
        _hashCode += getFec_prim_trans();
        if (getInd_presencia_datos() != null) {
            _hashCode += getInd_presencia_datos().hashCode();
        }
        if (getInd_trans_restantes() != null) {
            _hashCode += getInd_trans_restantes().hashCode();
        }
        _hashCode += getTransacciones_c();
        if (getTransacciones() != null) {
            _hashCode += getTransacciones().hashCode();
        }
        _hashCode += new Double(getMonto1()).hashCode();
        _hashCode += new Double(getMonto2()).hashCode();
        _hashCode += new Double(getMonto3()).hashCode();
        _hashCode += new Double(getMonto4()).hashCode();
        _hashCode += new Double(getMonto5()).hashCode();
        _hashCode += new Double(getMonto6()).hashCode();
        _hashCode += new Double(getMonto7()).hashCode();
        _hashCode += new Long(getNumero1()).hashCode();
        _hashCode += new Long(getNumero2()).hashCode();
        _hashCode += new Long(getNumero3()).hashCode();
        _hashCode += new Long(getNumero4()).hashCode();
        _hashCode += new Long(getNumero5()).hashCode();
        _hashCode += new Long(getNumero6()).hashCode();
        _hashCode += new Long(getNumero7()).hashCode();
        if (getFlag1() != null) {
            _hashCode += getFlag1().hashCode();
        }
        if (getFlag2() != null) {
            _hashCode += getFlag2().hashCode();
        }
        if (getFlag3() != null) {
            _hashCode += getFlag3().hashCode();
        }
        if (getFlag4() != null) {
            _hashCode += getFlag4().hashCode();
        }
        if (getFlag5() != null) {
            _hashCode += getFlag5().hashCode();
        }
        if (getFlag6() != null) {
            _hashCode += getFlag6().hashCode();
        }
        if (getFlag7() != null) {
            _hashCode += getFlag7().hashCode();
        }
        if (getString1() != null) {
            _hashCode += getString1().hashCode();
        }
        if (getString2() != null) {
            _hashCode += getString2().hashCode();
        }
        if (getString3() != null) {
            _hashCode += getString3().hashCode();
        }
        if (getString4() != null) {
            _hashCode += getString4().hashCode();
        }
        if (getString5() != null) {
            _hashCode += getString5().hashCode();
        }
        if (getString6() != null) {
            _hashCode += getString6().hashCode();
        }
        if (getString7() != null) {
            _hashCode += getString7().hashCode();
        }
        if (getFiller_04() != null) {
            _hashCode += getFiller_04().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
