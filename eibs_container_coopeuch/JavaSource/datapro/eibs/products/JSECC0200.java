package datapro.eibs.products;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ECC020001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;


/**
 * 
 * @author evargas
 *
 */
public class JSECC0200 extends JSEIBSServlet {


	protected static final int R_ENTER_ACTIVATE_SALES = 100;
	protected static final int R_LIST_ACTIVATE_SALES  = 200;	
	protected static final int A_LIST            = 300;

	private static final long serialVersionUID = 5872589705926125000L;
	

	@Override
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
			case R_ENTER_ACTIVATE_SALES:
				procReqActivateSalesList(user, req, res, session);
				break;
			case R_LIST_ACTIVATE_SALES:
				procActionActivateSalesList(user, req, res, session);
				break;			
			default:
				forward(SuperServlet.devPage, req, res);
				break;
		}
	}
	
	protected void procReqActivateSalesList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
			//Only for actual date
			ECC020001Message msgSerch = new ECC020001Message();
			msgSerch.setE01SPVISD(user.getBigDecimalE01RDD());
			msgSerch.setE01SPVISM(user.getBigDecimalE01RDM());
			msgSerch.setE01SPVISY(user.getBigDecimalE01RDY());
			
			//Only for user branchs 
			msgSerch.setE01SPVINB(user.getE01UBR());
			
			ses.setAttribute("msgSerch", msgSerch);
			
			String center = req.getParameter("center");
			center = (center==null)?"":center;
			forward("ECC0200_activate_sales_enter_search.jsp?center="+center, req, res);	
	}
	
	
	protected void procActionActivateSalesList(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession session)
			throws ServletException, IOException {
			
			MessageProcessor mp = null;
			try {
				mp = getMessageProcessor("ECC0200", req);

				ECC020001Message msg = (ECC020001Message) mp.getMessageRecord("ECC020001");				
				setMessageRecord(req, msg);//caption  search value
				msg.setH01USERID(user.getH01USR());
			 	mp.sendMessage(msg);
			
				// Receive salesPlatform list
				ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			 	 
		        if(mp.hasError(msgError)){		        	
		        	ECC020001Message hmsg = new ECC020001Message();
		        	 setMessageRecord(req, hmsg);//caption  search value	
		        	 session.setAttribute("msgSerch", hmsg);
		        	 session.setAttribute("error", msgError);
		        	 forward("ECC0200_activate_sales_enter_search.jsp", req, res);
		        }else{		        
		        	session.setAttribute("actSaleslist", list);
					forwardOnSuccess("ECC0200_activate_sales_list.jsp", req, res);
		        }

			} finally {
				if (mp != null)	mp.close();
			}
		}	

}