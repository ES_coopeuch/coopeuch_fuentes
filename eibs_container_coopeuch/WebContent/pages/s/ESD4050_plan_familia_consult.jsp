<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<%@page import="datapro.eibs.sockets.MessageRecord"%>
<%@page import="datapro.eibs.beans.ESD405001Message"%>

<html>
<head>
<title>Mantenedor Plan Familia</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ESD405001List" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "ListaVinculo" class= "datapro.eibs.beans.JBList"  scope="session" />
<jsp:useBean id= "valorPlan" class= "java.lang.String"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<SCRIPT Language="Javascript">
builtNewMenu(Plan_Socio_Con);

//  Process according with user selection
 var bandera;	
 function goAction(op) {
	
	
	switch (op){
	// Validate & Write 
  	case 1:  {
    	document.forms[0].APPROVAL.value = 'N';
       	break;
        }
	// Validate and Approve
	case 2:  {
 		document.forms[0].APPROVAL.value = 'Y';
       	break;
		}
		case 3:{
			self.window.location.href = "<%=request.getContextPath()%>/pages/background.jsp";
			break;
		}
	}
}	

function guardarRutInvitados(obj){
	document.forms[0].RUTSINVITADOS.value = "";
	if(document.forms[0].CANTIDAD.value != "1"){
		for (i = 0; i < document.forms[0].CANTIDAD.value; i++){
			if (document.forms[0].ROW[i].checked){
				document.forms[0].RUTSINVITADOS.value = document.forms[0].RUTSINVITADOS.value + document.forms[0].ROW[i].value + ";";
			}
		}
	}
}

function guardarRutDesasociados(obj){
	document.forms[0].RUTSDESASOCIDOS.value = "";
	if(document.forms[0].CANTIDAD.value != "1"){
		for (i = 0; i < document.forms[0].CANTIDAD.value; i++){
			if (document.forms[0].ROWD[i].checked){
				document.forms[0].RUTSDESASOCIDOS.value = document.forms[0].RUTSDESASOCIDOS.value + document.forms[0].ROWD[i].value + ";";
			}
		}
	}
}

</SCRIPT>

 
<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<jsp:useBean id= "planFamilia" class= "datapro.eibs.beans.ESD405001Message"  scope="session" />
<%
 if ( !userPO.getPurpose().equals("NEW") ) {

%>

   <SCRIPT Language="Javascript">
     builtNewMenu(client_personal_opt);
     //document.forms[0].E01STS.value = '2';
     //document.forms[0].E01CCL.value = ' ';
   </SCRIPT>

<%
}
%>

</head>

<body bgcolor="#FFFFFF">

<h3 align="center">Consultar Plan Familia<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="plan_familia_new, ESD4050"  ></h3>
<hr size="4">
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4050" >
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
<INPUT TYPE=HIDDEN NAME="APPROVAL" VALUE="N">
<input type="hidden" name="E01FL5" size="2" maxlength="1" value="<%= client.getE01FL5().trim()%>">
<input type="hidden" name="E01LGT" size="2" maxlength="1" value="<%= client.getE01LGT().trim()%>">
<input type="hidden" name="E01CUN" size="2" maxlength="1" value="<%= userPO.getCusNum()%>">
<input type="hidden" name="E01IDN" size="2" maxlength="1" value="<%= userPO.getHeader2()%>">
<INPUT TYPE=HIDDEN NAME="IDPLANSOCIO" VALUE="<%= planFamilia.getE01PEST()%>">
<INPUT TYPE=HIDDEN NAME="RUTSINVITADOS" VALUE="">
<INPUT TYPE=HIDDEN NAME="RUTSDESASOCIDOS" VALUE="">
<br>
<h4 align="center">Datos del socio Titular</h4>
<table class="tableinfo">
	<tr > 
		<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">Cliente: </td>
					<td nowrap width="12%" aling="left" > <%=userPO.getCusNum() %> </td>
							
					<td nowrap width="6%" aling="right">RUT: </td>
					<td nowrap width="14%" aling="left" > <%=userPO.getHeader2() %> </td>
										
					<td nowrap width="6%" aling="right">ID Plan Socio: </td>
					<td nowrap width="14%" aling="left" > <%=planFamilia.getE01PEST() %> </td>
										
					<td nowrap width="8%" aling="right">Nombre: </td>
					<td nowrap width="20%" aling="left" > <%=userPO.getHeader3() %> </td>
					
					<td nowrap width="8%" aling="right">Estado Plan: </td>
					<td nowrap width="20%" aling="left" > SOLO PLAN FAMILIA </td>
				</tr>
			</table> 
		</td>
	</tr>
</table>
<br>
<%
if ( (!ESD405001List.isEmpty() ) ) {
 %>
<table  id=cfTable class="tableinfo">
	<tr>
		<td NOWRAP valign="top" width="100%">
			<table id="headTable" width="100%">
				<tr id="trdark">
					<th align=center nowrap width="5%">Invitar <br> a mi Plan</th>
					<th align=center nowrap width="5%">Pertenece <br> al Plan</th>
					<th align=center nowrap width="10%">Rut</th>
					<th align=center nowrap width="25%"> Nombre Completo</th>
					<th align=center nowrap width="10%"> Vinculo</th>
					<th align=center nowrap width="15%"> Tiene <br> Plan Socio</th>
				</tr>
				<%
     					
     						
    	  					int row = 0;
    	  					int i = 0;
    	  					ESD405001List.initRow();
          					while (ESD405001List.getNextRow()) {
          						String cadena ="";
            					ESD405001Message msgList = (ESD405001Message) ESD405001List.getRecord();
            					valorPlan = msgList.getL01FILLE4();%>
            					
            					<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>" >
     								<td NOWRAP align="center" width="5%">
     								<% if (msgList.getE01PEST().equals("S")){ %>
     									<input type="checkbox" name="ROW" value="<%= msgList.getE01PIPS()%>" disabled checked onclick="guardarRutInvitados(this);">
     								<% }else{ %>
     									<input type="checkbox" name="ROW" value="<%= msgList.getE01PIPS()%>" disabled onclick="guardarRutInvitados(this);">
     								<% } %>
     								</td>
     								<% if (msgList.getE01PUNP().equals("S")){ %>
     									<td NOWRAP align=center> <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > </td>
     								<% }else{ %>
     									<td NOWRAP align=center> <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" > </td>
     								<% } %>
     								<td NOWRAP align=center> <%= msgList.getE01PIPS() %> </td>
     								<td NOWRAP align=center> <%= msgList.getE01PSNM() %> </td>
     								<td NOWRAP align=center> <%
										ListaVinculo.initRow();
                						int k=1;
                						while (ListaVinculo.getNextRow()) {                							
                							String [] cadenas = ListaVinculo.getRecord().split(",");
                    						if (ListaVinculo.getFlag().equals("")) {
                    							
                    							if(cadenas[0].equals(msgList.getE01PVIN())){
                    								out.println( cadenas[1] );
                    								break;
                    							}
                    							k++;
                    						}        
                						}
 										%> </td>
     								<td NOWRAP align=center> <%= msgList.getE01PTPS() %> </td>  								
     							</tr>
     							
     							<%  i++; 
     						} %>			
			</table>
			<input type="hidden" name="CANTIDAD" size="2" maxlength="10" value="<%= row%>">
		</td>
	</tr>
</table>
<%}else{ %>
<table class="tbenter" width=100% align=center>
	<tr>
		<td class= width="20%">
			<div align="center"><b>No hay registros</b></div>
		</td>
	</tr>	
</table>
<%} %>
<br>
<h4>Cobro</h4>
    <table  class="tableinfo">
  		<tr bordercolor="#FFFFFF">
  			<td nowrap>
	  			<table cellspacing="0" cellpadding="2" width="100%" border="0">
 					<tr id="trclear">
  						<td nowrap width="40%">
  							<div align="right">Tipo de plan :</div>
  						</td>
  						<td nowrap width="20%" colspan="5">
  							<%=planFamilia.getL01FILLE6() %>
                  		</td>
                  		<td width="20%">
  							<div align="right">Valor a Pagar :</div>
  						</td>
  						<td width="20%">
  							<%= valorPlan %> UF
                  		</td>
  					</tr>
  				</table>
  			</td>
  		</tr>
  	</table>	
<br>
<table width="100%">		
	<tr>
		<td width="15%">
			<div align="center"> 
				<input id="EIBSBTN" type="button" name="Submit" value="Cancelar" onClick="javascript:goAction(3);">
			</div>	
		</td>
		<td width="75%">
			<div align="center">
				<input id="EIBSBTN" type="button" name="Submit2" value="Enviar" onClick="javascript:goAction(2);">
			</div>	
		</td>
	</tr>	
</table>	



</form>
</body>
</html>

