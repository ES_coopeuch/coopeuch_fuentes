<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="datarec" class="datapro.eibs.beans.EPV116001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

  	  function cerrarVentana(){
		window.open('','_parent','');
		window.close(); 
  	}
  	
//  Process according with user selection
 function goAction(op) {
	
   	switch (op){
	//Cancel
	case 1:  {
 		//document.forms[0].SCREEN.value = "101";
 		cerrarVentana(); 		
       	break;
		}
	}
	//document.forms[0].submit();
 }
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
	boolean newOnly=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<%
	// Determina si es nuevo o mantencion
	if (userPO.getPurpose().equals("NEW")){
			newOnly=false;
		} else if (userPO.getPurpose().equals("MAINTENANCE")) {
    		newOnly=false;
		} else {
    		newOnly=true;		
		}
%>

<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	CONSULTA DE TARJETA ALIANZA
A TERCEROS<%} else if (maintenance){ %>
	MANTENIMIENTO DE TARJETA ALIANZA
A TERCEROS
<%} else { %>
	NUEVA TARJETA ALIANZA
<%} %>

 <% String emp = (String)session.getAttribute("EMPTA");
 	emp = (emp==null)?"":emp;//si es blanco viene llamado por menu, sino viene llamdo desde la pantalla EPV1010
 %>
 <%if ("".equals(emp)){ %>
 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="tarjeta_alianza_maintenance.jsp, EPV1160"></h3>
<hr size="4">
<%}%>
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1160" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="BANCO"  value="<%= currUser.getE01UBK().trim()%>">
  <input type=HIDDEN name="H01FLGMAS"  value="<%= datarec.getH01FLGMAS().trim()%>">

 <% int row = 0;%>
 
<%if ("".equals(emp)){ %>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="datarec" property="E01PVTCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Propuesta : 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="datarec" property="E01PVTNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">Sequencia :  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="datarec" property="E01PVTSEQ" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="3" maxlength="2" readonly="true"/>
             </td>
         </tr>
       </table>
      </td>
    </tr>
  </table>
  <%}else{%>
  	<input  type="hidden" name="E01PVTCUN" value="<%=datarec.getE01PVTCUN()%>">
  	<input  type="hidden" name="E01PVTNUM" value="<%=datarec.getE01PVTNUM()%>">
  	<input  type="hidden" name="E01PVTSEQ" value="<%=datarec.getE01PVTSEQ()%>"> 
  <%} %>
  
<%if ("".equals(emp)){ %>  
  <h4>Datos de la Tatjeta Alianza</h4>
<%} %>    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Codigo Tarjeta :</div>
            </td>
            <td width="35%">
				<eibsinput:help name="datarec" property="E01PVTCOD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER %>" 
					fn_param_one="E01PVTCOD" fn_param_two="E01DESCRI" fn_param_three="Y" readonly="<%=newOnly%>"/>
 	        </td>
            <td width="15%"> 
              <div align="right">Descripcion :</div>
            </td>
            <td width="35%">            
	            <eibsinput:text property="E01DESCRI" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="true"/>
	        </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Numero Tarjeta :</div>
            </td>
            <td width="35%">            
	            <eibsinput:text name="datarec" property="E01PVTNTR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CARD_NUMBER%>" required="true" readonly="<%=readOnly%>"/>
	        </td>
            <td width="15%"> 
              <div align="right"></div>
            </td>
            <td width="35%">            
	        </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Cantidad :</div>
            </td>
            <td width="35%">            
	            <eibsinput:text name="datarec" property="E01PVTCAN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="4" maxlength="3" readonly="<%=readOnly%>"/>
	        </td>
            <td width="15%"> 
              <div align="right">Valor Unitario :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01PVTVAU" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Monto Total :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01PVTAMT" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"  readonly="true"/>
            </td>
            <td width="15%"> 
              <div align="right"></div>
            </td>
            <td width="35%">            
	        </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>
  

  
   <%if  (!readOnly) { %>
      <div align="center"> 
          <input id="EIBSBTN" type=submit name="Submit" value="Enviar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>
    <% } else { %>
      <div align="center"> 
          <input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>     
    <% } %>  
  </form>
  
 <%if ("S".equals(request.getAttribute("ACT"))){%>
 <script type="text/javascript">
 	  //recargamos la pagina que nos llamo..	  
	  window.opener.location.href =  '<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1160?SCREEN=101&E01PVTCUN=<%=userPO.getCusNum()%>&E01PVTNUM=<%=userPO.getHeader23()%>';	   	   
     <%//NOTA: solo para activar el check de la pagina integral%>
     <%  String re =(String) session.getAttribute("EMPTA");%>
	 <%  if ("S".equals(re)){%>
			window.opener.parent.setRecalculate3();	        
	 <% } %>   	 
  	  //cerramos la ventana
  	  cerrarVentana();	  
 </script>
 <% } %>  
   
</body>
</HTML>
