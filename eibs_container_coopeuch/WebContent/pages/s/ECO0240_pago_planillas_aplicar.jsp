
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ECO020001Message,datapro.eibs.beans.ECO020002Message"%>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.datapro.constants.Entities"%> 

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<title>Listado de Convenios</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
 
<jsp:useBean id="apply" class="datapro.eibs.beans.ECO024004Message" scope="session" />
<jsp:useBean id="HClient" class="datapro.eibs.beans.ECO024001Message" scope="session" />
<jsp:useBean id="ECO024002ListFilter" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

   
function Validar() 
{
   var formLength= document.forms[0].elements.length;
   var form = document.forms[0];
   
   var	IndAlerta = 0;	

   for(n=1;n<10;n++)
   {
	   var Str_concepto = form["E04OFFCO"+ n].value;
	   var Flt_Monto = parseFloat(form["E04OFFAM"+ n].value);
	   
	   if(Str_concepto != "" && Flt_Monto == 0) 
  		   if(!(confirm("Item " + Str_concepto + " contine valor en cero. Esta seguro de procesar?")))
  		   {
  		   		var	IndAlerta = 1;
  		   		break;
  		   }	
  		   		
  	}
   
	if(IndAlerta == 0)
		 document.forms[0].submit();
   
   
//cierre
}	

function goBack()
{  
	document.forms[0].SCREEN.value=200; 
	document.forms[0].submit();
}
  	
  	
  	builtHPopUp();
  	
	function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   		init(opth,field,bank,ccy,field1,field2,opcod);
		showPopupHelp();
	}
  	
</script>


</head>

<body onload="tableresize();">
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Pago de Planilla
	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="pago_planillas_aplicar.jsp, ECO0240"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0240">
<input type="hidden" name="SCREEN" value="500">
<input type="hidden" name="E01SELECU" value="<%=HClient.getE01PLHECU()%>">

<table class="tableinfo" align="center" width="100%">
	<tr>
	<td nowrap valign="top" width="100%">
		<table width="100%">
			<tr id="trdark">
				<th align="right"  nowrap  width="10%">Nro. Cliente Empresa :</th>
				<th align="left"   nowrap width="15%"><%=HClient.getE01PLHECU()%></th>
				<th align="right"  nowrap width="10%">Rut Empresa :</th>
				<th align="left"   nowrap width="15%"><%=HClient.getE01PLHEID()%></th>
				<th align="right"  nowrap width="10%">Nombre Empresa :</th>
				<th align="left"   nowrap  width="40%"><%=HClient.getE01PLHENM() %></th>				
			</tr>
		</table>
	</td>
  </tr>
</table>
<table id="mainTable" class="tableinfo" align="center">
  <tr>
	<td nowrap valign="top" width="100%">
		<table width="100%">
			<tr id="trclear">
				<td align="center" width="13%">Nro. Planillas Seleccionadas</td>
				<td align="center" width="9%">Moneda</td>	
				<td align="center" width="13%">Total Planilla</td>				
				<td align="center" width="13%">Monto Recibido</td>				
				<td align="center" width="13%">Recibido Aplicado</td>				
				<td align="center" width="13%"><B>Recibido Rechazado</B></td>				
				<td align="center" width="13%">Pagado de Menos</td>						
				<td align="center" width="13%">Monto Impago</td>				
			</tr>
			<tr id="trdark">		
				<td align="center" nowrap  >
					<%=ECO024002ListFilter.size()%>
				</td>	
				<td align="center" nowrap  >								
					<eibsinput:text name="apply" property="E04PLHRCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" readonly="true"/>	
				</td>							
				<td align="center" nowrap  >		
					<eibsinput:text name="apply" property="E04PLHPAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>				
				</td>
				<td align="center" nowrap  >
					<eibsinput:text name="apply" property="E04PLHPPG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>					
				</td>
				<td align="center" nowrap  >
					<eibsinput:text name="apply" property="E04PLHPAP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>									
				</td>
				<td align="center" nowrap  >
					<eibsinput:text name="apply" property="E04PLHPRE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>										
				</td>
				<td align="center" nowrap  >
					<eibsinput:text name="apply" property="E04PLHPPN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
				</td>
				<td align="center" nowrap  >
					<eibsinput:text name="apply" property="E04PLHNRP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
				</td>
			</tr>			
		</table>
      </td>
    </tr>
 </table>


<h4>Informacion Del Pago</h4>
<table class="tableinfo">
  <tr > 
    <td> 
      <table cellspacing=0 cellpadding=2 width="100%" border="0">
      	<tr id="trdark">
            <td width="40%"> 
              <div align="right">Fecha Valor :</div>
            </td>
            <td width="60%"> 
    	        <eibsinput:date name="apply" fn_year="E04PLPVD3" fn_month="E04PLPVD2" fn_day="E04PLPVD1" />
            </td>
          </tr>
      	<tr id="trclear">
            <td width="40%"> 
              <div align="right">Banco Sucursal :</div>
            </td>
            <td width="60%"> 
				<input type="text" name="E04PLPBNK" maxlength="2" size="3" value="01" READONLY>
 	 			<input type="text" name="E04PLPBRN" maxlength="4" size="5" value="100" onkeypress=" enterInteger()" class="TXTRIGHT" READONLY>
 	 		</td>
          </tr>
      	<tr id="trdark">
            <td width="40%"> 
              <div align="right">Descripci&oacute;n :</div>
            </td>
            <td width="60%"> 
	  			<eibsinput:text name="apply" property="E04PLPNAR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NARRATIVE %>" />
            </td>
          </tr>
          

     
     </table>
     

  <h4>Origen de Fondos</h4>
  <TABLE id="mainTable" class="tableinfo">
  <TR>
   <TD>
	<table id="headTable" >
    <tr id="trdark"> 
      <td nowrap align="center" > Concepto</td>
      <td nowrap align="center" >Banco </td>
      <td nowrap align="center" >Sucursal</td>
      <td nowrap align="center" >Moneda</td>
      <td nowrap align="center" >Referencia</td>
      <td nowrap align="center" >Monto</td>
    </tr>
    </table> 
      
    <div id="dataDiv" style="height:60; overflow-y :scroll; z-index:0" >
     <table id="dataTable" >
          <%
  				   int amount = 9;
 				   String name;
  					for ( int i=1; i<=amount; i++ ) {
   					  name = i + "";
   			%> 
          <tr id="trclear"> 
            <td nowrap > 
              <div align="center" nowrap> 
                <input type="text" name="E04OFFOP<%= name %>" id="E04OFFOP<%= name %>" value="<%= apply.getField("E04OFFOP"+name).getString().trim()%>" size="3" maxlength="3">
                <input type="hidden" name="E04OFFGL<%= name %>" value="<%= apply.getField("E04OFFGL"+name).getString().trim()%>">
                <input type="text" name="E04OFFCO<%= name %>" size="21" maxlength="20" readonly value="<%= apply.getField("E04OFFCO"+name).getString().trim()%>" 
                  oncontextmenu="showPopUp(conceptHelp,this.name,document.forms[0].E04PLPBNK.value,'','E04OFFGL<%= name %>','E04OFFOP<%= name %>','PL'); return false;">
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
               <%if(apply.getField("E04OFFBK"+name).getString().trim().equals("")) {%>
                <input type="text" name="E04OFFBK<%= name %>" size="2" maxlength="2" value="01">
               <%} else { %>
                <input type="text" name="E04OFFBK<%= name %>" size="2" maxlength="2" value="<%= apply.getField("E04OFFBK"+name).getString().trim()%>">
               <%}%>

              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E04OFFBR<%= name %>" size="4" maxlength="4" value="<%= apply.getField("E04OFFBR"+name).getString().trim()%>"
                oncontextmenu="showPopUp(branchHelp,this.name,document.forms[0].E04PLPBNK.value,'','','',''); return false;">
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
               <%if(apply.getField("E04OFFCY"+name).getString().trim().equals("")) {%>
                <input type="text" name="E04OFFCY<%= name %>" size="3" maxlength="3" value="CLP"
                oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E04PLPBNK.value,'','','',''); return false;">
               <%} else { %>
                <input type="text" name="E04OFFCY<%= name %>" size="3" maxlength="3" value="<%= apply.getField("E04OFFCY"+name).getString().trim()%>"
                oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E04PLPBNK.value,'','','',''); return false;">
               <%}%>

              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E04OFFAC<%= name %>" id="E04OFFAC<%= name %>" size="12" maxlength="12"  value="<%= apply.getField("E04OFFAC"+name).getString().trim()%>"
                oncontextmenu="showPopUp(accountHelp,this.name,document.forms[0].E04PLPBNK.value,'','','','RT'); return false;">
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E04OFFAM<%= name %>" size="23" maxlength="15"  value="<%= apply.getField("E04OFFAM"+name).getString().trim()%>" onKeypress="enterDecimal()">
              </div>
            </td>
          </tr>
          <%
    		}
    		%> 
    	  </table>
        </div>
        
     <table id="footTable" > 
          <tr id="trdark"> 
            <td nowrap  align="right"><b>Equivalente Moneda del Certificado :</b> 
            </td>
            <td nowrap align="center"><b><i><strong> 
                <input type="text" name="E04OFFEQV" size="23" maxlength="17" readonly value="<%= apply.getE04OFFEQV().trim()%>">
                </strong></i></b>
            </td>
          </tr>
        </table>
	  </TD>  
 </TR>	
</TABLE>  
	</td>
	</tr>
</table>
 
<table align="center" id="TBHELPN">
	<tr>
	<td>
	<div id="DIVSUBMIT" align="center">
    	<input id="EIBSBTN" type=button name="Submit" value="Enviar" onClick="javascript:Validar()">	

	</div>
	</td>
	</tr>
</table>
	
</form>
 <SCRIPT language="javascript">
    function tableresize() {
     adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  window.onresize=tableresize;
  </SCRIPT>
</body>
</html>
