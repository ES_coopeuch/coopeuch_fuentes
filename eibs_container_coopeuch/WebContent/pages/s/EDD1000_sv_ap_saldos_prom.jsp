<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>

<html>
<head>
<title>Saldos Promedios Ahorro Vivienda</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "svSaldos" class= "datapro.eibs.beans.EDD210001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

	builtNewMenu(sv_a_opt);

</SCRIPT>

</head>


<body bgcolor="#FFFFFF">


<% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors();");
     out.println("</SCRIPT>");
 }
   out.println("<SCRIPT> initMenu(); </SCRIPT>");
%>

<h3 align="center">Consulta Saldos Promedios <br> Ahorro Vivienda<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="rt_ap_tit.jsp,EDD1000"></h3>
<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXEDD0000" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="49">
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" readonly name="E04DEACUN" size="10" maxlength="9"  value="<%= userPO.getHeader2().trim() %>">
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"> 
                <input type="text" readonly name="E04CUSNA1" size="45" maxlength="45" value="<%= userPO.getHeader3().trim()%>">
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
                <input type="text" readonly name="E06ACC" size="13" maxlength="12" value="<%= svSaldos.getE01AVPACC().trim()%>">
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
                <input type="text" readonly name="E01DEACCY" size="3" maxlength="3" value="<%= userPO.getCurrency().trim()%>" >
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left">
                <input type="text" readonly name="E04DEAPRO" size="5" maxlength="4"value="<%= userPO.getHeader1().trim()%>">
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Saldos Promedio</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table id="headTable" width="100%"> 
          <tr id="trclear"> 
			<td nowrap width="40%" >
				<div align="right">SPEM 1er Semestre (UF) :</div>
			</td>
            <td nowrap width="60%" >
              <eibsinput:text property="E01AVPMS1" size="14" maxlength="13" name="svSaldos" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true" />            
            </td>
         </tr>
          <tr id="trdark"> 
			<td nowrap width="40%" >
				<div align="right">SPEM 2do Semestre (UF) :</div>
			</td>
            <td nowrap width="60%" >
              <eibsinput:text property="E01AVPMS2" size="14" maxlength="13" name="svSaldos" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true" />            
            </td>
         </tr>  
          <tr id="trclear"> 
			<td nowrap width="40%" >
				<div align="right">SPEM 3er Semestre (UF) :</div>
			</td>
            <td nowrap width="60%" >
              <eibsinput:text property="E01AVPMS3" size="14" maxlength="13" name="svSaldos" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true" />            
            </td>
         </tr>   
          <tr id="trdark"> 
			<td nowrap width="40%" >
				<div align="right">SPEM 4to Semestre (UF) :</div>
			</td>
            <td nowrap width="60%" >
              <eibsinput:text property="E01AVPMS4" size="14" maxlength="13" name="svSaldos" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true" />            
            </td>
         </tr>                      
        </table>
      </td>
    </tr>
  </table>

</form>
</body>
</html>
