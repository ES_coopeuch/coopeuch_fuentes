<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Definici&oacute;n de Tablas - Ingreso Lista</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "RTTabEleList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="RTTab" class="datapro.eibs.beans.EDD232005Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 

<script language="JavaScript">



function goAction(op) {

	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}


function cancel() {
	document.forms[0].SCREEN.value = 4100;
	document.forms[0].submit();
}


function goDelete() {

	if(confirm("Esta seguro que desea borrar este codigo?")){
		document.forms[0].opt.value = 3;
		document.forms[0].submit();
	}
}

</SCRIPT>  

</head>

<BODY>
<h3 align="center">Lista de Elementos de Tabla<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="tabla_element_list, EDD2320"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2320" >
    <input type=HIDDEN name="SCREEN" value="6800">
    <input type=HIDDEN name="opt"> 
  
  <h4>Datos Tabla</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Tabla :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E05CMTCTDC" size="5" maxlength="4" value="<%= RTTab.getE05CMTCTDC().trim()%>" 	 readonly>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Descripci&oacute;n  :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E05CMTCGDC" size="31" maxlength="30" value="<%= RTTab.getE05CMTCGDC().trim()%>" readonly>
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Objetivo Tabla :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCTCC" size="2" maxlength="1" value="<%= RTTab.getE05CMTCTCC().trim()%>" readonly>
                <input type="text" name="E05CMTCTCG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCTCG().trim()%>" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Tipo Tabla :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCITB" size="2" maxlength="1" value="<%= RTTab.getE05CMTCITB().trim()%>" readonly>
                <input type="text" name="E05CMTCITG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCITG().trim()%>" readonly>
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Tipo Resultado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCTCC" size="2" maxlength="1" value="<%= RTTab.getE05CMTCTCC().trim()%>" readonly>
                <input type="text" name="E05CMTCTMG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCTMG().trim()%>" readonly>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Moneda Tabla :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCMND" size="4" maxlength="3" value="<%= RTTab.getE05CMTCMND().trim()%>" readonly>
                <input type="text" name="E05CMTCMNG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCMNG().trim()%>"  readonly>
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Moneda Resulato :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCMRT" size="4" maxlength="3" value="<%= RTTab.getE05CMTCMRT().trim()%>" readonly>
                <input type="text" name="E05CMTCMRG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCMRG().trim()%>"  readonly>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left"> 
               </div>
            </td>
          </tr> 
         </table>
      </td>
    </tr>
  </table>
    <%
	if ( RTTabEleList.getNoResult() ) {
 %>

  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
          <table class="tbenter" width=100% align=center>
            <tr> 
              <td class=TDBKG width="50%"> 
                <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
              </td>
              <td class=TDBKG width="50%"> 
                <div align="center"><a href="javascript:cancel()"><b>Volver</b></a></div>
              </td>
            </tr>
          </table>
          <p>&nbsp;</p>
          
        </div>

	  </TD>
	</TR>
    </TABLE>
	
  <%  
		}
	else {
%> <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }

%> 
 
          
  <table class="tbenter" width=100% align=center height="8%">
    <tr> 
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
      </td>
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(2)"><b>Modificar</b></a></div>
      </td>
	<td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goDelete(3)"><b>Borrar</b></a></div>
      </td>      
	<td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:cancel()"><b>Volver</b></a></div>
      </td>      
    </tr>
  </table>
  <br>
 
 

  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
            <th align=LEFT nowrap >ELEMENTO 1</th>
            <th align=LEFT nowrap >ELEMENTO 2</th>
            <th align=LEFT nowrap >ELEMENTO 3</th>
            <th align=LEFT nowrap >ELEMENTO 4</th>
            <th align=LEFT nowrap >ELEMENTO 5</th>
            <th align=LEFT nowrap >ELEMENTO 6</th>
            <th align=LEFT nowrap >ELEMENTO 7</th>
            <th align=LEFT nowrap >ELEMENTO 8</th>
            <th align=LEFT nowrap >MONTO O PORCENTAJE</th>
          </tr>
 
           <%
                RTTabEleList.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (RTTabEleList.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	datapro.eibs.beans.EDD232006Message msgList = (datapro.eibs.beans.EDD232006Message) RTTabEleList.getRecord();
		 %>
          <tr id="dataTable<%= RTTabEleList.getCurrentRow() %>"> 
            <td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE2" value="<%= RTTabEleList.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE06CMTLF81() %></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE06CMTLF82() %></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE06CMTLF83() %></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE06CMTLF84() %></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE06CMTLF85() %></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE06CMTLF86() %></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE06CMTLF87() %></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE06CMTLF88() %></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE06CMTLMCB() %></td>
          </tr>
          <%}%>
          </table>
      </td>
    </tr>
  </table>

  
   

<SCRIPT language="JavaScript">
	showChecked("CURRCODE2");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

</body>
</html>
