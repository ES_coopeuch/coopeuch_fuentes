/* Double Master for banesco */
package com.datapro.eibs.internet;

import datapro.eibs.beans.*;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;
import java.io.*;
import java.net.Socket;
import javax.servlet.*;
import javax.servlet.http.*;

public class JSESS2040 extends SuperServlet
{

    public JSESS2040()
    {
        LangPath = "S";
    }

    public JSESS2040(int logType)
    {
        super(logType);
        LangPath = "S";
    }

    public void service(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        Socket s = null;
        MessageContext mc = null;
        ESS0030DSMessage msgUser = null;
        HttpSession session = null;
        session = req.getSession(false);
        if(session == null)
        {
            try
            {
                res.setContentType("text/html");
                printLogInAgain(res.getWriter());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                flexLog("Exception ocurred. Exception = " + e);
            }
        } else
        {
            int screen = 3;
            try
            {
                msgUser = (ESS0030DSMessage)session.getAttribute("currUser");
                LangPath = SuperServlet.rootPath + msgUser.getE01LAN() + "/";
                try
                {
                    flexLog("Opennig Socket Connection");
                    s = new Socket(SuperServlet.hostIP, SuperServlet.iniSocket + 1);
                    s.setSoTimeout(SuperServlet.sckTimeOut);
                    mc = new MessageContext(new DataInputStream(new BufferedInputStream(s.getInputStream())), new DataOutputStream(new BufferedOutputStream(s.getOutputStream())), "datapro.eibs.beans");
                    try
                    {
                        screen = Integer.parseInt(req.getParameter("SCREEN"));
                    }
                    catch(Exception e)
                    {
                        flexLog("Screen set to default value");
                    }
                    switch(screen)
                    {
                    case 3: // '\003'
                        procReqStatus(mc, msgUser, req, res, session);
                        break;

                    case 2: // '\002'
                        procActionStatus(mc, msgUser, req, res, session);
                        break;

                    case 5: // '\005'
                        procReqActivate(mc, msgUser, req, res, session);
                        break;

                    case 4: // '\004'
                        procActionActivate(mc, msgUser, req, res, session);
                        break;

                    default:
                        res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.devPage);
                        break;
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    int sck = SuperServlet.iniSocket + 1;
                    flexLog("Socket not Open(Port " + sck + "). Error: " + e);
                    res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.sckNotOpenPage);
                }
                finally
                {
                    s.close();
                }
            }
            catch(Exception e)
            {
                flexLog("Error: " + e);
                res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.sckNotRespondPage);
            }
        }
    }

    protected void procActionStatus(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ELEERRMessage msgError = null;
        ESS204001Message msgList = null;
        boolean IsNotError = false;
        String USERID = null;
        USERID = req.getParameter("USERID");
        try
        {
            flexLog("Send Initial Data");
            msgList = (ESS204001Message)mc.getMessageRecord("ESS204001");
            msgList.setH01USERID(user.getH01USR());
            msgList.setH01PROGRM("ESS2040");
            msgList.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgList.setH01OPECOD("0005");
            msgList.setE01EUSUSR(USERID);
            msgList.send();
            msgList.destroy();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            try
            {
                msgError = new ELEERRMessage();
            }
            catch(Exception ex)
            {
                flexLog("Error: " + ex);
            }
            msgError = (ELEERRMessage)newmessage;
            IsNotError = msgError.getERRNUM().equals("0");
            flexLog("IsNotError = " + IsNotError);
            flexLog("Putting java beans into the session");
            ses.setAttribute("error", msgError);
            res.sendRedirect(SuperServlet.srctx + "/servlet/com.datapro.eibs.internet.JSESS2040?SCREEN=3");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
    }

    protected void procReqStatus(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ELEERRMessage msgError = null;
        ESS204001Message msgList = null;
        JBList beanList = null;
        UserPos userPO = null;
        boolean IsNotError = false;
        try
        {
            msgError = new ELEERRMessage();
        }
        catch(Exception ex)
        {
            flexLog("Error: " + ex);
        }
        String LASTUSER="";
		try{
			LASTUSER = req.getParameter("LASTUSER");
			if(LASTUSER==null){
				LASTUSER = "";
			}
		}catch(Exception er){
			LASTUSER = "";
		}
        try
        {
            flexLog("Send Initial Data");
            msgList = (ESS204001Message)mc.getMessageRecord("ESS204001");
            msgList.setH01USERID(user.getH01USR());
            msgList.setH01PROGRM("ESS2040");
            msgList.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgList.setH01OPECOD("0015");
            msgList.setE01EUSUSR(LASTUSER);
            msgList.send();
            msgList.destroy();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR"))
            {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            newmessage.getFormatName();
            try
            {
                beanList = new JBList();
            }
            catch(Exception ex)
            {
                flexLog("Error: " + ex);
            }
            boolean firstTime = true;
            String marker = "";
            String myFlag = "";
            StringBuffer myRow = null;
            int indexRow = 0;
            userPO = new UserPos();
            for(; !msgList.getE01FINREC().equals("*"); newmessage = mc.receiveMessage())
            {
                msgList = (ESS204001Message)newmessage;
                marker = msgList.getE01FINREC();
                if(marker.equals("*"))
                {
                    beanList.setShowNext(false);
                    break;
                }
                if(firstTime)
                    firstTime = false;
                String sStatus = "";
                if(msgList.getE01EUSSTS().equals("1"))
                    sStatus = "Activo";
                else
                if(msgList.getE01EUSSTS().equals("2"))
                    sStatus = "Inactivo";
                else
                if(msgList.getE01EUSSTS().equals("3"))
                    sStatus = "Suspendido";
                else
                if(msgList.getE01EUSSTS().equals("4"))
                    sStatus = "Pendiente";
                String sType = "";
                if(msgList.getE01EUSCTY().equals("1"))
                    sType = "Corporativo";
                else
                if(msgList.getE01EUSCTY().equals("2"))
                    sType = "Personal";
				if(msgList.getE01EUSUSR().trim().length()>0)  LASTUSER = msgList.getE01EUSUSR();    
                myRow = new StringBuffer("<TR>");
                myRow.append("<TD width=10%><input type=\"radio\" name=\"USERID\" checked value=\"" + msgList.getE01EUSUSR() + "\"></TD>");
                myRow.append("<TD width=20%>" + msgList.getE01EUSUSR() + "</TD>");
                myRow.append("<TD width=20%>" + msgList.getE01EUSCUN() + "</TD>");
                myRow.append("<TD width=30%>" + msgList.getE01CUSNA1() + "</TD>");
                myRow.append("<TD width=10%>" + sStatus + "</TD>");
                myRow.append("<TD width=10%>" + sType + "</TD>");
                myRow.append("</TR>");
                beanList.addRow(myFlag, myRow.toString());
                indexRow++;
                if(!marker.equals("+"))
                    continue;
                beanList.setShowNext(true);
                break;
            }

            flexLog("Putting java beans into the session");
            ses.setAttribute("userPO", userPO);
            ses.setAttribute("appList", beanList);
            ses.setAttribute("error", msgError);
			ses.setAttribute("LASTUSER", LASTUSER);
			
            if(beanList.getNoResult())
                try
                {
                    flexLog("About to call Page: " + LangPath + "MISC_no_result.jsp");
                    res.sendRedirect(SuperServlet.srctx + LangPath + "MISC_no_result.jsp");
                }
                catch(Exception e)
                {
                    flexLog("Exception calling page " + e);
                }
            else
                try
                {
                    flexLog("About to call Page: " + LangPath + "ESS2040_status_list.jsp");
                    callPage(LangPath + "ESS2040_status_list.jsp", req, res);
                }
                catch(Exception e)
                {
                    flexLog("Exception calling page " + e);
                }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
    }

    protected void showERROR(ELEERRMessage m)
    {
        if(SuperServlet.logType != 0)
        {
            flexLog("ERROR received.");
            flexLog("ERROR number:" + m.getERRNUM());
            flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01() + " code : " + m.getERDF01());
            flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02() + " code : " + m.getERDF02());
            flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03() + " code : " + m.getERDF03());
            flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04() + " code : " + m.getERDF04());
            flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05() + " code : " + m.getERDF05());
            flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06() + " code : " + m.getERDF06());
            flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07() + " code : " + m.getERDF07());
            flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08() + " code : " + m.getERDF08());
            flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09() + " code : " + m.getERDF09());
            flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10() + " code : " + m.getERDF10());
        }
    }

    protected void procActionActivate(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ELEERRMessage msgError = null;
        ESS204001Message msgList = null;
        boolean IsNotError = false;
        String USERID = null;
        USERID = req.getParameter("USERID");
        String ENTITY = req.getParameter("E01EUSENT");
        if(ENTITY==null) ENTITY="";
        try
        {
            flexLog("Send Initial Data");
            msgList = (ESS204001Message)mc.getMessageRecord("ESS204001");
            msgList.setH01USERID(user.getH01USR());
            msgList.setH01PROGRM("ESS2040");
            msgList.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgList.setH01OPECOD("0005");
            msgList.setH01FLGWK1(req.getParameter("AOPT"));
            msgList.setE01EUSUSR(USERID);
			msgList.setH01PROGRM(ENTITY);
            msgList.send();  
            msgList.destroy();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            try
            {
                msgError = new ELEERRMessage();
            }
            catch(Exception ex)
            {
                flexLog("Error: " + ex);
            }
            msgError = (ELEERRMessage)newmessage;
            IsNotError = msgError.getERRNUM().equals("0");
            flexLog("IsNotError = " + IsNotError);
            flexLog("Putting java beans into the session");
            ses.setAttribute("error", msgError);
            res.sendRedirect(SuperServlet.srctx + "/servlet/com.datapro.eibs.internet.JSESS2040?SCREEN=5");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
    }

    protected void procReqActivate(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ELEERRMessage msgError = null;
        ESS204001Message msgList = null;
        JBList beanList = null;
        UserPos userPO = null;
        boolean IsNotError = false;
        try
        {
            msgError = new ELEERRMessage();
        }
        catch(Exception ex)
        {
            flexLog("Error: " + ex);
        }
		String LASTUSER = "";
		
		try{
			LASTUSER = req.getParameter("LASTUSER");
			if(LASTUSER==null){
				LASTUSER = "";
			}
		}catch(Exception er){
			LASTUSER = "";
		}
        
        try
        {
            flexLog("Send Initial Data");
            msgList = (ESS204001Message)mc.getMessageRecord("ESS204001");
            msgList.setH01USERID(user.getH01USR());
            msgList.setH01PROGRM("ESS2040");
            msgList.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgList.setH01OPECOD("0016");
            msgList.setE01EUSUSR(LASTUSER);
            msgList.send();
            msgList.destroy();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR"))
            {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            newmessage.getFormatName();
            try
            {
                beanList = new JBList();
            }
            catch(Exception ex)
            {
                flexLog("Error: " + ex);
            }
            boolean firstTime = true;
            String marker = "";
            String myFlag = "";
            StringBuffer myRow = null;
            int indexRow = 0;
            userPO = new UserPos();
            for(; !msgList.getE01FINREC().equals("*"); newmessage = mc.receiveMessage())
            {
                msgList = (ESS204001Message)newmessage;
                marker = msgList.getE01FINREC();
                if(marker.equals("*"))
                {
                    beanList.setShowNext(false);
                    break;
                }
                if(firstTime)
                    firstTime = false;
                String sStatus = "";
                if(msgList.getE01EUSSTS().equals("1"))
                    sStatus = "Activo";
                else
                if(msgList.getE01EUSSTS().equals("2"))
                    sStatus = "Inactivo";
                else
                if(msgList.getE01EUSSTS().equals("3"))
                    sStatus = "Suspendido";
                else
                if(msgList.getE01EUSSTS().equals("4"))
                    sStatus = "Pendiente";
                String sType = "";
                if(msgList.getE01EUSCTY().equals("1")) 
                    sType = "Corporativo";
                else
                if(msgList.getE01EUSCTY().equals("2"))
                    sType = "Personal";
                String link = "<a href='javascript:CenterWindow(webapp+\"/servlet/com.datapro.eibs.internet.JSESS2000?SCREEN=409&TYPE=2&CUSID=" + msgList.getE01EUSUSR() + "&E01EUSENT=" + msgList.getH01PROGRM() + "\",850,850,2)'>";
                String linkf = "</a>";
                if(msgList.getE01EUSUSR().trim().length()>0)	LASTUSER = msgList.getE01EUSUSR();
                myRow = new StringBuffer("<TR>");
                myRow.append("<TD width=10%><input type=\"radio\" name=\"USERIDX\" checked value=\"" + msgList.getE01EUSUSR() + "\" onclick=\"selUser('" + msgList.getE01EUSUSR() + "','" + msgList.getH01PROGRM() +"')\"></TD>");
                myRow.append("<TD width=20%>" + link + msgList.getE01EUSUSR() + linkf + "</TD>");
                myRow.append("<TD width=20%>" + msgList.getE01EUSCUN() + "</TD>");
                myRow.append("<TD width=30%>" + msgList.getE01CUSNA1() + "</TD>");
                myRow.append("<TD width=10%>" + sStatus + "</TD>");
                myRow.append("<TD width=10%>" + sType + "</TD>");
                myRow.append("</TR>");
                beanList.addRow(msgList.getE01EUSUSR(), myRow.toString());
                
                indexRow++;
                if(!marker.equals("+"))
                    continue;
                beanList.setShowNext(true);
                break;
            }

            flexLog("Putting java beans into the session");
            ses.setAttribute("userPO", userPO);
            ses.setAttribute("appList", beanList);
            ses.setAttribute("error", msgError);
			ses.setAttribute("LASTUSER", LASTUSER);
            if(beanList.getNoResult())
                try
                {
                    flexLog("About to call Page: " + LangPath + "MISC_no_result.jsp");
                    res.sendRedirect(SuperServlet.srctx + LangPath + "MISC_no_result.jsp");
                }
                catch(Exception e)
                {
                    flexLog("Exception calling page " + e);
                }
            else
                try
                {
                    flexLog("About to call Page: " + LangPath + "ESS2040_activate_list.jsp");
                    callPage(LangPath + "ESS2040_activate_list.jsp", req, res);
                }
                catch(Exception e)
                {
                    flexLog("Exception calling page " + e);
                }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
    }

    protected static final int R_STATUS = 3;
    protected static final int A_STATUS = 2;
    protected static final int R_ACTIVATE = 5;
    protected static final int A_ACTIVATE = 4;
    protected String LangPath;
}
