<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Definici&oacute;n de Tablas - Ingreso Lista</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="RTTab" class="datapro.eibs.beans.EDD232005Message"  scope="session" />
<jsp:useBean id="RTTabEle" class="datapro.eibs.beans.EDD232006Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

</head>

<BODY>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="JavaScript">
function cancel() {
	document.forms[0].SCREEN.value = 6100;
	document.forms[0].submit();
}

</SCRIPT>  

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "NEW".equals(userPO.getPurpose()) ? "" : "readonly";     
%>



<h3 align="center">
<% if(userPO.getPurpose().equals("NEW")){ %>
Nuevo Elememto de Tabla
<% } else {%>
Mantenci&oacute;n de Elemento de Tabla
<% } %>
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="tabla_element_maitenance, EDD2320"></h3>
<hr size="4">
<form name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2320" >
    <input type=HIDDEN name="SCREEN" value="6600">

  <h4>Datos Tabla</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Tabla :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E05CMTCTDC" size="5" maxlength="4" value="<%= RTTab.getE05CMTCTDC().trim()%>" 	 readonly>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Descripci&oacute;n  :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E05CMTCGDC" size="31" maxlength="30" value="<%= RTTab.getE05CMTCGDC().trim()%>" readonly>
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Objetivo Tabla :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCTCC" size="2" maxlength="1" value="<%= RTTab.getE05CMTCTCC().trim()%>" readonly>
                <input type="text" name="E05CMTCTCG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCTCG().trim()%>" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Tipo Tabla :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCITB" size="2" maxlength="1" value="<%= RTTab.getE05CMTCITB().trim()%>" readonly>
                <input type="text" name="E05CMTCITG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCITG().trim()%>" readonly>
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Tipo Resultado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCTCC" size="2" maxlength="1" value="<%= RTTab.getE05CMTCTCC().trim()%>" readonly>
                <input type="text" name="E05CMTCTMG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCTMG().trim()%>" readonly>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Moneda Tabla :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCMND" size="4" maxlength="3" value="<%= RTTab.getE05CMTCMND().trim()%>" readonly>
                <input type="text" name="E05CMTCMNG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCMNG().trim()%>"  readonly>
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Moneda Resulato :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCMRT" size="4" maxlength="3" value="<%= RTTab.getE05CMTCMRT().trim()%>" readonly>
                <input type="text" name="E05CMTCMRG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCMRG().trim()%>"  readonly>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left"> 
               </div>
            </td>
          </tr> 
         </table>
      </td>
    </tr>
  </table>

  <h4>Datos Elemento</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">Elemento 1 :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E06CMTLF81" size="7" maxlength="6" value="<%= RTTabEle.getE06CMTLF81().trim()%>" <%=readonly %>>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Elemento 2 :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E06CMTLF82" size="7" maxlength="6" value="<%= RTTabEle.getE06CMTLF82().trim()%>" <%=readonly %>>
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Elemento 3 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E06CMTLF83" size="7" maxlength="6" value="<%= RTTabEle.getE06CMTLF83().trim()%>" <%=readonly %>>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Elemento 4 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E06CMTLF84" size="7" maxlength="6" value="<%= RTTabEle.getE06CMTLF84().trim()%>" <%=readonly %>>
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Elemento 5 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E06CMTLF85" size="7" maxlength="6" value="<%= RTTabEle.getE06CMTLF85().trim()%>" <%=readonly %>>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Elemento 6 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E06CMTLF86" size="7" maxlength="6" value="<%= RTTabEle.getE06CMTLF86().trim()%>" <%=readonly %>>
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Elemento 7 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E06CMTLF87" size="7" maxlength="6" value="<%= RTTabEle.getE06CMTLF87().trim()%>" <%=readonly %>>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Elemento 8 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E06CMTLF88" size="7" maxlength="6" value="<%= RTTabEle.getE06CMTLF88().trim()%>" <%=readonly %> >
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Monto de Porcentaje</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E06CMTLMCB" size="17" maxlength="16" value="<%= RTTabEle.getE06CMTLMCB().trim()%>"  onkeypress="enterDecimal()" class="TXTRIGHT">
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
          </tr> 
          
  </table>

  
    <div align="center">
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel()">
  </div>
  
</form>

</body>
</html>
