<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>
<HTML><HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css"> 
<TITLE>Probacion PosVenta de Tarjetas</TITLE>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
 
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "ccPvta" class= "datapro.eibs.beans.ECC016001Message"  scope="session" />
 
<script language="JavaScript">
</script>

</HEAD>
<body > 
<h3 align="center">Aprobacion Solicitudes <br> Post-Venta Tarjetas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cc_posventa_pproval_enter_search,ECC0160"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0160">

  <input type=HIDDEN name="SCREEN" value="200">  
    <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trclear"> 
   	        <td nowrap width="10%">&nbsp;</td>
            <td nowrap width="10%"> 
              <div align="right"><b>B&uacute;squeda por: </b></div>
            </td>
            <td nowrap width="5%">&nbsp;</td>
            <td nowrap width="10%">&nbsp;</td>
            <td nowrap width="40%">&nbsp;</td>
                      
            </tr>
		    <tr id="trclear">
	         <td nowrap>&nbsp;</td>            
            <td nowrap> 
              <div align="right"><b>Sucursal : </b></div>
            </td>
            <td nowrap width="5%">
                <input type="text" name="E01SELINB" size="5" maxlength="4" value="<%= userPO.getBranch() %>" >
              <a href="javascript:GetBranch('E01SELINB','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" border="0"  ></a>                
              (en blanco para todas)</td>
             </tr>
             <tr id="trclear">
             <td nowrap>&nbsp;</td> 
             <td nowrap> 
              <div align="right"><b>Fecha de Solicitud : </b></div>
            </td>
            <td nowrap width="40%">
                <input type="text" name="E01SELISD" size="3" maxlength="2" value="<%= userPO.getHeader1() %>">
                <input type="text" name="E01SELISM" size="3" maxlength="2" value="<%= userPO.getHeader2() %>">
                <input type="text" name="E01SELISY" size="5" maxlength="4" value="<%= userPO.getHeader3() %>">   
				</td>
			</tr> 
		</table>
      </td>
    </tr>
  </table>
  <br>
          <div align="center"> 
            <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
          </div>


</FORM>
</BODY>
</HTML>
 