/**
 * DireccionesAsociadas.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class DireccionesAsociadas  implements java.io.Serializable {
    private java.math.BigInteger id;  // attribute
    private java.math.BigInteger idConsulta;
    private java.lang.String direccion;
    private java.lang.String comuna;
    private java.lang.String ciudad;
    private java.math.BigInteger IFecha;
    private java.lang.String fuente;
    private java.lang.String tipo;

    public DireccionesAsociadas() {
    }

    public java.math.BigInteger getId() {
        return id;
    }

    public void setId(java.math.BigInteger id) {
        this.id = id;
    }

    public java.math.BigInteger getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(java.math.BigInteger idConsulta) {
        this.idConsulta = idConsulta;
    }

    public java.lang.String getDireccion() {
        return direccion;
    }

    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }

    public java.lang.String getComuna() {
        return comuna;
    }

    public void setComuna(java.lang.String comuna) {
        this.comuna = comuna;
    }

    public java.lang.String getCiudad() {
        return ciudad;
    }

    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }

    public java.math.BigInteger getIFecha() {
        return IFecha;
    }

    public void setIFecha(java.math.BigInteger IFecha) {
        this.IFecha = IFecha;
    }

    public java.lang.String getFuente() {
        return fuente;
    }

    public void setFuente(java.lang.String fuente) {
        this.fuente = fuente;
    }

    public java.lang.String getTipo() {
        return tipo;
    }

    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        DireccionesAsociadas other = (DireccionesAsociadas) obj;
        boolean _equals;
        _equals = true
            && ((this.direccion==null && other.getDireccion()==null) || 
             (this.direccion!=null &&
              this.direccion.equals(other.getDireccion())))
            && ((this.comuna==null && other.getComuna()==null) || 
             (this.comuna!=null &&
              this.comuna.equals(other.getComuna())))
            && ((this.ciudad==null && other.getCiudad()==null) || 
             (this.ciudad!=null &&
              this.ciudad.equals(other.getCiudad())))
            && ((this.fuente==null && other.getFuente()==null) || 
             (this.fuente!=null &&
              this.fuente.equals(other.getFuente())))
            && ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        DireccionesAsociadas history = (DireccionesAsociadas) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId())))
            && ((this.idConsulta==null && other.getIdConsulta()==null) || 
             (this.idConsulta!=null &&
              this.idConsulta.equals(other.getIdConsulta())))
            && ((this.IFecha==null && other.getIFecha()==null) || 
             (this.IFecha!=null &&
              this.IFecha.equals(other.getIFecha())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        DireccionesAsociadas history = (DireccionesAsociadas) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getIdConsulta() != null) {
            _hashCode += getIdConsulta().hashCode();
        }
        if (getDireccion() != null) {
            _hashCode += getDireccion().hashCode();
        }
        if (getComuna() != null) {
            _hashCode += getComuna().hashCode();
        }
        if (getCiudad() != null) {
            _hashCode += getCiudad().hashCode();
        }
        if (getIFecha() != null) {
            _hashCode += getIFecha().hashCode();
        }
        if (getFuente() != null) {
            _hashCode += getFuente().hashCode();
        }
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
