<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Pago Aporte Convenios</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "msg" class= "datapro.eibs.beans.ECO200101Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
function ChangeDisplay() 
{
	
	SecFilMonto = document.getElementById("FilMonto");
	SecBtnEnviar = document.getElementById("BtnEnviar");
	SecBtnSelArchivo = document.getElementById("BtnSelArchivo");
	
	SecFilMonto.style.display = "none"; 
	SecBtnSelArchivo.style.display = "none"; 
	SecBtnEnviar.style.display = "";  

}		
		

function GoAction() 
{

	if(confirm ("�Esta Seguro que Desea Procesar ?"))
	{
		document.forms[0].SCREEN.value = "300";
		document.forms[0].submit();		
	}
}			
</SCRIPT>



</head>

<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>
 
<H3 align="center">Pago Aporte Convenios<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="pag_apo_enter, ECO2001"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO2001"  >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">

<%if(msg.getH01FLGWK2().equals("1")){ %>
<p align="center"><b>Se ha sometido a proceso el pago de aporte de convenios en base a los par&aacute;metros ingresados</b></p>
<% }%>

  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0"> 

  	<tr id="trdark"> 
        <td align=CENTER width="50%"> 
          <div align="right">Mes :</div>
        </td>
        <td align="left" width="50%"> 
						<select name="E01PERMM" onchange="ChangeDisplay();" <%if(msg.getH01FLGWK2().equals("1")){out.print("disabled");} %>> 
 		                    <option value="1" <% if (msg.getE01PERMM().equals("1")) out.print("selected"); %>>Enero</option>
 		                    <option value="2" <% if (msg.getE01PERMM().equals("2")) out.print("selected"); %>>Febrero</option>
 		                    <option value="3" <% if (msg.getE01PERMM().equals("3")) out.print("selected"); %>>Marzo</option>
 		                    <option value="4" <% if (msg.getE01PERMM().equals("4")) out.print("selected"); %>>Abril</option>
 		                    <option value="5" <% if (msg.getE01PERMM().equals("5")) out.print("selected"); %>>Mayo</option>
 		                    <option value="6" <% if (msg.getE01PERMM().equals("6")) out.print("selected"); %>>Junio</option>
 		                    <option value="7" <% if (msg.getE01PERMM().equals("7")) out.print("selected"); %>>Julio</option>
 		                    <option value="8" <% if (msg.getE01PERMM().equals("8")) out.print("selected"); %>>Agosto</option>
 		                    <option value="9" <% if (msg.getE01PERMM().equals("9")) out.print("selected"); %>>Septiembre</option>
 		                    <option value="10" <% if (msg.getE01PERMM().equals("10")) out.print("selected"); %>>Octubre</option>
 		                    <option value="11" <% if (msg.getE01PERMM().equals("11")) out.print("selected"); %>>Noviembre</option>
 		                    <option value="12" <% if (msg.getE01PERMM().equals("12")) out.print("selected"); %>>Diciembre</option>
            			</select>
        </td>
      </tr>
  	<tr id="trclear"> 
        <td align=CENTER width="50%"> 
          <div align="right">A�o :</div>
        </td>
        <td align="left" width="50%"> 
	 	         <input type="text" name="E01PERAA" size="5" maxlength="4" value="<%=msg.getE01PERAA().trim()  %>" onKeypress="enterInteger()"  class="TXTRIGHT" <%if(msg.getH01FLGWK2().equals("1")){out.print("readonly");} else {out.print("onchange=\"ChangeDisplay();\" onfocus=\"ChangeDisplay();\" ");} %>>
        </td>
      </tr>



  	<tr id="FilMonto" <%if(!msg.getH01FLGWK1().equals("1")){out.print("style=\"display: none;\"");} %>> 
        <td align=CENTER width="50%"> 
          <div align="right">Monto a Pagar :</div>
        </td>
        <td align="left" width="50%"> 
	 	         <input type="text" name="E01AMTCNV" size="15" value="<%=msg.getE01AMTCNV().trim()  %>" readonly class="TXTRIGHT">
        </td>
      </tr>
  </table>


<%if(!msg.getH01FLGWK2().equals("1")){ %>
 <div align="left" id="BtnEnviar" <%if(msg.getH01FLGWK1().equals("1") && !msg.getE01AMTCNV().trim().equals("0") ){out.print("style=\"display: none;\"");} %> >
  <p align="center"><input id="EIBSBTN" type=submit name="Submit" value="Enviar"></p>
 </div> 

 <div align="left" id="BtnSelArchivo" <%if(!msg.getH01FLGWK1().equals("1") || msg.getE01AMTCNV().trim().equals("0")){out.print("style=\"display: none;\"");} %>>
      <p align="center"><input id="EIBSBTN" type="button" name="Epago" value="Enviar a pago" onclick="GoAction()"></p>
 </div>

<% }%>


</form>
</body>
</html>
