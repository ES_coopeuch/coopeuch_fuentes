package datapro.eibs.client;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDD100002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD403001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ESD408001Message;
import datapro.eibs.beans.ESD408002Message;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.Util;
import datapro.eibs.products.JOActionRedirect;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageContextHandler;
import datapro.eibs.sockets.MessageRecord;

public class JSESD4080 extends JSEIBSServlet {

	protected static final int R_PASSWORD 		= 1;
	protected static final int A_APPROVAL 		= 2;
	protected static final int R_DELETE		 	= 3;
	protected static final int R_APPROVAL 		= 5;
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) 
		throws ServletException, IOException {
			switch (screen) {
				case R_DELETE:
					procReqDelete(user, req, res, session, screen);
					break;
				case R_APPROVAL:
					procReqApproval(user, req, res, session);
					break;
				case A_APPROVAL:
					procActionApproval(user, req, res, session);
					//procReqEntityList(user, req, res, session, screen);
					break;
				default:
					forward(devPage, req, res);
					break;
			}
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	protected void procReqDelete(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) 
			throws ServletException, IOException {
		
		MessageRecord newmessage = null;
		ELEERRMessage msgError = null;
		boolean IsNotError = false;
		MessageContext mc = null;
		String idCliente = "";
		String rutCliente = "";
		try{
			mc = new MessageContext(super.getMessageHandler("ESD4080", req));
			ESD408001Message msgAprobacion = (ESD408001Message) mc.getMessageRecord("ESD408001");
			
			idCliente = req.getParameter("IDSOCIOSELEC");
			rutCliente = req.getParameter("RUTSOCIOSELEC");
			
			msgAprobacion.setH01USR(user.getH01USR());
			msgAprobacion.setH01PGM("ESD4080");
			msgAprobacion.setH01TIM(getTimeStamp());
			msgAprobacion.setH01SCR("01");
			msgAprobacion.setH01OPE("0005");
			msgAprobacion.setE01CUN(idCliente);
			msgAprobacion.setE01RUT(rutCliente);
			
			msgAprobacion.send();	
			msgAprobacion.destroy();	
			flexLog("ESD408001 Message Sent");
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
		
		// Receive Data
		try{
			newmessage = mc.receiveMessage();
			
			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage)newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
			  }
			  else{
				  procReqApproval(user, req, res, session);
				  flexLog("Message " + newmessage.getFormatName() + " received.");
			  }
		}catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
	}
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	private void procActionApproval(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) 
		throws IOException, ServletException {
		
		MessageProcessor mp = null;
		MessageContext mc = null;
		String idCliente = "";
		String rutCliente = "";
		boolean CV = false;
		boolean TC = false;
		
		JBObjList list = (datapro.eibs.beans.JBObjList) session.getAttribute("ESD408001List");
		
		try {
			mc = new MessageContext(super.getMessageHandler("EDD100002", req));
			
			list.initRow();
			ESD408001Message msgList = null;
            while (list.getNextRow()) {
            	msgList = (ESD408001Message) list.getRecord();
            	if (!msgList.getE01ACCV().equals("0") && !CV){
            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCV());
            		CV = true;
            	}
            	
            	if (!msgList.getE01ACCH().equals("0"))
            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCH());
            	
            	if (!msgList.getE01ACCT().equals("0") && !TC){
            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCT());
            		TC = true;
            	}
            }
		} catch (Exception e) {
			flexLog("Error al aprobar producto " + e);
		}
		  
		idCliente = req.getParameter("IDSOCIOSELEC");
		rutCliente = req.getParameter("RUTSOCIOSELEC");
		
		try {
			mp = getMessageProcessor("ESD4080", req);
			ESD408001Message mpApro = (ESD408001Message) mp.getMessageRecord("ESD408001");
			
			mpApro.setH01USR(user.getH01USR());
			mpApro.setH01PGM("ESD4080");
			mpApro.setH01TIM(getTimeStamp());
			mpApro.setH01SCR("01");
			mpApro.setH01OPE("0001");	
			mpApro.setE01CUN(idCliente);
			mpApro.setE01RUT(rutCliente);
			
			mp.sendMessage(mpApro);
			
			MessageRecord msg = mp.receiveMessageRecord();
			
			if (mp.hasError(msg)) {
				session.setAttribute("error", msg);
				forward("ESD4080_approval_list.jsp", req, res);
			} else{
				procReqApproval(user, req, res, session);
			}
		} finally {
			if (mp != null) mp.close();
		}
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	private void procReqApproval(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) 
		throws IOException, ServletException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;	
		try{
			mp = getMessageProcessor("ESD4080", req);
			
			ESD408001Message msgPlan = (ESD408001Message) mp.getMessageRecord("ESD408001");
			
			msgPlan.setH01USR(user.getH01USR());
			msgPlan.setH01PGM("ESD4080");
			msgPlan.setH01TIM(getTimeStamp());
			msgPlan.setH01SCR("01");
			msgPlan.setH01OPE("0015");
			
			mp.sendMessage(msgPlan);
			
			JBObjList list = mp.receiveMessageRecordList("H01MAS");
			JBObjList listAprob = new JBObjList();
			String idPlan = "";
			int i = 0;
			ESD408001Message msgList = null;
			list.initRow();
            while (list.getNextRow()) {
            	msgList = (ESD408001Message) list.getRecord();
            	if (!msgList.getE01ACMAX1().equals(idPlan)){
            		ESD408001Message msgListAprob = null;
        			msgListAprob = new ESD408001Message();
            		msgListAprob.setE01ACMAX1(msgList.getE01ACMAX1());
            		msgListAprob.setE01CUN(msgList.getE01CUN());
            		msgListAprob.setE01RUT(msgList.getE01RUT());
            		msgListAprob.setE01NOM(msgList.getE01NOM());
            		if (msgList.getE01ACCV().equals("0"))
            			msgListAprob.setE01ACMAX4("0");
            		else
            			msgListAprob.setE01ACMAX4("1");
            		if (msgList.getE01ACCH().equals("0"))
            			msgListAprob.setE01ACMAX5("0");
            		else
            			msgListAprob.setE01ACMAX5("1");
            		if (msgList.getE01ACCT().equals("0"))
            			msgListAprob.setE01ACMAX6("0");
            		else
            			msgListAprob.setE01ACMAX6("1");
            		listAprob.addRow(msgListAprob);
            		
            		idPlan = msgList.getE01ACMAX1();
            		i++;
            	}else{
            		msgList = (ESD408001Message) list.getRecord();
            	}
            }
			if(mp.hasError(list)){
				session.setAttribute("error", mp.getError(list));
				forward("error_viewer.jsp", req, res);
			}else{
				session.setAttribute("userPO", userPO);
				session.setAttribute("ESD408001ListAprob", listAprob);
				session.setAttribute("ESD408001List", list);
				session.setAttribute("ESD4080", msgPlan);
				session.setAttribute("userPO", userPO);
				forward("ESD4080_approval_list.jsp", req, res);
			}
		}catch (Exception e){
			flexLog("Input data error " + e);
			forward("error_viewer.jsp", req, res);
		}finally{
			if(mp != null) mp.close();
		}			
	}
	
	//------------aprobacion de los productos cuenta vista o una libreta de ahorro-----------------------------------------------------------
	protected void procActionApprovalProductos(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, String numCuenta) 
		throws ServletException, IOException {

		EDD100002Message msgList = null;

		// Send Initial data
		try{
		
			flexLog("Send Initial Data");
			msgList = (EDD100002Message)mc.getMessageRecord("EDD100002");
			msgList.setH02USERID(user.getH01USR());
		 	msgList.setH02PROGRM("EDD1000");
		 	msgList.setH02TIMSYS(getTimeStamp());
		 	//session.setAttribute("numCuenta", datapro.eibs.master.Util.justifyRight(rtFinish.getE15ACCNUM().trim(),12));
		 	msgList.setE02ACMACC(numCuenta);
		 	msgList.setE02ACTION("A");
		 	msgList.setE02MSGTXT("");
		 	msgList.send();	
		 	msgList.destroy();
		}catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}	
	//---------------------------------------------------------------------------------------------------------------------------------------
}
