<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page import = "datapro.eibs.master.Util" %>
<HTML>
<HEAD>
<TITLE>
Historico de Eventos
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
</HEAD>

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "cifList" class= "datapro.eibs.beans.JBList"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<BODY>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">
<%
if (userPO.getPurpose().equals("INQUIRY")){
%>

<%
if ( userPO.getHeader23().equals("G") ||  userPO.getHeader23().equals("V")){
%>
	builtNewMenu(ln_i_1_opt);
<%   
}
else if (userPO.getHeader23().equals("DFT")) {
%>
	builtNewMenu(dft_i_opt);
<%   
}
else  {
%>
	builtNewMenu(ln_i_2_opt);
<%   
}
%>
<%
}
%>

function PrintPreview() {

  <% 
  int iniPos = cifList.getFirstRec() - 1;
  out.println("var pos = " + iniPos + ";");
  %>
	var pg = '<%=request.getContextPath()%>/pages/s/EAPR010_his_list_print_fp.jsp';
	CenterWindow(pg,720,500,2);

}


</SCRIPT>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
   if (userPO.getPurpose().equals("INQUIRY")){ 
   out.println("<SCRIPT> initMenu(); </SCRIPT>");}
%> 

<FORM>
<div id="imprime">
  <h3 align="center">Historico de Eventos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="his_list_fp.jsp,EAPR010"> 
  </h3>
  <hr size="4">
  <BR>
  <table class="tableinfo">
    <tr > 
      <td nowrap height="94"> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="15%"> 
              <div align="right"> <b>Fechas desde :</b></div>
            </td>
            <td nowrap width="1%">&nbsp;</td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01FRDTE1" size="3" maxlength="2" readonly value="<%= userPO.getHeader9().trim()%>">
                <input type="text" name="E01FRDTE2" size="3" maxlength="2" readonly value="<%= userPO.getHeader10().trim()%>">
                <input type="text" name="E01FRDTE3" size="5" maxlength="4" readonly value="<%= userPO.getHeader11().trim()%>">
              </div>
            </td>
            <td nowrap width="15%"> 
              <div align="right"><b>hasta :</b></div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01TODTE1" size="3" maxlength="2" readonly value="<%= userPO.getHeader12().trim()%>">
                <input type="text" name="E01TODTE2" size="3" maxlength="2" readonly value="<%= userPO.getHeader13().trim()%>">
                <input type="text" name="E01TODTE3" size="5" maxlength="4" readonly value="<%= userPO.getHeader14().trim()%>">
              </div>
            </td>
            <td nowrap width="15%"> 
              <div align="right"><b></b></div>
            </td>
            <td nowrap width="15%"> 
              <div align="right"><b></b></div>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="15%" height="26"> 
              <div align="right"><b>Sucursal :</b></div>
            </td>
            <td nowrap width="1%" height="26">&nbsp;</td>
            <td nowrap width="15%" height="26"> 
              <div align="left"> 
                <input type="text" name="E01HISBRN" size="05" maxlength="04"  readonly value="<%= userPO.getHeader18().trim()%>">
              </div>
            </td>
            <td nowrap width="15%" height="26"> 
              <div align="right"><b>Evento :</b></div>
            </td>
            <td nowrap width="15%" height="26"> 
              <div align="left"> 
                <input type="text" name="E01HISEVN" size="05" maxlength="04"  readonly value="<%= userPO.getHeader17().trim()%>">
              </div>
            </td>
            <td nowrap width="15%"> 
              <div align="right"><b>Aplicacion :</b></div>
            </td>
            <td nowrap width="15%"> 
                <input type="text" name="E01HISACD" size="05" maxlength="04"  readonly value="<%= userPO.getHeader19().trim()%>">
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <BR>
<%
	if ( cifList.getNoResult() ) {
   		out.print("<center><h4>No hay resultados que correspondan a su criterio de b�squeda</h4></center>");
	}
	else {
%>
  <h4 align="left">Eventos</h4>
  <TABLE id=cfTable class="tableinfo">
    <TR id=trdark> 
      <TH ALIGN=CENTER nowrap>Nro<br>Operacion</TH>
      <TH ALIGN=CENTER nowrap>Nro<br>Cliente</TH>
      <TH ALIGN=CENTER nowrap>Rut</TH>
      <TH ALIGN=CENTER nowrap>Nombre</TH>
      <TH ALIGN=CENTER nowrap>Producto</TH>
      <TH ALIGN=CENTER nowrap>Tasa</TH>
      <TH ALIGN=CENTER nowrap>Capital</TH>
      <TH ALIGN=CENTER nowrap>Interes</TH>
      <TH ALIGN=CENTER nowrap>Moneda</TH>
      <TH ALIGN=CENTER nowrap>Plazo</TH>
      <TH ALIGN=CENTER nowrap>Ejecutivo<br>Ventas</TH>
      <TH ALIGN=CENTER nowrap>Convenio</TH>
      <TH ALIGN=CENTER nowrap>Fecha<br>Origen</TH>
      <TH ALIGN=CENTER nowrap>Fecha<br>Vencimiento</TH>
      <TH ALIGN=CENTER nowrap>Estado</TH>
      <TH ALIGN=CENTER nowrap>Fecha</TH>
    </TR>
    <%
                cifList.initRow();
                while (cifList.getNextRow()) {
                    if (cifList.getFlag().equals("")) {
                    		out.println(cifList.getRecord());
                    }
                }
              %> 
  </TABLE>
  <BR>
  
  <TABLE class="tbenter" WIDTH="98%" ALIGN=CENTER>
  <TR>
  <TD WIDTH="50%" ALIGN=LEFT>
 <%
        if ( cifList.getShowPrev() ) {
      			int pos = cifList.getFirstRec() - 51;
      			out.println("<A HREF=\""+request.getContextPath()+"/servlet/datapro.eibs.products.JSEAPR010?SCREEN=1&Pos=" + pos +"\"><img src=\""+request.getContextPath()+"/images/s/previous_records.gif\" border=0></A>");
        }
%> 
	</TD>
   <TD WIDTH="50%" ALIGN=RIGHT> 
 <%      
        if ( cifList.getShowNext() ) {
      			int pos = cifList.getLastRec();
      			out.println("<A HREF=\""+request.getContextPath()+"/servlet/datapro.eibs.products.JSEAPR010?SCREEN=1&Pos=" + pos +"\"><img src=\""+request.getContextPath()+"/images/s/next_records.gif\" border=0></A>");
        }
%>
   </TD>
  </TR>
  </TABLE>

  <p align=center>&nbsp; </p>

  <%
  }
%> 

</div>  
  <div align="center"> 
    <input id="EIBSBTN" type=button name="Submit" OnClick="PrintPreview()" value="Imprimir">
  </div>

</FORM>

</BODY>


</HTML>
