<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Tablas de Condonaciones</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="conDetails" class="datapro.eibs.beans.EDL053201Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (userPO.getPurpose().equals("NEW")){
			readOnly=false;
		} else {
			readOnly=true;
		}
%>
<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
%>


<H3 align="center"><% if (userPO.getPurpose().equals("NEW")) out.print("Crear "); else if (userPO.getPurpose().equals("DELETE")) out.print("Eliminar  ");else out.print("Mantencion "); %>Tabla de Condonaciones<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="condonaciones_detail.jsp, EDL0532"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEDL0532" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Canal </b></div>
            </td>
            <td width="35%" > 
            <div align="left"> 
               <select name="E01GCMCHA" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(conDetails.getE01GCMCHA().equals("01")||
                    conDetails.getE01GCMCHA().equals("02") || 
                    conDetails.getE01GCMCHA().equals("03") || 
                    conDetails.getE01GCMCHA().equals("04") || 
                    conDetails.getE01GCMCHA().equals("05") ||
                    conDetails.getE01GCMCHA().equals("06") ||                                          
                    conDetails.getE01GCMCHA().equals("07"))) out.print("selected"); %>>Cualquiera</option>
                    <option value="01" <% if (conDetails.getE01GCMCHA().equals("01")) out.print("selected"); %>>Caja</option>
                    <option value="02" <% if (conDetails.getE01GCMCHA().equals("02")) out.print("selected"); %>>Planilla</option>                   
                    <option value="03" <% if (conDetails.getE01GCMCHA().equals("03")) out.print("selected"); %>>Pago Automatico</option>                   
                    <option value="04" <% if (conDetails.getE01GCMCHA().equals("04")) out.print("selected"); %>>Internet</option> 
                    <option value="05" <% if (conDetails.getE01GCMCHA().equals("05")) out.print("selected"); %>>Interfaz</option>  
                    <option value="06" <% if (conDetails.getE01GCMCHA().equals("06")) out.print("selected"); %>>Plataforma</option> 
                    <option value="07" <% if (conDetails.getE01GCMCHA().equals("07")) out.print("selected"); %>>Interfaz Exenta</option>                                       
                  </select>
                  </div>
             </td>   
             <td nowrap >  </td>   
            </tr>
           <tr id="trclear">
            <td nowrap width="16%" > 
              <div align="right"><b>Centro de Responsabilidad </b></div>
            </td>
            <td nowrap width="35%"> 
                 <eibsinput:cnofc name="conDetails" property="E01GCMRES" flag="XD" disabled="<%=readOnly%>" fn_code="E01GCMRES" fn_description="D01GCMRES" />
                 <eibsinput:text property="D01GCMRES" name="conDetails" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="true" disabled="<%=readOnly%>"/>
            </td>
          <td nowrap >  </td> 
          </tr>
           <tr id="trdark">
            <td nowrap width="16%" > 
              <div align="right"><b>Estado del Pr&eacute;stamo </b></div>
            </td>
            <td width="35%" > 
            <div align="left"> 
               <select name="E01GCMDLC" <%=readOnly?"disabled":""%> >
                    <option value=" " <% if (!(conDetails.getE01GCMDLC().equals("1")||
                    conDetails.getE01GCMDLC().equals("2") || 
                    conDetails.getE01GCMDLC().equals("3") ||                      
                    conDetails.getE01GCMDLC().equals("4"))) out.print("selected"); %>> 
                    </option>
                    <option value="1" <% if (conDetails.getE01GCMDLC().equals("1")) out.print("selected"); %>>Vigente</option>
                    <option value="2" <% if (conDetails.getE01GCMDLC().equals("2")) out.print("selected"); %>>Vencido</option>                   
                    <option value="3" <% if (conDetails.getE01GCMDLC().equals("3")) out.print("selected"); %>>Castigado</option>                   
                    <option value="4" <% if (conDetails.getE01GCMDLC().equals("4")) out.print("selected"); %>>Castigado No Informado</option>                     
                  </select>
                  </div>
             </td> 
            <td nowrap > 
              <input type="radio" <%=readOnly?"disabled":""%> name="E01GCMFL2" value=" " <%if (!conDetails.getE01GCMFL2().equals("1") && 
                                                                  (!conDetails.getE01GCMFL2().equals("2")) &&
                                                                  (!conDetails.getE01GCMFL2().equals("3"))) out.print("checked"); %>>Ninguno                                       
            </td>              
          </tr>
           <tr id="trclear">
            <td nowrap width="16%" > </td>
            <td width="35%" > </td> 
            <td nowrap > 
              <input type="radio" <%=readOnly?"disabled":""%> name="E01GCMFL2" value="1" <%if (conDetails.getE01GCMFL2().equals("1")) out.print("checked"); %>>Avenimiento                                                      
            </td>              
          </tr>
           <tr id="trdark">
            <td nowrap width="16%" > </td>
            <td width="35%" > </td> 
            <td nowrap >              
              <input type="radio" <%=readOnly?"disabled":""%> name="E01GCMFL2" value="2" <%if (conDetails.getE01GCMFL2().equals("2")) out.print("checked"); %>>Convenio Pago                                       
            </td>              
          </tr> 
           <tr id="trclear">
            <td nowrap width="16%" > </td>
            <td width="35%" > </td> 
            <td nowrap > 
              <input type="radio" <%=readOnly?"disabled":""%> name="E01GCMFL2" value="3" <%if (conDetails.getE01GCMFL2().equals("3")) out.print("checked"); %>>Cobranza Judicial                                        
            </td>              
          </tr>                   
        </table>
      </td>
    </tr>
  </table>
  <br>  
   <h4>Condonaci&oacute;n</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap height="16"> 
              <div align="center"><b>Gastos de Cobranza </b></div>
            </td>
            <td width="30%"> 
              <input type="radio" name="E01GCMAGC" value="Y"  <%if (!conDetails.getE01GCMAGC().equals("N")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01GCMAGC" value="N"  <%if (conDetails.getE01GCMAGC().equals("N")) out.print("checked"); %>>
              No
			</td>
            <td nowrap width="16%" > 
              <div align="right"><b> </b></div>
            </td> 
           <td nowrap width="16%" > 
              <div align="right"><b> </b></div>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap  height="16"> 
              <div align="center"><b>Mora </b></div>
            </td>
            <td width="30%"> 
              <input type="radio" name="E01GCMAIM" value="Y"  <%if (!conDetails.getE01GCMAIM().equals("N")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01GCMAIM" value="N"  <%if (conDetails.getE01GCMAIM().equals("N")) out.print("checked"); %>>
              No
			</td>
            <td nowrap width="16%" > 
              <div align="right"><b> </b></div>
            </td> 
           <td nowrap width="16%" > 
              <div align="right"><b> </b></div>
            </td>			
           </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>
  <p><br>
  </p>
  <div align="center"> 
            <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
          </div>
  </form>
</body>
</html>
