/**
 * ResultsInfraccionesLaborales.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResultsInfraccionesLaborales  implements java.io.Serializable {
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.InfraccionesLaborales infraccionesLaborales;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDetalleInfraccionesLab resultsDetalleInfraccionesLab;

    public ResultsInfraccionesLaborales() {
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.InfraccionesLaborales getInfraccionesLaborales() {
        return infraccionesLaborales;
    }

    public void setInfraccionesLaborales(cl.Sinacofi.coopeuch.creditos.predictorriesgo.InfraccionesLaborales infraccionesLaborales) {
        this.infraccionesLaborales = infraccionesLaborales;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDetalleInfraccionesLab getResultsDetalleInfraccionesLab() {
        return resultsDetalleInfraccionesLab;
    }

    public void setResultsDetalleInfraccionesLab(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDetalleInfraccionesLab resultsDetalleInfraccionesLab) {
        this.resultsDetalleInfraccionesLab = resultsDetalleInfraccionesLab;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsInfraccionesLaborales history = (ResultsInfraccionesLaborales) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ResultsInfraccionesLaborales other = (ResultsInfraccionesLaborales) obj;
        boolean _equals;
        _equals = true
            && ((this.infraccionesLaborales==null && other.getInfraccionesLaborales()==null) || 
             (this.infraccionesLaborales!=null &&
              this.infraccionesLaborales.equals(other.getInfraccionesLaborales())))
            && ((this.resultsDetalleInfraccionesLab==null && other.getResultsDetalleInfraccionesLab()==null) || 
             (this.resultsDetalleInfraccionesLab!=null &&
              this.resultsDetalleInfraccionesLab.equals(other.getResultsDetalleInfraccionesLab())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsInfraccionesLaborales history = (ResultsInfraccionesLaborales) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getInfraccionesLaborales() != null) {
            _hashCode += getInfraccionesLaborales().hashCode();
        }
        if (getResultsDetalleInfraccionesLab() != null) {
            _hashCode += getResultsDetalleInfraccionesLab().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
