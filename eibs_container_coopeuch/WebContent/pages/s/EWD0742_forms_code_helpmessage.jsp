<html>
<head><title>Forms Code Help</title>
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ewd0742Help" class= "datapro.eibs.beans.JBList"   scope="session"/>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<SCRIPT language="JavaScript">
	
	setTimeout("top.close()", <%= datapro.eibs.master.JSEIBSProp.getPopUpTimeOut() %>)

	function enter(code) {
		var form = top.opener.document.forms[0];
		if (isValidObject(form[top.opener.fieldName])) {
			form[top.opener.fieldName].value = code;
			form[top.opener.fieldName].focus();
			form[top.opener.fieldName].select();
		}
		top.close();
 	}

</script>
</head>
<body>

<%
	if ( ewd0742Help.getNoResult() ) {
%>
		<TABLE class="tbenter" width=100% height=100%>
   			<TR>
      			<TD><div align="center"> <b> No hay resultados para su criterio de B�squeda </b></div></TD>
      		</TR>
   		</TABLE>
<%   		
	} else {
%>
      
    <TABLE class="tableinfo" align="center" style="width:'95%'">
    	<TR id="trdark"> 
			<th width="20%">Codigo</th>
			<th width="10%">Tipo</th>
			<th width="30%">Descripcion</th>
			<th width="40%">Formulario</th>
		</TR>

<%
         ewd0742Help.initRow();
         while (ewd0742Help.getNextRow()) {
             if (ewd0742Help.getFlag().equals("")) {
             		out.println(ewd0742Help.getRecord());
             }
         }
%> 

  	</TABLE>

<%
   }
%>
</body>
</html>