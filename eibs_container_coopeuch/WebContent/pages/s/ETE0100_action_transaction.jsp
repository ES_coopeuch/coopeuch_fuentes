<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ETE010002Message"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ETE010003Message"%>
<%@page import="com.datapro.constants.EibsFields"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Trasferencias - Seguridad y Parámetros</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="listbancos" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="listproductos" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">
   $(function()
    {
		$("#radio_key1").attr("checked", false);
		$("#radio_key2").attr("checked", false);
    }
    );


function goAction(op) 
{
	if (op =='400')
	{
		document.forms[0].SCREEN.value = op;
		document.forms[0].submit();	
		return;	
	}		

	var ok = false;

 	for(n=0; n<document.forms[0].elements.length; n++)
    {
      	var element = document.forms[0].elements[n];
      	if(element.name == "E01INSFIN") 
	    {	
	      	if (element.checked == true) 
	      	{
	      		document.getElementById("codinsfin").value = element.value; 
        		ok = true;
        		break;
			}
	    }
     } 
      
     if(ok)
     {
      	var confirm1 = true;
      	
      	if (op =='600')
			confirm1 = confirm("Desea Eliminar la Institución Seleccionada?");
      	
	  	if (confirm1)
	  	{
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		
     } 
     else 
		alert("Debe Seleccionar una Institución para Continuar.");	   
}


function goAction2(op) 
{
	if (op =='800')
	{
		document.forms[0].SCREEN.value = op;
		document.forms[0].submit();	
		return;	
	}		

	var ok = false;

 	for(n=0; n<document.forms[0].elements.length; n++)
    {
      	var element = document.forms[0].elements[n];
      	if(element.name == "E03ASOPRO") 
	    {	
	      	if (element.checked == true) 
	      	{
	      		document.getElementById("codasopro").value = element.value; 
        		ok = true;
        		break;
			}
	    }
     } 
      
     if(ok)
     {
      	var confirm1 = true;
      	
      	if (op =='1000')
			confirm1 = confirm("Desea Eliminar el Producto Seleccionado?");
      	
	  	if (confirm1)
	  	{
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		
     } 
     else 
		alert("Debe Seleccionar un Producto para Continuar.");	   
}








function goAction3(op) {

 	document.forms[0].SCREEN.value = op;
	document.forms[0].submit();		
}




</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Transferencias - Seguridad y Par&aacute;metros
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="action_transaction.jsp, ETE0100">
</h3>

<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.tefmulti.JSETE0100">
<input type="hidden" name="SCREEN" value="100">
<input type="hidden" name="codinsfin" value="" id="codinsfin">  
<input type="hidden" name="codasopro" value="" id="codasopro">  



<table  class="tableinfo">
	<tr bordercolor="#FFFFFF"> 
		<td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Tipo de Transacci&oacute;n : 
              </td>
             <td nowrap width="10%" align="left">
               <input type="text" name="E02LIFCOD" value="<%=session.getAttribute("CODTRX")%>" size="4"  readonly>
               <input type="text" name="E02LIFDCO" value="<%=session.getAttribute("DESTRX") %>" size="40" readonly>
             </td>
         </tr>
        </table>
      </td>
    </tr>
</table>

<table width="100%">
<tr id="trclear" >
<td>
<hr style="  border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));  "/>
<h3 align="center" >INSTITUCIONES FINANCIERAS</h3>
</td>
</tr>
</table>

<table class="tbenter" width="100%">
<tr>
<td>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg"> 
			<a href="javascript:goAction('400')"><b>NUEVA</b></a>
		</td>
		<td align="center" class="tdbkg">
			<a href="javascript:goAction('500')"> <b>MANTENCI&Oacute;N</b> </a>
		</td>
		<td align="center" class="tdbkg">
			<a href="javascript:goAction('600')"> <b>ELIMINAR</b> </a>
		</td>
	</tr>
</table>

</td>
</tr>

<tr>
<td>

<table id="headTable"  width="100%" align="left">
	<tr id="trdark">
			<th align="center" nowrap width="5%"></th>
			<th align="center" nowrap width="40%">Instituci&oacute;n Financiera</th>
			<th align="center" nowrap width="10%">Monto M&iacute;nimo</th>
			<th align="center" nowrap width="10%">Monto M&aacute;ximo</th>
			<th align="center" nowrap width="10%">Monto L&iacute;mite</th>
			<th align="center" nowrap width="10%">Monto Diario</th>
			<th align="center" nowrap width="10%">Monto 1ra Trx</th>
			<th align="center" nowrap width="10%">Factor</th>
			<th align="center" nowrap width="10%">Estado</th>

	</tr>
		<%
			listbancos.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (listbancos.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ETE010002Message regtrans = (ETE010002Message) listbancos.getRecord();
		%>
	<tr>
			
			<td nowrap align="center">
				<input type="radio"  name="E01INSFIN" id="radio_key1" value="<%=listbancos.getCurrentRow() %>" <%=chk%>/>
			</td>
			
			<td nowrap align="left">
				<%=regtrans.getE02LIFDSB()%>
			</td>

			<td nowrap align="right">
				<%=regtrans.getE02LIFMIN()  %>
			</td>
			<td nowrap align="right">
				<%=regtrans.getE02LIFMAX()  %>
			</td>
			<td nowrap align="right">
				<%=regtrans.getE02LIFLIM()  %>
			</td>
			<td nowrap align="right">
				<%=regtrans.getE02LIFDAY()  %>
			</td>
			<td nowrap align="right">
				<%=regtrans.getE02LIFPRI()  %>
			</td>
			<td nowrap align="right">
				<%=regtrans.getE02LIFFA1()  %>
			</td>
			
			<td nowrap align="center">
				<%if(regtrans.getE02LIFSTS().equals("A"))
					out.print("ACTIVA");
				  else
				  	if(regtrans.getE02LIFSTS().equals("C"))
						out.print("CERRADA");
					else 
						out.print("");
				%>
			</td>
		    
		    <td>
		    </td>
	</tr>
		<%
			}
		%>
	</table>
</td>
</tr>
<tr>
<td>
<br><br>
<table class="tbenter" width="100%">
<tr id="trclear">
<td>
<hr style="  border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));  "/>
<h3 align="center">RELACI&Oacute;N PRODUCTOS</h3>
</td>
</tr>
</table>

<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg"> 
			<a href="javascript:goAction2('800')"><b>NUEVA</b></a>
		</td>
		<td align="center" class="tdbkg">
			<a href="javascript:goAction2('1000')"> <b>ELIMINAR</b></a>
		</td>
	</tr>
</table>

</td>
</tr>

<TR>
<TD>

	<table id="headTable"  width="100%" align="left">
		<tr id="trdark">
			<th align="center" nowrap width="5%"></th>
			<th align="center" nowrap width="40%">Producto Origen</th>
			<th align="center" nowrap width="40%">Producto / Instituci&oacute;n Financiera Destino</th>
			<th align="center" nowrap width="15%"></th>
		</tr>
		<%
			listproductos.initRow();
				k = 0;
				firstTime = true;
				chk = "";
				while (listproductos.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ETE010003Message regtrans = (ETE010003Message) listproductos.getRecord();
		%>
		<tr>
			
			<td nowrap align="center"><input type="radio" name="E03ASOPRO" id="radio_key2" value="<%=listproductos.getCurrentRow() %>" <%=chk%>/></td>
			<td nowrap align="left">
				<%=regtrans.getE03ICTPRO()%> - <%=regtrans.getE03ICTDRO()%>
			</td>

			<td nowrap align="left">
				<%=regtrans.getE03ICTPRD()%> - <%=regtrans.getE03ICTDRD()%>
			</td>
		    
		    <td>
		    </td>

		</tr>
		<%
			}
		%>
	</table>


</TD>
</TR>

</table>

</form>
</body>
</html>
