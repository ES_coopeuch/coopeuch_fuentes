/**
 * ExcepcionNivel.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public class ExcepcionNivel  implements java.io.Serializable {
    private java.lang.String nivel;
    private java.lang.String exception;
    private java.lang.String stacktrace;

    public ExcepcionNivel() {
    }

    public java.lang.String getNivel() {
        return nivel;
    }

    public void setNivel(java.lang.String nivel) {
        this.nivel = nivel;
    }

    public java.lang.String getException() {
        return exception;
    }

    public void setException(java.lang.String exception) {
        this.exception = exception;
    }

    public java.lang.String getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(java.lang.String stacktrace) {
        this.stacktrace = stacktrace;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        ExcepcionNivel other = (ExcepcionNivel) obj;
        boolean _equals;
        _equals = true
            && ((this.nivel==null && other.getNivel()==null) || 
             (this.nivel!=null &&
              this.nivel.equals(other.getNivel())))
            && ((this.exception==null && other.getException()==null) || 
             (this.exception!=null &&
              this.exception.equals(other.getException())))
            && ((this.stacktrace==null && other.getStacktrace()==null) || 
             (this.stacktrace!=null &&
              this.stacktrace.equals(other.getStacktrace())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ExcepcionNivel history = (ExcepcionNivel) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ExcepcionNivel history = (ExcepcionNivel) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getNivel() != null) {
            _hashCode += getNivel().hashCode();
        }
        if (getException() != null) {
            _hashCode += getException().hashCode();
        }
        if (getStacktrace() != null) {
            _hashCode += getStacktrace().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
