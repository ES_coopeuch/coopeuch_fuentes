package datapro.eibs.plugins;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.datapro.security.HistUserAccessEntry;
import com.datapro.security.UserRegistryEibsImpl;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.master.ServiceLocator;

/**
 * @author fhernandez
 *
 */
public class SessionListener implements HttpSessionListener {
	
	private static final String REGISTRY_KEY = ServiceLocator.getInstance()
		.getDataSourceJndiName(UserRegistryEibsImpl.DB_REGISTRY);

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	public void sessionCreated(HttpSessionEvent event) {		
		HttpSession session = event.getSession();
		
		ESS0030DSMessage msgUser = (datapro.eibs.beans.ESS0030DSMessage) session
			.getAttribute("currUser");
		if (msgUser != null) {
			try {
				UserRegistryEibsImpl userRegistryFacade = new UserRegistryEibsImpl(
						session.getServletContext().getInitParameter("realm"));
				userRegistryFacade.initialize(REGISTRY_KEY);
				HistUserAccessEntry accessEntry = new HistUserAccessEntry();
				accessEntry.setUid(msgUser.getH01USR());
				accessEntry.setPwdHistory(HistUserAccessEntry.ZERO);
				//userRegistryFacade.updateAccessHistory(accessEntry);
			} catch (Exception e) {
				System.out.println("The User Session couldn't be updated on DB : Cause By :" + e.getMessage());
			} 	
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	public void sessionDestroyed(HttpSessionEvent event) {
		HttpSession session = event.getSession();

		ESS0030DSMessage msgUser = (datapro.eibs.beans.ESS0030DSMessage) session
			.getAttribute("currUser");
		if (msgUser != null) {
			try {
				UserRegistryEibsImpl userRegistryFacade = new UserRegistryEibsImpl(
						session.getServletContext().getInitParameter("realm"));
				userRegistryFacade.initialize(REGISTRY_KEY);
				HistUserAccessEntry accessEntry = new HistUserAccessEntry();
				accessEntry.setUid(msgUser.getH01USR());
				accessEntry.setPwdHistory(new Integer("3"));
				//userRegistryFacade.updateAccessHistory(accessEntry);
			} catch (Exception e) {
				System.out.println("The User Session couldn't be updated on DB : Cause By :" + e.getMessage());
			} 				
		}
	}

}
