package com.datapro.eibs.security.struts.bean;

import java.math.BigDecimal;

/**
 * @author ogarcia
 *
 * This bean holds information from the user connected
 * and the scope should be session.
 * 
 */
public class JBUser {

	private String uid = null;
	private String password = null;
	private String dateFormat = null;
	private int bank;
	private int branch;
	private BigDecimal costCenter = null;
	private String gid = null;
	private String name = null;
	private String identification = null;
	private int runDay;
	private int runMonth;
	private int runYear;
	private String status = null;
	
	
	
	/**
	 * Constructor for User.
	 */
	public JBUser() {
		super();
	}

	/**
	 * Returns the bank.
	 * @return int
	 */
	public int getBank() {
		return bank;
	}

	/**
	 * Returns the branch.
	 * @return int
	 */
	public int getBranch() {
		return branch;
	}

	/**
	 * Returns the costCenter.
	 * @return BigDecimal
	 */
	public BigDecimal getCostCenter() {
		return costCenter;
	}

	/**
	 * Returns the dateFormat.
	 * @return String
	 */
	public String getDateFormat() {
		return dateFormat;
	}

	/**
	 * Returns the gid.
	 * @return String
	 */
	public String getGid() {
		return gid;
	}

	/**
	 * Returns the identification.
	 * @return String
	 */
	public String getIdentification() {
		return identification;
	}

	/**
	 * Returns the name.
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the password.
	 * @return String
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Returns the runDay.
	 * @return int
	 */
	public int getRunDay() {
		return runDay;
	}

	/**
	 * Returns the runMonth.
	 * @return int
	 */
	public int getRunMonth() {
		return runMonth;
	}

	/**
	 * Returns the runYear.
	 * @return int
	 */
	public int getRunYear() {
		return runYear;
	}

	/**
	 * Returns the status.
	 * @return String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Returns the uid.
	 * @return String
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * Sets the bank.
	 * @param bank The bank to set
	 */
	public void setBank(int bank) {
		this.bank = bank;
	}

	/**
	 * Sets the branch.
	 * @param branch The branch to set
	 */
	public void setBranch(int branch) {
		this.branch = branch;
	}

	/**
	 * Sets the costCenter.
	 * @param costCenter The costCenter to set
	 */
	public void setCostCenter(BigDecimal costCenter) {
		this.costCenter = costCenter;
	}

	/**
	 * Sets the dateFormat.
	 * @param dateFormat The dateFormat to set
	 */
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	/**
	 * Sets the gid.
	 * @param gid The gid to set
	 */
	public void setGid(String gid) {
		this.gid = gid;
	}

	/**
	 * Sets the identification.
	 * @param identification The identification to set
	 */
	public void setIdentification(String identification) {
		this.identification = identification;
	}

	/**
	 * Sets the name.
	 * @param name The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the password.
	 * @param password The password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Sets the runDay.
	 * @param runDay The runDay to set
	 */
	public void setRunDay(int runDay) {
		this.runDay = runDay;
	}

	/**
	 * Sets the runMonth.
	 * @param runMonth The runMonth to set
	 */
	public void setRunMonth(int runMonth) {
		this.runMonth = runMonth;
	}

	/**
	 * Sets the runYear.
	 * @param runYear The runYear to set
	 */
	public void setRunYear(int runYear) {
		this.runYear = runYear;
	}

	/**
	 * Sets the status.
	 * @param status The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Sets the uid.
	 * @param uid The uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

}
