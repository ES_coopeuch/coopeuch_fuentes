<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
<head>
<title>Tabla de Provision Cuentas Contables</title>
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<link rel="stylesheet" href="../../theme/Master.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="GENERATOR" content="Rational Software Architect">

<jsp:useBean id="EDL0526Record" class="datapro.eibs.beans.EDL052601Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<%
		if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>

</head>
<body>
<H3 align="center">Tabla de Provision Cuentas Contables<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ledger_accounts_provisions_table.jsp, EDL0526"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEDL0526" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="4">
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Tipo  de Credito:</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2"> 
              	<input type="text" name="E01PCCTPC" maxlength="1" size="5" value="<%= EDL0526Record.getE01PCCTPC()%>" >
              	</font></font></font>
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Tipo de Garantia:</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2"> 
              	<input type="text" name="E01PCCTGA" maxlength="4" size="5" value="<%= EDL0526Record.getE01PCCTGA()%>" >
              	</font></font></font>
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Categoria de Riesgo:</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2"> 
              	<input type="text" name="E01PCCCRG" maxlength="2" size="5" value="<%= EDL0526Record.getE01PCCCRG()%>" >
              	</font></font></font>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>  
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="15%" align = "right"> 
              <b>Tipo de Provision</b>
             </td>
             <td nowrap width="5%">
              <input type="text" name="E01PCCTPP" maxlength="1" size="5" value="<%= EDL0526Record.getE01PCCTPP()%>" >
             </td>
            <td nowrap width="25%"> 
              <div align="right"><b>Cuenta de Debito Capital</b></div>
            </td>
            <td nowrap width="55%"> 
              <div align="left" >
                <input type="text"  name="E01PCCCDP" maxlength="16" size="20" value="<%= EDL0526Record.getE01PCCCDP()%>" >
                <a href="javascript:GetLedger('E01PCCCDP','','','')">
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Help" border="0" ></a>
              </div>
            </td>
        </tr>
         <tr id="trclear"> 
           <td nowrap width="15%"> 
           </td>
           <td nowrap width="5%"> 
           </td>
            <td nowrap width="25%"> 
              <div align="right"><b>Cuenta de Credito Capital</b></div>
            </td>
            <td nowrap width="55%"> 
              <div align="left" >
                <input type="text"  name="E01PCCCCP" maxlength="16" size="20" value="<%= EDL0526Record.getE01PCCCCP()%>" >
                <a href="javascript:GetLedger('E01PCCCCP','','','')">
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Help" border="0" ></a>
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
          <td nowrap width="15%"> 
           </td>
           <td nowrap width="5%"> 
           </td>
           	<td nowrap width="25%"> 
              <div align="right"><b>Cuenta de Debito Intereses</b></div>
            </td>
            <td nowrap width="55%"> 
              <div align="left" >
                <input type="text"  name="E01PCCCDI" maxlength="16" size="20" value="<%= EDL0526Record.getE01PCCCDI()%>" >
                <a href="javascript:GetLedger('E01PCCCDI','','','')">
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Help" border="0" ></a>
              </div>
            </td>
         </tr>
          <tr id="trclear"> 
          	<td nowrap width="15%"> 
           </td>
           <td nowrap width="5%"> 
           </td>
            <td nowrap width="25%"> 
              <div align="right"><b>Cuenta de Credito Intereses</b></div>
            </td>
            <td nowrap width="55%"> 
              <div align="left" >
                <input type="text"  name="E01PCCCCI" maxlength="16" size="20" value="<%= EDL0526Record.getE01PCCCCI()%>" >
                <a href="javascript:GetLedger('E01PCCCCI','','','')">
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Help" border="0" ></a>
              </div>
            </td>
            </tr>
        	<tr id="trdark"> 
        	<td nowrap width="15%"> 
           </td>
           <td nowrap width="5%"> 
           </td>
        	 <td nowrap width="25%"> 
              <div align="right"><b>Cuenta de Debito Otros Gastos</b></div>
            </td>
             <td nowrap width="55%"> 
              <div align="left" >
                <input type="text"  name="E01PCCCDO" maxlength="16" size="20" value="<%= EDL0526Record.getE01PCCCDO()%>" >
                <a href="javascript:GetLedger('E01PCCCDO','','','')">
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Help" border="0" ></a>
              </div>
              </td>
              </tr>
             <tr id="trclear">  
             <td nowrap width="15%"> 
           </td>
           <td nowrap width="5%"> 
           </td>
            <td nowrap width="25%"> 
              <div align="right"><b>Cuenta de Credito Otros Gastos</b></div>
            </td>
            <td nowrap width="55%"> 
              <div align="left" >
                <input type="text" name="E01PCCCCO" maxlength="16" size="20" value="<%= EDL0526Record.getE01PCCCCO()%>" >
                <a href="javascript:GetLedger('E01PCCCCO','','','')">
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Help" border="0" ></a>
              </div>
              </td>
              </tr>
  		</table>
  </td>
  </tr>
  </table>
  <br>
  <p>
  <br>
  </p>
  <div align="center"> 
  	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
  </form>
</body>
</html>