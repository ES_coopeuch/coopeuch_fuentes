<html>
<head>
<title>Codigos Especiales</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
</head>

<jsp:useBean id= "codes" class= "datapro.eibs.beans.ESD008003Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

     <% 
   if ( userPO.getOption().equals("CLIENT_P") ) {
   %>
		builtNewMenu(client_ap_personal_opt);
  <%      
   }
   else
   {
   %>
		builtNewMenu(client_ap_corp_opt);
   <%
   }
   %>

</SCRIPT>

<body bgcolor="#FFFFFF">

 
 
 <% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
  if ( !userPO.getPurpose().equals("NEW") ) {
    out.println("<SCRIPT> initMenu(); </SCRIPT>");
 }
%>

<h3 align="center">C&oacute;digos de Clasificaci&oacute;n <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="client_ap_both_codes, ESD0100"></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="32">
  
 <% int row = 0;%>
<table class="tableinfo">
  <tr > 
    <td> 
      <table cellspacing="0" cellpadding="2" width="100%" class="tbhead"  align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap width="10%" align="right"> Cliente: 
               </td>
          <td nowrap width="12%" align="left">
      			<%= userPO.getHeader1()%>
          </td>
            <td nowrap width="6%" align="right">ID:  
            </td>
          <td nowrap width="14%" align="left">
      			<%= userPO.getHeader2()%>
          </td>
            <td nowrap width="8%" align="right"> Nombre: 
               </td>
          <td nowrap width="50%"align="left">
      			<%= userPO.getHeader3()%>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p><center> 
    <table class="tableinfo">
      <tr > 
        <td >
          <table cellspacing="0" cellpadding="2" width="100%" border="0" >
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">Ejecutivo Principal :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03OFC().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03OFC" size="5" maxlength="4" value="<%= codes.getE03OFC().trim()%>">
                <input type="text" readonly name="D03OFC" size="45" maxlength="45" value="<%= codes.getD03OFC().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
               <td width="30%"> 
                <div align="right">Ejecutivo Secundario :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03OF2().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03OF2" size="5" maxlength="4" value="<%= codes.getE03OF2().trim()%>">
                <input type="text" readonly name="D03OF2" size="45" maxlength="45" value="<%= codes.getD03OF2().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">Sector Econ&oacute;mico :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03INC().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03INC" size="5" maxlength="4" value="<%= codes.getE03INC().trim()%>">
                <input type="text" readonly name="D03INC" size="45" maxlength="45" value="<%= codes.getD03INC().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="3%"> 
                <div align="right">Actividad Econ&oacute;mica :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03BUC().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03BUC" size="5" maxlength="4" value="<%= codes.getE03BUC().trim()%>">
                <input type="text" readonly name="D03BUC" size="45" maxlength="45" value="<%= codes.getD03BUC().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%" height="42"> 
                <div align="right">Pa&iacute;s de Residencia 
                  :</div>
              </td>
              <td width="70%" height="42"> 
                <input type="text" readonly <% if (codes.getF03GEC().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03GEC" size="5" maxlength="4" value="<%= codes.getE03GEC().trim()%>">
                <input type="text" readonly name="D03GEC" size="45" maxlength="45" value="<%= codes.getD03GEC().trim()%>">
              </td>
            </tr>

            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="3%"> 
                <div align="right">Canal de Ventas :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03BUC().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03SCH" size="5" maxlength="4" value="<%= codes.getE03SCH().trim()%>">
                <input type="text" readonly name="D03SCH" size="45" maxlength="45" value="<%= codes.getD03SCH().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%" height="42"> 
                <div align="right">Fuentes de Información :</div>
              </td>
              <td width="70%" height="42"> 
                <input type="text" readonly <% if (codes.getF03GEC().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03SST" size="5" maxlength="4" value="<%= codes.getE03SST().trim()%>">
                <input type="text" readonly name="D03SST" size="45" maxlength="45" value="<%= codes.getD03SST().trim()%>">
              </td>
            </tr>

            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">Tipo de Relación :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03UC1().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03UC1" size="5" maxlength="4" value="<%= codes.getE03UC1().trim()%>">
                <input type="text" readonly name="D03UC1" size="45" maxlength="45" value="<%= codes.getD03UC1().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">Clasificaci&oacute;n :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03UC2().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03UC2" size="5" maxlength="4" value="<%= codes.getE03UC2().trim()%>">
                <input type="text" readonly name="D03UC2" size="45" maxlength="45" value="<%= codes.getD03UC2().trim()%>">
              </td>
            </tr>

            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">Sub Clasificaci&oacute;n :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03SCL().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03SCL" size="5" maxlength="4" value="<%= codes.getE03SCL().trim()%>">
                <input type="text" readonly name="D03SCL" size="45" maxlength="45" value="<%= codes.getD03SCL().trim()%>">
              </td>
            </tr>


	        <%  if (currUser.getE01INT().trim().equals("18")) {%>         
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">Nivel Socioeconomico :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03UC3().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03UC3" size="5" maxlength="4" value="<%= codes.getE03UC3().trim()%>">
                <input type="text" readonly name="D03UC3" size="45" maxlength="45" value="<%= codes.getD03UC3().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">Unidad de Negocio :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03UC4().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03UC4" size="6" maxlength="6" value="<%= codes.getE03UC4().trim()%>">
                <input type="text" readonly name="D03UC4" size="45" maxlength="45" value="<%= codes.getD03UC4().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">Segmento :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03UC5().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03UC5" size="5" maxlength="4" value="<%= codes.getE03UC5().trim()%>">
                <input type="text" readonly name="D03UC5" size="45" maxlength="45" value="<%= codes.getD03UC5().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">Subsegmento :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03UC6().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03UC6" size="5" maxlength="4" value="<%= codes.getE03UC6().trim()%>">
                <input type="text" readonly name="D03UC6" size="45" maxlength="45" value="<%= codes.getD03UC6().trim()%>">
              </td>
            </tr>

			<%if(codes.getE03LGT().equals("1")){ %> 
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">Con fin de lucro :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03SCL().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03FLC" size="2" maxlength="2" value="<%if (codes.getE03FLC().equals("Y")) out.print("SI"); else out.print("NO"); %>">
              </td>
            </tr>

            <% }%>

            <% } else {%>

            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">C&oacute;digo de Usuario 3 :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03UC3().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03UC3" size="5" maxlength="4" value="<%= codes.getE03UC3().trim()%>">
                <input type="text" readonly name="D03UC3" size="45" maxlength="45" value="<%= codes.getD03UC3().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">C&oacute;digo de Usuario 4 :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03UC4().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03UC4" size="6" maxlength="6" value="<%= codes.getE03UC4().trim()%>">
                <input type="text" readonly name="D03UC4" size="45" maxlength="45" value="<%= codes.getD03UC4().trim()%>">
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="30%"> 
                <div align="right">C&oacute;digo de Usuario 5 :</div>
              </td>
              <td width="70%"> 
                <input type="text" readonly <% if (codes.getF03UC5().equals("Y")) out.print("id=\"txtchanged\""); %> name="E03UC5" size="5" maxlength="4" value="<%= codes.getE03UC5().trim()%>">
                <input type="text" readonly name="D03UC5" size="45" maxlength="45" value="<%= codes.getD03UC5().trim()%>">
              </td>
            </tr>
          <% }%>  

          </table>
        </td>
      </tr>
    </table>
    
  </center>
</form>
</body>
</html>

