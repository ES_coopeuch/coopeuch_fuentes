package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Creado     por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  06/06/2013                                                                                      **/
/**  Objetivo                :  Servicio para carga de archivo de movimientos para elINPUT                                      **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.EGL601001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.Util;
import datapro.eibs.services.ExcelResultSet;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.services.ExcelXLSXResultSet;

public class JSEGL6010 extends JSEIBSServlet {

	private ServletConfig config = null;
	
	protected static final int A_ENTER_FILE = 1;	
	protected static final int R_ENTER_FILE_PM = 2;
	protected static final int R_ENTER_FILE_PY = 3;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
        flexLog("Entre con Screen.." + screen);   
        
        switch (screen) {
			// Request
			case R_ENTER_FILE_PM :
				procReqImport(user, req, res, session, "PM");
				break;
			case R_ENTER_FILE_PY :
				procReqImport(user, req, res, session, "PY");
				break;
			case A_ENTER_FILE :
				procReadExcelFile(user, req, res, session);
				break;
			default :
				forward(devPage, req, res);
				break;
		}
	}

	private void procReadExcelFile(
			ESS0030DSMessage user, 
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session) 
		throws ServletException, IOException {
		UserPos userPO = getUserPos(session);		
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		String PageToCall = "";
		int rows = 0;
		//read file
		try {
			SmartUpload mySmartUpload = new SmartUpload();
			mySmartUpload.initialize(config, req, res);
			mySmartUpload.upload();
			com.jspsmart.upload.File file =  mySmartUpload.getFiles().getFile(0);
			try {
				userPO.setBank(mySmartUpload.getRequest().getParameter("E01TRABNK").trim());
			} catch (Exception e) {
			}
			try {
				userPO.setBranch(mySmartUpload.getRequest().getParameter("E01TRABRN").trim());
			} catch (Exception e) {
			}
			try {
				userPO.setHeader1(mySmartUpload.getRequest().getParameter("E01TRABTH").trim());
			} catch (Exception e) {
			}
			InputStream xls = Util.getStreamFromObject(file);													
			
			ResultSet rs = null;
			
			if (ExcelUtils.isXLSXVersion(file.getFilePathName())) {
				rs = new ExcelXLSXResultSet(xls, 0);
				((ExcelXLSXResultSet)rs).init();
			} else {
				rs = new ExcelResultSet(xls, 0);
				((ExcelResultSet)rs).init();
			}
			
			mp = getMessageProcessor("EGL6010", req);
			
			boolean error = false;
			while (rs.next()) {
				EGL601001Message msg = (EGL601001Message) mp.getMessageRecord("EGL601001");
	            msg.setUSUARIO(user.getH01USR());             
				ExcelUtils.populate(rs, msg);
				msg.setORGBNK(userPO.getBank().trim());		 	  
  			    msg.setORGBRN(userPO.getBranch().trim());		 	  
				msg.setORGBTH(userPO.getHeader1().trim());
				msg.setPROCESO(userPO.getHeader2().trim());	
				flexLog("Mensaje enviado detalle..." + msg);   				
				mp.sendMessage(msg);
				rows++;
			}
			if (!error) {
				EGL601001Message msg = (EGL601001Message) mp.getMessageRecord("EGL601001");
	            msg.setUSUARIO(user.getH01USR());
				msg.setORGBNK(userPO.getBank().trim());		 	  
  			    msg.setORGBRN(userPO.getBranch().trim());		 	  
				msg.setORGBTH(userPO.getHeader1().trim());		
				msg.setPROCESO(userPO.getHeader2().trim());	
				msg.setFLAGMAS("*"); //Last Element List
				flexLog("Mensaje enviado final..." + msg);  
				mp.sendMessage(msg);
				//	Receive Error Message
			 	msgError = (ELEERRMessage) mp.receiveMessageRecord();
			 	if (mp.hasError(msgError)) {
			 		PageToCall = "EGL6010_transfer_file.jsp";
			 	} else {
					req.setAttribute("rows", String.valueOf(rows));
					PageToCall = "EGL6010_transfer_file_confirm.jsp";
			 	}
			}
			
			flexLog("Putting java beans into the session");
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			forward(PageToCall, req, res);
			
		} catch (Exception e) {
			msgError = new ELEERRMessage();
			msgError.setERRNUM("2");
			msgError.setERNU01("0001");		//4		                
			msgError.setERDS01("Error en Record: " + (rows + 1));//70
			msgError.setERNU02("0002");		//4		                
			msgError.setERDS02(e.getMessage().trim().length() > 70 ? e.getMessage().substring(0, 69) : e.getMessage().trim());//70
			session.setAttribute("error", msgError);						
			forward("EGL6010_transfer_file.jsp", req, res);
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
	private void procReqImport(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session,  
			String proceso)
		throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		userPO.setHeader2(proceso);
		ELEERRMessage msgError = new ELEERRMessage(); 
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		
		if(proceso.equals("PY"))
		   session.setAttribute("PY", "true");
		else 
		   session.setAttribute("PY", "false");
		
		forward("EGL6010_transfer_file.jsp", req, res);
	}

}
