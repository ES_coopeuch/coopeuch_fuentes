/*
 * Created on Apr 8, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package datapro.eibs.products;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELC040001Message;
import datapro.eibs.beans.ELC040002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageContextHandler;
import datapro.eibs.sockets.MessageRecord;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 * StandBy Letter Of Credit Maintenance
 */
public class JSELC0410 extends SuperServlet {

	String LangPath = "s/";
	
	private static final int R_OPENING_MAINT = 3;
	private static final int A_OPENING_MAINT = 4;
	private static final int R_ENTER_MAINT = 200;
	
	public JSELC0410() {
		super();
	}
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = (HttpSession) req.getSession(false);
		if (session == null) {
			try {
				res.setContentType("text/html");
				super.printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {
			int screen = -1;

			ESS0030DSMessage  user = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");
			// Here we should get the path from the user profile
			LangPath = rootPath + user.getE01LAN() + "/";
			
			Socket s = null;
			try {
				s = new Socket(hostIP, getInitSocket(req) + 1);
				s.setSoTimeout(sckTimeOut);
				MessageContext  mc =
					new MessageContext(
						new DataInputStream(new BufferedInputStream(s.getInputStream())),
						new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
						"datapro.eibs.beans");
						
				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
					flexLog("Screen  Number: " + screen);
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}
				
				String PageToCall = "";

				switch (screen) {
					case R_ENTER_MAINT :
						procReqEnterMaint(req, res, screen);
						break;
					case R_OPENING_MAINT : // OPENING MAINTENANCE
						procReqOpeningMaint(mc, user, req, res, screen);
						break;
					case A_OPENING_MAINT :
						procActionOpeningMaint(mc, user, req, res, screen);
						break;
					default :
						PageToCall = "MISC_not_available.jsp";
						callPage(PageToCall, req, res);
						break;
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				res.sendRedirect(srctx + LangPath + sckNotRespondPage);
			} finally {
				if (s != null) s.close();
				flexLog("Socket used by JSELC0410 closed.");
			}
		}	
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void procActionOpeningMaint(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos  userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		String PageToCall = "";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ELC040001Message msg = (ELC040001Message) session.getAttribute("msg");
			msg = (ELC040001Message) msgHandle.initMessage(msg, user.getH01USR(), "0005");
			initTransaction(userPO, String.valueOf(screen), "MAINTENANCE");
			msg.setH01SCRCOD("01");
			msgHandle.setFieldsFromPage(req, msg);
			msgHandle.sendMessage(msg);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			boolean isNotError = msgError.getERRNUM().equals("0");
			msg = (ELC040001Message) msgHandle.receiveMessage();
			userPO.setIdentifier(msg.getE01LCMACC());
			userPO.setHeader11(msg.getE01LCMORF());
			userPO.setHeader12(msg.getE01LCMTRF());
			userPO.setHeader13(msg.getE01LCMPRO());
			userPO.setHeader14(msg.getE01DSCPRO());
			putDataInSession(session, userPO, msgError, msg);
			if (isNotError) {
				procReqDetailsMaint(mc, user, req, res, screen);
			} else {
				PageToCall = "ELC0400_sb_basic_info.jsp";
				callPage(PageToCall, req, res);
			}		
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void procReqDetailsMaint(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos  userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		String PageToCall = "";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ELC040002Message msg = (ELC040002Message) msgHandle.initMessage("ELC040002", user.getH01USR(), "0002");
			initTransaction(userPO, String.valueOf(screen), "MAINTENANCE");
			msg.setH02SCRCOD("01");
			msg.setE02LCMCUN("E01LCMCUN");
			try {
				msg.setE02LCMACC(req.getParameter("E02LCMACC").toUpperCase());
			} catch (Exception e) {
				msg.setE02LCMACC(userPO.getIdentifier());
			}
			userPO.setIdentifier(msg.getE02LCMACC());
			msgHandle.sendMessage(msg);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			boolean isNotError = msgError.getERRNUM().equals("0");
			msg = (ELC040002Message) msgHandle.receiveMessage();
			userPO.setHeader1(msg.getE02LCMPRO());
			userPO.setIdentifier(msg.getE02LCMACC());
			userPO.setBank(msg.getE02LCMBNK());
			userPO.setHeader2(msg.getE02DSCPRO());
			userPO.setHeader3(msg.getE02LCMTRF());
			userPO.setAccNum(msg.getE02LCMACC());
			putDataInSession(session, userPO, msgError, msg);
			if (isNotError) {
				PageToCall = "ELC0400_sb_details.jsp";
				callPage(PageToCall, req, res);
			} else {
				PageToCall = "ELC0400_sb_enter_maint.jsp";
				callPage(PageToCall, req, res);
			}	
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void procReqOpeningMaint(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos  userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		String PageToCall = "";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ELC040001Message msg = (ELC040001Message) msgHandle.initMessage("ELC040001", user.getH01USR(), "0002");
			initTransaction(userPO, String.valueOf(screen), "MAINTENANCE");
			msg.setH01SCRCOD("01");
			msg.setE01LCMCUN("E01LCMCUN");
			try {
				msg.setE01LCMACC(req.getParameter("E01LCMACC").toUpperCase());
			} catch (Exception e) {
				msg.setE01LCMACC(userPO.getIdentifier());
			}
			try {
				msg.setE01LCMSWF(req.getParameter("E01LCMSWF").toUpperCase());
			} catch (Exception e) {
			}
			try {
				msg.setE01LCMAMF(req.getParameter("AMENDMENT").toUpperCase());
			} catch (Exception e) {
			}
			try {
				msg.setH01FLGMAS(req.getParameter("H01FLGMAS"));
			} catch (Exception e) {
			}
			userPO.setIdentifier(msg.getE01LCMACC());
			msgHandle.sendMessage(msg);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			boolean isNotError = msgError.getERRNUM().equals("0");
			msg = (ELC040001Message) msgHandle.receiveMessage();
			userPO.setHeader1(msg.getE01LCMPRO());
			userPO.setIdentifier(msg.getE01LCMACC());
			userPO.setBank(msg.getE01LCMBNK());
			userPO.setHeader2(msg.getE01DSCPRO());
			userPO.setHeader3(msg.getE01LCMTRF());
			userPO.setAccNum(msg.getE01LCMACC());
			putDataInSession(session, userPO, msgError, msg);
			if (isNotError) {
				PageToCall = "ELC0400_sb_basic_info.jsp";
				callPage(PageToCall, req, res);
			} else {
				PageToCall = "ELC0400_sb_enter_maint.jsp";
				callPage(PageToCall, req, res);
			}	
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 */
	private void procReqEnterMaint(HttpServletRequest req, HttpServletResponse res, int screen) {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos  userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		String PageToCall = "ELC0400_sb_enter_maint.jsp";
		initTransaction(userPO, String.valueOf(screen), "MAINTENANCE");
		userPO.setHeader18("NO_MENU");
		userPO.setProdCode("CC");
		putDataInSession(session, userPO, null, new ELC040001Message());
		callPage(PageToCall, req, res);
	}
	
	private void initTransaction(UserPos  userPO, String optMenu, String purpose) {
		userPO.setOption(optMenu);
		userPO.setPurpose(purpose);
	}
	
	public void callPage(String page, HttpServletRequest req, HttpServletResponse res) {
		try {
			super.callPage(LangPath + page, req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e.toString() + e.getMessage());
		}
		return; 
	}
	
	private void putDataInSession(HttpSession session, UserPos  userPO, ELEERRMessage msgError, MessageRecord msg) {
		try {
			flexLog("Putting java beans into the session");

			if (msgError == null) {
				msgError = new ELEERRMessage(); 
			}
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			if (msg != null) session.setAttribute("msg", msg);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error at putBeansInSession(): " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

}
