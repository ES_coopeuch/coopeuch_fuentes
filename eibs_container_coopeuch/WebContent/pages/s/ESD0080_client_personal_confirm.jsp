<html>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">

<head>
<title>Confirmaci�n de la creaci�n de un Cliente Juridico </title>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008001Message"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT LANGUAGE="javascript">
 function finish(){
 	
 		
 		if (document.getElementById("E01SFR").value == 'A') {
 			if(<%=session.getAttribute("BanderaUsu")%>  == "1"){
 				<% String res = request.getParameter("NuevoSI"); %>
 					self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4030?SCREEN=500";
 					document.forms[0].submit();
 				
 			}
 		}
 		else{
 			if(<%=session.getAttribute("BanderaUsu")%>  == "1"){
 				
 				<% res = request.getParameter("NuevoSI"); %>
 				if(<%=res %>  == "1"){
 					self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4030?SCREEN=500";
 				}else{
 					document.forms[0].submit();
 				}
 			}
 			else 
 			     self.window.location.href = "<%=request.getContextPath()%>/pages/background.jsp";
 		}	
  }
  
 setTimeout("finish();", 5000);
 

 
</SCRIPT>

</head>

<body>

 <h3 align="center">Confirmaci�n  <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="client_personal_confirm, ESD0080" ></h3>
 <hr size="4">
 <FORM METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080?SCREEN=1" >
  <INPUT TYPE=HIDDEN NAME="E01SFR" VALUE="<%= client.getE01SFR().trim()%>">
  <table width="100%" height="100%" border="1" bordercolor="#000000">
    <tr > 
      <td> 
        <table width="100%" height="100%">
          <tr> 
            <td align=center>
		       	<% if (client.getE01SFR().equals("A")) {%>
			  		El cliente <b><%= userPO.getHeader3()%></b> se ha aprobado con el n�mero  <b><%= userPO.getCusNum()%></b> dentro del sistema.
				<%} else { %>
			  		El cliente <b><%= userPO.getHeader3()%></b> se ha creado/modificado satisfactoriamente y se le ha asignado el n�mero  <b><%= userPO.getCusNum()%></b> dentro del sistema.
				<% } %>

            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
 </FORM>

</body>
</html>
