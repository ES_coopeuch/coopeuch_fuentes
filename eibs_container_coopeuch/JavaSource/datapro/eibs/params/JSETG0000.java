package datapro.eibs.params;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD038001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ETG000000Message;
import datapro.eibs.beans.ETG000001Message;
import datapro.eibs.beans.ETG000002Message;
import datapro.eibs.beans.ETG000003Message;
import datapro.eibs.beans.ETG000004Message;
import datapro.eibs.beans.ETG000005Message;
import datapro.eibs.beans.ETG000006Message;
import datapro.eibs.beans.ETG000007Message;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.sockets.MessageRecord;

public class JSETG0000 extends JSEIBSServlet {

	protected static final int R_CODES_LIST		= 100;
	protected static final int A_MAINTENANCE 	= 600;
	protected static final int A_POSITION 		= 800;
	
	protected static final int R_CODES_ELE_LIST		= 1100;
	protected static final int A_POSITION_ELE 		= 1800;
	protected static final int A_MAINTENANCE_ELE 	= 1600;
	
	
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_CODES_LIST :
				procReqCodesList(user, req, res, session);
				break;
				// Action
			case A_POSITION :
				procActionPos(user, req, res, session);
				break;
			case A_MAINTENANCE :
				procActionMaintenance(user, req, res, session);
				break;
			case R_CODES_ELE_LIST :
				procReqCodesEleList(user, req, res, session);
				break;
			case A_POSITION_ELE :
				procActionPosEle(user, req, res, session);
				break;
			case A_MAINTENANCE_ELE :
				procActionMaintenanceEle(user, req, res, session);
				break;

			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

	private void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("ETG0000", req);
			ETG000000Message msgRT = (ETG000000Message) session.getAttribute("refCodes");
			msgRT.setH00USERID(user.getH01USR());
			msgRT.setH00PROGRM("ETG0000");
			msgRT.setH00TIMSYS(getTimeStamp());
			msgRT.setH00OPECOD("0002");
			if(userPO.getPurpose().equals("NEW"))
					msgRT.setH00FLGWK2("1");
			
			setMessageRecord(req, msgRT);
			
			mp.sendMessage(msgRT);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRT = (ETG000000Message) mp.receiveMessageRecord("ETG000000");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("refCodes", msgRT);
				session.setAttribute("userPO", userPO);
				
				forward("ETG0000_cntin_table_details.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("refCodes", msgRT);
				session.setAttribute("userPO", userPO);
				
				procReqCodesList(user, req, res, session);
			}
		} 

		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

	private void procActionPos(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNew(user, req, res, session);
				break;
			case 3 : //Deletion
				userPO.setPurpose("DELETE");
				session.setAttribute("userPO", userPO);
				procActionDelete(user, req, res, session);
				break;
			default : //Maintenance
				userPO.setPurpose("MAINTENANCE");
				session.setAttribute("userPO", userPO);
				procReqMaintenance(user, req, res, session);
				break;
		}
	}

	private void procReqMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		
		JBObjList bl = (JBObjList) session.getAttribute("ETG000000Help");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		bl.setCurrentRow(idx);
		ETG000000Message msgDoc = (ETG000000Message) bl.getRecord();

		try {
			mp = getMessageProcessor("ETG0000", req);
			msgDoc.setH00USERID(user.getH01USR());
			msgDoc.setH00PROGRM("ETG0000");
			msgDoc.setH00TIMSYS(getTimeStamp());
			msgDoc.setH00OPECOD("0004");
		
			mp.sendMessage(msgDoc);
		
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgDoc = (ETG000000Message) mp.receiveMessageRecord("ETG000000");

			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("refCodes", msgDoc);
				session.setAttribute("userPO", userPO);
			
				procReqCodesList(user, req, res, session);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("refCodes", msgDoc);
				session.setAttribute("userPO", userPO);
			
				int pos = bl.getFirstRec() - 1;
				forward("ETG0000_cntin_table_details.jsp?FromRecord=" +  pos, req, res);
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

	private void procActionDelete(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
	
		JBObjList bl = (JBObjList) session.getAttribute("ETG000000Help");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		bl.setCurrentRow(idx);
		ETG000000Message msgDoc = (ETG000000Message) bl.getRecord();

		try {
			mp = getMessageProcessor("ETG0000", req);
			msgDoc.setH00USERID(user.getH01USR());
			msgDoc.setH00PROGRM("ETG0000");
			msgDoc.setH00TIMSYS(getTimeStamp());
			msgDoc.setH00OPECOD("0005");
		
			mp.sendMessage(msgDoc);
		
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgDoc = (ETG000000Message) mp.receiveMessageRecord("ETG000000");

			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("refCodes", msgDoc);
				session.setAttribute("userPO", userPO);
			
				procReqCodesList(user, req, res, session);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("refCodes", msgDoc);
				session.setAttribute("userPO", userPO);
			
				procReqCodesList(user, req, res, session);
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

	private void procReqNew(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		flexLog("Putting java beans into the session");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("refCodes", new ETG000000Message());
		
		forward("ETG0000_cntin_table_details.jsp", req, res);
	}

	private void procReqCodesList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		BigDecimal pos;
		try {
			pos = new BigDecimal(req.getParameter("FromRecord"));
		} catch (Exception e) {
			pos = new BigDecimal("0");
		}
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("ETG0000", req);
			ETG000000Message msgList = (ETG000000Message) mp.getMessageRecord("ETG000000", user.getH01USR(), "0001");
			msgList.setETG00NREC(pos);
			msgList.setETG00NPAG("30");
						
			mp.sendMessage(msgList);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList list = mp.receiveMessageRecordList("H00FLGMAS", "ETG00NREC");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);

				forward("error_viewer.jsp", req, res);
			} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("ETG000000Help", list);
				session.setAttribute("userPO", userPO);
				
				forward("ETG0000_cntin_table_list.jsp", req, res);
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}


	//
	private void procReqCodesEleList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		BigDecimal pos;
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		ETG000000Message msgDoc = null;
	
		try 
		{
		
			//Rescato tabla seleccionada
			try {
				JBObjList bl = (JBObjList) session.getAttribute("ETG000000Help");
				int idx = 0;
				idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
				bl.setCurrentRow(idx);
				msgDoc = (ETG000000Message) bl.getRecord();

			} catch (Exception e) {
				msgDoc = (ETG000000Message) session.getAttribute("refCodes");
			}

			//Lectura de descripción de Cabeceras.	

			mp = getMessageProcessor("ETG0000", req);
			msgDoc.setH00USERID(user.getH01USR());
			msgDoc.setH00PROGRM("ETG0000");
			msgDoc.setH00TIMSYS(getTimeStamp());
			msgDoc.setH00OPECOD("0004");
	
			mp.sendMessage(msgDoc);
	
			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgDoc = (ETG000000Message) mp.receiveMessageRecord("ETG000000");

			if (!mp.hasError(msgError)) 
			{

				//Lectura de Lista de Elementos.
				try { pos = new BigDecimal(req.getParameter("FromRecord"));} 
				catch (Exception e) {pos = new BigDecimal("0");}
		
				try {
					mp = getMessageProcessor("ETG0000", req);
					ETG000000Message msgList = (ETG000000Message) mp.getMessageRecord("ETG000000", user.getH01USR(), "0003");
					msgList.setETG00NREC(pos);
					msgList.setETG00NPAG("30");
					msgList.setETG00TAB(new BigDecimal(msgDoc.getETG00ELE()));
					msgList.setETG00FMTO(msgDoc.getETG00FMTO());
					msgList.setETG00TCOD(msgDoc.getETG00TCOD());
		
					mp.sendMessage(msgList);
			
					msgError = (ELEERRMessage) mp.receiveMessageRecord();
			
					JBObjList list = null;
			
					if(msgDoc.getETG00FMTO().equals("1"))
						list = mp.receiveMessageRecordList("H01FLGMAS", "ETG01NREC");
					if(msgDoc.getETG00FMTO().equals("2"))
						list = mp.receiveMessageRecordList("H02FLGMAS", "ETG02NREC");
					if(msgDoc.getETG00FMTO().equals("3"))
						list = mp.receiveMessageRecordList("H03FLGMAS", "ETG03NREC");
					if(msgDoc.getETG00FMTO().equals("4"))
						list = mp.receiveMessageRecordList("H04FLGMAS", "ETG04NREC");
					if(msgDoc.getETG00FMTO().equals("5"))
						list = mp.receiveMessageRecordList("H05FLGMAS", "ETG05NREC");
					if(msgDoc.getETG00FMTO().equals("6"))
						list = mp.receiveMessageRecordList("H06FLGMAS", "ETG06NREC");
					if(msgDoc.getETG00FMTO().equals("7"))
						list = mp.receiveMessageRecordList("H07FLGMAS", "ETG07NREC");

			
					if (mp.hasError(msgError)) {
						flexLog("Putting java beans into the session");
						session.setAttribute("error", msgError);
						session.setAttribute("refCodes", msgDoc);
						session.setAttribute("userPO", userPO);

						forward("error_viewer.jsp", req, res);
					} else {
						flexLog("Putting java beans into the session");
						session.setAttribute("ETG000000HelpEle", list);
						session.setAttribute("refCodes", msgDoc);
						session.setAttribute("userPO", userPO);
				
						forward("ETG0000_cntin_ele_table_list.jsp", req, res);
					}
				}
				catch (Exception e) {
					throw new ServletException(e);
				}
			}	
			else
			{
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("refCodes", msgDoc);
				session.setAttribute("userPO", userPO);
					forward("error_viewer.jsp", req, res);
			}	
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}
//	

	
	private void procActionPosEle(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNewEle(user, req, res, session);
				break;
			case 3 : //Deletion
				userPO.setPurpose("DELETE");
				session.setAttribute("userPO", userPO);
				procActionDeleteEle(user, req, res, session);
				break;
			default : //Maintenance
				userPO.setPurpose("MAINTENANCE");
				session.setAttribute("userPO", userPO);
				procReqMaintenanceEle(user, req, res, session);
				break;
		}
	}

//
	private void procReqMaintenanceEle(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		ETG000001Message msgF1 = null;
		ETG000002Message msgF2 = null;
		ETG000003Message msgF3 = null;
		ETG000004Message msgF4 = null;
		ETG000005Message msgF5 = null;
		ETG000006Message msgF6 = null;
		ETG000007Message msgF7 = null;
		
		ETG000000Message msgRT =  (ETG000000Message) session.getAttribute("refCodes");

		setMessageRecord(req, msgRT);
		
		JBObjList bl = (JBObjList) session.getAttribute("ETG000000HelpEle");
		int idx = 0;
		
		try {idx = Integer.parseInt(req.getParameter("CURRCODE2").trim());} 
		catch (Exception e) {throw new ServletException(e);}
		
		bl.setCurrentRow(idx);
		
		if(msgRT.getETG00FMTO().equals("1"))
			msgF1 = (ETG000001Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("2"))
			msgF2 = (ETG000002Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("3"))
			msgF3 = (ETG000003Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("4"))
			msgF4 = (ETG000004Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("5"))
			msgF5 = (ETG000005Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("6"))
			msgF6 = (ETG000006Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("7"))
			msgF7 = (ETG000007Message) bl.getRecord();

		session.setAttribute("refCodes", msgRT);
		session.setAttribute("msgF1", msgF1);
		session.setAttribute("msgF2", msgF2);
		session.setAttribute("msgF3", msgF3);
		session.setAttribute("msgF4", msgF4);
		session.setAttribute("msgF5", msgF5);
		session.setAttribute("msgF6", msgF6);
		session.setAttribute("msgF7", msgF7);
		session.setAttribute("userPO", userPO);
		
		forward("ETG0000_cntin_ele_table_details.jsp", req, res);

	}

//
	private void procActionMaintenanceEle(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ETG000001Message msgF1 = (ETG000001Message) session.getAttribute("msgF1");
		ETG000002Message msgF2 = (ETG000002Message) session.getAttribute("msgF2");
		ETG000003Message msgF3 = (ETG000003Message) session.getAttribute("msgF3");
		ETG000004Message msgF4 = (ETG000004Message) session.getAttribute("msgF4");
		ETG000005Message msgF5 = (ETG000005Message) session.getAttribute("msgF5");
		ETG000006Message msgF6 = (ETG000006Message) session.getAttribute("msgF6");
		ETG000007Message msgF7 = (ETG000007Message) session.getAttribute("msgF7");
		ELEERRMessage msgError = null;
			
		try {
			
			ETG000000Message msg =  (ETG000000Message) session.getAttribute("refCodes");
			setMessageRecord(req, msg); 

			mp = getMessageProcessor("ETG0000", req);
			
			if(msg.getETG00FMTO().trim().equals("1"))
			{
				msgF1.setH01USERID(user.getH01USR());
				msgF1.setH01PROGRM("ETG0000");
				msgF1.setH01TIMSYS(getTimeStamp());
				msgF1.setH01OPECOD("0002");
				setMessageRecord(req, msgF1);
				msgF1.setETG01TAB(new BigDecimal(msg.getETG00ELE()));
				msgF1.setH01FLGWK3((msg.getETG00TCOD()));
				
				if(userPO.getPurpose().equals("NEW"))
					msgF1.setH01FLGWK2("1");
				
				mp.sendMessage(msgF1);

				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msgF1 = (ETG000001Message) mp.receiveMessageRecord("ETG000001");
		
			}
			
			if(msg.getETG00FMTO().trim().equals("2"))
			{
				msgF2.setH02USERID(user.getH01USR());
				msgF2.setH02PROGRM("ETG0000");
				msgF2.setH02TIMSYS(getTimeStamp());
				msgF2.setH02OPECOD("0002");
				setMessageRecord(req, msgF2);
				msgF2.setETG02TAB(new BigDecimal(msg.getETG00ELE()));
				msgF2.setH02FLGWK3((msg.getETG00TCOD()));

				if(userPO.getPurpose().equals("NEW"))
					msgF2.setH02FLGWK2("1");

				mp.sendMessage(msgF2);
				
				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msgF2 = (ETG000002Message) mp.receiveMessageRecord("ETG000002");
		
			}
			if(msg.getETG00FMTO().trim().equals("3"))
			{
				msgF3.setH03USERID(user.getH01USR());
				msgF3.setH03PROGRM("ETG0000");
				msgF3.setH03TIMSYS(getTimeStamp());
				msgF3.setH03OPECOD("0002");
				setMessageRecord(req, msgF3);
				msgF3.setETG03TAB(new BigDecimal(msg.getETG00ELE()));
				msgF3.setH03FLGWK3((msg.getETG00TCOD()));

				if(userPO.getPurpose().equals("NEW"))
					msgF3.setH03FLGWK2("1");
		
				mp.sendMessage(msgF3);

				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msgF3 = (ETG000003Message) mp.receiveMessageRecord("ETG000003");

			}
			if(msg.getETG00FMTO().trim().equals("4"))
			{
				msgF4.setH04USERID(user.getH01USR());
				msgF4.setH04PROGRM("ETG0000");
				msgF4.setH04TIMSYS(getTimeStamp());
				msgF4.setH04OPECOD("0002");
				setMessageRecord(req, msgF4);
				msgF4.setETG04TAB(new BigDecimal(msg.getETG00ELE()));
				msgF4.setH04FLGWK3((msg.getETG00TCOD()));

				if(userPO.getPurpose().equals("NEW"))
					msgF4.setH04FLGWK2("1");
				
				mp.sendMessage(msgF4);
		
				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msgF4 = (ETG000004Message) mp.receiveMessageRecord("ETG000004");
			}
			if(msg.getETG00FMTO().trim().equals("5"))
			{
				msgF5.setH05USERID(user.getH01USR());
				msgF5.setH05PROGRM("ETG0000");
				msgF5.setH05TIMSYS(getTimeStamp());
				msgF5.setH05OPECOD("0002");
				setMessageRecord(req, msgF5);
				msgF5.setETG05TAB(new BigDecimal(msg.getETG00ELE()));
				msgF5.setH05FLGWK3((msg.getETG00TCOD()));

				if(userPO.getPurpose().equals("NEW"))
					msgF5.setH05FLGWK2("1");

				mp.sendMessage(msgF5);
				
				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msgF5 = (ETG000005Message) mp.receiveMessageRecord("ETG000005");

			}
			if(msg.getETG00FMTO().trim().equals("6"))
			{
				msgF6.setH06USERID(user.getH01USR());
				msgF6.setH06PROGRM("ETG0000");
				msgF6.setH06TIMSYS(getTimeStamp());
				msgF6.setH06OPECOD("0002");
				setMessageRecord(req, msgF6);
				msgF6.setETG06TAB(new BigDecimal(msg.getETG00ELE()));
				msgF6.setH06FLGWK3((msg.getETG00TCOD()));

				if(userPO.getPurpose().equals("NEW"))
					msgF6.setH06FLGWK2("1");
				
				mp.sendMessage(msgF6);

				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msgF6 = (ETG000006Message) mp.receiveMessageRecord("ETG000006");
			}
			if(msg.getETG00FMTO().trim().equals("7"))
			{
				msgF7.setH07USERID(user.getH01USR());
				msgF7.setH07PROGRM("ETG0000");
				msgF7.setH07TIMSYS(getTimeStamp());
				msgF7.setH07OPECOD("0002");
				setMessageRecord(req, msgF7);
				msgF7.setETG07TAB(new BigDecimal(msg.getETG00ELE()));
				msgF7.setH07FLGWK3((msg.getETG00TCOD()));

				if(userPO.getPurpose().equals("NEW"))
					msgF7.setH07FLGWK2("1");
				
				mp.sendMessage(msgF7);

				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msgF7 = (ETG000007Message) mp.receiveMessageRecord("ETG000007");
			}

			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("refCodes", msg);
				session.setAttribute("msgF1", msgF1);
				session.setAttribute("msgF2", msgF2);
				session.setAttribute("msgF3", msgF3);
				session.setAttribute("msgF4", msgF4);
				session.setAttribute("msgF5", msgF5);
				session.setAttribute("msgF6", msgF6);
				session.setAttribute("msgF7", msgF7);
				session.setAttribute("userPO", userPO);
				
				forward("ETG0000_cntin_ele_table_details.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("refCodes", msg);
				session.setAttribute("userPO", userPO);
				
				procReqCodesEleList(user, req, res, session);
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}
//
	
	private void procReqNewEle(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
	
		ETG000001Message msgF1 = null;
		ETG000002Message msgF2 = null;
		ETG000003Message msgF3 = null;
		ETG000004Message msgF4 = null;
		ETG000005Message msgF5 = null;
		ETG000006Message msgF6 = null;
		ETG000007Message msgF7 = null;
		
		session.setAttribute("msgF1", msgF1);
		session.setAttribute("msgF2", msgF2);
		session.setAttribute("msgF3", msgF3);
		session.setAttribute("msgF4", msgF4);
		session.setAttribute("msgF5", msgF5);
		session.setAttribute("msgF6", msgF6);
		session.setAttribute("msgF7", msgF7);
		
		ETG000000Message msgRT =  (ETG000000Message) session.getAttribute("refCodes");
		setMessageRecord(req, msgRT);

		flexLog("Putting java beans into the session");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("refCodes", msgRT);
		
		forward("ETG0000_cntin_ele_table_details.jsp", req, res);
	}
//
	
	private void procActionDeleteEle(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
	
		try
		{
		ETG000001Message msgF1 = (ETG000001Message) session.getAttribute("msgF1");
		ETG000002Message msgF2 = (ETG000002Message) session.getAttribute("msgF2");
		ETG000003Message msgF3 = (ETG000003Message) session.getAttribute("msgF3");
		ETG000004Message msgF4 = (ETG000004Message) session.getAttribute("msgF4");
		ETG000005Message msgF5 = (ETG000005Message) session.getAttribute("msgF5");
		ETG000006Message msgF6 = (ETG000006Message) session.getAttribute("msgF6");
		ETG000007Message msgF7 = (ETG000007Message) session.getAttribute("msgF7");
		ELEERRMessage msgError = null;
			
		
		ETG000000Message msgRT =  (ETG000000Message) session.getAttribute("refCodes");
		setMessageRecord(req, msgRT);
		
		JBObjList bl = (JBObjList) session.getAttribute("ETG000000HelpEle");
		int idx = 0;
		
		try {idx = Integer.parseInt(req.getParameter("CURRCODE2").trim());} 
		catch (Exception e) {throw new ServletException(e);}
		
		bl.setCurrentRow(idx);
		
		if(msgRT.getETG00FMTO().equals("1"))
			msgF1 = (ETG000001Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("2"))
			msgF2 = (ETG000002Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("3"))
			msgF3 = (ETG000003Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("4"))
			msgF4 = (ETG000004Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("5"))
			msgF5 = (ETG000005Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("6"))
			msgF6 = (ETG000006Message) bl.getRecord();
		
		if(msgRT.getETG00FMTO().equals("7"))
			msgF7 = (ETG000007Message) bl.getRecord();

		mp = getMessageProcessor("ETG0000", req);
		
		if(msgRT.getETG00FMTO().trim().equals("1"))
		{
			msgF1.setH01USERID(user.getH01USR());
			msgF1.setH01PROGRM("ETG0000");
			msgF1.setH01TIMSYS(getTimeStamp());
			msgF1.setH01OPECOD("0005");
			msgF1.setETG01TAB(new BigDecimal(msgRT.getETG00ELE()));
			msgF1.setH01FLGWK3((msgRT.getETG00TCOD()));
			
			mp.sendMessage(msgF1);

			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgF1 = (ETG000001Message) mp.receiveMessageRecord("ETG000001");
	
		}
		
		if(msgRT.getETG00FMTO().trim().equals("2"))
		{
			msgF2.setH02USERID(user.getH01USR());
			msgF2.setH02PROGRM("ETG0000");
			msgF2.setH02TIMSYS(getTimeStamp());
			msgF2.setH02OPECOD("0005");
			msgF2.setETG02TAB(new BigDecimal(msgRT.getETG00ELE()));
			msgF2.setH02FLGWK3((msgRT.getETG00TCOD()));
			
			mp.sendMessage(msgF2);
			
			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgF2 = (ETG000002Message) mp.receiveMessageRecord("ETG000002");
		}

		if(msgRT.getETG00FMTO().trim().equals("3"))
		{
			msgF3.setH03USERID(user.getH01USR());
			msgF3.setH03PROGRM("ETG0000");
			msgF3.setH03TIMSYS(getTimeStamp());
			msgF3.setH03OPECOD("0005");
			msgF3.setETG03TAB(new BigDecimal(msgRT.getETG00ELE()));
			msgF3.setH03FLGWK3((msgRT.getETG00TCOD()));
	
			mp.sendMessage(msgF3);

			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgF3 = (ETG000003Message) mp.receiveMessageRecord("ETG000003");

		}
		if(msgRT.getETG00FMTO().trim().equals("4"))
		{
			msgF4.setH04USERID(user.getH01USR());
			msgF4.setH04PROGRM("ETG0000");
			msgF4.setH04TIMSYS(getTimeStamp());
			msgF4.setH04OPECOD("0005");
			msgF4.setETG04TAB(new BigDecimal(msgRT.getETG00ELE()));
			msgF4.setH04FLGWK3((msgRT.getETG00TCOD()));
			
			mp.sendMessage(msgF4);
	
			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgF4 = (ETG000004Message) mp.receiveMessageRecord("ETG000004");
		}
		if(msgRT.getETG00FMTO().trim().equals("5"))
		{
			msgF5.setH05USERID(user.getH01USR());
			msgF5.setH05PROGRM("ETG0000");
			msgF5.setH05TIMSYS(getTimeStamp());
			msgF5.setH05OPECOD("0005");
			setMessageRecord(req, msgF5);
			msgF5.setETG05TAB(new BigDecimal(msgRT.getETG00ELE()));
			msgF5.setH05FLGWK3((msgRT.getETG00TCOD()));

			mp.sendMessage(msgF5);
			
			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgF5 = (ETG000005Message) mp.receiveMessageRecord("ETG000005");

		}
		if(msgRT.getETG00FMTO().trim().equals("6"))
		{
			msgF6.setH06USERID(user.getH01USR());
			msgF6.setH06PROGRM("ETG0000");
			msgF6.setH06TIMSYS(getTimeStamp());
			msgF6.setH06OPECOD("0005");
			msgF6.setETG06TAB(new BigDecimal(msgRT.getETG00ELE()));
			msgF6.setH06FLGWK3((msgRT.getETG00TCOD()));
			
			mp.sendMessage(msgF6);

			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgF6 = (ETG000006Message) mp.receiveMessageRecord("ETG000006");
		}
		if(msgRT.getETG00FMTO().trim().equals("7"))
		{
			msgF7.setH07USERID(user.getH01USR());
			msgF7.setH07PROGRM("ETG0000");
			msgF7.setH07TIMSYS(getTimeStamp());
			msgF7.setH07OPECOD("0005");
			msgF7.setETG07TAB(new BigDecimal(msgRT.getETG00ELE()));
			msgF7.setH07FLGWK3((msgRT.getETG00TCOD()));
			
			mp.sendMessage(msgF7);

			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgF7 = (ETG000007Message) mp.receiveMessageRecord("ETG000007");
		}
		
		
		
		session.setAttribute("refCodes", msgRT);
		session.setAttribute("msgF1", msgF1);
		session.setAttribute("msgF2", msgF2);
		session.setAttribute("msgF3", msgF3);
		session.setAttribute("msgF4", msgF4);
		session.setAttribute("msgF5", msgF5);
		session.setAttribute("msgF6", msgF6);
		session.setAttribute("msgF7", msgF7);

		
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqCodesEleList(user, req, res, session);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				int pos = bl.getFirstRec() - 1;
				procReqCodesEleList(user, req, res, session);
			}

	} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

	
//	
}
