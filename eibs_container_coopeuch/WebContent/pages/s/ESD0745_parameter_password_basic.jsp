<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<%@page import="com.datapro.eibs.constants.HelpTypes"%>



<html>
<%@ page import = "datapro.eibs.master.Util" %>
<head>
<title>Mantenimiento Control de Par�metros para Contrase�a eIBS</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V4.0 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="pp" class="datapro.eibs.beans.ESD074501Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

</head>

<body bgcolor="#FFFFFF">

 <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0") ;
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Mantenimiento Control de Par�metros para Contrase�a eIBS<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="parameter_password_basic, ESD0745" ></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.security.JSESD0745" >
  <p>
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="3">
    <INPUT TYPE=HIDDEN NAME="OPT" VALUE="<%= userPO.getPurpose()%>">
  </p>
  <table  class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
			
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Banco :</div>
            </td>
            <td nowrap> 
              <input type="text" name="E01EPRBNK" size="3" maxlength="2" value="<%= pp.getE01EPRBNK().trim()%>" <% if (!userPO.getPurpose().equals("N")) out.print("readonly");%>>
             </td>  
          </tr>
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Descripci�n :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRDSC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Informaci�n B�sica</h4>
  <table  class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
			
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Dias de Acceso al sistema e-IBS :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPREXP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="4" maxlength="3"/>
              <b>(0=Indefinido)</b>
             </td>  
          </tr>
          <tr id="trclear"> 
            <td nowrap width="50%"> 
              <div align="right">Cantidad de Caracteres en la Clave :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRPSL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="3" maxlength="2"/>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Numero de Caracteres Repetidos :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRRPC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="3" maxlength="2"/>
	              <b>(0=No Repeticiones Permitidas)</b>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Cantidad de letras que se pude tener :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRLET" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="3" maxlength="2"/>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Intentos fallidos antes de inactivar :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRETM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="3" maxlength="2"/>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Dias de inactividad para desabilitar :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRDPI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="4" maxlength="3"/>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Frecuencia para cambio de la Clave :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRCHG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="4" maxlength="3"/>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Historico de claves no Permitidas :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRFG1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="2" maxlength="1"/>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>
  <h4>Informacion sobre puertos para el e-IBS y d-IBS </h4>
  <table  class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="50%"> 
              <div align="right">Puerto Inicial para Socket del e-IBS :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRSK1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="6" maxlength="5"/>
             </td>  
          </tr>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Puerto para el Socket Monitor de d-IBS :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRSK2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="6" maxlength="5"/>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Puerto Inicial para Socket del d-IBS :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRSK3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="6" maxlength="5"/>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Tiempo de espera para Cancelacion :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="pp" property="E01EPRTOU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="6" maxlength="5"/>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Generacion del archivo Trace :</div>
            </td>
            <td nowrap> 
              <input type="radio" name="E01EPRTRC" value="Y" <%if (pp.getE01EPRTRC().equals("Y")) out.print("checked"); %>>S�
              <input type="radio" name="E01EPRTRC" value="N" <%if (pp.getE01EPRTRC().equals("N")) out.print("checked"); %>>No
            </td>
          </tr>
         </table>
      </td>
    </tr>
  </table>
 
  <br>
   <p align="center">
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
</form>
</body>
</html>

