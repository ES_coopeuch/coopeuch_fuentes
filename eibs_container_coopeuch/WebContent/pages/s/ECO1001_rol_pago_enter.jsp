<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Generaci&oacute;n Interfaces UNAP</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id="msgControl" class="datapro.eibs.beans.ECO100101Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

</head>

<body>

<SCRIPT LANGUAGE="JavaScript">
function DisplayArch(ImpoExep) 
{
	
	InputArch = document.getElementById("SelArchivo");
	
	if(ImpoExep=="S")	
		InputArch.style.display = "";  
	else
		InputArch.style.display = "none"; 

}		
		

function GoAction() 
{
	
	ImpoExep = document.getElementById("E01FLGFIL").value;
	document.forms[0].action = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO1001?SCREEN=200&E01FLGFIL="+ImpoExep;
	document.forms[0].submit();		

}			
		
</SCRIPT>

<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>
 
<H3 align="center">Generaci&oacute;n Interfaces UNAP<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="rol_pago, ECO1001"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action=""  enctype="multipart/form-data">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">

  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0"> 
  	<tr id="trdark"> 
	    <%if(msgControl.getE01CODRET().equals("0")){ %>	
        <td align=CENTER width="50%"> 
          <div align="right" >Importar Excepciones 
          	<select name="E01FLGFIL" onchange="DisplayArch(this.value);">   
            	<option value="S" >SI</option>                   
            	<option value="N" selected>NO</option>
	  		</select> 
          </div>
        </td>
        <td align=CENTER width="50%"> 
          <div align="left" id="SelArchivo" style="display: none;">
          : <input type="file" name="file" size="45"></div>
        </td>
	    <%} else { %>	
        <td align=CENTER > 
          <div align="center" ><B>Actualmente el proceso esta en ejecuci&oacute;n. Favor intentar en unos minutos...</B>
          </div>
        </td>
	    <%}%>        
      </tr>
  </table>
  <%if(msgControl.getE01CODRET().equals("0")){ %>	
  <p align="center">
      <input id="EIBSBTN" type="button" name="Generar" value="Generar" onclick="GoAction()">
  </p>
  <%}%>        

</form>
</body>
</html>
