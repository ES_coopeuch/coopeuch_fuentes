<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Productos de Prestamos Nuevo/Mantenimiento</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


</head>

<jsp:useBean id="prd" class="datapro.eibs.beans.ESD070001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body onload=javascript:init()>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     error.setERRNUM("0");
     out.println("</SCRIPT>");
     }
%>
<SCRIPT Language="Javascript">
       
     builtNewMenu(prd_loan_opt);
     initMenu();
     
function init() {
      if ("<%= prd.getE01APCSF1().trim() %>" == 'Y') {
			Options('S') ;
      }
}
      
function Options(Type) {
	if (Type == 'S') {	
	document.forms[0].E01APCSF1.value='Y'	
	divAddOpt.style.display = "";
	};
	else {
	document.forms[0].E01APCSF1.value='N'		
	divAddOpt.style.display = "none";
	};
}		 
       
</SCRIPT>

<h3 align="center"> Producto de Pr&eacute;stamos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="products_loans.jsp, ESD0700"></h3>
<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0700" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="1">
  <INPUT TYPE=HIDDEN NAME="E01APCACD" VALUE="<%= prd.getE01APCACD()%>">
  <table class="tableinfo">
    <tr > 
      <td> 
        <table cellspacing="0" cellpadding="2" width="100%"  class="tbhead">
          <tr id=trdark> 
            <td nowrap width="10%" align="right"> Banco: </td>
            <td nowrap width="12%" align="left"> 
              <input type="text"  name="E01APCBNK" size="3" maxlength="2" value="<%= prd.getE01APCBNK()%>" readonly>
            </td>
            <td nowrap width="6%" align="right">Producto: </td>
            <td nowrap width="14%" align="left"> 
              <input type="text"  name="E01APCCDE" size="6" maxlength="4" value="<%= prd.getE01APCCDE()%>" readonly>
            </td>
            <td nowrap width="8%" align="right"> Tipo de Producto : </td>
            <td nowrap width="50%"align="left"> 
              <input type="text"  name="E01APCTYP" size="6" maxlength="4" value="<%= prd.getE01APCTYP()%>" readonly>
              - 
              <input type="text"  name="E01DSCTYP" size="45" maxlength="45" value="<%= prd.getE01DSCTYP()%>" readonly>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Informaci&oacute;n General</h4>

  <table class="tableinfo">
    <tr > 
      <td > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td width="25%" > 
              <div align="right">Descripci&oacute;n :</div>
            </td>
            <td width="30%" > 
              <input type="text"  name="E01APCDS1" size="50" maxlength="45" value="<%= prd.getE01APCDS1()%>">
            </td>
            <td width="25%"> 
              <div align="right">Nombre de Mercadeo :</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCDS2" size="28" maxlength="25" value="<%= prd.getE01APCDS2()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="25%"> 
              <div align="right">C&oacute;digo de Moneda :</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCCCY" size="3" maxlength="3" value="<%= prd.getE01APCCCY()%>">
              <a href="javascript:GetCurrency('E01APCCCY','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a> 
            </td>
            <td width="25%"> 
              <div align="right">Ofrecimiento por :</div>
            </td>
            <td width="30%">
            <SELECT name="E01APCFTT">
					<OPTION value="1"
						<%if (prd.getE01APCFTT().equals("1")) { out.print("selected"); }%>>Internet</OPTION>
					<OPTION value="I"
						<%if (prd.getE01APCFTT().equals("I")) { out.print("selected"); }%>>Internacional</OPTION>
					<OPTION value="L"
						<%if (prd.getE01APCFTT().equals("L")) { out.print("selected"); }%>>Local</OPTION>
					<OPTION value="3"
						<%if (prd.getE01APCFTT().equals("3")) { out.print("selected"); }%>>Plataforma EIBS</OPTION>
					<OPTION value="4"
						<%if (prd.getE01APCFTT().equals("4")) { out.print("selected"); }%>>Plataforma Venta</OPTION>
					<OPTION value="5"
						<%if (prd.getE01APCFTT().equals("5")) { out.print("selected"); }%>>Cualquier
					Medio</OPTION>
					<OPTION value="N"
						<%if (prd.getE01APCFTT().equals("N")) { out.print("selected"); }%>>No
					Ofrecer</OPTION>
				</SELECT>
            </td>
          </tr>
          <tr id="trdark"> 
            <td width="25%"> 
              <div align="right"> Cuenta Contable:</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCGLN" size="18" maxlength="16" value="<%= prd.getE01APCGLN() %>">
              <a href="javascript:GetLedger('E01APCGLN',document.forms[0].E01APCBNK.value,document.forms[0].E01APCCCY.value,document.forms[0].E01APCACD.value,'','<%= prd.getE01APCTYP().trim()%>')">
              <img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0" ></a> 
            </td>
            <td width="25%"> 
              <div align="right">Retenci&oacute;n de Impuestos :</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCTAX" size="3" maxlength="2" value="<%= prd.getE01APCTAX()%>" readonly>
              <a href="javascript:GetCode('E01APCTAX','STATIC_ln_prod_tax_ret.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a> 
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="25%"> 
              <div align="right">Tabla de Documentos :</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCFTF" size="4" maxlength="2" value="<%= prd.getE01APCFTF() %>" onKeypress="enterInteger()">
              <a href="javascript:GetDocInv('E01APCFTF')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a> 
            </td>
            <td width="25%"> 
              <div align="right">Tipo de Residencia :</div>
            </td>
            <td width="30%"> 
              <select name="E01APCRES">
                <option value="" <%if (prd.getE01APCRES().equals(""))   { out.print("selected"); }%>>Cualquiera</option> 
                <option value="1" <%if (prd.getE01APCRES().equals("1")) { out.print("selected"); }%>>Residentes</option>
                <option value="2" <%if (prd.getE01APCRES().equals("2")) { out.print("selected"); }%>>Extranjeros</option>
              </select>
            </td>
          </tr>
          <tr id="trdark">
            <td width="25%"> 
              <div align="right">Producto Familia :</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCUC1" size="4" maxlength="2" value="<%= prd.getE01APCUC1() %>">
            </td>
            <td width="25%"> 
              <div align="right">Tipo de Cliente :</div>
            </td>
            <td width="30%">
            <SELECT name="E01APCFRA">
					<OPTION value=" "
						<%if (prd.getE01APCFRA().equals(" ")) { out.print("selected"); }%>>Cualquiera</OPTION>
					<OPTION value="0"
						<%if (prd.getE01APCFRA().equals("0")) { out.print("selected"); }%>>Socio</OPTION>
					<OPTION value="1"
						<%if (prd.getE01APCFRA().equals("1")) { out.print("selected"); }%>>Empresa</OPTION>
					<OPTION value="2"
						<%if (prd.getE01APCFRA().equals("2")) { out.print("selected"); }%>>Persona</OPTION>
				</SELECT>
            </td>
          </tr>
		  <tr id="trclear">
	          	<td>
	          		<div align="right">Visualizar Sitio Privado :</div>
	          	</td>
	          	<td >
	          		<input type="radio" name="E01APAFG1" value="Y"  <%if (prd.getE01APAFG1().equals("Y")) out.print("checked"); %>>
	              Si 
	              <input type="radio" name="E01APAFG1" value="N"  <%if (prd.getE01APAFG1().equals("N")) out.print("checked"); %>>
	              No
	            </td>
	            <td>
				</td>
				<td>
				</td>
			</tr>
        </table>
      </td>
    </tr>
  </table>

  <h4>Informaci&oacute;n Adicional</h4>

  <table class="tableinfo" width="779" >
    <tr > 
      <td > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trclear"> 
            <td width="25%"> 
              <div align="right">Clase de Prestamo : </div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCFRN" size="3" maxlength="1" value="<%= prd.getE01APCFRN() %>" readonly>
              <a href="javascript:GetCode('E01APCFRN','STATIC_ln_cred_class.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a> 
            </td>
            <td width="25%"> 
              <div align="right">Entregas Graduales : </div>
            </td>
            <td width="30%"> 
              <input type="radio" name="E01APCCHR" value="Y"  <%if (!prd.getE01APCCHR().equals("N")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APCCHR" value="N"  <%if (prd.getE01APCCHR().equals("N")) out.print("checked"); %>>
              No
			</td>
          </tr>
          <tr id="trdark"> 
            <td width="25%"> 
              <div align="right">Refinanciamiento :</div>
            </td>
            <td width="30%">
              <input type="text"  name="E01APCSF3" size="3" maxlength="1" value="<%= prd.getE01APCSF3() %>" readonly>
              <a href="javascript:GetCode('E01APCSF3','STATIC_ln_ref.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a> 
            </td>
            <td width="25%"> 
              <div align="right">Intereses en la Cuota : </div>
            </td>
            <td width="30%"> 
              <input type="radio" name="E01APCREL" value="Y"  <%if (!prd.getE01APCREL().equals("N")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APCREL" value="N"  <%if (prd.getE01APCREL().equals("N")) out.print("checked"); %>>
              No</td>
          </tr>
          <tr id="trclear"> 
            <td width="25%"> 
              <div align="right">C&aacute;lculo de Interes Normal :</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCMCI" size="3" maxlength="1" value="<%= prd.getE01APCMCI() %>" readonly>
              <a href="javascript:GetCode('E01APCMCI','STATIC_ln_prov.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a> 
            </td>
            <td width="25%"> 
              <div align="right"> Deducciones :</div>
            </td>
            <td width="30%"> 
              <input type="radio" name="E01APCSF1" value="Y" onClick="javascript:Options('S')" <%if (prd.getE01APCSF1().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APCSF1" value="N" onClick="javascript:Options('H')" <%if (prd.getE01APCSF1().equals("N")) out.print("checked"); %>>
              No</td>
          </tr>
          <tr id="trdark"> 
            <td width="25%"> 
              <div align="right">C&aacute;lculo de Interes Mora :</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCMCP" size="3" maxlength="1" value="<%= prd.getE01APCMCP() %>" readonly>
              <a href="javascript:GetCode('E01APCMCP','STATIC_ln_mor.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a></td>
            <td width="25%"> 
              <div align="right">Garant&iacute;as :</div>
            </td>
            <td width="30%">
              <input type="radio" name="E01APCSF2" value="Y"  <%if (prd.getE01APCSF2().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APCSF2" value="N"  <%if (prd.getE01APCSF2().equals("N")) out.print("checked"); %>>
              No</td>
          </tr>
          <tr id="trclear"> 
            <td width="25%"> 
              <div align="right">Ciclo de Pago de Intereses :</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCIPD" size="3" maxlength="3" value="<%= prd.getE01APCIPD() %>" >
              <a href="javascript:GetCode('E01APCIPD','STATIC_ln_pay_int.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a></td>
            <td width="25%"> 
              <div align="right">Control de Pagos :</div>
            </td>
            <td width="30%">
              <input type="radio" name="E01APCSF4" value="Y"  <%if (prd.getE01APCSF4().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APCSF4" value="N"  <%if (prd.getE01APCSF4().equals("N")) out.print("checked"); %>>
              No</td>
          </tr>
          <tr id="trdark"> 
            <td width="25%"> 
              <div align="right">Ciclo de Pago Principal :</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APCPPD" size="3" maxlength="3" value="<%= prd.getE01APCPPD() %>" >
              <a href="javascript:GetCode('E01APCPPD','STATIC_ln_pay_int.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a></td>
            <td width="25%"> 
              <div align="right">Propuesta Cr&eacute;dito :</div>
            </td>
            <td width="30%">
              <input type="radio" name="E01APCCRF" value="1"  <%if (prd.getE01APCCRF().equals("1")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APCCRF" value="N"  <%if (prd.getE01APCCRF().equals("N")) out.print("checked"); %>>
              No</td>
          </tr>
          <tr id="trclear"> 
            <td width="25%"> 
              <div align="right">Tabla de Tarifas :</div>
            </td>
            <td width="30%"> 
              <input type="text" name="E01APCTLN" size="3" maxlength="2" value="<%= prd.getE01APCTLN()%>">
              <a href="javascript:GetLoanTable('E01APCTLN',document.forms[0].E01APCTYP.value)"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a></td>
            <td width="25%"> 
              <div align="right">Tabla Previsi&oacute;n :</div>
            </td>
            <td width="30%">
              <input type="text" name="E01APCRLT" size="3" maxlength="2" value="<%= prd.getE01APCRLT() %>">
              <a href="javascript:GetPrevTable('E01APCRLT')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"  ></a>
            </td>
          </tr>
          <tr id="trdark"> 
            <td width="25%"> 
              <div align="right">Estructura de Tasas :</div>
            </td>
            <td width="30%"> 
              <input type="text" name="E01APCCDT" size="3" maxlength="2" value="<%= prd.getE01APCCDT() %>">
              <a href="javascript:GetRateTable('E01APCCDT')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a></td>
            <td width="25%"> 
              <div align="right">Libera Garant&iacute;a :</div>
            </td>
            <td width="30%">
              <input type="radio" name="E01APCRCL" value="Y"  <%if (prd.getE01APCRCL().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APCRCL" value="N"  <%if (prd.getE01APCRCL().equals("N")) out.print("checked"); %>>
              No</td>
          </tr>
          <tr id="trclear"> 
            <td width="25%"> 
              <div align="right">Tabla Tasa Flotante o Lider :</div>
            </td>
            <td width="30%"> 
              <input type="text" name="E01APCFRT" size="3" maxlength="2" value="<%= prd.getE01APCFRT() %>">
 	              <a href="javascript:GetFloating('E01APCFRT')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Tabla de Tasas Flotantes" align="absmiddle" border="0" ></a> 
              <select name="E01APCFTY">
	                <option value=" " <% if (!(prd.getE01APCFTY().equals("FP") ||prd.getE01APCFTY().equals("FS"))) out.print("selected"); %>></option>
					<option value="FP" <%if (prd.getE01APCFTY().equals("FP")) { out.print("selected"); }%>>Primaria</option>
					<option value="FS" <%if (prd.getE01APCFTY().equals("FS")) { out.print("selected"); }%>>Secundaria</option>
				</select>

            </td>
            <td width="25%"> 
              <div align="right">Tipo Monto de Garant&iacute;a :</div>
            </td>
            <td width="30%">
			 <select name="E01APCAMC">
                <option value="1" <%if (prd.getE01APCAMC().equals("1")) { out.print("selected"); }%>>Solo Capital</option>
                <option value="2" <%if (prd.getE01APCAMC().equals("2")) { out.print("selected"); }%>>Capital e Intereses</option>
                <option value="3" <%if (prd.getE01APCAMC().equals("3")) { out.print("selected"); }%>>Capital, Intereses y Mora</option>
              </select>
			</td>
          </tr>
   
          <tr id="trdark"> 
            <td width="25%"> 
              <div align="right">Tabla de Tramos Preferenciales :</div>
            </td>
            <td width="30%"> 
              <input type="text" name="E01APCCC4" size="3" maxlength="2" value="<%= prd.getE01APCCC4() %>">
              <a href="javascript:GetLimit('E01APCCC4',document.forms[0].E01APCBNK.value,'')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a> 
            </td>
            <td width="25%"> 
              <div align="right">Ciclo Cambio Tasa :</div>
            </td>
            <td width="30%">
              <input type="text" name="E01APCRRD" size="3" maxlength="3" value="<%= prd.getE01APCRRD() %>">
                 <a href="javascript:GetCode('E01APCRRD','STATIC_cycle_rev.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a> 
            </td>
          </tr>

          <tr id="trclear"> 
            <td width="25%"> 
              <div align="right">Termino Contrato :</div>
            </td>
            <td width="30%"> 
              <select name="E01APCFL4">
					<option value=""></option>
					<option value="C" <%if (prd.getE01APCFL4().equals("C")) { out.print("selected"); }%>>Corto Plazo</option>
					<option value="L" <%if (prd.getE01APCFL4().equals("L")) { out.print("selected"); }%>>Largo Plazo</option>
				</select>
            </td>
            <td width="25%"> 
              <div align="right">Penalidad por Prepago :</div>
            </td>
            <td width="30%">
              <input type="text" name="E01APCROY" size="3" maxlength="3" value="<%= prd.getE01APCROY() %>" onKeypress="enterInteger()">
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap width=25% align="right"> Codigo Actividad Economica :</td>	        
	        <td nowrap width="30%"> 
              <input type="text" name="E01APACDE" size="4" maxlength="4" value="<%= prd.getE01APACDE().trim()%>">
              <a href="javascript:GetCodeDescCNOFC('E01APACDE','D01APANME','2C')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0"></a>
            </td>
          
            <td width="25%"> 
              <div align="right">Tipo Prestamo S&uacute;per :</div>
            </td>
            <td width="30%"> 
               <input type="text"  name="E01APCFL3" size="3" maxlength="1" value="<%= prd.getE01APCFL3() %>" >
              <a href="javascript:GetCode('E01APCFL3','STATIC_ln_prod_sup.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a> 
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width=25% align="right"> Numero Meses No Pago :</td>	        
	        <td nowrap width="30%"> 
              <select name="E01APCFL5">
					<option value=""></option>
					<option value="1" <%if (prd.getE01APCFL5().equals("1")) { out.print("selected"); }%>>1 Mes</option>
					<option value="2" <%if (prd.getE01APCFL5().equals("2")) { out.print("selected"); }%>>2 Meses</option>
					<option value="3" <%if (prd.getE01APCFL5().equals("3")) { out.print("selected"); }%>>3 Meses</option>
				</select>
            </td>
 			 <%if(currUser.getE01INT().equals("07")){%> 
            <td width="25%"> 
              <div align="right">Sujeto Cobro FECI :</div>
            </td>
            <td width="30%"> 
              <input type="radio" name="E01APCTX2" value="Y"  <%if (prd.getE01APCTX2().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APCTX2" value="N"  <%if (!prd.getE01APCTX2().equals("Y")) out.print("checked"); %>>
              No
            </td>
			  <% } else { %>
          	<td>
          		<div align="right">Tipo de Credito :</div>
          	</td>
          	<td>
          		<select name="E01APCFL2">
					<option value="" <% if (prd.getE01APCFL2().equals("")) out.print("selected");%>> </option>
					<option value="1" <% if (prd.getE01APCFL2().equals("1")) out.print("selected");%>>Credito de Consumo</option>
					<option value="2" <% if (prd.getE01APCFL2().equals("2")) out.print("selected");%>>Credito Estudios Superiores</option>
					<option value="3" <% if (prd.getE01APCFL2().equals("3")) out.print("selected");%>>Credito Comercial</option>
					<option value="4" <% if (prd.getE01APCFL2().equals("4")) out.print("selected");%>>Credito Fogape</option>
             	</select>
            </td>
			  <% } %>
          </tr>

        </table>
      </td>
    </tr>
  </table>


  <%if(currUser.getE01INT().equals("18") &&  prd.getH01FLGWK3().equals("H")){%> 
  
  <h4>Creditos Hipotecarios</h4>

  <table class="tableinfo">
    <tr > 
      <td > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td width="25%"> 
              <div align="right">Credito Preferencial :</div>
            </td>
            <td width="30%"> 
              <select name="E01APARFL">
					<option value=""></option>
					<option value="1" <%if (prd.getE01APARFL().equals("1")) { out.print("selected"); }%>>Vivienda Nueva</option>
					<option value="2" <%if (prd.getE01APARFL().equals("2")) { out.print("selected"); }%>>Forestal</option>
					<option value="3" <%if (prd.getE01APARFL().equals("3")) { out.print("selected"); }%>>Restauraci�n</option>
				</select>
            </td>
            <td nowrap width=25% > 
              <div align="right">Utiliza Tabla de Desarrollo :</div>
            </td>	        
	        <td nowrap width="30%"> 
               <input type="radio" name="E01APATY1" value="Y"  <%if (prd.getE01APATY1().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APATY1" value="N"  <%if (prd.getE01APATY1().equals("N")) out.print("checked"); %>>
              No
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width=25% align="right"> 
              <div align="right">M&aacute;ximo meses de gracia :</div>
			</td>	        
	        <td nowrap width="30%"> 
              <input type="text"  name="E01APAUS1" size="4" maxlength="3" value="<%= prd.getE01APAUS1() %>" onkeypress="enterInteger()">
            </td>
            <td width="25%"> 
 	           <div align="right">Constitucion de Dividendos :</div> 
            </td>
            <td width="30%"> 
	            <input type="radio" name="E01APARTB" value="Y"  <%if (prd.getE01APARTB().equals("Y")) out.print("checked"); %>>
	              Si 
	              <input type="radio" name="E01APARTB" value="N"  <%if (prd.getE01APARTB().equals("N")) out.print("checked"); %>>
	              No         
	        </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap width=25% > 
              <div align="right">Producto a generar del pasivo :</div>
			</td>	        
	        <td nowrap width="30%"> 
              <input type="text" name="E01APAPRD" size="6" maxlength="4" value="<%= prd.getE01APAPRD().trim()%>">
	             <a href="javascript:GetProduct('E01APAPRD','10','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
            </td>
            <td width="25%"> 
               <div align="right">Porcentaje m&aacute;ximo financiamiento : </div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APAOIS" size="10" maxlength="9" value="<%= prd.getE01APAOIS() %>" onkeypress="enterDecimal()">
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width=25% > 
              <div align="right">Meses  Gracia para 1er Dividendo :</div>
			</td>	        
	        <td nowrap width="30%"> 
              <input type="text"  name="E01APAUS3" size="4" maxlength="3" value="<%= prd.getE01APAUS3() %>" onKeypress="enterInteger()">
            </td>
            <td width="25%"> 
              <div align="right">Dias para aplicar 1er cambio  Tasa :</div>
            </td>
            <td width="30%"> 
              <input type="text"  name="E01APAANP" size="5" maxlength="4" value="<%= prd.getE01APAANP() %>" onKeypress="enterInteger()">
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap width=25% > 
              <div align="right">Opciones de prepago : </div>
			</td>	        
	        <td nowrap width="30%"> 
              <select name="E01APAPTY">
				<option value=""></option>
                <option value="A" <%if (prd.getE01APAPTY().equals("A")) { out.print("selected"); }%>>Adelgazamiento</option>
                <option value="D" <%if (prd.getE01APAPTY().equals("D")) { out.print("selected"); }%>>Disminuci&oacute;n plazo</option>
                <option value="B" <%if (prd.getE01APAPTY().equals("B")) { out.print("selected"); }%>>Ambos</option>
                <option value="N" <%if (prd.getE01APAPTY().equals("N")) { out.print("selected"); }%>>No permite</option> 
              </select>
            </td>
            <td width="25%"> 
              <div align="right">% Minimo de capital para prepago :</div>
				</td>
            <td width="30%"> 
              <input type="text"  name="E01APART1" size="10" maxlength="9" value="<%= prd.getE01APART1()%>" onkeypress="enterDecimal()">  
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width=25% > 
              <div align="right">D&iacute;as aceleracion autom&aacute;tica : </div>
			</td>	        
	        <td nowrap width="30%"> 
              <input type="text"  name="E01APAUS2" size="4" maxlength="3" value="<%= prd.getE01APAUS2() %>" onkeypress="enterInteger()">  
            </td>
            <td width="25%"> 
              <div align="right">Exclusi&oacute;n no pago de dividendo : </div>
				</td>
            <td width="30%"> 
               <input type="radio" name="E01APATY5" value="Y"  <%if (prd.getE01APATY5().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APATY5" value="N"  <%if (prd.getE01APATY5().equals("N")) out.print("checked"); %>>
              No
			</td>
          </tr>

          <tr id="trdark"> 
            <td nowrap width=25% > 
              <div align="right">Distribucci&oacute;n meses de no pago : </div>
			</td>	        
	        <td nowrap width="30%"> 
              <select name="E01APATY2">
                <option value="C" <%if (prd.getE01APATY2().equals("C")) { out.print("selected"); }%>>Capitalizaci&oacute;n</option>
                <option value="D" <%if (prd.getE01APATY2().equals("D")) { out.print("selected"); }%>>Distribucci&oacute;n</option>
                <option value="" <%if (prd.getE01APATY2().equals("")) { out.print("selected"); }%>></option>
              </select>
            </td>
            <td width="25%"> 
              <div align="right">Cobro Seguro en meses de gracia : </div>
			</td>
            <td width="30%"> 
              <select name="E01APATY3">
                <option value="1" <%if (prd.getE01APATY3().equals("1")) { out.print("selected"); }%>>Primer dividendo</option>
                <option value="2" <%if (prd.getE01APATY3().equals("2")) { out.print("selected"); }%>>Capitalizarlo</option>
                <option value="3" <%if (prd.getE01APATY3().equals("3")) { out.print("selected"); }%>>Cobro en el Curce</option>
                <option value="N" <%if (prd.getE01APATY3().equals("N")) { out.print("selected"); }%>>No cobrarlo</option>
              </select>
			</td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width=25% > 
              <div align="right">Metodo C&aacute;lculo comision prepago :</div>
			</td>	        
	        <td nowrap width="30%"> 
              <select name="E01APATY6">
                <option value="0" <%if (prd.getE01APATY6().equals("0")) { out.print("selected"); }%>>Cupon actual</option>
                <option value="1" <%if (prd.getE01APATY6().equals("1")) { out.print("selected"); }%>>Actual + proximo cupon</option>
                <option value="2" <%if (prd.getE01APATY6().equals("2")) { out.print("selected"); }%>>Actual + proximo 2 cupones</option>
                <option value="3" <%if (prd.getE01APATY6().equals("3")) { out.print("selected"); }%>>Actual + proximo 3 cupones</option>
                <option value="4" <%if (prd.getE01APATY6().equals("4")) { out.print("selected"); }%>>Actual + proximo 4 cupones</option>
                <option value="5" <%if (prd.getE01APATY6().equals("5")) { out.print("selected"); }%>>Actual + proximo 5 cupones</option>
                <option value="6" <%if (prd.getE01APATY6().equals("6")) { out.print("selected"); }%>>Actual + proximo 6 cupones</option>
                <option value="7" <%if (prd.getE01APATY6().equals("7")) { out.print("selected"); }%>>Actual + proximo 7 cupones</option>
                <option value="8" <%if (prd.getE01APATY6().equals("8")) { out.print("selected"); }%>>Actual + proximo 8 cupones</option>
                <option value="9" <%if (prd.getE01APATY6().equals("9")) { out.print("selected"); }%>>Actual + proximo 9 cupones</option>
                <option value="" <%if (prd.getE01APATY6().equals("")) { out.print("selected"); }%>>No calcula comision </option>   
              </select>
            </td>
            <td width="25%"> 
              <div align="right">Fecha Inicio Calculo Interes :</div>
			</td>
            <td width="30%"> 
            	<select name="E01APAF10">
              		<option value="0" <%if (prd.getE01APAF10().equals("0")) { out.print("selected"); }%>>Firma Escritura</option>
              		<option value="1" <%if (prd.getE01APAF10().equals("1")) { out.print("selected"); }%>>Fecha Apertura</option>
              	</select>
			</td>
          </tr>

        </table>
      </td>
    </tr>
  </table>

  <% } %>

  
 <h4>L&iacute;mites</h4>

  <table class="tableinfo" width="779" >
    <tr > 
      <td > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="trdark"> 
            <td width="25%"> 
              <div align="right">Monto M&iacute;nimo Apertura :</div>
            </td>
            <td width="30%">
            <input type="text" name="E01APCAM1" size="17" maxlength="15" value="<%= prd.getE01APCAM1().trim()%>" onKeypress="enterDecimal()">
            </td>
            <td width="25%"> 
              <div align="right">Monto M&aacute;ximo Apertura : </div>
            </td>
            <td width="30%">
            <input type="text" name="E01APCAM2" size="17" maxlength="15" value="<%= prd.getE01APCAM2().trim()%>" onKeypress="enterDecimal()">
            </td>
          </tr>

          <tr id="trclear"> 
            <td width="25%"> 
              <div align="right">Plazo M&iacute;nimo en Meses :</div>
            </td>
            <td width="30%">
            <input type="text" name="E01APCUC2" size="5" maxlength="4" value="<%= prd.getE01APCUC2().trim()%>" onKeypress="enterInteger()">
            </td>
            <td width="25%"> 
              <div align="right">Plazo M&aacute;ximo en Meses : </div>
            </td>
            <td width="30%">
            <input type="text" name="E01APCUC3" size="5" maxlength="4" value="<%= prd.getE01APCUC3().trim()%>" onKeypress="enterInteger()">
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>



  
  <div id="divAddOpt" style="display:none">  
  <h4>Deducciones</h4>
  <table class="tableinfo">
  	<tr id="trdark">
  		<td align="center">C�digo</td>
  		<td align="center">Descripcion</td>
  		<td align="center">Tipo de Cobro</td>
  		<td align="center">Forma de Pago</td>
  	</tr>
  
  	<tr id="trclear">
    	<td align="center">
    	   <eibsinput:help name="prd" property="E01APDDED1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" 
   	    		fn_param_one="E01APDDED1" fn_param_two="D01APDDED1" fn_param_three="I" />
		</td> 
    	<td align="center">
       	   <eibsinput:text	name="prd" property="D01APDDED1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
		</td>
  		<td align="center"> 
  		            <select name="E01APDFL21">
						<OPTION value=""></OPTION>
						<option value="O" <%if (prd.getE01APDFL21().equals("O")) { out.print("selected"); }%>>Apertura</option>
						<option value="L" <%if (prd.getE01APDFL21().equals("L")) { out.print("selected"); }%>>Ciclo de Pago</option>
					</select>
  		
  		</td>
  		<td align="center"> <SELECT name="E01APDFL31">
			<OPTION value=""></OPTION>
			<OPTION value="1" <%if (prd.getE01APDFL31().equals("1")) { out.print("selected"); }%>>Financiado</OPTION>
			<OPTION value="2" <%if (prd.getE01APDFL31().equals("2")) { out.print("selected"); }%>>Pre-Pagado</OPTION>
		</SELECT>
  		</td>
  	</tr>

  	<tr id="trdark">
    	<td align="center">
    	   <eibsinput:help name="prd" property="E01APDDED2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" 
   	    		fn_param_one="E01APDDED2" fn_param_two="D01APDDED2" fn_param_three="I" />
		</td> 
    	<td align="center">
       	   <eibsinput:text	name="prd" property="D01APDDED2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
		</td>
  		<td align="center"> 
  		            <select name="E01APDFL22">
						<OPTION value=""></OPTION>
						<option value="O" <%if (prd.getE01APDFL22().equals("O")) { out.print("selected"); }%>>Apertura</option>
						<option value="L" <%if (prd.getE01APDFL22().equals("L")) { out.print("selected"); }%>>Ciclo de Pago</option>
					</select>
  		
  		</td>
  		<td align="center"><SELECT name="E01APDFL32">
			<OPTION value=""></OPTION>
			<OPTION value="1" <%if (prd.getE01APDFL32().equals("1")) { out.print("selected"); }%>>Financiado</OPTION>
			<OPTION value="2" <%if (prd.getE01APDFL32().equals("2")) { out.print("selected"); }%>>Pre-Pagado</OPTION>
		</SELECT>
  		</td>
  	</tr>

  	<tr id="trclear">
    	<td align="center">
    	   <eibsinput:help name="prd" property="E01APDDED3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" 
   	    		fn_param_one="E01APDDED3" fn_param_two="D01APDDED3" fn_param_three="I" />
		</td> 
    	<td align="center">
       	   <eibsinput:text	name="prd" property="D01APDDED3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
		</td>
  		<td align="center"> 
  		            <select name="E01APDFL23">
						<OPTION value=""></OPTION>
						<option value="O" <%if (prd.getE01APDFL23().equals("O")) { out.print("selected"); }%>>Apertura</option>
						<option value="L" <%if (prd.getE01APDFL23().equals("L")) { out.print("selected"); }%>>Ciclo de Pago</option>
					</select>
  		
  		</td>
  		<td align="center"> <SELECT name="E01APDFL33">
			<OPTION value=""></OPTION>
			<OPTION value="1" <%if (prd.getE01APDFL33().equals("1")) { out.print("selected"); }%>>Financiado</OPTION>
			<OPTION value="2" <%if (prd.getE01APDFL33().equals("2")) { out.print("selected"); }%>>Pre-Pagado</OPTION>
		</SELECT>
  		</td>
  	</tr>

  	<tr id="trdark">
    	<td align="center">
    	   <eibsinput:help name="prd" property="E01APDDED4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" 
   	    		fn_param_one="E01APDDED4" fn_param_two="D01APDDED4" fn_param_three="I" />
		</td> 
    	<td align="center">
       	   <eibsinput:text	name="prd" property="D01APDDED4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
		</td>
  		<td align="center"> 
  		            <select name="E01APDFL24">
						<OPTION value=""></OPTION>
						<option value="O" <%if (prd.getE01APDFL24().equals("O")) { out.print("selected"); }%>>Apertura</option>
						<option value="L" <%if (prd.getE01APDFL24().equals("L")) { out.print("selected"); }%>>Ciclo de Pago</option>
					</select>
  		
  		</td>
  		<td align="center"> <SELECT name="E01APDFL34">
			<OPTION value=""></OPTION>
			<OPTION value="1" <%if (prd.getE01APDFL34().equals("1")) { out.print("selected"); }%>>Financiado</OPTION>
			<OPTION value="2" <%if (prd.getE01APDFL34().equals("2")) { out.print("selected"); }%>>Pre-Pagado</OPTION>
		</SELECT>
  		</td>
  	</tr>

  	<tr id="trclear">
    	<td align="center">
    	   <eibsinput:help name="prd" property="E01APDDED5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" 
   	    		fn_param_one="E01APDDED5" fn_param_two="D01APDDED5" fn_param_three="I" />
		</td> 
    	<td align="center">
       	   <eibsinput:text	name="prd" property="D01APDDED5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
		</td>
  		<td align="center"> 
  		            <select name="E01APDFL25">
						<OPTION value=""></OPTION>
						<option value="O" <%if (prd.getE01APDFL25().equals("O")) { out.print("selected"); }%>>Apertura</option>
						<option value="L" <%if (prd.getE01APDFL25().equals("L")) { out.print("selected"); }%>>Ciclo de Pago</option>
					</select>
  		
  		</td>
  		<td align="center"> <SELECT name="E01APDFL35">
			<OPTION value=""></OPTION>
			<OPTION value="1" <%if (prd.getE01APDFL35().equals("1")) { out.print("selected"); }%>>Financiado</OPTION>
			<OPTION value="2" <%if (prd.getE01APDFL35().equals("2")) { out.print("selected"); }%>>Pre-Pagado</OPTION>
		</SELECT>
  		</td>
  	</tr>

  	<tr id="trdark">
    	<td align="center">
    	   <eibsinput:help name="prd" property="E01APDDED6" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" 
   	    		fn_param_one="E01APDDED6" fn_param_two="D01APDDED6" fn_param_three="I" />
		</td> 
    	<td align="center">
       	   <eibsinput:text	name="prd" property="D01APDDED6" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
		</td>
  		<td align="center"> 
  		            <select name="E01APDFL26">
						<OPTION value=""></OPTION>
						<option value="O" <%if (prd.getE01APDFL26().equals("O")) { out.print("selected"); }%>>Apertura</option>
						<option value="L" <%if (prd.getE01APDFL26().equals("L")) { out.print("selected"); }%>>Ciclo de Pago</option>
					</select>
  		
  		</td>
  		<td align="center"> <SELECT name="E01APDFL36">
			<OPTION value=""></OPTION>
			<OPTION value="1" <%if (prd.getE01APDFL36().equals("1")) { out.print("selected"); }%>>Financiado</OPTION>
			<OPTION value="2" <%if (prd.getE01APDFL36().equals("2")) { out.print("selected"); }%>>Pre-Pagado</OPTION>
		</SELECT>
  		</td>
  	</tr>

  	<tr id="trclear">
    	<td align="center">
    	   <eibsinput:help name="prd" property="E01APDDED7" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" 
   	    		fn_param_one="E01APDDED7" fn_param_two="D01APDDED7" fn_param_three="I" />
		</td> 
    	<td align="center">
       	   <eibsinput:text	name="prd" property="D01APDDED7" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
		</td>
  		<td align="center"> 
  		            <select name="E01APDFL27">
						<OPTION value=""></OPTION>
						<option value="O" <%if (prd.getE01APDFL27().equals("O")) { out.print("selected"); }%>>Apertura</option>
						<option value="L" <%if (prd.getE01APDFL27().equals("L")) { out.print("selected"); }%>>Ciclo de Pago</option>
					</select>
  		
  		</td>
  		<td align="center"> <SELECT name="E01APDFL37">
			<OPTION value=""></OPTION>
			<OPTION value="1" <%if (prd.getE01APDFL37().equals("1")) { out.print("selected"); }%>>Financiado</OPTION>
			<OPTION value="2" <%if (prd.getE01APDFL37().equals("2")) { out.print("selected"); }%>>Pre-Pagado</OPTION>
		</SELECT>
  		</td>
  	</tr>

  	<tr id="trdark">
    	<td align="center">
    	   <eibsinput:help name="prd" property="E01APDDED8" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" 
   	    		fn_param_one="E01APDDED8" fn_param_two="D01APDDED8" fn_param_three="I" />
		</td> 
    	<td align="center">
       	   <eibsinput:text	name="prd" property="D01APDDED8" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
		</td>
  		<td align="center"> 
  		            <select name="E01APDFL28">
						<OPTION value=""></OPTION>
						<option value="O" <%if (prd.getE01APDFL28().equals("O")) { out.print("selected"); }%>>Apertura</option>
						<option value="L" <%if (prd.getE01APDFL28().equals("L")) { out.print("selected"); }%>>Ciclo de Pago</option>
					</select>
  		
  		</td>
  		<td align="center"> <SELECT name="E01APDFL38">
			<OPTION value=""></OPTION>
			<OPTION value="1" <%if (prd.getE01APDFL38().equals("1")) { out.print("selected"); }%>>Financiado</OPTION>
			<OPTION value="2" <%if (prd.getE01APDFL38().equals("2")) { out.print("selected"); }%>>Pre-Pagado</OPTION>
		</SELECT>
  		</td>
  	</tr>

  	<tr id="trclear">
    	<td align="center">
    	   <eibsinput:help name="prd" property="E01APDDED9" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" 
   	    		fn_param_one="E01APDDED9" fn_param_two="D01APDDED9" fn_param_three="I" />
		</td> 
    	<td align="center">
       	   <eibsinput:text	name="prd" property="D01APDDED9" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
		</td>
  		<td align="center"> 
  		            <select name="E01APDFL29">
						<OPTION value=""></OPTION>
						<option value="O" <%if (prd.getE01APDFL29().equals("O")) { out.print("selected"); }%>>Apertura</option>
						<option value="L" <%if (prd.getE01APDFL29().equals("L")) { out.print("selected"); }%>>Ciclo de Pago</option>
					</select>
  		
  		</td>
  		<td align="center"> <SELECT name="E01APDFL39">
			<OPTION value=""></OPTION>
			<OPTION value="1" <%if (prd.getE01APDFL39().equals("1")) { out.print("selected"); }%>>Financiado</OPTION>
			<OPTION value="2" <%if (prd.getE01APDFL39().equals("2")) { out.print("selected"); }%>>Pre-Pagado</OPTION>
		</SELECT>
  		</td>
  	</tr>

   	
  </table>
  </div>
  
  <h4>Direcciones de Acceso</h4>
  <table class="tableinfo">
    <tr > 
      <td > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td width="20%"> 
              <div align="right">Audio : </div>
            </td>
            <td width="80%"> 
              <input type="text"  name="E01APEAUD" size="82" maxlength="80" value="<%= prd.getE01APEAUD() %>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="20%"> 
              <div align="right">Video : </div>
            </td>
            <td width="80%"> 
              <input type="text"  name="E01APEVID" size="82" maxlength="80" value="<%= prd.getE01APEVID() %>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td width="20%"> 
              <div align="right">HTML : </div>
            </td>
            <td width="80%"> 
              <input type="text"  name="E01APEHTM" size="82" maxlength="80" value="<%= prd.getE01APEHTM() %>">
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

<!-- IFRS DETERIORO -->
  <h4>IFRS - Deterioro</h4>

  <table class="tableinfo">
    <tr > 
      <td > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td width="25%" > 
              <div align="right">Grupo Deterioro :</div>
            </td>
            <td width="30%" >
            <select name="E01APAF01">
					<option value=""
						<%if (prd.getE01APAF01().equals(""))   { out.print("selected"); }%>>SELECCIONE</option>
					<option value="H"
						<%if (prd.getE01APAF01().equals("H")) { out.print("selected"); }%>>Hipotecario</option>
					<option value="P"
						<%if (prd.getE01APAF01().equals("P")) { out.print("selected"); }%>>Planilla</option>
					<option value="O"
						<%if (prd.getE01APAF01().equals("O")) { out.print("selected"); }%>>Otros</option>
				</select></td>
            <td width="25%"> 
              <div align="right">Arrastre Hipotecario :</div>
            </td>
            <td width="30%"> 
              <input type="radio" name="E01APAF02" value="Y"  <%if (prd.getE01APAF02().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APAF02" value="N"  <%if (prd.getE01APAF02().equals("N")) out.print("checked"); %>>
              No
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="25%" rowspan="2"> 
              <div align="right"></div>
            </td>
            <td width="30%" rowspan="2">            </td>
            <td width="25%"> 
              <div align="right">Arrastre Planilla :</div>
            </td>
            <td width="30%">
            <input type="radio" name="E01APAF03" value="Y"
					<%if (prd.getE01APAF03().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APAF03" value="N"
					<%if (prd.getE01APAF03().equals("N")) out.print("checked"); %>>
              No</td>
          </tr>
          <tr id="trdark"> 
            <td width="25%"> 
              <div align="right">Arrastre Otros :</div>
            </td>
            <td width="30%">
				<input type="radio" name="E01APAF04" value="Y"
					<%if (prd.getE01APAF04().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APAF04" value="N"
					<%if (prd.getE01APAF04().equals("N")) out.print("checked"); %>>
              No 
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<!-- FIN IFRS -->


  		<div align="center"> 
    		<input id="EIBSBTN" type=button name="Submit" OnClick="submit()" value="Enviar">
  		</div>
	


 </form>
</body>
</html>
