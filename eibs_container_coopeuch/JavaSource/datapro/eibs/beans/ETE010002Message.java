package datapro.eibs.beans;

import datapro.eibs.sockets.*;
import java.io.*;
import java.math.*;

import java.util.*;

/**
* Class generated by AS/400 CRTCLASS command from ETE010002 physical file definition.
* 
* File level identifier is 1161016225331.
* Record format level identifier is 3A6A3447C3CEE.
*/

public class ETE010002Message extends MessageRecord
{
  final static String fldnames[] = {
                                     "H02USERID",
                                     "H02PROGRM",
                                     "H02TIMSYS",
                                     "H02SCRCOD",
                                     "H02OPECOD",
                                     "H02FLGMAS",
                                     "H02FLGWK1",
                                     "H02FLGWK2",
                                     "H02FLGWK3",
                                     "E02LIFDCO",
                                     "E02LIFCOD",
                                     "E02LIFSBI",
                                     "E02LIFDSB",
                                     "E02LIFSTS",
                                     "E02LIFMIN",
                                     "E02LIFMAX",
                                     "E02LIFLIM",
                                     "E02LIFFA1",
                                     "E02LIFDAY",
                                     "E02LIFPRI",
                                     "E02LIFFMM",
                                     "E02LIFFMD",
                                     "E02LIFFMY",
                                     "E02LIFLMT",
                                     "E02LIFLMU"
                                   };
  final static String tnames[] = {
                                   "H02USERID",
                                   "H02PROGRM",
                                   "H02TIMSYS",
                                   "H02SCRCOD",
                                   "H02OPECOD",
                                   "H02FLGMAS",
                                   "H02FLGWK1",
                                   "H02FLGWK2",
                                   "H02FLGWK3",
                                   "E02LIFDCO",
                                   "E02LIFCOD",
                                   "E02LIFSBI",
                                   "E02LIFDSB",
                                   "E02LIFSTS",
                                   "E02LIFMIN",
                                   "E02LIFMAX",
                                   "E02LIFLIM",
                                   "E02LIFFA1",
                                   "E02LIFDAY",
                                   "E02LIFPRI",
                                   "E02LIFFMM",
                                   "E02LIFFMD",
                                   "E02LIFFMY",
                                   "E02LIFLMT",
                                   "E02LIFLMU"
                                 };
  final static String fid = "1161016225331";
  final static String rid = "3A6A3447C3CEE";
  final static String fmtname = "ETE010002";
  final int FIELDCOUNT = 25;
  private static Hashtable tlookup = new Hashtable();
  private CharacterField fieldH02USERID = null;
  private CharacterField fieldH02PROGRM = null;
  private CharacterField fieldH02TIMSYS = null;
  private CharacterField fieldH02SCRCOD = null;
  private CharacterField fieldH02OPECOD = null;
  private CharacterField fieldH02FLGMAS = null;
  private CharacterField fieldH02FLGWK1 = null;
  private CharacterField fieldH02FLGWK2 = null;
  private CharacterField fieldH02FLGWK3 = null;
  private CharacterField fieldE02LIFDCO = null;
  private DecimalField fieldE02LIFCOD = null;
  private CharacterField fieldE02LIFSBI = null;
  private CharacterField fieldE02LIFDSB = null;
  private CharacterField fieldE02LIFSTS = null;
  private DecimalField fieldE02LIFMIN = null;
  private DecimalField fieldE02LIFMAX = null;
  private DecimalField fieldE02LIFLIM = null;
  private DecimalField fieldE02LIFFA1 = null;
  private DecimalField fieldE02LIFDAY = null;
  private DecimalField fieldE02LIFPRI = null;
  private DecimalField fieldE02LIFFMM = null;
  private DecimalField fieldE02LIFFMD = null;
  private DecimalField fieldE02LIFFMY = null;
  private CharacterField fieldE02LIFLMT = null;
  private CharacterField fieldE02LIFLMU = null;

  /**
  * Constructor for ETE010002Message.
  */
  public ETE010002Message()
  {
    createFields();
    initialize();
  }

  /**
  * Create fields for this message.
  * This method implements the abstract method in the MessageRecord superclass.
  */
  protected void createFields()
  {
    recordsize = 276;
    fileid = fid;
    recordid = rid;
    message = new byte[getByteLength()];
    formatname = fmtname;
    fieldnames = fldnames;
    tagnames = tnames;
    fields = new MessageField[FIELDCOUNT];

    fields[0] = fieldH02USERID =
    new CharacterField(message, HEADERSIZE + 0, 10, "H02USERID");
    fields[1] = fieldH02PROGRM =
    new CharacterField(message, HEADERSIZE + 10, 10, "H02PROGRM");
    fields[2] = fieldH02TIMSYS =
    new CharacterField(message, HEADERSIZE + 20, 12, "H02TIMSYS");
    fields[3] = fieldH02SCRCOD =
    new CharacterField(message, HEADERSIZE + 32, 2, "H02SCRCOD");
    fields[4] = fieldH02OPECOD =
    new CharacterField(message, HEADERSIZE + 34, 4, "H02OPECOD");
    fields[5] = fieldH02FLGMAS =
    new CharacterField(message, HEADERSIZE + 38, 1, "H02FLGMAS");
    fields[6] = fieldH02FLGWK1 =
    new CharacterField(message, HEADERSIZE + 39, 1, "H02FLGWK1");
    fields[7] = fieldH02FLGWK2 =
    new CharacterField(message, HEADERSIZE + 40, 1, "H02FLGWK2");
    fields[8] = fieldH02FLGWK3 =
    new CharacterField(message, HEADERSIZE + 41, 1, "H02FLGWK3");
    fields[9] = fieldE02LIFDCO =
    new CharacterField(message, HEADERSIZE + 42, 45, "E02LIFDCO");
    fields[10] = fieldE02LIFCOD =
    new DecimalField(message, HEADERSIZE + 87, 5, 0, "E02LIFCOD");
    fields[11] = fieldE02LIFSBI =
    new CharacterField(message, HEADERSIZE + 92, 4, "E02LIFSBI");
    fields[12] = fieldE02LIFDSB =
    new CharacterField(message, HEADERSIZE + 96, 45, "E02LIFDSB");
    fields[13] = fieldE02LIFSTS =
    new CharacterField(message, HEADERSIZE + 141, 1, "E02LIFSTS");
    fields[14] = fieldE02LIFMIN =
    new DecimalField(message, HEADERSIZE + 142, 17, 2, "E02LIFMIN");
    fields[15] = fieldE02LIFMAX =
    new DecimalField(message, HEADERSIZE + 159, 17, 2, "E02LIFMAX");
    fields[16] = fieldE02LIFLIM =
    new DecimalField(message, HEADERSIZE + 176, 17, 2, "E02LIFLIM");
    fields[17] = fieldE02LIFFA1 =
    new DecimalField(message, HEADERSIZE + 193, 2, 0, "E02LIFFA1");
    fields[18] = fieldE02LIFDAY =
    new DecimalField(message, HEADERSIZE + 195, 17, 2, "E02LIFDAY");
    fields[19] = fieldE02LIFPRI =
    new DecimalField(message, HEADERSIZE + 212, 17, 2, "E02LIFPRI");
    fields[20] = fieldE02LIFFMM =
    new DecimalField(message, HEADERSIZE + 229, 3, 0, "E02LIFFMM");
    fields[21] = fieldE02LIFFMD =
    new DecimalField(message, HEADERSIZE + 232, 3, 0, "E02LIFFMD");
    fields[22] = fieldE02LIFFMY =
    new DecimalField(message, HEADERSIZE + 235, 5, 0, "E02LIFFMY");
    fields[23] = fieldE02LIFLMT =
    new CharacterField(message, HEADERSIZE + 240, 26, "E02LIFLMT");
    fields[24] = fieldE02LIFLMU =
    new CharacterField(message, HEADERSIZE + 266, 10, "E02LIFLMU");

    synchronized (tlookup)
    {
      if (tlookup.isEmpty())
      {
        for (int i = 0; i < tnames.length; i++)
          tlookup.put(tnames[i], new Integer(i));
      }
    }

    taglookup = tlookup;
  }

  /**
  * Set field H02USERID using a String value.
  */
  public void setH02USERID(String newvalue)
  {
    fieldH02USERID.setString(newvalue);
  }

  /**
  * Get value of field H02USERID as a String.
  */
  public String getH02USERID()
  {
    return fieldH02USERID.getString();
  }

  /**
  * Set field H02PROGRM using a String value.
  */
  public void setH02PROGRM(String newvalue)
  {
    fieldH02PROGRM.setString(newvalue);
  }

  /**
  * Get value of field H02PROGRM as a String.
  */
  public String getH02PROGRM()
  {
    return fieldH02PROGRM.getString();
  }

  /**
  * Set field H02TIMSYS using a String value.
  */
  public void setH02TIMSYS(String newvalue)
  {
    fieldH02TIMSYS.setString(newvalue);
  }

  /**
  * Get value of field H02TIMSYS as a String.
  */
  public String getH02TIMSYS()
  {
    return fieldH02TIMSYS.getString();
  }

  /**
  * Set field H02SCRCOD using a String value.
  */
  public void setH02SCRCOD(String newvalue)
  {
    fieldH02SCRCOD.setString(newvalue);
  }

  /**
  * Get value of field H02SCRCOD as a String.
  */
  public String getH02SCRCOD()
  {
    return fieldH02SCRCOD.getString();
  }

  /**
  * Set field H02OPECOD using a String value.
  */
  public void setH02OPECOD(String newvalue)
  {
    fieldH02OPECOD.setString(newvalue);
  }

  /**
  * Get value of field H02OPECOD as a String.
  */
  public String getH02OPECOD()
  {
    return fieldH02OPECOD.getString();
  }

  /**
  * Set field H02FLGMAS using a String value.
  */
  public void setH02FLGMAS(String newvalue)
  {
    fieldH02FLGMAS.setString(newvalue);
  }

  /**
  * Get value of field H02FLGMAS as a String.
  */
  public String getH02FLGMAS()
  {
    return fieldH02FLGMAS.getString();
  }

  /**
  * Set field H02FLGWK1 using a String value.
  */
  public void setH02FLGWK1(String newvalue)
  {
    fieldH02FLGWK1.setString(newvalue);
  }

  /**
  * Get value of field H02FLGWK1 as a String.
  */
  public String getH02FLGWK1()
  {
    return fieldH02FLGWK1.getString();
  }

  /**
  * Set field H02FLGWK2 using a String value.
  */
  public void setH02FLGWK2(String newvalue)
  {
    fieldH02FLGWK2.setString(newvalue);
  }

  /**
  * Get value of field H02FLGWK2 as a String.
  */
  public String getH02FLGWK2()
  {
    return fieldH02FLGWK2.getString();
  }

  /**
  * Set field H02FLGWK3 using a String value.
  */
  public void setH02FLGWK3(String newvalue)
  {
    fieldH02FLGWK3.setString(newvalue);
  }

  /**
  * Get value of field H02FLGWK3 as a String.
  */
  public String getH02FLGWK3()
  {
    return fieldH02FLGWK3.getString();
  }

  /**
  * Set field E02LIFDCO using a String value.
  */
  public void setE02LIFDCO(String newvalue)
  {
    fieldE02LIFDCO.setString(newvalue);
  }

  /**
  * Get value of field E02LIFDCO as a String.
  */
  public String getE02LIFDCO()
  {
    return fieldE02LIFDCO.getString();
  }

  /**
  * Set field E02LIFCOD using a String value.
  */
  public void setE02LIFCOD(String newvalue)
  {
    fieldE02LIFCOD.setString(newvalue);
  }

  /**
  * Get value of field E02LIFCOD as a String.
  */
  public String getE02LIFCOD()
  {
    return fieldE02LIFCOD.getString();
  }

  /**
  * Set numeric field E02LIFCOD using a BigDecimal value.
  */
  public void setE02LIFCOD(BigDecimal newvalue)
  {
    fieldE02LIFCOD.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02LIFCOD as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02LIFCOD()
  {
    return fieldE02LIFCOD.getBigDecimal();
  }

  /**
  * Set field E02LIFSBI using a String value.
  */
  public void setE02LIFSBI(String newvalue)
  {
    fieldE02LIFSBI.setString(newvalue);
  }

  /**
  * Get value of field E02LIFSBI as a String.
  */
  public String getE02LIFSBI()
  {
    return fieldE02LIFSBI.getString();
  }

  /**
  * Set field E02LIFDSB using a String value.
  */
  public void setE02LIFDSB(String newvalue)
  {
    fieldE02LIFDSB.setString(newvalue);
  }

  /**
  * Get value of field E02LIFDSB as a String.
  */
  public String getE02LIFDSB()
  {
    return fieldE02LIFDSB.getString();
  }

  /**
  * Set field E02LIFSTS using a String value.
  */
  public void setE02LIFSTS(String newvalue)
  {
    fieldE02LIFSTS.setString(newvalue);
  }

  /**
  * Get value of field E02LIFSTS as a String.
  */
  public String getE02LIFSTS()
  {
    return fieldE02LIFSTS.getString();
  }

  /**
  * Set field E02LIFMIN using a String value.
  */
  public void setE02LIFMIN(String newvalue)
  {
    fieldE02LIFMIN.setString(newvalue);
  }

  /**
  * Get value of field E02LIFMIN as a String.
  */
  public String getE02LIFMIN()
  {
    return fieldE02LIFMIN.getString();
  }

  /**
  * Set numeric field E02LIFMIN using a BigDecimal value.
  */
  public void setE02LIFMIN(BigDecimal newvalue)
  {
    fieldE02LIFMIN.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02LIFMIN as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02LIFMIN()
  {
    return fieldE02LIFMIN.getBigDecimal();
  }

  /**
  * Set field E02LIFMAX using a String value.
  */
  public void setE02LIFMAX(String newvalue)
  {
    fieldE02LIFMAX.setString(newvalue);
  }

  /**
  * Get value of field E02LIFMAX as a String.
  */
  public String getE02LIFMAX()
  {
    return fieldE02LIFMAX.getString();
  }

  /**
  * Set numeric field E02LIFMAX using a BigDecimal value.
  */
  public void setE02LIFMAX(BigDecimal newvalue)
  {
    fieldE02LIFMAX.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02LIFMAX as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02LIFMAX()
  {
    return fieldE02LIFMAX.getBigDecimal();
  }

  /**
  * Set field E02LIFLIM using a String value.
  */
  public void setE02LIFLIM(String newvalue)
  {
    fieldE02LIFLIM.setString(newvalue);
  }

  /**
  * Get value of field E02LIFLIM as a String.
  */
  public String getE02LIFLIM()
  {
    return fieldE02LIFLIM.getString();
  }

  /**
  * Set numeric field E02LIFLIM using a BigDecimal value.
  */
  public void setE02LIFLIM(BigDecimal newvalue)
  {
    fieldE02LIFLIM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02LIFLIM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02LIFLIM()
  {
    return fieldE02LIFLIM.getBigDecimal();
  }

  /**
  * Set field E02LIFFA1 using a String value.
  */
  public void setE02LIFFA1(String newvalue)
  {
    fieldE02LIFFA1.setString(newvalue);
  }

  /**
  * Get value of field E02LIFFA1 as a String.
  */
  public String getE02LIFFA1()
  {
    return fieldE02LIFFA1.getString();
  }

  /**
  * Set numeric field E02LIFFA1 using a BigDecimal value.
  */
  public void setE02LIFFA1(BigDecimal newvalue)
  {
    fieldE02LIFFA1.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02LIFFA1 as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02LIFFA1()
  {
    return fieldE02LIFFA1.getBigDecimal();
  }

  /**
  * Set field E02LIFDAY using a String value.
  */
  public void setE02LIFDAY(String newvalue)
  {
    fieldE02LIFDAY.setString(newvalue);
  }

  /**
  * Get value of field E02LIFDAY as a String.
  */
  public String getE02LIFDAY()
  {
    return fieldE02LIFDAY.getString();
  }

  /**
  * Set numeric field E02LIFDAY using a BigDecimal value.
  */
  public void setE02LIFDAY(BigDecimal newvalue)
  {
    fieldE02LIFDAY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02LIFDAY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02LIFDAY()
  {
    return fieldE02LIFDAY.getBigDecimal();
  }

  /**
  * Set field E02LIFPRI using a String value.
  */
  public void setE02LIFPRI(String newvalue)
  {
    fieldE02LIFPRI.setString(newvalue);
  }

  /**
  * Get value of field E02LIFPRI as a String.
  */
  public String getE02LIFPRI()
  {
    return fieldE02LIFPRI.getString();
  }

  /**
  * Set numeric field E02LIFPRI using a BigDecimal value.
  */
  public void setE02LIFPRI(BigDecimal newvalue)
  {
    fieldE02LIFPRI.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02LIFPRI as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02LIFPRI()
  {
    return fieldE02LIFPRI.getBigDecimal();
  }

  /**
  * Set field E02LIFFMM using a String value.
  */
  public void setE02LIFFMM(String newvalue)
  {
    fieldE02LIFFMM.setString(newvalue);
  }

  /**
  * Get value of field E02LIFFMM as a String.
  */
  public String getE02LIFFMM()
  {
    return fieldE02LIFFMM.getString();
  }

  /**
  * Set numeric field E02LIFFMM using a BigDecimal value.
  */
  public void setE02LIFFMM(BigDecimal newvalue)
  {
    fieldE02LIFFMM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02LIFFMM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02LIFFMM()
  {
    return fieldE02LIFFMM.getBigDecimal();
  }

  /**
  * Set field E02LIFFMD using a String value.
  */
  public void setE02LIFFMD(String newvalue)
  {
    fieldE02LIFFMD.setString(newvalue);
  }

  /**
  * Get value of field E02LIFFMD as a String.
  */
  public String getE02LIFFMD()
  {
    return fieldE02LIFFMD.getString();
  }

  /**
  * Set numeric field E02LIFFMD using a BigDecimal value.
  */
  public void setE02LIFFMD(BigDecimal newvalue)
  {
    fieldE02LIFFMD.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02LIFFMD as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02LIFFMD()
  {
    return fieldE02LIFFMD.getBigDecimal();
  }

  /**
  * Set field E02LIFFMY using a String value.
  */
  public void setE02LIFFMY(String newvalue)
  {
    fieldE02LIFFMY.setString(newvalue);
  }

  /**
  * Get value of field E02LIFFMY as a String.
  */
  public String getE02LIFFMY()
  {
    return fieldE02LIFFMY.getString();
  }

  /**
  * Set numeric field E02LIFFMY using a BigDecimal value.
  */
  public void setE02LIFFMY(BigDecimal newvalue)
  {
    fieldE02LIFFMY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02LIFFMY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02LIFFMY()
  {
    return fieldE02LIFFMY.getBigDecimal();
  }

  /**
  * Set field E02LIFLMT using a String value.
  */
  public void setE02LIFLMT(String newvalue)
  {
    fieldE02LIFLMT.setString(newvalue);
  }

  /**
  * Get value of field E02LIFLMT as a String.
  */
  public String getE02LIFLMT()
  {
    return fieldE02LIFLMT.getString();
  }

  /**
  * Set field E02LIFLMU using a String value.
  */
  public void setE02LIFLMU(String newvalue)
  {
    fieldE02LIFLMU.setString(newvalue);
  }

  /**
  * Get value of field E02LIFLMU as a String.
  */
  public String getE02LIFLMU()
  {
    return fieldE02LIFLMU.getString();
  }

}
