<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<title>Informaci�n  B�sica de Garant�as</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="gaMant" class= "datapro.eibs.beans.ERA001101Message"  scope="session"/>
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session"/>
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session"/>
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<% String TablaDepreciar=""; %>

<% if ( !userPO.getPurpose().equals("NEW") ) { %>
<SCRIPT Language="Javascript">
   
    builtNewMenu(colla_M_opt);
    initMenu();

    
</SCRIPT>
<% } %> 
<SCRIPT Language="Javascript">

  builtHPopUp();

  function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }


function ShowIdDepreciar(TablaDepreciar){
	// alert(TablaDepreciar ); 
	if( TablaDepreciar !== "" && TablaDepreciar !== null){
		document.getElementById('DivDepreciar').style.display='block' ;
	}  else {
		document.getElementById('DivDepreciar').style.display='none' ;
	}
}


function UpdateFlag(val) {
  document.forms[0].H01FLGWK2.value = (val==true)?"X":"";
 }
</SCRIPT>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0"); 
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
   
%> 
<h3 align="center">Garant&iacute;as - Informaci&oacute;n B&aacute;sica
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ga_basic.jsp,ERA0011"></h3> 
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSERA0011" >


<%
	if ( !userPO.getPurpose().equals("NEW") ) {
%>
  		<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
<% } else {  %>
   		<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="500">
<% } %>
    
   <input type="hidden" name="E01ROCBNK" value="<%= gaMant.getE01ROCBNK().trim()%>" >               
   <input type="hidden" name="E01ROCBRN" value="<%= gaMant.getE01ROCBRN().trim()%>" >
   <input type="hidden" name="E01ROCGLN" value="<%= gaMant.getE01ROCGLN().trim()%>" >
   <input type="hidden" name="E01ROCTYP" value="<%= gaMant.getE01ROCTYP().trim()%>">
   <input type="hidden" name="E01ROCDCC" value="<%= gaMant.getE01ROCDCC().trim()%>" >
   <input type="hidden" name="E01ROCSTN" value="<%= gaMant.getE01ROCSTN().trim()%>" >
   <input type="hidden" name="DSC" value="" >               
                                        
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark">
            <td nowrap> 
              <div align="right"><b>Cliente : </b></div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <%	if ( userPO.getPurpose().equals("NEW") ) { %>  
                <input type="text" name="E01ROCCUN" size="11" maxlength="9" value="<%= gaMant.getE01ROCCUN().trim()%>">
                <a href="javascript:GetCustomerDescId('E01ROCCUN','E01CUSNA1','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0" ></a>
                <% } else {	%> 
                <input type="text" name="E01ROCCUN" readonly size="11" maxlength="9" value="<%= gaMant.getE01ROCCUN().trim()%>">
              	<% }%>
              </div>
            </td>
            <td nowrap> 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap> 
              <div align="left">
                <input type="text" name="E01CUSNA1" readonly size="62" maxlength="60" value="<%= gaMant.getE01CUSNA1().trim()%>" >
              </div>
            </td>
          </tr>
          <tr id="trdark">
            <td nowrap > 
              <div align="right"><b>No. Referencia : </b></div>
            </td>
            <td nowrap>
             <%	if ( userPO.getPurpose().equals("NEW") ) {	%> 
              <input type="hidden" name="E01ROCREF" size="14" maxlength="12" value="<%= gaMant.getE01ROCREF().trim()%>">
              <input type="text" name="TMPREF" size="14" maxlength="12" value="<% if (gaMant.getE01ROCREF().trim().equals("999999999999")) out.print("NUEVA CUENTA"); else out.print(gaMant.getE01ROCREF().trim()); %>" readonly>
              <% } else { %> 
              <input type="text" name="E01ROCREF" size="14" maxlength="12" value="<%= gaMant.getE01ROCREF().trim()%>" readonly>
              <% } 	%>
            </td> 
            <td nowrap> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap> 
              <div align="left">
               <input type="text" name="E01ROCCCY" size="4" maxlength="3" value="<%= gaMant.getE01ROCCCY().trim()%>" readonly> 
              </div>
            </td>
           </tr>
          <tr id="trdark">             
            <td nowrap> 
              <div align="right"><b>Producto :</b> </div>
            </td>
            <td nowrap> 
              <div align="left">
                <input type="text" name="PRODUCT" size="38" value="<%= gaMant.getE01ROCPRD().trim()%> - <%= gaMant.getE01ROCDPR().trim()%>" readonly>                 
                <input type="hidden" name="E01ROCPRD" size="4" maxlength="4" value="<%= gaMant.getE01ROCPRD().trim()%>" readonly> 
                <input type="hidden" name="E01ROCDPR" size="30" maxlength="30" value="<%= gaMant.getE01ROCDPR().trim()%>" readonly> 
              </div>
            </td>
            <td nowrap> 
              <div align="right"><b>Tipo de Garant�a :</b> </div>
            </td>
            <td nowrap> 
              <div align="left">
                <input type="text" name="TYPE" size="38" value="<%= gaMant.getE01ROCTYP().trim()%> - <%= gaMant.getE01ROCDGT().trim()%>" readonly>                 
                <input type="hidden" name="E01ROCCGT" size="4" maxlength="4" value="<%= gaMant.getE01ROCCGT().trim()%>" readonly> 
                <input type="hidden" name="E01ROCDGT" size="30" maxlength="30" value="<%= gaMant.getE01ROCDGT().trim()%>" readonly> 
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Informaci&oacute;n B&aacute;sica</h4>
      
  <table class="tableinfo" width="100%">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right"> Propietario : </div>
            </td>
            <td nowrap colspan=3 width="100%">
               <table id="headTable" >
    			<tr id="trdark">
    			    <td nowrap align="center" ></td> 
    			    <td nowrap align="center" >C&oacute;digo</td> 
      				<td nowrap align="center" >ID</td>
      				<td nowrap align="center" >Nombre</td>
    			</tr>
    		  </table>
    
    		  <div id="dataDiv" style="height:60; overflow-y :scroll; z-index:0" >
    		    <table id="dataTable" width="100%">
          		<%
  			 		int amount = 10;
 			 		String name;
  			 		for ( int i=1; i<=amount; i++ ){
   				  		if (i<10) name = "0" + i; else name= "" + i;
   				%> 
          		<tr id="trclear"> 
            		<td nowrap>
						<div align="center">
							<%= i %>	
						</div>
					</td>
            		<td nowrap>
						<div align="center"><input type="text"
							name="E01ROWN<%= name %>" size="9" maxlength="9"
							value="<%= gaMant.getField("E01ROWN"+name).getString().trim()%>"
							oncontextmenu="showPopUp(custdescidHelp,'E01ROWN<%= name %>','','','E01RNAM<%= name %>','E01RUTO<%= name %>',''); return false;">
						</div>
						</td>
            		<td nowrap >
						<div align="center"><input type="text"
							name="E01RUTO<%= name %>" size="31" maxlength="30"
							value="<%= gaMant.getField("E01RUTO"+name).getString().trim()%>"
							oncontextmenu="showPopUp(custdescidHelp,'E01ROWN<%= name %>','','','E01RNAM<%= name %>','E01RUTO<%= name %>',''); return false;">
						</div>
						</td>
            		<td nowrap width="250">
						<div align="center"><input type="text" name="E01RNAM<%= name %>"
							size="62" maxlength="60"
							value="<%= gaMant.getField("E01RNAM"+name).getString().trim()%>"
							oncontextmenu="showPopUp(custdescidHelp,'E01ROWN<%= name %>','','','E01RNAM<%= name %>','E01RUTO<%= name %>',''); return false;">
						</div>
					</td>            		            
          		</tr>
          		<%	}	%> 
       			</table>
     		 </div>
            </td>            
          </tr>


          <tr id="trdark">  
            <td nowrap> 
              <div align="right">Fecha Apertura : </div>
            </td>
            <td nowrap >
             <INPUT type="text" name="E01ROCOP1" size=2 maxlength=2 value="<%= gaMant.getE01ROCOP1()%>" readonly>
             <INPUT type="text" name="E01ROCOP2" size=2 maxlength=2 value="<%= gaMant.getE01ROCOP2()%>" readonly>
             <INPUT type="text" name="E01ROCOP3" size=4 maxlength=4 value="<%= gaMant.getE01ROCOP3()%>" readonly>
            </td>
            <td nowrap >
              <div align="right">Fecha Vencimiento : </div>
            </td>
            <td nowrap >
             <INPUT type="text" name="E01ROCMT1" size=2 maxlength=2 value="<%= gaMant.getE01ROCMT1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCMT2" size=2 maxlength=2 value="<%= gaMant.getE01ROCMT2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCMT3" size=4 maxlength=4 value="<%= gaMant.getE01ROCMT3()%>" onKeyPress="enterInteger()">
            </td>
          </tr>

   		  <tr id="trclear"> 
			<td nowrap> 
              <div align="right">
              			  <% if (gaMant.getE01ROCTYP().equals("H")) out.print("Estado de la Garant&iacute;a :"); else out.print("Estado del Bien :"); %> 
               </div>
            </td>
            <td nowrap>            	 
			  <% if (gaMant.getE01ROCTYP().equals("H")) {%>
               <INPUT type="text" name="E01ROCEST" size="5" maxlength="4" value="<%= gaMant.getE01ROCEST().trim()%>">
              <a href="javascript:GetCodeDescCNOFC('E01ROCEST','','1L')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
 			  <%} else { %>
              <INPUT type="text" name="E01ROCEST" size="2" maxlength="1" value="<%= gaMant.getE01ROCEST().trim()%>">
              <A href="javascript:GetCode('E01ROCEST','STATIC_coll_help_benef.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"  ></a>
			  <%}%>
               <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
            <td nowrap> 
              <div align="right">Origen de la Garant�a : </div>
            </td>
            <td nowrap>            	 
              <INPUT type="text" name="E01ROCUC1" size="5" maxlength="4" value="<%= gaMant.getE01ROCUC1().trim()%>">
              <a href="javascript:GetCodeDescCNOFC('E01ROCUC1','','G1')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
			</td>

          </tr>


   		  <tr id="trdark"> 
<% if (!gaMant.getE01ROCTYP().equals("H")) { %>
			<td nowrap> 
              <div align="right">Tabla de Depreciaci�n : </div>
            </td>
            <td nowrap>            	 
              <INPUT type="text" name="E01ROCUC9" size="5" maxlength="4" value="<%= gaMant.getE01ROCUC9().trim()%>" onfocus="ShowIdDepreciar(this.value);" onblur="ShowIdDepreciar(this.value);">
              <a href="javascript:GetCodeDescCNOFC('E01ROCUC9','','G6')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
            </td>
<% } else { %>
			<td nowrap> 
              <div align="right"></div>
            </td>
            <td nowrap>            	 
            </td>
<% }%>

            <td nowrap> 
              <div align="right">Contenido de la Garant�a : </div>
            </td>
            <td nowrap>            	 
              <INPUT type="text" name="E01ROCAYR" size="5" maxlength="4" value="<%= gaMant.getE01ROCAYR().trim()%>">
            </td>
          </tr>

          <tr id="trclear">
            <td nowrap> 
              <div align="right">Porcentaje de Cobertura :</div>
            </td>
            <td nowrap>            	 
	       	  <div align="left"> 
              	<input type="text" name="E01ROCPRS"  size="11" maxlength="7" value="<%= gaMant.getE01ROCPRS().trim()%>" class="TXTRIGHT" onKeypress="enterInteger()">
              </div>
            </td>           

<% if (gaMant.getE01ROCTYP().equals("H")) { %>
			<td nowrap> 
              <div align="right">Tipo Garant&iacute;a :</div>
            </td>
            <td nowrap>            	 
              <INPUT type="text" name="E01ROCUC2" size="5" maxlength="4" value="<%= gaMant.getE01ROCUC2().trim()%>">
              <a href="javascript:GetCodeDescCNOFC('E01ROCUC2','','1K')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
            </td>
<% } else { %>
            <td nowrap> 
              <div align="right"></div>
            </td>
            <td nowrap>            	 
              <div align="left">
              </div>
            </td>
<% }%>

		  </tr>	                  
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Descripci�n :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCSP1" size="72" maxlength="70" value="<%= gaMant.getE01ROCSP1().trim()%>" >
            </td>
          </tr>			
          <tr id="trclear">  
			<td nowrap> 
              <div align="right"> </div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCSP2" size="72" maxlength="70" value="<%= gaMant.getE01ROCSP2().trim()%>" >
            </td>
          </tr>
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right"> </div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCSP3" size="72" maxlength="70" value="<%= gaMant.getE01ROCSP3().trim()%>" >
            </td>
          </tr>			
          <tr id="trclear">  
			<td nowrap> 
              <div align="right"> </div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCSP4" size="72" maxlength="70" value="<%= gaMant.getE01ROCSP4().trim()%>" >
            </td>
          </tr>
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right"> </div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCSP5" size="72" maxlength="70" value="<%= gaMant.getE01ROCSP5().trim()%>" >
            </td>
          </tr>			
         </table>
      </td>
    </tr>
  </table>

<% if (!gaMant.getE01ROCTYP().equals("H")) { %>
  <h4>Ubicaci�n</h4>
      
  <table class="tableinfo" width="100%">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
	    <% System.out.println("Value of E01ROCSTN	="+gaMant.getE01ROCSTN()); %> 
	    <% System.out.println("Value of E01ROCBNK	="+gaMant.getE01ROCBNK()); %> 
    	<% System.out.println("Value of E01ROCBRN	="+gaMant.getE01ROCBRN()); %>     
	    <% System.out.println("Value of E01ROCGLN	="+gaMant.getE01ROCGLN()); %> 
    	<% System.out.println("Value of E01ROCTYP	="+gaMant.getE01ROCTYP()); %>         
    	<% System.out.println("Value of E01ROCDCC	="+gaMant.getE01ROCDCC()); %>             
    
        <%if( gaMant.getH01SCRCOD().equals("07")){%> 
			<tr id="trdark">
            <td nowrap> 
              <div align="right">Calle : </div>
            </td>
            <td nowrap> 
              <input type="text" name="E01NA2"  size="37" maxlength="35" value="<%=gaMant.getE01NA2().trim()%>">
           	</td>
				<TD nowrap align="right">Provincia : </TD>
				<TD nowrap><INPUT type="text" name="D01STE" size="37" maxlength="35"
					value="<%= gaMant.getD01STE().trim()%>"> <A
					href="javascript:GetCodeDescCNOFC('E01STE','D01STE','PV')"> <IMG
					src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda"
					align="bottom" border="0" style="cursor:hand"></A></TD>
			</tr>
          <tr id="trclear">  
            <td nowrap> 
              <div align="right">Residencial/edificio : </div>
            </td>
				<td nowrap><input type="text" name="E01NA3" size="37"
					maxlength="35" value="<%= gaMant.getE01NA3().trim()%>"></td>
				<TD nowrap align="right">Distrito : </TD>
				<TD nowrap><INPUT type="text" name="D01TID" size="37" maxlength="35"
					value="<%= gaMant.getD01TID().trim()%>"> <IMG
					src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda"
					align="bottom" border="0" style="cursor:hand"
					onclick="javascript:GetCodeDescCNOFC('E01TID','D01TID','PI')"></TD>
			</tr>
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">No. Casa/Apto :</div>
            </td>
				<td nowrap><input type="text" name="E01NA4" size="37"
					maxlength="35" value="<%= gaMant.getE01NA4().trim()%>"></td>
				<TD nowrap align="right">Corregimiento : </TD>
				<TD nowrap><INPUT type="text" name="D01PID" size="37" maxlength="35"
					value="<%= gaMant.getD01PID().trim()%>"> <A
					href="javascript:GetCodeDescCNOFC('E01PID','D01PID','PE')"> <IMG
					src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda"
					align="bottom" border="0" style="cursor:hand"></A></TD>
			</tr>
          <tr id="trclear">  
            <td nowrap> 
              <div align="right">Apartado Postal : </div>
            </td>
				<td nowrap><input type="hidden" name="E01STE" size="6"
					maxlength="4" value="<%= gaMant.getE01STE().trim()%>"> <A
					href="javascript:GetCodeDescCNOFC('E01STE','D01STE','PV')"> </A><INPUT
					type="text" name="E01POB" size="11" maxlength="10"
					value="<%= gaMant.getE01POB().trim()%>"></td>
				<TD nowrap align="right">Pa�s : </TD>
				<TD nowrap><INPUT type="text" name="E01CTR" size="21" maxlength="20"
					value="<%= gaMant.getE01CTR().trim()%>"></TD>
			</tr>
          <tr id="trdark">  
            <td nowrap> 
              <div align="right"></div>
            </td>
				<td nowrap ><input type="hidden" name="E01TID" size="6"
					maxlength="4" value="<%= gaMant.getE01TID().trim()%>"> </td>
				<TD nowrap align="right">C�digo Postal : </TD>
				<TD nowrap><%System.out.println( "Cod Postal=" + gaMant.getE01ZPC().trim() ) ; %>
				<INPUT type="hidden" name="E01ZPC" size="17" maxlength="15"
					value='<%= (gaMant.getE01ZPC() != null && !gaMant.getE01ZPC().trim().equals("") && gaMant.getE01ZPC().trim().length() > 4 )
              	?gaMant.getE01ZPC().substring(0,3):""%>'>
				<INPUT type="text" name="D01ZPC" size="17" maxlength="15"
					value='<%= (gaMant.getE01ZPC() != null && !gaMant.getE01ZPC().trim().equals("") && gaMant.getE01ZPC().trim().length() > 5 )
              	?gaMant.getE01ZPC().substring(4):""%>'>
				<IMG src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda"
					align="bottom" border="0" style="cursor:hand"
					onclick="javascript:GetCodeDescCNOFC('E01ZPC','D01ZPC' ,'59')"></TD>
			</tr>

			<%} else {%>
 
            <tr id="trdark"> 
              <td nowrap> 
                <div align="right">Direcci�n Principal :</div>
              </td>
				<td nowrap ><input type="text" name="E01NA2" size="36"
					maxlength="35" value="<%= gaMant.getE01NA2().trim()%>"></td>
				<TD nowrap align="right">Comuna : </TD>
				<TD nowrap><INPUT type="text" name="E01CTY" size="31" maxlength="30"
					value="<%= gaMant.getE01CTY().trim()%>"></TD>
			</tr>
          <tr id="trclear">  
              <td nowrap> 
                <div align="right">N&uacute;mero :</div>
              </td>
				<td nowrap ><input type="text" name="E01NA3" size="36"
					maxlength="35" value="<%= gaMant.getE01NA3().trim()%>"></td>
				<TD nowrap align="right">Regi&oacute;n : </TD>
				<TD nowrap><INPUT type="text" name="E01STE" size="5" maxlength="4"
					value="<%= gaMant.getE01STE().trim()%>"> <A
					href="javascript:GetCodeCNOFC('E01STE','27')"> <IMG
					src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda"
					align="bottom" border="0"></A></TD>
			</tr>
            <tr id="trdark"> 
              <td nowrap> 
                <div align="right">N&deg; Depto. / Estac. / Bod. :</div>
              </td>
				<td nowrap ><input type="text" name="E01NA4" size="36"
					maxlength="35" value="<%= gaMant.getE01NA4().trim()%>"></td>
				<TD nowrap align="right">Rol Estacionamiento : </TD>
				<TD nowrap><INPUT type="text" name="E01CTR" size="21"
					maxlength="20" value="<%= gaMant.getE01CTR().trim()%>"></TD>
			</tr>
          <tr id="trclear">  
              <td nowrap> 
                <div align="right">Rol Matriz : </div>
              </td>
				<td nowrap><INPUT type="text" name="E01POB" size="11"
					maxlength="10" value="<%= gaMant.getE01POB().trim()%>"></td>
				<TD nowrap align="right">Rol Bodega : </TD>
				<TD nowrap><INPUT type="text" name="E01ZPC" size="16"
					maxlength="15" value="<%= gaMant.getE01ZPC().trim()%>"></TD>
			</tr>

         <%} %>   
         </table>
      </td>
    </tr>
  </table>
<% }%>

<%-- A. BIENES MUEBLES --%>

<% System.out.println("Bloque Detalle E01ROCTYP="+gaMant.getE01ROCTYP()); %> 
<% if (gaMant.getE01ROCTYP().equals("A")) { %>

  <h4>Detalle de Garant�a</h4>

    <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">          

   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Valor Inicial :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCAM1" size="17" maxlength="17" value="<%= gaMant.getE01ROCAM1().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
			<td nowrap> 
              <div align="right">Valor Actual :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCFAA" size="17" maxlength="17" value="<%= gaMant.getE01ROCFAA().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
          </tr>
   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">N�mero de P�liza :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCLOC" size="37" maxlength="35" value="<%= gaMant.getE01ROCLOC().trim()%>" >
            </td>
			<td nowrap> 
              <div align="right">Valor de Cobertura :</div>
            </td>
            <td nowrap colspan="3">            	 
	            <INPUT type="text" name="E01ROCAM2" size="17" maxlength="17" value="<%= gaMant.getE01ROCAM2().trim()%>" onkeypress="enterDecimal()">
            </td>
          </tr>
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Fecha Renovaci�n P�liza :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCPDD" size=2 maxlength=2 value="<%= gaMant.getE01ROCPDD()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCPDM" size=2 maxlength=2 value="<%= gaMant.getE01ROCPDM()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCPDY" size=4 maxlength=4 value="<%= gaMant.getE01ROCPDY()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Aseguradora :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCAIN" size="5" maxlength="4" value="<%= gaMant.getE01ROCAIN().trim()%>" >
              <a href="javascript:GetBroker('E01ROCAIN')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="middle" border="0"></a> 
            </td>
          </tr>

   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Fecha de Aval�o :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCAP1" size=2 maxlength=2 value="<%= gaMant.getE01ROCAP1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCAP2" size=2 maxlength=2 value="<%= gaMant.getE01ROCAP2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCAP3" size=4 maxlength=4 value="<%= gaMant.getE01ROCAP3()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Fiduciaria :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCU10" size="5" maxlength="3" value="<%= gaMant.getE01ROCU10().trim()%>" >
              <a href="javascript:GetBroker('E01ROCU10')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="middle" border="0"></a> 
            </td>
          </tr>
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Fecha Adquisici�n :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCID1" size=2 maxlength=2 value="<%= gaMant.getE01ROCID1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCID2" size=2 maxlength=2 value="<%= gaMant.getE01ROCID2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCID3" size=4 maxlength=4 value="<%= gaMant.getE01ROCID3()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Identificaci�n Fideicomiso :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCRF4" size=32 maxlength=30 value="<%= gaMant.getE01ROCRF4()%>">
            </td>
          </tr>
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">N�mero de Serie :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCRF2" size=22 maxlength=20 value="<%= gaMant.getE01ROCRF2()%>">
            </td>
			<td nowrap> 
              <div align="right">Modelo :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCRF3" size=12 maxlength=10 value="<%= gaMant.getE01ROCRF3()%>">
            </td>
          </tr>
   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Marca :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCNPH" size=17 maxlength=15 value="<%= gaMant.getE01ROCNPH()%>">
            </td>
			<td nowrap> 
              <div align="right">Vencimiento en :</div>
            </td>
            <td nowrap colspan="2">            	 
	         <INPUT type="text" name="E01ROCYPH" size="5" maxlength="4" value="<%= gaMant.getE01ROCYPH().trim()%>" readonly>
            </td>
			<td>
              <div align="left">d�as</div>
            </td>
          </tr>

        </table>          
      </td> 
    </tr> 
    </table>
<% } %> 


<%-- B. BIENES INMUEBLES --%>

<% System.out.println("Bloque Detalle E01ROCTYP="+gaMant.getE01ROCTYP()); %> 
<% if (gaMant.getE01ROCTYP().equals("B")) { %>

  <h4>Detalle de Garant�a</h4>

    <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">          
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Urbano=1 Rural=2 :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCNUN" size="11" maxlength="9" value="<%= gaMant.getE01ROCNUN().trim()%>" onkeypress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Superficie Edificada :</div>
            </td>
            <td nowrap>            	 
            <INPUT type="text" name="E01ROCSPR" size="10" maxlength="8" value="<%= gaMant.getE01ROCSPR().trim()%>" onkeypress="enterDecimal()">
            </td>
			<td nowrap> 
              <div align="right">Superficie Terreno :</div>
            </td>
            <td nowrap>            	 
            <INPUT type="text" name="E01ROCTRN" size="10" maxlength="8" value="<%= gaMant.getE01ROCTRN().trim()%>" onkeypress="enterDecimal()">
            </td>
          </tr>
   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Inscripci�n Registro P�blico :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCLOC" size="62" maxlength="60" value="<%= gaMant.getE01ROCLOC().trim()%>" >
            </td>
			<td nowrap> 
              <div align="right">Uf/m2 Edificada :</div>
            </td>
            <td nowrap>            	 
        	    <INPUT type="text" name="E01ROCFPH" size="10" maxlength="8" value="<%= gaMant.getE01ROCFPH().trim()%>" onkeypress="enterDecimal()">
            </td>
			<td nowrap> 
              <div align="right">Uf/m2 Terreno :</div>
            </td>
            <td nowrap>            	 
    	        <INPUT type="text" name="E01ROCNPH" size="10" maxlength="8" value="<%= gaMant.getE01ROCNPH().trim()%>" onkeypress="enterDecimal()">
            </td>
          </tr>
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Programa :</div>
            </td>
            <td nowrap colspan="3">            	 
	            <INPUT type="text" name="E01ROCCPH" size="10" maxlength="8" value="<%= gaMant.getE01ROCCPH().trim()%>" onkeypress="enterInteger()">            </td>
			<td nowrap> 
              <div align="right">Tipo de Vivienda :</div>
            </td>
            <td nowrap colspan="3">            	 
              <INPUT type="text" name="E01ROCUC2" size="5" maxlength="4" value="<%= gaMant.getE01ROCUC2().trim()%>">
              <a href="javascript:GetCodeDescCNOFC('E01ROCUC2','','G2')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
            </td>
          </tr>
   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Valor Comercial :</div>
            </td>
            <td nowrap colspan="3">            	 
	            <INPUT type="text" name="E01ROCAM1" size="17" maxlength="17" value="<%= gaMant.getE01ROCAM1().trim()%>" onkeypress="enterDecimal()">
            </td>
			<td nowrap> 
              <div align="right">Notario :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCU10" size="5" maxlength="4" value="<%= gaMant.getE01ROCU10().trim()%>" >
              <a href="javascript:GetBroker('E01ROCU10')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="middle" border="0"></a> 
            </td>
          </tr>
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Valor Liquidaci&oacute;n :</div>
            </td>
            <td nowrap colspan="3">            	 
	            <INPUT type="text" name="E01ROCAM2" size="17" maxlength="17" value="<%= gaMant.getE01ROCAM2().trim()%>" onkeypress="enterDecimal()">            </td>
			<td nowrap> 
              <div align="right">Fecha Tasaci&oacute;n Inicial :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCAP1" size=2 maxlength=2 value="<%= gaMant.getE01ROCAP1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCAP2" size=2 maxlength=2 value="<%= gaMant.getE01ROCAP2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCAP3" size=4 maxlength=4 value="<%= gaMant.getE01ROCAP3()%>" onKeyPress="enterInteger()">
             </td>
          </tr>
   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Valor Contable :</div>
            </td>
            <td nowrap colspan="3">            	 
	            <INPUT type="text" name="E01ROCFAA" size="17" maxlength="17" value="<%= gaMant.getE01ROCFAA().trim()%>" onkeypress="enterDecimal()">
           </td>
			<td nowrap> 
              <div align="right">Fecha &Uacute;ltima Tasaci&oacute;n  :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCST1" size=2 maxlength=2 value="<%= gaMant.getE01ROCST1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCST2" size=2 maxlength=2 value="<%= gaMant.getE01ROCST2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCST3" size=4 maxlength=4 value="<%= gaMant.getE01ROCST3()%>" onKeyPress="enterInteger()">
            </td>
          </tr>
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Tipo de Construcci&oacute;n :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCUC3" size="5" maxlength="4" value="<%= gaMant.getE01ROCUC3().trim()%>" >
              <a href="javascript:GetCodeDescCNOFC('E01ROCUC3','','2N')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
            </td>
			<td nowrap> 
              <div align="right">Fecha Recepci&oacute;n Municipal :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCID1" size=2 maxlength=2 value="<%= gaMant.getE01ROCID1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCID2" size=2 maxlength=2 value="<%= gaMant.getE01ROCID2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCID3" size=4 maxlength=4 value="<%= gaMant.getE01ROCID3()%>" onKeyPress="enterInteger()">
             </td>
          </tr>
   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Condici�n de la Hipoteca :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCNC1" size=4 maxlength=2 value="<%= gaMant.getE01ROCNC1()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Georeferencia :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCRF4" size=47 maxlength=45 value="<%= gaMant.getE01ROCRF4()%>">
            </td>
          </tr>
   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Fecha de Escritura :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCNDD" size=2 maxlength=2 value="<%= gaMant.getE01ROCNDD()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCNDM" size=2 maxlength=2 value="<%= gaMant.getE01ROCNDM()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCNDY" size=4 maxlength=4 value="<%= gaMant.getE01ROCNDY()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Subsidio :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCRF5" size=10 maxlength=8 value="<%= gaMant.getE01ROCRF5()%>" onKeyPress="enterInteger()">
           </td>
          </tr>

   		  <tr id="trclear"> 
			<td nowrap> 
              <div align="right">Fecha de Vencimiento de Tasaci&oacute;n</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCRE1" size=2 maxlength=2 value="<%= gaMant.getE01ROCRE1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCRE2" size=2 maxlength=2 value="<%= gaMant.getE01ROCRE2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCRE3" size=4 maxlength=4 value="<%= gaMant.getE01ROCRE3()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Vencimiento en :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCYPH" size="5" maxlength="4" value="<%= gaMant.getE01ROCYPH().trim()%>" onkeypress="enterInteger()">  d�as
           </td>
          </tr>

        </table>          
      </td> 
    </tr> 
    </table>
<% } %> 

<%-- C. DEPOSITOS EN EL BANCO/OTROS BANCOS --%>

<% System.out.println("Bloque Detalle E01ROCTYP="+gaMant.getE01ROCTYP()); %> 
<% if (gaMant.getE01ROCTYP().equals("C")) { %>

  <h4>Detalle de Garant�a</h4>

    <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">          
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Valor Inicial :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCFAA" size="17" maxlength="17" value="<%= gaMant.getE01ROCFAA().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
			<td nowrap> 
              <div align="right">Valor de lo Pignorado :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCAM1" size="17" maxlength="17" value="<%= gaMant.getE01ROCAM1().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
          </tr>
   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Fecha de Renovaci�n del Plazo :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCAP1" size=2 maxlength=2 value="<%= gaMant.getE01ROCAP1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCAP2" size=2 maxlength=2 value="<%= gaMant.getE01ROCAP2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCAP3" size=4 maxlength=4 value="<%= gaMant.getE01ROCAP3()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Fiduciaria :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCU10" size="5" maxlength="4" value="<%= gaMant.getE01ROCU10().trim()%>" >
              <a href="javascript:GetBroker('E01ROCU10')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="middle" border="0"></a> 
            </td>
          </tr>
   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Fecha de Retenci�n :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCNDM" size=2 maxlength=2 value="<%= gaMant.getE01ROCNDM()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCNDD" size=2 maxlength=2 value="<%= gaMant.getE01ROCNDD()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCNDY" size=4 maxlength=4 value="<%= gaMant.getE01ROCNDY()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Identificaci�n Fideicomiso :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCRF4" size=32 maxlength=30 value="<%= gaMant.getE01ROCRF4()%>">
            </td>
          </tr>
   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">N�mero de la Garant�a o Contrato :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCLOC" size="37" maxlength="35" value="<%= gaMant.getE01ROCLOC().trim()%>" >
            </td>
			<td nowrap> 
              <div align="right">C�digo de Banco :</div>
            </td>
            <td nowrap colspan="3">            	 
              <INPUT type="text" name="E01ROCUC2" size="5" maxlength="4" value="<%= gaMant.getE01ROCUC2().trim()%>">
              <a href="javascript:GetCodeDescCNOFC('E01ROCUC2','','G4')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
            </td>
          </tr>
   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Vencimiento en :</div>
            </td>
            <td nowrap colspan="2">            	 
            <INPUT type="text" name="E01ROCYPH" size="5" maxlength="4" value="<%= gaMant.getE01ROCYPH().trim()%>" onkeypress="enterInteger()">
            </td>
            <td nowrap>            	 
              <div align="left">d�as</div>
            </td>
			<td nowrap> 
              <div align="right"></div>
            </td>
            <td nowrap colspan="3">            	 
            </td>
          </tr>

        </table>          
      </td> 
    </tr> 
    </table>
<% } %> 

<%-- D. GARANTIAS PRENDARIAS --%>

<% System.out.println("Bloque Detalle E01ROCTYP="+gaMant.getE01ROCTYP()); %> 
<% if (gaMant.getE01ROCTYP().equals("D")) { %>

  <h4>Detalle de Garant�a</h4>

    <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">          
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Valor Inicial :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCAM1" size="17" maxlength="17" value="<%= gaMant.getE01ROCAM1().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
			<td nowrap> 
              <div align="right">Valor de Mercado :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCFAA" size="17" maxlength="17" value="<%= gaMant.getE01ROCFAA().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
          </tr>
   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Fecha Ultima Cotizaci�n del Instrumento :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCAP1" size=2 maxlength=2 value="<%= gaMant.getE01ROCAP1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCAP2" size=2 maxlength=2 value="<%= gaMant.getE01ROCAP2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCAP3" size=4 maxlength=4 value="<%= gaMant.getE01ROCAP3()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Fiduciaria :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCU10" size="5" maxlength="3" value="<%= gaMant.getE01ROCU10().trim()%>" >
              <a href="javascript:GetBroker('E01ROCU10')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="middle" border="0"></a> 
            </td>
          </tr>
   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">C�digo Emisor, Custodio o Depositario :</div>
            </td>
            <td nowrap colspan="3">            	 
              <INPUT type="text" name="E01ROCUC2" size="5" maxlength="4" value="<%= gaMant.getE01ROCUC2().trim()%>">
              <a href="javascript:GetCodeDescCNOFC('E01ROCUC2','','G4')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
            </td>
			<td nowrap> 
              <div align="right">Identificaci�n Fideicomiso :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCRF4" size=32 maxlength=30 value="<%= gaMant.getE01ROCRF4()%>">
            </td>
          </tr>
   		  <tr id="trclear"> 
			<td nowrap> 
              <div align="right">Pa�s de Emisi�n :</div>
            </td>
            <td nowrap colspan="3">            	 
				<INPUT type="text" name="E01ROCGRK" size="5" maxlength="4" value="<%= gaMant.getE01ROCGRK().trim()%>">
              <a href="javascript:GetCodeCNOFC('E01ROCGRK','15')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"  ></a> 
            </td>
			<td nowrap> 
              <div align="right">Cantidad :</div>
            </td>
            <td nowrap colspan="3">            	 
	            <INPUT type="text" name="E01ROCNUN" size="11" maxlength="9" value="<%= gaMant.getE01ROCNUN().trim()%>" onkeypress="enterInteger()">
            </td>
          </tr>
   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Calificaci�n de la Emisi�n :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCNC1" size=4 maxlength=2 value="<%= gaMant.getE01ROCNC1()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Calificaci�n del Emisor :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCNC2" size=4 maxlength=2 value="<%= gaMant.getE01ROCNC2()%>" onKeyPress="enterInteger()">
            </td>
          </tr>
   		  <tr id="trclear"> 
			<td nowrap> 
              <div align="right">Tipo de Instrumento Financiero :</div>
            </td>
            <td nowrap colspan="3">            	 
              <INPUT type="text" name="E01ROCUC3" size="5" maxlength="4" value="<%= gaMant.getE01ROCUC3().trim()%>">
              <a href="javascript:GetCodeDescCNOFC('E01ROCUC3','','G5')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
            </td>
			<td nowrap> 
              <div align="right">Instr.Financi/Prenda Agraria O Ganadera :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCLOC" size="37" maxlength="35" value="<%= gaMant.getE01ROCLOC().trim()%>" >
            </td>
          </tr>
   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Vencimiento en :</div>
            </td>
            <td nowrap colspan="2">            	 
            <INPUT type="text" name="E01ROCYPH" size="5" maxlength="4" value="<%= gaMant.getE01ROCYPH().trim()%>" onkeypress="enterInteger()">
            </td>
            <td nowrap>            	 
              <div align="left">d�as</div>
            </td>
			<td nowrap> 
              <div align="right"></div>
            </td>
            <td nowrap colspan="3">            	 
            </td>
          </tr>

        </table>          
      </td> 
    </tr> 
    </table>
<% } %> 

<%-- E. OTROS TIPOS DE GARANTIA --%>

<% System.out.println("Bloque Detalle E01ROCTYP="+gaMant.getE01ROCTYP()); %> 
<% if (gaMant.getE01ROCTYP().equals("E")) { %>

  <h4>Detalle de Garant�a</h4>

    <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">          
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Valor Inicial :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCAM1" size="17" maxlength="17" value="<%= gaMant.getE01ROCAM1().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
			<td nowrap> 
              <div align="right">Valor en Libro :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCFAA" size="17" maxlength="17" value="<%= gaMant.getE01ROCFAA().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
          </tr>
   		  <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Fecha Renovaci�n Carta de Cr�dito o Avales/Fianzas :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCAP1" size=2 maxlength=2 value="<%= gaMant.getE01ROCAP1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCAP2" size=2 maxlength=2 value="<%= gaMant.getE01ROCAP2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCAP3" size=4 maxlength=4 value="<%= gaMant.getE01ROCAP3()%>" onKeyPress="enterInteger()">
            </td>
			<td nowrap> 
              <div align="right">Fecha de Emisi�n :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCID1" size=2 maxlength=2 value="<%= gaMant.getE01ROCID1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCID2" size=2 maxlength=2 value="<%= gaMant.getE01ROCID2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCID3" size=4 maxlength=4 value="<%= gaMant.getE01ROCID3()%>" onKeyPress="enterInteger()">
            </td>
          </tr>
   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Fiduciaria :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCU10" size="5" maxlength="3" value="<%= gaMant.getE01ROCU10().trim()%>" >
              <a href="javascript:GetBroker('E01ROCU10')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="middle" border="0"></a> 
            </td>
			<td nowrap> 
              <div align="right">Identificaci�n Fideicomiso :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCRF4" size=32 maxlength=30 value="<%= gaMant.getE01ROCRF4()%>">
            </td>
          </tr>
   		  <tr id="trclear"> 
			<td nowrap> 
              <div align="right">N�mero C/C o Cesiones Sobre Pagar�s/Cartera :</div>
            </td>
            <td nowrap colspan="3">            	 
              <input type="text" name="E01ROCLOC" size="37" maxlength="35" value="<%= gaMant.getE01ROCLOC().trim()%>" >
            </td>
			<td nowrap> 
              <div align="right">C�digo Emisor, Custodio, Depositario :</div>
            </td>
            <td nowrap colspan="3">            	 
              <INPUT type="text" name="E01ROCUC2" size="5" maxlength="4" value="<%= gaMant.getE01ROCUC2().trim()%>">
              <a href="javascript:GetCodeDescCNOFC('E01ROCUC2','','G6')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
            </td>
          </tr>
   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Vencimiento en :</div>
            </td>
            <td nowrap colspan="2">            	 
            <INPUT type="text" name="E01ROCYPH" size="5" maxlength="4" value="<%= gaMant.getE01ROCYPH().trim()%>" onkeypress="enterInteger()">
            </td>
            <td nowrap>            	 
              <div align="left">d�as</div>
            </td>
			<td nowrap> 
              <div align="right"></div>
            </td>
            <td nowrap colspan="3">            	 
            </td>
          </tr>

        </table>          
      </td> 
    </tr> 
    </table>
<% } %> 

<%-- F. AVALES --%>

<% System.out.println("Bloque Detalle E01ROCTYP="+gaMant.getE01ROCTYP()); %> 
<% if (gaMant.getE01ROCTYP().equals("F")) { %>

  <h4>Detalle de Garant�a</h4>

    <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">          
   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Fecha de Emisi�n :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCID1" size=2 maxlength=2 value="<%= gaMant.getE01ROCID1()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCID2" size=2 maxlength=2 value="<%= gaMant.getE01ROCID2()%>" onKeyPress="enterInteger()">
             <INPUT type="text" name="E01ROCID3" size=4 maxlength=4 value="<%= gaMant.getE01ROCID3()%>" onKeyPress="enterInteger()">
            </td>
            <td nowrap> 
              <div align="right">Monto del Aval :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCAM1" size="17" maxlength="17" value="<%= gaMant.getE01ROCAM1().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
          </tr>
   		  <tr id="trclear"> 
			<td nowrap> 
              <div align="right">Saldo :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCAM2" size="17" maxlength="17" value="<%= gaMant.getE01ROCAM2().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
			<td nowrap> 
              <div align="right">Tabla de Provisi�n :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCNC1" size=4 maxlength=2 value="<%= gaMant.getE01ROCNC1()%>">
              <a href="javascript:GetPrevTable('E01ROCNC1')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0" ></a> 
            </td>
          </tr>
   		  <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Monto de la Garant�a :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCFAA" size="17" maxlength="17" value="<%= gaMant.getE01ROCFAA().trim()%>" onkeypress="enterDecimal()">
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
			<td nowrap> 
              <div align="right">Provisi�n Establecida :</div>
            </td>
            <td nowrap colspan="3">            	 
            <INPUT type="text" name="E01ROCINA" size="17" maxlength="17" value="<%= gaMant.getE01ROCINA().trim()%>" onkeypress="enterDecimal()">
            </td>
          </tr>
   		  <tr id="trclear"> 
			<td nowrap> 
              <div align="right">Vencimiento en :</div>
            </td>
            <td nowrap colspan="2">            	 
            <INPUT type="text" name="E01ROCYPH" size="5" maxlength="4" value="<%= gaMant.getE01ROCYPH().trim()%>" readonly>
            </td>
            <td nowrap>            	 
              <div align="left">d�as</div>
            </td>
			<td nowrap> 
              <div align="right">Clasificaci�n de Riesgo :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCFL1" size=4 maxlength=2 value="<%= gaMant.getE01ROCFL1()%>" onKeyPress="enterInteger()">
            </td>
          </tr>

        </table>          
      </td> 
    </tr> 
    </table>
<% } %>

<% if (gaMant.getE01ROCTYP().equals("H")) { %>

  <h4>Detalle de Garant�a</h4>

    <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">          

   		  <tr id="trdark"> 
   		    <td nowrap> 
              <div align="right">Valor Actual :</div>
            </td>
            <td nowrap >            	 
              <INPUT type="text" name="E01ROCCOP" size="17" maxlength="17" value="<%= gaMant.getE01ROCCOP().trim()%>" onkeypress="enterDecimal()">
               <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
            <td nowrap> 
            </td>
            <td nowrap >            	 
            </td>
          </tr>
          
      </table>          
      </td> 
    </tr> 
    </table>
<% } %>

<% if (gaMant.getE01ROCTYP().equals("I")) { %>

  <h4>Detalle de Garant�a</h4>

    <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">          

   		  <tr id="trdark"> 
   		    <td nowrap> 
              <div align="right">Valor Actual :</div>
            </td>
            <td nowrap>            	 
              <INPUT type="text" name="E01ROCFAA" size="17" maxlength="17" value="<%= gaMant.getE01ROCFAA().trim()%>" onkeypress="enterDecimal()">
               <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0" width="16" height="20"  > 
            </td>
            <td nowrap> 
            </td>
            <td nowrap>            	 
            </td>
          </tr>
          
      </table>          
      </td> 
    </tr> 
    </table>
<% } %>


<%	if ( !userPO.getPurpose().equals("NEW") ) { %>
<div id="DivDepreciar" style="position:relative; display:none;">

<h4>Informaci�n Sobre Depreciaci�n</h4>
    <table class="tableinfo">
      <tr > 
        <td >
          <table cellspacing="0" cellpadding="2" width="100%" border="0" >

   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Valor Original :</div>
            </td>
            <td nowrap colspan="3">            	 
               <input type="text" name="E01ROCAM1" readonly size="17" maxlength="15" value="<%= gaMant.getE01ROCAM1().trim()%>">
            </td>
            <td nowrap> 
              <div align="right">Valor a Depreciar :</div>
            </td>
            <td nowrap colspan="3">            	 
	           <INPUT type="text" name="E01ROCCOP" readonly size="17" maxlength="17" value="<%= gaMant.getE01ROCCOP().trim()%>">
            </td>
          </tr>

   		  <tr id="trclear"> 
			<td nowrap> 
              <div align="right">Fecha de Adquisici�n :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCID1" readonly size=2 maxlength=2 value="<%= gaMant.getE01ROCID1()%>" >
             <INPUT type="text" name="E01ROCID2" readonly size=2 maxlength=2 value="<%= gaMant.getE01ROCID2()%>" >
             <INPUT type="text" name="E01ROCID3" readonly size=4 maxlength=4 value="<%= gaMant.getE01ROCID3()%>" >
            </td>
            <td nowrap> 
              <div align="right">Fecha Ultima Depreciaci�n :</div>
            </td>
            <td nowrap colspan="3">            	 
             <INPUT type="text" name="E01ROCNX1" readonly size=2 maxlength=2 value="<%= gaMant.getE01ROCNX1()%>">
             <INPUT type="text" name="E01ROCNX2" readonly size=2 maxlength=2 value="<%= gaMant.getE01ROCNX2()%>">
             <INPUT type="text" name="E01ROCNX3" readonly size=4 maxlength=4 value="<%= gaMant.getE01ROCNX3()%>">
            </td>
          </tr>


   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Tiempo Transcurrido :</div>
            </td>
            <td nowrap colspan="3">            	 
               <input type="text" id="txtright" readonly name="E01ROCTCU" size="8" maxlength="7" value="<%= gaMant.getE01ROCTCU().trim()%>">            </td>
            <td nowrap> 
              <div align="right">Meses a Depreciar :</div>
            </td>
            <td nowrap colspan="3">            	 
	           <INPUT type="text" name="E01ROCCYC" readonly size="4" maxlength="3" value="<%= gaMant.getE01ROCCYC().trim()%>">
            </td>
          </tr>


   		  <tr id="trclear"> 
			<td nowrap> 
              <div align="right">Tabla/Porcentaje de Depreciaci�n :</div>
            </td>
            <td nowrap colspan="3">            	 
                <INPUT type="text" name="E011ROCUC9" readonly size="5" maxlength="4" value="<%= gaMant.getE01ROCUC9().trim()%>" >
           		<input type="text" name="E011ROCRAT" readonly size="6" maxlength="5" value="<%= gaMant.getE01ROCRAT().trim()%>">
            <td nowrap> 
              <div align="right">Depreciaci�n Acumulada :</div>
            </td>
            <td nowrap colspan="3">            	 
	           <INPUT type="text" name="E01ROCINA" size="17" readonly maxlength="17" value="<%= gaMant.getE01ROCINA().trim()%>">
            </td>
          </tr>


   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right">Valor  Garant�a :</div>
            </td>
            <td nowrap colspan="3">            	 
           		<input type="text" name="E01ROCFAA" readonly size="17" maxlength="15" value="<%= gaMant.getE01ROCFAA().trim()%>">
			<% if (gaMant.getE01ROCTYP().equals("B")) { %>

            <td nowrap> 
              <div align="right">Valor Mejoras :</div>
            </td>
            <td nowrap colspan="3">            	 
	           <INPUT type="text" name="E01ROCAM2" readonly size="17" maxlength="17" value="<%= gaMant.getE01ROCAM2().trim()%>">
            </td>

			<% } else {%>

            <td nowrap> 
              <div align="right"> </div>
            </td>
            <td nowrap colspan="3">            	 
              <div align="right"> </div>
            </td>

			<% } %>
          </tr>


   		  <tr id="trclear"> 
			<td nowrap> 
              <div align="right"> </div>
            </td>
            <td nowrap colspan="3">            	 
              <div align="right"> </div>
            <td nowrap> 
              <div align="right"> </div>
            </td>
            <td nowrap colspan="3">            	 
              <div align="right"> </div>
            </td>
          </tr>


   		  <tr id="trdark"> 
			<td nowrap> 
              <div align="right"> </div>
            </td>
            <td nowrap colspan="3">            	 
              <div align="right"><b>Efectuar c�lculo y registro Contable : </b/></div>
            <td nowrap> 
              <input type="checkbox" name="RECALC" value="" onClick="UpdateFlag(this.checked)" <% if (gaMant.getH01FLGWK2().trim().equals("X")) out.print(" checked disabled");%>>
            </td>
            <td nowrap colspan="3">            	 
              <div align="right"> </div>
            </td>
          </tr>



          </table>
        </td>
      </tr>
    </table>
</div>
<% } %>


 
  <p align=center>
  	<input class="EIBSBTN" id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
  <SCRIPT Language="Javascript">
	
   function waitSimulate() {
     document.body.style.cursor = "wait";
   }
   document.forms[0].onsubmit= waitSimulate;

  function tableresize() {
     adjustEquTables(headTable,dataTable,dataDiv,0,false);
     <% if (gaMant.getE01ROCTYP().equals("05")) { %>
     	adjustEquTables(headTable1,dataTable1,dataDiv1,0,false);
     <%    } %>
   }
  tableresize();
  window.onresize=tableresize;

  ShowIdDepreciar("");

  </SCRIPT> 
  </FORM>
 </BODY>
 </html>