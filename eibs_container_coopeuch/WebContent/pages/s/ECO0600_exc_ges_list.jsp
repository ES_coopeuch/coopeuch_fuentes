<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page
	import="datapro.eibs.master.Util,datapro.eibs.beans.ECO060001Message"%>
<%@page import="com.datapro.constants.EibsFields"%>

<html>
<head>
<title>Gesti&oacute;n de Aprobaci&oacute;n Masiva de Ex-Convenios</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="ExCon" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"   scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"    	   scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

$(function(){
$("#radio_key").attr("checked", false);
});

function goAction(op) {
var ok = false;

   		if (op =='1100' || op == '1200')
   		{
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}
 		else 
   		{
 			
			//VERIFICA UN REGISTRO SELECCIONADO
			for(n=0; n<document.forms[0].elements.length; n++)
			{
				var element = document.forms[0].elements[n];
		   	 if(element.name == "ID_Interfaz") 
		   	 {	
		   	 	if (element.checked == true) 
		   	 	{
		   	   		document.getElementById("codigo_lista").value = element.value; 
   	    	 		ok = true;
   	    	 		break;
				}
		 	 }
			}

			//SE SELECCIONO UN GEGISTRO      
    		if ( ok ) 
    		{
				var confirm1 = true;
      		
     		 	if (op =='1500')
      			{
      				confirm1 = confirm("�Desea Eliminar el Archivo Seleccionado?");
      			
					if (confirm1){
						document.forms[0].SCREEN.value = op;
						document.forms[0].submit();		
					}
				}
 				else
				{
					document.forms[0].SCREEN.value = op;
					document.forms[0].submit();		
				} 
     		} 
     		else 
     		{
				alert("Debe seleccionar un registro para continuar.");	   
	 		}    
		}
	}

</SCRIPT>

</head>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>



<H3 align="center">Gesti&oacute;n de Aprobaci&oacute;n Masiva de Ex-Convenios
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="exc_ges_list.jsp, ECO0600">
</H3>

<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0600" target="main">
<input type="hidden" name="SCREEN" value="1200"> 
<input type="hidden" name="codigo_lista" value="" id="codigo_lista"> 
<input type="hidden" name="E01CMHSTS" value="<%=request.getAttribute("E01CMHSTS")%>">
<input type="hidden" name="E01CMHFDD" value="<%=request.getAttribute("E01CMHFDD")%>"> 
<input type="hidden" name="E01CMHFDM" value="<%=request.getAttribute("E01CMHFDM")%>"> 
<input type="hidden" name="E01CMHFDY" value="<%=request.getAttribute("E01CMHFDY")%>"> 
<input type="hidden" name="E01CMHFHD" value="<%=request.getAttribute("E01CMHFHD")%>"> 
<input type="hidden" name="E01CMHFHM" value="<%=request.getAttribute("E01CMHFHM")%>"> 
<input type="hidden" name="E01CMHFHY" value="<%=request.getAttribute("E01CMHFHY")%>"> 
<input type="hidden" name="E01CMHIDC" value="<%=request.getAttribute("E01CMHIDC")%>">
<input type="hidden" name="E01CMHREC" value="<%=request.getAttribute("E01CMHREC")%>">

<h3 align="center">FILTROS DE LA CONSULTA</h3>
<table class="tableinfo" width="100%">
	<tr bordercolor="#FFFFFF">
		<td id="trdark">
		<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
			<tr>
				<td nowrap width="10%" align="right">ID Archivo :</td>
				<td nowrap width="20%" align="left"><input type="text"
					name="E01CCHIDC" size="15" maxlength="14"
					value="<%out.print(request.getAttribute("E01CMHIDC"));%>" readonly>
				</td>

				<td nowrap width="15%" align="right">Fecha Carga Desde :</td>
				<td nowrap width="20%" align="left"><input type="text"
					name="E01fecha" size="12"
					value="<%out.print(session.getAttribute("fecha_inicial"));%>"
					readonly></td>

				<td nowrap width="15%" align="right">Fecha Carga Hasta :</td>
				<td nowrap width="20%" align="left"><input type="text"
					name="E01fecha2" size="12"
					value="<%out.print(session.getAttribute("fecha_final"));%>"
					readonly></td>
			</tr>

			<tr>
				<td nowrap width="10%" align="right">Estado :</td>
				<td nowrap width="20%" align="left"><input type="text"
					name="Estado" size="23" maxlength="20"
					value="<%if (request.getAttribute("E01CMHSTS").equals("T"))
				out.print("TODOS");
			else if (request.getAttribute("E01CMHSTS").equals("C"))
				out.print("CARGADO");
			else if (request.getAttribute("E01CMHSTS").equals("A"))
				out.print("PENDIENTE");
			else if (request.getAttribute("E01CMHSTS").equals("V"))
				out.print("RECHAZADO");
			else if (request.getAttribute("E01CMHSTS").equals("P"))
				out.print("PROCESADO");
			else
				out.print(" ");%>"
					readonly></td>
				<td nowrap colspan="4"></td>


			</tr>
		</table>
		</td>
	</tr>
</table>




<%
	if (ExCon.getNoResult()) {
%>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center"><font size="3"> <b> No hay
		resultados que correspondan a su criterio de b�squeda. </b> </font></div>
		</td>
	</tr>
	<tr>
		<td>
			<p align="center">
				<input id="EIBSBTN" type="button" onclick="javascript:goAction('1100');"   name=Volver value="Volver">
			</p>
		</td>
	</tr>
</table>
<%
	} else {
%>

<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%">
			<a href="javascript:goAction('1300')"><b>Consultar</b></a>
		</td>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('1400')"> <b>Procesar</b></a> 
		</td>
		<td align="center" class="tdbkg" width="10%">
			<a href="javascript:goAction('1500')"><b>Eliminar</b></a> 
		</td>
		<td align="center" class="tdbkg" width="10%">
			<a href="javascript:goAction('1200')"><b>Refrescar</b></a> 
		</td>
	</tr>
</table>


<table class="tbenter" width=100%>
	<tr>
		<td height="20"></td>
	</tr>

	<tr>
		<td>
		<table id="headTable" width="100%" align="left">
			<tr id="trdark">
				<th align="center" nowrap width="30"></th>
				<th align="center" nowrap width="100">ID</th>
				<th align="center" nowrap width="100">Fecha Carga</th>
				<th align="center" nowrap width="120">Reg</th>
				<th align="center" nowrap width="120">Reg Error</th>
				<th align="center" nowrap width="150">Estado</th>
				<th align="center" nowrap width="100">Fecha Estado</th>
				<th align="center" nowrap width="150">Usuario</th>
				<th align="center" nowrap width=""></th>
			</tr>

			<%
				ExCon.initRow();
					int k = 0;
					boolean firstTime = true;
					String chk = "";
					while (ExCon.getNextRow()) {

						ECO060001Message pvprd = (ECO060001Message) ExCon
								.getRecord();
			%>
			<tr>
				<td nowrap>
					<input type="radio" name="ID_Interfaz"	id="codigo_lista" value="<%=ExCon.getCurrentRow()%>" <%=chk%> />
				</td>
				<td nowrap align="center"><%=pvprd.getE01CMHIDC()%></td>
				<td nowrap align="center">
				<%
					out.print(pvprd.getE01CMHCAD() + "/" + pvprd.getE01CMHCAM()
									+ "/" + pvprd.getE01CMHCAY());
				%>
				</td>
				<td nowrap align="center"><%=pvprd.getE01CMHCAR()%></td>
				<td nowrap align="center"><%=pvprd.getE01CMHVAR()%></td>
				<td nowrap align="center"><%=pvprd.getE01CMHGST()%></td>
				<td nowrap align="center">
				<%
					out.print(pvprd.getE01CMHCAD() + "/" + pvprd.getE01CMHCAM()
									+ "/" + pvprd.getE01CMHCAY());
				%>
				</td>
				<td nowrap align="left"><%=pvprd.getE01CMHUPU()%></td>
				<td nowrap align="center"></td>
			</tr>
			<%
				}
			%>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table class="tbenter" width="98%" align="center">
			<tr>
				<td width="40%" align="left">
				<%
					if (ExCon.getShowPrev()) {
							int pos = ExCon.getFirstRec() - 10;

							out
									.println("<A HREF=\""
											+ request.getContextPath()
											+ "/servlet/datapro.eibs.client.JSECO0600?SCREEN=1200&posicion="
											+ pos + "&mark=atr�s" + "&E01CMHSTS="
											+ request.getAttribute("E01CMHSTS")
											+ "&E01CMHFDD="
											+ request.getAttribute("E01CMHFDD")
											+ "&E01CMHFDM="
											+ request.getAttribute("E01CMHFDM")
											+ "&E01CMHFDY="
											+ request.getParameter("E01CMHFDY")
											+ "&E01CMHFHD="
											+ request.getAttribute("E01CMHFHD")
											+ "&E01CMHFHM="
											+ request.getAttribute("E01CMHFHM")
											+ "&E01CMHFHY="
											+ request.getAttribute("E01CMHFHY")
											+ "&E01CMHIDC="
											+ request.getAttribute("E01CMHIDC")

											+ "\"><IMG border=\"0\" src=\""
											+ request.getContextPath()
											+ "/images/s/previous_records.gif\" ></A>");
						}
				%>
				</td>
				<td width="20%" align="center">
					<p align="center">
						<input id="EIBSBTN" type="button" onclick="javascript:goAction('1100');"   name=Volver value="Volver">
					</p>
 				</td>	
 				<td width="40%" align="right">
				<%
					if (ExCon.getShowNext()) {
							int pos = ExCon.getLastRec();
							out
									.println("<A HREF=\""
											+ request.getContextPath()
											+ "/servlet/datapro.eibs.client.JSECO0600?SCREEN=1200&posicion="
											+ pos + "&mark=adelante" + "&E01CMHSTS="
											+ request.getAttribute("E01CMHSTS")
											+ "&E01CMHFDD="
											+ request.getAttribute("E01CMHFDD")
											+ "&E01CMHFDM="
											+ request.getAttribute("E01CMHFDM")
											+ "&E01CMHFDY="
											+ request.getParameter("E01CMHFDY")
											+ "&E01CMHFHD="
											+ request.getAttribute("E01CMHFHD")
											+ "&E01CMHFHM="
											+ request.getAttribute("E01CMHFHM")
											+ "&E01CMHFHY="
											+ request.getAttribute("E01CMHFHY")
											+ "&E01CMHIDC="
											+ request.getAttribute("E01CMHIDC")

											+ "\"><IMG border=\"0\" src=\""
											+ request.getContextPath()
											+ "/images/s/next_records.gif\" ></A>");
						}
				%>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<%
	}
%> 
     </form>
</body>
</html>
