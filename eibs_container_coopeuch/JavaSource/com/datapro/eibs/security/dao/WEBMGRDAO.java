package com.datapro.eibs.security.dao;

import com.datapro.eibs.access.DAOSuper;
import com.datapro.eibs.security.vo.WEBMGR;

public class WEBMGRDAO extends DAOSuper {

	protected void initAuditFields(Object vo) {
		
		if (vo != null) {
			//((WEBOG)vo).setOGRLMM(month);
			//((WEBOG)vo).setOGRLMD(day);
			//((WEBOG)vo).setOGRLMY(year);
			((WEBMGR)vo).setOGRLMT(timestamp);
			((WEBMGR)vo).setOGRLMU(user);
		}
	}
}
