package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Creado por              :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  06/03/2013                                                                                      **/
/**  Objetivo                :  Proceso de Orden de Pago Masivo     (NUEVO)                                                     **/
/*********************************************************************************************************************************/


import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.PropertyResourceBundle;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jspsmart.upload.SmartUpload;
import com.jspsmart.upload.SmartUploadException;

import datapro.eibs.beans.ECO100002Message;
import datapro.eibs.beans.EOF200001Message;
import datapro.eibs.beans.EOF200002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.ServiceLocator;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.master.Util;
import datapro.eibs.services.ExcelResultSet;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.services.ExcelXLSXResultSet;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;
import datapro.eibs.sockets.MessageContext;

public class JSEOF2000 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_OP_MASIVO_ENTER = 100;	
	protected static final int A_OP_MASIVO_ENTER = 200;
	protected static final int A_CARGADOS_LIST = 300;
	protected static final int A_VALIDADOS_LIST = 400;	
	protected static final int A_APLICADOS_LIST = 500;
	protected static final int A_RECHAZADOS_LIST = 550;	
	protected static final int A_BORRAR_LIST = 600;
	protected static final int A_VALIDAR_LIST = 700;
	protected static final int A_CONSULTA_DETALLE = 800;	
	protected static final int A_APLICAR_LIST = 900;	
	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	/**
	 * 
	 */
	protected void processRequest(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res,
			HttpSession session, 
			int screen) 
	throws ServletException, IOException {
		screen =  A_OP_MASIVO_ENTER;
		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}			
	    	flexLog("Screen..." + screen);
		switch (screen) {
		case R_OP_MASIVO_ENTER:
			procReqOPMasivoEnter(user, req, res, session);
			break;
		case A_OP_MASIVO_ENTER:
			procActionOPMasivoEnter(user, req, res, session);
			break;			
		case A_CARGADOS_LIST:
			procActionCargadosList(user, req, res, session);
			break;	
		case A_VALIDADOS_LIST:
			procActionValidadosList(user, req, res, session);
			break;	
		case A_APLICADOS_LIST:
			procActionAplicadosList(user, req, res, session);
			break;		
		case A_RECHAZADOS_LIST:
			procActionRechazadosList(user, req, res, session);
			break;	
		case A_BORRAR_LIST:
			procActionBorrarCargados(user, req, res, session);
			break;			
		case A_VALIDAR_LIST:
			procActionValidarCargados(user, req, res, session);
			break;			
		case A_APLICAR_LIST:
			procActionAplicarAprobados(user, req, res, session);
			break;
		case A_CONSULTA_DETALLE:
			procActionConsultaDetalle(user, req, res, session);
			break;				
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}
	/**
	 * procReqOPMasivoEnter
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqOPMasivoEnter(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession ses)
			throws ServletException, IOException {
		UserPos userPO = getUserPos(ses);
		ELEERRMessage msgError = new ELEERRMessage();
		
		ses.setAttribute("error", msgError);
		ses.setAttribute("userPO", userPO);
		
		forward("EOF2000_orden_pago_masivo_enter.jsp", req, res);

	}
	
	/**
	 * procActionOPMasivoEnter: find the list of forms depending on status, the program will epvl1005
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionOPMasivoEnter(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {
		
	UserPos userPO = getUserPos(session);
	ELEERRMessage msgError = new ELEERRMessage();
	MessageProcessor mp = null;
	String PageToCall = "";
	int rows = 0;
	//read file
	try {
		mp = getMessageProcessor("ECO1000", req);
		
		SmartUpload mySmartUpload = new SmartUpload();
		mySmartUpload.initialize(config, req, res);
		mySmartUpload.upload();
		com.jspsmart.upload.File file =  mySmartUpload.getFiles().getFile(0);
		
		InputStream xls = Util.getStreamFromObject(file);													
		
		ResultSet rs = null;
		if (ExcelUtils.isXLSXVersion(file.getFilePathName())) {
			rs = new ExcelXLSXResultSet(xls, 0);
			((ExcelXLSXResultSet)rs).init();
		} else {
			rs = new ExcelResultSet(xls, 0);
			((ExcelResultSet)rs).init();
		}
		
		boolean error = false;
		while (rs.next()) {
			ECO100002Message msg = (ECO100002Message) mp.getMessageRecord("ECO100002");
            msg.setH02USER(user.getH01USR());
			ExcelUtils.populate(rs, msg);
			flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);
			rows++;
		}
		if (!error) {
			ECO100002Message msg = (ECO100002Message) mp.getMessageRecord("ECO100002");
            msg.setH02USER(user.getH01USR());
			msg.setH02FLGMAS("*"); //Last Element List
			mp.sendMessage(msg);
			//	Receive Error Message
			msgError = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(msgError)) {
				PageToCall = "EOF2000_orden_pago_masivo_enter.jsp";
			} else {
				req.setAttribute("rows", String.valueOf(rows));
				PageToCall = "EOF2000_orden_pago_masivo_process.jsp";
			}
		}
		
		flexLog("Putting java beans into the session");
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		forward(PageToCall, req, res);
		
	} catch (Exception e) {
		msgError = new ELEERRMessage();
		msgError.setERRNUM("2");
		msgError.setERNU01("0001");		//4		                
		msgError.setERDS01("Error en Record: " + (rows + 1));//70
		msgError.setERNU02("0002");		//4		                
		msgError.setERDS02(e.getMessage().trim().length() > 70 ? e.getMessage().substring(0, 69) : e.getMessage().trim());//70
		session.setAttribute("error", msgError);						
		forward("EOF2000_orden_pago_masivo_enter.jsp", req, res);
	} finally {
		if (mp != null)	mp.close();
	}
}
	
	/**
	 * procActionCargadosList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionCargadosList(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EOF2000", req);

			EOF200001Message msg = (EOF200001Message) mp.getMessageRecord("EOF200001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());
 
			//Sends message
		//	flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
            userPO.setPurpose("CARGADOS");
			session.setAttribute("userPO", userPO);
			session.setAttribute("EOF200001List", list);
			forwardOnSuccess("EOF2000_orden_pago_masivo_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	/**
	 * procActionValidadosList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionValidadosList(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EOF2000", req);

			EOF200001Message msg = (EOF200001Message) mp.getMessageRecord("EOF200001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0016");
			msg.setH01TIMSYS(getTimeStamp());
						  
			//Sends message
		//	flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
            userPO.setPurpose("VALIDADOS");
			session.setAttribute("userPO", userPO);
			session.setAttribute("EOF200001List", list);
			forwardOnSuccess("EOF2000_orden_pago_masivo_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	/**
	 * procActionAplicadosList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionAplicadosList(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EOF2000", req);

			EOF200001Message msg = (EOF200001Message) mp.getMessageRecord("EOF200001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0017");
			msg.setH01TIMSYS(getTimeStamp());
						  
			//Sends message
		//	flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
            userPO.setPurpose("APLICADOS");
			session.setAttribute("userPO", userPO);
			session.setAttribute("EOF200001List", list);
			forwardOnSuccess("EOF2000_orden_pago_masivo_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	/**
	 * procActionAplicadosList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionRechazadosList(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EOF2000", req);

			EOF200001Message msg = (EOF200001Message) mp.getMessageRecord("EOF200001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0018");
			msg.setH01TIMSYS(getTimeStamp());
						  
			//Sends message
		//	flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
            userPO.setPurpose("RECHAZADOS");
			session.setAttribute("userPO", userPO);
			session.setAttribute("EOF200001List", list);
			forwardOnSuccess("EOF2000_orden_pago_masivo_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}	

	/**
	 * procActionBorrarCargados	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionBorrarCargados(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EOF2000", req);

			EOF200002Message msg = (EOF200002Message) mp.getMessageRecord("EOF200002");
			msg.setH02USERID(user.getH01USR());
			msg.setH02OPECOD("0009");
			msg.setH02TIMSYS(getTimeStamp());
						  
			//Sends message
//			flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();

			if (!mp.hasError(msgError))	{
				procActionCargadosList(user, req, res, session);			
			} else {
				session.setAttribute("error", msgError);
				forward("EOF2000_orden_pago_masivo_list.jsp", req, res);
			}	
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	/**
	 * procActionValidarCargados	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionValidarCargados(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EOF2000", req);

			EOF200002Message msg = (EOF200002Message) mp.getMessageRecord("EOF200002");
			msg.setH02USERID(user.getH01USR());
			msg.setH02OPECOD("0005");
			msg.setH02TIMSYS(getTimeStamp());
						  
			//Sends message
			mp.sendMessage(msg);

			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
//            flexLog("mensaje recibido.." + msgError);
			if (!mp.hasError(msgError))	{
				procActionCargadosList(user, req, res, session);			
			} else {
				session.setAttribute("error", msgError);
				forward("EOF2000_orden_pago_masivo_list.jsp", req, res);
			}	
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	/**
	 * procActionValidarCargados	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionAplicarAprobados(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EOF2000", req);

			EOF200002Message msg = (EOF200002Message) mp.getMessageRecord("EOF200002");
			msg.setH02USERID(user.getH01USR());
			msg.setH02OPECOD("0002");
			msg.setH02TIMSYS(getTimeStamp());
						  
			//Sends message
			mp.sendMessage(msg);

			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
//            flexLog("mensaje recibido.." + msgError);
			if (!mp.hasError(msgError))	{
				procActionValidadosList(user, req, res, session);			
			} else {
				session.setAttribute("error", msgError);
				forward("EOF2000_orden_pago_masivo_list.jsp", req, res);
			}	
		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	protected void procActionConsultaDetalle(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			UserPos userPO = getUserPos(ses);
			// Receive Data
			try {					
				JBObjList bl = (JBObjList) ses.getAttribute("EOF200001List");
				int idx = req.getParameter("ORDEN")==null || req.getParameter("ORDEN").equals("")?0:Integer.parseInt(req.getParameter("ORDEN"));								
		
				EOF200001Message  msgDoc = (EOF200001Message) bl.get(idx);						
				
			    ses.setAttribute("opMasivo", msgDoc);	
				ses.setAttribute("userPO", userPO);			
			    forward("EOF2000_inq_orden_pago_masivo.jsp", req, res);
			    
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("Socket Communication Error");
			}
	}
	
}
