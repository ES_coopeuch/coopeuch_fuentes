<!-- Created by Nicol�s Valeria ------Datapro----- 19/02/2015 --> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC200001Message"%>


<%@page import="datapro.eibs.beans.ERC005002Message"%>
<%@page import="datapro.eibs.beans.ERC005003Message"%><html>
<head>
<title>Sistema Bancario: Conciliaci�n Bancaria</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "data" class= "datapro.eibs.beans.ERC005001Message"  scope="session" />
<jsp:useBean id= "descBanco" class= "java.lang.String"  scope="session" />
<jsp:useBean id="listCredito" class="datapro.eibs.beans.JBObjList" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<style type="text/css">
.headerTable{
	border-top-width : 1px;
	border-right-width : 1px;
	border-bottom-width : 1px;
	border-left-width : 1px;
	border-color: #990000;
	border-style : solid solid solid solid;
	width:100%;
}
.tablaData{
	font-family: "Verdana, Arial, Helvetica, sans-serif";
	font-size:8pt;
	background-color: #F0F0F0;
	border-color: red;
	color: #990000;
}
</style>
<script type="text/javascript">

var totalHaber = 0;
var cantHaber = 0;

function update(obj){
	if (obj.checked == '1'){
		document.getElementById('totalHaber').value= 'S';
	}else{
		document.getElementById('totalHaber').value= 'N';
	}
}

function goPage(){
	if (document.getElementById('totalHaber').value == 'S'){
	
		document.forms[0].submit();
	}
	else
	{
		alert ('No ha seleccionado movimientos para contabilizar');
	}
}

function goBack(){
	document.getElementById('SCREEN').value = "400";
	document.forms[0].submit();
	
}
</script>
</head>

<body>
 
<H3 align="center">Contabilizaci�n  Movimientos  Cartola<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="search_data.jsp, ERC0045"></H3>

<hr size="4">
<br>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0050">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="300">
    <INPUT TYPE=HIDDEN name="H01FLGWK3" id="DES_CON" VALUE="C">
    <INPUT TYPE=HIDDEN name="totalHaber" id="totalHaber" VALUE="N">
    
    <table width="100%" cellpadding="2" cellspacing="0" border="0" class="TABLEINFO" align="center">
    	<tbody style="width: 100%">
    		<tr style="width: 100%">
    			<td nowrap>
    				<table width="100%" cellpadding="2" cellspacing="0" border="0" class="TBHEAD" align="center">
    					<tbody>
    						<tr class="TRDARK">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Banco :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01RCHRBK" size="5" maxlength="4" value="<%=data.getE01RCHRBK()%>" readonly>
	       							<input type="text" name="E01DSCRBK" readonly="readonly" size="43" maxlength="43" value="<%=descBanco%>" readonly>
    							</td>
    							<td>
    								<div align="right">
    									Fecha Desde :
    								</div>
    							</td>
    							<td>
    								<input style="vertical-align: middle" type="text" name="E01DESDDD" size="3" id="fecha4" maxlength="2" value="<%=data.getE01DESDDD()%>" readonly>
					                <input style="vertical-align: middle" type="text" name="E01DESDDM" size="3" id="fecha5" maxlength="2" value="<%=data.getE01DESDDM()%>" readonly>
					                <input style="vertical-align: middle" type="text" name="E01DESDDY" size="5" id="fecha6" maxlength="4" value="<%=data.getE01DESDDY()%>" readonly>
    							</td>
    						</tr>
    						<tr class="TRCLEAR">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Cuenta Banco :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01BRMCTA" size="23" maxlength="20" value="<%=data.getE01BRMCTA()%>" readonly>
    							</td>
    							<td>
    								<div align="right">
    									Fecha Hasta :
    								</div>
    							</td>
    							<td>
    								<input style="vertical-align: middle" type="text" name="E01HASDDD" size="3" id="fecha4" maxlength="2" value="<%=data.getE01HASDDD()%>" readonly>
					                <input style="vertical-align: middle" type="text" name="E01HASDDM" size="3" id="fecha5" maxlength="2" value="<%=data.getE01HASDDM()%>" readonly>
					                <input style="vertical-align: middle" type="text" name="E01HASDDY" size="5" id="fecha6" maxlength="4" value="<%=data.getE01HASDDY()%>" readonly>
    							</td>
    						</tr>
    						<tr class="TRDARK">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Cuenta IBS :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01BRMACC" size="10" maxlength="7" value="<%=data.getE01BRMACC()%>" readonly>
    							</td>
    							<td>
    								<div align="right">
    								N� Cartola :
 									</div>
    							</td>
    							<td>
    								<input type="text" name="E01RCSSTN" size="23" mareadonlyreadonlyxlength="8" value="<%=data.getE01RCSSTN()%>" readonly>
    							</td>
    						</tr>
    					</tbody>
    				</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    <br>
    <div style="display: block;width: 100%" >
	   		
	   		<tr class="headerTable">
	   			<table id="headTable2" class="tbhead" cellspacing=0 cellpadding=2 width="100%" border="1">
					<td nowrap class="headerTable" style="width: 100%;text-align: center;"><b>T R A N S A C C I O N E S   D E  C A R T O L A</b></td>   
				</table>
						
	   			<td style="width: 100%;text-align: center;">
	   				<table id="headTable2" class="tbhead" cellspacing=0 cellpadding=2 width="100%" border="1">
						<tr id="trdark">
							<th nowrap align="center" width="10%">&nbsp;</th>
							<th nowrap align="center" width="15%">Fecha</th>
							<th nowrap align="center" width="10%">REFERENCIA</th>
							<th nowrap align="center" width="45%">Descripci�n</th>
							<th nowrap align="center" width="20%">CREDITO</th>
						</tr>
					</table>
	   			</td>
	   		</tr>
	   
    </div>
    
    <div style="width: 100%">
		
			<table align="right" width="100%" border="0" cellpadding="2" cellspacing="0">
				<%
				int i = 1;
					i = 1;
					while(listCredito.getNextRow()){
						 ERC005002Message msg = (ERC005002Message) listCredito.getRecord();
				 %>
						<tr <%if ((i%2) == 0){%>class="TRCLEAR"<%}else{%>class="TRDARK"<%}%>>
							<td nowrap align="center" width="5%">
								<input type="checkbox" id="<%=msg.getE02RCSAMD()%>" name="rowHaber" value="<%=msg.getE02RCSUID()%>" onclick="update(this)">
							</td>
							<td nowrap align="center" width="25%">
								<div><%=Util.formatDate(msg.getE02RCSSDD(), msg.getE02RCSSDM(), msg.getE02RCSSDY())%></div>
							</td>
							<td nowrap align="center" width="8%">
								<div><%=Util.formatCell(msg.getE02RCSCKN())%></div>
							</td>
							<td nowrap align="left" width="45%">
								<div><%= msg.getE02RCSGLO()%></div>
							</td>
							<td nowrap align="right" width="15%">
								<div><%=Util.formatCCY(msg.getE02RCSAMD())%></div>
							</td>
						</tr>
				<%
				   	i++; 
				} %>
	
			</table>	
	</div>
	
    <br>

		<table>
			<td>
				<input id="EIBSBTN" type="button" name="Submit" value="Volver" onclick="goBack()">
			</td>
			    
			<td>
				<input id="EIBSBTN" type="button" name="Submit" value="Enviar" onclick="goPage()">
			</td>
		</table>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>

</form>
</body>
</html>
