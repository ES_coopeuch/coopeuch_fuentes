<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ESG060001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<title>Reporte de Ventas de Seguros</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="msglist" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">

  	function selectAll(obj) {
  		var form = document.forms[0];
		var formLength = form.elements.length;
    	for (n=0; n<formLength; n++) {
    		var elemt = form.elements[n];
      		if (elemt.type == 'checkbox') {
      			elemt.checked = obj.checked;
      		}
    	}
  	}
  	
	function checkvalidate(checks) {
		if (checks.length > 0){//mas de un registro
		    for (i = 0; lcheck = checks[i]; i++) {
		        if (lcheck.checked) {
		            return true;
		            break;
		        }
		    }			
		}else{
			if (checks.checked){
				return true;			
			}
	    }
	    
	    return false;
	    
	}
	



	function goAction(){
	
		if(confirm('Est� seguro de realizar esta operaci�n de cierre de d�a?'))
		{
			document.forms[0].SCREEN.value = "300";
	    	document.forms[0].submit();
	    }	
	}
	
	
	function goReporte(){
		document.forms[0].SCREEN.value = "400";
	    document.forms[0].submit();
	}
	
  
</script>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Reporte de Ventas de Seguros<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="reporte_ventas_seguros_list.jsp, ESG0600"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESG0600"  >
<input type="hidden" name="SCREEN" value="300">

<input type="hidden" name="E01PACFID" value="<%=String.format("%02d", Integer.parseInt(request.getAttribute("DIAI").toString()))%>">
<input type="hidden" name="E01PACFIM" value="<%=String.format("%02d", Integer.parseInt(request.getAttribute("MESI").toString()))%>">
<input type="hidden" name="E01PACFIY" value="<%=request.getAttribute("ANOI")%>">
<input type="hidden" name="E01PACFHD" value="<%=String.format("%02d", Integer.parseInt(request.getAttribute("DIAF").toString()))%>">
<input type="hidden" name="E01PACFHM" value="<%=String.format("%02d", Integer.parseInt(request.getAttribute("MESF").toString()))%>">
<input type="hidden" name="E01PACFHY" value="<%=request.getAttribute("ANOF")%>">

<br>
<b><p align="left">SUCURSAL : <%=currUser.getE01UBR()%></p></b>

<table class="tableinfo" align="center" width="100%">
	<tr>
	<td nowrap valign="top" width="100%">
		<table width="100%">
			<tr id="trdark">
				<th align="right"  nowrap width="25%">Fecha Desde :</th>
				<th align="left"   nowrap width="25%"><%=String.format("%02d", Integer.parseInt(request.getAttribute("DIAI").toString())) %> / <%=String.format("%02d", Integer.parseInt(request.getAttribute("MESI").toString()))%> / <%=request.getAttribute("ANOI")%> </th>
				<th align="right"  nowrap width="25%">Fecha Hasta :</th>
				<th align="left"   nowrap width="25%"><%=String.format("%02d", Integer.parseInt(request.getAttribute("DIAF").toString()))%> / <%=String.format("%02d", Integer.parseInt(request.getAttribute("MESF").toString()))%> / <%=request.getAttribute("ANOF")%> </th>			
			</tr>
		</table>
	</td>
  </tr>
</table>

<%
	if (msglist.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=20%>
	<TR>
		<TD>
		<div align="center"><font size="3"><b> No hay datos pendientes para su proceso. Verifique al generar reporte.</b></font></div>
		</TD>
	</TR>
</TABLE>
		<br>
	<div id="DIVSUBMIT" align="center">
		<input id="EIBSBTN" type=button name="Reporte" value="Generar Reporte" onClick="goReporte()">
	</div>
<%
	} else {
%>	


<br>

<H4>&nbsp;
		<input type="checkbox" id="all" name="all" onclick="selectAll(this)"> Seleccionar Todas
</H4>
<table id="mainTable" class="tableinfo" align="center">
	<tr>
		<td nowrap valign="top" width="100%">
			<table id="headTable" width="100%">
				<tr id="trdark">
					<th align="center" nowrap width="5%">Sel</th>				
					<th align="center" nowrap width="15%">N. Operaci&oacute;n</th>				
					<th align="center" nowrap width="10%">Rut Socio</th>				
					<th align="center" nowrap width="25%">Nombre Socio</th>
					<th align="center" nowrap width="25%">Nombre Ejecutivo</th>
					<th align="center" nowrap width="10%">Producto Asociado</th>
					<th align="center" nowrap width="10%">Fecha de Inicio</th>
					
				</tr>
			</table>

		<div id="dataDiv1" class="scbarcolor" >
		<table id="dataTable">
			<% 
				int ix = 0;
				boolean bInd = false;
				
				msglist.initRow();
					while (msglist.getNextRow()) 
					{
						ESG060001Message convObj = (ESG060001Message) msglist.getRecord();
						if("*".equals(convObj.getE01MARCA().trim())){
							bInd = true;
						
						}
			%>
			<tr>
	            <td nowrap width="5%">
	            	<input type="checkbox" name="E02PLHSEL"  onclick="" value="<%=ix++%>"
	            		<%=("*".equals(convObj.getE01MARCA().trim())?"checked='checked'":"")%> >
	            </td>
				<td nowrap align="left">  <%=Util.formatCell(convObj.getE01PACPAC())%></td>
				<td nowrap align="left">  <%=Util.formatCell(convObj.getE01CUSIDN())%></td>
				<td nowrap align="left">  <%=Util.formatCell(convObj.getE01CUSNA1())%></td>	
				<td nowrap align="left">  <%=Util.formatCell(convObj.getE01NOMEJE())%></td>	
				<td nowrap align="left">  <%=Util.formatCell(convObj.getE01APCSHN())%></td>
				<td nowrap align="center"><%=Util.formatCell(convObj.getE01PACFEC())%></td>
			</tr>
			<%
				}
			%>
		</table>
		</div>
		</td>
	</tr>
</table>
 
<table align="center" id="TBHELPN">
	<tr>
	<td>
	<div id="DIVSUBMIT" align="center">
		<%if(bInd){ %>
		<input id="EIBSBTN" type=button name="Procesar" value="Procesar" onClick="goAction()">
		<% } else { %>
		<input id="EIBSBTN" type=button name="Procesar" value="Procesar" onClick="goAction()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="EIBSBTN" type=button name="Reporte" value="Generar Reporte" onClick="goReporte()">
		
		<% }%>
	</div>
	</td>
	</tr>
</table>
<script type="text/javascript">
     function resizeDoc() {
 	     adjustEquTables(headTable, dataTable, dataDiv1,1,false);
      }
     resizeDoc();
     window.onresize=resizeDoc; 
</script> 

 <script type="text/javascript">    
     <% //marcamos si tenia alguna planilla seleccionada
     	String[] valPosSelected=(String[])session.getAttribute("ListPosSelected"); 
		valPosSelected= (valPosSelected==null)?new String[0]:valPosSelected;
		for (int i = 0; i < valPosSelected.length; i++) {
		%>
		document.forms[0].E02PLHSEL[<%=valPosSelected[i]%>].checked=true;
		<%				
		}
	 %>	
</script> 	 
<%
	}
%>
</form>

</body>
</html>
