<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<jsp:useBean id="holMsg" class="datapro.eibs.beans.ESD011001Message"  scope="session" />
<jsp:useBean id="error"  class="datapro.eibs.beans.ELEERRMessage"  scope="session"/>
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" 	scope="session" />

<script language="JavaScript">
function enter(){
	  document.forms[0].submit();
	 }
</script> 

</head>
<body>

 <% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     error.setERRNUM("0");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
    }
%>
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSESD0110" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="3">
  <input type=HIDDEN name="E01SELCTR" value="<%= holMsg.getE01SELCTR().trim()%>">
  <input type=HIDDEN name="E01SELRGN" value="<%= holMsg.getE01SELRGN().trim()%>">

  <h3 align="center">Mantenimiento Dias Feriados<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="hollidays_maint.jsp,ESD0110"> 
  </h3>
  <hr size="4">
<BR> 
<TABLE class="tableinfo" align="center" width="85%">
	<TBODY>
		<TR>
			<TD>
			<TABLE width="100%" border="0" cellspacing="1" cellpadding="0">
				<TBODY>
					<TR id="trdark">
						<TD nowrap width="16%" align="right">Pais :</TD>
						<TD nowrap width="16%" align="left">
  				            <eibsinput:text name="holMsg" property="E01HOLCTR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>" readonly="true"/>
						</TD>
						<%
 						if (holMsg.getE01HOLRGN().trim().equals("")){
						%> 						
						<TD nowrap width="16%" align="right">Feriado Nacional</TD>
 						<%} else {%>
						<TD nowrap width="16%" align="right">Region :</TD>
						<TD nowrap width="16%" align="left">
  				            <eibsinput:text name="holMsg" property="E01HOLRGN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>" readonly="true"/>
						</TD>
 						<%}%>
						<TD nowrap width="16%" align="right">A�o :</TD>
						<TD nowrap width="16%" align="left">
  				            <eibsinput:text name="holMsg" property="E01SELYEAR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="5" maxlength="4" readonly="true"/>
						</TD>
					</TR>
				</TBODY>
			</TABLE>
			</TD>
		</TR>
	</TBODY>
</TABLE>
<BR>
<TABLE class="tableinfo" align="center" >
	<TBODY>
		<TR align="center">
			<TD>
			<TABLE width="100%" align="center" cellspacing="1" cellpadding="0">
				<tr id="trdark">
					<td  align="center" valign="top" width="50%">Fecha del Feriado &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Eliminar</td>
				</tr>
				<tr>
					<td  align="center" width="50%">
				<div style="height:400px; overflow-y: scroll">
					<%
					String year,month,day,flg,wek,yearV,monthV,dayV,flgV,wekV;
					for(int i = 1; i <= 180; i++)
					{
						year =	"E01HOY" + ( (i<10) ? "00"+Integer.toString(i) : (i<100) ? "0"+Integer.toString(i): Integer.toString(i));
						month =	"E01HOM" + ( (i<10) ? "00"+Integer.toString(i) : (i<100) ? "0"+Integer.toString(i): Integer.toString(i));
						day =	"E01HOD" + ( (i<10) ? "00"+Integer.toString(i) : (i<100) ? "0"+Integer.toString(i): Integer.toString(i));
						wek =	"E01HOW" + ( (i<10) ? "00"+Integer.toString(i) : (i<100) ? "0"+Integer.toString(i): Integer.toString(i));
						flg =	"E01HOF" + ( (i<10) ? "00"+Integer.toString(i) : (i<100) ? "0"+Integer.toString(i): Integer.toString(i));
						yearV = holMsg.getField(year).getString().trim();
						monthV= holMsg.getField(month).getString().trim();
						dayV =	holMsg.getField(day).getString().trim();
						wekV =	holMsg.getField(wek).getString().trim();
						flgV = "D";
						if (yearV.equals("0")) yearV = "";
						else if (Integer.parseInt(yearV) < 10) yearV = "0" + yearV;
						if (dayV.equals("0")) dayV = "";
						if (monthV.equals("0")) monthV = "";
						%>
						<input size="3" maxlength="2" name="<%=month%>" value="<%=monthV%>" onkeypress="enterInteger()" class="TXTRIGHT">
						<input size="3" maxlength="2" name="<%=day%>"   value="<%=dayV%>" onkeypress="enterInteger()" class="TXTRIGHT">
						<input size="5" maxlength="4" name="<%=year%>"  value="<%=yearV%>" onkeypress="enterInteger()" class="TXTRIGHT"> 
						<A href="javascript:DatePicker(document.forms[0].<%=month%>,document.forms[0].<%=day%>,document.forms[0].<%=year%>)"><IMG
							src="<%=request.getContextPath()%>/images/calendar.gif" alt="help"
							align="middle" border="0"> </A> 
						<input size="2" maxlength="2" name="<%=wek%>"   value="<%=wekV%>" readonly>
						<input type="checkbox" name="<%=flg%>"  value="<%=flgV%> " > 
						<br>
					<%}%>
				</div>
				</td>
				</tr>
			</TABLE>
			</TD>
		</TR>
	</TBODY>
</TABLE>

<BR>

  
  <p align="center"> 
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
      
</form>
</body>
</html>
