<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.EPV119501Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

 builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
	init(opth,field,bank,ccy,field1,field2,opcod);
	showPopupHelp();
}

 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	Consulta Descuento en Plataforma de Venta
<%} else if (maintenance){ %>
	Mantenci�n Descuento en Plataforma de Venta
<%} else { %>
	Nuevo  Descuento en Plataforma de Venta
<%} %>

 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PVDES_maintenance.jsp, EPV1195"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1195" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01UBK" value="<%= currUser.getE01UBK().trim()%>">
  
 <% int row = 0;%>
 
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">C�digo de Descuento :</div>
            </td>
            <td width="70%" > 
	             <eibsinput:text property="E01PVDRCD" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" readonly="true"/>
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Descripci�n :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01PVDDES" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL%>" readonly="<%=readOnly %>" />
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="40%" > 
              <div align="right">Tipo de Recaudaci�n :</div>
            </td>
            <td width="60%" > 
               <select name="E01PVDREC" <%=readOnly?"disabled":""%>>
                    <option value="P" <% if (cnvObj.getE01PVDREC().equals("P")) out.print("selected"); %>>Planilla</option>
                    <option value="D" <% if (cnvObj.getE01PVDREC().equals("D")) out.print("selected"); %>>Pago Directo</option>  
                    <option value="A" <% if (cnvObj.getE01PVDREC().equals("A")) out.print("selected"); %>>Ambos</option>                                      
                  </select>
	        </td>
           </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Tasa de Descuento :</div>
            </td>
            <td width="70%" > 
              <eibsinput:text property="E01PVDDIS" size="10" maxlength="10" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="<%=readOnly %>" />                
            </td>
           </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Vigencia Desde :</div>
            </td>
            <td width="70%" > 
   	        <eibsinput:date name="cnvObj" fn_year="E01PVDVDY" fn_month="E01PVDVDM" fn_day="E01PVDVDD" readonly="<%=readOnly %>"/>
	        </td>
          </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Vigencia Hasta :</div>
            </td>
            <td width="70%" > 
   	        <eibsinput:date name="cnvObj" fn_year="E01PVDVHY" fn_month="E01PVDVHM" fn_day="E01PVDVHD" readonly="<%=readOnly %>" />
	        </td>
          </tr>           
        </table>
      </td>
    </tr>
  </table>

<%if  (!readOnly) { %>
    <div align="center"> 
        <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    </div>
<% } %>  

  </form>
</body>
</HTML>
