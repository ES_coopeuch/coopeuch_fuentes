package datapro.eibs.client;

/**
 * Servlet de Asociar Integrante.
 * Creation date: (16/03/2018)
 * @author: David Medina
 */

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD008001Message;
import datapro.eibs.beans.ESD404001Message;
import datapro.eibs.beans.ESD405001Message;
import datapro.eibs.beans.ESD406001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.EWD0002DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

public class JSESD4050 extends JSEIBSServlet {
	
	// Action 
	
	// entering options
	protected static final int R_ENTER_NEW 	 = 100;
	protected static final int A_ENTER_FAMILIA_SELECTION = 200;
	protected static final int A_ENTER_FAMILIA_NEW = 300;
	protected static final int A_ENTER_FAMILIA_INTEGRANT_NEW = 400;
	protected static final int A_ENTER_FAMILIA_NEW_ENVIAR = 500;
	protected static final int A_ENTER_FAMILIA_INTEGRANT_NEW_ENVIAR = 600;
	
	protected static final int R_MAINTANCE 	= 1000;
	protected static final int A_MAINTANCE_FAMILIA = 1200;
	protected static final int A_MAINTANCE_FAMILIA_ENVIAR = 1300;
	
	protected static final int R_CONSULT 	= 3000;
	protected static final int A_CONSULT_FAMILIA = 3200;
	
	protected static final int R_APROBACION = 2000;
	
	protected String LangPath = "S";
	protected String bandUsuario = "0";
	protected String bandPerson = "3";
	protected String bandPR = "0";
	
	
	/**
	 * JSESD4030 constructor comment.
	 */
	public JSESD4050() {
		super();
		// TODO Apéndice de constructor generado automáticamente
	}
	
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

		flexLog("free resources used by JSESD4060");
		
	}
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		MessageContext mc = null;
		ESS0030DSMessage msgUser = null;
	  	
		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			}
			catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e); 
			}
		}else{
			screen = R_ENTER_NEW;
			
			try{
				
				msgUser = (datapro.eibs.beans.ESS0030DSMessage)session.getAttribute("currUser");
				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";
				try{
					flexLog("Opennig Socket Connection");
					mc = new MessageContext(super.getMessageHandler("ESD4060", req));
					
					try{
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					}catch(Exception e){
						flexLog("Screen set to default value");
					}
					
					switch (screen) {
						// BEGIN Entering
						// Request
						case R_ENTER_NEW : 
							procReqEnterNew(msgUser, req, res, session);
							break;
						case A_ENTER_FAMILIA_SELECTION:
							procActionEnterFamiliaSelection(mc, msgUser, req, res, session, screen);
							break;
						case A_ENTER_FAMILIA_NEW:
							procActionEnterFamiliaNew(mc, msgUser, req, res, session, screen);
							break;
						case A_ENTER_FAMILIA_NEW_ENVIAR:
							procActionEnterFamiliaNewEnviar(mc, msgUser, req, res, session, screen);
							break;
						case A_ENTER_FAMILIA_INTEGRANT_NEW:
							procActionEnterFamiliaIntegrantNew(mc, msgUser, req, res, session, screen);
							break;
						case A_ENTER_FAMILIA_INTEGRANT_NEW_ENVIAR:
							procActionEnterIntegrantNewEnviar(mc, msgUser, req, res, session, screen);
							break;
						case R_MAINTANCE :
							procReqMaintance(msgUser, req, res, session);
							break;
						case A_MAINTANCE_FAMILIA : 
							procActionMaintanceFamilia(mc, msgUser, req, res, session, screen);
							break;
						case A_MAINTANCE_FAMILIA_ENVIAR : 
							procActionMaintanceFamiliaEnviar(mc, msgUser, req, res, session, screen);
							break;
						case R_CONSULT :
							procReqEnterConsult(user, req, res, session);
							break;
						case A_CONSULT_FAMILIA : 
							procActionEnterFamiliaConsult(mc, msgUser, req, res, session, screen);
							break;
						default :
							res.sendRedirect(super.srctx + LangPath + super.devPage);
							break;
					}
				}catch(Exception e){
					e.printStackTrace();
					flexLog("Socket not Open(" + mc.toString() + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
				}
				finally {
					mc.close();
				}
			}catch(Exception e){
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}
		}
	}
	
	/**
	 * Metodo de error
	 * @param m
	 */
	protected void showERROR(ELEERRMessage m)
	{
		if (logType != NONE) {
			
			flexLog("ERROR received.");
			
			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01() + " code : " + m.getERDF01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02() + " code : " + m.getERDF02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03() + " code : " + m.getERDF03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04() + " code : " + m.getERDF04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05() + " code : " + m.getERDF05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06() + " code : " + m.getERDF06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07() + " code : " + m.getERDF07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08() + " code : " + m.getERDF08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09() + " code : " + m.getERDF09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10() + " code : " + m.getERDF10());
			
		}
	}
	
	
	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqEnterNew(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
				throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos	userPO = null;	

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos(); 
			userPO.setOption("PFAMILIA");
			userPO.setPurpose("NEW");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

	  	} catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		try {
			flexLog("About to call Page: " + LangPath + "ESD4050_familia_both_enter.jsp");
			callPage(LangPath + "ESD4050_familia_both_enter.jsp", req, res);	
		}
		catch (Exception e) {
			flexLog("Exception calling page " + e);
		}
	}
	
	
	/**
	 * 
	 * @param mc
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @param screen
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionEnterFamiliaSelection(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		
		MessageRecord newmessage = null;
		MessageRecord newmessage2 = null;
		MessageContext mcID = null;
		ESD008001Message msgClientPersonal = null;
		ESD405001Message msgPlanFamilia = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		boolean IsNotError = false;
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		
		// Send Initial data
		try
		{
			mcID = new MessageContext(super.getMessageHandler("ESD0080", req));
			msgClientPersonal = (ESD008001Message)mcID.getMessageRecord("ESD008001");
		 	msgClientPersonal.setH01USR(user.getH01USR());
		 	msgClientPersonal.setH01PGM("ESD0080");
		 	msgClientPersonal.setH01TIM(getTimeStamp());
		 	msgClientPersonal.setH01SCR("01");
		 	msgClientPersonal.setH01OPE("0002");
		 	try {
			 	if (req.getParameter("E01CUN") != null){
				 	msgClientPersonal.setE01CUN(req.getParameter("E01CUN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01CUN("0");
			    flexLog("Input data error " + e);
			}
			try {
			 	if (req.getParameter("E01IDN") != null && 
			 		req.getParameter("E01IDN").indexOf("-") != -1	){
//			 	 For PANAMA LONG IDENTIFICATION
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));			 		
			 	}else {
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01IDN("");
			    flexLog("Input data error " + e);
			}
		 	//msgClientPersonal.setE01IDN("");
			msgClientPersonal.send();	
		 	msgClientPersonal.destroy();
		 	flexLog("ESD008001 Message Sent");
		 	
		}		
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		// Receive Error Message
		try
		{
		  newmessage = mcID.receiveMessage();
		  
		  if (newmessage.getFormatName().equals("ELEERR")) {
			msgError = (ELEERRMessage)newmessage;
			IsNotError = msgError.getERRNUM().equals("0");
			flexLog("IsNotError = " + IsNotError);
			showERROR(msgError);
		  }
		  else
			flexLog("Message " + newmessage.getFormatName() + " received.");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}	
		
		// Receive Data
		try
		{
			newmessage = mcID.receiveMessage();
			
			if (newmessage.getFormatName().equals("ESD008001")) {
				try {
					msgClientPersonal = new datapro.eibs.beans.ESD008001Message();
					flexLog("ESD008001 Message Received");
			  	} catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}

				msgClientPersonal = (ESD008001Message)newmessage;

				userPO.setCusNum(msgClientPersonal.getE01CUN());
				userPO.setOption("CLIENT_P");
				userPO.setCusType(msgClientPersonal.getE01LGT());
				userPO.setHeader1(msgClientPersonal.getE01CUN());
				userPO.setHeader2(msgClientPersonal.getE01IDN());
				userPO.setHeader3(msgClientPersonal.getE01NA1());
				
				if (!msgClientPersonal.getE01IDN().equals("") &&
					msgClientPersonal.getE01IDN().indexOf("-") != -1 ) {
					
					userPO.setHeader2(msgClientPersonal.getE01IDN());
				}
				
				
				flexLog("Putting java beans into the session");

				if (!IsNotError) {  // There are no errors
					try {
						
						flexLog("About to call Page: /pages/" + LangPath + "ESD4050_familia_both_enter.jsp");
						callPage(LangPath + "ESD4050_familia_both_enter.jsp", req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
			
			else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		}
		catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		// Send Initial data
		try{
			msgPlanFamilia = (ESD405001Message)mc.getMessageRecord("ESD405001");
			msgPlanFamilia.setH01USR(user.getH01USR());
			msgPlanFamilia.setH01PGM("ESD4050");
			msgPlanFamilia.setH01TIM(getTimeStamp());
			msgPlanFamilia.setH01SCR("01");
			msgPlanFamilia.setH01OPE("0014");
		 	try {
			 	if (req.getParameter("E01CUN") != null){
			 		msgPlanFamilia.setL01FILLE1(msgClientPersonal.getE01CUN()!=null ? msgClientPersonal.getE01CUN() : userPO.getCusNum());
			 		userPO.setCusNum(msgPlanFamilia.getL01FILLE1().toString());
			 	}
			}catch (Exception e){
				msgPlanFamilia.setL01FILLE1("0");
			    flexLog("Input data error " + e);
			}
			
			try {
			 	if (req.getParameter("E01IDN") != null && 
			 		req.getParameter("E01IDN").indexOf("-") != -1	){
//			 	 For PANAMA LONG IDENTIFICATION
			 		msgPlanFamilia.setL01FILLE5(msgClientPersonal.getE01IDN());
			 		userPO.setHeader2(msgPlanFamilia.getL01FILLE5().toString());
			 	}else {
			 		msgPlanFamilia.setL01FILLE5(msgClientPersonal.getE01IDN());
			 		userPO.setHeader2(msgPlanFamilia.getL01FILLE5().toString());
			 	}
			}catch (Exception e){
				msgPlanFamilia.setL01FILLE5("");
			    flexLog("Input data error " + e);
			}
			//se desbloquea cuando este listo el programa
		 	msgPlanFamilia.send();	
			msgPlanFamilia.destroy();
		 	flexLog("ESD405001 Message Sent");	
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		//recibe data
		try{
			newmessage2 = mc.receiveMessage();
			if(newmessage2.getFormatName().equals("ESD405001")){
				try {
					msgPlanFamilia = new datapro.eibs.beans.ESD405001Message();
					flexLog("ESD405001 Message Received");
			  	} catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}
			  	
			  	
			  	msgPlanFamilia = (ESD405001Message)newmessage2;
			  	userPO.setCusNum(msgPlanFamilia.getL01FILLE1());
				userPO.setOption("CLIENT_P");
				req.setAttribute("IDN", msgClientPersonal.getE01IDN());
				
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("planFamilia", msgPlanFamilia);
				session.setAttribute("userPO", userPO);
				if (IsNotError) { 
					try{	
						if (msgPlanFamilia.getL01FILLE4().equals("")){
							flexLog("ESD405001 Message Sent");
							callPage(LangPath + "ESD4050_plan_familia_validation.jsp", req, res);
						}else if (msgPlanFamilia.getL01FILLE4().equals("SPS")){
							flexLog("ESD405001 Message Sent");
							callPage(LangPath + "ESD4050_plan_familia_selection.jsp", req, res);
						}else{
							procActionMaintanceFamilia(mc, user, req, res, session, screen);
						}

					}catch (Exception e) {
						flexLog("Exception calling page " + e);
						flexLog("About to call Page: /pages/" + LangPath + "ESD4050_familia_both_enter.jsp");
						callPage(LangPath + "ESD4050_familia_both_enter.jsp", req, res);
					}
				}
				else {  // There are errors
					try {
						flexLog("About to call Page: /pages/" + LangPath + "ESD4050_familia_both_enter.jsp");
						callPage(LangPath + "ESD4050_familia_both_enter.jsp", req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
	}//fin metodo procActionEnterIntegrantNew
	
	
	/**
	 * 
	 * @param mc
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @param screen
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionEnterFamiliaNew(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		
		MessageRecord newmessage = null;
		MessageRecord newmessage2 = null;
		MessageContext mcID = null;
		ESD008001Message msgClientPersonal = null;
		ESD405001Message msgPlanFamilia = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		boolean IsNotError = false;
		
		//---------------------LLamada al RP de vinculos --------------------------
		MessageContext mc1 = null;
    	String codeflag = "FL";
      	int rows = 0;
      	int posIni = 0;
      	int posEnd = 0;
      	String marker = "";
      	//String selNew = "";
      	String selOld = "";
      	//String fromRec = "0";
      	boolean firstTime = true;
      	JBList beanList = new JBList();
		try{
			mc1 = new MessageContext(new MessageProcessor("EWD0002").getMessageHandler());
       	}
       	catch (Exception e)
       	{
       		e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
       	}
       	
       	MessageRecord newmessage3 = null;
       	EWD0002DSMessage msgHelp = null;
       	
        // Send Request
       	try{
       		msgHelp = (EWD0002DSMessage)mc.getMessageRecord("EWD0002DS");
       	 	msgHelp.setEWDTBL(codeflag);
       	 	
       	 	msgHelp.send();	
       	 	msgHelp.destroy();
       	}		
       	catch (Exception e){
       	  e.printStackTrace();
       	  flexLog("Send Client Header Information error " + e);
       	}
       	
        // Receive Help
       	try{
       		newmessage3 = mc.receiveMessage();
       		
       		if (newmessage3.getFormatName().equals("EWD0002DS")) {
                msgHelp =  (EWD0002DSMessage)newmessage3;
         	}
       		
       		StringBuffer myRow = null;
       		
       		int ct = 0;
			while (ct++ < datapro.eibs.master.JSEIBSProp.getMaxIterations()) {
				newmessage3 = mc.receiveMessage();
				if (newmessage3.getFormatName().equals("EWD0002DS")) {
					msgHelp =  (EWD0002DSMessage)newmessage3;
                    marker = msgHelp.getEWDOPE();
                    if ( marker.equals("*") ) {
                    	break;
					}
					if (firstTime) {
						firstTime = false;
				        posIni= Integer.parseInt(msgHelp.getEWDREC().trim());
			     	}
                    String myCode = null;
                    String myDesc = null;
                    
                    myCode = msgHelp.getEWDCOD().trim();
                    myDesc = msgHelp.getEWDDSC().trim();
                    selOld = msgHelp.getEWDSHO().trim();
                    
                    myRow = new StringBuffer("");
                    
                    myRow.append(myCode+","+myDesc);
                    
                    
                    beanList.addRow("", myRow.toString());
					rows++;
   						
   					if (marker.equals("+")) {
   						posEnd= Integer.parseInt(msgHelp.getEWDREC().trim());
						break;
					}
   				}
          	   	else {
          	   		flexLog("Message " + newmessage3.getFormatName() + " received.");
          		    break;
          		}
       		}
       		
       	}catch(Exception e){
       		flexLog("Error Read: " + e);
       	}
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		
		// Send Initial data
		try
		{
			mcID = new MessageContext(super.getMessageHandler("ESD0080", req));
			msgClientPersonal = (ESD008001Message)mcID.getMessageRecord("ESD008001");
		 	msgClientPersonal.setH01USR(user.getH01USR());
		 	msgClientPersonal.setH01PGM("ESD0080");
		 	msgClientPersonal.setH01TIM(getTimeStamp());
		 	msgClientPersonal.setH01SCR("01");
		 	msgClientPersonal.setH01OPE("0002");
		 	try {
			 	if (req.getParameter("E01CUN") != null){
				 	msgClientPersonal.setE01CUN(req.getParameter("E01CUN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01CUN("0");
			    flexLog("Input data error " + e);
			}
			try {
			 	if (req.getParameter("E01IDN") != null && 
			 		req.getParameter("E01IDN").indexOf("-") != -1	){
//			 	 For PANAMA LONG IDENTIFICATION
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));			 		
			 	}else {
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01IDN("");
			    flexLog("Input data error " + e);
			}
		 	//msgClientPersonal.setE01IDN("");
			msgClientPersonal.send();	
		 	msgClientPersonal.destroy();
		 	flexLog("ESD008001 Message Sent");
		 	
		}		
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		// Receive Error Message
		try
		{
		  newmessage = mcID.receiveMessage();
		  
		  if (newmessage.getFormatName().equals("ELEERR")) {
			msgError = (ELEERRMessage)newmessage;
			IsNotError = msgError.getERRNUM().equals("0");
			flexLog("IsNotError = " + IsNotError);
			showERROR(msgError);
		  }
		  else
			flexLog("Message " + newmessage.getFormatName() + " received.");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}	
		
		// Receive Data
		try
		{
			newmessage = mcID.receiveMessage();
			
			if (newmessage.getFormatName().equals("ESD008001")) {
				try {
					msgClientPersonal = new datapro.eibs.beans.ESD008001Message();
					flexLog("ESD008001 Message Received");
			  	} catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}

				msgClientPersonal = (ESD008001Message)newmessage;

				userPO.setCusNum(msgClientPersonal.getE01CUN());
				userPO.setOption("CLIENT_P");
				userPO.setCusType(msgClientPersonal.getE01LGT());
				userPO.setHeader1(msgClientPersonal.getE01CUN());
				userPO.setHeader2(msgClientPersonal.getE01IDN());
				userPO.setHeader3(msgClientPersonal.getE01NA1());
				
				if (!msgClientPersonal.getE01IDN().equals("") &&
					msgClientPersonal.getE01IDN().indexOf("-") != -1 ) {
					
					userPO.setHeader2(msgClientPersonal.getE01IDN());
				}
				
				
				flexLog("Putting java beans into the session");

				if (!IsNotError) {  // There are no errors
					try {
						
						flexLog("About to call Page: /pages/" + LangPath + "ESD4050_familia_both_enter.jsp");
						callPage(LangPath + "ESD4050_familia_both_enter.jsp", req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
			
			else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		}
		catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		MessageProcessor mp = null;	
		// Send Initial data
		try{
			mp = getMessageProcessor("ESD4050", req);
			
			msgPlanFamilia = (ESD405001Message)mc.getMessageRecord("ESD405001");
			msgPlanFamilia.setH01USR(user.getH01USR());
			msgPlanFamilia.setH01PGM("ESD4050");
			msgPlanFamilia.setH01TIM(getTimeStamp());
			msgPlanFamilia.setH01SCR("01");
			msgPlanFamilia.setH01OPE("0015");
			msgPlanFamilia.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPlanFamilia.setL01FILLE5(userPO.getHeader2().toString());
			
			//se desbloquea cuando este listo el programa
			mp.sendMessage(msgPlanFamilia);
			
			JBObjList list = mp.receiveMessageRecordList("H01MAS");
			
			if(mp.hasError(list)){
				session.setAttribute("error", mp.getError(list));
				forward("error_viewer.jsp", req, res);
			}else{
				boolean tienePlanFamilia = false;
				list.initRow();
				while (list.getNextRow()) {
					ESD405001Message msgList = (ESD405001Message) list.getRecord();
					if (msgList.getL01FILLE3().equals("1")){
						tienePlanFamilia = true;
					}
				}
				msgPlanFamilia.setE01PEST("PS" + userPO.getCusNum());
				userPO.setPurpose("PLANFAMILIA");
				session.setAttribute("userPO", userPO);
				session.setAttribute("ESD405001List", list);
				session.setAttribute("ListaVinculo", beanList);				
				session.setAttribute("planFamilia", msgPlanFamilia);
				if (!tienePlanFamilia){
					flexLog("About to call Page: /pages/" + LangPath + "ESD4050_plan_familia_new.jsp");
					callPage(LangPath + "ESD4050_plan_familia_new.jsp", req, res);
				}else
					procActionMaintanceFamilia(mc, user, req, res, session, screen);
			}
			
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
	}//fin metodo procActionEnterFamiliaNew
	
	/**
	 * 
	 * @param mc
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @param screen
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionEnterFamiliaNewEnviar(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {

		ESD405001Message msgPlanFamilia = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		String planSocioTitular = "";
		
		String [] ruts = req.getParameter("RUTS").split(";");
		planSocioTitular = req.getParameter("IDPLANSOCIO");		
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		
		
		
		MessageProcessor mp = null;	
		
		MessageRecord msg = null;
		// Send Initial data
		try{
			
			for(int i=0; i < ruts.length;  i++){
				mp = getMessageProcessor("ESD4050", req);
				
				msgPlanFamilia = (ESD405001Message)mc.getMessageRecord("ESD405001");
				msgPlanFamilia.setH01USR(user.getH01USR());
				msgPlanFamilia.setH01PGM("ESD4050");
				msgPlanFamilia.setH01TIM(getTimeStamp());
				msgPlanFamilia.setH01SCR("01");
				msgPlanFamilia.setH01OPE("0010");
				msgPlanFamilia.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
				msgPlanFamilia.setL01FILLE5(userPO.getHeader2().toString());
				msgPlanFamilia.setE01PIPS(ruts[i].toString());
				msgPlanFamilia.setL01FILLE6(planSocioTitular);
				msgPlanFamilia.setE01PEST(req.getParameter("E01PSTCP"));
				
				//se desbloquea cuando este listo el programa
				mp.sendMessage(msgPlanFamilia);
				msg = mp.receiveMessageRecord();
			}
			
			if (!mp.hasError(msg)) {
				session.setAttribute("IPplanFamilia", "PF"+userPO.getCusNum());
				callPage(LangPath + "ESD4050_plan_familia_confirm.jsp", req, res);
			}
			
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
	}//fin metodo procActionEnterFamiliaNewEnviar
	
	/**
	 * 
	 * @param mc
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @param screen
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionEnterFamiliaIntegrantNew(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		
		MessageRecord newmessage = null;
		MessageRecord newmessage2 = null;
		MessageContext mcID = null;
		ESD008001Message msgClientPersonal = null;
		ESD405001Message msgPlanFamilia = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		boolean IsNotError = false;
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		
		// Send Initial data
		try
		{
			mcID = new MessageContext(super.getMessageHandler("ESD0080", req));
			msgClientPersonal = (ESD008001Message)mcID.getMessageRecord("ESD008001");
		 	msgClientPersonal.setH01USR(user.getH01USR());
		 	msgClientPersonal.setH01PGM("ESD0080");
		 	msgClientPersonal.setH01TIM(getTimeStamp());
		 	msgClientPersonal.setH01SCR("01");
		 	msgClientPersonal.setH01OPE("0002");
		 	try {
			 	if (req.getParameter("E01CUN") != null){
				 	msgClientPersonal.setE01CUN(req.getParameter("E01CUN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01CUN("0");
			    flexLog("Input data error " + e);
			}
			try {
			 	if (req.getParameter("E01IDN") != null && 
			 		req.getParameter("E01IDN").indexOf("-") != -1	){
//			 	 For PANAMA LONG IDENTIFICATION
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));			 		
			 	}else {
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01IDN("");
			    flexLog("Input data error " + e);
			}
		 	//msgClientPersonal.setE01IDN("");
			msgClientPersonal.send();	
		 	msgClientPersonal.destroy();
		 	flexLog("ESD008001 Message Sent");
		 	
		}		
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		// Receive Error Message
		try
		{
		  newmessage = mcID.receiveMessage();
		  
		  if (newmessage.getFormatName().equals("ELEERR")) {
			msgError = (ELEERRMessage)newmessage;
			IsNotError = msgError.getERRNUM().equals("0");
			flexLog("IsNotError = " + IsNotError);
			showERROR(msgError);
		  }
		  else
			flexLog("Message " + newmessage.getFormatName() + " received.");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}	
		
		// Receive Data
		try
		{
			newmessage = mcID.receiveMessage();
			
			if (newmessage.getFormatName().equals("ESD008001")) {
				try {
					msgClientPersonal = new datapro.eibs.beans.ESD008001Message();
					flexLog("ESD008001 Message Received");
			  	} catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}

				msgClientPersonal = (ESD008001Message)newmessage;

				userPO.setCusNum(msgClientPersonal.getE01CUN());
				userPO.setOption("CLIENT_P");
				userPO.setCusType(msgClientPersonal.getE01LGT());
				userPO.setHeader1(msgClientPersonal.getE01CUN());
				userPO.setHeader2(msgClientPersonal.getE01IDN());
				userPO.setHeader3(msgClientPersonal.getE01NA1());
				
				if (!msgClientPersonal.getE01IDN().equals("") &&
					msgClientPersonal.getE01IDN().indexOf("-") != -1 ) {
					
					userPO.setHeader2(msgClientPersonal.getE01IDN());
				}
				
				
				flexLog("Putting java beans into the session");

				if (!IsNotError) {  // There are no errors
					try {
						
						flexLog("About to call Page: /pages/" + LangPath + "ESD4050_familia_both_enter.jsp");
						callPage(LangPath + "ESD4050_familia_both_enter.jsp", req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
			
			else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		}
		catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		MessageProcessor mp = null;	
		// Send Initial data
		try{
			mp = getMessageProcessor("ESD4050", req);
			
			msgPlanFamilia = (ESD405001Message)mc.getMessageRecord("ESD405001");
			msgPlanFamilia.setH01USR(user.getH01USR());
			msgPlanFamilia.setH01PGM("ESD4050");
			msgPlanFamilia.setH01TIM(getTimeStamp());
			msgPlanFamilia.setH01SCR("01");
			msgPlanFamilia.setH01OPE("0017");
			msgPlanFamilia.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPlanFamilia.setL01FILLE5(userPO.getHeader2().toString());
			
			//se desbloquea cuando este listo el programa
			mp.sendMessage(msgPlanFamilia);
			
			JBObjList list = mp.receiveMessageRecordList("H01MAS");
			
			if(mp.hasError(list)){
				session.setAttribute("error", mp.getError(list));
				forward("error_viewer.jsp", req, res);
			}else{
				msgPlanFamilia.setE01PEST("PS" + userPO.getCusNum());
				userPO.setPurpose("PLANFAMILIA");
				session.setAttribute("userPO", userPO);
				session.setAttribute("ESD405001List", list);
				session.setAttribute("planFamilia", msgPlanFamilia);
				flexLog("About to call Page: /pages/" + LangPath + "ESD4050_plan_familia_integrant_new.jsp");
				callPage(LangPath + "ESD4050_plan_familia_integrant_new.jsp", req, res);
			}
			
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
	}//fin metodo procActionEnterIntegrantNew
	
	
	/**
	 * 
	 * @param mc
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @param screen
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionEnterIntegrantNewEnviar(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {

		ESD405001Message msgPlanFamilia = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		String idPlanSocio = "";
		String rutSocioTitular = "";
		String idPlanSocioTitular = "";
		String cunSocioTitular = "";
		
		idPlanSocio = req.getParameter("IDPLANSOCIO");
		rutSocioTitular = req.getParameter("RUT");
		idPlanSocioTitular = req.getParameter("IDPLANSOCIOTITULAR");
		cunSocioTitular = idPlanSocioTitular.substring(2);
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");	
		
		MessageProcessor mp = null;	
		
		MessageRecord msg = null;
		// Send Initial data
		try{
			mp = getMessageProcessor("ESD4050", req);
			
			msgPlanFamilia = (ESD405001Message)mc.getMessageRecord("ESD405001");
			msgPlanFamilia.setH01USR(user.getH01USR());
			msgPlanFamilia.setH01PGM("ESD4050");
			msgPlanFamilia.setH01TIM(getTimeStamp());
			msgPlanFamilia.setH01SCR("01");
			msgPlanFamilia.setH01OPE("0011");
			msgPlanFamilia.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPlanFamilia.setL01FILLE2(cunSocioTitular);
			msgPlanFamilia.setL01FILLE5(userPO.getHeader2().toString());
			msgPlanFamilia.setE01PIPS(rutSocioTitular);
			msgPlanFamilia.setL01FILLE4(idPlanSocioTitular);
			msgPlanFamilia.setL01FILLE6(idPlanSocio);
			
			//se desbloquea cuando este listo el programa
			mp.sendMessage(msgPlanFamilia);
			msg = mp.receiveMessageRecord();
			
			if (!mp.hasError(msg)) {
				callPage(LangPath + "ESD4050_plan_familia_selection.jsp", req, res);
			}
			
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
	}//fin metodo procActionEnterIntegrantNewEnviar
	
	
	protected void procReqMaintance(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses) throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos	userPO = null;	

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos(); 
			userPO.setOption("PFAMILIA");
			userPO.setPurpose("MAINTANCE");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex); 
		}

		try {
			flexLog("About to call Page: " + LangPath + "ESD4050_familia_maintance_both_enter.jsp");
			callPage(LangPath + "ESD4050_familia_maintance_both_enter.jsp", req, res);	
		}
		catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}
	
	protected void procActionMaintanceFamilia(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		MessageRecord newmessage = null;
		MessageContext mcID = null;
		ESD008001Message msgClientPersonal = null;
		ESD405001Message msgPlanFamilia = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		boolean IsNotError = false;
		
		//---------------------LLamada al RP de vinculos --------------------------
		MessageContext mc1 = null;
    	String codeflag = "FL";
      	int rows = 0;
      	int posIni = 0;
      	int posEnd = 0;
      	String marker = "";
      	//String selNew = "";
      	String selOld = "";
      	//String fromRec = "0";
      	boolean firstTime = true;
		JBList beanList = new JBList();
		try{
			mc1 = new MessageContext(new MessageProcessor("EWD0002").getMessageHandler());
       	}
       	catch (Exception e)
       	{
       		e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
       	}
       	
       	MessageRecord newmessage3 = null;
       	EWD0002DSMessage msgHelp = null;
       	
        // Send Request
       	try{
       		msgHelp = (EWD0002DSMessage)mc.getMessageRecord("EWD0002DS");
       	 	msgHelp.setEWDTBL(codeflag);
       	 	
       	 	msgHelp.send();	
       	 	msgHelp.destroy();
       	}		
       	catch (Exception e){
       	  e.printStackTrace();
       	  flexLog("Send Client Header Information error " + e);
       	}
       	
        // Receive Help
       	try{
       		newmessage3 = mc.receiveMessage();
       		
       		if (newmessage3.getFormatName().equals("EWD0002DS")) {
                msgHelp =  (EWD0002DSMessage)newmessage3;
         	}
       		
       		StringBuffer myRow = null;
       		
       		int ct = 0;
			while (ct++ < datapro.eibs.master.JSEIBSProp.getMaxIterations()) {
				newmessage3 = mc.receiveMessage();
				if (newmessage3.getFormatName().equals("EWD0002DS")) {
					msgHelp =  (EWD0002DSMessage)newmessage3;
                    marker = msgHelp.getEWDOPE();
                    if ( marker.equals("*") ) {
                    	break;
					}
					if (firstTime) {
						firstTime = false;
				        posIni= Integer.parseInt(msgHelp.getEWDREC().trim());
			     	}
                    String myCode = null;
                    String myDesc = null;
                    
                    myCode = msgHelp.getEWDCOD().trim();
                    myDesc = msgHelp.getEWDDSC().trim();
                    selOld = msgHelp.getEWDSHO().trim();
                    
                    myRow = new StringBuffer("");
                    
                    myRow.append(myCode+","+myDesc);
                    
                    
                    beanList.addRow("", myRow.toString());
					rows++;
   						
   					if (marker.equals("+")) {
   						posEnd= Integer.parseInt(msgHelp.getEWDREC().trim());
						break;
					}
   				}
          	   	else {
          	   		flexLog("Message " + newmessage3.getFormatName() + " received.");
          		    break;
          		}
       		}
       		
       	}catch(Exception e){
       		flexLog("Error Read: " + e);
       	}
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		
		// Send Initial data
		try
		{
			mcID = new MessageContext(super.getMessageHandler("ESD0080", req));
			msgClientPersonal = (ESD008001Message)mcID.getMessageRecord("ESD008001");
		 	msgClientPersonal.setH01USR(user.getH01USR());
		 	msgClientPersonal.setH01PGM("ESD0080");
		 	msgClientPersonal.setH01TIM(getTimeStamp());
		 	msgClientPersonal.setH01SCR("01");
		 	msgClientPersonal.setH01OPE("0002");
		 	try {
			 	if (req.getParameter("E01CUN") != null){
				 	msgClientPersonal.setE01CUN(req.getParameter("E01CUN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01CUN("0");
			    flexLog("Input data error " + e);
			}
			try {
			 	if (req.getParameter("E01IDN") != null && 
			 		req.getParameter("E01IDN").indexOf("-") != -1	){
//			 	 For PANAMA LONG IDENTIFICATION
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));			 		
			 	}else {
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01IDN("");
			    flexLog("Input data error " + e);
			}
		 	//msgClientPersonal.setE01IDN("");
			msgClientPersonal.send();	
		 	msgClientPersonal.destroy();
		 	flexLog("ESD008001 Message Sent");
		 	
		}		
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		// Receive Error Message
		try
		{
		  newmessage = mcID.receiveMessage();
		  
		  if (newmessage.getFormatName().equals("ELEERR")) {
			msgError = (ELEERRMessage)newmessage;
			IsNotError = msgError.getERRNUM().equals("0");
			flexLog("IsNotError = " + IsNotError);
			showERROR(msgError);
		  }
		  else
			flexLog("Message " + newmessage.getFormatName() + " received.");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}	
		
		// Receive Data
		try
		{
			newmessage = mcID.receiveMessage();
			
			if (newmessage.getFormatName().equals("ESD008001")) {
				try {
					msgClientPersonal = new datapro.eibs.beans.ESD008001Message();
					flexLog("ESD008001 Message Received");
			  	} catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}

				msgClientPersonal = (ESD008001Message)newmessage;

				userPO.setCusNum(msgClientPersonal.getE01CUN());
				userPO.setOption("CLIENT_P");
				userPO.setCusType(msgClientPersonal.getE01LGT());
				userPO.setHeader1(msgClientPersonal.getE01CUN());
				userPO.setHeader2(msgClientPersonal.getE01IDN());
				userPO.setHeader3(msgClientPersonal.getE01NA1());
				
				if (!msgClientPersonal.getE01IDN().equals("") &&
					msgClientPersonal.getE01IDN().indexOf("-") != -1 ) {
					
					userPO.setHeader2(msgClientPersonal.getE01IDN());
				}
				
				
				flexLog("Putting java beans into the session");

				if (!IsNotError) {  // There are no errors
					try {
						
						flexLog("About to call Page: /pages/" + LangPath + "ESD4050_familia_maintance_both_enter.jsp");
						callPage(LangPath + "ESD4050_familia_maintance_both_enter.jsp", req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
			
			else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		}
		catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		MessageProcessor mp = null;	
		// Send Initial data
		try{
			mp = getMessageProcessor("ESD4050", req);
			
			msgPlanFamilia = (ESD405001Message)mc.getMessageRecord("ESD405001");
			msgPlanFamilia.setH01USR(user.getH01USR());
			msgPlanFamilia.setH01PGM("ESD4050");
			msgPlanFamilia.setH01TIM(getTimeStamp());
			msgPlanFamilia.setH01SCR("01");
			msgPlanFamilia.setH01OPE("0015");
			msgPlanFamilia.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPlanFamilia.setL01FILLE5(userPO.getHeader2().toString());			
			
			//se desbloquea cuando este listo el programa
			mp.sendMessage(msgPlanFamilia);
			
			JBObjList list = mp.receiveMessageRecordList("H01MAS");
			
			if(mp.hasError(list)){
				session.setAttribute("error", mp.getError(list));
				forward("error_viewer.jsp", req, res);
			}else{
				list.initRow();
				while (list.getNextRow()) {
					ESD405001Message msgList = (ESD405001Message) list.getRecord();
					msgPlanFamilia.setL01FILLE4(msgList.getL01FILLE4());
					msgPlanFamilia.setL01FILLE5(msgList.getL01FILLE5());
					msgPlanFamilia.setL01FILLE6(msgList.getL01FILLE6());
					break;
				}
				msgPlanFamilia.setE01PEST("PF" + userPO.getCusNum());
				userPO.setPurpose("PLANFAMILIA");
				session.setAttribute("userPO", userPO);
				session.setAttribute("ESD405001List", list);
				session.setAttribute("ListaVinculo", beanList);				
				session.setAttribute("planFamilia", msgPlanFamilia);
				flexLog("About to call Page: /pages/" + LangPath + "ESD4050_plan_familia_maintance.jsp");
				callPage(LangPath + "ESD4050_plan_familia_maintance.jsp", req, res);
			}
			
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
	}//fin metodo procActionMaintanceFamilia

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqEnterConsult(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
		throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
	
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos(); 
			userPO.setOption("PSOCIO");
			userPO.setPurpose("CONSULT");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);
		} catch (Exception ex) {
			flexLog("Error: " + ex); 
		}

		try {
			flexLog("About to call Page: " + LangPath + "ESD4050_familia_consult_both_enter.jsp");
			callPage(LangPath + "ESD4050_familia_consult_both_enter.jsp", req, res);
		}catch (Exception e) {
			flexLog("Exception calling page " + e);
		}
	}
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	protected void procActionEnterFamiliaConsult(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		MessageRecord newmessage = null;
		MessageContext mcID = null;
		ESD008001Message msgClientPersonal = null;
		ESD405001Message msgPlanFamilia = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		boolean IsNotError = false;
		
		//---------------------LLamada al RP de vinculos --------------------------
		MessageContext mc1 = null;
    	String codeflag = "FL";
      	int rows = 0;
      	int posIni = 0;
      	int posEnd = 0;
      	String marker = "";
      	//String selNew = "";
      	String selOld = "";
      	//String fromRec = "0";
      	boolean firstTime = true;
		JBList beanList = new JBList();
		try{
			mc1 = new MessageContext(new MessageProcessor("EWD0002").getMessageHandler());
       	}
       	catch (Exception e)
       	{
       		e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
       	}
       	
       	MessageRecord newmessage3 = null;
       	EWD0002DSMessage msgHelp = null;
       	
        // Send Request
       	try{
       		msgHelp = (EWD0002DSMessage)mc.getMessageRecord("EWD0002DS");
       	 	msgHelp.setEWDTBL(codeflag);
       	 	
       	 	msgHelp.send();	
       	 	msgHelp.destroy();
       	}		
       	catch (Exception e){
       	  e.printStackTrace();
       	  flexLog("Send Client Header Information error " + e);
       	}
       	
        // Receive Help
       	try{
       		newmessage3 = mc.receiveMessage();
       		
       		if (newmessage3.getFormatName().equals("EWD0002DS")) {
                msgHelp =  (EWD0002DSMessage)newmessage3;
         	}
       		
       		StringBuffer myRow = null;
       		
       		int ct = 0;
			while (ct++ < datapro.eibs.master.JSEIBSProp.getMaxIterations()) {
				newmessage3 = mc.receiveMessage();
				if (newmessage3.getFormatName().equals("EWD0002DS")) {
					msgHelp =  (EWD0002DSMessage)newmessage3;
                    marker = msgHelp.getEWDOPE();
                    if ( marker.equals("*") ) {
                    	break;
					}
					if (firstTime) {
						firstTime = false;
				        posIni= Integer.parseInt(msgHelp.getEWDREC().trim());
			     	}
                    String myCode = null;
                    String myDesc = null;
                    
                    myCode = msgHelp.getEWDCOD().trim();
                    myDesc = msgHelp.getEWDDSC().trim();
                    selOld = msgHelp.getEWDSHO().trim();
                    
                    myRow = new StringBuffer("");
                    
                    myRow.append(myCode+","+myDesc);
                    
                    
                    beanList.addRow("", myRow.toString());
					rows++;
   						
   					if (marker.equals("+")) {
   						posEnd= Integer.parseInt(msgHelp.getEWDREC().trim());
						break;
					}
   				}
          	   	else {
          	   		flexLog("Message " + newmessage3.getFormatName() + " received.");
          		    break;
          		}
       		}
       		
       	}catch(Exception e){
       		flexLog("Error Read: " + e);
       	}
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		
		// Send Initial data
		try
		{
			mcID = new MessageContext(super.getMessageHandler("ESD0080", req));
			msgClientPersonal = (ESD008001Message)mcID.getMessageRecord("ESD008001");
		 	msgClientPersonal.setH01USR(user.getH01USR());
		 	msgClientPersonal.setH01PGM("ESD0080");
		 	msgClientPersonal.setH01TIM(getTimeStamp());
		 	msgClientPersonal.setH01SCR("01");
		 	msgClientPersonal.setH01OPE("0002");
		 	try {
			 	if (req.getParameter("E01CUN") != null){
				 	msgClientPersonal.setE01CUN(req.getParameter("E01CUN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01CUN("0");
			    flexLog("Input data error " + e);
			}
			try {
			 	if (req.getParameter("E01IDN") != null && 
			 		req.getParameter("E01IDN").indexOf("-") != -1	){
//			 	 For PANAMA LONG IDENTIFICATION
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));			 		
			 	}else {
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01IDN("");
			    flexLog("Input data error " + e);
			}
		 	//msgClientPersonal.setE01IDN("");
			msgClientPersonal.send();	
		 	msgClientPersonal.destroy();
		 	flexLog("ESD008001 Message Sent");
		 	
		}		
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		// Receive Error Message
		try
		{
		  newmessage = mcID.receiveMessage();
		  
		  if (newmessage.getFormatName().equals("ELEERR")) {
			msgError = (ELEERRMessage)newmessage;
			IsNotError = msgError.getERRNUM().equals("0");
			flexLog("IsNotError = " + IsNotError);
			showERROR(msgError);
		  }
		  else
			flexLog("Message " + newmessage.getFormatName() + " received.");
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}	
		
		// Receive Data
		try
		{
			newmessage = mcID.receiveMessage();
			
			if (newmessage.getFormatName().equals("ESD008001")) {
				try {
					msgClientPersonal = new datapro.eibs.beans.ESD008001Message();
					flexLog("ESD008001 Message Received");
			  	} catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}

				msgClientPersonal = (ESD008001Message)newmessage;

				userPO.setCusNum(msgClientPersonal.getE01CUN());
				userPO.setOption("CLIENT_P");
				userPO.setCusType(msgClientPersonal.getE01LGT());
				userPO.setHeader1(msgClientPersonal.getE01CUN());
				userPO.setHeader2(msgClientPersonal.getE01IDN());
				userPO.setHeader3(msgClientPersonal.getE01NA1());
				
				if (!msgClientPersonal.getE01IDN().equals("") &&
					msgClientPersonal.getE01IDN().indexOf("-") != -1 ) {
					
					userPO.setHeader2(msgClientPersonal.getE01IDN());
				}
				
				
				flexLog("Putting java beans into the session");

				if (!IsNotError) {  // There are no errors
					try {
						
						flexLog("About to call Page: /pages/" + LangPath + "ESD4050_familia_consult_both_enter.jsp");
						callPage(LangPath + "ESD4050_familia_consult_both_enter.jsp", req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
			
			else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		}
		catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		MessageProcessor mp = null;	
		// Send Initial data
		try{
			mp = getMessageProcessor("ESD4050", req);
			
			msgPlanFamilia = (ESD405001Message)mc.getMessageRecord("ESD405001");
			msgPlanFamilia.setH01USR(user.getH01USR());
			msgPlanFamilia.setH01PGM("ESD4050");
			msgPlanFamilia.setH01TIM(getTimeStamp());
			msgPlanFamilia.setH01SCR("01");
			msgPlanFamilia.setH01OPE("0015");
			msgPlanFamilia.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPlanFamilia.setL01FILLE5(userPO.getHeader2().toString());
			msgPlanFamilia.setL01FILLE5("PF" + userPO.getCusNum());
			
			//se desbloquea cuando este listo el programa
			mp.sendMessage(msgPlanFamilia);
			
			JBObjList list = mp.receiveMessageRecordList("H01MAS");
			
			if(mp.hasError(list)){
				session.setAttribute("error", mp.getError(list));
				forward("error_viewer.jsp", req, res);
			}else{
				msgPlanFamilia.setE01PEST("PF" + userPO.getCusNum());
				userPO.setPurpose("PLANFAMILIA");
				session.setAttribute("userPO", userPO);
				session.setAttribute("ESD405001List", list);
				session.setAttribute("ListaVinculo", beanList);				
				session.setAttribute("planFamilia", msgPlanFamilia);
				flexLog("About to call Page: /pages/" + LangPath + "ESD4050_plan_familia_consult.jsp");
				callPage(LangPath + "ESD4050_plan_familia_consult.jsp", req, res);
			}
			
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
	}//fin metodo procActionEnterFamiliaConsult
	
	/**
	 * 
	 * @param mc
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @param screen
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintanceFamiliaEnviar(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {

		ESD405001Message msgPlanFamilia = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		String planSocioTitular = "";
		
		String [] ruts = req.getParameter("RUTSINVITADOS").split(";");
		planSocioTitular = req.getParameter("IDPLANSOCIO");		
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		
		
		
		MessageProcessor mp = null;	
		
		MessageRecord msg = null;
		// Send Initial data
		try{
			
			for(int i=0; i < ruts.length;  i++){
				String [] datos = ruts[i].split(",");
				mp = getMessageProcessor("ESD4050", req);
				
				msgPlanFamilia = (ESD405001Message)mc.getMessageRecord("ESD405001");
				msgPlanFamilia.setH01USR(user.getH01USR());
				msgPlanFamilia.setH01PGM("ESD4050");
				msgPlanFamilia.setH01TIM(getTimeStamp());
				msgPlanFamilia.setH01SCR("01");
				msgPlanFamilia.setH01OPE("0018");
				msgPlanFamilia.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
				msgPlanFamilia.setL01FILLE5(userPO.getHeader2().toString());
				msgPlanFamilia.setE01PUNP(datos[1].toString());
				msgPlanFamilia.setE01PIPS(datos[0].toString());
				msgPlanFamilia.setE01PSNM(datos[2].toString());
				msgPlanFamilia.setL01FILLE6(planSocioTitular);
				msgPlanFamilia.setE01PEST(req.getParameter("E01PSTCP"));
				
				//se desbloquea cuando este listo el programa
				mp.sendMessage(msgPlanFamilia);
				msg = mp.receiveMessageRecord();
			}
			
			if (!mp.hasError(msg)) {
				session.setAttribute("IPplanFamilia", "PF"+userPO.getCusNum());
				callPage(LangPath + "ESD4050_plan_familia_confirm.jsp", req, res);
			}
			
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
	}//fin metodo procActionMaintanceFamiliaEnviar
}

