/**
 * Response.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public class Response  implements java.io.Serializable {
    private _125._62._21._10.Excepcion error;
    private _125._62._21._10.Tasa tasa;
    private _125._62._21._10.Salida salida;

    public Response() {
    }

    public _125._62._21._10.Excepcion getError() {
        return error;
    }

    public void setError(_125._62._21._10.Excepcion error) {
        this.error = error;
    }

    public _125._62._21._10.Tasa getTasa() {
        return tasa;
    }

    public void setTasa(_125._62._21._10.Tasa tasa) {
        this.tasa = tasa;
    }

    public _125._62._21._10.Salida getSalida() {
        return salida;
    }

    public void setSalida(_125._62._21._10.Salida salida) {
        this.salida = salida;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        Response history = (Response) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        Response other = (Response) obj;
        boolean _equals;
        _equals = true
            && ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError())))
            && ((this.tasa==null && other.getTasa()==null) || 
             (this.tasa!=null &&
              this.tasa.equals(other.getTasa())))
            && ((this.salida==null && other.getSalida()==null) || 
             (this.salida!=null &&
              this.salida.equals(other.getSalida())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        Response history = (Response) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getTasa() != null) {
            _hashCode += getTasa().hashCode();
        }
        if (getSalida() != null) {
            _hashCode += getSalida().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
