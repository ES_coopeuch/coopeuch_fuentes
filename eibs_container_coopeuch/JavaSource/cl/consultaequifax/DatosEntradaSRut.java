/**
 * DatosEntradaSRut.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.consultaequifax;

public class DatosEntradaSRut  implements java.io.Serializable {
    private java.lang.String rutCliente;
    private java.lang.String canalOrigen;
    private java.lang.String tipoEvaluacion;
    private int rentaLiquida;

    public DatosEntradaSRut() {
    }

    public java.lang.String getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(java.lang.String rutCliente) {
        this.rutCliente = rutCliente;
    }

    public java.lang.String getCanalOrigen() {
        return canalOrigen;
    }

    public void setCanalOrigen(java.lang.String canalOrigen) {
        this.canalOrigen = canalOrigen;
    }

    public java.lang.String getTipoEvaluacion() {
        return tipoEvaluacion;
    }

    public void setTipoEvaluacion(java.lang.String tipoEvaluacion) {
        this.tipoEvaluacion = tipoEvaluacion;
    }

    public int getRentaLiquida() {
        return rentaLiquida;
    }

    public void setRentaLiquida(int rentaLiquida) {
        this.rentaLiquida = rentaLiquida;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        DatosEntradaSRut other = (DatosEntradaSRut) obj;
        boolean _equals;
        _equals = true
            && ((this.rutCliente==null && other.getRutCliente()==null) || 
             (this.rutCliente!=null &&
              this.rutCliente.equals(other.getRutCliente())))
            && ((this.canalOrigen==null && other.getCanalOrigen()==null) || 
             (this.canalOrigen!=null &&
              this.canalOrigen.equals(other.getCanalOrigen())))
            && ((this.tipoEvaluacion==null && other.getTipoEvaluacion()==null) || 
             (this.tipoEvaluacion!=null &&
              this.tipoEvaluacion.equals(other.getTipoEvaluacion())))
            && this.rentaLiquida == other.getRentaLiquida();
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        DatosEntradaSRut history = (DatosEntradaSRut) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        DatosEntradaSRut history = (DatosEntradaSRut) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getRutCliente() != null) {
            _hashCode += getRutCliente().hashCode();
        }
        if (getCanalOrigen() != null) {
            _hashCode += getCanalOrigen().hashCode();
        }
        if (getTipoEvaluacion() != null) {
            _hashCode += getTipoEvaluacion().hashCode();
        }
        _hashCode += getRentaLiquida();
        __hashHistory.set(null);
        return _hashCode;
    }

}
