package datapro.eibs.salesplatform;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;
import org.jdom.input.SAXBuilder;
import org.jdom.*;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV101004Message;
import datapro.eibs.beans.EPV201004Message;
import datapro.eibs.master.JSEIBSProp;

public class JMSMotorClient{
	
	static Logger logger = Logger.getLogger(JMSMotorClient.class);
	
	String msResp = "";
	

	/**
	 * method that parses the xml response 	 
	 * @return List 1.ELEERRMessage 2.-EPV101004Message
	 */
	@SuppressWarnings("unchecked")
	public List parseXMLResponse(){
		
		//define variables to return
		ELEERRMessage msgError = new ELEERRMessage();
		EPV101004Message msgResp = new EPV101004Message();
		
		if  (msResp==null || "".equals(msResp)){
			msgError.setERRNUM("1");
            msgError.setERNU01("01");//4
            msgError.setERDS01("No se Ha recibido Respuesta del Motor. Verifique Conexion.!");//70
		}else{
			SAXBuilder builder = new SAXBuilder();			
			Document doc;
			try {
				doc = builder.build(new StringReader(msResp));
				Element raiz = doc.getRootElement();
				//group Errors.
				Element errors = raiz.getChild("errors");
				List errList = errors.getChildren();
				if (errList!=null&&errList.size()>0){
					Element err = (Element)errList.get(0);
					String c = err.getAttributeValue("cod");
					String d =  err.getAttributeValue("desc");
					msgError.setERRNUM("1");
		            msgError.setERNU01((c.length()>4?(c.substring(0,4)):c));//4
	                msgError.setERDS01((d.length() >70?(d.substring(0,70)):d));//70	
	        		//send data
	        		List respuesta = new ArrayList();
	        		respuesta.add(msgError);
	        		respuesta.add(msgResp);		
	        		return respuesta;		                
				}				
				//Element of message
				String solicitud = (raiz.getChild("IdLlamada")).getText();
				String target = (raiz.getChild("BussinesObject")).getText();
				msgResp.setE04PVMNUM(solicitud.trim());
				msgResp.setE04BUSOBJ(target.trim());
				
				//firt group response
				Element result1 = raiz.getChild("Resultados1");
					//firt element FiltrosPoliticas
					String cdesSt =  result1.getChild("FiltrosPoliticas").getChild("CategoriaDeDecision").getText();			
					String desSt = result1.getChild("FiltrosPoliticas").getChild("Decision").getText();
					msgResp.setE04CODEC1(cdesSt.trim());
					msgResp.setE04DSCDE1(desSt.trim());
					//second element TablaDeReasonCodes 20 ocurrencias..
					List listResp = result1.getChild("TablaDeReasonCodes").getChildren();	
					int fil = 1;
					for (Iterator iterator = listResp.iterator(); iterator.hasNext();) {
						String fila = (fil>9)?""+fil:"0"+fil;
						Element resp = (Element) iterator.next();
						String cod = resp.getAttributeValue("cod");
						String desc = resp.getAttributeValue("desc");
						msgResp.getField("E04REAC"+fila).setString(cod.trim());
						msgResp.getField("E04DSCR"+fila).setString(desc.trim());
						if (fil ==20) break;//to ensure just 20					
						fil++;
					}//				
					//third element VarAux				
					String campanapdSt = result1.getChild("VarAux").getChild("Campanapd").getText();
					msgResp.setE04CODCAM(campanapdSt);
					
				//firt group response
				Element result2 = raiz.getChild("Resultados2");
					//firt element DecisionDeRiesgos
					String cdesicSt = result2.getChild("DecisionDeRiesgos").getChild("CategoriaDeDecision").getText();			
					String rcodeSt = result2.getChild("DecisionDeRiesgos").getChild("ReasonCode").getText();
					String desicSt = result2.getChild("DecisionDeRiesgos").getChild("Decision").getText();
					msgResp.setE04CATDEC(cdesicSt.trim());
					msgResp.setE04CODEC2(rcodeSt.trim());
					msgResp.setE04DSCDE2(desicSt.trim());				
					//second element DocumentacionRequerida 10 ocurrencias..
					List listDocRq = result2.getChild("DocumentacionRequerida").getChildren();

					int vuel = 1;
					for (Iterator iterator = listDocRq.iterator(); iterator.hasNext();) {	
						String fila = (vuel>9)?""+vuel:"0"+vuel;					
						Element resp = (Element) iterator.next();
						String id = resp.getAttributeValue("id");
						String desc = resp.getAttributeValue("desc");
						msgResp.getField("E04CODD"+fila).setString(id.trim());
						msgResp.getField("E04DSCD"+fila).setString(desc.trim());	
						if (vuel ==10) break;//to ensure just 10							
						vuel++;
						
					}//EoF
					//third element EvaluaciondePoliticas 20 ocurrencias..
					List listevalRq = result2.getChild("EvaluaciondePoliticas").getChildren();
					int i = 1;
					for (Iterator iterator = listevalRq.iterator(); iterator.hasNext();) {
						String fila = (i>9)?""+i:"0"+i;
						Element resp = (Element) iterator.next();
						String cod = resp.getAttributeValue("cod");
						String desc = resp.getAttributeValue("desc");
						msgResp.getField("E04RESC"+fila).setString(cod.trim());
						msgResp.getField("E04DESR"+fila).setString(desc.trim());
						if (i ==20) break;//to ensure just 20					
						i++;				
					}//EoF			
					//four element TablaSernac 16 ocurrencias..
					List listSernac = result2.getChild("TablaSernac").getChildren();
					int a = 1;
					for (Iterator iterator = listSernac.iterator(); iterator.hasNext();) {
						String fila = (a>9)?""+a:"0"+a;
						Element resp = (Element) iterator.next();
						String cod = resp.getAttributeValue("cod");
						String desc = resp.getAttributeValue("desc");
						msgResp.getField("E04CRSE"+fila).setString(cod.trim());
						msgResp.getField("E04DESS"+fila).setString(desc.trim());
						if (a ==16) break;//to ensure just 16					
						a++; 					
					}//EoF			
					//five element 
					//RequiereAval
					String val0 = ((Element)result2.getChild("Aux").getChildren().get(0)).getAttributeValue("valor");					
					msgResp.setE04INDAVA(val0.trim());
					//MontoPermitidoPD
					String val1 = ((Element)result2.getChild("Aux").getChildren().get(1)).getAttributeValue("valor");
					msgResp.setE04MAXPDI(val1.trim());	
					
					try {
							String InVisadoSt = result1.getChild("VarAux").getChild("IndicadorVisado").getText();
							msgResp.setH04FLGWK1(InVisadoSt.trim());
					}catch (Exception e){
							msgResp.setH04FLGWK1("0");
					}
							
			}catch (Exception e){
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();		
				msgError.setERRNUM("3");
	            msgError.setERNU01("01");//4
	            msgError.setERDS01(className);
	            msgError.setERNU02("02");//4
	            msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
	            msgError.setERNU03("03");//4
	            msgError.setERDS03("Para mas informacion revizar los archivos de log.");				
				System.out.println("ERROR MOTOR:"+e);
				e.printStackTrace();						
			}
		}
		//send data
		List respuesta = new ArrayList();
		respuesta.add(msgError);
		respuesta.add(msgResp);		
		return respuesta;		
	}
	

	//JLS PRICING 
	/**
	 * method that parses the xml response 	 
	 * @return List 1.ELEERRMessage 2.-EPV101004Message
	 */
	@SuppressWarnings("unchecked")
	public List parseXMLResponseJB(){
		
		//define variables to return
		ELEERRMessage msgError = new ELEERRMessage();
		EPV201004Message msgResp = new EPV201004Message();
		
		if  (msResp==null || "".equals(msResp)){
			msgError.setERRNUM("1");
            msgError.setERNU01("01");//4
            msgError.setERDS01("No se Ha recibido Respuesta del Motor. Verifique Conexion.!");//70
		}else{
			SAXBuilder builder = new SAXBuilder();			
			Document doc;
			try {
				doc = builder.build(new StringReader(msResp));
				Element raiz = doc.getRootElement();
				//group Errors.
				Element errors = raiz.getChild("errors");
				List errList = errors.getChildren();
				if (errList!=null&&errList.size()>0){
					Element err = (Element)errList.get(0);
					String c = err.getAttributeValue("cod");
					String d =  err.getAttributeValue("desc");
					msgError.setERRNUM("1");
		            msgError.setERNU01((c.length()>4?(c.substring(0,4)):c));//4
	                msgError.setERDS01((d.length() >70?(d.substring(0,70)):d));//70	
	        		//send data
	        		List respuesta = new ArrayList();
	        		respuesta.add(msgError);
	        		respuesta.add(msgResp);		
	        		return respuesta;		                
				}				
				//Element of message
				String solicitud = (raiz.getChild("IdLlamada")).getText();
				String target = (raiz.getChild("BussinesObject")).getText();
				msgResp.setE04PVMNUM(solicitud.trim());
				msgResp.setE04BUSOBJ(target.trim());
				
				//firt group response
				Element result1 = raiz.getChild("Resultados1");
					//firt element FiltrosPoliticas
					String cdesSt =  result1.getChild("FiltrosPoliticas").getChild("CategoriaDeDecision").getText();			
					String desSt = result1.getChild("FiltrosPoliticas").getChild("Decision").getText();
					msgResp.setE04CODEC1(cdesSt.trim());
					msgResp.setE04DSCDE1(desSt.trim());
					//second element TablaDeReasonCodes 20 ocurrencias..
					List listResp = result1.getChild("TablaDeReasonCodes").getChildren();	
					int fil = 1;
					for (Iterator iterator = listResp.iterator(); iterator.hasNext();) {
						String fila = (fil>9)?""+fil:"0"+fil;
						Element resp = (Element) iterator.next();
						String cod = resp.getAttributeValue("cod");
						String desc = resp.getAttributeValue("desc");
						msgResp.getField("E04REAC"+fila).setString(cod.trim());
						msgResp.getField("E04DSCR"+fila).setString(desc.trim());
						if (fil ==20) break;//to ensure just 20					
						fil++;
					}//				
					//third element VarAux				
					String campanapdSt = result1.getChild("VarAux").getChild("Campanapd").getText();
					msgResp.setE04CODCAM(campanapdSt);
					
				//firt group response
				Element result2 = raiz.getChild("Resultados2");
					//firt element DecisionDeRiesgos
					String cdesicSt = result2.getChild("DecisionDeRiesgos").getChild("CategoriaDeDecision").getText();			
					String rcodeSt = result2.getChild("DecisionDeRiesgos").getChild("ReasonCode").getText();
					String desicSt = result2.getChild("DecisionDeRiesgos").getChild("Decision").getText();
					msgResp.setE04CATDEC(cdesicSt.trim());
					msgResp.setE04CODEC2(rcodeSt.trim());
					msgResp.setE04DSCDE2(desicSt.trim());				
					//second element DocumentacionRequerida 10 ocurrencias..
					List listDocRq = result2.getChild("DocumentacionRequerida").getChildren();

					int vuel = 1;
					for (Iterator iterator = listDocRq.iterator(); iterator.hasNext();) {	
						String fila = (vuel>9)?""+vuel:"0"+vuel;					
						Element resp = (Element) iterator.next();
						String id = resp.getAttributeValue("id");
						String desc = resp.getAttributeValue("desc");
						msgResp.getField("E04CODD"+fila).setString(id.trim());
						msgResp.getField("E04DSCD"+fila).setString(desc.trim());	
						if (vuel ==10) break;//to ensure just 10							
						vuel++;
						
					}//EoF
					//third element EvaluaciondePoliticas 20 ocurrencias..
					List listevalRq = result2.getChild("EvaluaciondePoliticas").getChildren();
					int i = 1;
					for (Iterator iterator = listevalRq.iterator(); iterator.hasNext();) {
						String fila = (i>9)?""+i:"0"+i;
						Element resp = (Element) iterator.next();
						String cod = resp.getAttributeValue("cod");
						String desc = resp.getAttributeValue("desc");
						msgResp.getField("E04RESC"+fila).setString(cod.trim());
						msgResp.getField("E04DESR"+fila).setString(desc.trim());
						if (i ==20) break;//to ensure just 20					
						i++;				
					}//EoF			
					//four element TablaSernac 16 ocurrencias..
					List listSernac = result2.getChild("TablaSernac").getChildren();
					int a = 1;
					for (Iterator iterator = listSernac.iterator(); iterator.hasNext();) {
						String fila = (a>9)?""+a:"0"+a;
						Element resp = (Element) iterator.next();
						String cod = resp.getAttributeValue("cod");
						String desc = resp.getAttributeValue("desc");
						msgResp.getField("E04CRSE"+fila).setString(cod.trim());
						msgResp.getField("E04DESS"+fila).setString(desc.trim());
						if (a ==16) break;//to ensure just 16					
						a++; 					
					}//EoF			
					//five element 
					//RequiereAval
					String val0 = ((Element)result2.getChild("Aux").getChildren().get(0)).getAttributeValue("valor");					
					msgResp.setE04INDAVA(val0.trim());
					//MontoPermitidoPD
					String val1 = ((Element)result2.getChild("Aux").getChildren().get(1)).getAttributeValue("valor");
					msgResp.setE04MAXPDI(val1.trim());	
					
					try {
							String InVisadoSt = result1.getChild("VarAux").getChild("IndicadorVisado").getText();
							msgResp.setH04FLGWK1(InVisadoSt.trim());
					}catch (Exception e){
							msgResp.setH04FLGWK1("0");
					}
							
			}catch (Exception e){
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();		
				msgError.setERRNUM("3");
	            msgError.setERNU01("01");//4
	            msgError.setERDS01(className);
	            msgError.setERNU02("02");//4
	            msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
	            msgError.setERNU03("03");//4
	            msgError.setERDS03("Para mas informacion revizar los archivos de log.");				
				System.out.println("ERROR MOTOR:"+e);
				e.printStackTrace();						
			}
		}
		//send data
		List respuesta = new ArrayList();
		respuesta.add(msgError);
		respuesta.add(msgResp);		
		return respuesta;		
	}
	//JLS PRICING 
	
	
	/**
	 * method that sends message and waits for server response in XML format
	 * 
	 * */
	public ELEERRMessage sendMessage(String idSol,String bussinesObj,String userId) {

		ELEERRMessage msgError = new ELEERRMessage();	
		
		String texto = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"
				+ "<E1>" + "<IdSolicitud>"+idSol+"</IdSolicitud>"
				+ "<BussinesObject>"+bussinesObj+"</BussinesObject>" 
				+ "<UserId>"+userId+"</UserId>"+ "</E1>";

		Connection queueConnection = null;
		ConnectionFactory factory = null;
		Queue queue = null;
		Session session = null;
		MessageProducer producer = null;
		MessageConsumer consumer = null;
		TemporaryQueue tempqueue = null;
		TextMessage message = null;
		long timeout = new Long(JSEIBSProp.getJndiTimeout()).longValue();//
		String jndiQueue = JSEIBSProp.getJndiQueue(); 
		String jndiConnectionFactory = JSEIBSProp.getJndiConnectionFactory();

		try {
			InitialContext ic = new InitialContext();
			factory = (ConnectionFactory) ic.lookup(jndiConnectionFactory);
			queue = (Queue) ic.lookup(jndiQueue);
			queueConnection = factory.createConnection();				
			queueConnection.start();
			session = (Session) queueConnection.createSession(false,QueueSession.AUTO_ACKNOWLEDGE);
			message = session.createTextMessage();
			message.setText(texto);
			logger.info("XML ENVIADO:"+texto);			
			producer = session.createProducer(queue);
			tempqueue = session.createTemporaryQueue();
			consumer = session.createConsumer(tempqueue);
			message.setJMSReplyTo(tempqueue);
			producer.send(message);
			// esperar timeout milisegundos por la respuesta
			TextMessage msg = (TextMessage) consumer.receive(timeout);

			if (msg != null) {
				msResp = msg.getText();
				logger.info("XML RECIBIDO:" + msResp);					
			} else {
				logger.info("XML RECIBIDO:" + "Sin Respuesta de Motor");					
				msgError.setERRNUM("1");
	            msgError.setERNU01("01");//4
	            msgError.setERDS01("Motor No emite respuesta. Por favor verifique status.!");//70	
			}

		}catch (Exception e) {
			String className = e.getClass().getName();
			String description = e.getMessage() == null ? "Exception General" : e.getMessage();		
			msgError.setERRNUM("3");
            msgError.setERNU01("01");//4
            msgError.setERDS01(className);
            msgError.setERNU02("02");//4
            msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
            msgError.setERNU03("03");//4
            msgError.setERDS03("Para mas informacion revizar los archivos de log.");
            System.out.println("ERROR MOTOR:"+e);                 
			e.printStackTrace();
		} 
		finally {

			try {
				if (producer != null)
					producer.close();
			} catch (JMSException e) {
			}
			try {
				if (consumer != null)
					consumer.close();
			} catch (JMSException e) {
			}
			try {
				if (session != null)
					session.close();
			} catch (JMSException e) {
			}
			try {
				if (queueConnection != null)
					queueConnection.close();
			} catch (JMSException e) {
			}

		}		
		return msgError;
	}		
	
}

