package datapro.eibs.master;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class can be used to return an object current application
 * root file-system path to the /classes directory or the object directory.
 * if not object is provided LocateMe is the target object.
 * @author: Frank Hernandez
 */
public class LocateMe {
	private Object obj = null;
	private URL url = null;
	
	public LocateMe () {
		obj = this;
		setUrl("");
	}

	public LocateMe (Object obj) {
		this.obj = obj;
		setUrl("");
	}
	
	public LocateMe (String resourceName) {
		obj = this;
		setUrl(resourceName);
	}
	
	public LocateMe (Object obj, String resourceName) {
		this.obj = obj; 
		setUrl(resourceName);
	}
	
	private Object getObj() {
		return obj;
	}
	
	private Class getTargetClass() {
		return getObj() == null ? LocateMe.class.getClass() : getObj().getClass();
	}
	
	public void setUrl(URL url) {
		this.url = url;
	}
	
	public void setUrl(String resourceName){
		url = getTargetClass().getResource(resourceName);
		if (url == null) {
			String tmp = resourceName;
			for (int i = 0; i < 5; i++) {
				tmp = "../" + tmp;
				url = getTargetClass().getResource(tmp);
				if (url != null) break;
			}
		}
		if (url == null) url = getTargetClass().getResource("");
		if (url == null) url = getTargetClass().getResource("LocateMe.class");
	}
	
	public URL getUrl(){
		return url;
	}
	
	private String getPackagePath(){
		String myPackage = "";
		myPackage = getTargetClass().getPackage().getName();
		myPackage = myPackage.replace('.', '/') + "/";
		return myPackage;
	}	
	
	public String getClassesRoot(){
		String sClassesRoot = "";
		if (getWarPath().equals("")) {
			String sClassPath = getClassPath();
			sClassesRoot = sClassPath.substring(0, sClassPath.length() - getPackagePath().length());
			if (sClassesRoot.lastIndexOf('/') != (sClassesRoot.length() - 1)) sClassesRoot = sClassesRoot + "/";
		} else {
			sClassesRoot = getWarPath()+"/WEB-INF/classes/";
		}
		System.out.println("Classes Root Path = " + sClassesRoot);
		return sClassesRoot; 
	}
	
	public String getClassPath(){
		try {
			return getUrl() != null ? getUrl().getPath() : "";
			//return getUrl() != null ? (new URI(getUrl().toString()).getPath()) : "";
			//return getUrl() != null ? (URLDecoder.decode(getUrl().getPath(), "UTF-8")) : "";
		} catch (Exception e) {
			return "";
		}
	}
	
	public String getWarPath(){
		String path = "";
		if (getClassPath().indexOf("WEB-INF") > -1) {
			String nm = getClassPath();
			path = nm.substring(0, nm.indexOf("WEB-INF") - 1);
		}
		return path;
	}
	
	public String getEarPath(){
		String path = "";
		if (!getWarPath().equals("")) {
			String nm = getWarPath();
			path = nm.substring(0, nm.lastIndexOf("/"));
		} else if(getClassPath().indexOf(".jar") > -1){
			String classpath = getClassPath();
			String nm = classpath.substring(0, classpath.indexOf(".jar"));
			path = classpath.substring(0, nm.lastIndexOf("/"));
		} 
		return path;
	}
	
	public String getEarMetaFolderPath(){
		String path = "";
		if (!getEarPath().equals(""))
			path = getEarPath() + "/META-INF/";
		System.out.println("EAR META-INF Path = " + path);
		return path;
	}
	
	public String getWarMetaFolderPath(){
		String path = "";
		if (!getWarPath().equals(""))
			path = getWarPath() + "/META-INF/";
		System.out.println("WAR META-INF Path = " + path);
		return path;
	}
	
	public boolean checkFile(String pathname) {
		File file = new File(pathname.replaceFirst("file:", ""));
		boolean result = file.exists() && file.isFile(); 
		if (file.exists() && file.isFile()) {
			System.out.println("URL is : " + pathname);
		} else {
			System.out.println("Path Not Found = " + pathname);
		}
		return result;
	}
	
	private String filePath(String pathname) {
		String result = "";
		if (pathname.charAt(0) == '/') {
			result = "file:" + pathname.substring(1);
		} else {
			result = pathname;
		}
		return result;
	}
	
	public URL fileUrl(String fName){
		LocateMe locateMe = new LocateMe(fName);
		URL url = null;
		try {
			if (checkFile(fName))
				return new URL(filePath(fName));
			if (checkFile(locateMe.getEarMetaFolderPath() + fName)) 
				return new URL(filePath(locateMe.getEarMetaFolderPath() + fName));
			if (checkFile(locateMe.getWarMetaFolderPath() + fName)) 
				return new URL(filePath(locateMe.getWarMetaFolderPath() + fName));
			if (checkFile(locateMe.getClassesRoot() + fName)) 
				return new URL(filePath(locateMe.getClassesRoot() + fName));
			if (checkFile(locateMe.getUrl().getPath())) 
				return locateMe.getUrl();
			if (checkFile(fName)) 
				return new URL(fName);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return url;
	}	
	
}
