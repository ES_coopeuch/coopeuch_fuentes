/**
 * Salida.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class Salida  implements java.io.Serializable {
    private java.lang.String codError;
    private java.lang.String desError;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsEstadoCedula resultsEstadoCedula;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsMorosidad resultsMorosidad;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsInfraccionesLaborales resultsInfraccionesLaborales;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsPrendasSinDesplazamiento resultsPrendasSinDesplazamiento;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsMultadosBcoCentral resultsMultadosBcoCentral;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDeudaSistemaFinanciero resultsDeudaSistemaFinanciero;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDireccionesAsociadas resultsDireccionesAsociadas;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDirectiorioPersonas resultsDirectiorioPersonas;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsAvaldoBienesRaices resultsAvaldoBienesRaices;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsProtestosyDocumentosVigentes resultsProtestosyDocumentosVigentes;

    public Salida() {
    }

    public java.lang.String getCodError() {
        return codError;
    }

    public void setCodError(java.lang.String codError) {
        this.codError = codError;
    }

    public java.lang.String getDesError() {
        return desError;
    }

    public void setDesError(java.lang.String desError) {
        this.desError = desError;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsEstadoCedula getResultsEstadoCedula() {
        return resultsEstadoCedula;
    }

    public void setResultsEstadoCedula(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsEstadoCedula resultsEstadoCedula) {
        this.resultsEstadoCedula = resultsEstadoCedula;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsMorosidad getResultsMorosidad() {
        return resultsMorosidad;
    }

    public void setResultsMorosidad(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsMorosidad resultsMorosidad) {
        this.resultsMorosidad = resultsMorosidad;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsInfraccionesLaborales getResultsInfraccionesLaborales() {
        return resultsInfraccionesLaborales;
    }

    public void setResultsInfraccionesLaborales(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsInfraccionesLaborales resultsInfraccionesLaborales) {
        this.resultsInfraccionesLaborales = resultsInfraccionesLaborales;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsPrendasSinDesplazamiento getResultsPrendasSinDesplazamiento() {
        return resultsPrendasSinDesplazamiento;
    }

    public void setResultsPrendasSinDesplazamiento(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsPrendasSinDesplazamiento resultsPrendasSinDesplazamiento) {
        this.resultsPrendasSinDesplazamiento = resultsPrendasSinDesplazamiento;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsMultadosBcoCentral getResultsMultadosBcoCentral() {
        return resultsMultadosBcoCentral;
    }

    public void setResultsMultadosBcoCentral(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsMultadosBcoCentral resultsMultadosBcoCentral) {
        this.resultsMultadosBcoCentral = resultsMultadosBcoCentral;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDeudaSistemaFinanciero getResultsDeudaSistemaFinanciero() {
        return resultsDeudaSistemaFinanciero;
    }

    public void setResultsDeudaSistemaFinanciero(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDeudaSistemaFinanciero resultsDeudaSistemaFinanciero) {
        this.resultsDeudaSistemaFinanciero = resultsDeudaSistemaFinanciero;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDireccionesAsociadas getResultsDireccionesAsociadas() {
        return resultsDireccionesAsociadas;
    }

    public void setResultsDireccionesAsociadas(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDireccionesAsociadas resultsDireccionesAsociadas) {
        this.resultsDireccionesAsociadas = resultsDireccionesAsociadas;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDirectiorioPersonas getResultsDirectiorioPersonas() {
        return resultsDirectiorioPersonas;
    }

    public void setResultsDirectiorioPersonas(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsDirectiorioPersonas resultsDirectiorioPersonas) {
        this.resultsDirectiorioPersonas = resultsDirectiorioPersonas;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsAvaldoBienesRaices getResultsAvaldoBienesRaices() {
        return resultsAvaldoBienesRaices;
    }

    public void setResultsAvaldoBienesRaices(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsAvaldoBienesRaices resultsAvaldoBienesRaices) {
        this.resultsAvaldoBienesRaices = resultsAvaldoBienesRaices;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsProtestosyDocumentosVigentes getResultsProtestosyDocumentosVigentes() {
        return resultsProtestosyDocumentosVigentes;
    }

    public void setResultsProtestosyDocumentosVigentes(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResultsProtestosyDocumentosVigentes resultsProtestosyDocumentosVigentes) {
        this.resultsProtestosyDocumentosVigentes = resultsProtestosyDocumentosVigentes;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        Salida other = (Salida) obj;
        boolean _equals;
        _equals = true
            && ((this.codError==null && other.getCodError()==null) || 
             (this.codError!=null &&
              this.codError.equals(other.getCodError())))
            && ((this.desError==null && other.getDesError()==null) || 
             (this.desError!=null &&
              this.desError.equals(other.getDesError())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        Salida history = (Salida) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.resultsEstadoCedula==null && other.getResultsEstadoCedula()==null) || 
             (this.resultsEstadoCedula!=null &&
              this.resultsEstadoCedula.equals(other.getResultsEstadoCedula())))
            && ((this.resultsMorosidad==null && other.getResultsMorosidad()==null) || 
             (this.resultsMorosidad!=null &&
              this.resultsMorosidad.equals(other.getResultsMorosidad())))
            && ((this.resultsInfraccionesLaborales==null && other.getResultsInfraccionesLaborales()==null) || 
             (this.resultsInfraccionesLaborales!=null &&
              this.resultsInfraccionesLaborales.equals(other.getResultsInfraccionesLaborales())))
            && ((this.resultsPrendasSinDesplazamiento==null && other.getResultsPrendasSinDesplazamiento()==null) || 
             (this.resultsPrendasSinDesplazamiento!=null &&
              this.resultsPrendasSinDesplazamiento.equals(other.getResultsPrendasSinDesplazamiento())))
            && ((this.resultsMultadosBcoCentral==null && other.getResultsMultadosBcoCentral()==null) || 
             (this.resultsMultadosBcoCentral!=null &&
              this.resultsMultadosBcoCentral.equals(other.getResultsMultadosBcoCentral())))
            && ((this.resultsDeudaSistemaFinanciero==null && other.getResultsDeudaSistemaFinanciero()==null) || 
             (this.resultsDeudaSistemaFinanciero!=null &&
              this.resultsDeudaSistemaFinanciero.equals(other.getResultsDeudaSistemaFinanciero())))
            && ((this.resultsDireccionesAsociadas==null && other.getResultsDireccionesAsociadas()==null) || 
             (this.resultsDireccionesAsociadas!=null &&
              this.resultsDireccionesAsociadas.equals(other.getResultsDireccionesAsociadas())))
            && ((this.resultsDirectiorioPersonas==null && other.getResultsDirectiorioPersonas()==null) || 
             (this.resultsDirectiorioPersonas!=null &&
              this.resultsDirectiorioPersonas.equals(other.getResultsDirectiorioPersonas())))
            && ((this.resultsAvaldoBienesRaices==null && other.getResultsAvaldoBienesRaices()==null) || 
             (this.resultsAvaldoBienesRaices!=null &&
              this.resultsAvaldoBienesRaices.equals(other.getResultsAvaldoBienesRaices())))
            && ((this.resultsProtestosyDocumentosVigentes==null && other.getResultsProtestosyDocumentosVigentes()==null) || 
             (this.resultsProtestosyDocumentosVigentes!=null &&
              this.resultsProtestosyDocumentosVigentes.equals(other.getResultsProtestosyDocumentosVigentes())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        Salida history = (Salida) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getCodError() != null) {
            _hashCode += getCodError().hashCode();
        }
        if (getDesError() != null) {
            _hashCode += getDesError().hashCode();
        }
        if (getResultsEstadoCedula() != null) {
            _hashCode += getResultsEstadoCedula().hashCode();
        }
        if (getResultsMorosidad() != null) {
            _hashCode += getResultsMorosidad().hashCode();
        }
        if (getResultsInfraccionesLaborales() != null) {
            _hashCode += getResultsInfraccionesLaborales().hashCode();
        }
        if (getResultsPrendasSinDesplazamiento() != null) {
            _hashCode += getResultsPrendasSinDesplazamiento().hashCode();
        }
        if (getResultsMultadosBcoCentral() != null) {
            _hashCode += getResultsMultadosBcoCentral().hashCode();
        }
        if (getResultsDeudaSistemaFinanciero() != null) {
            _hashCode += getResultsDeudaSistemaFinanciero().hashCode();
        }
        if (getResultsDireccionesAsociadas() != null) {
            _hashCode += getResultsDireccionesAsociadas().hashCode();
        }
        if (getResultsDirectiorioPersonas() != null) {
            _hashCode += getResultsDirectiorioPersonas().hashCode();
        }
        if (getResultsAvaldoBienesRaices() != null) {
            _hashCode += getResultsAvaldoBienesRaices().hashCode();
        }
        if (getResultsProtestosyDocumentosVigentes() != null) {
            _hashCode += getResultsProtestosyDocumentosVigentes().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
