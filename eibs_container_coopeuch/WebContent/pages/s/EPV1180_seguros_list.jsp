<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV118001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV118001List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">

function getRadioButtonSelectedValue(ctrl)
{
    for(i=0;i<ctrl.length;i++){
        if(ctrl[i].checked) return ctrl[i].value;
    }
    return ctrl.value
}

function showChecking(idx) {
	for ( var i=0; i<dataTable.rows.length; i++ ) {
       dataTable.rows[i].className="trnormal";
	}
    dataTable.rows[idx].className="trhighlight";

}

  function goAction(op) {
	var ok = false;
	var cun = "";
	var pg = "";

	if (op != '200'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01PSGSEQ") 
	      	{	
	      		if (element.checked == true) {
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = true;
      	
      	if (op =='202'){
      		confirm1 = confirm("Desea Eliminar el Seguro seleccionado?");
      	}
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			if (op =='202'){//solo para eliminar..
				document.forms[0].submit();		
			     <%//NOTA: solo para activar el check de la pagina integral%>
			     <%  String re =(String) session.getAttribute("EMPSG");%>
				 <%  if ("S".equals(re)){%>
						parent.setRecalculate3();	       
				 <% } %>						
			}else{
				dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1180?SCREEN="+document.forms[0].SCREEN.value+
					  "&cusNum="+document.forms[0].cusNum.value+"&Header23="+document.forms[0].Header23.value;
//				if (op =='201' || op =='203'){
				if (op =='201'){				
					  dir = dir + "&E01PSGSEQ="+getRadioButtonSelectedValue(document.forms[0].E01PSGSEQ);					  	
				}				
				CenterWindow(dir,950,550,2);
			}	
		}		

     } else {
		alert("Debe seleccionar un numero de seguros para continuar.");	   
	 }
      
	}
	
function GetInqDet(col)
{
	var dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1180?SCREEN=203&cusNum="+document.forms[0].cusNum.value+"&Header23="+document.forms[0].Header23.value;
	dir = dir + "&E01PSGSEQ="+col;
	CenterWindow(dir,950,550,2);
} 	
</script>

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

 <% String emp = (String)session.getAttribute("EMPSG");
 	emp = (emp==null)?"":emp;//si es blanco viene llamado por menu, sino viene llamdo desde la pantalla EPV1010
 %>
 <%if ("".equals(emp)){ %>
<h3 align="center">Listado de Seguros<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="seguros_list.jsp,EPV1180"></h3>
<hr size="4">
<%}%>
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1180">
<input type="hidden" name="SCREEN" value="201"> 
<input type=HIDDEN name="totalRow" value="0">

<%if ("".equals(emp)){ %>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Propuesta : 
               </td>
             <td nowrap width="50%"align="left">
   		        <input type="text" name="Header23" size="13" maxlength="12" value="<%= userPO.getHeader23()%>" readonly>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
<%}else{%>
	    <input type="hidden" name="cusNum" size="13" maxlength="12" value="<%= userPO.getCusNum()%>" readonly>		
    	<input type="hidden" name="Header23" size="13" maxlength="12" value="<%= userPO.getHeader23()%>" readonly>
<%}%>  
  
<%if (!"I".equals(emp)){ %>	    		 
<table class="tbenter" width="100%">
	<tr>
 		<td align="center" width="20%"> 		
 			<!-- <a href="#" onclick="goAction('200')"> <b>Crear</b> </a>  -->
		</td> 
		<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('201')"> <b>Modificar</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
    		href="javascript:goAction('202')"> <b>Borrar</b> </a></td> 
		<%if ("".equals(emp)){ %>	    		 
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
		<%}%>		
	</tr>
</table>
<%}%>  

<%
	if (EPV118001List.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>

		<table  id="mainTable" ALIGN=CENTER style="width:'95%'" height="68%" border="0">
		<tr height="5%">
		<TD NOWRAP width="100%" >
  				<TABLE id="headTable" width="97%" >
						<tr id="trdark">
							<th align="center" nowrap width="5%"></th>
							<th align="center" nowrap width="10%">Sec</th>
							<th align="center" nowrap width="10%">Codigo</th>
							<th align="center" nowrap width="40%">Descripcion</th>
							<th align="center" nowrap width="10%">Seleccion</th>
							<th align="right" nowrap width="20%">Monto</th>
						</tr>
       			</TABLE>
      		</td>
		</tr>
		<tr height="95%">    
			<td NOWRAP width="100%">    
   			    <div id="dataDiv1" class="scbarcolor" style="width:100%; height:100%; overflow:auto;">
    				<table id="dataTable" width="97%" > 			

		<% 		double suma = 0;
				EPV118001List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EPV118001List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EPV118001Message convObj = (EPV118001Message) EPV118001List.getRecord();
					suma += convObj.getBigDecimalE01PSGPGA().doubleValue();
		%>
		<tr>
			<td nowrap width="2%">
				<input type="radio" name="E01PSGSEQ"	value="<%=convObj.getE01PSGSEQ()%>" <%=chk%> 
					onClick="javascript:showChecking(<%= EPV118001List.getCurrentRow() %>);"/>
			</td>
			<td nowrap align="center"><a href="javascript:GetInqDet('<%=convObj.getE01PSGSEQ()%>');"><%=Util.formatCell(convObj.getE01PSGSEQ())%></a></td>
			<td nowrap align="center"><a href="javascript:GetInqDet('<%=convObj.getE01PSGSEQ()%>');"><%=convObj.getE01PSGSCD()%></a></td>
			<td nowrap align="left"><a href="javascript:GetInqDet('<%=convObj.getE01PSGSEQ()%>');"><%=convObj.getE01PSGSNM()%></a></td>
			<td nowrap align="center"><a href="javascript:GetInqDet('<%=convObj.getE01PSGSEQ()%>');"><input disabled type="checkbox" name="E01PSGSEL" value="Y" <%if (convObj.getE01PSGSEL().equals("Y")){out.print(" checked");}; %>/></a></td>
			<td nowrap align="right"><a href="javascript:GetInqDet('<%=convObj.getE01PSGSEQ()%>');"><%=convObj.getE01PSGPGA()%></a></td>
		</tr>
		<%
			}
		%>
		<tr>
			<td align="center" nowrap >&nbsp;</td>
			<td align="center" nowrap >&nbsp;</td>						
			<td align="center" nowrap >&nbsp;</td>
			<td align="center" nowrap >&nbsp;</td>
			<td nowrap align="center"><b>TOTAL : </b> </td>
			<td nowrap align="right"><b><%=datapro.eibs.master.Util.formatCCY(suma)%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
		</tr>								
				</table>
			</div>
		</TD>
	</TR>	
</TABLE>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV118001List.getShowPrev()) {
					int pos = EPV118001List.getFirstRec() - 13;
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1180?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV118001List.getShowNext()) {
					int pos = EPV118001List.getLastRec();
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1180?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>
</form>
<%
if (!EPV118001List.getNoResult()) {
%>

<SCRIPT language="JavaScript">
	showChecked("E01PSGSEQ");  
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(headTable, dataTable, dataDiv1,1,false);
	}
	resizeDoc();   			
   	window.onresize=resizeDoc;        
</SCRIPT>
<%
	}
%>
</body>
</html>
