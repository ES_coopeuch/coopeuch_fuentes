<!-- Hecho por Alonso Arana ------Datapro-----06/02/2014 -->
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC200001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html> 
<head>
<title>Conciliaci�n Bancos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ERClist" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id="lista_entrada" class="datapro.eibs.beans.ERC200001Message" scope="session"/>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

  $(function(){
     $("#radio_key").attr("checked", false);
  });


  function goAction(op) 
  {
    var ok = false;

    for(n=0; n<document.forms[0].elements.length; n++)
	  {
	     var element = document.forms[0].elements[n];
	     if(element.name == "E01RCCTCD") 
	     {	
	        if (element.checked == true) {
	      	   document.getElementById("codigo_lista").value = element.value; 
        	   ok = true;
        	   break;
			}
	      }
	   }

     if ( ok ) 
     {
        var confirm1 = false;
      	
      	if (op =='900')
      	  {
      	   confirm1 = confirm("�Desea Eliminar la cartola seleccionada?");
  	   	   
  	   	   if (confirm1)
  	   	      {
			   document.forms[0].SCREEN.value = op;
			   document.forms[0].submit();		
	          }
          }
  	   else if (op=='1200')
  	      {
	  		document.forms[0].SCREEN.value = op;
			document.forms[0].action = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0040";
			document.forms[0].submit();	
	      }
	   else 
  	      {
			   document.forms[0].SCREEN.value = op;
			   document.forms[0].submit();		
	      } 

     } 
     else 
		alert("Debe seleccionar un registro para continuar.");	   
  }





</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Gesti�n de Cartolas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ERC2000_statement_list.jsp,ERC2000"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC2000">
<input type="hidden" name="SCREEN" value="201">
<input type="hidden" name="codigo_lista" value="" id="codigo_lista">  

<input type="hidden" name="E01RCHSTN" value="<%=request.getAttribute("numero_cartola") %>">
<input type="hidden" name="E01RCHAPF" value="<%=request.getAttribute("estado") %>"> 
<input type="hidden" name="E01BRMEID" value="<%=session.getAttribute("codigo_banco") %>"> 

<input type="hidden" name="E01RCHTFC" value="<%=request.getParameter("E01RCHTFC") %>" >
<input type="hidden" name="E01BRMACC" value="<%=session.getAttribute("cuenta_ibs")%>">
<input type="hidden" name="E01DSCRBK" value="<%=session.getAttribute("nombre_banco") %>">
  
<input type="hidden" name="E01RCHREC" value="<%=lista_entrada.getE01RCHREC()%>">
  
<input type="hidden" name="E01RCHFSD" value="<%=request.getParameter("E01RCHFSD") %>"> 
<input type="hidden" name="E01RCHFSM" value="<%=request.getParameter("E01RCHFSM") %>"> 
<input type="hidden" name="E01RCHFSY" value="<%=request.getParameter("E01RCHFSY") %>"> 

<input type="hidden" name="E01RCHFLD" value="<%=request.getParameter("E01RCHFLD") %>"> 
<input type="hidden" name="E01RCHFLM" value="<%=request.getParameter("E01RCHFLM") %>"> 
<input type="hidden" name="E01RCHFLY" value="<%=request.getParameter("E01RCHFLY") %>"> 


 
   <table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td id="trdark"> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" >
         <tr>
		<td nowrap width="10%" align="center" colspan="4"><h4>FILTROS DE LA CONSULTA</h4></td>
		</tr>
		
          <tr>
          
             <td nowrap width="10%" align="right">Banco : 
              </td>
             <td nowrap width="10%" align="left" colspan="3">
	  			<input type="text" name="codigo_banco" value="<% out.print(session.getAttribute("codigo_banco")+"-"+session.getAttribute("nombre_banco"));%>" 
	  			size="60"  readonly>
             </td>    
         </tr>
         
          <tr>
             <td nowrap width="10%" align="right">Cuenta Banco : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCTA" size="23" maxlength="20" value="<%=session.getAttribute("cuenta_banco")%>" readonly>
             </td>
        <td nowrap width="10%" align="right">Cuenta IBS : 
               </td>
             <td nowrap width="50%"align="left">
             
 <input type="text" name="cuenta_ibs" size="23" maxlength="20" value="<%=session.getAttribute("cuenta_ibs")%>" readonly>
             </td>
             </tr>
      
           <tr>
             <td nowrap width="10%" align="right">Fecha Carga Desde : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha" size="23" maxlength="20" value="<% 
 
 if(lista_entrada.getE01RCHTFC().equals("2")){
 out.print(request.getAttribute("fecha_inicial"));
 }
 
  %>" readonly>
             </td>
        <td nowrap width="10%" align="right">Fecha Carga Hasta : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha2" size="23" maxlength="20" value="<% 
  if(lista_entrada.getE01RCHTFC().equals("2"))
  {
   out.print(request.getAttribute("fecha_final"));
 }
  %>" readonly>
             </td>
             </tr>
        
        <tr>
             <td nowrap width="10%" align="right">Fecha Cartola Desde : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha" size="23" maxlength="20" value="
<%
  if(lista_entrada.getE01RCHTFC().equals("1")){
 out.print(request.getAttribute("fecha_inicial"));

;} %>
 " readonly >
             </td>
        <td nowrap width="10%" align="right">Fecha Cartola Hasta : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha2" size="23" maxlength="20" value="
<%
  if(lista_entrada.getE01RCHTFC().equals("1")){

 out.print(request.getAttribute("fecha_final"));

} %>" readonly>
             </td>
             </tr>
             
             
               <tr>
             <td nowrap width="10%" align="right">Estado : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01estado" size="23" maxlength="20" value="<%
 
 if(request.getAttribute("estado").equals("T"))
 out.print("TODOS");
 else if(request.getAttribute("estado").equals("1"))
 	out.print("PENDIENTE");
 else if(request.getAttribute("estado").equals("2"))
 	out.print("CONCILIADA PARCIAL");
 else if(request.getAttribute("estado").equals("3"))
 	out.print("CONCILIADA TOTAL");
 else if(request.getAttribute("estado").equals("4"))
 	out.print("CON ERRORES");
 else if(request.getAttribute("estado").equals("5"))
 	out.print("ELIMINADA");
 else
 	out.print(" ");
 
 %>" readonly >
             </td>
        <td nowrap width="10%" align="right">N� Cartola : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01Cartola" size="23" maxlength="20" value="<%=request.getAttribute("numero_cartola")%>" readonly>
             </td>
             </tr>
        </table>
      </td>
    </tr>
  </table>
 
 


<%
	if (ERClist.getNoResult()) {
%>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>

<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('300')"><b>Consultar</b></a>
		</td>
	
		<td align="center" class="tdbkg" width="10%">
			<a href="javascript:goAction('801')"> <b>Conciliaci�n Autom�tica</b> </a>
		</td>
	
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('1200')"><b>Conciliaci�n Manual</b></a>
		</td>
		
		
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('500')"><b>Desconciliaci�n</b></a>
		</td>

		<td align="center" class="tdbkg" width="10%"><a
			href="javascript:goAction('600')"> <b>Exportar a Excel</b> </a></td>

		<td align="center" class="tdbkg" width="10%"><a
			href="javascript:goAction('900')"><b>Eliminar</b></a>
		</td>

	</tr>
</table>

<table class="tbenter" width=100% >
<tr><td height="20"></td></tr>

<tr>
<td>

<table id="headTable"  width="100%" align="left">
		<tr id="trdark">
			<th align="center" nowrap width="30"></th>
			<th align="center" nowrap width="100">Cartola</th>

						<th align="center" nowrap width="100">Fecha Carga</th>
						<th align="center" nowrap width="100">Estado</th>
						<th align="center" nowrap width="50">Reg</th>
							<th align="center" nowrap width="50">Reg Error</th>
			<th align="center" nowrap width="100">Fecha de Cartola Desde</th>
			<th align="center" nowrap width="100">Fecha de Cartola Hasta</th>
			<th align="center" nowrap width="100">Saldo Inicial</th>
			<th align="center" nowrap width="100">Saldo Final</th>		
					<th align="center" nowrap width="100">Tipo Carga</th>	
		</tr>
		
		<%
			ERClist.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (ERClist.getNextRow()) {
					//if (firstTime) {
						//firstTime = false;
						//chk = "checked";
					//} else {
					//	chk = "";
				//	}
				ERC200001Message pvprd = (ERC200001Message) ERClist.getRecord();
		%>
		<tr>
		
		<td nowrap><input type="radio" name="E01RCCTCD" id="radio_key" value="<%= ERClist.getCurrentRow()%>" <%=chk%>/></td>
		<td nowrap align="center"><%=pvprd.getE01RCHSTN() %></td>
		<td nowrap align="center"><% out.print(pvprd.getE01RCHSDD()+"/"+pvprd.getE01RCHSDM()+"/"+pvprd.getE01RCHSDY());  %></td>
	<td nowrap align="center"><%=pvprd.getE01DESAPF()%></td>
		
 	  	<td nowrap align="center"><%=pvprd.getE01RCHTRG()%></td>
			
 	  	<td nowrap align="center"><%=pvprd.getE01RCHTRE() %></td>
 	  <td nowrap align="center">
			
			<%
			
			if(pvprd.getE01RCHAPF().equals("4")){
			
			out.print("");
			
			}
			
			else{
			
			
			if(pvprd.getE01RCHFSD().equals("0")){
			
			out.print("");
			}
			
			else if(pvprd.getE01RCHFSM().equals("0")){
			
			out.print("");
			}
			
			else if(pvprd.getE01RCHFSY().equals("0")){
			
			
			out.print("");
			}
			
			else{
			
			out.print(pvprd.getE01RCHFSD()+"/"+pvprd.getE01RCHFSM()+"/"+pvprd.getE01RCHFSY()); 
				}

			}
			 %>
		
			</td>	
 	<td nowrap align="center">
			
			<%
			
			if(pvprd.getE01RCHAPF().equals("4")){
			
			out.print("");
			
			}
			
			else{
			
			if(pvprd.getE01RCHFLD().equals("0")){
			
			out.print("");
			}	
			
			
			else if(pvprd.getE01RCHFLM().equals("0")){
			
			out.print("");
			}	
			
			
			else if(pvprd.getE01RCHFLY().equals("0")){
			
			out.print("");
			}	
			
				else{
			out.print(pvprd.getE01RCHFLD()+"/"+pvprd.getE01RCHFLM()+"/"+pvprd.getE01RCHFLY()); 
			}
			
			
			}
			 %>
			</td>
		 	  	<td nowrap align="right"><%=pvprd.getE01RCHBBL()%></td>
				
 	  	<td nowrap align="right"><%=pvprd.getE01RCHBBF()%></td>
		
		 	  	<td nowrap align="center">
		 	  	<% 
		 	  	  if(pvprd.getE01RCHTCA().equals("A")){
 
 out.print("AUTO");
 }
else if(pvprd.getE01RCHTCA().equals("M")){
 
 out.print("MAN");
 }
		 	  	%>
		 	  	</td>
		</tr>
		<%
			}
		%>
	</table>
</td>
</tr>	
	<tr>
	<td>
	<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (ERClist.getShowPrev()) {
					int pos = ERClist.getFirstRec() - 10;					
					
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.products.JSERC2000?SCREEN=200&posicion="
									+ pos
									+"&mark=atr�s"
									+"&E01RCHSTN="+request.getAttribute("numero_cartola")
									+"&E01RCHAPF="+request.getAttribute("estado")
									+"&E01BRMEID="+session.getAttribute("codigo_banco")
									+"&E01RCHTFC="+request.getParameter("E01RCHTFC") 
									+"&E01BRMACC="+session.getAttribute("cuenta_ibs")
									+"&E01DSCRBK="+session.getAttribute("nombre_banco")
									+"&E01BRMCTA="+session.getAttribute("cuenta_banco")
									+"&E01RCHFSD="+request.getParameter("E01RCHFSD")
									+"&E01RCHFSM="+request.getParameter("E01RCHFSM")
									+"&E01RCHFSY="+request.getParameter("E01RCHFSY")
									
									+"&E01RCHFLD="+request.getParameter("E01RCHFLD")
									+"&E01RCHFLM="+request.getParameter("E01RCHFLM")
									+"&E01RCHFLY="+request.getParameter("E01RCHFLY")
									
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (ERClist.getShowNext()) {
					int pos = ERClist.getLastRec();
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.products.JSERC2000?SCREEN=200&posicion="
									+pos
									+"&mark=adelante"
									+"&E01RCHSTN="+request.getAttribute("numero_cartola")
									+"&E01RCHAPF="+request.getAttribute("estado")
									+"&E01BRMEID="+session.getAttribute("codigo_banco")
									+"&E01RCHTFC="+request.getParameter("E01RCHTFC") 
									+"&E01BRMACC="+session.getAttribute("cuenta_ibs")
									+"&E01DSCRBK="+session.getAttribute("nombre_banco")
									+"&E01BRMCTA="+session.getAttribute("cuenta_banco")
									+"&E01RCHFSD="+request.getParameter("E01RCHFSD")
									+"&E01RCHFSM="+request.getParameter("E01RCHFSM")
									+"&E01RCHFSY="+request.getParameter("E01RCHFSY")
							
									+"&E01RCHFLD="+request.getParameter("E01RCHFLD")
									+"&E01RCHFLM="+request.getParameter("E01RCHFLM")
									+"&E01RCHFLY="+request.getParameter("E01RCHFLY")
									
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/next_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
</td>	
</tr>	
</table>
<%
	}
%>


	 
 

</form>
</body>
</html>
