<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Validaci&oacute;n Interfaces Locales Motor de Pago</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "Inter" class= "datapro.eibs.beans.EREC10101Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

</head>

<body>
 <% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>

<H3 align="center">Validaci&oacute;n Interfaz Motor de Pago <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="interface_process.jsp, EREC101"></H3>

<hr size="4">
<p>&nbsp;</p>

<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Interfaz : 
              </td>
             <td nowrap width="10%" align="left">
              <input type="text" name="E01REOIDE" size="5" maxlength="4" readonly value="<%= Inter.getE01REOIDE() %>">
              <input type="text" name="E01REODES" size="51" maxlength="50" readonly value="<%=request.getAttribute("E01REODES")%>">
             </td>    
         </tr>
        </table>
      </td>
    </tr>
  </table>


  
<br>

<table class="tableinfo" width="100%">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">     
		    <tr>
				<div align="center">
  					<h3>
  						<br>La Interfaz ha sido sometida a proceso. Consulte su estado en Log Validaci&oacute;n Int. M.P.<br>
  					</h3>
  				 </div>		    
		      <td > 
		      </td>
		    </tr>
		 </table>
	  </td>
	</tr>
</table>

<br>
</body>
</html>
