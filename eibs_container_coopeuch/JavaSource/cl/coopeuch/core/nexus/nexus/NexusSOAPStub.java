/**
 * NexusSOAPStub.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * q0834.18 v82708152146
 */

package cl.coopeuch.core.nexus.nexus;

public class NexusSOAPStub extends com.ibm.ws.webservices.engine.client.Stub implements cl.coopeuch.core.nexus.nexus.NexusPortType {
    public NexusSOAPStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws com.ibm.ws.webservices.engine.WebServicesFault {
        if (service == null) {
            super.service = new com.ibm.ws.webservices.engine.client.Service();
        }
        else {
            super.service = service;
        }
        super.engine = ((com.ibm.ws.webservices.engine.client.Service) super.service).getEngine();
        initTypeMapping();
        super.cachedEndpoint = endpointURL;
        super.connection = ((com.ibm.ws.webservices.engine.client.Service) super.service).getConnection(endpointURL);
        super.messageContexts = new com.ibm.ws.webservices.engine.MessageContext[2];
    }

    private void initTypeMapping() {
        javax.xml.rpc.encoding.TypeMapping tm = super.getTypeMapping(com.ibm.ws.webservices.engine.Constants.URI_LITERAL_ENC);
        java.lang.Class javaType = null;
        javax.xml.namespace.QName xmlType = null;
        javax.xml.namespace.QName compQName = null;
        javax.xml.namespace.QName compTypeQName = null;
        com.ibm.ws.webservices.engine.encoding.SerializerFactory sf = null;
        com.ibm.ws.webservices.engine.encoding.DeserializerFactory df = null;
        javaType = cl.coopeuch.core.nexus.DatosEstadosDeCuentaPDF.class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "DatosEstadosDeCuentaPDF");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanSerializerFactory.class, javaType, xmlType);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializerFactory.class, javaType, xmlType);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = cl.coopeuch.core.nexus.RespuestaEstadosDeCuentaPDF.class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "RespuestaEstadosDeCuentaPDF");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanSerializerFactory.class, javaType, xmlType);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializerFactory.class, javaType, xmlType);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = cl.coopeuch.core.nexus.Error.class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "Error");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanSerializerFactory.class, javaType, xmlType);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializerFactory.class, javaType, xmlType);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = cl.coopeuch.core.nexus.DatosMovimientosNoFacturados.class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "DatosMovimientosNoFacturados");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanSerializerFactory.class, javaType, xmlType);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializerFactory.class, javaType, xmlType);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados.class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "RespuestaMovimientosNoFacturados");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanSerializerFactory.class, javaType, xmlType);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializerFactory.class, javaType, xmlType);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = cl.coopeuch.core.nexus.ListaTransacciones.class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "listaTransacciones");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanSerializerFactory.class, javaType, xmlType);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializerFactory.class, javaType, xmlType);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = cl.coopeuch.core.nexus.ArrayOfDetalleError.class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "ArrayOfDetalleError");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanSerializerFactory.class, javaType, xmlType);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializerFactory.class, javaType, xmlType);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = cl.coopeuch.core.nexus.DetalleError.class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "DetalleError");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanSerializerFactory.class, javaType, xmlType);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializerFactory.class, javaType, xmlType);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = cl.coopeuch.core.nexus.RECORD.class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "RECORD");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanSerializerFactory.class, javaType, xmlType);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializerFactory.class, javaType, xmlType);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _consultaEstadosDeCuentaPDFOperation0 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getconsultaEstadosDeCuentaPDFOperation0() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "entrada"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "DatosEstadosDeCuentaPDF"), cl.coopeuch.core.nexus.DatosEstadosDeCuentaPDF.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("inputPosition","0");
        _params0[0].setOption("partQNameString","{http://nexus.core.coopeuch.cl}DatosEstadosDeCuentaPDF");
        _params0[0].setOption("partName","DatosEstadosDeCuentaPDF");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "salida"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "RespuestaEstadosDeCuentaPDF"), cl.coopeuch.core.nexus.RespuestaEstadosDeCuentaPDF.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("outputPosition","0");
        _returnDesc0.setOption("partQNameString","{http://nexus.core.coopeuch.cl}RespuestaEstadosDeCuentaPDF");
        _returnDesc0.setOption("partName","RespuestaEstadosDeCuentaPDF");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _consultaEstadosDeCuentaPDFOperation0 = new com.ibm.ws.webservices.engine.description.OperationDesc("consultaEstadosDeCuentaPDF", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "consultaEstadosDeCuentaPDF"), _params0, _returnDesc0, _faults0, "http://www.example.org/Nexus/consultaEstadosDeCuentaPDF");
        _consultaEstadosDeCuentaPDFOperation0.setOption("inoutOrderingReq","false");
        _consultaEstadosDeCuentaPDFOperation0.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "NexusPortType"));
        _consultaEstadosDeCuentaPDFOperation0.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "consultaEstadosDeCuentaPDFResponse"));
        _consultaEstadosDeCuentaPDFOperation0.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "NexusService"));
        _consultaEstadosDeCuentaPDFOperation0.setOption("buildNum","q0834.18");
        _consultaEstadosDeCuentaPDFOperation0.setOption("ResponseNamespace","http://nexus.core.coopeuch.cl");
        _consultaEstadosDeCuentaPDFOperation0.setOption("targetNamespace","http://nexus.core.coopeuch.cl/nexus");
        _consultaEstadosDeCuentaPDFOperation0.setOption("ResponseLocalPart","consultaEstadosDeCuentaPDFResponse");
        _consultaEstadosDeCuentaPDFOperation0.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "consultaEstadosDeCuentaPDFRequest"));
        _consultaEstadosDeCuentaPDFOperation0.setUse(com.ibm.ws.webservices.engine.enumtype.Use.LITERAL);
        _consultaEstadosDeCuentaPDFOperation0.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.WRAPPED);
        return _consultaEstadosDeCuentaPDFOperation0;

    }

    private int _consultaEstadosDeCuentaPDFIndex0 = 0;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getconsultaEstadosDeCuentaPDFInvoke0(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_consultaEstadosDeCuentaPDFIndex0];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(NexusSOAPStub._consultaEstadosDeCuentaPDFOperation0);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("http://www.example.org/Nexus/consultaEstadosDeCuentaPDF");
            mc.setEncodingStyle(com.ibm.ws.webservices.engine.Constants.URI_LITERAL_ENC);
            mc.setProperty(com.ibm.wsspi.webservices.Constants.SEND_TYPE_ATTR_PROPERTY, Boolean.FALSE);
            mc.setProperty(com.ibm.wsspi.webservices.Constants.ENGINE_DO_MULTI_REFS_PROPERTY, Boolean.FALSE);
            super.primeMessageContext(mc);
            super.messageContexts[_consultaEstadosDeCuentaPDFIndex0] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public cl.coopeuch.core.nexus.RespuestaEstadosDeCuentaPDF consultaEstadosDeCuentaPDF(cl.coopeuch.core.nexus.DatosEstadosDeCuentaPDF entrada) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getconsultaEstadosDeCuentaPDFInvoke0(new java.lang.Object[] {entrada}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        } 
        try {
            return (cl.coopeuch.core.nexus.RespuestaEstadosDeCuentaPDF) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (cl.coopeuch.core.nexus.RespuestaEstadosDeCuentaPDF) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), cl.coopeuch.core.nexus.RespuestaEstadosDeCuentaPDF.class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _consultaMovimientosNoFacturadosOperation1 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getconsultaMovimientosNoFacturadosOperation1() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params1 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "entrada"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "DatosMovimientosNoFacturados"), cl.coopeuch.core.nexus.DatosMovimientosNoFacturados.class, false, false, false, false, true, false), 
          };
        _params1[0].setOption("inputPosition","0");
        _params1[0].setOption("partQNameString","{http://nexus.core.coopeuch.cl}DatosMovimientosNoFacturados");
        _params1[0].setOption("partName","DatosMovimientosNoFacturados");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc1 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "salida"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "RespuestaMovimientosNoFacturados"), cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados.class, true, false, false, false, true, false); 
        _returnDesc1.setOption("outputPosition","0");
        _returnDesc1.setOption("partQNameString","{http://nexus.core.coopeuch.cl}RespuestaMovimientosNoFacturados");
        _returnDesc1.setOption("partName","RespuestaMovimientosNoFacturados");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults1 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _consultaMovimientosNoFacturadosOperation1 = new com.ibm.ws.webservices.engine.description.OperationDesc("consultaMovimientosNoFacturados", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "consultaMovimientosNoFacturados"), _params1, _returnDesc1, _faults1, "http://www.example.org/Nexus/consultaMovimientosNoFacturados");
        _consultaMovimientosNoFacturadosOperation1.setOption("inoutOrderingReq","false");
        _consultaMovimientosNoFacturadosOperation1.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "NexusPortType"));
        _consultaMovimientosNoFacturadosOperation1.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "consultaMovimientosNoFacturadosResponse"));
        _consultaMovimientosNoFacturadosOperation1.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "NexusService"));
        _consultaMovimientosNoFacturadosOperation1.setOption("buildNum","q0834.18");
        _consultaMovimientosNoFacturadosOperation1.setOption("ResponseNamespace","http://nexus.core.coopeuch.cl");
        _consultaMovimientosNoFacturadosOperation1.setOption("targetNamespace","http://nexus.core.coopeuch.cl/nexus");
        _consultaMovimientosNoFacturadosOperation1.setOption("ResponseLocalPart","consultaMovimientosNoFacturadosResponse");
        _consultaMovimientosNoFacturadosOperation1.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "consultaMovimientosNoFacturadosRequest"));
        _consultaMovimientosNoFacturadosOperation1.setUse(com.ibm.ws.webservices.engine.enumtype.Use.LITERAL);
        _consultaMovimientosNoFacturadosOperation1.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.WRAPPED);
        return _consultaMovimientosNoFacturadosOperation1;

    }

    private int _consultaMovimientosNoFacturadosIndex1 = 1;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getconsultaMovimientosNoFacturadosInvoke1(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_consultaMovimientosNoFacturadosIndex1];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(NexusSOAPStub._consultaMovimientosNoFacturadosOperation1);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("http://www.example.org/Nexus/consultaMovimientosNoFacturados");
            mc.setEncodingStyle(com.ibm.ws.webservices.engine.Constants.URI_LITERAL_ENC);
            mc.setProperty(com.ibm.wsspi.webservices.Constants.SEND_TYPE_ATTR_PROPERTY, Boolean.FALSE);
            mc.setProperty(com.ibm.wsspi.webservices.Constants.ENGINE_DO_MULTI_REFS_PROPERTY, Boolean.FALSE);
            super.primeMessageContext(mc);
            super.messageContexts[_consultaMovimientosNoFacturadosIndex1] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados consultaMovimientosNoFacturados(cl.coopeuch.core.nexus.DatosMovimientosNoFacturados entrada) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getconsultaMovimientosNoFacturadosInvoke1(new java.lang.Object[] {entrada}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        } 
        try {
            return (cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados.class);
        }
    }

    private static void _staticInit() {
        _consultaMovimientosNoFacturadosOperation1 = _getconsultaMovimientosNoFacturadosOperation1();
        _consultaEstadosDeCuentaPDFOperation0 = _getconsultaEstadosDeCuentaPDFOperation0();
    }

    static {
       _staticInit();
    }
}
