<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<html>
<head>
<title>Manteci&oacute;n de Tarifas</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="Inter" class="datapro.eibs.beans.EREC10001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

function cancel() {
	document.forms[0].SCREEN.value = 200;
	document.forms[0].submit();
}

</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "NEW".equals(userPO.getPurpose()) ? "" : "readonly";
     
%>


<H3 align="center">
<% if(userPO.getPurpose().equals("NEW")){ %>
Nuevo Rango
<% } else {%>
Mantenci&oacute;n de Rango
<% } %>
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="inter_maitenance, EREC100"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.motorpago.JSEREC100" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="400">
 
 
   <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0" bordercolor="#000000">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table class="tableinfo" cellspacing=0 cellpadding=2 width="100%" border="0">
		  <tr> 
            <td nowrap width="10%"> 
              <div align="right">Interfaz :</div>
            </td>
            <td nowrap width="90%"> 
              <input type="text" name="E01RERIDE" size="5" maxlength="4" readonly value="<%=userPO.getIdentifier() %>">
              <input type="text" name="E01RERDES" size="41" maxlength="40" readonly value="<%=userPO.getHeader1() %>">
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>
  
  <h4>Datos Rango</h4>
  <table  class="tableinfo">
	<tr bordercolor="#FFFFFF"> 
		<td nowrap> 
        	<table cellspacing="0" cellpadding="2" width="100%" border="0">
          	<tr id="trdark"> 
            	<td nowrap width="20%"> 
             		<div align="right">Id Interfaz :</div>
            	</td>
            	<td nowrap width="15%"> 
              		<div align="left"> 
                		<input type="text" name="E01RERIDE" size="5" maxlength="4" value="<%= userPO.getIdentifier().trim()%>"  readonly>
              		</div>
            	</td>
            	<td nowrap width="20%"> 
              		<div align="right">Correlativo  :</div>
            	</td>
            	<td nowrap> 
              		<div align="left" width="45%"> 
                		<input type="text" name="E01RERCOR" size="3" maxlength="2" value="<%= Inter.getE01RERCOR().trim()%>" onkeypress="enterInteger()" <%=readonly%>>
              		</div>
            	</td>
           		<td nowrap width="20%"> 
             		 <div align="right">Descripci&oacute;n :</div>
           		</td>
            	<td nowrap> 
           		   <div align="left" width="45%"> 
               		 <input type="text" name="E01RERCON" size="41" maxlength="40" value="<%= Inter.getE01RERCON().trim()%>"  >
              		</div>
            	</td>
          	</tr>

          	<tr id="trclear"> 
           		<td nowrap height="23"> 
              		<div align="right">Valor M&iacute;nimo :</div>
            	</td>
            	<td nowrap> 
              		<div align="left"> 
                		<input type="text" name="E01RERMIN" size="16" maxlength="15" value="<%= Inter.getE01RERMIN().trim()%>" class="TXTRIGHT" onkeypress="enterDecimal()">
              		</div>
            	</td>
            	<td nowrap> 
              		<div align="right">Valor M&aacute;ximo :</div>
            	</td>
            	<td nowrap> 
              		<div align="left"> 
                		<input type="text" name="E01RERMAX" size="16" maxlength="15" value="<%= Inter.getE01RERMAX().trim()%>" class="TXTRIGHT" onkeypress="enterDecimal()">
              		</div>
            	</td>
            	<td nowrap> 
              		<div align="right">Indicador Activaci&oacute;n :</div>
            	</td>
            	<td nowrap> 
              		<div align="left" width="45%"> 
               			<select name="E01RERIND">  
                  			<option value="S" <% if (Inter.getE01RERIND().trim().equals("S")) out.print("selected"); %>>S</option>                   
                  			<option value="N" <% if (!Inter.getE01RERIND().trim().equals("S")) out.print("selected"); %>>N</option>
                		</select>                
              		</div>
            	</td>
          	</tr>
			</table>
		</td>
	</tr>
  </table>

  <div align="center">
    <input id="EIBSBTN" type="submit" name="Enviar" value="Enviar" >
    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel()">
  </div>
  </form>

</body>
</html>
