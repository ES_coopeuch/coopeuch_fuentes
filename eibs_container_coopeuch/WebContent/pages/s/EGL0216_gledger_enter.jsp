<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>


<style type="text/css">
 .TXTRIGHT {
 font-family: "Verdana, Arial, Helvetica, sans-serif";
 font-size:8pt;
 color:black;
 text-transform : uppercase;
 text-align: right;
 border:0; 
 }
 
 .TXTRIGHT2 {
 font-family: "Verdana, Arial, Helvetica, sans-serif";
 font-size:8pt;
 color:black;
 text-transform : uppercase;
 text-align: right;
 border:0; 
 background-color: #F2F1F1;
 }

 .TXTRIGHT3 {
 font-family: "Verdana, Arial, Helvetica, sans-serif";
 font-size:8pt;
 color:black;
 text-transform : uppercase;
 text-align: right;
 border:0; 
 font-weight: bold;

 }

 
 
 
  </style>

<title>Correcci&oacuten Monetaria</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "gLedger" class= "datapro.eibs.beans.EGL021601Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>


<script language="Javascript">



function PrintPreviewPDF() {


  CenterWindow('<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEGL0216?SCREEN=300' ,600,500,4);
}

</script> 

<%!
	public String fmtDec(double d) {
		String fmt1 = "#,##0.00";
		java.text.DecimalFormat df = new java.text.DecimalFormat(fmt1);
		return df.format(d);		
	}
%>


<%!
	public String fmtDouble(double d) {
		String fmt = "#,##0";
		java.text.DecimalFormat df = new java.text.DecimalFormat(fmt);
		return df.format(d);		
	}
%>


</head>
<body bgcolor="#FFFFFF">
 
<H3 align="center">Correcci&oacuten Monetaria de Capital Cuotas de Participaci&oacuten<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="gledger_enter,EGL0216"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEGL0216" >
  <p> 
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">
  </p>
  <h4>&nbsp;</h4>

<TABLE width="100%" border=1 > 

<TR>

<TD  width="88%"   rowspan="3"   style="font-size:09pt;" >
<b> Correcci&oacuten Monetaria de Capital Cuotas de Participaci&oacuten ($)</b>
</TD>
<TD width="8%" >Fecha</TD>
<TD width="10%"  > <%= gLedger.getH01TIMSYS().substring(4,6)%>/<%=gLedger.getH01TIMSYS().substring(2, 4)%>/<%="20" + gLedger.getH01TIMSYS().substring(0, 2)%>
</TD>


</TR>
<TR>
                        

<TD>Hora</TD
><TD> <%= gLedger.getH01TIMSYS().substring(6, 8)%>:<%=gLedger.getH01TIMSYS().substring(8, 10)%>
</TD>


</TR>
<TR>
                        
<TD>Usuario</TD><TD> <%= gLedger.getH01USERID().trim()%></TD>
</TR>


</TABLE>

<br>
<br>






<TABLE  width="100%" border="1" frame="border" >

<TR  id="trdark">
<TH width="15%">Mes</TH>
<TH width="15%">Saldo Capital ($)</TH>
<TH width="10%">Correccion Monetaria ($)</TH>
<TH width="10%">Saldo Reajustes ($)</TH>
<TH width="10%">Saldo Nro. Cuotas</TH>
<TH width="10%">Aporte Neto ($)</TH>
<TH width="5%">%CM</TH>
<TH width="10%">Monto CM ($)</TH>
<TH width="15%">Capital Actualizado ($)</TH>
</TR>

<%
    double SCAPY = Double.parseDouble(gLedger.getE01SCAPY());
    double SCMPY = Double.parseDouble(gLedger.getE01SCMPY());
    double SREPY = Double.parseDouble(gLedger.getE01SREPY());
    double SNCPY = Double.parseDouble(gLedger.getE01SNCPY());
    double APNPY = Double.parseDouble(gLedger.getE01APNPY());
    double MCMPY = Double.parseDouble(gLedger.getE01MCMPY()); 
    double CACPY = Double.parseDouble(gLedger.getE01CACPY()); 
%>





<TR id="trclear">
<TD>Cierre a&ntildeo Anterior</TD>
<TD   align = "left"  nowrap><input size=  "21"  type="text" name="E01SCAPY" value="<%=fmtDouble(SCAPY)%>"   class="TXTRIGHT2"  readonly> </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SCMPY" value="<%=fmtDouble(SCMPY)%>"   class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SREPY" value="<%=fmtDouble(SREPY)%>"   class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SNCPY" value="<%=fmtDouble(SNCPY)%>"   class="TXTRIGHT2"  readonly> </TD>
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01APNPY" value="<%=fmtDouble(APNPY)%>"   class="TXTRIGHT2"  readonly  >  </TD> 
<% if (gLedger.getE01PYEAR().equals("1")) {%>
		<TD   align = "right"       ><input type="text" name="E01PCMPY"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCMPY().trim()%>"  class="TXTRIGHT2"  > </TD>
<% } else { %>
		<TD   align = "right"       ><input type="text" name="E01PCMPY"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCMPY().trim()%>"  class="TXTRIGHT2" readonly > </TD>
<% } %>

<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01MCMPY" value="<%=fmtDouble(MCMPY)%>"   class="TXTRIGHT2"  readonly>  </TD>  
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01CACPY" value="<%=fmtDouble(CACPY)%>"   class="TXTRIGHT2"  readonly></TD>
</TR>

<%
    double SCA01 = Double.parseDouble(gLedger.getE01SCA01());
    double SCM01 = Double.parseDouble(gLedger.getE01SCM01());
    double SRE01 = Double.parseDouble(gLedger.getE01SRE01());
    double SNC01 = Double.parseDouble(gLedger.getE01SNC01());
    double APN01 = Double.parseDouble(gLedger.getE01APN01());
    double MCM01 = Double.parseDouble(gLedger.getE01MCM01()); 
    double CAC01 = Double.parseDouble(gLedger.getE01CAC01()); 
%>




<TR id="trdark"> 
<TD>Enero</TD>               
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01SCA01" value="<%=fmtDouble(SCA01)%>"   class="TXTRIGHT"  readonly> </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SCM01" value="<%=fmtDouble(SCM01)%>"   class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SRE01" value="<%=fmtDouble(SRE01)%>"   class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SNC01" value="<%=fmtDouble(SNC01)%>"   class="TXTRIGHT"  readonly> </TD>
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01APN01" value="<%=fmtDouble(APN01)%>"   class="TXTRIGHT"  readonly  >  </TD>  

<% if (gLedger.getE01MES01().equals("1")) {%>
<TD   align = "right"><input type="text"  name="E01PCM01"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM01().trim()%>" class="TXTRIGHT"  > </TD> 
<% } else { %>
<TD   align = "right"><input type="text"  name="E01PCM01"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM01().trim()%>" class="TXTRIGHT"  readonly  > </TD> 
<% } %>


<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM01" value="<%=fmtDouble(MCM01)%>"    class="TXTRIGHT"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC01" value="<%=fmtDouble(CAC01)%>"    class="TXTRIGHT"  readonly></TD>
</TR>



<%
    double SCA02 = Double.parseDouble(gLedger.getE01SCA02());
    double SCM02 = Double.parseDouble(gLedger.getE01SCM02());
    double SRE02 = Double.parseDouble(gLedger.getE01SRE02());
    double SNC02 = Double.parseDouble(gLedger.getE01SNC02());
    double APN02 = Double.parseDouble(gLedger.getE01APN02());
    double MCM02 = Double.parseDouble(gLedger.getE01MCM02()); 
    double CAC02 = Double.parseDouble(gLedger.getE01CAC02()); 
%>






<TR id="trclear"><TD>Febrero</TD>            
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01SCA02" value="<%=fmtDouble(SCA02)%>"    class="TXTRIGHT2"  readonly> </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SCM02" value="<%=fmtDouble(SCM02)%>"    class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SRE02" value="<%=fmtDouble(SRE02)%>"    class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SNC02" value="<%=fmtDouble(SNC02)%>"    class="TXTRIGHT2"  readonly> </TD>
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01APN02" value="<%=fmtDouble(APN02)%>"    class="TXTRIGHT2"  readonly  >  </TD>  

<% if (gLedger.getE01MES02().equals("1")) {%>
<TD   align = "right"><input type="text" name="E01PCM02"  onkeypress=" enterDecimal(2) " size="7" maxlength="6"   value="<%= gLedger.getE01PCM02().trim()%>"  class="TXTRIGHT2"  > </TD> 
<% } else { %>
<TD   align = "right"><input type="text" name="E01PCM02"  onkeypress=" enterDecimal(2) " size="7" maxlength="6"   value="<%= gLedger.getE01PCM02().trim()%>"  class="TXTRIGHT2"  readonly  > </TD> 
<% } %>


<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM02" value="<%=fmtDouble(MCM02)%>"     class="TXTRIGHT2"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC02" value="<%=fmtDouble(CAC02)%>"     class="TXTRIGHT2"  readonly></TD>
</TR>

<%
    double SCA03 = Double.parseDouble(gLedger.getE01SCA03());
    double SCM03 = Double.parseDouble(gLedger.getE01SCM03());
    double SRE03 = Double.parseDouble(gLedger.getE01SRE03());
    double SNC03 = Double.parseDouble(gLedger.getE01SNC03());
    double APN03 = Double.parseDouble(gLedger.getE01APN03());
    double MCM03 = Double.parseDouble(gLedger.getE01MCM03()); 
    double CAC03 = Double.parseDouble(gLedger.getE01CAC03()); 
%>





<TR id="trdark"><TD>Marzo</TD>               
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01SCA03" value="<%=fmtDouble(SCA03)%>"    class="TXTRIGHT"  readonly> </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SCM03" value="<%=fmtDouble(SCM03)%>"    class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SRE03" value="<%=fmtDouble(SRE03)%>"    class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SNC03" value="<%=fmtDouble(SNC03)%>"    class="TXTRIGHT"  readonly> </TD>
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01APN03" value="<%=fmtDouble(APN03)%>"    class="TXTRIGHT"  readonly  >  </TD>  

<% if (gLedger.getE01MES03().equals("1")) {%>
<TD   align = "right"><input type="text"  name="E01PCM03"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM03().trim()%>" class="TXTRIGHT"  > </TD> 
<% } else { %>
<TD   align = "right"><input type="text"  name="E01PCM03"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM03().trim()%>" class="TXTRIGHT"  readonly  > </TD> 
<% } %>



<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM03" value="<%=fmtDouble(MCM03)%>"     class="TXTRIGHT"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC03" value="<%=fmtDouble(CAC03)%>"     class="TXTRIGHT"  readonly></TD>
</TR>

<%
    double SCA04 = Double.parseDouble(gLedger.getE01SCA04());
    double SCM04 = Double.parseDouble(gLedger.getE01SCM04());
    double SRE04 = Double.parseDouble(gLedger.getE01SRE04());
    double SNC04 = Double.parseDouble(gLedger.getE01SNC04());
    double APN04 = Double.parseDouble(gLedger.getE01APN04());
    double MCM04 = Double.parseDouble(gLedger.getE01MCM04()); 
    double CAC04 = Double.parseDouble(gLedger.getE01CAC04()); 
%>




<TR id="trclear"><TD>Abril</TD>              
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01SCA04" value="<%=fmtDouble(SCA04)%>"    class="TXTRIGHT2"  readonly> </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SCM04" value="<%=fmtDouble(SCM04)%>"    class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SRE04" value="<%=fmtDouble(SRE04)%>"    class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SNC04" value="<%=fmtDouble(SNC04)%>"    class="TXTRIGHT2"  readonly> </TD>
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01APN04" value="<%=fmtDouble(APN04)%>"    class="TXTRIGHT2"  readonly  >  </TD>  

<% if (gLedger.getE01MES04().equals("1")) {%>
<TD   align = "right"><input type="text" name="E01PCM04"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM04().trim()%>"  class="TXTRIGHT2"  > </TD> 
<% } else { %>
<TD   align = "right"><input type="text" name="E01PCM04"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM04().trim()%>"  class="TXTRIGHT2"  readonly  > </TD> 
<% } %>



<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM04" value="<%=fmtDouble(MCM04)%>"     class="TXTRIGHT2"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC04" value="<%=fmtDouble(CAC04)%>"     class="TXTRIGHT2"  readonly></TD>
</TR>


<%
    double SCA05 = Double.parseDouble(gLedger.getE01SCA05());
    double SCM05 = Double.parseDouble(gLedger.getE01SCM05());
    double SRE05 = Double.parseDouble(gLedger.getE01SRE05());
    double SNC05 = Double.parseDouble(gLedger.getE01SNC05());
    double APN05 = Double.parseDouble(gLedger.getE01APN05());
    double MCM05 = Double.parseDouble(gLedger.getE01MCM05()); 
    double CAC05 = Double.parseDouble(gLedger.getE01CAC05()); 
%>




<TR id="trdark"><TD>Mayo</TD>                
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01SCA05" value="<%=fmtDouble(SCA05)%>"    class="TXTRIGHT"  readonly> </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SCM05" value="<%=fmtDouble(SCM05)%>"    class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SRE05" value="<%=fmtDouble(SRE05)%>"    class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SNC05" value="<%=fmtDouble(SNC05)%>"    class="TXTRIGHT"  readonly> </TD>
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01APN05" value="<%=fmtDouble(APN05)%>"    class="TXTRIGHT"  readonly  >  </TD>  

<% if (gLedger.getE01MES05().equals("1")) {%>
<TD   align = "right"><input type="text"  name="E01PCM05"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM05().trim()%>" class="TXTRIGHT"  > </TD> 
<% } else { %>
<TD   align = "right"><input type="text"  name="E01PCM05"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM05().trim()%>" class="TXTRIGHT"  readonly  > </TD> 
<% } %>


<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM05" value="<%=fmtDouble(MCM05)%>"     class="TXTRIGHT"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC05" value="<%=fmtDouble(CAC05)%>"     class="TXTRIGHT"  readonly></TD>
</TR>


<%
    double SCA06 = Double.parseDouble(gLedger.getE01SCA06());
    double SCM06 = Double.parseDouble(gLedger.getE01SCM06());
    double SRE06 = Double.parseDouble(gLedger.getE01SRE06());
    double SNC06 = Double.parseDouble(gLedger.getE01SNC06());
    double APN06 = Double.parseDouble(gLedger.getE01APN06());
    double MCM06 = Double.parseDouble(gLedger.getE01MCM06()); 
    double CAC06 = Double.parseDouble(gLedger.getE01CAC06()); 
%>





<TR id="trclear"><TD>Junio</TD>              
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01SCA06" value="<%=fmtDouble(SCA06)%>"    class="TXTRIGHT2"  readonly> </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SCM06" value="<%=fmtDouble(SCM06)%>"    class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SRE06" value="<%=fmtDouble(SRE06)%>"    class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size=  "15"  type="text" name="E01SNC06" value="<%=fmtDouble(SNC06)%>"    class="TXTRIGHT2"  readonly> </TD>
<TD   align = "right" nowrap><input size=  "21"  type="text" name="E01APN06" value="<%=fmtDouble(APN06)%>"    class="TXTRIGHT2"  readonly  >  </TD>  

<% if (gLedger.getE01MES06().equals("1")) {%>
<TD   align = "right"><input type="text" name="E01PCM06"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM06().trim()%>" class="TXTRIGHT2"   > </TD> 
<% } else { %>
<TD   align = "right"><input type="text" name="E01PCM06"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM06().trim()%>" class="TXTRIGHT2"  readonly   > </TD> 
<% } %>


<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM06" value="<%=fmtDouble(MCM06)%>"     class="TXTRIGHT2"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC06" value="<%=fmtDouble(CAC06)%>"     class="TXTRIGHT2"  readonly></TD>
</TR>


<%
    double SCA07 = Double.parseDouble(gLedger.getE01SCA07());
    double SCM07 = Double.parseDouble(gLedger.getE01SCM07());
    double SRE07 = Double.parseDouble(gLedger.getE01SRE07());
    double SNC07 = Double.parseDouble(gLedger.getE01SNC07());
    double APN07 = Double.parseDouble(gLedger.getE01APN07());
    double MCM07 = Double.parseDouble(gLedger.getE01MCM07()); 
    double CAC07 = Double.parseDouble(gLedger.getE01CAC07()); 
%>





<TR id="trdark"><TD>Julio</TD>               
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01SCA07" value="<%=fmtDouble(SCA07)%>"     class="TXTRIGHT"  readonly> </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SCM07" value="<%=fmtDouble(SCM07)%>"     class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SRE07" value="<%=fmtDouble(SRE07)%>"     class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SNC07" value="<%=fmtDouble(SNC07)%>"     class="TXTRIGHT"  readonly> </TD>
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01APN07" value="<%=fmtDouble(APN07)%>"     class="TXTRIGHT"  readonly  >  </TD>  

<% if (gLedger.getE01MES07().equals("1")) {%>
<TD   align = "right"><input type="text"  name="E01PCM07"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM07().trim()%>" class="TXTRIGHT"  > </TD> 
<% } else { %>
<TD   align = "right"><input type="text"  name="E01PCM07"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM07().trim()%>" class="TXTRIGHT"  readonly  > </TD> 
<% } %>


<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM07" value="<%=fmtDouble(MCM07)%>"    class="TXTRIGHT"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC07" value="<%=fmtDouble(CAC07)%>"    class="TXTRIGHT"  readonly></TD>
</TR>


<%
    double SCA08 = Double.parseDouble(gLedger.getE01SCA08());
    double SCM08 = Double.parseDouble(gLedger.getE01SCM08());
    double SRE08 = Double.parseDouble(gLedger.getE01SRE08());
    double SNC08 = Double.parseDouble(gLedger.getE01SNC08());
    double APN08 = Double.parseDouble(gLedger.getE01APN08());
    double MCM08 = Double.parseDouble(gLedger.getE01MCM08()); 
    double CAC08 = Double.parseDouble(gLedger.getE01CAC08()); 
%>





<TR id="trclear"><TD>Agosto</TD>             
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01SCA08" value="<%=fmtDouble(SCA08)%>"    class="TXTRIGHT2"  readonly> </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SCM08" value="<%=fmtDouble(SCM08)%>"    class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SRE08" value="<%=fmtDouble(SRE08)%>"    class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SNC08" value="<%=fmtDouble(SNC08)%>"    class="TXTRIGHT2"  readonly> </TD>
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01APN08" value="<%=fmtDouble(APN08)%>"    class="TXTRIGHT2"  readonly  >  </TD>  

<% if (gLedger.getE01MES08().equals("1")) {%>
<TD   align = "right"><input type="text" name="E01PCM08"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM08().trim()%>" class="TXTRIGHT2"   > </TD> 
<% } else { %>
<TD   align = "right"><input type="text" name="E01PCM08"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM08().trim()%>" class="TXTRIGHT2"   readonly  > </TD> 
<% } %>



<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM08" value="<%=fmtDouble(MCM08)%>"    class="TXTRIGHT2"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC08" value="<%=fmtDouble(CAC08)%>"    class="TXTRIGHT2"  readonly></TD>
</TR>

<%
    double SCA09 = Double.parseDouble(gLedger.getE01SCA09());
    double SCM09 = Double.parseDouble(gLedger.getE01SCM09());
    double SRE09 = Double.parseDouble(gLedger.getE01SRE09());
    double SNC09 = Double.parseDouble(gLedger.getE01SNC09());
    double APN09 = Double.parseDouble(gLedger.getE01APN09());
    double MCM09 = Double.parseDouble(gLedger.getE01MCM09()); 
    double CAC09 = Double.parseDouble(gLedger.getE01CAC09()); 
%>





<TR id="trdark"><TD>Septiembre</TD>          
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01SCA09" value="<%=fmtDouble(SCA09)%>"    class="TXTRIGHT"  readonly> </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SCM09" value="<%=fmtDouble(SCM09)%>"    class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SRE09" value="<%=fmtDouble(SRE09)%>"    class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SNC09" value="<%=fmtDouble(SNC09)%>"    class="TXTRIGHT"  readonly> </TD>
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01APN09" value="<%=fmtDouble(APN09)%>"    class="TXTRIGHT"  readonly  >  </TD>  

<% if (gLedger.getE01MES09().equals("1")) {%>
<TD   align = "right"><input type="text"  name="E01PCM09"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM09().trim()%>" class="TXTRIGHT"  > </TD> 
<% } else { %>
<TD   align = "right"><input type="text"  name="E01PCM09"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM09().trim()%>" class="TXTRIGHT"  readonly  > </TD> 
<% } %>




<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM09" value="<%=fmtDouble(MCM09)%>"    class="TXTRIGHT"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC09" value="<%=fmtDouble(CAC09)%>"    class="TXTRIGHT"  readonly></TD>
</TR>


<%
    double SCA10 = Double.parseDouble(gLedger.getE01SCA10());
    double SCM10 = Double.parseDouble(gLedger.getE01SCM10());
    double SRE10 = Double.parseDouble(gLedger.getE01SRE10());
    double SNC10 = Double.parseDouble(gLedger.getE01SNC10());
    double APN10 = Double.parseDouble(gLedger.getE01APN10());
    double MCM10 = Double.parseDouble(gLedger.getE01MCM10()); 
    double CAC10 = Double.parseDouble(gLedger.getE01CAC10()); 
%>




<TR id="trclear"><TD>Octubre</TD>            
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01SCA10" value="<%=fmtDouble(SCA10)%>"    class="TXTRIGHT2"  readonly> </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SCM10" value="<%=fmtDouble(SCM10)%>"    class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SRE10" value="<%=fmtDouble(SRE10)%>"    class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SNC10" value="<%=fmtDouble(SNC10)%>"    class="TXTRIGHT2"  readonly> </TD>
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01APN10" value="<%=fmtDouble(APN10)%>"    class="TXTRIGHT2"  readonly  >  </TD>  

<% if (gLedger.getE01MES10().equals("1")) {%>
<TD   align = "right"><input type="text" name="E01PCM10"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM10().trim()%>" class="TXTRIGHT2"   > </TD> 
<% } else { %>
<TD   align = "right"><input type="text" name="E01PCM10"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM10().trim()%>" class="TXTRIGHT2"   readonly  > </TD> 
<% } %>




<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM10" value="<%=fmtDouble(MCM10)%>"    class="TXTRIGHT2"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC10" value="<%=fmtDouble(CAC10)%>"    class="TXTRIGHT2"  readonly></TD>
</TR>

<%
    double SCA11 = Double.parseDouble(gLedger.getE01SCA11());
    double SCM11 = Double.parseDouble(gLedger.getE01SCM11());
    double SRE11 = Double.parseDouble(gLedger.getE01SRE11());
    double SNC11 = Double.parseDouble(gLedger.getE01SNC11());
    double APN11 = Double.parseDouble(gLedger.getE01APN11());
    double MCM11 = Double.parseDouble(gLedger.getE01MCM11()); 
    double CAC11 = Double.parseDouble(gLedger.getE01CAC11()); 
%>






<TR id="trdark"><TD>Noviembre</TD>           
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01SCA11" value="<%=fmtDouble(SCA11)%>"     class="TXTRIGHT"  readonly> </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SCM11" value="<%=fmtDouble(SCM11)%>"     class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SRE11" value="<%=fmtDouble(SRE11)%>"     class="TXTRIGHT"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SNC11" value="<%=fmtDouble(SNC11)%>"     class="TXTRIGHT"  readonly> </TD>
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01APN11" value="<%=fmtDouble(APN11)%>"     class="TXTRIGHT"  readonly  >  </TD>  

<% if (gLedger.getE01MES11().equals("1")) {%>
<TD   align = "right"><input type="text"  name="E01PCM11"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM11().trim()%>" class="TXTRIGHT" > </TD> 
<% } else { %>
<TD   align = "right"><input type="text"  name="E01PCM11"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM11().trim()%>" class="TXTRIGHT"  readonly > </TD> 
<% } %>



<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM11" value="<%=fmtDouble(MCM11)%>"     class="TXTRIGHT"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC11" value="<%=fmtDouble(CAC11)%>"     class="TXTRIGHT"  readonly>  </TD>
</TR>


<%
    double SCA12 = Double.parseDouble(gLedger.getE01SCA12());
    double SCM12 = Double.parseDouble(gLedger.getE01SCM12());
    double SRE12 = Double.parseDouble(gLedger.getE01SRE12());
    double SNC12 = Double.parseDouble(gLedger.getE01SNC12());
    double APN12 = Double.parseDouble(gLedger.getE01APN12());
    double MCM12 = Double.parseDouble(gLedger.getE01MCM12()); 
    double CAC12 = Double.parseDouble(gLedger.getE01CAC12()); 
%>





<TR id="trclear"><TD>Diciembre</TD>          
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01SCA12" value="<%=fmtDouble(SCA12)%>"     class="TXTRIGHT2"  readonly> </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SCM12" value="<%=fmtDouble(SCM12)%>"     class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SRE12" value="<%=fmtDouble(SRE12)%>"     class="TXTRIGHT2"  readonly > </TD> 
<TD   align = "right" nowrap><input size= "15"  type="text" name="E01SNC12" value="<%=fmtDouble(SNC12)%>"     class="TXTRIGHT2"  readonly> </TD>
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01APN12" value="<%=fmtDouble(APN12)%>"     class="TXTRIGHT2"  readonly  >  </TD>  

<% if (gLedger.getE01MES12().equals("1")) {%>
<TD   align = "right"><input type="text" name="E01PCM12"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM12().trim() %>" class="TXTRIGHT2"   > </TD> 
<% } else { %>
<TD   align = "right"><input type="text" name="E01PCM12"  onkeypress=" enterDecimal(2) " size="7" maxlength="6" value="<%= gLedger.getE01PCM12().trim()%>" class="TXTRIGHT2"  readonly > </TD> 
<% } %>

<TD   align = "right" nowrap><input size= "15"  type="text" name="E01MCM12" value="<%=fmtDouble(MCM12)%>"     class="TXTRIGHT2"  readonly>  </TD>  
<TD   align = "right" nowrap><input size= "21"  type="text" name="E01CAC12" value="<%=fmtDouble(CAC12)%>"     class="TXTRIGHT2"  readonly></TD>
</TR>

<%
    double TAPN = Double.parseDouble(gLedger.getE01TAPN());
    double TMCM = Double.parseDouble(gLedger.getE01TMCM());
    double TCAC = Double.parseDouble(gLedger.getE01TCAC());
%>







<TR id="trdark" >
<TD  width="15%"><b>Totales</b></TD>
<TD  width="15%"  align = "right"></TD>
<TD  width="10%"  align = "right"></TD>
<TD  width="10%"  align = "right"></TD>
<TD  width="10%"  align = "right"></TD>
<TD  width="10%"  align = "right"> <b> <input size= "21"  type="text" name="E01TAPN" value= "<%=fmtDouble(TAPN)%>" class="TXTRIGHT3"  readonly > </b> </TD>
<TD  width="5%"   align = "right"></TD>
<TD  width="10%"  align = "right"><b> <input size= "15"  type="text" name="E01TMCM"  value= "<%=fmtDouble(TMCM)%>" class="TXTRIGHT3"  readonly > </b></TD>
<TD  width="15%"  align = "right"><b> <input size= "21"  type="text" name="E01TCAC"  value= "<%=fmtDouble(TCAC)%>" class="TXTRIGHT3"  readonly > </b></TD>
</TR>    


</TABLE>


<br>
<br>

<%
    double CMCA = Double.parseDouble(gLedger.getE01CMCA());
    double REAR = Double.parseDouble(gLedger.getE01REAR());
    double AJUC = Double.parseDouble(gLedger.getE01AJUC());
%>



<TABLE   width="30%" border="1" >

<TR  id="trdark"> <TD width="15%"   align="left">Correccion Monetaria ($)</TD><TD width="15%" align = "right" nowrap><%=fmtDouble(CMCA)%></TD></TR>
<TR  id="trclear"><TD               align="left">Reajustes Reales ($)    </TD>      <TD             align = "right" nowrap><%=fmtDouble(REAR)%></TD></TR>
<TR  id="trdark"> <TD               align="left">Ajuste Contable ($)     </TD>      <TD             align = "right" nowrap><%=fmtDouble(AJUC)%></TD></TR>


</TABLE>

<br>
<br>

<TABLE   width="30%"  frame="border">

<%
    double NCUO = Double.parseDouble(gLedger.getE01NCUO());
    double CREA = Double.parseDouble(gLedger.getE01CREA());
%>



<TR   id="trdark"><TD width="15%" align="left">Valor Cuota Actual ($)</TD><TD width="15%" align = "right" nowrap><%=gLedger.getE01VCUO()%></TD></TR>
<TR   id="trclear"><TD  align="left">Nro de Cuotas Total</TD> <TD align = "right" nowrap><%=fmtDouble(NCUO)%></TD></TR>
<TR   id="trdark"><TD  align="left">Capital Reajustado  ($)   </TD>   <TD align = "right" nowrap><%=fmtDouble(CREA)%></TD></TR>
<TR   id="trclear"><TD  align="left">Valor Cuota Estimado ($) </TD><TD align = "right" nowrap><%=gLedger.getE01VEST()%></TD></TR>


</TABLE>





  <p>
  <div align="center"> 
	   <input id="EIBSBTN" type=submit name="Enviar" value="Calcular"  >
  </div>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
