/**
 *  Automatic Numbering Maintenance
 *  
 * 	Update CNTRLNUM * 
 * Author: Henry G.
 * Date  : 02/03/2011
 * Source File Name:   JSESD0003.java
 *		SCREEN = 100 Request Portfolio Maintenance Selection Screen
 *		SCREEN = 200 Submit Portfolio Selection and send Basic Screen
 *		SCREEN = 300 Submit Portfolio Maintenance from Basic Screen
 *		SCREEN = 500 Portfolio Inquiry opcod 3
 *		SCREEN = 501 Portfolio Inquiry from Approval opcod 2
 **/
package datapro.eibs.params;

import datapro.eibs.beans.*;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.*;
import java.io.*;
import java.net.Socket;
import java.util.Enumeration;
import javax.servlet.*;
import javax.servlet.http.*;

public class JSESD0003 extends SuperServlet {

    protected String LangPath;

    public JSESD0003() {
        LangPath = "S";
    }

    public void destroy() {
        flexLog("free resources used by JSESD0003");
    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    
    
	//	*------------------------------------------------------------------------------------------------------*
	//	SHOW MAIN SCREEN
	//	SCREEN = 100
	//
    
	protected  void procReqEnter(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			ELEERRMessage msgError = null;
			UserPos userPO = null;

			

			try {
				msgError = new ELEERRMessage();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
							
			try {	
				userPO = new UserPos();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}		
				
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);

			

			try {
				flexLog("About to call Page: "	+ LangPath	+ "ESD0003_auto_numbering_main_frame.jsp");
				callPage(LangPath + "ESD0003_auto_numbering_main_frame.jsp", req, res);
			} catch (Exception e) {
				flexLog("Exception calling page " + e);
			}

		}
    
    
	//	*------------------------------------------------------------------------------------------------------*
	//	SHOW LIST OF PRODUCT TYPE 
	//	SCREEN = 150
	//    
            
    protected void procReqList(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses) throws ServletException, IOException {    	
        MessageRecord newmessage = null;
        ESD000301Message msgList = null;
        ELEERRMessage msgError = null;
        UserPos userPO = null;
        boolean IsNotError = false;
        JBObjList beanList = null;
        
        try {
            msgError = new ELEERRMessage();
        }
        catch(Exception ex) {
            flexLog("Error: " + ex);
        }
        userPO = (UserPos)ses.getAttribute("userPO");
        
        String scCode = null;
        if(req.getParameter("CFL") == null)
            scCode = "AP";
        else
            try {
                scCode = req.getParameter("CFL");
            }
            catch(Exception e) {
                scCode = "AP";
            }
                      
        try {
            msgList = (ESD000301Message)mc.getMessageRecord("ESD000301");
            msgList.setH01USERID(user.getH01USR());
            msgList.setH01PROGRM("ESD0003");
            msgList.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgList.setH01SCRCOD(scCode);
            msgList.setH01OPECOD("1000");
            
            // Send data to Host
            msgList.send();
            msgList.destroy();
            flexLog("ESD000301 Message Sent");
        }
        catch(Exception e) {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
		try {
			// Receive Message
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {
	
				msgError = (ELEERRMessage)newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
	
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
	
				try {
					flexLog("About to call Page: " + LangPath + "ESD0003_auto_numbering_blank_frame.jsp");
					callPage(LangPath + "ESD0003_auto_numbering_blank_frame.jsp", req, res);						
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
								
			} else if (newmessage.getFormatName().equals("ESD000301")) {
				
				beanList = new JBObjList();
				String marker = "";
				msgList = null;
				boolean firsttime = true;
	
				while (true) {
					msgList = (ESD000301Message) newmessage;
					marker = msgList.getE01INDOPE();
		
					if (firsttime == true) {
						firsttime = false;
					//	userPO.setIdentifier(msgList.getE01NUMATY());
					}
					if (marker.equals("*")) {
						beanList.setShowNext(false);
						break;
					} else {
						beanList.addRow(msgList);
						if (marker.equals("+")) {
							beanList.setShowNext(true);
							break;
						}
					}
					newmessage = mc.receiveMessage();
				}
					
								
				flexLog("Putting java beans into the session");
				ses.setAttribute("dvList", beanList);
				ses.setAttribute("userPO", userPO);	
	
				try {
					flexLog("About to call Page: " + LangPath + "ESD0003_auto_numbering_list_frame.jsp");
					callPage(LangPath + "ESD0003_auto_numbering_list_frame.jsp", req, res);						
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
					
			} else {
				flexLog("Message " + newmessage.getFormatName() + " received.");
			}
							
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
    }


	//	*------------------------------------------------------------------------------------------------------*
	//	SHOW LIST OF PRODUCTS
	//	SCREEN = 200
	//
    protected void procShowList(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses) throws ServletException, IOException {
        MessageRecord newmessage = null;
        ESD000301Message msgList = null;
        ELEERRMessage msgError = null;
        UserPos userPO = null;
        boolean IsNotError = false;
        JBObjList beanList = null;
        
        try {
            msgError = new ELEERRMessage();
        }
        catch(Exception ex) {
            flexLog("Error: " + ex);
        }
        userPO = (UserPos)ses.getAttribute("userPO");
        
        String scCode = null;
        if(req.getParameter("CFL") == null)
            scCode = "04";
        else
            try {
                scCode = req.getParameter("CFL");
            }
            catch(Exception e) {
                scCode = "04";
            }
        
        
        String apCode = null;
        if(req.getParameter("ACD") == null)
            apCode = "01";
        else
            try {
                apCode = req.getParameter("ACD");
            }
            catch(Exception e) {
                apCode = "01";
            }            
      
        try {
            msgList = (ESD000301Message)mc.getMessageRecord("ESD000301");
            msgList.setH01USERID(user.getH01USR());
            msgList.setH01PROGRM("ESD0003");
            msgList.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgList.setH01SCRCOD(scCode);
            msgList.setH01OPECOD("0001");
            msgList.setE01NUMACD(apCode);
            
            // Send data to Host
            msgList.send();
            msgList.destroy();
            flexLog("ESD000301 Message Sent");
        }
        catch(Exception e) {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
		try {
			// Receive Message
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {
	
				msgError = (ELEERRMessage)newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
	
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
	
				try {
					flexLog("About to call Page: " + LangPath + "ESD0003_auto_numbering_blank_frame.jsp");
					callPage(LangPath + "ESD0003_auto_numbering_blank_frame.jsp", req, res);						
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
								
			} else if (newmessage.getFormatName().equals("ESD000301")) {
				
				beanList = new JBObjList();
				String marker = "";
				msgList = null;
				boolean firsttime = true;
	
				while (true) {
					msgList = (ESD000301Message) newmessage;
					marker = msgList.getE01INDOPE();
		
					if (firsttime == true) {
						firsttime = false;
					//	userPO.setIdentifier(msgList.getE01NUMATY());
					}
					if (marker.equals("*")) {
						beanList.setShowNext(false);
						break;
					} else {
						beanList.addRow(msgList);
						if (marker.equals("+")) {
							beanList.setShowNext(true);
							break;
						}
					}
					newmessage = mc.receiveMessage();
				}
					
								
				flexLog("Putting java beans into the session");
				ses.setAttribute("dvList", beanList);
				ses.setAttribute("userPO", userPO);	
	
				try {
					flexLog("About to call Page: " + LangPath + "ESD0003_auto_numbering_list_product.jsp");
					callPage(LangPath + "ESD0003_auto_numbering_list_product.jsp", req, res);						
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
					
			} else {
				flexLog("Message " + newmessage.getFormatName() + " received.");
			}
							
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
    }

    
	//  *------------------------------------------------------------------------------------------------------*
	//	DISPLAY NUMBERING PAGE
	//	SCREEN = 300
	//
    protected void procShowNumbering(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses) throws ServletException, IOException {
        MessageRecord newmessage = null;
        ESD000301Message msgAutoNum = null;
        ELEERRMessage msgError = null;
        UserPos userPO = null;
        boolean IsNotError = false;
        try {
            msgError = new ELEERRMessage();
        }
        catch(Exception ex) {
            flexLog("Error: " + ex);
        }
        userPO = (UserPos)ses.getAttribute("userPO");
        try {
            flexLog("Send Initial Data");
            // msgAutoNum = (ESD000301Message)ses.getAttribute("autoNum");
            msgAutoNum = (ESD000301Message)mc.getMessageRecord("ESD000301");
            msgAutoNum.setH01USERID(user.getH01USR());
            msgAutoNum.setH01PROGRM("ESD0003");
            msgAutoNum.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgAutoNum.setH01SCRCOD("01");
            msgAutoNum.setH01OPECOD("0002");
            Enumeration enu = msgAutoNum.fieldEnumeration();
            MessageField field = null;
            String value = null;
            while(enu.hasMoreElements())  {
                field = (MessageField)enu.nextElement();
                try {
                    value = req.getParameter(field.getTag()).toUpperCase().trim();
                    if(value != null)
                        field.setString(value);
                }
                catch(Exception exception) { }
            }

            mc.sendMessage(msgAutoNum);
            msgAutoNum.destroy();
            flexLog("ESD000301 Message Sent");
        }
        catch(Exception e) {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR")) {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
            } else {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try {
            newmessage = mc.receiveMessage();
            try {
                msgAutoNum = new ESD000301Message();
            }
            catch(Exception ex) {
                flexLog("Error: " + ex);
            }
            msgAutoNum = (ESD000301Message)newmessage;
            userPO.setIdentifier(msgAutoNum.getE01NUMLST());
            flexLog("Putting java beans into the session");
            ses.setAttribute("error", msgError);
            ses.setAttribute("autoNum", msgAutoNum);
            if(IsNotError) {
               userPO.setPurpose("MAINTENANCE");
               ses.setAttribute("userPO", userPO);
               try {
            	   flexLog("About to call Page: " + LangPath + "ESD0003_auto_numbering_seq_consecutive.jsp");
                   callPage(LangPath + "ESD0003_auto_numbering_seq_consecutive.jsp", req, res);
               } catch(Exception e) {
                   flexLog("Exception calling page " + e);
               }
            } 
        }
        catch(Exception e) {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
    }


	//	*------------------------------------------------------------------------------------------------------*
	//	PROCESSING AUTOMATIC NUMBERING
	//	SCREEN 400
	//
	protected void proAutomaticNumbering(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses) throws ServletException, IOException {
			MessageRecord newmessage = null;
			ESD000301Message msgAutoNum = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			boolean IsNotError = false;
			try {
				msgError = new ELEERRMessage();
			}
			catch(Exception ex) {
				flexLog("Error: " + ex);
			}
			userPO = (UserPos)ses.getAttribute("userPO");
			try {
				msgAutoNum = (ESD000301Message)mc.getMessageRecord("ESD000301");
				msgAutoNum.setH01USERID(user.getH01USR());
				msgAutoNum.setH01PROGRM("ESD0003");
				msgAutoNum.setH01TIMSYS(SuperServlet.getTimeStamp());
				msgAutoNum.setH01SCRCOD("01");
				msgAutoNum.setH01OPECOD("0003");
	            Enumeration enu = msgAutoNum.fieldEnumeration();
	            MessageField field = null;
	            String value = null;
	            while(enu.hasMoreElements())  {
	                field = (MessageField)enu.nextElement();
	                try {
	                    value = req.getParameter(field.getTag()).toUpperCase().trim();
	                    if(value != null)
	                        field.setString(value);
	                }
	                catch(Exception exception) { }
	            }
															
				msgAutoNum.send();
				msgAutoNum.destroy();
				flexLog("ESD000301 Message Sent");
			}
			catch(Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}
			try {
				newmessage = mc.receiveMessage();
				if(newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage)newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else {
					flexLog("Message " + newmessage.getFormatName() + " received.");
				}
			}
			catch(Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}
			try {
				newmessage = mc.receiveMessage();
				if(newmessage.getFormatName().equals("ESD000301")) {
					try {
						msgAutoNum = new ESD000301Message();
					}
					catch(Exception ex) {
						flexLog("Error: " + ex);
					}
					msgAutoNum = (ESD000301Message)newmessage;
					flexLog("Putting java beans into the session");
					ses.setAttribute("error", msgError);
					if(!IsNotError) {	// Errors Found
						ses.setAttribute("userPO", userPO);
						ses.setAttribute("autoNum", msgAutoNum);
//						try {
//							flexLog("About to call Page: " + LangPath + "ESD0088I_cus_port_basic.jsp");
//							callPage(LangPath + "ESD0088I_cus_port_basic.jsp", req, res);
//						}
//						catch(Exception e) {
//							flexLog("Exception calling page " + e);
//						}
						res.sendRedirect(SuperServlet.srctx + "/servlet/datapro.eibs.params.JSESD0003?SCREEN=300&E01NUMACD=" + msgAutoNum.getE01NUMACD() + "&E01NUMATY=" + msgAutoNum.getE01NUMATY() + "&E01NUMDSC=" + msgAutoNum.getE01NUMDSC());
					}
				} else {
					flexLog("Message " + newmessage.getFormatName() + " received.");
				}
			}
			catch(Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}
		}


	//	*------------------------------------------------------------------------------------------------------*
	//	PORTFOLIO MAINTENANCE
	//	MENU
	//
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Socket s = null;
        MessageContext mc = null;
        ESS0030DSMessage msgUser = null;
        HttpSession session = null;
        session = req.getSession(false);
        if(session == null) {
            try {
                res.setContentType("text/html");
                printLogInAgain(res.getWriter());
            }
            catch(Exception e) {
                e.printStackTrace();
                flexLog("Exception ocurred. Exception = " + e);
            }
        } else {
            int screen = 100;
            try {
                msgUser = (ESS0030DSMessage)session.getAttribute("currUser");
                LangPath = SuperServlet.rootPath + msgUser.getE01LAN() + "/";
                try {
                    flexLog("Opennig Socket Connection");
                    s = new Socket(SuperServlet.hostIP, SuperServlet.iniSocket + 1);
                    s.setSoTimeout(SuperServlet.sckTimeOut);
                    mc = new MessageContext(new DataInputStream(new BufferedInputStream(s.getInputStream())), new DataOutputStream(new BufferedOutputStream(s.getOutputStream())), "datapro.eibs.beans");
                    try {
                        screen = Integer.parseInt(req.getParameter("SCREEN"));
                    }
                    catch(Exception e) {
                        flexLog("Screen set to default value");
                    }
                    switch(screen) {
					case 100: // Show main Screen
						procReqEnter(msgUser, req, res, session);
						break;
					
					case 150: // Show list of Product Types
						procReqList(mc, msgUser, req, res, session);
						break;
						
					case 200: // Show List of products 
						procShowList(mc, msgUser, req, res, session);
						break;
						
                    case 300: // Submit Maintenance 
                    	procShowNumbering(mc, msgUser, req, res, session);
						break;
    
					case 400:   // Automatic Numbering
						proAutomaticNumbering(mc, msgUser, req, res, session);
					break;


                    default:
                        res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.devPage);
                        break;

                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                    int sck = SuperServlet.iniSocket + 1;
                    flexLog("Socket not Open(Port " + sck + "). Error: " + e);
                    res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.sckNotOpenPage);
                }
                finally {
                    s.close();
                }
            }
            catch(Exception e) {
                flexLog("Error: " + e);
                res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.sckNotRespondPage);
            }
        }
    }


	//	*------------------------------------------------------------------------------------------------------*
	//	ERROR HANDLING
	//	
	//
    protected void showERROR(ELEERRMessage m) {
        if(SuperServlet.logType != 0) {
            flexLog("ERROR received.");
            flexLog("ERROR number:" + m.getERRNUM());
            flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
            flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
            flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
            flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
            flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
            flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
            flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
            flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
            flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
            flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());
        }
    }
}
