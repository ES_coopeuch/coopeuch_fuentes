<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Apertura de Certificados</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%@ page import = "java.io.*,java.net.*,datapro.eibs.beans.*,datapro.eibs.master.*,java.math.*" %>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id="cdNew" class="datapro.eibs.beans.EDL013001Message"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<SCRIPT LANGUAGE="javascript">
 
 
 function CheckSubmit(act)
{
   //document.getElementById('E01DEAMD1').value = "0";
   //document.getElementById('E01DEAMD2').value = "0";
  // document.getElementById('E01DEAMD3').value = "0";
   document.forms[0].ACTION.value=act;
   document.forms[0].submit();
   							
   
}

</SCRIPT>
</head>
<body nowrap>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
      out.println("showErrors()");
     out.println("</SCRIPT>");
     }
     
 boolean protect = JSEIBSProp.getProtectedBNKBRN();
%>
<SCRIPT LANGUAGE="javascript">
builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   if (field.substring(0,8) == "E01OFFAC"){
     var index = field.substr(8,1);
     var concepto=document.getElementById("E01OFFOP"+index).value;
     if (concepto == "01")
       Client=document.getElementById("E01DEACUN").value;
     else
       Client="";
   }
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }

function showMandato(){
	
	var TipoDep = <%= cdNew.getE01DEANR1().trim().indexOf("RENO")%>;
	
	if(TipoDep <= 0)
		document.forms[0].E01DEACLF.value = "C";
	else	
		document.forms[0].E01DEACLF.value = "B";

	
	if( document.forms[0].E01DEAECU.value == "1" ){
				document.getElementById('IDMAN').style.display='block' ;
	} else {
				document.getElementById('IDMAN').style.display='block' ;
	}

}

</SCRIPT>
<h3 align="center">Apertura de <%=cdNew.getE01DEANR1() %> <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cd_opening.jsp,EDL0130" width="32" height="32"> 
</h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXEDL0130" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
  <INPUT TYPE=HIDDEN NAME="ACTION" VALUE="F">
  <input type=HIDDEN name="E01DEAACD" value="<%= cdNew.getE01DEAACD().trim()%>"">
  <input type=HIDDEN name="E01DEAACC" value="<%= cdNew.getE01DEAACC().trim()%>"">
  <input type=HIDDEN name="E01DLXACC" value="<%= cdNew.getE01DLXACC().trim()%>"">
  <input type=HIDDEN name="E01DLXAMT" value="<%= cdNew.getE01DLXAMT().trim()%>"">


  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left">
                <input type="text" name="E01DEACUN" size="10" maxlength="9" value="<%= cdNew.getE01DEACUN().trim()%>">
                <a href="javascript:GetCustomerDescId('E01DEACUN','E01CUSNA1','')">
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a>
                <a href="javascript:NewShortCust('E01DEACUN', 'E01CUSNA1', '')"> Nuevo</a>
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2">
                <input type="text" name="E01CUSNA1" size="45" maxlength="45" value="<%= cdNew.getE01CUSNA1().trim()%>">
                </font></font></font></div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left">
                <input type="text" name="E01DEAACC2" size="15" maxlength="12" value="NUEVA CUENTA">
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
                <input type="text" name="E01DEACCY" size="4" maxlength="3" value="<%= cdNew.getE01DEACCY().trim()%>" readonly>
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b>
                <input type="text" name="E01DEAPRO" size="5" maxlength="4" value="<%= cdNew.getE01DEAPRO().trim()%>" readonly>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Datos B&aacute;sicos de la Operaci&oacute;n</h4> 
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
          <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right">Nombre del Certificado :</div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" name="E01DEANME" size="60" maxlength="80" value="<%= cdNew.getE01DEANME().trim()%>">                       
            </td>
            <td nowrap width="25%">    
            </td>
            <td nowrap width="27%">  
            </td>
          </tr>        
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Fecha de Apertura :</div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" name="E01DEAOD1" size="3" maxlength="2" value="<%= cdNew.getE01DEAOD1().trim()%>">
              <input type="text" name="E01DEAOD2" size="3" maxlength="2" value="<%= cdNew.getE01DEAOD2().trim()%>">
              <input type="text" name="E01DEAOD3" size="5" maxlength="4" value="<%= cdNew.getE01DEAOD3().trim()%>">
            </td>
            <td nowrap width="25%"> 
              <div align="right">T&eacute;rmino :</div>
            </td>
            <td nowrap width="27%"> 
              <input type="text" name="E01DEATRM" size="6" maxlength="5" value="<%= cdNew.getE01DEATRM().trim()%>">
              <select name="E01DEATRC">
                <option value=" " <% if (!(cdNew.getE01DEATRC().equals("D") ||cdNew.getE01DEATRC().equals("M")||cdNew.getE01DEATRC().equals("Y"))) out.print("selected"); %>></option>
                <option value="D" <% if(cdNew.getE01DEATRC().equals("D")) out.print("selected");%>>D&iacute;a(s)</option>
                <option value="M" <% if(cdNew.getE01DEATRC().equals("M")) out.print("selected");%>>Mes(es)</option>
                <option value="Y" <% if(cdNew.getE01DEATRC().equals("Y")) out.print("selected");%>>A&ntilde;o(s)</option>
              </select>
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="absbottom" border="0" > 
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right">Fecha de Vencimiento :</div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" name="E01DEAMD1" id="E01DEAMD1" size="3" maxlength="2" value="<%= cdNew.getE01DEAMD1().trim()%>" readonly>
              <input type="text" name="E01DEAMD2" id="E01DEAMD2" size="3" maxlength="2" value="<%= cdNew.getE01DEAMD2().trim()%>"  readonly>
              <input type="text" name="E01DEAMD3" id="E01DEAMD3" size="5" maxlength="4" value="<%= cdNew.getE01DEAMD3().trim()%>"  readonly>
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="absbottom" border="0" > 
            </td>
            <td nowrap width="25%"> 
              <div align="right">Tasa Inter&eacute;s/Spread :</div>
            </td>
            <td nowrap width="27%"> 
              <input type="text" name="E01DEARTE" id="txtright" size="10" maxlength="9" value="<%= cdNew.getE01DEARTE().trim()%>" >
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="25%" > 
              <div align="right">Tipo de Tasa Flotante :</div>
            </td>
            <td nowrap width="23%" > 
              <input type="text" name="E01DEAFTB" size="3" maxlength="2" value="<%= cdNew.getE01DEAFTB().trim()%>">
              <a href="javascript:GetFloating('E01DEAFTB')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absmiddle" border="0" ></a> 
              <select name="E01DEAFTY">
                <option value=" " <% if (!(cdNew.getE01DEAFTY().equals("FP") ||cdNew.getE01DEAFTY().equals("FS"))) out.print("selected"); %>></option>
                <option value="FP" <% if (cdNew.getE01DEAFTY().equals("FP")) out.print("selected"); %>>FP</option>
                <option value="FS" <% if (cdNew.getE01DEAFTY().equals("FS")) out.print("selected"); %>>FS</option>
              </select>
            </td>
            <td nowrap width="25%" > 
              <div align="right">Tasa Variable :</div>
            </td>
            <td nowrap width="27%" > 
              <input type="text" name="E01FLTRTE" id="txtright" size="9" maxlength="9" value="<%= cdNew.getE01FLTRTE().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="25%" > 
              <div align="right">Ciclo/Fecha Revis.Tasa :</div>
            </td>
            <td nowrap width="23%" > 
              <input type="text" name="E01DEARRP" size="3" maxlength="3" value="<%= cdNew.getE01DEARRP().trim()%>" onblur="rightAlignCharNumber()">
              /
              <input type="text" name="E01DEARD1" size="3" maxlength="2" value="<%= cdNew.getE01DEARD1().trim()%>" onKeypress="enterInteger()">
              <input type="text" name="E01DEARD2" size="3" maxlength="2" value="<%= cdNew.getE01DEARD2().trim()%>" onKeypress="enterInteger()">
              <input type="text" name="E01DEARD3" size="5" maxlength="4" value="<%= cdNew.getE01DEARD3().trim()%>" onKeypress="enterInteger()"> 
            </td>
            <td nowrap width="25%" > 
              <div align="right">N&uacute;mero Referencia :</div>
            </td>
            <td nowrap width="27%" > 
              <input type="text" name="E01DEAREF" size="13" maxlength="12" value="<%= cdNew.getE01DEAREF().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="25%" > 
              <div align="right">Monto Original :</div>
            </td>
            <td nowrap width="23%" > 
                <eibsinput:text name="cdNew" property="E01DEAOAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>"  required="true"/>
            </td>
            <td nowrap width="25%" > 
              <div align="right">Tasa de Cambio :</div>
            </td>
            <td nowrap width="27%" > 
              <input type="text" name="E01DEAEXR" id="txtright" size="11" maxlength="11" value="<%= cdNew.getE01DEAEXR().trim()%>" onKeypress="enterDecimal()">
            </td>
          </tr>
          <tr id="trclear">
            <td nowrap width="25%" >
              <div align="right">Moneda :</div>
            </td>
            <td nowrap width="23%" >
              <input type="text" name="E01DEACCY2" size="4" maxlength="3" value="<%= cdNew.getE01DEACCY().trim()%>" readonly>
            </td>
            <td nowrap width="25%" >
              <div align="right">Centro de Costo : </div>
            </td>
            <td nowrap width="27%" >
              <input type="text" name="E01DEACCN" size="9" maxlength="8" value="<%= cdNew.getE01DEACCN().trim()%>">
              <a href="javascript:GetCostCenter('E01DEACCN','01')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Centros de Costo" align="absmiddle" border="0" ></a> 
            </td>
          </tr>

          <tr id="trdark"> 
            <td width="25%" > 
              <div align="right">Direcciones de Correo :</div>
            </td>
            <td width="25%" > 
              <input type="text" name="E01DEAMLA" size="3" maxlength="2" value="<%= cdNew.getE01DEAMLA().trim()%>">
              <a href="javascript:GetMailing('E01DEAMLA',document.forms[0].E01DEACUN.value)"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Direcciones de Correo del Cliente" align="absmiddle" border="0"></a> 
            </td>
            <td width="25%" > 
              <div align="right">&#191;Bloqueo Correspondencia&#63; :</div>
            </td>
    		<td>
       			<input type="radio" name="E01DEASUT" value="Y" onClick="document.forms[0].E01DEASUT.value='Y'"
	  			<%if(cdNew.getE01DEASUT().equals("Y")) out.print("checked");%>>
          			S&iacute; 
       			<input type="radio"  name="E01DEASUT" value="N" onClick="document.forms[0].E01DEASUT.value='N'"
	  			<%if(cdNew.getE01DEASUT().equals("N")) out.print("checked");%>>
           			No 
			</td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width="25%" > 
              <div align="right">Tabla de Tasa :</div>
            </td>
            <td nowrap width="23%" >
              <input type="text" name="E01DEARTB" size="3" maxlength="2" value="<%= cdNew.getE01DEARTB().trim()%>">
              <a href="javascript:GetRateTable('E01DEARTB')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="help" align="absmiddle" border="0" ></a> 
            </td>
            <td width="25%" > 
              <div align="right">Retenci&oacute;n/Impuesto :</div>
            </td>
            <td width="25%" > 
              <input type="text" name="E01DEAWHF" size="2" maxlength="1" value="<%= cdNew.getE01DEAWHF().trim()%>">
              <a href="javascript:GetCode('E01DEAWHF','STATIC_cd_taxes.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a> 
            </td>
          </tr>
          <tr id="trdark">            
            <td nowrap width="25%" >
              <div align="right">Documento en Custodia :</div>  
            </td>
            <td nowrap width="23%">
             <SELECT name="E01DEAECU" onchange="showMandato()">
                <OPTION value="N" <% if (!(cdNew.getE01DEAECU().equals("1") || cdNew.getE01DEAECU().equals("2"))) {out.print("selected"); %><%}%>>No Custodia</OPTION>
                <OPTION value="1" <% if (cdNew.getE01DEAECU().equals("1")) {out.print("selected"); %><%}%>> Electronica</OPTION>
                <OPTION value="2" <% if (cdNew.getE01DEAECU().equals("2")) {out.print("selected"); %><%}%>>Custodia Fisica</OPTION>
              </SELECT>
            </td>
            <td nowrap width="25%" > 
              <div align="right">Banco/Sucursal :</div>
            </td>
            <td nowrap width="23%" >
            <% if (!protect) {%>
              <input type="text" name="E01DEABNK" size="2" maxlength="2" value="<%= cdNew.getE01DEABNK().trim()%>" >
              <input type="text" name="E01DEABRN" size="5" maxlength="4" value="<%= cdNew.getE01DEABRN().trim()%>">
              <a href="javascript:GetBranch('E01DEABRN','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"  ></a>
            <% } else { %>
              <input type="text" name="E01DEABNK" size="2" maxlength="2" value="<%= cdNew.getE01DEABNK().trim()%>" readonly>
              <input type="text" name="E01DEABRN" size="5" maxlength="4" value="<%= cdNew.getE01DEABRN().trim()%>" readonly>
            <% } %>
            </td>      
          </tr>
	    <tr id="trclear">            
        	<td nowrap width="25%" >
       			<div align="right">Documento Con Mandato :</div>
       		</td> 
    		<TD>
       			<input type="radio" name="E01DEA2TC" value="Y" onClick="document.forms[0].E01DEA2TC.value='Y'"
	  			<%if(cdNew.getE01DEA2TC().equals("Y")) out.print("checked");%>>
          			S&iacute; 
       			<input type="radio" name="E01DEA2TC" value="N" onClick="document.forms[0].E01DEA2TC.value='N'"
	  			<%if(cdNew.getE01DEA2TC().equals("N")) out.print("checked");%>>
           			No 
			</TD>
        	<td nowrap width="25%" >
       			<div align="right">Cuenta Mandato :</div>
       		</td> 
    		<td>
              <input type="text" name="E01DEAREX" size="13" maxlength="12" value="<%= cdNew.getE01DEAREX().trim()%>" onKeypress="enterInteger()">
			</td>
   		</tr>
	    <tr id="trdark">            
        	<td nowrap width="25%" >
       			<div align="right">Documento Impreso :</div>
       		</td> 
    		<td>
       			<input type="radio" disabled name="E01DEAF01" value="Y" onClick="document.forms[0].E01DEAF01.value='Y'"
	  			<%if(cdNew.getE01DEAF01().equals("Y")) out.print("checked");%>>
          			S&iacute; 
       			<input type="radio"  name="E01DEAF01" value="N" onClick="document.forms[0].E01DEAF01.value='N'"
	  			<%if(cdNew.getE01DEAF01().equals("N")) out.print("checked");%>>
           			No 
			</td>
            <td width="25%" > 
              <div align="right">Clase de Certificado :</div>
            </td>
            <td width="25%" > 
              <input type="text" name="E01DEACLF" size="2" maxlength="1" value="<%= cdNew.getE01DEACLF().trim()%>">
              <a href="javascript:GetCode('E01DEACLF','STATIC_cd_class.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"  ></a> 
            </td>
   		</tr>
          
          
        </table>
      </td>
    </tr>
  </table>

<% if ((cdNew.getE01FLGFRA().trim().equals("Y"))) {%> 
  <h4>Depositos Desmaterializados</h4> 
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">

	    <%if( currUser.getE01INT().equals("18")){%> 	
		    <tr id="trdark">            
	        	<td nowrap width="40%" >
    	   			<div align="right">Monto pago Final :</div>
    	   		</td> 
 		   		<td nowrap width="20%" >
       	         <eibsinput:text name="cdNew" property="E01PAYOFF" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />
 				</td>
            <td nowrap width="20%" align="left"> 
 					<input id="EIBSBTN" type=button name="calcular" value="Calcular" onClick="CheckSubmit('C')">				
            </td> 	
            <td nowrap >&nbsp; </td>            			
  	 		</tr>
        <% } else { %>
		    <tr id="trdark">            
   		     	<td nowrap width="25%" >
       				<div align="right">Numero Documentos :</div>
       			</td> 
    			<td nowrap width="25%" >
       	         <eibsinput:text name="cdNew" property="E01DEANFR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" maxlength="3" size="4"/>
				</td>
	      	     <td nowrap width="25%" > 
        	      <div align="right">En Monto de :</div>
	            </td>      
	            <td nowrap width="25%" >
    	            <eibsinput:text name="cdNew" property="E01DEAAFR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />
     	       </td>      
	   		</tr>
		    <tr id="trclear">            
	        	<td nowrap width="25%" >
    	   			<div align="right">Monto pago Final :</div>
    	   		</td> 
 		   		<td nowrap width="25%" >
       	         <eibsinput:text name="cdNew" property="E01PAYOFF" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />
 				</td>
	            <td nowrap width="25%" > 
	            </td>      
	            <td nowrap width="25%" >
  	          </td>      
  	 		</tr>
		<% } %> 
       </table>
      </td>
    </tr>
  </table>
<% } %> 

<h4>Origen de Fondos</h4>
  
  <TABLE id="mainTable" class="tableinfo">
  <TR>
   <TD>
	<table id="headTable" >
    <tr id="trdark"> 
      <td nowrap align="center" >Concepto</td>
      <td nowrap align="center" >Banco </td>
      <td nowrap align="center" >Sucursal</td>
      <td nowrap align="center" >Moneda</td>
      <td nowrap align="center" >Referencia</td>
      <td nowrap align="center" >Monto</td>
    </tr>
    </table> 
      
    <div id="dataDiv" style="height:60; overflow-y :scroll; z-index:0" >
     <table id="dataTable" >
          <%
  				   int amount = 9;
 				   String name;
  					for ( int i=1; i<=amount; i++ ) {
   					  name = i + "";
   			%> 
          <tr id="trclear"> 
            <td nowrap > 
              <div align="center" nowrap> 
                <input type="text" name="E01OFFOP<%= name %>" id="E01OFFOP<%= name %>" value="<%= cdNew.getField("E01OFFOP"+name).getString().trim()%>" size="3" maxlength="3">
                <input type="hidden" name="E01OFFGL<%= name %>" value="<%= cdNew.getField("E01OFFGL"+name).getString().trim()%>">
                <input type="text" name="E01OFFCO<%= name %>" size="35" maxlength="35" readonly value="<%= cdNew.getField("E01OFFCO"+name).getString().trim()%>" 
                  oncontextmenu="showPopUp(conceptHelp,this.name,document.forms[0].E01DEABNK.value,'','E01OFFGL<%= name %>','E01OFFOP<%= name %>',document.forms[0].E01DEAACD.value); return false;">
              </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E01OFFBK<%= name %>" size="2" maxlength="2" value="<%= cdNew.getField("E01OFFBK"+name).getString().trim()%>">
              </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E01OFFBR<%= name %>" size="4" maxlength="4" value="<%= cdNew.getField("E01OFFBR"+name).getString().trim()%>"
                oncontextmenu="showPopUp(branchHelp,this.name,document.forms[0].E01DEABNK.value,'','','',''); return false;">
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFCY<%= name %>" size="3" maxlength="3" value="<%= cdNew.getField("E01OFFCY"+name).getString().trim()%>"
                oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E01DEABNK.value,'','','',''); return false;">
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFAC<%= name %>" id="E01OFFAC<%= name %>" size="12" maxlength="12"  value="<%= cdNew.getField("E01OFFAC"+name).getString().trim()%>"
                oncontextmenu="showPopUp(accountCustomerHelp,this.name,document.forms[0].E01DEABNK.value,'',document.forms[0].E01DEACUN.value,'','RT'); return false;">
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01OFFAM<%= name %>" size="23" maxlength="15"  value="<%= cdNew.getField("E01OFFAM"+name).getString().trim()%>" onKeypress="enterDecimal()">
              </div>
            </td>
          </tr>
          <%
    		}
    		%> 
    	  </table>
        </div>
        
     <table id="footTable" > 
          <tr id="trdark"> 
            <td nowrap  align="right"><b>Equivalente Moneda del Certificado :</b> 
            </td>
            <td nowrap align="center"><b><i><strong> 
                <input type="text" name="E01OFFEQV" size="23" maxlength="17" readonly value="<%= cdNew.getE01OFFEQV().trim()%>">
                </strong></i></b>
            </td>
          </tr>
        </table>
  
</TD>  
</TR>	
</TABLE>  

  
 <SCRIPT language="javascript">
    function tableresize() {
     adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  tableresize();
  window.onresize=tableresize;
  showMandato();
  </SCRIPT>
  <% if(error.getERWRNG().equals("Y")){%>
   <h4 style="text-align:center"><input type="checkbox" name="H01FLGWK2" value="A" <% if(cdNew.getH01FLGWK2().equals("A")){ out.print("checked");} %>>
       Enviar Excepci�n</h4>
  <% } %>         
  <p align="center"> 
    <input id="EIBSBTN" type=button name="Submit"  onClick="CheckSubmit('F')" value="Enviar">
  </p>

  
  </form>
</body>
</html>
