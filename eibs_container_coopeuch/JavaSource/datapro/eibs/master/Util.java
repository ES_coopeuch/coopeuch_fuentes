// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 12-02-2015 17:09:41
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Util.java

package datapro.eibs.master;

import com.adobe.fdf.FDFDoc;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.sockets.DecimalField;
import datapro.eibs.sockets.MessageField;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

// Referenced classes of package datapro.eibs.master:
//            JSEIBSMSGProp

public class Util
{

    public Util()
    {
    }

    public static String addDot(String number)
    {
        String result = "";
        if(number.length() == 1)
            number = (new StringBuilder(String.valueOf(number))).append("00").toString();
        result = (new StringBuilder(String.valueOf(number.substring(0, number.length() - 2)))).append(".").append(number.substring(number.length() - 2)).toString();
        return result;
    }

    public static String addLeftChar(char ch, int len, String str)
    {
        for(int i = str.length(); i < len; i++)
            str = (new StringBuilder(String.valueOf(ch))).append(str).toString();

        return str;
    }

    public static String fcolorCCY(String num)
    {
        num = formatCCY(num);
        if(num.startsWith("-"))
            num = (new StringBuilder("<FONT COLOR=RED>")).append(num).append("</FONT>").toString();
        return num;
    }

    public static String formatAdd(String s1, String s2, String s3)
    {
        String r = "";
        if(!s1.trim().equals(""))
            r = (new StringBuilder(String.valueOf(r))).append(s1.trim()).append("<br>").toString();
        if(!s2.trim().equals(""))
            r = (new StringBuilder(String.valueOf(r))).append(s2.trim()).append("<br>").toString();
        if(!s3.trim().equals(""))
            r = (new StringBuilder(String.valueOf(r))).append(s3.trim()).append("<br>").toString();
        if(r.trim().equals(""))
            r = "&nbsp;";
        return r;
    }

    public static String formatBlank(String s)
    {
        String r = null;
        if(s.trim().equals("&nbsp;"))
            r = "";
        else
            r = s.trim();
        return r;
    }

    public static String formatCCY(double value)
    {
        return DecimalField.formatCCY(value);
    }

    public static String formatCCY(String value)
    {
        double numDouble = 0.0D;
        try
        {
            numDouble = Double.parseDouble(value);
        }
        catch(NumberFormatException e1)
        {
            try
            {
                numDouble = DecimalField.parseDouble(value);
            }
            catch(NumberFormatException e2)
            {
                return value;
            }
        }
        return DecimalField.formatCCY(numDouble);
    }

    public static String formatCCY(double value, String CCYS)
    {
        return (new StringBuilder(String.valueOf(CCYS))).append(formatCCY(value)).toString();
    }

    public static String formatCCY(String value, String CCYS)
    {
        return (new StringBuilder(String.valueOf(CCYS))).append(formatCCY(value)).toString();
    }

    public static double parseCCYtoDouble(String value)
    {
        double numDouble = 0.0D;
        char spt = DecimalField.getDecimalSeparator();
        value = takeCharacter(value, DecimalField.getGroupingSeparator());
        if(spt != '.')
            value = value.replace(spt, '.');
        numDouble = Double.parseDouble(value);
        return numDouble;
    }

    public static String takeCharacter(String s, char ch)
    {
        String result = null;
        int posi = s.indexOf(ch);
        if(posi > -1)
            while(posi > -1) 
            {
                result = s.substring(0, posi);
                result = (new StringBuilder(String.valueOf(result))).append(s.substring(posi + 1)).toString();
                posi = result.indexOf(ch);
                s = result;
            }
        else
            result = s;
        return result;
    }

    public static String formatCell(String s)
    {
        String r = null;
        String rs = null;
        String ls = null;
        int pos = 0;
        if(s.trim().equals(""))
        {
            r = "&nbsp;";
        } else
        {
            for(s = s.trim(); s.indexOf("'") != -1; s = (new StringBuilder(String.valueOf(ls))).append("&#39;").append(rs).toString())
            {
                pos = s.indexOf("'");
                ls = s.substring(0, pos);
                rs = s.substring(pos + 1, s.length());
            }

            for(; s.indexOf("\"") != -1; s = (new StringBuilder(String.valueOf(ls))).append("&#34;").append(rs).toString())
            {
                pos = s.indexOf("\"");
                ls = s.substring(0, pos);
                rs = s.substring(pos + 1, s.length());
            }

            r = s;
        }
        return r;
    }

    public static String formatDate(String d1, String d2, String d3)
    {
        if(d1.length() == 0)
            d1 = "0";
        if(d2.length() == 0)
            d2 = "0";
        if(d3.length() == 0)
            d3 = "0";
        d1 = d1.length() != 1 ? d1 : (new StringBuilder("0")).append(d1).toString();
        d2 = d2.length() != 1 ? d2 : (new StringBuilder("0")).append(d2).toString();
        d3 = d3.length() != 1 ? d3 : (new StringBuilder("0")).append(d3).toString();
        if(d1.equals("00") && d2.equals("00") && d3.equals("00"))
            return "&nbsp;";
        else
            return (new StringBuilder(String.valueOf(d1))).append("/").append(d2).append("/").append(d3).toString();
    }

    public static String formatID(String c, String l)
    {
        String fc = "";
        for(int i = 0; i < 9 - c.length(); i++)
            fc = (new StringBuilder(String.valueOf(fc))).append("0").toString();

        String fl = "";
        for(int i = 0; i < 4 - l.length(); i++)
            fl = (new StringBuilder(String.valueOf(fl))).append("0").toString();

        return (new StringBuilder(String.valueOf(fc))).append(c).append(fl).append(l).toString();
    }

    public static String formatTime(String t)
    {
        int len = t.length();
        String time = len >= 6 ? t : addLeftChar('0', 6, t);
        String hh = addLeftChar('0', 2, time.substring(0, 2));
        String mm = addLeftChar('0', 2, time.substring(2, 4));
        String ss = addLeftChar('0', 2, time.substring(4, time.length()));
        return (new StringBuilder(String.valueOf(hh))).append(":").append(mm).append(":").append(ss).toString();
    }

    public static String formatYear(int y)
    {
        return formatYear(Integer.toString(y));
    }

    public static String formatYear(String y)
    {
        if(y.length() < 4)
            try
            {
                int aux = Integer.parseInt(y);
                if(aux == 0)
                    y = "2000";
                else
                if(aux > 52)
                    y = (new StringBuilder("19")).append(y).toString();
                else
                    y = (new StringBuilder("20")).append(justifyRight(y, 2)).toString();
            }
            catch(Exception e)
            {
                y = "";
            }
        return y;
    }

    public static String justifyLeft(String c, int l)
    {
        String fc = "";
        for(int i = 0; i < l - c.length(); i++)
            fc = (new StringBuilder(String.valueOf(fc))).append("0").toString();

        return (new StringBuilder(String.valueOf(c))).append(fc).toString();
    }

    public static String justifyRight(String c, int l)
    {
        String fc = "";
        for(int i = 0; i < l - c.length(); i++)
            fc = (new StringBuilder(String.valueOf(fc))).append("0").toString();

        return (new StringBuilder(String.valueOf(fc))).append(c).toString();
    }

    public static String leftValue(String s)
    {
        int pos = 0;
        for(pos = 0; pos < s.length(); pos++)
            if(s.charAt(pos) == '_')
                break;

        return s.substring(0, pos);
    }

    public static String rightValue(String s)
    {
        int pos = 0;
        for(pos = 0; pos < s.length(); pos++)
            if(s.charAt(pos) == '_')
                break;

        if(pos == s.length())
            return "0";
        else
            return s.substring(pos + 1);
    }

    public static String[] splitField(String field, int row, int col)
    {
        String result[] = new String[row];
        for(int i = 0; i < row; i++)
            result[i] = "";

        if(field.length() > 0)
            if(field.length() <= col)
            {
                result[0] = field;
            } else
            {
                int enter = field.length() / col;
                double rest = field.length() % col;
                if(rest > 0.0D)
                    enter++;
                for(int i = 0; i < enter; i++)
                    if(i == enter - 1)
                        result[i] = field.substring(i * col, field.length());
                    else
                        result[i] = field.substring(i * col, i * col + col);

            }
        return result;
    }

    public static String takeComa(String number)
    {
        String result = "";
        int posi = number.indexOf(",");
        if(posi > -1)
            while(posi > -1) 
            {
                result = number.substring(0, posi);
                result = (new StringBuilder(String.valueOf(result))).append(number.substring(posi + 1)).toString();
                posi = result.indexOf(",");
                number = result;
            }
        else
            result = number;
        return result;
    }

    public static String takeDot(String number)
    {
        String result = "";
        int posi = number.indexOf(".");
        if(posi > -1)
        {
            result = number.substring(0, posi);
            result = (new StringBuilder(String.valueOf(result))).append(number.substring(posi + 1)).toString();
        } else
        {
            result = (new StringBuilder(String.valueOf(number))).append("00").toString();
        }
        return result;
    }

    public static String addRightChar(char ch, int len, String str)
    {
        for(int i = str.length(); i < len; i++)
            str = (new StringBuilder(String.valueOf(str))).append(ch).toString();

        return str;
    }

    public static String concat(String s[], String ch)
    {
        String r = "";
        int count = 0;
        do
            try
            {
                if(!r.equals("") && !s[count].trim().equals(""))
                    r = (new StringBuilder(String.valueOf(r))).append(ch).append(s[count].trim()).toString();
                else
                    r = (new StringBuilder(String.valueOf(r))).append(s[count].trim()).toString();
                count++;
            }
            catch(Exception exception)
            {
                return r;
            }
        while(true);
    }

    public static String concatBR(String s[])
    {
        String r = "";
        int count = 0;
        do
            try
            {
                if(!r.equals("") && !s[count].trim().equals(""))
                    r = (new StringBuilder(String.valueOf(r))).append("<BR>").append(s[count].trim()).toString();
                else
                    r = (new StringBuilder(String.valueOf(r))).append(s[count].trim()).toString();
                count++;
            }
            catch(Exception exception)
            {
                return r;
            }
        while(true);
    }

    public static String fcolorDate(String d1, String d2, String d3, String mature)
    {
        d1 = d1.length() != 1 ? d1 : (new StringBuilder("0")).append(d1).toString();
        d2 = d2.length() != 1 ? d2 : (new StringBuilder("0")).append(d2).toString();
        d3 = d3.length() != 1 ? d3 : (new StringBuilder("0")).append(d3).toString();
        String Month = "";
        String Day = "";
        String Year = "";
        String Dat = "";
        String fcolor = mature;
        String date = "";
        String fdate = "";
        if(d1.equals("00") && d2.equals("00") && d3.equals("00"))
            return "&nbsp;";
        date = (new StringBuilder(String.valueOf(d1))).append("/").append(d2).append("/").append(d3).toString();
        if(mature == "R")
            fdate = (new StringBuilder("<FONT COLOR=RED>")).append(date).append("</FONT>").toString();
        else
        if(mature == "O")
            fdate = (new StringBuilder("<FONT COLOR=ORANGE>")).append(date).append("</FONT>").toString();
        else
            fdate = (new StringBuilder(String.valueOf(d1))).append("/").append(d2).append("/").append(d3).toString();
        return fdate;
    }

    public static String fcolorDateFull(String fmt, String lg, String d1, String d2, String d3, String mature)
    {
        d1 = d1.length() != 1 ? d1 : (new StringBuilder("0")).append(d1).toString();
        d2 = d2.length() != 1 ? d2 : (new StringBuilder("0")).append(d2).toString();
        d3 = d3.length() != 1 ? d3 : (new StringBuilder("0")).append(d3).toString();
        String fcolor = mature;
        String date = "";
        String fdate = "";
        String Month = "";
        String Day = "";
        String Year = "";
        String Dat = "";
        if(d1.equals("00") && d2.equals("00") && d3.equals("00"))
            return "&nbsp;";
        if(fmt.equals("MDY"))
        {
            Month = d1;
            Day = d2;
            Year = d3;
        } else
        if(fmt.equals("DMY"))
        {
            Month = d2;
            Day = d1;
            Year = d3;
        } else
        if(fmt.equals("YMD"))
        {
            Month = d2;
            Day = d3;
            Year = d1;
        } else
        if(fmt.equals("YDM"))
        {
            Month = d3;
            Day = d2;
            Year = d1;
        }
        JSEIBSMSGProp.setPropertyFileLang(lg);
        switch(Integer.parseInt(Month))
        {
        case 1: // '\001'
            Month = JSEIBSMSGProp.getMSG0005();
            break;

        case 2: // '\002'
            Month = JSEIBSMSGProp.getMSG0006();
            break;

        case 3: // '\003'
            Month = JSEIBSMSGProp.getMSG0007();
            break;

        case 4: // '\004'
            Month = JSEIBSMSGProp.getMSG0008();
            break;

        case 5: // '\005'
            Month = JSEIBSMSGProp.getMSG0009();
            break;

        case 6: // '\006'
            Month = JSEIBSMSGProp.getMSG0010();
            break;

        case 7: // '\007'
            Month = JSEIBSMSGProp.getMSG0011();
            break;

        case 8: // '\b'
            Month = JSEIBSMSGProp.getMSG0012();
            break;

        case 9: // '\t'
            Month = JSEIBSMSGProp.getMSG0013();
            break;

        case 10: // '\n'
            Month = JSEIBSMSGProp.getMSG0014();
            break;

        case 11: // '\013'
            Month = JSEIBSMSGProp.getMSG0015();
            break;

        case 12: // '\f'
            Month = JSEIBSMSGProp.getMSG0016();
            break;
        }
        if(fmt.equals("MDY"))
            Dat = (new StringBuilder(String.valueOf(Month))).append("-").append(Day).append("-").append(Year).toString();
        else
        if(fmt.equals("DMY"))
            Dat = (new StringBuilder(String.valueOf(Day))).append("-").append(Month).append("-").append(Year).toString();
        else
        if(fmt.equals("YMD"))
            Dat = (new StringBuilder(String.valueOf(Year))).append("-").append(Month).append("-").append(Day).toString();
        else
        if(fmt.equals("YDM"))
            Dat = (new StringBuilder(String.valueOf(Year))).append("-").append(Day).append("-").append(Month).toString();
        if(mature == "R")
            fdate = (new StringBuilder("<FONT COLOR=RED>")).append(Dat).append("</FONT>").toString();
        else
        if(mature == "O")
            fdate = (new StringBuilder("<FONT COLOR=ORANGE>")).append(Dat).append("</FONT>").toString();
        else
            fdate = Dat;
        return fdate;
    }

    public static String formatDate(String d)
    {
        d = d.length() != 5 ? d : (new StringBuilder("0")).append(d).toString();
        if(d.length() == 6)
        {
            String d1 = d.substring(0, 2);
            String d2 = d.substring(2, 4);
            String d3 = d.substring(4, 6);
            return (new StringBuilder(String.valueOf(d1))).append("/").append(d2).append("/").append(d3).toString();
        }
        if(d.length() == 8)
        {
            String d1 = d.substring(0, 2);
            String d2 = d.substring(2, 4);
            String d3 = d.substring(4, 8);
            return (new StringBuilder(String.valueOf(d1))).append("/").append(d2).append("/").append(d3).toString();
        } else
        {
            return "";
        }
    }

    public static String formatDateFull(String fmt, String lg, String d1, String d2, String d3)
    {
        String Month = "";
        String Day = "";
        String Year = "";
        String Dat = "";
        if(d1.length() == 0)
            d1 = "0";
        if(d2.length() == 0)
            d2 = "0";
        if(d3.length() == 0)
            d3 = "0";
        d1 = d1.length() != 1 ? d1 : (new StringBuilder("0")).append(d1).toString();
        d2 = d2.length() != 1 ? d2 : (new StringBuilder("0")).append(d2).toString();
        d3 = d3.length() != 1 ? d3 : (new StringBuilder("0")).append(d3).toString();
        if(d1.equals("00") && d2.equals("00") && d3.equals("00"))
            return "&nbsp;";
        if(fmt.equals("MDY"))
        {
            Month = d1;
            Day = d2;
            Year = d3;
        } else
        if(fmt.equals("DMY"))
        {
            Month = d2;
            Day = d1;
            Year = d3;
        } else
        if(fmt.equals("YMD"))
        {
            Month = d2;
            Day = d3;
            Year = d1;
        } else
        if(fmt.equals("YDM"))
        {
            Month = d3;
            Day = d2;
            Year = d1;
        }
        JSEIBSMSGProp.setPropertyFileLang(lg);
        switch(Integer.parseInt(Month))
        {
        case 1: // '\001'
            Month = JSEIBSMSGProp.getMSG0005();
            break;

        case 2: // '\002'
            Month = JSEIBSMSGProp.getMSG0006();
            break;

        case 3: // '\003'
            Month = JSEIBSMSGProp.getMSG0007();
            break;

        case 4: // '\004'
            Month = JSEIBSMSGProp.getMSG0008();
            break;

        case 5: // '\005'
            Month = JSEIBSMSGProp.getMSG0009();
            break;

        case 6: // '\006'
            Month = JSEIBSMSGProp.getMSG0010();
            break;

        case 7: // '\007'
            Month = JSEIBSMSGProp.getMSG0011();
            break;

        case 8: // '\b'
            Month = JSEIBSMSGProp.getMSG0012();
            break;

        case 9: // '\t'
            Month = JSEIBSMSGProp.getMSG0013();
            break;

        case 10: // '\n'
            Month = JSEIBSMSGProp.getMSG0014();
            break;

        case 11: // '\013'
            Month = JSEIBSMSGProp.getMSG0015();
            break;

        case 12: // '\f'
            Month = JSEIBSMSGProp.getMSG0016();
            break;
        }
        if(fmt.equals("MDY"))
            Dat = (new StringBuilder(String.valueOf(Month))).append("-").append(Day).append("-").append(Year).toString();
        else
        if(fmt.equals("DMY"))
            Dat = (new StringBuilder(String.valueOf(Day))).append("-").append(Month).append("-").append(Year).toString();
        else
        if(fmt.equals("YMD"))
            Dat = (new StringBuilder(String.valueOf(Year))).append("-").append(Month).append("-").append(Day).toString();
        else
        if(fmt.equals("YDM"))
            Dat = (new StringBuilder(String.valueOf(Year))).append("-").append(Day).append("-").append(Month).toString();
        return Dat;
    }

    public static String formatCustomDate(String fmt_eibs, String mask, String lang, String d1, String d2, String d3)
    {
        String Month = "";
        String Day = "";
        String Year = "";
        String Dat = "";
        DateFormat myDateFormat = new SimpleDateFormat(mask, new Locale(lang, "", ""));
        if(d1.length() == 0)
            d1 = "0";
        if(d2.length() == 0)
            d2 = "0";
        if(d3.length() == 0)
            d3 = "0";
        d1 = d1.length() != 1 ? d1 : (new StringBuilder("0")).append(d1).toString();
        d2 = d2.length() != 1 ? d2 : (new StringBuilder("0")).append(d2).toString();
        d3 = d3.length() != 1 ? d3 : (new StringBuilder("0")).append(d3).toString();
        if(d1.equals("00") && d2.equals("00") && d3.equals("00"))
            return "&nbsp;";
        if(fmt_eibs.equals("MDY"))
        {
            Month = d1;
            Day = d2;
            Year = d3;
        } else
        if(fmt_eibs.equals("DMY"))
        {
            Month = d2;
            Day = d1;
            Year = d3;
        } else
        if(fmt_eibs.equals("YMD"))
        {
            Month = d2;
            Day = d3;
            Year = d1;
        } else
        if(fmt_eibs.equals("YDM"))
        {
            Month = d3;
            Day = d2;
            Year = d1;
        }
        Calendar myCalendar = Calendar.getInstance();
        myCalendar.set(Integer.parseInt(formatYear(Year)), Integer.parseInt(Month) - 1, Integer.parseInt(Day));
        Date myDate = myCalendar.getTime();
        String myReturn = myDateFormat.format(myDate);
        return myReturn;
    }

    public static String trim(String s)
    {
        return s.trim();
    }

    public static boolean existFile(String filename)
    {
        boolean exists = false;
        try
        {
            File f = new File(filename);
            exists = f.exists();
        }
        catch(Exception e)
        {
            exists = false;
        }
        return exists;
    }

    public static String unformatHTML(String s)
    {
        String rs = null;
        String ls = null;
        int pos = 0;
        s = s.trim();
        if(!s.equals(""))
        {
            for(; s.indexOf("&#39;") != -1; s = (new StringBuilder(String.valueOf(ls))).append("'").append(rs).toString())
            {
                pos = s.indexOf("&#39;");
                ls = s.substring(0, pos);
                rs = s.substring(pos + 5, s.length());
            }

            for(; s.indexOf("&#34;") != -1; s = (new StringBuilder(String.valueOf(ls))).append("\"").append(rs).toString())
            {
                pos = s.indexOf("&#34;");
                ls = s.substring(0, pos);
                rs = s.substring(pos + 5, s.length());
            }

        }
        return s;
    }

    public static String replace(String Text, String Old, String New)
    {
        if(Old.length() == 0)
            return Text;
        StringBuffer buf = new StringBuffer();
        int i = 0;
        int j;
        for(j = 0; (i = Text.indexOf(Old, j)) > -1; j = i + Old.length())
            buf.append((new StringBuilder(String.valueOf(Text.substring(j, i)))).append(New).toString());

        if(j < Text.length())
            buf.append(Text.substring(j));
        return buf.toString();
    }

    public static HashMap getPropertiesKeysValues(String file)
        throws MissingResourceException
    {
        HashMap keys_values = new HashMap();
        try
        {
            PropertyResourceBundle props = (PropertyResourceBundle)PropertyResourceBundle.getBundle(file);
            String strKey;
            for(Enumeration enumer = props.getKeys(); enumer.hasMoreElements(); keys_values.put(strKey, props.getString(strKey)))
                strKey = (String)enumer.nextElement();

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return keys_values;
    }

    public static JBObjList getYearList(int begin, int ending)
    {
        JBObjList result = new JBObjList();
        for(; begin <= ending; begin++)
            result.addRow(String.valueOf(begin));

        return result;
    }

    public static String formatDateTime(String f, String t)
    {
        String d = " ";
        t = t.length() != 11 ? t : (new StringBuilder("0")).append(t).toString();
        if(t.length() == 12)
        {
            String d1 = t.substring(0, 2);
            String d2 = t.substring(2, 4);
            String d3 = t.substring(4, 6);
            String t1 = t.substring(6, 8);
            String t2 = t.substring(8, 10);
            String t3 = t.substring(10, 12);
            if(f.equals("MDY"))
                d = (new StringBuilder(String.valueOf(d1))).append("/").append(d2).append("/").append(d3).append(" ").append(t1).append(":").append(t2).append(":").append(t3).toString();
            else
            if(f.equals("DMY"))
                d = (new StringBuilder(String.valueOf(d2))).append("/").append(d1).append("/").append(d3).append(" ").append(t1).append(":").append(t2).append(":").append(t3).toString();
            else
            if(f.equals("YMD"))
                d = (new StringBuilder(String.valueOf(d3))).append("/").append(d1).append("/").append(d2).append(" ").append(t1).append(":").append(t2).append(":").append(t3).toString();
            else
            if(f.equals("YDM"))
                d = (new StringBuilder(String.valueOf(d3))).append("/").append(d2).append("/").append(d1).append(" ").append(t1).append(":").append(t2).append(":").append(t3).toString();
        }
        return d;
    }

    public static String formatJavascript(String s)
    {
        s = s.trim();
        s = replace(s, "'", "\\'");
        s = replace(s, "\"", "'+dq+'");
        s = replace(s, "&#39;", "\\'");
        return s;
    }

    public static String formatJavascript(MessageField field)
    {
        return formatJavascript(field.toString()).trim();
    }

    public static String cleanInvalidXmlCharacters(String inputString)
    {
        if(inputString == null)
            return null;
        StringBuffer sbOutput = new StringBuffer();
        for(int i = 0; i < inputString.length(); i++)
        {
            char nx = inputString.charAt(i);
            if(nx > '+' && nx < ':' || nx > '?' && nx < '[' || nx > '`' && nx < '{' || nx == ' ' || nx == '_')
                sbOutput.append(nx);
        }

        return sbOutput.toString();
    }

    public static float parseFloat(String number)
    {
        try
        {
            return Float.parseFloat(number);
        }
        catch(Exception e)
        {
            return Float.parseFloat("0");
        }
    }

    public static String escapeJson(String string)
    {
        String newString = string;
        for(int i = 0; i < JSON_TO_TAKE.length; i++)
        {
            for(char mChar = JSON_TO_TAKE[i]; newString.indexOf(mChar) > -1; newString = takeCharacter(newString, mChar));
        }

        for(int i = 0; i < JSON_TO_REPLACE.length; i += 2)
        {
            for(String mChar = JSON_TO_REPLACE[i]; newString.indexOf(mChar) > -1; newString = replace(newString, mChar, JSON_TO_REPLACE[i + 1]));
        }

        return newString;
    }

    public static InputStream getStreamFromObject(Object object)
        throws IOException
    {
        if(object instanceof com.jspsmart.upload.File)
        {
            com.jspsmart.upload.File file = (com.jspsmart.upload.File)object;
            byte buffer[] = new byte[file.getSize()];
            for(int index = 0; index < file.getSize(); index++)
                buffer[index] = file.getBinaryData(index);

            return new ByteArrayInputStream(buffer);
        }
        if(object instanceof File)
        {
            File file = (File)object;
            return new FileInputStream(file);
        }
        if(object instanceof URL)
        {
            URL file = (URL)object;
            return file.openStream();
        }
        if(object instanceof URLConnection)
        {
            URLConnection file = (URLConnection)object;
            return file.getInputStream();
        }
        if(object instanceof FDFDoc)
        {
            FDFDoc file = (FDFDoc)object;
            byte buffer[] = file.Save();
            return new ByteArrayInputStream(buffer);
        }
        if(object instanceof StringBuffer)
        {
            StringBuffer file = (StringBuffer)object;
            byte buffer[] = new byte[file.length()];
            buffer = file.toString().getBytes();
            return new ByteArrayInputStream(buffer);
        } else
        {
            return null;
        }
    }

    public static String getTimeFromTimestamp(String timestamp)
    {
        try
        {
            return timestamp.substring(11).replace('.', ':');
        }
        catch(Exception e)
        {
            return "00:00:00:000";
        }
    }

    public static String getTimeFromTimestamp(Timestamp timestamp)
    {
        Date date = new Date(timestamp.getTime());
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
        return formatter.format(date);
    }

    public static boolean isWeekend(int year, int month, int day)
    {
        Calendar cal = new GregorianCalendar(year, month, day);
        day = cal.get(7);
        return day == 7 || day == 1;
    }

    public static final char JSON_TO_TAKE[] = {
        '"', '\\', '\b', '\f', '\n', '\r', '\t'
    };
    public static final String JSON_TO_REPLACE[] = {
        "\301", "A", "\341", "a", "\311", "E", "\351", "e", "\315", "I", 
        "\355", "i", "\323", "O", "\363", "o", "\332", "U", "\372", "u", 
        "\361", "n"
    };

}
