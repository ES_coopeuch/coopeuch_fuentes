package datapro.eibs.menu;

import java.io.IOException;
import java.text.Collator;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.eibs.exception.FacadeException;
import com.datapro.eibs.facade.FASecurity;
import com.datapro.eibs.security.vo.WEBMMViewByUser;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ESS0040DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JSESS0040Bean extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1208010523540109618L;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		main(user, req, res, session);
	}

	private void procOldSecurityMenu(ESS0030DSMessage user, 
			HttpServletRequest req, HttpServletResponse res, 
			HttpSession session) throws ServletException, IOException {
			
			session.removeAttribute("mainMenu");
			session.removeAttribute("subMenu");
			
			JBObjList mainMenu = new JBObjList();
			
			MessageProcessor mp = null;
			try {
				mp = getMessageProcessor("ESS0040", req);
				ESS0040DSMessage msgMenu = (ESS0040DSMessage) mp.getMessageRecord("ESS0040DS");
				msgMenu.setESSUSR(user.getH01USR());
				msgMenu.setESSTYP(user.getE01LAN().toUpperCase());
				
				mp.sendMessage(msgMenu);
				
				msgMenu = (ESS0040DSMessage) mp.receiveMessageRecord("ESS0040DS");
				
				if ("T".equals(msgMenu.getESSTYP())) {
					Vector lst = new Vector();
					JBObjList tmpMainMenu = new JBObjList();
					JBObjList subMenu = new JBObjList();
					
					while (true) {
						msgMenu = (ESS0040DSMessage) mp.receiveMessageRecord("ESS0040DS");
						
						if ("M".equals(msgMenu.getESSTYP())) {
							addMenuItem(tmpMainMenu, lst, msgMenu);
						} else if ("C".equals(msgMenu.getESSTYP())) {
							addSubMenuItem(subMenu, msgMenu);
						} else if ("*".equals(msgMenu.getESSTYP())) {
							break;
						}
					}
					
					mainMenu = sortMenuItem(tmpMainMenu, lst);
					
					if (mainMenu.getNoResult()) {
						forward("ESS0040_menu_access_denied.jsp", req, res);
					} else {
						session.setAttribute("mainMenu", mainMenu);
						session.setAttribute("subMenu", subMenu);
						forward("ESS0040_menu_access_authorized.jsp?WEBPATH=" + webAppPath, req, res);
					}
				} else if ("*".equals(msgMenu.getESSTYP())) {
					forward("ESS0040_menu_access_denied.jsp", req, res);
				} else {
					forward("ESS0040_menu_access_denied.jsp", req, res);
				}
			
			} finally {
				if (mp != null) mp.close();
			}
	}
	
	private void procSecurityMenu(ESS0030DSMessage msgUser, 
			HttpServletRequest req, HttpServletResponse res, 
			HttpSession session) throws ServletException, IOException {
			
		session.removeAttribute("mainMenu");
		session.removeAttribute("subMenu");
		
		JBObjList mainMenu = new JBObjList();
		JBObjList subMenu = new JBObjList();
		
		FASecurity facade = new FASecurity();
		facade.setSessionUser(msgUser);
		
		try {
			List list = facade.getMainMenu(facade.getUser());
			if (!list.isEmpty()) {
				Iterator iter = list.listIterator();
				while (iter.hasNext()) {
					WEBMMViewByUser msgMenu = (WEBMMViewByUser) iter.next();
					if ("M".equals(msgMenu.getESSTYP())) {
						mainMenu.addRow(msgMenu);
					} else if ("C".equals(msgMenu.getESSTYP())) {
						subMenu.addRow(msgMenu);
					}	
				}
			}
			session.setAttribute("mainMenu", mainMenu);
			session.setAttribute("subMenu", subMenu);
			
			if (mainMenu.getNoResult()) {
				forward("ESS0040_menu_access_denied.jsp", req, res);
			} else {
				forward("ESS0040_menu_access_authorized.jsp?WEBPATH=" + webAppPath, req, res);
			}
		} catch (FacadeException e) {
			throw new ServletException(e);
		}
	}
	
	private void main(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		JBObjList mainMenu = (JBObjList) session.getAttribute("mainMenu");
		if (mainMenu != null) {
			if (mainMenu.getNoResult()) {
				forward("ESS0040_menu_access_denied.jsp", req, res);
			} else {
				forward("ESS0040_menu_access_authorized.jsp?WEBPATH=" + webAppPath, req, res);
			}
		} else {
        	procSecurityMenu(user, req, res, session);
			//procOldSecurityMenu(user, req, res, session);
		}
	}
	
	protected JBObjList sortMenuItem(JBObjList tmpMainMenu, Vector lst) {
		
		JBObjList mainMenu = new JBObjList();
		
		Set set = new HashSet();
		set.addAll(lst);
		lst = new Vector(set);
		Collator esCollator = Collator.getInstance(new Locale("en","US"));
		Collections.sort(lst, esCollator);
		
		String mnuText = "";
		ESS0040DSMessage msgOpt = new ESS0040DSMessage();
		for (int i=0; i<lst.size(); i++) {
			mnuText = (String) lst.get(i);
			tmpMainMenu.initRow();
			while (tmpMainMenu.getNextRow()) {
				msgOpt = (ESS0040DSMessage) tmpMainMenu.getRecord();
				if (mnuText.equals(msgOpt.getESSDSC())) {
					mainMenu.addRow(msgOpt);
					break;
				}
			}
		}
		
		return mainMenu;
	}

	protected void addSubMenuItem(JBObjList subMenu, ESS0040DSMessage msgMenu) {
		
		subMenu.addRow(msgMenu);
	}

	protected void addMenuItem(JBObjList tmpMainMenu, Vector lst, ESS0040DSMessage msgMenu) {
		
		tmpMainMenu.addRow(msgMenu);
		lst.add(msgMenu.getESSDSC());
	}

}
