/*
 * Created on Jan 20, 2009
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.datapro.eibs.images.vo;

import com.datapro.eibs.access.VOSuper;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SCNDOCView extends VOSuper {

	/**
	 * The TBLSSQ attribute.
	*/
	protected java.math.BigDecimal TBLSSQ = null;
   
	/**
	 * Gets the TBLSSQ value.
	*/
	public java.math.BigDecimal getTBLSSQ() {
	   return TBLSSQ;
	}
   
	/**
	 * Sets the TBLSSQ value.
	*/
	public void setTBLSSQ(java.math.BigDecimal TBLSSQ) {
	   this.TBLSSQ = TBLSSQ;
	}
	/**
	 * The TBLPAG attribute.
	*/
	protected java.math.BigDecimal TBLPAG = null;
   
	/**
	 * Gets the TBLPAG value.
	*/
	public java.math.BigDecimal getTBLPAG() {
	   return TBLPAG;
	}
   
	/**
	 * Sets the TBLPAG value.
	*/
	public void setTBLPAG(java.math.BigDecimal TBLPAG) {
	   this.TBLPAG = TBLPAG;
	}
	/**
	 * The TBLDSC attribute.
	*/
	protected java.lang.String TBLDSC = "";
   
	/**
	 * Gets the TBLDSC value.
	*/
	public java.lang.String getTBLDSC() {
	   return TBLDSC.toUpperCase().trim();
	}
   
	/**
	 * Sets the TBLDSC value.
	*/
	public void setTBLDSC(java.lang.String TBLDSC) {
	   this.TBLDSC = TBLDSC;
	}
	
	/**
	 * The TBLSTS attribute.
	*/
	protected java.lang.String TBLSTS = "";
   
	/**
	 * Gets the TBLSTS value.
	*/
	public java.lang.String getTBLSTS() {
	   return TBLSTS.toUpperCase().trim();
	}
   
	/**
	 * Sets the TBLSTS value.
	*/
	public void setTBLSTS(java.lang.String TBLSTS) {
	   this.TBLSTS = TBLSTS;
	}
	
	/**
	 * The TBLLMD attribute.
	*/
	protected java.math.BigDecimal TBLLMD = null;
   
	/**
	 * Gets the TBLLMD value.
	*/
	public java.math.BigDecimal getTBLLMD() {
	   return TBLLMD;
	}
   
	/**
	 * Sets the TBLLMD value.
	*/
	public void setTBLLMD(java.math.BigDecimal TBLLMD) {
	   this.TBLLMD = TBLLMD;
	}
	/**
	 * The TBLLMM attribute.
	*/
	protected java.math.BigDecimal TBLLMM = null;
   
	/**
	 * Gets the TBLLMM value.
	*/
	public java.math.BigDecimal getTBLLMM() {
	   return TBLLMM;
	}
   
	/**
	 * Sets the TBLLMM value.
	*/
	public void setTBLLMM(java.math.BigDecimal TBLLMM) {
	   this.TBLLMM = TBLLMM;
	}
	/**
	 * The TBLLMY attribute.
	*/
	protected java.math.BigDecimal TBLLMY = null;
   
	/**
	 * Gets the TBLLMY value.
	*/
	public java.math.BigDecimal getTBLLMY() {
	   return TBLLMY;
	}
   
	/**
	 * Sets the TBLLMY value.
	*/
	public void setTBLLMY(java.math.BigDecimal TBLLMY) {
	   this.TBLLMY = TBLLMY;
	}
	/**
	 * The TBLUID attribute.
	*/
	protected java.math.BigDecimal TBLUID = null;
   

	/**
	 * @return
	 */
	public java.math.BigDecimal getTBLUID() {
		return TBLUID;
	}

	/**
	 * @param decimal
	 */
	public void setTBLUID(java.math.BigDecimal decimal) {
		TBLUID = decimal;
	}
	/**
	 * The IMGUID attribute.
	*/
	protected java.math.BigDecimal IMGUID = null;
   
	/**
	 * Gets the IMGUID value.
	*/
	public java.math.BigDecimal getIMGUID() {
	   return IMGUID;
	}
   
	/**
	 * Sets the IMGUID value.
	*/
	public void setIMGUID(java.math.BigDecimal IMGUID) {
	   this.IMGUID = IMGUID;
	}
	/**
	 * The IMGBIN attribute.
	*/
	protected byte[] IMGBIN = null;
   
	/**
	 * Gets the IMGBIN value.
	*/
	public byte[] getIMGBIN() {
	   return IMGBIN;
	}
   
	/**
	 * Sets the IMGBIN value.
	*/
	public void setIMGBIN(byte[] IMGBIN) {
	   this.IMGBIN = IMGBIN;
	}
	/**
	 * The IMGLEN attribute.
	*/
	protected java.math.BigDecimal IMGLEN = ZERO;
   
	/**
	 * Gets the IMGLEN value.
	*/
	public java.math.BigDecimal getIMGLEN() {
	   return IMGLEN;
	}
   
	/**
	 * Sets the IMGLEN value.
	*/
	public void setIMGLEN(java.math.BigDecimal IMGLEN) {
	   this.IMGLEN = IMGLEN;
	}

}
