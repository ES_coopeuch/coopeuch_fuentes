package datapro.eibs.salesplatform;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.eibs.internet.generics.Util;


import datapro.eibs.beans.EPV103801Message;
import datapro.eibs.beans.EPV107001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV107002Message;
import datapro.eibs.beans.EPV114001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageRecord;

public class JSEPV1070 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_PLATFORM_LIST = 100;	
	protected static final int A_PLATFORM_LIST = 101;
	
	protected static final int R_PLATFORM_INQUIRY = 203;
	

	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		switch (screen) {
		case R_PLATFORM_LIST:
			procReqPlatformList(user, req, res, session);
			break;
		case A_PLATFORM_LIST:
			procActionPlatformList(user, req, res, session);
			break;
		case R_PLATFORM_INQUIRY:
			procReqPlatformInquiry(user, req, res, session);
			break;
		default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}
	
	/**
	 * procReqPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		mp = getMessageProcessor("EPV1070", req);

		EPV107001Message msg = (EPV107001Message) mp.getMessageRecord("EPV107001");
		msg.setE01SELDTL("Y");
		msg.setE01SELTOT("Y");
		msg.setE01SELBNK(user.getE01UBK());
		msg.setE01SELBRN(user.getE01UBR());
		msg.setE01SELFIM(user.getE01RDM());
		msg.setE01SELFID(user.getE01RDD());
		msg.setE01SELFIY(user.getE01RDY());
		msg.setE01SELFFM(user.getE01RDM());
		msg.setE01SELFFD(user.getE01RDD());
		msg.setE01SELFFY(user.getE01RDY());
		
		try {
			session.setAttribute("ventas", msg);
			flexLog("About to call Page: EPV1070_salesplatform_selection_enter.jsp");
			forward("EPV1070_salesplatform_selection_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	
	
	/**
	 * procActionPlatformList: find the list of forms depending on status, the program will epvl1070
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = getUserPos(session);

		try {

			mp = getMessageProcessor("EPV1070", req);

			EPV107001Message msg = (EPV107001Message) mp.getMessageRecord("EPV107001", user.getH01USR(), "0015");
			EPV107002Message tot = (EPV107002Message) mp.getMessageRecord("EPV107002");
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);
			userPO.setHeader8(msg.getE01SELDTL());
			userPO.setHeader9(msg.getE01SELTOT());

			// Sends message
			mp.sendMessage(msg);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", error);
				flexLog("About to call Page: EPV1070_salesplatform_selection_enter.jsp");
				forward("EPV1070_salesplatform_selection_enter.jsp", req, res);
			} else {
				// Receive salesPlatform list
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
				// receive total information
				tot = (EPV107002Message) mp.receiveMessageRecord();

				// if there are NO errors display list
				session.setAttribute("EPV107001List", list);
				session.setAttribute("total", tot);
				
				forwardOnSuccess("EPV1070_salesplatform_list.jsp", req, res);
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	protected void procReqPlatformInquiry(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		try {
			JBObjList list = (JBObjList) session.getAttribute("EPV107001List");
			list.setCurrentRow(Util.parseInt(req.getParameter("ROW")));
			session.setAttribute("ventas", list.getRecord());
			
			forward("EPV1070_salesplatform_inquiry.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

 }	



