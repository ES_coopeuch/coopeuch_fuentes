<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>

<html>
<head>
<title>Parametros de Cheques Oficiales / Orden de Pago</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="checksParam" class="datapro.eibs.beans.EOF000001Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }
   
</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
%>

  
 <% int row = 0;%>

<H3 align="center">Parametros de Control de Cheques<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="of_chk_parameters_details, EOF0000"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEOF0000" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" >
              <div align="right"><b>Banco :</b></div>
            </td>
            <td nowrap width="20%" ><b>
              <input type="text" name="E01OFCBNK" size="3" maxlength="2"  value="<%= checksParam.getE01OFCBNK().trim()%>" readonly>
              </b></td>
            <td nowrap width="16%" >
              <div align="right"><b>Moneda :</b></div>
            </td>
            <td nowrap colspan="3" >
              <div align="left">
                <input type="text" name="E01OFCCCY" size="4" maxlength="3" value="<%= checksParam.getE01OFCCCY().trim()%>" readonly>
                </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Formato :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> <b> 
                <input type="text" name="E01OFCFTY" size="3" maxlength="2"  value="<%= checksParam.getE01OFCFTY().trim()%>" >
                </b></div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Descripci&oacute;n :</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><b><font face="Arial"><font face="Arial"><font size="2"> 
                <eibsinput:text name="checksParam" property="E01OFCDSC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"/>
            </font></font></font></b></div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Informaci&oacute;n B&aacute;sica</h4>  
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cuenta Contable Cheques por Imprimir :</div>
            </td>
            <td nowrap width="30%"> 
              <div align="left"> 
              <eibsinput:help name="checksParam" property="E01OFCXPR"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>"
					fn_param_one="E01OFCXPR"
					fn_param_two="document.forms[0].E01OFCBNK.value" fn_param_three="document.forms[0].E01OFCCCY.value" />
            </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta Contable Cheques Impresos :</div>
            </td>
            <td nowrap width="30%"> 
              <div align="left"> 
              <eibsinput:help name="checksParam" property="E01OFCPRT"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>"
					fn_param_one="E01OFCPRT"
					fn_param_two="document.forms[0].E01OFCBNK.value" fn_param_three="document.forms[0].E01OFCCCY.value" />
			</div>		
           </td>
          </tr>
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
           <td nowrap width="20%"> 
              <div align="right">Cuenta Contable Retenciones:</div>
            </td>
            <td nowrap width="30%"> 
              <div align="left"> 
              <eibsinput:help name="checksParam" property="E01OFCWTH"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>"
					fn_param_one="E01OFCWTH"
					fn_param_two="document.forms[0].E01OFCBNK.value" fn_param_three="document.forms[0].E01OFCCCY.value" />
					</div>
         </td>
            <td nowrap width="20%"> 
              <div align="right">Porcentaje por Retenciones :</div>
            </td>
            <td nowrap width="30%"> 
              <div align="left"> 
                <eibsinput:text name="checksParam" property="E01OFCWTP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="false"/>
          </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">N&uacute;mero de D&iacute;as Adelanto Generar 
                Pagos :</div>
            </td>
            <td nowrap width="30%"> 
              <div align="left"> 
                <eibsinput:text name="checksParam" property="E01OFCVYS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" maxlength="3" size="4"/>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tipo de N&uacute;meraci&oacute;n :</div>
            </td>
            <td nowrap width="30%"> 
              <div align="left"> 
                <input type="text" name="E01OFCNXO" maxlength="1" size="2" value="<%= checksParam.getE01OFCNXO().trim()%>" >
                <a href="javascript:GetCode('E01OFCNXO','STATIC_of_chk_params_num.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0" ></a> 
              </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Lineas al Final Varios Pagos en 1 Cheque :</div>
            </td>
            <td nowrap width="30%"> 
              <div align="left"> 
                <eibsinput:text name="checksParam" property="E01OFCPCK" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" maxlength="2" size="3"/>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tipo de Documento :</div>
            </td>
            <td nowrap width="30%"> 
              <div align="left">
				<SELECT name="E01BAFDTY">
					<OPTION value="1" <% if(checksParam.getE01BAFDTY().equals("1")) out.print("selected");%>>Cheque Oficial</OPTION>
					<OPTION value="3" <% if(checksParam.getE01BAFDTY().equals("3")) out.print("selected");%>>Giro</OPTION>
					<OPTION value="4" <% if(checksParam.getE01BAFDTY().equals("4")) out.print("selected");%>>Cheque Terceros</OPTION>
				</SELECT></div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cuenta Contable D&iacute;as Inactivo 1 :</div>
            </td>
            <td nowrap width="30%"> 
              <eibsinput:help name="checksParam" property="E01OFCGI1"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>"
					fn_param_one="E01OFCGI1"
					fn_param_two="document.forms[0].E01OFCBNK.value" fn_param_three="document.forms[0].E01OFCCCY.value" />
            </td>
            <td nowrap width="20%"> 
              <div align="right">D&iacute;as Inactivo 1 :</div>
            </td>
            <td nowrap width="30%"> 
              <div align="left"> 
                 <eibsinput:text name="checksParam" property="E01OFCDI1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" maxlength="3" size="4"/>
              </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cuenta Contable D&iacute;as Inactivo 2 :</div>
            </td>
            <td nowrap width="30%"> 
              <eibsinput:help name="checksParam" property="E01OFCGI2"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>"
					fn_param_one="E01OFCGI2"
					fn_param_two="document.forms[0].E01OFCBNK.value" fn_param_three="document.forms[0].E01OFCCCY.value" />
            </td>
            <td nowrap width="20%"> 
              <div align="right">D&iacute;as Inactivo 2 :</div>
            </td>
            <td nowrap width="30%"> 
              <div align="left"> 
 	             <eibsinput:text name="checksParam" property="E01OFCDI2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" maxlength="3" size="4"/>
              </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Tipo de Proceso :</div>
            </td>
            <td nowrap width="30%"> 
             <div align="left">
				<SELECT name="E01OFCPRC">
					<OPTION value="O" <% if(checksParam.getE01OFCPRC().equals("O")) out.print("selected");%>>ONLINE</OPTION>
					<OPTION value="B" <% if(checksParam.getE01OFCPRC().equals("B")) out.print("selected");%>>BATCH/MASIVO</OPTION>
				</SELECT></div>
            </td>          
            <td nowrap width="20%"> 
              <div align="right">Concepto Contable :</div>
            </td>
            <td nowrap width="30%"> 
            <div align="left" nowrap> 
            	<input type=text name="E01OFCCON" id="E01OFCCON" value="<%= checksParam.getE01OFCCON().trim()%>" size="3" maxlength="3"> 
            	<input type=HIDDEN name="C01OFCCON" value="<%= checksParam.getC01OFCCON().trim()%>">            	          	
            	<input type="text" name="D01OFCCON" size="25" maxlength="25" readonly value="<%= checksParam.getD01OFCCON().trim()%>"
            	 oncontextmenu="showPopUp(conceptHelp,this.name,document.forms[0].E01OFCBNK.value,'','C01OFCCON','E01OFCCON','OF')">
            </div>       
            </td>
          </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Forma eIBS :</div>
            </td>
            <td nowrap width="30%">
              <input type="text" name="E01OFCPTH" maxlength="80" size="60" value="<%= checksParam.getE01OFCPTH().trim()%>" >
            </td>
            <td nowrap width="20%"> 
              <div align="right"></div>
            </td>
            <td nowrap width="30%">
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Cuentas</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="center">Secuencia</div>
            </td>
            <td nowrap> 
              <div align="center">Cargo</div>
            </td>
            <td nowrap> 
              <div align="center">Factor</div>
            </td>
            <td nowrap> 
              <div align="center">IVA</div>
            </td>
            <td nowrap> 
              <div align="center">Cuenta Ingresos</div>
            </td>
            <td nowrap> 
              <div align="center">Descripci&oacute;n</div>
            </td>
            <td nowrap> 
              <div align="center">Monto L&iacute;mite</div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="23"> 
              <div align="center"> 
                <input type="text" name="E01BAF101" value="<%= checksParam.getE01BAF101().trim()%>" size="4" maxlength="3">
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center"> 
                <input type="text" name="E01BAFA01" value="<%= checksParam.getE01BAFA01().trim()%>" size="17" maxlength="15">
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center"> 
                <input type="text" name="E01BAF201" size="2" maxlength="1" 
                oncontextmenu="GetCode(this.name,'STATIC_par_eof0000_fact.jsp')"
                value="<%= checksParam.getE01BAF201().trim()%>" >
				 
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center"> 
                <input type="text" name="E01BAFI01" size="2" maxlength="1" value="<%= checksParam.getE01BAFI01().trim()%>"
				 >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center"> 
                <input type="text" name="E01BAFG01" size="17" maxlength="16" value="<%= checksParam.getE01BAFG01().trim()%>"
                	oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01OFCBNK.value,'','','','')">
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="F01BAFD01" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"/>
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="E01BAFL01" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
            </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF102" value="<%= checksParam.getE01BAF102().trim()%>" size="4" maxlength="3">
              </div>
            </td>
            <td nowrap height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFA02" value="<%= checksParam.getE01BAFA02().trim()%>" size="17" maxlength="15">
              </div>
            </td>
            <td nowrap height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF202" size="2" maxlength="1" value="<%= checksParam.getE01BAF202().trim()%>"
				 >
              </div>
            </td>
            <td nowrap height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFI02" size="2" maxlength="1" value="<%= checksParam.getE01BAFI02().trim()%>"
				 >
              </div>
            </td>
            <td nowrap height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFG02" size="17" maxlength="16" value="<%= checksParam.getE01BAFG02().trim()%>"
                	oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01OFCBNK.value,'','','','')">
              </div>
            </td>
            <td nowrap height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="F01BAFD02" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"/>
              </div>
            </td>
            <td nowrap height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="E01BAFL02" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
           </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF103" value="<%= checksParam.getE01BAF103().trim()%>" size="4" maxlength="3">
              </div>
            </td>
            <td nowrap width="40%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFA03" value="<%= checksParam.getE01BAFA03().trim()%>" size="17" maxlength="15">
              </div>
            </td>
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF203" size="2" maxlength="1" value="<%= checksParam.getE01BAF203().trim()%>"
				 >
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFI03" size="2" maxlength="1" value="<%= checksParam.getE01BAFI03().trim()%>"
				 >
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFG03" size="17" maxlength="16" value="<%= checksParam.getE01BAFG03().trim()%>"
                	oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01OFCBNK.value,'','','','')">
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                 <eibsinput:text name="checksParam" property="F01BAFD03" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"/>
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="E01BAFL03" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
           </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF104" value="<%= checksParam.getE01BAF104().trim()%>" size="4" maxlength="3">
              </div>
            </td>
            <td nowrap width="40%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFA04" value="<%= checksParam.getE01BAFA04().trim()%>" size="17" maxlength="15">
              </div>
            </td>
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF204" size="2" maxlength="1" value="<%= checksParam.getE01BAF204().trim()%>"
				 >
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFI04" size="2" maxlength="1" value="<%= checksParam.getE01BAFI04().trim()%>"
				 >
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFG04" size="17" maxlength="16" value="<%= checksParam.getE01BAFG04().trim()%>"
                	oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01OFCBNK.value,'','','','')">
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="F01BAFD04" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"/>
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="E01BAFL04" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
             </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF105" value="<%= checksParam.getE01BAF105().trim()%>" size="4" maxlength="3">
              </div>
            </td>
            <td nowrap width="40%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFA05" value="<%= checksParam.getE01BAFA05().trim()%>" size="17" maxlength="15">
              </div>
            </td>
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF205" size="2" maxlength="1" value="<%= checksParam.getE01BAF205().trim()%>"
				 >
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFI05" size="2" maxlength="1" value="<%= checksParam.getE01BAFI05().trim()%>"
				 >
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFG05" size="17" maxlength="16" value="<%= checksParam.getE01BAFG05().trim()%>"
                	oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01OFCBNK.value,'','','','')">
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="F01BAFD05" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"/>
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="E01BAFL05" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
             </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF106" value="<%= checksParam.getE01BAF106().trim()%>" size="4" maxlength="3">
              </div>
            </td>
            <td nowrap width="40%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFA06" value="<%= checksParam.getE01BAFA06().trim()%>" size="17" maxlength="15">
              </div>
            </td>
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF206" size="2" maxlength="1" value="<%= checksParam.getE01BAF206().trim()%>"
				 >
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFI06" size="2" maxlength="1" value="<%= checksParam.getE01BAFI06().trim()%>"
				 >
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFG06" size="17" maxlength="16" value="<%= checksParam.getE01BAFG06().trim()%>"
                	oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01OFCBNK.value,'','','','')">
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="F01BAFD06" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"/>
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="E01BAFL06" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
           </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF107" value="<%= checksParam.getE01BAF107().trim()%>" size="4" maxlength="3">
              </div>
            </td>
            <td nowrap width="40%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFA07" value="<%= checksParam.getE01BAFA07().trim()%>" size="17" maxlength="15">
              </div>
            </td>
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF207" size="2" maxlength="1" value="<%= checksParam.getE01BAF207().trim()%>"/>				
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFI07" size="2" maxlength="1" value="<%= checksParam.getE01BAFI07().trim()%>"
				 />
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFG07" size="17" maxlength="16" value="<%= checksParam.getE01BAFG07().trim()%>"
                	oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01OFCBNK.value,'','','','')">
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="F01BAFD07" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"/>
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="E01BAFL07" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
             </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF108" value="<%= checksParam.getE01BAF108().trim()%>" size="4" maxlength="3">
              </div>
            </td>
            <td nowrap width="40%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFA08" value="<%= checksParam.getE01BAFA08().trim()%>" size="17" maxlength="15">
              </div>
            </td>
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF208" size="2" maxlength="1" value="<%= checksParam.getE01BAF208().trim()%>"
				 />
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFI08" size="2" maxlength="1" value="<%= checksParam.getE01BAFI08().trim()%>"
				 />
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFG08" size="17" maxlength="16" value="<%= checksParam.getE01BAFG08().trim()%>"
                	oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01OFCBNK.value,'','','','')">
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="F01BAFD08" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"/>
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="E01BAFL08" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
             </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF109" value="<%= checksParam.getE01BAF109().trim()%>" size="4" maxlength="3">
              </div>
            </td>
            <td nowrap width="40%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFA09" value="<%= checksParam.getE01BAFA09().trim()%>" size="17" maxlength="15">
              </div>
            </td>
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF209" size="2" maxlength="1" value="<%= checksParam.getE01BAF209().trim()%>"
				 />
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFI09" size="2" maxlength="1" value="<%= checksParam.getE01BAFI09().trim()%>"
				 >
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFG09" size="17" maxlength="16" value="<%= checksParam.getE01BAFG09().trim()%>"
                	oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01OFCBNK.value,'','','','')">
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="F01BAFD09" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"/>
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="E01BAFL09" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
              </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF110" value="<%= checksParam.getE01BAF110().trim()%>" size="4" maxlength="3">
              </div>
            </td>
            <td nowrap width="40%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFA10" value="<%= checksParam.getE01BAFA10().trim()%>" size="17" maxlength="15">
              </div>
            </td>
            <td nowrap width="16%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAF2010" size="2" maxlength="1" value="<%= checksParam.getE01BAF210().trim()%>"
				 />
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFI10" size="2" maxlength="1" value="<%= checksParam.getE01BAFI10().trim()%>"
				 >
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <input type="text" name="E01BAFG010" size="17" maxlength="16" value="<%= checksParam.getE01BAFG10().trim()%>"
                	oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01OFCBNK.value,'','','','')">
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                 <eibsinput:text name="checksParam" property="F01BAFD10" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="false"></eibsinput:text>
              </div>
            </td>
            <td nowrap width="28%" height="19"> 
              <div align="center"> 
                <eibsinput:text name="checksParam" property="E01BAFL10" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
            </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <div align="center">
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
  </form>
</body>
</html>
