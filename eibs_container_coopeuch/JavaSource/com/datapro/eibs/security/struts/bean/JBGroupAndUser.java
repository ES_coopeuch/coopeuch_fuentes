package com.datapro.eibs.security.struts.bean;

/**
 * @author ogarcia
 *
 */
public class JBGroupAndUser {
	
	private String GRPID = "";
	private String USRID = "";
	private String STATUS = "";
	private String IMGSTS = "";
	private String IMGGOU = "";
	private String URL = "";

	/**
	 * Returns the GRPID.
	 * @return String
	 */
	public String getGRPID() {
		return GRPID.trim();
	}

	/**
	 * Returns the STATUS.
	 * @return String
	 */
	public String getSTATUS() {
		return STATUS;
	}

	/**
	 * Returns the USRID.
	 * @return String
	 */
	public String getUSRID() {
		return USRID.trim();
	}

	/**
	 * Sets the GRPID.
	 * @param GRPID The GRPID to set
	 */
	public void setGRPID(String GRPID) {
		this.GRPID = GRPID;
	}

	/**
	 * Sets the STATUS.
	 * @param STATUS The STATUS to set
	 */
	public void setSTATUS(String STATUS) {
		this.STATUS = STATUS;
	}

	/**
	 * Sets the USRID.
	 * @param USRID The USRID to set
	 */
	public void setUSRID(String USRID) {
		this.USRID = USRID;
	}

	/**
	 * Returns the IMGGOU.
	 * @return String
	 */
	public String getIMGGOU() {
		return IMGGOU.trim();
	}

	/**
	 * Returns the IMGSTS.
	 * @return String
	 */
	public String getIMGSTS() {
		return IMGSTS.trim();
	}

	/**
	 * Sets the IMGGOU.
	 * @param IMGGOU The IMGGOU to set
	 */
	public void setIMGGOU(String IMGGOU) {
		this.IMGGOU = IMGGOU;
	}

	/**
	 * Sets the IMGSTS.
	 * @param IMGSTS The IMGSTS to set
	 */
	public void setIMGSTS(String IMGSTS) {
		this.IMGSTS = IMGSTS;
	}

	/**
	 * Returns the URL.
	 * @return String
	 */
	public String getURL() {
		return URL;
	}

	/**
	 * Sets the URL.
	 * @param URL The URL to set
	 */
	public void setURL(String URL) {
		this.URL = URL;
	}

}
