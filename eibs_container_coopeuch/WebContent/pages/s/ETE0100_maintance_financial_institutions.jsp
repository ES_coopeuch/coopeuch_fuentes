<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ETE010002Message"%>
<%@page import="com.datapro.constants.EibsFields"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Transferencias - Seguridad y Parámetros</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="IfiObj" class= "datapro.eibs.beans.ETE010002Message"  scope="session" />
<jsp:useBean id="listbancos" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">
function goAction3(op) {

 	document.forms[0].SCREEN.value = op;
	document.forms[0].submit();		
}
</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Transferencias - Seguridad y Par&aacute;metros
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="maintance_financial_institutions.jsp, ETE0100">
</h3>

<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.tefmulti.JSETE0100">
<input type="hidden" name="SCREEN" value="100">

<table  class="tableinfo">
	<tr bordercolor="#FFFFFF"> 
		<td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Tipo de Transacci&oacute;n : 
              </td>
             <td nowrap width="10%" align="left">
               <input type="text" name="E02LIFCOD" value="<%=session.getAttribute("CODTRX")%>" size="4"  readonly>
               <input type="text" name="E02LIFDCO" value="<%=session.getAttribute("DESTRX") %>" size="40" readonly>
             </td>
         </tr>
        </table>
      </td>
    </tr>
</table>

<table  class="tableinfo">
	<tr bordercolor="#FFFFFF"> 
		<td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Instituci&oacute;n : 
              </td>
             <td nowrap width="90%" align="left" colspan="3" >
       			 <div align="left"> 
 	     			  <input type="hidden" name="E02LIFCOD" value="<%if(!userPO.getPurpose().equals("NEW")) out.print(IfiObj.getE02LIFCOD()); else out.print(session.getAttribute("CODTRX"));%>" size="5" maxlength="4" <%if(userPO.getPurpose().equals("MAINTENANCE")) out.print("readonly"); %> >
 	     			  <input type="text" name="E02LIFSBI" value="<%= IfiObj.getE02LIFSBI()%>" size="5" maxlength="4" <%if(userPO.getPurpose().equals("MAINTENANCE")) out.print("readonly"); %> >
        			   <input type="text" name="E02LIFDSB" value="<%= IfiObj.getE02LIFDSB()%>" size="50" maxlength="45"  readonly >
 <%if(!userPO.getPurpose().equals("MAINTENANCE")) 
 {%>               
	   				    <a href="javascript:GetCodeDescCNOFC('E02LIFSBI','E02LIFDSB','X3')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
	     			   <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >  
 <%}%>               
 
        		</div>
             </td>
             <td nowrap width="10%" align="right"> Estado :   
              </td>
             <td nowrap width="60%" align="left" >
               <select name="E02LIFSTS" >
                    <option value="" <% if (IfiObj.getE02LIFSTS().equals("")) out.print("selected"); %>>Seleccione</option>
                    <option value="A" <% if (IfiObj.getE02LIFSTS().equals("A")) out.print("selected"); %>>ACTIVA</option>                   
                    <option value="C" <% if (IfiObj.getE02LIFSTS().equals("C")) out.print("selected"); %>>CERRADA</option>                   
                  </select>
               
               
             </td>
         </tr>
                   <tr>
             <td nowrap width="14%" align="right"> Monto M&iacute;nimo : 
              </td>
             <td nowrap width="12%" align="left">
               <eibsinput:text name="IfiObj" property="E02LIFMIN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />	
             </td>
             <td nowrap width="12%" align="right"> Monto M&aacute;ximo : 
              </td>
             <td nowrap width="12%" align="left">
               <eibsinput:text name="IfiObj" property="E02LIFMAX" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />	
             </td>
             <td nowrap width="12%" align="right"> Monto L&iacute;mite : 
              </td>
             <td nowrap width="12%" align="left">
               <eibsinput:text name="IfiObj" property="E02LIFLIM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />	
             </td>
         </tr>
          <tr>
             <td nowrap width="14%" align="right"> Monto Diario : 
              </td>
             <td nowrap width="12%" align="left">
               <eibsinput:text name="IfiObj" property="E02LIFDAY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />	
             </td>
             <td nowrap width="12%" align="right"> Monto Primer TEF :  
              </td>
             <td nowrap width="12%" align="left">
                <eibsinput:text name="IfiObj" property="E02LIFPRI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />	
             </td>
             <td nowrap width="12%" align="right">Factor :
              </td>
             <td nowrap width="12%" align="left">
    	       <input type="text" name="E02LIFFA1" value="<%= IfiObj.getE02LIFFA1()%>" size="2" maxlength="1" onkeypress="enterInteger()" >
         </tr>
         
        </table>
      </td>
    </tr>
</table>
<p align="center">
	<input id="EIBSBTN" type="button" name="Enviar" value="Enviar" onclick="goAction3('700')">
	<input id="EIBSBTN" type="button" name="Volver" value="Volver" onclick="goAction3('200')">
	
</p>

<h4>Instituciones Financieras Actuales</h4>
<hr style="  border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));  "/>

<table class="tbenter" width="100%">

<tr>
<td>

<table id="headTable"  width="100%" align="left">
	<tr id="trdark">
			<th align="center" nowrap width="35%">Instituci&oacute;n Financiera</th>
			<th align="center" nowrap width="10%">Monto M&iacute;nimo</th>
			<th align="center" nowrap width="10%">Monto M&aacute;ximo</th>
			<th align="center" nowrap width="10%">Monto L&iacute;mite</th>
			<th align="center" nowrap width="10%">Monto Diario</th>
			<th align="center" nowrap width="10%">Monto 1ra Trx</th>
			<th align="center" nowrap width="10%">Factor</th>
			<th align="center" nowrap width="5%">Estado</th>
	</tr>
		<%
			listbancos.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (listbancos.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ETE010002Message regtrans = (ETE010002Message) listbancos.getRecord();
		%>
	<tr>
			
			<td nowrap align="left">
				<%=regtrans.getE02LIFDSB() %>
			</td>

			<td nowrap align="right">
				<%=regtrans.getE02LIFMIN()  %>
			</td>
			<td nowrap align="right">
				<%=regtrans.getE02LIFMAX()  %>
			</td>
			<td nowrap align="right">
				<%=regtrans.getE02LIFLIM()  %>
			</td>
			<td nowrap align="right">
				<%=regtrans.getE02LIFDAY()  %>
			</td>
			<td nowrap align="right">
				<%=regtrans.getE02LIFPRI()  %>
			</td>
			<td nowrap align="right">
				<%=regtrans.getE02LIFFA1()  %>
			</td>
			
			<td nowrap align="center">
				<%if(regtrans.getE02LIFSTS().equals("A"))
					out.print("ACTIVA");
				  else
				  	if(regtrans.getE02LIFSTS().equals("C"))
						out.print("CERRADA");
					else 
						out.print("NO DEF");
			%>
			</td>
		    
	</tr>
		<%
			}
		%>
	</table>
</td>
</tr>


</table>

</form>
</body>
</html>
