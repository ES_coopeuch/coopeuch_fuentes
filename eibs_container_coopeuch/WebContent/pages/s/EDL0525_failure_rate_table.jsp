<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
<head>
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<link rel="stylesheet" href="../../theme/Master.css" type="text/css">
<title>Tabla del Porcentaje de Incumplimiento de Garantias</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="GENERATOR" content="Rational Software Architect">

<jsp:useBean id="EDL0525Record" class="datapro.eibs.beans.EDL052501Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

</head>
<body>
<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
%>

<H3 align="center">Tabla del Porcentaje de Incumplimiento de Garantias<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="failure_rate_table.jsp, EDL0525"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEDL0525" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="4">
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Tipo  de Garantia:</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2"> 
              	<input type="text" name="E01GPITGA" maxlength="4" size="5" value="<%= EDL0525Record.getE01GPITGA()%>" >
              	</font></font></font>
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Clase de Credito:</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2"> 
              	<input type="text" name="E01GPITPC" maxlength="4" size="5" value="<%= EDL0525Record.getE01GPITPC()%>" >
              	</font></font></font>
              </div>
             </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>  
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="33%"> 
              <div align="center"><b>Clase de Garantia</b></div>
            </td>
            <td nowrap width="33%"> 
              <div align="center"><b>Subclase de Garantia</b></div>
            </td>
            <td nowrap width="33%"> 
              <div align="center"><b>PDI Porcentaje Base</b></div>
            </td>
        </tr>
          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="center"> 
              	<input type="text" name="E01GPICLG" maxlength="4" size="5" value="<%= EDL0525Record.getE01GPICLG()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPISCG" maxlength="5" size="6" value="<%= EDL0525Record.getE01GPISCG()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPIPDI" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIPDI()%>" >
              </div>
            </td>
          </tr>
          </table>
          <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
          	<td nowrap width="25%"> 
              <div align="center"><b>Dias de Incumplimiento 1</b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>PDI Porcentaje de Incumplimiento 1</b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b> Dias de Incumplimiento 2</b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>PDI Porcentaje de Incumplimiento 2</b></div>
            </td>
            
         </tr>
          <tr id="trclear"> 
          <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPIDI1" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIDI1()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPIPI1" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIPI1()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center"> 
              	<input type="text" name="E01GPIDI2" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIDI2()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPIPI2" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIPI2()%>" >
              </div>
        </tr>
          <tr id="trdark"> 
          <td nowrap width="25%"> 
              <div align="center"><b>Dias de Incumplimiento 3</b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>PDI Porcentaje de Incumplimiento 3</b></div>
            </td>
          <td nowrap width="25%"> 
              <div align="center"><b>Dias de Incumplimiento 4</b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>PDI Porcentaje de Incumplimiento 4</b></div>
            </td>
         </tr>
          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="center"> 
              	<input type="text" name="E01GPIDI3" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIDI3()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPIPI3" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIPI3()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPIDI4" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIDI4()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPIPI4" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIPI4()%>" >
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
          <td nowrap width="25%"> 
              <div align="center"><b>Dias de Incumplimiento 5</b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>PDI Porcentaje de Incumplimiento 5</b></div>
            </td>
          <td nowrap width="25%"> 
              <div align="center"><b>Dias de Incumplimiento 6</b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>PDI Porcentaje de Incumplimiento 6</b></div>
            </td>
         </tr>
         <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="center"> 
              	<input type="text" name="E01GPIDI5" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIDI5()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPIPI5" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIPI5()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPIDI6" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIDI6()%>" >
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="center">
                <input type="text" name="E01GPIPI6" maxlength="6" size="9" value="<%= EDL0525Record.getE01GPIPI6()%>" >
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>
  <p>
  <br>
  </p>
  <div align="center"> 
  	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
  </form>
</body>
</html>
