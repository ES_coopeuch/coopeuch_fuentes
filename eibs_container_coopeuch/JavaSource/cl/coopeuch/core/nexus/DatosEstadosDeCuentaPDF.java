/**
 * DatosEstadosDeCuentaPDF.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.coopeuch.core.nexus;

public class DatosEstadosDeCuentaPDF  implements java.io.Serializable {
    private java.lang.String num_cta_tar;
    private int fecha_fact;
    private java.lang.String tipo_eecc_pdf;

    public DatosEstadosDeCuentaPDF() {
    }

    public java.lang.String getNum_cta_tar() {
        return num_cta_tar;
    }

    public void setNum_cta_tar(java.lang.String num_cta_tar) {
        this.num_cta_tar = num_cta_tar;
    }

    public int getFecha_fact() {
        return fecha_fact;
    }

    public void setFecha_fact(int fecha_fact) {
        this.fecha_fact = fecha_fact;
    }

    public java.lang.String getTipo_eecc_pdf() {
        return tipo_eecc_pdf;
    }

    public void setTipo_eecc_pdf(java.lang.String tipo_eecc_pdf) {
        this.tipo_eecc_pdf = tipo_eecc_pdf;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        DatosEstadosDeCuentaPDF other = (DatosEstadosDeCuentaPDF) obj;
        boolean _equals;
        _equals = true
            && ((this.num_cta_tar==null && other.getNum_cta_tar()==null) || 
             (this.num_cta_tar!=null &&
              this.num_cta_tar.equals(other.getNum_cta_tar())))
            && this.fecha_fact == other.getFecha_fact()
            && ((this.tipo_eecc_pdf==null && other.getTipo_eecc_pdf()==null) || 
             (this.tipo_eecc_pdf!=null &&
              this.tipo_eecc_pdf.equals(other.getTipo_eecc_pdf())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        DatosEstadosDeCuentaPDF history = (DatosEstadosDeCuentaPDF) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        DatosEstadosDeCuentaPDF history = (DatosEstadosDeCuentaPDF) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getNum_cta_tar() != null) {
            _hashCode += getNum_cta_tar().hashCode();
        }
        _hashCode += getFecha_fact();
        if (getTipo_eecc_pdf() != null) {
            _hashCode += getTipo_eecc_pdf().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
