<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV101003Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />
<jsp:useBean id="ventas" class="datapro.eibs.beans.EPV107001Message"  scope="session" />

<title>Plataforma de Venta</title>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1"> 

function validar(forma){
	if (forma.customer_number.value==""){
		alert("Debe introducir un Numero de cliente para Continuar.!");
		return false;
	}
	return true;
}
</script>
</head>
<body>

<%
	if (!error.getERRNUM().equals("0")) {
		out.println("<script type=\"text/javascript\">");
		error.setERRNUM("0");
		out.println("showErrors()");
		out.println("</script>");
	}
%>

<h3 align="center">Reporte de Ventas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_selection_enter.jsp,JSEPV1070"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1070">
<input type="hidden" name="SCREEN" value="101">
<br>

<% int row = 0;%>
<h4>Seleccion </h4>

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Banco/Oficina :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="ventas" property="E01SELBNK" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BANK %>"  />
 	 			<eibsinput:help name="ventas" property="E01SELBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" 
 	 				fn_param_one="E01SELBRN" fn_param_two="document.forms[0].E01SELBNK.value" fn_param_three=" " />
             </td>
             <td nowrap align="right" width="20%"> Convenio :</td>
             <td nowrap align="left" width="30%">
	          <input type="text" name="E01SELCNV" size="5" maxlength="4" value="<%= ventas.getE01SELCNV().trim()%>">
	            <a href="javascript:GetCodeDescDeal('E01SELCNV','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absbottom" border="0" ></a> 
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Supervisor :</td>
             <td nowrap align="left" width="30%">
	            <eibsinput:cnofc name="ventas" property="E01SELSUP" required="false" flag="CA" fn_code="E01SELSUP" />
             </td>
             <td nowrap align="right" width="20%"> Vendedor :</td>
             <td nowrap align="left" width="30%">
	            <eibsinput:cnofc name="ventas" property="E01SELVEN" required="false" flag="CA" fn_code="E01SELVEN" />
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Fecha Inicial :</td>
             <td nowrap align="left" width="30%">
    	        <eibsinput:date name="ventas" fn_year="E01SELFIY" fn_month="E01SELFIM" fn_day="E01SELFID" />
             </td>
             <td nowrap align="right" width="20%"> Fecha Final :</td>
             <td nowrap align="left" width="30%">
    	        <eibsinput:date name="ventas" fn_year="E01SELFFY" fn_month="E01SELFFM" fn_day="E01SELFFD" />
             </td>
         </tr>         
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%">Muestra Detalle : </td>
             <td nowrap align="left" width="30%">
 			  <input type="checkbox" name="E01SELDTL" value="Y" <%if (ventas.getE01SELDTL().equals("Y")){out.print(" checked");}; %>/>
            </td>
             <td nowrap align="right" width="20%"> Muestra Totales :</td>
             <td nowrap align="left" width="30%">
 			  <input type="checkbox" name="E01SELTOT" value="Y" <%if (ventas.getE01SELTOT().equals("Y")){out.print(" checked");}; %>/>
             </td>
         </tr>         
        </table>
      </td>
    </tr>
  </table>

 <div align="center"> 
     <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
 </div>

</form>
</body>
</html>
