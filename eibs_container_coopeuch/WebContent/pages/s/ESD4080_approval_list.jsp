<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="datapro.eibs.sockets.MessageRecord"%>
<%@page import="datapro.eibs.beans.ESD408001Message"%>
<%@page import="com.ibm.as400.access.User"%>
<html>

<HEAD>
<TITLE>
Approval Plan List
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<jsp:useBean id= "ESD408001List" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "ESD408001ListAprob" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "ESD4080" class= "datapro.eibs.beans.ESD408001Message"  scope="session" />
<jsp:useBean id= "rut" class= "java.lang.String"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
 

<script language="javascript">
	var reason = '';
  	var accOfac = '';
  	var auxiliar;
	
	function goAction(op) {		
		if(document.forms[0].CANTIDAD.value != "1"){
			if (document.forms[0].IDCLIENTEFILA.value != ""){			
				document.forms[0].IDSOCIOSELEC.value = document.forms[0].IDSOCIO[document.forms[0].IDCLIENTEFILA.value].value; 
				document.forms[0].RUTSOCIOSELEC.value = document.forms[0].RUTSOCIO[document.forms[0].IDCLIENTEFILA.value].value;
				if (op == "R" || op == "D"){
					document.forms[0].SCREEN.value="3";
					document.forms[0].submit();
				}else{
					document.forms[0].SCREEN.value="2";
					document.forms[0].submit();
				}
			}else
				alert('Debe seleccionar un plan aprobar');
		}else{
			if (document.forms[0].IDCLIENTEFILA.value != ""){
				document.forms[0].IDSOCIOSELEC.value = document.forms[0].IDSOCIO.value;
				document.forms[0].RUTSOCIOSELEC.value = document.forms[0].RUTSOCIO.value;
				if (op == "R" || op == "D"){
					document.forms[0].SCREEN.value="3";
					document.forms[0].submit();
				}else{
					document.forms[0].SCREEN.value="3";
					document.forms[0].submit();
				}
			}
			else
				alert('Debe seleccionar un plan aprobar');
		}
		return;
 	}
 
 

 	function goExit(){
  		window.location.href="<%=request.getContextPath()%>/pages/background.jsp";
  	}    

	function guardarID(obj){
		document.forms[0].IDCLIENTEFILA.value = obj.value;
	}
</script>
</HEAD>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<BODY onload="MM_preloadImages('<%=request.getContextPath()%>/images/s/approve_over.gif','<%=request.getContextPath()%>/images/s/reject_over.gif')">
<h3 align="center">Aprobaci&oacute;n de Planes<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="approval_list.jsp,ESD0100"> 
</h3>
<hr size="4">
  
  
<FORM Method="post" Action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4080">
	<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
	<INPUT TYPE=HIDDEN NAME="action" VALUE="A">
	<INPUT TYPE=HIDDEN NAME="reason" VALUE="">
	<INPUT TYPE=HIDDEN NAME="totalRow" VALUE="0">
	<INPUT TYPE=HIDDEN NAME="CHECKENC" VALUE=" ">
	<input type="hidden" name="IDCLIENTEFILA" size="2" maxlength="2">
	<input type="hidden" name="IDPLANSELEC" size="2" maxlength="2">
	<input type="hidden" name="IDSOCIOSELEC" size="20" maxlength="20">
	<input type="hidden" name="RUTSOCIOSELEC" size="20" maxlength="20">
	<input type="hidden" name="PLANid" size="2" maxlength="2" value="0">
	<input type="HIDDEN" name="seleccion" size="100" maxlength="100" value="">
   	<TABLE class="tbenter">
    	<TR>
      		<TD class=TDBKG> 
        <div align="center"><a href="javascript:goAction('A')" id="linkApproval"><b>Aprobar</b></a></div>
      </TD>
      <TD class=TDBKG> 
        <div align="center"><a href="javascript:goAction('R')" id="linkReject"><b>Rechazar</b></a></div>
      </TD>
      <TD class=TDBKG> 
        <div align="center"><a href="javascript:goAction('D')" id="linkDelete"><b>Eliminar</b></a></div>
      </TD>
      <TD class=TDBKG> 
        <div align="center"><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
      </TD>
    </TR>
  </TABLE>
  
 <TABLE  id="mainTable" class="tableinfo">
 	<TR> 
   		<TD NOWRAP valign="top" width="100%"> 
    		<TABLE id="headTable" >
    			<TR id="trdark">  
      				<TH ALIGN=CENTER NOWRAP width="5%">&nbsp;</TH>
            		<TH ALIGN=CENTER NOWRAP width="15%">Id Plan</TH>
            		<TH ALIGN=CENTER NOWRAP width="10%">Id Socio</TH>
            		<TH ALIGN=CENTER NOWRAP width="40%">Nombre del Socio</TH>
            		<TH ALIGN=CENTER NOWRAP width="15%">Cuenta Vista o Coopeuch</TH>
            		<TH ALIGN=CENTER NOWRAP width="15%">Libreta de Ahorro</TH>
            		<TH ALIGN=CENTER NOWRAP width="15%">Tarjeta de Cr&eacute;dito</TH>
    			</TR>
    			<% 
    			///////////////////////////////////////
    			int row = 0;
                int i=1;
                ESD408001ListAprob.initRow();
                while (ESD408001ListAprob.getNextRow()) {  
                	ESD408001Message msgList = (ESD408001Message) ESD408001ListAprob.getRecord();%>                	
                	<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>" >
                		<td NOWRAP align="center" width="5%">
                			<input type="radio" name="ROW" value="<%= ESD408001ListAprob.getCurrentRow()%>" onclick="guardarID(this);">     						
     						<input type="hidden" name="IDPLAN" size="2" maxlength="10" value="<%= msgList.getE01PRR().trim()%>">
  							<input type="hidden" name="IDSOCIO" size="2" maxlength="1" value="<%= msgList.getE01CUN().trim()%>">
  							<input type="hidden" name="RUTSOCIO" size="2" maxlength="1" value="<%= msgList.getE01RUT().trim()%>">
     					</td>
     					<td NOWRAP  align=center> <%= msgList.getE01ACMAX1() %> </td>
     					<td NOWRAP  align=center> <%= msgList.getE01CUN() %> </td>
     					<td NOWRAP  align=center> <%= msgList.getE01NOM() %> </td>
     					<% if (msgList.getE01ACMAX4().equals("1")) {%>
     						<td align="center" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" ></td>
     					<%} else { %>
     						<td align="center" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" ></td>
     					<% } %> 	
     					<% if (msgList.getE01ACMAX5().equals("1")) {%>
     						<td nowrap name="planSocio" property="E01PSPACV" width="12%" align="center" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" ></td>
     					<%} else { %>
     						<td nowrap name="planSocio" property="E01PSPACV" width="12%" align="center" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" ></td>
     					<% } %> 	
     					<% if (msgList.getE01ACMAX6().equals("1")) {%>
     						<td nowrap name="planSocio" property="E01PSPACV" width="12%" align="center" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" ></td>
     					<%} else { %>
     						<td nowrap name="planSocio" property="E01PSPACV" width="12%" align="center" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" ></td>
     					<% } %> 	
     					
     					
     					<%
             			i++;
                	%>
                	</tr>
                	<%         
                }
    			%>
       		</TABLE>
       		<input type="hidden" name="CANTIDAD" size="2" maxlength="10" value="<%= row%>">
   		</TD>
  	</TR>	
</TABLE>


</FORM>

</BODY>
</HTML>
