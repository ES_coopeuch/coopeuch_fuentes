<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="document" class="datapro.eibs.beans.EPV103501Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript">

 function goQuery(screen) {
 	var dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1060?SCREEN="+screen+"&row="+ document.forms[0].row.value+"&E01PVMNUM="+document.forms[0].E01PVMNUM.value; 	
 	CenterWindow(dir,850,650,2);
 }

 function goDeudaConsolidada() {
 	var dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECIF215?SCREEN=4&opt=20&VS=S&E01CUN="+ document.forms[0].E01PVMCUN.value;
 	CenterWindow(dir,850,650,2);
 }

 function goConsultaSinacofi() {
 	var dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1035?SCREEN=500&E01CUSIDE="+ document.forms[0].E01CUSIDE.value+"&E01CUSIDS="+ document.forms[0].E01CUSIDS.value;
 	CenterWindow(dir,950,650,2);
 } 
 function enviar(opt) {
	  document.forms[0].E01FLGOPE.value = opt;
	  if (opt=='R'){
	  	if (document.forms[0].E01CODREC.value==''){
	  		alert('Debe colocar Causal de Rechazo...!!!');
	  		document.forms[0].E01CODREC.focus();
	  		return;
	  	}
	  }
	  document.forms[0].submit();
 }
 
 function limitText(limitField, limitNum) {

	if (limitField.value.length > limitNum + 1) { 
		if (document.forms[0].LAN.value == 'S') {
			alert('L�mite de texto excedido');
		} else {
			alert('Your input has been truncated');
		}	
	}	
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	}
}


function UpdateMotivo(check) 
{
   if(check.checked==0)
   {
	InpSinExc = document.getElementById('E01SINEXC');
	InpSinExc.value = "N";  
	
	fila = document.getElementById('listexep');
	fila.style.display = "";  
	
	limpiarMotivoAll();
	
   } 
	else 
   {
	InpSinExc = document.getElementById('E01SINEXC');
	InpSinExc.value = "S";  
	
	fila = document.getElementById('listexep');
	fila.style.display = "none";  
   } 
}  


function limpiarMotivo(cod, descod) 
{

	codmot = document.getElementById(cod);
	desmot = document.getElementById(descod);
	if(codmot.value=="0000" || codmot.value=="0060" )
	{
		codmot.value="";
		desmot.value="";
	}	
} 

function limpiarMotivoAll(cod, descod) 
{
	codmot = document.getElementById('E01EXEP01');
	desmot = document.getElementById('E01EXED01');
    codmot.value="";
    desmot.value="";

	codmot = document.getElementById('E01EXEP02');
	desmot = document.getElementById('E01EXED02');
    codmot.value="";
    desmot.value="";

	codmot = document.getElementById('E01EXEP03');
	desmot = document.getElementById('E01EXED03');
    codmot.value="";
    desmot.value="";

	codmot = document.getElementById('E01EXEP04');
	desmot = document.getElementById('E01EXED04');
    codmot.value="";
    desmot.value="";

	codmot = document.getElementById('E01EXEP05');
	desmot = document.getElementById('E01EXED05');
    codmot.value="";
    desmot.value="";
} 


 </script>
</head>


<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center"> Plataforma de Ventas - Visado Senior de Documentos<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_documentos_senior.jsp,JSEPV1035"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1035">
<input type="hidden" name="SCREEN" value="600">
<input type="hidden" name="LAN" value="S"> 
<input type=HIDDEN name="E01FLGOPE"  value="<%= document.getE01FLGOPE().trim()%>">
<input type=HIDDEN name="E01PVMBNK"  value="<%= document.getE01PVMBNK().trim()%>">
<input type="hidden" name="row" value="<%=request.getAttribute("row")%>"> 
<input type=HIDDEN name="E01CCNOFC"  value="<%= document.getE01CCNOFC().trim()%>">

 <% int row = 0;%>
 
 <h4>Datos Solicitud </h4> 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="30%">	  			
	  			<eibsinput:text name="document" property="E01PVMNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="30%">
				<eibsinput:date name="document" fn_year="E01PVMODY" fn_month="E01PVMODM" fn_day="E01PVMODD" readonly="true"/>             	  			
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" > Agencia :</td>
             <td nowrap align="left">
				<eibsinput:text name="document" property="E01PVMBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true" />	  			
             </td>
             <td nowrap align="right"> Ejecutivo :  </td>
             <td nowrap align="left" >    	        
             	<eibsinput:text name="document" property="E01PVMOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>" readonly="true" />
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" >Estatus :</td>
             <td nowrap align="left">
				<eibsinput:text name="document" property="E01PVMSTS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />
             	<eibsinput:text name="document" property="E01DSCSTS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" readonly="true"/>    	                     					  			
             </td>
             <td nowrap align="right"></td>
             <td nowrap align="left" >    	        
             </td>
         </tr>                
        </table>
      </td>
    </tr>
  </table>

  <table  class="tbenter" width="100%">
  	<tr>
		<table class="tbenter" width="90%">
			<tr>	
               <td nowrap align="center" width="20%" class="tdbkg" ><a href="javascript:goQuery('250')">Solicitud</a> </td>
               <td nowrap align="center" width="20%" class="tdbkg" ><a href="javascript:goQuery('350')">Evaluacion</a> </td>
               <td nowrap align="center" width="20%" class="tdbkg" ><a href="javascript:goQuery('450')">Visado</a> </td>
			   <td nowrap align="center" width="20%" class="tdbkg" ><a href="javascript:goDeudaConsolidada()">Deuda Consolidada</a> </td>
 			   <td nowrap align="center" width="20%" class="tdbkg" ><a href="javascript:goConsultaSinacofi()">Consulta Bureau</a> </td>			   
			</tr>
			<tr>	
               <td nowrap align="center" width="20%" >&nbsp;</td>
               <td nowrap align="center" width="20%" >&nbsp;</td>
               <td nowrap align="center" width="20%" >&nbsp;</td>
			   <td nowrap align="center" width="20%" ><%="Y".equals(document.getH01FLGWK2())?"<font size=\"2\" color=\"red\"><b>Deuda Modificada</b></font>":""%></td>
 			   <td nowrap align="center" width="20%" ><%="Y".equals(document.getH01FLGWK1())?"<font size=\"2\" color=\"red\"><b>Bureau Modificado</b></font>":""%></td>	   
			</tr>			
		</table>  		
  	</tr>
  </table>
  
<h4>Datos Solicitante </h4> 
 <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="30%">
				<eibsinput:text name="document" property="E01PVMCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" />	  			
             </td>
             <td nowrap align="right" width="20%"> RUT :</td>
             <td nowrap align="left" width="30%">
             	<eibsinput:text name="document" property="E01CUSIDE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>    	        
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> Nombre :</td>
             <td nowrap align="left" >
	  			<eibsinput:text name="document" property="E01CUSNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
             <td nowrap align="right" width="20%"> Serie :</td>
             <td nowrap align="left" width="30%">
             	<eibsinput:text name="document" property="E01CUSIDS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>    	        
             </td>             
         </tr>  
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> Direcci�n :</td>
             <td nowrap align="left" colspan="3">
	  			<eibsinput:text name="document" property="E01CUSAD1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" size="60" readonly="true"/>
             </td>
         </tr>         
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> &nbsp;</td>
             <td nowrap align="left" colspan="3">
	  			<eibsinput:text name="document" property="E01CUSAD2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" size="60" readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> &nbsp;</td>
             <td nowrap align="left" colspan="3">
	  			<eibsinput:text name="document" property="E01CUSAD3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" size="60" readonly="true"/>
             </td>
         </tr>    
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> &nbsp;</td>
             <td nowrap align="left" colspan="3">
	  			<eibsinput:text name="document" property="E01CUSAD4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" size="60" readonly="true"/>
             </td>
         </tr>                 
        </table>
      </td>
    </tr>
  </table>

<h4>Datos Empleador </h4> 
 <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> RUT :</td>
             <td nowrap align="left" width="20%">
             	<eibsinput:text name="document" property="E01EMPIDE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>    	        
             </td>
             <td nowrap align="right" width="20%"> Nombre :</td>
             <td nowrap align="left" width="40%">
	  			<eibsinput:text name="document" property="E01EMPNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> Direcci�n :</td>
             <td nowrap align="left" colspan="3">
	  			<eibsinput:text name="document" property="E01EMPAD1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" size="60" readonly="true"/>
             </td>
         </tr>         
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> &nbsp;</td>
             <td nowrap align="left" colspan="3">
	  			<eibsinput:text name="document" property="E01EMPAD2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" size="60" readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> &nbsp;</td>
             <td nowrap align="left" colspan="3">
	  			<eibsinput:text name="document" property="E01EMPAD3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" size="60" readonly="true"/>
             </td>
         </tr>    
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> &nbsp;</td>
             <td nowrap align="left" colspan="3">
	  			<eibsinput:text name="document" property="E01EMPAD4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>"  size="60" readonly="true"/>
             </td>
         </tr>                 
        </table>
      </td>
    </tr>
  </table>

 <% row = 0;%>
<%if(document.getE01LNTYPG().equals("T")){%> 
  <h4>Detalle Tarjeta de Credito </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="document" property="E01LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="document" property="E01DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Direccion :</div>
            </td>
            <td nowrap width="30%">
              <input type="text" name="E01TCCMLA" size="3" maxlength="2" value="<%= document.getE01TCCMLA().trim()%>" readonly >
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Local :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01TCCUMB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Extranjera :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01TCCUME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Dia de Pago :</div>
            </td>
            <td nowrap width="30%">             
				<select name="E01TCDYPG" disabled>
					<option></option>
					<option value="3" <% if (document.getE01TCDYPG().equals("3")) out.print("selected");%>>3</option>
					<option value="23" <% if (document.getE01TCDYPG().equals("23")) out.print("selected");%>>23</option>
				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Porcentaje de Pago :</div>
            </td>
            <td nowrap width="30%">            
				<select name="E01TCPRPG" disabled>
					<option></option>
					<option value="5" <% if (document.getE01TCPRPG().equals("5")) out.print("selected");%>>El 5%</option>
					<option value="100" <% if (document.getE01TCPRPG().equals("100")) out.print("selected");%>>El 100%</option>
				</select>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">
				<select name="E01TCMEPG" disabled>
					<option></option>
					<option value="1" <% if (document.getE01TCMEPG().equals("1")) out.print("selected");%>>Con Cuenta</option>
					<option value="2" <% if (document.getE01TCMEPG().equals("2")) out.print("selected");%>>Sin Cuenta</option>
				</select>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:text name="document" property="E01TCACPG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
            </td>
          </tr>  

        </table>
      </td>
    </tr>
  </table>

<%} else {%> 
  <h4>Detalle del Credito </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Tipo de Producto :</div>
            </td>
            <td nowrap width="30%" >
            	<eibsinput:text name="document" property="E01LNTYPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>	
            	<eibsinput:text name="document" property="E01DSTYPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Termino del Contrato :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNTERM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" readonly="true" /> 
	            <input type="text" readonly name="E01LNTERC" size="10" 
				  value="<% if (document.getE01LNTERC().equals("D")) out.print("D&iacute;a(s)");
							else if (document.getE01LNTERC().equals("M")) out.print("Mes(es)");
							else if (document.getE01LNTERC().equals("Y")) out.print("A&ntilde;o(s)");
							else out.print("");%>" >
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="document" property="E01LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="document" property="E01DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Valor de la Cuota :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01LNCUAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
          </tr>                 
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Monto del Prestamo :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01LNOAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Monto Liquido :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01LNNAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
          </tr>  
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Primer Pago :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:date name="document" fn_year="E01LNPXPY" fn_month="E01LNPXPM" fn_day="E01LNPXPD" readonly="true" />
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
            </td>
            <td nowrap width="30%">
            	&nbsp; 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">            
				<select name="E01LNPVIA" <%="disabled"%>>
					<option value=""
						<% if (document.getE01LNPVIA().equals("")) out.print("selected");%>>Caja</option>
					<option value="1"
						<% if (document.getE01LNPVIA().equals("1")) out.print("selected");%>>PAC</option>							            
					<option value="2"
						<% if (document.getE01LNPVIA().equals("2")) out.print("selected");%>>Convenio</option>
					<option value="4"
						<% if (document.getE01LNPVIA().equals("4")) out.print("selected");%>>PAC/Multibanco</option>				
				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:text name="document" property="E01LNPACC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
            </td>
          </tr>
          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Promociones :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01DSPROM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tasa Promoci�n:</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNTSPO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/>
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Descuentos :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01DSDESC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">% Descuento :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNTSDS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
          </tr>  
          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Codigo Convenio :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNCONV" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right"> % descuento del convenio : </div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNDRAT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Tasa de Interes :</div>
            </td>
            <td nowrap width="30%">
            	 <eibsinput:text name="document" property="E01LNRATE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
            </td>
            <td nowrap width="30%">
            	&nbsp;
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Holgura:</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01PVMOLA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
            </td>
            <td nowrap width="30%">
				&nbsp;            	
            </td>
          </tr>   
          
        </table>
      </td>
    </tr>
  </table>

<%} %>     
  
 <h4>Otros Valores </h4> 
 <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Tienda Virtual :</td>
             <td nowrap align="left" width="30%">
             	<eibsinput:text name="document" property="E01TVIAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL %>" readonly="true"/>    	        
             </td>
             <td nowrap align="right" width="20%"> Reliquidaciones :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="document" property="E01PRLAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL %>" readonly="true"/>
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Seguros :</td>
             <td nowrap align="left" width="30%">
             	<eibsinput:text name="document" property="E01SGRAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL %>" readonly="true"/>    	        
             </td>
             <td nowrap align="right" width="20%"> Tarjeta Alianza :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="document" property="E01TRAAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL %>" readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cheques a Terceros :</td>
             <td nowrap align="left" width="30%">
             	<eibsinput:text name="document" property="E01CTRAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL %>" readonly="true"/>    	        
             </td>
             <td nowrap align="right" width="20%"> Otros Cargos :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="document" property="E01OTHAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL %>" readonly="true"/>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
 <h4>Resultado Evaluaci�n Motor</h4>
  <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="30%">Decisi�n : </td>
             <td nowrap align="left" width="70%">
             	<eibsinput:text name="document" property="E01PVMEC1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true"/>
             	<eibsinput:text name="document" property="E01DSCDE1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR%>" size="80" readonly="true"/>    	                     
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" > Observaci�n :</td>
             <td nowrap align="left">
             <TEXTAREA name="E01REMREC" rows="10" cols="100" readonly="readonly"><%
	          	for (int i = 1; i <= 16; i++) {
	          		String fila = (i>9)?""+i:"0"+i;
					String desc = document.getField("E01DSCE"+fila).getString();
					if (!"".equals(desc)){
						out.print(desc+" \n");
					}				
				}
			   %></TEXTAREA>
             </td>

          </tr>       
        </table>
      </td>
    </tr>
  </table>  

 <h4>Causal </h4> 
 <% row = 0;%>
 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="30%"> Causal :</td>
             <td nowrap align="left" width="70%">	  			
				 <eibsinput:cnofc name="document" property="E01CODREC" required="false" flag="RV" fn_code="E01CODREC" fn_description="E01DSCREC"/>
				<eibsinput:text property="E01DSCREC" name="document" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="false" readonly="true"/>				              
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" > Comentarios :</td>
             <td nowrap align="left">
					<TEXTAREA name="E01REMREC" rows="3" cols="100" 	            
					onKeyDown="limitText(this.form.E01REMREC,500);"
					onKeyUp="limitText(this.form.E01REMREC,500);"
					><%=document.getE01REMREC().trim()%></TEXTAREA>			
             </td>

          </tr>
        </table>
      </td>
    </tr>
  </table>
 
   <h4>Excepciones</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" align="right">
		       <input type="checkbox" name="chk1" value="" onClick="UpdateMotivo(this)" <% if (document.getE01SINEXC().trim().equals("S")) out.print(" checked");%>>
			   <input type="hidden"  id="E01SINEXC" name="E01SINEXC" value="<% if (document.getE01SINEXC().trim().equals("S")) out.print("S"); else out.print("N"); %>">
        	</td>
            <td nowrap width="30%" align="right">
				<div align="left"><b>Sin Excepciones</b/></div>        	</td>
  		    <td nowrap width="50%" align="left">       	 

        	</td>
          </tr> 
			
        </table>
        </td>
    </tr>
    
    <tr id='listexep' <% if (document.getE01SINEXC().trim().equals("S")) out.print(" style=\"display: none\"");%>> 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
            </td>
            <td nowrap width="30%"> 
              <div align="right">Motivo de Excepci&oacute;n :</div>
            </td>
            
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01EXEP01" size="4" maxlength="3" value="<%= document.getE01EXEP01().trim()%>"  onfocus="limpiarMotivo('E01EXEP01','E01EXED01' );"  readonly style="text-align:right;">
                <input type="text" name="E01EXED01" size="50" maxlength="3" value="<%= document.getE01EXED01().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNOFC('E01EXEP01','E01EXED01','<%=document.getE01CCNOFC() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
            </td>
            <td nowrap> 
              <div align="right">Motivo de Excepci&oacute;n :</div>
            </td>
            
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01EXEP02" size="4" maxlength="3" value="<%= document.getE01EXEP02().trim()%>" onfocus="limpiarMotivo('E01EXEP02','E01EXED02' );"  readonly style="text-align:right;">
                <input type="text" name="E01EXED02" size="50" maxlength="3" value="<%= document.getE01EXED02().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNOFC('E01EXEP02','E01EXED02','<%=document.getE01CCNOFC() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
            </td>
            <td nowrap> 
              <div align="right">Motivo de Excepci&oacute;n :</div>
            </td>
            
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01EXEP03" size="4" maxlength="3" value="<%= document.getE01EXEP03().trim()%>" onfocus="limpiarMotivo('E01EXEP03','E01EXED03' );"   readonly style="text-align:right;">
                <input type="text" name="E01EXED03" size="50" maxlength="3" value="<%= document.getE01EXED03().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNOFC('E01EXEP03','E01EXED03','<%=document.getE01CCNOFC() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
            </td>
            <td nowrap> 
              <div align="right">Motivo de Excepci&oacute;n :</div>
            </td>
            
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01EXEP04" size="4" maxlength="3" value="<%= document.getE01EXEP04().trim()%>" onfocus="limpiarMotivo('E01EXEP04','E01EXED04' );" readonly style="text-align:right;">
                <input type="text" name="E01EXED04" size="50" maxlength="3" value="<%= document.getE01EXED04().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNOFC('E01EXEP04','E01EXED04','<%=document.getE01CCNOFC() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
            </td>
            <td nowrap> 
              <div align="right">Motivo de Excepci&oacute;n :</div>
            </td>
            
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01EXEP05" size="4" maxlength="3" value="<%= document.getE01EXEP05().trim()%>" onfocus="limpiarMotivo('E01EXEP05','E01EXED05' );"  readonly style="text-align:right;">
                <input type="text" name="E01EXED05" size="50" maxlength="3" value="<%= document.getE01EXED05().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNOFC('E01EXEP05','E01EXED05','<%=document.getE01CCNOFC() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 
        </table>
        </td>
    </tr>
  </table>    
 
 
 
 
 
 
 
  <br>
   <div align="center">
	   <input id="EIBSBTN" type="button"" name="Rechazar" value="Rechazar" onclick="enviar('R');">
	   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
	   <input id="EIBSBTN" type="button"" name="Aprobar" value="Aprobar" onclick="enviar('A');">	                    
   </div>
 
</form>

</body>
</html>
