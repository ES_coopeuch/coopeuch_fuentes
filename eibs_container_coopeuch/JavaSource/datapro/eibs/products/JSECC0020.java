package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Creado     por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  22/11/2012                                                                                      **/
/**  Objetivo                :  Servicio para Mantenimiento de Solicitudes de mantencion de Tarjetas de Credito                 **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/
import java.beans.Beans;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.*;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSECC0020 extends datapro.eibs.master.SuperServlet {

 	// credit card 
	
	protected static final int R_ENTER_SEARCH             = 200;
	protected static final int R_ENTER_LIST               = 300;
	protected static final int R_ENTER_MAINT              = 400;
	protected static final int A_MAINTENANCE              = 500;
	protected String LangPath = "S";

	/**
	 *  
	 */
	public JSECC0020() {
		super();
	}
	/**
	 * This method was created by  
	 */
	public void destroy() {

		flexLog("free resources used by JSECC0020");

	}
	/**
	 * This method was created by  
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/**
	 * Ingreso dato de busqueda del cliente de tarjeta de Credito.
	 */
	protected void procReqTCEnterMaint(
			ESS0030DSMessage user, 
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession ses)
	throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		
		try {
		
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos(); 
			userPO.setRedirect("/servlet/datapro.eibs.products.JSECC0020?SCREEN=300");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);
		
			} catch (Exception ex) {
			flexLog("Error: " + ex); 
			}
		
		try {
			flexLog("About to call Page: " + LangPath + "ECC0020_cc_enter_maint_posventa.jsp");
			callPage(LangPath + "ECC0020_cc_enter_maint_posventa.jsp", req, res);
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		}	
	/**
	 * Armo y envio lista de tarjetas del cliente seleccionado
	 */
	protected void procReqTCCardsList(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECC004001Message msgList = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		userPO.setPurpose("INQUIRY");

		// Send Initial data
		try {
			msgList = (ECC004001Message) mc.getMessageRecord("ECC004001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ECC0040");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			msgList.setH01OPECOD("0005");
			msgList.setE01CCRNXN("");
			msgList.setE01CCRCID(req.getParameter("E01CCRCID"));	
		    msgList.send();
		    msgList.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError =
						(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
							getClass().getClassLoader(),
							"datapro.eibs.beans.ELEERRMessage");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else {
				flexLog("Message " + newmessage.getFormatName() + " received.");				
			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e + newmessage);
			throw new RuntimeException("Socket Communication Error Receiving");
		}

		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECC004001")) {

				JBObjList beanList = new JBObjList();

				boolean firstTime = true;
				String marker = "";
				String chk = "";
					while (true) {

					msgList = (ECC004001Message) newmessage;
					marker = msgList.getH01FLGMAS();
					flexLog("Mensaje recibido..." + msgList);
					if (firstTime) {
						userPO.setCusNum(msgList.getE01CCMCUN());
						userPO.setCusName(msgList.getE01CCMNME());
						userPO.setHeader1(msgList.getE01CCRCID());
						userPO.setCurrency(msgList.getE01CCMCCY());
						userPO.setOfficer(msgList.getE01CCMOFC());
						userPO.setProdCode(msgList.getE01CCMPRO());
						userPO.setIdentifier(msgList.getE01CCRCID());
						userPO.setAccNum(msgList.getE01CCMACC());
						userPO.setHeader20(msgList.getD01CCMPRO());	
						userPO.setHeader21(msgList.getE01CCRNXN());	
						firstTime = false;
						chk = "checked";

					} else {
						chk = "";
					}

					if (marker.equals("*")) {
						beanList.setShowNext(false);
						break;
					} else {
						beanList.addRow(msgList);

						if (marker.equals("+")) {
							beanList.setShowNext(true);

							break;
						}
					}
					newmessage = mc.receiveMessage();
				}

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("ECC0040Help", beanList);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog(
							"About to call Page: " + LangPath + "ECC0020_cc_cards_list.jsp");
						callPage(LangPath + "ECC0020_cc_cards_list.jsp", req, res);					
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				
				} else {
					try {
						flexLog("About to call Page: " + LangPath + "ECC0020_cc_enter_maint_posventa.jsp");
						callPage(LangPath + "ECC0020_cc_enter_maint_posventa.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}				}				

			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Data Receiving");
		}

	}
	
	protected void procActionPosMaint(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			UserPos userPO = null;

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			String inptOPT = " ";
			inptOPT = req.getParameter("OPT");

			ECC002001Message ccPvta = null;	
			ECC004001Message msgDoc = null;	
			userPO.setPurpose("MAINTENANCE");
			userPO.setOption(inptOPT);
			// Receive Data
			try {
				ccPvta = (ECC002001Message) mc.getMessageRecord("ECC002001");					
				JBObjList bl = (JBObjList) ses.getAttribute("ECC0040Help");
				int idx = req.getParameter("CURRCODE")==null?0:req.getParameter("CURRCODE").equals("")?0:Integer.parseInt(req.getParameter("CURRCODE"));								
				bl.setCurrentRow(idx);
		
				msgDoc = (ECC004001Message) bl.getRecord();				
				userPO.setCusNum(msgDoc.getE01CCMCUN());
				userPO.setCusName(msgDoc.getE01CCMNME());
				userPO.setCurrency(msgDoc.getE01CCMCCY());
				userPO.setOfficer(msgDoc.getE01CCMOFC());
				userPO.setProdCode(msgDoc.getE01CCMPRO());
				userPO.setIdentifier(msgDoc.getE01CCRCID());
				userPO.setAccNum(msgDoc.getE01CCMACC());
				userPO.setHeader20(msgDoc.getD01CCMPRO());	
				userPO.setHeader21(msgDoc.getE01CCRNXN());
				userPO.setHeader22(msgDoc.getE01CCRNUM());	
				userPO.setHeader23(msgDoc.getE01CCRTPI());
                userPO.setHeader9(msgDoc.getE01CCRTDC());				
				flexLog("Putting java beans into the session");
				ses.setAttribute("userPO", userPO);			
		
				try {
					  if (userPO.getHeader9().equals("C"))
					  {
						  ses.setAttribute("ccNew", msgDoc);
						  ses.setAttribute("ccPvta", ccPvta);	
						  flexLog(
								  "About to call Page: "
								  + LangPath
								  + "ECC0020_cc_maint_posventa.jsp");
						  callPage(
								  LangPath + "ECC0020_cc_maint_posventa.jsp",
								  req,
								  res);
					  }
					  else
					  {
						  ses.setAttribute("dcNew", msgDoc);
						  ses.setAttribute("dcPvta", ccPvta);	
						  flexLog(
								  "About to call Page: "
								  + LangPath
								  + "ECC0020_dc_maint_posventa.jsp");
						  callPage(
								  LangPath + "ECC0020_dc_maint_posventa.jsp",
								  req,
								  res);  
					  }
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
		
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}
}
	
	protected void procActionMaintenance(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			MessageRecord newmessage = null;
			ECC002001Message ccPvta = null;
			ECC004001Message ccNew = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			boolean IsNotError = false;

			try {
				msgError = new ELEERRMessage();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			// Send Initial data
			try {
				flexLog("Send Initial Data");
			    if (userPO.getHeader9().equals("C"))
			    {
			    	ccNew = (ECC004001Message) ses.getAttribute("ccNew");
			    	ccPvta = (ECC002001Message) ses.getAttribute("ccPvta");	
			    }
			    else
			    {
					ccNew = (ECC004001Message) ses.getAttribute("dcNew");
					ccPvta = (ECC002001Message) ses.getAttribute("dcPvta");	
			    }	
				userPO.setCusNum(ccNew.getE01CCMCUN());
				userPO.setCusName(ccNew.getE01CCMNME());
				userPO.setCurrency(ccNew.getE01CCMCCY());
				userPO.setOfficer(ccNew.getE01CCMOFC());
				userPO.setProdCode(ccNew.getE01CCMPRO());
				userPO.setIdentifier(ccNew.getE01CCRCID());
				userPO.setAccNum(ccNew.getE01CCMACC());
				userPO.setHeader20(ccNew.getD01CCMPRO());	
				userPO.setHeader21(ccNew.getE01CCRNXN());
				userPO.setHeader22(ccNew.getE01CCRNUM());	
				userPO.setHeader23(ccNew.getE01CCRTPI());
                userPO.setHeader9(ccNew.getE01CCRTDC());
                
				ccPvta.setH01USERID(user.getH01USR());
				ccPvta.setH01PROGRM("ECC0020");
				ccPvta.setH01TIMSYS(getTimeStamp());
				ccPvta.setH01OPECOD("0005");
				ccPvta.setH01SCRCOD(userPO.getOption());				
				ccPvta.setE01SPVCID(userPO.getIdentifier());
				ccPvta.setE01SPVNXN(userPO.getHeader21());
				ccPvta.setE01SPVNUM(userPO.getHeader22());	
				ccPvta.setE01SPVTPI(userPO.getHeader23());
                ccPvta.setE01SPVTDC(userPO.getHeader9());
                ccPvta.setE01SPVACC(userPO.getAccNum());                
				// all the fields here
				java.util.Enumeration enu = ccPvta.fieldEnumeration();
				MessageField field = null;
				String value = null;
				while (enu.hasMoreElements()) {
					field = (MessageField) enu.nextElement();
					try {
						value = req.getParameter(field.getTag()).toUpperCase();
						if (value != null) {
							field.setString(value);
						}
					} catch (Exception e) {
					}
				}

				// msgCD.send();
				flexLog("mensaje enviado..." + ccPvta);
				mc.sendMessage(ccPvta);
				ccPvta.destroy();				
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Error Message
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ECC002001")) {
					try {
						ccPvta = new ECC002001Message();
						flexLog("ECC002001 Message Received");
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					ccPvta = (ECC002001Message) newmessage;

					userPO.setPurpose("MAINTENANCE");
//					userPO.setAccNum(ccNew.getE01CCMACC());

					flexLog("Putting java beans into the session");
					ses.setAttribute("error", msgError);					
					ses.setAttribute("userPO", userPO);

					if (IsNotError) { // There are no errors
						try {
							flexLog("About to call Page2: " + LangPath + "background.jsp");
							res.sendRedirect(super.srctx + "/pages/background.jsp");
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else { // There are errors
						try {
							  if (userPO.getHeader9().equals("C"))
							  {
								  ses.setAttribute("ccPvta", ccPvta);
								  ses.setAttribute("ccNew", ccNew);								  	
								  flexLog(
										  "About to call Page: "
										  + LangPath
										  + "ECC0020_cc_maint_posventa.jsp");
								  callPage(
										  LangPath + "ECC0020_cc_maint_posventa.jsp",
										  req,
										  res);
							  }
							  else
							  {
								  ses.setAttribute("dcPvta", ccPvta);
								  ses.setAttribute("dcNew", ccNew);			
								  flexLog(
										  "About to call Page: "
										  + LangPath
										  + "ECC0020_dc_maint_posventa.jsp");
								  callPage(
										  LangPath + "ECC0020_dc_maint_posventa.jsp",
										  req,
										  res);  
							  }
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}

					}
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}
		}

	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		Socket s = null;
		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			int screen = R_ENTER_SEARCH;

			try {

				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					s = new Socket(super.hostIP, getInitSocket(req) + 1);
					s.setSoTimeout(super.sckTimeOut);
					mc =
						new MessageContext(
							new DataInputStream(new BufferedInputStream(s.getInputStream())),
							new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
							"datapro.eibs.beans");
				

				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
					flexLog("Screen = " + screen);					
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}

				switch (screen) {
					// Request
				case R_ENTER_SEARCH :
					procReqTCEnterMaint(msgUser, req, res, session);
					break;
					
				case R_ENTER_LIST :
					procReqTCCardsList(mc, msgUser, req, res, session);
					break;
					
				case R_ENTER_MAINT :
					procActionPosMaint(mc, msgUser, req, res, session);
					break;					
				case A_MAINTENANCE :
					procActionMaintenance(mc, msgUser, req, res, session);
					break;
									
					default :
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
				}

				} catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
					//return;
				} finally {
					s.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}

		}

	}
	
	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

		}
	}


}