<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="datarec" class="datapro.eibs.beans.EPV114001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script language="javascript">

 function consultarSinacofi() {	
 	//validamos que haya colocado la serie rut.
 	if (document.forms[0].E01PVTIDE.value==''){
 		alert("Para efectuar esta consulta  debe Tener un Rut!");
	 	document.forms[0].E01PVTIDE.focus();
	 	return;
 	} 	
 	if (document.forms[0].E01PVTIDS.value==''){
 		alert("Para efectuar esta consulta debe colocar la serie Rut.!");
	 	document.forms[0].E01PVTIDS.focus();
	 	return;
 	}  	 	
 	if (document.forms[0].E01PVTCPR.value==''){
 		alert("Para efectuar esta consulta debe tener un Rut del empleador.!");
	 	document.forms[0].E01PVTCPR.focus();
	 	return;
 	} 
 	
 	 	
	document.forms[0].SCREEN.value = '850';	
	document.forms[0].submit();
 }
 
  	 function cerrarVentana(){
		window.open('','_parent','');
		window.close(); 
  	}
  	
   	function calcularMaximoAvalar(){
		document.forms[0].SCREEN.value='750';
		document.forms[0].submit();  
  	}	
  	
//  Process according with user selection
 function goAction(op) { 
 	if (op=='1'){//Cancel
		cerrarVentana();		 	
 	} else if (op=='2'){//submit -- enviar
		if (!document.forms[0].E01PVTCED.value==''){//no ha consultado sinacofi
	 		alert("Debe Consultar Bureau Para Continuar..!!!");
		 	document.forms[0].Consultar.focus();
		 	return false;	
	 	}
	 	document.getElementById('E01PVTREL').disabled = false;	
		document.forms[0].submit(); 	 	  	
 	}else if (op=='3'){//submit -- consultar datos aval.
		if (document.forms[0].E01PVTTCU.value=='' || document.forms[0].E01PVTTCU.value=='0'){//no ha colocado numero de cliente
	 		alert("Debe Colocar un numero de cliente para hacer esta consulta!!!");
		 	document.forms[0].E01PVTTCU.focus();
		 	return false;	
	 	} 	 	 
		document.forms[0].SCREEN.value='700';
		document.forms[0].submit(); 	
 	}
 	   
 }

 function mostrarDeuda() {
  	if (document.getElementById('end').style.display ==''){
		document.getElementById('end').style.display ='none'; 
		document.getElementById('e1').innerText ='+'; 	
  	}else{  	
		document.getElementById('end').style.display ='';
		document.getElementById('e1').innerText ='-';		  	
	}
 }
 
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
	boolean newOnly=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<%
	// Determina si es nuevo o mantencion
	if (userPO.getPurpose().equals("NEW")){
			newOnly=false;
		} else if (userPO.getPurpose().equals("MAINTENANCE")) {
    		newOnly=false;
		} else {
    		newOnly=true;		
		}
%>

<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	CONSULTA DE TITULARES - AVALES
<%} else if (maintenance){ %>
	MANTENIMIENTO DE TITULARES - AVALES
<%} else { %>
	NUEVO TITULARES - AVALES
<%} %>
 <% String emp = (String)session.getAttribute("EMPTI");
 	emp = (emp==null)?"":emp;//si es blanco viene llamado por menu, sino viene llamdo desde la pantalla EPV1010
 %>
 <%if ("".equals(emp)){ %> 
 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="titulars_maintenance.jsp, EPV1140"></h3>
<hr size="4">
<%}%>
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1140">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01UBK"  value="<%= currUser.getE01UBK().trim()%>">
  <input type=HIDDEN name="H01FLGMAS"  value="<%= datarec.getH01FLGMAS().trim()%>">

 <% int row = 0;%>
<%if ("".equals(emp)){ %> 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="datarec" property="E01PVTCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Propuesta : 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="datarec" property="E01PVTNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">Sequencia :  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="datarec" property="E01PVTSEQ" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="3" maxlength="2" readonly="true"/>
             </td>
         </tr>
       </table>
      </td>
    </tr>
  </table>
  <%}else{%>
  	<input  type="hidden" name="E01PVTCUN" value="<%=datarec.getE01PVTCUN()%>">
  	<input  type="hidden" name="E01PVTNUM" value="<%=datarec.getE01PVTNUM()%>">
  	<input  type="hidden" name="E01PVTSEQ" value="<%=datarec.getE01PVTSEQ()%>"> 
  <%} %>

<%if ("".equals(emp)){ %>      
  <h4>Datos del Titular - Aval</h4>
<%} %>      
  <table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
             <td > 
              <div align="right">Cliente Aval :</div>
            </td>
           <td>
 				<eibsinput:help name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" property="E01PVTTCU" 
					fn_param_one="E01PVTTCU" fn_param_two="E01PVTNME" fn_param_three="E01PVTIDE"   readonly="<%=readOnly %>"/>
				<input id="EIBSBTN" type=button name="ConsultarDatos" value="Consultar Aval"  onclick="goAction('3');">   					
            </td>
            <td > 
              <div align="right">Nombre :</div>
            </td>
            <td >
            	<eibsinput:text name="datarec" property="E01PVTNME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" size="35" required="true" readonly="true"/>
            </td>
                        
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td > 
              <div align="right">Rut :</div>
            </td>
            <td >
            	<eibsinput:text name="datarec" property="E01PVTIDE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" required="true" readonly="true"/>
            </td>
            <td > 
              <div align="right">Serie :</div>
            </td>
            <td >
            	<eibsinput:text name="datarec" property="E01PVTIDS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" size="12" maxlength="10" required="true" readonly="<%=readOnly%>"/>
				<input id="EIBSBTN" type=button name="Consultar" value="Consultar Bureau"  onclick="consultarSinacofi();">            	
            </td>            
          </tr>


          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td > 
              <div align="right">Porcentaje :</div>
            </td>
            <td >
					<eibsinput:text name="datarec" property="E01PVTPOR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>" readonly="<%=readOnly%>" required="true"/>              
	        </td>
           <td > 
              <div align="right">Relacion :</div>
            </td>
            <td > 
               <select id="E01PVTREL" name="E01PVTREL" disabled="disabled">
                    <option value=" " <% if (!(datarec.getE01PVTREL().equals("A")||datarec.getE01PVTREL().equals("T"))) out.print("selected"); %>> </option>
                    <option value="A" <% if (datarec.getE01PVTREL().equals("A")) out.print("selected"); %> selected>Aval</option>
                    <option value="T" <% if (datarec.getE01PVTREL().equals("T")) out.print("selected"); %>>2do Titular</option>                   
                  </select>
	        </td>
          </tr>
          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td > 
              <div align="right">Profesi�n :</div>
            </td>
            <td >
				<eibsinput:text name="datarec" property="E01PVTPRF" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />
	  			<eibsinput:text name="datarec" property="E01DSCPRF" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" readonly="true"  size="35"/>   
	        </td>
           <td > 
              <div align="right">Tipo de Contrato :</div>
            </td>
            <td > 
			  <input type="text" name="E01PVTNEM" size="5" maxlength="4" value="<%= datarec.getE01PVTNEM().trim()%>" readonly="true">
              <input type="text" name="E01DSCNEM" size="26" maxlength="20" value="<%= datarec.getE01DSCNEM().trim()%>" readonly="true">   				
	        </td>
          </tr>          
          
          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td > 
              <div align="right">Rut C&oacute;nyuge :</div>
            </td>
            <td >
				<eibsinput:text name="datarec" property="E01PVTRCO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true" />
	        </td>
           <td > 
              <div align="right">Nombre C&oacute;nyuge :</div>
            </td>
            <td > 
				<eibsinput:text name="datarec" property="E01PVTNCO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" readonly="true" />
	        </td>
          </tr>          
          
          
          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'>   
            <td > 
              <div align="right">Ap. Pat. C&oacute;nyuge :</div>
            </td>
            <td >
				<eibsinput:text name="datarec" property="E01PVTPCO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" readonly="true" />
	        </td>
           <td > 
              <div align="right">Ap. Mat. C&oacute;nyuge :</div>
            </td>
            <td > 
				<eibsinput:text name="datarec" property="E01PVTMCO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" readonly="true" />
	        </td>
          </tr>          
          
          <tr id='<%=(row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td > 
              <div align="right">Rut Empleador :</div>
            </td>
            <td >
				<input type="text" name="E01PVTCPR" size="16" maxlength="15" value="<%= datarec.getE01PVTCPR().trim()%>" readonly="true">
	        </td>
           <td > 
              <div align="right">Nombre Empleador :</div>
            </td> 
            <td > 
				<input type="text" name="E01PVTCP1" size="36" maxlength="35" value="<%= datarec.getE01PVTCP1().trim()%>" readonly="true">
	        </td>
          </tr>    
        </table>
      </td>
    </tr>
  </table>


  <h4>Datos BUREAU</h4>

  <table class="tableinfo">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">      
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%" >&nbsp;</td>          
            <td nowrap width="40%" >&nbsp;</td>
            <td nowrap width="25%" align="center"><b>Informados</b></td>
            <td nowrap width="25%" align="center"><b>Ajustados</b></td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%" >&nbsp;</td>          
            <td nowrap width="40%" >Cantidad Protestos ultimos 6 meses</td>
            <td nowrap width="25%" align="center">
            	<eibsinput:text name="datarec" property="E01PVTU6I" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" size="6" maxlength="5" readonly="true"/>
            </td>
            <td nowrap width="25%" align="center"> 
            	<eibsinput:text name="datarec" property="E01PVTU6A" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  size="6" maxlength="5" />
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%" >&nbsp;</td>          
            <td nowrap width="40%" >Cantidad Cheques Protestados ultimos 12 meses:</td>
            <td nowrap width="25%" align="center">
            	<eibsinput:text name="datarec" property="E01PVTU12" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  size="6" maxlength="5" readonly="true"/>
            </td>
            <td nowrap width="25%" align="center">
            	<eibsinput:text name="datarec" property="E01PVT12A" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  size="6" maxlength="5" />
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%" >&nbsp;</td>          
            <td nowrap width="40%" >Monto Total Protestos</td>
            <td nowrap width="25%" align="center">
            	<eibsinput:text name="datarec" property="E01PVTPRO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
            <td nowrap width="25%" align="center">
            	<eibsinput:text name="datarec" property="E01PVTPRA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  />
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%" >&nbsp;</td>          
            <td nowrap width="40%" >Monto Mora Total</td>
            <td nowrap width="25%" align="center">
            	<eibsinput:text name="datarec" property="E01PVTMOT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"   readonly="true"/>
            </td>
            <td nowrap width="25%" align="center">
            	<eibsinput:text name="datarec" property="E01PVTMOA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  />
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%" >&nbsp;</td>          
            <td nowrap width="40%" >Fecha Vigencia Cedula</td>
            <td nowrap width="25%" align="center">
            	<eibsinput:date name="datarec" fn_year="E01PVTVCY" fn_month="E01PVTVCM" fn_day="E01PVTVCD" readonly="true"/>
            </td>
            <td nowrap width="25%" align="center">
            	&nbsp;
            </td>
          </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%" >&nbsp;</td>          
            <td nowrap width="40%" colspan="3">Estado Cedula:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<eibsinput:text name="datarec" property="E01PVTCED" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR%>"  size="2" maxlength="1" readonly="true"/>   	            
	            <eibsinput:text name="datarec" property="E01DSCCED" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR%>"  size="40" maxlength="30" readonly="true"/>            
            </td>
          </tr>    

        </table>                       
        </td>
    </tr>
  </table>    
  
 <h4>Endeudamiento   (Pesos) <a id="e1" href="Javascript:mostrarDeuda();">-</a></h4>
<div id="end">
  <table class="tableinfo">
    <tr > 
      <td nowrap >
      <%row=0; %>
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">    
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%" align="center"><b>SBIF</b></td>
            <td nowrap width="21%" align="center"><b>Deuda <%=datarec.getE01PVTPEM()%> / <%=datarec.getE01PVTPEY()%></b></td>
            <td nowrap width="29%" align="center"><b>Compromiso Ponderado</b></td>
            <td nowrap width="23%" align="center"><b>Compromiso Ajustado</b></td>                        
          </tr>
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Consumo</b></div>
            </td>
            <td nowrap width="21%" align="center"> 
             	<eibsinput:text name="datarec" property="E01PVTCON" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTCONP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="23%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTCONM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" />    
            </td>
          </tr>
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Comercial</b> </div>
            </td>
            <td nowrap width="21%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTCMR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTCMRP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="23%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTCMRM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"   />    
            </td>
          </tr>
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%">&nbsp;&nbsp;&nbsp;<b>Hipotecario</b></td>
            <td nowrap width="21%" align="center"> 
             	<eibsinput:text name="datarec" property="E01PVTHIP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTHIPP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="23%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTHIPM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"   />    
            </td>
          </tr>
         <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Disponible</b> </div>
            </td>
            <td nowrap width="21%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTDIS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTDISP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="23%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTDISM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  onchange="setRecalculate()" />    
            </td>
          </tr>
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%">&nbsp;&nbsp;&nbsp;<b>Contingente</b></td>
            <td nowrap width="21%" align="center"> 
             	<eibsinput:text name="datarec" property="E01PVTCNT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTCNTP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="23%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTCNTM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"   />    
            </td>
          </tr>
         <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>TOTAL</b></div>
            </td>
            <td nowrap width="21%" align="center">
				<eibsinput:text name="datarec" property="E01PVTTOD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
			</td>
            <td nowrap width="29%" align="center">
	            <eibsinput:text name="datarec" property="E01PVTTOC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="23%" align="center">
            	<eibsinput:text name="datarec" property="E01PVTTOA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"   />
            </td>
          </tr>                  
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left" style="font: bold;"></div>
            </td>
            <td nowrap width="21%" align="center"></td>
            <td nowrap width="29%" align="center"></td>
            <td nowrap width="23%" align="center"></td>
          </tr>
         <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="center" style="font: bold;">COOPEUCH </div>
            </td>
			<td nowrap width="21%" align="center"><b>Saldo</b></td>
			<td nowrap width="29%" align="center"><b>Mensualidad</b></td>
			<td nowrap width="23%" align="center"></td>
          </tr>                    
         <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Planilla</b></div>
            </td>
            <td nowrap width="21%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTPLA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTPLAP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="23%" align="center"></td>
          </tr>                    
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Pago Directo</b></div>
            </td>
            <td nowrap width="21%" align="center"> 
             	<eibsinput:text name="datarec" property="E01PVTPDI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTPDIP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="23%" align="center"></td>
          </tr>                                                                  
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Tarjeta de Credito</b></div>
            </td>
            <td nowrap width="21%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTTCR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTTCRP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="23%" align="center"></td>
          </tr>            
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Ahorro</b></div>
            </td>
            <td nowrap width="21%" align="center"> 
				&nbsp;   
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTAHOP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="23%" align="center"></td>
          </tr>
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Cuotas Participaci�n</b></div>
            </td>
            <td nowrap width="21%" align="center"> 
				&nbsp;    
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTCUOP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="23%" align="center"></td>
          </tr>
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Pac Seguros</b></div>
            </td>
            <td nowrap width="21%" align="center"> 
				&nbsp;   
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTPACP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="23%" align="center"></td>
          </tr>    
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>TOTAL</b></div>
            </td>
            <td nowrap width="21%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTTOT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="29%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTTOTP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="23%" align="center"></td>
          </tr> 
         <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%">&nbsp; </td>
            <td nowrap width="21%" align="center">&nbsp;</td>
            <td nowrap width="29%" align="center">&nbsp;</td>
            <td nowrap width="23%" align="center">&nbsp;</td>
          </tr>                          
         <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="center" style="font: bold;">MOROSIDAD </div>
            </td>
			<td nowrap width="21%" align="center"><b>Deuda</b></td>
			<td nowrap width="29%" align="center"></td>
			<td nowrap width="23%" align="center"></td>
          </tr> 
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%">&nbsp;&nbsp;&nbsp;<b>Directos al d�a e impagos &lt; 30 d�as</b></td>
            <td nowrap width="21%" align="center"> 
             	<eibsinput:text name="datarec" property="E01PVTD30" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="29%" align="center"></td>
            <td nowrap width="23%" align="center"></td>
          </tr>
         <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Directos impagos entre 30 y &lt; 90 d�as</b> </div>
            </td>
            <td nowrap width="21%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVTD90" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="29%" align="center"></td>
            <td nowrap width="23%" align="center"></td>
          </tr>
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%">&nbsp;&nbsp;&nbsp;<b>Directos impagos entre 90 y &lt; 180 d�as</b></td>
            <td nowrap width="21%" align="center"> 
             	<eibsinput:text name="datarec" property="E01PVT180" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="29%" align="center"></td>
            <td nowrap width="23%" align="center"></td>
          </tr>
         <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;&nbsp;<b>Directos impagos entre 180 d�as y &lt; a 3 a�os</b> </div>
            </td>
            <td nowrap width="21%" align="center"> 
				<eibsinput:text name="datarec" property="E01PVT3AN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />    
            </td>
            <td nowrap width="29%" align="center"></td>
            <td nowrap width="23%" align="center"></td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="27%">&nbsp;&nbsp;&nbsp;<b>Directos impagos &gt;= 3 a�os</b></td>
            <td nowrap width="21%" align="center"> 
             	<eibsinput:text name="datarec" property="E01PVTM3A" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="29%" align="center"></td>
            <td nowrap width="23%" align="center"></td>
          </tr>
         <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"></td>
            <td nowrap width="21%" align="center"></td>
            <td nowrap width="29%" align="center"></td>
            <td nowrap width="23%" align="center"></td>
          </tr>                    
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;<b>Indirectos al d�a e impagos &lt; 30 d�as</b></div>
            </td>
            <td nowrap width="21%" align="center"> 
             	<eibsinput:text name="datarec" property="E01PVTI30" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true" />
            </td>
            <td nowrap width="29%" align="center"></td>
            <td nowrap width="23%" align="center"></td>
          </tr>   
         <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%">&nbsp;&nbsp;<b>Indirectos impagos &gt;= 30 d�as y &lt; 3 a�os</b> </td>
            <td nowrap width="21%" align="center">
            	<eibsinput:text name="datarec" property="E01PVTI3A" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
            </td>
            <td nowrap width="29%" align="center"></td>
            <td nowrap width="23%" align="center"></td>
          </tr>                    
          <tr id="<%=(row++%2==0?"trdark":"trclear")%>"> 
            <td nowrap width="27%"> 
              <div align="left">&nbsp;&nbsp;<b>Indirectos impagos&gt;= 3 a�os</b></div>
            </td>
            <td nowrap width="21%" align="center">
            	<eibsinput:text name="datarec" property="E01PVTIM3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
            </td>
            <td nowrap width="29%" align="center"></td>
            <td nowrap width="23%" align="center"></td>
          </tr>                                                            
        </table>                       
        </td>
    </tr>
  </table>
 </div>
 
 <h4>Ingresos </h4>
  <%row=0; %>
  <table class="tableinfo" width="100%">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">       
           <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'>  
            <td nowrap width="27%"> 
              <div align="right">Renta Bruta :</div>
            </td>
            <td nowrap width="21%"> 
			  <!--  <eibsinput:text name="datarec" property="E01PVTRBR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" required="true"   />-->     
			  <input type="text" name="E01PVTRBR" value="5000000"  " class="TXTRIGHT">         
            </td>
            <td nowrap width="29%">
    	       <div align="right">Renta Liquida :</div>            
            </td>
            <td nowrap width="23%">
				<!-- <eibsinput:text name="datarec" property="E01PVTICU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" required="true"   /> -->           
			  <input type="text" name="E01PVTICU" value="5000000"  " class="TXTRIGHT">         

            </td>
          </tr>
           <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td nowrap> 
              <div align="right">Renta Depurada :</div>
            </td>
            <td nowrap> 
				<!-- <eibsinput:text name="datarec" property="E01PVTRDE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" required="true"  onchange="calcularMaximoAvalar()" />  -->           
			  <input type="text" name="E01PVTRDE" value="5000000"  " class="TXTRIGHT">         
            </td>
            <td nowrap>
                <div align="right">Fecha Act. Renta  :</div>
            </td>
            <td nowrap>
            	<eibsinput:date name="datarec" fn_year="E01PVTARY" fn_month="E01PVTARM" fn_day="E01PVTARD" required="true" />    
            </td>
            </tr>  
           <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'>  
            <td nowrap></td>
            <td nowrap></td>            
            <td nowrap></td>
            <td nowrap></td>
          </tr>    
                                         
           <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'>  
            <td nowrap>
                <div align="right">Monto Maximo a Avalar :</div>
            </td>
            <td nowrap width="21%">
					<eibsinput:text name="datarec" property="E01PVTMAV" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" required="false" readonly="true"/>            
            </td>
            <td nowrap>&nbsp;</td>
            <td nowrap>&nbsp;</td>
          </tr>          
        </table>                       
        </td>
    </tr>
  </table>
 
     <h4>Convenio</h4>
    
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trdark"> 
            <td nowrap width="27%"> 
              <div align="right">Convenio :</div>
            </td>
            <td nowrap width="21%"> 
              <input type="text" name="E01PVTCNV" size="5" maxlength="4" value="<%= datarec.getE01PVTCNV().trim()%>" readonly="true">
             </td>
            <td nowrap width="23%"> 
              <div align="right">Descripci�n  :</div>
            </td>
            <td nowrap width="29%"> 
              <input type="text" name="E01DSCCNV" size="45" maxlength="35" value="<%= datarec.getE01DSCCNV().trim()%>" readonly="true">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="27%" > 
              <div align="right">Estado  :</div>
            </td>
            <td nowrap width="21%" > 
              <input type="hidden" name="E01PVTSCN" size="16" maxlength="15" value="<%= datarec.getE01PVTSCN().trim()%>" readonly="true">
              <input type="text" name="E01DSCSCN" size="26" maxlength="25" value="<%= datarec.getE01DSCSCN().trim()%>" readonly="true">                            
            </td>
            <td nowrap width="23%"> 
              <div align="right">Proximo  Vencimiento :</div>
            </td>
            <td nowrap width="29%"> 
              <eibsinput:date name="datarec" fn_year="E01PVTPVY" fn_month="E01PVTPVM" fn_day="E01PVTPVD" readonly="true" readonly="true" />   
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="27%"> 
              <div align="right">Descuento Empleador :</div>
            </td>
            <td nowrap width="21%"> 
  			  <eibsinput:text name="datarec" property="E01PVTDCT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>" readonly="true" />
             </td>
            <td nowrap width="23%"> 
              <div align="right">Descuento por excepci�n  :</div>
            </td>
            <td nowrap width="29%"> 
	            <eibsinput:text name="datarec" property="E01PVTDEX" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>" required="true"  />
				&nbsp;&nbsp;&nbsp;</td>
          </tr>
          <tr id="trclear"> 
            <td nowrap> 
             	 <div align="right">Maximo Descuento Convenio  :</div>
            </td>
            <td nowrap> 
 				<eibsinput:text name="datarec" property="E01PVTMBC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/>             
            </td>  
			<td nowrap > 
              
            </td>
            <td nowrap width="29%"> 
	             
            </td>                   
          </tr>          
        </table>
        </td>
    </tr>
  </table> 
  
  <br> 
   
   <%if  (!readOnly) { %>
      <div align="center"> 
          <input id="EIBSBTN" type="button" name="Submit" value="Enviar" onclick="goAction('2');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="goAction('1');">
      </div>
    <% } else { %>
      <div align="center"> 
          <input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="goAction('1');">
      </div>     
    <% } %>  
    
  </form>
 
 <%if ("S".equals(request.getAttribute("CALCULAR"))){%>
 <script type="text/javascript">	
	 document.forms[0].E01PVTMAV.focus();	
 </script>
 <% } %>  
 
 <%if ("S".equals(request.getAttribute("ACT"))){%>
 <script type="text/javascript">
 	  //recargamos la pagina que nos llamo..	  
	  window.opener.location.href =  '<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1140?SCREEN=101&E01PVTCUN=<%=userPO.getCusNum()%>&E01PVTNUM=<%=userPO.getHeader23()%>';	   	   
  	  //cerramos la ventana
  	  cerrarVentana();	  
 </script>
 <% } %>  
</body> 
</HTML>