package datapro.eibs.tables;

/**
 * This type was created by Orestes Garcia.
 */
import java.io.*;
import java.net.*;
import java.beans.Beans;
import javax.servlet.*;
import javax.servlet.http.*;
import datapro.eibs.beans.*;
import datapro.eibs.master.SuperServlet;

import datapro.eibs.sockets.*;

public class JSECN0030 extends datapro.eibs.master.SuperServlet {

	protected static final int R_INQUIRY = 100;
	protected static final int A_INQUIRY = 200;
	protected static final int R_INQUIRY_PIZZ = 300;
	protected static final int A_INQUIRY_PIZZ = 400;

	protected String LangPath = "S";

	/**
	 * Insert the method's description here.
	 * Creation date: (1/14/00 12:29:44 PM)
	 */
	public JSECN0030() {
		super();
	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

		flexLog("free resources used by JSCD0030");

	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqInquiry(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		try {
			flexLog("About to call Page: " + LangPath + "ECN0030_rate_table_enter.jsp");
			callPage(LangPath + "ECN0030_rate_table_enter.jsp", req, res);
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}

	protected void procActionInquiry(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
	
		MessageRecord newmessage = null;
		ESS0030DSMessage msgUser = null;
		ECN0030DSMessage msgCDRenRat = null;
		ECN0030DSMessage msgApyRate = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;

		boolean IsNotError = false;

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = "0100";

		// Send Initial data
		try {
			msgCDRenRat = (ECN0030DSMessage) mc.getMessageRecord("ECN0030DS");
			msgCDRenRat.setH02USERID(user.getH01USR());
			msgCDRenRat.setH02PROGRM("ESD0711");
			msgCDRenRat.setH02TIMSYS(getTimeStamp());
			msgCDRenRat.setH02SCRCOD("01");
			msgCDRenRat.setH02OPECOD(opCode);
			
			try {
				msgCDRenRat.setE02CDRCCY(req.getParameter("E02CDRCCY"));
			}
			catch (Exception e) {}
			
			try {
				msgCDRenRat.setE02CDRRTB(req.getParameter("E02CDRRTB"));
			}
			catch (Exception e) {}

			try {
				msgCDRenRat.setE02CDRDT1(req.getParameter("E02CDRDT1"));
				msgCDRenRat.setE02CDRDT2(req.getParameter("E02CDRDT2"));
				msgCDRenRat.setE02CDRDT3(req.getParameter("E02CDRDT3"));
			}
			catch (Exception e) {}
			
			msgCDRenRat.send();
			msgCDRenRat.destroy();

			flexLog("ECN0030DS Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			return;
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
				ses.setAttribute("error", msgError);

				try {
					flexLog("About to call Page: " + LangPath + "ECN0030_rate_table_enter.jsp");
					callPage(LangPath + "ECN0030_rate_table_enter.jsp", req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
									
			} else if (newmessage.getFormatName().equals("ECN0030DS")) {
				try {
				msgCDRenRat = new ECN0030DSMessage();
					flexLog("ECN0030DS Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
				msgCDRenRat = (ECN0030DSMessage) newmessage;
			
				if (newmessage.getFormatName().equals("ECN0030DS")) {
					try {
					msgApyRate = new ECN0030DSMessage();
						flexLog("ECN0030DS Received");
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}
				
					msgApyRate = (ECN0030DSMessage) newmessage;
				
					flexLog("Putting java beans into the session");
					ses.setAttribute("error", msgError);
					ses.setAttribute("cdRenRat", msgCDRenRat);
					ses.setAttribute("cdAPY", msgApyRate);

					try {
						flexLog("About to call Page: " + LangPath + "ECN0030_rate_table.jsp");
						callPage(LangPath + "ECN0030_rate_table.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else
					flexLog("Message "	+ newmessage.getFormatName() + " received.");
			} else
				flexLog("Message "	+ newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqInquiryPizz(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		try {
			flexLog("About to call Page: " + LangPath + "ECN0030_rate_table_pizz_enter.jsp");
			callPage(LangPath + "ECN0030_rate_table_pizz_enter.jsp", req, res);
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}

	protected void procActionInquiryPizz(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
	
		MessageRecord newmessage = null;
		ESS0030DSMessage msgUser = null;
		ECN0030DSMessage msgCDRenRat = null;
		ECN0030DSMessage msgApyRate = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
	
		boolean IsNotError = false;
	
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
	
		String opCode = "0200";
	
		// Send Initial data
		try {
			msgCDRenRat = (ECN0030DSMessage) mc.getMessageRecord("ECN0030DS");
			msgCDRenRat.setH02USERID(user.getH01USR());
			msgCDRenRat.setH02PROGRM("ESD0711");
			msgCDRenRat.setH02TIMSYS(getTimeStamp());
			msgCDRenRat.setH02SCRCOD("01");
			msgCDRenRat.setH02OPECOD(opCode);
			
			try {
				msgCDRenRat.setE02CDRCCY(req.getParameter("E02CDRCCY"));
			}
			catch (Exception e) {}
			
			try {
				msgCDRenRat.setE02CDRRTB(req.getParameter("E02CDRRTB"));
			}
			catch (Exception e) {}

			try {
				msgCDRenRat.setE02CDRDT1(req.getParameter("E02CDRDT1"));
				msgCDRenRat.setE02CDRDT2(req.getParameter("E02CDRDT2"));
				msgCDRenRat.setE02CDRDT3(req.getParameter("E02CDRDT3"));
			}
			catch (Exception e) {}
			
			msgCDRenRat.send();
			msgCDRenRat.destroy();
	
			flexLog("ECN0030DS Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			return;
		}
	
		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();
	
			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
				ses.setAttribute("error", msgError);
	
				try {
					flexLog("About to call Page: " + LangPath + "ECN0030_rate_table_pizz_enter.jsp");
					callPage(LangPath + "ECN0030_rate_table_pizz_enter.jsp", req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
									
			} else if (newmessage.getFormatName().equals("ECN0030DS")) {
				try {
				msgCDRenRat = new ECN0030DSMessage();
					flexLog("ECN0030DS Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
				msgCDRenRat = (ECN0030DSMessage) newmessage;
	
				if (newmessage.getFormatName().equals("ECN0030DS")) {
					try {
					msgApyRate = new ECN0030DSMessage();
						flexLog("ECN0030DS Received");
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}
				
					msgApyRate = (ECN0030DSMessage) newmessage;
				
					flexLog("Putting java beans into the session");
					ses.setAttribute("error", msgError);
					ses.setAttribute("cdRenRat", msgCDRenRat);
					ses.setAttribute("cdAPY", msgApyRate);
	
					try {
						flexLog("About to call Page: " + LangPath + "ECN0030_rate_table_pizz.jsp");
						callPage(LangPath + "ECN0030_rate_table_pizz.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else
					flexLog("Message "	+ newmessage.getFormatName() + " received.");
			} else
				flexLog("Message "	+ newmessage.getFormatName() + " received.");
	
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	/**
	 * This method was created by Orestes Garcia.
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 */
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		Socket s = null;
		MessageContext mc = null;
		ESS0030DSMessage msgUser = null;
		HttpSession session = null;
		session = req.getSession(false);
		
		if(session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			}
			catch(Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {
			
			int screen = R_INQUIRY;
			
			try {
				
				msgUser = (ESS0030DSMessage)session.getAttribute("currUser");
				LangPath = SuperServlet.rootPath + msgUser.getE01LAN() + "/";
				
				try {
					flexLog("Opennig Socket Connection");
					s = new Socket(SuperServlet.hostIP, SuperServlet.iniSocket + 1);
					s.setSoTimeout(SuperServlet.sckTimeOut);
					mc = new MessageContext(new DataInputStream(new BufferedInputStream(s.getInputStream())), new DataOutputStream(new BufferedOutputStream(s.getOutputStream())), "datapro.eibs.beans");
					
					try {
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					}
					catch(Exception e) {
						flexLog("Screen set to default value");
					}
					
					switch(screen) {
						case R_INQUIRY: 
							procReqInquiry(mc, msgUser, req, res, session);
							break;
	
						case A_INQUIRY: 
							procActionInquiry(mc, msgUser, req, res, session);
							break;
	
						case R_INQUIRY_PIZZ: 
							procReqInquiryPizz(mc, msgUser, req, res, session);
							break;
	
						case A_INQUIRY_PIZZ: 
							procActionInquiryPizz(mc, msgUser, req, res, session);
							break;
	
						default:
							res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.devPage);
							break;
					}
				}
				catch(Exception e) {
					e.printStackTrace();
					int sck = SuperServlet.iniSocket + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.sckNotOpenPage);
				}
				finally {
					s.close();
				}
			}
			catch(Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.sckNotRespondPage);
			}
		}
	}
	
	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

		}
	}
}