package datapro.eibs.beans;

import datapro.eibs.sockets.*;
import java.io.*;
import java.math.*;

import java.util.*;

/**
* Class generated by AS/400 CRTCLASS command from EDL090302 physical file definition.
* 
* File level identifier is 1151013094628.
* Record format level identifier is 3C88BFA5431EF.
*/

public class EDL090302Message extends MessageRecord
{
  final static String fldnames[] = {
                                     "H02USERID",
                                     "H02PROGRM",
                                     "H02TIMSYS",
                                     "H02SCRCOD",
                                     "H02OPECOD",
                                     "H02FLGMAS",
                                     "H02FLGWK1",
                                     "H02FLGWK2",
                                     "H02FLGWK3",
                                     "E02DLPACC",
                                     "E02DLPPNU",
                                     "E02DLPPDM",
                                     "E02DLPPDD",
                                     "E02DLPPDY",
                                     "E02DLPPPM",
                                     "E02DLPIPM",
                                     "E02DLPPIA",
                                     "E02DLPTOT",
                                     "E02DLPFL2",
                                     "E02DLPVEN",
                                     "E02DLPDTM",
                                     "E02DLPDTD",
                                     "E02DLPDTY",
                                     "E02DLPPAG",
                                     "E02ENDFLD",
                                     "E02SALCRE"
                                   };
  final static String tnames[] = {
                                   "H02USERID",
                                   "H02PROGRM",
                                   "H02TIMSYS",
                                   "H02SCRCOD",
                                   "H02OPECOD",
                                   "H02FLGMAS",
                                   "H02FLGWK1",
                                   "H02FLGWK2",
                                   "H02FLGWK3",
                                   "E02DLPACC",
                                   "E02DLPPNU",
                                   "E02DLPPDM",
                                   "E02DLPPDD",
                                   "E02DLPPDY",
                                   "E02DLPPPM",
                                   "E02DLPIPM",
                                   "E02DLPPIA",
                                   "E02DLPTOT",
                                   "E02DLPFL2",
                                   "E02DLPVEN",
                                   "E02DLPDTM",
                                   "E02DLPDTD",
                                   "E02DLPDTY",
                                   "E02DLPPAG",
                                   "E02ENDFLD",
                                   "E02SALCRE"
                                 };
  final static String fid = "1151013094628";
  final static String rid = "3C88BFA5431EF";
  final static String fmtname = "EDL090302";
  final int FIELDCOUNT = 26;
  private static Hashtable tlookup = new Hashtable();
  private CharacterField fieldH02USERID = null;
  private CharacterField fieldH02PROGRM = null;
  private CharacterField fieldH02TIMSYS = null;
  private CharacterField fieldH02SCRCOD = null;
  private CharacterField fieldH02OPECOD = null;
  private CharacterField fieldH02FLGMAS = null;
  private CharacterField fieldH02FLGWK1 = null;
  private CharacterField fieldH02FLGWK2 = null;
  private CharacterField fieldH02FLGWK3 = null;
  private DecimalField fieldE02DLPACC = null;
  private DecimalField fieldE02DLPPNU = null;
  private DecimalField fieldE02DLPPDM = null;
  private DecimalField fieldE02DLPPDD = null;
  private DecimalField fieldE02DLPPDY = null;
  private DecimalField fieldE02DLPPPM = null;
  private DecimalField fieldE02DLPIPM = null;
  private DecimalField fieldE02DLPPIA = null;
  private DecimalField fieldE02DLPTOT = null;
  private CharacterField fieldE02DLPFL2 = null;
  private DecimalField fieldE02DLPVEN = null;
  private DecimalField fieldE02DLPDTM = null;
  private DecimalField fieldE02DLPDTD = null;
  private DecimalField fieldE02DLPDTY = null;
  private DecimalField fieldE02DLPPAG = null;
  private CharacterField fieldE02ENDFLD = null;
  private DecimalField fieldE02SALCRE = null;

  /**
  * Constructor for EDL090302Message.
  */
  public EDL090302Message()
  {
    createFields();
    initialize();
  }

  /**
  * Create fields for this message.
  * This method implements the abstract method in the MessageRecord superclass.
  */
  protected void createFields()
  {
    recordsize = 192;
    fileid = fid;
    recordid = rid;
    message = new byte[getByteLength()];
    formatname = fmtname;
    fieldnames = fldnames;
    tagnames = tnames;
    fields = new MessageField[FIELDCOUNT];

    fields[0] = fieldH02USERID =
    new CharacterField(message, HEADERSIZE + 0, 10, "H02USERID");
    fields[1] = fieldH02PROGRM =
    new CharacterField(message, HEADERSIZE + 10, 10, "H02PROGRM");
    fields[2] = fieldH02TIMSYS =
    new CharacterField(message, HEADERSIZE + 20, 12, "H02TIMSYS");
    fields[3] = fieldH02SCRCOD =
    new CharacterField(message, HEADERSIZE + 32, 2, "H02SCRCOD");
    fields[4] = fieldH02OPECOD =
    new CharacterField(message, HEADERSIZE + 34, 4, "H02OPECOD");
    fields[5] = fieldH02FLGMAS =
    new CharacterField(message, HEADERSIZE + 38, 1, "H02FLGMAS");
    fields[6] = fieldH02FLGWK1 =
    new CharacterField(message, HEADERSIZE + 39, 1, "H02FLGWK1");
    fields[7] = fieldH02FLGWK2 =
    new CharacterField(message, HEADERSIZE + 40, 1, "H02FLGWK2");
    fields[8] = fieldH02FLGWK3 =
    new CharacterField(message, HEADERSIZE + 41, 1, "H02FLGWK3");
    fields[9] = fieldE02DLPACC =
    new DecimalField(message, HEADERSIZE + 42, 13, 0, "E02DLPACC");
    fields[10] = fieldE02DLPPNU =
    new DecimalField(message, HEADERSIZE + 55, 6, 0, "E02DLPPNU");
    fields[11] = fieldE02DLPPDM =
    new DecimalField(message, HEADERSIZE + 61, 3, 0, "E02DLPPDM");
    fields[12] = fieldE02DLPPDD =
    new DecimalField(message, HEADERSIZE + 64, 3, 0, "E02DLPPDD");
    fields[13] = fieldE02DLPPDY =
    new DecimalField(message, HEADERSIZE + 67, 5, 0, "E02DLPPDY");
    fields[14] = fieldE02DLPPPM =
    new DecimalField(message, HEADERSIZE + 72, 17, 2, "E02DLPPPM");
    fields[15] = fieldE02DLPIPM =
    new DecimalField(message, HEADERSIZE + 89, 17, 2, "E02DLPIPM");
    fields[16] = fieldE02DLPPIA =
    new DecimalField(message, HEADERSIZE + 106, 17, 2, "E02DLPPIA");
    fields[17] = fieldE02DLPTOT =
    new DecimalField(message, HEADERSIZE + 123, 17, 2, "E02DLPTOT");
    fields[18] = fieldE02DLPFL2 =
    new CharacterField(message, HEADERSIZE + 140, 1, "E02DLPFL2");
    fields[19] = fieldE02DLPVEN =
    new DecimalField(message, HEADERSIZE + 141, 5, 0, "E02DLPVEN");
    fields[20] = fieldE02DLPDTM =
    new DecimalField(message, HEADERSIZE + 146, 3, 0, "E02DLPDTM");
    fields[21] = fieldE02DLPDTD =
    new DecimalField(message, HEADERSIZE + 149, 3, 0, "E02DLPDTD");
    fields[22] = fieldE02DLPDTY =
    new DecimalField(message, HEADERSIZE + 152, 5, 0, "E02DLPDTY");
    fields[23] = fieldE02DLPPAG =
    new DecimalField(message, HEADERSIZE + 157, 17, 2, "E02DLPPAG");
    fields[24] = fieldE02ENDFLD =
    new CharacterField(message, HEADERSIZE + 174, 1, "E02ENDFLD");
    fields[25] = fieldE02SALCRE =
    new DecimalField(message, HEADERSIZE + 175, 17, 2, "E02SALCRE");

    synchronized (tlookup)
    {
      if (tlookup.isEmpty())
      {
        for (int i = 0; i < tnames.length; i++)
          tlookup.put(tnames[i], new Integer(i));
      }
    }

    taglookup = tlookup;
  }

  /**
  * Set field H02USERID using a String value.
  */
  public void setH02USERID(String newvalue)
  {
    fieldH02USERID.setString(newvalue);
  }

  /**
  * Get value of field H02USERID as a String.
  */
  public String getH02USERID()
  {
    return fieldH02USERID.getString();
  }

  /**
  * Set field H02PROGRM using a String value.
  */
  public void setH02PROGRM(String newvalue)
  {
    fieldH02PROGRM.setString(newvalue);
  }

  /**
  * Get value of field H02PROGRM as a String.
  */
  public String getH02PROGRM()
  {
    return fieldH02PROGRM.getString();
  }

  /**
  * Set field H02TIMSYS using a String value.
  */
  public void setH02TIMSYS(String newvalue)
  {
    fieldH02TIMSYS.setString(newvalue);
  }

  /**
  * Get value of field H02TIMSYS as a String.
  */
  public String getH02TIMSYS()
  {
    return fieldH02TIMSYS.getString();
  }

  /**
  * Set field H02SCRCOD using a String value.
  */
  public void setH02SCRCOD(String newvalue)
  {
    fieldH02SCRCOD.setString(newvalue);
  }

  /**
  * Get value of field H02SCRCOD as a String.
  */
  public String getH02SCRCOD()
  {
    return fieldH02SCRCOD.getString();
  }

  /**
  * Set field H02OPECOD using a String value.
  */
  public void setH02OPECOD(String newvalue)
  {
    fieldH02OPECOD.setString(newvalue);
  }

  /**
  * Get value of field H02OPECOD as a String.
  */
  public String getH02OPECOD()
  {
    return fieldH02OPECOD.getString();
  }

  /**
  * Set field H02FLGMAS using a String value.
  */
  public void setH02FLGMAS(String newvalue)
  {
    fieldH02FLGMAS.setString(newvalue);
  }

  /**
  * Get value of field H02FLGMAS as a String.
  */
  public String getH02FLGMAS()
  {
    return fieldH02FLGMAS.getString();
  }

  /**
  * Set field H02FLGWK1 using a String value.
  */
  public void setH02FLGWK1(String newvalue)
  {
    fieldH02FLGWK1.setString(newvalue);
  }

  /**
  * Get value of field H02FLGWK1 as a String.
  */
  public String getH02FLGWK1()
  {
    return fieldH02FLGWK1.getString();
  }

  /**
  * Set field H02FLGWK2 using a String value.
  */
  public void setH02FLGWK2(String newvalue)
  {
    fieldH02FLGWK2.setString(newvalue);
  }

  /**
  * Get value of field H02FLGWK2 as a String.
  */
  public String getH02FLGWK2()
  {
    return fieldH02FLGWK2.getString();
  }

  /**
  * Set field H02FLGWK3 using a String value.
  */
  public void setH02FLGWK3(String newvalue)
  {
    fieldH02FLGWK3.setString(newvalue);
  }

  /**
  * Get value of field H02FLGWK3 as a String.
  */
  public String getH02FLGWK3()
  {
    return fieldH02FLGWK3.getString();
  }

  /**
  * Set field E02DLPACC using a String value.
  */
  public void setE02DLPACC(String newvalue)
  {
    fieldE02DLPACC.setString(newvalue);
  }

  /**
  * Get value of field E02DLPACC as a String.
  */
  public String getE02DLPACC()
  {
    return fieldE02DLPACC.getString();
  }

  /**
  * Set numeric field E02DLPACC using a BigDecimal value.
  */
  public void setE02DLPACC(BigDecimal newvalue)
  {
    fieldE02DLPACC.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPACC as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPACC()
  {
    return fieldE02DLPACC.getBigDecimal();
  }

  /**
  * Set field E02DLPPNU using a String value.
  */
  public void setE02DLPPNU(String newvalue)
  {
    fieldE02DLPPNU.setString(newvalue);
  }

  /**
  * Get value of field E02DLPPNU as a String.
  */
  public String getE02DLPPNU()
  {
    return fieldE02DLPPNU.getString();
  }

  /**
  * Set numeric field E02DLPPNU using a BigDecimal value.
  */
  public void setE02DLPPNU(BigDecimal newvalue)
  {
    fieldE02DLPPNU.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPPNU as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPPNU()
  {
    return fieldE02DLPPNU.getBigDecimal();
  }

  /**
  * Set field E02DLPPDM using a String value.
  */
  public void setE02DLPPDM(String newvalue)
  {
    fieldE02DLPPDM.setString(newvalue);
  }

  /**
  * Get value of field E02DLPPDM as a String.
  */
  public String getE02DLPPDM()
  {
    return fieldE02DLPPDM.getString();
  }

  /**
  * Set numeric field E02DLPPDM using a BigDecimal value.
  */
  public void setE02DLPPDM(BigDecimal newvalue)
  {
    fieldE02DLPPDM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPPDM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPPDM()
  {
    return fieldE02DLPPDM.getBigDecimal();
  }

  /**
  * Set field E02DLPPDD using a String value.
  */
  public void setE02DLPPDD(String newvalue)
  {
    fieldE02DLPPDD.setString(newvalue);
  }

  /**
  * Get value of field E02DLPPDD as a String.
  */
  public String getE02DLPPDD()
  {
    return fieldE02DLPPDD.getString();
  }

  /**
  * Set numeric field E02DLPPDD using a BigDecimal value.
  */
  public void setE02DLPPDD(BigDecimal newvalue)
  {
    fieldE02DLPPDD.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPPDD as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPPDD()
  {
    return fieldE02DLPPDD.getBigDecimal();
  }

  /**
  * Set field E02DLPPDY using a String value.
  */
  public void setE02DLPPDY(String newvalue)
  {
    fieldE02DLPPDY.setString(newvalue);
  }

  /**
  * Get value of field E02DLPPDY as a String.
  */
  public String getE02DLPPDY()
  {
    return fieldE02DLPPDY.getString();
  }

  /**
  * Set numeric field E02DLPPDY using a BigDecimal value.
  */
  public void setE02DLPPDY(BigDecimal newvalue)
  {
    fieldE02DLPPDY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPPDY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPPDY()
  {
    return fieldE02DLPPDY.getBigDecimal();
  }

  /**
  * Set field E02DLPPPM using a String value.
  */
  public void setE02DLPPPM(String newvalue)
  {
    fieldE02DLPPPM.setString(newvalue);
  }

  /**
  * Get value of field E02DLPPPM as a String.
  */
  public String getE02DLPPPM()
  {
    return fieldE02DLPPPM.getString();
  }

  /**
  * Set numeric field E02DLPPPM using a BigDecimal value.
  */
  public void setE02DLPPPM(BigDecimal newvalue)
  {
    fieldE02DLPPPM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPPPM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPPPM()
  {
    return fieldE02DLPPPM.getBigDecimal();
  }

  /**
  * Set field E02DLPIPM using a String value.
  */
  public void setE02DLPIPM(String newvalue)
  {
    fieldE02DLPIPM.setString(newvalue);
  }

  /**
  * Get value of field E02DLPIPM as a String.
  */
  public String getE02DLPIPM()
  {
    return fieldE02DLPIPM.getString();
  }

  /**
  * Set numeric field E02DLPIPM using a BigDecimal value.
  */
  public void setE02DLPIPM(BigDecimal newvalue)
  {
    fieldE02DLPIPM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPIPM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPIPM()
  {
    return fieldE02DLPIPM.getBigDecimal();
  }

  /**
  * Set field E02DLPPIA using a String value.
  */
  public void setE02DLPPIA(String newvalue)
  {
    fieldE02DLPPIA.setString(newvalue);
  }

  /**
  * Get value of field E02DLPPIA as a String.
  */
  public String getE02DLPPIA()
  {
    return fieldE02DLPPIA.getString();
  }

  /**
  * Set numeric field E02DLPPIA using a BigDecimal value.
  */
  public void setE02DLPPIA(BigDecimal newvalue)
  {
    fieldE02DLPPIA.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPPIA as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPPIA()
  {
    return fieldE02DLPPIA.getBigDecimal();
  }

  /**
  * Set field E02DLPTOT using a String value.
  */
  public void setE02DLPTOT(String newvalue)
  {
    fieldE02DLPTOT.setString(newvalue);
  }

  /**
  * Get value of field E02DLPTOT as a String.
  */
  public String getE02DLPTOT()
  {
    return fieldE02DLPTOT.getString();
  }

  /**
  * Set numeric field E02DLPTOT using a BigDecimal value.
  */
  public void setE02DLPTOT(BigDecimal newvalue)
  {
    fieldE02DLPTOT.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPTOT as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPTOT()
  {
    return fieldE02DLPTOT.getBigDecimal();
  }

  /**
  * Set field E02DLPFL2 using a String value.
  */
  public void setE02DLPFL2(String newvalue)
  {
    fieldE02DLPFL2.setString(newvalue);
  }

  /**
  * Get value of field E02DLPFL2 as a String.
  */
  public String getE02DLPFL2()
  {
    return fieldE02DLPFL2.getString();
  }

  /**
  * Set field E02DLPVEN using a String value.
  */
  public void setE02DLPVEN(String newvalue)
  {
    fieldE02DLPVEN.setString(newvalue);
  }

  /**
  * Get value of field E02DLPVEN as a String.
  */
  public String getE02DLPVEN()
  {
    return fieldE02DLPVEN.getString();
  }

  /**
  * Set numeric field E02DLPVEN using a BigDecimal value.
  */
  public void setE02DLPVEN(BigDecimal newvalue)
  {
    fieldE02DLPVEN.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPVEN as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPVEN()
  {
    return fieldE02DLPVEN.getBigDecimal();
  }

  /**
  * Set field E02DLPDTM using a String value.
  */
  public void setE02DLPDTM(String newvalue)
  {
    fieldE02DLPDTM.setString(newvalue);
  }

  /**
  * Get value of field E02DLPDTM as a String.
  */
  public String getE02DLPDTM()
  {
    return fieldE02DLPDTM.getString();
  }

  /**
  * Set numeric field E02DLPDTM using a BigDecimal value.
  */
  public void setE02DLPDTM(BigDecimal newvalue)
  {
    fieldE02DLPDTM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPDTM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPDTM()
  {
    return fieldE02DLPDTM.getBigDecimal();
  }

  /**
  * Set field E02DLPDTD using a String value.
  */
  public void setE02DLPDTD(String newvalue)
  {
    fieldE02DLPDTD.setString(newvalue);
  }

  /**
  * Get value of field E02DLPDTD as a String.
  */
  public String getE02DLPDTD()
  {
    return fieldE02DLPDTD.getString();
  }

  /**
  * Set numeric field E02DLPDTD using a BigDecimal value.
  */
  public void setE02DLPDTD(BigDecimal newvalue)
  {
    fieldE02DLPDTD.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPDTD as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPDTD()
  {
    return fieldE02DLPDTD.getBigDecimal();
  }

  /**
  * Set field E02DLPDTY using a String value.
  */
  public void setE02DLPDTY(String newvalue)
  {
    fieldE02DLPDTY.setString(newvalue);
  }

  /**
  * Get value of field E02DLPDTY as a String.
  */
  public String getE02DLPDTY()
  {
    return fieldE02DLPDTY.getString();
  }

  /**
  * Set numeric field E02DLPDTY using a BigDecimal value.
  */
  public void setE02DLPDTY(BigDecimal newvalue)
  {
    fieldE02DLPDTY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPDTY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPDTY()
  {
    return fieldE02DLPDTY.getBigDecimal();
  }

  /**
  * Set field E02DLPPAG using a String value.
  */
  public void setE02DLPPAG(String newvalue)
  {
    fieldE02DLPPAG.setString(newvalue);
  }

  /**
  * Get value of field E02DLPPAG as a String.
  */
  public String getE02DLPPAG()
  {
    return fieldE02DLPPAG.getString();
  }

  /**
  * Set numeric field E02DLPPAG using a BigDecimal value.
  */
  public void setE02DLPPAG(BigDecimal newvalue)
  {
    fieldE02DLPPAG.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02DLPPAG as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02DLPPAG()
  {
    return fieldE02DLPPAG.getBigDecimal();
  }

  /**
  * Set field E02ENDFLD using a String value.
  */
  public void setE02ENDFLD(String newvalue)
  {
    fieldE02ENDFLD.setString(newvalue);
  }

  /**
  * Get value of field E02ENDFLD as a String.
  */
  public String getE02ENDFLD()
  {
    return fieldE02ENDFLD.getString();
  }

  /**
  * Set field E02SALCRE using a String value.
  */
  public void setE02SALCRE(String newvalue)
  {
    fieldE02SALCRE.setString(newvalue);
  }

  /**
  * Get value of field E02SALCRE as a String.
  */
  public String getE02SALCRE()
  {
    return fieldE02SALCRE.getString();
  }

  /**
  * Set numeric field E02SALCRE using a BigDecimal value.
  */
  public void setE02SALCRE(BigDecimal newvalue)
  {
    fieldE02SALCRE.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E02SALCRE as a BigDecimal.
  */
  public BigDecimal getBigDecimalE02SALCRE()
  {
    return fieldE02SALCRE.getBigDecimal();
  }

}
