<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<HTML>
<HEAD>
<TITLE>Lista de Formularios</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<%@ page import="datapro.eibs.master.JSEIBSProp" %>

<link href="<%=request.getContextPath()%>/pages/style.css"	rel="stylesheet">

<jsp:useBean id= "rtBasic" class= "datapro.eibs.beans.EDD000001Message"  scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="pdfList" class="datapro.eibs.beans.JBObjList" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="javascript">


  	function goView(pdf) {
  		var ope = document.forms[0].E01ACMACC.value;
  		var flg1 = document.forms[0].E01ACMACL.value;
  		
    	var page = '<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2300?SCREEN=200&action=1&copies=1&E15ACCNUM='+ ope +'&E01ACMACL='+ flg1 +'&pdfName=' + pdf;
		CenterNamedWindow(page,'pdf',600,500,7);
  	}

</script>

</HEAD>

<BODY>


<h3 align="center">Formularios<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="pdf_list, EDD2300"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXEDD0000" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" value="44">


<INPUT TYPE=HIDDEN NAME="pdfName" VALUE=""> 
<INPUT TYPE=HIDDEN NAME="copies" VALUE=""> 

<INPUT TYPE=HIDDEN NAME="E01ACMACL" VALUE="<%=rtBasic.getE01ACMACL()%>">
<INPUT TYPE=HIDDEN NAME="E01INDICA" VALUE="PDF">


  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Cuenta : 
              </td>
             <td nowrap width="10%" align="left">
             	<input type="text" size="15" maxlength="16"  name="E01ACMACC" value="<%= userPO.getAccNum().trim()%>" readonly >
             </td>
             <td nowrap width="10%" align="right">N&uacute;mero :  
             </td>
             <td nowrap width="10%" align="left">            
             	<input type="text" size="10" maxlength="9"  value="<%= userPO.getCusNum().trim()%>" readonly >
             </td>
             <td nowrap width="10%" align="right"> Nombre : 
               </td>
             <td nowrap width="50%"align="left">
             	<input type="text" size="50" maxlength="51"  name="" value="<%= userPO.getCusName().trim()%>" readonly >
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table> 
  <br>
  <br>

  <TABLE class="tableinfo" ALIGN=CENTER>
	<COL WIDTH=10%><COL WIDTH=50%><COL WIDTH=30%>
	<tr id="trclear" align="center"> 
  		
  		<th align=CENTER nowrap ><div align="center" >
  		Recuerde Imprimir Formularios y Solicitar al Socio su Firma</div></th>
 	</tr>
	<tr id="trdark" height="30">
	</tr>
</TABLE>
  
  <TABLE class="tableinfo" ALIGN=CENTER>
	<COL WIDTH=10%><COL WIDTH=50%><COL WIDTH=30%>
	<tr id="trclear"> 
  		<th align=CENTER nowrap ><div align="left">Nombre del Formulario</div></th>
 	</tr>
      <tr id="trdark">
            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:goView('CHECK_LIST_DE_DOCUMENTOS_CUENTA_VISTA.pdf');">CHECK LIST DE DOCUMENTOS CUENTA VISTA</A></td>
      </tr>
      <tr id="trdark">
            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:goView('CONTRATO_DE_APERTURA_DE_CUENTA_VISTA.pdf');">CONTRATO DE APERTURA DE CUENTA VISTA</A></td>
      </tr>
      <tr id="trdark">                         
            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:goView('HOJA_DE_RESUMEN_CONTRATO.pdf');">HOJA DE RESUMEN CONTRATO</A></td>
      </tr>
      <tr id="trdark">
            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:goView('ANEXO_COMISIONES_CUENTA_VISTA.pdf');">ANEXO COMISIONES CUENTA VISTA</A></td>
      </tr>
      <tr id="trdark">
            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:goView('REGISTRO_DE_FIRMA.pdf');">REGISTRO DE FIRMA</A></td>
      </tr>
      <tr id="trdark">
            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:goView('ANEXO_SERVICIO_DE_ATENCION_AL_CLIENTE_(SAC).pdf');">ANEXO SERVICIO DE ATENCION AL CLIENTE (SAC)</A></td>
      </tr>
      <tr id="trdark">
            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:goView('MANDATO_DE_ABONO_DE_REMUNERACIONES.pdf');">MANDATO DE ABONO DE REMUNERACIONES</A></td>
      </tr>

</TABLE>
<%if(!userPO.getHeader1().equals("R")){ %>
 <div align="center"> 
     &nbsp;&nbsp;&nbsp;&nbsp;    
     <input id="EIBSBTN" type="submit" name="Cancel" value="Continuar">    
 </div>
<%} %>

</FORM>

</BODY>
</HTML>
