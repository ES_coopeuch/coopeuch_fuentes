<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Tabla de Rangos Permitidos Interfaces</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "InterList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<script language="JavaScript">



function goAction(op) {
	if(op==3)
		document.forms[0].SCREEN.value = "100";
		
	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}
</SCRIPT>  

</head>

<BODY>
<h3 align="center">Tabla de Rangos Permitidos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="interface_list, EREC100"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.motorpago.JSEREC100" >
    <input type=HIDDEN name="SCREEN" value="300">
    <input type=HIDDEN name="opt"> 

  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0" bordercolor="#000000">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table class="tableinfo" cellspacing=0 cellpadding=2 width="100%" border="0">
		  <tr> 
            <td nowrap width="10%"> 
              <div align="right">Interfaz :</div>
            </td>
            <td nowrap width="90%"> 
              <input type="text" name="E01RERIDE" size="5" maxlength="4" readonly value="<%=userPO.getIdentifier() %>">
              <input type="text" name="E01RERDES" size="41" maxlength="40" readonly value="<%=userPO.getHeader1() %>">
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>


  
<%
	if ( InterList.getNoResult() ) {
 %>
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
          <table class="tbenter" width=100% align=center>
            <tr> 
              <td class=TDBKG > 
                <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
              </td>
            </tr>
          </table>
          <p>&nbsp;</p>
        </div>
	  </TD>
	</TR>
  </TABLE>

  <%  
	}
	else 
	{
 
  if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }

%> 
 
          
  <table class="tbenter" width=100% align=center height="8%">
    <tr> 
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
      </td>
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(2)"><b>Modificar</b></a></div>
      </td>
	  <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(3)"><b>Volver</b></a></div>
      </td>      
    </tr>
  </table>
  <br>

  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
            <th align=CENTER nowrap width="20%">ID. INTERFAZ</th>
            <th align=LEFT nowrap >NRO. CAMPO</th>
            <th align=LEFT nowrap >DESCRIPCI&Oacute;N</th>
            <th align=LEFT nowrap >IND. DESDE</th>
            <th align=LEFT nowrap >IND. HASTA</th>
            <th align=LEFT nowrap >IND. ACTIVACI&Oacute;N</th>
          </tr>
 
           <%
                InterList.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (InterList.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	datapro.eibs.beans.EREC10001Message msgList = (datapro.eibs.beans.EREC10001Message) InterList.getRecord();
		 %>
          <tr id="dataTable<%= InterList.getCurrentRow() %>"> 
            <td NOWRAP  align=CENTER ><input type="radio" name="CURRCODE" value="<%= InterList.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE01RERIDE() %></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE01RERCOR() %></td>
            <td NOWRAP  align=left ><%= msgList.getE01RERCON() %></td>
            <td NOWRAP  align="right" ><%= msgList.getE01RERMIN() %></td>
            <td NOWRAP  align="right" ><%= msgList.getE01RERMAX() %></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE01RERIND() %></td>

          </tr>
          <%
                }
              %>
          </table>
      </td>
    </tr>
  </table>
  
     
<SCRIPT language="JavaScript">
	showChecked("CURRCODE");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

</body>
</html>
