package datapro.eibs.client;

/**
 * Servlet de Asociar Integrante.
 * Creation date: (16/03/2018)
 * @author: David Medina
 */

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD008001Message;
import datapro.eibs.beans.ESD407001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

public class JSESD4070 extends JSEIBSServlet {
	
	// Action 
	
	protected static final int R_ENTER_NEW 	 = 100;
	protected static final int A_ENTER_ASOCIAR_NEW 	 = 200;
	protected static final int A_ENTER_PRODUCT_NEW 	 = 300;
	
	
	protected String LangPath = "S";
	
	
	/**
	 * JSESD4030 constructor comment.
	 */
	public JSESD4070() {
		super();
		// TODO Apéndice de constructor generado automáticamente
	}
	
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

		flexLog("free resources used by JSESD4070");
		
	}
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		MessageContext mc = null;
		ESS0030DSMessage msgUser = null;
	  	
		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			}
			catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e); 
			}
		}else{
			screen = R_ENTER_NEW;
			
			try{
				
				msgUser = (datapro.eibs.beans.ESS0030DSMessage)session.getAttribute("currUser");
				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";
				try{
					flexLog("Opennig Socket Connection");
					mc = new MessageContext(super.getMessageHandler("ESD4060", req));
					
					try{
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					}catch(Exception e){
						flexLog("Screen set to default value");
					}
					
					switch (screen) {
						// BEGIN Entering
						// Request
						case R_ENTER_NEW : 
							
							break;
							
						case A_ENTER_ASOCIAR_NEW : 
							procActionEnterIntegrantNew(mc, msgUser, req, res, session, screen);
							break;
							
						case A_ENTER_PRODUCT_NEW : 
							
							break;
						
						default :
							res.sendRedirect(super.srctx + LangPath + super.devPage);
							break;
					}
				}catch(Exception e){
					e.printStackTrace();
					flexLog("Socket not Open(" + mc.toString() + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
				}
				finally {
					mc.close();
				}
			}catch(Exception e){
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}
		}
	}
	
	/**
	 * Metodo de error
	 * @param m
	 */
	protected void showERROR(ELEERRMessage m)
	{
		if (logType != NONE) {
			
			flexLog("ERROR received.");
			
			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01() + " code : " + m.getERDF01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02() + " code : " + m.getERDF02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03() + " code : " + m.getERDF03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04() + " code : " + m.getERDF04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05() + " code : " + m.getERDF05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06() + " code : " + m.getERDF06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07() + " code : " + m.getERDF07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08() + " code : " + m.getERDF08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09() + " code : " + m.getERDF09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10() + " code : " + m.getERDF10());
			
		}
	}
	
	
	
	/**
	 * Metodo de registro de nuevas asignaciones 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionEnterIntegrantNew(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		
		MessageRecord newmessage = null;
		MessageRecord newmessage1 = null;
		MessageProcessor mp = null;	
		MessageContext mcID = null;
		ESD008001Message msgClientPersonal = null;
		ESD407001Message msgPlanSocio = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		boolean IsNotError = false;
		UserPos	userPOAux = null;
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		
		if (req.getParameter("NUEVO").equals("0")){
			// Send Initial data
			try
			{			
				mcID = new MessageContext(super.getMessageHandler("ESD0080", req));
				msgClientPersonal = (ESD008001Message)mcID.getMessageRecord("ESD008001");
			 	msgClientPersonal.setH01USR(user.getH01USR());
			 	msgClientPersonal.setH01PGM("ESD0080");
			 	msgClientPersonal.setH01TIM(getTimeStamp());
			 	msgClientPersonal.setH01SCR("01");
			 	msgClientPersonal.setH01OPE("0002");
			 	try {
				 	if (req.getParameter("E01CUN") != null){
					 	msgClientPersonal.setE01CUN(req.getParameter("E01CUN"));
				 	}
				}
				catch (Exception e)
				{
					msgClientPersonal.setE01CUN("0");
				    flexLog("Input data error " + e);
				}
				try {
				 	if (req.getParameter("E01IDN") != null && 
				 		req.getParameter("E01IDN").indexOf("-") != -1	){
	//			 	 For PANAMA LONG IDENTIFICATION
				 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));			 		
				 	}else {
				 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));
				 	}
				}
				catch (Exception e)
				{
					msgClientPersonal.setE01IDN("");
				    flexLog("Input data error " + e);
				}
			 	//msgClientPersonal.setE01IDN("");
				msgClientPersonal.send();	
			 	msgClientPersonal.destroy();
			 	flexLog("ESD008001 Message Sent");
			 	
			}		
			catch (Exception e)
			{
				e.printStackTrace();
				flexLog("Error: " + e);
			  	throw new RuntimeException("Socket Communication Error");
			}
			
			// Receive Error Message
			try
			{
			  newmessage = mcID.receiveMessage();
			  
			  if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage)newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			  }
			  else
				flexLog("Message " + newmessage.getFormatName() + " received.");
				
			}
			catch (Exception e)
			{
				e.printStackTrace();
				flexLog("Error: " + e);
			  	throw new RuntimeException("Socket Communication Error");
			}	
			
			// Receive Data
			try
			{
				newmessage = mcID.receiveMessage();
				
				if (newmessage.getFormatName().equals("ESD008001")) {
					try {
						msgClientPersonal = new datapro.eibs.beans.ESD008001Message();
						flexLog("ESD008001 Message Received");
				  	} catch (Exception ex) {
						flexLog("Error: " + ex); 
				  	}
	
					msgClientPersonal = (ESD008001Message)newmessage;
	
					userPO.setCusNum(msgClientPersonal.getE01CUN());
					userPO.setOption("CLIENT_P");
					userPO.setCusType(msgClientPersonal.getE01LGT());
					userPO.setHeader1(msgClientPersonal.getE01CUN());
					userPO.setHeader2(msgClientPersonal.getE01IDN());
					userPO.setHeader3(msgClientPersonal.getE01NA1());
					
					if (!msgClientPersonal.getE01IDN().equals("") &&
						msgClientPersonal.getE01IDN().indexOf("-") != -1 ) {
						
						userPO.setHeader2(msgClientPersonal.getE01IDN());
					}
					
					
					flexLog("Putting java beans into the session");
	
					if (!IsNotError) {  // There are no errors
						try {
							
							flexLog("About to call Page: /pages/" + LangPath + "ESD4060_person_integrant_both_enter.jsp");
							callPage(LangPath + "ESD4060_person_integrant_both_enter.jsp", req, res);	
						}
						catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
				
				else
					flexLog("Message " + newmessage.getFormatName() + " received.");
	
			}
			catch (Exception e)	{
				e.printStackTrace();
				flexLog("Error: " + e);
			  	throw new RuntimeException("Socket Communication Error");
			}
			
			// Send Initial data
			try{
				msgPlanSocio = (ESD407001Message)mc.getMessageRecord("ESD407001");
				msgPlanSocio.setH01USR(user.getH01USR());
				msgPlanSocio.setH01PGM("ESD4070");
				msgPlanSocio.setH01TIM(getTimeStamp());
				msgPlanSocio.setH01SCR("01");
				msgPlanSocio.setH01OPE("0015");
			 	try {
				 	if (req.getParameter("E01CUN") != null){
				 		msgPlanSocio.setL01FILLE1(msgClientPersonal.getE01CUN()!=null ? msgClientPersonal.getE01CUN() : userPO.getCusNum());
				 		userPO.setCusNum(msgPlanSocio.getL01FILLE1().toString());
				 	}
				}catch (Exception e){
					msgPlanSocio.setL01FILLE1("0");
				    flexLog("Input data error " + e);
				}
				
				try {
				 	if (req.getParameter("E01IDN") != null && 
				 		req.getParameter("E01IDN").indexOf("-") != -1	){
	//			 	 For PANAMA LONG IDENTIFICATION
				 		msgPlanSocio.setL01FILLE5(msgClientPersonal.getE01IDN());
				 		userPO.setHeader2(msgPlanSocio.getL01FILLE5().toString());
				 	}else {
				 		msgPlanSocio.setL01FILLE5(msgClientPersonal.getE01IDN());
				 		userPO.setHeader2(msgPlanSocio.getL01FILLE5().toString());
				 	}
				}catch (Exception e){
					msgPlanSocio.setL01FILLE5("");
				    flexLog("Input data error " + e);
				}
				//se desbloquea cuando este listo el programa
			 	msgPlanSocio.send();	
				msgPlanSocio.destroy();
				
				// Receive Data
				try
				{
					newmessage1 = mc.receiveMessage();
					
					if (newmessage1.getFormatName().equals("ESD406001")) {
						try {
							msgPlanSocio = new datapro.eibs.beans.ESD407001Message();
							flexLog("ESD406001 Message Received");
					  	} catch (Exception ex) {
							flexLog("Error: " + ex); 
					  	}
	
					  	msgPlanSocio = (ESD407001Message)newmessage1;		
						
						flexLog("Putting java beans into the session");
	
						if (!IsNotError) {  // There are no errors
							try {
								flexLog("About to call Page: /pages/" + LangPath + "ESD4060_person_integrant_both_enter.jsp");
								forward("ESD4060_person_integrant_both_enter.jsp", req, res);	
							}
							catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						}
					}
					
					else
						flexLog("Message " + newmessage.getFormatName() + " received.");
	
				}
				catch (Exception e)	{
					e.printStackTrace();
					flexLog("Error: " + e);
				  	throw new RuntimeException("Socket Communication Error");
				}
				
				mp = getMessageProcessor("ESD4070", req);
				
				ESD407001Message msgPersonalRelation = (ESD407001Message) mp.getMessageRecord("ESD407001");
				
				msgPersonalRelation.setH01USR(user.getH01USR());
				msgPersonalRelation.setH01PGM("ESD4070");
				msgPersonalRelation.setH01TIM(getTimeStamp());
				msgPersonalRelation.setH01SCR("01");
				msgPersonalRelation.setH01OPE("0015");
				msgPersonalRelation.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
				msgPersonalRelation.setL01FILLE5(userPO.getHeader2().toString());
				msgPersonalRelation.setL01FILLE4("R");
				
				mp.sendMessage(msgPersonalRelation);
				
				JBObjList list = mp.receiveMessageRecordList("H01MAS");
				if(mp.hasError(list)){
					session.setAttribute("error", mp.getError(list));
					forward("error_viewer.jsp", req, res);
				}else{
					userPOAux = userPO;
					session.setAttribute("msgPlanSocio", msgPlanSocio);
					session.setAttribute("ESD406001List", list);
					session.setAttribute("userPOAux", userPOAux);
				 	flexLog("ESD406001 Message Sent");	
				 	forward("ESD4060_asign_integrant_new.jsp", req, res);
				}
				
				if(mp != null) mp.close();
			}		
			catch (Exception e){
				e.printStackTrace();
				flexLog("Error: " + e);
			  	throw new RuntimeException("Socket Communication Error");
			}	
		}
		else{
			JBObjList list = (datapro.eibs.beans.JBObjList) session.getAttribute("ESD407001List");
			msgPlanSocio = (ESD407001Message) session.getAttribute("msgPlanSocio");
			String rut = (String) session.getAttribute("rut");
			session.setAttribute("msgPlanSocio", msgPlanSocio);
			session.setAttribute("rut", rut);
			session.setAttribute("ESD406001List", list);
		 	flexLog("ESD406001 Message Sent");	
		 	forward("ESD4060_asign_integrant_new.jsp", req, res);
		}
	}//fin metodo procActionEnterIntegrantNew
	
	
}

