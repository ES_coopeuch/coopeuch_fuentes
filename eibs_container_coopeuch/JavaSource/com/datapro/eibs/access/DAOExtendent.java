/*
 * Created on Aug 27, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.datapro.eibs.access;

import org.apache.log4j.Logger;

import com.datapro.access.ValueObject;
import com.datapro.eibs.exception.ItemNotFoundException;
import com.datapro.eibs.exception.ItemNotUniqueException;
import com.datapro.eibs.exception.ItemUpdatedByAnotherUser;
import com.datapro.exception.DAOException;
import com.datapro.generic.beanutil.BeanCopier;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class DAOExtendent extends DAOSuper {

	private final static Logger log = Logger.getLogger(DAOExtendent.class);
	
	public int insert(Object[] obj) throws DAOException {
		return super.insertImpl(getFindSql(), (Object[]) obj);
	}

	/* (non-Javadoc)
	 * @see com.datapro.eibs.access.DISuper#insert(com.datapro.access.ValueObject)
	 */
	public int insert(ValueObject vo) throws DAOException {
		setAuditVar(vo);
		return super.insertImpl(vo);
	}

	/* (non-Javadoc)
	 * @see com.datapro.eibs.access.DISuper#update(com.datapro.access.ValueObject)
	 */
	public int update(ValueObject vo) throws DAOException,
			ItemUpdatedByAnotherUser {

		try {
			KeySuper key = (KeySuper) newInstance(getKeyClass());

			BeanCopier.populate(vo, key);
			
			Object bean = findByPrimaryKey(key);
			if (!checkDataIntegrity(vo, bean)) {
				throw new ItemUpdatedByAnotherUser(
						EXCEPTION_ITEM_UPDATE_BY_ANOTHER_USER);
			}
			setAuditVar(vo);
			return super.updateImpl(vo, key);

		} catch (ItemNotFoundException e) {
			log.error(e);
			throw new DAOException(e.toString());
		} catch (ItemNotUniqueException e) {
			log.error(e);
			throw new DAOException(e.toString());
		}

	}

	public int update(Object[] args) throws DAOException {
		return super.updateImpl(getFindSql(), args);
	}
	
	protected String getAuditDate() {
		
		String result = "";
		String year = this.year.toString();
		String month = this.month.toString().length() > 1 ? this.month.toString() : "0" + this.month.toString();
		String day = this.day.toString().length() > 1 ? this.day.toString() : "0" + this.day.toString();
		
		result = year + month + day;
		return result;
	}

	protected abstract void setAuditVar(Object vo); 
	
	protected abstract boolean checkDataIntegrity(Object vo, Object bean);
	
	protected abstract String getFindSql();
	
}
