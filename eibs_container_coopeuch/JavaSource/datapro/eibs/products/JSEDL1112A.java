package datapro.eibs.products;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.EDL111201Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.services.ExcelResultSet;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.services.ExcelXLSXResultSet;
import datapro.eibs.master.Util;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JSEDL1112A extends JSEIBSServlet {

	private ServletConfig config = null;
	
	protected static final int R_ENTER_FILE = 2;
	protected static final int A_ENTER_FILE = 1;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			// Request
			case R_ENTER_FILE :
				procReqImport(user, req, res, session);
				break;
			case A_ENTER_FILE :
				procReadExcelFile(user, req, res, session);
				break;
			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

	private void procReadExcelFile(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		File tmp = null;
		String PageToCall = "";
		//read file
		try {
			mp = getMessageProcessor("EDL1112", req);
			
			SmartUpload mySmartUpload = new SmartUpload();
			mySmartUpload.initialize(config, req, res);
			mySmartUpload.upload();
			com.jspsmart.upload.File file =  mySmartUpload.getFiles().getFile(0);
			
			InputStream xls = Util.getStreamFromObject(file);
			
			ResultSet rs = null;
			if (ExcelUtils.isXLSXVersion(file.getFilePathName())) {
				rs = new ExcelXLSXResultSet(xls, 0);
				((ExcelXLSXResultSet)rs).init();
			} else {
				rs = new ExcelResultSet(xls, 0);
				((ExcelResultSet)rs).init();
			}
			
			boolean error = false;
			int rows = 0;
			File fTxt = new File(JSEIBSProp.getImgTempPath(), "txt.tmp");
			FileOutputStream f = new FileOutputStream(fTxt); 
			while (rs.next()) {
//				EDL111201Message msg = (EDL111201Message) mp.getMessageRecord("EDL111201");
//	            msg.setH01PROGRM("EDL1112");
//	            msg.setH01USERID(user.getH01USR());
//	            msg.setH01TIMSYS(getTimeStamp());
//	            msg.setH01SCRCOD("01");
//	            msg.setH01OPECOD("0002");
//				ExcelUtils.populate(rs, msg);
//				mp.sendMessage(msg);
//				rows++;
				int l50=50;
				int l25=25;
				String col0 = rs.getString(0);
				char a = 0;
				col0 = (col0.length()>l50)?col0.substring(0, l50):Util.addRightChar(a,(l50-col0.length()),col0);
				String col1 = rs.getString(1);
				col1 = (col1.length()>l25)?col1.substring(0, l25):Util.justifyLeft(col1,(l25-col0.length()));
				//
				String linea = col0 + col1;
				f.write(linea.getBytes());//probar esto..				
			}
			f.flush();
			f.close();
			//hacer el ftp del archivo txt.tmp
			if (!error) {
				EDL111201Message msg = (EDL111201Message) mp.getMessageRecord("EDL111201");
	            msg.setH01PROGRM("EDL1112");
	            msg.setH01USERID(user.getH01USR());
	            msg.setH01TIMSYS(getTimeStamp());
	            msg.setH01SCRCOD("01");
	            msg.setH01OPECOD("0002");
				msg.setH01FLGMAS("*"); //Last Element List
				mp.sendMessage(msg);
				//	Receive Error Message
				msgError = (ELEERRMessage) mp.receiveMessageRecord();
				if (mp.hasError(msgError)) {
					PageToCall = "EDL1112_ln_transfer_file.jsp";
				} else {
					req.setAttribute("rows", String.valueOf(rows));
					PageToCall = "EDL1112_ln_transfer_file_confirm.jsp";
				}
			}
			
			flexLog("Putting java beans into the session");
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			forward(PageToCall, req, res);
			
		} catch (Exception e) {
			flexLog("Exception: " + e.getClass().getName() + " -- Error: " + e.getMessage());
			throw new ServletException(e);
		} finally {
			if (tmp != null) tmp.delete(); 
			if (mp != null)	mp.close();
		}
	}
	
	private void procReqImport(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		
		forward("EDL1112_ln_transfer_file.jsp", req, res);
	}

}
