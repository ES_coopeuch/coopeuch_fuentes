/**
 * Salida.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public class Salida  implements java.io.Serializable {
    private int codigoRetorno;
    private java.lang.String glosaRetorno;
    private _125._62._21._10.ElementoSalida[] resultado;
    private _125._62._21._10.RolDescuento rolDescuento;

    public Salida() {
    }

    public int getCodigoRetorno() {
        return codigoRetorno;
    }

    public void setCodigoRetorno(int codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
    }

    public java.lang.String getGlosaRetorno() {
        return glosaRetorno;
    }

    public void setGlosaRetorno(java.lang.String glosaRetorno) {
        this.glosaRetorno = glosaRetorno;
    }

    public _125._62._21._10.ElementoSalida[] getResultado() {
        return resultado;
    }

    public void setResultado(_125._62._21._10.ElementoSalida[] resultado) {
        this.resultado = resultado;
    }

    public _125._62._21._10.ElementoSalida getResultado(int i) {
        return resultado[i];
    }

    public void setResultado(int i, _125._62._21._10.ElementoSalida value) {
        this.resultado[i] = value;
    }

    public _125._62._21._10.RolDescuento getRolDescuento() {
        return rolDescuento;
    }

    public void setRolDescuento(_125._62._21._10.RolDescuento rolDescuento) {
        this.rolDescuento = rolDescuento;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        Salida other = (Salida) obj;
        boolean _equals;
        _equals = true
            && this.codigoRetorno == other.getCodigoRetorno()
            && ((this.glosaRetorno==null && other.getGlosaRetorno()==null) || 
             (this.glosaRetorno!=null &&
              this.glosaRetorno.equals(other.getGlosaRetorno())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        Salida history = (Salida) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.resultado==null && other.getResultado()==null) || 
             (this.resultado!=null &&
              java.util.Arrays.equals(this.resultado, other.getResultado())))
            && ((this.rolDescuento==null && other.getRolDescuento()==null) || 
             (this.rolDescuento!=null &&
              this.rolDescuento.equals(other.getRolDescuento())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        Salida history = (Salida) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        _hashCode += getCodigoRetorno();
        if (getGlosaRetorno() != null) {
            _hashCode += getGlosaRetorno().hashCode();
        }
        if (getResultado() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResultado());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResultado(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRolDescuento() != null) {
            _hashCode += getRolDescuento().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
