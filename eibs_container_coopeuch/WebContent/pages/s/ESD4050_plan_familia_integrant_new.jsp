<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<%@page import="datapro.eibs.sockets.MessageRecord"%>
<%@page import="datapro.eibs.beans.ESD405001Message"%>

<html>
<head>
<title>Nuevo Plan Familia</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ESD405001List" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "planFamilia" class= "datapro.eibs.beans.ESD405001Message"  scope="session" />
<jsp:useBean id= "ListaVinculo" class= "datapro.eibs.beans.JBList"  scope="session" />
<jsp:useBean id= "valorPlan" class= "java.lang.String"  scope="session" />

<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<!--<jsp:useBean id= "servlet" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "paginaPS" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "operation" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "IDuser" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "NOBuser" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "banderaCV" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "banderaLH" class= "java.lang.String"  scope="session" />-->

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<SCRIPT Language="Javascript">

//  Process according with user selection
 var bandera;
 function goAgregarPersonasRelacionadas(){
 	self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4030?SCREEN=500";
 }
 	
 function goAction(op) {
	
	switch (op){
	// Validate & Write 
  	case 1:  {
    	document.forms[0].APPROVAL.value = 'N';
       	break;
        }
	// Validate and Approve
	case 2:  {
		if(document.forms[0].CANTIDAD.value != "1"){
			for (i = 0; i < document.forms[0].CANTIDAD.value; i++){
				if (document.forms[0].ROW[i].checked){
					document.forms[0].RUT.value = document.forms[0].ROW[i].value.split(",")[0];
					document.forms[0].IDPLANSOCIOTITULAR.value = document.forms[0].ROW[i].value.split(",")[1];
				}
			}
		}else if(document.forms[0].CANTIDAD.value == "1"){
			if (document.forms[0].ROW.checked){
				document.forms[0].RUT.value = document.forms[0].ROW.value.split(",")[0];
				document.forms[0].IDPLANSOCIOTITULAR.value = document.forms[0].ROW.value.split(",")[1];
			}
		}
		if (document.forms[0].RUT.value != ""){
			document.forms[0].SCREEN.value = "600";
			document.forms[0].submit();
 		}else{
 			alert("Debe marcar una invitacion");
 		}
       	break;
		}
	case 3:{
		document.forms[0].SCREEN.value = "200";
	 	document.forms[0].submit();
		break;
		}
	}
}

function guardarRut(obj){
	document.forms[0].RUT.value = "";
	document.forms[0].RUT.value = obj.value.split(",")[0];
	document.forms[0].IDPLANSOCIOTITULAR.value = obj.value.split(",")[1];
}

</SCRIPT>
</head>

<body bgcolor="#FFFFFF">

<h3 align="center">Integrante de Plan Familia<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="plan_familia_new, ESD4050"  ></h3>
<hr size="4">
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4050" >
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
<INPUT TYPE=HIDDEN NAME="APPROVAL" VALUE="N">
<input type="hidden" name="E01FL5" size="2" maxlength="1" value="<%= client.getE01FL5().trim()%>">
<input type="hidden" name="E01LGT" size="2" maxlength="1" value="<%= client.getE01LGT().trim()%>">
<input type="hidden" name="E01CUN" size="2" maxlength="1" value="<%= userPO.getCusNum()%>">
<input type="hidden" name="E01IDN" size="2" maxlength="1" value="<%= userPO.getHeader2()%>">
<INPUT TYPE=HIDDEN NAME="IDPLANSOCIO" VALUE="<%= planFamilia.getE01PEST()%>">
<INPUT TYPE=HIDDEN NAME="IDPLANSOCIOTITULAR" VALUE="">
<INPUT TYPE=HIDDEN NAME="RUT" VALUE="">
<br>
<h4 align="center">Datos del socio Titular</h4>
<table class="tableinfo">
	<tr > 
		<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">Cliente: </td>
					<td nowrap width="12%" aling="left" > <%=userPO.getCusNum() %> </td>
							
					<td nowrap width="6%" aling="right">RUT: </td>
					<td nowrap width="14%" aling="left" > <%=userPO.getHeader2() %> </td>
										
					<td nowrap width="6%" aling="right">ID Plan Socio: </td>
					<td nowrap width="14%" aling="left" >  <%=planFamilia.getE01PEST() %> </td>
										
					<td nowrap width="8%" aling="right">Nombre: </td>
					<td nowrap width="20%" aling="left" > <%=userPO.getHeader3() %> </td>
				</tr>
			</table> 
		</td>
	</tr>
</table>
<br>
<h4 align="center">Aceptar Invitaci�n de Uni�n de Planes</h4>
<%
if ( (!ESD405001List.isEmpty() ) ) {
 %>
<table  id=cfTable class="tableinfo">
	<tr>
		<td NOWRAP valign="top" width="100%">
			<table id="headTable" width="100%">
				<tr id="trdark">
					<th align=center nowrap width="15%">N� Plan</th>
					<th align=center nowrap width="70%"> Titular del Plan</th>
					<th align=center nowrap width="15%"> Aceptar Plan</th>
				</tr>
				<%
     					
     						
    	  					int row = 0;
    	  					int i = 0;
    	  					ESD405001List.initRow();
          					while (ESD405001List.getNextRow()) {
          						String cadena ="";
            					ESD405001Message msgList = (ESD405001Message) ESD405001List.getRecord();
            					valorPlan = msgList.getL01FILLE4();%>
            					
            					<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>" >
     								<td NOWRAP align=center> <%= msgList.getE01PEST() %> </td>
     								<td NOWRAP align=center> <%= msgList.getE01PSNM() %> </td>
     								<td NOWRAP align="center" width="5%">
     								<%if(!msgList.getE01PUNP().equals(("S"))){ %>
     									<input type="radio" name="ROW" value="<%= msgList.getE01PIPS()+ ',' + msgList.getE01PEST()%>"  onclick="guardarRut(this);">
     								<% }else{ %>
     									<input type="radio" name="ROW" value="<%= msgList.getE01PIPS()+ ',' + msgList.getE01PEST()%>" checked onclick="guardarRut(this);">
     								<%} %>
     								</td>     								
     							</tr>
     							
     							<%  i++; 
     						} %>			
			</table>
			<input type="hidden" name="CANTIDAD" size="2" maxlength="10" value="<%= row%>">
		</td>
	</tr>
</table>
<%}else{ %>
<table class="tbenter" width=100% align=center>
	<tr>
		<td class= width="20%">
			<div align="center"><b>No hay registros</b></div>
		</td>
	</tr>	
</table>
<%} %>
<br>
<table width="100%">		
	<tr>
		<td width="15%">
			<div align="center"> 
				<input id="EIBSBTN" type="button" name="Submit" value="Volver" onClick="javascript:goAction(3);">
			</div>	
		</td>
		<td width="75%">
			<div align="center">
				<input id="EIBSBTN" type="button" name="Submit2" value="Enviar" onClick="javascript:goAction(2);">
			</div>	
		</td>
	</tr>	
</table>	



</form>
</body>
</html>

