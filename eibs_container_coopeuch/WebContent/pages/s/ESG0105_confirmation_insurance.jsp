<%//P�gina hecha por Alonso Arana ---Datapro ---- 24/10/2013 %>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ESG010501Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Confirmaci�n de renuncia de Seguros</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ESG1050list" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>	
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

  
  $(function(){
					$("#EIBSBTN").click(function(){
    if($('#headTable').find('input[type=checkbox]:checked').length == 0)
    {
        alert('Seleccionar un seguro a retractar');
        
        return false;
    }
});
				
                
});
  


var flag = 1;
var estado=0;
var seguros=0;

function modificar_boxes(chk){

	cuantos=0;
	for (i=0; i < seguros; i++ )
	{
   		cod_seg=document.forms[0].codigo_seguro[i].value.substring(0, document.forms[0].codigo_seguro[i].value.indexOf('@'));

	   	if (cod_seg==105 && document.forms[0].codigo_seguro[i].checked==true) 
      		 cuantos++;
	}
	
	if(cuantos>0 && chk.checked==true)
	{
    	alert("Solo es posible retractar seguros de avales en forma unitaria.");
    	chk.checked=false;
	}
	
	else 
	{
		if (flag==1)
		{
 			 estado=1;
  			 flag = 2;  
		}
		else
 		{ 
  			estado=0;
  			flag = 1;    
 		}
		for (i=0; i < seguros ; i++ )
		{
   			cod_seg=document.forms[0].codigo_seguro[i].value.substring(0, document.forms[0].codigo_seguro[i].value.indexOf('@'));
   
   			if (cod_seg==102 || cod_seg==103 || cod_seg==104 || cod_seg==122 || cod_seg==123 || cod_seg==124 || cod_seg==125)
    		{
     			document.forms[0].codigo_seguro[i].checked=estado;
			}  
		}
	}
}


function validar(chk){

	cuantos=0;
	for (i=0; i < seguros; i++ )
	{
   		cod_seg=document.forms[0].codigo_seguro[i].value.substring(0, document.forms[0].codigo_seguro[i].value.indexOf('@'));

	   	if (cod_seg==105 && document.forms[0].codigo_seguro[i].checked==true) 
      		 cuantos++;
	}
	
	if(cuantos>0 && chk.checked==true)
	{
    	alert("Solo es posible retractar seguros de avales en forma unitaria.");
    	chk.checked=false;
	}
	else
	{
		for (i=0; i < seguros; i++ )
		{
   			cod_seg=document.forms[0].codigo_seguro[i].value.substring(0, document.forms[0].codigo_seguro[i].value.indexOf('@'));
   
  			if ((cod_seg==107 || cod_seg==121) && document.forms[0].codigo_seguro[i].checked==true)
       			chk.checked=true;
		}
	}
}

function ValMype(chk)
{
    OtrosSec=0;
	cuantos=1;
	for (i=0; i < seguros; i++ )
	{
   		cod_seg=document.forms[0].codigo_seguro[i].value.substring(0, document.forms[0].codigo_seguro[i].value.indexOf('@'));

	   	if (cod_seg==105 && document.forms[0].codigo_seguro[i].checked==true) 
    	{  
      		if(cuantos>1)	  
       		{
       			alert("Debe retractar un aval a la vez.");
       			chk.checked=false;
       		}
      		 cuantos++;
		}	
	
		if(cod_seg!=105 && document.forms[0].codigo_seguro[i].checked==true)
       		OtrosSec=1;
	}
	
	if(OtrosSec==1 && cuantos>1)
	{
    	alert("Solo es posible retractar seguros de avales en forma unitaria.");
    	chk.checked=false;
    }
}

</script>
</head>
<body>
<h3 align="center">Confirmaci�n de Retracto de Seguros<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="confirmation_insurance.jsp,ESG0105"></h3>
<hr size="4">

 <h4>Datos del Cliente </h4>
 <table  class="tableinfo">
    <tr> 
  	 <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Nombre : 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="userPO" property="cusName" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">Identificaci�n :  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="ID" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>

<table>
</table>

<h4>Datos de la Cuenta </h4>
 
<table  class="tableinfo">
    <tr> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Cuenta : 
              </td>
             <td nowrap width="20%" align="left">
	 			<%=request.getAttribute("cuenta_sel") %>
	 		 </td>
             <td nowrap width="10%" align="right"> 	 
		           Vigencia desde:
             </td>
             <td nowrap width="20%" align="left">
				   <%=request.getAttribute("fecha_vigencia") %>
             </td>
             <td nowrap width="10%" align="right"> 	 
		         Monto:
             </td>
             <td nowrap width="20%" align="left">
			    <%=request.getAttribute("MontoCr�dito") %>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>

 <h4>Datos de la Compa�ia: </h4>
 <table  class="tableinfo">
    <tr> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="left"> Compa�ia : 
	              <%=request.getAttribute("nombre_compania") %> 
              </td>
             <td nowrap width="10%" align="left"> 
    	        C�digo de compa�ia:
        	    <%=request.getAttribute("rut_compania") %>  
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>


<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESG0010">
<input type="hidden" name="SCREEN" value="502">
<input type="hidden" name="cusNum" value="<%=userPO.getCusNum() %>" >
<input type="hidden" name="cuenta"  value="<%=request.getAttribute("cuenta_sel")  %>" >

 <h4>Datos Seguros : </h4>
 	
 	<table id="headTable" width="100%" align="left">
		<tr id="trdark">
			<th align="center" nowrap width="5%"></th>
			<th align="center" nowrap width="20%">Seguro</th>
			<th align="center" nowrap width="10%">Prima </th>
			<th align="center" nowrap width="10%">Devoluci�n</th>
			<th align="center" nowrap width="10%">Porcentaje (%)</th>
			<th align="center" nowrap width="15%">RUT Aval</th>
			<th align="center" nowrap width="30%"></th>
		</tr>
 
 	<%
		ESG1050list.initRow();
		boolean firstTime = true;
		String cta = (String)request.getAttribute("cuenta_sel");
		String nbr = (String)request.getAttribute("id_compania");
		int flg = 0; 

		String chk = "";
		int seg = 0;
				
		while (ESG1050list.getNextRow()) 
		{
			ESG010501Message convObj = (ESG010501Message)ESG1050list.getRecord();
			if (nbr.equals(convObj.getE01SRECIA())&& cta.equals(convObj.getE01SREPAC())) 
			{
               seg=seg + 1;
		%>
	           <tr>
					<td nowrap>
					   <input type="checkbox" name="codigo_seguro" id="codigo_seguro"  Value="<%=convObj.getE01SRECOD()%>@<%=convObj.getE01SRENUM()%>@<%=convObj.getE01SRESOC()%> "
				   		<% 
				   			if(convObj.getE01SRECOD().equals("107") || convObj.getE01SRECOD().equals("121") )
				   				out.print("onClick=modificar_boxes(this)");
		  				   	else
					   			if(convObj.getE01SRECOD().equals("105"))
					   				out.print("onClick=ValMype(this)");
					   			else
					    			out.print("onClick=validar(this)");
				    	%>
					    >
					</td>

					<td nowrap align="left">
						<%=Util.formatCell(convObj.getE01SREDSC())%>
					</td>
					<td nowrap align="right">
						<%=Util.formatCell(convObj.getE01SREMTP())%>
					</td>			
					<td nowrap align="right">
					    <%=Util.formatCell(convObj.getE01SRESOC())%>
					</td>
					<td nowrap align="center">
					    <% if(!convObj.getE01SREFRY().equals("")) 
					    		out.print(Util.formatCell(convObj.getE01SREFRY()));
					    %>
					</td>
					<td nowrap align="left">
					    <% if(!convObj.getE01SRERMK().equals("")) 
					    		out.print(Util.formatCell(convObj.getE01SRERMK()));
					    %>
					</td>
					<TD>
					</TD>
			</tr>
		<%	}
		}%>

<script type="text/javascript">
<% out.print("seguros = " + seg + ";"); %>
</script>
<tr align="center">
		<td nowrap colspan="7" align="center">
		<div id="DIVSUBMIT" align="center">
			<input id="EIBSBTN" type="submit" name="Submit" value="Enviar" ></div>
		</td>
</tr>
</table>
</form>
</body>
</html>


