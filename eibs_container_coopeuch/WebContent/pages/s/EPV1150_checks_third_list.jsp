<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV115001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV115001List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">

function showChecking(idx) {
	for ( var i=0; i<dataTable.rows.length; i++ ) {
       dataTable.rows[i].className="trnormal";
	}
    dataTable.rows[idx].className="trhighlight";

}

function getRadioButtonSelectedValue(ctrl)
{
    for(i=0;i<ctrl.length;i++){
        if(ctrl[i].checked) return ctrl[i].value;
    }
    return ctrl.value
}

  function goAction(op) {
	var ok = false;
	var cun = "";
	var pg = "";

	if (op != '200'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01PVCSEQ") 
	      	{	
	      		if (element.checked == true) {
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = true;
      	
      	if (op =='202'){
      		confirm1 = confirm("Desea Eliminar el Cheque a Tercero seleccionado?");
      	}
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			if (op =='202'){//solo para eliminar..
			     <%//NOTA: solo para activar el check de la pagina integral%>
			     <%  String re =(String) session.getAttribute("EMPCT");%>
				 <%  if ("S".equals(re)){%>
						parent.setRecalculate3();	       
				 <% } %> 			
				document.forms[0].submit();				
			}else{
				dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1150?SCREEN="+document.forms[0].SCREEN.value+
					  "&cusNum="+document.forms[0].cusNum.value+"&Header23="+document.forms[0].Header23.value;					 	
//				if (op =='201' || op =='203'){
				if (op =='201'){				
					  dir = dir +  "&E01PVCSEQ="+getRadioButtonSelectedValue(document.forms[0].E01PVCSEQ);		
				}					  				
				CenterWindow(dir,820,350,2);
			}		
		}		

     } else {
		alert("Debe seleccionar un numero de Cheque a Tercero para continuar.");	   
	 }
      
	}
	
function GetInqDet(col)
{
	dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1150?SCREEN=203&cusNum="+document.forms[0].cusNum.value+"&Header23="+document.forms[0].Header23.value;					 						  		
	dir = dir + "&E01PVCSEQ="+col;
	CenterWindow(dir,820,500,2);
}	
</script>

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<% session.setAttribute("E01TOTCHK","0"); %> 

 <% String emp = (String)session.getAttribute("EMPCT");
 	emp = (emp==null)?"":emp;//si es blanco viene llamado por menu, sino viene llamdo desde la pantalla EPV1010
 %>
 <%if ("".equals(emp)){ %>
<h3 align="center">Listado de Cheques a Terceros<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="checks_third.jsp,EPV1150"></h3>
<hr size="4">
<%}%>
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1150">
<input type="hidden" name="SCREEN" value="201"> 
<input type=HIDDEN name="totalRow" value="0">
	
<%if ("".equals(emp)){ %>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Propuesta : 
               </td>
             <td nowrap width="50%"align="left">
   		        <input type="text" name="Header23" size="13" maxlength="12" value="<%= userPO.getHeader23()%>" readonly>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
<%}else{%>
	    <input type="hidden" name="cusNum" size="13" maxlength="12" value="<%= userPO.getCusNum()%>" readonly>		
    	<input type="hidden" name="Header23" size="13" maxlength="12" value="<%= userPO.getHeader23()%>" readonly>
<%}%>  

<%if (!"I".equals(emp)){ %>	    		 
<table class="tbenter" width="100%">
	<tr>
 		<td align="center" class="tdbkg" width="20%"><a href="#"
			onclick="goAction('200')"> <b>Crear</b> </a></td> 
			<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('201')"> <b>Modificar</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
    		href="javascript:goAction('202')"> <b>Borrar</b> </a></td>
		<%if ("".equals(emp)){ %>	    		 
				<td align="center" class="tdbkg" width="20%"><a
					href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
				</td>
		<%}%>		
	</tr>
</table>
<%}%>  

<%
	if (EPV115001List.getNoResult()) {
%>
	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="2%">&nbsp;</th>
			<th align="center" nowrap width="5%">Sec</th>						
			<th align="center" nowrap width="15%">Rut</th>			
			<th align="center" nowrap width="30%">Nombre</th>
			<th align="center" nowrap width="10%">Sucursal</th>
			<th align="center" nowrap width="15%">Monto</th>	
		</tr>
	</table>
	<br>
	<table class="tbenter" width=100%>
		<tr>
			<td>
			<div align="center">
				<font size="3">
					<b> No existen cheques a terceros asociadas a la solicitud. </b>
				</font>
			</div>
			</td>
		</tr>
	</table>	
<%
	} else {
%>

	<table  id="mainTable" ALIGN=CENTER style="width:'95%'" height="68%" border="0">
		<tr height="5%">
		<TD NOWRAP width="100%" >
  				<TABLE id="headTable" width="97%" >
  					<TR id="trdark">  
						<th align="center" nowrap width="2%">&nbsp;</th>
						<th align="center" nowrap width="5%">Sec</th>						
						<th align="center" nowrap width="15%">Rut</th>			
						<th align="center" nowrap width="30%">Nombre</th>
						<th align="center" nowrap width="10%">Sucursal</th>
						<th align="center" nowrap width="15%">Monto</th>	
						<th align="center" nowrap width="20%"></th>	
          			</TR>
       			</TABLE>
      		</td>
		</tr>
		<tr height="95%">    
			<td NOWRAP width="100%">    		
   			    <div id="dataDiv1" class="scbarcolor" style="width:100%; height:100%; overflow:auto;">
    				<table id="dataTable" width="97%" >     						
								<%	double suma = 0;
									EPV115001List.initRow();
										int k = 0;
										boolean firstTime = true;
										String chk = "";
										while (EPV115001List.getNextRow()) {
											if (firstTime) {
												firstTime = false;
												chk = "checked";
											} else {
												chk = "";
											}
											EPV115001Message convObj = (EPV115001Message) EPV115001List.getRecord();
											suma += convObj.getBigDecimalE01PVCVAL().doubleValue();
								%>
								<tr id="dataTable<%= EPV115001List.getCurrentRow() %>">
									<td nowrap width="2%">
										<input type="radio" name="E01PVCSEQ"	value="<%=convObj.getE01PVCSEQ()%>" <%=chk%> 
											onClick="javascript:showChecking(<%= EPV115001List.getCurrentRow() %>);"/>
									</td>
									<td nowrap width="5%" align="center"><a href="javascript:GetInqDet('<%=convObj.getE01PVCSEQ()%>');"><%=Util.formatCell(convObj.getE01PVCSEQ())%></a></td>
									<td nowrap align="center"><a href="javascript:GetInqDet('<%=convObj.getE01PVCSEQ()%>');"><%=convObj.getE01PVCRUT()%></a></td>																		
									<td nowrap align="left"><a href="javascript:GetInqDet('<%=convObj.getE01PVCSEQ()%>');"><%=convObj.getE01PVCNME()%></a></td>
									<td nowrap align="right"><a href="javascript:GetInqDet('<%=convObj.getE01PVCSEQ()%>');"><%=convObj.getE01PVCBRN()%></a></td>
									<td nowrap align="right"><a href="javascript:GetInqDet('<%=convObj.getE01PVCSEQ()%>');"><%=convObj.getE01PVCVAL() %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
									</td>									
									
								</tr>
								<%
									}
								%>
								<tr>
									<td align="center" nowrap >&nbsp;</td>
									<td align="center" nowrap >&nbsp;</td>						
									<td align="center" nowrap >&nbsp;</td>
									<td align="center" nowrap >&nbsp;</td>
									<td nowrap align="center"><b>TOTAL : </b> </td>
									<td nowrap align="right"><b><%=datapro.eibs.master.Util.formatCCY(suma)%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
									<input type="hidden" name="E01TOTCHK" value="<%=datapro.eibs.master.Util.formatCCY(suma)%>" >
									<% session.setAttribute("E01TOTCHK",datapro.eibs.master.Util.formatCCY(suma)); %>           
									
									
									</td>
								</tr>								
					</table>
   				</div>
   			</TD>
		</TR>	
	</TABLE>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV115001List.getShowPrev()) {
					int pos = EPV115001List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1150?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV115001List.getShowNext()) {
					int pos = EPV115001List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1150?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>
</form>
<%
	if (!EPV115001List.getNoResult()) {
%> 
<SCRIPT language="JavaScript">
	showChecked("E01PVCSEQ");  
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(headTable, dataTable, dataDiv1,1,false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>
<%
	}
%>
</body>
</html>
