package com.datapro.eibs.security.access.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.exception.BranchNotFoundException;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.UserBranchNotFoundException;
import com.datapro.eibs.security.access.jdbc.bean.CNTRLDBA;
import com.datapro.eibs.security.bean.CNTRLDBANME;
import com.datapro.generic.access.DAOSuper;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.BeanParser;
import com.datapro.generic.beanutil.BeanSql;

/**
 * @author ogarcia
 *
 */
public class DAOUserBranch extends DAOSuper {

	private static Category cat =
		Category.getInstance(DAOUserBranch.class.getName());

	/**
	 * Constructor for DAOUserBranch.
	 * @param dbid
	 */
	public DAOUserBranch(String dbid) {
		super(dbid);
	}

	/**
	 * Constructor for DAOUserBranch.
	 */
	public DAOUserBranch() {
		super();
	}

	/**
	 * Constructor for DAOUserBranch.
	 * @param connection
	 */
	public DAOUserBranch(Connection connection) {
		super(connection);
	}

	/**
	 * Delete branch for a given user.
	 */
	public int delete(String uid) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM CNTRLDBA WHERE DBAUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

	/**
	 * Delete branch for a given user.
	 */
	public int delete(String uid, String bnk) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM CNTRLDBA WHERE DBAUSR=? AND DBABNK=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, bnk);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

	/**
	 * Delete branch for a given user.
	 */
	public int delete(String uid, String bnk, String brn, String all) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM CNTRLDBA WHERE DBAUSR=? AND DBABNK=? AND DBABRN=? AND DBAALL=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, bnk);
			ps.setString(3, brn);
			ps.setString(4, all);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

	/**
	 * Insert user's branch.
	 */
	public int insert(CNTRLDBA bean) throws DAOException {

		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.insertPrepareStatement();
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

	/**
	 * Insert user's branch.
	 */
	public void insert(BeanList list) throws DAOException {

		try {
			list.initRow();
			int result = 0;
			while (list.getNextRow()) {
				CNTRLDBA bean = (CNTRLDBA) list.getRecord();
			
				BeanSql bs = new BeanSql(bean);
				String sql = bs.insertPrepareStatement();
				PreparedStatement ps = cnx.prepareStatement(sql);
				bs.parsePrepareStatement(ps);
				ps.executeUpdate();
				ps.close();
			}

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

	}

	/**
	 * Get user record.
	 */
	public CNTRLDBA getUserBranch(String uid, String bnk, String brn, String all)
		throws UserBranchNotFoundException, DAOException {

		CNTRLDBA bean = null;

		try {
			String sql = "SELECT * FROM CNTRLDBA WHERE DBAUSR=? AND DBABNK=? AND DBABRN=? AND DBAALL=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, bnk);
			ps.setString(3, brn);
			ps.setString(4, all);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNTRLDBA();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} else {
				throw new UserBranchNotFoundException(
					"User branch not found exception(uid:" + uid + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return bean;

	}

	/**
	 * Get branches per bank.
	 */
	public BeanList getUserBranchesName(String uid)
		throws BranchNotFoundException, DAOException {

		BeanList list = null;

		try {
			String sql = "SELECT a.*, b.BRNNME FROM CNTRLDBA a, CNTRLBRN b" +
						 "WHERE a.DBAUSR=? AND a.DBABNK=b.BRNBNK AND a.DBABRN=b.BRNNUM AND a.DBAALL<>? " +
						 "UNION ALL SELECT a.*, 'ALL' AS BRNNME FROM CNTRLDBA a WHERE a.DBAUSR=? AND a.DBAALL=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, "*");
			ps.setString(3, "*");
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				CNTRLDBANME bean = new CNTRLDBANME();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new BranchNotFoundException(
					"Branch by user not found exception(userid: " + uid + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}

}
