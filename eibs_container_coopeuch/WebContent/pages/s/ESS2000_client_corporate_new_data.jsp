<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META name="GENERATOR" content="IBM WebSphere Page Designer V4.0 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<title>Corporate User</title>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<jsp:useBean id="cusdata" class="datapro.eibs.beans.ESS200001Message" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script language="Javascript1.1">
  function SelCuentaCli(CampoDest,Campo1,Campo2,Campo3,Cliente){
     if(Cliente =="" || Cliente=="0"){
        alert("No. de Cliente debe ser Seleccionado");
        return;
     }
     GetAccByClient(CampoDest,'','','',document.forms[0].E01EUSCUN.value)
  }
  
  function GeneratePassword(){
     if(confirm("Esta seguro que desea generar una nueva contrasena para este usuario")){
        //page = webapp + "/servlet/com.datapro.eibs.internet.JSESS2000?SCREEN=425&E01EUSUSR="+document.forms[0].E01EUSUSR.value;
        page = webapp + "/pages/s/ESS2000_personal_password_generated.jsp?E01EUSUSR="+document.forms[0].E01EUSUSR.value+"&E01EUSENT=" +document.forms[0].E01EUSENT.value;
        //page = webapp + "/servlet/com.datapro.eibs.internet.SilentPrintServlet";
        CenterWindow(page,600,450,1);
     }
  }
</script>
</head>

<body bgcolor="#FFFFFF">

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0") ;
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
 %> 
 
 
<h3 align="center">Usuario Corporativo Internet<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cliente_corporate_new_data, ESS2000" ></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/com.datapro.eibs.internet.JSESS2000" onsubmit="return FieldNotBlank(this)">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="101">
  <INPUT TYPE=HIDDEN NAME="E01EUSCTY" VALUE="1">
  <!-- INPUT TYPE=HIDDEN NAME="E01EUSCFM" VALUE="1" -->
  <INPUT TYPE=HIDDEN NAME="E01EUSTPT" VALUE="N">

  <h4>Informaci�n General</h4>
  <table class="tableinfo">
      <tr > 
        <td nowrap> 
          
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
          <tr id="trdark"> 
            <td nowrap width="40%" ><div align="right">ID Entidad :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" ID="MANDATORY" name="E01EUSUSR" size="12" maxlength="10" READONLY value="<%= cusdata.getE01EUSUSR().trim()%>">
                                    <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="absbottom" border="0"  ></div></td>
          </tr>
          <tr id="teclear"> 
            <td nowrap width="40%" ><div align="right">Usuario Administrador :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text"  READONLY  ID="MANDATORY" name="E01EUSENT" size="12" maxlength="10" value="<%= cusdata.getE01EUSENT().trim()%>">
                                    <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="absbottom" border="0"  ></div></td>
          </tr>
<%
if(userPO.getPurpose().equals("")){
%>           
          <tr id="trdark"> 
            <td nowrap width="40%" ><div align="right">Contrase�a Usuario Administrador :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="password" ID="MANDATORY" name="E01EUSACP" readonly size="26" maxlength="25" value="<%= cusdata.getE01EUSACP().trim()%>"> Autogenerada</div></td>
          </tr>
<%
}
else{
%>   
          <tr id="trdark"> 
            <td nowrap width="40%" ><div align="right">Contrase�a Usuario Administrador :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="password" name="E01EUSACP" readonly size="26" maxlength="25" value="<%= cusdata.getE01EUSACP().trim()%>">
            <a href="JavaScript:GeneratePassword()" ><img src="<%=request.getContextPath()%>/images/keygen.gif" ALT="Regenerar Password" align="absbottom" border="0"  ></a>
             </div> 
            </td>
          </tr>
<%
}
%>      
          <tr id="trdark"> 
            <td nowrap width="40%" ><div align="right">M�ltiples Administradores :</div></td>
            <td nowrap width="60%" ><div align="left">
              <% if(!cusdata.getH01FLGWK2().trim().equals("9")){ %>
               <SELECT NAME="E01EUSCFM">
                 <OPTION VALUE="1" <%if (cusdata.getE01EUSCFM().equals("1")) {out.print("selected"); }%>>NO</OPTION>
                 <OPTION VALUE="2" <%if (cusdata.getE01EUSCFM().equals("2")) {out.print("selected"); }%>>SI</OPTION>
               </SELECT>            
              <% }else{%>
                <input name="E01EUSCFM" VALUE="2" TYPE=HIDDEN>SI
              <% }%> 
             </div> 
            </td>
          </tr>
  
          <tr id="teclear"> 
            <td nowrap width="40%" ><div align="right">Estado Usuario Administrador :</div></td>
            <td nowrap width="60%" ><div align="left"><SELECT NAME="E01EUSSTS">
             <OPTION VALUE="1" <%if (cusdata.getE01EUSSTS().equals("1")) {out.print("selected"); }%>>ACTIVO</OPTION>
             <OPTION VALUE="2" <%if (cusdata.getE01EUSSTS().equals("2")) {out.print("selected"); }%>>INACTIVO</OPTION>
             <OPTION VALUE="3" <%if (cusdata.getE01EUSSTS().equals("3")) {out.print("selected"); }%>>SUSPENDIDO</OPTION>
             <OPTION VALUE="4" <%if (cusdata.getE01EUSSTS().equals("4")) {out.print("selected"); }%>>PENDIENTE ACTIVACION</OPTION>
             </SELECT></div></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="40%" ><div align="right">No. Cliente :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" ID="MANDATORY" name="E01EUSCUN" size="10" maxlength="9" value="<%= cusdata.getE01EUSCUN().trim()%>" <%= cusdata.getH01FLGWK2().trim().equals("9")?"READONLY":"" %>>
            						<% if(!cusdata.getH01FLGWK2().trim().equals("9")){ %>
                                    <a href="javascript:GetCustomerDescId('E01EUSCUN','E01EUSCON','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="help" align="absbottom" border="0" ></a>
                                    <% } %>
                                    <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="absbottom" border="0"  ></div></td>
          </tr>
          <tr id="teclear"> 
            <td nowrap width="40%" ><div align="right">Cuenta Primaria :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" ID="MANDATORY" name="E01EUSACC" size="13" maxlength="12" value="<%= cusdata.getE01EUSACC().trim()%>" onkeypress="enterInteger()">
                                    <a href="javascript:SelCuentaCli('E01EUSACC','','','',document.forms[0].E01EUSCUN.value)"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a></div></td> 
          </tr>
          <tr id="trdark"> 
            <td nowrap width="40%" ><div align="right">Tipo Acumulaci�n :</div></td>
            <td nowrap width="60%" ><div align="left"> <SELECT NAME="E01EUSMXT">
             <OPTION VALUE="1" <%if (cusdata.getE01EUSMXT().equals("1")) {out.print("selected"); }%>>DIARIO</OPTION>
             <OPTION VALUE="2" <%if (cusdata.getE01EUSMXT().equals("2")) {out.print("selected"); }%>>SEMANAL</OPTION>
             </SELECT></div></td>
          </tr>
          <tr id="teclear"> 
            <td nowrap width="40%" ><div align="right">Monto L�mite Transferencias Internas:</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" name="E01EUSMAX" size="18" maxlength="16" value="<%= cusdata.getE01EUSMAX().trim()%>" <%= cusdata.getH01FLGWK2().trim().equals("9")?"READONLY":"" %> ></div></td>
          </tr>
           <tr id="trdark"> 
            <td nowrap width="40%" ><div align="right">Monto L�mite Acumulado Transferencias Internas:</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" name="E01EUSAMX" size="18" maxlength="16" value="<%= cusdata.getE01EUSAMX().trim()%>" <%= cusdata.getH01FLGWK2().trim().equals("9")?"READONLY":"" %> ></div></td>
          </tr>
          <tr id="teclear"> 
            <td nowrap width="40%" ><div align="right">Monto M�ximo Transferencias a Terceros :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" name="E01EUSTTL" size="18" maxlength="16" value="<%= cusdata.getE01EUSTTL().trim()%>" <%= cusdata.getH01FLGWK2().trim().equals("9")?"READONLY":"" %>></div></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="40%" ><div align="right">Monto M�ximo Acumulado Transferencias a Terceros :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" name="E01EUSTAL" size="18" maxlength="16" value="<%= cusdata.getE01EUSTAL().trim()%>" <%= cusdata.getH01FLGWK2().trim().equals("9")?"READONLY":"" %>></div></td>
          </tr>
          <tr id="teclear"> 
            <td nowrap width="40%" ><div align="right">Monto M�ximo Transferencias Externas :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" name="E01EUSETL" size="18" maxlength="16" value="<%= cusdata.getE01EUSETL().trim()%>" <%= cusdata.getH01FLGWK2().trim().equals("9")?"READONLY":"" %>></div></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="40%" ><div align="right">Monto M�ximo Acumulado Transferencias Externas :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" name="E01EUSEAL" size="18" maxlength="16" value="<%= cusdata.getE01EUSEAL().trim()%>" <%= cusdata.getH01FLGWK2().trim().equals("9")?"READONLY":"" %>></div></td>
          </tr>
          
          <tr id="teclear"> 
            <td nowrap width="40%" ><div align="right">Persona Responsable :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" name="E01EUSCON" size="60" maxlength="60" value="<%= cusdata.getE01EUSCON().trim()%>" <%= cusdata.getH01FLGWK2().trim().equals("9")?"READONLY":"" %>></div></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="40%" ><div align="right">Correo Electr�nico :</div></td>
            <td nowrap width="60%" ><div align="left"> <input type="text" ID="MANDATORY" name="E01EUSIAD" size="60" maxlength="60" value="<%= cusdata.getE01EUSIAD().trim()%>">
                                    <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="absbottom" border="0"  ></div></td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    
  <p align="center"> <input id="EIBSBTN" type=submit name="Submit" value="Enviar"> </p>
</form>
</body>
</html>
