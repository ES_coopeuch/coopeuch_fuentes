package datapro.eibs.plugins;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * @author fhernandez
 * 
 */
public class RequestFilter implements Filter {

	private final static Logger log = Logger.getLogger(RequestFilter.class);

	private FilterConfig config = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
		config = filterConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		if ((request instanceof HttpServletRequest)
				&& (response instanceof HttpServletResponse)) {
			
			verifySession((HttpServletRequest) request, 
					(HttpServletResponse) response);

		}
		if(false) filterRequestParameters(request);
		filterChain.doFilter(request, response);
	}
	
	private void filterRequestParameters(ServletRequest request) {
		Iterator i = request.getParameterMap().entrySet().iterator();
		while (i.hasNext()) {
			Map.Entry entry = (Map.Entry) i.next();
			String[] paramValues = (String[])entry.getValue();
			for (int j = 0; j < paramValues.length; j++) {
				StringBuffer outText = new StringBuffer();
				if (paramValues[j] != null) {
					for (int x = 0; x < paramValues[j].length(); x++) {
						char nx = paramValues[j].charAt(x);
						if ((nx > 43 && nx < 58) || (nx > 63 && nx < 91) || (nx > 96 && nx < 123) || (nx == 32) || (nx == 95)) {
							outText = outText.append(nx);
						}
					}
				}
				paramValues[j] = outText.toString();
			}
		}
		
	}

	private boolean isSessionControlRequiredForThisResource(
			HttpServletRequest httpServletRequest) {
		String requestPath = httpServletRequest.getRequestURI();
		String resource = requestPath.substring(httpServletRequest.getContextPath().length()+1);
		if (resource.indexOf("JSESS0030") > 0
			|| resource.indexOf("JSLogOff") > 0){
			return false;
		}
		if (resource.indexOf("pages") == 0) {
			resource = resource.substring("pages".length()+1);
		}
		if (resource.indexOf('/') > 0) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isSessionInvalid(HttpServletRequest httpServletRequest){
		boolean sessionInValid = 
			(httpServletRequest.getRequestedSessionId() != null
				&& !httpServletRequest.isRequestedSessionIdValid())
			&&  httpServletRequest.isUserInRole("roleEibsUser");
		return sessionInValid;
	}
	
	public boolean verifySession(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) 
		throws IOException {
		
		//is session expire control required for this request?
		if (isSessionControlRequiredForThisResource(httpServletRequest)) {
			// is session invalid?
			if (isSessionInvalid(httpServletRequest)) {
				log.info("session is invalid!" );
				if (false) {
					String timeoutUrl = 
						httpServletRequest.getContextPath() + "/" + config.getInitParameter("page_session_timeout");
					httpServletResponse.sendRedirect(timeoutUrl);
					log.info("Redirecting to timeoutpage : "
							+ timeoutUrl);
				} else {
					printLogInAgain(httpServletRequest, httpServletResponse);
				}
				return false;
			}
		}
		return true;
	}
	
	/**
	 * This method prints the error information.
	 * @param printStream PrintStream
	 * @param e Throwable
	 */
	public void printLogInAgain(ServletRequest request, ServletResponse response) throws IOException {

		String signOnMsg = "You must login into your session";		
		response.setContentType("text/html");		
		PrintWriter out = response.getWriter();
		out.println("<HTML>");
		out.println("<HEAD>");
		out.println("<TITLE>Close</TITLE>");
		out.println("</HEAD>");
		out.println("<SCRIPT LANGUAGE=\"JavaScript\">");
		out.println("  function LogIn() {");
		out.println("		alert('eIBS Message\\n" + signOnMsg + "');");
		out.println("		top.close()");
		out.println("  }");
		out.println("</SCRIPT>");
		out.println("<BODY onLoad=\"LogIn()\">");
		out.println("</BODY>");
		out.println("</HTML>");
		out.close();
		out.close();
		return;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub

	}


}