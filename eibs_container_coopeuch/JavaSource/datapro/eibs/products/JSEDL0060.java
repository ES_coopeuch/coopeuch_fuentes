package datapro.eibs.products;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDET05101Message;
import datapro.eibs.beans.EDL006001Message;
import datapro.eibs.beans.EDL006501Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ETG000000Message;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.sockets.MessageRecord;

public class JSEDL0060 extends JSEIBSServlet {

	protected static final int A_LINEAS_ESTATALES_LIST = 100;
	protected static final int A_LINEAS_ESTATALES_LIST_INQ = 110;
	protected static final int A_LINEAS_ESTATALES_LIST_INQEST = 120;
	protected static final int R_SELECT_LINEAS_ESTATALES = 200;
	protected static final int A_MAINTENANCE_LINEAS_ESTATALES = 300;
	protected static final int R_SELECT_SUB_LINEAS_ESTATALES = 400;
	protected static final int A_MAINTENANCE_SUB_LINEAS_ESTATALES = 500;
	protected static final int R_SELECT_COMUNAS_LINEAS_ESTATALES = 600;
	protected static final int A_MAINTENANCE_COMUNAS_LINEAS_ESTATALES = 700;


	
		protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case A_LINEAS_ESTATALES_LIST :
				procActLinEstList(user, req, res, session,  "MAINT");
				break;
			case A_LINEAS_ESTATALES_LIST_INQ :
				procActLinEstList(user, req, res, session, "INQ");
				break;				
			case A_LINEAS_ESTATALES_LIST_INQEST :
				procActLinEstListEst(user, req, res, session, "INQ");
				break;				
			case R_SELECT_LINEAS_ESTATALES :
				procSelLinEst(user, req, res, session);
				break;
			case A_MAINTENANCE_LINEAS_ESTATALES:
				procActMainLinEst(user, req, res, session);
				break;
			case R_SELECT_SUB_LINEAS_ESTATALES :
				procSelSubLinEst(user, req, res, session);
				break;
			case A_MAINTENANCE_SUB_LINEAS_ESTATALES:
				procActMainSubLinEst(user, req, res, session);
				break;
			case R_SELECT_COMUNAS_LINEAS_ESTATALES :
				procSelComunas(user, req, res, session);
				break;
			case A_MAINTENANCE_COMUNAS_LINEAS_ESTATALES:
				procActMainComunas(user, req, res, session);
				break;

			
			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

	private void procActLinEstList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String act ) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		userPO.setHeader1(act);
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDL0060", req);
			EDL006001Message msgList = (EDL006001Message) mp.getMessageRecord("EDL006001", user.getH01USR(), "0001");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("LN");
			
			req.setAttribute("E01MLNST1", "");
			req.setAttribute("E01MLNDS1", "");

			mp.sendMessage(msgList);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				forward("error_viewer.jsp", req, res);
			
			} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("listLinEst", list);
				session.setAttribute("userPO", userPO);
				
				forward("EDL0060_lineas_estatales_list.jsp", req, res);
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

	private void procActLinEstListEst(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String act ) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		userPO.setHeader1(act);
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDL0060", req);
			EDL006001Message msgList = (EDL006001Message) mp.getMessageRecord("EDL006001", user.getH01USR(), "0006");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("LN");
			msgList.setH01FLGWK2(req.getParameter("filestado"));

			if(req.getParameter("E01MLNST1")==null)
				req.setAttribute("E01MLNST1", "");
			else
				req.setAttribute("E01MLNST1", req.getParameter("E01MLNST1"));

			if(req.getParameter("E01MLNDS1")==null)
				req.setAttribute("E01MLNDS1", "");
			else
				req.setAttribute("E01MLNDS1", req.getParameter("E01MLNDS1"));
			

			mp.sendMessage(msgList);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				forward("error_viewer.jsp", req, res);
			
			} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("listLinEst", list);
				session.setAttribute("userPO", userPO);
				
				forward("EDL0060_lineas_estatales_list.jsp", req, res);
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

	private void procSelLinEst(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session ) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqMainLinEst(user, req, res, session);
				break;
			case 2 : //MAINT
				userPO.setPurpose("MAINT");
				session.setAttribute("userPO", userPO);
				procReqMainLinEst(user, req, res, session);
				break;
			case 3 : //Sub Linea
				 procActSubLinEstList(user, req, res, session);
				break;
			case 4 : //COMUNAS
				 userPO.setHeader2("");
				 procActComunasList(user, req, res, session);
				 break;
			case 5 : //INQ
				userPO.setPurpose("INQ");
				session.setAttribute("userPO", userPO);
				procReqMainLinEst(user, req, res, session);
				break;
			case 6 : //DELETE
				userPO.setPurpose("DELLIN");
				session.setAttribute("userPO", userPO);
				procDelete(user, req, res, session);
				break;				
			default : 
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

	private void procReqMainLinEst(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage(); 
		EDL006001Message msg =  null; 

		if(!userPO.getPurpose().equals("NEW")){
			JBObjList bl = (JBObjList) session.getAttribute("listLinEst");
			int idx = 0;
			try {
				idx = Integer.parseInt(req.getParameter("CURRCODEL").trim());
				bl.setCurrentRow(idx);
				msg = (EDL006001Message) bl.getRecord();
			} catch (Exception e) {
				msg = (EDL006001Message) session.getAttribute("msgLinEst");
			}
		}

			
		session.setAttribute("msgLinEst", msg);
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		
		forward("EDL0060_lineas_estatales_maint.jsp", req, res);
	}
	

	private void procActMainLinEst(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDL0060", req);
			EDL006001Message msgLinEst = (EDL006001Message) session.getAttribute("msgLinEst");
			msgLinEst.setH01USERID(user.getH01USR());
			msgLinEst.setH01PROGRM("EDL0060");
			msgLinEst.setH01TIMSYS(getTimeStamp());
			if(userPO.getPurpose().equals("MAINT"))
				msgLinEst.setH01OPECOD("0003");
			else
				msgLinEst.setH01OPECOD("0002");
			
			msgLinEst.setH01SCRCOD("LN");
			
			setMessageRecord(req, msgLinEst);
			
			mp.sendMessage(msgLinEst);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgLinEst = (EDL006001Message) mp.receiveMessageRecord("EDL006001");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("msgLinEst", msgLinEst);
				session.setAttribute("userPO", userPO);
				
				forward("EDL0060_lineas_estatales_maint.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				
				procActLinEstList(user, req, res, session, "");
			}
		} 

		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}
	
	private void procActSubLinEstList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		EDL006001Message msg = null;
		
		JBObjList bl = (JBObjList) session.getAttribute("listLinEst");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODEL").trim());
			bl.setCurrentRow(idx);
			msg = (EDL006001Message) bl.getRecord();
		} catch (Exception e) {
			msg = (EDL006001Message) session.getAttribute("msgLinEst");
		}
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDL0060", req);
			EDL006001Message msgList = (EDL006001Message) mp.getMessageRecord("EDL006001", user.getH01USR(), "0001");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setE01MLNSEQ(msg.getE01MLNSEQ());
			msgList.setH01SCRCOD("SL");
			
			mp.sendMessage(msgList);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				forward("error_viewer.jsp", req, res);
			
			} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("listSubLinEst", list);
				session.setAttribute("msgLinEst", msg);
				session.setAttribute("userPO", userPO);
				
				forward("EDL0060_sub_lineas_estatales_list.jsp", req, res);
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}	

	private void procSelSubLinEst(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqMainSubLinEst(user, req, res, session);
				break;
			case 2 : //MAINT
				userPO.setPurpose("MAINT");
				session.setAttribute("userPO", userPO);
				procReqMainSubLinEst(user, req, res, session);
				break;
			case 3 : //DELETE
				userPO.setPurpose("DELSUB");
				userPO.setHeader2("SUB");
				session.setAttribute("userPO", userPO);
				procDelete(user, req, res, session);
				break;
			case 4 : //COMUNAS
				 userPO.setHeader2("SUB");
				 procActComunasList(user, req, res, session);
				 break;
			case 5 : //INQ
				userPO.setPurpose("INQ");
				session.setAttribute("userPO", userPO);
				procReqMainSubLinEst(user, req, res, session);
				break;

			default : 
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}	
	
	private void procReqMainSubLinEst(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage(); 

		EDL006001Message msg = null;

		if(!userPO.getPurpose().equals("NEW")){
			JBObjList bl = (JBObjList) session.getAttribute("listSubLinEst");
			int idx = 0;
			try {
				idx = Integer.parseInt(req.getParameter("CURRCODESL").trim());
				bl.setCurrentRow(idx);
				msg = (EDL006001Message) bl.getRecord();
			} catch (Exception e) {
				msg = (EDL006001Message) session.getAttribute("msgSubLinEst");
			}
		}
		
		session.setAttribute("msgSubLinEst", msg);
		
		EDL006001Message msgLinEst = (EDL006001Message) session.getAttribute("msgLinEst");

		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		session.setAttribute("msgLinEst", msgLinEst);
		
		forward("EDL0060_sub_lineas_estatales_maint.jsp", req, res);
	}

	private void procActMainSubLinEst(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDL0060", req);
			EDL006001Message msgSubLinEst = (EDL006001Message) session.getAttribute("msgSubLinEst");
			msgSubLinEst.setH01USERID(user.getH01USR());
			msgSubLinEst.setH01PROGRM("EDL0060");
			msgSubLinEst.setH01TIMSYS(getTimeStamp());
			if(userPO.getPurpose().equals("MAINT"))
				msgSubLinEst.setH01OPECOD("0003");
			else 
				msgSubLinEst.setH01OPECOD("0002");
				
			msgSubLinEst.setH01SCRCOD("SL");
			
			setMessageRecord(req, msgSubLinEst);
			
			mp.sendMessage(msgSubLinEst);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgSubLinEst = (EDL006001Message) mp.receiveMessageRecord("EDL006001");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("msgSubLinEst", msgSubLinEst);
				session.setAttribute("userPO", userPO);
				
				forward("EDL0060_sub_lineas_estatales_maint.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				
				procActSubLinEstList(user, req, res, session);
			}
		} 

		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

	private void procActComunasList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		EDL006001Message msg=null;
		JBObjList bl = null;
		int idx = 0;
		
		if(userPO.getHeader2().equals(""))
			bl = (JBObjList) session.getAttribute("listLinEst");
		else
			bl = (JBObjList) session.getAttribute("listSubLinEst");

		try {
			if(userPO.getHeader2().equals(""))
				idx = Integer.parseInt(req.getParameter("CURRCODEL").trim());
			else
				idx = Integer.parseInt(req.getParameter("CURRCODESL").trim());

			bl.setCurrentRow(idx);
			msg = (EDL006001Message) bl.getRecord();
		} catch (Exception e) {
			if(userPO.getHeader2().equals(""))
				msg = (EDL006001Message) session.getAttribute("msgLinEst");
			else
			{	
				msg = (EDL006001Message) session.getAttribute("msgLinEst");
				session.setAttribute("msgSubLinEst", msg);
			}	
		}
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDL0060", req);
			EDL006001Message msgList = (EDL006001Message) mp.getMessageRecord("EDL006001", user.getH01USR(), "0001");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setE01MLNSEQ(msg.getE01MLNSEQ());
			msgList.setE01MLNIND(msg.getE01MLNIND());
			msgList.setH01SCRCOD("CM");
			
			mp.sendMessage(msgList);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				forward("error_viewer.jsp", req, res);
			
			} else {

				flexLog("Putting java beans into the session");
				session.setAttribute("listComunas", list);
				session.setAttribute("msgLinEst", msg);
				session.setAttribute("userPO", userPO);
				
				forward("EDL0060_comunas_lineas_estatales_list.jsp", req, res);
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}	

	private void procSelComunas(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqMainComunas(user, req, res, session);
				break;
			case 2 : //Delete
				userPO.setPurpose("DELCOM");
				session.setAttribute("userPO", userPO);
				procDelete(user, req, res, session);
				break;

			default : 
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}	
	
	private void procReqMainComunas(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage(); 
		MessageProcessor mp = null;
		EDL006501Message msg = null;

		
		EDL006001Message msgComunas = (EDL006001Message) session.getAttribute("msgComunas");
		EDL006001Message msgLinEst = (EDL006001Message) session.getAttribute("msgLinEst");

		mp = getMessageProcessor("EDL0060", req);
		EDL006001Message msgList = (EDL006001Message) mp.getMessageRecord("EDL006001", user.getH01USR(), "0006");
		msgList.setH01TIMSYS(getTimeStamp());
		msgList.setE01MLNSEQ(msgLinEst.getE01MLNSEQ());
		msgList.setE01MLNIND(msgLinEst.getE01MLNIND());
		msgList.setH01SCRCOD("CM");
		
		mp.sendMessage(msgList);
		
		msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
		JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");

		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		session.setAttribute("msgComunas", msgComunas);
		session.setAttribute("listComunas", list);
		
		forward("EDL0060_comunas_lineas_estatales_maint.jsp", req, res);
	}

	private void procActMainComunas(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
				mp = getMessageProcessor("EDL0060", req);
				EDL006001Message msgComunas = (EDL006001Message) session.getAttribute("msgComunas");
				msgComunas.setH01USERID(user.getH01USR());
				msgComunas.setH01PROGRM("EDL0060");
				msgComunas.setH01TIMSYS(getTimeStamp());
				msgComunas.setH01OPECOD("0002");
				msgComunas.setH01SCRCOD("CM");
			
				setMessageRecord(req, msgComunas);
			
				mp.sendMessage(msgComunas);
			
				ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msgComunas = (EDL006001Message) mp.receiveMessageRecord("EDL006001");
			
				if (mp.hasError(msgError)) 
				{
					flexLog("Putting java beans into the session");
					session.setAttribute("error", msgError);
					session.setAttribute("msgComunas", msgComunas);
					session.setAttribute("userPO", userPO);
				
					forward("EDL0060_comunas_lineas_estatales_maint.jsp", req, res);
				} else {
					flexLog("Putting java beans into the session");
				
					procReqMainComunas(user, req, res, session);    
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception calling page " + e);
			}
		
			finally {
				if (mp != null)	mp.close();
			}
	}

	private void procDelete(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		EDL006001Message msg=null;
		JBObjList bl = null;
		int idx = 0;
		
		if(	userPO.getPurpose().equals("DELCOM"))
			bl = (JBObjList) session.getAttribute("listComunas");
		else 		
			if(userPO.getHeader2().equals(""))
				bl = (JBObjList) session.getAttribute("listLinEst");
			else
				bl = (JBObjList) session.getAttribute("listSubLinEst");

		try {
			if(	userPO.getPurpose().equals("DELCOM"))
				idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
			else
			{	
			if(userPO.getHeader2().equals(""))
				idx = Integer.parseInt(req.getParameter("CURRCODEL").trim());
			else
				idx = Integer.parseInt(req.getParameter("CURRCODESL").trim());
			}
			bl.setCurrentRow(idx);
			msg = (EDL006001Message) bl.getRecord();
		} catch (Exception e) {
			if(userPO.getHeader2().equals(""))
				msg = (EDL006001Message) session.getAttribute("msgLinEst");
			else
			{	
				msg = (EDL006001Message) session.getAttribute("msgLinEst");
				session.setAttribute("msgSubLinEst", msg);
			}	
		}
		
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		
		try {
			mp = getMessageProcessor("EDL0060", req);
			EDL006001Message msgList = (EDL006001Message) mp.getMessageRecord("EDL006001", user.getH01USR(), "0004");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setE01MLNSEQ(msg.getE01MLNSEQ());
			msgList.setE01MLNIND(msg.getE01MLNIND());
			msgList.setE01MLNCCM(msg.getE01MLNCCM());
			msgList.setE01MLNGTZ(msg.getE01MLNGTZ());
			
			if(userPO.getPurpose().equals("DELCOM"))
				msgList.setH01SCRCOD("CM");
			else if(userPO.getPurpose().equals("DELSUB"))
					msgList.setH01SCRCOD("SL");
				 else 
					 msgList.setH01SCRCOD("LN");
			
			mp.sendMessage(msgList);
			
			msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msgList = (EDL006001Message) mp.receiveMessageRecord("EDL006001");
			
			if (mp.hasError(msgError)) {
				if(userPO.getPurpose().equals("DELCOM"))
					session.setAttribute("listComunas", bl);
					else if(userPO.getPurpose().equals("DELSUB"))
						session.setAttribute("listSubLinEst", bl);
					else 
						session.setAttribute("listLinEst", bl);
				
				session.setAttribute("msgLinEst", msg);
				session.setAttribute("userPO", userPO);
				session.setAttribute("error", msgError);

				if(userPO.getPurpose().equals("DELCOM"))
						forward("EDL0060_comunas_lineas_estatales_list.jsp", req, res);
					else if(userPO.getPurpose().equals("DELSUB"))
							forward("EDL0060_sub_lineas_estatales_list.jsp", req, res);
						 else 
							forward("EDL0060_lineas_estatales_list.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				if(userPO.getPurpose().equals("DELCOM"))
					procActComunasList(user, req, res, session);
				else if(userPO.getPurpose().equals("DELSUB"))
						procActSubLinEstList(user, req, res, session);
					 else 
						procActLinEstList(user, req, res, session,userPO.getHeader1());
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}	

	
	
//	
}
