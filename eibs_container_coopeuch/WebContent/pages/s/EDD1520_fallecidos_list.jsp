<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EDD152002Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>

<%@page import="java.math.BigDecimal"%><html>
<head>
<title>Nomina de Fallecidos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EDD152002List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script type="text/javascript">

</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Nomina de Fallecidos Cargada 
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="fallecidos_list.jsp,EDD1520"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD1520">
<input type="hidden" name="SCREEN" value="201"> 


<%
	if (EDD152002List.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%	} else  { %>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="100%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>

 <br>
	<table  id="mainTable" class="tableinfo" ALIGN=CENTER style="width:'100%'" height="70%">
    	<tr height="5%"> 
    		<TD NOWRAP width="100%" >
  				<TABLE id="headTable" width="100%" >
  					<TR id="trdark">  
						<th align="center" nowrap width="5%">Rut</th>
						<th align="center" nowrap width="5%">Fecha de Defunsion</th>																																
          			</TR>
       			</TABLE>
      		</td>
    	</tr>
		<tr height="95%">    
			<td NOWRAP width="100%">       
  
   			    <div id="dataDiv1" class="scbarcolor" style="width:100%; height:100%; overflow:auto;">
    				<table id="dataTable" width="100%" > 
		<%
			EDD152002List.initRow();
        	int cantidad = 0;			
			while (EDD152002List.getNextRow()) {
				EDD152002Message opmst = (EDD152002Message) EDD152002List.getRecord();
		%>
		<tr>
			<td nowrap align="center" width="5%"><%=opmst.getE02FALLRUT()%></td>			
			<td nowrap align="center" width="5%"><%=opmst.getE02FALLDIA()%>/<%=opmst.getE02FALLMES()%>/<%=opmst.getE02FALLANO()%></td>																		   																		   
		</tr>
		<%
		    	cantidad++; 		    
			}
		%>
	</table>
	</div>
	</td>
	</tr>
	</table>
  <table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			class="tbhead">
			<tr id="trdark">
				<td nowrap width="25%">
				<div align="right"><b>Total de Registros  :</b></div>
				</td>
			<td nowrap width="25%">
				<div align="left"><input type="text" 
					size="10" maxlength="9" value="<%=cantidad%>" readonly>
				</div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
	  
    				 
  <SCRIPT language="JavaScript">
		
			function resizeDoc() {
      		 	divResize();
     		    adjustEquTables(headTable, dataTable, dataDiv1,1,false);
      		}
	 		resizeDoc();   			
     		window.onresize=resizeDoc;        
     </SCRIPT>
  
     


<%}%>

  </form>

</body>
</html>
