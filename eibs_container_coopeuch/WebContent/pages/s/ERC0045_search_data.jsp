<!-- Created by Nicol�s Valeria ------Datapro----- 19/02/2015 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC200001Message"%>

<html>
<head>
<title>Sistema Bancario: Conciliaci�n Bancaria</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "data" class= "datapro.eibs.beans.ERC004501Message"  scope="session" />
<jsp:useBean id="listDebito" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="listCredito" class="datapro.eibs.beans.JBObjList" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<style type="text/css">
.headerTable{
	border-top-width : 1px;
	border-right-width : 1px;
	border-bottom-width : 1px;
	border-left-width : 1px;
	border-color: #990000;
	border-style : solid solid solid solid;
	width:100%;
}
.tablaData{
	font-family: "Verdana, Arial, Helvetica, sans-serif";
	font-size:8pt;
	background-color: #F0F0F0;
	border-color: red;
	color: #990000;
}
</style>
<script type="text/javascript">
var totalDebe = 0;
var totalHaber = 0;
var cantDebe = 0;
var cantHaber = 0;

function update(obj){
	if (obj.checked){
		if (obj.id == 'rowDebe'){
			totalDebe = parseInt(totalDebe) + parseInt(obj.value);
			document.getElementById('totalDebe').innerHTML = '<b>$ ' + totalDebe + '.00</b>';
			cantDebe = parseInt(cantDebe) + parseInt('1');
		}else {
			totalHaber = parseInt(totalHaber) + parseInt(obj.value);
			document.getElementById('totalHaber').innerHTML = '<b>$ ' + totalHaber + '.00</b>';
			cantHaber = parseInt(cantHaber) + parseInt('1');
		}
	} else {
		if (obj.id == 'rowDebe'){
			totalDebe = parseInt(totalDebe) - parseInt(obj.value);
			document.getElementById('totalDebe').innerHTML = '<b>$ ' + totalDebe + '.00</b>';
			cantDebe = parseInt(cantDebe) - parseInt('1');
		}else {
			totalHaber = parseInt(totalHaber) - parseInt(obj.value);
			document.getElementById('totalHaber').innerHTML = '<b>$ ' + totalHaber + '.00</b>';
			cantHaber = parseInt(cantHaber) - parseInt('1');
		}
	}
}

function goPage(){
	if (cantDebe == 0 && cantHaber == 0) {
			alert("Error. No hay transacciones seleccionadas.");
	}else if (confirm("Ha seleccionado " + cantDebe + " movimientos del Debe y " + cantHaber + " movimientos del Haber. \n\r �Desea usted continuar con esta selecci�n?")){
		if (document.getElementById('totalDebe').innerHTML == document.getElementById('totalHaber').innerHTML){
			document.forms[0].submit();
		}else {
			alert('Error. Los totales de las transacciones seleccionadas no son iguales');
		}
	}
}

</script>
</head>

<body>
 
<H3 align="center">Conciliaci&oacute;n IBS - IBS<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="search_data.jsp, ERC0045"></H3>

<hr size="4">
<br>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0045">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="300">
    <table width="100%" cellpadding="2" cellspacing="0" border="0" class="TABLEINFO" align="center">
    	<tbody style="width: 100%">
    		<tr style="width: 100%">
    			<td nowrap>
    				<table width="100%" cellpadding="2" cellspacing="0" border="0" class="TBHEAD" align="center">
    					<tbody>
    						<tr class="TRDARK">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Banco :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01RCHRBK" size="5" maxlength="4" value="">
	       							<input type="text" name="E01DSCRBK" readonly="readonly" size="43" maxlength="43" >
    							</td>
    							<td>
    								<div align="right">
    									Fecha Desde :
    								</div>
    							</td>
    							<td>
    								<input style="vertical-align: middle" type="text" name="E01DESDDD" id="fecha1" size="3" maxlength="2" value="" readonly>
					                <input style="vertical-align: middle" type="text" name="E01DESDDM" id="fecha2" size="3" maxlength="2" value="" readonly>
					                <input style="vertical-align: middle" type="text" name="E01DESDDY" id="fecha3" size="5" maxlength="4" value="" readonly>
    							</td>
    						</tr>
    						<tr class="TRCLEAR">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Cuenta Banco :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01BRMCTA" size="23" maxlength="20" value="">
    							</td>
    							<td>
    								<div align="right">
    									Fecha Hasta :
    								</div>
    							</td>
    							<td>
    								<input style="vertical-align: middle" type="text" name="E01HASDDD" size="3" id="fecha4" maxlength="2" value="" readonly>
					                <input style="vertical-align: middle" type="text" name="E01HASDDM" size="3" id="fecha5" maxlength="2" value="" readonly>
					                <input style="vertical-align: middle" type="text" name="E01HASDDY" size="5" id="fecha6" maxlength="4" value="" readonly>
    							</td>
    						</tr>
    						<tr class="TRDARK">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Cuenta IBS :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01BRMACC" size="7" maxlength="7" value="">
    							</td>
    							<td>
    								<div align="right">
    									Opci&oacute;n :
    								</div>
    							</td>
    							<td>
    								<input type="radio" name="H01FLGWK3" id="DES_CON" checked="checked" value="C"> Conciliaci&oacute;n
    								<input type="radio" name="H01FLGWK3" id="DES_CON" value="D"> Desconciliaci&oacute;n
    							</td>
    						</tr>
    					</tbody>
    				</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    <br>
    <div style="display: block;width: 100%" >
    <table cellpadding="2" cellspacing="0" border="0" style="background-color: #ffffff; float: left;" width="49.5%" >
   		<tr style="width: 100%">
   			<td nowrap class="headerTable" style="width: 100%;text-align: center;"><b>D E B E</b></td>
   		</tr>
   		<tr class="headerTable">
   			<td style="width: 100%;text-align: center;">
   				<table id="headTable2" class="tbhead" cellspacing=0 cellpadding=2 width="100%" border="1">
					<tr id="trdark">
						<th nowrap align="center" width="5%">&nbsp;</th>
						<th nowrap align="center" width="15%">Fecha</th>
						<th nowrap align="center" width="10%">REF.</th>
						<th nowrap align="center" width="45%">Descripci�n</th>
						<th nowrap align="center" width="20%">Monto</th>
					</tr>
				</table>
   			</td>
   		</tr>
    </table>
    <table cellpadding="2" cellspacing="0" border="0" style="background-color: #ffffff; float: right;" width="49.5%">
   		<tr style="width: 100%">
   			<td nowrap class="headerTable" style="width: 100%;text-align: center;"><b>H A B E R</b></td>
   		</tr>
   		<tr class="headerTable">
   			<td style="width: 100%;text-align: center;">
   				<table id="headTable2" class="tbhead" cellspacing=0 cellpadding=2 width="100%" border="1">
					<tr id="trdark">
						<th nowrap align="center" width="5%">&nbsp;</th>
						<th nowrap align="center" width="15%">Fecha</th>
						<th nowrap align="center" width="10%">REF.</th>
						<th nowrap align="center" width="45%">Descripci�n</th>
						<th nowrap align="center" width="20%">Monto</th>
					</tr>
				</table>
   			</td>
   		</tr>
    </table>
    </div>
    <div style="width: 100%">
		<div class="tablaData" style="height: 300px;overflow: auto; margin-top: 0px; float: left;width: 49.5%">
			<table align="left" width="100%" border="0">
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowDebe" name="poto1" value="2" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowDebe" name="poto1" value="2" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowDebe" name="poto1" value="2" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowDebe" name="poto1" value="2" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowDebe" name="poto1" value="2" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowDebe" name="poto1" value="2" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowDebe" name="poto1" value="2" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowDebe" name="poto1" value="2" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="tablaData" style="width: 49.5%;float: right;overflow: auto;height: 300px;">
			<table align="right" width="100%" border="0">
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowhaber" name="potoo8" value="3" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowhaber" name="potoo8" value="3" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowhaber" name="potoo8" value="3" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowhaber" name="potoo8" value="3" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowhaber" name="potoo8" value="3" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowhaber" name="potoo8" value="3" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowhaber" name="potoo8" value="3" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr>
				<tr>
					<td nowrap align="center" width="5%">
						<input type="checkbox" id="rowhaber" name="potoo8" value="3" onclick="update(this)">
					</td>
					<td nowrap align="center" width="15%">
						<div>DD-MM-YYYY</div>
					</td>
					<td nowrap align="center" width="10%">
						<div>12345678</div>
					</td>
					<td nowrap align="center" width="45%">
						<div>12345678901234567890123456789012345</div>
					</td>
					<td nowrap align="right" width="20%">
						<div>999.000.000</div>
					</td>
				</tr> 
			</table>
		</div>
	</div>
	<table cellpadding="2" cellspacing="0" border="0" style="background-color: #ffffff" align="center" width="100%">
    	<tbody style="width: 100%">
    		<tr style="width: 100%">
    			<td class="headerTable" id="totalDebe" style="width: 49.5%;text-align: right;"><b>$ 0.00</b></td>
    			<td style="width: 1%"></td>
    			<td class="headerTable" id="totalHaber" style="width: 49.5%;text-align: right;"><b>$ 0.00</b></td>
    		</tr>
    	</tbody>
    </table>
    <br>
    
<p align="center">
	<input id="EIBSBTN" type="button" name="Submit" value="Enviar" onclick="goPage()">
</p>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
