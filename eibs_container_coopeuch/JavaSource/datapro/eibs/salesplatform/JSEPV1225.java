package datapro.eibs.salesplatform;

/*******************************************************************************************************************************/
/**  Creado por              :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  28/02/2013                                                                                      **/
/**  Objetivo                :  Mantenedor de Tabla de descuentos de Aval                                                       **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV122501Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;

public class JSEPV1225 extends JSEIBSServlet {

	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int A_PVAPD_LIST = 100;	
	protected static final int R_PVAPD_NEW = 200;
	protected static final int R_PVAPD_MAINT = 201;
	protected static final int R_PVAPD_DELETE = 202;	
	protected static final int R_PVAPD_INQUIRY = 203;
	protected static final int A_PVAPD_MAINT = 600;
	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
	    	flexLog("Screen..." + screen);
		switch (screen) {
		case A_PVAPD_LIST:
			procActionTablaPVAPDList(user, req, res, session);
			break;			
		case R_PVAPD_NEW:
			procReqNew(user, req, res, session);
			break;
		case R_PVAPD_MAINT:
			procReqMaintenance(user, req, res, session);
			break;
		case R_PVAPD_INQUIRY:
		  	procReqView(user, req, res, session);
			break;
		case A_PVAPD_MAINT:
			procActionMaintenance(user, req, res, session);
			break;
		case R_PVAPD_DELETE:
			procReqDelete(user, req, res, session);
			break;
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}
	/**
	 * procActionTablaPVAPDList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionTablaPVAPDList(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EPV1225", req);

			EPV122501Message msg = (EPV122501Message) mp.getMessageRecord("EPV122501");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());
						  
			//Sends message
		//	flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
 
			session.setAttribute("userPO", userPO);
			session.setAttribute("EPV122501List", list);
			forwardOnSuccess("EPV1225_PVAPD_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqTablaPVAPD: This Method show a single  TablaPVAPD either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqNew(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res,
			HttpSession session) 
		throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("NEW");
	
		try {
			mp = getMessageProcessor("EPV1225", req);
			
			//Creates the message with operation code depending on the option
			EPV122501Message msg = (EPV122501Message) mp.getMessageRecord("EPV122501");
			msg.setH01USERID(user.getH01USR());
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01OPECOD("0001");
							
			//Sets the number for maintenance and inquiry options
			if (req.getParameter("E01PVAFAC") != null) {
					msg.setE01PVAFAC(req.getParameter("E01PVAFAC"));
			}		
			//Send message
			//flexLog("mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV122501Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
					forward("EPV1225_PVAPD_maintenance.jsp", req, res);
			} else {
				//if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1225_PVAPD_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	protected void procReqMaintenance(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			EPV122501Message msgDoc = null;
			UserPos userPO = null;

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			userPO.setPurpose("MAINTENANCE");			

			// Receive Data
			try {
				JBObjList bl = (JBObjList) ses.getAttribute("EPV122501List");
				int idx = Integer.parseInt(req.getParameter("PVAFAC"));
				bl.setCurrentRow(idx);

				msgDoc = (EPV122501Message) bl.getRecord();
			
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("cnvObj", msgDoc);

				try {
					forward("EPV1225_PVAPD_maintenance.jsp?readOnly=false", req, res);

				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}

	/**
	 * procActionMaintenance
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1225", req);

			EPV122501Message msg = (EPV122501Message) session.getAttribute("cnvObj");			
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0005");							
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			//flexLog("mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV122501Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go back to list
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1225?SCREEN=100", res);
			} else {
				//if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				forward("EPV1225_PVAPD_maintenance.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqDelete
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDelete(
			ESS0030DSMessage user, 
			HttpServletRequest req,
			HttpServletResponse res, 
			HttpSession ses)
			throws ServletException, IOException {
		EPV122501Message msg = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1225", req);
			JBObjList bl = (JBObjList) ses.getAttribute("EPV122501List");
			int idx = Integer.parseInt(req.getParameter("PVAFAC"));
			bl.setCurrentRow(idx);
			msg = (EPV122501Message) bl.getRecord();
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0009");
			msg.setH01TIMSYS(getTimeStamp());
						
			//Send message
			//flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive Error and Data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV122501Message) mp.receiveMessageRecord();

			//Sets session with required data
			ses.setAttribute("userPO", userPO);
			ses.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//If there are no errors request the list again
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1225?SCREEN=100", res);				
			} else {
				//if there are errors show the list without updating
				ses.setAttribute("error", msgError);
				forward("EPV1225_PVAPD_maintenance.jsp?readOnly=true", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	protected void procReqView(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			EPV122501Message msgDoc = null;
			UserPos userPO = null;

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			// Receive Data
			try {
				JBObjList bl = (JBObjList) ses.getAttribute("EPV122501List");
				int idx = Integer.parseInt(req.getParameter("PVAFAC"));
				bl.setCurrentRow(idx);

				msgDoc = (EPV122501Message) bl.getRecord();
			
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("cnvObj", msgDoc);

				try {
					forward("EPV1225_PVAPD_maintenance.jsp?readOnly=true", req, res);

				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}
}
