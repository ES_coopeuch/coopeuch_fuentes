package datapro.eibs.helps;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.EWD0742DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JSEWD0742 extends JSEIBSServlet {

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		main(user, req, res, session);
	}

	private void main(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		String pageName = "";
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EWD0742", req);
			EWD0742DSMessage msgHelp = (EWD0742DSMessage) mp.getMessageRecord("EWD0742DS");
			
			mp.sendMessage(msgHelp);
			
			JBObjList list = mp.receiveMessageRecordList("EWDOPE");
			
			if (mp.hasError(list)) {
				ELEERRMessage msgError = (ELEERRMessage) mp.getError(list);
				session.setAttribute("error", msgError);
				pageName = "error_viewer.jsp";
			} else {
				JBList beanList = new JBList();
				StringBuffer myRow = null;
				String idn = "";
				list.initRow();
				while (list.getNextRow()) {
					msgHelp = (EWD0742DSMessage) list.getRecord();
					
					myRow = new StringBuffer("<TR>");
					myRow.append(
						"<td nowrap align=\"center\"><a href=\"javascript:enter('"
							+ msgHelp.getEWDFCD()
							+ "')\">"
							+ msgHelp.getEWDFCD()
							+ "</a></td>");
					myRow.append(
						"<td nowrap align=\"center\"><a href=\"javascript:enter('"
							+ msgHelp.getEWDFCD()
							+ "')\">"
							+ msgHelp.getEWDFTY()
							+ "</a></td>");
					myRow.append(
						"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getEWDFCD()
							+ "')\">"
							+ msgHelp.getEWDDSC()
							+ "</a></td>");
					myRow.append(
						"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getEWDFCD()
							+ "')\">"
							+ msgHelp.getEWDPTH()
							+ "</a></td>");

					myRow.append("</TR>");
					beanList.addRow("", myRow.toString());
				}
				
				session.setAttribute("ewd0742Help", beanList);
				pageName = "EWD0742_forms_code_helpmessage.jsp";
			}	
			
            forward(pageName, req, res);
			
		} finally {
			if (mp != null) mp.close();
		}
	}

}
