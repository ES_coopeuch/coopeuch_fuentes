<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Mantenimiento de PAC de Seguros</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.ESG001001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

//  Process according with user selection
 function goAction(op) {
	
   	switch (op){
	//Cancel
	case 1:  {
 		document.forms[0].SCREEN.value = "101";
       	break;
		}
	}
	document.forms[0].submit();
 }
 

function setModoRec(){
       
       var ModRec = document.forms[0].E01PACRCD.value;
    
       if(ModRec=="2")
   		{
				document.forms[0].E01PACPAC.disabled=false;
				document.getElementById('lupa1').style.visibility = 'visible';
		}
       if(ModRec=="5")
   		{
				document.forms[0].E01PACPAC.value='0';
				document.forms[0].E01PACPAC.disabled=true;
				document.getElementById('lupa1').style.visibility = 'hidden';
   		 }
}
function SeguroFraude()
{
	var codigo = document.getElementById("E01PACCOD").value;
	
	if(codigo == "SFRA")
	{
	    
	 document.getElementById("dataSeguroFI").style.visibility = 'hidden';
	 document.getElementById("dataSeguroFV").style.visibility = 'hidden';
	    
		
		document.forms[0].E01PACFID.readOnly = true;
		document.forms[0].E01PACFIM.readOnly = true;
		document.forms[0].E01PACFIY.readOnly = true;
		document.forms[0].E01PACFVY.readOnly = true;
		document.forms[0].E01PACFVM.readOnly = true;
		document.forms[0].E01PACFVD.readOnly = true;
	}else
	{
		
	    document.getElementById("dataSeguroFI").style.visibility = 'visible';
	    document.getElementById("dataSeguroFV").style.visibility = 'visible';
		document.forms[0].E01PACFID.readOnly = false;
		document.forms[0].E01PACFIM.readOnly = false;
		document.forms[0].E01PACFIY.readOnly = false;
		document.forms[0].E01PACFVY.readOnly = false;
		document.forms[0].E01PACFVM.readOnly = false;
		document.forms[0].E01PACFVD.readOnly = false;
	}
 
	
} 
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
	boolean newOnly=false;
	boolean inqOnly=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<%
	// Determina si es solo consulta
	if (request.getParameter("inqOnly") != null ){
		if (request.getParameter("inqOnly").toLowerCase().equals("true")){
			inqOnly=true;
		} else {
			inqOnly=false;
		}
	}
%>
<%
	// Determina si es nuevo o mantencion
	if (userPO.getPurpose().equals("NEW")){
			newOnly=false;
		} else if (userPO.getPurpose().equals("MAINTENANCE")) {
    		newOnly=true;
		} else {
    		newOnly=true;		
		}
%>

<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	CONSULTA PAC DE SEGUROS
<%} else if (maintenance){ %>
	MANTENIMIENTO PAC DE SEGUROS
<%} else { %>
	NUEVO PAC DE SEGUROS
<%} %>
 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PAC_seguros_maintenance.jsp, ESG0010"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESG0010">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01PACBNK"  value="<%= currUser.getE01UBK().trim()%>">
  <input type=HIDDEN name="H01FLGMAS"  value="<%= cnvObj.getH01FLGMAS().trim()%>">
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Cliente Numero : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="cnvObj" property="E01PACCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Nombre : 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="cnvObj" property="E01CUSNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">Identificaci�n :  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="cnvObj" property="E01CUSIDN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>
             </td>
         </tr>
          <tr>
             <td nowrap width="10%" align="right"> Seguro Numero : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="cnvObj" property="E01PACNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Fecha : 
               </td>
             <td nowrap width="50%"align="left">
    	        <eibsinput:date name="currUser" fn_year="E01RDY" fn_month="E01RDM" fn_day="E01RDD" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Usuario : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="currUser" property="H01USR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Poliza de Seguro </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" >

          <tr id="trdark"> 
            <td width="15%" > 
              <div align="right">Descripcion :</div>
            </td>
            <td width="35%" > 
                 <eibsinput:text property="E01PACDSC" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=newOnly%>"/>
	        </td>
            <td width="15%" > 
              <div align="right">Poliza Numero :</div>
            </td>
            <td width="35%" > 
                 <eibsinput:text name="cnvObj" property="E01PACPLZ" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_REFERENCE%>" required="false" readonly="<%=newOnly%>"/>
	        </td>
          </tr>

          <tr id="trclear"> 
            <td width="15%" > 
              <div align="right">Seguro :</div>
            </td>
            <td width="35%" > 
                 <eibsinput:cnofc name="cnvObj" property="E01PACCOD" required="false" flag="IT" fn_code="E01PACCOD" fn_description="E01SEGNME" readonly="<%=newOnly%>" size="4" maxlength="4" onblur="SeguroFraude()"/>
                 <eibsinput:text property="E01SEGNME" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="true" size="40" maxlength="40" readonly="true"/>
	        </td>
           <td width="15%"> 
              <div align="right">Fecha Inicio :</div>
            </td>
            <td width="35%" > 
    	        <eibsinput:date name="cnvObj" identificador="dataSeguroFI" fn_year="E01PACFIY" fn_month="E01PACFIM" fn_day="E01PACFID" readonly="<%=newOnly%>" />
    	        
            </td>
          </tr>

          <tr id="trdark"> 
            <td width="15%" > 
              <div align="right">Plan :</div>
            </td>
 <%           if (newOnly) { %>
              <td width="35%" > 
              <input type="text" name="E01PACPLN" size="3" maxlength="2" value="<%= cnvObj.getE01PACPLN().trim()%>" readonly="true">
              <eibsinput:text property="E01PLNNME" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="true" size="40" maxlength="40" readonly="true"/>
	        </td>
 <%          } else { %>	
            <td width="35%" > 
              <input type="text" name="E01PACPLN" size="3" maxlength="2" value="<%= cnvObj.getE01PACPLN().trim()%>">
              <a href="javascript:GetTablaSeguros('E01PACPLN',document.forms[0].E01PACCOD.value,'E01PLNNME')" ><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"  ></a>
                 <eibsinput:text property="E01PLNNME" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="true" size="40" maxlength="40" readonly="true"/>
	        </td>

 <%          } %>	                
           <td width="15%"> 
              <div align="right">Fecha Vencimiento :</div>
            </td>
            <td width="35%" > 
    	        <eibsinput:date name="cnvObj" identificador="dataSeguroFV" fn_year="E01PACFVY" fn_month="E01PACFVM" fn_day="E01PACFVD" readonly="<%=newOnly%>" />
            </td>
          </tr>

          <tr id="trclear"> 
            <td width="15%" > 
              <div align="right">Moneda Seguro :</div>
            </td>
            <td width="35%" > 
 	 			<eibsinput:text name="cnvObj" property="E01PACCYS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" required="false" readonly="true" />
	        </td>
           <td width="15%"> 
              <div align="right">Monto Asegurado :</div>
            </td>
            <td width="35%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACMTS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="<%=newOnly%>"/>
            </td>
          </tr>

          <tr id="trdark"> 
            <td width="15%" > 
              <div align="right">Moneda Prima :</div>
            </td>
            <td width="35%" > 
 	 			<eibsinput:text name="cnvObj" property="E01PACCYP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" required="false" readonly="true"/>
	        </td>
           <td width="15%"> 
              <div align="right">Monto Prima :</div>
            </td>
            <td width="35%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACMTP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="<%=newOnly%>"/>
            </td>
          </tr>

          <tr id="trclear"> 
            <td width="15%" > 
              <div align="right">Compa�ia :</div>
            </td>
            <td width="35%" > 
				<eibsinput:text name="cnvObj" property="E01PACCIA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER %>" readonly="true"/>
                <eibsinput:text property="E01CIANME" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="true"/>
	        </td>
           <td width="15%"> 
              <div align="right">Prima Total :</div>
            </td>
            <td width="35%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACPRT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="<%=newOnly%>"/>
            </td>
          </tr>

          <tr id="trdark"> 
            <td width="15%" > 
              <div align="right">Estado :</div>
            </td>   
            <td width="35%" > 
               <select name="E01PACSTS" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(cnvObj.getE01PACSTS().equals("1")||cnvObj.getE01PACSTS().equals("2") || cnvObj.getE01PACSTS().equals("3")|| cnvObj.getE01PACSTS().equals("4"))) out.print("selected"); %>> 
                    </option>
                    <option value="1" <% if (cnvObj.getE01PACSTS().equals("1")) out.print("selected"); %>>Vigente</option>
                    <option value="2" <% if (cnvObj.getE01PACSTS().equals("2")) out.print("selected"); %>>Cancelado</option>                   
                    <option value="3" <% if (cnvObj.getE01PACSTS().equals("3")) out.print("selected"); %>>Bloqueado</option>                   
                    <option value="4" <% if (cnvObj.getE01PACSTS().equals("4")) out.print("selected"); %>>Suspendido</option>                   
                  </select>
             <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0"  > 
	        </td>	             
           <td width="15%"> 
              <div align="right">Fecha contrato :</div>
            </td>
            <td width="35%"> 
    	        <eibsinput:date name="cnvObj" fn_year="E01PACFCY" fn_month="E01PACFCM" fn_day="E01PACFCD" readonly="<%=newOnly%>"/>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>
  
  <h4>Recaudacion </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="trdark"> 
            <td width="15%" > 
              <div align="right">Modo Recaudacion :</div>
            </td>
            <td width="35%" > 
 <%           if (!currUser.getE01INT().equals("18")) { %>  
					<select name="E01PACRCD" onchange="setModoRec()" <%=readOnly?"disabled":""%>>
						<option></option>
						<option value="1" <% if (cnvObj.getE01PACRCD().equals("1")) out.print("selected");%>>Planilla/Nomina</option>
						<option value="2" <% if (cnvObj.getE01PACRCD().equals("2")) out.print("selected");%>>PAC/Automatico</option>
						<option value="3" <% if (cnvObj.getE01PACRCD().equals("3")) out.print("selected");%>>Caja</option>
						<option value="5" <% if (cnvObj.getE01PACRCD().equals("5")) out.print("selected");%>>PAC Multibanco</option>
					</select>
 <%          } else { %>	
					<select name="E01PACRCD" onchange="setModoRec()" <%=readOnly?"disabled":""%>>
						<option></option>
						<option value="2" <% if (cnvObj.getE01PACRCD().equals("2")) out.print("selected");%>>PAC/Automatico</option>
						<option value="5" <% if (cnvObj.getE01PACRCD().equals("5")) out.print("selected");%>>PAC Multibanco</option>
					</select>
 <%          } %>	                
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0"  > 
          </td>
            <td width="15%"> 
              <div align="right">Dia del cargo :</div>
            </td>
            <td width="35%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACDYS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="3" maxlength="2" required="true" readonly="<%=readOnly %>" />
            </td>
          </tr>

          <tr id="trclear"> 
            <td width="15%"> 
              <div align="right">Frecuencia Pago :</div>
            </td>
            <td width="35%"> 
				<select name="E01PACFRE" <%=readOnly?"disabled":""%> >
					<option></option>
					<option value="D" <% if (cnvObj.getE01PACFRE().equals("D")) out.print("selected");%>>Diario</option>
					<option value="M" <% if (cnvObj.getE01PACFRE().equals("M")) out.print("selected");%>>Mensual</option>
					<option value="S" <% if (cnvObj.getE01PACFRE().equals("S")) out.print("selected");%>>Semestral</option>					
					<option value="Y" <% if (cnvObj.getE01PACFRE().equals("Y")) out.print("selected");%>>Anual</option>
				</select>
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0"  > 
            </td>
            <td width="15%" > 
              <div align="right">Numero de Pagos :</div>
            </td>
            <td width="35%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACNPM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="4" maxlength="3" required="true" readonly="<%=readOnly %>"/>
          </td>
          </tr>

          <tr id="trdark"> 
            <td width="15%" > 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td width="35%" > 
                <div align="left"> 
                <input type="text" name="E01PACPAC" size="13" maxlength="12" value="<%= cnvObj.getE01PACPAC().trim()%>">
                <a id="lupa1" href="javascript:GetAccountCustomer('E01PACPAC',document.forms[0].E01PACBNK.value,'RY','N',document.forms[0].E01PACCUN.value)"> 
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a>
                </div>
	        </td>
           <td width="15%"> 
              <div align="right">Primas Mora Maxima :</div>
            </td>
            <td width="35%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACMPI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="4" maxlength="3"  readonly="true"/>
            </td>
          </tr>
          
          <tr id="trclear"> 
            <td width="15%" > 
              <div align="right">Codigo Convenio :</div>
            </td>
            <td width="35%" > 
	           <input type="text" name="E01PACCNV" size="5" maxlength="4" value="<%= cnvObj.getE01PACCNV().trim()%>" readonly>
	        </td>
           <td width="15%"> 
              <div align="right">Dias Mora Maxima :</div>
            </td>
            <td width="35%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACMMR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="4" maxlength="3"  readonly="true"/>
            </td>
          </tr>
          
          <tr id="trdark"> 	
            <td width="15%" > 
              <div align="right">Cuenta Contable Abono :</div>
            </td>
            <td width="35%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACABK" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BANK %>" readonly="true"/>
 		        <eibsinput:text name="cnvObj" property="E01PACABR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
 		        <eibsinput:text name="cnvObj" property="E01PACACY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" readonly="true"/>
 		        <eibsinput:text name="cnvObj" property="E01PACAGL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER %>" readonly="true"/>
            </td>
            <td width="15%" > 
              <div align="right">Maximo Insistencias :</div>
            </td>
            <td width="35%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACMIN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="4" maxlength="3"  readonly="true"/>
	        </td>
          </tr>

          <tr id="trclear"> 
            <td width="15%" > 
              <div align="right">Cuenta Corriente Abono :</div>
            </td>
            <td width="35%" > 
 				<eibsinput:help name="cnvObj" property="E01PACAAC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" required="false" fn_param_one="E01PACAAC" fn_param_two="document.forms[0].E01PACBNK.value" fn_param_three="RT" readonly="<%=readOnly %>"/>
		        </td>
           <td width="15%"> 
              <div align="right">% Intermediacion :</div>
            </td>
            <td width="35%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACPIN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="true" />
            </td>
          </tr>
          
        </table>
      </td>
    </tr>
  </table>

  
  <h4>Beneficiarios </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

         <tr id="trdark"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">Nro</div>
           </td>
           <td width="15%" > 
              <div align="center">Rut</div>
           </td>
           <td width="30%" > 
              <div align="center">Nombre</div>
           </td>
           <td width="10%" > 
              <div align="center">Telefono</div>
           </td>
           <td width="10%" > 
              <div align="Center">Relacion</div>
           </td>
           <td width="5%" > 
              <div align="center">%</div>
           </td>
           <td width="10%" > 
           </td>
          </tr>

         <tr id="trclear"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">1</div>
           </td>
           <td width="15%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACRT1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="30%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACNM1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACPH1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
                  <select name="E01PACRL1" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(cnvObj.getE01PACRL1().equals("1")||cnvObj.getE01PACRL1().equals("2") || cnvObj.getE01PACRL1().equals("3")||cnvObj.getE01PACRL1().equals("4")||cnvObj.getE01PACRL1().equals("5")||cnvObj.getE01PACRL1().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (cnvObj.getE01PACRL1().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (cnvObj.getE01PACRL1().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (cnvObj.getE01PACRL1().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (cnvObj.getE01PACRL1().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (cnvObj.getE01PACRL1().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="9" <% if (cnvObj.getE01PACRL1().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
           <td width="5%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACPR1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
           </td>
          </tr>

         <tr id="trdark"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">2</div>
           </td>
           <td width="15%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACRT2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="30%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACNM2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACPH2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
                  <select name="E01PACRL2" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(cnvObj.getE01PACRL2().equals("1")||cnvObj.getE01PACRL2().equals("2") || cnvObj.getE01PACRL2().equals("3")||cnvObj.getE01PACRL2().equals("4")||cnvObj.getE01PACRL2().equals("5")||cnvObj.getE01PACRL2().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (cnvObj.getE01PACRL2().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (cnvObj.getE01PACRL2().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (cnvObj.getE01PACRL2().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (cnvObj.getE01PACRL2().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (cnvObj.getE01PACRL2().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (cnvObj.getE01PACRL2().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
           <td width="5%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACPR2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
           </td>
          </tr>

         <tr id="trclear"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">3</div>
           </td>
           <td width="15%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACRT3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="30%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACNM3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACPH3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
                  <select name="E01PACRL3" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(cnvObj.getE01PACRL3().equals("1")||cnvObj.getE01PACRL3().equals("2") || cnvObj.getE01PACRL3().equals("3")||cnvObj.getE01PACRL3().equals("4")||cnvObj.getE01PACRL3().equals("5")||cnvObj.getE01PACRL3().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (cnvObj.getE01PACRL3().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (cnvObj.getE01PACRL3().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (cnvObj.getE01PACRL3().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (cnvObj.getE01PACRL3().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (cnvObj.getE01PACRL3().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (cnvObj.getE01PACRL3().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
           <td width="5%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACPR3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
           </td>
          </tr>

         <tr id="trdark"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">4</div>
           </td>
           <td width="15%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACRT4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="30%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACNM4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACPH4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
                  <select name="E01PACRL4" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(cnvObj.getE01PACRL4().equals("1")||cnvObj.getE01PACRL4().equals("2") || cnvObj.getE01PACRL4().equals("3")||cnvObj.getE01PACRL4().equals("4")||cnvObj.getE01PACRL4().equals("5")||cnvObj.getE01PACRL4().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (cnvObj.getE01PACRL4().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (cnvObj.getE01PACRL4().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (cnvObj.getE01PACRL4().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (cnvObj.getE01PACRL4().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (cnvObj.getE01PACRL4().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (cnvObj.getE01PACRL4().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
           <td width="5%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACPR4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
           </td>
          </tr>

         <tr id="trclear"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">5</div>
           </td>
           <td width="15%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACRT5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="30%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACNM5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACPH5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
                  <select name="E01PACRL5" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(cnvObj.getE01PACRL5().equals("1")||cnvObj.getE01PACRL5().equals("2") || cnvObj.getE01PACRL5().equals("3")||cnvObj.getE01PACRL5().equals("4")||cnvObj.getE01PACRL5().equals("5")||cnvObj.getE01PACRL5().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (cnvObj.getE01PACRL5().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (cnvObj.getE01PACRL5().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (cnvObj.getE01PACRL5().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (cnvObj.getE01PACRL5().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (cnvObj.getE01PACRL5().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (cnvObj.getE01PACRL5().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
           <td width="5%" > 
 		        <eibsinput:text name="cnvObj" property="E01PACPR5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
           </td>
          </tr>
          
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Propiedad </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="trdark"> 
            <td width="10%" > 
            </td>
           <td width="20%"> 
              <div align="right">Descripcion :</div>
            </td>
            <td width="40%"> 
			<%if (readOnly){ %>
                <textarea name="E01PACRMK" cols="50" rows="5" readonly ><%= cnvObj.getE01PACRMK()%> </textarea>
			<%} else { %>
                <textarea name="E01PACRMK" cols="50" rows="5" ><%= cnvObj.getE01PACRMK()%> </textarea>
		    <% } %>  
            </td>
            <td width="10%" > 
	        </td>
          </tr>


          <tr id="trdark"> 
            <td width="10%" > 
            </td>
           <td width="20%"> 
              <div align="right">Direcci�n :</div>
            </td>
            <td width="40%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACMA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>"  readonly="<%=readOnly %>"/>
            </td>
            <td width="10%" > 
	        </td>
          </tr>

          <tr id="trdark"> 
            <td width="10%" > 
            </td>
           <td width="20%"> 
              <div align="right"> </div>
            </td>
            <td width="40%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACMA2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>"  readonly="<%=readOnly %>"/>
            </td>
            <td width="10%" > 
	        </td>
          </tr>

          <tr id="trdark"> 
            <td width="10%" > 
            </td>
           <td width="20%"> 
              <div align="right"> </div>
            </td>
            <td width="40%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACMA3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>"  readonly="<%=readOnly %>"/>
            </td>
            <td width="10%" > 
	        </td>
          </tr>

          <tr id="trdark"> 
            <td width="10%" > 
            </td>
           <td width="20%"> 
              <div align="right"> </div>
            </td>
            <td width="40%"> 
 		        <eibsinput:text name="cnvObj" property="E01PACMA4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>"  readonly="<%=readOnly %>"/>
            </td>
            <td width="10%" > 
	        </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>
  
  <h4>Vendedor </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="trdark"> 
            <td width="15%" > 
            </td>
           <td width="15%"> 
              <div align="right">Ejecutivo Cuenta :</div>
            </td>
            <td width="55%"> 
                 <eibsinput:cnofc name="cnvObj" property="E01PACOFI" required="false" flag="15" fn_code="E01PACOFI" fn_description="E01OFINME" readonly="<%=newOnly%>"/>
                 <eibsinput:text property="E01OFINME" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="true" readonly="true"/>
            </td>
            <td width="15%" >
	        </td>
          </tr>

          <tr id="trclear"> 
            <td width="15%" > 
            </td>
           <td width="15%"> 
              <div align="right">Canal de venta :</div>
            </td>
            <td width="55%"> 
                 <eibsinput:cnofc name="cnvObj" property="E01PACCNL" required="false" flag="62" fn_code="E01PACCNL" fn_description="E01CNLNME" readonly="<%=newOnly%>"/>
                 <eibsinput:text property="E01CNLNME" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="true"/>
            </td>
            <td width="15%" > 
            </td>
          </tr>

          <tr id="trdark"> 
            <td width="15%" > 
            </td>
           <td width="15%"> 
              <div align="right">Oficina de venta :</div>
            </td>
            <td width="55%"> 
 	 			<eibsinput:help name="cnvObj" property="E01PACBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" required="false" readonly="<%=newOnly%>"
 	 				fn_param_one="E01PACBRN" fn_param_two="document.forms[0].E01PACBNK.value" fn_param_three="E01BRNNME" />
                 <eibsinput:text property="E01BRNNME" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="true" readonly="true"/>
            </td>
            <td width="15%" > 
            </td>
          </tr>

          <tr id="trclear"> 
            <td width="15%" > 
            </td>
            <td width="15%" > 
              <div align="right">Vendedor del Seguro:</div>
            </td>
            <td width="55%" > 
				<eibsinput:help name="cnvObj" property="E01PACVDR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER %>" 
					fn_param_one="E01PACVDR" fn_param_two="E01VDRNME" fn_param_three="S" readonly="<%=newOnly%>"/>
                 <eibsinput:text property="E01VDRNME" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="true" readonly="true"/>
	        </td>
            <td width="15%" > 
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

      <% if(error.getERWRNG().equals("Y")){%>
   <h4 style="text-align:center"><input type="checkbox" name="H01FLGWK2" value="A" <% if(cnvObj.getH01FLGWK2().equals("A")){ out.print("checked");} %>>
      Aceptar con Advertencias</h4>
  <% } %>
  
  
   <%if  (!readOnly) { %>
      <div align="center"> 
          <input id="EIBSBTN" type=submit name="Submit" value="Enviar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>
    <% } else if  (!inqOnly){ %>
      <div align="center"> 
          <input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>    
    <% } %>  
  </form>

  <script type="text/javascript">
    setModoRec();
 </script>  

<script type="text/javascript">
SeguroFraude();
</script>
</body>
</HTML>
