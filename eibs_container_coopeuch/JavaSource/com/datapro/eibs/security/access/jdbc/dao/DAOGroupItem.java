package com.datapro.eibs.security.access.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.MenuOptionNotAssignedException;
import com.datapro.eibs.security.access.jdbc.bean.SECGRPITM;
import com.datapro.eibs.security.access.jdbc.bean.SECMNU;
import com.datapro.eibs.security.access.jdbc.bean.SECMNUCHK;
import com.datapro.generic.access.DAOSuper;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.BeanParser;
import com.datapro.generic.beanutil.BeanSql;

/**
 * @author ogarcia
 *
 */
public class DAOGroupItem extends DAOSuper {

	private static Category cat =
		Category.getInstance(DAOGroupItem.class.getName());

	/**
	 * Constructor for DAOGroupItem.
	 * @param dbid
	 */
	public DAOGroupItem(String dbid) {
		super(dbid);
	}

	/**
	 * Constructor for DAOGroupItem.
	 */
	public DAOGroupItem() {
		super();
	}

	/**
	 * Constructor for DAOGroupItem.
	 * @param connection
	 */
	public DAOGroupItem(Connection connection) {
		super(connection);
	}

	/**
	 * Select user option for left menu.
	 */
	public BeanList getMenuItems(String id)
		throws MenuOptionNotAssignedException, DAOException {

		BeanList list = null;

		try {
			String sql =
				"SELECT SM.* FROM SECGRPITM SGI, SECMNU SM WHERE SGI.MNUIDN=SM.MNUIDN AND USRID=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				SECMNU bean = new SECMNU();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new MenuOptionNotAssignedException(
					"Not options assigned(id:" + id + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}

	/**
	 * Select user option with flag checked for setting group access.
	 */
	public BeanList getMenuAccesItems(String uid)
		throws DAOException {

		BeanList list = null;

		try {
			String sql =
				"SELECT SM.*, SG.MNUIDN AS CHECKED FROM SECMNU SM LEFT OUTER JOIN SECGRPITM SG ON SM.MNUIDN=SG.MNUIDN AND SG.USRID=? WHERE SM.STATUS=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, Security.MENU_ITEM_ACTIVE);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				SECMNUCHK bean = new SECMNUCHK();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}

	/**
	 * Delete security group item record(s).
	 */
	public int delete(String uid) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM SECGRPITM WHERE USRID=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

	/**
	 * Insert security group item record.
	 */
	public int insert(SECGRPITM bean) throws DAOException {

		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.insertPrepareStatement();
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}


}
