<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ECO016501Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Listado de Convenios por Empleador</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="list" class="datapro.eibs.beans.JBObjList"	scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">

  	function goExcel() {
  	    var items = "";
  	    var estado = "";
  	    var convenio = "";
        var Str_fecha = "";
		var Str_anomes = "";

  	    var estado2 = "";
  	    var convenio2 = "";
        var Str_fecha2 = "";
		var Str_anomes2 = "";

  	    var form = document.forms[0];
		var formLength = form.elements.length;

		var IndAlerta = 0;
//validacion de planillas emitidas.
   	for (n=0; n<formLength; n++) 
		{
    		var elemt = form.elements[n];
      	  	if (elemt.type == 'checkbox') 
      	  	{
				if (elemt.checked) 
				{
					if(elemt.value!=1000)
						items += "&ROW=" + elemt.value;
					
					estado = form["estado_"+elemt.value].value;
		        	if(estado == "EMITIDA")
			    	{ 
						convenio = form["convenio_"+elemt.value].value;
			   			Str_fecha = form["fecha_"+elemt.value].value;
						Str_anomes = Str_fecha.substr(6,4) + Str_fecha.substr(3,2);

			    		for (nn=0; nn<formLength; nn++) 
			    		{
   							var elemt2 = form.elements[nn];
     						if (elemt2.type == 'checkbox') 
      						{
 								estado2 = form["estado_"+elemt2.value].value;
								if(estado2 == "EMITIDA")
								{
									convenio2 = form["convenio_"+elemt2.value].value;
									Str_fecha2 = form["fecha_"+elemt2.value].value;
									Str_anomes2 = Str_fecha2.substr(6,4) + Str_fecha2.substr(3,2);
						  			if((convenio == convenio2) && (parseInt(Str_anomes2) < parseInt(Str_anomes)))
										IndAlerta = 1;
	

									
		    					}
    						}
						}
					}
           		}	
   		  	}
		}
//Fin validacion
	var validaAler = true;								
	if(IndAlerta == 1)
		validaAler = confirm("Advertencia:Existen planillas anteriores EMITIDAS");

	if(items!='' && validaAler)
	{
		var pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0165?SCREEN=3" + items;
		CenterWindow(pg, 600, 500, 2);
	}
//cierre
  	}
  	
  	
  	
  	
  	
  	function selectAll(obj) {
  		var form = document.forms[0];
		var formLength = form.elements.length;
    	for (n=0; n<formLength; n++) {
    		var elemt = form.elements[n];
      		if (elemt.type == 'checkbox') {
      			elemt.checked = obj.checked;
      		}
    	}
  	}
	
	
	function goAction(op) 
	{
  		document.forms[0].SCREEN.value = op;
		document.forms[0].submit();		
   	}
	
</script>

</head>

<body>
<% 

	if ( !error.getERRNUM().equals("0")  ) {
    	error.setERRNUM("0");
     	out.println("<SCRIPT Language=\"Javascript\">");
     	out.println("       showErrors()");
     	out.println("</SCRIPT>");
 	}
 	
%>

<h3 align="center">Listado de Convenios por Empleador
	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="covenant_list.jsp, ECO0165"></h3>
<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0165">

	<input type="hidden" name="SCREEN" value="3"> 

  	<table  class="tableinfo">
    	<tr bordercolor="#FFFFFF"> 
      		<td nowrap> 
		        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
		          	<tr>
			             <td nowrap width="10%" align="right"> Empleador: 
			              </td>
			             <td nowrap width="10%" align="left">
				  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
			             </td>
			             <td nowrap width="10%" align="right">Identificaci�n:  
			             </td>
			             <td nowrap width="10%" align="left">
				  			<eibsinput:text name="userPO" property="ID" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>
			             </td>
			             <td nowrap width="10%" align="right"> Nombre: 
			               </td>
			             <td nowrap width="50%"align="left">
				  			<eibsinput:text name="userPO" property="cusName" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
			             </td>
		         	</tr>
		        </table>
      		</td>
    	</tr>
  	</table>

<%
	if (list.getNoResult()) {
%>
		<TABLE class="tbenter" width=100% height=90%>
			<TR>
				<TD>
					<div align="center">
						<font size="3"><b> No hay resultados que correspondan a su criterio de b�squeda. </b></font>
					</div>
				</TD>
			</TR>
		</TABLE>
	<TABLE class="tbenter">
	    <TR>     	      
	      <TD class=TDBKG width="100%"> 
	        <div align="center"><a href="javascript:checkClose()"><b>Salir</b></a></div>
	      </TD>  	          
	    </TR>
	</TABLE>
	
<%
	} else {

			list.initRow();
			list.getNextRow();
			ECO016501Message convObj = (ECO016501Message) list.getRecord();

%>
	<H4>&nbsp;</H4>

  	<TABLE  class="tableinfo">
    	<tr bordercolor="#FFFFFF"> 
      		<td nowrap> 
		        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
		          	<tr>
			             <td nowrap width="30%" align="left"> 
				            	<input type="checkbox" id="all" name="all" value="1000" onclick="selectAll(this)"><b> Seleccionar Todas</b>
								<input type="hidden" name="estado_1000" value="TODAS">
			             </td>
			             <td nowrap width="20%" align="right"> 
			             		<b>Seleccione Estado de Planilla :</b>
			             </td>
			             <td nowrap width="50%"align="left">
      						 <input type="text" name="H01FLGWK1" size="5" maxlength="4"  onfocus="goAction('2');"   value="<%if(convObj.getH01FLGWK1().trim().equals("")){out.print("A");}else{out.print(convObj.getH01FLGWK1().trim());} %>" readonly >
      						 <input type="text" name="E01DSCCON" size="50" maxlength="45" value="<%if(convObj.getH01FLGWK1().trim().equals("")){out.print("TODAS");}else{out.print(convObj.getE01DSCCON().trim());} %>" readonly >
									<a href="javascript:GetCodeDescCNOFC('H01FLGWK1','E01DSCCON','1O')">
        							<img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
			             </td>
		         	</tr>
		        </table>
      		</td>
    	</tr>
  	</table>

	<TABLE class="tbenter">
	    <TR>     	      
	      <TD class=TDBKG width="50%"> 
	        <div align="center"><a href="javascript:goExcel()">Generar<br>EXCEL</a></div>
	      </TD>	    
	      <TD class=TDBKG width="50%"> 
	        <div align="center"><a href="javascript:checkClose()"><b>Salir</b></a></div>
	      </TD>  	          
	    </TR>
	</TABLE>


	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="5%">&nbsp;</th>
			<th align="center" nowrap width="10%">Convenio</th>
			<th align="center" nowrap width="10%">Planilla</th>
			<th align="center" nowrap width="10%">Mes Desto.</th>
			<th align="center" nowrap width="10%">Fecha Emisi&oacute;n</th>
			<th align="center" nowrap width="25%">Nombre</th>
			<th align="center" nowrap width="10%">Fecha Aplic.</th>
			<th align="center" nowrap width="10%">Monto Aplicado</th>			
			<th align="center" nowrap width="10%">Monto Informado</th>
			<th align="center" nowrap width="10%">Estado</th>
		</tr>
		<%
			list.initRow();
			int k = 0;
			boolean firstTime = true;
			String chk = "";
			while (list.getNextRow()) {
				if (firstTime) {
					firstTime = false;
					chk = "checked";
				} else {
					chk = "";
				}
				convObj = (ECO016501Message) list.getRecord();
		%>
		<tr>
			<td nowrap><input type="checkbox" name="ROW_<%=list.getCurrentRow()%>" value="<%=list.getCurrentRow()%>" /></td>
			<td nowrap align="center"><%=Util.formatCell(convObj.getE01PLHCDE())%></td>
			<td nowrap align="center"><%=Util.formatCell(convObj.getE01PLHNUM())%></td>
			<td nowrap align="center"><%=Util.formatCell(convObj.getE01PLHFVM())%></td>
			<td nowrap align="center"><%=Util.formatDate(convObj.getE01PLHFED(), convObj.getE01PLHFEM(), convObj.getE01PLHFEY())%></td>
			<td nowrap align="left"><%=Util.formatCell(convObj.getE01PLHCNM())%></td>
			<td nowrap align="center"><%=Util.formatDate(convObj.getE01PLHFRD(), convObj.getE01PLHFRM(), convObj.getE01PLHFRY())%></td>
			
			<%  
			    String Strtotal = "";
			    try {
      					double Monto1 = Double.parseDouble(convObj.getE01PLHPRE().replace(",",""));
      					double Monto2 = Double.parseDouble(convObj.getE01PLHPAP().replace(",",""));
      					double total = Monto1 + Monto2;
      					Strtotal = new Double(total).toString();
			    } catch(Exception e) {
      					Strtotal = "0";
    			}
			%>
			<td nowrap align="right"><%=Util.formatCCY(Strtotal)%></td>
			<td nowrap align="right"><%=Util.formatCCY(convObj.getE01PLHPAM())%></td>
			<td nowrap align="center"><%=Util.formatCell(convObj.getE01DSCSTA())%></td>
			<input type="hidden" name="convenio_<%=list.getCurrentRow()%>" value="<%=Util.formatCell(convObj.getE01PLHCDE())%>">
			<input type="hidden" name="fecha_<%=list.getCurrentRow()%>" value="<%=Util.formatDate(convObj.getE01PLHFED(), convObj.getE01PLHFEM(), convObj.getE01PLHFEY())%>">
			<input type="hidden" name="estado_<%=list.getCurrentRow()%>"   value="<%=Util.formatCell(convObj.getE01DSCSTA())%>">
		</tr>
		<%
			}
		%>
	</table>

<%
	}
%>

</form>
</body>
</html>
