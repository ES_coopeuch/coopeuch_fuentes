<%// Hecho por Alonso Arana-Datapro 21 de Octubre del 2013 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="RetractosList" class="datapro.eibs.beans.ESG010503Message" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<title>Retracto de Seguros</title>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
</head>
<body>

<%
	if (!error.getERRNUM().equals("0")) {
		out.println("<script type=\"text/javascript\">");
		error.setERRNUM("0");
		out.println("showErrors()");
		out.println("</script>");
	}
%>

<h3 align="center">Formulario de T�rmino Anticipado de Seguros de Vida<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PAC_client_search.jsp,ESG0010"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESG105">
<input type="hidden" name="SCREEN" value="">
<br>

<table id="TBHELPN" width="100%" border="0" cellspacing="0"
	cellpadding="0" style="margin-left: center; margin-right: center;">
	
	
	 <tr > 
      <td>
      <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:normal'>
      	<b style='mso-bidi-font-weight:normal'>
      	<span lang=ES-CLstyle='font-size:10.0pt'>
      	<br>
      	Se�ores<br>
      	NOMBRE CNIA SEGURO<br>
      	Presente
      	</span></b>
      </p>

      </td>
 	</tr>
 	<tr>
      <td>
      
      </td>
    </tr>
	 <tr>
      <td>
      	<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:normal'>
      	<span lang=ES-CL style='font-size:10.0pt'>
      	Solicito eliminar el seguro de acuerdo a la informaci�n que a continuaci�n detallo:
      	</span>
      	</p>
      
      </td>
    </tr>
	
<tr>
<td align="center">	<p class=MsoNoSpacing align=center style='text-align:center'>
	  		<b style='mso-bidi-font-weight:normal'>
	  			<span lang=ES-CL style='font-size:10.0pt; mso-bidi-font-size:11.0pt'>
	 			<br>DATOS CONTRATANTE P�LIZA<br><br>
	  			</span>
	  		</b>
	  	</p> </td>
</tr>


	<tr>
	

		<td align="right" width="50%" nowrap>Nombre del cliente :&nbsp;</td>
		
		<td  width="50%"><%=userPO.getCusName() %></td>		
	</tr>
	
	<tr>
		<td align="right" width="50%" nowrap>RUT:&nbsp;</td>
		
		<td  width="50%"><%=userPO.getIdentifier() %></td>		
	</tr>
	<tr>
		<td align="right" width="50%" nowrap>Celular :&nbsp;</td>
		
		<td  width="50%"><%=RetractosList.getE03SREHPN() %></td>		
	</tr>
	
	
	
	<tr>
		<td align="right" width="50%" nowrap>Correo Electr�nico :&nbsp;</td>
		
		<td  width="50%"><%=RetractosList.getE03SREIAD()%></td>		
	</tr>
	
	
	
	
	
	<tr>
	<td align="center">Datos de la P�liza</td>
	</tr>
	<tr>
		<td align="right" width="50%" nowrap>N� de P�liza :&nbsp;</td>
		
		<td  width="50%"><%=RetractosList.getE03SREPLZ() %></td>		
	</tr>
	<tr>
		<td align="right" width="50%" nowrap>N� de Operaci�n:&nbsp;</td>
		
		<td  width="50%"><%=RetractosList.getE03SREPAC() %></td>		
	</tr>
	<tr></tr>
	<tr>
		<td align="right" width="50%" nowrap>Seguro :&nbsp;</td>
		
		<td  width="50%"><input type="text" name="insurance" size="10" maxlength="9"></td>		
	</tr>
	<tr>
		<td align="right" width="50%" nowrap>Descripci�n :&nbsp;</td>
		
		<td  width="50%"><input type="text" name="description" size="10" maxlength="9"></td>		
	</tr>
	
	
	<tr>
		<td align="right" width="50%" nowrap>Compa�ia :&nbsp;</td>
		
		<td  width="50%"> <%=RetractosList.getE03SRENM1()%></td>		
	</tr>
	
	
	


	
	

<tr>
	
		

		
		
	</tr>

	
	<tr><td height="30" colspan="2">&nbsp;</td></tr>
	<tr>
		<td  width="35%">
			<div align="center"><input id="EIBSBTN" type="submit"
			name="Submit" onClick="window.print()" value="Enviar"></div>
		</td>
		<td  width="35%">
			<div align="center"><input id="EIBSBTN" type="submit"
			name="Submit" value="Volver"></div>
		</td>
		</tr>
	
	
</table>

</form>
</body>
</html>
 