<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import = "java.lang.Object" %>

<HTML>
<HEAD>
<TITLE>
Reemplazo de Cheques de Terceros
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "offBasic" class= "datapro.eibs.beans.EOF011501Message"  scope="session" />
<jsp:useBean id= "dvDocDet" class="datapro.eibs.beans.ETL051002Message"  scope="session" />
<jsp:useBean id= "userPO"   class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error"    class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT Language="javascript">


function Reemplazar(){
	
	if (document.forms[0].E01ADT.value == ""){
		alert("Debe ingresar tipo de reemplazo" );
		return false;
	}	

	//LIMPIA CAMPOS 
	document.forms[0].E01BCOOP1.value = "";
	document.forms[0].E01BCOOP2.value = "";
	document.forms[0].E01MTOEFE.value = "";
	document.getElementById("c1").checked = false;
	document.getElementById("c1").checked = false;
	//
	
	document.getElementById("E01BCOOP1").removeAttribute("readonly");
	
	if (document.forms[0].E01ADT.value == 'RMV'){ // mismo valor
		document.forms[0].E01BCOOP1.value = document.forms[0].MONTO.value;
		document.getElementById("E01BCOOP1").setAttribute("readonly", true);
		document.forms[0].TIPOREE.value = 'RMV';
		document.getElementById("chq2").style.visibility="hidden";
		document.getElementById("efectivo").style.visibility="hidden";
	}
	if (document.forms[0].E01ADT.value == 'RMA'){ //mayor valor
		document.forms[0].TIPOREE.value = 'RMA';
		document.getElementById("chq2").style.visibility="hidden";
		document.getElementById("efectivo").style.visibility="visible";
	}
	if (document.forms[0].E01ADT.value == 'RME'){ // menor valor
		document.forms[0].TIPOREE.value = 'RME';
		document.forms[0].E02OFMAID_1.value = document.forms[0].E02OFMAID.value; // asigna rut aplic a ch 2
		document.forms[0].E02OFMAPL_1.value = document.forms[0].E02OFMAPL.value; // asigna nom aplic a ch 2
		document.getElementById("chq2").style.visibility="visible";
		document.getElementById("efectivo").style.visibility="hidden";
	}

	document.getElementById("trx").style.visibility="visible";
	document.getElementById("txtRee").style.visibility="visible";

	document.getElementById("boton").style.visibility="hidden";
	return true;
}	

function ValidaEnvia(){
	
	if (document.forms[0].E01ADT.value == ""){
		alert("Debe ingresar tipo de reemplazo" );
		return false;
	}
	
	if (document.forms[0].E01ADT.value == 'RMV'){ // mismo valor
		if (!document.forms[0].E01OFMBSL1[0].checked){
			if (!document.forms[0].E01OFMBSL1[1].checked){
				alert("Debe seleccionar un banco para Cheque 1" );
				return false;
			}
		}
		if (document.forms[0].E01OFMBSL1[0].checked){
			if (document.forms[0].E01OFMBSL1[0].value == '99'){
			
				alert("No puede seleccionar banco 99  para Cheque 1" );
				return false;
			}
		}
		if (document.forms[0].E01OFMBSL1[1].checked){
			if (document.forms[0].E01OFMBSL1[1].value == '99'){
			
				alert("No puede seleccionar banco 99  para Cheque 1" );
				return false;
			}
		}
	}
	
	
	if (document.forms[0].E01ADT.value == 'RME'){ // menor valor
		if (!document.forms[0].E01OFMBSL1[0].checked){
			if (!document.forms[0].E01OFMBSL1[1].checked){
				alert("Debe seleccionar un banco para Cheque 1" );
				return false;
			}
		}
		if (!document.forms[0].E01OFMBSL2[0].checked){
			if (!document.forms[0].E01OFMBSL2[1].checked){
				alert("Debe seleccionar un banco para Cheque 2" );
				return false;
			}
		}
		
		if (document.forms[0].E01OFMBSL1[0].checked){
			if (document.forms[0].E01OFMBSL1[0].value == '99'){
			
				alert("No puede seleccionar banco 99  para Cheque 1" );
				return false;
			}
		}
		if (document.forms[0].E01OFMBSL1[1].checked){
			if (document.forms[0].E01OFMBSL1[1].value == '99'){
			
				alert("No puede seleccionar banco 99  para Cheque 1" );
				return false;
			}
		}
		
		if (document.forms[0].E01OFMBSL2[0].checked){
			if (document.forms[0].E01OFMBSL2[0].value == '99'){
				alert("No puede seleccionar banco 99  para Cheque 2" );
				return false;
			}
		}
		if (document.forms[0].E01OFMBSL2[1].checked){
			if (document.forms[0].E01OFMBSL2[1].value == '99'){
				alert("No puede seleccionar banco 99  para Cheque 2" );
				return false;
			}
		}
		
	}
	
	
	
	if (document.forms[0].E01ADT.value == 'RMA'){ //mayor valor
		if (!document.forms[0].E01OFMBSL1[0].checked){
			if (!document.forms[0].E01OFMBSL1[1].checked){
				alert("Debe seleccionar un banco para Cheque 1" );
				return false;
			}
		}
		if (document.forms[0].E01BCOOP1.value ==""){
			alert("Debe Ingresar Monto Cheque 1" );
			return false;
		}
		if (document.forms[0].E01MTOEFE.value ==""){
			alert("Debe Ingresar Monto en Efectivo" );
			return false;
		}
		
		if (document.forms[0].E01OFMBSL1[0].checked){
			if (document.forms[0].E01OFMBSL1[0].value == '99'){
			
				alert("No puede seleccionar banco 99  para Cheque 1" );
				return false;
			}
		}
		if (document.forms[0].E01OFMBSL1[1].checked){
			if (document.forms[0].E01OFMBSL1[1].value == '99'){
			
				alert("No puede seleccionar banco 99  para Cheque 1" );
				return false;
			}
		}
	}
		
	if (document.forms[0].E01OFMBSL1[0].checked){
		document.forms[0].BCOSELCH1.value = '1'; // se4lecciona bco 1 de ch 1
	}else{
		document.forms[0].BCOSELCH1.value = '2'; // se4lecciona bco 2 de ch 1
	}
	
	if (document.forms[0].E01OFMBSL2[0].checked){
		document.forms[0].BCOSELCH2.value = '1'; // se4lecciona bco 1 de ch 2
	}else{
		document.forms[0].BCOSELCH2.value = '2'; // se4lecciona bco 2 de ch 2
	}
	
	if (document.forms[0].E01ADT.value == 'RME'){ // menor valor
		if (document.forms[0].E01BCOOP1.value == ""){
			alert("Debe ingresar monto cheque 1" );
			return false;
		}
		if (document.forms[0].E01BCOOP2.value == ""){
			alert("Debe ingresar monto cheque 2" );
			return false;
		}
	}
	
	
	document.forms[0].SCREEN.value = '500';
	document.forms[0].submit();
}

</SCRIPT>

</HEAD>

<BODY>

<% 
	if ( !error.getERRNUM().equals("0")  ) {
 	 error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 	}
%> 

<h3 align="center">Reemplazo de Cheques de Terceros<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="dv_inq_doc_det, EOF0130"></h3>
  
<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEOF0130">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="700">
  <INPUT TYPE=HIDDEN NAME="MONTO" VALUE="<%= Util.fcolorCCY(dvDocDet.getE02OFMMCH().trim())%>">
  <INPUT TYPE=HIDDEN NAME="E01RMPCHQ" VALUE="<%= dvDocDet.getE02OFMRE2().trim()%>"> <!-- nro cheque-->
  
  <INPUT TYPE=HIDDEN NAME="E01RMPBID" VALUE="<%= dvDocDet.getE02OFMBID().trim()%>"> <!-- rut benef-->
  <INPUT TYPE=HIDDEN NAME="E01RMPBNF" VALUE="<%= dvDocDet.getE02OFMBNF().trim()%>"> <!-- nom benef-->
  <INPUT TYPE=HIDDEN NAME="E01RMPTYP" VALUE=""><!-- Tipo Reemplazo -->
  
  
   
  <INPUT TYPE=HIDDEN NAME="TIPOREE" VALUE=""><!-- Tipo Reemplazo -->
  
  <INPUT TYPE=HIDDEN NAME="E02OFMAID" VALUE="<%= dvDocDet.getE02OFMAID()%>"><!-- rut Aplicante -->
  <INPUT TYPE=HIDDEN NAME="E02OFMAPL" VALUE="<%= dvDocDet.getE02OFMAPL()%>"><!-- nom Aplicante -->
  
  <INPUT TYPE=HIDDEN NAME="CODDESBCO1" VALUE="<%= offBasic.getE01OFMBC1() + "-" + offBasic.getE01OFMBD1().toUpperCase()%>"><!-- cod + desc bco 1  -->
  <INPUT TYPE=HIDDEN NAME="CODDESBCO2" VALUE="<%= offBasic.getE01OFMBC2() + "-" + offBasic.getE01OFMBD2().toUpperCase()%>"><!-- cod + desc bco 2  -->
  
  <INPUT TYPE=HIDDEN NAME="BCOSELCH1" VALUE="<%= offBasic.getE01OFMBC2() + "-" + offBasic.getE01OFMBD2().toUpperCase()%>"><!-- bco seleccionado ch 1 -->
  <INPUT TYPE=HIDDEN NAME="BCOSELCH2" VALUE="<%= offBasic.getE01OFMBC2() + "-" + offBasic.getE01OFMBD2().toUpperCase()%>"><!-- bco seleccionado ch 2 -->
  
  <h4>Datos Cheque a reemplazar</h4>
  
   <table class="tableinfo">
      <tr > 
        <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
	          <tr  id="trclear" > 
		            <td nowrap  width="20%" height="23" > 
		              <div align="right">Referencia Cheque : </div>
				    </td>
		            <td nowrap width="20%" height="23" > 
		              <%= dvDocDet.getE02OFMNCH().trim()%>
		            </td>
		           
		            <td nowrap width="30%" height="10" > 
		              <div align="right">Fecha :</div>
		            </td>
		            <td nowrap width="20%" height="10" > 
		              <%= Util.formatDate(dvDocDet.getE02OFMED1(),dvDocDet.getE02OFMED2(),dvDocDet.getE02OFMED3())%>
		            </td>
		             <td nowrap width="10%"></td>
	          </tr>   
	          <tr id="trdark"> 
		            <td nowrap width="20%" > 
		              <div align="right">Numero de Cheque :</div>
		            </td>
		            <td nowrap width="20%" > 
		              <%= dvDocDet.getE02OFMRE2().trim()%>
		            </td>
		            
		            <td nowrap width="30%" height="18" > 
		              <div align="right">Monto :</div>
		            </td>
		            <td nowrap height="20" width="20%" > 
		              <%= Util.fcolorCCY(dvDocDet.getE02OFMMCH().trim())%>
		            </td>
		             <td nowrap width="10%"></td>
	          </tr>  
          
	          <tr  id="trclear" > 
		            <td nowrap width="20%" height="23" > 
		              <div align="right">Beneficiario :</div>
		            </td>
		            <td nowrap height="20" width="20%" > 
		              <%= dvDocDet.getE02OFMBID().trim()%> - <%= dvDocDet.getE02OFMBNF().trim()%>
		            </td>
		            
		            <td nowrap width="30%" > 
		              <div align="right">Sucursal de Pago :</div>
		            </td>
		            <td nowrap width="20%" > 
		              <%= dvDocDet.getE02PMTBRN().trim()%>
		            </td>
		             <td nowrap width="10%"></td>
	          </tr>
           
	          <tr id="trdark"> 
		            <td nowrap width="15%"  > 
		              <div align="right">Banco Emisor : </div>
		            </td>
		            <td nowrap width="20%" > 
						<div align="left">
							<%= dvDocDet.getE02OFMADR().trim()%>
						</div>
					</td>
					<td nowrap width="30%"  > 
		              <div align="right">Fecha Emision :</div>
		            </td>
		            <td nowrap width="25%" > 
		              <%= Util.formatDate(dvDocDet.getE02OFMMD1(),dvDocDet.getE02OFMMD2(),dvDocDet.getE02OFMMD3())%>
		            </td>
		            <td nowrap width="10%"></td>
	          </tr>
                 
             
		            
			          <tr id="trclear"> 
			              <td nowrap width="15%" align="right"   >Tipo Reemplazo :</td>
			  			  <td nowrap width="40%" >
			             
			                <input type="text" name="E01ADT" size="5"  maxlength="4"  value="" readonly onfocus="javascript:Reemplazar()"> 
			                <input type="text" name="D01ADT" size="40" maxlength="30" value="" readonly >
			                   <a href="javascript:GetCodeDescCNOFC('E01ADT','D01ADT','RR')">
			                   		<img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0">
			                   </a>
			                  
				               <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
								     
						  </td>
						        
						  <td>

						  </td>
						  <td nowrap width="15%"></td>  
			          	  <td nowrap width="10%"></td>  
			           </tr>
          	  
         
        </table>
      </td>
    </tr>
  </table>
  
 
  <div  style="visibility:hidden" id="trx" >
  <h4>Datos de la Transaccion</h4>

   <table class="tableinfo" >
    <tr > 
 		<td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
	          <tr id="trclear">
		      	<td nowrap colspan="4"> 
		        	<h4>Nuevo Cheque 1</h4>
		  	  	<td nowrap >
	          </tr>
	          <tr id="trdark"> 
		            <td nowrap width="20%"> 
		              <div align="right">Pagar a la orden de :</div>
		            </td>
		            <td nowrap width="20%">
		               <input type="text"  name="D01EDL_1" size="80" maxlength="80" value="<%= dvDocDet.getE02OFMBNF().trim()%>" readonly>
		            </td>
		            <td nowrap width="20%"></td>
		            <td nowrap width="40%"></td>
	          </tr>
	          <tr id="trclear"> 
		            <td nowrap width="20%"> 
		              <div align="right">Identificación :</div>
		            </td>
		            <td nowrap width="20%">
		              <input type="text"  name="E01UC9_2" size="25" maxlength="25" value="<%= dvDocDet.getE02OFMBID().trim()%>" readonly> 
		            </td>
		            <td nowrap width="20%"> 
		              <div align="right">La suma de : </div>
		            </td>
		            <td nowrap width="20%"><input type="text" id="E01BCOOP1" name="E01BCOOP1" size="20" maxlength="21" value="" onkeypress=" enterDecimal()" class="TXTRIGHT">
		            </td>
	          </tr>
	          <tr id="trdark"> 
		            <td nowrap width="20%"> 
			              <div align="right">Cheque del Banco :</div>
			        </td>
		        	<td nowrap width="30%" colspan="3"  > 
		              <input type="radio"  id="c1" name="E01OFMBSL1" value="<%=offBasic.getE01OFMBC1()%>"><%= offBasic.getE01OFMBC1() + "-" + offBasic.getE01OFMBD1().toUpperCase()%> 
		              <br>
		              <input type="radio" id="c1" name="E01OFMBSL1" value="<%=offBasic.getE01OFMBC2()%>"><%= offBasic.getE01OFMBC2() + "-" + offBasic.getE01OFMBD2().toUpperCase()%>
		            </td>
            		
            		<td nowrap width="20%"></td>
            		<td nowrap width="40%"></td>
          	  </tr>
          </table>
          
          <div  style="visibility:hidden" id="chq2" >
	          <table cellspacing="0" cellpadding="2" width="100%" border="0">
		          <tr id="trclear">
			      	<td nowrap colspan="4"> 
			        	<h4>Nuevo Cheque 2</h4>
			  	  	<td nowrap >
		          </tr>
		          <tr id="trdark"> 
			            <td nowrap width="20%" height="25"> 
			              <div align="right">Pagar a la orden de :</div>
			            </td>
			            <td nowrap width="20%" height="25">
			               <input type="text" name="E02OFMAPL_1" size="80" maxlength="80" value="<%= dvDocDet.getE02OFMBNF().trim()%>" readonly>
			            </td>
			            <td nowrap width="20%" height="25"></td>
			            <td nowrap width="40%" height="25"></td>
		          </tr>
		          <tr id="trclear"> 
			            <td nowrap width="20%" height="25"> 
			              <div align="right">Identificación :</div>
			            </td>
			            <td nowrap width="20%" height="25">
			              <input type="text"  name="E02OFMAID_1" size="25" maxlength="25" value="<%= dvDocDet.getE02OFMBID().trim()%>" readonly> 
			            </td>
			            <td nowrap width="20%" height="25"> 
			              <div align="right">La suma de : </div>
			            </td>
			            <td nowrap width="20%" height="25"><input type="text" name="E01BCOOP2" size="20" maxlength="21" value="" onkeypress=" enterDecimal()"  class="TXTRIGHT"></td>
		          </tr>
		          <tr id="trdark"> 
			            <td nowrap width="20%"> 
	              			<div align="right">Cheque del Banco :</div>
			            </td>
		            	<td nowrap width="30%" colspan="3">
		            		<input type="radio" id="c2" name="E01OFMBSL2" value="<%= offBasic.getE01OFMBC1()%>"	><%= offBasic.getE01OFMBC1() + "-" + offBasic.getE01OFMBD1().toUpperCase()%> 
							<br>
		              		<input type="radio" id="c2" name="E01OFMBSL2" value="<%= offBasic.getE01OFMBC2()%>" ><%= offBasic.getE01OFMBC2() + "-" + offBasic.getE01OFMBD2().toUpperCase()%>
		              	</td>
			                     		
	            		<td nowrap width="20%"></td>
	            		<td nowrap width="40%"></td>
	          		</tr>
	      
	          </table>
          </div>        
          
          <div  style="visibility:hidden" id="efectivo" >
	          <table cellspacing="0" cellpadding="2" width="100%" border="0">
		          <tr id="trclear">
		      		<td nowrap colspan="4"> 
		        		<h4>Efectivo</h4>
		  	  		<td nowrap >
		          </tr>
		          <tr id="trdark"> 
			            <td nowrap width="20%"> 
			              <div align="left">Ingreso por caja la suma de :</div>
			            </td>
			            <td nowrap width="20%">
			               <input type="text"  name="E01MTOEFE" size="21" maxlength="20" value="" onkeypress=" enterDecimal()"  class="TXTRIGHT">  
			            </td>
			            <td nowrap width="20%"></td>
			            <td nowrap width="40%"></td>
		          </tr>
	          </table>
         </div> 
        </td>
   	</tr>
   	
   	<td>
					
    </table>
  
 
  
	  <td>
	  	<div align="center">
	  		<input id="EIBSBTN" type=button name="Submit" OnClick="ValidaEnvia()" value="Enviar">
	    </div>
	  </td> 
	  
	   
 </div> <!-- fin fanel datos transaccion visible/invisible -->

  </FORM>
 
   
  
</BODY>
</HTML>
