<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ESG001001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Listado de PAC de Seguros</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ESG001001List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>	
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>
<script type="text/javascript">

        $(function(){
					
					$("#key").attr("checked", false);
                
				});



function goAction(op,index) {

document.forms[0].elements[index].checked = true;
$("input[name=key][value=" + index + "]").prop('checked', true); 
     document.forms[0].SCREEN.value = op;
	document.forms[0].submit();

}
  
 function showAddInfo(idxRow){
   //tbAddInfo.rows[0].cells[1].style.color="blue";   
   //tbAddInfo.rows[0].cells[1].innerHTML=extraInfo(document.forms[0]["TXTDATA"+idxRow].value,4);
   } 
   
 function extraInfo(textfields,noField) {
	 var pos=0
	 var s= textfields;
	 for ( var i=0; i<noField ; i++ ) {
	   pos=textfields.indexOf("<br>",pos+1);
	  }
	 s=textfields.substring(0,pos);
	 return(s);
 }  

  function goActionDocuments() {
	var ok = false;
	var cun = "";
	var pg = "";
	var cosnum = '0';
	var codseg = '';
	
	 	for(n=0; n<document.forms[0].elements.length; n++)
	    {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "key") 
	      	{	
	      		if (element.checked == true) {
        			ok = true;   			
        			cosnum =  eval("document.forms[0].PAC"+element.value+".value");
        			codseg =  eval("document.forms[0].COD"+element.value+".value");
        			break;
				}
	      	}
	     }
      
      if ( ok ) {
      	var url_doc = '<%=request.getContextPath()%>/pages/s/EFRM000C_pdf_form.jsp?SCREEN=1&OPE_CODE=87&CUST_NUMBER='+document.forms[0].cusNum.value+'&REQ_NUMBER='+cosnum+'&REQ_CODSEG='+codseg;      	
      	getPdfForms(url_doc);
     } else {
		alert("Debe seleccionar un seguro para continuar.");	   
	 }
      
	}	
</script>

</head>

<body>
<%if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">PAC de Seguros<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="PAC_Inq_seguros_list.jsp,ESG0010"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESG0010I">
<input type="hidden" name="SCREEN" value="201"> 

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Nombre : 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="userPO" property="cusName" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">Identificaci�n :  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="ID" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>

<table class="tbenter" width="100%">
	<tr>
		<td align="center"  width="15%">&nbsp;</td>
		<td align="center" width="15%">&nbsp;</td>
		<td align="center"  width="15%">&nbsp;</td>
		<td align="center" width="15%">&nbsp;</td>
		<td align="center" class="tdbkg" width="15%"><a	
			href="javascript:goActionDocuments()"> <b>Formularios PDF</b> </a></td>
		<td align="center" width="15%">&nbsp;</td>			
	</tr>
</table>

<%
	if (ESG001001List.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>


<% if(request.getAttribute("flag").equals("E")){ %>
<table class="tbenter" width=100% height=10%>
	<tr>
		<td>
		<div align="center">
			<font size="2">
				<b> Informaci�n de Siniestros no disponible, reporte este problema. </b>
			</font>
		</div>
		</td>
	</tr>
</table>

<%} %>


	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="5%"></th>
			<th align="center" nowrap width="10%">N&uacute;mero</th>
			<th align="center" nowrap width="10%">Codigo</th>
			<th align="center" nowrap width="30%">Descripcion</th>
			<th align="center" nowrap width="10%">Estado</th>
			<th align="center" nowrap width="10%">Fecha de Apertura</th>
			<th align="center" nowrap>&nbsp;</th>	
			<th align="center" nowrap width="10%">Siniestrado</th>		
			<th align="center" nowrap width="10%">Fecha Siniestro</th>	
					<th align="center" nowrap width="10%">Fecha de Retracto</th>	
		</tr>
		<%
			ESG001001List.initRow();
				boolean firstTime = true;
				String chk = "";
				while (ESG001001List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ESG001001Message convObj = (ESG001001Message) ESG001001List
							.getRecord();
		%>
		<tr>
				<td nowrap><input type="radio" name="key" id="key"
					value="<%=ESG001001List.getCurrentRow()%>" onclick="showAddInfo(<%=ESG001001List.getCurrentRow()%>);"
					<%=chk%>></td>
			<td nowrap align="center"><input type="hidden" name="PAC<%=ESG001001List.getCurrentRow()%>" value="<%=convObj.getE01PACNUM()%>">
			<a href="javascript:goAction('203','<%=ESG001001List.getCurrentRow()%>');"><%=Util.formatCell(convObj.getE01PACNUM())%></a>
			</td>
			<td nowrap align="center"><input type="hidden" name="COD<%=ESG001001List.getCurrentRow()%>" value="<%=convObj.getE01PACCOD()%>">
			<a href="javascript:goAction('203','<%=ESG001001List.getCurrentRow()%>');"><%=Util.formatCell(convObj.getE01PACCOD())%></a>
			</td>			
			<td nowrap align="left"><a href="javascript:goAction('203','<%=ESG001001List.getCurrentRow()%>');"><%=convObj.getE01PACDSC()%></a></td>
			<td nowrap align="center"><a href="javascript:goAction('203','<%=ESG001001List.getCurrentRow()%>');"><% if (convObj.getE01PACSTS().equals("1")) out.print("<b>Vigente</b>"); 
																			 if (convObj.getE01PACSTS().equals("2")) out.print("<b>Cancelado</b>");
																			 if (convObj.getE01PACSTS().equals("3")) out.print("<b>Bloqueado</b>");
																			 if (convObj.getE01PACSTS().equals("4")) out.print("<b>Suspendido</b>");
																			 if	(convObj.getE01PACSTS().equals("5")) out.print("<b>Retractado</b>");
																			  %></a></td>
			<td nowrap align="center"><a href="javascript:goAction('203','<%=ESG001001List.getCurrentRow()%>');"><%=Util.formatCell(convObj.getE01PACFID() + "/"	+ convObj.getE01PACFIM() + "/" + convObj.getE01PACFIY())%></a>
			</td>
			<td nowrap align="center"><a href="javascript:goAction('203','<%=ESG001001List.getCurrentRow()%>');"><%=Util.formatCell(convObj.getE01DSCRTY())%></a></td>
			<td nowrap align="center" ><a href="javascript:goAction('203','<%=ESG001001List.getCurrentRow()%>');"><%=Util.formatCell(convObj.getE01SININD()) %> </a> </td>
			<td nowrap align="center"><a href="javascript:goAction('203','<%=ESG001001List.getCurrentRow()%>');"><%
			
			if(!convObj.getE01SINFED().equals("0")){
			out.print(Util.formatCell(convObj.getE01SINFED()+ "/"+ convObj.getE01SINFEM()+"/"+convObj.getE01SINFEY()));
			}
			
			%></a></td>		
			<td nowrap align="center"><a href="javascript:goAction('203','<%=ESG001001List.getCurrentRow()%>');">
			<%
			if(!convObj.getE01SREFRD().equals("0")){
			out.print(Util.formatCell(convObj.getE01SREFRD()+"/"+convObj.getE01SREFRM()+"/"+ convObj.getE01SREFRY()+" "+convObj.getE01SREHOR())); 
			}
			
			%>
			</a></td>
		
		</tr>
		<%
			}
		%>
	</table>


<script type="text/javascript">
     showAddInfo(0);     
     
</script> <br>

<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (ESG001001List.getShowPrev()) {
					int pos = ESG001001List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSESG0010?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (ESG001001List.getShowNext()) {
					int pos = ESG001001List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSESG0010?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>
</form>

</body>
</html>

