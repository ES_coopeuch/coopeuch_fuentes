/**
 * DatosConsulta.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class DatosConsulta  implements java.io.Serializable {
    private java.lang.String rut;
    private java.lang.String dv;
    private java.lang.String serie;
    private java.lang.String consultas;

    public DatosConsulta() {
    }

    public java.lang.String getRut() {
        return rut;
    }

    public void setRut(java.lang.String rut) {
        this.rut = rut;
    }

    public java.lang.String getDv() {
        return dv;
    }

    public void setDv(java.lang.String dv) {
        this.dv = dv;
    }

    public java.lang.String getSerie() {
        return serie;
    }

    public void setSerie(java.lang.String serie) {
        this.serie = serie;
    }

    public java.lang.String getConsultas() {
        return consultas;
    }

    public void setConsultas(java.lang.String consultas) {
        this.consultas = consultas;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        DatosConsulta other = (DatosConsulta) obj;
        boolean _equals;
        _equals = true
            && ((this.rut==null && other.getRut()==null) || 
             (this.rut!=null &&
              this.rut.equals(other.getRut())))
            && ((this.dv==null && other.getDv()==null) || 
             (this.dv!=null &&
              this.dv.equals(other.getDv())))
            && ((this.serie==null && other.getSerie()==null) || 
             (this.serie!=null &&
              this.serie.equals(other.getSerie())))
            && ((this.consultas==null && other.getConsultas()==null) || 
             (this.consultas!=null &&
              this.consultas.equals(other.getConsultas())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        DatosConsulta history = (DatosConsulta) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        DatosConsulta history = (DatosConsulta) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getRut() != null) {
            _hashCode += getRut().hashCode();
        }
        if (getDv() != null) {
            _hashCode += getDv().hashCode();
        }
        if (getSerie() != null) {
            _hashCode += getSerie().hashCode();
        }
        if (getConsultas() != null) {
            _hashCode += getConsultas().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
