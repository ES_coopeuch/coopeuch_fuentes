<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Consulta de Certificados</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cdMant" class="datapro.eibs.beans.EDL016002Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

   builtNewMenu(cd_i_opt);
 
</SCRIPT>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
 		error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
  out.println("<SCRIPT> initMenu(); </SCRIPT>");
 

%> 
<h3 align="center">Informaci&oacute;n B&aacute;sica<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cd_inq_basic, EDL0130"></h3>
<hr size="4">
<form method="post" action="eIBS_R04M07.pages.s.eIBS_R04M07.pages.s.eIBS_R04M07.pages.s._R04M07_WebApp.eIBS_R04M07.pages.s.eIBS_R04M07.pages.s.eIBS_R04M07.pages.s._R04M07_WebApp.eIBS_R04M07.pages.s.eIBS_R04M07.pages.s.eIBS_R04M07.pages.s._R04M07_WebApp.eIBS_R04M07.pages.s.eIBS_R04M07.pages.s.eIBS_R04M07.pages.s._R04M07_WebApp.eIBS_R04M07.pages.s.eIBS_R04M07.pages.s.eIBS_R04M07.pages.s.javascripts.nav_aid.js" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="41">
  <% if (!cdMant.getE02PENDAP().trim().equals("")) { %> 
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
  	<tr>
  		<td align="right" valign="top" width="85%" style="color:red;font-size:12;"><b><%=cdMant.getE02PENDAP()%></b></td>
  		<td width="5%"><h4>&nbsp;</h4></td>
  	</tr>
  </table>
  <% } %>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="14%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="9%" > 
              <div align="left"> 
                <input type="text" name="E02CUN2" size="10" maxlength="9" readonly value="<%= userPO.getHeader2().trim()%>">
              </div>
            </td>
            <td nowrap width="12%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E02NA12" size="45" maxlength="45" readonly value="<%= userPO.getHeader3().trim()%>">
              </div>
            </td>
            <td nowrap > 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap ><b> 
              <input type="text" name="E02PRO2" size="5" maxlength="4" readonly value="<%= userPO.getHeader1().trim()%>">
              </b></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="14%"> 
              <div align="right"><b>Contrato :</b></div>
            </td>
            <td nowrap width="9%"> 
              <div align="left"> 
                <input type="text" name="E02ACC" size="13" maxlength="12" value="<%= userPO.getIdentifier().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="12%"> 
              <div align="right"><b>Oficial :</b></div>
            </td>
            <td nowrap width="33%"> 
              <div align="left"><b> 
                <input type="text" name="E02NA122" size="45" maxlength="45" readonly value="<%= userPO.getOfficer().trim()%>">
                </b> </div>
            </td>
            <td nowrap width="11%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="21%"> 
              <div align="left"><b> 
                <input type="text" name="E02DEACCY" size="4" maxlength="3" value="<%= userPO.getCurrency().trim()%>" readonly>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table class=tbenter>
   <tr > 
      <td nowrap> 
   		<h4>Informaci&oacute;n B&aacute;sica</h4>
      </td>
      <td nowrap align=left> 
   		<b>CONTRATO :<font color="#ff6600"> <% 	if (cdMant.getE02COLATR().trim().equals("") )
   													out.print("NO GARANTIZADO");
   												else
   													out.print(cdMant.getE02COLATR());
   											%></font></b>
      </td>
      <td nowrap align=right> 
   		<b>ESTADO :</b>
      </td>
      <td nowrap> 
   		<b><font color="#ff6600"><%= cdMant.getE02STATUS().trim()%></font></b>
      </td>
    </tr>
  </table>
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trclear"> 
            <td nowrap width="26%" height="35"> 
              <div align="right">Nombre del Certificado :</div>
            </td>
            <td nowrap width="31%" height="35"> 
              <input type="text" name="E02DEANME" size="60" maxlength="80" value="<%= cdMant.getE02DEANME().trim()%>" readonly>
            </td>
            <td nowrap width="20%" height="35"> 
            </td>
            <td nowrap width="23%" height="35"> 
            </td>
          </tr>        
          <tr id="trdark"> 
            <td nowrap width="26%" height="35"> 
              <div align="right">Monto Original :</div>
            </td>
            <td nowrap width="31%" height="35"> 
              <input type="text" name="E02DEAOAM" size="23" maxlength="17" value="<%= cdMant.getE02DEAOAM().trim()%>" readonly>
            </td>
            <td nowrap width="20%" height="35"> 
              <div align="right">Fecha de Apertura :</div>
            </td>
            <td nowrap width="23%" height="35"> 
              <input type="text" name="E02DEAOD1" size="3" maxlength="2" value="<%= cdMant.getE02DEAOD1().trim()%>" readonly>
              <input type="text" name="E02DEAOD2" size="3" maxlength="2" value="<%= cdMant.getE02DEAOD2().trim()%>" readonly>
              <input type="text" name="E02DEAOD3" size="5" maxlength="4" value="<%= cdMant.getE02DEAOD3().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="26%"> 
              <div align="right">Provisi&oacute;n de Inter&eacute;s :</div>
            </td>
            <td nowrap width="31%"> 
              <input type="text" readonly name="E02DEAIFL" size="40" maxlength="40" 
				  value="<% if (cdMant.getE02DEAIFL().equals("1")) out.print("Capital Vigente");
							else if (cdMant.getE02DEAIFL().equals("2")) out.print("Capital Original");
							else if (cdMant.getE02DEAIFL().equals("3")) out.print("Capital Vigente - Capital Vencido");
							else if (cdMant.getE02DEAIFL().equals("4")) out.print("No Calcula Intereses");
							else out.print("");%>" >

            </td>
            <td nowrap width="20%"> 
              <div align="right">Fecha de Vencimiento :</div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" name="E02DEAMD1" size="3" maxlength="2" value="<%= cdMant.getE02DEAMD1().trim()%>" readonly>
              <input type="text" name="E02DEAMD2" size="3" maxlength="2" value="<%= cdMant.getE02DEAMD2().trim()%>" readonly>
              <input type="text" name="E02DEAMD3" size="5" maxlength="4" value="<%= cdMant.getE02DEAMD3().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark">
            <td nowrap width="26%" height="23">
              <div align="right">Per&iacute;odo Base :</div>
            </td>
            <td nowrap width="31%" height="23"> 
              <input type="text" name="E02DEABAS" size="3" maxlength="3" value="<%= cdMant.getE02DEABAS().trim()%>" readonly>
            </td>
            <td nowrap width="20%" height="23"> 
              <div align="right">T&eacute;rmino :</div>
            </td>
            <td nowrap width="23%" height="23"> 
              <input type="text" name="E02DEATRM" size="5" maxlength="5" value="<%= cdMant.getE02DEATRM().trim()%>" readonly>
              <input type="text" name="E02DEATRC" size="10" 
				  value="<% if (cdMant.getE02DEATRC().equals("D")) out.print("D&iacute;a(s)");
							else if (cdMant.getE02DEATRC().equals("M")) out.print("Mes(es)");
							else if (cdMant.getE02DEATRC().equals("Y")) out.print("A&ntilde;o(s)");
							else out.print("");%>" 
				readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="26%" height="23"> 
              <div align="right">Tabla de Tasa Flotante :</div>
            </td>
            <td nowrap width="31%" height="23"> 
              <input type="text" name="E02DEAFTB" size="3" maxlength="2" value="<%= cdMant.getE02DEAFTB().trim()%>" readonly>
              <input type="text" name="E02DEAFTY" size="10" 
				  value="<% if (cdMant.getE02DEAFTY().equals("FP")) out.print("Primaria");
							else if (cdMant.getE02DEAFTY().equals("FS")) out.print("Secundaria");
							else out.print("");%>"  readonly>
              <input type="text" readonly name="E02FTLRTE" size="10" maxlength="9" value="<%= cdMant.getE02FTLRTE().trim()%>">
            </td>
            <td nowrap width="20%" height="23"> 
              <div align="right">Tasa de Inter&eacute;s/Spread :</div>
            </td>
            <td nowrap width="23%" height="23"> 
              <input type="text" name="E02DEARTE" size="12" maxlength="11" value="<%= cdMant.getE02DEARTE().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="26%" height="19"> 
              <div align="right">Tipo de Inter&eacute;s :</div>
            </td>
            <td nowrap width="31%" height="19"> 
              <input type="text" readonly name="E02DEAICT" size="40" maxlength="35" 
				value="<% if (cdMant.getE02DEAICT().equals("S")) out.print("Al Vencimiento Calendario");
							else if (cdMant.getE02DEAICT().equals("M")) out.print("Al Vencimiento Comercial");
							else if (cdMant.getE02DEAICT().equals("P")) out.print("Prepagados Calendario");
							else if (cdMant.getE02DEAICT().equals("A")) out.print("Prepagados Comerciales");
							else if (cdMant.getE02DEAICT().equals("D")) out.print("Descontados Calendario");
							else if (cdMant.getE02DEAICT().equals("T")) out.print("Descontados Comerciales");
							else if (cdMant.getE02DEAICT().equals("R")) out.print("Capitalizados(CD's)");
							else if (cdMant.getE02DEAICT().equals("1")) out.print("Al Vencimiento Calendario");
							else if (cdMant.getE02DEAICT().equals("2")) out.print("Al Vencimiento Comercial");
							else if (cdMant.getE02DEAICT().equals("3")) out.print("Prepagados Calendario");
							else if (cdMant.getE02DEAICT().equals("4")) out.print("Prepagados Comerciales");
							else if (cdMant.getE02DEAICT().equals("5")) out.print("Descontados Calendario");
							else if (cdMant.getE02DEAICT().equals("6")) out.print("Descontados Comerciales");
							else if (cdMant.getE02DEAICT().equals("7")) out.print("Capitalizados (CD's)");
							else if (cdMant.getE02DEAICT().equals("8")) out.print("Regla 78");
							else out.print("");%>" >
            </td>
            <td nowrap width="20%" height="19"> 
              <div align="right">Condici&oacute;n de Contrato :</div>
            </td>
            <td nowrap width="23%" height="19"> 
              <input type="text" name="E02DEADLC" size="2" value="<%= cdMant.getE02DEADLC()%>" 
				readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="26%" height="19"> 
              <div align="right">Banco / Sucursal :</div>
            </td>
            <td nowrap width="31%" height="19"> 
              <input type="text" name="E02DEABNK" size="2" maxlength="2" value="<%= cdMant.getE02DEABNK().trim()%>" readonly>
              / 
              <input type="text" name="E02DEABRN" size="5" maxlength="4" value="<%= cdMant.getE02DEABRN().trim()%>" readonly>
            </td>
            <td nowrap width="20%" height="19"> 
              <div align="right">Cuenta Contable :</div>
            </td>
            <td nowrap width="23%" height="19">
              <input type="text" name="E02DEAGLN" size="18" maxlength="16" value="<%= cdMant.getE02DEAGLN().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="26%" height="19"> 
              <div align="right">Tasa de Cambio :</div>
            </td>
            <td nowrap width="31%" height="19"> 
              <input type="text" name="E02DEAEXR" size="11" maxlength="11" value="<%= cdMant.getE02DEAEXR().trim()%>" readonly>
            </td>
            <td nowrap width="20%" height="19"> 
              <div align="right">Centro de Costos :</div>
            </td>
            <td nowrap width="23%" height="19">
              <input type="text" name="E02DEACCN" size="8" maxlength="8" value="<%= cdMant.getE02DEACCN().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="26%" height="19"> 
              <div align="right">Contrapartida :</div>
            </td>
            <td nowrap width="31%" height="19"> 
              <input type="text" name="E02DEAOFB" size="2" maxlength="2" value="<%= cdMant.getE02DEAOFB().trim()%>" readonly>
              <input type="text" name="E02DEAOCR" size="4" maxlength="4" value="<%= cdMant.getE02DEAOCR().trim()%>" readonly>
              <input type="text" name="E02DEAOCY" size="3" maxlength="3" value="<%= cdMant.getE02DEAOCY().trim()%>" readonly>
              <input type="text" name="E02DEAOGL" size="17" maxlength="16" value="<%= cdMant.getE02DEAOGL().trim()%>" readonly>
              <input type="text" name="E02DEAOAC" size="13" maxlength="12" value="<%= cdMant.getE02DEAOAC().trim()%>" readonly>
            </td>
            <td nowrap width="20%" height="19"> 
              <div align="right">L&iacute;nea de Cr&eacute;dito :</div>
            </td>
            <td nowrap width="23%" height="19">
              <input type="text" name="E02DEACMC" size="9" maxlength="9" value="<%= cdMant.getE02DEACMC().trim()%>" readonly>
              <input type="text" name="E02DEACMN" size="4" maxlength="4" value="<%= cdMant.getE02DEACMN().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="26%" height="19"> 
              <div align="right">Ciclo/Fecha Revis. Tasa :</div>
            </td>
            <td nowrap width="31%" height="19"> 
              <input type="text" name="E02DEARRP" size="3" maxlength="3" value="<%= cdMant.getE02DEARRP().trim()%>" readonly>
              / 
              <input type="text" name="E02DEARR1" size="3" maxlength="2" value="<%= cdMant.getE02DEARR1().trim()%>" readonly>
              <input type="text" name="E02DEARR2" size="3" maxlength="2" value="<%= cdMant.getE02DEARR2().trim()%>" readonly>
              <input type="text" name="E02DEARR3" size="5" maxlength="4" value="<%= cdMant.getE02DEARR3().trim()%>" readonly>
            </td>
            <td nowrap width="20%" height="19"> 
              <div align="right"> C&oacute;digo Direcciones de Correo:</div>
            </td>
            <td nowrap width="23%" height="19"> 
              <input type="text" name="E02DEAMLA2" size="2" maxlength="1" value="<%= cdMant.getE02DEAMLA().trim()%>" readonly>
             </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="26%" height="19"> 
              <div align="right">Retenci&oacute;n / Impuesto :</div>
            </td>
            <td nowrap width="31%" height="19"> 
              <input type="text" readonly name="E02DEAWHF" size="35" maxlength="35" 
				value="<% if (cdMant.getE02DEAWHF().equals("1")) out.print("Retenci�n sobre Intereses ISR");
							else if (cdMant.getE02DEAWHF().equals("2")) out.print("Cobre del IVA");
							else if (cdMant.getE02DEAWHF().equals("3")) out.print("IVA mas ISR");
							else if (cdMant.getE02DEAWHF().equals("4")) out.print("IVA solo en Comisiones");
							else if (cdMant.getE02DEAWHF().equals("5")) out.print("IVA solo en Intereses");
							else if (cdMant.getE02DEAWHF().equals("6")) out.print("Debito Bancario IDB");
							else if (cdMant.getE02DEAWHF().equals("7")) out.print("IDB mas ISR");
							else if (cdMant.getE02DEAWHF().equals("8")) out.print("IDB mas IVA");
							else if (cdMant.getE02DEAWHF().equals("9")) out.print("Todo Tipo de Impuesto");
							else if (cdMant.getE02DEAWHF().equals("N")) out.print("No Calcula Impuestos");
							else if (cdMant.getE02DEAWHF().equals("F")) out.print("Cobro del FECI");
							else if (cdMant.getE02DEAWHF().equals("G")) out.print("FECI e ITBMS(Panam�)");
							else out.print("");%>" >
            </td>
            <td nowrap width="20%" height="19"> 
              <div align="right">&#191;Bloqueo Correspondencia&#63; :</div>
            </td>
    	 		<TD>
           			<input type="radio" name="E02DEASUT" disabled value="Y" onClick="document.forms[0].E02DEASUT.value='Y'"
		  			<%if(cdMant.getE02DEASUT().equals("Y")) out.print("checked");%>>
              			S&iacute; 
           			<input type="radio" name="E02DEASUT" disabled value="N" onClick="document.forms[0].E02DEASUT.value='N'"
		  			<%if(cdMant.getE02DEASUT().equals("N")) out.print("checked");%>>
              			No 
    			</TD>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="26%" height="19"> 
              <div align="right">Porcentaje Garant&iacute;a :</div>
            </td>
            <td nowrap width="31%" height="19"> 
              <input type="text" name="E02DEACPE2" size="7" maxlength="7" value="<%= cdMant.getE02DEACPE().trim()%>" readonly>
            </td>
            <td nowrap width="20%" height="19"> 
              <div align="right">N&uacute;mero de Referencia :</div>
            </td>
            <td nowrap width="23%" height="19"> 
              <input type="text" name="E02DEAREF" size="12" maxlength="12" value="<%= cdMant.getE02DEAREF().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear">
            <td nowrap>
              <div align="right">Documento en Custodia :</div>
            </td>
            <td nowrap>
           <input type="text" name="E02DEAECU" size="12" 
				  value="<% if (cdMant.getE02DEAECU().equals("1")) out.print("Electronica");
							else if (cdMant.getE02DEAECU().equals("2")) out.print("Custodia Fisica");
							else out.print("No Custodia");%>" 
				readonly>
               </td>
            <td nowrap width="25%" > 
              <div align="right">Banco/Sucursal :</div>
            </td>
            <td nowrap width="23%">           
              <input type="text" name="E02DEABNK" size="2" maxlength="2" value="<%= cdMant.getE02DEABNK().trim()%>" readonly>
              <input type="text" name="E02DEABRN" size="4" maxlength="4" value="<%= cdMant.getE02DEABRN().trim()%>" readonly>
            </td>      
          </tr>
			<tr id="trdark">
          		<td nowrap width="25%" >
           			<div align="right">Documento Con Mandato :</div>
          		</td> 
    	 		<TD>
           			<input type="radio" name="E02DEA2TC" disabled value="Y" onClick="document.forms[0].E02DEA2TC.value='Y'"
		  			<%if(cdMant.getE02DEA2TC().equals("Y")) out.print("checked");%>>
              			S&iacute; 
           			<input type="radio" name="E02DEA2TC" disabled value="N" onClick="document.forms[0].E02DEA2TC.value='N'"
		  			<%if(cdMant.getE02DEA2TC().equals("N")) out.print("checked");%>>
              			No 
    			</TD>
	        	<td nowrap width="25%" >
	       			<div align="right">Cuenta Mandato :</div>
	       		</td> 
	    		<td>
	              <input type="text" name="E02DEAREA" size="13" maxlength="12" value="<%= cdMant.getE02DEAREA().trim()%>" readonly>
				</td>
			</tr>
			<tr id="trclear">
	        	<td nowrap width="25%" >
	       			<div align="right">Documento Impreso :</div>
	       		</td> 
	    		<td>
	       			<input type="radio" disabled name="E02DEAF01" value="Y" onClick="document.forms[0].E02DEAF01.value='Y'"
		  			<%if(cdMant.getE02DEAF01().equals("Y")) out.print("checked");%>>
	          			S&iacute; 
	       			<input type="radio" disabled name="E02DEAF01" value="N" onClick="document.forms[0].E02DEAF01.value='N'"
		  			<%if(cdMant.getE02DEAF01().equals("N")) out.print("checked");%>>
	           			No 
				</td>
            <td nowrap width="20%" height="19"> 
              <div align="right">Clase de Certificado :</div>
            </td>
            <td nowrap width="23%" height="19"> 
              <input type="text" name="E02DEACLF" size="2" maxlength="1" value="<%= cdMant.getE02DEACLF().trim()%>" readonly>
            </td>
			</tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Instrucciones de Renovaci&oacute;n</h4>
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="24%"> 
              <div align="right">C&oacute;digo de Renovaci&oacute;n :</div>
            </td>
            <td nowrap width="37%"> 
              <input type="text" name="E02DEAROC" size="2" maxlength="1" value="<%= cdMant.getE02DEAROC().trim()%>" readonly>
            </td>
            <td nowrap width="16%"> 
              <div align="right">Tasa / Tipo de Renovaci&oacute;n :</div>
            </td>
            <td nowrap width="23%">
              <input type="text" name="E02DEAROR" size="9" maxlength="9" value="<%= cdMant.getE02DEAROR().trim()%>" readonly>
              <input type="text" name="E02DEAHTM" size="2" maxlength="1" value="<%= cdMant.getE02DEAHTM().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="24%"> 
              <div align="right">Cuenta Contable de Repago :</div>
            </td>
            <td nowrap width="37%"> 
              <input type="text" name="E02DEAREB" size="2" maxlength="2" value="<%= cdMant.getE02DEAREB().trim()%>" readonly>
              <input type="text" name="E02DEARPR" size="4" maxlength="4" value="<%= cdMant.getE02DEARPR().trim()%>" readonly>
              <input type="text" name="E02DEARPC" size="3" maxlength="3" value="<%= cdMant.getE02DEARPC().trim()%>" readonly>
              <input type="text" name="E02DEARGL" size="17" maxlength="16" value="<%= cdMant.getE02DEARGL().trim()%>" readonly>
            </td>
            <td nowrap width="16%"> 
              <div align="right">Cuenta detalle de Repago :</div>
            </td>
            <td nowrap width="23%">
              <input type="text" name="E02DEARAC" size="13" maxlength="12" value="<%= cdMant.getE02DEARAC().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="24%" height="23"> 
              <div align="right">Monto de Renovaci&oacute;n :</div>
            </td>
            <td nowrap width="37%" height="23">
              <input type="text" name="E02DEAROA" size="15" maxlength="15" value="<%= cdMant.getE02DEAROA().trim()%>" readonly>
            </td>
            <td nowrap width="16%" height="23"> 
              <div align="right">Per&iacute;odo :</div>
            </td>
            <td nowrap width="23%" height="23">
              <input type="text" name="E02DEAROY" size="4" maxlength="3" value="<%= cdMant.getE02DEAROY().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="24%" height="19"> 
              <div align="right">Tabla de Tasa :</div>
            </td>
            <td nowrap width="37%" height="19">
              <input type="text" name="E02DEARTB" size="2" maxlength="2" value="<%= cdMant.getE02DEARTB().trim()%>" readonly>
            </td>
            <td nowrap width="16%" height="19"> 
              <div align="right">Forma de Pago :</div>
            </td>
            <td nowrap width="23%" height="19">
              <input type="text" name="E02DEAPVI" size="2" maxlength="1" value="<%= cdMant.getE02DEAPVI().trim()%>" readonly>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
</body>
</html>
