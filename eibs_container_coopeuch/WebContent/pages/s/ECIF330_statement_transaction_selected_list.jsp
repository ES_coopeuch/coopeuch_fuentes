<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Consulta de Cartolas</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%@ page import = "datapro.eibs.master.Util,datapro.eibs.beans.*" %>

<jsp:useBean id="stmlist" class="datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id="header" class="datapro.eibs.beans.ECIF33003Message" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

</SCRIPT>

</head>

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>


<h3 align="center">Consulta de Cartolas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="statement_transaction_selected_list.jsp, ECIF330"></h3>
<hr size="4">
<p>&nbsp;</p>

<form method="post">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" class="tbhead" align="center">
          <tr id=trdark> 
            <td nowrap width="10%" align="right"> 
              <div align="right">Cliente : </div>
            </td>
            <td nowrap width="10%" align="left"> <%=header.getE03ACMCUN()%> </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Nombre : </div>
            </td>
            <td nowrap width="30%"align="left"> <%= header.getE03CUSNA1()%> </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Identificación : </div>
            </td>
            <td nowrap width="30%" align="left"> <%= header.getE03CUSIDE()%> </td>
          </tr>
          <tr id=trclear> 
            <td nowrap width="10%" align="right"> Cuenta : </td>
            <td nowrap width="10%" align="left"> <%= header.getE03SELACC()%> </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Producto : </div>
            </td>
            <td nowrap width="30%" align="left"> 
            	<div> 
            	 <%=header.getE03ACMPRO()%> - 
            	 <%=header.getE03DSCPRO()%> 
                </div>  
            </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Oficial Cuenta : </div>
            </td>
            <td nowrap width="30%" align="left"> 
            	<div> 
            	 <%=header.getE03ACMOFC()%> - 
            	 <%=header.getE03DSCOFC()%> 
                </div>  
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <BR>
  <% if (stmlist.getNoResult()) {%>
  <br/>
  <br/>
 <br/>  
      <h3 align=center>No existen Movimientos de la Cuenta Para los Parametros Solicitados.</h3>
  <% } else { %>
  
  <table class="tableinfo">
    <tr > 
      <td nowrap>
        <table id="headTable" >
		    <tr id="trdark">  
		      <th align="center" nowrap>Fecha <br> Proceso</TH>
		      <th align="center" nowrap>Fecha <br> Valor</TH>	      
		      <th align="center" nowrap>Codigo</TH>
		      <th align="center" nowrap>Descipción</TH>
		      <th align="center" nowrap>Ref</TH>
		      <th align="center" nowrap>Monto Debito</TH>
		      <th align="center" nowrap>Monto Credito</TH>		      
		    </TR>
		</table>  
   		<div id="dataDiv1" class="scbarcolor" style="padding:0" nowrap>
    		<table id="dataTable"  >
               <%          		
          		stmlist.initRow();
   		  		while (stmlist.getNextRow()) {
          			ECIF33004Message message = (ECIF33004Message) stmlist.getRecord();
          		%>    		
          			  <tr> 
				         <td nowrap align="center"> <%=(message.getE04STDBDD().length()>1?"":"0")+message.getE04STDBDD()+"/"+(message.getE04STDBDM().length()>1?"":"0")+message.getE04STDBDM()+"/"+message.getE04STDBDY()%></td>
				         <td nowrap align="center"> <%=(message.getE04STDVDD().length()>1?"":"0")+message.getE04STDVDD()+"/"+(message.getE04STDVDM().length()>1?"":"0")+message.getE04STDVDM()+"/"+message.getE04STDVDY()%></td>	
				         <td nowrap align="center"> <%= message.getE04STDCDE()%></td>				         
				         <td nowrap align="left"> <%= message.getE04STDNAR()%></td>
				         <td nowrap align="center"> <%= message.getE04STDCKN()%></td>
				         <td nowrap align="right"> <%= message.getE04STDDBA()%></td>
				         <td nowrap align="right"> <%= message.getE04STDCRA()%></td>
				     </tr>
				 <%
          		   }         
                %>         
		     </table>   
  		</div>	
          		 
      </td>
    </tr>
  </table>

  <TABLE class="tbenter" WIDTH="98%" ALIGN=CENTER>
  <TR>
  <TD WIDTH="50%" ALIGN=LEFT>
  <%
        if ( stmlist.getShowPrev() ) {
      			int pos = stmlist.getFirstRec() - 6;//51
      			out.println("<A HREF=\""+request.getContextPath()+"/servlet/datapro.eibs.client.JSECIF330?SCREEN=250&Pos=" + pos +"\"><img src=\""+request.getContextPath()+"/images/s/previous_records.gif\" border=0></A>");
        }
 %> 
   </TD>
 	<TD WIDTH="50%" ALIGN=RIGHT>
 <%      
        if ( stmlist.getShowNext() ) {
      			int pos	 = stmlist.getLastRec();
      			out.println("<A HREF=\""+request.getContextPath()+"/servlet/datapro.eibs.client.JSECIF330?SCREEN=250&Pos=" + pos +"\"><img src=\""+request.getContextPath()+"/images/s/next_records.gif\" border=0></A>");
        }
%>
  	</TD>
 	</TR>
 	</TABLE>

  
  <% } %>
  
  </form>
  <SCRIPT Language="Javascript">
    function resizeDoc() {
       adjustEquTables(headTable, dataTable, dataDiv1,1,false);
    }
  	resizeDoc();
  	window.onresize=resizeDoc;
  </SCRIPT>  
</body>
</html>
