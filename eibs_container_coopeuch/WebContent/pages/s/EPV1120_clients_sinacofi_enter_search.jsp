<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<title>Plataforma de Venta</title>

<%
  String ni = (String)session.getAttribute("NIVEL"); 
  ni = (ni==null?"":ni);
 %>	
 
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<SCRIPT LANGUAGE="javascript">
function validar() {
	var rut = validateRut(document.forms[0].rut.value, <%=currUser.getE01INT()%>);
	if (rut == 0) {
		alert('Debe Introducir un Rut Valido Para continuar');
		document.forms[0].rut.focus();	
		return false;		
	} else if (document.forms[0].serie.value==""){
		alert('Debe Introducir una Serie Valida Para continuar');
		document.forms[0].serie.focus();
		return false;
	}
	//buscar que al menos un checkBox esta selected.
	document.forms[0].rut.value = rut;
	var serie = document.forms[0].serie.value;
	document.forms[0].serie.value = serie.toUpperCase(); 
	var sel=false;	
	if (document.forms[0].flat01.checked){
		sel=true;
	}else if (document.forms[0].flat02.checked){
		sel=true;	
	}else if (document.forms[0].flat82.checked){
		sel=true;	
	}
<%if ("M".equals(ni) || "A".equals(ni)){ %>	
	if (document.forms[0].flat07.checked){
		sel=true;	
	}
<%} %>	
<%if ("A".equals(ni)){ %>
	if (document.forms[0].flat08.checked){
		sel=true;
	}else if (document.forms[0].flat14.checked){
		sel=true;
	}else if (document.forms[0].flat16.checked){
		sel=true;
	}else if (document.forms[0].flat18.checked){
		sel=true;
	}else if (document.forms[0].flat26.checked){
		sel=true;
	}else if (document.forms[0].flat32.checked){
		sel=true;
	}	
<%} %>		
	if (sel==false){
		alert('Debe Seleccionar al menos una consulta para continuar..!!');
		document.forms[0].flat01.focus();
		return false;
	}
	
	return true;   
}
        
</SCRIPT>

</head>
<body>

<%
	if (!error.getERRNUM().equals("0")) {
		out.println("<script type=\"text/javascript\">");
		error.setERRNUM("0");
		out.println("showErrors()");
		out.println("</script>");
	}
%>

<h3 align="center">
Consulta BUREAU
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="clients_sinacofi_enter_search.jsp,EPV1120"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1120" onsubmit="return validar();">
<input type="hidden" name="SCREEN" value="101">
<input type=HIDDEN name="E01UBK"  value="<%= currUser.getE01UBK().trim()%>">
<br>

<table id="TBHELPN" width="100%" border="0" cellspacing="0"
	cellpadding="0" style="margin-left: center; margin-right: center;">
	<tr>
		<td align="right" width="50%" nowrap>Rut :&nbsp;</td>
		<td  width="50%">
           	<input type="text" name="rut" maxlength="25" size="27" value="" >
           	<a id="linkHelp" href="javascript:GetCustomerDescId('cun','nombre','rut')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="null" align="bottom" border="0"/></a>
           	<img src="<%=request.getContextPath()%>/images/Check.gif" align="bottom" border="0"/>
		
			
		</td>
	</tr>
	<tr>
		<td align="right"  nowrap>Serie  :&nbsp;</td>
		<td  >			
			<input type="text" name="serie" maxlength="10" size="12" value=""><img src="<%=request.getContextPath()%>/images/Check.gif" align="bottom" border="0"/>        
		</td>
	</tr>
	<tr>
		<td align="right"  nowrap>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>	
	<tr>
		<td align="center"  nowrap colspan="2" height="40">			
			<input type="hidden" name="cun" maxlength="25" size="15" value="" readonly="readonly">
			<input type="hidden" name="nombre" maxlength="25" size="15" value="" readonly="readonly">
			Seleccione una consulta a BUREAU :
		</td>
	</tr>	
	<tr>
		<td align="right" width="50%" nowrap>
			<input type="checkbox" name="flat01"  onclick="" value="1">   			
		</td>
		<td  width="50%" align="left">
			 Estado C�dula con Nro Serie Registro Civil
		</td>
	</tr>
	<tr>
		<td align="right" width="50%" nowrap>
			<input type="checkbox" name="flat02"  onclick="" value="2">   			
		</td>
		<td  width="50%" align="left">
			 Sistema de Morosidad
		</td>
	</tr>
	<tr>
		<td align="right" width="50%" nowrap>
			<input type="checkbox" name="flat82"  onclick="" value="82">   			
		</td>
		<td  width="50%" align="left">
			 BIC - Protestos Y Documentos Vigentes
		</td>
	</tr>
 <%if ("M".equals(ni) || "A".equals(ni)){ %>
	<tr>
		<td align="right" width="50%" nowrap>
			<input type="checkbox" name="flat07"  onclick="" value="7">   			
		</td>
		<td  width="50%" align="left">
			 Infractores Laborales y Previsionales
		</td>
	</tr>	
 <%} %>
<%if ("A".equals(ni)){ %> 						
	<tr>
		<td align="right" width="50%" nowrap>
			<input type="checkbox" name="flat08"  onclick="" value="8">   			
		</td>
		<td  width="50%" align="left">
			 Prendas sin Desplazamiento
		</td>
	</tr>
	<tr>
		<td align="right" width="50%" nowrap>
			<input type="checkbox" name="flat18"  onclick="" value="18">   			
		</td>
		<td  width="50%" align="left">
			 Direcciones Asociadas al Rut 
		</td>
	</tr>
	<tr>
		<td align="right" width="50%" nowrap>
			<input type="checkbox" name="flat26"  onclick="" value="26">   			
		</td>
		<td  width="50%" align="left">
			 Directorio de Personas 
		</td>
	</tr>
	<tr>
		<td align="right" width="50%" nowrap>
			<input type="checkbox" name="flat32"  onclick="" value="32">   			
		</td>
		<td  width="50%" align="left">
			 Bienes ra�ces 
		</td>
	</tr>
<%} %>	
	<tr>
		<td align="right" width="50%" nowrap height="40" >
		</td>
		<td  width="50%" align="left">
		</td>
	</tr>
			
</table>

  <p align="center">
  	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>

</form>
</body>
</html>
 