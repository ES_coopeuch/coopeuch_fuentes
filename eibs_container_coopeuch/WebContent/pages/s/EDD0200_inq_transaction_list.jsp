<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Transacciones PostVenta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%@ page import = "datapro.eibs.master.Util,datapro.eibs.beans.*" %>

<% datapro.eibs.beans.JBObjList actInqTrxlist = (datapro.eibs.beans.JBObjList)session.getAttribute("actInqTrxlist"); %>

<jsp:useBean id="msgSerch" class= "datapro.eibs.beans.EDD020001Message"  scope="session" />
<jsp:useBean id="Resumen" class= "datapro.eibs.beans.EDD020002Message"  scope="session" />
<jsp:useBean id="userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/CrossBrowserFunctions.js"> </SCRIPT>
<SCRIPT Language="Javascript">
function PrintPreview(fecha, hora,trxn,dipositivo,numRec,numTarj,monto,codAut,typTrxn,state,ipareo) {
	var pg = '<%=request.getContextPath()%>/pages/s/EDD0200_inq_transaction_list_details.jsp?fecha='+fecha+'&hora='+hora+'&trxn='+trxn+'&dipositivo='+dipositivo+'&numRec='+numRec+'&numTarj='+numTarj+'&monto='+monto+'&codAut='+codAut+'&typTrxn='+typTrxn+'&state='+state+'&ipareo='+ipareo;	
	CenterWindow(pg,720,500,2);

}
  function show(event,fecha, hora,trxn,dipositivo,numRec,numTarj,monto,codAut,typTrxn,state,ipareo){
  	 hidediv('InqWindow');
     document.getElementById("fecha").value=fecha;  
     document.getElementById("hora").value=hora;  
     document.getElementById("trxn").value=trxn;  
     
     document.getElementById("dipositivo").value=dipositivo;  
     document.getElementById("numRec").value=numRec;  
     document.getElementById("numTarj").value=numTarj;  
     document.getElementById("monto").value=monto;  
     document.getElementById("codAut").value=codAut;  
     document.getElementById("typTrxn").value=typTrxn;  
     document.getElementById("state").value=state;
     document.getElementById("ipareo").value=ipareo;  
     
     
     //move then windos
     var margin=5;
     var tempX = 0;     
     var tempY = 0;
     tempX = event.clientX + document.body.scrollLeft;
     tempY = event.clientY + document.body.scrollTop;
     if (tempY < 0){tempY = 0;}
     if (tempX < 0){tempX = 0;}
     var elementoDiv = document.getElementById('inqWindow');
     elementoDiv.style.top = (tempY+margin);    
     elementoDiv.style.left = (tempX+margin); 
     //show the windows
      showdiv('inqWindow');    
      //document.getElementById("state").focus();
  }
  
  
</SCRIPT>

</head>

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>


<h3 align="center">Consulta Transacciones ATM Cuenta Vista<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="inq_transaction_list.jsp,EDD0200"></h3>
<hr size="4">
  <br/>
  <br/>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" class="tbhead" align="center">
          <tr id=trdark> 
            <td nowrap width="10%" align="right"> 
              <div align="right">Fecha Contable: </div>
            </td>
            <td nowrap width="10%" align="left">
	            <%=(msgSerch.getE01LOGFCD().length()>1?"":"0")+msgSerch.getE01LOGFCD()+"/"+(msgSerch.getE01LOGFCM().length()>1?"":"0")+msgSerch.getE01LOGFCM()+"/"+msgSerch.getE01LOGFCY()%>
            </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Canal : </div>
            </td>
			<td nowrap width="10%" align="left">
			<%=("ATMBECH".equals(msgSerch.getE01LOGCAJ())?"Banco Estado":("ATMTRBK".equals(msgSerch.getE01LOGCAJ())?"Transbank":("Redbanc"))) %>
			</td> 
            <td nowrap width="10%" align="right"> 
              <div align="right">Tipo de Consulta  : </div>
            </td>
			<td nowrap width="10%" align="left">
			<%= "D".equals(msgSerch.getE01FLGSUP())?"Detalle":"Resumen"%>
			</td> 			           
          </tr>          
        </table>
      </td>
    </tr>
  </table>  
  <% if (actInqTrxlist != null && actInqTrxlist.getNoResult()) {%>
  <br/>
  <br/>
  <br/>  
      <h3 align=center>No existen Movimientos Detalles Para la fecha y canal seleccionado.</h3>
  <br/>
  <br/>
  <br/>  
  <% } else if (actInqTrxlist != null) { %>
  
  <table class="tableinfo">
    <tr > 
      <td nowrap>
        <table id="headTable" >
		    <tr id="trdark">  
			  <th align="center" nowrap width="5%"> Detalle </TH>		    		   
		      <th align="center" nowrap width="10%">Fecha Transacción</TH>
		      <th align="center" nowrap width="30%">Descripción Transacción</TH>			      	      
		      <th align="center" nowrap width="20%">Número Tarjeta</TH>
		      <th align="center" nowrap width="15%">Monto</TH>
		      <th align="center" nowrap width="10%">Tipo Transacción</TH>
		      <th align="center" nowrap width="10%">Indicador de Pareo</TH>		      
		    </TR>
		</table>  
   		<div id="dataDiv1" class="scbarcolor" style="padding:0" nowrap>
    		<table id="dataTable"  >
               <%          		
          		actInqTrxlist.initRow();
   		  		while (actInqTrxlist.getNextRow()) {
          			EDD020001Message message = (EDD020001Message) actInqTrxlist.getRecord();
          			String fecha = (message.getE01LOGDTD().length()>1?"":"0")+message.getE01LOGDTD()+"/"+(message.getE01LOGDTM().length()>1?"":"0")+message.getE01LOGDTM()+"/"+message.getE01LOGDTY();  
          		%>    		
          			  <tr> 
						<td nowrap align="center">
				         <a href="javascript:PrintPreview('<%=fecha%>','<%= message.getE01LOGTME()%>','<%= message.getE01LOGTCO()%>','<%= message.getE01LOGDIS()%>','<%= message.getE01LOGNRE()%>','<%= message.getE01LOGTAR()%>','<%= message.getE01LOGAMT()%>','<%= message.getE01LOGAUT()%>','<%= message.getE01LOGREV()%>','<%= message.getE01LOGPAR()%>','<%= message.getD01LOGPAR()%>')" onmouseout="hidediv('InqWindow');" onmouseover="show(event,'<%=fecha%>','<%= message.getE01LOGTME()%>','<%= message.getE01LOGTCO()%>','<%= message.getE01LOGDIS()%>','<%= message.getE01LOGNRE()%>','<%= message.getE01LOGTAR()%>','<%= message.getE01LOGAMT()%>','<%= message.getE01LOGAUT()%>','<%= message.getE01LOGREV()%>','<%= message.getE01LOGPAR()%>','<%= message.getD01LOGPAR()%>')">
				         	<img src="<%=request.getContextPath()%>/images/page_properties.gif" alt="Detalles" align="bottom" border="0"> 
				         </a>
				         </td>	          			  
				         <td nowrap align="center">
				         	<%=fecha%>
				         </td>				         
				         <td nowrap align="left">
				         	<%= message.getD01LOGTCO()%>
				         </td>
				         <td nowrap align="center">
				         	<%= message.getE01LOGTAR()%>
				         </td>
				         <td nowrap align="right">
							<%= message.getE01LOGAMT()%>
				         </td>
				         <td nowrap align="center">
				         	<%= message.getE01LOGREV()%>
				         </td>	
				         <td nowrap align="center">
				         	<%= message.getE01LOGPAR()%>
				         </td>					         			         
				     </tr>
				 <%
          		   }         
                %>         
		     </table>   
  		</div>	
          		 
      </td>
    </tr>
  </table>
  
  <SCRIPT Language="Javascript">
    function resizeDoc() {
       adjustEquTables(headTable, dataTable, dataDiv1,1,false);
    }
  	resizeDoc();
  	window.onresize=resizeDoc;
  </SCRIPT>
  
  <div id="InqWindow" class="search" style="top:30%;left:20%;position:absolute;z-index:2" >
    <TABLE class="tableinfo" align="center" style="width:95%">
       <tr id="trdark">
         <td colspan="2"><b>Detalle Transacción</b></td>
       </tr> 
       <TR id="trclear"> 
         <td align="right">
           Fecha Transacción : 
         </td>
         <td>
            <input type="text" id="fecha" name="fecha" size="12" alt="Fecha Transacción" readOnly="readonly" />
         </td> 
        </tr>
        <tr id="trdark" > 
         <td align="right">Hora Transacción :  </td>
         <td>
            <input type="text" id="hora" name="hora" size="12" alt="Hora Transacción" readOnly="readonly"/> (HHMMSS)
         </td>
         </tr>
       <TR id="trclear"> 
         <td align="right">
           Transacción : 
         </td>
         <td>
            <input type="text" id="trxn" name="trxn" size="30"  alt="Transacción" readOnly="readonly" />
         </td> 
        </tr>
        <tr id="trdark" > 
         <td align="right">
            Dispositivo : 
         </td>
         <td >
            <input type="text" id="dipositivo" name="dipositivo" size="30" alt="Dispositivo" readOnly="readonly"/>
         </td>
         </tr>

		<TR id="trclear"> 
         <td align="right">
           Numero Recibo : 
         </td>
         <td>
            <input type="text" id="numRec" name="numRec" size="30"  alt="Numero Recibo" readOnly="readonly" />
         </td> 
        </tr>
        <tr id="trdark" > 
         <td align="right">
            Numero Tarjeta : 
         </td>
         <td >
            <input type="text" id="numTarj" name="numTarj" size="30" alt="Numero Tarjeta" readOnly="readonly"/>
         </td>
         </tr>

		<TR id="trclear"> 
         <td align="right">
           Monto : 
         </td>
         <td>
            <input type="text" id="monto" name="monto" size="30"  alt="Monto" readOnly="readonly" />
         </td> 
        </tr>
        <tr id="trdark" > 
         <td align="right">
            Codigo Autorización : 
         </td>
         <td >
            <input type="text" id="codAut" name="codAut" size="30" alt="Codigo Autorización" readOnly="readonly"/>
         </td>
         </tr>
         
		<TR id="trclear"> 
         <td align="right">
           Tipo de Transacción : 
         </td>
         <td>
            <input type="text" id="typTrxn" name="typTrxn" size="30"  alt="Tipo de Transacción" readOnly="readonly" />
         </td> 
        </tr>
        <tr id="trdark" > 
         <td align="right">
            Estado : 
         </td>
         <td >
            <input type="text" id="state" name="state" size="30" alt="Estado" readOnly="readonly"/>
         </td>
         </tr>
		<tr id="trclear" > 
         <td align="right">
            Indicador de Pareo : 
         </td>
         <td >
            <input type="text" id="ipareo" name="ipareo" size="30" alt="Estado" readOnly="readonly"/>
         </td>
         </tr>         
		<TR id="trdark"> 
         <td align="right">
          &nbsp;
         </td>
         <td>
          &nbsp;
         </td> 
        </tr>         
      </table>
    </div>

    <script language="JavaScript">
       hidediv('InqWindow');
    </script>    
  <% } %>
   
    <h4>Resumen de Transacciones</h4><table class="tableinfo">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
 		  <tr id="trdark"> 
            <td nowrap colspan="2"> 
              <div align="center" style="font: bold;">Consulta Saldo</div>
            </td>
            <td nowrap colspan="2">
             <div align="center" style="font: bold;">Compras</div>
            </td>
          </tr>        
          <tr id="trclear"> 
            <td nowrap width="27%" > 
              <div align="right">Cantidad Consultas Directas :</div>
            </td>
            <td nowrap width="21%" >
             	<div align="left"><%=Resumen.getE02CONSLD()%></div>
            </td>
            <td nowrap width="29%" > 
              <div align="right">Cantidad  Compras Directas : </div>
            </td>
            <td nowrap width="23%" >   
              <div align="left"><%=Resumen.getE02CNCMCV()%></div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="27%" > 
              <div align="right">Cantidad Reversa : </div>
            </td>
            <td nowrap width="21%" >
				<div align="left"><%=Resumen.getE02REVSLD()%></div>                          
            </td>
            <td nowrap width="29%" >              
            	<div align="right">Monto Compras Directas : </div>
            </td>
            <td nowrap width="23%" >
            	<div align="left"><%=Resumen.getE02MTCMCV()%></div>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="27%" > 
              <div align="right">Cantidad  Notificaciones : </div>
            </td>
            <td nowrap width="21%" >
            	<div align="left"><%=Resumen.getE02NOTSLD()%></div>
            </td>				            
            <td nowrap width="29%"> 
              <div align="right">Cantidad  Compras  Reversas   : </div>
            </td>
            <td nowrap width="23%">  				
				<div align="left"><%=Resumen.getE02CNRVCM()%></div>                        
            </td>
            
          </tr>   
          <tr id="trdark"> 
           <td nowrap colspan="2"> 
              <div align="center" style="font: bold;">Giros</div>
            </td>
            <td nowrap width="29%"> 
              <div align="right">Monto  Compras Reversas : </div>
            </td>
            <td nowrap width="23%">
            	<div align="left"><%=Resumen.getE02MTRVCM()%></div>                    
            </td>
            
          </tr>    
          <tr id="trclear"> 
            <td nowrap width="27%" > <div align="right">Cantidad Giros Directos : </div></td>
            <td nowrap width="21%" >
				<div align="left"><%=Resumen.getE02NUMGIR()%></div>            
	        </td>
            <td nowrap width="29%" > 
                <div align="right">Cantidad  Compras Notificaciones : </div>  
            </td>
            <td nowrap width="23%" >                               
            	<div align="left"><%=Resumen.getE02CNNTCM()%></div> 
            </td>
          </tr>         
          <tr id="trdark"> 
            <td nowrap width="27%" > <div align="right">Monto Giros Directos : </div></td>
            <td nowrap width="21%" >
            	<div align="left"><%=Resumen.getE02MTOGIR()%></div>
            </td>
            <td nowrap width="29%" > 
                <div align="right">Monto  Compras Notificaciones : </div> 
            </td>
            <td nowrap width="23%" >                               
            	<div align="left"><%=Resumen.getE02MTNTCM()%></div>
            </td>
          </tr>       
                                         
		  <tr id="trclear"> 
            <td nowrap width="27%" > <div align="right">Cantidad Giros Reserva : </div></td>
            <td nowrap width="21%" >
            	<div align="left"><%=Resumen.getE02CNRVGR()%></div>
            </td>
           <td nowrap colspan="2">
             <div align="center" style="font: bold;">Cambio Offset ATM</div>
            </td>
          </tr>  
		 <tr id="trdark"> 
            <td nowrap width="27%" > <div align="right">Monto Giros Rserva : </div></td>
            <td nowrap width="21%" >
            	<div align="left"><%=Resumen.getE02MTRVGR()%></div>
            </td>
            <td nowrap width="29%" > 
              <div align="right">Cantidad Cambios PIN Directas : </div>
            </td>
            <td nowrap width="23%" >                               
            	<div align="left"><%=Resumen.getE02NUMPIN()%></div>
            </td>
          </tr> 
		 <tr id="trclear"> 
            <td nowrap width="27%" > <div align="right">Cantidad Giros Notificaciones : </div></td>
            <td nowrap width="21%" >
            	<div align="left"><%=Resumen.getE02CNNTGR()%></div>
            </td>
            <td nowrap width="29%" ><div align="right">Cantidad Cambios PIN Reversas : </div></td>
            <td nowrap width="23%" ><div align="left"><%=Resumen.getE02REVPIN()%></div></td>
          </tr>  
		 <tr id="trdark"> 
            <td nowrap width="27%" > <div align="right">Monto Giros Notificaciones : </div></td>
            <td nowrap width="21%" >
            		<div align="left"><%=Resumen.getE02MTNTGR()%></div>
            </td>
            <td nowrap width="29%" >
            <div align="right">Cantidad Cambios PIN Notificaciones : </div>
            </td>
            <td nowrap width="23%" ><div align="left"><%=Resumen.getE02NOTPIN()%></div></td>
          </tr>                                                                   
        </table>
        </td>
    </tr>
  </table>
      
</body>
</html>
