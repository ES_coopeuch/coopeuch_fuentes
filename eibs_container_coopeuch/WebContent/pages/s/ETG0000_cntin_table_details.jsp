<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<html>
<head>
<title>Tabla de Codigo</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="refCodes" class="datapro.eibs.beans.ETG000000Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

function cancel() {
	document.forms[0].SCREEN.value = 100;
	document.forms[0].submit();
}

function cambiarcheck(check, indeti, txteti) 
{
   if(check.checked==0)
   {
	checkv = document.getElementById(indeti);
	checkv.value = "";  
	
	txtetiqueta  = document.getElementById(txteti);
	txtetiqueta.value = ""; 
	txtetiqueta.readOnly = true;
   } 
	else 
   {
	checkv = document.getElementById(indeti);
	checkv.value = "Y";  

	txtetiqueta  = document.getElementById(txteti);
	txtetiqueta.readOnly = false;
   } 
}  



function blockcodmenacep(tipocod) 
{
	inpcma=document.getElementById("CODMENACE");
	if(tipocod=="A")
	{
		inpcma.value="0";  
		inpcma.readOnly = true;
	}
	else
	{
		inpcma.value="0";  
		inpcma.readOnly = false;
	}
}


function cambiarDisplay(formato, indcarga) 
{
var ind = 0;

	if(indcarga==0)
	{ 	
		for (var x=1;x<12;x++) 
		{ 	
			id="cm" + x; 
			fila = document.getElementById(id);
			fila.style.display = "";  
	
			id="chk" + x; 
			check = document.getElementById(id);
			check.checked=0;  

			if(x<10)
				id="ETG00FLD0" + x;
			else
				id="ETG00FLD" + x;
				 
			checkv = document.getElementById(id);
			checkv.checked=0;  
			checkv.value="";  
	
			id="txt" + x; 
			texto = document.getElementById(id);
			texto.value="";  
			texto.readOnly = true;
		}
	}


	if(formato=="1" || formato=="0"  )	
	{
		ind=6;

		label = document.getElementById("lbl1");
		label.innerHTML="CAMPO 1 - X(40)";  

		label = document.getElementById("lbl2");
		label.innerHTML="CAMPO 2 - S(07)(02)";  

		label = document.getElementById("lbl3");
		label.innerHTML="CAMPO 3 - S(05)(02)";  

		label = document.getElementById("lbl4");
		label.innerHTML="CAMPO 4 - X(05)";  

		label = document.getElementById("lbl5");
		label.innerHTML="CAMPO 5 - X(15)";  

		label = document.getElementById("lbl6");
		label.innerHTML="CAMPO 6 - S(04)";  
	}

	if(formato=="2")	
	{
		ind=11;

		label = document.getElementById("lbl1");
		label.innerHTML="CAMPO 1 - X(24)";  

		label = document.getElementById("lbl2");
		label.innerHTML="CAMPO 2 - X(05)";  

		label = document.getElementById("lbl3");
		label.innerHTML="CAMPO 3 - X(05)";  

		label = document.getElementById("lbl4");
		label.innerHTML="CAMPO 4 - X(05)";  

		label = document.getElementById("lbl5");
		label.innerHTML="CAMPO 5 - X(05)";  

		label = document.getElementById("lbl6");
		label.innerHTML="CAMPO 6 - S(05)";  

		label = document.getElementById("lbl7");
		label.innerHTML="CAMPO 7 - S(05)";  

		label = document.getElementById("lbl8");
		label.innerHTML="CAMPO 8 - X(03)";  

		label = document.getElementById("lbl9");
		label.innerHTML="CAMPO 9 - X(03)";  

		label = document.getElementById("lbl10");
		label.innerHTML="CAMPO 10 - S(15)(02)";  

		label = document.getElementById("lbl11");
		label.innerHTML="CAMPO 11 - S(03)";  
	}

	if(formato=="3")	
	{
		ind=1;

		label = document.getElementById("lbl1");
		label.innerHTML="CAMPO 1 - X(80)";  
	}

	if(formato=="4")	
	{
		ind=6;

		label = document.getElementById("lbl1");
		label.innerHTML="CAMPO 1 - X(28)";  

		label = document.getElementById("lbl2");
		label.innerHTML="CAMPO 2 - S(09)(02)";  

		label = document.getElementById("lbl3");
		label.innerHTML="CAMPO 3 - S(09)(02)";  

		label = document.getElementById("lbl4");
		label.innerHTML="CAMPO 4 - S(09)(02)";  

		label = document.getElementById("lbl5");
		label.innerHTML="CAMPO 5 - S(15)(02)";  

		label = document.getElementById("lbl6");
		label.innerHTML="CAMPO 6 - S(02)";  

	}

	if(formato=="5")
	{	
		ind=9;

		label = document.getElementById("lbl1");
		label.innerHTML="CAMPO 1 - X(28)";  

		label = document.getElementById("lbl2");
		label.innerHTML="CAMPO 2 - X(12)";  

		label = document.getElementById("lbl3");
		label.innerHTML="CAMPO 3 - X(02)";  

		label = document.getElementById("lbl4");
		label.innerHTML="CAMPO 4 - X(04)";  

		label = document.getElementById("lbl5");
		label.innerHTML="CAMPO 5 - X(04)";  

		label = document.getElementById("lbl6");
		label.innerHTML="CAMPO 6 - S(05)";  

		label = document.getElementById("lbl7");
		label.innerHTML="CAMPO 7 - S(05)";  

		label = document.getElementById("lbl8");
		label.innerHTML="CAMPO 8 - S(15)(02)";
		
		label = document.getElementById("lbl9");
		label.innerHTML="CAMPO 9 - S(03)";
		  
	}

	if(formato=="6")	
	{
		ind=8;
		
		label = document.getElementById("lbl1");
		label.innerHTML="CAMPO 1 - S(08)(02)";  

		label = document.getElementById("lbl2");
		label.innerHTML="CAMPO 2 - S(08)(02)";  

		label = document.getElementById("lbl3");
		label.innerHTML="CAMPO 3 - S(08)(02)";  

		label = document.getElementById("lbl4");
		label.innerHTML="CAMPO 4 - S(07)(02)";  

		label = document.getElementById("lbl5");
		label.innerHTML="CAMPO 5 - S(07)(02)";  

		label = document.getElementById("lbl6");
		label.innerHTML="CAMPO 6 - S(07)(02)";  

		label = document.getElementById("lbl7");
		label.innerHTML="CAMPO 7 - S(12)";  

		label = document.getElementById("lbl8");
		label.innerHTML="CAMPO 8 - A(11)";  

	}

if(formato=="7")	
	{
		ind=5;

		label = document.getElementById("lbl1");
		label.innerHTML="CAMPO 1 - X(32)";  

		label = document.getElementById("lbl2");
		label.innerHTML="CAMPO 2 - X(04)";  

		label = document.getElementById("lbl3");
		label.innerHTML="CAMPO 3 - S(12)";  

		label = document.getElementById("lbl4");
		label.innerHTML="CAMPO 4 - S(16)";  

		label = document.getElementById("lbl5");
		label.innerHTML="CAMPO 5 - S(16)";  
	}

	for (var x=ind+1;x<12;x++) 
	{ 	
		id="cm" + x; 
		fila = document.getElementById(id);
		fila.style.display = "none";   
	}
}   


function ValidaEtiquetas() 
{
	var a=0;
	var ind=0;
	var formato = document.getElementById("ETG00FMTO");
	
	if(formato.value==1);
		ind=7
	if(formato.value==2);
		ind=11
	if(formato.value==3);
		ind=1
	if(formato.value==4);
		ind=5
	if(formato.value==5);
		ind=8
	if(formato.value==6);
		ind=7
	if(formato.value==7);
		ind=4

	for (var x=1;x<ind-1;x++) 
	{ 	
		id="chk" + x; 
		check = document.getElementById(id);

		if(check.checked==true)
		{
			id="txt" + x; 
			texto = document.getElementById(id);
			if(texto.value=="")
			{
			 alert("Las etiquetas seleccionadas no pueden estar vac�as ");
			 a=1;
			 break;
			}
			
		}
	}

	if(a==1)
		return false;
	else	
		document.forms[0].submit();	
}



</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "NEW".equals(userPO.getPurpose()) ? "" : "readonly";
     
%>


<H3 align="center">Tabla de C&oacute;digos del Sistema <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cntin_table_details.jsp, ETG0000"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSETG0000" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <INPUT TYPE=HIDDEN NAME="E01CNOLAN" VALUE="<%= currUser.getE01LAN() %>">
  <INPUT TYPE=HIDDEN NAME="FromRecord" VALUE="<%= request.getParameter("FromRecord") %>">
  
  <h4>Par&aacute;metros de Definici&oacute;n de Tablas</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Tabla :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="ETG00ELE" size="9" maxlength="8" value="<%= refCodes.getETG00ELE().trim()%>" onkeypress="enterInteger()" <%=readonly%> style="text-align:right;">
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Nombre o T&iacute;tulo :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="ETG00DESCL" size="66" maxlength="58" value="<%= refCodes.getETG00DESCL().trim()%>" >
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Tipo C&oacute;digo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
	  			<select name="ETG00TCOD" onchange="blockcodmenacep(this.value);">   
                   <option value="N" <% if (refCodes.getETG00TCOD().equals("N")) out.print("selected"); %>>N&uacute;merico</option>                   
                  <option value="A" <% if (refCodes.getETG00TCOD().equals("A")) out.print("selected"); %>>AlfaN&uacute;merico</option>
	  			</select>  
              </div>
            </td>
            <td nowrap> 
              <div align="right">C&oacute;digo Menor Aceptado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" id="CODMENACE" name="ETG00NROM" size="3" maxlength="3" value="<%= refCodes.getETG00NROM().trim()%>" onkeypress="enterInteger()"
                <% if (refCodes.getETG00TCOD().equals("A")) out.print("readonly"); %> style="text-align:right;">
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Es Tabla Usuario :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
	  			<select name="ETG00TUSR">  
                  <option value="S" <% if (refCodes.getETG00TUSR().equals("S")) out.print("selected"); %>>S</option>                   
                  <option value="N" <% if (refCodes.getETG00TUSR().equals("N")) out.print("selected"); %>>N</option>
                </select>                
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">C&oacute;digo de Grupo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG00TGRP" size="4" maxlength="3" value="<%= refCodes.getETG00TGRP().trim()%>" readonly style="text-align:right;">
                <input type="text" name="ETG00TGRD" size="50" maxlength="3" value="<%= refCodes.getETG00TGRD().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNTIN('ETG00TGRP','ETG00TGRD','10')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
              </div>
            </td>
          </tr> 
          
        </table>
      </td>
    </tr>
  </table>

  <h4>Selecci&oacute;n de Formato</h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="trdark"> 
            <td width="15%" > 
              <div align="right">C&oacute;digo de Formato :</div>
            </td>
            <td width="10%" > 
	  			<select name="ETG00FMTO" id="ETG00FMTO" onchange="cambiarDisplay(this.value, 0);">  
                  <option value="1" <% if (refCodes.getETG00FMTO().equals("1")) out.print("selected"); %>>Formato 1</option>                   
                  <option value="2" <% if (refCodes.getETG00FMTO().equals("2")) out.print("selected"); %>>Formato 2</option>
                  <option value="3" <% if (refCodes.getETG00FMTO().equals("3")) out.print("selected"); %>>Formato 3</option>
                  <option value="4" <% if (refCodes.getETG00FMTO().equals("4")) out.print("selected"); %>>Formato 4</option>
                  <option value="5" <% if (refCodes.getETG00FMTO().equals("5")) out.print("selected"); %>>Formato 5</option>
                  <option value="6" <% if (refCodes.getETG00FMTO().equals("6")) out.print("selected"); %>>Formato 6</option>                   
                  <option value="7" <% if (refCodes.getETG00FMTO().equals("7")) out.print("selected"); %>>Formato 7</option>
                </select>
         	</td>
         	<td width="15%" > 
            	<div align="right">C&oacute;digo : </div>
            </td> 
            <td width="60%" > 
				<input type="text"  name="ETG00NOMCD" size="50" maxlength="45" value="<%= refCodes.getETG00NOMCD()%>" />
            </td>              
         	
           </tr>
          <tr id="trclear"> 
	         <td > 
             </td>
	         <td > 
            	<div align="center"><b>Seleccione</b></div>
             </td>
            <td > 
            	<div align="left"><b>Campos Disponibles</b></div>
            </td> 
            <td > 
            	<div align="left"><b>Etiqueta de Campo</b></div>
            </td>              
   		  </tr>

          <tr id="cm1"  > 
	         <td  > 
             </td>
           	<td  align="center"> 
        		<input type="checkbox" id="chk1" onclick="cambiarcheck(this, 'ETG00FLD01', 'txt1');" value="<%=refCodes.getETG00FLD01().trim()%>" <%if (refCodes.getETG00FLD01().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD01"  value="<%=refCodes.getETG00FLD01().trim() %>">
	        </td>  
	        <td  align="left" > 
          		<label id="lbl1">CAMPO 1</label> 	        
          	</td>  
            <td  align="left"> 
				<input type="text"  id="txt1" name="ETG00NOM01" size="81" maxlength="80" value="<%= refCodes.getETG00NOM01()%>" <%if (!refCodes.getETG00FLD01().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         

          <tr id="cm2"> 
	         <td > 
             </td>
           	<td  align="center"> 
        		<input type="checkbox" id="chk2" onclick="cambiarcheck(this, 'ETG00FLD02', 'txt2');" value="<%=refCodes.getETG00FLD02().trim()%>" <%if (refCodes.getETG00FLD02().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD02"  value="<%=refCodes.getETG00FLD02().trim() %>">
	        </td>  
	        <td  align="left"> 
          		<label id="lbl2">CAMPO 2</label> 	        
	        </td>  
            <td align="left"> 
				<input type="text"  id="txt2" name="ETG00NOM02" size="81" maxlength="80" value="<%= refCodes.getETG00NOM02()%>" <%if (!refCodes.getETG00FLD02().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         

          <tr id="cm3"> 
	         <td > 
             </td>
           	<td  align="center"> 
        		<input type="checkbox" id="chk3" onclick="cambiarcheck(this, 'ETG00FLD03', 'txt3');" value="<%=refCodes.getETG00FLD03().trim()%>" <%if (refCodes.getETG00FLD03().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD03"  value="<%=refCodes.getETG00FLD03().trim() %>">
	        </td>  
	        <td  align="left"> 
          		<label id="lbl3">CAMPO 3</label> 	        
	        </td>  
            <td  align="left"> 
				<input type="text"  id="txt3" name="ETG00NOM03" size="81" maxlength="80" value="<%= refCodes.getETG00NOM03()%>" <%if (!refCodes.getETG00FLD03().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         

          <tr id="cm4"> 
	         <td  > 
             </td>
           	<td  align="center"> 
        		<input type="checkbox" id="chk4" onclick="cambiarcheck(this, 'ETG00FLD04', 'txt4');" value="<%=refCodes.getETG00FLD04().trim()%>" <%if (refCodes.getETG00FLD04().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD04"  value="<%=refCodes.getETG00FLD04().trim() %>">
	        </td>  
	        <td  align="left"> 
          		<label id="lbl4">CAMPO 4</label> 	        
	        </td>  
            <td align="left"> 
				<input type="text" id="txt4" name="ETG00NOM04" size="81" maxlength="80" value="<%= refCodes.getETG00NOM04()%>" <%if (!refCodes.getETG00FLD04().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         

          <tr id="cm5"> 
	         <td  > 
             </td>
           	<td align="center"> 
        		<input type="checkbox" id="chk5" onclick="cambiarcheck(this, 'ETG00FLD05', 'txt5');" <%if (refCodes.getETG00FLD05().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD05"  value="<%=refCodes.getETG00FLD05().trim() %>">
 	        </td>  
	        <td align="left"> 
          		<label id="lbl5">CAMPO 5</label> 	        
	        </td>  
            <td  align="left"> 
				<input type="text"  id="txt5" name="ETG00NOM05" size="81" maxlength="80" value="<%= refCodes.getETG00NOM05()%>" <%if (!refCodes.getETG00FLD05().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         

          <tr id="cm6"> 
	         <td  > 
             </td>
           	<td  align="center"> 
        		<input type="checkbox" id="chk6" onclick="cambiarcheck(this, 'ETG00FLD06', 'txt6');" value="<%=refCodes.getETG00FLD06().trim()%>" <%if (refCodes.getETG00FLD06().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD06"  value="<%=refCodes.getETG00FLD06().trim() %>">
	        </td>  
	        <td align="left"> 
          		<label id="lbl6">CAMPO 6</label> 	        
	        </td>  
            <td  align="left"> 
				<input type="text"  id="txt6" name="ETG00NOM06" size="81" maxlength="80" value="<%= refCodes.getETG00NOM06()%>" <%if (!refCodes.getETG00FLD06().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         

          <tr id="cm7"> 
	         <td  > 
             </td>
           	<td  align="center"> 
        		<input type="checkbox" id="chk7" onclick="cambiarcheck(this, 'ETG00FLD07', 'txt7');"  value="<%=refCodes.getETG00FLD07().trim()%>" <%if (refCodes.getETG00FLD07().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD07"  value="<%=refCodes.getETG00FLD07().trim() %>">
	        </td>  
	        <td  align="left"> 
          		<label id="lbl7">CAMPO 7</label> 	        
	        </td>  
            <td align="left"> 
				<input type="text"  id="txt7" name="ETG00NOM07" size="81" maxlength="80" value="<%= refCodes.getETG00NOM07()%>" <%if (!refCodes.getETG00FLD07().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         

          <tr id="cm8"> 
	         <td > 
             </td>
           	<td  align="center"> 
        		<input type="checkbox" id="chk8"  onclick="cambiarcheck(this, 'ETG00FLD08', 'txt8');" value="<%=refCodes.getETG00FLD08().trim()%>" <%if (refCodes.getETG00FLD08().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD08"  value="<%=refCodes.getETG00FLD08().trim() %>">
	        </td>  
	        <td  align="left"> 
          		<label id="lbl8">CAMPO 8</label> 	        
	        </td>  
            <td align="left"> 
				<input type="text"  id="txt8" name="ETG00NOM08" size="81" maxlength="80" value="<%= refCodes.getETG00NOM08()%>" <%if (!refCodes.getETG00FLD08().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         

          <tr id="cm9"> 
	         <td > 
             </td>
           	<td align="center"> 
        		<input type="checkbox" id="chk9" onclick="cambiarcheck(this, 'ETG00FLD09', 'txt9');" value="<%=refCodes.getETG00FLD09().trim()%>" <%if (refCodes.getETG00FLD09().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD09"  value="<%=refCodes.getETG00FLD09().trim() %>">
	        </td>  
	        <td  align="left"> 
          		<label id="lbl9">CAMPO 9</label> 	        
	        </td>  
            <td  align="left"> 
				<input type="text" id="txt9" name="ETG00NOM09" size="81" maxlength="80" value="<%= refCodes.getETG00NOM09()%>" <%if (!refCodes.getETG00FLD09().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         

          <tr id="cm10"> 
	         <td  > 
             </td>
           	<td  align="center"> 
        		<input type="checkbox" id="chk10" onclick="cambiarcheck(this, 'ETG00FLD10', 'txt10');"  value="<%=refCodes.getETG00FLD10().trim()%>" <%if (refCodes.getETG00FLD10().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD10"  value="<%=refCodes.getETG00FLD10().trim() %>">
	        </td>  
	        <td  align="left"> 
          		<label id="lbl10">CAMPO 10</label> 	        
	        </td>  
            <td align="left"> 
				<input type="text"  id="txt10" name="ETG00NOM10" size="81" maxlength="80" value="<%= refCodes.getETG00NOM10()%>" <%if (!refCodes.getETG00FLD10().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         

          <tr id="cm11"> 
	         <td  > 
             </td>
           	<td  align="center"> 
        		<input type="checkbox" id="chk11" onclick="cambiarcheck(this, 'ETG00FLD11', 'txt11');"  value="<%=refCodes.getETG00FLD11().trim()%>" <%if (refCodes.getETG00FLD11().equals("Y")){out.print(" checked");}; %>/>
        		<input type="hidden"  name="ETG00FLD11"  value="<%=refCodes.getETG00FLD11().trim() %>">
	        </td>  
	        <td  align="left"> 
          		<label id="lbl11">CAMPO 11</label> 	        
	        </td>  
            <td align="left"> 
				<input type="text"  id="txt11" name="ETG00NOM11" size="81" maxlength="80" value="<%= refCodes.getETG00NOM11()%>" <%if (!refCodes.getETG00FLD11().equals("Y")){out.print("readonly");}; %>/>
            </td>
          </tr>         


        </table>
      </td>
    </tr>
  </table>


  <div align="center">
    <input id="EIBSBTN" type="button" name="Enviar" value="Enviar" onclick="ValidaEtiquetas()">
    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel()">
  </div>
  </form>
 <SCRIPT Language="Javascript">
            cambiarDisplay(<%= refCodes.getETG00FMTO().trim() %>, 1);
     </SCRIPT> 
  
</body>
</html>
