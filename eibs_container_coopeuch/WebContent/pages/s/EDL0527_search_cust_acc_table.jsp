<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<link rel="stylesheet" href="../../theme/Master.css" type="text/css">
<title>Consulta Calificacion de Provision</title>

<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EDL0527Record"
	class="datapro.eibs.beans.EDL052701Message" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"
	scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage"
	scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"
	scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="javascript">

function goNew() 
{
	document.getElementById("SCREEN").value="2";
	document.forms[0].submit();
}

</SCRIPT>

<%
		if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>

</head>
<body >
<h3 align="center">Consulta Calificacion de Provision<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="search_cust_acc_table.jsp ,EDL0527"></h3>
<hr size="4">
<form method="post"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEDL0527">
<INPUT TYPE=HIDDEN NAME="SCREEN">
<table class="tbenter" width=100% align=center>
	<tr>
		<td class=TDBKG width="30%">
		<div align="center"><a href="javascript:goNew()"><b>Volver
		a la lista de prestamos</b></a></div>
		</td>
		<td class=TDBKG width="30%">
		<div align="center"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
		</td>
	</tr>
</table>
<br>
<table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			class="tbhead">
			<%
			if (!EDL0527Record.getE01CPVCUN().equals("0")) {
			%>
			<tr id="trdark">
				<td nowrap width="10%"><b>Cliente :</b></td>
				<td nowrap width="10%"><eibsinput:text name="EDL0527Record"
					property="E01CPVCUN"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/></td>
				<%
				}
				%>
				<td nowrap width="20%"></td>
				<td nowrap width="10%"><b>Numero de Prestamo :</b></td>
				<td nowrap width="10%"><eibsinput:text name="EDL0527Record"
					property="E01CPVACC"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/></td>
				<td nowrap width="20%"></td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trdark">
				<td nowrap width="5%"><b>Fecha en que se Abrio  :
				</b></td>
				<td nowrap width="5%"><b>Mes :</b></td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVODM"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_MONTH %>" readonly="true"/></td>
				<td nowrap width="5%"><b> Dia :</b></td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVODD"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DAYS %>" readonly="true"/></td>
				<td nowrap width="5%"><b> A�o :</b></td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVODY"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_YEAR %>" readonly="true"/></td>
				<td nowrap width="30%"></td>
				<td nowrap width="5%" aling="right"><b>Fecha : </b></td>
				<td nowrap width="5%" align="left"><b>A�o :</b></td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVDTY" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_YEAR %>" readonly="true"/></td>
				<td nowrap width="5%" aling="right"><b>Mes :</b></td>
				<td nowrap width="5%" aling="left"><eibsinput:text
					property="E01CPVDTM" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_MONTH %>" readonly="true"/></td>
				<td nowrap width="5%"></td>
			</tr>
		</table>
	</tr>
</table>
<H4>Datos B&aacute;sicos</H4>
<table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing=0 cellpadding=2 width="100%" border="0">
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Valor del Prestamo :</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVPRI" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Valor Intereses :</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVMEI" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
			<td nowrap width="5%">
				<div align="right">Valor otros cargos:</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVOTC" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Valor Exposicion:</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVTEX" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Dias de mora:</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVMOR" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DAYS %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Valor nueva exposicion:</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVNEA" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">Interes por Riesgo:</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVIRS" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="%">
				<div align="right">Cargos por Riesgo:</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVCFR" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Tipo de Producto:</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVTYP" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Codigo del Producto:</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVPRO" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
			<td nowrap width="5%">
				<div align="right">Linea de Credito:</div>
				</td>
				<td nowrap width="5%" align="left"><eibsinput:text
					property="E01CPVCMN" name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%"></td>
			</tr>
		</table>
</table>
<p>&nbsp;</p>
<table class=tbenter>
	<tr>
		<td nowrap>
		<h4>Informaci&oacute;n B&aacute;sica de Garantias</h4>
		</td>
	</tr>
</table>
<table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Calificacion de Riesgo:</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVQRS"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Garantia s/n :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVGAR"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">Clase de Garantia :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCGR"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">% de Garantia :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPGR"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">% Minimo de Provisi&oacute;n :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPMP"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Garantia Neta de Provisi&oacute;n :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPNG"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">Valor minimo Provision Capital:</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPPM"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Provisi&oacute;n Capital:</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPVP"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Interes de Provisi&oacute;n :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPVI"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Provision Otros :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPVO"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">Cantidad de la Garantia :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVAGR"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Tipo de Garantia :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVTGR"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Subtipo de Garantia :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVSTG"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%"></td>
				<td nowrap width="5%"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>
<table class=tbenter>
	<tr>
		<td nowrap>
		<h4>Informaci&oacute;n B&aacute;sica del Segmento</h4>
		</td>
	</tr>
</table>
<table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trdark">
				<td nowrap width="5%">
					<div align="right">Segmento :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVSEG"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
					<div align="right">Incumplimiento 0/1 :</div>
				</td>
				<td nowrap width="5%">
					<eibsinput:text property="E01CPVINC"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
				</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
					<div align="right">Maxima altura de mora 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVMAM"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Mora actual 31-60. 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVAMB"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Mora actual 61-90. 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVAMC"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Maxima mora 31-60. 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVMMB"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">Maxima mora 61-90. 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVMMC"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Maxima mora >90. 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVMMD"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Prestamos activos 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCRB"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Garantia idonea 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCGI"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">Numero de trimestres :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVNTE"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Primer  trimestre 01 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVT01"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Segundo  trimestre :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVT02"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Tercer  trimestre :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVT03"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">Sumario de  trimestres :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVTSU"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Comportamiento Regular Anual :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCAR"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Comportamiento Anual malo :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCAM"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Prenda 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPRE"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<p>&nbsp;</p>
<table class=tbenter>
	<tr>
		<td nowrap>
		<h4>Informaci&oacute;n de Provisiones</h4>
		</td>
	</tr>
</table>
<table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Garantia   Hipotecaria 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVHIP"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Aumento mayor 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPRT"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">Porcentaje incumplimiento 0/1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVTIP"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">% Z :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVVLZ"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Puntaje :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPUN"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Categoria Objetiva :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCRE"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id = trclear>
				<td nowrap width="5%">
				<div align="right">Categoria Subjetiva :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCRE"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Categoria Final de Riesgo :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCRE"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Categoria de Riesgo Homologada :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCRH"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Perdidas Esperadas :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPES"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">% Probabilidad Incumplimiento Matriz A :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPIA"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">% Probabilidad Incumplimiento Matriz B :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPIB"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">% Perdida Incumplimiento :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPDI"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">% Perdida Incumplimiento Ponderada :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPDP"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">CCI  Capital mes anterior :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCCA"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">CCI Intereses mes anterior :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCIA"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">CCI Otros cargos mes anterior :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCOA"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Valor Exposicion mes anterior :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVVEA"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
			<td nowrap width="5%">
				<div align="right">% EXP. T/T-1 :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPED"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Provisi&oacute;n PCI Capital :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPIC"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Provisi&oacute;n CCI Capital :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCIC"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Provisi&oacute;n PCI Interes :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPII"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">Provisi&oacute;n CCI Interes :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCII"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Provisi&oacute;n PCI Otros cargos :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVPIO"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="5%">
				<div align="right">Provisi&oacute;n CCI Otros cargos :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVCIO"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap width="5%">
				<div align="right">Metodologia de calculo :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVMCA"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="5%">
				<div align="right">Total Provisi&oacute;n :</div>
				</td>
				<td nowrap width="5%"><eibsinput:text property="E01CPVTPV"
					name="EDL0527Record"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/></td>
				<td nowrap></td>
				<td nowrap></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
