package datapro.eibs.tefmulti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adobe.fdf.FDFDoc;

import datapro.eibs.beans.EFRM00001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100500Message;
import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV109001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ETE020001Message;
import datapro.eibs.beans.ETE020002Message;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.reports.JSEFRM000PDF;
import datapro.eibs.sockets.MessageRecord;

public class JSETE0200A extends JSEFRM000PDF {

	protected static final int R_EVALUATION_LIST = 1000;	
	protected static final int R_FORMALIZATION_LIST = 2000;
	protected static final int A_LIST = 3000;
	protected static final int R_FORM_SIGNING_CHANNELS = 100;	
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_FORM_SIGNING_CHANNELS:
				procReqFormSigningChannels(user, req, res, session);
				break;
			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}

	protected void procReqFormSigningChannels(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException 
		{
		
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try 
		{

			mp = getMessageProcessor("ETE0200", req);

			ETE020002Message msg = (ETE020002Message) mp.getMessageRecord("ETE020002", user.getH01USR(), "0001");

			setMessageRecord(req, msg);
			
			mp.sendMessage(msg);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();

			msg = (ETE020002Message) mp.receiveMessageRecord();

			if (!mp.hasError(error)) 
			{
				FDFDoc outputFDF = new FDFDoc();
				if (session.getAttribute("pdfData") != null) {
					session.removeAttribute("pdfData");
				}
				if (dstXML == null || dstXML.isEmpty()) {
					initDataStructure();			
					flexLog("XML file was reloaded.");
				}

				getFormData(outputFDF, (String)formatNames.get("ETE020002") + ".", msg);

				int index = 0;
				String prefix = "";
				String ddsName = msg.getFormatName();
				prefix = getPrefix(msg);
				buildFormList(outputFDF, msg, "." + prefix, index++);

				String urlPDF = datapro.eibs.master.JSEIBSProp.getFORMPDFURL() + req.getParameter("pdfName");
				sendPdf(
						req, 
						res, 
						urlPDF, 
						outputFDF, 
						req.getParameter("action"),
						req.getParameter("copies"),
						""
						);
			}
			else
			{
				String MsgErr = error.getERNU01() + " - " + error.getERDS01();
				session.setAttribute("error_msg", MsgErr);
				forward("EFRM000_pdf_forms_error.jsp", req, res);
			}	
		} 
		catch (Exception e) {
			session.setAttribute("error_msg", "");
			forward("EFRM000_pdf_forms_error.jsp", req, res);
		}
		finally {
			if (mp != null) mp.close();
		}
	}
//
}
