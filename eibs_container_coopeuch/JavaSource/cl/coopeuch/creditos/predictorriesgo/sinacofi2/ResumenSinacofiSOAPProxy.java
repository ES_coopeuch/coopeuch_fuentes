package cl.coopeuch.creditos.predictorriesgo.sinacofi2;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;

public class ResumenSinacofiSOAPProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofi _service = null;
        private cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiPortType _proxy = null;
        private Dispatch<Source> _dispatch = null;

        public Descriptor() {
            _service = new  cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofi();
            initCommon();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new  cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofi(wsdlLocation, serviceName);
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getResumenSinacofiSOAP();
        }

        public  cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiPortType getProxy() {
            return _proxy;
        }

        public Dispatch<Source> getDispatch() {
            if(_dispatch == null ) {
                QName portQName = new QName("http://resumensinacofi.core.coopeuch.cl/resumensinacofi", "ResumenSinacofiSOAP");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if(!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if(_dispatch != null ) {
            bp = (BindingProvider) _dispatch;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }
    }

    public ResumenSinacofiSOAPProxy() {
        _descriptor = new Descriptor();
    }

    public ResumenSinacofiSOAPProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public ResumenAntecedentesComercialesResponse resumenAntecedentesComerciales(DatosConsultaSum parameters) {
        return _getDescriptor().getProxy().resumenAntecedentesComerciales(parameters);
    }

}