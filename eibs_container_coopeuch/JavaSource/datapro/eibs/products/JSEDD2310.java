package datapro.eibs.products;

/*******************************************************************************************************************************/
/**  Creado por              :  Patricia Cataldo L.                 DATAPRO                                                    **/
/**  Identificacion          :  PCL01                                                                                          **/
/**  Fecha                   :  14/01/2013                                                                                     **/
/**  Objetivo                :  Mantenedor de Tabla de tramos de Postulacion Subsidio, para Ahorro Vivienda                    **/
/**                                                                                                                            **/
/*********************************************************************************************************************************/

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adobe.fdf.FDFDoc;

import datapro.eibs.beans.ECC014001Message;
import datapro.eibs.beans.EDD000001Message;
import datapro.eibs.beans.EDD001001Message;
import datapro.eibs.beans.EDD230001Message;
import datapro.eibs.beans.EDD231001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EDD200001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.EWD0008DSMessage;
import datapro.eibs.beans.EWD0014DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

public class JSEDD2310 extends JSEDD2300 { 

	/**
	 * 
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int R_ENTER_TARIF = 100;
	protected static final int A_ENTER_TARIF = 200;
	protected static final int A_MAINT_TARIF = 300;

	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		
	    switch (screen) {
	    
	    case R_ENTER_TARIF:
	    	procReqEnterTarifado(user, req, res, session);
			break;
	    
	    case A_ENTER_TARIF:
	    	procActEnterTarifado(user, req, res, session);
			break;
	
	    case A_MAINT_TARIF:
	    	procMaintTarifado(user, req, res, session);
			break;
			
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	

	protected void procReqEnterTarifado(
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: EDD2310_enter_tarifado.jsp");
			forward("EDD2310_enter_tarifado.jsp", req, res);
			} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}
	


	protected void procMaintTarifado(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException 
		{

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		MessageProcessor mp = null;
		try  
		{
 
			mp = getMessageProcessor("EDD2310", req);

			EDD231001Message msg = (EDD231001Message) mp.getMessageRecord("EDD231001", user.getH01USR(), "0003");

			setMessageRecord(req, msg);

			mp.sendMessage(msg);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();

			msg = (EDD231001Message) mp.receiveMessageRecord();

			if (mp.hasError(error)) 
			{
				userPO.setHeader3(msg.getE01ACMNA1());
				userPO.setCurrency(msg.getE01ACMCCY());
				userPO.setHeader1(msg.getE01ACMPRO());
				
				session.setAttribute("rtBasic", msg);
				session.setAttribute("error", error);
				forward("EDD2310_tarifa_maintenance.jsp", req, res);
			}
			else
			{
				session.setAttribute("error", error);
				session.setAttribute("userPO", userPO);
				forward("EDD2310_forms_req_aprobacion.jsp", req, res);
			}	
		} 
		finally {
			if (mp != null) mp.close();
		}
	}
	
	


	protected void procActEnterTarifado(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException 
		{

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		MessageProcessor mp = null;
		try  
		{
 
			mp = getMessageProcessor("EDD2310", req);

			EDD231001Message msg = (EDD231001Message) mp.getMessageRecord("EDD231001", user.getH01USR(), "0001");

			try {
				msg.setE01ACMACC(req.getParameter("E01ACMACC"));
				userPO.setIdentifier(req.getParameter("E01ACMACC"));
			} catch (Exception e) {
				msg.setE01ACMACC("0");
			}

			mp.sendMessage(msg);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();

			msg = (EDD231001Message) mp.receiveMessageRecord();

			if (!mp.hasError(error)) 
			{
				userPO.setHeader3(msg.getE01ACMNA1());
				userPO.setCurrency(msg.getE01ACMCCY());
				userPO.setHeader1(msg.getE01ACMPRO());
				
				session.setAttribute("rtBasic", msg);
				session.setAttribute("error", error);
				forward("EDD2310_tarifa_maintenance.jsp", req, res);
			}
			else
			{
				session.setAttribute("error", error);
				forward("EDD2310_enter_tarifado.jsp", req, res);
			}	
		} 
		finally {
			if (mp != null) mp.close();
		}
	}
	
}
