<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@page import="com.datapro.constants.EibsFields"%>
<%@ page import = "datapro.eibs.master.Util" %>

<html>
<head> 
<title>Consulta de Prestamos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="inqLoans" class="datapro.eibs.beans.EDL016001Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

<%
if ( userPO.getHeader23().equals("G") ||  userPO.getHeader23().equals("V")){
%>
	builtNewMenu(ln_i_1_opt);
<%   
}
else  {
%>
	builtNewMenu(ln_i_2_opt);
<%   
}
%>

</SCRIPT> 


</head>

<body nowrap>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }
    out.println("<SCRIPT> initMenu(); </SCRIPT>");
 
%>  
<div align="center"></div>
<h3 align="center"> Consulta de Préstamos - Saldos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ln_balances,EDL0160"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXEDL0160" >
  <input type=HIDDEN name="SCREEN" value="1">
  <% if (!inqLoans.getE01PENDAP().trim().equals("")) { %> 
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
  	<tr>
  		<td align="right" valign="top" width="85%" style="color:red;font-size:12;"><b><%=inqLoans.getE01PENDAP()%></b></td>
  		<td width="5%"><h4>&nbsp;</h4></td>
  	</tr>
  </table>
  <% } %>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="10%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="5%" > 
              <div align="left"> 
                <input type="text" name="E02CUN2" size="10" maxlength="9" readonly value="<%= userPO.getHeader2().trim()%>">
              </div>
            </td>
            <td nowrap width="10%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap width="35%"> 
              <div align="left"> 
                <input type="text" name="E02NA12" size="48" maxlength="45" readonly value="<%= userPO.getHeader3().trim()%>">
              </div>
            </td>
            <td nowrap width="10%"> 
              <div align="right" ><b>Producto :</b></div>
            </td>
            <td nowrap width="15%"><b> 
              <input type="text" name="E02PRO2" size="4" maxlength="4" readonly value="<%= userPO.getHeader1().trim()%>">
              </b></td><!--
              Estado del rut modificado debido a EIFRS deterioro parte 2
             -->
             <% if(userPO.getHeader4().equalsIgnoreCase("S")){ %>
            <td nowrap width="5%"> 
              <div align="right"><b>Estado RUT: </b></div>
            </td>
            <td nowrap width="10%"><b> 
              <div align="left"><%=userPO.getHeader5()%>
              </b>
            </td>
            <% } %>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="10%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="5%"> 
              <div align="left"> 
                <input type="text" name="E02ACC" size="13" maxlength="12" value="<%= userPO.getIdentifier().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="10%"> 
              <div align="right"><b>Oficial :</b></div>
            </td>
            <td nowrap width="35%"> 
              <div align="left"><b> 
                <input type="text" name="E02NA122" size="48" maxlength="45" readonly value="<%= userPO.getOfficer().trim()%>">
                </b> </div>
            </td>
            <td nowrap width="10%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"><b> 
         	  <eibsinput:text name="inqLoans" property="E01DEACCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" readonly="true"/>    
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table class=tbenter>
   <tr > 
      <td nowrap> 
      <% 
 		if (inqLoans.getH01FLGWK3().equals("R")) {
 	 %> 
   		<h4>Sumario Moneda Reajustable </h4>
   	 <% 
 		} else {
 	 %>
 	    <h4>Sumario</h4>
 	 <% 
 		}
 	 %>   
      </td>
      <td nowrap align=left> 
   		<b>CREDITO :<font color="#ff6600"> <% 	if (inqLoans.getE01COLATR().trim().equals("") )
   													out.print("SIN GARANTIA");
   												else
   													out.print(inqLoans.getE01COLATR());
   											%></font></b>
      </td>
      <td nowrap align=right> 
   		<b>ESTADO :</b>
      </td>
      <td nowrap> 
   		<b><font color="#ff6600"><%= inqLoans.getE01STATUS().trim()%></font></b>
      </td>
    </tr>
  </table>
  <table class="tableinfo">
    <tr > 
      <td nowrap>  
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Monto Original :</div>
            </td>
            <td nowrap >
         	  <eibsinput:text name="inqLoans" property="E01DEAOAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVOAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>   
 		    </td> 
            <td nowrap > 
              <div align="right">Fecha de Apertura :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEAOD1" size="3" maxlength="2" value="<%= inqLoans.getE01DEAOD1().trim()%>" readonly>
              <input type="text" name="E01DEAOD2" size="3" maxlength="2" value="<%= inqLoans.getE01DEAOD2().trim()%>" readonly>
              <input type="text" name="E01DEAOD3" size="5" maxlength="4" value="<%= inqLoans.getE01DEAOD3().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Saldo Principal :</div>
            </td>
            <td nowrap >               
              <eibsinput:text name="inqLoans" property="E01DEAMEP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVMEP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>               
            </td>
            <td nowrap > 
              <div align="right">Fecha de Vencimiento :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEAMD1" size="3" maxlength="2" value="<%= inqLoans.getE01DEAMD1().trim()%>" readonly>
              <input type="text" name="E01DEAMD2" size="3" maxlength="2" value="<%= inqLoans.getE01DEAMD2().trim()%>" readonly>
              <input type="text" name="E01DEAMD3" size="5" maxlength="4" value="<%= inqLoans.getE01DEAMD3().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Reajuste :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="inqLoans" property="E01DEAREA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap > 
              <div align="right">T&eacute;rmino :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEATRM" size="5" maxlength="5" value="<%= inqLoans.getE01DEATRM().trim()%>">
              <input type="text" name="E01DEATRC" size="10" 
				  value="<% if (inqLoans.getE01DEATRC().equals("D")) out.print("D&iacute;a(s)");
							else if (inqLoans.getE01DEATRC().equals("M")) out.print("Mes(es)");
							else if (inqLoans.getE01DEATRC().equals("Y")) out.print("A&ntilde;o(s)");
							else out.print("");%>" 
				readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Saldo Intereses :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="inqLoans" property="E01DEAMEI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVMEI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>              
            </td>
            <td nowrap > 
              <div align="right">Tasa Inter&eacute;s :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="inqLoans" property="E01NOWRTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>      
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Interes por Mora :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="inqLoans" property="E01DEAMEM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVMEM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>              
            </td>
            <td nowrap > 
              <div align="right">Per&iacute;odo Base :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEABAS" size="4" maxlength="3" value="<%= inqLoans.getE01DEABAS().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear">
            <td nowrap >
              <div align="right">Deducciones :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="inqLoans" property="E01TOTDED" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVDED" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>              
            </td>
            <td nowrap >
              <div align="right">Ciclo / Fecha Principal :</div>
            </td>
            <td nowrap >
              <input type="text" name="E01DEAPPD" size="3" maxlength="3" value="<%= inqLoans.getE01DEAPPD().trim()%>" readonly>
              / 
              <input type="text" name="E01DEAHE1" size="3" maxlength="2" value="<%= inqLoans.getE01DEAHE1().trim()%>" readonly>
              <input type="text" name="E01DEAHE2" size="3" maxlength="2" value="<%= inqLoans.getE01DEAHE2().trim()%>" readonly>
              <input type="text" name="E01DEAHE3" size="5" maxlength="4" value="<%= inqLoans.getE01DEAHE3().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap >
              <div align="right">Comisiones :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="inqLoans" property="E01TOTCOM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVCOM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>
             </td>
            <td nowrap >
              <div align="right">Ciclo / Fecha Intereses :</div>
            </td>
            <td nowrap >
              <input type="text" name="E01DEAIPD" size="3" maxlength="3" value="<%= inqLoans.getE01DEAIPD().trim()%>" readonly>
              / 
              <input type="text" name="E01DEARD1" size="3" maxlength="2" value="<%= inqLoans.getE01DEARD1().trim()%>" readonly>
              <input type="text" name="E01DEARD2" size="3" maxlength="2" value="<%= inqLoans.getE01DEARD2().trim()%>" readonly>
              <input type="text" name="E01DEARD3" size="5" maxlength="4" value="<%= inqLoans.getE01DEARD3().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap >
              <div align="right">Impuestos :</div>
            </td>
            <td nowrap >
           	  <eibsinput:text name="inqLoans" property="E01TOTIMP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVIMP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>
			</td>
            <td nowrap >
              <div align="right">Ciclo / Fecha Cambio Tasa :</div>
            </td>
            <td nowrap >
              <input type="text" name="E01DEARRP" size="3" maxlength="3" value="<%= inqLoans.getE01DEARRP().trim()%>" readonly>
              / 
              <input type="text" name="E01DEARR1" size="3" maxlength="2" value="<%= inqLoans.getE01DEARR1().trim()%>" readonly>
              <input type="text" name="E01DEARR2" size="3" maxlength="2" value="<%= inqLoans.getE01DEARR2().trim()%>" readonly>
              <input type="text" name="E01DEARR3" size="5" maxlength="4" value="<%= inqLoans.getE01DEARR3().trim()%>" readonly>
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap >
              <div align="right">F.E.C.I :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="inqLoans" property="E01DLCAV3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVAV3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>
           </td>
            <td nowrap >
              <div align="right">F.E.C.I Vencido :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="inqLoans" property="E01DLCAV4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVAV4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>
            </td>
          </tr>

          <tr id="trclear">  
            <td nowrap >
              <div align="right">IVA :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="inqLoans" property="E01TOTIVA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVIVA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>
            </td>
            <td nowrap >
              <div align="right">Cuota Financiera :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="inqLoans" property="E01DEAROA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>      
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap >
              <div align="right">Saldo Total :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="inqLoans" property="E01MEMBAL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>                                
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01REVBAL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="13" maxlength="13"/>
 			 <% } %>
            </td>
            <td nowrap >
              <div align="right">Otros Cargos :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="inqLoans" property="E01OTHCHG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>      
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Inter&eacute;s Calculado Hasta :</div>
            </td>
            <td nowrap >
              <input type="text" name="E01DEALC1" size="3" maxlength="2" value="<%= inqLoans.getE01DEALC1().trim()%>" readonly>
              <input type="text" name="E01DEALC2" size="3" maxlength="2" value="<%= inqLoans.getE01DEALC2().trim()%>" readonly>
              <input type="text" name="E01DEALC3" size="5" maxlength="4" value="<%= inqLoans.getE01DEALC3().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Cuota Total :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="inqLoans" property="E01TOTPYM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>      
            </td>
          </tr> 
        </table>
      </td>
    </tr> 
  </table>  

<%  if ((currUser.getE01INT().trim().equals("18"))) {%> 
  <h4>Distribucion de Valores</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
         <tr id="trdark"> 
            <td nowrap > 
              <div align="center"> </div>
            </td>
            <td nowrap > 
              <div align="center"><b> Principal</b></div>
            </td>
            <td nowrap > 
              <div align="center"><b>Reajuste</b></div>
            </td>
            <td nowrap > 
              <div align="center"><b>Interes</b></div>
            </td>
             <td nowrap > 
              <div align="center"><b>Interes Mora</b></div>
            </td>
           <td nowrap > 
              <div align="center"><b>Reajuste Mora</b></div>
            </td>
           <td nowrap > 
              <div align="center"><b>Cobranza</b></div>
            </td>
          </tr>
         <tr id="trclear"> 
            <td nowrap > 
              <div align="center"> Vigente</div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCPVI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCRVI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCIVI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
             <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCMVI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
           <td nowrap > 
               <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCXVI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
           </td>
           <td nowrap > 
               <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCCVI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
           </td>
          </tr>
         <tr id="trdark"> 
            <td nowrap > 
              <div align="center"> Vencido</div>
            </td>
           <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCPVE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCRVE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCIVE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
             <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCMVE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
           <td nowrap > 
               <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCXVE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
           </td>
           <td nowrap > 
               <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCCVE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
           </td>
          </tr>
         <tr id="trclear"> 
            <td nowrap > 
              <div align="center"> Suspendido</div>
            </td>
           <td nowrap > 
              <div align="center">
              </div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCRSU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCISU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
             <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCMSU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
           <td nowrap > 
               <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCXSU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
           </td>
           <td nowrap > 
               <div align="center">
              </div>
           </td>
          </tr>
         <tr id="trdark"> 
            <td nowrap > 
              <div align="center"> Castigado</div>
            </td>
           <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCPCA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCRCA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCICA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
             <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCMCA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
           <td nowrap > 
               <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCXCA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
           </td>
           <td nowrap > 
               <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCCCA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
           </td>
          </tr>
         <tr id="trdark"> 
            <td nowrap > 
              <div align="center"> Total</div>
            </td>
           <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCPTO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCRTO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
            <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCITO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
             <td nowrap > 
              <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCMTO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
            </td>
           <td nowrap > 
               <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCXTO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
           </td>
           <td nowrap > 
               <div align="center">
	         	  <eibsinput:text name="inqLoans" property="E01DLCCTO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
              </div>
           </td>
          </tr>

        </table>
      </td>
    </tr> 
  </table>  
<% } %> 


<jsp:include page="ESD0840_reevaluation_inquiry.jsp">
	<jsp:param name="flag" value="<%=inqLoans.getH01FLGWK3()%>" />
</jsp:include>


  <h4>Informaci&oacute;n Adicional</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right"> Suspension Devengo :</div>
            </td>
            <td nowrap >
              <input type="text" name="E01STATUS" size="2" maxlength="1" value="<%= inqLoans.getE01DLCSUS().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Principal Vencido :</div>
            </td>
            <td nowrap >
         	  <eibsinput:text name="inqLoans" property="E01DEAPDU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"> Calificaci&oacute;n :</div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" >
              <input type="text" name="E01CALIFI" size="2" maxlength="1" value="<%= inqLoans.getE01CALIFI().trim()%>" readonly>
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
              <div align="right">Inter&eacute;s Vencido :</div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
         	  <eibsinput:text name="inqLoans" property="E01DEAIDU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right"> D&iacute;as en Mora :</div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" >
              <input type="text" name="E01MORDYS" size="4" maxlength="4" value="<%= inqLoans.getE01MORDYS().trim()%>" readonly>
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
              <div align="right">Previsi&oacute;n Principal :</div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" >
         	  <eibsinput:text name="inqLoans" property="E01DLCPPR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap  > 
              <div align="right">Cuotas Pagadas :</div>
            </td>
            <td nowrap  bordercolor="#000000" >
              <input type="text" name="E01CUOPAG" size="4" maxlength="4" value="<%= inqLoans.getE01CUOPAG().trim()%>" readonly>
            </td>
            <td nowrap  bordercolor="#000000" > 
              <div align="right">Previsi&oacute;n Intereses :</div>
            </td>
            <td nowrap  bordercolor="#000000" >
         	  <eibsinput:text name="inqLoans" property="E01DLCPIN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Cuotas Vencidas :</div>
            </td>
            <td nowrap  bordercolor="#000000" height="32"> 
              <input type="text" name="E01CUOVEN" size="4" maxlength="4" value="<%= inqLoans.getE01CUOVEN().trim()%>" readonly>
            </td>
            <td nowrap  bordercolor="#000000" height="32"> 
              <div align="right">Intereses Contingencia :</div>
            </td>
            <td nowrap  bordercolor="#000000" height="32">
          	  <eibsinput:text name="inqLoans" property="E01DLCICO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trclear">
            <td nowrap >
              <div align="right">Fecha Ultima Renovaci&oacute;n :</div>
            </td>
            <td nowrap  bordercolor="#000000">
              <input type="text" name="E01DEAEX1" size="3" maxlength="2" value="<%= inqLoans.getE01DEAEX1().trim()%>" readonly>
              <input type="text" name="E01DEAEX2" size="3" maxlength="2" value="<%= inqLoans.getE01DEAEX2().trim()%>" readonly>
              <input type="text" name="E01DEAEX3" size="5" maxlength="4" value="<%= inqLoans.getE01DEAEX3().trim()%>" readonly>
            </td>
            <td nowrap  bordercolor="#000000">
              <div align="right">Mora Contingencia :</div>
            </td>
            <td nowrap  bordercolor="#000000">
         	  <eibsinput:text name="inqLoans" property="E01DLCPCO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap >
              <div align="right">N&uacute;mero Renovaciones :</div>
            </td>
            <td nowrap  bordercolor="#000000">
              <input type="text" name="E01DEARON" size="2" maxlength="2" value="<%= inqLoans.getE01DEARON().trim()%>" readonly>
            </td>
            <td nowrap  bordercolor="#000000">
              <div align="right">Ultimo Cambio Calificaci&oacute;n :</div>
            </td>
            <td nowrap  bordercolor="#000000">
              <input type="text" name="E01DLCCO1" size="3" maxlength="2" value="<%= inqLoans.getE01DLCCO1().trim()%>" readonly>
              <input type="text" name="E01DLCCO2" size="3" maxlength="2" value="<%= inqLoans.getE01DLCCO2().trim()%>" readonly>
              <input type="text" name="E01DLCCO3" size="5" maxlength="4" value="<%= inqLoans.getE01DLCCO3().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Posee Garant&iacute;as :</div>
            </td>
            <td nowrap  bordercolor="#000000">
              <input type="text" name="E01DEAHTM" size="2" maxlength="1" value="<%= inqLoans.getE01DEAHTM().trim()%>" readonly>
            </td>
            <td nowrap  bordercolor="#000000"> 
              <div align="right">L&iacute;nea de Cr&eacute;dito :</div>
            </td>
            <td nowrap  bordercolor="#000000">
              <input type="text" name="E01DEACMC" size="9" maxlength="9" value="<%= inqLoans.getE01DEACMC().trim()%>" readonly>
              <input type="text" name="E01DEACMN2" size="4" maxlength="4" value="<%= inqLoans.getE01DEACMN().trim()%>" readonly>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Otros Saldos</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right"> Principal Pagado :</div>
            </td>
            <td nowrap > 
         	  <eibsinput:text name="inqLoans" property="E01PRIPYM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
            <td nowrap > 
              <div align="right">Interes Acumulado :</div>
            </td>
            <td nowrap > 
         	  <eibsinput:text name="inqLoans" property="E01DEAIAL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"> Promedio Principal :</div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
         	  <eibsinput:text name="inqLoans" property="E01DEAAVP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
              <div align="right">Inter&eacute;s Pagado :</div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
         	  <eibsinput:text name="inqLoans" property="E01DEAIPL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right"> Principal D&iacute;a Ayer :</div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
         	  <eibsinput:text name="inqLoans" property="E01DEAPRI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
              <div align="right">Inter&eacute;s de Ayer :</div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" >
         	  <eibsinput:text name="inqLoans" property="E01YESINT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trclear">
            <td nowrap  >
              <div align="right">Valor Participado : </div>
            </td>
            <td nowrap  bordercolor="#000000" >
         	  <eibsinput:text name="inqLoans" property="E01VALPAR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
            <td nowrap  bordercolor="#000000" >
              <div align="right">Inter&eacute;s Ajustado :</div>
            </td>
            <td nowrap  bordercolor="#000000" >
         	  <eibsinput:text name="inqLoans" property="E01DEAIJL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap  > 
              <div align="right">Deducciones Pagadas :</div>
            </td>
            <td nowrap  bordercolor="#000000" > 
         	  <eibsinput:text name="inqLoans" property="E01DEADEL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
            <td nowrap  bordercolor="#000000" > 
              <div align="right">Inter&eacute;s Pagado A&ntilde;o :</div>
            </td>
            <td nowrap  bordercolor="#000000" > 
         	  <eibsinput:text name="inqLoans" property="E01DEAIPY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Comisiones Pagadas :</div>
            </td>
            <td nowrap  bordercolor="#000000" height="20"> 
         	  <eibsinput:text name="inqLoans" property="E01DEACPL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
            <td nowrap  bordercolor="#000000" height="20"> 
              <div align="right">Inter&eacute;s Diario :</div>
            </td>
            <td nowrap  bordercolor="#000000" height="20"> 
         	  <eibsinput:text name="inqLoans" property="E01DLYINT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Impuestos Pagados :</div>
            </td>
            <td nowrap  bordercolor="#000000"> 
         	  <eibsinput:text name="inqLoans" property="E01DEATPL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
            <td nowrap  bordercolor="#000000"> 
              <div align="right">Mora Acumulado :</div>
            </td>
            <td nowrap  bordercolor="#000000">
         	  <eibsinput:text name="inqLoans" property="E01DEAPIA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
			</td>         
          </tr>
          
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">IVA Pagado :</div>
            </td>
            <td nowrap  bordercolor="#000000"> 
         	  <eibsinput:text name="inqLoans" property="E01DEAIVL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
            <td nowrap  bordercolor="#000000"> 
              <div align="right">Mora Pagada:</div>
            </td>
            <td nowrap  bordercolor="#000000">
         	  <eibsinput:text name="inqLoans" property="E01DEAPIP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
           </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap >
              <div align="right">Acumulado Interes DGI  :</div>
            </td>
            <td nowrap >
         	  <eibsinput:text name="inqLoans" property="E01DLCAV1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
           <td nowrap >
              <div align="right">Pagado Interes DGI  :</div>
            </td>
            <td nowrap >
         	  <eibsinput:text name="inqLoans" property="E01DLCAV2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap >
              <div align="right">Ajustado Interes DGI  :</div>
            </td>
            <td nowrap >
         	  <eibsinput:text name="inqLoans" property="E01DEATDM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
            <td nowrap >
              <div align="right">Mensual Interes DGI  :</div>
            </td>
            <td nowrap >
         	  <eibsinput:text name="inqLoans" property="E01DEAAVI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>    
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <h4>Tasas </h4>

  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Tasa Base / Spread Actual : </div>
            </td>
            <td nowrap > 
              <eibsinput:text name="inqLoans" property="E01DEARTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>      
            </td>
            <td nowrap> 
              <div align="right">Tasa Flotante Actual :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="inqLoans" property="E01DEAFRT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>      
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Tasa Base / Spread Anterior : </div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
              <eibsinput:text name="inqLoans" property="E01DEAPBR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>      
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
              <div align="right">Tasa Flotante Anterior :</div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
              <eibsinput:text name="inqLoans" property="E01DEAPFR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>      
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Tasa Efectiva : </div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
              <eibsinput:text name="inqLoans" property="E01DEASPR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>      
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
              <div align="right">Pr&oacute;xima Tasa Flotante :</div>
            </td>
            <td nowrap  bordercolor="#FFFFFF" > 
              <eibsinput:text name="inqLoans" property="E01DEANER" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>      
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Fechas </h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Fecha Valor :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEASD1" size="3" maxlength="2" value="<%= inqLoans.getE01DEASD1().trim()%>" readonly>
              <input type="text" name="E01DEASD2" size="3" maxlength="2" value="<%= inqLoans.getE01DEASD2().trim()%>" readonly>
              <input type="text" name="E01DEASD3" size="5" maxlength="4" value="<%= inqLoans.getE01DEASD3().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Fecha Ultimo Cambio Contable :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEALG1" size="3" maxlength="2" value="<%= inqLoans.getE01DEALG1().trim()%>" readonly>
              <input type="text" name="E01DEALG2" size="3" maxlength="2" value="<%= inqLoans.getE01DEALG2().trim()%>" readonly>
              <input type="text" name="E01DEALG3" size="5" maxlength="4" value="<%= inqLoans.getE01DEALG3().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Fecha Ultimo Cambio de Tasa :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEARC1" size="3" maxlength="2" value="<%= inqLoans.getE01DEARC1().trim()%>" readonly>
              <input type="text" name="E01DEARC2" size="3" maxlength="2" value="<%= inqLoans.getE01DEARC2().trim()%>" readonly>
              <input type="text" name="E01DEARC3" size="5" maxlength="4" value="<%= inqLoans.getE01DEARC3().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right"> Fecha Ultimo Pago Capital :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEALP1" size="3" maxlength="2" value="<%= inqLoans.getE01DEALP1().trim()%>" readonly>
              <input type="text" name="E01DEALP2" size="3" maxlength="2" value="<%= inqLoans.getE01DEALP2().trim()%>" readonly>
              <input type="text" name="E01DEALP3" size="5" maxlength="4" value="<%= inqLoans.getE01DEALP3().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Fecha Pr&oacute;ximo Cambio de Tasa :</div>
            </td>
            <td nowrap> 
              <input type="text" name="E01DEANR1" size="3" maxlength="2" value="<%= inqLoans.getE01DEANR1().trim()%>" readonly>
              <input type="text" name="E01DEANR2" size="3" maxlength="2" value="<%= inqLoans.getE01DEANR2().trim()%>" readonly>
              <input type="text" name="E01DEANR3" size="5" maxlength="4" value="<%= inqLoans.getE01DEANR3().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Fecha Ultimo Pago Intereses :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEALI1" size="3" maxlength="2" value="<%= inqLoans.getE01DEALI1().trim()%>" readonly>
              <input type="text" name="E01DEALI2" size="3" maxlength="2" value="<%= inqLoans.getE01DEALI2().trim()%>" readonly>
              <input type="text" name="E01DEALI3" size="5" maxlength="4" value="<%= inqLoans.getE01DEALI3().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear">
            <td nowrap >
              <div align="right">Fecha Ultima Modificaci&oacute;n :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEALM1" size="3" maxlength="2" value="<%= inqLoans.getE01DEALM1().trim()%>" readonly>
              <input type="text" name="E01DEALM2" size="3" maxlength="2" value="<%= inqLoans.getE01DEALM2().trim()%>" readonly>
              <input type="text" name="E01DEALM3" size="5" maxlength="4" value="<%= inqLoans.getE01DEALM3().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Modificado por :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEAUSR" size="15" maxlength="10" value="<%= inqLoans.getE01DEAUSR().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Inter&eacute;s Pagado Hasta :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEAIT1" size="3" maxlength="2" value="<%= inqLoans.getE01DEAIT1().trim()%>" readonly>
              <input type="text" name="E01DEAIT2" size="3" maxlength="2" value="<%= inqLoans.getE01DEAIT2().trim()%>" readonly>
              <input type="text" name="E01DEAIT3" size="5" maxlength="4" value="<%= inqLoans.getE01DEAIT3().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Principal Pagado Hasta :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEAPT1" size="3" maxlength="2" value="<%= inqLoans.getE01DEAPT1().trim()%>" readonly>
              <input type="text" name="E01DEAPT2" size="3" maxlength="2" value="<%= inqLoans.getE01DEAPT2().trim()%>" readonly>
              <input type="text" name="E01DEAPT3" size="5" maxlength="4" value="<%= inqLoans.getE01DEAPT3().trim()%>" readonly>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
</body>
</html>
