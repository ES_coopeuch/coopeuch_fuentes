<html> 
<head>
<title>Mantenimiento de Datos Comunes</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<jsp:useBean id="error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session"/>
<jsp:useBean id="msgCD" class="datapro.eibs.beans.EDL084801Message"  scope="session" />
<jsp:useBean id="userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

</head>
<body>

 
 <% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     error.setERRNUM("0");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
    }
%>
  <h3 align="center">Mantenimiento de Datos Comunes<br>Nuevo<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="common_info_new.jsp, EDL0848"> 
  </h3>
  <hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0848" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="400">
  <INPUT TYPE=HIDDEN NAME="E01DLRRTP" VALUE="<%= userPO.getHeader1() %>">
  
  <table class="tableinfo">
      <tr> 
        <td nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
                  <tr id="trdark"> 
              <td nowrap width="16%"> 
                <div align="right">C�digo :</div>
              </td>
              <td nowrap width="20%">  
                <input type="text" name="E01DLRCOD" size="7" maxlength="6" value="<%= msgCD.getE01DLRCOD() %>">
              </td>
              <td nowrap width="16%"> 
              </td>
              <td nowrap width="20%">  
              </td>
            </tr>

            <tr id="trclear"> 
              <td nowrap width="16%"> 
                <div align="right">Nombre :</div>
              </td>
              <td nowrap width="20%">  
                <input type="text" name="E01DLRNME" size="48" maxlength="45" value="<%= msgCD.getE01DLRNME() %>">
              </td>
              <td nowrap width="16%"> 
              </td>
              <td nowrap width="20%">  
              </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="16%"> 
                <div align="right">Direcci�n 1 :</div>
              </td>
              <td nowrap width="20%">  
                <input type="text" name="E01DLRAD1" size="48" maxlength="45" value="<%= msgCD.getE01DLRAD1() %>">
              </td>
              <td nowrap width="16%"> 
                <div align="right">Identificaci�n :</div>
              </td>
              <td nowrap width="20%">  
                <input type="text" name="E01DLRIDN" size="26" maxlength="25" value="<%= msgCD.getE01DLRIDN() %>">
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="16%"> 
                <div align="right">Direcci�n 2 :</div>
              </td>
              <td nowrap width="20%">  
                <input type="text" name="E01DLRAD2" size="48" maxlength="45" value="<%= msgCD.getE01DLRAD2() %>">
              </td>
              <td nowrap width="16%"> 
                <div align="right">Tel�fono :</div>
              </td>
              <td nowrap width="20%">  
                <input type="text" name="E01DLRPHN" size="16" maxlength="15" value="<%= msgCD.getE01DLRPHN() %>">
              </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="16%"> 
                <div align="right">Estado/Provincia :</div>
              </td>
              <td nowrap width="20%">  
                <input type="text" name="E01DLRCOM" size="5" maxlength="4" value="<%= msgCD.getE01DLRCOM() %>">
                <input type="text" name="D01DLRNME" size="46" maxlength="45" value="<%= msgCD.getD01DLRNME() %>">
			    <a href="javascript:GetCodeDescCNOFC('E01DLRCOM','D01DLRNME','PV')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0"></a> 
              </td>
              <td nowrap width="16%"> 
                <div align="right">Ciudad :</div>
              </td>
              <td nowrap width="20%">  
                <input type="text" name="E01DLRCIT" size="5" maxlength="4" value="<%= msgCD.getE01DLRCIT() %>">
 				<a href="javascript:GetCNOFCFilteredCodes('E01DLRCIT','',document.forms[0].E01DLRCOM.value,'84')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0"></a> 
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="16%"> 
                <div align="right">Agencia :</div>
              </td>
              <td nowrap width="20%">  
                <input type="text" name="E01DLRBRN" size="5" maxlength="4" value="<%= msgCD.getE01DLRBRN() %>">
                <input type="text" name="D01DLRBRN" size="46" maxlength="45" value="<%= msgCD.getD01DLRBRN() %>">
				<a href="javascript:GetBranch('E01DLRBRN','','D01DLRBRN')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"></a>
              </td>
              <td nowrap width="16%"> 
              </td>
              <td nowrap width="20%">  
              </td>
            </tr>
		  </table>
        </td>
      </tr>
    </table>
  <BR>
<p align="center"> 
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar" >
</p>
      
</form>
</body>
</html>
