<!-- Hecho por Alonso Arana ------Datapro-----06/02/2014 -->
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC200001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Conciliación Bancos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "ERCSeleccion" class= "datapro.eibs.beans.ERC200001Message"  scope="session" />
<jsp:useBean id="lista_entrada" class="datapro.eibs.beans.ERC200001Message" scope="session"/>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">
</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Conciliación Automática<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ERC2000_statement_details.jsp,ERC2000"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC2000">
<input type="hidden" name="SCREEN" value="200">





<input type="hidden" name="E01RCHAPF" value="<%=lista_entrada.getE01RCHAPF() %>"> 
<input type="hidden" name="E01BRMEID" value="<%=session.getAttribute("codigo_banco") %>"> 
<input type="hidden" name="E01BRMCTA" value="<%=session.getAttribute("cuenta_banco") %>">
<input type="hidden" name="E01RCHTFC" value="<%=lista_entrada.getE01RCHTFC() %>" >
<input type="hidden" name="E01BRMACC" value="<%=session.getAttribute("cuenta_ibs")%>">
  <input type="hidden" name="E01DSCRBK" value="<%=session.getAttribute("nombre_banco") %>">

<input type="hidden" name="E01RCHSTN" value="<%=lista_entrada.getE01RCHSTN() %>">

<input type="hidden" name="E01RCHFSD" value="<%=lista_entrada.getE01RCHFSD() %>"> 
<input type="hidden" name="E01RCHFSM" value="<%=lista_entrada.getE01RCHFSM() %>"> 
<input type="hidden" name="E01RCHFSY" value="<%=lista_entrada.getE01RCHFSY() %>"> 

<input type="hidden" name="E01RCHFLD" value="<%=lista_entrada.getE01RCHFLD() %>"> 
<input type="hidden" name="E01RCHFLM" value="<%=lista_entrada.getE01RCHFLM() %>"> 
<input type="hidden" name="E01RCHFLY" value="<%=lista_entrada.getE01RCHFLY() %>"> 







  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">   Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<% out.print(session.getAttribute("codigo_banco")+"-"+session.getAttribute("nombre_banco"));%>" 
	  			size="60"  readonly>
             </td>          
         </tr>
         
           <tr>
                
            <td nowrap width="10%" align="right"> Cuenta Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<%=session.getAttribute("cuenta_banco")%>" 
	  			size="30"  readonly>
             </td>
                    <td nowrap width="10%" align="right"> Cuenta IBS : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<%=session.getAttribute("cuenta_ibs")%>" 
	  			size="30"  readonly>
             </td>  
                
                </tr>
        </table>
      </td>
    </tr>
  </table>
 
 <p>&nbsp;</p>
   <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">Cartola : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="cartola" value="<%=ERCSeleccion.getE01RCHSTN() %>" 
	  			size="23"  readonly>
             </td>  
             
               <td nowrap width="10%" align="right">Fecha Carga : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="fecha2" size="23" maxlength="20" value="<% out.print(ERCSeleccion.getE01RCHSDD()+"/"+ERCSeleccion.getE01RCHSDM()+"/"+ERCSeleccion.getE01RCHSDY());%>" readonly>
             </td>  
         </tr>
         
          <tr>
             <td nowrap width="10%" align="right">Estado : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="fecha21" size="23" maxlength="20" value="<%=ERCSeleccion.getE01DESAPF() %>" readonly>
             </td>
        <td nowrap width="10%" align="right">Fecha Estado : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01B" size="23" maxlength="20" value="<% out.print(ERCSeleccion.getE01RCHLMD()+"/"+ERCSeleccion.getE01RCHLMM()+"/"+ERCSeleccion.getE01RCHLMY());%>" readonly>
             </td>
             </tr>
      
           <tr>
             <td nowrap width="10%" align="right">Fecha Cartola Desde : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha" size="23" maxlength="20" value="<%
		String	fecha=ERCSeleccion.getE01RCHFSD().trim()+"/"+ERCSeleccion.getE01RCHFSM().trim()+"/"+ERCSeleccion.getE01RCHFSY().trim(); 
				out.print(fecha.trim());
			
			 %>" readonly>
             </td>
        <td nowrap width="10%" align="right">Fecha Cartola Hasta : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha2" size="23" maxlength="20" value="<%			out.print(ERCSeleccion.getE01RCHFLD()+"/"+ERCSeleccion.getE01RCHFLM()+"/"+ERCSeleccion.getE01RCHFLY());  %>" readonly>
             </td>
             </tr>
        
        <tr>
             <td nowrap width="10%" align="right">Saldo Inicial : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha" size="23" maxlength="20" value="<%=ERCSeleccion.getE01RCHBBL()%>" readonly >
             </td>
        <td nowrap width="10%" align="right">Saldo Final : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha2" size="23" maxlength="20" value="<%=ERCSeleccion.getE01RCHBBF()%>" readonly>
             </td>
             </tr>
             
             
               <tr>
             <td nowrap width="10%" align="right">Registros : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha" size="23" maxlength="20" value="<%=ERCSeleccion.getE01RCHTRG()%>" readonly >
             </td>
        <td nowrap width="10%" align="right">Tipo Carga : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha2" size="23" maxlength="20" value="<%
 
  if(ERCSeleccion.getE01RCHTCA().equals("A")){
 
 out.print("AUTOMÁTICA");
 }
else if(ERCSeleccion.getE01RCHTCA().equals("M")){
 
 out.print("MANUAL");
 }

 
 
 %>" readonly>
             </td>
             </tr>
             
            
        </table>
      </td>
    </tr>
  </table>
 
 
 <p>&nbsp;</p>
<table class="tableinfo" width="100%">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">     
		    <tr>
				<div align="center">
  					<h3>
  			<br>Se ha sometido el proceso de conciliación automática<br>
  					</h3>
  				 </div>		    
		      <td > 
		      </td>
		    </tr>
		 </table>
	  </td>
	</tr>
</table>

  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Volver">
  </p>

</form>
</body>
</html>
