<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Definici&oacute;n de Comisiones - Relaci&oacute;n</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="RTComis" class="datapro.eibs.beans.EDD232001Message"  scope="session" />
<jsp:useBean id="RefRel" class="datapro.eibs.beans.EDD232002Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

</head>

<BODY>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="JavaScript">
function cancel() {
	document.forms[0].SCREEN.value = 1100;
	document.forms[0].submit();
}

</SCRIPT>  

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "NEW".equals(userPO.getPurpose()) ? "" : "readonly";     
%>



<h3 align="center">Definici&oacute;n de Comisiones - 
<% if(userPO.getPurpose().equals("NEW")){ %>
Nueva Relaci&oacute;n
<% } else {%>
Mantenci&oacute;n Relaci&oacute;n

<% } %>




<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="commission_relation_maitenance, EDD2320"></h3>
<hr size="4">
<form name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2320" >
    <input type=HIDDEN name="SCREEN" value="1600">

  <h4>Datos Comisi&oacute;n</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Comisi&oacute;n :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01CMTMCCM" size="5" maxlength="4" value="<%= RTComis.getE01CMTMCCM().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Descripci&oacute;n  :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E01CMTMDCM" size="31" maxlength="30" value="<%= RTComis.getE01CMTMDCM().trim()%>" readonly>
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Tipo de Comisi&oacute;n :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMTCM" size="2" maxlength="1" value="<%= RTComis.getE01CMTMTCM().trim()%>" readonly>
                <input type="text" name="E01CMTMTCG" size="31" maxlength="30" value="<%= RTComis.getE01CMTMTCG().trim()%>" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Tipo Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMTCB" size="2" maxlength="1" value="<%= RTComis.getE01CMTMTCB().trim()%>" >
                <input type="text" name="E01CMTMTBG" size="31" maxlength="30" value="<%= RTComis.getE01CMTMTBG().trim()%>" >
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Periocidad de Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMPCB" size="3" maxlength="2" value="<%= RTComis.getE01CMTMPCB().trim()%>"  readonly>
                <input type="text" name="E01CMTMPCG" size="31" maxlength="30" value="<%= RTComis.getE01CMTMPCG().trim()%>"  readonly>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Modalidad de Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMMCB" size="2" maxlength="1" value="<%= RTComis.getE01CMTMMCB().trim()%>"   readonly>
                <input type="text" name="E01CMTMMCG" size="31" maxlength="30" value="<%= RTComis.getE01CMTMMCG().trim()%>"   readonly>
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">D&iacute;a de C&aacute;lculo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMDCC" size="3" maxlength="2" value="<%= RTComis.getE01CMTMDCC().trim()%>"  readonly>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">D&iacute;a de Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMDAP" size="2" maxlength="1" value="<%= RTComis.getE01CMTMDAP().trim()%>"   readonly>
              </div>
            </td>
          </tr> 
          
          <tr id="trclear"> 
            <td nowrap  height="23"> 
              <div align="right">Impuesto :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMIPT" size="2" maxlength="1" value="<%= RTComis.getE01CMTMIPT().trim()%>"   readonly>
              </div>
            </td>
            <td nowrap height="23"> 
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Datos Relaci&oacute;n</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Producto :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E02CMRLPRO" size="5" maxlength="4" value="<%= RefRel.getE02CMRLPRO().trim()%>" readonly>
                <input type="text" name="E02CMRLPRG" size="31" maxlength="30" value="<%= RefRel.getE02CMRLPRG().trim()%>"  readonly>
                <% if(userPO.getPurpose().equals("NEW")){ %>
               		 <a href="javascript:GetProduct('E02CMRLPRO','01','','E02CMRLPRG')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0" ></a> 
                <% } %>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Cargo  :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E02CMRLCCA" size="5" maxlength="4" value="<%= RefRel.getE02CMRLCCA().trim()%>" readonly>
                <input type="text" name="E02CMRLCCG" size="31" maxlength="30" value="<%= RefRel.getE02CMRLCCG().trim()%>"  readonly>
                <% if(userPO.getPurpose().equals("NEW")){ %>
 				<a href="javascript:GetRetCod('E02CMRLCCA','ZZZZ', 'E02CMRLCCG')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="middle" border="0" ></a>                 
                <% } %>
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">C&oacute;digo de Transacci&oacute;n :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E02CMRLTRX" size="5" maxlength="4" value="<%= RefRel.getE02CMRLTRX().trim()%>" readonly>
                <input type="text" name="E02CMRLTRG" size="20" maxlength="21" value="<%= RefRel.getE02CMRLTRG().trim()%>" readonly>
                <% if(userPO.getPurpose().equals("NEW")){ %>
				<a href="javascript:GetCodeDescCNTIN('E02CMRLTRX','E02CMRLTRG','164')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
                <% } %>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Con Pl&aacute;stico :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
			<% if(!userPO.getPurpose().equals("NEW")) { %>
                <input type="text" name="E02CMRLPLA" size="2" maxlength="1" value="<%=RefRel.getE02CMRLPLA()%>" <%=readonly %>>
			<% } else {%>
	  			<select name="E02CMRLPLA">   
                  <option value="S" <% if (RefRel.getE02CMRLPLA().equals("S")) out.print("selected"); %>>S</option>                   
                  <option value="N" <% if (!RefRel.getE02CMRLPLA().equals("S")) out.print("selected"); %>>N</option>
                </select>                
			<% }%>
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Filtro 1 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E02CMRLFI1" size="5" maxlength="4" value="<%= RefRel.getE02CMRLFI1().trim()%>" <%=readonly %>> 
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Filtro 2 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E02CMRLFI2" size="5" maxlength="4" value="<%= RefRel.getE02CMRLFI2().trim()%>" <%=readonly %> >
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Filtro 3 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E02CMRLFI3" size="5" maxlength="4" value="<%= RefRel.getE02CMRLFI3().trim()%>"  <%=readonly %>>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Filtro 4 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E02CMRLFI4" size="5" maxlength="4" value="<%= RefRel.getE02CMRLFI4().trim()%>" <%=readonly %> >
              </div>
            </td>
          </tr> 
          
          <tr id="trclear"> 
            <td nowrap  height="23"> 
              <div align="right">Filtro 5 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E02CMRLFI5" size="5" maxlength="4" value="<%= RefRel.getE02CMRLFI5().trim()%>"  <%=readonly %>>
              </div>
            </td>
            <td nowrap height="23"> 
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">C&oacute;digo de Tarifa :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E02CMRLCTF" size="5" maxlength="4" value="<%= RefRel.getE02CMRLCTF().trim()%>" readonly >
                <input type="text" name="E02CMRLCTG" size="20" maxlength="21" value="<%= RefRel.getE02CMRLCTG().trim()%>" readonly>
				<a href="javascript:GetCodeDescFILES('E02CMRLCTF','E02CMRLCTG','CMPTF')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">C&oacute;digo de Descuento :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E02CMRLCDC" size="5" maxlength="4" value="<%= RefRel.getE02CMRLCDC().trim()%>"   readonly>
                <input type="text" name="E02CMRLCDG" size="20" maxlength="21" value="<%= RefRel.getE02CMRLCDG().trim()%>" readonly>
				<a href="javascript:GetCodeDescFILES('E02CMRLCDC','E02CMRLCDG','CMDCA')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 

        </table>
      </td>
    </tr>
  </table>

  
    <div align="center">
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel()">
  </div>
  
</form>

</body>
</html>
