<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Transacciones</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<jsp:useBean id= "crossRef" class= "datapro.eibs.beans.EGL034002Message"  scope="session" /> 
<jsp:useBean id= "gLedger" class= "datapro.eibs.beans.EGL034001Message"  scope="session" /> 
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" /> 
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

</head>
<body > 

<SCRIPT LANGUAGE="JavaScript">
 builtHPopUp();

 function showPopUp(optHelp,fieldname,bank,ccy,aux1,aux2,opcode) {
   init(optHelp,fieldname,bank,ccy,aux1,aux2,opcode);
   showPopupHelp();
   }   
</SCRIPT>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">
            showErrors();
     </SCRIPT>
 <%
 }
%>


<h3 align="center">Mantenimiento Referencias Cruzadas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="crossRef_basic,EGL0340"></h3>
<hr size="4">      
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEGL0340">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="5">    
  <INPUT TYPE=HIDDEN NAME="E02GLMACD" value="<%= crossRef.getE02GLMACD().trim()%>">
  <table class="tableinfo" id="trdark">
    <tr id="trdark"> 
      <td align="right"  nowrap width="30%">
       Cuenta Contable :
      </td>
      <td align="right"  nowrap > 
        <div align="left">         
           <input type="text" name="E02GLMBNK" size="3" maxlength="2" readonly value = "<%= crossRef.getE02GLMBNK() %>">         
           <input type="text" name="E02GLMCCY" size="4" maxlength="3" readonly value = "<%= crossRef.getE02GLMCCY() %>">          
           <input type="text" name="E02GLMGLN" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMGLN() %>">          
        </div>
      </td>
    </tr>
    <tr id="trdark"> 
      <td align="right" nowrap width="30%"  >
        Descripción :
      </td>
      <td align="right" nowrap > 
        <div align="left"> 
          <input type="text" name="E02GLMDSC" size="60" maxlength="60" readonly value="<%= crossRef.getE02GLMDSC() %>">
        </div>
      </td>
    </tr>   
  </table>
  
  
  <% if (crossRef.getE02GLMACD().equals("10")) { %>
  <H4>Modulo de Prestamos</H4>

    <%if(currUser.getE01INT().equals("18")){%> 

	  <table class="tableinfo">
	    <tr  id="trdark">
	      <TH align="left" colspan=2>Devengos </TH>
	    </tr> 
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Debito por Interes por Cobrar : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXDR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXDR() %>">
	        <input type="text" name="D02GLMXDR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXDR() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Credito por Interes por Cobrar : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXCR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCR() %>">
	        <input type="text" name="D02GLMXCR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCR() %>">
	      </td>
	    </tr>
	    <% if (!gLedger.getE01GLMREV().equals("N")) {%>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap> 
	        Debito Reajuste por Cobrar : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMRIN" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMRIN() %>">
	        <input type="text" name="D02GLMRIN" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMRIN() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap> 
	        Credito Reajuste por Cobrar : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMREX" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMREX() %>">
	        <input type="text" name="D02GLMREX" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMREX() %>">
	      </td>
	    </tr>
	    <% } %>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap> 
	        Debito Cargo por Mora : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXLR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLR() %>">
	        <input type="text" name="D02GLMXLR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLR() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap> 
	        Credito Cargo por Mora : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXLC" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLC() %>">
	        <input type="text" name="D02GLMXLC" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLC() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap> 
	        Cuenta de Cobranza Otros Cargos : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMX32" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX32() %>">
	        <input type="text" name="D02GLMX32" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX32() %>">
	      </td>
	    </tr>
	    <tr  id="trdark">
	      <TH align="left" colspan=2>Vencidos</TH>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Debito Orden Interes Suspendido : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG01" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG01() %>">
	        <input type="text" name="D02GLXG01" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG01() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Credito Responsabilidad Int. Suspendido.: </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG02" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG02() %>">
	        <input type="text" name="D02GLXG02" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG02() %>">
	      </td>
	    </tr>
	    <% if (!gLedger.getE01GLMREV().equals("N")) {%>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Debito Orden Reajuste Suspendido : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG03" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG03() %>">
	        <input type="text" name="D02GLXG03" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG03() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Credito Responsabilidad Reaj. Suspendido.: </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG04" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG04() %>">
	        <input type="text" name="D02GLXG04" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG04() %>">
	      </td>
	    </tr>
	    <% } %>

	     <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Debito Interes Mora Suspendido : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMX19" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX19() %>">
	        <input type="text" name="D02GLMX19" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX19() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Crédito Interes Mora Suspendido : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMX20" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX20() %>">
	        <input type="text" name="D02GLMX20" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX20() %>">
	      </td>
	    </tr>
	   
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Cuenta de Principal Activo Vencido : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG05" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG05() %>">
	        <input type="text" name="D02GLXG05" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG05() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Cuenta de Interes Activo Vencido : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG06" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG06() %>">
	        <input type="text" name="D02GLXG06" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG06() %>">
	      </td>
	    </tr>
	    <% if (!gLedger.getE01GLMREV().equals("N")) {%>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Cuenta de Reajuste Activo Vencido : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG07" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG07() %>">
	        <input type="text" name="D02GLXG07" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG07() %>">
	      </td>
	    </tr>
	    <% } %>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Cuenta de Cobranza Activo Vencido : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG08" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG08() %>">
	        <input type="text" name="D02GLXG08" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG08() %>">
	      </td>
	    </tr>
	    
	    
	    <% if (!gLedger.getE01GLMREV().equals("N")) {%>
	    <tr  id="trdark">
	      <TH align="left" colspan=2>Cuotas Reajustables Vencidas  </TH>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Debito Cuenta Orden Reajuste por Capital : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG09" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG09() %>">
	        <input type="text" name="D02GLXG09" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG09() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Credito Cuenta Orden Reajuste por Capital : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG10" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG10() %>">
	        <input type="text" name="D02GLXG10" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG10() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Debito Cuenta Orden Reajuste por Interes : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG11" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG11() %>">
	        <input type="text" name="D02GLXG11" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG11() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Credito Cuenta Orden Reajuste por Interes : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG12" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG12() %>">
	        <input type="text" name="D02GLXG12" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG12() %>">
	      </td>
	    </tr>
	    <% } %>
	    <tr  id="trdark">
	      <TH align="left" colspan=2>Castigo  </TH>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Debito Cuenta Castigo : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG16" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG16() %>">
	        <input type="text" name="D02GLXG16" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG16() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Credito Cuenta Castigo : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG17" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG17() %>">
	        <input type="text" name="D02GLXG17" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG17() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Cuenta de Provision Individual : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMX28" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX28() %>">
	        <input type="text" name="D02GLMX28" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX28() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Cuenta de Provision Global : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMX29" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX29() %>">
	        <input type="text" name="D02GLMX29" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX29() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Cuenta Uso de Provision : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMX30" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX30() %>">
	        <input type="text" name="D02GLMX30" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX30() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Cuenta de Responsabilidad : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMX31" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX31() %>">
	        <input type="text" name="D02GLMX31" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX31() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Debito Castigo No Informado : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG19" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG19() %>">
	        <input type="text" name="D02GLXG19" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG19() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Credito Castigo No Informado : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG20" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG20() %>">
	        <input type="text" name="D02GLXG20" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG20() %>">
	      </td>
	    </tr>
	
	    <tr  id="trdark">
	      <TH align="left" colspan=2>Referencias Adicionales  </TH>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Ingreso por Recupero Interes Suspenso : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXSO" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSO() %>">
	        <input type="text" name="D02GLMXSO" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSO() %>">
	      </td>
	    </tr>
	    <% if (!gLedger.getE01GLMREV().equals("N")) {%>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Ingreso por Recupero Reajuste Suspenso : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG14" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG14() %>">
	        <input type="text" name="D02GLXG14" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG14() %>">
	      </td>
	    </tr>    
	    <% } %>    
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Ingreso por Recupero  Mora Suspenso: </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXSD" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSD() %>">
	        <input type="text" name="D02GLMXSD" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSD() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Ingreso por Recupero Periodo Actual : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG15" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG15() %>">
	        <input type="text" name="D02GLXG15" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG15() %>">
	      </td>
	    </tr>
		<tr  id="trclear">
	      <td align="right" width="30%" nowrap>Ingreso por Recupero Periodo Anterior : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG13" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG13() %>">
	        <input type="text" name="D02GLXG13" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG13() %>">
	      </td>
	    </tr>   
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap> 
	        Cuenta de Prestamo Vencido : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXSM" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSM() %>">
	        <input type="text" name="D02GLMXSM" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSM() %>">
	      </td>
	    </tr>    
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap> 
	        Cuenta de Prestamo Vigente : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXSR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSR() %>">
	        <input type="text" name="D02GLMXSR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSR() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap> 
	        Intereses Pagos por Anticipado : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG18" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG18() %>">
	        <input type="text" name="D02GLXG18" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG18() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap> 
	        Cobranza Extrajudicial : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG43" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG43() %>">
	        <input type="text" name="D02GLXG43" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG43() %>">
	      </td>
	    </tr>

	    <% if (crossRef.getH02FLGWK3().equals("H")) {%>

	    <% if (gLedger.getE01GLMCLS().equals("1") ||gLedger.getE01GLMCLS().equals("6") || gLedger.getE01GLMCLS().equals("8")) { //activo %>
		     <tr  id="trdark">
		      <TH align="left" colspan=2>HIPOTECARIOS ACTIVOS </TH>
		    </tr>

		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Distribucion Fondos: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG21" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG21() %>">
		        <input type="text" name="D02GLXG21" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG21() %>">
		      </td>
		    </tr>
		
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Comisiones Letras Hipotecarias: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG22" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG22() %>">
		        <input type="text" name="D02GLXG22" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG22() %>">
		      </td>
		    </tr>
		      
		      <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Amortizacion Directa Letras Hipotecarias: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG23" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG23() %>">
		        <input type="text" name="D02GLXG23" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG23() %>">
		      </td>
		    </tr>
		      
		      <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Responsabilidad Amortizacion Directa Letras Hipotecarias : </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG24" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG24() %>">
		        <input type="text" name="D02GLXG24" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG24() %>">
		      </td>
		    </tr>
		      
		     <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Comision Letras Hipotecarias Suspendidas Vigentes: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG25" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG25() %>">
		        <input type="text" name="D02GLXG25" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG25() %>">
		      </td>
		    </tr>
		     
		     <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Comision Letras Hipotecarias Suspendidas Vencidas: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG26" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG26() %>">
		        <input type="text" name="D02GLXG26" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG26() %>">
		      </td>
		    </tr>
		      
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Interes Suspendido Vigente: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG27" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG27() %>">
		        <input type="text" name="D02GLXG27" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG27() %>">
		      </td>
		    </tr>
		
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Reajuste Suspendido Vigente: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG28" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG28() %>">
		        <input type="text" name="D02GLXG28" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG28() %>">
		      </td>
		    </tr>
		      
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Constituci&oacute;n Dividendos Vigentes: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG29" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG29() %>">
		        <input type="text" name="D02GLXG29" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG29() %>">
		      </td>
		    </tr>
		
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Constituci&oacute;n Dividendos Vencidos: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG30" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG30() %>">
		        <input type="text" name="D02GLXG30" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG30() %>">
		      </td>
		    </tr>
		    
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Constituci&oacute;n Dividendos Acelerados: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG31" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG31() %>">
		        <input type="text" name="D02GLXG31" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG31() %>">
		      </td>
		    </tr>
		
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Castigos del año: </td> 
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG32" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG32() %>">
		        <input type="text" name="D02GLXG32" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG32() %>">
		      </td>
		    </tr>
		
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Constituci&oacute;n Seguros Vigentes: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG33" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG33() %>">
		        <input type="text" name="D02GLXG33" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG33() %>">
		      </td>
		    </tr>
		
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Constituci&oacute;n Seguros Vencidos: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG34" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG34() %>">
		        <input type="text" name="D02GLXG34" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG34() %>">
		      </td>
		    </tr>
		
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Constituci&oacute;n Seguros Castigados: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG35" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG35() %>">
		        <input type="text" name="D02GLXG35" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG35() %>">
		      </td>
		    </tr>
		    
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Comision Letras Hipotecarias Suspendidas Castigadas: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG36" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG36() %>">
		        <input type="text" name="D02GLXG36" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG36() %>">
		      </td>
		    </tr>        
		    
			<tr  id="trclear">
		      <td align="right" width="30%" nowrap>Fondo de Amortizaci&oacute;n Extraordinaria: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG37" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG37() %>">
		        <input type="text" name="D02GLXG37" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG37() %>">
		      </td>
		    </tr>
		
			<tr  id="trclear">
		      <td align="right" width="30%" nowrap>Fondo de Inter&eacute;s Ordinario: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG38" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG38() %>">
		        <input type="text" name="D02GLXG38" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG38() %>">
		      </td>
		    </tr>
		
			<tr  id="trclear">
		      <td align="right" width="30%" nowrap>Reajuste Suspendido Castigado: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG39" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG39() %>">
		        <input type="text" name="D02GLXG39" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG39() %>">
		      </td>
		    </tr>

			<tr  id="trclear">
		      <td align="right" width="30%" nowrap>Interés Suspendido Castigado: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG40" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG40() %>">
		        <input type="text" name="D02GLXG40" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG40() %>">
		      </td>
		    </tr>
	
		<% } %> 	    

		<% if (gLedger.getE01GLMCLS().equals("2")) { // pasivo %>
		     <tr  id="trdark">
		      <TH align="left" colspan=2>HIPOTECARIOS PASIVOS </TH>
		    </tr>
		
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Constituci&oacute;n Cupones y Letras Sorteadas: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG21" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG21() %>">
		        <input type="text" name="D02GLXG21" size="35" maxlength="35" readonly value = "<%= crossRef.getD02GLXG21() %>">
		      </td>
		    </tr>
		
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Pago DCV: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG22" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG22() %>">
		        <input type="text" name="D02GLXG22" size="35" maxlength="35" readonly value = "<%= crossRef.getD02GLXG22() %>">
		      </td>
		    </tr>
		      
		      <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Amortización Ordinaria Letras Hipotecarias: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG23" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG23() %>">
		        <input type="text" name="D02GLXG23" size="35" maxlength="35" readonly value = "<%= crossRef.getD02GLXG23() %>">
		      </td>
		    </tr>
		      
		      <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Interés Ordinario Letras Hipotecarias: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG24" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG24() %>">
		        <input type="text" name="D02GLXG24" size="35" maxlength="35" readonly value = "<%= crossRef.getD02GLXG24() %>">
		      </td>
		    </tr>
		      
		     <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Amortización Extraordinaria Letras Hipotecarias: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG25" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG25() %>">
		        <input type="text" name="D02GLXG25" size="35" maxlength="35" readonly value = "<%= crossRef.getD02GLXG25() %>">
		      </td>
		    </tr>
		     
		     <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Responsabilidad Letras Hipotecarias: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG26" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG26() %>">
		        <input type="text" name="D02GLXG26" size="35" maxlength="35" readonly value = "<%= crossRef.getD02GLXG26() %>">
		      </td>
		    </tr>
		    
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Pagar&eacute; Cup&oacute;n: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG27" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG27() %>">
		        <input type="text" name="D02GLXG27" size="35" maxlength="35" readonly value = "<%= crossRef.getD02GLXG27() %>">
		      </td>
		    </tr>
		    
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Responsabilidad Pagar&eacute; Cup&oacute;n: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG28" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG28() %>">
		        <input type="text" name="D02GLXG28" size="35" maxlength="35" readonly value = "<%= crossRef.getD02GLXG28() %>">
		      </td>
		    </tr>
		    
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Reserva T&eacute;cnica: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG29" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG29() %>">
		        <input type="text" name="D02GLXG29" size="35" maxlength="35" readonly value = "<%= crossRef.getD02GLXG29() %>">
		      </td>
		    </tr>
		    
		    <tr  id="trclear">
		      <td align="right" width="30%" nowrap>Responsabilidad Reserva T&eacute;cnica: </td>
		      <td align="left" width="60%" nowrap> 
		        <input type="text" name="E02GLXG30" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG30() %>">
		        <input type="text" name="D02GLXG30" size="35" maxlength="35" readonly value = "<%= crossRef.getD02GLXG30() %>">
		      </td>
		    </tr>
		    
		   <% } %>      
		   <% } %>      
		 
	  </table>
	
  	<% } else { %>
  
	  <table class="tableinfo">
	    <tr  id="trdark">
	      <TH align="left" colspan=2>Periodificación de Interes  </TH>
	    </tr> 
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Debito por Interes Normal : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXDR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXDR() %>">
	        <input type="text" name="D02GLMXDR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXDR() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Credito por Interes Normal : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXCR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCR() %>"> 
	        <input type="text" name="D02GLMXCR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCR() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Debito por Interes de Mora : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXLR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLR() %>">
	        <input type="text" name="D02GLMXLR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLR() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Credito por Interes de Mora : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXLC" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLC() %>">
	        <input type="text" name="D02GLMXLC" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLC() %>">
	      </td>
	    </tr>
	
	    <%if(currUser.getE01INT().equals("06")){%> 
	    
	        <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Intereses Recibidos Por Anticipado   : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG22" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG22() %>">
	        <input type="text" name="D02GLXG22" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG22() %>">
	      </td>
	    </tr>
	        <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Reguladora Activo para devengo de interes : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG23" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG23() %>">
	        <input type="text" name="D02GLXG23" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG23() %>">
	      </td>
	    </tr>
	        <tr  id="trclear">
	      <td align="right" width="30%"> 
	          Reguladora Pasivo para devengo de interes : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG24" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG24() %>">
	        <input type="text" name="D02GLXG24" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG24() %>">
	      </td>
	    </tr>
	      <% } %>
	 
	 <% if (!gLedger.getE01GLMREV().equals("N")) { %>
	    <tr  id="trdark">
	      <TH align="left" colspan=2>Cuentas de Reajuste </TH>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Debito por Reajuste : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMRIN" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMRIN() %>">
	        <input type="text" name="D02GLMRIN" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMRIN() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Credito por Reajuste : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMREX" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMREX() %>">
	        <input type="text" name="D02GLMREX" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMREX() %>">
	      </td>
	    </tr>
	  <% } %>
	      
	    <tr  id="trdark">
	      <TH align="left" colspan=2>Renovación Reestructuración  </TH>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta Debito Contingencia : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXO2" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXO2() %>">
	        <input type="text" name="D02GLMXO2" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXO2() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta Credito Contingencia : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMX16" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX16() %>">
	        <input type="text" name="D02GLMX16" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX16() %>">
	      </td>
	    </tr>
	    <tr  id="trdark">
	      <TH align="left" colspan=2>Entradas Garantias  </TH>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta Debito Contingencia : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXIE" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXIE() %>">
	        <input type="text" name="D02GLMXIE" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXIE() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta Credito Contingencia : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMDFP" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMDFP() %>">
	        <input type="text" name="D02GLMDFP" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMDFP() %>">
	      </td>
	    </tr>
	    <tr  id="trdark">
	      <TH align="left" colspan=2>Referencias Adicionales  </TH>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta de Ingresos por Intereses/Orden : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXSO" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSO() %>">
	        <input type="text" name="D02GLMXSO" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSO() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta de Ingresos por Mora/Orden : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXSD" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSD() %>">
	        <input type="text" name="D02GLMXSD" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSD() %>">
	      </td>
	    </tr>
	
	    <% if (!gLedger.getE01GLMREV().equals("N")) { %>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta de reajuste pendiente Activo: 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMX25" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX25() %>">
	        <input type="text" name="D02GLMX25" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX25() %>">
	      </td>
	    </tr>
		<% } %>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta de Interes Pendiente Activo : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXCC" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCC() %>">
	        <input type="text" name="D02GLMXCC" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCC() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta de Prestamos Vencidos : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXSM" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSM() %>">
	        <input type="text" name="D02GLMXSM" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSM() %>">
	      </td>
	    </tr>    
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta de Prestamos % Capital Vencido : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMX17" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX17() %>">
	        <input type="text" name="D02GLMX17" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX17() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta Activo Vencido : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXSS" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSS() %>">
	        <input type="text" name="D02GLMXSS" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSS() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Cuenta de Prestamo Vigente : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLMXSR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSR() %>">
	        <input type="text" name="D02GLMXSR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSR() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Prórrogas De Préstamos  : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG21" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG21() %>">
	        <input type="text" name="D02GLXG21" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG21() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%"> 
	        Gastos Extraordinarios : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG29" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG29() %>">
	        <input type="text" name="D02GLXG29" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG29() %>">
	      </td>
	    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap> 
	        Intereses Pagos por Anticipado : 
	      </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG18" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG18() %>">
	        <input type="text" name="D02GLXG18" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG18() %>">
	      </td>
	    </tr>
	    
	  </table>
      <% } %>
  <% } %>
  
  
  <% if (crossRef.getE02GLMACD().equals("50") || crossRef.getE02GLMACD().equals("51")) { %>
  <H4>Modulo de Cobranzas</H4>
  <table class="tableinfo"> 
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Comisiones por Cobrar : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXDR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXDR() %>">
        <input type="text" name="D02GLMXDR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXDR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Contrapartida : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXCR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCR() %>">
        <input type="text" name="D02GLMXCR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Retenciones de Impuestos : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX16" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX16() %>">
        <input type="text" name="D02GLMX16" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX16() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Otras Retenciones : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX17" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX17() %>">
        <input type="text" name="D02GLMX17" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX17() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Intereses de Terceros : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXST" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXST() %>">
        <input type="text" name="D02GLMXST" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXST() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Gastos del Corresponsal : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXIE" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXIE() %>">
        <input type="text" name="D02GLMXIE" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXIE() %>">
      </td>
    </tr>
  </table>
  <% } %>
  
  <% if (crossRef.getE02GLMACD().equals("40") || crossRef.getE02GLMACD().equals("41") 
  		|| crossRef.getE02GLMACD().equals("42") || crossRef.getE02GLMACD().equals("43")) { %>
  <H4>Modulo de Cartas de Credito</H4>
  <table class="tableinfo"> 
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Comisiones por Cobrar : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXDR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXDR() %>">
        <input type="text" name="D02GLMXDR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXDR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Contrapartida : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXCR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCR() %>">
        <input type="text" name="D02GLMXCR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Debito Aceptaciones: 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSD" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSD() %>">
        <input type="text" name="D02GLMXSD" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSD() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Credito Aceptaciones : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXST" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXST() %>">
        <input type="text" name="D02GLMXST" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXST() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Garantia Efectivo : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSR() %>">
        <input type="text" name="D02GLMXSR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Aceptaciones Descontadas : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSO" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSO() %>">
        <input type="text" name="D02GLMXSO" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSO() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Refinanciamientos (Prestamos) : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSS" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSS() %>">
        <input type="text" name="D02GLMXSS" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSS() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Debito Pagos Diferidos  : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXIE" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXIE() %>">
        <input type="text" name="D02GLMXIE" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXIE() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Credito Pagos Diferidos : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMDFP" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMDFP() %>">
        <input type="text" name="D02GLMDFP" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMDFP() %>">
      </td>
    </tr>
    <tr  id="trdark">
      <TH align="left" colspan=2>Periodificación de Interes  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Debito por Intereses : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX21" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX21() %>">
        <input type="text" name="D02GLMX21" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX21() %>">
      </td>
    </tr>
    
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Credito por Intereses : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX22" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX22() %>">
        <input type="text" name="D02GLMX22" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX22() %>">
      </td>
    </tr>   
  </table>
  <% } %>
  
  <% if (crossRef.getE02GLMACD().equals("01") || crossRef.getE02GLMACD().equals("02") || crossRef.getE02GLMACD().equals("03") || crossRef.getE02GLMACD().equals("04")) { %>
  <H4>Modulo de Depositos a la Vista</H4>
  <table class="tableinfo">
    <tr  id="trdark"> 
      <TH align="left" colspan=2>Creditos </TH>
    </tr> 
    <tr  id="trclear">
      <td align="right" width="30%">Cuenta Credito de Interes : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXCR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCR() %>">
        <input type="text" name="D02GLMXCR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%">Cargos por Servicio Saldo Minimo : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSM" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSM() %>">
        <input type="text" name="D02GLMXSM" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSM() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%">Cargos por Servicio Diferidos/Sobregiro : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSR() %>">
        <input type="text" name="D02GLMXSR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%">Cargos por Servicio Suspension Pagos : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSS" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSS() %>">
        <input type="text" name="D02GLMXSS" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSS() %>">
      </td>
    </tr>
        <tr  id="trclear">
      <td align="right" width="30%">Cargos por Servicio Cuentas Inactivas : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSD" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSD() %>">
        <input type="text" name="D02GLMXSD" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSD() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%">Cargos por Servicio Varios : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXST" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXST() %>">
        <input type="text" name="D02GLMXST" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXST() %>">
      </td>
    </tr>
        <tr  id="trclear">
      <td align="right" width="30%">Interes por Sobregiro : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSO" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSO() %>">
        <input type="text" name="D02GLMXSO" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSO() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%">Garantias (Contingencia) : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXDR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXDR() %>">
        <input type="text" name="D02GLMXDR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXDR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%">Comisiones por Cancelaciones : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXCC" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCC() %>">
        <input type="text" name="D02GLMXCC" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCC() %>">
      </td>
    </tr>
        <tr  id="trclear">
      <td align="right" width="30%">Cheques Certificados : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX17" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX17() %>">
        <input type="text" name="D02GLMX17" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX17() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%">Interes en Suspenso por Sobregiros : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXLC" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLC() %>">
        <input type="text" name="D02GLMXLC" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLC() %>">
      </td>
    </tr>
	<TR>
		<TD align="right" width="30%">Cargo de Timbres por Cheque :</TD>
		<TD align="left" width="60%" nowrap>
			<input type="text" name="E02GLMX28" size="17" maxlength="16" readonly value="<%= crossRef.getE02GLMX28() %>"> 
			<input type="text" name="D02GLMX28" size="60" maxlength="60" readonly value="<%= crossRef.getD02GLMX28() %>">
		</TD>
	</TR>
	<tr  id="trdark">
      <TH align="left" colspan=2>Debitos </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta por Contra Sobregiro Garantizado : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXLR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLR() %>">
        <input type="text" name="D02GLMXLR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta por Contra Sobregiro No Garantizado : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXOD" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXOD() %>">
        <input type="text" name="D02GLMXOD" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXOD() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta por Contra Sobregiro Mayor 66 Dias : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXO2" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXO2() %>">
        <input type="text" name="D02GLMXO2" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXO2() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Gastos Intereses (MMK,SAV,NOW) : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXIE" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXIE() %>">
        <input type="text" name="D02GLMXIE" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXIE() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Garantias (Contingencia) : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMDFP" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMDFP() %>">
        <input type="text" name="D02GLMDFP" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMDFP() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Interes de Sobregiro a Cobrar : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX16" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX16() %>">
        <input type="text" name="D02GLMX16" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX16() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Por Contra Cuentas Inactivas : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX19" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX19() %>">
        <input type="text" name="D02GLMX19" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX19() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Por Contra Cuentas Dormidas : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX20" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX20() %>">
        <input type="text" name="D02GLMX20" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX20() %>">
      </td>
    </tr>
	    <tr  id="trclear">
	      <td align="right" width="30%" nowrap>Gastos por Bonificaciones : </td>
	      <td align="left" width="60%" nowrap> 
	        <input type="text" name="E02GLXG01" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG01() %>">
	        <input type="text" name="D02GLXG01" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG01() %>">
	      </td>
	    </tr>
    <tr  id="trdark">
      <TH align="left" colspan=2>Intereses de Sobregiros  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Contingencias Debito : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX26" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX26() %>">
        <input type="text" name="D02GLMX26" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX26() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Contingencias Credito : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX27" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX27() %>">
        <input type="text" name="D02GLMX27" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX27() %>">
      </td>
    </tr>
    <tr  id="trdark">
      <TH align="left" colspan=2>Otras Entradas  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Diferidos a 24 Horas : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX21" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX21() %>">
        <input type="text" name="D02GLMX21" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX21() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Diferidos a 48 Horas : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX22" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX22() %>">
        <input type="text" name="D02GLMX22" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX22() %>">
      </td>
    </tr>    
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Diferidos a 72 Horas : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX23" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX23() %>">
        <input type="text" name="D02GLMX23" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX23() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Diferidos a 96 Horas : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX24" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX24() %>">
        <input type="text" name="D02GLMX24" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX24() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Diferidos Mas 96 Horas : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX25" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX25() %>">
        <input type="text" name="D02GLMX25" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX25() %>">
      </td>
    </tr>
  </table>
  <% } %>

  <% if (crossRef.getE02GLMACD().equals("06")) { %>
  <H4>Modulo de Cuotas de Participacion</H4>
   <table class="tableinfo"> 
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta por Capitalizacion : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG01" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG01() %>">
        <input type="text" name="D02GLXG01" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG01() %>">
      </td>
    </tr>
  </table>
  <% } %>

  
  <% if (crossRef.getE02GLMACD().equals("90") || crossRef.getE02GLMACD().equals("91") || crossRef.getE02GLMACD().equals("92")
  		|| crossRef.getE02GLMACD().equals("70")  || crossRef.getE02GLMACD().equals("71")  || crossRef.getE02GLMACD().equals("94")) { %>

  <H4>Modulo de Amortización / Garantias / Lineas de Credito / Otros Productos</H4>
   <table class="tableinfo"> 
    <tr  id="trclear">
      <td align="right" width="2%">(*)</td>
      <td align="right" width="28%"> 
        Cuenta Contarpartida : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXDR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXDR() %>">
        <input type="text" name="D02GLMXDR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXDR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="2%">(**)</td>
      <td align="right" width="28%"> 
        Cuenta Amortización : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXCR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCR() %>">
        <input type="text" name="D02GLMXCR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="2%"></td>
      <td align="right" width="28%"> 
        Cuenta de Debito Monto Usado : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX21" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX21() %>">
        <input type="text" name="D02GLMX21" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX21() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="2%"></td>
      <td align="right" width="28%"> 
        Cuenta de Credito Monto Usado : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX22" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX22() %>">
        <input type="text" name="D02GLMX22" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX22() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="2%"></td>
      <td align="right" colspan="2">         
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" valign="top" width="2%">(*)</td>
      <td align="left" colspan="2">
       Cuenta de Contrapartida sera usada para GARANTIAS, LINEAS DE CREDITO Y OTROS.         
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" valign="top" width="2%">(**)</td>
      <td align="left" colspan="2">
       La Cuenta de Ingresos x Comision o Gastos Prepagados serán utilizadas para la Amortización Diaria;
       Ingresos y Egresos serán afectados a diario de acuerdo a la clasificación de la Cuenta Principal Utilizada.
       (Cuenta Contable Reflejada Arriba)        
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" valign="top" width="2%">(**)</td>
      <td align="left" colspan="2">
       Lineas de Credito usaran la cuenta de Amortización para incluir la cuenta de Ingresos por Comisión.         
      </td>
    </tr>
  </table>
  <% } %>
  
  <% if (crossRef.getE02GLMACD().equals("11") || crossRef.getE02GLMACD().equals("12") || crossRef.getE02GLMACD().equals("14") || crossRef.getE02GLMACD().equals("15")) { %>
  <H4>Modulo de Contratos</H4>
  <table class="tableinfo">
    <tr  id="trdark">
      <TH align="left" colspan=2>Periodificación de Interes  </TH>
    </tr> 
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Debito por Interes Normal : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXDR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXDR() %>">
        <input type="text" name="D02GLMXDR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXDR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito por Interes Normal : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXCR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCR() %>">
        <input type="text" name="D02GLMXCR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Debito por Interes de Mora : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXLR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLR() %>">
        <input type="text" name="D02GLMXLR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito por Interes de Mora : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXLC" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLC() %>">
        <input type="text" name="D02GLMXLC" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLC() %>">
      </td>
    </tr>
        <tr  id="trdark">
      <TH align="left" colspan=2>Entradas Garantias  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Debito Contingencia : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXIE" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXIE() %>">
        <input type="text" name="D02GLMXIE" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXIE() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Credito Contingencia : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMDFP" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMDFP() %>">
        <input type="text" name="D02GLMDFP" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMDFP() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Debito Monto Usado : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX21" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX21() %>">
        <input type="text" name="D02GLMX21" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX21() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Credito Monto Usado : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX22" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX22() %>">
        <input type="text" name="D02GLMX22" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX22() %>">
      </td>
    </tr>
    <tr  id="trdark">
      <TH align="left" colspan=2>Referencias Adicionales  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%">Operación dada en Garantia : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSO" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSO() %>">
        <input type="text" name="D02GLMXSO" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSO() %>">
      </td>
    </tr>
       <tr  id="trclear">
      <td align="right" width="30%">Cuenta Origen del Contrato : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSR() %>">
        <input type="text" name="D02GLMXSR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSR() %>">
      </td>
    </tr>
        <tr  id="trclear">
      <td align="right" width="30%">Cuenta Contratos Vencidos : </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSM" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSM() %>">
        <input type="text" name="D02GLMXSM" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSM() %>">
      </td>
    </tr>
     
  </table>
  <% } %>
  
  <% if (crossRef.getE02GLMACD().equals("13")) { %>
  <H4>Modulo de Inversiones (Papel Comercial)</H4>
  <table class="tableinfo">
    <tr  id="trdark">
      <TH align="left" colspan=2>Periodificación de Interes  </TH>
    </tr> 
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Cuenta de Debito por Interes : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXDR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXDR() %>">
        <input type="text" name="D02GLMXDR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXDR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Cuenta de Credito por Interes  : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXCR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCR() %>">
        <input type="text" name="D02GLMXCR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCR() %>">
      </td>
    </tr>
    <tr  id="trdark">
      <TH align="left" colspan=2>Custodio (Contingencias)  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Cuenta de Debito : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXIE" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXIE() %>">
        <input type="text" name="D02GLMXIE" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXIE() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Cuenta de Credito : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMDFP" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMDFP() %>">
        <input type="text" name="D02GLMDFP" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMDFP() %>">
      </td>
    </tr>
    <tr  id="trdark">
      <TH align="left" colspan=2>Otras Entradas  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Amortización de Prima : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXSD" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSD() %>">
        <input type="text" name="D02GLMXSD" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSD() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Amortización de Descuento : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXSO" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSO() %>">
        <input type="text" name="D02GLMXSO" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSO() %>">
      </td>
    </tr>    
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Ajuste de Precio de Mercado : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXSM" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSM() %>">
        <input type="text" name="D02GLMXSM" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSM() %>"> 
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%" nowrap> 
        Contrapartida Ajuste por Ganancias : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXSR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSR() %>">
        <input type="text" name="D02GLMXSR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%" nowrap> 
        Contrapartida Ajuste por Perdidas : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXCC" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCC() %>">
        <input type="text" name="D02GLMXCC" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCC() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Ganancias por Ventas : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXSS" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSS() %>">
        <input type="text" name="D02GLMXSS" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSS() %>">
      </td>
    </tr>    
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Perdidas por Ventas : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXLC" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLC() %>">
        <input type="text" name="D02GLMXLC" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLC() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Cuenta de Inversiones Vencidas : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMX16" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX16() %>">
        <input type="text" name="D02GLMX16" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX16() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Cuenta de Ganancias Realizadas : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXO2" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXO2() %>">
        <input type="text" name="D02GLMXO2" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXO2() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="21%"> 
        Cuenta de Perdidas Realizadas : 
      </td>
      <td align="left" nowrap width="79%"> 
        <input type="text" name="E02GLMXLR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLR() %>">
        <input type="text" name="D02GLMXLR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLR() %>">
      </td>
    </tr>
  </table>
  <% } %>
  
  <% if (crossRef.getE02GLMACD().equals("19")) { %>
  <H4>Modulo de Proyectos de Constructor</H4>
  <table class="tableinfo">
    <tr  id="trdark">
      <TH align="left" colspan=2>Solicitud  </TH>
    </tr>     
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito por Solicitud Recibida : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXCR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCR() %>">
        <input type="text" name="D02GLMXCR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCR() %>">
      </td>
    </tr>
    
    <tr  id="trdark">
      <TH align="left" colspan=2>Aprobación Solicitud  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Debito Solicitud Aprobada : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSM" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSM() %>">
        <input type="text" name="D02GLMXSM" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSM() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito Solicitud Aprobada : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSR() %>">
        <input type="text" name="D02GLMXSR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSR() %>">
      </td>
    </tr>    
    <tr  id="trdark">
      <TH align="left" colspan=2>Protocolización  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Debito Protocolizado por Utilizar : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSS" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSS() %>">
        <input type="text" name="D02GLMXSS" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSS() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito Protocolizado por Utilizar : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSD" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSD() %>">
        <input type="text" name="D02GLMXSD" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSD() %>">
      </td>
    </tr>
    <tr  id="trdark">
      <TH align="left" colspan=2>Referencias Adicionales  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Contribución : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXST" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXST() %>">
        <input type="text" name="D02GLMXST" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXST() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Anticipos : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXSO" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSO() %>">
        <input type="text" name="D02GLMXSO" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSO() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Prima Fondo de Garantia : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXDR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXDR() %>">
        <input type="text" name="D02GLMXDR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXDR() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Retención Fiel Cumplimiento : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXCC" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCC() %>">
        <input type="text" name="D02GLMXCC" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCC() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Pago de Valuaciones : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX16" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX16() %>">
        <input type="text" name="D02GLMX16" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX16() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Comisiones y Otros : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXOD" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXOD() %>">
        <input type="text" name="D02GLMXOD" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXOD() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Honorarios Inspector  : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXO2" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXO2() %>">
        <input type="text" name="D02GLMXO2" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXO2() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Retención Fiel Cumplimiento Inspector : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXIE" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXIE() %>">
        <input type="text" name="D02GLMXIE" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXIE() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Contrapartida Adicional : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX17" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX17() %>">
        <input type="text" name="D02GLMX17" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX17() %>">
      </td>
    </tr>
  </table>
  <% } %>
  
  <% if (crossRef.getE02GLMACD().equals("10") || crossRef.getE02GLMACD().equals("11") || crossRef.getE02GLMACD().equals("12") || crossRef.getE02GLMACD().equals("13") || crossRef.getE02GLMACD().equals("14") || crossRef.getE02GLMACD().equals("15")) { %>
  <H4>Referencias Cruzadas sobre Contratos</H4>
  <table class="tableinfo">
    <tr  id="trdark">
      <TH align="left" colspan=2>Contratos a Futuro  </TH>
    </tr> 
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Debito Contingencia : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXST" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXST() %>">
        <input type="text" name="D02GLMXST" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXST() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito Contingencia : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMXOD" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXOD() %>">
        <input type="text" name="D02GLMXOD" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXOD() %>">
      </td>
    </tr>
    <tr  id="trdark">
      <TH align="left" colspan=2>Proyeccion Intereses  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Intereses Debito : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX23" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX23() %>">
        <input type="text" name="D02GLMX23" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX23() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Intereses Creditos : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX24" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX24() %>">
        <input type="text" name="D02GLMX24" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX24() %>">
      </td>
    </tr>
    <tr  id="trdark">
      <TH align="left" colspan=2>Referencias Adicionales Contratos </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Debito Custodia Magnetica : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX26" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX26() %>">
        <input type="text" name="D02GLMX26" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX26() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito Custodia Magnetica : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX27" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX27() %>">
        <input type="text" name="D02GLMX27" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX27() %>">
      </td>
    </tr>
    <%if(currUser.getE01INT().equals("03")){%> 
     <tr  id="trdark">
      <TH align="left" colspan=2>Politica Habitacional (Cuentas de Orden)  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Contrapartida Principal : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG01" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG01() %>">
        <input type="text" name="D02GLXG01" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG01() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta Contable Fideicomiso: 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG02" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG02() %>">
        <input type="text" name="D02GLXG02" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG02() %>">
      </td>
    </tr>
    
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Disponibilidad : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG03" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG03() %>">
        <input type="text" name="D02GLXG03" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG03() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%">Aporte FAOV por Pagar : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG04" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG04() %>">
        <input type="text" name="D02GLXG04" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG04() %>">
      </td>
    </tr>   
     <tr  id="trclear">
      <td align="right" width="30%">Contrapartida por Seguros : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG05" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG05() %>">
        <input type="text" name="D02GLXG05" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG05() %>">
      </td>
    </tr>     
    <tr  id="trclear">
      <td align="right" width="30%"> 
         : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG06" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG06() %>">
        <input type="text" name="D02GLXG06" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG06() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Intermediación Financiera : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG07" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG07() %>">
        <input type="text" name="D02GLXG07" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG07() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Ingresos por Intermediación : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG08" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG08() %>">
        <input type="text" name="D02GLXG08" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG08() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Obligaciones con BANAVIH : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG09" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG09() %>">
        <input type="text" name="D02GLXG09" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG09() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Gastos   BANAVIH : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG10" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG10() %>">
        <input type="text" name="D02GLXG10" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG10() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Rendimientos no Cobrados : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG11" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG11() %>">
        <input type="text" name="D02GLXG11" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG11() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Costos Operativos BANAVIH : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG12" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG12() %>">
        <input type="text" name="D02GLXG12" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG12() %>">
      </td>
    </tr>
     <tr  id="trclear">
      <td align="right" width="30%"> 
        Fideicomiso de Inversion : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG13" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG13() %>">
        <input type="text" name="D02GLXG13" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG13() %>">
      </td>
    </tr>
  <% } %>
  </table>
  <% } %>
  
  <% if (gLedger.getE01GLMPRV().equals("Y")) { %>
  <H4>Cuentas para Previsiones</H4>
  <table class="tableinfo">
    <tr  id="trdark">
      <TH align="left" colspan=2>Año Actual </TH>
    </tr> 
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Debito Principal : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG41" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG41() %>">
        <input type="text" name="D02GLXG41" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG41() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito Principal : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX31" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX31() %>">
        <input type="text" name="D02GLMX31" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX31() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Debito Intereses : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLXG42" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLXG42() %>">
        <input type="text" name="D02GLXG42" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLXG42() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito Intereses : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX20" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX20() %>">
        <input type="text" name="D02GLMX20" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX20() %>">
      </td>
    </tr>
    <tr  id="trdark">
      <TH align="left" colspan=2>Año Pasado  </TH>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Debito Principal : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX28" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX28() %>">
        <input type="text" name="D02GLMX28" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX28() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito Principal : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMX29" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX29() %>">
        <input type="text" name="D02GLMX29" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX29() %>">
      </td>
    </tr>
  </table>
  <% } %>
  
  <% if (gLedger.getE01GLMRVF().equals("Y")) { %>
  <H4>Cuentas de Revaluación Moneda Extranjera</H4>
  <table class="tableinfo"> 
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta de Ingreso : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMRIN" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMRIN() %>">
        <input type="text" name="D02GLMRIN" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMRIN() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta de Egreso : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMREX" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMREX() %>">
        <input type="text" name="D02GLMREX" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMREX() %>">
      </td>
    </tr>
   </table>
  <% } %>
  
  <% if (gLedger.getE01GLMRVF().equals("4")) { %>
  <H4>Cuentas de Diferencial Cambiario</H4>
  <table class="tableinfo"> 
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Cuenta de Diferencial Cambiario : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMRIN" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMRIN() %>">
        <input type="text" name="D02GLMRIN" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMRIN() %>">
      </td>
    </tr>
   </table>
  <% } %>
  <% if (crossRef.getE02GLMATY().equals("BND") ||
				 crossRef.getE02GLMATY().equals("EQT") ||
				 crossRef.getE02GLMATY().equals("MUT") ||
				 crossRef.getE02GLMATY().equals("ACD") ||				 
				 crossRef.getE02GLMATY().equals("PFS")) { %> 
  <h4>Cuentas de Inversiones </h4>
  <table class="tableinfo">
    <tr  id="trdark"> 
      <td align="right" width="30%"> 
        <div align="right">Cuenta Compensacion :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMX32" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX32() %>">
          <input type="text" name="D02GLMX32" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX32() %>">
        </div>
      </td>
    </tr>
    <tr  id="trclear"> 
      <td align="right" width="30%"> 
        <div align="right">Ganancias/Perdidas    Ingresos : </div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMXCR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCR() %>">
          <input type="text" name="D02GLMXCR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCR() %>">
        </div>
      </td>
    </tr>
    <tr  id="trdark"> 
      <td align="right" width="30%"> 
        <div align="right">Ganancias/Perdidas    Gastos : </div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMXSM" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSM() %>">
          <input type="text" name="D02GLMXSM" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSM() %>">
        </div>
      </td>
    </tr>
    <tr  id="trclear"> 
      <td align="right" width="30%"> 
        <div align="right">Debito Interes Devengos :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMXSS" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSS() %>">
          <input type="text" name="D02GLMXSS" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSS() %>">
        </div>
      </td>
    </tr>
    <tr  id="trdark"> 
      <td align="right" width="30%"> 
        <div align="right">Credito Interes Devengos  :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMXSD" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSD() %>">
          <input type="text" name="D02GLMXSD" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSD() %>">
        </div>
      </td>
    </tr>
    <tr  id="trclear"> 
      <td align="right" width="30%"> 
        <div align="right">Debito Ordenes de Venta Pendientes :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMXST" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXST() %>">
          <input type="text" name="D02GLMXST" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXST() %>">
        </div>
      </td>
    </tr>
    <tr  id="trdark"> 
      <td align="right" width="30%"> 
        <div align="right">Credito Ordenes de Venta Pendientes :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMXSO" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSO() %>">
          <input type="text" name="D02GLMXSO" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSO() %>">
        </div>
      </td>
    </tr>
    <tr  id="trclear"> 
      <td align="right" width="30%"> 
        <div align="right">Debito Ordenes de Compra Pendientes :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMXLR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXLR() %>">
          <input type="text" name="D02GLMXLR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXLR() %>">
        </div>
      </td>
    </tr>
    <tr  id="trdark"> 
      <td align="right" width="30%"> 
        <div align="right">Credito Ordenes de Compra Pendientes :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMXO2" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXO2() %>">
          <input type="text" name="D02GLMXO2" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXO2() %>">
        </div>
      </td>
    </tr>
    
    <tr  id="trclear"> 
      <td align="right" width="30%"> 
        <div align="right">Comision acumulada de custodia Debito :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMX17" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX17() %>">
          <input type="text" name="D02GLMX17" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX17() %>">
        </div>
      </td>
    </tr>
    <tr  id="trdark"> 
      <td align="right" width="30%"> 
        <div align="right">Comision acumulada de custodia Credito :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMX18" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMX18() %>">
          <input type="text" name="D02GLMX18" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMX18() %>">
        </div>
      </td>
    </tr>
    <tr  id="trclear"> 
      <td align="right" width="30%"> 
        <div align="right">Cuenta Comision del Comisionista :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMXSR" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXSR() %>">
          <input type="text" name="D02GLMXSR" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXSR() %>">
        </div>
      </td>
    </tr>
    <tr  id="trdark"> 
      <td align="right" width="30%"> 
        <div align="right">Cuenta Gastos del Comisionista  :</div>
      </td>
      <td align="left" width="60%" nowrap> 
        <div align="left"> 
          <input type="text" name="E02GLMXCC" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMXCC() %>">
          <input type="text"  name="D02GLMXCC" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMXCC() %>">
        </div>
      </td>
    </tr>
    
  </table>
  
<H4>Posicion Banco</H4>
<TABLE class="tableinfo">
	<TBODY>
		<TR id="trdark">
			<TD align="right" width="30%">
			<DIV align="right">Cuenta Face value :</DIV>
			</TD>
			<TD align="left" width="60%" nowrap>
			<DIV align="left">
				<INPUT type="text" name="E02GLMDFP" size="17" maxlength="16" readonly value="<%= crossRef.getE02GLMDFP() %>">
				<INPUT type="text" name="D02GLMDFP0" size="60" maxlength="60" readonly value="<%= crossRef.getD02GLMDFP() %>">
			</DIV>
			</TD>
		</TR>
		<TR id="trclear">
			<TD align="right" width="30%">
			<DIV align="right">Cuenta Prima/Descuento :</DIV>
			</TD>
			<TD align="left" width="60%" nowrap>
			<DIV align="left">
				<INPUT type="text" name="E02GLMX16" size="17" maxlength="16" readonly value="<%= crossRef.getE02GLMX16() %>">
				<INPUT type="text" name="D02GLMX160" size="60" maxlength="60" readonly value="<%= crossRef.getD02GLMX16() %>">
			</DIV>
			</TD>
		</TR>
		<TR id="trdark">
			<TD align="right" width="30%">
			<DIV align="right">Devengo de Intereses tiempo de compra :</DIV>
			</TD>
			<TD align="left" width="60%" nowrap>
			<DIV align="left">
				<INPUT type="text" name="E02GLMXIE" size="17" maxlength="16" readonly value="<%= crossRef.getE02GLMXIE() %>">
				<INPUT type="text" name="D02GLMXIE" size="60" maxlength="60" readonly value="<%= crossRef.getD02GLMXIE() %>">
			</DIV>
			</TD>
		</TR>
		<TR id="trclear">
			<TD align="right" width="30%">
			<DIV align="right">Cuenta Debito en poder del Banco :</DIV>
			</TD>
			<TD align="left" width="60%" nowrap>
			<DIV align="left">
				<INPUT type="text" name="E02GLMX19" size="17" maxlength="16" readonly value="<%= crossRef.getE02GLMX19() %>">
				<INPUT type="text" name="D02GLMX19" size="60" maxlength="60" readonly value="<%= crossRef.getD02GLMX19() %>">
			</DIV>
			</TD>			
		</TR>
		<TR id="trdark">
			<TD align="right" width="30%">
			<DIV align="right">Cuenta Credito en poder del Banco:</DIV>
			</TD>
			<TD align="left" width="60%" nowrap>
			<DIV align="left">
				<INPUT type="text" name="E02GLMX20" size="17" maxlength="16" readonly value="<%= crossRef.getE02GLMX20() %>">
				<INPUT type="text" name="D02GLMX20" size="60" maxlength="60" readonly value="<%= crossRef.getD02GLMX20() %>">
			</DIV>
			</TD>		
		</TR>		
	</TBODY>
</TABLE>
<% } %>

 <% if (!gLedger.getE01GLMREV().equals("N") && !crossRef.getE02GLMACD().equals("10")) { %>
  <H4>Cuentas de Reajuste/Corrección Monetaria</H4>
  <table class="tableinfo"> 
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Debito por Reajuste : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMRIN" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMRIN() %>">
        <input type="text" name="D02GLMRIN" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMRIN() %>">
      </td>
    </tr>
    <tr  id="trclear">
      <td align="right" width="30%"> 
        Credito por Reajuste : 
      </td>
      <td align="left" width="60%" nowrap> 
        <input type="text" name="E02GLMREX" size="17" maxlength="16" readonly value = "<%= crossRef.getE02GLMREX() %>">
        <input type="text" name="D02GLMREX" size="60" maxlength="60" readonly value = "<%= crossRef.getD02GLMREX() %>">
      </td>
    </tr>
   </table>
  <% } %>


</form>
</body>
</html>
