<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<html>
<head>
<title>Manteci&oacute;n de Tablas</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="RTTab" class="datapro.eibs.beans.EDD232005Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

function cancel() {
	document.forms[0].SCREEN.value = 4100;
	document.forms[0].submit();
}





</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "NEW".equals(userPO.getPurpose()) ? "" : "readonly";
     
%>


<H3 align="center">
<% if(userPO.getPurpose().equals("NEW")){ %>
Nueva Tabla
<% } else {%>
Mantenci&oacute;n Tabla
<% } %>
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="tabla_maitenance, EDD2320"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2320" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="4600">
  
  <h4>Datos Tabla</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Tabla :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E05CMTCTDC" size="5" maxlength="4" value="<%= RTTab.getE05CMTCTDC().trim()%>" 	 <%=readonly%>>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Descripci&oacute;n  :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E05CMTCGDC" size="31" maxlength="30" value="<%= RTTab.getE05CMTCGDC().trim()%>" >
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Objetivo Tabla :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCTCC" size="2" maxlength="1" value="<%= RTTab.getE05CMTCTCC().trim()%>" readonly>
                <input type="text" name="E05CMTCTCG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCTCG().trim()%>" readonly>
				<a href="javascript:GetCodeDescCNTIN('E05CMTCTCC','E05CMTCTCG','119')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Tipo Tabla :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCITB" size="2" maxlength="1" value="<%= RTTab.getE05CMTCITB().trim()%>" readonly>
                <input type="text" name="E05CMTCITG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCITG().trim()%>" readonly>
				<a href="javascript:GetCodeDescCNTIN('E05CMTCITB','E05CMTCITG','120')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>                
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Tipo Resultado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCTMT" size="2" maxlength="1" value="<%= RTTab.getE05CMTCTMT().trim()%>" >
                <input type="text" name="E05CMTCTMG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCTMG().trim()%>" readonly>
				<a href="javascript:GetCodeDescCNTIN('E05CMTCTMT','E05CMTCTMG','121')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Moneda Tabla :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCMND" size="3" maxlength="3" value="<%= RTTab.getE05CMTCMND().trim()%>" readonly >
                <input type="text" name="E05CMTCMNG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCMNG().trim()%>"  readonly>
				<a href="javascript:GetCurrencyDesc('E05CMTCMND','E05CMTCMNG','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="help" align="absbottom" border="0"></a> 
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Moneda Resulato :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E05CMTCMRT" size="4" maxlength="3" value="<%= RTTab.getE05CMTCMRT().trim()%>" readonly>
                <input type="text" name="E05CMTCMRG" size="31" maxlength="30" value="<%= RTTab.getE05CMTCMRG().trim()%>"  readonly>
				<a href="javascript:GetCurrencyDesc('E05CMTCMRT','E05CMTCMRG','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="help" align="absbottom" border="0"></a> 
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left"> 
               </div>
            </td>
          </tr> 
         </table>
      </td>
    </tr>
  </table>

  <div align="center">
    <input id="EIBSBTN" type="submit" name="Enviar" value="Enviar" >
    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel()">
  </div>
  </form>

</body>
</html>
