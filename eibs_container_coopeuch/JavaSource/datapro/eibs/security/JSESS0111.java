package datapro.eibs.security;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.ezmorph.Morpher;
import net.sf.ezmorph.MorpherRegistry;
import net.sf.ezmorph.bean.BeanMorpher;
import net.sf.json.JSONObject;
import net.sf.json.util.JSONUtils;

import com.datapro.eibs.access.VOSuper;
import com.datapro.eibs.exception.FacadeException;
import com.datapro.eibs.facade.FASecurity;
import com.datapro.eibs.security.vo.WEBCOViewTreeByUser;
import com.datapro.eibs.security.vo.WEBMGR;
import com.datapro.eibs.security.vo.WEBMO;
import com.datapro.eibs.security.vo.WEBMUS;
import com.datapro.eibs.security.vo.WEBOG;
import com.datapro.eibs.security.vo.WEBOU;
import com.datapro.eibs.setup.vo.CNTRLCNTExtRunDate;
import com.datapro.exception.ServiceLocatorException;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.services.ServiceLocator;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.Node;
import datapro.eibs.beans.Tree;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 * 
 */
public class JSESS0111 extends JSEIBSServlet {

	protected static final int ENTER = 0;
	protected static final int LIST_MENU_USER = 1;
	protected static final int UPDATE_MENU_USER = 2;
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case ENTER:
				forward("ESS0111_user_options.jsp", req, res);
				break;
			case LIST_MENU_USER:
				listMenuUser(user, req, res, session);
				break;
			// END Entering
			case UPDATE_MENU_USER:
				updateMenuUser(user, req, res, session);
				break;
			// END Entering
			default:
				forward(devPage, req, res);
				break;
		}
	}

	private void updateMenuUser(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		// Recovering Data to update
		String typ = req.getParameter("typ") == null ? "U" : req.getParameter("typ");

		//Rescata estado de arbol inicial.
		String jsonDataIni = req.getParameter("dataIni") == null ? "" : req.getParameter("dataIni");
		//Rescata estado de ultimo arbol presentado.
		if(jsonDataIni.equals(""))
			jsonDataIni = session.getAttribute("dataIni").toString();
			
		String jsonData = req.getParameter("data");
		
		//Guarda estado de arbol con ultima modificacion.
		session.setAttribute("dataIni",jsonData);
		
		FASecurity facade = new FASecurity();
		facade.setSessionUser(user);
		
		Connection cnx = null;	
		
		try {
			CNTRLCNTExtRunDate rundate = facade.getRunDate();

			JSONObject jsonObject = JSONObject.fromObject(jsonData);

			Map classMap = new HashMap();
			classMap.put("nodes", Node.class);

			Tree tree = (Tree) JSONObject.toBean(jsonObject, Tree.class, classMap);

			// post process
			MorpherRegistry morpherRegistry = JSONUtils.getMorpherRegistry();
			Morpher dynaMorpher = new BeanMorpher(Node.class, morpherRegistry);
			morpherRegistry.registerMorpher(dynaMorpher);
			
			List output = new ArrayList();
			
			for (Iterator i = tree.getNodes().iterator(); i.hasNext();) {
				output.add(morpherRegistry.morph(Node.class, i.next()));
			}
			BeanList options = new BeanList();
			tree.setNodes((ArrayList) output);
			ArrayList menus = tree.getNodes();

			
			//Ultimo arbol sin cambios.
			JSONObject jsonObjectIni = JSONObject.fromObject(jsonDataIni);

			classMap = new HashMap();
			classMap.put("nodes", Node.class);

			Tree treeIni = (Tree) JSONObject.toBean(jsonObjectIni, Tree.class, classMap);

			morpherRegistry = JSONUtils.getMorpherRegistry();
			dynaMorpher = new BeanMorpher(Node.class, morpherRegistry);
			morpherRegistry.registerMorpher(dynaMorpher);

			List outputIni = new ArrayList();
			
			for (Iterator i = treeIni.getNodes().iterator(); i.hasNext();) {
				outputIni.add(morpherRegistry.morph(Node.class, i.next()));
			}
			BeanList optionsIni = new BeanList();
			treeIni.setNodes((ArrayList) outputIni);
			ArrayList menusIni = treeIni.getNodes();
			//
			
			VOSuper vo = null;
			if ("G".equals(typ)) {
				vo = new WEBMGR();
			} else {
				vo = new WEBMUS();
			}
			options.addRow(vo);
			
	
			//INSERTAR NUEVOS Y GENERACION DE LOG
			String AccionM="";
			String AccionSM="";
			

			options.init();
			Iterator iterator = menus.iterator();
			while (iterator.hasNext()) 
			{
				Node menu = (Node) iterator.next();
				Iterator it = menu.getNodes().iterator();
				
				AccionM="INS";
				while (it.hasNext()) 
				{
					Node submenu = (Node) it.next();
				
					// Recorre menu original
					optionsIni.init();
					Iterator iteratorIni = menusIni.iterator();
					while (iteratorIni.hasNext()) 
					{

						
						Node menuIni = (Node) iteratorIni.next();
						Iterator itIni = menuIni.getNodes().iterator();
						if(menu.id.equals(menuIni.id))
						{
							AccionM="";
							AccionSM="INS";
							while (itIni.hasNext()) 
							{
								Node submenuIni = (Node) itIni.next();
								if(submenu.id.equals(submenuIni.id)) 
								{			
									AccionSM = "";
									break;
								}
							}
							if(AccionSM.equals("INS"))
							{
								if ("G".equals(typ)) 
								{
									vo = new WEBOG();
									((WEBOG)vo).setOGRUSR(tree.getId());
									((WEBOG)vo).setOGRSID(menu.getId());
									((WEBOG)vo).setOGRDEN(submenu.value);
									((WEBOG)vo).setOGRLIF(user.getE01LAN().toUpperCase());
									((WEBOG)vo).setOGRSEQ("0");
									((WEBOG)vo).setOGRLMM(rundate.getCNTRDM());
									((WEBOG)vo).setOGRLMD(rundate.getCNTRDD());
									((WEBOG)vo).setOGRLMY(rundate.getCNTRDY());
										
									InDelWEBOG(user, ((WEBOG)vo), "I");
									
								} else {
									vo = new WEBOU();
									((WEBOU)vo).setOUSUSR(tree.getId());
									((WEBOU)vo).setOUSSID(menu.getId());
									((WEBOU)vo).setOUSDEN(submenu.value);
									((WEBOU)vo).setOUSLIF(user.getE01LAN().toUpperCase());
									((WEBOU)vo).setOUSSEQ("0");
									((WEBOU)vo).setOUSLMM(rundate.getCNTRDM());
									((WEBOU)vo).setOUSLMD(rundate.getCNTRDD());
									((WEBOU)vo).setOUSLMY(rundate.getCNTRDY());

									InDelWEBOU(user, ((WEBOU)vo), "I");
									
								}
							}
						}
					}
					if(AccionM.equals("INS"))
					{
						it = menu.getNodes().iterator();
						while (it.hasNext()) 
						{
							submenu = (Node) it.next();						
							if ("G".equals(typ)) 
							{
								vo = new WEBOG();
								((WEBOG)vo).setOGRUSR(tree.getId());
								((WEBOG)vo).setOGRSID(menu.getId());
								((WEBOG)vo).setOGRDEN(submenu.value);
								((WEBOG)vo).setOGRLIF(user.getE01LAN().toUpperCase());
								((WEBOG)vo).setOGRSEQ("0");
								((WEBOG)vo).setOGRLMM(rundate.getCNTRDM());
								((WEBOG)vo).setOGRLMD(rundate.getCNTRDD());
								((WEBOG)vo).setOGRLMY(rundate.getCNTRDY());
									
								InDelWEBOG(user, ((WEBOG)vo), "I");
							} else {
								vo = new WEBOU();
								((WEBOU)vo).setOUSUSR(tree.getId());
								((WEBOU)vo).setOUSSID(menu.getId());
								((WEBOU)vo).setOUSDEN(submenu.value);
								((WEBOU)vo).setOUSLIF(user.getE01LAN().toUpperCase());
								((WEBOU)vo).setOUSSEQ("0");
								((WEBOU)vo).setOUSLMM(rundate.getCNTRDM());
								((WEBOU)vo).setOUSLMD(rundate.getCNTRDD());
								((WEBOU)vo).setOUSLMY(rundate.getCNTRDY());

								InDelWEBOU(user, ((WEBOU)vo), "I");
							}
						}							
					}		
				}
			}	
			//
			
			//BORRA ELIMINADOSY GENERACION DE LOG
			AccionM="";
			AccionSM="";

			optionsIni.init();
			Iterator iteratorIni = menusIni.iterator();
			while (iteratorIni.hasNext()) 
			{
				Node menuIni = (Node) iteratorIni.next();
				Iterator itIni = menuIni.getNodes().iterator();
				
				AccionM="DEL";
				while (itIni.hasNext()) 
				{
					Node submenuIni = (Node) itIni.next();
				
					options.init();
					iterator = menus.iterator();
					while (iterator.hasNext()) 
					{
						Node submenu = null;
						
						Node menu = (Node) iterator.next();
						Iterator it= menu.getNodes().iterator();
						if(menu.id.equals(menuIni.id))
						{
							AccionM="";
							AccionSM="DEL";
							while (it.hasNext()) 
							{
								submenu = (Node) it.next();
								if(submenu.id.equals(submenuIni.id)) 
								{			
									AccionSM = "";
									break;
								}
							}
							if(AccionSM.equals("DEL"))
							{	
								if ("G".equals(typ)) 
								{
									vo = new WEBOG();
									((WEBOG)vo).setOGRUSR(treeIni.getId());
									((WEBOG)vo).setOGRSID(menuIni.getId());
									((WEBOG)vo).setOGRDEN(submenuIni.value);
									((WEBOG)vo).setOGRLIF(user.getE01LAN().toUpperCase());
									((WEBOG)vo).setOGRSEQ("0");
									((WEBOG)vo).setOGRLMM(rundate.getCNTRDM());
									((WEBOG)vo).setOGRLMD(rundate.getCNTRDD());
									((WEBOG)vo).setOGRLMY(rundate.getCNTRDY());
										
									InDelWEBOG(user, ((WEBOG)vo), "E");
								} else {
									vo = new WEBOU();
									((WEBOU)vo).setOUSUSR(treeIni.getId());
									((WEBOU)vo).setOUSSID(menuIni.getId());
									((WEBOU)vo).setOUSDEN(submenuIni.value);
									((WEBOU)vo).setOUSLIF(user.getE01LAN().toUpperCase());
									((WEBOU)vo).setOUSSEQ("0");
									((WEBOU)vo).setOUSLMM(rundate.getCNTRDM());
									((WEBOU)vo).setOUSLMD(rundate.getCNTRDD());
									((WEBOU)vo).setOUSLMY(rundate.getCNTRDY());

									InDelWEBOU(user, ((WEBOU)vo), "E");
								}
							}	
						}
					}
					if(AccionM.equals("DEL"))
					{
						itIni = menuIni.getNodes().iterator();
						while (itIni.hasNext()) 
						{
							submenuIni = (Node) itIni.next();						
							if ("G".equals(typ)) 
							{
								vo = new WEBOG();
								((WEBOG)vo).setOGRUSR(treeIni.getId());
								((WEBOG)vo).setOGRSID(menuIni.getId());
								((WEBOG)vo).setOGRDEN(submenuIni.value);
								((WEBOG)vo).setOGRLIF(user.getE01LAN().toUpperCase());
								((WEBOG)vo).setOGRSEQ("0");
								((WEBOG)vo).setOGRLMM(rundate.getCNTRDM());
								((WEBOG)vo).setOGRLMD(rundate.getCNTRDD());
								((WEBOG)vo).setOGRLMY(rundate.getCNTRDY());
									
								InDelWEBOG(user, ((WEBOG)vo), "E");
							} else {
								vo = new WEBOU();
								((WEBOU)vo).setOUSUSR(treeIni.getId());
								((WEBOU)vo).setOUSSID(menuIni.getId());
								((WEBOU)vo).setOUSDEN(submenuIni.value);
								((WEBOU)vo).setOUSLIF(user.getE01LAN().toUpperCase());
								((WEBOU)vo).setOUSSEQ("0");
								((WEBOU)vo).setOUSLMM(rundate.getCNTRDM());
								((WEBOU)vo).setOUSLMD(rundate.getCNTRDD());
								((WEBOU)vo).setOUSLMY(rundate.getCNTRDY());

								InDelWEBOU(user, ((WEBOU)vo), "E");
							}
						}							
					}		
				}
			}	
		//	

		res.getWriter().println("Transaccion Exitosa");
			
		} catch (FacadeException e) {
			throw new ServletException(e.getMessage());
		}
	}

	private void listMenuUser(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException {
		
		String usr = req.getParameter("usr") == null ? "" : req.getParameter("usr");
		String typ = req.getParameter("typ") == null ? "U" : req.getParameter("typ");
		
		FASecurity facade = new FASecurity();
		try {
			Tree tree = new Tree();
			tree.setId(usr);
			tree.setReadonly(false);
			
			Node menu = null;
			
			List list = facade.getOptionsMainMenu(user.getE01LAN().toUpperCase());
			List option = facade.getOptionsSubMenu(usr, typ);
			
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				WEBMO mainMenu = (WEBMO) iterator.next();
				menu = new Node();
				menu.setId(mainMenu.getMOIDEN());
				menu.setDesc(mainMenu.getMOENDS().trim());
				menu.setType("menu");
				
				Iterator optiter = option.iterator();
				String subMenuAxu = "";
				while (optiter.hasNext()) {
					
					WEBCOViewTreeByUser subMenu = (WEBCOViewTreeByUser) optiter.next();
					
					if(!subMenu.getCMODEN().equals(subMenuAxu))
					{	
						if (subMenu.getCMOSID().equals(menu.getId())) 
						{
							Node child = new Node();
							child.setId(subMenu.getCMODEN());
							child.setValue(subMenu.getCMODEN());
							child.setDesc(subMenu.getCMONDS().trim());
							child.setSelected(subMenu.getCMOSTS().equals("S"));
							child.setReadonly(subMenu.getCMORWS().equals("RO"));
							child.setType("submenu");
							if (child.isSelected()) {
								menu.setSelected(true);
							}
							if (child.isReadonly()) {
								menu.setReadonly(true);
								if (!tree.isReadonly()) tree.setReadonly(true);
							}
							menu.addNode(child);
					
						}	
					}
					subMenuAxu = subMenu.getCMODEN();
				}
				tree.addNode(menu);
			}
			
			JSONObject jsonObject = JSONObject.fromObject(tree);
			try {
				res.setContentType("application/json");
				res.setCharacterEncoding("UTF-8");
				res.getWriter().println(jsonObject.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			throw new ServletException(e.getMessage());
		}
	}

		protected Connection getConnection() throws ServletException {
		Connection cnx = null;
		boolean isDataSource = false;
		isDataSource = JSEIBSProp.getEODDataSource();
		
		try {
			if (isDataSource) {
				cnx = ServiceLocator.getInstance().getDBConn("eibs-server");
			} else {
				cnx = ServiceLocator.getInstance().newJDBCCon("eibs-server");
			}
			return cnx;
		} catch (ServiceLocatorException e) {
			flexLog("Error: " + e.getClass().getName() + " --> " + e);
			throw new ServletException(e.getMessage());
		}
	}	
	

		
		private void InDelWEBOG(ESS0030DSMessage user, com.datapro.eibs.security.vo.WEBOG vo, String Action ) throws ServletException {
			
			String sql = "";
			Connection cnx = null;
			PreparedStatement ps = null;
			
			try {
				cnx = getConnection();	
				

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
					      "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOG', 'OGRUSR', '" + Action  + "', '" + ((WEBOG)vo).getOGRUSR() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOG)vo).getOGRUSR() + "', '" + ((WEBOG)vo).getOGRSID() + "', '" + ((WEBOG)vo).getOGRDEN() + "')";	
					ps = cnx.prepareStatement(sql);
				    ps.executeUpdate();
					cnx = getConnection();	

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU,  LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOG', 'OGRSID', '" + Action  + "', '" +((WEBOG)vo).getOGRSID() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOG)vo).getOGRUSR() + "', '" + ((WEBOG)vo).getOGRSID() + "', '" + ((WEBOG)vo).getOGRDEN() + "')";	
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOG', 'OGRDEN', '" + Action  + "', '" + ((WEBOG)vo).getOGRDEN() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOG)vo).getOGRUSR() + "', '" + ((WEBOG)vo).getOGRSID() + "', '" + ((WEBOG)vo).getOGRDEN() + "')";	
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOG', 'OGRLIF', '" + Action  + "', '" + ((WEBOG)vo).getOGRLIF() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOG)vo).getOGRUSR() + "', '" + ((WEBOG)vo).getOGRSID() + "', '" + ((WEBOG)vo).getOGRDEN() + "')";	
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOG', 'OGRSEQ', '" + Action  + "', '" + ((WEBOG)vo).getOGRSEQ() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOG)vo).getOGRUSR() + "', '" + ((WEBOG)vo).getOGRSID() + "', '" + ((WEBOG)vo).getOGRDEN() + "')";
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOG', 'OGRLMM', '" + Action  + "', '" + ((WEBOG)vo).getOGRLMM() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOG)vo).getOGRUSR()  + "', '" + ((WEBOG)vo).getOGRSID() + "', '" + ((WEBOG)vo).getOGRDEN() + "')";
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOG', 'OGRLMD', '" + Action  + "', '" + ((WEBOG)vo).getOGRLMD() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOG)vo).getOGRUSR()  + "', '" + ((WEBOG)vo).getOGRSID() + "', '" + ((WEBOG)vo).getOGRDEN() + "')";
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOG', 'OGRLMY', '" + Action  + "', '" + ((WEBOG)vo).getOGRLMY() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOG)vo).getOGRUSR()  + "', '" + ((WEBOG)vo).getOGRSID() + "', '" + ((WEBOG)vo).getOGRDEN() + "')";
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

				if(Action.equals("I"))
				{		
					sql = "INSERT INTO WEBOG (OGRUSR, OGRSID, OGRDEN, OGRLIF, OGRSEQ, OGRLMM, OGRLMD, OGRLMY, OGRLMU) " +
						  "VALUES('" + ((WEBOG)vo).getOGRUSR() + "', '" + ((WEBOG)vo).getOGRSID() + "', '" + ((WEBOG)vo).getOGRDEN() + "', '" + ((WEBOG)vo).getOGRLIF() + "', '" + ((WEBOG)vo).getOGRSEQ() + "', '" + ((WEBOG)vo).getOGRLMM() + "', '" + ((WEBOG)vo).getOGRLMD() + "', '" + ((WEBOG)vo).getOGRLMY() + "', '" + user.getH01USR() + "')";	
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();
				}
				else
				{
					sql = "DELETE WEBOG WHERE OGRUSR='" + ((WEBOG)vo).getOGRUSR() + "' AND OGRSID='"+ ((WEBOG)vo).getOGRSID() +"' AND OGRDEN='"+ ((WEBOG)vo).getOGRDEN() + "'";
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();
				}	
			} catch (SQLException e) {
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			} 

		}	
		
		private void InDelWEBOU(ESS0030DSMessage user, com.datapro.eibs.security.vo.WEBOU vo, String Action ) throws ServletException {
			
			String sql = "";
			Connection cnx = null;
			PreparedStatement ps = null;
			
			try {
				cnx = getConnection();	
				

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
					      "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOU', 'OUSUSR', '" + Action  + "', '" + ((WEBOU)vo).getOUSUSR() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOU)vo).getOUSUSR() + "', '" + ((WEBOU)vo).getOUSSID() + "', '" + ((WEBOU)vo).getOUSDEN() + "')";	
					ps = cnx.prepareStatement(sql);
				    ps.executeUpdate();
					cnx = getConnection();	

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOU', 'OUSSID', '" + Action  + "', '" +((WEBOU)vo).getOUSSID() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOU)vo).getOUSUSR() + "', '" + ((WEBOU)vo).getOUSSID() + "', '" + ((WEBOU)vo).getOUSDEN() + "')";	
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOU', 'OUSDEN', '" + Action  + "', '" + ((WEBOU)vo).getOUSDEN() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOU)vo).getOUSUSR() + "', '" + ((WEBOU)vo).getOUSSID() + "', '" + ((WEBOU)vo).getOUSDEN() + "')";	
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOU', 'OUSLIF', '" + Action  + "', '" + ((WEBOU)vo).getOUSLIF() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOU)vo).getOUSUSR() + "', '" + ((WEBOU)vo).getOUSSID() + "', '" + ((WEBOU)vo).getOUSDEN() + "')";	
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOU', 'OUSSEQ', '" + Action  + "', '" + ((WEBOU)vo).getOUSSEQ() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOU)vo).getOUSUSR() + "', '" + ((WEBOU)vo).getOUSSID() + "', '" + ((WEBOU)vo).getOUSDEN() + "')";	
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOU', 'OUSLMM', '" + Action  + "', '" + ((WEBOU)vo).getOUSLMM() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOU)vo).getOUSUSR()  + "', '" + ((WEBOU)vo).getOUSSID() + "', '" + ((WEBOU)vo).getOUSDEN() + "')";
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOU', 'OUSLMD', '" + Action  + "', '" + ((WEBOU)vo).getOUSLMD() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOU)vo).getOUSUSR()  + "', '" + ((WEBOU)vo).getOUSSID() + "', '" + ((WEBOU)vo).getOUSDEN() + "')";
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

					sql = "INSERT INTO LOGMPAR (LOGPGM, LOGUSE, LOGARC, LOGCAM, LOGTIP, LOGVAC, LOGLMY, LOGLMM, LOGLMD, LOGLMU, LOGKE1, LOGKE2, LOGKE3 ) " +
						  "VALUES('JSESS0111', '" + user.getH01USR() + "', 'WEBOU', 'OUSLMY', '" + Action  + "', '" + ((WEBOU)vo).getOUSLMY() + "', '" + user.getE01RDY() + "', '" + user.getE01RDM() +  "', '" + user.getE01RDD() + "', '"  + user.getH01USR() + "', '" + ((WEBOU)vo).getOUSUSR()  + "', '" + ((WEBOU)vo).getOUSSID() + "', '" + ((WEBOU)vo).getOUSDEN() + "')";
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();

		 		if(Action.equals("I"))
				{		
					sql = "INSERT INTO WEBOU (OUSUSR, OUSSID, OUSDEN, OUSLIF, OUSSEQ, OUSLMM, OUSLMD, OUSLMY, OUSLMU) " +
						  "VALUES('" + ((WEBOU)vo).getOUSUSR() + "', '" + ((WEBOU)vo).getOUSSID() + "', '" + ((WEBOU)vo).getOUSDEN() + "', '" + ((WEBOU)vo).getOUSLIF() + "', '" + ((WEBOU)vo).getOUSSEQ() + "', '" + ((WEBOU)vo).getOUSLMM() + "', '" + ((WEBOU)vo).getOUSLMD() + "', '" + ((WEBOU)vo).getOUSLMY()  + "', '" + user.getH01USR() + "')";
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();
				}
				else
				{
					sql = "DELETE WEBOU WHERE OUSUSR='" + ((WEBOU)vo).getOUSUSR() + "' AND OUSSID='"+ ((WEBOU)vo).getOUSSID() +"' AND OUSDEN='"+ ((WEBOU)vo).getOUSDEN() + "'";
					ps = cnx.prepareStatement(sql);
					ps.executeUpdate();
				}	
			} catch (SQLException e) {
				e.printStackTrace();
				throw new ServletException(e.getMessage());
			} 

		}	
		
}
