package com.datapro.eibs.security.object;

/**
 * Insert the type's description here.
 * Creation date: (7/19/00 6:55:55 PM)
 * @author: Ramses
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

import com.datapro.sql.manager.*;
import com.datapro.eibs.security.bean.*;
import com.datapro.eibs.security.dao.*;
import com.datapro.eibs.security.facade.*;
import com.datapro.generic.beanutil.*;

import java.sql.Connection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;


public class BranchProfile {

	/**
	 * 
	 */
	public BranchProfile() {
		super();
	}
	/**
	 * 
	 */
	public void destroy() {

	}

	public CNTRLBRN getBranch(String BANK,String BRANCH) {

		CNTRLBRNDao branchDao = new CNTRLBRNDao("AS400");
		CNTRLBRN branchBean = null;
		
		try {
			
			branchBean = branchDao.select(BANK,BRANCH);
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return branchBean;
	}
	
	public JBObjList getBranch(String BANK) {

		CNTRLBRNDao branchDao = new CNTRLBRNDao("AS400");
		JBObjList branchBean = null;
		
		try {
			
			branchBean = branchDao.select(BANK);
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return branchBean;
	}
	
	public JBObjList getBranch() {

		CNTRLBRNDao branchDao = new CNTRLBRNDao("AS400");
		JBObjList branchBean = null;
		
		try {
			
			branchBean = branchDao.select();
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return branchBean;
	}
	
	public JBObjList getBank() {

		CNTRLBRNDao branchDao = new CNTRLBRNDao("AS400");
		JBObjList bankList = null;
		
		try {
			
			bankList = branchDao.selectBank();
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return bankList;
	}
		
}
