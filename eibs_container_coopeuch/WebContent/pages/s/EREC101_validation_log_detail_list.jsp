<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Detalle Consulta de Log de Validaci&oacute;n Interfaz Motor de Pago</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "LogDetlist" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="LogList" class="datapro.eibs.beans.EREC10101Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<script language="JavaScript">
function goAction(op) {
	document.forms[0].submit();
}
</SCRIPT>  

</head>

<BODY>
<h3 align="center">Detalle Consulta de Log de Validaci&oacute;n Interfaz Motor de Pago
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="validation_log_detail_list, EREC101"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.motorpago.JSEREC101" >
    <input type=HIDDEN name="SCREEN" value="300">

  <p> 
  <h4>Datos Log Interfaz</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">ID Interfaz :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="S01REHIDE" size="5" maxlength="4" value="<%= LogList.getS01REHIDE().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Nombre Interfaz :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="S01REHINT" size="51" maxlength="50" value="<%= LogList.getS01REHINT().trim()%>" readonly>
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Fecha Validaci&oacute;n :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01FECVAL" size="11" maxlength="10" value="<% out.print(LogList.getS01REHFCD()+"/"+LogList.getS01REHFCM()+"/"+LogList.getS01REHFCY());  %>" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Estado de Validaci&oacute;n :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="S01REHDES" size="31" maxlength="30" value="<%= LogList.getS01REHDES().trim()%>" >
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Registros Procesados :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="S01REHTRP" size="12" maxlength="11" value="<%= LogList.getS01REHTRP().trim()%>"  class="TXTRIGHT" readonly>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Monto Total Reg. Procesados  :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="S01REHTMP" size="20" maxlength="19" value="<%= Util.formatCCY(LogList.getS01REHTMP().trim())%>"   class="TXTRIGHT" readonly>
              </div>
            </td>
          </tr> 

          <tr id="trclear"> 
            <td height="23"> 
              <div align="right">Registros Erroneos :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="S01REHTRE" size="12" maxlength="11" value="<%= LogList.getS01REHTRE().trim()%>"  class="TXTRIGHT" readonly>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Monto Total Reg. Erroneos :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="S01REHTME" size="20" maxlength="19" value="<%= Util.formatCCY(LogList.getS01REHTME().trim())%>"   class="TXTRIGHT" readonly>
              </div>
            </td>
          </tr> 
          
        </table>
      </td>
    </tr>
  </table>

<%
	if ( LogDetlist.getNoResult() ) {
 %>
  </p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
        </div>
	  </TD>
	</TR>
    </TABLE>
	  
  <table class="tbenter" width=100% align=center height="8%">
    <tr> 
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Volver</b></a></div>
      </td>
  </table>
  <br>	  
	  <%
	  	}
		else 
		{
	  %> 
   <table class="tbenter" width=100% align=center height="8%">
    <tr> 
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Volver</b></a></div>
      </td>
  </table>
  <br>
           
  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trclear"> 
            <th align="left" nowrap width="20%">DESCRIPCI&Oacute;N ERROR</th>
          </tr>
 
           <%
                LogDetlist.initRow();
				boolean firstTime = true;
				String chk = "";
				int a = 0;
        		while (LogDetlist.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	datapro.eibs.beans.EREC10101Message msgLogDetlist = (datapro.eibs.beans.EREC10101Message) LogDetlist.getRecord();
		 %>
          <tr <% if(a==1) {out.print("id=trclear");a=0;} else {out.print("id=trdark");a=1;}%>> 
            <td NOWRAP  align="left" ><%= msgLogDetlist.getS01REDDAT() %></td>
          </tr>
          <%
                }
              %>
          </table>
      </td>
    </tr>
  </table>

  
   

<SCRIPT language="JavaScript">
	showChecked("CURRCODE2");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }

%> 

</body>
</html>
