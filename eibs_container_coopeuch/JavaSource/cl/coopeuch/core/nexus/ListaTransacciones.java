/**
 * ListaTransacciones.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.coopeuch.core.nexus;

public class ListaTransacciones  implements java.io.Serializable {
    private cl.coopeuch.core.nexus.RECORD[] RECORD;

    public ListaTransacciones() {
    }

    public cl.coopeuch.core.nexus.RECORD[] getRECORD() {
        return RECORD;
    }

    public void setRECORD(cl.coopeuch.core.nexus.RECORD[] RECORD) {
        this.RECORD = RECORD;
    }

    public cl.coopeuch.core.nexus.RECORD getRECORD(int i) {
        return RECORD[i];
    }

    public void setRECORD(int i, cl.coopeuch.core.nexus.RECORD value) {
        this.RECORD[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ListaTransacciones history = (ListaTransacciones) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ListaTransacciones other = (ListaTransacciones) obj;
        boolean _equals;
        _equals = true
            && ((this.RECORD==null && other.getRECORD()==null) || 
             (this.RECORD!=null &&
              java.util.Arrays.equals(this.RECORD, other.getRECORD())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ListaTransacciones history = (ListaTransacciones) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getRECORD() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRECORD());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRECORD(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
