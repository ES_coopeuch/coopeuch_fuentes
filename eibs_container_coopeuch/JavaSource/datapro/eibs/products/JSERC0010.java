// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 12-02-2015 16:49:28
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   JSERC0010.java

package datapro.eibs.products;

import com.datapro.generics.Util;
import datapro.eibs.beans.*;
import datapro.eibs.master.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class JSERC0010 extends JSEIBSServlet {

	public JSERC0010() {
	}

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case 100: // 'd'
			procReqBanck(user, req, res, session);
			break;

		case 101: // 'e'
			procActionBancosList(user, req, res, session);
			break;

		case 201:
			procSeleccionMov(user, req, res, session, "MAINTENANCE");
			break;

		case 203:
			procSeleccionMov(user, req, res, session, "INQUIRY");
			break;

		case 200:
			procSeleccionMov(user, req, res, session, "NEW");
			break;

		case 202:
			procActionDelete(user, req, res, session);
			break;

		case 800:
			procActionNEW(user, req, res, session);
			break;

		case 700:
			procActionUpdate(user, req, res, session);
			// fall through

		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	protected void procReqBanck(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {
		UserPos userPO = (UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		try {
			flexLog("About to call Page: ERC0010_enter_banck.jsp");
			forward("ERC0010_enter_banck.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e)
					.toString());
		}
	}

	protected void procActionBancosList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			ERC001001Message msgList = (ERC001001Message) mp.getMessageRecord("ERC001001", user.getH01USR(), "0015");
			if (req.getParameter("E01BRMEID") != null) {
				String codigo_banco = req.getParameter("E01BRMEID");
				msgList.setE01RCCBNK(codigo_banco);
			}
			if (req.getParameter("E01RCCBNK") != null) {
				String codigo_banco = req.getParameter("E01RCCBNK");
				msgList.setE01RCCBNK(codigo_banco);
			}
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC0010_enter_banck.jsp");
				forward("ERC0010_enter_banck.jsp", req, res);
			} else {
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
				if (!list.isEmpty()) {
					ERC001001Message listMessage = (ERC001001Message) list
							.get(0);
					session.setAttribute("codigo_banco", listMessage
							.getE01RCCBNK());
					session.setAttribute("nombre_banco", listMessage
							.getE01BNKNME());
				} else {
					session.setAttribute("codigo_banco", req
							.getParameter("E01BRMEID"));
					session.setAttribute("nombre_banco", req
							.getParameter("E01DSCRBK"));
				}
				session.setAttribute("ERClist", list);
				forwardOnSuccess("ERC0010_moves_list.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procSeleccionMov(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {
		MessageProcessor mp;
		UserPos userPO;
		mp = null;
		userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC0010", req);
			userPO.setPurpose(option);
			if (option.equals("NEW")) {
				session.removeAttribute("cnvObj");
				forward("ERC0010_maintance_moves.jsp", req, res);
			}
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC001001Message listMessage = (ERC001001Message) list
						.get(index);
				if (option.equals("INQUIRY")) {
					session.setAttribute("userPO", userPO);
					session.setAttribute("cnvObj", listMessage);
					forward("ERC0010_maintance_moves.jsp?readOnly=true", req,
							res);
				}
				if (option.equals("MAINTENANCE")) {
					session.setAttribute("userPO", userPO);
					session.setAttribute("cnvObj", listMessage);
					forward("ERC0010_maintance_moves.jsp", req, res);
				}
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionNEW(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		UserPos userPO;
		MessageProcessor mp;
		userPO = (UserPos) session.getAttribute("userPO");
		mp = null;

		try {
			mp = getMessageProcessor("ERC0010", req);
			ERC001001Message msg = (ERC001001Message) mp
					.getMessageRecord("ERC001001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0001");
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);
			mp.sendMessage(msg);
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ERC001001Message) mp.receiveMessageRecord();
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);
			if (!mp.hasError(msgError)) {
				if (req.getParameter("E01RCCBNK") != null) {
					String codigo_banco = req.getParameter("E01RCCBNK");
					res
							.sendRedirect((new StringBuilder(String
									.valueOf(JSEIBSServlet.srctx)))
									.append(
											"/servlet/datapro.eibs.products.JSERC0010?SCREEN=101&E01BRMEID=")
									.append(codigo_banco).append("&E01DSCRBK=")
									.append(req.getParameter("nombre_banco"))
									.toString());
				}
			} else {
				session.setAttribute("error", msgError);
				forward("ERC0010_maintance_moves.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionDelete(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		UserPos userPO;
		MessageProcessor mp;
		userPO = (UserPos) session.getAttribute("userPO");
		mp = null;
		try {
			mp = getMessageProcessor("ERC0010", req);
			ERC001001Message msg = (ERC001001Message) mp
					.getMessageRecord("ERC001001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0009");
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01SCRCOD("01");
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC001001Message listMessage = (ERC001001Message) list
						.get(index);
				msg.setE01RCCBNK(listMessage.getE01RCCBNK());
				msg.setE01RCCTCD(listMessage.getE01RCCTCD());
			}
			mp.sendMessage(msg);
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ERC001001Message) mp.receiveMessageRecord();
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);
			if (!mp.hasError(msgError)) {
				if (req.getParameter("codigo_lista") != null) {
					String codigo_banco = msg.getE01RCCBNK();
					if (mp != null)
						mp.close();
					res
							.sendRedirect((new StringBuilder(String
									.valueOf(JSEIBSServlet.srctx)))
									.append(
											"/servlet/datapro.eibs.products.JSERC0010?SCREEN=101&E01BRMEID=")
									.append(codigo_banco).append("&E01DSCRBK=")
									.append(req.getParameter("nombre_banco"))
									.toString());
				}
			} else {
				session.setAttribute("error", msgError);
				forward("ERC0010_maintance_moves.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionUpdate(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		UserPos userPO;
		MessageProcessor mp = null;
		userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = null;
			mp = getMessageProcessor("ERC0010", req);
			ERC001001Message msg = (ERC001001Message) mp
					.getMessageRecord("ERC001001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0002");
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);
			mp.sendMessage(msg);
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ERC001001Message) mp.receiveMessageRecord();
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);
			if (!mp.hasError(msgError)) {
				if (req.getParameter("E01RCCBNK") != null) {
					String codigo_banco = req.getParameter("E01RCCBNK");
					res
							.sendRedirect((new StringBuilder(String
									.valueOf(JSEIBSServlet.srctx)))
									.append(
											"/servlet/datapro.eibs.products.JSERC0010?SCREEN=101&E01BRMEID=")
									.append(codigo_banco).append("&E01DSCRBK=")
									.append(req.getParameter("nombre_banco"))
									.toString());
				}
			} else {
				session.setAttribute("error", msgError);
				forward("ERC0010_maintance_moves.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	private static final long serialVersionUID = 1L;
	protected static final int A_enter_banck = 100;
	protected static final int A_enter_banck_list = 101;
	protected static final int N_enter_movement = 200;
	protected static final int N_Actualizar_movement = 201;
	protected static final int N_delete_movement = 202;
	protected static final int N_Consulta_movement = 203;
	protected static final int New_enter_movement = 800;
	protected static final int Update_enter_movement = 700;
}
