
package datapro.eibs.products;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERC004001Message;
import datapro.eibs.beans.ERC004002Message;
import datapro.eibs.beans.ERC004003Message;
import datapro.eibs.beans.ERC004004Message;
import datapro.eibs.beans.ERC200001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.DecimalField;
import datapro.eibs.sockets.MessageRecord;


public class JSERC0040 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8614082645594326785L;
	protected static final int R_STATEMENT_ENTER 	= 100;
	protected static final int A_STATEMENT_ENTER 	= 1200;
	
	protected static final int A_STATEMENT_SUBMIT 	= 400;
	
	protected static final int A_UPDATE_IBS_ADD 	= 500;
	protected static final int A_UPDATE_IBS_SUB 	= 501;
	protected static final int A_UPDATE_CAR_ADD 	= 600;
	protected static final int A_UPDATE_CAR_SUB 	= 601;
	private static final MessageRecord listMessage = null;

	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
	
		switch (screen) {
		case R_STATEMENT_ENTER:
			procReqStatementEnter(user, req, res, session);
			break;
		case A_STATEMENT_ENTER:
			procActStatementEnter(user, req, res, session);
			break;
		case A_STATEMENT_SUBMIT:
			procActStatement(user, req, res, session);
			break;
		case A_UPDATE_IBS_ADD:
		case A_UPDATE_IBS_SUB:
		case A_UPDATE_CAR_ADD:
		case A_UPDATE_CAR_SUB:
			procActUpdateTotal(user, req, res, session, screen);
			break;
		default :
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	private void procActUpdateTotal(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws IOException {
		
		JBObjList ibslist = (JBObjList) session.getAttribute("ibsList");
		JBObjList carlist = (JBObjList) session.getAttribute("carList");
		StringBuffer buffer = new StringBuffer();
		try {
			int row = Integer.parseInt(req.getParameter("row"));
			BigDecimal debit = new BigDecimal("0.00");
			BigDecimal credit = new BigDecimal("0.00");
			ERC004002Message ibsmsg = null;
			ERC004003Message carmsg = null;
			String id = "";
			switch (screen) {

			case A_UPDATE_IBS_ADD:
				id = "IBS_TOTAL";
				try {
					debit = new BigDecimal(Util.parseCCYtoDouble(req.getParameter("debit")));
				} catch (Exception e) {
					debit = new BigDecimal("0.00");
				}
				try {
					credit = new BigDecimal(Util.parseCCYtoDouble(req.getParameter("credit")));
				} catch (Exception e) {
					credit = new BigDecimal("0.00");
				}
				ibsmsg = (ERC004002Message) ibslist.get(row);
				debit = debit.add(ibsmsg.getBigDecimalE02RCTAMD());
				credit = credit.add(ibsmsg.getBigDecimalE02RCTAMC());
				break;

			case A_UPDATE_IBS_SUB:
				id = "IBS_TOTAL";
				try {
					debit = new BigDecimal(Util.parseCCYtoDouble(req.getParameter("debit")));
				} catch (Exception e) {
					debit = new BigDecimal("0.00");
				}
				try {
					credit = new BigDecimal(Util.parseCCYtoDouble(req.getParameter("credit")));
				} catch (Exception e) {
					credit = new BigDecimal("0.00");
				}
				ibsmsg = (ERC004002Message) ibslist.get(row);
				debit = debit.subtract(ibsmsg.getBigDecimalE02RCTAMD());
				credit = credit.subtract(ibsmsg.getBigDecimalE02RCTAMC());
				break;

			case A_UPDATE_CAR_ADD:
				id = "CAR_TOTAL";
				try {
					debit = new BigDecimal(Util.parseCCYtoDouble(req.getParameter("debit")));
				} catch (Exception e) {
					debit = new BigDecimal("0.00");
				}
				try {
					credit = new BigDecimal(Util.parseCCYtoDouble(req.getParameter("credit")));
				} catch (Exception e) {
					credit = new BigDecimal("0.00");
				}
				carmsg = (ERC004003Message) carlist.get(row);
				debit = debit.add(carmsg.getBigDecimalE03RCSAMD());
				credit = credit.add(carmsg.getBigDecimalE03RCSAMC());
				break;

			case A_UPDATE_CAR_SUB:
				id = "CAR_TOTAL";
				try {
					debit = new BigDecimal(Util.parseCCYtoDouble(req.getParameter("debit")));
				} catch (Exception e) {
					debit = new BigDecimal("0.00");
				}
				try {
					credit = new BigDecimal(Util.parseCCYtoDouble(req.getParameter("credit")));
				} catch (Exception e) {
					credit = new BigDecimal("0.00");
				}
				carmsg = (ERC004003Message) carlist.get(row);
				debit = debit.subtract(carmsg.getBigDecimalE03RCSAMD());
				credit = credit.subtract(carmsg.getBigDecimalE03RCSAMC());
				break;

			default:
				break;
			}
			buffer.append((new StringBuilder("<div id=\"")).append(id).append(
					"\">").toString());
			buffer
					.append("<table class=\"tableinfo\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\" border=\"0\">");
			buffer.append("<tr>");
			buffer.append("<td align=\"center\" width=\"62%\">");
			buffer.append("<div>  </div>");
			buffer.append("</td>");
			buffer.append("<td align=\"right\" width=\"19%\">");
			buffer.append((new StringBuilder("<input id=\"")).append(id)
					.append("_DT\" size=\"16\" value=\"").append(
							Util.formatCCY(debit.toString())).append(
							"\" readonly=\"readonly\"/>").toString());
			buffer.append("</td>");
			buffer.append("<td align=\"right\" width=\"19%\">");
			buffer.append((new StringBuilder("<input id=\"")).append(id)
					.append("_CT\" size=\"16\" value=\"").append(
							Util.formatCCY(credit.toString())).append(
							"\" readonly=\"readonly\"/>").toString());
			buffer.append("</td>");
			buffer.append("</tr>");
			buffer.append("</table>");
			buffer.append("</div>");
		} catch (Exception exception) {
		}
		sendXMLResponse(res, buffer);
	}

	private void procActStatement(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws IOException, ServletException {
		String car[];
		String ibs[];
		JBObjList ibslist;
		JBObjList carlist;
		MessageProcessor mp;
		car = req.getParameterValues("rowcar");
		ibs = req.getParameterValues("rowibs");
		ibslist = (JBObjList) session.getAttribute("ibsList");
		carlist = (JBObjList) session.getAttribute("carList");
		mp = null;
		
		try {
			mp = getMessageProcessor("ERC0040", req);
			ERC004004Message msg = (ERC004004Message) mp
					.getMessageRecord("ERC004004");
			msg.setH04USERID(user.getH01USR());
			msg.setH04PROGRM("ERC0040");
			msg.setH04OPECOD("0022");
			msg.setH04TIMSYS(getTimeStamp());
			
			if (req.getParameter("codigo_lista")!= null){

				   JBObjList list = (JBObjList)session.getAttribute("ERClist");
			   
				   int index = Integer.parseInt(req.getParameter("codigo_lista"));
				   ERC200001Message listMessage = (ERC200001Message)list.get(index);
					
				   msg.setE04RCHRBK(listMessage.getE01RCHRBK());
				   msg.setE04BRMCTA(listMessage.getE01RCHCTA());
				   msg.setE04CARSTN(listMessage.getE01RCHSTN());
				   
				   
			}
			
			for (int i = 0; i < car.length; i++) {
				int index = com.datapro.generic.tool.Util.parseInt(car[i]);
				ERC004003Message carmsg = (ERC004003Message) carlist.get(index);
				String tag = (new StringBuilder("E04SUID")).append(
						Util.addLeftChar('0', 2, String.valueOf(i + 1)))
						.toString();
				DecimalField field = (DecimalField) msg.getField(tag);
				field.setBigDecimal(carmsg.getBigDecimalE03RCSUID());
			}

			for (int i = 0; i < ibs.length; i++) {
				int index = com.datapro.generic.tool.Util.parseInt(ibs[i]);
				ERC004002Message ibsmsg = (ERC004002Message) ibslist.get(index);
				String tag = (new StringBuilder("E04TUID")).append(
						Util.addLeftChar('0', 2, String.valueOf(i + 1)))
						.toString();
				DecimalField field = (DecimalField) msg.getField(tag);
				field.setBigDecimal(ibsmsg.getBigDecimalE02RCTUID());
			}

			mp.sendMessage(msg);
			ELEERRMessage msgError = (ELEERRMessage) mp
					.receiveMessageRecord("ELEERR");
			if (mp.hasError(msgError)) {
				session.setAttribute("error", msgError);
				forward("ERC0040_rcbank_list_selection.jsp", req, res);
			} else {
				procActStatementEnter(user, req, res, session);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	private void procActStatementEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws IOException, ServletException {
		UserPos userPO;
		MessageProcessor mp;
		userPO = getUserPos(session);
		mp = null;
		try {
			mp = getMessageProcessor("ERC0040", req);
			ERC004001Message msg = (ERC004001Message) mp
					.getMessageRecord("ERC004001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01PROGRM("ERC0040");
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());
			String codigo_lista = "";
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Integer.parseInt(req.getParameter("codigo_lista"));
				ERC200001Message listMessage = (ERC200001Message) list
						.get(index);
				msg.setE01RCHRBK(listMessage.getE01RCHRBK());
				msg.setE01BRMCTA(listMessage.getE01RCHCTA());
				msg.setE01CARSTN(listMessage.getE01RCHSTN());
				msg.setE01BRMACC(listMessage.getE01RCHACC());
				if (req.getParameter("E01HASDDD") != null) {
					msg.setE01HASDDD(req.getParameter("E01HASDDD"));
					msg.setE01HASDDM(req.getParameter("E01HASDDM"));
					msg.setE01HASDDY(req.getParameter("E01HASDDY"));
				} else {
					msg.setE01HASDDD("0");
					msg.setE01HASDDM("0");
					msg.setE01HASDDY("0");
				}
			} else {
				msg.setE01RCHRBK("");
				msg.setE01BRMCTA("");
				msg.setE01CARSTN("");
				msg.setE01BRMACC("");
			}
			mp.sendMessage(msg);
			ELEERRMessage msgError = (ELEERRMessage) mp
					.receiveMessageRecord("ELEERR");
			if (mp.hasError(msgError)) {				
				
				
				MessageProcessor mp2=null;

				mp2 = getMessageProcessor("ERC2000", req);
				ERC200001Message msgList2 = (ERC200001Message) mp.getMessageRecord("ERC200001", user.getH01USR(), "0015");
				if (req.getParameter("E01BRMEID") != null) 
				{
					String codigo_banco = req.getParameter("E01BRMEID");
					msgList2.setE01RCHRBK(codigo_banco);
				}
				if (req.getParameter("E01BRMCTA") != null) 
				{
					String cuenta_banco = req.getParameter("E01BRMCTA");
					msgList2.setE01RCHCTA(cuenta_banco);
				}
				if (req.getParameter("posicion") != null)
					msgList2.setE01RCHREC(req.getParameter("posicion"));
				if (req.getParameter("mark") != null)
					flexLog((new StringBuilder("marker:")).append(req.getParameter("mark")).toString());
				if (req.getParameter("E01RCHSTN") != null) 
				{
					int numero = Integer.parseInt(req.getParameter("E01RCHSTN"));
					if (numero > 0)
						msgList2.setE01RCHAPF("T");
				}
				setMessageRecord(req, msgList2);
				String fecha1 = (new StringBuilder(String.valueOf(req.getParameter("E01RCHFSD")))).append("/").append(
						req.getParameter("E01RCHFSM")).append("/").append(
						req.getParameter("E01RCHFSY")).toString();
				String fecha2 = (new StringBuilder(String.valueOf(req.getParameter("E01RCHFLD")))).append("/").append(
						req.getParameter("E01RCHFLM")).append("/").append(
						req.getParameter("E01RCHFLY")).toString();
				req.setAttribute("fecha_inicial", fecha1);
				req.setAttribute("fecha_final", fecha2);
				if (req.getParameter("E01RCHSTN") != null)
					req.setAttribute("numero_cartola", req.getParameter("E01RCHSTN"));
				if (req.getParameter("E01RCHAPF") != null)
					req.setAttribute("estado", req.getParameter("E01RCHAPF"));
				session.setAttribute("lista_entrada", msgList2);

				mp.sendMessage(msgList2);

				ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
				if (mp.hasError(error)) 
				{
					session.setAttribute("error", error);
					flexLog("About to call Page: ERC2000_statement_enter.jsp");
					forward("ERC2000_statement_enter.jsp", req, res);
				} else 
				{
					JBObjList list2 = mp.receiveMessageRecordList("H01FLGMAS",
							"E01RCHREC");
					session.setAttribute("ERClist", list2);
					session.setAttribute("codigo_banco", req
							.getParameter("E01BRMEID"));
					session.setAttribute("cuenta_banco", req
							.getParameter("E01BRMCTA"));
					session.setAttribute("nombre_banco", req
							.getParameter("E01DSCRBK"));
					session.setAttribute("cuenta_ibs", req
							.getParameter("E01BRMACC"));
					forwardOnSuccess("ERC2000_statement_list.jsp", req, res);
				}
			} else {
				session.setAttribute("userPO", userPO);
				msg = (ERC004001Message) mp.receiveMessageRecord("ERC004001");
				JBObjList ibsList = mp.receiveMessageRecordList("H02FLGMAS");
				JBObjList carList = mp.receiveMessageRecordList("H03FLGMAS");
				req.setAttribute("codigo_lista", req
						.getParameter("codigo_lista"));
				session.setAttribute("msg", msg);
				session.setAttribute("ibsList", ibsList);
				session.setAttribute("carList", carList);
				if (req.getParameter("E01RCHAPF") != null)
					req.setAttribute("estado", req.getParameter("E01RCHAPF"));
				session.setAttribute("codigo_banco", req
						.getParameter("E01BRMEID"));
				session.setAttribute("cuenta_banco", req
						.getParameter("E01BRMCTA"));
				session.setAttribute("nombre_banco", req
						.getParameter("E01DSCRBK"));
				session.setAttribute("cuenta_ibs", req
						.getParameter("E01BRMACC"));
				String fecha1 = (new StringBuilder(String.valueOf(req
						.getParameter("E01RCHFSD")))).append("/").append(
						req.getParameter("E01RCHFSM")).append("/").append(
						req.getParameter("E01RCHFSY")).toString();
				String fecha2 = (new StringBuilder(String.valueOf(req
						.getParameter("E01RCHFLD")))).append("/").append(
						req.getParameter("E01RCHFLM")).append("/").append(
						req.getParameter("E01RCHFLY")).toString();
				req.setAttribute("fecha_inicial", fecha1);
				req.setAttribute("fecha_final", fecha2);
				if (req.getParameter("E01RCHSTN") != null) {
					req.setAttribute("E01Cartola", req
							.getParameter("E01RCHSTN"));
					req.setAttribute("numero_cartola", req
							.getParameter("E01RCHSTN"));
				}
				if (req.getParameter("E01RCHREC") != null)
					req
							.setAttribute("E01RCHREC", req
									.getParameter("E01RCHREC"));
				forward("ERC0040_rcbank_list_selection.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	private void procReqStatementEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		forward("ERC0040_rcbank_enter_selection.jsp", req, res);
	}
}
