package datapro.eibs.params;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;

import datapro.eibs.beans.EDD240001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERC004001Message;
import datapro.eibs.beans.ERC004003Message;
import datapro.eibs.beans.ERC004005Message;
import datapro.eibs.beans.ERC200001Message;
import datapro.eibs.beans.ERC200002Message;
import datapro.eibs.beans.ERC200003Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ETG000000Message;
import datapro.eibs.beans.ExcelColStyle;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.sockets.MessageField;

public class JSEDD2400 extends JSEIBSServlet {
	
	private static final long serialVersionUID = 1L;

	protected static final int R_ENTER_MANT = 100;
	protected static final int A_ENTER_MANT = 200;
	protected static final int A_ENTER_CONFIR = 300;
	
	public JSEDD2400() {
	}

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
	
		case R_ENTER_MANT:
			procReqEnterMant(user, req, res, session);
			break;
		
		case A_ENTER_MANT:
			procActEnterMant(user, req, res, session);
			break;

		case A_ENTER_CONFIR:
			procActEnterConfir(user, req, res, session);
			break;
			
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	/*Objetivo : Presenta pantalla de Mantenimiento Cambio Valor Cuota y Reajuste
	 *Autor : JLSP 
	 *Fecha : Marzo 2016 
	 */
	protected void procReqEnterMant(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
		UserPos userPO = (UserPos) ses.getAttribute("userPO"); 

		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDD2400", req);
			EDD240001Message msgCapRea = (EDD240001Message) mp.getMessageRecord("EDD240001", user.getH01USR(), "0001");
			msgCapRea.setH01TIMSYS(getTimeStamp());
			
			mp.sendMessage(msgCapRea);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgCapRea = (EDD240001Message) mp.receiveMessageRecord("EDD240001");

			ses.setAttribute("error", msgError);
			ses.setAttribute("CapRea", msgCapRea);
			ses.setAttribute("userPO", userPO);
			forward("EDD2400_capita_rea_enter_mantenance.jsp", req, res);

		} 
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
		finally {
			if (mp != null)	mp.close();
		}
	}

	/*Objetivo : Submite la Mantenimiento Cambio Valor Cuota y Reajuste
	 *Autor : JLSP 
	 *Fecha : Marzo 2016 
	 */
	protected void procActEnterMant(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
		UserPos userPO = (UserPos) ses.getAttribute("userPO"); 

		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDD2400", req);
			EDD240001Message msgCapRea = (EDD240001Message) mp.getMessageRecord("EDD240001", user.getH01USR(), "0002");
			msgCapRea.setH01TIMSYS(getTimeStamp());
			
			setMessageRecord(req, msgCapRea);
			
			mp.sendMessage(msgCapRea);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgCapRea = (EDD240001Message) mp.receiveMessageRecord("EDD240001");

			ses.setAttribute("CapRea", msgCapRea);
			ses.setAttribute("error", msgError);
			if (mp.hasError(msgError) && !(msgCapRea.getH01FLGWK1().trim().equals("X"))) {
				ses.setAttribute("error", msgError);
				forward("EDD2400_capita_rea_enter_mantenance.jsp", req, res);
			} 
			else 
			{
				forward("EDD2400_capita_rea_confirm_mantenance.jsp", req, res);
			}

		} 
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
		finally {
			if (mp != null)	mp.close();
		}
	}


	/*Objetivo : Confirma la Mantenimiento Cambio Valor Cuota y Reajuste
	 *Autor : JLSP 
	 *Fecha : Marzo 2016 
	 */
	protected void procActEnterConfir(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
		UserPos userPO = (UserPos) ses.getAttribute("userPO"); 

		MessageProcessor mp = null;
		try {

			mp = getMessageProcessor("EDD2400", req);
			EDD240001Message msgCapRea = (EDD240001Message) ses.getAttribute("CapRea");
			msgCapRea.setH01TIMSYS(getTimeStamp());
			msgCapRea.setH01USERID(user.getH01USR());
			msgCapRea.setH01OPECOD("0003");

			mp.sendMessage(msgCapRea);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgCapRea = (EDD240001Message) mp.receiveMessageRecord("EDD240001");

			ses.setAttribute("CapRea", msgCapRea);
			ses.setAttribute("error", msgError);
			forward("EDD2400_capita_rea_voucher_mantenance.jsp", req, res);

		} 
		catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}
}
