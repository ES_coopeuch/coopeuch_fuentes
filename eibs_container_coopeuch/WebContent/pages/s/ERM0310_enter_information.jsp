<!-- Hecho por Alonso Arana ------Datapro-----15/05/2014 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<html>
<head>
<title>Formulario de Mantenci�n de Par�metros de control de Acreencias </title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="JavaScript">
function goAction2(op) {


	if(op=='2000')
	{
		document.forms[0].SCREEN.value = op;
		document.forms[0].submit();	
	}
	else
	{
	    var sCadena = "";
	    var bInd = true;
		var sComparador = "";
        var sRepetidos = "";
        var bPro = true;
        
	    
		var iNpro = parseInt(document.forms[0].iNpro.value);

		if(iNpro>=1){
			sCadena = sCadena.concat(document.forms[0].E01ACRPR1.value);
			if(iNpro < parseInt(document.forms[0].E01ACRPR1.value))
				bInd=false;
		}	
			
		if(iNpro>=2){
			sCadena = sCadena.concat(document.forms[0].E01ACRPR2.value);
			if(iNpro < parseInt(document.forms[0].E01ACRPR2.value))
				bInd=false;
		}	

		if(iNpro>=3){
			sCadena = sCadena.concat(document.forms[0].E01ACRPR3.value);
			if(iNpro < parseInt(document.forms[0].E01ACRPR3.value))
				bInd=false;
		}	

		if(iNpro>=4){
			sCadena = sCadena.concat(document.forms[0].E01ACRPR4.value);
			if(iNpro < parseInt(document.forms[0].E01ACRPR4.value))
				bInd=false;
		}	

		if(iNpro>=5){
			sCadena = sCadena.concat(document.forms[0].E01ACRPR5.value);
			if(iNpro < parseInt(document.forms[0].E01ACRPR5.value))
				bInd=false;
		}	

		if(iNpro>=6){
			sCadena = sCadena.concat(document.forms[0].E01ACRPR6.value);
			if(iNpro < parseInt(document.forms[0].E01ACRPR6.value))
				bInd=false;
		}	

		if(iNpro>=7){
			sCadena = sCadena.concat(document.forms[0].E01ACRPR7.value);
			if(iNpro < parseInt(document.forms[0].E01ACRPR7.value))
				bInd=false;
		}	

		if(iNpro>=8){
			sCadena = sCadena.concat(document.forms[0].E01ACRPR8.value);
			if(iNpro < parseInt(document.forms[0].E01ACRPR8.value))
				bInd=false;
		}	

		if(iNpro>=9){
			sCadena = sCadena.concat(document.forms[0].E01ACRPR9.value);
			if(iNpro < parseInt(document.forms[0].E01ACRPR9.value))
				bInd=false;
		}	

		if(iNpro>=10){
			sCadena = sCadena.concat(document.forms[0].E01ACRPR10.value);
			if(iNpro < parseInt(document.forms[0].E01ACRPR10.value))
				bInd=false;
		}	

	
        for (var iCont = 0 ; iCont < sCadena.length ; iCont++){
            if (sComparador.indexOf(sCadena.substr(iCont,1)) == -1){
               sComparador += sCadena.substr(iCont,1);
           }else{
              if (sRepetidos.indexOf(sCadena.substr(iCont,1)) == -1)
                      sRepetidos += sCadena.substr(iCont,1);
            }
        }
				
		if(sRepetidos!="" && sRepetidos!="0"){
			alert("Las prioridades de pago no deben estar repetidas.");
			bPro = false;
		}
		
		if(!bInd){
			alert("Las prioridades pueden tener valor cero y no mayores a : " + iNpro.toString());
			bPro = false;
		}
				
		if(bPro)
			document.forms[0].submit();	


	}
	

}


</script>


<jsp:useBean id= "AcreList" class= "datapro.eibs.beans.ERM031001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" 	scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" 	scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage"  scope="session" />


</head>
<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>

<body bgcolor="#FFFFFF"  >

<%if (!error.getERRNUM().equals("0")) {
	error.setERRNUM("0");
	out.println("<SCRIPT Language=\"Javascript\">");
	out.println("       showErrors()");
	out.println("</SCRIPT>");
	}
	
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
 
%>





<h3 align="center">  <%if (readOnly){ %>
	Consulta de Control de Acreencias
<%} else if (maintenance){ %>
	Mantenci�n de Control de Acreencias
<%} else { %>
	Nuevo Control de Acreencias
<%} %>
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="enter_information, ERM0310"></h3>
<hr size="4">
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERM0310">
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="<%
 if(readOnly){
 out.print("600");
 } 
 
 if(maintenance){
 out.print("700");
 } 
  else{
   out.print("800");
  
  }
  
  
   %>"> 


 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="left" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">   Banco : 
              </td>
             <td nowrap width="5%" align="left">
	  			<input type="text" name="E01ACRBNK" value="<%=session.getAttribute("codigo_banco")%>" 
	  			size="2"  readonly>
             </td>          
          <td nowrap width="10%" align="left">
	  			<input type="text" name="E01DESBNK" value="<%=session.getAttribute("desc_banco")%>" 
	  			size="50"  readonly>
             </td> 
         
         </tr>
         
      
        </table>
      </td>
    </tr>
  </table>



<h4>Par�metros</h4>

<table class="tableinfo" cellspacing="0" cellpadding="2" width="100%"border="0">


	<tr id="trclear">		
		<td nowrap width="30%">
	<div align="right">Canal Condonaci�n:</div>
	  </td>
		<td nowrap width="30%">
		
	    	<select name="E01ACRCND">
	    <% boolean flag = false; %>
                       <option value="Y" <%if (AcreList.getE01ACRCND().equals("Y")) { flag = true; out.print("selected"); }%>>SI</option>
                <option value="N" <%if (AcreList.getE01ACRCND().equals("N")) { flag = true; out.print("selected"); }%>>NO</option>
             
              </select>
	    	 
		</td>
        <td nowrap width="25%">
          		
       
        </td> 
        		<td nowrap width="5%">
		</td>
	</tr>
	<tr id="trdark">
			<td nowrap width="30%">
<div align="right">Lote Contable:</div>
	  </td>
		<td nowrap width="30%">
	        
	          
   <input type="text" readonly name="E01ACRBTH" onKeypress="enterInteger()" size="7" maxlength="5" value="<%=AcreList.getE01ACRBTH().trim()%>">
        
	          	  
	          	  
		</td>
        <td nowrap width="25%">
          	
       
        </td> 
        		<td nowrap width="5%">

		</td>
	</tr>
	
	


	<tr id="trclear">
			<td nowrap width="30%">
<div align="right">Fecha �ltimo proceso:</div>
	  </td>
		<td nowrap width="30%">

   <input type="text" name="E01ACRPMD" readonly  id="fecha1" size="3" maxlength="2" value="<%=AcreList.getE01ACRPMD()%>">
                <input type="text" readonly name="E01ACRPMM" id="fecha2" size="3" maxlength="2" value="<%=AcreList.getE01ACRPMM()%>">
                <input type="text" readonly name="E01ACRPMY" id="fecha3" size="5" maxlength="4" value="<%=AcreList.getE01ACRPMY()%>">
               
		
		</td>
        <td nowrap width="25%">
          		
       
        </td> 
        		<td nowrap width="5%">

		</td>
	</tr>
	
	<tr id="trdark">
			<td nowrap width="30%">
<div align="right">Glosa Asiento Contable:</div>
	  </td>
		<td nowrap width="30%">

   <input type="text" name="E01ACRGL1"  size="60" maxlength="60" value="<%=AcreList.getE01ACRGL1().trim()%>">
        


		
		</td>
        <td nowrap width="25%">
          		
       
        </td> 
        		<td nowrap width="5%">

		</td>
	</tr>
	
	
		<tr id="trclear">
			<td nowrap width="30%">
<div align="right">Monto Aplicado:</div>
	  </td>
		<td nowrap width="30%">

   <input type="text" name="E01ACRAMT" readonly onKeypress="enterInteger()" size="15" maxlength="13" value="<%=AcreList.getE01ACRAMT().trim()%>">
        


		
		</td>
        <td nowrap width="25%">
          		
       
        </td> 
        		<td nowrap width="5%">

		</td>
	</tr>
	
	

	</table>
	

<h4>Prioridad de Pagos</h4>
<table class="tableinfo" cellspacing="0" cellpadding="2" width="100%"border="0">
<%
	int iNpro = 0;
	for(int iSec=1;iSec<11;iSec+=1){
		if(!AcreList.getField("E01ACRCD"+iSec).getString().equals(""))
		{
		 iNpro += 1;
%>

	<tr id="trclear">		
		<td nowrap width="30%">
			<div align="right">
			   <input type="hidden" name="E01ACRCD<%=iSec%>"  value="<%=AcreList.getField("E01ACRCD"+iSec).getString() %>">
			   <input type="text"  name="E01ACRPR<%=iSec%>"  onKeypress="enterInteger()" size="2" maxlength="1" value="<%=AcreList.getField("E01ACRPR"+iSec).getString() %>">
			</div>
	  	</td>
	
		<td nowrap width="30%">
			<div align="left">
			   <input type="hidden" name="E01ACRDS<%=iSec%>"  value="<%=AcreList.getField("E01ACRDS"+iSec).getString() %>">
			   <%=AcreList.getField("E01ACRDS"+iSec).getString() %>
			</div>
		</td>
        <td nowrap width="25%">
        </td> 
		<td nowrap width="5%">
		</td>
	</tr>

<%     }
	}

%>
	 <input type="hidden" name="iNpro"  value="<%=iNpro%>">

</table>

	
	<% if (maintenance){  %>
	<h4>Datos �ltima actualizaci�n</h4>

<table class="tableinfo" cellspacing="0" cellpadding="2" width="100%"border="0">


	<tr id="trclear">		
		<td nowrap width="30%">
  Fecha:
  			
                <input type="text" name="E01ACRLMD" readonly id="fecha1" size="3" maxlength="2" value="<%=AcreList.getE01ACRLMD()%>">
                <input type="text" name="E01ACRLMM" readonly id="fecha2" size="3" maxlength="2" value="<%=AcreList.getE01ACRLMM()%>">
                <input type="text" name="E01ACRLMY" readonly id="fecha3" size="5" maxlength="4" value="<%=AcreList.getE01ACRLMY()%>">
               
	  </td>
		<td nowrap width="30%">
	          		
	       
	          		
	          	
	          		</td>
		
        <td nowrap width="25%">
     		Usuario:
                <input type="text" name="E01ACRLMU"  size="13" readonly  value="<%=AcreList.getE01ACRLMU()%>">
               
	
          	
        </td> 
        		<td nowrap width="5%">
		</td>
	</tr>

	

	</table>
  <%} %>



	<% if(!maintenance){%>

	  <p align="center">
	
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>

	<% }%>


<% if(maintenance){ %>
	  <p align="center">
	    <input id="EIBSBTN" type=button name="Submit" value="Atr�s" onClick="javascript:goAction2('2000')">
	    <input id="EIBSBTN" type=button name="Submit" value="Enviar" onClick="javascript:goAction2('')">
  </p>
  
<%} %>
	</FORM>
	
	
	</body>
	
	</html>
	
	