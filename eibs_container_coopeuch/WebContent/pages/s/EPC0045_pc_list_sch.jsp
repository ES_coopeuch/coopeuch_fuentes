 <%@ page import = "datapro.eibs.master.Util" %>
 <%@ page import = "datapro.eibs.beans.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Cambio de Condicion de Proyecto</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">

<jsp:useBean id= "extList" class= "datapro.eibs.beans.JBObjList"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />


<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="javascript">

function goAction(op) {
 var delok= false;
 var page="";
 if (op == 1) {
     page = prefix +language + 'EPC0045_pc_new_sch.jsp';
 } else if (op == 2) {
     page = prefix +language + 'EPC0045_pc_maint_sch.jsp?ROW=' + document.forms[0].ROW.value;
 } else if (op == 4) {     
     page = prefix +language + 'EPC0045_pc_program_sch.jsp?ROW=' + document.forms[0].ROW.value; 
 } else if (op == 5) {     
     page = prefix +language + 'EPC0045_pc_accept_sch.jsp?ROW=' + document.forms[0].ROW.value; 
 }
   CenterWindow(page,800,800,2);
 }      

function goExit(){
  window.location.href="<%=request.getContextPath()%>/pages/background.jsp";

}

function showInfo(idxRow,d1,d2,d3){
   for ( var i=0; i<dataTable.rows.length; i++ ) {
       dataTable.rows[i].className="trnormal";
    }
   dataTable.rows[idxRow].className="trhighlight";
   document.forms[0].ROW.value = "" + idxRow;
  } 

</script>
</HEAD>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<BODY>


<FORM Method="post" Action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEPC0045">
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
<INPUT TYPE=HIDDEN NAME="OPT" VALUE="3">
<INPUT TYPE=HIDDEN NAME="ROW" VALUE="0">

<% if (userPO.getHeader22().equals("**")) {  %>
<h3 align="center">Cambio de Condicion de Proyecto<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="pc_list_sch.jsp,EPC0045"> 
</h3>
<% } %>


<% if (userPO.getHeader22().equals("03")) {  %>
<h3 align="center">Programar Protocolizacion<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="pc_list_sch.jsp,EPC0045"> 
</h3>
<% } %>


<% if (userPO.getHeader22().equals("06")) {  %>
<h3 align="center">Protocolizaci�n de Proyecto<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="pc_list_sch.jsp,EPC0045"> 
</h3>
<% } %>


<hr size="4">
  
  
<TABLE class="tbenter">
  <TR> 
 <%
	if ( !(extList.getNoResult() )) {
 %>
<% if (userPO.getHeader22().equals("**")) {  %>
    <TD ALIGN=CENTER CLASS=tdbkg><a href="javascript:goAction('2')">Modificar</a></TD>
<% } %>

<% if (userPO.getHeader22().equals("03")) {  %>
    <TD ALIGN=CENTER CLASS=tdbkg><a href="javascript:goAction('4')">Programar</a></TD>    
<% } %>

<% if (userPO.getHeader22().equals("06")) {  %>
    <TD ALIGN=CENTER CLASS=tdbkg><a href="javascript:goAction('5')">Protocolizar</a></TD>    
 <% } %>

 <% } %>
    <TD ALIGN=CENTER CLASS=tdbkg><a href="javascript:goExit()">Salir</a></TD>
  </TR>
</TABLE>


<%
	if ( extList.getNoResult() ) {
 %>
   	<TABLE class="tbenter" width=100% height=40%>
   	 <TR valign="middle">
      <TD> 
        <div align="center">       		
          <p><b>No hay resultados que correspondan a su criterio de b�squeda.         
            </b></p>
          </div>
      </TD>
     </TR>
   	</TABLE>
<%   		
	}
	else {
%>  
 <INPUT TYPE=HIDDEN NAME="E01DLLSD1" VALUE="">
 <INPUT TYPE=HIDDEN NAME="E01DLLSD2" VALUE="">
 <INPUT TYPE=HIDDEN NAME="E01DLLSD3" VALUE="">
 
 <TABLE  id="mainTable" class="tableinfo" >
 <TR> 
   <TD NOWRAP valign="top" width="100%">
    <TABLE id="headTable" >
    <TR id="trdark"> 
      <TH ALIGN=CENTER nowrap>&nbsp;</TH>
      <TH ALIGN=CENTER nowrap>N�mero</TH>
      <TH ALIGN=CENTER nowrap>Direcci�n Principal</TH>
      <TH ALIGN=CENTER nowrap>Ciudad</TH>
      <TH ALIGN=CENTER nowrap>Pais</TH>      
      <TH ALIGN=CENTER nowrap>Valor Solicitado</TH>
      <TH ALIGN=CENTER nowrap>Estado</TH>            
    </TR>
    </TABLE>
  
    <div id="dataDiv1" class="scbarcolor" style="padding:0">
    <table id="dataTable"  >
    <%
         int row;
		 try{row = Integer.parseInt(request.getParameter("ROW"));}catch(Exception e){row = 0;}
         extList.initRow();        
         while (extList.getNextRow()) {
            EPC004501Message msgPC = (EPC004501Message) extList.getRecord();
     %> 
            <TR> 
              <TD ALIGN=CENTER NOWRAP> 
                <INPUT TYPE="radio" NAME="EXTCHG" VALUE="<%= extList.getCurrentRow() %>" <% if (extList.getCurrentRow() == row) out.print("checked"); %> onClick="showInfo(<%= extList.getCurrentRow() %>,'<%=msgPC.getE01PCMACC()%>')">
              </TD>
              <TD ALIGN=CENTER NOWRAP><%= Util.formatCell(msgPC.getE01PCMACC()) %></TD>
              <TD ALIGN=LEFT NOWRAP><%= Util.formatCell(msgPC.getE01PCMAD1()) %></TD>
              <TD ALIGN=LEFT NOWRAP><%= Util.formatCell(msgPC.getE01PCMCTY()) %></TD>
              <TD ALIGN=LEFT NOWRAP><%= Util.formatCell(msgPC.getE01PCMCTR()) %></TD>
              <TD ALIGN=RIGHT NOWRAP><%= Util.formatCell(msgPC.getE01PCMVSL()) %></TD>              
              <TD ALIGN=CENTER NOWRAP><%= Util.formatCell(msgPC.getE01PCMRMK()) %></TD>
             </TR>
      <%              
         }        
    %>
    </table>
    </div>
   </TD> 
  </TR>	
 </TABLE>
 
  <SCRIPT language="JavaScript">  	
  	function resizeDoc() {
       adjustEquTables(headTable, dataTable, dataDiv1,1,false);
    }
    showChecked("EXTCHG");
    resizeDoc();
    window.onresize=resizeDoc;     
  </SCRIPT>
  <%
  }
 %>
 


</FORM>

</BODY>
</HTML>
