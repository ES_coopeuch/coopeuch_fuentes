<%@ page import="datapro.eibs.beans.ESD079301Message"%>
<%@ page import="datapro.eibs.master.Util"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<title>Aprobación de Solicitudes Cambio de Estado de Convenios</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"	rel="stylesheet">

<jsp:useBean id="Lista" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>
	

<script type="text/javascript">


  function goAction(op) {
	var ok = false;
	
    for(n=0; n<document.forms[0].elements.length; n++)
     {
     	var elementName= document.forms[0].elements[n].name;
     	if(elementName == "key") 
     	{
			if (document.forms[0].elements[n].checked == true) {
      			ok = true;
      			break;
			}
     	}
     }      
	
     if ( ok ) {     	
		document.forms[0].SCREEN.value = op;
		document.forms[0].submit();
     } else {
		alert("Debe seleccionar un convenio para continuar.");	   
	 }
		
	}

  
 function showAddInfo(idxRow){
   tbAddInfo.rows[0].cells[1].style.color="blue";   
   tbAddInfo.rows[0].cells[1].innerHTML=extraInfo(document.forms[0]["TXTDATA"+idxRow].value,4);
   } 
   
 function extraInfo(textfields,noField) {
	 var pos=0
	 var s= textfields;
	 for ( var i=0; i<noField ; i++ ) {
	   pos=textfields.indexOf("<br>",pos+1);
	  }
	 s=textfields.substring(0,pos);
	 return(s);
 }  
function showInq(num) {
	
	$("input[name=key][value=" + num + "]").prop('checked', true);
	
	page = webapp + "/servlet/datapro.eibs.client.JSECO0100?SCREEN=900&key=" + num;
	CenterWindow(page,600,500,2);
}
</script>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Aprobación de Solicitudes Cambio de Estado de Convenios<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="change_deal_status_approval_list.jsp, ESD0793"></h3>

<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0793">
<input type="hidden" name="SCREEN" value="100"> 

<table class="tbenter" width=100% align=center>
	<tr>
		<td class=TDBKG width="35%">
			<div align="center"><a href="javascript:goAction(200)" id="linkApproval"><b>Aprobar</b></a></div>
		</td>		
		<td class=TDBKG width="35%">
			<div align="center"><a href="javascript:goAction(300)" id="linkReject"><b>Rechazar</b></a></div>
		</td>		
	</tr>
</table>

<%
	if (Lista.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=90%>
	<TR>
		<TD>
		<div align="center"><font size="3"><b> No hay aprobaciones de solicitud de cambio de estado de convenios pendientes. a&cute;</b></font></div>
		</TD>
	</TR>
</TABLE>
<%
	} else {
%>
<table id="mainTable" class="tableinfo" align="center">
	<tr>
		<td nowrap valign="top">
		<table id="dataTable" width="100%">
			<tr id="trdark">
				<th align="center" nowrap></th>
				<th align="center" nowrap>Solicitud</th>
				<th align="center" nowrap>Codigo</th>
				<th align="center" nowrap>Fecha Cambio Estado</th>
				<th align="center" nowrap>Hora Cambio de Estado</th>
				<th align="center" nowrap>Estado</th>
				<th align="center" nowrap>Motivo</th>
				<th align="center" nowrap>Detalle</th>
				<th align="center" nowrap>Usuario</th>
			</tr>
			<%
				Lista.initRow();
					int k = 0;
					boolean firstTime = true;
					String chk = "";
					while (Lista.getNextRow()) {
						if (firstTime) {
							firstTime = false;
							chk = "checked";
						} else {
							chk = "";
						}
						ESD079301Message convObj = (ESD079301Message) Lista.getRecord();
			%>
			<tr>
				<td nowrap><input type="radio" name="key" value="<%= Lista.getCurrentRow() %>" <%=chk%>></td>
				<td nowrap align="center"><%= Util.formatCell(convObj.getE01COTNUM())%></td>
				<td nowrap align="center"><%= Util.formatCell(convObj.getE01COTCDE())%></td>
				<td nowrap align="center"><%= Util.formatCell(convObj.getE01COTFCH())%></td>
				<td nowrap align="center"><%= Util.formatTime(convObj.getE01COTHOR())%></td>
				<td nowrap align="center"><%= Util.formatCell(convObj.getE01COTDST())%></td>
				<td nowrap align="center"><%= Util.formatCell(convObj.getE01COTDMT())%></td>
				<td nowrap align="center"><%= Util.formatCell(convObj.getE01COTDET())%></td>
				<td nowrap align="center"><%= Util.formatCell(convObj.getE01COTUSR())%></td>
			</tr>
			<%
				}
			%>
		</table>
		</td>
	</tr>
</table>

<%
	}
%>
</form>

</body>
</html>
