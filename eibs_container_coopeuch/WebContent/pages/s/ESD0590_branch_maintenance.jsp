<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Maestro de Agencias</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="brnDetails" class="datapro.eibs.beans.ESD059001Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
%>


<H3 align="center">Mantenimiento del Maestro de Agencias <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="branch_maintenance.jsp, ESD0590"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSESD0590" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Banco :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <eibsinput:text name="brnDetails" property="E01BRNBNK" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BANK %>" readonly="true"/>
             </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Agencia :</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2"> 
                <eibsinput:text name="brnDetails" property="E01BRNNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
                </font></font></font></div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Informaci&oacute;n de la Agencia</h4>  
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right">Nombre :</div>
            </td>
            <td nowrap width="23%" colspan="3"> 
                <eibsinput:text name="brnDetails" property="E01BRNNME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" required="true" />
           </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="16%" height="23"> 
              <div align="right">Direcci&oacute;n :</div>
            </td>
            <td nowrap height="23" colspan="3"> 
                <eibsinput:text name="brnDetails" property="E01BRNADR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" />
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%" height="23"> 
            </td>
            <td nowrap height="23" colspan="3"> 
                <eibsinput:text name="brnDetails" property="E01BRNAD2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" />
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="16%" height="23"> 
            </td>
            <td nowrap height="23" colspan="3"> 
                <eibsinput:text name="brnDetails" property="E01BRNAD3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" />
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Ciudad :</div>
            </td>
            <td nowrap height="19" colspan="3"> 
               <eibsinput:cnofc name="brnDetails" flag="84" property="E01BRNCTC" fn_description="E01BRNCIT" required="false"/>
               <eibsinput:text
               		name="brnDetails" property="E01BRNCIT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
             </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Regi&oacute;n :</div>
            </td>
            <td nowrap height="19" colspan="3"> 
               <eibsinput:cnofc name="brnDetails" flag="07" property="E01BRNRGN" fn_description="E01RGNNME" required="false"/>
               <eibsinput:text
               		name="brnDetails" property="E01RGNNME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
           </td>
          </tr>
          <tr id="trdark">
            <td nowrap width="16%" height="19"> 
              <div align="right">Localidad :</div>
            </td>
            <td nowrap height="19" colspan="3"> 
            <eibsinput:cnofc name="brnDetails" flag="LC" property="E01BRNSBR" fn_description="E01SBRNME" required="false"/>
              <eibsinput:text
               		name="brnDetails" property="E01SBRNME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
          </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Telef&oacute;no :</div>
            </td>
            <td nowrap width="40%" height="19" colspan="3"> 
                <eibsinput:text name="brnDetails" property="E01BRNPHN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>" />
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Identificaci&oacute;n : </div>
            </td>
            <td nowrap width="28%" height="19" colspan="3"> 
               <eibsinput:text name="brnDetails" property="E01BRNDID" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" />
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width="16%"> 
              <div align="right">Notario :</div>
            </td>
            <td nowrap width="28%" colspan="3"> 
              <eibsinput:help name="brnDetails" property="E01BRNBKC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" 
          	    	fn_param_one="E01BRNBKC" fn_param_two="E01BKCNME" fn_param_three="N" />
               <eibsinput:text	name="brnDetails" property="E01BKCNME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
 			</td>
          </tr>

          <tr id="trdark"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Oficial Principal : </div>
            </td>
            <td nowrap width="28%" height="19" colspan="3"> 
               <eibsinput:cnofc name="brnDetails" flag="15" property="E01BRNOFC" fn_description="E01OFCNME" required="false"/>
               <eibsinput:text	name="brnDetails" property="E01OFCNME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
 	        </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Sub-Oficial :</div>
            </td>
            <td nowrap width="40%" height="19" colspan="3"> 
               <eibsinput:cnofc name="brnDetails" flag="15" property="E01BRNOF2" fn_description="E01OF2NME" required="false"/>
               <eibsinput:text name="brnDetails" property="E01OF2NME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
          </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right">Oficina Matriz :</div>
            </td>
	            <td nowrap width="28%"> 
             <eibsinput:help name="brnDetails" property="E01BRNMNB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH%>" required="false"
          	    	fn_param_one="E01BRNMNB" fn_param_two="document.forms[0].E01BRNBNK.value"/>
              <eibsinput:text name="brnDetails" property="E01MNBNME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
	        </td>
            <td nowrap width="16%" height="19"> 
              <div align="right">Tipo de Oficina :</div>
            </td>
            <td nowrap width="28%" height="19"> 
               	<SELECT name="E01BRNFL1">
					<OPTION value="L"
						<%if (brnDetails.getE01BRNFL1().equals("L")) { out.print("selected"); }%>>Local
					</OPTION>
					<OPTION value="I"
						<%if (brnDetails.getE01BRNFL1().equals("I")) { out.print("selected"); }%>>Internacional
					</OPTION>
					<OPTION value="M"
						<%if (brnDetails.getE01BRNFL1().equals("M")) { out.print("selected"); }%>>Casa Matriz
					</OPTION>
					<OPTION value="O"
						<%if (brnDetails.getE01BRNFL1().equals("O")) { out.print("selected"); }%>>Otra
					</OPTION>
				</SELECT>
			</td>

          </tr>
          
          <tr id="trclear"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Centro de Canje :</div>
            </td>
            <td nowrap width="40%" height="19"> 
             <eibsinput:help name="brnDetails" property="E01BRNCLB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH%>" required="false"
          	    	fn_param_one="E01BRNCLB" fn_param_two="document.forms[0].E01BRNBNK.value"/>
              <eibsinput:text name="brnDetails" property="E01CLRNME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
            </td>
            <%if(currUser.getE01INT().equals("04")){%>
            <td nowrap width="16%" height="19"> 
              <div align="right">Iva Preferencial :</div>
            </td>
            <% } else {%>
            <td nowrap width="16%" height="19"> 
              <div align="right">Canje Autom&aacute;tico :</div>
            </td>
            <%}%>
            <td nowrap width="28%" height="19"> 
              <input type="radio" name="E01BRNFL2" value="Y"  <%if (brnDetails.getE01BRNFL2().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01BRNFL2" value="N"  <%if (brnDetails.getE01BRNFL2().equals("N")) out.print("checked"); %>>
              No</td>
          </tr>
          
        </table>
      </td>
    </tr>
  </table>
  <br>
          <div align="center"> 
            <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
          </div>
  </form>
</body>
</HTML>
