package datapro.eibs.beans;

import datapro.eibs.sockets.*;
import java.io.*;
import java.math.*;

import java.util.*;

/**
* Class generated by AS/400 CRTCLASS command from ECO500001 physical file definition.
* 
* File level identifier is 1170413161413.
* Record format level identifier is 40FEC72BC1CF3.
*/

public class ECO500001Message extends MessageRecord
{
  final static String fldnames[] = {
                                     "H01USERID",
                                     "H01PROGRM",
                                     "H01TIMSYS",
                                     "H01SCRCOD",
                                     "H01OPECOD",
                                     "H01FLGMAS",
                                     "H01FLGWK1",
                                     "H01FLGWK2",
                                     "H01FLGWK3",
                                     "E01BLONUM",
                                     "E01DESNUM",
                                     "E01BLOCUN",
                                     "E01DESCUN",
                                     "E01BLOSTS",
                                     "E01DESSTS",
                                     "E01BLOBLK",
                                     "E01DESBLK",
                                     "E01BLOFIM",
                                     "E01BLOFID",
                                     "E01BLOFIY",
                                     "E01BLODIM",
                                     "E01BLODID",
                                     "E01BLODIY",
                                     "E01BLOLMT",
                                     "E01BLOLMU",
                                     "E01BLOLM1",
                                     "E01BLOLM2"
                                   };
  final static String tnames[] = {
                                   "H01USERID",
                                   "H01PROGRM",
                                   "H01TIMSYS",
                                   "H01SCRCOD",
                                   "H01OPECOD",
                                   "H01FLGMAS",
                                   "H01FLGWK1",
                                   "H01FLGWK2",
                                   "H01FLGWK3",
                                   "E01BLONUM",
                                   "E01DESNUM",
                                   "E01BLOCUN",
                                   "E01DESCUN",
                                   "E01BLOSTS",
                                   "E01DESSTS",
                                   "E01BLOBLK",
                                   "E01DESBLK",
                                   "E01BLOFIM",
                                   "E01BLOFID",
                                   "E01BLOFIY",
                                   "E01BLODIM",
                                   "E01BLODID",
                                   "E01BLODIY",
                                   "E01BLOLMT",
                                   "E01BLOLMU",
                                   "E01BLOLM1",
                                   "E01BLOLM2"
                                 };
  final static String fid = "1170413161413";
  final static String rid = "40FEC72BC1CF3";
  final static String fmtname = "ECO500001";
  final int FIELDCOUNT = 27;
  private static Hashtable tlookup = new Hashtable();
  private CharacterField fieldH01USERID = null;
  private CharacterField fieldH01PROGRM = null;
  private CharacterField fieldH01TIMSYS = null;
  private CharacterField fieldH01SCRCOD = null;
  private CharacterField fieldH01OPECOD = null;
  private CharacterField fieldH01FLGMAS = null;
  private CharacterField fieldH01FLGWK1 = null;
  private CharacterField fieldH01FLGWK2 = null;
  private CharacterField fieldH01FLGWK3 = null;
  private DecimalField fieldE01BLONUM = null;
  private CharacterField fieldE01DESNUM = null;
  private DecimalField fieldE01BLOCUN = null;
  private CharacterField fieldE01DESCUN = null;
  private CharacterField fieldE01BLOSTS = null;
  private CharacterField fieldE01DESSTS = null;
  private CharacterField fieldE01BLOBLK = null;
  private CharacterField fieldE01DESBLK = null;
  private DecimalField fieldE01BLOFIM = null;
  private DecimalField fieldE01BLOFID = null;
  private DecimalField fieldE01BLOFIY = null;
  private DecimalField fieldE01BLODIM = null;
  private DecimalField fieldE01BLODID = null;
  private DecimalField fieldE01BLODIY = null;
  private CharacterField fieldE01BLOLMT = null;
  private CharacterField fieldE01BLOLMU = null;
  private CharacterField fieldE01BLOLM1 = null;
  private CharacterField fieldE01BLOLM2 = null;

  /**
  * Constructor for ECO500001Message.
  */
  public ECO500001Message()
  {
    createFields();
    initialize();
  }

  /**
  * Create fields for this message.
  * This method implements the abstract method in the MessageRecord superclass.
  */
  protected void createFields()
  {
    recordsize = 340;
    fileid = fid;
    recordid = rid;
    message = new byte[getByteLength()];
    formatname = fmtname;
    fieldnames = fldnames;
    tagnames = tnames;
    fields = new MessageField[FIELDCOUNT];

    fields[0] = fieldH01USERID =
    new CharacterField(message, HEADERSIZE + 0, 10, "H01USERID");
    fields[1] = fieldH01PROGRM =
    new CharacterField(message, HEADERSIZE + 10, 10, "H01PROGRM");
    fields[2] = fieldH01TIMSYS =
    new CharacterField(message, HEADERSIZE + 20, 12, "H01TIMSYS");
    fields[3] = fieldH01SCRCOD =
    new CharacterField(message, HEADERSIZE + 32, 2, "H01SCRCOD");
    fields[4] = fieldH01OPECOD =
    new CharacterField(message, HEADERSIZE + 34, 4, "H01OPECOD");
    fields[5] = fieldH01FLGMAS =
    new CharacterField(message, HEADERSIZE + 38, 1, "H01FLGMAS");
    fields[6] = fieldH01FLGWK1 =
    new CharacterField(message, HEADERSIZE + 39, 1, "H01FLGWK1");
    fields[7] = fieldH01FLGWK2 =
    new CharacterField(message, HEADERSIZE + 40, 1, "H01FLGWK2");
    fields[8] = fieldH01FLGWK3 =
    new CharacterField(message, HEADERSIZE + 41, 1, "H01FLGWK3");
    fields[9] = fieldE01BLONUM =
    new DecimalField(message, HEADERSIZE + 42, 13, 0, "E01BLONUM");
    fields[10] = fieldE01DESNUM =
    new CharacterField(message, HEADERSIZE + 55, 45, "E01DESNUM");
    fields[11] = fieldE01BLOCUN =
    new DecimalField(message, HEADERSIZE + 100, 10, 0, "E01BLOCUN");
    fields[12] = fieldE01DESCUN =
    new CharacterField(message, HEADERSIZE + 110, 45, "E01DESCUN");
    fields[13] = fieldE01BLOSTS =
    new CharacterField(message, HEADERSIZE + 155, 1, "E01BLOSTS");
    fields[14] = fieldE01DESSTS =
    new CharacterField(message, HEADERSIZE + 156, 45, "E01DESSTS");
    fields[15] = fieldE01BLOBLK =
    new CharacterField(message, HEADERSIZE + 201, 4, "E01BLOBLK");
    fields[16] = fieldE01DESBLK =
    new CharacterField(message, HEADERSIZE + 205, 45, "E01DESBLK");
    fields[17] = fieldE01BLOFIM =
    new DecimalField(message, HEADERSIZE + 250, 3, 0, "E01BLOFIM");
    fields[18] = fieldE01BLOFID =
    new DecimalField(message, HEADERSIZE + 253, 3, 0, "E01BLOFID");
    fields[19] = fieldE01BLOFIY =
    new DecimalField(message, HEADERSIZE + 256, 3, 0, "E01BLOFIY");
    fields[20] = fieldE01BLODIM =
    new DecimalField(message, HEADERSIZE + 259, 3, 0, "E01BLODIM");
    fields[21] = fieldE01BLODID =
    new DecimalField(message, HEADERSIZE + 262, 3, 0, "E01BLODID");
    fields[22] = fieldE01BLODIY =
    new DecimalField(message, HEADERSIZE + 265, 3, 0, "E01BLODIY");
    fields[23] = fieldE01BLOLMT =
    new CharacterField(message, HEADERSIZE + 268, 26, "E01BLOLMT");
    fields[24] = fieldE01BLOLMU =
    new CharacterField(message, HEADERSIZE + 294, 10, "E01BLOLMU");
    fields[25] = fieldE01BLOLM1 =
    new CharacterField(message, HEADERSIZE + 304, 26, "E01BLOLM1");
    fields[26] = fieldE01BLOLM2 =
    new CharacterField(message, HEADERSIZE + 330, 10, "E01BLOLM2");

    synchronized (tlookup)
    {
      if (tlookup.isEmpty())
      {
        for (int i = 0; i < tnames.length; i++)
          tlookup.put(tnames[i], new Integer(i));
      }
    }

    taglookup = tlookup;
  }

  /**
  * Set field H01USERID using a String value.
  */
  public void setH01USERID(String newvalue)
  {
    fieldH01USERID.setString(newvalue);
  }

  /**
  * Get value of field H01USERID as a String.
  */
  public String getH01USERID()
  {
    return fieldH01USERID.getString();
  }

  /**
  * Set field H01PROGRM using a String value.
  */
  public void setH01PROGRM(String newvalue)
  {
    fieldH01PROGRM.setString(newvalue);
  }

  /**
  * Get value of field H01PROGRM as a String.
  */
  public String getH01PROGRM()
  {
    return fieldH01PROGRM.getString();
  }

  /**
  * Set field H01TIMSYS using a String value.
  */
  public void setH01TIMSYS(String newvalue)
  {
    fieldH01TIMSYS.setString(newvalue);
  }

  /**
  * Get value of field H01TIMSYS as a String.
  */
  public String getH01TIMSYS()
  {
    return fieldH01TIMSYS.getString();
  }

  /**
  * Set field H01SCRCOD using a String value.
  */
  public void setH01SCRCOD(String newvalue)
  {
    fieldH01SCRCOD.setString(newvalue);
  }

  /**
  * Get value of field H01SCRCOD as a String.
  */
  public String getH01SCRCOD()
  {
    return fieldH01SCRCOD.getString();
  }

  /**
  * Set field H01OPECOD using a String value.
  */
  public void setH01OPECOD(String newvalue)
  {
    fieldH01OPECOD.setString(newvalue);
  }

  /**
  * Get value of field H01OPECOD as a String.
  */
  public String getH01OPECOD()
  {
    return fieldH01OPECOD.getString();
  }

  /**
  * Set field H01FLGMAS using a String value.
  */
  public void setH01FLGMAS(String newvalue)
  {
    fieldH01FLGMAS.setString(newvalue);
  }

  /**
  * Get value of field H01FLGMAS as a String.
  */
  public String getH01FLGMAS()
  {
    return fieldH01FLGMAS.getString();
  }

  /**
  * Set field H01FLGWK1 using a String value.
  */
  public void setH01FLGWK1(String newvalue)
  {
    fieldH01FLGWK1.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK1 as a String.
  */
  public String getH01FLGWK1()
  {
    return fieldH01FLGWK1.getString();
  }

  /**
  * Set field H01FLGWK2 using a String value.
  */
  public void setH01FLGWK2(String newvalue)
  {
    fieldH01FLGWK2.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK2 as a String.
  */
  public String getH01FLGWK2()
  {
    return fieldH01FLGWK2.getString();
  }

  /**
  * Set field H01FLGWK3 using a String value.
  */
  public void setH01FLGWK3(String newvalue)
  {
    fieldH01FLGWK3.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK3 as a String.
  */
  public String getH01FLGWK3()
  {
    return fieldH01FLGWK3.getString();
  }

  /**
  * Set field E01BLONUM using a String value.
  */
  public void setE01BLONUM(String newvalue)
  {
    fieldE01BLONUM.setString(newvalue);
  }

  /**
  * Get value of field E01BLONUM as a String.
  */
  public String getE01BLONUM()
  {
    return fieldE01BLONUM.getString();
  }

  /**
  * Set numeric field E01BLONUM using a BigDecimal value.
  */
  public void setE01BLONUM(BigDecimal newvalue)
  {
    fieldE01BLONUM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01BLONUM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01BLONUM()
  {
    return fieldE01BLONUM.getBigDecimal();
  }

  /**
  * Set field E01DESNUM using a String value.
  */
  public void setE01DESNUM(String newvalue)
  {
    fieldE01DESNUM.setString(newvalue);
  }

  /**
  * Get value of field E01DESNUM as a String.
  */
  public String getE01DESNUM()
  {
    return fieldE01DESNUM.getString();
  }

  /**
  * Set field E01BLOCUN using a String value.
  */
  public void setE01BLOCUN(String newvalue)
  {
    fieldE01BLOCUN.setString(newvalue);
  }

  /**
  * Get value of field E01BLOCUN as a String.
  */
  public String getE01BLOCUN()
  {
    return fieldE01BLOCUN.getString();
  }

  /**
  * Set numeric field E01BLOCUN using a BigDecimal value.
  */
  public void setE01BLOCUN(BigDecimal newvalue)
  {
    fieldE01BLOCUN.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01BLOCUN as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01BLOCUN()
  {
    return fieldE01BLOCUN.getBigDecimal();
  }

  /**
  * Set field E01DESCUN using a String value.
  */
  public void setE01DESCUN(String newvalue)
  {
    fieldE01DESCUN.setString(newvalue);
  }

  /**
  * Get value of field E01DESCUN as a String.
  */
  public String getE01DESCUN()
  {
    return fieldE01DESCUN.getString();
  }

  /**
  * Set field E01BLOSTS using a String value.
  */
  public void setE01BLOSTS(String newvalue)
  {
    fieldE01BLOSTS.setString(newvalue);
  }

  /**
  * Get value of field E01BLOSTS as a String.
  */
  public String getE01BLOSTS()
  {
    return fieldE01BLOSTS.getString();
  }

  /**
  * Set field E01DESSTS using a String value.
  */
  public void setE01DESSTS(String newvalue)
  {
    fieldE01DESSTS.setString(newvalue);
  }

  /**
  * Get value of field E01DESSTS as a String.
  */
  public String getE01DESSTS()
  {
    return fieldE01DESSTS.getString();
  }

  /**
  * Set field E01BLOBLK using a String value.
  */
  public void setE01BLOBLK(String newvalue)
  {
    fieldE01BLOBLK.setString(newvalue);
  }

  /**
  * Get value of field E01BLOBLK as a String.
  */
  public String getE01BLOBLK()
  {
    return fieldE01BLOBLK.getString();
  }

  /**
  * Set field E01DESBLK using a String value.
  */
  public void setE01DESBLK(String newvalue)
  {
    fieldE01DESBLK.setString(newvalue);
  }

  /**
  * Get value of field E01DESBLK as a String.
  */
  public String getE01DESBLK()
  {
    return fieldE01DESBLK.getString();
  }

  /**
  * Set field E01BLOFIM using a String value.
  */
  public void setE01BLOFIM(String newvalue)
  {
    fieldE01BLOFIM.setString(newvalue);
  }

  /**
  * Get value of field E01BLOFIM as a String.
  */
  public String getE01BLOFIM()
  {
    return fieldE01BLOFIM.getString();
  }

  /**
  * Set numeric field E01BLOFIM using a BigDecimal value.
  */
  public void setE01BLOFIM(BigDecimal newvalue)
  {
    fieldE01BLOFIM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01BLOFIM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01BLOFIM()
  {
    return fieldE01BLOFIM.getBigDecimal();
  }

  /**
  * Set field E01BLOFID using a String value.
  */
  public void setE01BLOFID(String newvalue)
  {
    fieldE01BLOFID.setString(newvalue);
  }

  /**
  * Get value of field E01BLOFID as a String.
  */
  public String getE01BLOFID()
  {
    return fieldE01BLOFID.getString();
  }

  /**
  * Set numeric field E01BLOFID using a BigDecimal value.
  */
  public void setE01BLOFID(BigDecimal newvalue)
  {
    fieldE01BLOFID.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01BLOFID as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01BLOFID()
  {
    return fieldE01BLOFID.getBigDecimal();
  }

  /**
  * Set field E01BLOFIY using a String value.
  */
  public void setE01BLOFIY(String newvalue)
  {
    fieldE01BLOFIY.setString(newvalue);
  }

  /**
  * Get value of field E01BLOFIY as a String.
  */
  public String getE01BLOFIY()
  {
    return fieldE01BLOFIY.getString();
  }

  /**
  * Set numeric field E01BLOFIY using a BigDecimal value.
  */
  public void setE01BLOFIY(BigDecimal newvalue)
  {
    fieldE01BLOFIY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01BLOFIY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01BLOFIY()
  {
    return fieldE01BLOFIY.getBigDecimal();
  }

  /**
  * Set field E01BLODIM using a String value.
  */
  public void setE01BLODIM(String newvalue)
  {
    fieldE01BLODIM.setString(newvalue);
  }

  /**
  * Get value of field E01BLODIM as a String.
  */
  public String getE01BLODIM()
  {
    return fieldE01BLODIM.getString();
  }

  /**
  * Set numeric field E01BLODIM using a BigDecimal value.
  */
  public void setE01BLODIM(BigDecimal newvalue)
  {
    fieldE01BLODIM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01BLODIM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01BLODIM()
  {
    return fieldE01BLODIM.getBigDecimal();
  }

  /**
  * Set field E01BLODID using a String value.
  */
  public void setE01BLODID(String newvalue)
  {
    fieldE01BLODID.setString(newvalue);
  }

  /**
  * Get value of field E01BLODID as a String.
  */
  public String getE01BLODID()
  {
    return fieldE01BLODID.getString();
  }

  /**
  * Set numeric field E01BLODID using a BigDecimal value.
  */
  public void setE01BLODID(BigDecimal newvalue)
  {
    fieldE01BLODID.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01BLODID as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01BLODID()
  {
    return fieldE01BLODID.getBigDecimal();
  }

  /**
  * Set field E01BLODIY using a String value.
  */
  public void setE01BLODIY(String newvalue)
  {
    fieldE01BLODIY.setString(newvalue);
  }

  /**
  * Get value of field E01BLODIY as a String.
  */
  public String getE01BLODIY()
  {
    return fieldE01BLODIY.getString();
  }

  /**
  * Set numeric field E01BLODIY using a BigDecimal value.
  */
  public void setE01BLODIY(BigDecimal newvalue)
  {
    fieldE01BLODIY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01BLODIY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01BLODIY()
  {
    return fieldE01BLODIY.getBigDecimal();
  }

  /**
  * Set field E01BLOLMT using a String value.
  */
  public void setE01BLOLMT(String newvalue)
  {
    fieldE01BLOLMT.setString(newvalue);
  }

  /**
  * Get value of field E01BLOLMT as a String.
  */
  public String getE01BLOLMT()
  {
    return fieldE01BLOLMT.getString();
  }

  /**
  * Set field E01BLOLMU using a String value.
  */
  public void setE01BLOLMU(String newvalue)
  {
    fieldE01BLOLMU.setString(newvalue);
  }

  /**
  * Get value of field E01BLOLMU as a String.
  */
  public String getE01BLOLMU()
  {
    return fieldE01BLOLMU.getString();
  }

  /**
  * Set field E01BLOLM1 using a String value.
  */
  public void setE01BLOLM1(String newvalue)
  {
    fieldE01BLOLM1.setString(newvalue);
  }

  /**
  * Get value of field E01BLOLM1 as a String.
  */
  public String getE01BLOLM1()
  {
    return fieldE01BLOLM1.getString();
  }

  /**
  * Set field E01BLOLM2 using a String value.
  */
  public void setE01BLOLM2(String newvalue)
  {
    fieldE01BLOLM2.setString(newvalue);
  }

  /**
  * Get value of field E01BLOLM2 as a String.
  */
  public String getE01BLOLM2()
  {
    return fieldE01BLOLM2.getString();
  }

}
