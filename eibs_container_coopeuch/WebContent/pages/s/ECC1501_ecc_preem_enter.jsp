<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Preemisi�n de Tarjeta de Cr�dito</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="date" class="java.util.Date" scope="session" />      
<jsp:useBean id= "PreemHeader" class= "datapro.eibs.beans.ECC150101Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">

function addDate(){

 date = new Date();
 var month_ant = date.getMonth();
 var month = date.getMonth()+1;
 var day = date.getDate();
 var year = date.getFullYear();

 if (document.getElementById('fecha1').value == '')
	document.getElementById('fecha1').value = day;

 if (document.getElementById('fecha2').value == '')
	document.getElementById('fecha2').value = month_ant;

 if (document.getElementById('fecha3').value == '')
	document.getElementById('fecha3').value = year;

 if (document.getElementById('fecha4').value == '')
	document.getElementById('fecha4').value = day;

 if (document.getElementById('fecha5').value == '')
	document.getElementById('fecha5').value = month;

 if (document.getElementById('fecha6').value == '')
	document.getElementById('fecha6').value = year;

}
</script>
 
 

</head>

<body  onload="addDate();">
 
<H3 align="center">
		<%if(request.getAttribute("opt").equals("01"))
			out.print("Gesti�n de Preemisi�n de Tarjeta de Cr�dito");
		  else if(request.getAttribute("opt").equals("02"))
			out.print("Autorizaci�n de Preemisi�n de Tarjeta de Cr�dito");
		  else 
			out.print("Consulta de Preemisi�n de Tarjeta de Cr�dito");
		%>
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ECC_Preem_enter.jsp, ECC1500"></H3>

<hr size="4"> 
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC1501" >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="1200">
    <INPUT TYPE=HIDDEN NAME="opt" VALUE="<%=request.getAttribute("opt")%>">
 
<table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">ID Interfaz : 
             </td>
             <td nowrap width="20%" align="left">
	 	         <input type="text" name="E01CCHIDC" size="15" maxlength="14" value="<%=PreemHeader.getE01CCHIDC()%>">
             </td>                 

			<td nowrap width="15%" align="right">Fecha Desde : 
            </td>
            <td nowrap width="20%"align="left">
  				<div align="left" > 
                <input type="text" name="E01CCHFDD" id="fecha1" size="3" maxlength="2" value="" readonly="readonly">
                <input type="text" name="E01CCHFDM" id="fecha2" size="3" maxlength="2" value="" readonly="readonly">
                <input type="text" name="E01CCHFDY" id="fecha3" size="5" maxlength="4" value="" readonly="readonly">
                <a href="javascript:DatePicker(document.forms[0].E01CCHFDD,document.forms[0].E01CCHFDM,document.forms[0].E01CCHFDY)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
              </div>
             </td>         

			<td nowrap width="15%" align="right">Fecha Hasta : 
            </td>

            <td nowrap width="20%" align="left" >
  				<div align="left" > 
  			
                <input type="text" name="E01CCHFHD" id="fecha4" size="3" maxlength="2" value="" readonly="readonly">
                <input type="text" name="E01CCHFHM" id="fecha5" size="3" maxlength="2" value="" readonly="readonly">
                <input type="text" name="E01CCHFHY" id="fecha6" size="5" maxlength="4" value="" readonly="readonly">
                <a href="javascript:DatePicker(document.forms[0].E01CCHFHD,document.forms[0].E01CCHFHM,document.forms[0].E01CCHFHY)">
                <img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0" >
                </a> 	
              </div>
             </td>         

         </tr>
    
         <tr>
             <td nowrap width="10%" align="right">Estado :
             </td>
             <td nowrap width="20%"align="left">
						<select name="E01CCHSTS">
		<%if(request.getAttribute("opt").equals("01")){ %>
 		                    <option value="T">TODOS</option>
        				    <option value="C" >CARGADO</option>
             				<option value="V" >VALIDADO</option>
		<%}else if(request.getAttribute("opt").equals("02")){%>
             				<option value="V" >VALIDADO</option>
		<%} else {%>
 		                    <option value="T">TODOS</option>
        				    <option value="C" >CARGADO</option>
             				<option value="V" >VALIDADO</option>
             				<option value="A" >AUTORIZADO</option>
             				<option value="E" >ENVIADO</option>
            				<option value="R" >RECHAZADO</option>
		<%}%>
            				
            			</select>
			</td>

            <td nowrap  align="right" colspan="4"></td>
         </tr>
        </table>
      </td>
    </tr>
  </table>

  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
<script language="JavaScript">
  document.forms[0].E01CCHIDC.focus();
  document.forms[0].E01CCHIDC.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
