package datapro.eibs.products;

/**
 * Aprobación de Convenios
 * Creation date: (03/07/12)
 * @author: Catalina Sepulveda
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD079301Message;
import datapro.eibs.beans.ESD079302Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSESD0793 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_CHG_DEAL_STATUS_APPROVAL_LIST = 500;
	protected static final int R_PASSWORD = 100;	
	protected static final int R_CHG_DEAL_STATUS_APROVE = 200;	
	protected static final int R_CHG_DEAL_STATUS_REJECT = 300;
	
	protected static final String APPROVE = "A";
	protected static final String REJECT = "R";
	

	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		switch (screen) {
			case R_CHG_DEAL_STATUS_APPROVAL_LIST:
				procReqChgDealStatusApprovalList(user, req, res, session);
				break;		
			case R_CHG_DEAL_STATUS_APROVE:
				procActionChgDealStatusApproveReject(user, req, res, session, APPROVE);
				break;
			case R_CHG_DEAL_STATUS_REJECT:
				procActionChgDealStatusApproveReject(user, req, res, session, REJECT);
				break;	
			case R_PASSWORD :
				procReqPassword(req, res, session);
				break;				
			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}

	/**
	 * procActionApproveReject
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @param option
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionChgDealStatusApproveReject( 
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession session, String option)
			throws ServletException, IOException {
			
			UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
			
			MessageProcessor mp = null;
			
			try {
				
				mp = getMessageProcessor("ESD0793", req);
				JBObjList list = (JBObjList)session.getAttribute("Lista");
				int index = req.getParameter("key")==null?0:req.getParameter("key").equals("")?0:Integer.parseInt(req.getParameter("key"));
				
				ESD079301Message listMessage = (ESD079301Message)list.get(index);

				ESD079302Message msg = (ESD079302Message) mp.getMessageRecord("ESD079302", user.getH01USR(), "0002");
			 	msg.setE02ACCION(option);
			 	msg.setE02COTCDE(listMessage.getE01COTCDE());
			 	msg.setE02COTNUM(listMessage.getE01COTNUM());
			 	
			 	mp.sendMessage(msg);
			 	
			 	ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
	            
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				
				if (!mp.hasError(msgError)){
					procReqChgDealStatusApprovalList(user, req, res, session);
				} else {
					session.setAttribute("convObj", msg);
					forward("ESD0793_change_deal_status_approval_list.jsp", req, res);
				}
			} finally {
				if (mp != null)	mp.close();
			}
	    }	
	

	/**
	 * LCrosby 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException 
	 * @throws IOException
	 */
	protected void procReqChgDealStatusApprovalList(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession session)
			throws ServletException, IOException {
			
			MessageProcessor mp = null;
			ELEERRMessage msgError = null;

			
			try {
				mp = getMessageProcessor("ESD0793", req);

				ESD079301Message msgList = (ESD079301Message) mp.getMessageRecord("ESD079301", user.getH01USR(), "0001");
				mp.sendMessage(msgList);
			 	
				msgError = (ELEERRMessage) mp.receiveMessageRecord();
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
				
				if (mp.hasError(msgError)) {
					session.setAttribute("error", mp.getError(list));
					forward(sckNotRespondPage, req, res);	
				} else {
					session.setAttribute("Lista", list);
					forwardOnSuccess("ESD0793_change_deal_status_approval_list.jsp", req, res);
				}	

			} finally {
				if (mp != null)	mp.close();
			}
		}	

	/**
	 * procReqInquiry
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPassword(
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession ses)
			throws ServletException, IOException {
			flexLog ("Entre en procReqPassword ");
			UserPos	userPO = null;
			
			try {
			userPO = (datapro.eibs.beans.UserPos)ses.getAttribute("userPO");
			userPO.setRedirect("/servlet/datapro.eibs.products.JSESD0793?SCREEN=" + R_CHG_DEAL_STATUS_APPROVAL_LIST);
			ses.setAttribute("userPO", userPO);
			res.sendRedirect(super.srctx + "/servlet/datapro.eibs.menu.JSESS0030?SCREEN=7");
			
			}
			catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
			}	
			
			}
 }	



