<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page import = "datapro.eibs.master.Util" %>
<HTML>
<HEAD>
<TITLE>
Impresi�n Historico de Eventos 
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
</HEAD>

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "cifList" class= "datapro.eibs.beans.JBList"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

function doPrint(){
	if(!window.print){
       var msg ="Debe actualizar su navegador para imprimir";
	   alert(msg);
	   return;}

function checkSubmit(){
 document.forms[0].submit();
}
	
    window.focus();
	window.print();

	return;
}


</SCRIPT>

<BODY onload="doPrint()">

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%> 


<FORM>
  <h3 align="center">Historico de Eventos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="his_list_print_fp.jsp,EAPR010"> 
  </h3>
  <hr size="4">
  <br>
  <table class="tableinfo">
    <tr > 
      <td nowrap height="94"> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="15%"> 
              <div align="right"> <b>Fechas desde :</b></div>
            </td>
            <td nowrap width="1%">&nbsp;</td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01FRDTE1" size="3" maxlength="2" readonly value="<%= userPO.getHeader9().trim()%>">
                <input type="text" name="E01FRDTE2" size="3" maxlength="2" readonly value="<%= userPO.getHeader10().trim()%>">
                <input type="text" name="E01FRDTE3" size="5" maxlength="4" readonly value="<%= userPO.getHeader11().trim()%>">
              </div>
            </td>
            <td nowrap width="15%"> 
              <div align="right"><b>hasta :</b></div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01TODTE1" size="3" maxlength="2" readonly value="<%= userPO.getHeader12().trim()%>">
                <input type="text" name="E01TODTE2" size="3" maxlength="2" readonly value="<%= userPO.getHeader13().trim()%>">
                <input type="text" name="E01TODTE3" size="5" maxlength="4" readonly value="<%= userPO.getHeader14().trim()%>">
              </div>
            </td>
            <td nowrap width="15%"> 
              <div align="right"><b></b></div>
            </td>
            <td nowrap width="15%"> 
              <div align="right"><b></b></div>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="15%" height="26"> 
              <div align="right"><b>Sucursal :</b></div>
            </td>
            <td nowrap width="1%" height="26">&nbsp;</td>
            <td nowrap width="15%" height="26"> 
              <div align="left"> 
                <input type="text" name="E01HISBRN" size="05" maxlength="04"  readonly value="<%= userPO.getHeader18().trim()%>">
              </div>
            </td>
            <td nowrap width="15%" height="26"> 
              <div align="right"><b>Evento :</b></div>
            </td>
            <td nowrap width="15%" height="26"> 
              <div align="left"> 
                <input type="text" name="E01HISEVN" size="05" maxlength="04"  readonly value="<%= userPO.getHeader17().trim()%>">
              </div>
            </td>
            <td nowrap width="15%"> 
              <div align="right"><b>Aplicacion :</b></div>
            </td>
            <td nowrap width="15%"> 
                <input type="text" name="E01HISACD" size="05" maxlength="04"  readonly value="<%= userPO.getHeader19().trim()%>">
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <BR>
<%
	if ( cifList.getNoResult() ) {
   		out.print("<center><h4>No hay resultados que correspondan a su criterio de b�squeda</h4></center>");
	}
	else {
%>
  <p>&nbsp;</p>
  <TABLE class="tableinfo" >
    <TR> 
      <TH ALIGN=CENTER nowrap width="5%" >Nro<br>Operacion</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Nro<br>Cliente</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Rut</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Nombre</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Ejecutivo<br>Ventas</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Producto</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Convenio</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Fecha<br>Origen</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Fecha<br>Vencimiento</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Plazo</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Moneda</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Tasa</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Saldo<br>Capital</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Interes</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Estado</TH>
      <TH ALIGN=CENTER nowrap width="5%" >Fecha</TH>
    </TR>
    <%
                cifList.initRow();
                while (cifList.getNextRow()) {
                    if (cifList.getFlag().equals("")) {
                    		out.println(cifList.getRecord());
                    }
                }
              %> 
  </TABLE>

  
  <p>&nbsp;</p>
  <h4>&nbsp;</h4>
  <p>&nbsp;</p>
  <p align="left">&nbsp;</p>
  <p>&nbsp;</p>
  <p align=left>&nbsp; </p>

  <%
  }
%> 
  
</FORM>

</BODY>
</HTML>
