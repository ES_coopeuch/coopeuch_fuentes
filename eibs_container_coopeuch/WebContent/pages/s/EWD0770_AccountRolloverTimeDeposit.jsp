<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@page import="datapro.eibs.beans.EWD0205DSMessage"%><HTML>
<HEAD>
<META HTTP-EQUIV="Pragma" CONTENT="No-cache">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<TITLE>Account Rollover Time Deposit Help</TITLE>

<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "helpList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "helpBean" class= "datapro.eibs.beans.EWD0770DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT language="JavaScript">
	setTimeout("top.close()", <%= datapro.eibs.master.JSEIBSProp.getPopUpTimeOut() %>)
</SCRIPT>

<script language="javascript">

//<!-- Hide from old browsers
function selectCodes(code1,amt1) {
	var form = top.opener.document.forms[0];
	form[top.opener.fieldName].value = code1;
	form[top.opener.fieldDesc].value = amt1;  	 	  	   
	top.close();
 }
 
//-->
</script>
</HEAD>
<BODY>
 <h4>Certificados de Depositos a Renegociar</h4>
<form action="<%=request.getContextPath()%>/servlet/datapro.eibs.helps.JSEWD0770">
<INPUT TYPE=HIDDEN NAME="totalRow" VALUE="0">


<% if (helpList.isEmpty()){%>
<br><br>
<TABLE  id="mainTable" class="tableinfo" ALIGN=CENTER>
	<tr>
	<td ALIGN='CENTER'>No Existen Registros para Este Cliente</td>
	</tr>								
</TABLE>

<%} else { %>
			 
  <TABLE  id="mainTable" class="tableinfo" ALIGN=CENTER>
		   <TR id="trdark">
  					<TH ALIGN=CENTER>Cuenta</TH> 
  					<TH ALIGN=CENTER>Producto</TH> 
  					<TH ALIGN=CENTER>Descripcion</TH>
  					<TH ALIGN=CENTER>Monto</TH>
      		   </TR>
			<%	                  	
	
				while (helpList.getNextRow()) {
				
					helpBean = (datapro.eibs.beans.EWD0770DSMessage)helpList.getRecord();
					out.println("<tr>");
					out.println("<td ALIGN='center'><A HREF=\"javascript:selectCodes('" + helpBean.getEWDACC() +"','" + helpBean.getEWDAMT() +"')\">"  + helpBean.getEWDACC() + " </a></td>");
					out.println("<td ALIGN='center'><A HREF=\"javascript:selectCodes('" + helpBean.getEWDACC() +"','" + helpBean.getEWDAMT() +"')\">"  + helpBean.getEWDPRD() + " </a></td>");
					out.println("<td ALIGN='left'><A HREF=\"javascript:selectCodes('" + helpBean.getEWDACC() +"','" + helpBean.getEWDAMT() +"')\">"  + helpBean.getEWDDSC() + " </a></td>");
					out.println("<td ALIGN='left'><A HREF=\"javascript:selectCodes('" + helpBean.getEWDACC() +"','" + helpBean.getEWDAMT() +"')\">"  + helpBean.getEWDAMT()+ " </a></td>");
					out.println("</tr>");					
				}
			%>
	</TABLE>
			<%				
			}
			%>

</form>
</BODY>
</HTML>
