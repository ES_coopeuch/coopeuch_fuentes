package datapro.eibs.salesplatform;

/**
 * Plataforma de Ventas -ingreso / mantenimiento y simulacion de planillas
 * @author evargas   
 */
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.coopeuch.creditos.predictorriesgo.sinacofi2.DatosConsultaSum;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenAntecedentesComercialesResponse;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiPortType;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiSOAPProxy;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiTypeType;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100500Message;
import datapro.eibs.beans.EPV100601Message;
import datapro.eibs.beans.EPV100602Message;
import datapro.eibs.beans.EPV121601Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSEPV1006 extends JSEIBSServlet { 

	private static final long serialVersionUID = -5013250952357918505L;
	 
	protected static final int R_PLATFORM_LIST = 100;
	protected static final int A_PLATFORM_LIST = 101;
	protected static final int R_PLATFORM_NEW = 200;
	protected static final int R_PLATFORM_MAINT = 201;
	protected static final int A_PLATFORM_MAINTENANCE = 600;
	protected static final int R_PLATFORM_SINACOFI = 850;	
	
	/**
	 *  
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_PLATFORM_LIST:
			procReqPlatformList(user, req, res, session);
			break;
		case A_PLATFORM_LIST:
			procActionPlatformList(user, req, res, session);
			break;
		case R_PLATFORM_NEW:
			procReqPlatform(user, req, res, session, "NEW");
			break;
		case R_PLATFORM_MAINT:
			procReqPlatform(user, req, res, session, "MAINTENANCE");
			break;
		case A_PLATFORM_MAINTENANCE:
			procActionMaintenance(user, req, res, session);
			break;
		case R_PLATFORM_SINACOFI:
			procReqPlatformSinacofi(user, req, res, session);
			break;		
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	
	/**
	 * procReqPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
		UserPos userPO = getUserPos(ses);
		userPO.setRedirect("/servlet/datapro.eibs.salesplatform.JSEPV1006?SCREEN=101");
		userPO.setHeader1("Plataforma de Ventas - Ingreso Microcreditos");
		ses.setAttribute("userPO", userPO);
		forward("EPV1000_client_enter.jsp", req, res);
	}
	
	
	/**
	 * procActionPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		String customer = req.getParameter("E01CUN") == null ? "0" : req.getParameter("E01CUN").trim();
		
		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV1006", req);
			EPV100601Message msgList = (EPV100601Message) mp.getMessageRecord("EPV100601", user.getH01USR(), "0015");
			msgList.setE01SELCUN(customer);
			msgList.setE01SELSTS("1");
			// Sends message
			mp.sendMessage(msgList);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			if (mp.hasError(error)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", error);
				forward("EPV1000_client_enter.jsp", req, res);				
			} else {
				EPV100500Message header = (EPV100500Message) list.get(0);	
				userPO.setCusNum(header.getE00CUSCUN());
				userPO.setCusType(header.getE00CUSIDN());
				userPO.setCusName(header.getE00CUSNA1());
				session.setAttribute("userPO", userPO);
				list.remove(0);
				// if there are NO errors display list
				session.setAttribute("EPV100601List", list);
				session.setAttribute("userPO", userPO);							
				forwardOnSuccess("EPV1006_salesplatform_list.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}
		
	/**
	 * procReqPlatform: This Method show a single Sales Platform either for new
	 * registry or maintenance.
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatform(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		MessageProcessor mp = null;		
		try {
			UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
			userPO.setPurpose(option);
			mp = getMessageProcessor("EPV1006", req);

			EPV100602Message msg = null;

			// Creates the message with operation code depending on the option
			if (option.equals("NEW")) {
				// New
				msg = (EPV100602Message) mp.getMessageRecord("EPV100602", user.getH01USR(), "0001");
			} else if (option.equals("MAINTENANCE")) {
				// Maintenance
				msg = (EPV100602Message) mp.getMessageRecord("EPV100602", user.getH01USR(), "0002");
			}           
			// Sets the client number
           
			// Sets the platform number for maintenance
			if (req.getParameter("E01PVMNUM") != null) {
				msg.setE02PVMNUM(req.getParameter("E01PVMNUM"));
			}
          
			if (req.getParameter("E06CODCAM") != null) 
				{
				msg.setE02PVMCAM(req.getParameter("E06CODCAM"));
				}

			if (userPO.getCusNum() != null)
			{
				msg.setE02PVMCUN(userPO.getCusNum());
			}
			else
			{
			  if (req.getParameter("E06PCACUN") != null) 
			  {
				msg.setE02PVMCUN(req.getParameter("E06PCACUN"));				
			  }
				msg.setE02PVMCUN("0");
			}	

			//set only warning 
			if (req.getParameter("WARNING") != null && "A".equals(req.getParameter("WARNING"))) {
				msg.setH02FLGWK2(req.getParameter("WARNING"));//set A value
			}

			// Send message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV100602Message) mp.receiveMessageRecord();
			
			//just to know when maintenance
			if (option.equals("MAINTENANCE")) {
				msg.setH02FLGWK1("M");
				msg.setH02FLGWK2("X");//ya es mantenimiento, debe haber simulado				
			}

			session.setAttribute("platfObj", msg);// set header page
			if (!mp.hasError(msgError)) {
				// if there are no errors go to maintenance page				 
			
				//buscamos los productos para el combo
				try {
					mp = getMessageProcessor("EPV1216", req);
					EPV121601Message msg1 = (EPV121601Message) mp.getMessageRecord("EPV121601");
					msg1.setH01USERID(user.getH01USR());
					msg1.setH01OPECOD("0017");
					msg1.setH01TIMSYS(getTimeStamp());
					//Sends message
					mp.sendMessage(msg1);
					//Receive insurance  list
					JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");		 
					session.setAttribute("EPV121601List", list);
				} catch (Exception e) {
					JBObjList list = new JBObjList();		 
					session.setAttribute("EPV121601List", list);					
					e.printStackTrace();
				}				
				
				flexLog("About to call Page: EPV1006_salesplatform_maintenance.jsp");
				forward("EPV1006_salesplatform_maintenance.jsp", req, res);
			} else {
				// if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1006_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	
	/**
	 *  Process the Maintenance
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			req.setAttribute("SINACOFI",req.getParameter("SINACOFI")) ;//dejar guardado el estado para controlar consulta cedula
		}
		catch (Exception e) {
			req.setAttribute("SINACOFI","");
		}
		
		try {
			mp = getMessageProcessor("EPV1006", req);

			EPV100602Message msg = (EPV100602Message) mp.getMessageRecord("EPV100602", user.getH01USR(), "0003");

			// Sets message with page fields
			msg.setH02SCRCOD("01");
			setMessageRecord(req, msg);
			
			// Sending message
			flexLog("Mensaje enviado...procActionMaintenance .." + msg);
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV100602Message) mp.receiveMessageRecord();

			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				session.setAttribute("platfObj", msg);
				forward("EPV1006_salesplatform_maintenance.jsp", req, res);
				return;
			}			
		} finally {
			if (mp != null)
				mp.close();
		}
		
		
		try {
			mp = getMessageProcessor("EPV1006", req);

			EPV100602Message msg = (EPV100602Message) mp.getMessageRecord("EPV100602", user.getH01USR(), "0005");

			// Sets message with page fields
			msg.setH02SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);
			
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV100602Message) mp.receiveMessageRecord();
			
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			
			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and sho errors
				session.setAttribute("platfObj", msg);
				forward("EPV1006_salesplatform_maintenance.jsp", req, res);			
			} else {
				//TODO: hacer que vaya a evaluacion
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1010?SCREEN=201&E01PVMNUM="+ msg.getE02PVMNUM(), res);				
				// if there are no errors go back to list
			}

		} finally {
			if (mp != null)
				mp.close();
		}		
	}
	
	/**
	 * Metodo que invoca el webservice de sinacofi expuesto por coopeuch
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformSinacofi(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			EPV100602Message msg = (EPV100602Message) session.getAttribute("platfObj");
			setMessageRecord(req, msg);		
						
			
			try {
				String rut_s = msg.getE02PVMIDN().substring(0, msg.getE02PVMIDN().length()-1);
				String dv_s = msg.getE02PVMIDN().substring(msg.getE02PVMIDN().length()-1);
				BigInteger rut = new BigInteger(rut_s);
				String serie = msg.getE02PVMIDS();				

				String rutEmp_s;
				String dvEmp_s;
				BigInteger rutEmp;
				if (!"1".equals(msg.getE02PVMLGT())){ //solo para distinto de juridico 	 
					rutEmp_s = msg.getE02PVMCPR().substring(0, msg.getE02PVMCPR().length()-1);
					dvEmp_s = msg.getE02PVMCPR().substring(msg.getE02PVMCPR().length()-1);
					rutEmp = new BigInteger(rutEmp_s);
				}else{
					rutEmp_s =rut_s;
					dvEmp_s = dv_s;
					rutEmp =rut;
				}
				DatosConsultaSum parameters = new DatosConsultaSum();			
				parameters.setRut(rut);
				parameters.setDv(dv_s);
				parameters.setSerie(serie);
				parameters.setRutEmpleador(rutEmp);
				parameters.setDvEmpleador(dvEmp_s);
				parameters.setConsultas("A3,A2,A1,B");
				
					ResumenSinacofiSOAPProxy wsp = new ResumenSinacofiSOAPProxy();//inicializamos el servicio					
					ResumenAntecedentesComercialesResponse respWrapper  = wsp.resumenAntecedentesComerciales(parameters);
					if (respWrapper.getCodError()==null || "0".equals(respWrapper.getCodError()) 
							|| "".equals(respWrapper.getCodError().trim())){//no hay errores..
					ResumenSinacofiTypeType resp = respWrapper.getResumenSinacofiType();

					//seteamos las variables que responde
					msg.setE02PVPU12(new BigDecimal(resp.getCantidadProtestoAnual())); //Inf - cantidad protesto 12 meses
					msg.setE02PVP12A(new BigDecimal(resp.getCantidadProtestoAnual())); //Ajus			
					msg.setE02PVPU6I(new BigDecimal(resp.getCantidadProtestoSemestral())); //Inf cantidad protesto 6 meses
					msg.setE02PVPU6A(new BigDecimal(resp.getCantidadProtestoSemestral())); //Ajus	
					msg.setE02PVPMOT(new BigDecimal(resp.getMontoMoraTotal())); //Inf - monto de mora total					
					msg.setE02PVPMOA(new BigDecimal((resp.getMontoMoraTotal()))); //Ajus
					msg.setE02PVPPRO(new BigDecimal(resp.getMontoTotalProtesto())); //Inf 																			
					msg.setE02PVPPRA(new BigDecimal(resp.getMontoTotalProtesto())); //Ajus				
					msg.setE02PVPSCO(Integer.toString(resp.getScore()));
					String dscCed = (resp.getEstadoCedula()==null)?"NO RESPONSE":resp.getEstadoCedula();
					msg.setE02DSCCED(dscCed); //descripcion del edo cedula // codigo de 30
					msg.setE02PVPCED((resp.getCedulaVigente().length()>0)?resp.getCedulaVigente().substring(0,1):resp.getCedulaVigente()); //solo primera letra
					String f = resp.getFechaVencimientoCedula();//dd-mm-yyyy
					if (f!=null && !"".equals(f.trim())){						
						msg.setE02PVPVCD(f.substring(0,2));
						msg.setE02PVPVCM(f.substring(3,5));			
						msg.setE02PVPVCY(f.substring(6));
					}
				}else{
					String c = respWrapper.getCodError();
					c =(c==null)?"N/D":c; 
					String d = respWrapper.getDesError();
					d =(d==null)?"SIN RESPUESTA SERVICIO BUREAU":d;					
					ELEERRMessage msgError = new ELEERRMessage();
	                msgError.setERRNUM("1");
	                msgError.setERNU01((c.length() >4?(c.substring(0,4)):c));		//4		                
	                msgError.setERDS01((d.length() >70?(d.substring(0,70)):d));		//70
					session.setAttribute("error", msgError);									
				}
							
			}catch (Exception e1){
				e1.printStackTrace();
				String d = e1.getMessage();
				d=(d==null)?d=e1.toString():d;					
				//si hay error mandarlos en la pantalla
                ELEERRMessage msgError = new ELEERRMessage();
                msgError.setERRNUM("1");
                msgError.setERNU01("01");
                msgError.setERDS01((d.length() >70?(d.substring(0,70)):d));
				session.setAttribute("error", msgError);			
			}		
			req.setAttribute("SINACOFI", "S");			
			session.setAttribute("platfObj", msg);// seteamos nuevamente con la informacion obtenida
			forward("EPV1006_salesplatform_maintenance.jsp", req, res);

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
	
}
