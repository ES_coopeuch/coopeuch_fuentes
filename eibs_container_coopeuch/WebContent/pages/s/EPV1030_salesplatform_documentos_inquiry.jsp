<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="document" class="datapro.eibs.beans.EPV103001Message"  scope="session" />
<jsp:useBean id="EPV101003List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center"> Plataforma de Ventas - Visado de Documentos<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_documentos_inquiry.jsp,JSEPV1030"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1030">
<input type="hidden" name="SCREEN" value=" "> 
<input type=HIDDEN name="E01PVMBNK"  value="<%= document.getE01PVMBNK().trim()%>">

 <% int row = 0;%>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="document" property="E01PVMCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Nombre :</td>
             <td nowrap align="left" width="40%">
	  			<eibsinput:text name="document" property="E01CUSNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="document" property="E01PVMNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="40%">
    	        <eibsinput:date name="document" fn_year="E01PVMODY" fn_month="E01PVMODM" fn_day="E01PVMODD" readonly="true" readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Sucursal :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="document" property="E01PVMBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
             </td>
             <td nowrap align="right" width="20%"> Ejecutivo :</td>
             <td nowrap align="left" width="40%">
	  			<eibsinput:text name="document" property="E01PVMOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>" readonly="true"/>
             </td>
         </tr>         
        </table>
      </td>
    </tr>
  </table>

<%if(document.getE01LNTYPG().equals("T")){%> 

  <table class=tbenter>
   <tr > 
      <td nowrap> 
		  <h4>Informacion Tarjeta de Credito</h4>
      </td>
      <td nowrap align=right> 
   		<b>Estado :</b>
      </td>
      <td nowrap> 
		  <INPUT TYPE=HIDDEN NAME="E01DSCSTS" VALUE="<%=document.getE01DSCSTS()%>">  
   		<b><font color="#ff6600"><%= document.getE01DSCSTS().trim()%></font></b>
      </td>
    </tr>
  </table>

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="document" property="E01LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="document" property="E01DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Direccion :</div>
            </td>
            <td nowrap width="30%">
              <input type="text" name="E01TCCMLA" size="3" maxlength="2" value="<%= document.getE01TCCMLA().trim()%>" readonly >
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Local :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01TCCUMB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Extranjera :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01TCCUME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Dia de Pago :</div>
            </td>
            <td nowrap width="30%">             
				<select name="E01TCDYPG" disabled>
					<option></option>
					<option value="3" <% if (document.getE01TCDYPG().equals("3")) out.print("selected");%>>3</option>
					<option value="23" <% if (document.getE01TCDYPG().equals("23")) out.print("selected");%>>23</option>
				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Porcentaje de Pago :</div>
            </td>
            <td nowrap width="30%">            
				<select name="E01TCPRPG" disabled>
					<option></option>
					<option value="5" <% if (document.getE01TCPRPG().equals("5")) out.print("selected");%>>El 5%</option>
					<option value="100" <% if (document.getE01TCPRPG().equals("100")) out.print("selected");%>>El 100%</option>
				</select>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">
				<select name="E01TCMEPG" disabled>
					<option></option>
					<option value="1" <% if (document.getE01TCMEPG().equals("1")) out.print("selected");%>>Con Cuenta</option>
					<option value="2" <% if (document.getE01TCMEPG().equals("2")) out.print("selected");%>>Sin Cuenta</option>
				</select>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:text name="document" property="E01TCACPG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
            </td>
          </tr>  

        </table>
      </td>
    </tr>
  </table>

<%} else {%> 

  <table class=tbenter>
   <tr > 
      <td nowrap> 
		  <h4>Informacion del Credito</h4>
      </td>
      <td nowrap align=right> 
   		<b>Estado :</b>
      </td>
      <td nowrap> 
		  <INPUT TYPE=HIDDEN NAME="E01DSCSTS" VALUE="<%=document.getE01DSCSTS()%>">  
   		<b><font color="#ff6600"><%= document.getE01DSCSTS().trim()%></font></b>
      </td>
    </tr>
  </table>

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Tipo de Producto :</div>
            </td>
            <td nowrap width="30%" >
            	<eibsinput:text name="document" property="E01LNTYPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>	
            	<eibsinput:text name="document" property="E01DSTYPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Termino del Contrato :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNTERM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" readonly="true" /> 
	            <input type="text" readonly name="E01LNTERC" size="10" 
				  value="<% if (document.getE01LNTERC().equals("D")) out.print("D&iacute;a(s)");
							else if (document.getE01LNTERC().equals("M")) out.print("Mes(es)");
							else if (document.getE01LNTERC().equals("Y")) out.print("A&ntilde;o(s)");
							else out.print("");%>" >
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="document" property="E01LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="document" property="E01DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Valor de la Cuota :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01LNCUAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
          </tr>                 
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Monto del Prestamo :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01LNOAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Monto Liquido :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01LNNAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
          </tr>  
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Primer Pago :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:date name="document" fn_year="E01LNPXPY" fn_month="E01LNPXPM" fn_day="E01LNPXPD" readonly="true" />
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
            </td>
            <td nowrap width="30%">
            	&nbsp; 
            </td>
          </tr>   
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">              
				<select name="E01LNPVIA" <%="disabled"%>>					
					<option value=""
						<% if (document.getE01LNPVIA().equals("")) out.print("selected");%>>Caja</option>
					<option value="1"
						<% if (document.getE01LNPVIA().equals("1")) out.print("selected");%>>PAC</option>							            
					<option value="2"
						<% if (document.getE01LNPVIA().equals("2")) out.print("selected");%>>Convenio</option>
					<option value="4"
						<% if (document.getE01LNPVIA().equals("4")) out.print("selected");%>>PAC/Multibanco</option>	

				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:text name="document" property="E01LNPACC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
            </td>
          </tr>
          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Promociones :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01DSPROM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tasa Promoción:</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNTSPO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/>
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Descuentos :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01DSDESC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">% Descuento :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNTSDS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
          </tr>  
          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Codigo Convenio :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNCONV" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right"> % descuento del convenio : </div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNDRAT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Tasa de Interes :</div>
            </td>
            <td nowrap width="30%">
            	 <eibsinput:text name="document" property="E01LNRATE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
            </td>
            <td nowrap width="30%">
            	&nbsp;
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Holgura:</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01PVMOLA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
            </td>
            <td nowrap width="30%">
				&nbsp;            	
            </td>
          </tr>   

        </table>
      </td>
    </tr>
  </table>

<%} %>   


  <h4>Documentos Requeridos</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <th nowrap width="5%" align="center">Codigo</th>
            <th nowrap width="30%" align="left">Descripcion </th>
            <th nowrap width="10%" align="center">Estatus</th>
            <th nowrap width="50%" align="center">Comentario</th>
          </tr>
 
          <%
           String sx="";
           for (int ix=1; ix<30; ix++) {
            if (ix<10) sx="0"+ix; else sx=""+ix;
		    if(!document.getField("E01DCOD"+sx).getString().trim().equals("")){
          %> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="5%" align="center"> <%=document.getFieldString("E01DCOD"+sx)%> <input type=HIDDEN name="<%="E01DCOD"+sx%>"  value="<%=document.getFieldString("E01DCOD"+sx) %>"> </td>
            <td nowrap width="30%" align="left">  <%=document.getFieldString("E01DDES"+sx)%> <input type=HIDDEN name="<%="E01DDES"+sx%>"  value="<%=document.getFieldString("E01DDES"+sx) %>"> </td>
            <td nowrap width="10%" align="left">
 		       <p> 
                 <input type="radio" disabled name="<%="E01DSTS"+sx%>"  value="A" <%if (document.getField("E01DSTS"+sx).getString().trim().equals("A")) out.print("checked"); %>>
                  Aprobar <br>
                 <input type="radio" disabled name="<%="E01DSTS"+sx%>"  value="R" <%if (document.getField("E01DSTS"+sx).getString().trim().equals("R")) out.print("checked"); %>>
                   Rechazar 
               </p> 
        	</td>
            <td nowrap width="50%" align="center">
            <textarea  name="<%="E01DRMK"+sx%>"" cols="100" rows="3" readonly><%=document.getFieldString("E01DRMK"+sx)%>  </textarea>
        	</td>
          </tr> 
   		  <%}
   		   }%>
        </table>
        </td>
    </tr>
  </table>    
 
</form>

</body>
</html>
