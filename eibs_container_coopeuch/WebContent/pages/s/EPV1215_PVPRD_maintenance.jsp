<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.EPV121501Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	Consulta Tipos Evaluación Plataforma de Venta
<%} else if (maintenance){ %>
	Mantención Tipos Evaluación Plataforma de Venta
<%} else { %>
	Nuevo  Tipos Evaluación Plataforma de Venta
<%} %>

 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PVPRD_maintenance.jsp, EPV1215"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1215" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01UBK" value="<%= currUser.getE01UBK().trim()%>">
  
 <% int row = 0;%>
 
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Tipo Evaluación :</div>
            </td>
            <td width="70%" > 
	             <eibsinput:text property="E01PRDDOP" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" readonly="true"/>
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Descripción :</div>
            </td>
			<td nowrap width="70%" align="left"><% if (cnvObj.getE01PRDDOP().equals("P")) out.print("PLANILLA"); 
									               if (cnvObj.getE01PRDDOP().equals("D")) out.print("PAGO DIRECTO");
										           if (cnvObj.getE01PRDDOP().equals("S")) out.print("EVALUACION ESPECIAL");	
										           if (cnvObj.getE01PRDDOP().equals("M")) out.print("MICROCREDITO");	
										           if (cnvObj.getE01PRDDOP().equals("E")) out.print("COMPRA CARTERA");
										           if (cnvObj.getE01PRDDOP().equals("T")) out.print("TARJETA DE CREDITO");																			   																			   																		   
										           if (cnvObj.getE01PRDDOP().equals("A")) out.print("AUTOMOTRIZ");%></td>										           
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Producto Corto Plazo :</div>
            </td>
            <td width="70%" > 
              <input type="text" name="E01PRDPRC" size="5" maxlength="4" value="<%= cnvObj.getE01PRDPRC().trim()%>" <%=readOnly?"disabled":""%> >
			<%if  (!readOnly) { %>
              <a href="javascript:GetProduct('E01PRDPRC','10','<%=currUser.getE01UBK() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absmiddle" border="0"></a> 
             <%} %>                             
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Producto Largo Plazo :</div>
            </td>
            <td width="70%" > 
              <input type="text" name="E01PRDPRL" size="5" maxlength="4" value="<%= cnvObj.getE01PRDPRL().trim()%>" <%=readOnly?"disabled":""%> >
			<%if  (!readOnly) { %>
              <a href="javascript:GetProduct('E01PRDPRL','10','<%=currUser.getE01UBK() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absmiddle" border="0"></a> 
             <%} %>                             
	        </td>
          </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="40%" > 
              <div align="right">Flag Garantia :</div>
            </td>
            <td width="60%" > 
 		       <p> 
                 <input type="radio" name="E01PRDFGR"  value="Y" <%if (cnvObj.getE01PRDFGR().equals("Y")) out.print("checked"); %>  <%=readOnly?"disabled":""%> >
                  Si 
                 <input type="radio" name="E01PRDFGR"  value="N" <%if (cnvObj.getE01PRDFGR().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%>>
                  No
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
                               
	        </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>

<%if  (!readOnly) { %>
    <div align="center"> 
        <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    </div>
<% } %>  

  </form>
</body>
</HTML>
