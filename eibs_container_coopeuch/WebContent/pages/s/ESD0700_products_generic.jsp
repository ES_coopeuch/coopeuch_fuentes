<html>
<head>
<title>Consulta de Productos Genericos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<SCRIPT SRC="<%=request.getContextPath()%>/pages/s/javascripts/dynlayer.js"> </SCRIPT>
<SCRIPT SRC="<%=request.getContextPath()%>/pages/s/javascripts/pop_out.js"> </SCRIPT>
<SCRIPT SRC="<%=request.getContextPath()%>/pages/s/javascripts/nav_aid.js"> </SCRIPT>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

</head>

<jsp:useBean id="prd" class="datapro.eibs.beans.ESD070001Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     error.setERRNUM("0");
     out.println("</SCRIPT>");
     }
%>

<h3 align="center">Producto Generico<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="products_generic.jsp, ESD0700"></h3>
<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0700" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="1">
  <INPUT TYPE=HIDDEN NAME="E01APCACD" VALUE="<%= prd.getE01APCACD()%>">
  <table class="tableinfo">
    <tr > 
      <td> 
        <table cellspacing="0" cellpadding="2" width="100%"  class="tbhead"  align="center">
          <tr> 
            <td nowrap width="10%" align="right"> Banco: </td>
            <td nowrap width="12%" align="left"> 
              <input type="text"  name="E01APCBNK" size="3" maxlength="2" value="<%= prd.getE01APCBNK()%>" readonly>
            </td>
            <td nowrap width="6%" align="right"> Producto: </td>
            <td nowrap width="14%" align="left"> 
              <input type="text"  name="E01APCCDE" size="6" maxlength="4" value="<%= prd.getE01APCCDE()%>" readonly>
            </td>
            <td nowrap width="8%" align="right"> Tipo de Producto : </td>
            <td nowrap width="50%"align="left"> 
              <input type="text"  name="E01APCTYP" size="6" maxlength="4" value="<%= prd.getE01APCTYP()%>" readonly>
              - 
              <input type="text"  name="E01DSCTYP" size="45" maxlength="45" value="<%= prd.getE01DSCTYP()%>" readonly>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Informaci&oacute;n General</h4>

  <table class="tableinfo">
    <tr > 
      <td > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td> 
              <div align="right">Descripci&oacute;n :</div>
            </td>
            <td> 
              <input type="text"  name="E01APCDS1" size="50" maxlength="45" value="<%= prd.getE01APCDS1()%>">
            </td>
            <td> 
              <div align="right">Nombre de Mercadeo :</div>
            </td>
            <td> 
              <input type="text"  name="E01APCDS2" size="28" maxlength="25" value="<%= prd.getE01APCDS2()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td > 
              <div align="right">C&oacute;digo de Moneda :</div>
            </td>
            <td > 
              <input type="text"  name="E01APCCCY" size="3" maxlength="3" value="<%= prd.getE01APCCCY()%>">
              <a href="javascript:GetCurrency('E01APCCCY','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a> 
            </td>
            <td > 
              <div align="right">Ofrecimiento por :</div>
            </td>
            <td ><SELECT name="E01APCFTT">
					<OPTION value="1"
						<%if (prd.getE01APCFTT().equals("1")) { out.print("selected"); }%>>Internet</OPTION>
					<OPTION value="I"
						<%if (prd.getE01APCFTT().equals("I")) { out.print("selected"); }%>>Internacional</OPTION>
					<OPTION value="L"
						<%if (prd.getE01APCFTT().equals("L")) { out.print("selected"); }%>>Local</OPTION>
					<OPTION value="3"
						<%if (prd.getE01APCFTT().equals("3")) { out.print("selected"); }%>>Plataforma</OPTION>
					<OPTION value="5"
						<%if (prd.getE01APCFTT().equals("5")) { out.print("selected"); }%>>Cualquier
					Medio</OPTION>
					<OPTION value="N"
						<%if (prd.getE01APCFTT().equals("N")) { out.print("selected"); }%>>No
					Ofrecer</OPTION>
				</SELECT>
            </td>
          </tr>
          <tr id="trdark"> 
            <td > 
              <div align="right"> Cuenta Contable:</div>
            </td>
            <td > 
              <input type="text"  name="E01APCGLN" size="18" maxlength="16" value="<%= prd.getE01APCGLN().trim()%>">
              <a href="javascript:GetLedger('E01APCGLN',document.forms[0].E01APCBNK.value,document.forms[0].E01APCCCY.value,document.forms[0].E01APCACD.value,'','<%= prd.getE01APCTYP().trim()%>')">
				<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0" ></a> 
            </td>
            <td>
            	<div align="right"></div>
          	</td>
          	<td width="25%"> 
            </td>
          </tr>   
		  <tr id="trclear">
	          	<td>
	          		<div align="right">Visualizar Sitio Privado :</div>
	          	</td>
	          	<td >
	          		<input type="radio" name="E01APAFG1" value="Y"  <%if (prd.getE01APAFG1().equals("Y")) out.print("checked"); %>>
	              Si 
	              <input type="radio" name="E01APAFG1" value="N"  <%if (prd.getE01APAFG1().equals("N")) out.print("checked"); %>>
	              No
	            </td>
	            <td>
				</td>
				<td>
				</td>
			</tr>
        </table>
      </td>
    </tr>
  </table>

<!-- IFRS DETERIORO -->
  <h4>IFRS - Deterioro</h4>

  <table class="tableinfo">
    <tr > 
      <td > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td width="25%" > 
              <div align="right">Grupo Deterioro :</div>
            </td>
            <td width="30%" >
            <select name="E01APAF01">
					<option value=""
						<%if (prd.getE01APAF01().equals(""))   { out.print("selected"); }%>>SELECCIONE</option>
					<option value="H"
						<%if (prd.getE01APAF01().equals("H")) { out.print("selected"); }%>>Hipotecario</option>
					<option value="P"
						<%if (prd.getE01APAF01().equals("P")) { out.print("selected"); }%>>Planilla</option>
					<option value="O"
						<%if (prd.getE01APAF01().equals("O")) { out.print("selected"); }%>>Otros</option>
				</select></td>
            <td width="25%"> 
              <div align="right">Arrastre Hipotecario :</div>
            </td>
            <td width="30%"> 
              <input type="radio" name="E01APAF02" value="Y"  <%if (prd.getE01APAF02().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APAF02" value="N"  <%if (prd.getE01APAF02().equals("N")) out.print("checked"); %>>
              No
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="25%" rowspan="2"> 
              <div align="right"></div>
            </td>
            <td width="30%" rowspan="2">            </td>
            <td width="25%"> 
              <div align="right">Arrastre Planilla :</div>
            </td>
            <td width="30%">
            <input type="radio" name="E01APAF03" value="Y"
					<%if (prd.getE01APAF03().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APAF03" value="N"
					<%if (prd.getE01APAF03().equals("N")) out.print("checked"); %>>
              No</td>
          </tr>
          <tr id="trdark"> 
            <td width="25%"> 
              <div align="right">Arrastre Otros :</div>
            </td>
            <td width="30%">
				<input type="radio" name="E01APAF04" value="Y"
					<%if (prd.getE01APAF04().equals("Y")) out.print("checked"); %>>
              Si 
              <input type="radio" name="E01APAF04" value="N"
					<%if (prd.getE01APAF04().equals("N")) out.print("checked"); %>>
              No 
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<!-- FIN IFRS -->



  		<div align="center"> 
    		<input id="EIBSBTN" type=button name="Submit" OnClick="submit()" value="Enviar">
  		</div>
  

 </form>
</body>
</html>
