<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Informacion Basica</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008002Message"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<SCRIPT Language="Javascript">
   builtNewMenu(client_ap_corp_opt);
</SCRIPT>

</head>

<body bgcolor="#FFFFFF">

<SCRIPT> initMenu(); </SCRIPT>

 
<h3 align="center">Informaci&oacute;n Cliente Juridica <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="client_ap_corp_basic, ESD0100" ></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="12">
  <h4>Raz&oacute;n Social</h4>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td nowrap > 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap  width="23%"> 
                <div align="right">No Cliente :</div>
              </td>
              <td nowrap  colspan=3> 
                <input type="text" readonly <% if (client.getF02CUN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02CUN" size="10" maxlength="9" value="<%= client.getE02CUN()%>">
			  </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="44%"> 
                <div align="right">Nombre Legal :</div>
              </td>
              <td nowrap   width="56%">  
                <input type="text" readonly name="E02NA1" <% if (client.getF02NA1().equals("Y")) out.print("id=\"txtchanged\""); %> size="65" maxlength="60" value="<%= client.getE02NA1().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap  width="44%"> 
                <p align="right">Nombre Fantasía :</p>
              </td>
              <td nowrap  width="56%">  
                <input type="text" readonly <% if (client.getF02CP1().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02CP1" size="50" maxlength="45" value="<%= client.getE02CP1().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="44%"> 
                <div align="right">Nombre Corto :</div>
              </td>
              <td nowrap  width="56%">  
                <input type="text" readonly <% if (client.getF02SHN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02SHN" size="28" maxlength="25" value="<%= client.getE02SHN().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap  width="44%"> 
                <div align="right">Identificaci&oacute;n de Central de Riesgo :</div>
              </td>
              <td nowrap  width="56%">  
                <input type="text" readonly <% if (client.getF02FN2().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02FN2" size="45" maxlength="30" value="<%= client.getE02FN2().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="44%"> 
                <div align="right"></div>
              </td>
              <td nowrap  width="56%">  
                <input type="text" readonly <% if (client.getF02LN1().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02LN1" size="45" maxlength="30" value="<%= client.getE02LN1().trim()%>">
                </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

   		<%	if( client.getH02SCR().equals("07")){%> 	
			<jsp:include page="ESD0080_address_template_basic_banesco_panama.jsp" flush="true">
				<jsp:param name="suffix" value="E02" />
				<jsp:param name="title" value="Dirección" />
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
				<jsp:param name="basic" value="true" />
			</jsp:include>

		<% } else if( client.getH02SCR().equals("03")){%> 	 
			<jsp:include page="ESD0080_address_template_venezuela.jsp" flush="true">				
				<jsp:param name="title" value="Dirección" />
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
			</jsp:include>
		
		<%} else if( client.getH02SCR().equals("18")){%> 	
			<jsp:include page="ESD0080_address_template_basic_chile.jsp" flush="true">
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
				<jsp:param name="title" value="Dirección Principal" />
				<jsp:param name="suffix" value="02" />
			</jsp:include>
	
		<% } else {%> 	
			<jsp:include page="ESD0080_address_template_basic_generic.jsp" flush="true">
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
				<jsp:param name="title" value="Dirección" />
				<jsp:param name="suffix" value="02" />
				
			</jsp:include>
	
		<%} %>   
		
    <%if( client.getH02SCR().equals("07")){%> 	
		<jsp:include page="ESD0080_ident_template_Banesco.jsp" flush="true">
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="true" />
			<jsp:param name="title" value="Identificación" />
			<jsp:param name="suffix" value="02" />
		</jsp:include>
	
	<%} else {%> 
		<jsp:include page="ESD0080_ident_template_generic.jsp" flush="true">
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="true" />
			<jsp:param name="title" value="Identificación" />
			<jsp:param name="suffix" value="02" />

		</jsp:include>
	<%} %>   

    <h4>Fechas</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Fecha de Fundaci&oacute;n 
                  :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly <% if (client.getF02BDD().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02BDD" size="3" maxlength="2" value="<%= client.getE02BDD().trim()%>">
                <input type="text" readonly <% if (client.getF02BDM().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02BDM" size="3" maxlength="2" value="<%= client.getE02BDM().trim()%>">
                <input type="text" readonly <% if (client.getF02BDY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02BDY" size="5" maxlength="4" value="<%= client.getE02BDY().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Fecha de Primer Contacto 
                  :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly <% if (client.getF02IDD().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02IDD" size="3" maxlength="2" value="<%= client.getE02IDD().trim()%>">
                <input type="text" readonly <% if (client.getF02IDM().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02IDM" size="3" maxlength="2" value="<%= client.getE02IDM().trim()%>">
                <input type="text" readonly <% if (client.getF02IDY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02IDY" size="5" maxlength="4" value="<%= client.getE02IDY().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Fecha Inicio de Operaciones 
                  :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly <% if (client.getF02IED().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02IED" size="3" maxlength="2" value="<%= client.getE02IED().trim()%>">
                <input type="text" readonly <% if (client.getF02IEM().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02IEM" size="3" maxlength="2" value="<%= client.getE02IEM().trim()%>">                
                <input type="text" readonly <% if (client.getF02IEY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02IEY" size="5" maxlength="4" value="<%= client.getE02IEY().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Fecha de Registro 
                  :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly <% if (client.getF02RED().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02RED" size="3" maxlength="2" value="<%= client.getE02RED().trim()%>">
                <input type="text" readonly <% if (client.getF02REM().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02REM" size="3" maxlength="2" value="<%= client.getE02REM().trim()%>">                
                <input type="text" readonly <% if (client.getF02REY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02REY" size="5" maxlength="4" value="<%= client.getE02REY().trim()%>">
                </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <h4>Tel&eacute;fonos</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Tel&eacute;fono Oficina 1 :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly <% if (client.getF02HPN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02HPN" size="16" maxlength="15" value="<%= client.getE02HPN().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="42%"> 
                <div align="right">Tel&eacute;fono Oficina 2 :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly <% if (client.getF02PHN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02PHN" size="16" maxlength="15" value="<%= client.getE02PHN().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Tel&eacute;fono de Fax :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly <% if (client.getF02FAX().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02FAX" size="16" maxlength="15" value="<%= client.getE02FAX().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Tel&eacute;fono Celular :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly <% if (client.getF02PH1().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02PH1" size="16" maxlength="15" value="<%= client.getE02PH1().trim()%>">
                </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <h4>C&oacute;digo de Clasificaci&oacute;n</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left" >
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Ejecutivo Principal :</div>
              </td>
              <td nowrap width="58%" >  
                <input type="text" readonly <% if (client.getF02OFC().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02OFC" size="45" maxlength="45" value="<%= client.getD02OFC().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Ejecutivo Secundario :</div>
              </td>
              <td nowrap width="58%" > 
                <input type="text" readonly <% if (client.getF02OF2().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02OF2" size="45" maxlength="45" value="<%= client.getD02OF2().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Sector Econ&oacute;mico :</div>
              </td>
              <td nowrap width="58%" >  
                <input type="text" readonly <% if (client.getF02INC().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02INC" size="45" maxlength="45" value="<%= client.getD02INC().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Actividad Econ&oacute;mica :</div>
              </td>
              <td nowrap width="58%" > 
                <input type="text" readonly <% if (client.getF02BUC().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02BUC" size="45" maxlength="45" value="<%= client.getD02BUC().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Pa&iacute;s de Residencia :</div>
              </td>
              <td nowrap width="58%" >  
                <input type="text" readonly <% if (client.getF02GEC().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02GEC" size="45" maxlength="45" value="<%= client.getD02GEC().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Tipo de Relaci&oacute;n :</div>
              </td>
              <td nowrap  width="58%" > 
                <input type="text" readonly <% if (client.getF02UC1().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02UC1" size="45" maxlength="45" value="<%= client.getD02UC1().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Clasificaci&oacute;n :</div>
              </td>
              <td nowrap width="58%" >  
                <input type="text" readonly <% if (client.getF02UC2().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02UC2" size="45" maxlength="45" value="<%= client.getD02UC2().trim()%>">
                </td>
            </tr>

            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Sub Clasificaci&oacute;n :</div>
              </td>
              <td nowrap width="58%" >  
                <input type="text" readonly <% if (client.getF02SCL().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02SCL" size="45" maxlength="45" value="<%= client.getD02SCL().trim()%>">
                </td>
            </tr>

            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Nivel SocioEcon&oacute;mico :</div>
              </td>
              <td nowrap width="58%" > 
                <input type="text" readonly <% if (client.getF02UC3().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02UC3" size="45" maxlength="45" value="<%= client.getD02UC3().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Unidad de Negocio :</div>
              </td>
              <td nowrap width="58%" > 
                <input type="text" readonly <% if (client.getF02UC4().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02UC4" size="45" maxlength="45" value="<%= client.getD02UC4().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Segmento :</div>
              </td>
              <td nowrap width="58%" >  
                <input type="text" readonly <% if (client.getF02UC5().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02UC5" size="45" maxlength="45" value="<%= client.getD02UC5().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">SubSegmento :</div>
              </td>
              <td nowrap width="58%" >  
                <input type="text" readonly <% if (client.getF02UC6().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02UC6" size="45" maxlength="45" value="<%= client.getD02UC6().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Indicador Banco/Cooperativa :</div>
              </td>
              <td nowrap width="58%" >  
                <input type="text" readonly name="E02CRF" size="25" maxlength="25"
				  <% if (client.getF02CRF().equals("Y")) out.print("id=\"txtchanged\""); %>
                  value="<% if (client.getE02CRF().equals("1")) { out.print("Banco Corresponsal"); }
							else if (client.getE02CRF().equals("2")) { out.print("Banco Regular"); }
							else if (client.getE02CRF().equals("3")) { out.print("Cooperativa"); }
    						else if (client.getE02CRF().equals("0")) { out.print("Ninguno"); }
							else { out.print(""); } %>" >
              </td>
            </tr>

            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Con fin de lucro :</div>
              </td>
              <td nowrap width="58%" >  
              		<input type="text" readonly <% if (client.getF02FLC().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02FLC" size="3" maxlength="3" value="<%if (client.getE02FLC().equals("Y")) out.print("SI"); else out.print("NO"); %>">
              </td>
            </tr>
            
          </table>
        </td>
      </tr>
    </table>
    <h4>Datos Adicionales</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap  width="20%"> 
                <div align="right">No. de Registro :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02REN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02REN" size="16" maxlength="15" value="<%= client.getE02REN().trim()%>">
              </td>
              <td nowrap width="20%"> 
                <div align="right">A&ntilde;os Establecidos :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02NSO().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02NSO" size="3" maxlength="2" value="<%= client.getE02NSO().trim()%>">
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="20%"> 
                <div align="right">No. de Acciones :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02NST().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02NST" size="8" maxlength="7" value="<%= client.getE02NST().trim()%>">
              </td>
              <td nowrapwidth="20%" width="18%"> 
                <div align="right">No. de Accionistas :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02NSH().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02NSH" size="8" maxlength="7" value="<%= client.getE02NSH().trim()%>">
              </td>
            </tr>
			
			<tr id="trdark"> 
              <td nowrapwidth="20%" width="22%"> 
                <div align="right">Capital Inicial :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02CAI().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02CAI" size="16" maxlength="15" value="<%= client.getE02CAI().trim()%>"></td>
              <td nowrap width="20%"> 
                <div align="right">Capital Suscrito :</div>
              </td>
              <td nowrap  width="30%"> 
                <input type="text" readonly <% if (client.getF02CAS().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02CAS" size="16" maxlength="15" value="<%= client.getE02CAS().trim()%>">
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="20%"> 
                <div align="right">Capital Pagado :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02CAP().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02CAP" size="16" maxlength="15" value="<%= client.getE02CAP().trim()%>">
              </td>
              <td nowrap width="20%"> 
                <div align="right">Nivel de Ventas :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02INL().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02INL" size="2" maxlength="1" value="<%= client.getE02INL().trim()%>">
              </td>
            </tr>

            <tr id="trdark"> 
              <td nowrap  width="20%"> 
                <div align="right">Total Activos :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02TACT().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02TACT" size="16" maxlength="15" value="<%= client.getE02TACT().trim()%>">
              </td>
              <td nowrapwidth="20%" width="18%"> 
                <div align="right">Total Pasivos :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02TPAS().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02TPAS" size="16" maxlength="15" value="<%= client.getE02TPAS().trim()%>">
              </td>
            </tr>
			
			<tr id="trclear"> 
              <td nowrapwidth="20%" width="22%"> 
                <div align="right">Patrimonio :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02TPAT().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02TPAT" size="16" maxlength="15" value="<%= client.getE02TPAT().trim()%>"></td>
              <td nowrap width="20%"> 
                <div align="right">Resultado del Ejercicio :</div>
              </td>
              <td nowrap  width="30%"> 
                <input type="text" readonly <% if (client.getF02RESU().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02RESU" size="16" maxlength="15" value="<%= client.getE02RESU().trim()%>">
              </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="20%"> 
                <div align="right">Monto de Ingreso Renta :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02MING().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02MING" size="16" maxlength="15" value="<%= client.getE02MING().trim()%>">
              </td>
              <td nowrap width="20%"> 
                <div align="right"> </div>
              </td>
              <td nowrap width="30%"> 
              </td>
            </tr>



            <tr id="trclear"> 
              <td nowrap n width="20%"> 
                <div align="right">Tama&ntilde;o del Negocio :</div>
              </td>
              <td nowrap width="30%"> 
                <input type="text" readonly <% if (client.getF02SEX().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02SEX" size="2" maxlength="1" value="<%= client.getE02SEX().trim()%>">
              </td>
              <td nowrap width="20%"> 
                <div align="right">Area de Negocio :</div>
              </td>
              <td nowrap  width="30%"> 
                <input type="text" readonly <% if (client.getF02FL3().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02FL3" size="2" maxlength="1" value="<%= client.getE02FL3().trim()%>">
              </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap n width="20%"> 
                <div align="right">Nivel de Riesgo :</div>
              </td>
              <td nowrap width="30%"> 
                <INPUT type="text" readonly name="E02RSL" size="5" maxlength="4" value="<%= client.getE02RSL().trim()%>">
              </td>
              <td nowrap width="20%">
                <div align="right">Tabla Previsi&oacute;n :</div>
              </td>
              <td nowrap width="30%">
                <input type="text" readonly name="E02PRV" size="4" maxlength="2" value="<%= client.getE02PRV().trim()%>">
              </td>
            </tr>
            <tr id="trclear"> 
               <td nowrap  width="20%"> 
                <div align="right">Fuentes de Ingresos :</div>
              </td>
              <td nowrap width="30"> 
                <input type="text" readonly <% if (client.getF02SOI().equals("Y")) out.print("id=\"txtchanged\""); %> name="D02SOI" size="45" maxlength="45" value="<%= client.getD02SOI().trim()%>">
              </td>
              <td nowrap width="20%"> 
 	              <div align="right">Bloqueo Correspondencia :</div>
              </td>
              <td nowrap  width="30%"> 
                <input type="text" readonly <% if (client.getF02C18().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02C18" size="2" maxlength="1" value="<%= client.getE02C18().trim()%>">
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <h4>Datos Operativos</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td  nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap width="28%"> 
                <div align="right">Estado del Cliente  :</div>
              </td>
              <td nowrap  width="28%" bordercolor="#FFFFFF">  
                <input type="text" readonly name="E02STS" size="13" maxlength="12" <% if (client.getF02STS().equals("Y")) out.print("id=\"txtchanged\""); %>
				value="<% if (client.getE02STS().equals("1")) { out.print("Inactivo"); }
                    	  else if (client.getE02STS().equals("2")) { out.print("Activo"); }
                          else if (client.getE02STS().equals("3")) { out.print("Lista Mayor Riesgo"); }
                          else if (client.getE02STS().equals("4")) { out.print("Disuelta"); }
                          else { out.print(""); } %>">
                </td>
              <td nowrap  width="22%"> 
                <div align="right">Clase de Cliente :</div>
              </td>
              <td nowrap  width="22%" >  
                <input type="text" readonly name="E02CCL" size="13" maxlength="12" <% if (client.getF02CCL().equals("Y")) out.print("id=\"txtchanged\""); %>
				  value="<% if (client.getE02CCL().equals("0")) { out.print("Socio"); }
						  	else if (client.getE02CCL().equals("1")) { out.print("Emp/Gobierno");  }	
							else if (client.getE02CCL().equals("2")) { out.print("Emp/Privada"); }
							else if (client.getE02CCL().equals(" ")) { out.print("No Aplica"); }
							else { out.print(""); } %> " >
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="28%"> 
                <div align="right">Tipo de Cliente :</div>
              </td>
              <td nowrap width="28%" bordercolor="#FFFFFF">  
              <input type="text" readonly name="E02TYP" size="10" maxlength="10" 
			  <% if (client.getF02TYP().equals("Y")) out.print("id=\"txtchanged\""); %>
    		  value="<% if (client.getE02TYP().equals("R")) { out.print("Regular"); }
						else if (client.getE02TYP().equals("M")) { out.print("Master"); }
		                else if (client.getE02TYP().equals("G")) { out.print("Grupo"); }
						else { out.print(""); } %>">
              </td>
			  	
              <td nowrap  width="22%"> 
                <div align="right">Numero Grupo :</div>
              </td>
              <td nowrap  width="22%">  
                <input type="text" readonly <% if (client.getF02GRP().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02GRP" size="10" maxlength="9" value="<%= client.getE02GRP().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap  width="28%"> 
                <div align="right">Lenguaje :</div>
              </td>
              <td nowrap width="28%" bordercolor="#FFFFFF">  
                <input type="text" readonly name="E02LIF" size="10" maxlength="10" 
				<% if (client.getF02LIF().equals("Y")) out.print("id=\"txtchanged\""); %>
			    value="<% if (client.getE02LIF().equals("E")) { out.print("Ingles"); } 
                		else if (client.getE02LIF().equals("S")) { out.print("Español"); }
						else { out.print(""); } %>" >
              </td>
              <td nowrap  width="22%"> 
                <div align="right">Calificaci&oacute;n 
                  :</div>
              </td>
              <td nowrap width="22%">  
                <input type="text" readonly <% if (client.getF02FL2().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02FL2" size="2" maxlength="1" value="<%= client.getE02FL2().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="28%"> 
                <div align="right">Impuestos/Retenciones 
                  :</div>
              </td>
              <td nowrap  width="22%">  
                <input type="text" readonly <% if (client.getF02TAX().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02TAX" size="2" maxlength="1" value="<%= client.getE02TAX().trim()%>">
              </td>
              <td nowrap width="18%"> 
	              <div align="right">Sucursal Administrativa :</div>
	          </td>
	          <td nowrap width="36%"> 
	            <eibsinput:text name="client" property="E02BRA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH%>" readonly="true" />
	          </td>
  
            </tr>
            <tr id="trdark"> 
              <td nowrap width="28%"> 
                <div align="right">Clasificaci&oacute;n Individual :</div>
              </td>
              <td nowrap  width="22%">  
                <input type="text" readonly name="E02CIN" size="2" maxlength="1" value="<%= client.getE02CIN().trim()%>"> 
              </td>
              <td nowrap width="18%"> 
	          </td>
	          <td nowrap width="36%"> 
	          </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <h4>Miscelaneos</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td  nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap  width="18%"> 
                <div align="right">Referido por :</div>
              </td>
              <td nowrap  width="39%">  
                <input type="text" readonly <% if (client.getF02RBY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02RBY" size="5" maxlength="4" value="<%= client.getE02RBY().trim()%>">
                <input type="text" readonly <% if (client.getF02RBN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02RBN" size="45" maxlength="45" value="<%= client.getE02RBN().trim()%>">
                </td>
              <td nowrap  width="23%"> 
                <div align="right">Residente del Pa&iacute;s :</div>
              </td>
              <td nowrap   width="20%">  
              <input type="text" readonly name="E02FL1" size="5" maxlength="5" 
			  <% if (client.getF02FL1().equals("Y")) out.print("id=\"txtchanged\""); %>
			  value="<%if (client.getE02FL1().equals("1")) { out.print("Si"); } 
						else {out.print("No"); } %>" >
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="18%" > 
                <div align="right">Nivel de Consulta 
                  :</div>
              </td>
              <td nowrap width="39%">  
                <input type="text" readonly <% if (client.getF02ILV().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02ILV" size="1" maxlength="1" value="<%= client.getE02ILV().trim()%>">
                </td>
              <td nowrap width="23%" > 
                <div align="right">No. de Tarjetas ATM :</div>
              </td>
              <td nowrap width="20%" >  
                <input type="text" readonly <% if (client.getF02ATM().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02ATM" size="2" maxlength="1" value="<%= client.getE02ATM().trim()%>">
                </td>
            </tr>
           <tr id="trdark"> 
            <td nowrap  width="20%"> 
              <div align="right">Poder Especial :</div>
              </td>             
            <td nowrap  width="20%"> 
              <input type="radio" disabled name="E02L01" value="Y" <%if (client.getE02L01().equals("Y")) out.print("checked"); %>>
                S&iacute; 
                <input type="radio" disabled name="E02L01" value="N" <%if (!client.getE02L01().equals("Y")) out.print("checked"); %>>
                No 
                </td>            
            <td nowrap  width="18%"> 
              <div align="right">Fecha Poder Especial :</div>
              </td>     
              <td nowrap> 
	               <eibsinput:date name="client" fn_year="E02A1Y" fn_month="E02A1M" fn_day="E02A1D" readonly="true"/>
              </td>
            </tr>             
           <tr id="trclear"> 
              
            <td nowrap width="18%"> 
              <div align="right">Fuente :</div>
              </td>
              
            <td nowrap  width="39%"> 
               <%
              	boolean bTesoreria = (client.getE02FL8().indexOf("T") > -1);
              	boolean bFideicomiso = (client.getE02FL8().indexOf("F") > -1);
              	boolean bFEM = (client.getE02FL8().indexOf("E") > -1);
              	boolean bTerceros = (client.getE02FL8().indexOf("R") > -1);
              %>
              <INPUT type="checkbox" name="E02FL8_TES" value="1" <% if (bTesoreria == true) out.print("checked"); %> readonly>Tesoreria
              <INPUT type="checkbox" name="E02FL8_FID" value="1" <% if (bFideicomiso == true) out.print("checked"); %> readonly>Fideicomiso
              <INPUT type="checkbox" name="E02FL8_FEM" value="1" <% if (bFEM == true) out.print("checked"); %> readonly>FEM
              <INPUT type="checkbox" name="E02FL8_TER" value="1" <% if (bTerceros == true) out.print("checked"); %> readonly>Terceros
             </td>
              
            <td nowrap width="23%"> 
              
              </td>
              
            <td nowrap  width="20%"> 
              
              </td>
            </tr>            
            
          </table>
        </td>
      </tr>
    </table> 
  </div>
  
   	 <div align="center">
	 	<h4>Escritura P&uacute;blica</h4>
		<table class="tableinfo">
      	<tr > 
        	<td nowrap > 
          		<table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">

		            <tr id="trclear"> 
        		      <td nowrap  width="18%" align="right"> 
        		      		<div align="right">Nombre Empresa por 1 d&iacute;a :</div>
		              </td>
					  <td nowrap  width="39%"> 
                   		<input type="text" <% if (client.getF02NOME().equals("Y")) out.print("id=\"txtchanged\""); %> name="E02NOME" size="61" maxlength="60" value="<%= client.getE02NOME().trim()%>" readonly>
		              </td>
              		</tr>
            
           			<tr id="teclear"> 
            		  <td nowrap  width="20%"> 
             			 <div align="right">Solicitud :</div>
		              </td>             
        		      <td nowrap  width="20%">
                		<eibsinput:date name="client" fn_year="E02NEY" fn_month="E02NEM" fn_day="E02NED" required="false" readonly="true"/>
            		  </td>            
	               </tr> 
            
          </table>
        </td>
      </tr>
    </table>
 
 
    </div>
  
  
</form>
</body>
</html>
