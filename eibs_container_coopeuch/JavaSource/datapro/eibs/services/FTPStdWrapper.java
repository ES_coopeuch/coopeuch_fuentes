package datapro.eibs.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.StringTokenizer;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.datapro.generic.beanutil.BeanList;


/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FTPStdWrapper implements FTPWrapper {
	
	private String server; 
	private String username; 
	private String password; 
	private String local;
	private int filetype;
	
	private FTPClient ftp;

	public FTPStdWrapper() {
		super();
		ftp = new FTPClient();
		// Use passive mode as default because most of us are
        // behind firewalls these days.
		ftp.enterLocalPassiveMode();
	}

	public FTPStdWrapper(String server, String username, String password, String local) {
		this();
		this.server = server;
		this.username = username;
		this.password = password;
		this.local = local;
		this.filetype = BINARY;
	}
	
	public boolean open() throws IOException {
		ftp.connect(server);
		System.out.println("Connected to " + server + ".");
		// After connection attempt, you should check the reply code to verify success.
        int reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            System.out.println("FTP server refused connection.");
            return false;
        } else {
        	if (ftp.login(username, password)) {
       			System.out.println("Remote system is " + ftp.getSystemName());
           		return true;
        	} else {
        		return false;
        	}
        }	
	}
	
	public void close() throws IOException {
		if (ftp.isConnected()) {
			if (ftp.logout()) {
				ftp.disconnect();
				System.out.println("Connection closed.");
			}
		}
	}
	
	public BeanList getWorkDir() throws IOException {
		BeanList result = new BeanList();
		String [] files = ftp.listNames(getRemotePath());
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i];
			if (files[i].indexOf(getRemotePath()) >= 0) {
				fileName = fileName.substring(getRemotePath().length() + 1);
			}
			result.addRow(fileName);
		}
		return result;
	}
	
	public BeanList getWorkDir(String ext) throws IOException {
		BeanList result = new BeanList();
		BeanList files = getWorkDir();
		files.initRow();
		while (files.getNextRow()) {
			String name = (String) files.getRecord();
			String extension = name.lastIndexOf(".") < 0 ? "" : name.trim().substring(name.trim().lastIndexOf("."));
			if (!extension.equals("")) {
				for (StringTokenizer st = new StringTokenizer(ext, "|"); st.hasMoreElements();) {
					if (extension.toLowerCase().equals(st.nextToken().toLowerCase())) {
						result.addRow(name);
					}
				}
			}				
		}	
		return result;
	}
	
	public void download(String filename) throws IOException {
		OutputStream output = null;
		try {
			File file = new File(getLocalPath(), filename);
			try {
				output = new FileOutputStream(file);
			} catch (FileNotFoundException e) {
				if (file.createNewFile()) {
					output = new FileOutputStream(file);
				} else {
					throw new IOException("Can't create file. Please contact your network administrator.");
				}
			}
			ftp.setFileType(filetype);
			ftp.retrieveFile(filename, output);
		} finally {	
			if (output != null) output.close();
		}	
	}

	public void upload(String filename) throws IOException {
		File file = new File(local, filename);
		upload(new FileInputStream(file), filename);
	}
	
	public void upload(InputStream input,String filename) throws IOException {
		try {
			ftp.setFileType(filetype);
			ftp.storeFile(filename, input);
		} finally {	
			if (input != null) input.close();
		}	
	}
	
	public boolean delete(String filename) throws IOException {
		return ftp.deleteFile(filename);
	}
	
	public boolean makeDir(String directory) throws IOException {
		return ftp.makeDirectory(directory);
	}

	public boolean removeDir(String directory) throws IOException {
		return ftp.removeDirectory(directory);
	}

	public int getFiletype() {
		return filetype;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean cdRemotePath(String directory) throws IOException {
		return ftp.changeWorkingDirectory(directory);
	}

	public boolean command(String cmd, String params) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	public String getRemotePath() throws IOException {
		return ftp.printWorkingDirectory();
	}

	public void setLocalPath(String local) {
		this.local = local;
	}

	public void setServerName(String server) {
		this.server = server;
	}

	public void setUserName(String username) {
		this.username = username;
	}

	public String getLocalPath() {
		return local == null ? "" : local;
	}

	public void setFileType(int type) {
		this.filetype = type;
	}

}
