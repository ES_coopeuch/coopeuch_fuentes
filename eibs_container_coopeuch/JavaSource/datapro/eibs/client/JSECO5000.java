package datapro.eibs.client;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */ 
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Random;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;
import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.ECC150101Message;
import datapro.eibs.beans.ECC150102Message;
import datapro.eibs.beans.ECC150103Message;
import datapro.eibs.beans.ECO060001Message;
import datapro.eibs.beans.ECO060002Message;
import datapro.eibs.beans.ECO200101Message;
import datapro.eibs.beans.ECO500001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ETG000000Message;
import datapro.eibs.beans.ExcelColStyle;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.ServiceLocator;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;
import datapro.eibs.sockets.MessageField;

public class JSECO5000 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_BLO_ENTER = 100;	
	protected static final int A_BLO_ENTER = 200;
	protected static final int A_BLO_PROCESS  = 300;
	

	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		screen =  R_BLO_ENTER;
			
		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}		
		
		switch (screen) {
		case R_BLO_ENTER:
			procReqBloApoEnter(user, req, res, session);
			break;
		case A_BLO_ENTER:
			procActionBloApoEnter(user, req, res, session);
			break; 
		case A_BLO_PROCESS:
			procActionPloApoProcess(user, req, res, session);
			break; 


		}		
	}
	
	protected void procReqBloApoEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ses.setAttribute("error", new ELEERRMessage());
		ses.setAttribute("msg", new ECO500001Message());

		try {
			flexLog("About to call Page: ECO5000_blo_apo_enter.jsp");
			forward("ECO5000_blo_apo_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

	protected void procActionBloApoEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			
			mp = getMessageProcessor("ECO5000", req);
			ECO500001Message msg = (ECO500001Message) mp.getMessageRecord("ECO500001", user.getH01USR(), "0001");
			
			setMessageRecord(req, msg);			

			mp.sendMessage(msg);
						
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (ECO500001Message) mp.receiveMessageRecord("ECO500001");
	
			session.setAttribute("error", msgError);
			session.setAttribute("msg", msg);
			session.setAttribute("userPO", userPO);

			if (!mp.hasError(msgError)) {
				flexLog("About to call Page: ECO5000_blo_apo_process.jsp");
				forward("ECO5000_blo_apo_process.jsp", req, res);
			}
			else
			{
				flexLog("About to call Page: ECO5000_blo_apo_enter.jsp");
				forward("ECO5000_blo_apo_enter.jsp", req, res);
			}
			
		} finally { 
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionPloApoProcess(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			
			mp = getMessageProcessor("ECO5000", req);
			ECO500001Message msg = (ECO500001Message) mp.getMessageRecord("ECO500001", user.getH01USR(), "0002");
			
			setMessageRecord(req, msg);			

			mp.sendMessage(msg);
						
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (ECO500001Message) mp.receiveMessageRecord("ECO500001");
	
			msg.setH01FLGWK1("1");
			msg.setH01FLGWK2("1");
			
			session.setAttribute("error", msgError);
			session.setAttribute("msg", msg);
			session.setAttribute("userPO", userPO);
				
			flexLog("About to call Page: ECO5000_blo_apo_process.jsp");
			forward("ECO5000_blo_apo_process.jsp", req, res);

		} finally { 
			if (mp != null)
				mp.close();
		}
	}
	

	//END
}
	



