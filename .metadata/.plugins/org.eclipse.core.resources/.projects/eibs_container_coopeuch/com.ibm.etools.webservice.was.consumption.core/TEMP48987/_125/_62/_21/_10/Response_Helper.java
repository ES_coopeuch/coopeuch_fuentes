/**
 * Response_Helper.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public class Response_Helper {
    // Type metadata
    private static com.ibm.ws.webservices.engine.description.TypeDesc typeDesc =
        new com.ibm.ws.webservices.engine.description.TypeDesc(Response.class);

    static {
        com.ibm.ws.webservices.engine.description.FieldDesc field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("error");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "error"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://10.21.62.125:8186/pricing/precio", "Excepcion"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("tasa");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "tasa"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://10.21.62.125:8186/pricing/precio", "Tasa"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("salida");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "salida"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://10.21.62.125:8186/pricing/precio", "salida"));
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static com.ibm.ws.webservices.engine.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new Response_Ser(
            javaType, xmlType, typeDesc);
    };

    /**
     * Get Custom Deserializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new Response_Deser(
            javaType, xmlType, typeDesc);
    };

}
