package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Modificado por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  08/06/2012                                                                                      **/
/**  Objetivo                :  Servicio para Generacion y Emision de certificados de                                           **/
/**                             - Constancia Prepago                                                                          **/
/*********************************************************************************************************************************/

import java.io.*;
import java.net.*;
import java.beans.Beans;
import javax.servlet.*;
import javax.servlet.http.*;

import datapro.eibs.beans.*;

import datapro.eibs.master.Util;
 
import datapro.eibs.sockets.*;
import java.util.Hashtable;
import java.util.ArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.PropertyResourceBundle;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generic.beanutil.BeanList;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;

import datapro.eibs.beans.ECE002001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.sockets.DecimalField;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

import datapro.eibs.generic.SimpleAthenticator;
public class JSECE0030 extends datapro.eibs.master.SuperServlet {


	protected static final int R_ENTER_ID	 	 = 100;
	protected static final int A_LIST            = 2;
	protected static final int A_PRECANCEL		= 10;
	private String LangPath = "S";

/**
 * JSECE0030 constructor comment.
 */
public JSECE0030() {
	super();
}
/**
 * 
 */
public void destroy() {

	flexLog("free resources used by JSECE0030");
	
}
/**
 * 
 */
public void init(ServletConfig config) throws ServletException {
	super.init(config);
}

/**
 * This method was created in VisualAge.
 */
protected void procReqEnterID(
		ESS0030DSMessage user, 
		HttpServletRequest req, 
		HttpServletResponse res, 
		HttpSession ses)
		throws ServletException, IOException {

	ELEERRMessage msgError = null;
	UserPos	userPO = null;	

	try {

		msgError = new datapro.eibs.beans.ELEERRMessage();
		userPO = new datapro.eibs.beans.UserPos();
		userPO.setOption("LN");
		userPO.setPurpose("MAINTENANCE");
		ses.setAttribute("error", msgError);
		ses.setAttribute("userPO", userPO);
		
  	} catch (Exception ex) {
		flexLog("Error: " + ex); 
  	}

	try {
		flexLog("About to call Page: " + LangPath + "ECE0030_cer_ln_enter_payment.jsp");
		callPage(LangPath + "ECE0030_cer_ln_enter_payment.jsp", req, res);
	}
	catch (Exception e) {
		e.printStackTrace();
		flexLog("Exception calling page " + e);
	}

}

/**
 * This method was created in VisualAge.
 */
protected void procActionCancelation(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

	MessageRecord newmessage = null;
	ECE003001Message msgList = null;
	ELEERRMessage msgError = null;
	UserPos	userPO = null;	
	boolean IsNotError = false;

	try {
		msgError = new datapro.eibs.beans.ELEERRMessage();
	  	} 
	catch (Exception ex) {
		flexLog("Error: " + ex); 
  	}

	userPO = (datapro.eibs.beans.UserPos)ses.getAttribute("userPO");

	// Send Initial data
	try
	{
		flexLog("Send Initial Data");
		msgList = (ECE003001Message)mc.getMessageRecord("ECE003001");
		msgList.setH01USERID(user.getH01USR());
	 	msgList.setH01PROGRM("ECE0030");
	 	msgList.setH01TIMSYS(getTimeStamp());
	 	msgList.setH01SCRCOD("01");
	 	msgList.setH01OPECOD("0001");
	 	msgList.setE01DEAACC(req.getParameter("E01DEAACC"));
	 	msgList.setE01DEAVDD(req.getParameter("E01PAGDIA"));
	 	msgList.setE01DEAVMM(req.getParameter("E01PAGMES"));
	 	msgList.setE01DEAVAA(req.getParameter("E01PAGANO"));
		// all the fields here
	 	java.util.Enumeration enu = msgList.fieldEnumeration();
		MessageField field = null;
		String value = null;
		while (enu.hasMoreElements()) {
			field = (MessageField)enu.nextElement();
			try {
				value = req.getParameter(field.getTag()).toUpperCase();
				if (value != null) {
					field.setString(value);
				}
			}
			catch (Exception e) {
			}	
		}
	 	msgList.send();	
	 	msgList.destroy();

	 	flexLog("ECE003001 Message Sent");
	}		
	catch (Exception e)
	{
		e.printStackTrace();
		flexLog("Error: " + e);
	  	throw new RuntimeException("Socket Communication Error");
	}
		
	// Receive Error Message
	try {
		newmessage = mc.receiveMessage();

		if (newmessage.getFormatName().equals("ELEERR")) {
			try {
				msgError =
					(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
						getClass().getClassLoader(),
						"datapro.eibs.beans.ELEERRMessage");
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			msgError = (ELEERRMessage) newmessage;
			IsNotError = msgError.getERRNUM().equals("0");
			flexLog("IsNotError = " + IsNotError);
			showERROR(msgError);
		} else {
			flexLog("Message " + newmessage.getFormatName() + " received.");				
		}
	} catch (Exception e) {
		e.printStackTrace();
		flexLog("Error: " + e + newmessage);
		throw new RuntimeException("Socket Communication Error Receiving");
	}
	// Receive Data
	try {
		newmessage = mc.receiveMessage();

		if (newmessage.getFormatName().equals("ECE003001")) {

			JBObjList beanList = new JBObjList();

			boolean firstTime = true;
			String marker = "";
			String chk = "";
				while (true) {

				msgList = (ECE003001Message) newmessage;
				marker = msgList.getH01FLGMAS();
                flexLog("Mensaje recibido...." + msgList);
				if (firstTime) {
					userPO.setHeader1(msgList.getE01DEACUN());
					userPO.setHeader2(msgList.getE01CUSNA1());
					userPO.setHeader3(msgList.getE01DEAACC());
					userPO.setHeader4(msgList.getE01DEACCY());
					userPO.setHeader5(msgList.getE01DEAPRO());
					firstTime = false;
					chk = "checked";

				} else {
					chk = "";
				}

				if (marker.equals("*")) {
					beanList.setShowNext(false);
					break;
				} else {
					beanList.addRow(msgList);

					if (marker.equals("+")) {
						beanList.setShowNext(true);

						break;
					}
				}
				newmessage = mc.receiveMessage();
			}

			flexLog("Putting java beans into the session");
			ses.setAttribute("error", msgError);
			ses.setAttribute("ECE0030Help", beanList);
			ses.setAttribute("userPO", userPO);

			if (IsNotError) { // There are no errors
				try {
					flexLog(
						"About to call Page: "
							+ LangPath
							+ "ECE0030_cer_constancia_prepago.jsp");
					callPage(
						LangPath + "ECE0030_cer_constancia_prepago.jsp",
						req,
						res);					
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			
			} else {
				try {
					flexLog("About to call Page: " + LangPath + "ECE0030_cer_ln_enter_payment.jsp");
					callPage(LangPath + "ECE0030_cer_ln_enter_payment.jsp", req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			}				

		} else
			flexLog("Message " + newmessage.getFormatName() + " received.");

	} catch (Exception e) {
		e.printStackTrace();
		flexLog("Error: " + e);
		throw new RuntimeException("Socket Communication Data Receiving");
	}
	
}

protected void procActionPos(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		UserPos userPO = null;

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		flexLog(".....  = " + userPO.getOption()); 
		int inptOPT = 0;

		inptOPT = Integer.parseInt(req.getParameter("opt"));
        flexLog("Opcion = " + inptOPT);
		switch (inptOPT) {
			case 1 : //Imprimir Certificado
				procReqPDF(mc, user, req, res, ses);
				break;				
			default :
				res.sendRedirect(
					super.srctx
						+ "/servlet/datapro.eibs.products.JSECE0030?SCREEN=1");
				break;
		}
	}


protected void procReqPDF(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
		throws ServletException, IOException {
		DocumentException ex = null;
		ByteArrayOutputStream baosPDF = null;

		boolean ACT = false;
		try {
			baosPDF =
				generatePDFDocumentBytes(
					req,
					this.getServletContext(),
					ses,
					ACT);

				StringBuffer sbFilename = new StringBuffer();
				String fn = com.datapro.generic.tool.Util.getTimestamp().toString();
				fn = Util.replace(fn,":","-");
				fn = Util.replace(fn,".","-");
				sbFilename.append(fn);
				sbFilename.append(".pdf");

				res.setHeader("Cache-Control", "max-age=0");

				res.setContentType("application/pdf");

				StringBuffer sbContentDispValue = new StringBuffer();
				sbContentDispValue.append("inline");
				sbContentDispValue.append("; filename=");
				sbContentDispValue.append(sbFilename);

				res.setHeader(
					"Content-disposition",
					sbContentDispValue.toString());

				res.setContentLength(baosPDF.size());

				ServletOutputStream sos;

				sos = res.getOutputStream();

				baosPDF.writeTo(sos);

				sos.flush();

		} catch (DocumentException dex) {
			res.flushBuffer();
			res.setContentType("text/html");
			PrintWriter writer = null;
			writer = res.getWriter();
			writer.println(
				this.getClass().getName()
					+ " caught an exception: "
					+ dex.getClass().getName()
					+ "<br>");
			writer.println("<pre>");
			dex.printStackTrace(writer);
			writer.println("</pre>");
		} finally {
			if (baosPDF != null) {
				baosPDF.reset();
			}
		}

		return;

	}
protected ByteArrayOutputStream generatePDFDocumentBytes(
		final HttpServletRequest req,
		final ServletContext ctx,
		HttpSession session,
		boolean FLG)
		throws DocumentException 		{
	
		String detail60 = "INFORMACION IMPORTANTE";
		String detail61 = "LOS VALORES EXPRESADOS EN EL PRESENTE DOCUMENTO, NO CONSIDERAN CARGOS ADICIONALES POR MOROSIDAD. SI EXISTE MOROSIDAD, ESTA DEBE SER REGULARIZADA PREVIAMENTE AL PREPAGO.";
		String detail62 = "(*) EL PREPAGO SE HAR� EFECTIVO S�LO SI EL SOCIO SE ENCUENTRA AL D�A EN SUS PAGOS Y SIN MOROSIDAD.";
		
		String detail70 = "El Monto Prepago no considera los recargos por atrasos en el pago del Cr�dito.";	
		
		String detail71 = "Para los cr�ditos de consumo cuyas cuotas son pagadas a trav�s de descuento por planilla, s�lo se";
		String detail72 = "considerar�n para efectos del prepago las sumas correspondientes a las cuotas descontadas por planilla";
		String detail73 = "debidamente enteradas a Coopeuch. Si con posterioridad al prepago de un cr�dito Coopeuch Ltda. recibe";
		String detail74 = "fondos por concepto de pago de cuotas descontadas por planilla de dicho cr�dito ya prepagado, estas";
		String detail75 = "generar�n una acreencia a favor del socio, la cual podr� ser cobrada en cualquiera de las oficinas Coopeuch";
		
		String detail76 = "De conformidad a lo se�alado en la circular N� 2114 de la SVS de fecha 26 de julio de 2013, los seguros";
		String detail77 = "otorgados a partir del 01 de diciembre de 2013, tienen derecho a devoluci�n de prima pagada no devengada.";
		String detail78 = "La prima a devolver se calcular� en proporci�n al tiempo no transcurrido.";
		
	    ECE003001Message msgDoc = null;
		JBObjList bl = (JBObjList) session.getAttribute("ECE0030Help");
		int idx = 0;
		bl.setCurrentRow(idx);
		msgDoc = (ECE003001Message) bl.getRecord();
	    //Datos generales 
		String selcun = Util.unformatHTML(msgDoc.getE01DEACUN());       //Numero del cliente
		String selidn = Util.unformatHTML(msgDoc.getE01CUSIDN());   
		String rut = Util.unformatHTML(msgDoc.getE01CUSIDN());       //Rut del Cliente
        String mantisa= "";
        String digito="";
        String formattedRUT="";
        String periodicidad = " ";
		if (rut.length() > 0)
			{ 
	        mantisa=rut.substring(0,rut.length()-1);
	        digito=rut.substring(rut.length()-1);
	        DecimalFormat nf = new DecimalFormat("##,###,###"); 
	        formattedRUT=nf.format(Integer.valueOf(mantisa))+"-"+digito;
			}
		if (msgDoc.getE01DEAPER().equals("D"))
		{ 
			periodicidad="Diario";
		}
        else if (msgDoc.getE01DEAPER().equals("M"))
        {
			periodicidad="Mensual";
        }
        else
        {
			periodicidad="Anual ";	
        }
		Document doc = new Document(PageSize.A4, 36, 36, 36, 36);           //setaer tamano de hoja

		ByteArrayOutputStream baosPDF = new ByteArrayOutputStream();
		PdfWriter docWriter = null;

		try {
			docWriter = PdfWriter.getInstance(doc, baosPDF);

			if (FLG) {
				docWriter.setEncryption(
					PdfWriter.STRENGTH128BITS,
					selcun,
					selidn,
					PdfWriter.AllowCopy | PdfWriter.AllowPrinting);
			}

			doc.addAuthor("eIBS");
			doc.addCreationDate();
			doc.addProducer();
			doc.addCreator(msgDoc.getE01DEACUN());
			doc.addKeywords("pdf, itext, Java, open source, http");

			Font normalFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL);
			Font normalBoldFont = FontFactory.getFont(FontFactory.COURIER, 8, Font.BOLD);
			Font normalBoldFont7 = FontFactory.getFont(FontFactory.COURIER, 7, Font.BOLD);
			Font normalBoldFont6 = FontFactory.getFont(FontFactory.COURIER, 6, Font.BOLD);
			Font normalBoldUnderFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD | Font.UNDERLINE);
			Font headerFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL);
			Font headerBoldFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD);
			Font headerBoldFont7 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);
			Font headerBoldFont12 = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD);
			Font headerBoldUnderFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD | Font.UNDERLINE);

			Paragraph RAYA =
				    new Paragraph("_________________________________________________________________________________________________",
					normalBoldFont);
			Paragraph BLANK = new Paragraph(" ", headerBoldFont);
			//Header principal
			Paragraph HEADER00 = new Paragraph("Constancia Prepago", headerBoldFont12);
			//cabecera  izquierda
			Paragraph HEADER01 = new Paragraph("Impresi�n:", headerBoldFont);
			Paragraph HEADER02 = new Paragraph("Usuario:", headerBoldFont);
			Paragraph HEADER03 = new Paragraph("Centro de Costo:", headerBoldFont);
			Paragraph HEADER04 = new Paragraph("R�tulo: ", headerBoldFont);	
			Paragraph HEADER05 = new Paragraph("P�gina: ", headerBoldFont);
			//fin cabecera..
			
			//datos principales
			Paragraph HEADER06 = new Paragraph("Nombre", headerBoldFont);
			Paragraph HEADER07 = new Paragraph("Rut", headerBoldFont);
			Paragraph HEADER08 = new Paragraph("N�mero de Producto", headerBoldFont);
			Paragraph HEADER09 = new Paragraph("Tipo de Deuda", headerBoldFont);	
			Paragraph HEADER10 = new Paragraph("Fecha de Otorgamiento", headerBoldFont);	
			Paragraph HEADER11 = new Paragraph("Fecha Pactada �lt. Pago", headerBoldFont);
			Paragraph HEADER12 = new Paragraph("Monto Otorgado", headerBoldFont);
			Paragraph HEADER13 = new Paragraph("N�mero de Cuotas Pactadas", headerBoldFont);	
			Paragraph HEADER14 = new Paragraph("Periodicidad de Pago", headerBoldFont);
			Paragraph HEADER15 = new Paragraph("Tipo de Cr�dito", headerBoldFont);
			Paragraph HEADER16 = new Paragraph("Moneda de Origen", headerBoldFont);
			Paragraph HEADER17 = new Paragraph("Tasa de Inter�s", headerBoldFont);
			Paragraph HEADER18 = new Paragraph("Tipo Prepago", headerBoldFont);	
//   		Paragraph HEADER19 = new Paragraph("Gastos Notario", headerBoldFont);		
			Paragraph HEADER20 = new Paragraph("Cuotas Impagas", headerBoldFont);
			Paragraph HEADER21 = new Paragraph("Saldo Impago", headerBoldFont);
			Paragraph HEADER22 = new Paragraph("Primera Fecha Vencim.", headerBoldFont);
//			Paragraph HEADER23 = new Paragraph("Tipo Seguro", headerBoldFont);
			//fin datos principales
			
			Paragraph HEADER30 = new Paragraph("Fecha Vigencia", headerBoldFont7);			
			Paragraph HEADER31 = new Paragraph("Intereses Devengados", headerBoldFont7);
			//Paragraph HEADER32 = new Paragraph("Intereses Moratorios", headerBoldFont7);
			Paragraph HEADER33 = new Paragraph("Otros Cargos", headerBoldFont7);
			Paragraph HEADER34 = new Paragraph("Comisi�n Prepago", headerBoldFont7);
			Paragraph HEADER35 = new Paragraph("Devol. Seguros Totales", headerBoldFont7);
//			Paragraph HEADER36 = new Paragraph("Fecha Expiraci�n de Devoluci�n", headerBoldFont7);
			Paragraph HEADER38 = new Paragraph("Monto Prepago", headerBoldFont7);
			Paragraph HEADER37 = new Paragraph("Saldo Capital", headerBoldFont7);//se cambiaron de posicion..
	        DecimalFormat nf = new DecimalFormat("##,###,###"); 
			Paragraph DETAIL01 = new Paragraph(msgDoc.getE01DEACDD() + "/" + msgDoc.getE01DEACMM() + "/" + msgDoc.getE01DEACAA() + " " + msgDoc.getE01DEACHH() + ":" + msgDoc.getE01DEACMI() + ":" + msgDoc.getE01DEACSS(), normalBoldFont7);	
			Paragraph DETAIL02 = new Paragraph(msgDoc.getE01DEAUSU(), normalBoldFont7);
			Paragraph DETAIL03 = new Paragraph(msgDoc.getE01DEAOFI(), normalBoldFont7);
			Paragraph DETAIL04 = new Paragraph("Publico", normalBoldFont7);
			Paragraph DETAIL05 = new Paragraph("1 de 2 ", normalBoldFont7);
			Paragraph DETAIL06 = new Paragraph(msgDoc.getE01CUSNA1(), normalBoldFont);
			Paragraph DETAIL07 = new Paragraph(formattedRUT, normalBoldFont);
			Paragraph DETAIL08 = new Paragraph(msgDoc.getE01DEAACC(), normalBoldFont);
			Paragraph DETAIL09 = new Paragraph(msgDoc.getE01DEATDE(), normalBoldFont);
			Paragraph DETAIL10 = new Paragraph(msgDoc.getE01DEASDD()+ "/"+ msgDoc.getE01DEASMM()+ "/"+ msgDoc.getE01DEASAA(), normalBoldFont);
			Paragraph DETAIL11 = new Paragraph(msgDoc.getE01DEAUPD()+ "/"+ msgDoc.getE01DEAUPM()+ "/"+ msgDoc.getE01DEAUPA(), normalBoldFont);
			Paragraph DETAIL12 = new Paragraph("$" + nf.format(Integer.valueOf(msgDoc.getE01DEAOAM())), normalBoldFont);
			Paragraph DETAIL13 = new Paragraph(msgDoc.getE01DEANCU(), normalBoldFont);
			Paragraph DETAIL14 = new Paragraph(periodicidad, normalBoldFont);
			Paragraph DETAIL15 = new Paragraph(msgDoc.getE01DEATYP(), normalBoldFont);
			Paragraph DETAIL16 = new Paragraph(msgDoc.getE01DEAMON(), normalBoldFont);
			Paragraph DETAIL17 = new Paragraph(msgDoc.getE01DEATAS() + "%", normalBoldFont);
			Paragraph DETAIL18 = new Paragraph("Total", normalBoldFont);
//			Paragraph DETAIL19 = new Paragraph("$" + nf.format(Integer.valueOf(msgDoc.getE01DEAGNO())), normalBoldFont);
			Paragraph DETAIL20 = new Paragraph(msgDoc.getE01DEACUI(), normalBoldFont);
			Paragraph DETAIL21 = new Paragraph("$" + nf.format(Integer.valueOf(msgDoc.getE01DEAPRI())), normalBoldFont);
			Paragraph DETAIL22 = new Paragraph(msgDoc.getE01DEAPVD()+ "/"+ msgDoc.getE01DEAPVM()+ "/"+ msgDoc.getE01DEAPVA(), normalBoldFont);
//			Paragraph DETAIL23 = new Paragraph(msgDoc.getE01DEATSG(), normalBoldFont);
			Paragraph DETAIL24 = new Paragraph("2 de 2 ", normalBoldFont7);

			
			Paragraph DETAIL60 = new Paragraph(detail60, headerBoldFont);
			Paragraph DETAIL61 = new Paragraph(detail61, headerBoldFont);
			Paragraph DETAIL62 = new Paragraph(detail62, headerBoldFont);			
			
			Paragraph DETAIL70 = new Paragraph(detail70, headerFont);
			Paragraph DETAIL71 = new Paragraph(detail71, headerFont);
			Paragraph DETAIL72 = new Paragraph(detail72, headerFont);
			Paragraph DETAIL73 = new Paragraph(detail73, headerFont);
			Paragraph DETAIL74 = new Paragraph(detail74, headerFont);
			Paragraph DETAIL75 = new Paragraph(detail75, headerFont);
			Paragraph DETAIL76 = new Paragraph(detail76, headerFont);
			Paragraph DETAIL77 = new Paragraph(detail77, headerFont);
			Paragraph DETAIL78 = new Paragraph(detail78, headerFont);
			//Paragraph DETAIL79 = new Paragraph(detail79, headerFont);
			//Paragraph DETAIL80 = new Paragraph(detail80, headerFont);
			
			doc.open();
			
			
			//Encabezado--------------------------------------------------------------------------------
						
			Table table = new Table(1, 20);//1 columna y 20 filas,
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);				
			Cell cell = new Cell(HEADER00);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell); 
			doc.add(table); 

			table = new Table(5, 20);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(0);
			table.setSpacing(0);
			table.setWidth(100);		
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			table.addCell(cell);	
			table.addCell(cell);
			
			cell = new Cell(HEADER01);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL01);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
			
			cell = new Cell(HEADER02);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL02);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
				
			cell = new Cell(HEADER03);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL03);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
				
			cell = new Cell(HEADER04);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL04);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
				
			cell = new Cell(HEADER05);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			

			cell = new Cell(DETAIL05);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table); 			
			
			table = new Table(1, 20);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);		
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);			
			doc.add(table);                                     //Agrego Lineas a la tabla
			//--------------------------------------------------------------------------------		
			table = new Table(3, 20);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(0);
			table.setSpacing(0);
			table.setWidth(100);		
			
			
			cell = new Cell(HEADER06);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL06);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
				
			cell = new Cell(HEADER07);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
					
			cell = new Cell(DETAIL07);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(HEADER08);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL08);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(HEADER09);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(DETAIL09);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
				
			cell = new Cell(HEADER10);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(DETAIL10);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(HEADER11);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(DETAIL11);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(HEADER12);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL12);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(HEADER13);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(DETAIL13);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(HEADER14);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL14);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(HEADER15);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL15);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
					
			cell = new Cell(HEADER16);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(DETAIL16);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(HEADER17);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(DETAIL17);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(HEADER18);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL18);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
//			cell = new Cell(HEADER19);
//			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//			cell.setBorder(Rectangle.NO_BORDER);
//			table.addCell(cell);
							
//			cell = new Cell(DETAIL19);
//			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//			cell.setBorder(Rectangle.NO_BORDER);
//			table.addCell(cell);
			
//			cell = new Cell(BLANK);
//			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
//			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//			cell.setBorder(Rectangle.NO_BORDER);
//			table.addCell(cell);
						
			cell = new Cell(HEADER20);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL20);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(HEADER21);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(DETAIL21);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(HEADER22);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			cell = new Cell(DETAIL22);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
//			cell = new Cell(HEADER23);
//			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//			cell.setBorder(Rectangle.NO_BORDER);
//			table.addCell(cell);
							
//			cell = new Cell(DETAIL23);
//			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//			cell.setBorder(Rectangle.NO_BORDER);
//			table.addCell(cell);
			
//			cell = new Cell(BLANK);
//			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
//			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//			cell.setBorder(Rectangle.NO_BORDER);
//			table.addCell(cell);
			doc.add(table); 
			//-------------------------------------------------------------------------------
			//colocamos la observacion importante
			table = new Table(1, 13);//columnas , filas
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);
			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL60);
			//cell.setHeader(true);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);

			cell = new Cell(DETAIL61);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL62);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			doc.add(table);
			//--------------------------------------------------------------------------------
			
			table = new Table(7, 4);//9,4
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);		
			cell = new Cell(HEADER30);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER31);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			//cell = new Cell(HEADER32);
			//cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			//table.addCell(cell);
			cell = new Cell(HEADER33);
		    cell.setHorizontalAlignment(1);
		    table.addCell(cell);
			
			cell = new Cell(HEADER34);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER35);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
//			cell = new Cell(HEADER36);
//			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			table.addCell(cell);
			cell = new Cell(HEADER37);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER38);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);

            bl.initRow();
	        while (bl.getNextRow()) {
	        	    msgDoc = (datapro.eibs.beans.ECE003001Message) bl.getRecord();
	        			Paragraph DETAIL30 = new Paragraph(msgDoc.getE01DEAVDD()+ "-"+ msgDoc.getE01DEAVMM()+ "-"+ msgDoc.getE01DEAVAA(), normalBoldFont7);
	        			Paragraph DETAIL31 = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01DEAIND())), normalBoldFont7);
	        			
	        			Paragraph DETAIL33 = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01DEAINM())), normalBoldFont7);
	        			if (msgDoc.getE01DEAINM()==null || msgDoc.getE01DEAINM().equals("")|| msgDoc.getE01DEAINM().equals("0")){
	        				DETAIL33 = new Paragraph("", normalBoldFont7);
	        			}
	        			
	        			Paragraph DETAIL34 = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01DEACPR())), normalBoldFont7);
	        			
	        			Paragraph DETAIL35 = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01DEADSE())), normalBoldFont7);
	        			if (msgDoc.getE01DEADSE()==null || msgDoc.getE01DEADSE().equals("")|| msgDoc.getE01DEADSE().equals("0")){
	        				DETAIL35 = new Paragraph("", normalBoldFont7);
	        			}
	        			
//	      				Paragraph DETAIL36 = new Paragraph(msgDoc.getE01DEAEDD()+ "-"+ msgDoc.getE01DEAEMM()+ "-"+ msgDoc.getE01DEAEAA(), normalBoldFont7);
//	        			if ((msgDoc.getE01DEAEDD()==null || msgDoc.getE01DEAEDD().equals("")|| msgDoc.getE01DEAEDD().equals("0")) && 
//	        				(msgDoc.getE01DEAEMM()==null || msgDoc.getE01DEAEMM().equals("")|| msgDoc.getE01DEAEMM().equals("0")) &&
//	        				(msgDoc.getE01DEAEAA()==null || msgDoc.getE01DEAEAA().equals("")|| msgDoc.getE01DEAEAA().equals("0"))){
//	        				DETAIL36 = new Paragraph("", normalBoldFont7);
//	        			}
	        			
	        			Paragraph DETAIL38 = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01DEAMTO())), normalBoldFont7);
	        			Paragraph DETAIL37 = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01DEASDO())), normalBoldFont7);//las intercambie
	        			
	        			
	        			cell = new Cell(DETAIL30);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL31);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			table.addCell(cell);
//	        			cell = new Cell(DETAIL32);
//	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	        			table.addCell(cell);
	        			cell = new Cell(DETAIL33);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL34);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL35);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			table.addCell(cell);
//	        			cell = new Cell(DETAIL36);
//	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	        			table.addCell(cell);
	        			cell = new Cell(DETAIL37);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL38);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			table.addCell(cell);	        			
	                }
			doc.add(table);		
			
			//Cambio de pagina  -----------------------------------------------------------------------------	
			table = new Table(5, 20);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);	
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			//5 primeras lineas
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
			//
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
			//otras 5 lineas
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
				
			cell = new Cell(HEADER05);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			

			cell = new Cell(DETAIL24);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
							
			doc.add(table);
			
			table = new Table(1, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);
			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);			
			
			cell = new Cell(DETAIL70);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
				
			cell = new Cell(DETAIL71);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL72);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL73);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
	
			cell = new Cell(DETAIL74);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
				
			cell = new Cell(DETAIL75);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
		
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL76);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL77);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL78);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
//			cell = new Cell(DETAIL79);
//			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			cell.setBorder(Rectangle.NO_BORDER);
//			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			//cell = new Cell(DETAIL80);
			//cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			//cell.setBorder(Rectangle.NO_BORDER);
			//table.addCell(cell);
			
			doc.add(table);
			
			
		} catch (DocumentException dex) {
			baosPDF.reset();
			throw dex;
		} finally {
			if (doc != null) {
				doc.close();
			}
			if (docWriter != null) {
				docWriter.close();
			}
		}

		if (baosPDF.size() < 1) {
			throw new DocumentException(
				"document has " + baosPDF.size() + " bytes");
		}

		return baosPDF;
	}



public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
   
	Socket s = null;
	MessageContext mc = null;

	ESS0030DSMessage msgUser = null;
  	HttpSession session = null;

	session = (HttpSession)req.getSession(false); 
	
	if (session == null) {
		try {
			res.setContentType("text/html");
			printLogInAgain(res.getWriter());
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception ocurred. Exception = " + e);
		}
	}
	else {

		int screen = R_ENTER_ID;
		
		try {
		
			msgUser = (datapro.eibs.beans.ESS0030DSMessage)session.getAttribute("currUser");

			// Here we should get the path from the user profile
			LangPath = super.rootPath + msgUser.getE01LAN() + "/";

			try
			{
				flexLog("Opennig Socket Connection");
				s = new Socket(super.hostIP, getInitSocket(req) + 1);
				s.setSoTimeout(super.sckTimeOut);
			  	mc = new MessageContext(new DataInputStream(new BufferedInputStream(s.getInputStream())),
							      	    new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
									    "datapro.eibs.beans");
			
			try {
				screen = Integer.parseInt(req.getParameter("SCREEN"));
			}
			catch (Exception e) {
				flexLog("Screen set to default value");
			}
		    flexLog("Entre con Screen = " + screen);
			switch (screen) {
	
			    case R_ENTER_ID : 
			    	procReqEnterID(msgUser, req, res, session);
				break;
				case A_LIST :
					procActionPos(mc, msgUser, req, res, session);
					break;
				case A_PRECANCEL :
					procActionCancelation(mc, msgUser, req, res, session);
					break;
				default :
					res.sendRedirect(super.srctx +LangPath + super.devPage);
					break;
			}
			}
			catch (Exception e) {
				e.printStackTrace();
				int sck = getInitSocket(req) + 1;
				flexLog("Socket not Open(Port " + sck + "). Error: " + e);
				res.sendRedirect(super.srctx +LangPath + super.sckNotOpenPage);
			//	return;
			}
			finally {
				s.close();
			}
			

		}
		catch (Exception e) {
			flexLog("Error: " + e);
			res.sendRedirect(super.srctx +LangPath + super.sckNotRespondPage);
		}
		
	}
	
}
protected void showERROR(ELEERRMessage m) {
	if (logType != NONE) {

		flexLog("ERROR received.");

		flexLog("ERROR number:" + m.getERRNUM());
		flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
		flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
		flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
		flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
		flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
		flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
		flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
		flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
		flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
		flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

	}
}

}