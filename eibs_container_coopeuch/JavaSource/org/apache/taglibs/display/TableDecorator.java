/**
 * $Id: TableDecorator.java,v 1.1 2011/12/01 13:47:08 pcataldo Exp $
 *
 * Status: Ok
 *
 * Todo:
 *   - setup the correct deprecation rules, so that people know to extend
 *     this class, rather then the Decorator class.
 **/

package org.apache.taglibs.display;

public class TableDecorator extends Decorator
{
   public TableDecorator() {
      super();
   }
}
