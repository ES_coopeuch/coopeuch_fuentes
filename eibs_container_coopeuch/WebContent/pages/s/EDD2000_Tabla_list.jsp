<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EDD200001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Ahorro Vivienda</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EDD200001List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script type="text/javascript">

  function goAction(op) 
  {
	var ok = false;
	var cun = "";
	var pg = ""; 
	if (op != '200')
	{	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "TPSTTR") 
	      	{	
	      		if (element.checked == true) 
	      		{
	      			document.getElementById("E01TPSTTR").value = element.value;
        			ok = true;	        			
        			break;
				}
	      	}
	      }
      } 
      else 
      {
      	ok = true;
      }
	 
      if ( ok ) 
      {
      	var confirm1 = true;
      	
      	if (op =='202')
      	{
      		confirm1 = confirm("Desea Eliminar este Registro Seleccionado?");
      	}
		if (confirm1)
		{
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		

     } else 
     {
		alert("Debe seleccionar un registro para continuar.");	   
	 }    
	}
 
function closeHiddenDivNew(){
	setVisibility(document.getElementById("hiddenDivNew"), "hidden");
}

</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Tipos de Tramos Postulacion a Subsidio Habitacional<br>Ahorro Vivienda<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="Tabla_list.jsp,EDD2000"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEDD2000">
<input type="hidden" name="SCREEN" value="201"> 
<input type="hidden" name="E01TPSTTR" value="">
 
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('200')"> <b>Crear</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('201')"> <b>Modificar</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('202')"> <b>Borrar</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>

<%
	if (EDD200001List.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>

	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="5%"></th>
			<th align="center" nowrap width="5%">Tipo<br>Tramo</th>
			<th align="center" nowrap width="30%">Descripci�n</th>
			<th align="center" nowrap width="12%">Monto Vivienda<br>Desde UF</th>
			<th align="center" nowrap width="12%">Monto Vivienda<br>Hasta UF</th>
			<th align="center" nowrap width="12%">Monto Subsidio<br>Desde UF</th>
			<th align="center" nowrap width="12%">Monto Subsidio<br>Hasta UF</th>			
    		<th align="center" nowrap width="12%">Ahorro Minimo<br>Pactado</th>				
		</tr>
		<%
			EDD200001List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EDD200001List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EDD200001Message pvprd = (EDD200001Message) EDD200001List
							.getRecord();
		%>
		<tr>
			<td nowrap><input type="radio" name="TPSTTR"	value="<%=pvprd.getE01TPSTTR() %>" <%=chk%>/></td>
			<td nowrap align="right"><a href="javascript:goAction('203');"><%=Util.formatCell(pvprd.getE01TPSTTR()) %></a></td>
			<td nowrap align="left"><a href="javascript:goAction('203');"><%=pvprd.getE01TPSDSC() %></a></td> 																			   																		   
			<td nowrap align="right"><a href="javascript:goAction('203');"><%=Util.formatCell(pvprd.getE01TPSVDU()) %></a></td>	
			<td nowrap align="right"><a href="javascript:goAction('203');"><%=Util.formatCell(pvprd.getE01TPSVHU()) %></a></td>	
			<td nowrap align="right"><a href="javascript:goAction('203');"><%=Util.formatCell(pvprd.getE01TPSSDU()) %></a></td>
			<td nowrap align="right"><a href="javascript:goAction('203');"><%=Util.formatCell(pvprd.getE01TPSSHU()) %></a></td>
			<td nowrap align="right"><a href="javascript:goAction('203');"><%=Util.formatCell(pvprd.getE01TPSAMP()) %></a></td>						
		</tr>
		<%
			}
		%>
	</table>
<%
	}
%>

</form>
<SCRIPT language="JavaScript">
 	document.getElementById("hiddenDivNew").onclick=cancelBub;
	document.getElementById("eibsNew").onclick=showHiddenDivNew;  
</SCRIPT>
</body>
</html>

