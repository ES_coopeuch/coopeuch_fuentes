<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.datapro.constants.Entities"%>
<html>
<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import = "datapro.eibs.beans.ESD403001Message" %>
<head>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<script language="javascript">
	function enter(code1,code2,code3) {
		var oForm = top.opener.document.forms[0];
		if(top.opener.fieldName != "" && oForm[top.opener.fieldName]){oForm[top.opener.fieldName].value = code1}
  		if(top.opener.fieldDesc != "" && oForm[top.opener.fieldDesc]){oForm[top.opener.fieldDesc].value = code2}
  		if(top.opener.fieldId != "" && oForm[top.opener.fieldId]){oForm[top.opener.fieldId].value = code3}
  		top.close();
 	}
</script>
</head>

<jsp:useBean id= "ESD400001List" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<body>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<form>
<h3 align="center">
AYUDA DE
<%if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_ACCEPTANCES)){ %>
	ACEPTACIONES
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_BANK_REFERENCES)){ %>
	REFERENCIAS BANCARIAS
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_BENEFICIARIES)){ %>
	BENEFICIARIOS
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_BOARD_OF_DIRECTORS)){ %>
	JUNTA DIRECTIVA
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_COLLATERAL_ADDRESS)){ %>
	DIRECCIONES DE GARANTIAS
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_COMMERCIAL_REFERENCES)){ %>
	REFERENCIAS COMERCIALES
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_CONTACT)){ %>
	CONTACTOS
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_CREDIT_LINE_AVALS)){ %>
	AVALES DE LINEA DE CREDITO
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_CUSTOMER_ASSETS)){ %>
	ACTIVOS
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_CUSTOMER_BENEFICIARIES)){ %>
	BENEFICIARIOS DE CLIENTES
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_CUSTOMER_LEGAL_REPRESENTATIVES)){ %>
	REPRESENTANTES LEGALES DEL CLIENTE
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_CUSTOMER_LIABILITIES)){ %>
	PASIVOS
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_DISC_ECONOMIC_ACTIVITY)){ %>
	ACTIVIDAD ECONOMICA
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_DOCUMENTS)){ %>
	DOCUMENTOS
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_INSURANCE_BENEFICIARIES)){ %>
	BENEFICIARIOS DE SEGURO
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_LEGAL_REPRESENTATIVES)){ %>
	REPRESENTANTES LEGALES
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_MAILING_ADDRESS)){ %>
	DIRECCIONES
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_MANAGERS_WITH_CREDIT)){ %>
	SUPERVISORES CON CREDITO
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_PERSONAL_REFERENCE)){ %>
	REFERENCIAS PERSONALES
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_SECONDARY_NAME_FOR_CREDIT)){ %>
	SEGUNDO NOMBRE PARA CREDITO
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_SIGNER)){ %>
	FIRMANTES
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_STOCK_HOLDER)){ %>
	ACCIONISTAS
<%} else if (userPO.getHeader10().equals(Entities.ENTITY_TYPE_VALUE_OF_ASSETS)){ %>
	VALOR DE ACTIVOS
<%} %></h3>

<%
	if ( ESD400001List.getNoResult() ) {
		String sMsg = "";
		if (userPO.getHeader1().equals("*")) {
			sMsg = "Usuario no tiene permisos para ver este Cliente";
		} else {
			//sMsg = "There is no match for your search criteria";
			sMsg = "No hay datos que correspondan con su criterio de busqueda";
		}%>	
		<TABLE class="tbenter" width=100% height=100%>
	 		<TR>
    			<TD> 
        			<div align="center"> <b> <%= sMsg %></b> </div>
      		  	</TD>
      		</TR>
   		</TABLE>
   		<%
	}
	else {
		 if (userPO.getHeader10().equals("1")){%>
		 	 <table id="tableinfo" align="center" width="95%">
		 	 	<tr id="trdark">
		 	 		<th align=center nowrap width="20%">Numero</th>
		 	 		<th align=center nowrap width="40%">Nombre</th>
		 	 		<th align=center nowrap width="40%">Dirección</th>
		 	 	</tr>
		 	 	<%  ESD403001List.initRow();
                	while (ESD403001List.getNextRow()) {
						ESD403001Message msgObj = (ESD403001Message) ESD403001List.getRecord();%>
					<tr>
						<td NOWRAP  align=center width="20%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"><%= msgObj.getE01MAN() %></a></td>
						<td NOWRAP  align=center width="40%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"><%= msgObj.getE01MA1() %></a></td>
						<td NOWRAP  align=center width="40%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"/></td>
					</tr>
					<%} %>
				</table>
		 <%} else if (userPO.getHeader10().equals("6")){%>
		 	<table id="tableinfo" align="center" width="95%">
		 		<tr id="trdark">
		 			<th align=center nowrap width="20%">Numero</th>
		 			<th align=center nowrap width="40%">Nombre</th>
		 			<th align=center nowrap width="40%">Dirección</th>
		 		</tr>
		 		<% ESD400001List.initRow();
                	while (ESD400001List.getNextRow()) {
							ESD400001Message msgObj = (ESD400001Message) ESD400001List.getRecord();%>
							<tr>
								<td NOWRAP  align=center width="20%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"><%= msgObj.getE01MAN() %></a></td>
								<td NOWRAP  align=center width="40%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"><%= msgObj.getE01MA1() %></a></td>
								<td NOWRAP  align=center width="40%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"><%= msgObj.getE01MA2() %></a></td>
							</tr>
					<%} %>
		 	</table>
		 <%} else {%>
		 	<table id="tableinfo" align="center" width="95%">
		 		<tr id="trdark">
		 			<th align=center nowrap width="20%">Numero</th>
		 			<th align=center nowrap width="40%">Nombre</th>
		 			<th align=center nowrap width="20%">Identificación</th>
		 			<th align=center nowrap width="10%">Tipo</th>
		 			<th align=center nowrap width="10%">Estado</th>
		 		</tr>
		 		<%
   				ESD403001List.initRow();
                while (ESD403001List.getNextRow()) {
					ESD403001Message msgObj = (ESD403001Message) ESD403001List.getRecord();%>
					<tr> 
            			<td NOWRAP  align=center width="20%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"><%= msgObj.getE01MAN() %></a></td>
            			<td NOWRAP  align=left width="40%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"><%= msgObj.getE01MA1() %></a></td>
            			<td NOWRAP  align=left width="20%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"><%= msgObj.getE01BNI() %></a></td>
            			<td NOWRAP  align=left width="10%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"><%= msgObj.getE01RTP() %></a></td>
            			<td NOWRAP  align=left width="10%"><a href="#" onclick="enter('<%= msgObj.getE01MAN() %>', '<%= msgObj.getE01MA1() %>', '<%= msgObj.getE01BNI() %>');"><%= msgObj.getD01RTX() %></a></td>
          			</tr>
				<%} %>
				</table>
		 <% } %>
		 <TABLE  class="tbenter" WIDTH="98%" ALIGN=CENTER>
		 	<tr>
		 		<td WIDTH="50%" ALIGN=LEFT height="25">
		 		<%if ( ESD403001List.getShowPrev() ) {
      					int pos =ESD403001List.getFirstRec() - 81;
      			   		out.print("<A HREF=\""+request.getContextPath()+"/servlet/datapro.eibs.client.JSESD4030?SCREEN=105&RECTYP="+userPO.getHeader10()+"> <img src=\""+request.getContextPath()+"/images/s/previous_records.gif\" border=0></A>");
        		}%>
		 		</td>
		 		<td WIDTH="50%" ALIGN=RIGHT height="25">
		 		<%if ( ESD403001List.getShowNext() ) {
      					int pos = ESD403001List.getLastRec();
      					ut.print("<A HREF=\""+request.getContextPath()+"/servlet/datapro.eibs.client.JSESD4030?SCREEN=105&RECTYP="+userPO.getHeader10()+"><img src=\""+request.getContextPath()+"/images/s/next_records.gif\" border=0></A>");
				}%>
		 		</td>
		 	</tr>
		 </TABLE>	
	<%} %>
</form>
</body>
</html>