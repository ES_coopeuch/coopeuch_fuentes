package datapro.eibs.client;

/**
 * Servlet de Plan socio.
 * Creation date: (15/02/18)  
 * @author: Jose Vivas
 */

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adobe.fdf.FDFDoc;

import datapro.eibs.beans.EDD100002Message;
import datapro.eibs.beans.EFRM00001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100500Message;
import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV109001Message;
import datapro.eibs.beans.ESD008001Message;
import datapro.eibs.beans.ESD404001Message;
import datapro.eibs.beans.ESD408001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.reports.JSEFRM000PDF;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

public class JSESD4040F extends JSEFRM000PDF {
	
	protected static final int R_EVALUATION_LIST = 1000;	
	protected static final int R_FORMALIZATION_LIST = 2000;
	protected static final int A_LIST = 3000;
	protected static final int R_FORMS_LIST = 4000;	
	
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_EVALUATION_LIST:
			case R_FORMALIZATION_LIST:	  
				procReqList(user, req, res, session, screen);
				break;
			case A_LIST:
				procActionList(user, req, res, session);
				break;
			case R_FORMS_LIST:
				procReqFormList(user, req, res, session);
				break;
			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}

	protected void procReqFormList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		UserPos userPO = getUserPos(session);
		String opecode = "EVALUATION".equals(userPO.getPurpose()) ? "1000" : "2000";
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1090", req);
			EPV109001Message msgList = (EPV109001Message) mp.getMessageRecord("EPV109001", user.getH01USR(), opecode);
			try {
				msgList.setE01PVMNUM(req.getParameter("E01PVMNUM").trim());
			} catch (Exception e) {
				msgList.setE01PVMNUM(userPO.getAccNum());
			}
			msgList.setE01PVMCUN(userPO.getCusNum());
			
			mp.sendMessage(msgList);
			
			JBObjList list = mp.receiveMessageRecordList("E01MORFRM");
			
			if (mp.hasError(list)) {
				session.setAttribute("error", mp.getError(list));
				forward("EPV1090_salesplatform_list.jsp", req, res);
			} else if (!list.isEmpty()) {
				FDFDoc outputFDF = new FDFDoc();
				if (session.getAttribute("pdfData") != null) {
					session.removeAttribute("pdfData");
				}
				if (dstXML == null || dstXML.isEmpty()) {
					initDataStructure();			
					flexLog("XML file was reloaded.");
				}
				// Add General Information like User ID....
				getFormData(outputFDF, (String)formatNames.get("ESS0030DS") + ".", user);
                MessageRecord newmessage = mp.receiveMessageRecord();
                
                String StrOpe = newmessage.getFieldString("E60LNSACC").toString();
                String StrNum = req.getParameter("E01PVMNUM").trim();
                String StrPrd = newmessage.getFieldString("E60PVEPRD").toString();
                session.setAttribute("StrOpe", StrOpe);
                session.setAttribute("StrNum", StrNum);
                session.setAttribute("StrPrd", StrPrd);
                
                int index = 0;
                String prefix = "";
                String ddsName = newmessage.getFormatName();
				while (true) {
					if (!ddsName.equals(newmessage.getFormatName())
						|| !prefix.equals(getPrefix(newmessage))) {
						index = (hasIndex(newmessage.getFormatName())) ? 1 : 0;
					}
					prefix = getPrefix(newmessage);
                	ddsName = newmessage.getFormatName();
                    if (ddsName.equals("EFRM00001")) {
                        break;
                    } else {                             	
                		buildFormList(outputFDF, newmessage, "." + prefix, index++);
                		newmessage = mp.receiveMessageRecord();
                    }
				}
				 
            	String StrId = newmessage.getFieldString("E01APFDDS").toString(); 
            	session.setAttribute("StrId", StrId);

				if (list.getLastRec() == 0) {
					EFRM00001Message msgHeader = (EFRM00001Message) list.get(0);
					String urlPDF = datapro.eibs.master.JSEIBSProp.getFORMPDFURL() + msgHeader.getE01APFPTH();
					sendPdf(
						req, 
						res, 
						urlPDF, 
						outputFDF, 
						msgHeader.getE01APFOPE(), 
						msgHeader.getE01APFCPI(), 
						msgHeader.getE01APFDDS());
				} else {
					flexLog("Putting java beans into the session");
					session.setAttribute("pdfData", outputFDF);							
					session.setAttribute("pdfList", list);
					forward("ESD4040F_pdf_list.jsp", req, res);
				}
			} else {
				session.setAttribute("error_msg", "");
				forward("EFRM000_pdf_forms_error.jsp", req, res);
			}
			
			
		} finally {
			if (mp != null) mp.close();
		}
	}

	private void procActionList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1005", req);
			EPV100501Message msgList = (EPV100501Message) mp.getMessageRecord("EPV100501", user.getH01USR(), "0015");
			try {
				msgList.setE01SELCUN(req.getParameter("customer_number").trim());
			} catch (Exception e) {
				msgList.setE01SELCUN(userPO.getCusNum());
			}
			msgList.setE01SELSTS("EVALUATION".equals(userPO.getPurpose()) ? "5" : "6"); //B: Ofertadas, G: Formalizadas.

			// Sends message
			mp.sendMessage(msgList);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			//
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				forward("EPV1090_salesplatform_client_search.jsp", req, res);
			} else {
				EPV100500Message header = (EPV100500Message) list.get(0);	
				userPO.setCusNum(header.getE00CUSCUN());
				userPO.setCusType(header.getE00CUSIDN());
				userPO.setCusName(header.getE00CUSNA1());
				// save customer number
				userPO.setRedirect(srctx +  "/servlet/datapro.eibs.salesplatform.JSEPV1090?SCREEN=" + A_LIST);
				// if there are NO errors display list
				session.setAttribute("userPO", userPO);
				list.remove(0);
				session.setAttribute("EPV100501List", list);
				if (list.size() == 0) {//then no occurrences
					if (!"S".equals(req.getParameter("fin"))){//Only show when not fin
						error.setERRNUM("1");
						error.setERNU01("0001");		//4		                
						error.setERDS01("No existen solicitudes evaluadas para este Cliente");//70					
						session.setAttribute("error", error);						
					}
					forward("EPV1090_salesplatform_client_search.jsp", req, res);	
				} else {
					forward("EPV1090_salesplatform_list.jsp", req, res);
				}				
			}

		} finally {
			if (mp != null) mp.close();
		}
	}

	private void procReqList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		userPO.setPurpose(screen == R_EVALUATION_LIST ? "EVALUATION" : "FORMALIZATION");
		session.setAttribute("userPO", userPO);
		forward("EPV1090_salesplatform_client_search.jsp", req, res);
	}
}
