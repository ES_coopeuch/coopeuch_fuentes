<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Nuevo Plan Socio</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<SCRIPT Language="Javascript">

function goCV() {
	self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0711?TYPE=RT&tipoProducto=CV&pagOperation=1";
}

function goCA() {
	self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0711?TYPE=04&tipoProducto=LA&pagOperation=1";
}

//  Process according with user selection
 var bandera;	
 function goAction(op) {
	
	
	switch (op){
	// Validate & Write 
  	case 1:  {
    	document.forms[0].SCREEN.value="200";   
    	document.forms[0].NUEVO.value="2"; 	
		document.forms[0].submit();
       	break;
        }
	// Validate and Approve
	case 2:  {
		if (document.forms[0].TIENECV.value == "S" && document.forms[0].TIENELA.value == "S")
		{
	 		document.forms[0].SCREEN.value="200";   
	    	document.forms[0].NUEVO.value="1"; 	
			document.forms[0].submit();
	       	break;
       	}
       	else {
       		alert("Para ser integrante de un plan debe tener por lo menos una cuenta vista y una libreta de ahorro");
       		break;
		}
	}
	}
}	


</SCRIPT>

 
<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "userPOAux" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<jsp:useBean id= "msgPlanSocioIntegrante" class= "datapro.eibs.beans.ESD406001Message"  scope="session" />
<jsp:useBean id= "msgPlanSocio" class= "datapro.eibs.beans.ESD406001Message"  scope="session" />
<jsp:useBean id= "idCliente" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "nombreCliente" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "servlet" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "nuevo" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "ESD406001List" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "rut" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "la" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "cv" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "tc" class= "java.lang.String"  scope="session" />
</head>

<body bgcolor="#FFFFFF">


<h3 align="center">Productos del Integrante<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="productos_integrantes, ESD4070"  ></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4060" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
  <INPUT TYPE=HIDDEN NAME="APPROVAL" VALUE="N">
  <input type="hidden" name="E01CUN" value="<%= userPO.getCusNum() %>" size=15 maxlength=9 >
  <input type="hidden" name="E01IDN" value="<%= userPO.getHeader2() %>" size=15 maxlength=9 >
  <input type="hidden" name="IDCLIENTE" value="<%= idCliente %>" size=15 maxlength=9 >
  <input type="hidden" name="NOMBRECLIENTE" value="<%= nombreCliente %>" size=15 maxlength=9 >
  <input type="hidden" name="NUEVO" value="<%= nuevo %>" size=15 maxlength=9 >
  <input type="hidden" name="RUT" value="<%= rut %>" size=15 maxlength=9 >
  <input type="hidden" name="LA" value="<%= la %>" size=15 maxlength=9 >
  <input type="hidden" name="CV" value="<%= cv %>" size=15 maxlength=9 >
  <input type="hidden" name="TC" value="<%= tc %>" size=15 maxlength=9 >
  <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">Cliente: </td>
					<td nowrap width="12%" aling="left" > <%=msgPlanSocioIntegrante.getL01FILLE3() %> </td>
					
					<td nowrap width="6%" aling="right">RUT: </td>
					<td nowrap width="14%" aling="left" > <%=msgPlanSocioIntegrante.getE01AIIDA() %> </td>
					
					<td nowrap width="8%" aling="right">Nombre: </td>
					<td nowrap width="20%" aling="left" > <%=msgPlanSocioIntegrante.getE01AINMA() %> </td>
					
					<td nowrap width="8%" aling="right">Estado Plan: </td>
					<td nowrap width="20%" aling="left" > <% if (msgPlanSocioIntegrante.getE01AIED().equals("Y")) out.print("Activo"); else out.print("Inactivo"); %> </td>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
	
    <br>
    <h4 align="center">Productos Activos</h4>
    <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">Cuenta Coopeuch </td>
					<% if (msgPlanSocioIntegrante.getE01AIPACV().equals("S") || cv.equals("CV")) {%>
						<td nowrap width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >
						<input type="hidden" name="TIENECV" value="S" size=15 maxlength=9 > 
						</td>
					<%} else { %>
						<td nowrap width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" >
						<input type="hidden" name="TIENECV" value="N" size=15 maxlength=9 >  
						</td>
					<% } %>
					<td nowrap width="6%" aling="right">Cuenta Ahorro: </td>
					<% if (msgPlanSocioIntegrante.getE01AIPALA().equals("S") || la.equals("LA")) {%>
						<td nowrap width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >
						<input type="hidden" name="TIENELA" value="S" size=15 maxlength=9 >  
						</td>
					<%} else { %>
						<td nowrap width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" >
						<input type="hidden" name="TIENELA" value="N" size=15 maxlength=9 >  
						</td>
					<% } %>
					<td nowrap width="8%" aling="right">Tarjeta de Credito: </td>
					<% if (msgPlanSocioIntegrante.getE01AIPATC().equals("S") || tc.equals("TC")) {%>
						<td nowrap width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >
						</td>
					<%} else { %>
						<td nowrap width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" > </td>
					<% } %>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
    <br>
    <h4 align="center">Asociar Productos al Plan</h4>
    <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td class=TDBKG width="30%"> 
					<% if (msgPlanSocioIntegrante.getE01AIPACV().equals("S")) {%>
						<div align="center" disabled><b>Cuenta Coopeuch</b></div>    
					<%} else { %>
						<div align="center"><a href="javascript:goCV()"><b>Cuenta Coopeuch</b></a></div>    
					<% } %>         
                    </td>
                    
                    <td class=TDBKG width="30%"> 
                     <div align="center"><a href="javascript:goCA()"><b>Cuenta Ahorro</b></a></div>              
                    </td>					
						
					<td class=TDBKG width="30%"> 
					<% if (msgPlanSocioIntegrante.getE01AIPATC().equals("S")) {%>
						<div align="center" disabled><b>Tarjeta de Credito</b></div>      
					<%} else { %>
						<div align="center"><a href="javascript:goNew()"><b>Tarjeta de Credito</b></a></div>      
					<% } %>
                             
                    </td>
					
				</tr>
			</table> 
		</td>
      </tr>
    </table>  	
  	<br>
	<table width="100%">		
  	<tr>
			<td width="15%">
	  		  <div align="center"> 
	     		<input id="EIBSBTN" type="button" name="Submit" value="Imprimir" onClick="javascript:goAction(1);">
	     	  </div>	
	  		</td>
	  		<td width="15%">
	  		  <div align="center"> 
	     		<input id="EIBSBTN" type="button" name="Submit" value="Cancelar" onClick="javascript:goAction(1);">
	     	  </div>	
	  		</td>
			<td width="75%">
  		  		<div align="center">
				<input id="EIBSBTN" type="button" name="Submit2" value="A�adir" onClick="javascript:goAction(2);">
     	  	 	</div>	
	  		</td>
		
  	</tr>	
</table>	
</form>
</body>
</html>

