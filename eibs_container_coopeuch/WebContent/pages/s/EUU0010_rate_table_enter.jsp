<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<META HTTP-EQUIV="Pragma" CONTENT="No-cache">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<title>Tasas Maximas</title>
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
</head>

<body nowrap>

<h3 align="center">Nueva Tabla de Tasa Maxima Convencional<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="rate_table_enter, EUU0010"></h3>
<hr size="4">
<form method="post"  action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEUU0010">
  <input type=HIDDEN name="SCREEN" value="3">
  <INPUT TYPE=HIDDEN NAME="E02CDRSFL" VALUE="">
  
  <table  class="tbenter" width="100%" border="0" cellspacing=0 cellpadding=2>
	  <tr>
	  	<td width="50%"> 
          <div align="right">Banco : </div>
        </td>
        <td>
        	<input type="text" name="E01USUBNK" size="3" maxlength="3" onKeypress="enterInteger()" value="01">
        </td>
      </tr>
	  <tr>
	    <td width="50%"> 
          <div align="right">C&oacute;digo de Moneda : </div>
        </td>
	  	<td>
	  	   <input type="text" name="E01USUCCY" size="4" maxlength="3" value="CLP">
	  	   <a href="javascript:GetCurrency('E01USUCCY','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"  ></a>         
        </td>
	  </tr>
	  <tr>
	  	<td width="50%"> 
          <div align="right">Fecha : </div>
        </td>
        <td>
        	<input type="text" name="E01USUDDD" size="3" maxlength="2" onKeypress="enterInteger()">
        	<input type="text" name="E01USUDMM" size="3" maxlength="2" onKeypress="enterInteger()">
        	<input type="text" name="E01USUDYY" size="5" maxlength="4" onKeypress="enterInteger()">
        </td>
      </tr>
	  <tr>
	    <td width="50%"> 
          <div align="right">Plazo : </div>
        </td>
	  	<td>
	  	   <input type="text" name="E01USUPZO" size="6" maxlength="5">         
        </td>
	  </tr>      
	  <tr>
	    <td width="50%"> 
          <div align="right">Moneda Monto: </div>
        </td>
	  	<td>
	  	   <input type="text" name="E01USURCY" size="4" maxlength="3">
	  	   <a href="javascript:GetCurrency('E01USURCY','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"  ></a>         
        </td>
	  </tr>
	  <tr>
	    <td width="50%"> 
          <div align="right">Dias Base: </div>
        </td>
	  	<td>
	  	   <input type="text" name="E01USUBSE" size="4" maxlength="3" onKeypress="enterInteger()" value="360">
        </td>
	  </tr>	 
	   
  </table>
      
  <p align="center">         
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
  
<script language="JavaScript">
  document.forms[0].E01USUDDD.focus();
  document.forms[0].E01USUDDD.select();
</script>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>

</html>



