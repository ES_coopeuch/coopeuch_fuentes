package com.datapro.eibs.security.object;

/**
 * Insert the type's description here.
 * Creation date: (7/19/00 6:55:55 PM)
 * @author: Ramses
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.math.BigDecimal;
import com.datapro.sql.manager.*;
import com.datapro.eibs.security.bean.*;
import com.datapro.eibs.security.dao.*;
import com.datapro.eibs.security.facade.*;
import com.datapro.generic.beanutil.*;

import java.sql.Connection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;


public class GroupProfile {

	/**
	 * 
	 */
	public GroupProfile() {
		super();
	}
	/**
	 * 
	 */
	public void destroy() {

	}
	
	public JBObjList getGroup() {

		CNTRLBTHDao groupDao = new CNTRLBTHDao("AS400");
		
		JBObjList userBean = new JBObjList();
		try {
			
			userBean = groupDao.selectGroup();
			//groupDao.close();
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return userBean;

	}
	
	public CNTRLBTH getGroup(String USERID) {

		CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
		CNTRLBTH userBTHBean = null;
		
		try {
			
			userBTHBean = userBTHDao.select(USERID);
			//userBTHDao.close();
				
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
				 
		return userBTHBean;
	}
	
	public JBObjList getUserByGroup(String GROUPID) {

		CNTRLBTHDao groupDao = new CNTRLBTHDao("AS400");
		
		JBObjList userBean = new JBObjList();
		try {
			
			userBean = groupDao.selectByGroup(GROUPID);
			//groupDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return userBean;

	}
	
	public int delete(String GROUPID) {
				
		int result = 0;

		try {
			CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
			result = userBTHDao.delete(GROUPID);
			if (result != 0) {
				    int result1 = deleteGroupPrivilege(GROUPID);
			}
			//userBTHDao.close();
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 
		
		return result;
	}
	
	public int updateUserByGroup(String GROUPID) {
				
		int result = 0;

		try {
			CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
			result = userBTHDao.updateByGroup(GROUPID);
			//userBTHDao.close();
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 

		return result;
	}
	
	public int update(CNTRLBTH userBTH) {
		
		CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
				
		
		int result=0;
		try {
						
			result = userBTHDao.update(userBTH);
			//userBTHDao.close();			    
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 

		return result;
	}
	
	public int add(CNTRLBTH userBTH) {
		
		CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
				
		
		int result=0;
		try {
						
			result = userBTHDao.insert(userBTH);
			//userBTHDao.close();			    
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 

		return result;
	}
		
	
	public JBObjList getGroupPrivilege(String GROUPID) {

		CNTRLPVLDao groupPVLDao = new CNTRLPVLDao("AS400");
		
		JBObjList userBean = new JBObjList();
		try {
			
			userBean = groupPVLDao.select(GROUPID);
			//groupPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return userBean;
	}
	
	public CNTRLPVL getGroupPrivilege(String USERID,String PVLCOD) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		CNTRLPVL userPVLBean = null;
		
		try {
			
			userPVLBean = userPVLDao.select(USERID,PVLCOD);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return userPVLBean;
	}
	
	public int deleteGroupPrivilege(String USERID) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		int result = 0;
		
		try {
			
			result = userPVLDao.delete(USERID);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
	
	public int deleteGroupPrivilege(String USERID,String PVLCOD) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		int result = 0;
		
		try {
			
			result = userPVLDao.delete(USERID,PVLCOD);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
	
	public int addGroupPrivilege(String USERID,String PVLCOD,String PVLVAL) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		CNTRLPVL bean = new CNTRLPVL();
		int result = 0;
		
		try {
			bean.setPVLUSR(USERID);
			bean.setPVLPVL(PVLCOD);
			bean.setPVLVAL(PVLVAL);
			result = userPVLDao.insert(bean);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
	
	public int updateGroupPrivilege(String USERID,String PVLCOD,String PVLVAL) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		CNTRLPVL bean = new CNTRLPVL();
		int result = 0;
		
		try {
			bean.setPVLUSR(USERID);
			bean.setPVLPVL(PVLCOD);
			bean.setPVLVAL(PVLVAL);
			result = userPVLDao.update(bean);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
}
