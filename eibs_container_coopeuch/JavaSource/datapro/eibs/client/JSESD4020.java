package datapro.eibs.client;

/*********************************************************************************************************************************/
/**  Creado     por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  07/26/2012                                                                                      **/
/**  Objetivo                :  Servicio de mantencion de datos adicionales CES                                                 **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/
import java.beans.Beans;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.DataTransaction;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD402001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSESD4020 extends JSEIBSServlet {

	// Action 
	protected static final int R_MAINTENANCE = 1;
	
	protected static final int R_NEW = 300;
	protected static final int R_DELETE = 700;
	protected static final int A_MAINTENANCE = 600;
	protected static final int R_INQUIRY_LIST = 800;
	protected static final int R_INQUIRY = 900;
	protected static final int R_APPROVAL = 200;

	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
        flexLog("entre a processRequest " + screen);
		switch (screen) {
			case R_MAINTENANCE :
				procReqMaintenance(user, req, res, session);
				break;	
			case A_MAINTENANCE :
				procActionMaintenance(user, req, res, session);
				break;	
			case R_NEW :
				procReqNew(user, req, res, session);
				break;
			case R_DELETE :
				procReqDelete(user, req, res, session);
				break;
	
			case R_INQUIRY :
				procReqInquiry(user, req, res, session);
				break;		
			default :
				//forward("MISC_not_available.jsp", req, res);
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}
	
protected void procReqMaintenance(
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession session)
		throws ServletException, IOException {
		DataTransaction cesData =	(DataTransaction) session.getAttribute("cesData");
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("ESD4020", req);
			ESD402001Message msgList = (ESD402001Message) mp.getMessageRecord("ESD402001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ESD4020");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			msgList.setH01OPECOD("0001");
			msgList.setE01ADICUN(cesData.getRefNum());
		 	msgList.setE01ADIACC(cesData.getAccNum());		
		 	msgList.setE01ADIRTP("S");	
		 	msgList.setE01ADIMAN("01");	
		 	mp.sendMessage(msgList);
		 
		 	ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msgList = (ESD402001Message) mp.receiveMessageRecord();
			if (!mp.hasError(msgError)) {
				userPO.setPurpose("MAINTENANCE");
			} else {	
				userPO.setPurpose("NEW");
			}
			session.setAttribute("userPO", userPO);
			session.setAttribute("lnCes", msgList);
			
			forward("ESD4020_ln_datos_ces.jsp", req, res);				

		} finally {
			if (mp != null)	mp.close();
		}
  }	
  	
protected void procActionMaintenance(
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession session)
		throws ServletException, IOException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		MessageProcessor mp = null;
		
		try {
			mp = getMessageProcessor("ESD4020", req);
			ESD402001Message msgList = (ESD402001Message) mp.getMessageRecord("ESD402001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ESD4020");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			if (userPO.getPurpose().equals("NEW")) {
				msgList.setH01OPECOD("0002"); 
			} else {	
				msgList.setH01OPECOD("0003"); 
			}
			msgList.setE01ADIACC(userPO.getIdentifier());
			msgList.setE01ADICUN(userPO.getHeader2());	
		 	msgList.setE01ADIRTP("S");	
		 	msgList.setE01ADIMAN("01");	
			
			java.util.Enumeration enu = msgList.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}
	        flexLog("mensaje enviado..." + msgList);
		 	mp.sendMessage(msgList);
		 	
		 	ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
		 	msgList = (ESD402001Message) mp.receiveMessageRecord();
            
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("lnCes", msgList);
			forward("ESD4020_ln_datos_ces.jsp", req, res);				

		} finally {
			if (mp != null)	mp.close();
		}
    }

protected void procReqNew(
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession session)
		throws ServletException, IOException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		MessageProcessor mp = null;
		
		try {
			
			JBObjList bl = (JBObjList) session.getAttribute("ESD402001Help");
						
			mp = getMessageProcessor("ESD4020", req);

			ESD402001Message msgList = (ESD402001Message) mp.getMessageRecord("ESD402001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ESD4020");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			msgList.setH01OPECOD("0001"); 

		 	mp.sendMessage(msgList);
		 
		 	ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msgList = (ESD402001Message) mp.receiveMessageRecord();

			if (!mp.hasError(msgError)) {
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				session.setAttribute("lnCes", msgList);
				forward("ESD4020_ln_datos_ces.jsp", req, res);
			} else {	
				session.setAttribute("error", msgError);
				forward("error_viewer.jsp", req, res);
			}				

		} finally {
			if (mp != null)	mp.close();
		}
  }	

protected void procReqDelete(
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession session)
		throws ServletException, IOException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("MAINTENANCE");
		
		MessageProcessor mp = null;
		
		try {
			
			JBObjList bl = (JBObjList) session.getAttribute("ESD402001Help");
						
			mp = getMessageProcessor("ESD4020", req);

			ESD402001Message msgList = (ESD402001Message) mp.getMessageRecord("ESD402001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ESD4020");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			msgList.setH01OPECOD("0009");
		 	

		 	mp.sendMessage(msgList);
		 
		 	ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			if (!mp.hasError(msgError)) {
				redirectToPage("/servlet/datapro.eibs.client.JSESD4020?SCREEN=100&RECTYP=" +userPO.getHeader10(), res);
			} else {	
				session.setAttribute("error", msgError);
				forward("error_viewer.jsp", req, res);
			}				

		} finally {
			if (mp != null)	mp.close();
		}
  }	


protected void procReqInquiry(
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession session)
		throws ServletException, IOException {
		DataTransaction cesData =	(DataTransaction) session.getAttribute("cesData");
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		MessageProcessor mp = null;
		
		try {						
			mp = getMessageProcessor("ESD4020", req);
			ESD402001Message msgList = (ESD402001Message) mp.getMessageRecord("ESD402001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ESD4020");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01OPECOD("0001");
			msgList.setE01ADICUN(cesData.getRefNum());
		 	msgList.setE01ADIACC(cesData.getAccNum());		
		 	msgList.setE01ADIRTP("S");	
		 	msgList.setE01ADIMAN("01");	 	
		 	mp.sendMessage(msgList);
		 
		 	ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msgList = (ESD402001Message) mp.receiveMessageRecord();
			session.setAttribute("userPO", userPO);
			session.setAttribute("lnCes", msgList);
			forward("ESD4020_ln_inq_datos_ces.jsp", req, res);
		

		} finally {
			if (mp != null)	mp.close();
		}
  }	

 }	



