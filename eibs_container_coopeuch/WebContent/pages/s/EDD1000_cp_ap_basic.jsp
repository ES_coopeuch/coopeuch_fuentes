<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>

<html>
<head>
<title>Cuentas Cuotas de Participacion</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="cpBasic" class="datapro.eibs.beans.EDD000001Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

	builtNewMenu(cpar_a_opt); 
 
</SCRIPT>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
   out.println("<SCRIPT> initMenu(); </SCRIPT>");
%> 
<H3 align="center"> Aprobacion de Cuotas de Participaci&oacute;n<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cp_ap_basic.jsp, EDD1000" width="32" height="32"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXEDD0000" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="62">
  <INPUT TYPE=HIDDEN NAME="E01ACMBNK" VALUE="<%= cpBasic.getE01ACMBNK().trim()%>">
  <INPUT TYPE=HIDDEN NAME="E01ACMATY" VALUE="<%= cpBasic.getE01ACMATY().trim()%>">
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left">
                <input type="text" name="E01ACMCUN" size="10" maxlength="9" value="<%= cpBasic.getE01ACMCUN().trim()%>">
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2">
                <input type="text" name="E01CUSNA1" size="45" maxlength="45" value="<%= cpBasic.getE01CUSNA1().trim()%>">
                </font></font></font></div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left">
                <input type="text" name="E01ACMACC" size="13" maxlength="12" value="<%= cpBasic.getE01ACMACC().trim()%>">
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
                <input type="text" name="E01DEACCY" size="3" maxlength="3" value="<%= userPO.getCurrency().trim()%>" readonly>
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b>
                <input type="text" name="E01ACMPRO" size="4" maxlength="4" value="<%= cpBasic.getE01ACMPRO().trim()%>">
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <H4>Datos B&aacute;sicos de la Operaci&oacute;n</H4>
<table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
         <tr id="trclear"> 
            <td nowrap width="29%"> 
              <div align="right">Nombre de la Cuenta:</div>
            </td>
            <td nowrap width="19%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMNME().equals("Y")) out.print("id=\"txtchanged\""); %>  name="E01ACMNME" size="60" maxlength="80" value="<%= cpBasic.getE01ACMNME().trim()%>">
            </td>
            <td nowrap width="26%"> 
              <div align="right">C&oacute;digo Referencial:</div>
            </td>
            <td nowrap width="26%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMRCD().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMRCD" size="25" maxlength="30" 
				  value="<% if (cpBasic.getE01ACMRCD().equals("D")) out.print("Referencia Debitos");
							else if (cpBasic.getE01ACMRCD().equals("C")) out.print("Referencia Creditos");
							else if (cpBasic.getE01ACMRCD().equals("A")) out.print("Referencia Ambos");
						    else out.print("");%>" 
				>
            </td>
          </tr>        
          <tr id="trdark"> 
            <td nowrap width="29%"> 
              <div align="right">Fecha de Apertura:</div>
            </td>
            <td nowrap width="19%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMOP1().equals("Y")) out.print("id=\"txtchanged\""); %>  name="E01ACMOP1" size="3" maxlength="2" value="<%= cpBasic.getE01ACMOP1().trim()%>">
              <input type="text" readonly <% if (cpBasic.getF01ACMOP2().equals("Y")) out.print("id=\"txtchanged\""); %>  name="E01ACMOP2" size="3" maxlength="2" value="<%= cpBasic.getE01ACMOP2().trim()%>">
              <input type="text" readonly <% if (cpBasic.getF01ACMOP3().equals("Y")) out.print("id=\"txtchanged\""); %>  name="E01ACMOP3" size="5" maxlength="4" value="<%= cpBasic.getE01ACMOP3().trim()%>">
            </td>
            <td nowrap width="26%"> 
              <div align="right">Fecha de Ingreso:</div>
            </td>
            <td nowrap width="26%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMSU1().equals("Y")) out.print("id=\"txtchanged\""); %>  name="E01ACMSU1" size="3" maxlength="2" value="<%= cpBasic.getE01ACMSU1().trim()%>">
              <input type="text" readonly <% if (cpBasic.getF01ACMSU2().equals("Y")) out.print("id=\"txtchanged\""); %>  name="E01ACMSU2" size="3" maxlength="2" value="<%= cpBasic.getE01ACMSU2().trim()%>">
              <input type="text" readonly <% if (cpBasic.getF01ACMSU3().equals("Y")) out.print("id=\"txtchanged\""); %>  name="E01ACMSU3" size="5" maxlength="4" value="<%= cpBasic.getE01ACMSU3().trim()%>"> 
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="29%"> 
              <div align="right">Retenci&oacute;n de Impuestos:</div>
            </td>
            <td nowrap width="19%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMWHF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMWHF" size="2" maxlength="1" value="<%= cpBasic.getE01ACMWHF().trim()%>">
            </td>
            <td nowrap width="26%"> 
              <div align="right">Tipo Cuenta :</div>
            </td>
            <td nowrap width="26%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMPEC().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMPEC" size="20" maxlength="20" 
				  value="<% if (cpBasic.getE01ACMPEC().equals("1")) out.print("Indistinto");
							else if (cpBasic.getE01ACMPEC().equals("2")) out.print("Conjunto");
							else out.print("");%>" 
				>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="29%" height="23"> 
              <div align="right">Mensajes al Cliente:</div>
            </td>
            <td nowrap width="19%" height="23"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMPMF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMPMF" size="3" maxlength="2" 
				  value="<% if (cpBasic.getE01ACMPMF().equals("Y")) out.print("Si");
							else if (cpBasic.getE01ACMPMF().equals("N")) out.print("No");
							else out.print("");%>" 
				>
            </td>
            <td nowrap width="26%" height="23"> 
              <div align="right">Manejo de Sub-Cuentas:</div>
            </td>
            <td nowrap width="26%" height="23"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMPTF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMPTF" size="3" maxlength="2" 
				  value="<% if (cpBasic.getE01ACMPTF().equals("Y")) out.print("Si");
							else if (cpBasic.getE01ACMPTF().equals("N")) out.print("No");
							else out.print("");%>" 
				>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="29%" height="19"> 
              <div align="right">N&uacute;mero de Direcci&oacute;n Postal:</div>
            </td>
            <td nowrap width="19%" height="19"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMMLA().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMMLA" size="2" maxlength="2" value="<%= cpBasic.getE01ACMMLA().trim()%>">
            </td>
            <td nowrap width="29%" height="19"> 
              <div align="right">Centro de Costo:</div>
            </td>
            <td nowrap width="19%" height="19"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMCCN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMCCN" size="9" maxlength="8" value="<%= cpBasic.getE01ACMCCN().trim()%>">
            </td>

          </tr>
          <tr id="trdark"> 
            <td nowrap width="29%" height="19"> 
              <div align="right">Diferidos Locales:</div>
            </td>
            <td nowrap width="19%" height="19"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMWLF().equals("Y")) out.print("id=\"txtchanged\""); %> size="2" maxlength="1" value="<%= cpBasic.getE01ACMWLF().trim()%>" name="E01ACMWLF">
            </td>
            <td nowrap width="26%" height="19"> 
              <div align="right">Diferidos No Locales:</div>
            </td>
            <td nowrap width="26%" height="19"> 
              <input type="text" size="2" readonly <% if (cpBasic.getF01ACMWNF().equals("Y")) out.print("id=\"txtchanged\""); %> maxlength="1" value="<%= cpBasic.getE01ACMWNF().trim()%>" name="E01ACMWNF">
            </td>
          </tr>
          <tr id="trclear">
            <td nowrap width="26%" height="19"> 
              <div align="right">Tabla de Documentos Requeridos:</div>
            </td>
            <td nowrap width="26%" height="19"> 
              <input type="text" size="2" maxlength="1" readonly value="<%= cpBasic.getE01APCTNU().trim()%>" name="text">
            </td>          
            <td nowrap width="29%" height="19">
              <div align="right">Banco/Sucursal :</div>
            </td>
            <td nowrap colspan=3>            
              <input type="text" name="E01ACMBNK" size="2" maxlength="2" value="<%= cpBasic.getE01ACMBNK().trim()%>" readonly>
              <input type="text" name="E01ACMBRN" size="5" maxlength="4" value="<%= cpBasic.getE01ACMBRN().trim()%>" readonly>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <H4>Informaci&oacute;n para Cargos por Servicios</H4>
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="30%"> 
              <div align="right">Cargos por Servicios :</div>
            </td>
            <td nowrap width="18%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMSCF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMSCF" size="3" maxlength="2" 
				  value="<% if (cpBasic.getE01ACMSCF().equals("Y")) out.print("Si");
							else if (cpBasic.getE01ACMSCF().equals("N")) out.print("No");
							else out.print("");%>" 
				>
            </td>
            <td nowrap width="28%"> 
              <div align="right">Frecuencia Cobro de Cargos :</div>
            </td>
            <td nowrap width="24%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMSHF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMSHF" size="25" maxlength="30" 
				  value="<% if (cpBasic.getE01ACMSHF().equals("D")) out.print("Diario");
							else if (cpBasic.getE01ACMSHF().equals("W")) out.print("Semanal");
							else if (cpBasic.getE01ACMSHF().equals("B")) out.print("Quincenal");
							else if (cpBasic.getE01ACMSHF().equals("M")) out.print("Mensual");
							else if (cpBasic.getE01ACMSHF().equals("Q")) out.print("Trimestral");
						    else out.print("");%>" 
				>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="30%"> 
              <div align="right">C&oacute;digos Tabla de Cargos :</div>
            </td>
            <td nowrap width="18%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMACL().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMACL" size="2" maxlength="2" value="<%= cpBasic.getE01ACMACL().trim()%>">
            </td>
            <td nowrap width="28%"> 
              <div align="right">Ciclo/D&iacute;a Cobro de Cargos :</div>
            </td>
            <td nowrap width="24%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMSHY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMSHY" size="3" maxlength="2" value="<%= cpBasic.getE01ACMSHY().trim()%>">
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <H4>Informaci&oacute;n Estado de Cuenta</H4>
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="30%"> 
              <div align="right">Frecuencia de Estado de Cuenta :</div>
            </td>
            <td nowrap width="18%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMSTF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMSTF" size="25" maxlength="30" 
				  value="<% if (cpBasic.getE01ACMSTF().equals("D")) out.print("Diario");
							else if (cpBasic.getE01ACMSTF().equals("W")) out.print("Semanal");
							else if (cpBasic.getE01ACMSTF().equals("B")) out.print("Quincenal");
							else if (cpBasic.getE01ACMSTF().equals("M")) out.print("Mensual");
							else if (cpBasic.getE01ACMSTF().equals("Q")) out.print("Trimestral");
							else if (cpBasic.getE01ACMSTF().equals("S")) out.print("Semestral");
							else if (cpBasic.getE01ACMSTF().equals("Y")) out.print("Anual");
						    else out.print("");%>" 
				>
            </td>
            <td nowrap width="28%"> 
              <div align="right">Retenci&oacute;n de Correos :</div>
            </td>
            <td nowrap width="24%"><font face="Arial" size="2"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMHSF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMHSF" size="3" maxlength="2" 
				  value="<% if (cpBasic.getE01ACMHSF().equals("H")) out.print("Si");
							else if (cpBasic.getE01ACMHSF().equals("")) out.print("No");
							else out.print("");%>" 
				>
              </font> </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="30%"> 
              <div align="right">Ciclo/D&iacute;a Impresi&oacute;n Estado de Cuentas 
                :</div>
            </td>
            <td nowrap width="18%"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMSDY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMSDY" size="3" maxlength="2" value="<%= cpBasic.getE01ACMSDY().trim()%>">
            </td>
            <td nowrap width="28%"> 
              <div align="right">Estado de Cuentas Consolidado :</div>
            </td>
            <td nowrap width="24%"><font face="Arial" size="2"> </font><font face="Arial" size="2"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMCSF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMCSF" size="3" maxlength="2" 
				  value="<% if (cpBasic.getE01ACMCSF().equals("Y")) out.print("Si");
							else if (cpBasic.getE01ACMCSF().equals("N")) out.print("No");
							else out.print("");%>" 
				>
              </font></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="30%" height="23"> 
              <div align="right">Tipo de Estado de Cuenta :</div>
            </td>
            <td nowrap width="18%" height="23"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMSTY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMSTY" size="25" maxlength="25" 
				  value="<% if (cpBasic.getE01ACMSTY().equals("P")) out.print("Personal");
							else if (cpBasic.getE01ACMSTY().equals("C")) out.print("Corporativa");
							else if (cpBasic.getE01ACMSTY().equals("N")) out.print("Ninguna");
						    else out.print("");%>" 
				>
            </td>
            <td nowrap width="28%" height="23"> 
              <div align="right">Forma de Env&iacute;o de Estado de Cuenta :</div>
            </td>
            <td nowrap width="24%" height="23"> 
              <input type="text" readonly <% if (cpBasic.getF01ACMSTE().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACMSTE" size="25" maxlength="25" 
				  value="<% if (cpBasic.getE01ACMSTE().equals("T")) out.print("Telex");
							else if (cpBasic.getE01ACMSTE().equals("P")) out.print("Impresora");
							else if (cpBasic.getE01ACMSTE().equals("F")) out.print("Facsimil");
							else if (cpBasic.getE01ACMSTE().equals("E")) out.print("Correo Electronico");
							else if (cpBasic.getE01ACMSTE().equals("N")) out.print("Ninguno");
						    else out.print("");%>" 	>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
 
  <H4>Cuotas de Participaci&oacute;n</H4>
<table id="mainTable" class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trdark">
				<td nowrap width="24%">
				<div align="right">Codigo de Convenio :</div>
				</td>
				<td nowrap width="21%"><input type="text" size="5" maxlength="4" name="E01ACMCNV" value="<%=cpBasic.getE01ACMCNV().trim()%>" readonly>
				</td>
				<td nowrap width="25%">
				<div align="right">Fecha de Inicio Mandato :</div>
				</td>
				<td nowrap width="24%">
					<eibsinput:date name="cpBasic" fn_year="E01ACASTY" fn_month="E01ACASTM" fn_day="E01ACASTD" required="false" readonly="true" />
				</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="30%">
				<div align="right">N�mero de Cuotas Pactadas :</div>
				</td><td nowrap width="21%"><input type="text" size="12" maxlength="12" name="E01ACACON" value="<%=cpBasic.getE01ACACON().trim()%>" readonly> (Mensuales)
				</td>
				<td nowrap width="25%">
				<div align="right">Monto en Moneda Nacional :</div>
				</td><td nowrap width="24%">
					<eibsinput:text name="cpBasic" property="E01ACAAUG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" required="false" readonly="true" /> (Mensual)
				</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="23%">
					<div align="right">Medio de Pago :</div>
				</td><td nowrap width="21%">
					<select name="E01ACATYR" disabled>
						<option></option>
						<option value="0" <% if (cpBasic.getE01ACATYR().equals("0")) out.print("selected");%>>No Programado</option>
						<option value="1" <% if (cpBasic.getE01ACATYR().equals("1")) out.print("selected");%>>Planilla/Nomina</option>
						<option value="2" <% if (cpBasic.getE01ACATYR().equals("2")) out.print("selected");%>>PAC/Automatico</option>
						<option value="3" <% if (cpBasic.getE01ACATYR().equals("3")) out.print("selected");%>>Caja</option>
						<option value="4" <% if (cpBasic.getE01ACATYR().equals("4")) out.print("selected");%>>PAC Multibanco</option>
					</select>
				</td>
				<td nowrap width="25%">
				<div align="right">Cuenta  a Debitar :</div>
				</td><td nowrap width="24%">
					<eibsinput:text name="cpBasic" property="E01ACAPAC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" required="false" readonly="true" />
				</td>			
			</tr>
			<tr id="trclear">
				<td nowrap width="25%">
				<div align="right">Frecuencia :</div>
				</td>
				<td nowrap width="24%"><select name="E01ACAFRE" disabled>
					<option></option>
					<option value="M" <% if (cpBasic.getE01ACAFRE().equals("M")) out.print("selected");%>>Mensual</option>
					<option value="Q" <% if (cpBasic.getE01ACAFRE().equals("Q")) out.print("selected");%>>Trimestral</option>
					<option value="S" <% if (cpBasic.getE01ACAFRE().equals("S")) out.print("selected");%>>Semestral</option>
					<option value="Y" <% if (cpBasic.getE01ACAFRE().equals("Y")) out.print("selected");%>>Anual</option>
				</select>
				</td>
			    <td nowrap width="25%">
				<div align="right">D�a de aplicaci�n :</div>
				</td>
					<td nowrap width="24%"><input type="text" size="2" maxlength="3" name="E01ACADMP" value="<%=cpBasic.getE01ACADMP().trim()%>" readonly>
				</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="24%">
				<div align="right">Moneda :</div>
				</td>
				<td nowrap width="21%">
					<eibsinput:text name="cpBasic" property="E01ACACCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY%>" required="false" readonly="true" />
				</td>			
				<td nowrap width="23%">
					<div align="right">Estado :</div>
				</td><td nowrap width="21%">
					<select name="E01ACASTS" disabled>
					<option></option>
					<option value="1" <% if (cpBasic.getE01ACASTS().equals("1")) out.print("selected"); %>>Vigente</option>
					<option value="2" <% if (cpBasic.getE01ACASTS().equals("2")) out.print("selected"); %>>No Vigente</option>
				</select>
				</td>
				<td nowrap width="25%"></td><td nowrap width="24%"></td>			
			</tr>
		</table>
		</td>
	</tr>
</table>

<% if (cpBasic.getH01FLGMAS().equals("N")) {%>  
  <H4>Origen de Fondos</H4>
  <TABLE id="mainTable" class="tableinfo">
  <TR><TD>
  
   <table id="headTable" >
    <tr id="trdark"> 
      <td nowrap align="center" > Concepto</td>
      <td nowrap align="center" >Banco </td>
      <td nowrap align="center" >Sucursal</td>
      <td nowrap align="center" >Moneda</td>
      <td nowrap align="center" >Referencia</td>
      <td nowrap align="center" >Monto</td>
    </tr>
    </table> 
      
    <div id="dataDiv" style="height:60; overflow-y :scroll; z-index:0" >
     <table id="dataTable" >
            <%
  				   int amount = 9;
 				   String name;
  					for ( int i=1; i<=amount; i++ ) {
   					  name = i + "";
   			%> 
          <tr id="trclear"> 
            <td nowrap > 
              <div align="center"> 
                <input type="hidden" name="E01OFFOP<%= name %>" value="<%= cpBasic.getField("E01OFFOP"+name).getString().trim()%>">
                <input type="hidden" name="E01OFFGL<%= name %>" value="<%= cpBasic.getField("E01OFFGL"+name).getString().trim()%>">
                <input type="text" name="E01OFFCO<%= name %>" size="25" maxlength="25" readonly value="<%= cpBasic.getField("E01OFFCO"+name).getString().trim()%>" >
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01OFFBK<%= name %>" size="2" maxlength="2" value="<%= cpBasic.getField("E01OFFBK"+name).getString().trim()%>"  readonly >
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01OFFBR<%= name %>" size="4" maxlength="4" value="<%= cpBasic.getField("E01OFFBR"+name).getString().trim()%>"
                 readonly >
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01OFFCY<%= name %>" size="3" maxlength="3" value="<%= cpBasic.getField("E01OFFCY"+name).getString().trim()%>"
                 readonly >
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01OFFAC<%= name %>" size="12" maxlength="12"  value="<%= cpBasic.getField("E01OFFAC"+name).getString().trim()%>"
                 readonly >
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFAM<%= name %>" size="15" maxlength="15"  value="<%= cpBasic.getField("E01OFFAM"+name).getString().trim()%>"  readonly >
              </div>
            </td>
          </tr>
          <%
    		}
    		%> 
           </table>
        </div>
        
        <TABLE id="footTable" > 
          <tr id="trdark"> 
            <td nowrap align="right"><b>Equivalente Moneda de la Cuenta:</b> 
            </td>
            <td nowrap  align="center"> 
                <input type="text" name="E01OFFEQV" size="15" maxlength="15" readonly value="<%= cpBasic.getE01OFFEQV().trim()%>">
            </td>
          </tr>
        </table>
        
   </TD>  
</TR>	
</TABLE>    
 <SCRIPT language="javascript">
    function tableresize() {
     adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  tableresize();
  window.onresize=tableresize;
  </SCRIPT>
 <%
    		}
    		%>       
  </form>
</body>
</html>
