<%// Hecho por Alonso Arana-----------------------------------20/01/2014 -------------Datapro--------------- %>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Reconciliaci�n bancos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.ERC001001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">


 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	Consulta de Movimientos de Bancos
<%} else if (maintenance){ %>
	Mantenci�n de Movimientos de Bancos
<%} else { %>
	Nuevo Movimientos de Bancos
<%} %>



   




 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ERC0010_maintenance_moves.jsp, ERC0010"></h3>

      <table  class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Codigo del banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<%=session.getAttribute("codigo_banco")%>" 
	  			size="4"  readonly>
             </td>
             <td nowrap width="10%" align="right">Nombre del Banco : 
               </td>
             <td nowrap width="50%"align="left">
	  	<input type="text" name="nombre_banco" value="<%=session.getAttribute("nombre_banco") %>" size="40" readonly>
             </td>
             
             
         </tr>
        </table>
      </td>
    </tr>
  </table>

<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0010" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="<%
 if(readOnly){
 out.print("600");
 } 
 
 if(maintenance){
 out.print("700");
 } 
  else{
   out.print("800");
  
  }
  
  
   %>">
    <INPUT TYPE="HIDDEN" NAME="E01RCCBNK" VALUE="<%=session.getAttribute("codigo_banco") %>">

   
 <INPUT TYPE="HIDDEN" NAME="E01RCSCTA'" VALUE="">
 <% int row = 0;%> 
    
  <table  class="tableinfo" width="100%" border="0">
    <tr> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">          
          
             <tr id="trclear"> 
            <td > 
              <div align="right">C�digo de Movimiento:</div>
            </td>
            <td  > 
            <%if (readOnly){%>
	  <eibsinput:text property="E01RCCTCD" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TEXT%>" maxlength="4"  readonly="true"/>

            <%}else if (maintenance){%>
            	  <eibsinput:text property="E01RCCTCD" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TEXT%>" maxlength="4" readonly="true"  size="4"/>
            <%}
            
            else{
            %>
                   	
       <eibsinput:text property="E01RCCTCD" name="cnvObj" maxlength="4"  eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TEXT%>" size="4"/>
            <% 
            }
             %>
	             
	        </td>
          </tr>
          
          <tr id="trdark"> 
            <td> 
            <input type="hidden" name="E01RCSCTA" value="20" size="23">
              <div align="right" id="div_valor">Glosa:</div>
            </td>
            <td> 
 		            <%if (readOnly){%>
 		            
 		             <eibsinput:text property="E01RCCNME" name="cnvObj" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%> "/>
          		 
          		<% 
          		 }
          		
       else if(maintenance){%>
            
          
                 <eibsinput:text property="E01RCCNME" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"/>
        
          		 
          		 <% }
          		 
          		 else
          		 {
          		 %>
          	
          	       <eibsinput:text property="E01RCCNME" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"/>
          	
          	
 				<% 
          		 }
          		 %>          		
                               
	        </td>
          </tr>
                    
          <tr id="trclear"> 
            <td> 
              <div align="right">Tipo:</div>
            </td>
            <td> 
 		      
 		      <%if(readOnly){%> 		      
 		        <p> 
 		    
 		    <% boolean flag = false; %><select name="E01RCCDCC"  disabled>
                       <option value="C" <%if (cnvObj.getE01RCCDCC().equals("C")) { flag = true; out.print("selected"); }%>>CR�DITO</option>
                <option value="D" <%if (cnvObj.getE01RCCDCC().equals("D")) { flag = true; out.print("selected"); }%>>D�BITO</option>
             
              </select>
 		    
                 
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
 				
 		    	<%}  else if(maintenance){%>
  	     <p> 
 		       
 		    <% boolean flag = false; %><select name="E01RCCDCC">
                       <option value="C" <%if (cnvObj.getE01RCCDCC().equals("C")) { flag = true; out.print("selected"); }%>>CR�DITO</option>
                <option value="D" <%if (cnvObj.getE01RCCDCC().equals("D")) { flag = true; out.print("selected"); }%>>D�BITO</option>
             
              </select>
                
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
 	
                 <%}
                 
                           else {%>
 		 	    <select name="E01RCCDCC" >
                <% boolean flag = false; %>
     
                <option value="C"  <%if (cnvObj.getE01RCCDCC().equals("C")) { flag = true; out.print("selected"); }%> >CR�DITO</option>
                <option value="D"  <%if (cnvObj.getE01RCCDCC().equals("D")) { flag = true; out.print("selected"); }%>>D�BITO</option>
                
              </select>

 		
                 <%}%> 
                             
	        </td>
          </tr>
          
	         
          
          
           <tr id="trdark"> 
            <td> 
              <div align="right">SubTipo:</div>
            </td>
            <td> 
 		       
 		       <%if (readOnly){%> <% boolean flag = false; %><select name="E01RCCSCO"  disabled>
                       <option value="AB" <%if (cnvObj.getE01RCCSCO().equals("AB")) { flag = true; out.print("selected"); }%>>Abono</option>
                <option value="DP" <%if (cnvObj.getE01RCCSCO().equals("DP")) { flag = true; out.print("selected"); }%>>Dep�sito</option>
                <option value="CH" <%if (cnvObj.getE01RCCSCO().equals("CH")) { flag = true; out.print("selected"); } %>>Cheque</option>
              <option value="CA" <%if (cnvObj.getE01RCCSCO().equals("CA")) { flag = true; out.print("selected"); } %>>Cargo</option>
              </select>

 		
                 <%}
                 
                 
                 
                 else if(maintenance){%>
             <!-- Hecho por Alonso Arana ------Datapro-----21/01/2014 -->
                    <select name="E01RCCSCO">
                <% boolean flag = false; %>
                <option value="AB" <%if (cnvObj.getE01RCCSCO().equals("AB")) { flag = true; out.print("selected"); }%>>Abono</option>
                <option value="DP" <%if (cnvObj.getE01RCCSCO().equals("DP")) { flag = true; out.print("selected"); }%>>Dep�sito</option>
                <option value="CH" <%if (cnvObj.getE01RCCSCO().equals("CH")) { flag = true; out.print("selected"); } %>>Cheque</option>
              <option value="CA" <%if (cnvObj.getE01RCCSCO().equals("CA")) { flag = true; out.print("selected"); } %>>Cargo</option>
              </select>
                 <% 
                 }

 		               
 		                else {%>
 		 	    <select name="E01RCCSCO" >
                <% boolean flag = false; %>
     
                <option value="AB" <%if (cnvObj.getE01RCCSCO().equals("AB")) { flag = true; out.print("selected"); }%>>Abono</option>
                <option value="DP" <%if (cnvObj.getE01RCCSCO().equals("DP")) { flag = true; out.print("selected"); }%>>Deposito</option>
                  <option value="CH" <%if (cnvObj.getE01RCCSCO().equals("CH")) { flag = true; out.print("selected"); }%>>Cheque</option>
                       <option value="CA" <%if (cnvObj.getE01RCCSCO().equals("CA")) { flag = true; out.print("selected"); }%>>Cargo</option>
              </select>

 		
                 <%}%> 
 		               
 		                
	        </td>
          </tr>
	

         
        </table>
      </td>
    </tr>
  </table>

<%if  (!readOnly) { %>
    <div align="center"> 
        <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    </div>
<% } %>  


  </form>
</body>
</HTML>
