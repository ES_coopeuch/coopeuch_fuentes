<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%>
<html>
<head>
<title>Debit Cards Mantenance</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id="dcPvta" class="datapro.eibs.beans.ECC016001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<SCRIPT LANGUAGE="javascript">
           
</SCRIPT>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0"); 
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
  
%> 

</head>
<body>
<h3 align="center" >Plataforma PosVenta<BR> Aprobacion Solicitudes de Tarjetas de Debito<br> <%if (userPO.getOption().equals("1")) out.print("A C T I V A C I O N");%>
                                                                        <%if (userPO.getOption().equals("2")) out.print("A D I C I O N A L E S");%>
																	    <%if (userPO.getOption().equals("4")) out.print("Bloqueo/Desbloqueo");%>
                                                                        <%if (userPO.getOption().equals("5")) out.print("Reemision Tarjeta Titular");%>
                                                                        <%if (userPO.getOption().equals("8")) out.print("Reseteo Clave");%> 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    																	    

<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="dc_posventa_approval_inq.jsp,ECC0160"> 
</h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0160" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="500">
  <% int row = 0;%>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">N�mero Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
		        <eibsinput:text name="dcPvta" property="E01CCRNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CARD_NUMBER %>" readonly="true" />            
            </td>
            <td nowrap width="25%"> 
              <div align="right">Tipo de Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCRTPI" disabled>
                <option value=" " <% if (!(
		                	dcPvta.getE01CCRTPI().equals("T") || 
                			dcPvta.getE01CCRTPI().equals("A"))) 
                			out.print("selected"); %> selected></option>
                <option value="T" <% if(dcPvta.getE01CCRTPI().equals("T")) out.print("selected");%>>Titular</option>
                <option value="A" <% if(dcPvta.getE01CCRTPI().equals("A")) out.print("selected");%>>Adicional</option>                			
              </select>
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Nombre TarjetaHabiente : </div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="E01CCRNAM" size="35" maxlength="35" readonly value="<%= dcPvta.getE01CCRNAM().trim()%>">               
            </td>
            <td nowrap width="25%"> 
              <div align="right">Rut TarjetaHabiente : </div>
            </td>
            <td nowrap width="23%"> 
                <input type="text" name="E01SPVCI2" size="15" maxlength="15" readonly value="<%= dcPvta.getE01SPVCI2().trim()%>">               
            </td>
          </tr>   
        </table>
      </td>
    </tr>
  </table>  
  <h4>Tarjeta</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Estado Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCRSTS" disabled>
                <option value=" " <% if (!(
		                	dcPvta.getE01CCRSTS().equals("00") || 
                			dcPvta.getE01CCRSTS().equals("01"))) 
                			out.print("selected"); %> selected></option>
                <option value="00" <% if(dcPvta.getE01CCRSTS().equals("00")) out.print("selected");%>>Inactiva</option>
                <option value="01" <% if(dcPvta.getE01CCRSTS().equals("01")) out.print("selected");%>>Activa</option>  
              </select>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Activaci�n : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="dcPvta" fn_year="E01CCRATY" fn_month="E01CCRATM" fn_day="E01CCRATD" readonly="true"/> 
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Bloqueo Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="D01CCRLKC" size="35" maxlength="35" readonly value="<%= dcPvta.getD01CCRLKC().trim()%>">  		              
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Bloqueo : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="dcPvta" fn_year="E01CCRBKY" fn_month="E01CCRBKM" fn_day="E01CCRBKD" readonly="true"/>
            </td>
          </tr>   
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%">                
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Creaci�n : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="dcPvta" fn_year="E01CCRISY" fn_month="E01CCRISM" fn_day="E01CCRISD" readonly="true"/>
            </td>
          </tr>   
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%">                
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Expiraci�n : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="dcPvta" fn_year="E01CCREXY" fn_month="E01CCREXM" fn_day="E01CCREXD" readonly="true"/>        
            </td>
          </tr>    
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%">                
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Cambio Estado : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="dcPvta" fn_year="E01CCRLSY" fn_month="E01CCRLSM" fn_day="E01CCRLSD" readonly="true"/>                           
            </td>
          </tr>                                                   
        </table>
      </td>
    </tr>
  </table>  
  <h4>Cuenta</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">N�mero Cuenta : </div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" name="E01CCMACC" size="12" maxlength="12" readonly value="<%= dcPvta.getE01CCMACC().trim()%>">             
            </td>
            <td nowrap width="25%"> 
              <div align="right">Producto : </div>
            </td>
            <td nowrap width="23%">
              <input type="text" name="E01ACMPRO" size="4" maxlength="4" readonly value="<%= dcPvta.getE01ACMPRO().trim()%>"> 
              <input type="text" name="D01ACMPRO" size="35" maxlength="35" readonly value="<%= dcPvta.getD01ACMPRO().trim()%>">                                                        
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Estado de la Cuenta : </div>
            </td>
            <td nowrap width="23%"> 
             <select name="E01ACMAST" disabled>
                <option value=" " <% if (!(dcPvta.getE01ACMAST().equals("A") ||dcPvta.getE01ACMAST().equals("C")
				||dcPvta.getE01ACMAST().equals("I")||dcPvta.getE01ACMAST().equals("D")
				||dcPvta.getE01ACMAST().equals("O")||dcPvta.getE01ACMAST().equals("E")
				||dcPvta.getE01ACMAST().equals("T"))) out.print("selected"); %>></option>
                <option value="A" <% if (dcPvta.getE01ACMAST().equals("A")) out.print("selected"); %>>Activa</option>
                <option value="C" <% if (dcPvta.getE01ACMAST().equals("C")) out.print("selected"); %>>Cancelada</option>
                <option value="I" <% if (dcPvta.getE01ACMAST().equals("I")) out.print("selected"); %>>Inactiva 1</option>
                <option value="D" <% if (dcPvta.getE01ACMAST().equals("D")) out.print("selected"); %>>Inactiva 2</option>
                <option value="O" <% if (dcPvta.getE01ACMAST().equals("O")) out.print("selected"); %>>Controlada</option>
				<option value="E" <% if (dcPvta.getE01ACMAST().equals("E")) out.print("selected"); %>>Embargada</option>
				<option value="T" <% if (dcPvta.getE01ACMAST().equals("T")) out.print("selected"); %>>Acepta S�lo Dep�sitos</option>
              </select>
            </td>
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%"> 
            </td>
          </tr>           
        </table>
      </td>
    </tr>
  </table>
  <br>
<br>
<%-----------------------------------------------------------------------------------------Activacion de la Tarjeta ---------%>
<% if (userPO.getOption().equals("1")) { %>
 <h4>Activaci&oacute;n de la Tarjeta</h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                 
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Estado de Tarjeta Anterior : </div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCRSTS" disabled>
                <option value="0" <% if(dcPvta.getE01CCRSTS().equals("0")) out.print("selected");%>>Inactiva</option>  
                <option value="1" <% if(dcPvta.getE01CCRSTS().equals("1")) out.print("selected");%>>Activa</option>                               			
              </select>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Estado de Tarjeta Nuevo : </div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCRSTS" disabled>
                <option value="0" <% if(dcPvta.getE01CCRSTS().equals("0")) out.print("selected");%>>Activa</option>                			
              </select>
            </td>
          </tr>                 
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                            
        </table>
      </td>
    </tr>
  </table> 
<%} %>
<%-----------------------------------------------------------------------------------------Ingreso Adicionales --------%>  
<% if (userPO.getOption().equals("2")) { %> 
 <h4>Ingreso de Adicionales</h4>      
   <table class="tableinfo">  
   <tr>
   <td >     
    <table cellspacing="0" cellpadding="2" width="100%" border="0" >
           <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Rut Adicional 1 : </div>
            </td>
            <td nowrap width="32%"> 
             <input type="text" name="E01SPVRA1" size="25" maxlength="25" value="<%= dcPvta.getE01SPVRA1() %>" readonly >
            </td>
             <td nowrap width="25%"> </td>  
             <td nowrap width="20%"> </td>                        
          </tr>        
          <tr id="trclear">
            <td nowrap width="22%"> 
              <div align="right">Rut Adicional 2 : </div>
            </td>
            <td nowrap width="32%"> 
             <input type="text" name="E01SPVRA2" size="25" maxlength="25" value="<%= dcPvta.getE01SPVRA2() %>" readonly >
            </td>
             <td nowrap width="25%"> </td>  
             <td nowrap width="20%"> </td>                        
          </tr>    
          <tr id="trdark">
            <td nowrap width="22%"> 
              <div align="right">Rut Adicional 3 : </div>
            </td>
            <td nowrap width="32%"> 
             <input type="text" name="E01SPVRA3" size="25" maxlength="25" value="<%= dcPvta.getE01SPVRA3() %>" readonly >
            </td>
             <td nowrap width="25%"> </td>  
             <td nowrap width="20%"> </td>                        
          </tr>             
        </table>
   </td>
   </tr>     
</table>          
<%}%>   
 
<%-----------------------------------------------------------------------------------------Bloqueos y Desbloqueos ---------%> 
<% if (userPO.getOption().equals("4")) { %>  
 <h4>Bloqueos - Desbloqueos </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                  
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Bloqueo/Desbloqueo Anterior : </div>
            </td>
             <td nowrap>
              <select name="E01SPVBLI" disabled>
                <option value=" "><% if (dcPvta.getE01SPVBLI().equals(" ")) out.print("selected"); %>>Sin Bloqueo</option> 
   	     	    <option value="O" <% if (dcPvta.getE01SPVBLI().equals("O")) out.print("selected"); %>>Bloqueo Perdida</option>
	   	   	    <option value="N" <% if (dcPvta.getE01SPVBLI().equals("N")) out.print("selected"); %>>Bloqueo Perdida</option>
                <option value="B" <% if (dcPvta.getE01SPVBLI().equals("B")) out.print("selected"); %>>Bloqueo Banco 2</option>
                <option value="S" <% if (dcPvta.getE01SPVBLI().equals("S")) out.print("selected"); %>>Bloqueo Banco</option>
                <option value="W" <% if (dcPvta.getE01SPVBLI().equals("W")) out.print("selected"); %>>Bloqueo Internacional</option> 
                <option value="F" <% if (dcPvta.getE01SPVBLI().equals("F")) out.print("selected"); %>>Fraude</option>                                               
              </select>             
            </td>
            <td nowrap width="25%"> 
              <div align="right">Bloqueo/Desbloqueo Nuevo : </div>
            </td>
             <td nowrap>
              <select name="E01SPVBLN" disabled>
                <option value=" "><% if (dcPvta.getE01SPVBLN().equals(" ")) out.print("selected"); %>>Sin Bloqueo</option>  
   	     	    <option value="O" <% if (dcPvta.getE01SPVBLN().equals("O")) out.print("selected"); %>>Bloqueo Perdida</option>
	   	   	    <option value="N" <% if (dcPvta.getE01SPVBLN().equals("N")) out.print("selected"); %>>Bloqueo Perdida</option>
                <option value="B" <% if (dcPvta.getE01SPVBLN().equals("B")) out.print("selected"); %>>Bloqueo Banco 2</option>
                <option value="S" <% if (dcPvta.getE01SPVBLN().equals("S")) out.print("selected"); %>>Bloqueo Banco</option>
                <option value="W" <% if (dcPvta.getE01SPVBLN().equals("W")) out.print("selected"); %>>Bloqueo Internacional</option>
                <option value="F" <% if (dcPvta.getE01SPVBLN().equals("F")) out.print("selected"); %>>Fraude</option>                                               
              </select>             
            </td>
          </tr>     
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                                       
        </table>
      </td>
    </tr>
  </table>   
 <%} %>
 <%-----------------------------------------------------------------------------------------Reemision de la Tarjeta ---------%> 
<% if (userPO.getOption().equals("5")) { %>  
 <h4>Reemisi&oacute;n</h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>        
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Motivo Remisi&oacute;n :</div>
            </td>
            <td nowrap width="23%"> 
                <input type="text" name="E01SPVMOT" size="4" maxlength="4" value="<%= dcPvta.getE01SPVMOT() %>" readonly >
                <input type="text" name="E01SPVRMK"  size="30" maxlength="30" value="<%= dcPvta.getE01SPVRMK()%>" readonly>
            </td>
            <td nowrap width="25%"> 
              <div align="right">
              Sucursal de Env�o de Tarjeta :
               </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> 
                <input type="text" name="E01SPVSEC" size="4" maxlength="4" value="<%=dcPvta.getE01SPVSEC()%>" readonly >
                <input type="text" name="DESCBRANCH"  size="30" maxlength="30" value="<%=dcPvta.getE01SPVBEC()%>" readonly>
              </div> 			
             </td>
          </tr> 
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                                       
        </table>
      </td>
    </tr>
  </table> 
 <%} %>
 

<%-----------------------------------------------------------------------------------------Reseteo Clave ---------%> 
<% if (userPO.getOption().equals("8")) { %>   
 <h4>Reseteo Clave </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">      
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>   
           <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="23%"> 
              <div align="Center"><b> Reseteo de Clave </b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>             
            <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                           
        </table>
      </td>
    </tr>
  </table>   
 <%}%>  
  </form>
</body>
</html>
