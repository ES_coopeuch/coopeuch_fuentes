package datapro.eibs.client;

/**
 * Pac de Seguros
 * @author: Jose M. Buitrago
 * 
 * Retracto de Seguros
 * @author: Alonso Arana	23/10/2013		----Datapro----	
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.tptp.platform.agentcontroller.internal.config.Variable;

import datapro.eibs.beans.ESG001001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESG001002Message;
import datapro.eibs.beans.ESG010501Message;
import datapro.eibs.beans.ESG010502Message;
import datapro.eibs.beans.ESG010503Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSESG0010 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int R_PAC_SEGUROS_LIST_NEW = 100;
	protected static final int R_PAC_SEGUROS_LIST_MAINTENANCE = 300;
	protected static final int A_PAC_SEGUROS_LIST = 101;
	protected static final int R_PAC_SEGUROS_NEW = 200;
	protected static final int R_PAC_SEGUROS_MAINT = 201;
	protected static final int R_PAC_SEGUROS_DELETE = 202;	
	protected static final int R_PAC_SEGUROS_INQUIRY = 203;
	protected static final int A_PAC_SEGUROS_MAINT = 600;

	//Retracto de Seguros
	
	protected static final int A_PAC_SEGUROS_RET_LIST = 400;
	protected static final int R_PAC_SEGUROS_RET_LIST = 401;
	protected static final int A_PAC_SEGUROS_RET_CONFIRMATION_REFUSE = 402;
	protected static final int A_PAC_SEGUROS_RET_REFUSE = 403;
	protected static final int A_PAC_SEGUROS_RET_LIST_REFUSE=502;
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_PAC_SEGUROS_LIST_NEW:
			procReqSegurosList(user, req, res, session, "NEW");
			break;
		case R_PAC_SEGUROS_LIST_MAINTENANCE:
			procReqSegurosList(user, req, res, session, "MAINTENANCE");
			break;
		case A_PAC_SEGUROS_LIST:
			procActionSegurosList(user, req, res, session, null);
			break;
		case R_PAC_SEGUROS_NEW:
			procReqSeguros(user, req, res, session, "NEW");
			break;
		case R_PAC_SEGUROS_MAINT:
			procReqSeguros(user, req, res, session, "MAINTENANCE");
			break;
		case R_PAC_SEGUROS_INQUIRY:
			procReqSeguros(user, req, res, session, "INQUIRY");
			break;
		case A_PAC_SEGUROS_MAINT:
			procActionMaintenance(user, req, res, session);
			break;
		case R_PAC_SEGUROS_DELETE:
			procReqDelete(user, req, res, session);
			break;
			
		case A_PAC_SEGUROS_RET_LIST:
			procReqRetractoSegurosList(user, req, res, session);
			break;
			
		case R_PAC_SEGUROS_RET_LIST:
			procActionRetractoList(user, req, res, session, null);
			break;
			
		case A_PAC_SEGUROS_RET_LIST_REFUSE:
			procSegurosRefuse(user, req, res, session, null);	
			break;
			
		case A_PAC_SEGUROS_RET_CONFIRMATION_REFUSE:
			procReqRetractoConfirmation(user, req,  res, session);
			break;
			
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	/**
	 * procActionMaintenance
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			String Accion = "";
			Accion = userPO.getPurpose();
			
						
			mp = getMessageProcessor("ESG0010", req);

			ESG001001Message msg = (ESG001001Message) mp.getMessageRecord("ESG001001", user.getH01USR(), "0005");

			//Setea este flag en 1 con el fin de que RPG solo valide firma de mandato para registro nuevo.
			if(Accion.equals("NEW"))
				msg.setH01FLGWK1("1");

			//Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ESG001001Message) mp.receiveMessageRecord();

		
			
			//Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("msg", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go back to list
				redirectToPage("/servlet/datapro.eibs.client.JSESG0010?SCREEN=101&customer_number="	+ msg.getE01PACCUN(), res);
			} else {
				//if there are errors go back to maintenance page and show errors
				session.setAttribute("cnvObj", msg);
				forward("ESG0010_PAC_seguros_maintenance.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	

	/**
	 * procReqSegurosList
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqSegurosList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses, String option )
			throws ServletException, IOException {
		UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		userPO.setPurpose(option);
		ses.setAttribute("userPO", userPO);
		try {
			flexLog("About to call Page: ESG0010_PAC_client_enter_search.jsp");
			forward("ESG0010_PAC_client_enter_search.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

	/**
	 * procActionSegurosList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionSegurosList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String option)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		String customer_number = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		boolean evaluation = false;
		
		try {
			mp = getMessageProcessor("ESG0010", req);

			ESG001001Message msgList = (ESG001001Message) mp.getMessageRecord("ESG001001", user.getH01USR(), "0015");
			//Sets the employee  number from either the first page or the maintenance page
			customer_number = "";
			if (req.getParameter("customer_number")!= null){
				customer_number = req.getParameter("customer_number");								
			} else {
				customer_number = req.getParameter("E01PACCUN");
			}
			
			if (option != null){
				evaluation = true;
				msgList.setE01PACSTS("E");
			}
			
			msgList.setE01PACCUN(customer_number);

			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();			
			if (mp.hasError(error)) { // if there are errors go back to first page
				session.setAttribute("error", error);
				flexLog("About to call Page: ESG0010_PAC_client_enter_search.jsp");
				forward("ESG0010_PAC_client_enter_search.jsp", req, res);
			} else {
				//Receive header information
				ESG001002Message header = (ESG001002Message)mp.receiveMessageRecord();
				userPO.setCusNum(header.getE02CUSCUN());
				userPO.setCusName(header.getE02CUSNA1());
				userPO.setID(header.getE02CUSIDN());

				//Receive insurance  list
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			
				//if there are NO errors display list
				session.setAttribute("ESG001001List", list);
				forwardOnSuccess("ESG0010_PAC_seguros_list.jsp?evaluation="+evaluation, req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqSeguros: This Method show a single Pac Seguros either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqSeguros(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ESG0010", req);
			userPO.setPurpose(option);

			ESG001001Message msg = null;
			
			//Creates the message with operation code depending on the option
			if (option.equals("NEW")) {
				//New
				msg = (ESG001001Message) mp.getMessageRecord("ESG001001", user.getH01USR(), "0001");
			} else if (option.equals("MAINTENANCE")) {
				//Maintenance
				msg = (ESG001001Message) mp.getMessageRecord("ESG001001", user.getH01USR(), "0002");
			} else {
				//Inquiry
				msg = (ESG001001Message) mp.getMessageRecord("ESG001001", user.getH01USR(), "0004");
			}
			
			//Sets the Customer number
			if (option.equals("NEW")) {
				msg.setE01PACCUN(req.getParameter("customer_number"));
			}
			else
			{
				msg.setE01PACCUN(userPO.getCusNum());
			}
			
			//Sets the insurance number for maintenance and inquiry options
			if (req.getParameter("E01PACNUM") != null) {
				msg.setE01PACNUM(req.getParameter("E01PACNUM"));
			}

			//Send message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ESG001001Message) mp.receiveMessageRecord();
			
			if (userPO.getCusName() == null || userPO.getCusName().equals("")){
				userPO.setCusName(msg.getE01CUSNA1());
				userPO.setID(msg.getE01CUSIDN());
			}

			if (option.equals("NEW")) 
			{
				userPO.setPurpose("NEW");
			} 
			else if (option.equals("MAINTENANCE")) 
			{
				userPO.setPurpose("MAINTENANCE");
			} 
			else if (option.equals("INQUIRY")) 
			{
				userPO.setPurpose("INQUIRY");			
			}
			session.setAttribute("cnvObj", msg);
			session.setAttribute("userPO", userPO);
			if (!mp.hasError(msgError)) {
				//if there are no errors go to maintenance page
				flexLog("About to call Page: ESG0010_PAC_seguros_maintenance.jsp");
				if (option.equals("INQUIRY")) {
					// if the request is an inquiry sets the readOlnly attribute 'true'
					forward("ESG0010_PAC_seguros_maintenance.jsp?readOnly=true", req, res);
				} else {
					forward("ESG0010_PAC_seguros_maintenance.jsp", req, res);
				}
			} else {
				//if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("ESG0010_PAC_seguros_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqDelete
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDelete(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("MAINTENANCE");

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("ESG0010", req);
			
			//Creates message with the 'Delete'operation code
			ESG001001Message msg = (ESG001001Message) mp.getMessageRecord("ESG001001", user.getH01USR(), "0009");
			
			//Sets required values
			msg.setH01SCRCOD("01");
			msg.setE01PACCUN(userPO.getCusNum());
			msg.setE01PACNUM(req.getParameter("E01PACNUM"));
			
				
			//Send message
			mp.sendMessage(msg);

			//Receive Error and Data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ESG001001Message) mp.receiveMessageRecord();

			if (!mp.hasError(msgError)) {
				//If there are no errors request the list again
				redirectToPage("/servlet/datapro.eibs.client.JSESG0010?SCREEN=101?customer_number="	+ msg.getE01PACCUN(), res);
			} else {
				//if there are errors show the list without updating
				session.setAttribute("error", msgError);
				forward("ESG0010_PAC_seguros_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	
	protected void procReqRetractoSegurosList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			forward("ESG0105_client_search.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}

	protected void procActionRetractoList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String option)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		String customer_number = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		boolean evaluation = false;
		
		try {
			mp = getMessageProcessor("ESG0105", req);

			ESG010501Message msgList = (ESG010501Message) mp.getMessageRecord("ESG010501", user.getH01USR(), "0001");
			//Sets the employee  number from either the first page or the maintenance page
			customer_number = "";
			if (req.getParameter("customer_number")!= null){
				customer_number = req.getParameter("customer_number");								
			} else {
				customer_number = req.getParameter("E01PACCUN");
			}
			
	
			msgList.setE01SRECUN(customer_number);

			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();			
			if (mp.hasError(error)) { // if there are errors go back to first page
				session.setAttribute("error", error);
				flexLog("About to call Page: ESG0105_client_search.jsp");
				forward("ESG0105_client_search.jsp", req, res);
			} else {
				//Receive header information
				ESG010501Message header = (ESG010501Message)mp.receiveMessageRecord();
				userPO.setCusNum(header.getE01SRECUN());
				userPO.setCusName(header.getE01SRESHN());
				userPO.setID(header.getE01SREIDN());

				//Receive insurance  list
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
				//if there are NO errors display list
				session.setAttribute("ESG0010501List", list);
				forward("ESG0105_insurance_ret_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}	

	
	
	
	protected void procReqRetractoConfirmation(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		try {
			if(req.getParameter("key")!=null){req.setAttribute("key", req.getParameter("key"));}
			
			UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			JBObjList list=(JBObjList) ses.getAttribute("ESG0010501List");	
			String clave=(String) req.getAttribute("key");

			int key = 0;
				try {key =Integer.parseInt(clave);
				} catch (Exception e) {	}
				ESG010501Message convObj = (ESG010501Message) list.get(key);
			
				String cia=convObj.getE01SRECIA();
				String  cuenta=convObj.getE01SREPAC();
				String vigencia=convObj.getE01SRESDD() + "/"	+ convObj.getE01SRESDM() + "/" + convObj.getE01SRESDY();
				req.setAttribute("cuenta_sel",cuenta);
				req.setAttribute("id_compania",cia);
				req.setAttribute("descripcion_cuenta",convObj.getE01SREDPA());
				req.setAttribute("MontoCrédito", convObj.getE01SREOAM());
				req.setAttribute("nombre_compania",convObj.getE01SRENM1());
				req.setAttribute("rut_compania",convObj.getE01SRECIA());
				req.setAttribute("fecha_vigencia",vigencia);
				
				ses.setAttribute("ESG1050list", list);
				ses.setAttribute("userPO", userPO);
			
			flexLog("About to call Page: ESG0105_confirmation_insurance.jsp");
			forward("ESG0105_confirmation_insurance.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	
	
	
	
	
	protected void procSegurosRefuse(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		MessageContext mc= null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ESG0105", req);
			userPO.setPurpose(option);

			ESG010502Message msg = null;
			MessageRecord newmessage = null;
				
			//Creates the message with operation code depending on the option
			msg = (ESG010502Message) mp.getMessageRecord("ESG010502", user.getH01USR(), "0001");
						
			if (req.getParameter("cuenta") != null) {
				msg.setE02SREPAC(req.getParameter("cuenta"));
			}
			if (req.getParameter("cusNum") != null) {
				msg.setE02SRECUN(req.getParameter("cusNum"));
			}

			String Segu ="";
			String[] parts;
			String Segu1 = ""; 
			String Secu1 = ""; 
			String Devo1 = ""; 

			//Sets the insurance number for maintenance and inquiry options
			if (req.getParameter("codigo_seguro") != null) {
				String[] seguros=req.getParameterValues("codigo_seguro");
				int numero_seguros=seguros.length;
			
				if(numero_seguros>=1){
					Segu =seguros[0];
					parts = Segu.split("@");
					Segu1 = parts[0]; 
					Secu1 = parts[1]; 
					Devo1 = parts[2]; 
					
					msg.setE02SRECO0(Segu1);
					msg.setE02SRENU0(Secu1);
					msg.setE02SREDV0(Devo1);
				}
				
				if(numero_seguros>=2){
					Segu =seguros[1];
					parts = Segu.split("@");
					Segu1 = parts[0]; 
					Secu1 = parts[1]; 
					Devo1 = parts[2]; 

					msg.setE02SRECO1(Segu1);
					msg.setE02SRENU1(Secu1);
					msg.setE02SREDV1(Devo1);
				}
				
				if(numero_seguros>=3){
					Segu =seguros[2];
					parts = Segu.split("@");
					Segu1 = parts[0]; 
					Secu1 = parts[1]; 
					Devo1 = parts[2]; 

					msg.setE02SRECO2(Segu1);
					msg.setE02SRENU2(Secu1);
					msg.setE02SREDV2(Devo1);
				}
				
				if(numero_seguros>=4){
					Segu =seguros[3];
					parts = Segu.split("@");
					Segu1 = parts[0]; 
					Secu1 = parts[1]; 
					Devo1 = parts[2]; 

					msg.setE02SRECO3(Segu1);
					msg.setE02SRENU3(Secu1);
					msg.setE02SREDV3(Devo1);
				}
				
				if(numero_seguros>=5){
					Segu =seguros[4];
					parts = Segu.split("@");
					Segu1 = parts[0]; 
					Secu1 = parts[1]; 
					Devo1 = parts[2]; 

					msg.setE02SRECO4(Segu1);
					msg.setE02SRENU4(Secu1);
					msg.setE02SREDV4(Devo1);
				}
				
				if(numero_seguros>=6){
					Segu =seguros[5];
					parts = Segu.split("@");
					Segu1 = parts[0]; 
					Secu1 = parts[1]; 
					Devo1 = parts[2]; 

					msg.setE02SRECO5(Segu1);
					msg.setE02SRENU5(Secu1);
					msg.setE02SREDV5(Devo1);
				}
				
				if(numero_seguros>=7){
					Segu =seguros[6];
					parts = Segu.split("@");
					Segu1 = parts[0]; 
					Secu1 = parts[1]; 
					Devo1 = parts[2]; 

					msg.setE02SRECO6(Segu1);
					msg.setE02SRENU6(Secu1);
					msg.setE02SREDV6(Devo1);
				}

				if(numero_seguros>=8){
					Segu =seguros[7];
					parts = Segu.split("@");
					Segu1 = parts[0]; 
					Secu1 = parts[1]; 
					Devo1 = parts[2]; 

					msg.setE02SRECO7(Segu1);
					msg.setE02SRENU7(Secu1);
					msg.setE02SREDV7(Devo1);
				}
				
				if(numero_seguros>=9){
					Segu =seguros[8];
					parts = Segu.split("@");
					Segu1 = parts[0]; 
					Secu1 = parts[1]; 
					Devo1 = parts[2]; 

					msg.setE02SRECO8(Segu1);
					msg.setE02SRENU8(Secu1);
					msg.setE02SREDV8(Devo1);
				}
				
				if(numero_seguros>=10){
					Segu =seguros[9];
					parts = Segu.split("@");
					Segu1 = parts[0]; 
					Secu1 = parts[1]; 
					Devo1 = parts[2]; 

					msg.setE02SRECO9(Segu1);
					msg.setE02SRENU9(Secu1);
					msg.setE02SREDV9(Devo1);
				}
			}
			
			
			mp.sendMessage(msg);
			//Send message

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
	 		
			ESG010503Message header = (ESG010503Message)mp.receiveMessageRecord();

			if (!mp.hasError(msgError)) {
				//if there are no errors go to maintenance page
				session.setAttribute("RetractosList", header);
				session.setAttribute("userPO", userPO);
				
				flexLog("About to call Page: ESG0105_comprobante.jsp");	
				forward("ESG0105_comprobante.jsp", req, res);
			} else {
				//if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("ESG0105_insurance_client_search.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	
}
