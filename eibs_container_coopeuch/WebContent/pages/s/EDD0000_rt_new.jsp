<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>
 <html>
<head>
<title>Cuentas Corrientes</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%@ page import = "java.io.*,java.net.*,datapro.eibs.beans.*,datapro.eibs.master.*,java.math.*" %>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<jsp:useBean id="rtBasic" class="datapro.eibs.beans.EDD000001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session"/>
<jsp:useBean id= "currClient" class= "datapro.eibs.beans.ESD080001Message"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" /> 
<jsp:useBean id= "idCliente" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "nombreCliente" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "servlet" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "pagina" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "tipoProducto" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "paginaPS" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "IDuser" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "NOBuser" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "pagOperation" class= "java.lang.String"  scope="session" />



<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

</head>
<body>
<SCRIPT Language="Javascript">
  builtHPopUp();

	function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   		if (field.substring(0,8) == "E01OFFAC"){
     	var index = field.substr(8,1);
     	var concepto=document.getElementById("E01OFFOP"+index).value;
     	if (concepto == "01")
       		Client=document.getElementById("E01ACMCUN").value;
     	else
       		Client="";
   		}
   	init(opth,field,bank,ccy,field1,field2,opcod);
   	showPopupHelp();
   }
</SCRIPT>

<% 
 String CUSCUN = "";
 String CUSNA1 = "";
 
 if (currClient != null && error.getERRNUM().equals("0")) {
    CUSCUN = currClient.getE01CUSCUN().trim();
    CUSNA1 = currClient.getE01CUSNA1().trim();
 } else {
    CUSCUN = rtBasic.getE01ACMCUN().trim();
    CUSNA1 = rtBasic.getE01CUSNA1().trim();
 }

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
 
 boolean protect = JSEIBSProp.getProtectedBNKBRN();
  
%> 
<H3 align="center"><% if (userPO.getPurpose().equals("NEW")) out.print("Apertura de Cuentas de Detalle");
   else if (userPO.getPurpose().equals("MAINTENANCE")) out.print("Mantenimiento de Cuentas de Detalle");
   else out.print("");%><img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="rt_new, EDD0000"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXEDD0000" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" value="29">
  <INPUT TYPE=HIDDEN NAME="E01ACMATY" VALUE="<%= rtBasic.getE01ACMATY().trim()%>">
  <INPUT TYPE=HIDDEN NAME="E01ACMACD" VALUE="<%= rtBasic.getE01ACMACD().trim()%>">
  <input type="hidden" name="idCliente" value="<%= idCliente %>" size=15 maxlength=9 >
  <input type="hidden" name="nombreCliente" value="<%= nombreCliente %>" size=15 maxlength=9 >
  <input type="hidden" name="pagina" value="<%= pagina %>" size=15 maxlength=9 >
  <input type="hidden" name="servlet" value="<%= servlet %>" size=15 maxlength=9 >
  <input type="hidden" name=tipoProducto value="<%= tipoProducto %>" size=15 maxlength=9 >
  <input type="hidden" name=paginaPS value="<%= paginaPS %>" size=15 maxlength=9 >
  <input type="hidden" name=pagOperation value="<%= pagOperation %>" size=15 maxlength=9 >
  
   
  
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente:</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left">
             
              <% if( servlet.equals("4040") ){ %>
              	   <input type="text" name="E01ACMCUN" id="E01ACMCUN" size="10" maxlength="9" value="<%= IDuser %>">
               <%} else if(servlet.equals("1") ){ %>
              		<input type="text" name="E01ACMCUN" id="E01ACMCUN" size="10" maxlength="9" value="<%=idCliente%>">
              <%} else { %>		
              		<input type="text" name="E01ACMCUN" id="E01ACMCUN" size="10" maxlength="9" value="<%= CUSCUN%>">
              <% } %>
               <a href="javascript:GetCustomerDescId('E01ACMCUN','E01CUSNA1','')">
                	<img src="<%=request.getContextPath()%>/images/1b.gif" alt="Busqueda de Clientes" align="absbottom" border="0" ></a>
                <a href="javascript:NewShortCust('E01ACMCUN', 'E01CUSNA1', '')">Nuevo</a>
                </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left">
              	 <% if( servlet.equals("4040") ){ %>
              	 	<input type="text" name="E01CUSNA1" size="45" maxlength="45" readonly value="<%= NOBuser%>">
              	 <%} else if(servlet.equals("1") ){ %>
              		<input type="text" name="E01CUSNA1" size="45" maxlength="45" readonly value="<%=nombreCliente%>">	
              	 <%} else { %>
              	 	<input type="text" name="E01CUSNA1" size="45" maxlength="45" readonly value="<%= CUSNA1%>">
                <% } %>
              	
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left">
                <input type="text" name="E01ACMACC" size="15" maxlength="12" readonly value="<% if (rtBasic.getE01ACMACC().trim().equals("999999999999")) out.print("NUEVA CUENTA"); else out.print(rtBasic.getE01ACMACC().trim()); %>">
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b>
                <input type="text" name="E01ACMCCY" size="4" maxlength="3" value="<%= userPO.getCurrency().trim()%>" readonly>
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b>
                <input type="text" name="E01ACMPRO" size="4" maxlength="4" readonly value="<%= userPO.getProdCode().trim()%>">
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <H4>Datos B&aacute;sicos de la Operaci&oacute;n</H4>
<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trclear"> 
            <td nowrap width="29%" height="19"> 
              <div align="right">Nombre Alias :</div>
            </td>
            <td nowrap> 
              <input type="text" name="E01ACMNME" size="50" maxlength="80" value="<%= rtBasic.getE01ACMNME().trim()%>">              
            </td>
            <td nowrap> 
            </td>
            <td nowrap>
            </td>
          </tr>        
          <tr id="trdark"> 
            <td nowrap width="29%"> 
              <div align="right">Fecha de Apertura :</div>
            </td>
            <td nowrap width="19%"> 
              <input type="text" name="E01ACMOP1" size="3" maxlength="2" value="<%= rtBasic.getE01ACMOP1().trim()%>" >
              <input type="text" name="E01ACMOP2" size="3" maxlength="2" value="<%= rtBasic.getE01ACMOP2().trim()%>" >
              <input type="text" name="E01ACMOP3" size="5" maxlength="4" value="<%= rtBasic.getE01ACMOP3().trim()%>" >   
               
            </td>
            <td nowrap width="26%"> 
              <div align="right">C&oacute;digo Referencial :</div>
            </td>
            <td nowrap width="26%">
              <select name="E01ACMRCD">
                <option value=" " <% if (!(rtBasic.getE01ACMRCD().equals("D") ||rtBasic.getE01ACMRCD().equals("C")||rtBasic.getE01ACMRCD().equals("A"))) out.print("selected"); %>></option>
                <option value="D" <% if (rtBasic.getE01ACMRCD().equals("D")) out.print("selected"); %>>D&eacute;bitos</option>
                <option value="C" <% if (rtBasic.getE01ACMRCD().equals("C")) out.print("selected"); %>>Cr&eacute;ditos</option>
                <option value="A" <% if (rtBasic.getE01ACMRCD().equals("A")) out.print("selected"); %>>Ambos</option>
              </select>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="29%"> 
              <div align="right">Retenci&oacute;n de Impuestos :</div>
            </td>
            <td nowrap width="19%"> 
              <input type="text" name="E01ACMWHF" size="2" maxlength="1" value="<%= rtBasic.getE01ACMWHF().trim()%>">
              <a href="javascript:GetCode('E01ACMWHF','STATIC_cd_taxes.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" border="0" ></a> 
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" > 
            </td>
            <td nowrap width="26%"> 
              <div align="right">Firmas Autorizadas :</div>
            </td>
            <td nowrap width="26%">
              <select name="E01ACMPEC">
                <option value=" " <% if (!(rtBasic.getE01ACMPEC().equals("1") ||rtBasic.getE01ACMPEC().equals("2"))) out.print("selected"); %>></option>
                <option value="1" <%if (rtBasic.getE01ACMPEC().equals("1")) out.print("selected"); %>>Indistinto</option>
                <option value="2" <% if (rtBasic.getE01ACMPEC().equals("2")) out.print("selected"); %>>Conjunto</option>
				</select>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="29%" height="23"> 
              <div align="right">Mensajes al Cliente :</div>
            </td>
            <td nowrap width="19%" height="23"> 
              <input type="hidden" name="E01ACMPMF" value="<%= rtBasic.getE01ACMPMF() %>">
              <input type="radio" name="CE01ACMPMF" value="Y" onClick="document.forms[0].E01ACMPMF.value='Y'"
			  <%if(rtBasic.getE01ACMPMF().equals("Y")) out.print("checked");%>>
              S&iacute; 
              <input type="radio" name="CE01ACMPMF" value="N" onClick="document.forms[0].E01ACMPMF.value='N'"
			  <%if(rtBasic.getE01ACMPMF().equals("N")) out.print("checked");%>>
              No </td>
            <td nowrap width="26%" height="23"> 
              <div align="right">Sobregiros Permitidos :</div>
            </td>
            <td nowrap width="26%" height="23"> 
              <input type="hidden" name="E01ACMF03" value="<%= rtBasic.getE01ACMF03()%>">
              <input type="radio" name="CE01ACMF03" value="1" onClick="document.forms[0].E01ACMF03.value='1'"
			  <%if(rtBasic.getE01ACMF03().equals("1")) out.print("checked");%>>
              S&iacute; 
              <input type="radio" name="CE01ACMF03" value="N" onClick="document.forms[0].E01ACMF03.value='N'"
			  <%if(rtBasic.getE01ACMF03().equals("N")) out.print("checked");%>>
              No <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" ></td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="29%" height="19"> 
              <div align="right">N&uacute;mero Direcci&oacute;n Postal :</div>
            </td>
            <td nowrap width="19%" height="19"> 
              <input type="text" name="E01ACMMLA" size="3" maxlength="2" value="<%= rtBasic.getE01ACMMLA().trim()%>">
              <a href="javascript:GetMailing('E01ACMMLA',document.forms[0].E01ACMCUN.value)"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="middle" border="0" ></a> 
            </td>
            <td nowrap width="26%" height="19"> 
              <div align="right">Manejo de Sub-Cuentas :</div>
            </td>
            <td nowrap width="26%" height="19"> 
              <input type="hidden" name="E01ACMPTF" value="<%= rtBasic.getE01ACMPTF()%>">
              <input type="radio" name="CE01ACMPTF" value="Y" onClick="document.forms[0].E01ACMPTF.value='Y'"
			  <%if(rtBasic.getE01ACMPTF().equals("Y")) out.print("checked");%>>
              S&iacute; 
              <input type="radio" name="CE01ACMPTF" value="N" onClick="document.forms[0].E01ACMPTF.value='N'"
			  <%if(rtBasic.getE01ACMPTF().equals("N")) out.print("checked");%>>
              No <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" ></td>
          </tr>
          <tr id="trdark">
            <td nowrap width="29%" height="19">
              <div align="right">Tabla de Documentos  :</div>
            </td>
            <td nowrap width="19%" height="19">
             <input type="text" name="E01APCTNU" size="3" readonly maxlength="2" value="<%= rtBasic.getE01APCTNU().trim()%>">
             
            </td>
            <td nowrap width="26%" height="19">
              <div align="right">Centro de Costo :</div>
            </td>
            <td nowrap width="26%" height="19">
              <input type="text" name="E01ACMCCN" size="9" maxlength="8" value="<%= rtBasic.getE01ACMCCN().trim()%>">
              <a href="javascript:GetCostCenter('E01ACMCCN','01')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="middle" border="0" ></a> 
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="29%" height="19"> 
              <div align="right">Diferidos Locales :</div>
            </td>
            <td nowrap width="19%" height="19">
              <input type="text" size="2" maxlength="1" value="<%= rtBasic.getE01ACMWLF().trim()%>" name="E01ACMWLF">
              <a href="javascript:GetCode('E01ACMWLF','STATIC_rt_delay.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" border="0" ></a> 
            </td>
            <td nowrap width="26%" height="19"> 
              <div align="right">Diferidos No Locales :</div>
            </td>
            <td nowrap width="26%" height="19">
              <input type="text" size="2" maxlength="1" value="<%= rtBasic.getE01ACMWNF().trim()%>" name="E01ACMWNF">
              <a href="javascript:GetCode('E01ACMWNF','STATIC_rt_delay.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" border="0" ></a> 
            </td>
          </tr>
          <tr id="trdark">
            <td nowrap width="29%" height="19">
              <div align="right">Banco/Sucursal :</div>
            </td>
            <td nowrap width="19%" height="19">
            <% if (!protect) {%>
              <input type="text" name="E01ACMBNK" size="2" maxlength="2" value="<%= rtBasic.getE01ACMBNK().trim()%>">
              <input type="text" name="E01ACMBRN" size="5" maxlength="4" value="<%= rtBasic.getE01ACMBRN().trim()%>">
              <a href="javascript:GetBranch('E01ACMBRN','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" border="0"  ></a>
             <% } else {%>
              <input type="text" name="E01ACMBNK" size="2" maxlength="2" value="<%= rtBasic.getE01ACMBNK().trim()%>" readonly>
              <input type="text" name="E01ACMBRN" size="5" maxlength="4" value="<%= rtBasic.getE01ACMBRN().trim()%>" readonly>
             <% }%> 
            </td>
            <td nowrap width="26%" height="19"> 
              <div align="right">Convenio Pago Remunerac.:</div>
            </td>
            <td nowrap width="19%" height="19"> 
              <input type="hidden" name="E01ACMF02" value="<%= rtBasic.getE01ACMF02() %>">
              <input type="radio" name="CE01ACMF02" value="Y" onClick="document.forms[0].E01ACMF02.value='Y'"
			  <%if(rtBasic.getE01ACMF02().equals("Y")) out.print("checked");%>>
              S&iacute; 
              <input type="radio" name="CE01ACMF02" value="N" onClick="document.forms[0].E01ACMF02.value='N'"
			  <%if(rtBasic.getE01ACMF02().equals("N")) out.print("checked");%>>
              No 
              </td>            
          </tr>
          
          <tr id="trclear"> 
            <td nowrap width="29%" height="19">
              <div align="right">Vendedor :</div>
            </td>
            <td nowrap width="19%" height="19">
               <input type="text" name="E01ACMUC1" size="5" maxlength="4" value="<%= rtBasic.getE01ACMUC1().trim()%>" readonly>
              <input type="text" name="E01DSCUC1" size="40" maxlength="35" value="<%= rtBasic.getE01DSCUC1().trim()%>" readonly>
              <a href="javascript:GetCodeDescCNOFC('E01ACMUC1','E01DSCUC1','CA')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0" ></a> 
            </td>
            <td nowrap width="26%" height="19"> 
              <div align="right"></div>
            </td>
            <td nowrap width="19%" height="19"> 
             </td>            
          </tr>

        </table>
      </td>
    </tr>
  </table>
  <H4>Informaci&oacute;n para Cargos por Servicios</H4>
  
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="30%"> 
              <div align="right">Cargos por Servicios :</div>
            </td>
            <td nowrap width="18%"> 
              <input type="hidden" name="E01ACMSCF" value="<%= rtBasic.getE01ACMSCF()%>">
              <input type="radio" name="CE01ACMSCF" value="Y" onClick="document.forms[0].E01ACMSCF.value='Y'"
			  <%if(rtBasic.getE01ACMSCF().equals("Y")) out.print("checked");%>  <%if(rtBasic.getE01ACMATY().equals("CVIS")) out.print("disabled");%>>
              S&iacute; 
              <input type="radio" name="CE01ACMSCF" value="N" onClick="document.forms[0].E01ACMSCF.value='N'"
			  <%if(rtBasic.getE01ACMSCF().equals("N")) out.print("checked");%> <%if(rtBasic.getE01ACMATY().equals("CVIS")) out.print("disabled");%>>
              No <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" ></td>
            <td nowrap width="28%"> 
              <div align="right">Frecuencia Cobro de Cargos :</div>
            </td>
            <td nowrap width="24%">
              <select name="E01ACMSHF" <%if(rtBasic.getE01ACMATY().equals("CVIS")) out.print("disabled");%>>
				<option value=" " <% if (!(rtBasic.getE01ACMSHF().equals("D") ||rtBasic.getE01ACMSHF().equals("W")
				||rtBasic.getE01ACMSHF().equals("B")||rtBasic.getE01ACMSHF().equals("M")||rtBasic.getE01ACMSHF().equals("Q")))
				out.print("selected"); %>></option>
                <option value="D" <%if (rtBasic.getE01ACMSHF().equals("D")) out.print("selected"); %>>Diario</option>
                <option value="W" <%if (rtBasic.getE01ACMSHF().equals("W")) out.print("selected"); %>>Semanal</option>
                <option value="B" <%if (rtBasic.getE01ACMSHF().equals("B")) out.print("selected"); %>>Quincenal</option>
                <option value="M" <%if (rtBasic.getE01ACMSHF().equals("M")) out.print("selected"); %>>Mensual</option>
                <option value="Q" <%if (rtBasic.getE01ACMSHF().equals("Q")) out.print("selected"); %>>Trimestral</option>
              </select>
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" > 
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="30%"> 
              <div align="right">Plan Tarifario :</div>
            </td>
            <td nowrap width="18%"> 
              <input type="text" name="E01ACMACL" size="3" maxlength="2" value="<%= rtBasic.getE01ACMACL().trim()%>" <%if(rtBasic.getE01ACMATY().equals("CVIS")) out.print("readonly");%>>
              
              <%if(rtBasic.getE01ACMATY().equals("CVIS")){%>
	              <a href="javascript:GetRetCod('E01ACMACL',document.forms[0].E01ACMATY.value,'',document.forms[0].E01ACMPRO.value)" >
              <% }else{%>            
	              <a href="javascript:GetRetCod('E01ACMACL',document.forms[0].E01ACMATY.value)">
              <% }%>            
              <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="middle" border="0" ></a> 
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" ></td>
            <td nowrap width="28%"> 
              <div align="right">Ciclo/D&iacute;a Cobro de Cargos :</div>
            </td>
            <td nowrap width="24%"> 
              <input type="text" name="E01ACMSHY" size="3" maxlength="2" value="<%= rtBasic.getE01ACMSHY().trim()%>" <%if(rtBasic.getE01ACMATY().equals("CVIS")) out.print("readonly");%>>
              <%if(!rtBasic.getE01ACMATY().equals("CVIS")){%>
	              <a href="javascript:GetCode('E01ACMSHY','STATIC_rt_ciclo.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" border="0" ></a> 
    	          <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" > 
        	  <%}%>	
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <H4>Informaci&oacute;n Estado de Cuenta</H4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="30%"> 
              <div align="right">Frecuencia  :</div>
            </td>
            <td nowrap width="18%">
              <select name="E01ACMSTF">
				<option value=" " <% if (!(rtBasic.getE01ACMSTF().equals("D") ||rtBasic.getE01ACMSTF().equals("W")
				||rtBasic.getE01ACMSTF().equals("B")||rtBasic.getE01ACMSTF().equals("M")
				||rtBasic.getE01ACMSTF().equals("Q")||rtBasic.getE01ACMSTF().equals("Y")))
				out.print("selected"); %>></option>

                <option value="M" <%if (rtBasic.getE01ACMSTF().equals("M")) out.print("selected"); %>>Mensual</option>

              </select>
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" > 
            </td>
            <td nowrap width="28%"> 
              <div align="right">Retenci&oacute;n de Correos :</div>
            </td>
            <td nowrap width="24%"> 
              <input type="hidden" name="E01ACMHSF" value="<%= rtBasic.getE01ACMHSF()%>">
              <input type="radio" name="CE01ACMHSF" value="H" onClick="document.forms[0].E01ACMHSF.value='H'"
			  <%if(rtBasic.getE01ACMHSF().equals("H")) out.print("checked");%>>
              S&iacute; 
              <input type="radio" name="CE01ACMHSF" value="" onClick="document.forms[0].E01ACMHSF.value=' '"
			  <%if(rtBasic.getE01ACMHSF().trim().equals("")) out.print("checked");%>>
              No </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="30%"> 
              <div align="right">Ciclo/D&iacute;a Impresi&oacute;n  
                :</div>
            </td>
            <td nowrap width="18%"> 
              <input type="text" name="E01ACMSDY" size="3" maxlength="2" value="<%= rtBasic.getE01ACMSDY().trim()%>">
              <a href="javascript:GetCode('E01ACMSDY','STATIC_rt_ciclo.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" border="0" ></a> 
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" > 
            </td>
            <td nowrap width="28%"> 
              <div align="right">Estado  Consolidado :</div>
            </td>
            <td nowrap width="24%">
              <input type="hidden" name="E01ACMCSF" value="<%= rtBasic.getE01ACMCSF()%>">
              <input type="radio" name="CE01ACMCSF" value="Y" onClick="document.forms[0].E01ACMCSF.value='Y'"
			  <%if(rtBasic.getE01ACMCSF().equals("Y")) out.print("checked");%>>
              S&iacute; 
              <input type="radio" name="CE01ACMCSF" value="N" onClick="document.forms[0].E01ACMCSF.value='N'"
			  <%if(rtBasic.getE01ACMCSF().equals("N")) out.print("checked");%>>
              No </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="30%" height="23"> 
              <div align="right">Tipo  Estado de 
                Cuenta :</div>
            </td>
            <td nowrap width="18%" height="23"> 
              <select name="E01ACMSTY">
                <option value=" " <% if (!(rtBasic.getE01ACMSTY().equals("P") ||rtBasic.getE01ACMSTY().equals("C")
				||rtBasic.getE01ACMSTY().equals("N"))) out.print("selected"); %>></option>
                <option value="P" <%if (rtBasic.getE01ACMSTY().equals("P")) out.print("selected"); %>>Personal</option>
                <option value="C" <%if (rtBasic.getE01ACMSTY().equals("C")) out.print("selected"); %>>Corporativa</option>
                <option value="N" <%if (rtBasic.getE01ACMSTY().equals("N")) out.print("selected"); %>>Ninguna</option>
              </select>
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" > 
            </td>
            <td nowrap width="28%" height="23"> 
              <div align="right">Forma de Env&iacute;o  :</div>
            </td>
            <td nowrap width="24%" height="23">
              <select name="E01ACMSTE" disabled="disabled">
				<option value="N" <% if (!(rtBasic.getE01ACMSTE().equals("T") ||rtBasic.getE01ACMSTE().equals("P")
				||rtBasic.getE01ACMSTE().equals("F")||rtBasic.getE01ACMSTE().equals("E")))
				out.print("selected"); %>>Ninguna</option>
                <option value="T" <%if (rtBasic.getE01ACMSTE().equals("T")) out.print("selected"); %>>Telex</option>
                <option value="P" <%if (rtBasic.getE01ACMSTE().equals("P")) out.print("selected"); %>>Impresora</option>
                <option value="F" <%if (rtBasic.getE01ACMSTE().equals("F")) out.print("selected"); %>>Facsimil</option>
                <option value="E" <%if (rtBasic.getE01ACMSTE().equals("E")) out.print("selected"); %>>Email</option>
              </select>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <H4>Asignaci&oacute;n de Chequeras</H4>
  <table  class="tableinfo">
    <tr> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark">
            <td nowrap><div align="right">Nombre Personalizado :</div></td>
            <td nowrap colspan=3>
            	<INPUT type="text" name="E01ACMCK1" size="35" maxlength="35" value="<%= rtBasic.getE01ACMCK1().trim()%>">
            </td>
          
          </tr>
          <tr id="trclear">
            <td nowrap>
              <div align="right">
              <SELECT name="E01ACMRE1">
                 <OPTION value="Y" <%if (!rtBasic.getE01ACMRE1().equals("O")) out.print("selected"); %>="">Y</OPTION>
                 <% if (currUser.getE01INT().equals("07")) { %>
                   <OPTION value="O" <%if (rtBasic.getE01ACMRE1().equals("O")) out.print("selected"); %>="">O</OPTION>               
                 <%} else { %>
                   <OPTION value="O" <%if (rtBasic.getE01ACMRE1().equals("O")) out.print("selected"); %>="">Y/O</OPTION>
                 <% } %>
              </SELECT>
              </div>
            </td>
            <td nowrap colspan=3>
              <INPUT type="text" name="E01ACMCK2" size="35" maxlength="35" value="<%= rtBasic.getE01ACMCK2().trim()%>">
            </td>
            
          </tr>
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Tipo de Chequera :</div>
            </td>
            <td nowrap <% if (!rtBasic.getH01FLGMAS().equals("N")) out.print("colspan=3");%>>
            <INPUT type="text" name="E01ACMTBK" size="3" maxlength="2" value="<%= rtBasic.getE01ACMTBK().trim()%>">
              <A href="javascript:GetTypCHK('E01ACMTBK','','<%= rtBasic.getE01ACMATY().trim()%>','<%= rtBasic.getE01ACMCCY().trim()%>')">
              <IMG src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="middle" border="0"></A> 
            </td>
            
            <% if (rtBasic.getH01FLGMAS().equals("N")) {%>
            <td nowrap>
              <div align="right">Cantidad :</div>
            </td>
            <td nowrap>
              <INPUT type="text" name="E01ACMNCB" size="3" maxlength="2" value="<%= rtBasic.getE01ACMNCB().trim()%>">
            </td>
            <% } %>
            
          </tr>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Cobro de Cargos  :</div>
            </td>
            <td nowrap ><INPUT type="radio" name="E01ACMCBC" value="Y" <% if(!rtBasic.getE01ACMCBC().equals("N")) out.print("checked");%>="">
              S&iacute; 
              <INPUT type="radio" name="E01ACMCBC" value="N" <% if(rtBasic.getE01ACMCBC().equals("N")) out.print("checked");%>="">
               No
            </td>
            <td nowrap> 
              <div align="right">Stock M�nimo  :</div>
            </td>
            <td nowrap >
            <INPUT type="text" name="E01ACMMSK" size="3" maxlength="2" value="<%= rtBasic.getE01ACMMSK().trim()%>"> 
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Restricci�n de Entregas :</div>
            </td>
            <td nowrap ><INPUT type="radio" name="E01ACMCON" value="Y" <% if(rtBasic.getE01ACMCON().equals("Y")) out.print("checked");%>="">
              S&iacute; 
              <INPUT type="radio" name="E01ACMCON" value="N" <% if(!rtBasic.getE01ACMCON().equals("Y")) out.print("checked");%>="">
              No
            </td>
            <td nowrap> 
              <div align="right"></div>
            </td>
            <td nowrap >
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
 <H4>Transferencia Programada</H4>
<table id="mainTable" class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trclear">
				<td nowrap width="25%">
				<div align="right">Fecha de Inicio :</div>
				</td>
				<td nowrap width="24%">
					<eibsinput:date name="rtBasic" fn_year="E01ACASTY" fn_month="E01ACASTM" fn_day="E01ACASTD" required="false" />
				</td>
				<td nowrap width="25%">
				<div align="right">Frecuencia :</div>
				</td>
				<td nowrap width="24%"><select name="E01ACAFRE">
					<option></option>
					<option value="M" <% if (rtBasic.getE01ACAFRE().equals("M")) out.print("selected");%>>Mensual</option>
					<option value="Q" <% if (rtBasic.getE01ACAFRE().equals("Q")) out.print("selected");%>>Trimestral</option>
					<option value="S" <% if (rtBasic.getE01ACAFRE().equals("S")) out.print("selected");%>>Semestral</option>
					<option value="Y" <% if (rtBasic.getE01ACAFRE().equals("Y")) out.print("selected");%>>Anual</option>
				</select>
				</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="24%">
				<div align="right">Moneda :</div>
				</td>
				<td nowrap width="21%">
					<eibsinput:help name="rtBasic" property="E01ACACCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY%>" required="false" fn_param_one="E01ACACCY" fn_param_two="document.forms[0].E01ACMBNK.value" />
				</td>
				<td nowrap width="25%">
					<div align="right">D�a de aplicaci�n :</div>
				</td>
					<td nowrap width="24%"><input type="text" size="2" maxlength="3" name="E01ACADMP" value="<%=rtBasic.getE01ACADMP().trim()%>">
				</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="23%">
					<div align="right">Medio de Pago :</div>
				</td><td nowrap width="21%">
					<select name="E01ACATYR">
						<option></option>
						<option value="2" <% if (rtBasic.getE01ACATYR().equals("2")) out.print("selected");%>>PAC/Automatico</option>
					</select>
				</td>
				<td nowrap width="25%">
				<div align="right">Cuenta  a Debitar:</div>
				</td><td nowrap width="24%">
		              <INPUT name="E01ACAPAC" size="13" maxlength="12" value="<%= rtBasic.getE01ACAPAC().trim()%>" >
					  <A href="javascript:GetAccByClient('E01ACAPAC','','RA','',document.forms[0].E01ACMCUN.value)"> 
					  <IMG src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absbottom" border="0"></A>
				</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="23%">
					<div align="right">Estado :</div>
				</td>
				<td nowrap width="21%">
					<select name="E01ACASTS">
					<option></option>
					<option value="1" <% if (rtBasic.getE01ACASTS().equals("1")) out.print("selected"); %>>Vigente</option>
					<option value="2" <% if (rtBasic.getE01ACASTS().equals("2")) out.print("selected"); %>>No Vigente</option>
				</select>
				</td>
				<td nowrap width="25%">
				<div align="right">Monto del Deposito:</div>
				</td>
				<td nowrap width="24%">
                    <input type="text" name="E01ACAAUG" size="17" maxlength="15" value="<%= rtBasic.getE01ACAAUG().trim()%>" onKeypress="enterDecimal()" >						
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<% if (!"18".equals(currUser.getE01INT())) { %>
  
<H4>Origen de Fondos</H4>
  
  <TABLE  id="mainTable" class="tableinfo">
  <TR><TD>
   <table id="headTable" >
    <tr id="trdark"> 
      <td nowrap align="center" >Concepto</td>
      <td nowrap align="center" >Banco</td>
      <td nowrap align="center" >Sucursal</td>
      <td nowrap align="center" > Moneda</td>
      <td nowrap align="center" > Referencia </td>
      <td nowrap align="center" > Monto</td>
    </tr>
    </table>
    
    <div id="dataDiv" style="height:60; overflow-y :scroll; z-index:0" >
     <table id="dataTable" >
          <%
  				   int amount = 9;
 				   String name;
  					for ( int i=1; i<=amount; i++ ) {
   					  name = i + "";
   			%> 
          <tr id="trclear"> 
            <td nowrap > 
                <div align="center" nowrap> 
                  <input type="text" name="E01OFFOP<%= name %>" id="E01OFFOP<%= name %>" value="<%= rtBasic.getField("E01OFFOP"+name).getString().trim()%>" size="3" maxlength="3">
                  <input type="hidden" name="E01OFFGL<%= name %>" value="<%= rtBasic.getField("E01OFFGL"+name).getString().trim()%>">
                <input type="text" name="E01OFFCO<%= name %>" size="25" maxlength="25" readonly value="<%= rtBasic.getField("E01OFFCO"+name).getString().trim()%>" 
                  oncontextmenu="showPopUp(conceptHelp,this.name,document.forms[0].E01ACMBNK.value,'','E01OFFGL<%= name %>','E01OFFOP<%= name %>',document.forms[0].E01ACMACD.value); return false;">
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFBK<%= name %>" size="2" maxlength="2" value="<%= rtBasic.getField("E01OFFBK"+name).getString().trim()%>">
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01OFFBR<%= name %>" size="4" maxlength="4" value="<%= rtBasic.getField("E01OFFBR"+name).getString().trim()%>"
                oncontextmenu="showPopUp(branchHelp,this.name,document.forms[0].E01ACMBNK.value,'','','',''); return false;">
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01OFFCY<%= name %>" size="3" maxlength="3" value="<%= rtBasic.getField("E01OFFCY"+name).getString().trim()%>"
                oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E01ACMBNK.value,'','','',''); return false;">
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFAC<%= name %>" id="E01OFFAC<%= name %>" size="12" maxlength="12"  value="<%= rtBasic.getField("E01OFFAC"+name).getString().trim()%>"
                oncontextmenu="showPopUp(accountCustomerHelp,this.name,document.forms[0].E01ACMBNK.value,'',document.forms[0].E01ACMCUN.value,'','RT'); return false;">
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFAM<%= name %>" size="15" maxlength="15"  value="<%= rtBasic.getField("E01OFFAM"+name).getString().trim()%>" onKeypress="enterDecimal()">
              </div>
            </td>
          </tr>
          <%
    		}
    		%> 
          </table>
        </div>
    
     <TABLE id="footTable">
          <tr id="trdark"> 
            <td nowrap align="right"><b>Equivalente Moneda de la Cuenta:</b>
            </td>
            <td nowrap  align="center"> 
                <input type="text" name="E01OFFEQV" size="15" maxlength="15" readonly value="<%= rtBasic.getE01OFFEQV().trim()%>">
            </td>
          </tr>
     </table>
<% } %>        
   </TD>  
</TR>	
</TABLE>    
 <SCRIPT language="javascript">
    function tableresize() {
     adjustEquTables(headTable,dataTable,dataDiv,0,true);
   	}
  		tableresize();
  		window.onresize=tableresize;
  </SCRIPT>
      
<% if(error.getERWRNG().equals("Y")){%>
   <h4 style="text-align:center"><input type="checkbox" name="H01FLGWK2" value="A" <% if(rtBasic.getH01FLGWK2().equals("A")){ out.print("checked");} %>>
      Aceptar con Aviso</h4>
  <% } %>     
          <div align="center"> 
            <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
          </div>
  </form>
</body>
</html>
