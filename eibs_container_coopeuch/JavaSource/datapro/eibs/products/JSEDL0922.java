package datapro.eibs.products;

import com.datapro.generics.Util;
import datapro.eibs.beans.*;
import datapro.eibs.master.*;
import datapro.eibs.sockets.MessageRecord;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class JSEDL0922 extends JSEIBSServlet {

	private static final long serialVersionUID = 1L;
	protected static final int R_ENTER_INQUIRY = 100;
	protected static final int A_ENTER_INQUIRY = 200;
	protected static final int A_LN_FORG_PUN = 300;

	public JSEDL0922() {
	}

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_ENTER_INQUIRY: 
			procReqEnterInquiry(user, req, res, session);
			break;

		case A_ENTER_INQUIRY: 
			procActionEnterInquiry(user, req, res, session);
			break;

		case A_LN_FORG_PUN: 
			procActionLnForgPun(user, req, res, session);
			break;

		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	
	protected void procReqEnterInquiry(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException 
			{
		
		UserPos userPO = (UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		
		try 
		{
			flexLog("About to call Page: EDL0922_ln_enter_inquiry.jsp");
			forward("EDL0922_ln_enter_inquiry.jsp", req, res);
		} 
		catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
		}
	}

	protected void procActionEnterInquiry(ESS0030DSMessage user,	HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException 
			{
		
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EDL0922", req);

			EDL092201Message msgLN = (EDL092201Message) mp.getMessageRecord("EDL092201", user.getH01USR(), "0001");
		
			try {
			 	if (req.getParameter("E01DEAACC") != null)
			 		msgLN.setE01DEAACC(req.getParameter("E01DEAACC"));
			}
			catch (Exception e)	{
				msgLN.setE01DEAACC("0");
			}
			
			mp.sendMessage(msgLN);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) 
			{
				session.setAttribute("error", error);
				
				flexLog("About to call Page: EDL0922_ln_enter_inquiry.jsp");
				forward("EDL0922_ln_enter_inquiry.jsp", req, res);
			} 
			else 
			{
				msgLN = (EDL092201Message) mp.receiveMessageRecord();


				session.setAttribute("lnCancel", msgLN);

				flexLog("About to call Page: EDL0922_ln_forgiveness_punished.jsp");
				forwardOnSuccess("EDL0922_ln_forgiveness_punished.jsp", req, res);
				
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	
	
	protected void procActionLnForgPun(ESS0030DSMessage user,	HttpServletRequest req, HttpServletResponse res, HttpSession session)
	throws ServletException, IOException 
	{

		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");

		try {
			mp = getMessageProcessor("EDL0922", req);

			EDL092202Message msgLN = (EDL092202Message) mp.getMessageRecord("EDL092202", user.getH01USR(), "0001");

			try {
				if (req.getParameter("E01DEAACC") != null)
					msgLN.setE02DEAACC(req.getParameter("E01DEAACC"));
			}
			catch (Exception e)	{
				msgLN.setE02DEAACC("0");
			}
	
			mp.sendMessage(msgLN);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) 
			{
				session.setAttribute("error", error);
		
				flexLog("About to call Page: EDL0922_ln_forgiveness_punished.jsp");
				forward("EDL0922_ln_forgiveness_punished.jsp", req, res);
			} 
			else 
			{
				msgLN = (EDL092202Message) mp.receiveMessageRecord();
		
				flexLog("About to call Page: EDL0922_ln_forgiveness_punished_res.jsp");
				forwardOnSuccess("EDL0922_ln_forgiveness_punished_res.jsp", req, res);
		
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
//
}
