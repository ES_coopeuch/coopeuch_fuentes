package datapro.eibs.products;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adobe.fdf.FDFDoc;

import datapro.eibs.beans.EDD000001Message;
import datapro.eibs.beans.EDD230001Message;
import datapro.eibs.beans.EDD231001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.reports.JSEFRM000PDF;

public class JSEDD2300 extends JSEFRM000PDF {

	protected static final int R_LIST_PDF = 100;
	protected static final int A_PDF_VIEW = 200;	

	protected static final int R_ENTER_REIM_DOCTOS = 300;
	protected static final int A_ENTER_REIM_DOCTOS = 400;	
	protected static final int A_ENTER_REIM_DOCTOS_LAT = 500;
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		
		switch (screen) {
			case R_LIST_PDF:
				procReqFormListPdf(user, req, res, session);
				break;

			case A_PDF_VIEW:
				procActPdfView(user, req, res, session);
				break;

			case R_ENTER_REIM_DOCTOS:
				procReqReimDoctos(user, req, res, session);
				break;
			
			case A_ENTER_REIM_DOCTOS:
				procActReimDoctos(user, req, res, session);
				break;

			case A_ENTER_REIM_DOCTOS_LAT:
				procActReimDoctosLat(user, req, res, session);
				break;

			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}


	protected void procReqFormListPdf(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException 
		{
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		EDD000001Message msgRT = new datapro.eibs.beans.EDD000001Message();

		setMessageRecord(req, msgRT);
		
		try {
			msgRT.setE01ACMACC(req.getParameter("E01ACMACC"));
		} catch (Exception e) {
			msgRT.setE01ACMACC("0");
		}
		
		try 
		{
			
			userPO.setCusNum(msgRT.getE01ACMCUN());
			userPO.setAccNum(msgRT.getE01ACMACC());
			userPO.setCusName(msgRT.getE01CUSNA1());
			userPO.setHeader1("N");

			
			session.setAttribute("rtBasic", msgRT);
			session.setAttribute("userPO", userPO);
			flexLog("About to call Page: EDD2300_pdf_list.jsp");
			forward("EDD2300_pdf_list.jsp", req, res);
		} 

		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

	protected void procActPdfView(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException 
		{

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		MessageProcessor mp = null;
		try  
		{

			mp = getMessageProcessor("EDD2300", req);

			EDD230001Message msg = (EDD230001Message) mp.getMessageRecord("EDD230001", user.getH01USR(), "0001");

			try {
				msg.setE01CMRACC(userPO.getIdentifier().trim());
			} catch (Exception e) {
				msg.setE01CMRACC("0");
			}
						
			mp.sendMessage(msg);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();

			msg = (EDD230001Message) mp.receiveMessageRecord();

			if (!mp.hasError(error)) 
			{
				FDFDoc outputFDF = new FDFDoc();
				if (session.getAttribute("pdfData") != null) {
					session.removeAttribute("pdfData");
				}
				if (dstXML == null || dstXML.isEmpty()) {
					initDataStructure();			
					flexLog("XML file was reloaded.");
				}

				getFormData(outputFDF, (String)formatNames.get("EDD230001") + ".", msg);

				int index = 0; 
				String prefix = "";
				prefix = getPrefix(msg);
				buildFormList(outputFDF, msg, "." + prefix, index++);

				String urlPDF = datapro.eibs.master.JSEIBSProp.getFORMPDFURL() + req.getParameter("pdfName");
				sendPdf(
						req, 
						res, 
						urlPDF, 
						outputFDF, 
						req.getParameter("action"),
						req.getParameter("copies"),
						""
						);
			}
			else
			{
				String MsgErr = error.getERNU01() + " - " + error.getERDS01();
				session.setAttribute("error_msg", MsgErr);
				forward("EFRM000_pdf_forms_error.jsp", req, res);
			}	
		} 
		
		catch (Exception e) {
			session.setAttribute("error_msg", "");
			forward("EFRM000_pdf_forms_error.jsp", req, res);
		}
		
		finally {
			if (mp != null) mp.close();
		}
	}

	protected void procReqReimDoctos(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			ELEERRMessage msgError = null;
			UserPos userPO = null;

			try {

				msgError = new datapro.eibs.beans.ELEERRMessage();
				userPO = new datapro.eibs.beans.UserPos();
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);

			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			try {
				flexLog("About to call Page: EDD2310_enter_tarifado.jsp");
				forward("EDD2300_enter_reimpresion_doctos.jsp", req, res);
				} catch (Exception e) {
				flexLog("Exception calling page " + e);
			}

		}


	protected void procActReimDoctos(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException 
		{

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		MessageProcessor mp = null;
		try  
		{
 
			mp = getMessageProcessor("EDD2310", req);

			EDD231001Message msg = (EDD231001Message) mp.getMessageRecord("EDD231001", user.getH01USR(), "0001");
			EDD000001Message msgRT = new datapro.eibs.beans.EDD000001Message();

			setMessageRecord(req, msgRT);

			
			try {
				msg.setE01ACMACC(req.getParameter("E01ACMACC"));
				userPO.setIdentifier(req.getParameter("E01ACMACC"));
			} catch (Exception e) {
				msg.setE01ACMACC("0");
			}

			mp.sendMessage(msg);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();

			msg = (EDD231001Message) mp.receiveMessageRecord();
 
			if (!mp.hasError(error)) 
			{
				userPO.setCusNum(msg.getE01ACMCUN());
				userPO.setAccNum(msg.getE01ACMACC());
				userPO.setCusName(msg.getE01ACMNA1());
				userPO.setHeader1("R");
				
				session.setAttribute("userPO", userPO);
				session.setAttribute("rtBasic", msgRT);
				flexLog("About to call Page: EDD2300_pdf_list.jsp");
				forward("EDD2300_pdf_list.jsp", req, res);
			}
			else
			{
				session.setAttribute("error", error);
				flexLog("About to call Page: EDD2310_enter_tarifado.jsp");
				forward("EDD2300_enter_reimpresion_doctos.jsp", req, res);
			}	
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null) mp.close();
		}
	}

	
	protected void procActReimDoctosLat(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException 
		{

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		MessageProcessor mp = null;
		try  
		{
 
			mp = getMessageProcessor("EDD2310", req);

			EDD231001Message msg = (EDD231001Message) mp.getMessageRecord("EDD231001", user.getH01USR(), "0001");
			EDD000001Message msgRT = new datapro.eibs.beans.EDD000001Message();

			
			try {
				msg.setE01ACMACC(userPO.getIdentifier().trim());
				msg.setH01FLGWK3("F");
			} catch (Exception e) {
				msg.setE01ACMACC("0");
			}

			mp.sendMessage(msg);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();

			msg = (EDD231001Message) mp.receiveMessageRecord();
 
			if (!mp.hasError(error)) 
			{
				userPO.setCusNum(msg.getE01ACMCUN());
				userPO.setAccNum(msg.getE01ACMACC());
				userPO.setCusName(msg.getE01ACMNA1());
				userPO.setHeader1("R");
				
				session.setAttribute("userPO", userPO);
				session.setAttribute("rtBasic", msgRT);
				flexLog("About to call Page: EDD2300_pdf_list.jsp");
				forward("EDD2300_pdf_list.jsp", req, res);
			}
			else
			{
				session.setAttribute("error_msg", "");
				flexLog("About to call Page: EFRM000_pdf_forms_error.jsp");
				forward("EFRM000_pdf_forms_error.jsp", req, res);
			}	
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null) mp.close();
		}
	}

}
