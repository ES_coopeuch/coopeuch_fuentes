<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util, datapro.eibs.beans.*" %>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id="header" class="datapro.eibs.beans.EPV108001Message" scope="session" />
<jsp:useBean id="EPV108002List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="EPV108003List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="EPV108004List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="EPV108005List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="EPV108006List" class="datapro.eibs.beans.JBObjList" scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>


<% 
	if ( !error.getERRNUM().equals("0") ) {
    	error.setERRNUM("0");
     	out.println("<SCRIPT Language=\"Javascript\">");
     	out.println("       showErrors()");
     	out.println("</SCRIPT>");
 	}
 	
 	int row = 0;
 	
 	String ownerAdd = header.getE01DRPMA1().trim();
 	ownerAdd += " " + header.getE01DRPMA2().trim();
 	ownerAdd += " " + header.getE01DRPMA3().trim();
 	ownerAdd += " " + header.getE01DRPMA4().trim();
 	
 	String commAdd = header.getE01DCMMA1().trim();
 	commAdd += " " + header.getE01DCMMA2().trim();
 	commAdd += " " + header.getE01DCMMA3().trim();
 	commAdd += " " + header.getE01DCMMA4().trim();
%>
<SCRIPT Language="Javascript">
 function goQuery(screen) {
 	var dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECB0040?SCREEN="+screen+"&CUN="+document.forms[0].E01CUSNUM.value; 	 	
 	CenterWindow(dir,850,650,2);
 }
</SCRIPT>
</head>
<body>

<h3 align="center">Estado de Situaci�n
	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_status_inquiry.jsp, EPV1080"></h3>
<hr size="4">

<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1080">
	<input type="hidden" name="SCREEN" value="">

	<h4>Informaci�n del Cliente</h4>
	
  	<table  class="tableinfo">
    	<tr bordercolor="#FFFFFF"> 
      		<td nowrap> 
        		<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<td nowrap align="right" width="10%">Cliente :</td>
			            <td nowrap align="left" width="21%" colspan="2">
				  			<eibsinput:text name="header" property="E01CUSNUM" size="8"  readonly="true" />
			 	 			<eibsinput:text name="header" property="E01CUSNA1" size="30"  readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">Estado :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01DSCSTA" size="19" readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">Rut :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01CUSIDN" size="19" readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">Fec. Estado :</td>
			            <td nowrap align="left" width="13%">
    	        			<eibsinput:date name="header" fn_year="E01CUSIDY" fn_month="E01CUSIDM" fn_day="E01CUSIDD" readonly="true"/>
			            </td>
        			</tr>
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<td nowrap align="right" width="10%">Mail :</td>
			            <td nowrap align="left" width="21%" colspan="2">
				  			<eibsinput:text name="header" property="E01CUSIAD" size="42"  readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">Fec. Socio :</td>
			            <td nowrap align="left" width="13%">
    	        			<eibsinput:date name="header" fn_year="E01CUSFSY" fn_month="E01CUSFSM" fn_day="E01CUSFSD" readonly="true"/>
			            </td>
        				<td nowrap align="right" width="10%">Fec. Nacim. :</td>
			            <td nowrap align="left" width="13%">
    	        			<eibsinput:date name="header" fn_year="E01CUSBDY" fn_month="E01CUSBDM" fn_day="E01CUSBDD" readonly="true"/>
			            </td>
        				<td nowrap align="right" width="10%">Oficina :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01DSCBRA" size="19" readonly="true" />
			            </td>
        			</tr>
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<td nowrap align="right" width="10%">Direc. Part. :</td>
			            <td nowrap align="left" width="21%" colspan="2">
                			<input type="text" name="OWNERADD" size="42" value="<%= ownerAdd %>" readonly>
			            </td>
        				<td nowrap align="right" width="10%">Telef. Part. :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01CUSHPN" size="19" readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">Calidad :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01CALIDA" size="19" readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">Usuario :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="H01USERID" size="19"readonly="true" />
			            </td>
        			</tr>
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<td nowrap align="right" width="10%">Direc. Com. :</td>
			            <td nowrap align="left" width="21%" colspan="2">
                			<input type="text" name="COMMADD" size="42" value="<%= commAdd %>" readonly>
			            </td>
        				<td nowrap align="right" width="10%">Telef. Com. :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01CUSPHN" size="19" readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">C.Pago Pla. :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01PAGPLA" size="19" readonly="true" />
			            </td>
			            <!-- Lugar a Modificar por EIFRS Deterioro -->
			            <% if (header.getE01FLGDET().equalsIgnoreCase("S")){ %>
        				<td nowrap align="right" width="10%">
        					Estado RUT:
						</td>
			            <td nowrap align="left" width="13%">
			            	<%=header.getE01TXTDET()%>
						</td>
						<% } else {%>
						<td nowrap align="right" width="10%">
						</td>
			            <td nowrap align="left" width="13%">
						</td>
						<% } %>
        			</tr>
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<td nowrap align="right" width="10%">Est. Civil :</td>
			            <td nowrap align="left" width="21%" colspan="2">
				  			<eibsinput:text name="header" property="E01DSCMST" size="42"  readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">Sexo :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01CUSSEX" size="19" readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">C.Pago Dir. :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01PAGDIR" size="19" readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">&nbsp;</td>
			            <td nowrap align="left" width="13%">&nbsp;</td>
        			</tr>
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<td nowrap align="right" width="10%">Gest. Cobranza :</td>
			            <td nowrap align="left" width="6%">
				  			<eibsinput:text name="header" property="E01GESCBZ" size="3" readonly="true" />
				  	    </td>
			            <td nowrap align="left" width="15%" class="tdbkg" >
				  			<a href="javascript:goQuery('200')">Gestion Cobranza</a>
				  	    </td>				  	    
			           	<td nowrap align="right" width="10%">Cambio Empl. :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01CAMEMP" size="19" readonly="true" />				  			
			            </td>
        				<td nowrap align="right" width="10%">Convenio Ant. :</td>			            
			            <td nowrap align="left" width="13%">
			            	<eibsinput:text name="header" property="E01CUSCNA" size="8" readonly="true" />				  			
			            </td>			            
        				<td nowrap align="right" width="10%">Fecha Conv. Ant. :</td>
			            <td nowrap align="left" width="13%">   	        			
			            	<eibsinput:date name="header" fn_year="E01CNASDY" fn_month="E01CNASDM" fn_day="E01CNASDD" readonly="true"/>
						</td>
        			</tr>
        		</table>
        	</td>
        </tr>
	</table>        		
	 
	<h4>Informaci�n del Convenio</h4>
	<%row = 0;%>
  	<table  class="tableinfo">
    	<tr bordercolor="#FFFFFF"> 
      		<td nowrap> 
        		<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<td nowrap align="right" width="10%">Convenio :</td>
			            <td nowrap align="left" width="21%">
				  			<eibsinput:text name="header" property="E01CUSCNV" size="8"  readonly="true" />
			 	 			<eibsinput:text name="header" property="E01DSCCNV" size="30"  readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">%Desc. Empl :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01DESEMP" size="19" readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">IRE :</td>
			            <td nowrap align="left" width="13%">
				  			<eibsinput:text name="header" property="E01VALIRE" size="19" readonly="true" />
			            </td>
        				<td nowrap align="right" width="10%">&nbsp;</td>
			            <td nowrap align="left" width="13%">&nbsp;</td>
        			</tr>
        		</table>
        	</td>
        </tr>
	</table>
	        		
	<h4>Ahorros</h4>
	<%row = 0;%>
  	<table  class="tableinfo">
    	<tr bordercolor="#FFFFFF"> 
      		<td nowrap> 
        		<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<th width="10%">&nbsp;</th>
        				<th width="15%">Aporte Men. ($)</th>
        				<th width="15%">Monto Total ($)</th>
        				<th width="60%">&nbsp;</th>
        			</tr>
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<td nowrap align="right" width="10%">Cuotas de Participacion :</td>
			            <td nowrap align="left" width="15%">
				  			<eibsinput:text name="header" property="E01CPAAPO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
			            </td>
			            <td nowrap align="left" width="15%">
				  			<eibsinput:text name="header" property="E01CPASAL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
			            </td>
			            <td nowrap align="left" width="60%">&nbsp;</td>
        			</tr>
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<td nowrap align="right" width="10%">Cuentas de Ahorro :</td>
			            <td nowrap align="left" width="15%">
				  			<eibsinput:text name="header" property="E01AHOAPO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
			            </td>
			            <td nowrap align="left" width="15%">
				  			<eibsinput:text name="header" property="E01AHOSAL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
			            </td>
			            <td nowrap align="left" width="60%">&nbsp;</td>
        			</tr>
        			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
        				<td nowrap align="right" width="10%">Cuentas Acreedoras :</td>
			            <td nowrap align="left" width="15%">
			            </td>
			            <td nowrap align="left" width="15%">
				  			<eibsinput:text name="header" property="E01REMSAL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
			            </td>
			            <td nowrap align="left" width="60%">&nbsp;</td>
        			</tr>
        		</table>
        	</td>
        </tr>
	</table>

	<h3>Deudas Vigentes</h3>
<%
	if (!EPV108002List.isEmpty()) { 
%>	
	<h4>Cr&eacute;ditos</h4>
	<%row = 0;%>

  	<table  class="tableinfo">
    	<tr bordercolor="#FFFFFF"> 
      		<td nowrap> 
				<table id="headTable" width="100%">
					<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
						<th align="center" nowrap width="10%">N�mero</th>
						<th align="center" nowrap width="10%">Promoci�n</th>
						<th align="center" nowrap width="5%">T. Pago</th>
						<th align="center" nowrap width="5%">Estado</th>
						<th align="center" nowrap width="10%">F. Otorgam</th>
						<th align="center" nowrap width="10%">Tasa</th>			
						<th align="center" nowrap width="10%">Mto. Cred.</th>
						<th align="center" nowrap width="10%">Saldo Cred.</th>
						<th align="center" nowrap width="10%">Val. Cuota</th>
						<th align="center" nowrap width="5%">Plazo</th>
						<th align="center" nowrap width="5%">Pagadas</th>
						<th align="center" nowrap width="5%">Tipo Deuda</th>
						<th align="center" nowrap width="5%">Aval</th>
					</tr>
<%
					EPV108002List.initRow();
					while (EPV108002List.getNextRow()) {
						EPV108002Message convObj = (EPV108002Message) EPV108002List.getRecord();
%>
		          		<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE02DEAACC())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE02DSCPRM())%></td>
							<td nowrap align="center" width="5%"><%=Util.formatCell(convObj.getE02TYPPAG())%></td>
							<td nowrap align="center" width="5%"><%=Util.formatCell(convObj.getE02ESTADO())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatDate(convObj.getE02FECAPD(), convObj.getE02FECAPM(), convObj.getE02FECAPY())%></td>
							<td nowrap align="right" width="10%"><%=Util.formatCCY(convObj.getE02DEARTE())%></td>
							<td nowrap align="right" width="10%"><%=Util.formatCCY(convObj.getE02APECRE())%></td>
							<td nowrap align="right" width="10%"><%=Util.formatCCY(convObj.getE02SALCRE())%></td>
							<td nowrap align="right" width="10%"><%=Util.formatCCY(convObj.getE02VALCUO())%></td>
							<td nowrap align="right" width="5%"><%=Util.formatCell(convObj.getE02TOTCUO())%></td>
							<td nowrap align="right" width="5%"><%=Util.formatCell(convObj.getE02CUPPAG())%></td>
							<td nowrap align="right" width="5%"><%=Util.formatCell(convObj.getE02TIPDEU())%></td>
							<td nowrap align="center" width="5%"><%=Util.formatCell(convObj.getE02AVLAYN())%></td>
						</tr>
<% 
					} 
%>
				</table>
        	</td>
        </tr>
	</table>
<%
	}
	 
	if (!EPV108003List.isEmpty()) { 
%>	
	<h4>Cr&eacute;ditos Morosos</h4>
	<%row = 0;%>

  	<table  class="tableinfo">
    	<tr bordercolor="#FFFFFF"> 
      		<td nowrap> 
				<table id="headTable" width="100%">
					<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
						<th align="center" nowrap width="15%">N�mero</th>
						<th align="center" nowrap width="15%">Cuota</th>
						<th align="center" nowrap width="15%">Vencimiento</th>
						<th align="center" nowrap width="15%">D�as Mora</th>
						<th align="center" nowrap width="10%">Tramo</th>
						<th align="center" nowrap width="15%">Mto. Abonado</th>			
						<th align="center" nowrap width="15%">Deuda Actual</th>
					</tr>
<%
					EPV108003List.initRow();
					while (EPV108003List.getNextRow()) {
						EPV108003Message convObj = (EPV108003Message) EPV108003List.getRecord();
%>
		          		<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
							<td nowrap align="center" width="15%"><%=Util.formatCell(convObj.getE03DEAACC())%></td>
							<td nowrap align="center" width="15%"><%=convObj.getE03CUOINI()%></td>
							<td nowrap align="center" width="15%"><%=Util.formatDate(convObj.getE03FECVED(), convObj.getE03FECVEM(), convObj.getE03FECVEY())%></td>
							<td nowrap align="center" width="15%"><%=Util.formatCell(convObj.getE03DISMOR())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE03TRAMOS())%></td>
							<td nowrap align="center" width="15%"><%=Util.formatCCY(convObj.getE03VALABO())%></td>
							<td nowrap align="center" width="15%"><%=Util.formatCCY(convObj.getE03VALMOR())%></td>
						</tr>
<% 
					} 
%>
				</table>
        	</td>
        </tr>
	</table>
<%
	}
	 
	if (!EPV108004List.isEmpty()) { 
%>	
	<h4>Tarjeta de Cr&eacute;dito</h4>
	<%row = 0;%>

  	<table  class="tableinfo">
    	<tr bordercolor="#FFFFFF"> 
      		<td nowrap> 
				<table id="headTable" width="100%">
					<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
						<th align="center" nowrap width="10%">N�mero</th>
						<th align="center" nowrap width="10%">Estado</th>
						<th align="center" nowrap width="10%">Fe. Venc.</th>
						<th align="center" nowrap width="10%">Deuda $</th>
						<th align="center" nowrap width="10%">Deuda U$</th>
						<th align="center" nowrap width="10%">Cupo Nac.</th>			
						<th align="center" nowrap width="10%">Cupo Int.</th>
						<th align="center" nowrap width="10%">Deuda Mora</th>
						<th align="center" nowrap width="10%">Pag. Min</th>
						<th align="center" nowrap width="5%">Tipo</th>
						<th align="center" nowrap width="5%">Titular</th>
					</tr>
<%
					EPV108004List.initRow();
					while (EPV108004List.getNextRow()) {
						EPV108004Message convObj = (EPV108004Message) EPV108004List.getRecord();
%>
		          		<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE04NUMACC())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE04ESTADO())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatDate(convObj.getE04FECVED(), convObj.getE04FECVEM(), convObj.getE04FECVEY())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCCY(convObj.getE04VALPES())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCCY(convObj.getE04VALDLS())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCCY(convObj.getE04CUPPES())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCCY(convObj.getE04CUPDLS())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCCY(convObj.getE04VALMOR())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCCY(convObj.getE04PAGMIN())%></td>
							<td nowrap align="center" width="5%"><%=Util.formatCell(convObj.getE04CDTIPO())%></td>
							<td nowrap align="center" width="5%"><%=Util.formatCell(convObj.getE04TITULA())%></td>
						</tr>
<% 
					} 
%>
				</table>
        	</td>
        </tr>
	</table>
<%
	} 
	
	if (!EPV108005List.isEmpty()) { 
%>	
	<h4>Garant�as</h4>
	<%row = 0;%>

  	<table  class="tableinfo">
    	<tr bordercolor="#FFFFFF"> 
      		<td nowrap> 
				<table id="headTable" width="100%">
					<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
						<th align="center" nowrap width="15%">N�mero</th>
						<th align="center" nowrap width="10%">Cobertura</th>
						<th align="center" nowrap width="20%">Descripci�n</th>
						<th align="center" nowrap width="15%">Valor Contab</th>
						<th align="center" nowrap width="10%">Tasaci�n Vig</th>
						<th align="center" nowrap width="10%">Seguro Vig</th>			
						<th align="center" nowrap width="10%">Prod</th>
						<th align="center" nowrap width="10%">Tipo Credito</th>
					</tr>
<%
					EPV108005List.initRow();
					while (EPV108005List.getNextRow()) {
						EPV108005Message convObj = (EPV108005Message) EPV108005List.getRecord();
%>
		          		<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE05COLACC())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE05COBERT())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE05DESCRI())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCCY(convObj.getE05COLAMT())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE05TASVIG())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE05SEGURO())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE05PROACC())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE05DSCCRE())%></td>
						</tr>
<% 
					} 
%>
				</table>
        	</td>
        </tr>
	</table>
<%
	} 
	
	if (!EPV108006List.isEmpty()) { 
%>	
	<h4>Cr&eacute;ditos Hipotecarios Morosos</h4>
	<%row = 0;%>

  	<table  class="tableinfo">
    	<tr bordercolor="#FFFFFF"> 
      		<td nowrap> 
				<table id="headTable" width="100%">
					<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
						<th align="center" nowrap width="15%">N�mero</th>
						<th align="center" nowrap width="10%">Cuota</th>
						<th align="center" nowrap width="20%">Vencimiento</th>
						<th align="center" nowrap width="15%">D&iacute;as Mora</th>
						<th align="center" nowrap width="10%">Tramo</th>
						<th align="center" nowrap width="10%">Monto Abonado</th>			
						<th align="center" nowrap width="10%">Deuda Actual</th>
					</tr>
<%
					EPV108006List.initRow();
					while (EPV108006List.getNextRow()) {
						EPV108006Message convObj = (EPV108006Message) EPV108006List.getRecord();
%>
		          		<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE06EXDACC())%></td>
							<td nowrap align="center" width="10%"><%=convObj.getE06EXDPNU()%></td>
							<td nowrap align="center" width="10%"><%=Util.formatDate(convObj.getE06EXDPDD(), convObj.getE06EXDPDM(), convObj.getE06EXDPDY())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE06DIAMOR())%></td>
							<td nowrap align="center" width="10%"><%=Util.formatCell(convObj.getE06TRAMOO())%></td>
							<td nowrap align="right" width="10%"><%=Util.formatCCY(convObj.getE06MTOABO())%></td>
							<td nowrap align="right" width="10%"><%=Util.formatCCY(convObj.getE06EXDPPD())%></td> 
						</tr>
<% 
					} 
%>
				</table>
        	</td>
        </tr>
	</table>
<%
	} 
%>


</form>
</body>
</html>