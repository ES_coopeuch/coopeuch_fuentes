package datapro.eibs.salesplatform;

/**
 * Registros  de Cheques a Terceros 
 * Archivo de Cheques a terceros para plataforma de venta
 * @author evargas
 */
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.coopeuch.creditos.predictorriesgo.sinacofi2.DatosConsultaSum;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenAntecedentesComercialesResponse;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiPortType;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiSOAPProxy;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiTypeType;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100502Message;
import datapro.eibs.beans.EPV114001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSEPV1140 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int R_TITULARS_LIST = 100;
	protected static final int A_TITULARS_LIST = 101;
	protected static final int R_TITULARS_NEW = 200;
	protected static final int R_TITULARS_MAINT = 201;
	protected static final int R_TITULARS_DELETE = 202;	
	protected static final int R_TITULARS_INQUIRY = 203;
	protected static final int A_TITULARS_MAINT = 600;
	
	protected static final int R_TITULARS_DATA_AVAL =700;
	protected static final int R_TITULARS_MAXIMO_AVALAR =750;		
	protected static final int R_TITULARS_SINACOFI = 850;		
	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_TITULARS_LIST:
			procReqTablaTitularsList(user, req, res, session);
			break;
		case A_TITULARS_LIST:
			procActionTablaTitularsList(user, req, res, session, null);
			break;
		case R_TITULARS_NEW:
			procReqTablaTitulars(user, req, res, session, "NEW");
			break;
		case R_TITULARS_MAINT:
			procReqTablaTitulars(user, req, res, session, "MAINTENANCE");
			break;
		case R_TITULARS_INQUIRY:
			procReqTablaTitulars(user, req, res, session, "INQUIRY");
			break;
		case A_TITULARS_MAINT:
			procActionMaintenance(user, req, res, session);
			break;
		case R_TITULARS_DELETE:
			procReqDelete(user, req, res, session);
			break;
		case R_TITULARS_DATA_AVAL:
			procReqTitularsDataAval(user, req, res, session);
			break;
		case R_TITULARS_MAXIMO_AVALAR:
			procReqTitularsMaximoAval(user, req, res, session);
			break;										
		case R_TITULARS_SINACOFI:
			procReqTitularsSinacofi(user, req, res, session);
			break;			
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}


	/**
	 * procReqTablaSEGUROSList
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTablaTitularsList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: EPV1140_titulars_enter_search.jsp");
			forward("EPV1140_titulars_enter_search.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

	/**
	 * procActionTablaTitularsList: Muestra los diferentes Cheques a terceros que posee Un cliente	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionTablaTitularsList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String option)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		boolean evaluation = false;
		
		try {
			mp = getMessageProcessor("EPV1140", req);

			EPV114001Message msg = (EPV114001Message) mp.getMessageRecord("EPV114001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());
			
			msg.setE01PVTCUN(req.getParameter("E01PVTCUN").trim());
			msg.setE01PVTNUM(req.getParameter("E01PVTNUM").trim());

			userPO.setCusNum(req.getParameter("E01PVTCUN").trim());
			userPO.setHeader23(req.getParameter("E01PVTNUM").trim());
			
			//Sends message
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
 
			session.setAttribute("userpPO", userPO);
			session.setAttribute("EPV114001List", list);
			forwardOnSuccess("EPV1140_titulars_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqTablaseguros: This Method show a single  Tabla Seguros either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTablaTitulars(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("EPV1140", req);
			userPO.setPurpose(option);
			
			//Creates the message with operation code depending on the option
			EPV114001Message msg = (EPV114001Message) mp.getMessageRecord("EPV114001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01TIMSYS(getTimeStamp());
			if (option.equals("NEW")) {
				msg.setH01OPECOD("0001");
			} else if (option.equals("MAINTENANCE")) {
				msg.setH01OPECOD("0002");
				msg.setE01PVTSEQ(req.getParameter("E01PVTSEQ").trim());
			} else {
				msg.setH01OPECOD("0004");
				msg.setE01PVTSEQ(req.getParameter("E01PVTSEQ").trim());
			}
			
			//Sets the number for maintenance and inquiry options
			msg.setE01PVTCUN(userPO.getCusNum());
			msg.setE01PVTNUM(userPO.getHeader23());

			//Send message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV114001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go to maintenance page
				flexLog("About to call Page: EPV1140_titulars_maintenance.jsp");
				if (option.equals("INQUIRY")) {
					// if the request is an inquiry sets the readOlnly attribute 'true'
					forward("EPV1140_titulars_maintenance.jsp?readOnly=true", req, res);
				} else {
					forward("EPV1140_titulars_maintenance.jsp", req, res);
				}
			} else {
				//if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1140_titulars_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procActionMaintenance
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1140", req);

			EPV114001Message msg = (EPV114001Message) mp.getMessageRecord("EPV114001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0005");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV114001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				req.setAttribute("ACT","S");
				forward("EPV1140_titulars_maintenance.jsp", req, res);
			} else {
				//if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				forward("EPV1140_titulars_maintenance.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqDelete
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDelete(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("MAINTENANCE");

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1140", req);
			
			//Creates message with the 'Delete'operation code
			EPV114001Message msg = (EPV114001Message) mp.getMessageRecord("EPV114001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0009");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets required values
			msg.setH01SCRCOD("01");
			//Sets the Code for delete options
			msg.setE01PVTCUN(userPO.getCusNum());
			msg.setE01PVTNUM(userPO.getHeader23());
			msg.setE01PVTSEQ(req.getParameter("E01PVTSEQ").trim());
			
			//Send message
			mp.sendMessage(msg);

			//Receive Error and Data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV114001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				//If there are no errors request the list again
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1140?SCREEN=101&E01PVTCUN="+ userPO.getCusNum()+"&E01PVTNUM="+ userPO.getHeader23(), res);
			} else {
				//if there are errors show the list without updating
				session.setAttribute("error", msgError);
				forward("EPV1140_titulars_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procReqTitularsDataAval(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1140", req);

			EPV114001Message msg = (EPV114001Message) mp.getMessageRecord("EPV114001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0007");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV114001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("error", msgError);					
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);
			
			forward("EPV1140_titulars_maintenance.jsp", req, res);
			
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procReqTitularsMaximoAval(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1140", req);

			EPV114001Message msg = (EPV114001Message) mp.getMessageRecord("EPV114001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0008");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV114001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("error", msgError);					
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);
			req.setAttribute("CALCULAR", "S");
			forward("EPV1140_titulars_maintenance.jsp", req, res);
			
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	/**
	 * Metodo que invoca el webservice de sinacofi expuesto por coopeuch
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTitularsSinacofi(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			
			EPV114001Message msg = (EPV114001Message) session.getAttribute("datarec");
			setMessageRecord(req, msg);		
			//TODO: comentar, solo temporal.
			msg.setE01PVTCED("N");
			
			try {
				String rut_s = msg.getE01PVTIDE().substring(0, msg.getE01PVTIDE().length()-1);
				String dv_s = msg.getE01PVTIDE().substring(msg.getE01PVTIDE().length()-1);
				BigInteger rut = new BigInteger(rut_s);
				String serie = msg.getE01PVTIDS();				
				
				String rutEmp_s = msg.getE01PVTCPR().substring(0, msg.getE01PVTCPR().length()-1);
				String dvEmp_s = msg.getE01PVTCPR().substring(msg.getE01PVTCPR().length()-1);				
				BigInteger rutEmp = new BigInteger(rutEmp_s);
				
				DatosConsultaSum parameters = new DatosConsultaSum();			
				parameters.setRut(rut);
				parameters.setDv(dv_s);
				parameters.setSerie(serie);
				parameters.setRutEmpleador(rutEmp);
				parameters.setDvEmpleador(dvEmp_s);
				parameters.setConsultas("A3,A2,A1,B");	
		
				ResumenSinacofiSOAPProxy wsp = new ResumenSinacofiSOAPProxy();//inicializamos el servicio					
				ResumenAntecedentesComercialesResponse respWrapper  = wsp.resumenAntecedentesComerciales(parameters);
				if (respWrapper.getCodError()==null || "0".equals(respWrapper.getCodError()) 
						|| "".equals(respWrapper.getCodError().trim())){//no hay errores..
				ResumenSinacofiTypeType resp = respWrapper.getResumenSinacofiType();

					//seteamos las variables que responde
					msg.setE01PVTU12(new BigDecimal(resp.getCantidadProtestoAnual())); //Inf - cantidad protesto 12 meses
					msg.setE01PVT12A(new BigDecimal(resp.getCantidadProtestoAnual())); //Ajus			
					msg.setE01PVTU6I(new BigDecimal(resp.getCantidadProtestoSemestral())); //Inf cantidad protesto 6 meses
					msg.setE01PVTU6A(new BigDecimal(resp.getCantidadProtestoSemestral())); //Ajus	
					msg.setE01PVTMOT(new BigDecimal(resp.getMontoMoraTotal())); //Inf - monto de mora total					
					msg.setE01PVTMOA(new BigDecimal((resp.getMontoMoraTotal()))); //Ajus
					msg.setE01PVTPRO(new BigDecimal(resp.getMontoTotalProtesto())); //Inf 																			
					msg.setE01PVTPRA(new BigDecimal(resp.getMontoTotalProtesto())); //Ajus	
					msg.setE01PVTSCO(Integer.toString(resp.getScore()));
					String dscCed = (resp.getEstadoCedula()==null)?"NO RESPONSE":resp.getEstadoCedula();
					msg.setE01DSCCED(dscCed); //descripcion del edo cedula // codigo de 30
					msg.setE01PVTCED((resp.getCedulaVigente().length()>0)?resp.getCedulaVigente().substring(0,1):resp.getCedulaVigente()); //solo primera letra
					String f = resp.getFechaVencimientoCedula();//dd-mm-yyyy
					if (f!=null && !"".equals(f.trim())){						
						msg.setE01PVTVCD(f.substring(0,2));
						msg.setE01PVTVCM(f.substring(3,5));			
						msg.setE01PVTVCY(f.substring(6));
					}
				}else{
					String c = respWrapper.getCodError();
					c =(c==null)?"N/D":c; 
					String d = respWrapper.getDesError();
					d =(d==null)?"SIN RESPUESTA SERVICIO BUREAU":d;					
					ELEERRMessage msgError = new ELEERRMessage();
	                msgError.setERRNUM("1");
	                msgError.setERNU01((c.length() >4?(c.substring(0,4)):c));		//4		                
	                msgError.setERDS01((d.length() >70?(d.substring(0,70)):d));		//70
					session.setAttribute("error", msgError);									
				}
							
			}catch (Exception e1){
				e1.printStackTrace();
				String d = e1.getMessage();
				d=(d==null)?d=e1.toString():d;					
				//si hay error mandarlos en la pantalla
                ELEERRMessage msgError = new ELEERRMessage();
                msgError.setERRNUM("1");
                msgError.setERNU01("01");
                msgError.setERDS01((d.length() >70?(d.substring(0,70)):d));
				session.setAttribute("error", msgError);			
			}		
			req.setAttribute("SINACOFI", "S");			

			session.setAttribute("datarec", msg);// seteamos nuevamente con la informacion obtenida
			forward("EPV1140_titulars_maintenance.jsp", req, res);

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}	

}


