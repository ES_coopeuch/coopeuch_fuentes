<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields,datapro.eibs.beans.EPV201004Message,datapro.eibs.beans.EPV201010Message,datapro.eibs.beans.EPV201001Message,datapro.eibs.beans.EPV170001Message,datapro.eibs.sockets.MessageRecord"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<html> 
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="EPV201004resp" class="datapro.eibs.beans.EPV201004Message"  scope="session" />
<jsp:useBean id="EPV170001List" class="datapro.eibs.beans.JBList"  scope="session" />


<%EPV201001Message platfObj01 = null;
  EPV201010Message platfObj10 = null;
  MessageRecord newmessage = (MessageRecord)session.getAttribute("platfObj"); 
  if (newmessage instanceof EPV201001Message) {
  	platfObj01 = (EPV201001Message)newmessage;
  }else{
 	platfObj10 = (EPV201010Message)newmessage;
  }
%>

<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />
<%EPV201004Message aval = (EPV201004Message)session.getAttribute("EPV201004respAval"); %>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

//  Process according with user selection
 function goAction(opcion) {
	document.forms[0].APR.value = opcion;
	document.forms[0].submit();
 }

 function goActionRegresar() {
	document.forms[0].SCREEN.value = '202';
	document.forms[0].submit();
 }
 
 
 
 </script>
</head>
         

<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>

<h3 align="center">
RESPUESTA DEL MOTOR<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_respuesta_motor.jsp,JSEPV2010"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV2010">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="900">
  <INPUT TYPE=HIDDEN NAME="APR" VALUE="">
  <INPUT TYPE=HIDDEN NAME="APR2" VALUE="<% if(EPV201004resp.getH04FLGWK1().equals("A"))  out.print("Z"); else out.print("0");%>">  
  <input type=HIDDEN name="customer_number"  value="<%= userPO.getCusNum()%>">
  <input type=HIDDEN name="E02PVMBNK"  value="<%= currUser.getE01UBK().trim()%>">  
  
  <%-- only click in Back (Regresar) --%>
  <INPUT TYPE=HIDDEN NAME="E01PVMNUM" VALUE="<%=(platfObj01!=null)?platfObj01.getE01PVMNUM():platfObj10.getE10PVMNUM()%>">

 <% int row = 0;%>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="20%">
             	<%=(platfObj01!=null)?platfObj01.getE01PVMCUN():platfObj10.getE10PVMCUN()%>	  			
             </td>
             <td nowrap align="right" width="20%"> Nombre :</td>
             <td nowrap align="left" width="40%">
             	<%=(platfObj01!=null)?platfObj01.getE01CUSNA1():platfObj10.getE10CUSNA1()%>             	  			
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="20%">
				<%=(platfObj01!=null)?platfObj01.getE01PVMNUM():platfObj10.getE10PVMNUM()%>              
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="40%">
				<%=(platfObj01!=null)?platfObj01.getE01PVMOPD():platfObj10.getE10PVMOPD()%>/
				<%=(platfObj01!=null)?platfObj01.getE01PVMOPM():platfObj10.getE10PVMOPM()%>/
				<%=(platfObj01!=null)?platfObj01.getE01PVMOPY():platfObj10.getE10PVMOPY()%>				             
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Sucursal :</td>
             <td nowrap align="left" width="20%">
				<%=(platfObj01!=null)?platfObj01.getE01PVMBRN():platfObj10.getE10PVMBRN()%>             
	  			
             </td>
             <td nowrap align="right" width="20%"> Ejecutivo :</td>
             <td nowrap align="left" width="40%">
				<%=(platfObj01!=null)?platfObj01.getE01PVMOFC():platfObj10.getE10PVMOFC()%>             
             </td>
         </tr>         
        </table>
      </td>
    </tr>
  </table>

  <h4>Resultado de Evaluaci�n Cliente</h4>
  <table class="tableinfo" width="100%">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="30%"> 
              <div align="right">Decisi�n :</div>
            </td>
            <td nowrap width="70%">
            	<%String decision ="Respuesta NO Esperada";
            	  if (!"".equals(EPV201004resp.getE04CODEC1())){
            	  		decision=EPV201004resp.getE04DSCDE1();//RECHAZADO
            	  }else{
            	 		decision=EPV201004resp.getE04DSCDE2();
            	  }
            	 %>
            	<B><%=decision%></B>
			</td>
          </tr>          
        </table>
        </td>
    </tr>
  </table> 
  
<%
	if (!"".equals(EPV201004resp.getE04CRSE01())) { 
%>

 <h4>Motivos de Rechazos</h4>
  <table class="tableinfo" width="100%">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Codigo :</div>
            </td>
            <td nowrap width="80%">
				<div align="left">Descripcion :</div> 
			</td>
          </tr>
          <%
          	for (int i = 1; i <= 20; i++) {
          		String fila = (i>9)?""+i:"0"+i;
				String cod = EPV201004resp.getField("E04RESC"+fila).getString();
				String dec = EPV201004resp.getField("E04DESR"+fila).getString();
				if (!"".equals(cod)){
			%>
			          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			            <td nowrap> 
			              <div align="right"><%=cod %></div>
			            </td>
			            <td nowrap >
							<div align="left"><%=dec%></div> 
						</td>
			          </tr>
			<%
				}				
			}
		   %>
          
        </table>
        </td>
    </tr>
  </table>
  
 <%
 	} 
 %> 
 
 <h4>Motivos de Rechazos Comerciales</h4>
  <table class="tableinfo" width="100%">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%="trclear"%>"> 
            <td nowrap width="20%"> 
              <div align="right"></div>
            </td>
            <td nowrap width="80%">
				<div align="left">Descripci�n :</div> 
			</td>
          </tr>
          <%
          	if ( EPV170001List.getNoResult() ) {
			%>
			          <tr id="<%="trdark" %>"> 
			            <td nowrap>			              
			            </td>
			            <td nowrap >
							<div align="left"></div> 
						</td>
			          </tr>
			<%
								
			}else{
			EPV170001List.initRow();
                while (EPV170001List.getNextRow()) {
                    if (EPV170001List.getFlag().equals("")) {
           %>  
           			 <tr id="<%="trdark" %>"> 
			            <td nowrap>			              
			            </td>
			            <td nowrap >
							<div align="left"><%out.println(EPV170001List.getRecord()); %></div> 
						</td>
			          </tr>       
                    		
           <% 
                    }
                }
		   %>
		   
		   
          <%} 
          %>
        </table>
        </td>
    </tr>
  </table>

<%   //Mostramos datos del AVAL..
	  if (aval!=null) {
	  //tiene aval
%>
  <h4>Resultado de Evaluaci�n Aval</h4>
  <table class="tableinfo" width="100%">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="30%"> 
              <div align="right">Decisi�n :</div>
            </td>
            <td nowrap width="70%">
            	<%String decisionaval ="Respuesta NO Esperada";
            	  if (!"".equals(aval.getE04CODEC1())){
            	  		decisionaval=aval.getE04DSCDE1();//RECHAZADO
            	  }else{
            	 		decisionaval=aval.getE04DSCDE2();
            	  }
            	 %>
            	<B><%=decisionaval%></B>
			</td>
          </tr>          
        </table>
        </td>
    </tr>
  </table> 
<%
	if (!"".equals(aval.getE04CRSE01())) { 
%>

 <h4>Motivos de Rechazos Aval</h4>
  <table class="tableinfo" width="100%">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Codigo :</div>
            </td>
            <td nowrap width="80%">
				<div align="left">Desccripcion :</div> 
			</td>
          </tr>
          <%
          	for (int i = 1; i <= 20; i++) {
          		String fila = (i>9)?""+i:"0"+i;
				String cod = aval.getField("E04RESC"+fila).getString();
				String dec = aval.getField("E04DESR"+fila).getString();
				if (!"".equals(cod)){
			%>
			          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			            <td nowrap> 
			              <div align="right"><%=cod %></div>
			            </td>
			            <td nowrap >
							<div align="left"><%=dec%></div> 
						</td>
			          </tr>
			<%
				}				
			}
		   %>
          
        </table>
        </td>
    </tr>
  </table> 
  
 <%
 	} 
 %> 
   
<%	  
	  }
 %>	  
   <div align="center">
   	<% 
	  if ("".equals(EPV201004resp.getE04CODEC1()) || (aval!=null && "".equals(aval.getE04CODEC1())) ){//no viene rechazado por politicas duras.
	%>
 		<input id="EIBSBTN" type=button name="Aceptar" value="Aceptar"    onclick="goAction('E');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
	<%	  
	  }
	 %>      
	   <input id="EIBSBTN" type=button name="Rechazar" value="Rechazar"  onclick="goAction('R');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   
	   <input id="EIBSBTN" type=button name="Regresar" value="Regresar"  onclick="goActionRegresar();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      	                         
   
   </div>
                     
  </form>
</body>
</HTML>
