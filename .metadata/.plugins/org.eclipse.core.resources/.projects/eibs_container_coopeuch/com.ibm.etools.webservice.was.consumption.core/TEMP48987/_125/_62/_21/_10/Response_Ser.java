/**
 * Response_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public class Response_Ser extends com.ibm.ws.webservices.engine.encoding.ser.BeanSerializer {
    /**
     * Constructor
     */
    public Response_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes,value,context));
        addElements(value,context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        Response bean = (Response) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_2;
          propValue = bean.getError();
          context.serialize(propQName, null, 
              propValue, 
              QName_3_5,
              true,null);
          propQName = QName_0_3;
          propValue = bean.getTasa();
          context.serialize(propQName, null, 
              propValue, 
              QName_3_6,
              true,null);
          propQName = QName_0_4;
          propValue = bean.getSalida();
          context.serialize(propQName, null, 
              propValue, 
              QName_3_4,
              true,null);
        }
    }
    private final static javax.xml.namespace.QName QName_3_6 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://10.21.62.125:8186/pricing/precio",
                  "Tasa");
    private final static javax.xml.namespace.QName QName_0_3 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "tasa");
    private final static javax.xml.namespace.QName QName_3_5 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://10.21.62.125:8186/pricing/precio",
                  "Excepcion");
    private final static javax.xml.namespace.QName QName_0_4 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "salida");
    private final static javax.xml.namespace.QName QName_0_2 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "error");
    private final static javax.xml.namespace.QName QName_3_4 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://10.21.62.125:8186/pricing/precio",
                  "salida");
}
