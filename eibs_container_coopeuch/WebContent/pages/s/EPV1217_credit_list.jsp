<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV121701Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV121701List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

   $(function(){
				
					$("#radio_key").attr("checked", false);
                
				});


  function goAction(op,index) {
	var ok = false;
	var cun = "";
	var pg = "";

  // document.forms[0].elements[index].checked = true;
 //document.forms[0].elements[index].checked = true;
//$("input[name=TPRTPR][value=" + index + "]").prop('checked', true); 
	if (op != '200'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "TPRTPR") 
	      	{	
	      		if (element.checked == true) {
	      			document.getElementById("E01LISCOD").value = element.value; 
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = true;
      	
      	if (op =='202'){
      		confirm1 = confirm("Desea Eliminar este Registro Seleccionado?");
      	}
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		

     } else {
		alert("Debe seleccionar un registro para continuar.");	   
	 }    
	}







function goAction2(op,index) {
	var ok = false;
	var cun = "";
	var pg = "";

  // document.forms[0].elements[index].checked = true;
 document.forms[0].elements[index].checked = true;
$("input[name=TPRTPR][value=" + index + "]").prop('checked', true); 
	if (op != '200'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "TPRTPR") 
	      	{	
	      		if (element.checked == true) {
	      			document.getElementById("E01LISCOD").value = element.value; 
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {	
      	
	
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
			

       
	}
	
	}




</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Mantenedor de Par�metros de Cr�dito Listo<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PRDRO_list.jsp,EPV1216"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1217">
<input type="hidden" name="SCREEN" value="201">
<input type="hidden" name="E01LISCOD" value="" id="E01LISCOD">  

 
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="20%"> 
			<a href="javascript:goAction('200')"><b>Crear</b></a>
		</td>
		<td align="center" class="tdbkg" width="20%">
			<a href="javascript:goAction('201')"> <b>Modificar</b> </a>
		</td>
		<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('202')"> <b>Borrar</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>

<%
	if (EPV121701List.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>

	<table id="headTable"  width="100%" align="left">
		<tr id="trdark">
			<th align="center" nowrap width="30"></th>
			<th align="center" nowrap width="80">Codigo</th>
			<th align="center" nowrap width="300">Descripci�n</th>
			<th align="center" nowrap width="100">Valor</th>
			<th align="center" nowrap width=""></th>
					
		</tr>
		<%
			EPV121701List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EPV121701List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EPV121701Message pvprd = (EPV121701Message) EPV121701List.getRecord();
		%>
		<tr>
			
			<td nowrap><input type="radio" name="TPRTPR" id="radio_key" value="<%=pvprd.getE01LISCOD() %>" <%=chk%>/></td>
			<td nowrap align="right"><a href="javascript:goAction2('203','<%=pvprd.getE01LISCOD()%>');"><%=pvprd.getE01LISCOD()  %></a></td>
			<td nowrap align="left"><a href="javascript:goAction2('203','<%=pvprd.getE01LISCOD()%>');"><%=pvprd.getE01LISDES()  %></a></td>
			<td nowrap align="right"><a href="javascript:goAction2('203','<%=pvprd.getE01LISCOD()%>');">
			
			<% 
			if(pvprd.getE01LISFLG().equals("N")){
			out.print(pvprd.getE01LISCNU());
			
			}
			if(pvprd.getE01LISFLG().equals("A")){
		
			out.print(pvprd.getE01LISCAL());
				}
			%></a></td>																			   																		   
		    <td></td>
		

		</tr>
		<%
			}
		%>
	</table>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV121701List.getShowPrev()) {
					int pos = EPV121701List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1217?SCREEN=100&codNum="
									+ request.getAttribute("codNum")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV121701List.getShowNext()) {
					int pos = EPV121701List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1217?SCREEN=100&codNum="
									+ request.getAttribute("codNum")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>
</form>
</body>
</html>

