<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page import="datapro.eibs.beans.UserPos"%><html>

<jsp:useBean id= "tituloPlan" class= "java.lang.String"  scope="session" />

<jsp:useBean id= "ESD404001List" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "userPOAux" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<jsp:useBean id= "planSocio" class= "datapro.eibs.beans.ESD404001Message"  scope="session" />
<jsp:useBean id= "operation" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "servlet" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "paginaPS" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "IDuser" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "NOBuser" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "banderaCV" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "banderaLH" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "banderaPag" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "numCuentaCV" class= "java.lang.String"  scope="session" />


<html>
<head>
<title>Mantenedor Plan Socio</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<SCRIPT Language="Javascript">

function goCV() {
	self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0711?TYPE=RT&tipoProducto=CV&pagOperation=2&E01CUN=" + document.forms[0].E01CUN.value + "&E01IDN=" + document.forms[0].E01IDN.value + "&nombreCliente=" + document.forms[0].nombreCliente.value + "&operation=" +document.forms[0].operation.value;
}


function goCA() {
	self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0711?TYPE=04&tipoProducto=LA&pagOperation=2&E01CUN=" + document.forms[0].E01CUN.value + "&E01IDN=" + document.forms[0].E01IDN.value + "&nombreCliente=" + document.forms[0].nombreCliente.value + "&operation=" +document.forms[0].operation.value;
}

function goTDC(){
	self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1005?SCREEN=200&tipoProducto=TC&pagOperation=2&E01CUN=" + document.forms[0].E01CUN.value + "&E01IDN=" + document.forms[0].E01IDN.value + "&nombreCliente=" + document.forms[0].nombreCliente.value;                                       
}

//  Process according with user selection
 var bandera;	
 function goAction(op) {
	
	
	switch (op){
		// Validate & Write 
  		case 1:  {
    		document.forms[0].APPROVAL.value = 'N';
       		break;
        }
		// Validate and Approve
		case 2:  {
				if (document.forms[0].H01WK2.checked == true)
					document.forms[0].cancelarPlan.value = "S";
					
				document.forms[0].SCREEN.value="500";
				document.forms[0].E01PSTCP.disabled = false;
				document.forms[0].submit();
			
			break;
		}
		case 3:{
			self.window.location.href = "<%=request.getContextPath()%>/pages/background.jsp";
			break;
		}
	}	
}

function tienePlan(){
	if (document.forms[0].TIENEPLAN.value != "S"){
		alert("El socio NO tiene un Plan.");
		self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4040?SCREEN=1000";
	}
}

function activarProductos(){
	if (document.forms[0].TIENECV.value == "N"){
		var LA = document.getElementById("LA");
		var h1 = document.createElement("h1");
    	h1.innerHTML = '<div id = "LA" align="center" disabled><a href="javascript:nada()"><br><b>Cuenta Ahorro</b></a></div>';
    	LA.appendChild(h1);
    	
    	var TC = document.getElementById("TCA");
		var h2 = document.createElement("h2");
		h2.innerHTML = '<div id = "TC" align="center" disabled><a href="javascript:nada()"><br><b>Tarjeta de Credito</b></a></div>';
		TC.appendChild(h2);
		
	}else{
		var LA = document.getElementById("LA");
		var h1 = document.createElement("h1");
    	h1.innerHTML = '<div id = "LA" align="center" ><a href="javascript:goCA()"><br><b>Cuenta Ahorro</b></a></div>';
    	LA.appendChild(h1);
    	
    	var TC = document.getElementById("TCA");
		var h2 = document.createElement("h2");
		h2.innerHTML = '<div id = "TC" align="center"><a href="javascript:goTDC()"><br><b>Tarjeta de Credito</b></a></div>';
		TC.appendChild(h2);
	}
}


function nada(){
	return;
}

</SCRIPT>



</head>

<body bgcolor="#FFFFFF" onload="tienePlan();activarProductos();">

 

<h3 align="center">Mantenedor Plan Socio<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="plan_socio_new, ESD4040"  ></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4040" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
  <INPUT TYPE=HIDDEN NAME="APPROVAL" VALUE="N">
  <input type="hidden" name="E01FL5" size="2" maxlength="1" value="<%= client.getE01FL5().trim()%>">
  <input type="hidden" name="E01LGT" size="2" maxlength="1" value="<%= client.getE01LGT().trim()%>">
  
  
 
  <INPUT TYPE=HIDDEN NAME="tarejetaCredito" VALUE="N">
  <input type="hidden" name="PlanPago" size="2" maxlength="1" value="0">
  <input type="hidden" name="cuentaVista" size="2" maxlength="1" value="N">
  <input type="hidden" name="libretaAhorro" size="2" maxlength="1" value="N">
  <input type="hidden" name="TDC" size="2" maxlength="1" value="N">  
  <input type="hidden" name="E01PSPRODUC" size="2" maxlength="1" value="">
  <input type="hidden" name="contTD" size="10" maxlength="2" value="">  
  <input type="hidden" name="contLH" size="2" maxlength="2" value="">
  <input type="hidden" name=paginaPS value="<%= paginaPS %>" size=15 maxlength=9 >
  <input type="hidden" name=IDuser value="<%= IDuser %>" size=15 maxlength=9 >
  <input type="hidden" name=NOBuser value="<%= NOBuser %>" size=15 maxlength=9 >
  <input type="hidden" name=operation value="<%= operation %>" size=15 maxlength=9 >
 <input type="hidden" name=cancelarPlan value="N" size=15 maxlength=9 >  
 <input type="hidden" name="E01CUN" value="<%= userPO.getCusNum() %>" size=15 maxlength=9 >
 <input type="hidden" name="E01IDN" value="<%= userPO.getHeader2() %>" size=15 maxlength=9 >
  <input type="hidden" name="nombreCliente" value="<%= userPO.getHeader3() %>" size=15 maxlength=9 >
  <input type="hidden" name="servlet" value="<%= servlet %>" size=15 maxlength=9 >
  <input type="hidden" name="TIENEPLAN" value="<%= planSocio.getL01FILLE4() %>" size=15 maxlength=9 >
  <input type="hidden" name="numCuentaCV" value="<%= numCuentaCV %>" size=15 maxlength=9 >
  <h4 align="center">Datos del Socio</h4>
  <table border = "0" width="100%">
  <tr>
  	<td width="80%">
  	</td>
  	<td>
  	</td>
  	<td>
  	</td>
  	<td>
  	</td>
  	<td>
  	</td>
  	<td aling="right" ><img src="<%=request.getContextPath()%>/images/pr111.png" alt="Personas Relacionadas" align="bottom" border="0"> <%=planSocio.getE01PSCRE() %>
  	</td>
  	<td>
  	</td>
  	<td aling="right" ><img src="<%=request.getContextPath()%>/images/inv222.png" alt="Invitaciones" align="bottom" border="0"><%=planSocio.getE01PSCIN() %>
  	</td>
  </tr>
  </table>
  <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">Cliente: </td>
					<td nowrap width="12%" aling="left" > <%=userPOAux.getCusNum() %> </td>
						
					<td nowrap width="6%" aling="right">RUT: </td>
					<td nowrap width="14%" aling="left" > <%= userPOAux.getHeader2() %> </td>
					
					<td nowrap width="8%" aling="right">Nombre: </td>
					<td nowrap width="20%" aling="left" > <%=userPOAux.getHeader3() %> </td>
					
					<td nowrap width="8%" aling="right">Estado del Socio: </td>
					<% if (planSocio.getL01FILLE6().equals("S")) {%>
						<td nowrap width="20%" aling="left" > ACTIVO  </td>
					<% }else{ %>
						<td nowrap width="20%" aling="left" > INACTIVO  </td>
					<% } %>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
	<br>
	<h4 align="center">Datos Plan Asociado</h4>
	<table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">Id Plan: </td>
					<td nowrap width="12%" aling="left" > <%=planSocio.getE01PSAUX4() %> </td>
						
					<td nowrap width="8%" aling="right">Descripcion: </td>
					<td nowrap width="20%" aling="left" > PLAN SOCIO </td>
					
					<td nowrap width="8%" aling="right">Estado Plan: </td>
					<td nowrap width="14%" aling="left" > <%=planSocio.getE01PSNPL() %> </td>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
    <br>
    <h4 align="center">Productos Activos</h4>
    <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">Cuenta Coopeuch </td>
					<% //if( planSocio.getE01PSPACV().equals("S") || planSocio.getE01PSPACV().equals("s") ){
						if( planSocio.getE01PSPACV().equals("S")){
						%>
						<td nowrap name="planSocio" property="E01PSPACV" width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
						<input type="hidden" name="TIENECV" value="S" size=15 maxlength=9 >
						</td> 
					<% 
						
					}else{
						%>
					 	<td nowrap name="planSocio" property="E01PSPACV" width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" > 
					 	<input type="hidden" name="TIENECV" value="N" size=15 maxlength=9 >
					 	</td> 
					<% 
					}
					%>
						
					<td nowrap width="6%" aling="right">Cuenta Ahorro </td>
					<% //if(planSocio.getE01PSPALA().equals("S") || planSocio.getE01PSPALA().equals("s")){ 
					   if( planSocio.getE01PSPALA().equals("S")){
						%>
						<td nowrap name="planSocio" property="E01PSPALA" width="12%" aling="left" ><img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
						<input type="hidden" name="TIENELA" value="S" size=15 maxlength=9 >
						</td>
						
					<%
					} else {
					%>
						<td nowrap name="planSocio" property="E01PSPALA" width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" > 
						<input type="hidden" name="TIENELA" value="N" size=15 maxlength=9 >
						</td>
					<%} %>
					
					<td nowrap width="8%" aling="right">Tarjeta de Credito </td>
					<% if(planSocio.getE01PSPATC().equals("S")){ %>
						<td nowrap name="planSocio" property="E01PSPATC" width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >  
						<input type="hidden" name="TIENETC" value="S" size=15 maxlength=9 >
						</td>
					<%} else {
					%>
						<td nowrap name="planSocio" property="E01PSPATC" width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" > 
						<input type="hidden" name="TIENETC" value="N" size=15 maxlength=9 >
						</td>
					<%} %>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
    <br>
    <h4 align="center">Asociar Productos al Plan</h4>
    <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<% if(planSocio.getE01PSPACV().equals("S")){ 
					%>
						<td class=TDBKG width="30%"> 
							<div align="center" disabled = false><b>Cuenta Coopeuch</b></div>              
                    	</td>
					<% 
					}
					else{ %>
						<td class=TDBKG width="30%"> 
                    		<div align="center" ><a href="javascript:goCV()"><b>Cuenta Coopeuch</b></a></div>              
<!--<div align="center" disabled = false><b>Cuenta Coopeuch</b></div>  -->
                    	</td>
					<% 
					}%>
					
                    
                    <td class=TDBKG width="30%"> 
                    	<div id = "LA"></div>              
                    </td>
                    
           			<td class=TDBKG width="30%"> 
                    	<div id = "TCA"></div>              
                    </td>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
    <br>
    <h4>Cobro</h4>
    <table  class="tableinfo">
  		<tr bordercolor="#FFFFFF">
  			<td nowrap>
	  			<table cellspacing="0" cellpadding="2" width="100%" border="0">
 					<tr id="trclear">
  						<td nowrap width="40%">
  							<div align="right">Tipo de plan :</div>
  						</td>
  						<td nowrap width="20%" colspan="3">
  							<input type="text" name="E01PSTCP" size="6" maxlength="4" value="<%= planSocio.getE01PSTCP() %>" disabled/>
  							<input type="hidden" name="E01PSTCP1" value="N" size=15 maxlength=9 >
                  			<a href="javascript:GetCodeDescCNTIN('E01PSTCP','E01PSTCP1','60')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0" ></a>
                  		</td>
                  		<td width="20%">
  							<div align="right">Valor a Pagar :</div>
  						</td>
  						<td width="20%">
  							<%=planSocio.getE01PSVBP() %> UF
                  		</td>
  					</tr>
  				</table>
  			</td>
  		</tr>
  	</table>
  	
  	<br>
    <h4>Cerrar el Plan</h4>
    <table  class="tableinfo">
  		<tr bordercolor="#FFFFFF">
  			<td nowrap>
  				<table cellspacing="0" cellpadding="2" width="100%" border="0">
    				<tr id="trclear">
      					<td nowrap width="40%">
        					<div align="center"><input type="checkbox" name="H01WK2" value="1" >Si quiere cerrar el plan</div>
      					</td>
    				</tr>
  				</table>
  			</td>
  		</tr>
  	</table>
    


	<table width="100%">		
  	<tr>
<!--			<td width="15%">-->
<!--	  		  <div align="center"> -->
<!--	     		<input id="EIBSBTN" type="button" name="Submit" value="Imprimir" onClick="javascript:goAction(1);">-->
<!--	     	  </div>	-->
<!--	  		</td>-->
	  		<td width="15%">
	  		  <div align="center"> 
	     		<input id="EIBSBTN" type="button" name="Submit" value="Cancelar" onClick="javascript:goAction(3);">
	     	  </div>	
	  		</td>
			<td width="75%">
  		  		<div align="center">
				<input id="EIBSBTN" type="button" name="Submit2" value="Enviar" onClick="javascript:goAction(2);">
     	  	 	</div>	
	  		</td>
		
  	</tr>	
</table>
</form>
</body>
</html>

