/**
 * InfraccionesLaborales.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class InfraccionesLaborales  implements java.io.Serializable {
    private java.math.BigInteger id;  // attribute
    private java.math.BigInteger numDeudasPrev;
    private java.math.BigInteger numMultas;

    public InfraccionesLaborales() {
    }

    public java.math.BigInteger getId() {
        return id;
    }

    public void setId(java.math.BigInteger id) {
        this.id = id;
    }

    public java.math.BigInteger getNumDeudasPrev() {
        return numDeudasPrev;
    }

    public void setNumDeudasPrev(java.math.BigInteger numDeudasPrev) {
        this.numDeudasPrev = numDeudasPrev;
    }

    public java.math.BigInteger getNumMultas() {
        return numMultas;
    }

    public void setNumMultas(java.math.BigInteger numMultas) {
        this.numMultas = numMultas;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        InfraccionesLaborales history = (InfraccionesLaborales) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        InfraccionesLaborales other = (InfraccionesLaborales) obj;
        boolean _equals;
        _equals = true
            && ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId())))
            && ((this.numDeudasPrev==null && other.getNumDeudasPrev()==null) || 
             (this.numDeudasPrev!=null &&
              this.numDeudasPrev.equals(other.getNumDeudasPrev())))
            && ((this.numMultas==null && other.getNumMultas()==null) || 
             (this.numMultas!=null &&
              this.numMultas.equals(other.getNumMultas())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        InfraccionesLaborales history = (InfraccionesLaborales) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getNumDeudasPrev() != null) {
            _hashCode += getNumDeudasPrev().hashCode();
        }
        if (getNumMultas() != null) {
            _hashCode += getNumMultas().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
