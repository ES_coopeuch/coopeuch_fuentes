/**
 * FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.consultaequifax;

public class FaultMessage  implements java.io.Serializable {
    private cl.consultaequifax.Error errores;

    public FaultMessage() {
    }

    public cl.consultaequifax.Error getErrores() {
        return errores;
    }

    public void setErrores(cl.consultaequifax.Error errores) {
        this.errores = errores;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        FaultMessage history = (FaultMessage) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        FaultMessage other = (FaultMessage) obj;
        boolean _equals;
        _equals = true
            && ((this.errores==null && other.getErrores()==null) || 
             (this.errores!=null &&
              this.errores.equals(other.getErrores())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        FaultMessage history = (FaultMessage) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getErrores() != null) {
            _hashCode += getErrores().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
