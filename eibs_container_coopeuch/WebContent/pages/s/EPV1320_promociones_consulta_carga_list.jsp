<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV132002Message"%>
<%@page import="com.datapro.constants.EibsFields"%>

<html> 
<head> 
<title>Consulta de Carga de Promociones</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="ListPromo" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"   scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"    	   scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

$(function(){
$("#radio_key").attr("checked", false);
});

function goAction(op) {
var ok = false;
	
		for(n=0; n<document.forms[0].elements.length; n++)
		{
			var element = document.forms[0].elements[n];
	   	 if(element.name == "ID_Interfaz")  
	   	 {	
	   	 	if (element.checked == true) 
	   	 	{
	   	   		document.getElementById("codigo_lista").value = element.value; 
       	 		ok = true;
       	 		break;
			}
	 	 }
		}
      
    	if ( ok ) 
    	{
				document.forms[0].SCREEN.value = op;
				document.forms[0].submit();		
     	} 
     	else 
     	{
			alert("Debe seleccionar un registro para continuar.");	   
	 	}    
}

</SCRIPT>

</head>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>

<h3 align="center">
Consulta de Carga de Promociones	
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="_promociones_consulta_carga_list.jsp, EPV1320">
</h3>

<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1320">
<input type="hidden" name="SCREEN" value=""> 
<input type="hidden" name="codigo_lista" value="" id="codigo_lista"> 

<%
	if (ListPromo.getNoResult()) {
%>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center"><font size="3"> 
		<b> 
		No se encontraron promociones cargadas en el periodo 
		</b> </font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>

<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" >
			<a href="javascript:goAction('400')"><b>Consultar</b></a></td>
		</td>
	</tr>
</table>


<table class="tbenter" width=100%>
	<tr>
		<td height="20"></td>
	</tr>

	<tr>
		<td>
		<table id="headTable" width="100%" align="left">
			<tr id="trdark">
				<th align="left" nowrap width="30"></th>
				<th align="left" nowrap width="100">ID Interfaz</th>
				<th align="left" nowrap width="100">Fecha Carga</th>
				<th align="left" nowrap width="100">Reg Totales</th>
				<th align="left" nowrap width="100">Reg OK</th>
				<th align="left" nowrap width="100">Reg Error</th>
				<th align="left" nowrap width="100">Tipo</th>
				<th align="left" nowrap width="100">Estado</th>
				<th align="left" nowrap width="100">Usuario</th>
				<th align="left" nowrap width=""></th>
			</tr>

			<%
				ListPromo.initRow();

					while (ListPromo.getNextRow()) {

						EPV132002Message pvpro = (EPV132002Message) ListPromo.getRecord(); 
			%>
			<tr>
				<td nowrap>
					<input type="radio" name="ID_Interfaz"	id="codigo_lista" value="<%=ListPromo.getCurrentRow()%>"  />
				</td>
				<td nowrap align="center"><%=pvpro.getE02PVHIDC()%></td>
				<td nowrap align="center">
				<%
					out.print(pvpro.getE02PVHDCA() + "/" + pvpro.getE02PVHMCA()+ "/" + pvpro.getE02PVHYCA());
				%>
				</td>
				<td nowrap align="center"><%=pvpro.getE02PVHLEE()%></td>
				<td nowrap align="center"><%=pvpro.getE02PVHPRO()%></td>
				<td nowrap align="center"><%=pvpro.getE02PVHERR()%></td>
				<td nowrap align="left"><% if (pvpro.getE02PVHTPR().equals("F")) out.print("FULL"); else if (pvpro.getE02PVHTPR().equals("P")) out.print("PARCIAL"); else out.print("NO DEF");  %>
				</td>
				<td nowrap align="left"><% if (pvpro.getE02PVHINP().equals("C")) out.print("CARGADO"); else if (pvpro.getE02PVHINP().equals("P")) out.print("PROCESADO"); else out.print("NO DEF");  %>
				</td>
				<td nowrap align="left"><%=pvpro.getE02PVHLMU()%></td>
				<td nowrap align="left"></td>
			</tr>
			<%
				}
			%>
		</table>
		</td>
	</tr>
</table>

<%
	}
%> 
	<SCRIPT language="JavaScript">
		showChecked("CURRCODE");
		
			function resizeDoc() {
      		 	divResize();
     		    adjustEquTables(headTable, dataTable, dataDiv1,1,false);
      		}
	 		resizeDoc();   			
     		window.onresize=resizeDoc;        
     </SCRIPT>
     </form>
</body>
</html>
