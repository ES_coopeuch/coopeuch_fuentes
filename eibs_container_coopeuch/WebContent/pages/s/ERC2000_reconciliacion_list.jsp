<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page	language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@ page import = "datapro.eibs.master.Util, datapro.eibs.beans.ERC004002Message, datapro.eibs.beans.ERC004003Message" %>	
<html>
<head>
<title>Conciliaci�n Manual de Cartolas</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/Ajax.js"> </SCRIPT>

<style type="text/css"> 
	input { 
         text-align:right; 
    } 
</style> 

</head>

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "msg" class= "datapro.eibs.beans.ERC004001Message"  scope="session" />
<jsp:useBean id= "carList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "ibsList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<body>

<script type="text/javascript">

  $(function(){
     $("#rowcar").attr("checked", false);
  });
  
  
  </script>

<script language="JavaScript">
function goAction(op) 
  {
    var ok = false;

    for(n=0; n<document.forms[0].elements.length; n++)
	  {
	     var element = document.forms[0].elements[n];
	     if(element.name == "rowcar") 
	     {	
	        if (element.checked == true) {
	      	   document.getElementById("codigo_lista").value = element.value; 
        	   ok = true;
        	   break;
			}
	      }
	   }

     if ( ok ) 
     {
        var confirm1 = false;
      	
      	if (op =='900')
      	  {
      	   confirm1 = confirm("�Desea Eliminar la cartola seleccionada?");
  	   	   
  	   	   if (confirm1)
  	   	      {
			   document.forms[0].SCREEN.value = op;
			   document.forms[0].submit();		
	          }
          }

	   else 
  	      {
			   document.forms[0].SCREEN.value = op;
			   document.forms[0].submit();		
	      } 

     } 
     else 
		alert("Debe seleccionar un registro para continuar.");	   
  }




  function goAction2(op) 
   {
	  if (op =='1200')	
 	 {
 	 	document.forms[0].SCREEN.value = op;
  	 	document.forms[0].action = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC2000?SCREEN=200&E01RCHSTN=<%=request.getAttribute("E01Cartola")%>&E01RCHAPF=<%=request.getAttribute("estado")%>&E01BRMEID=<%=session.getAttribute("codigo_banco")%>&E01RCHTFC=<%=request.getParameter("E01RCHTFC") %>&E01BRMACC=<%=session.getAttribute("cuenta_ibs")%>&E01DSCRBK=<%=session.getAttribute("nombre_banco")%>&E01BRMCTA=<%=session.getAttribute("cuenta_banco")%>&E01RCHFSD=<%=request.getParameter("E01RCHFSD")%>&E01RCHFSM=<%=request.getParameter("E01RCHFSM")%>&E01RCHFSY=<%=request.getParameter("E01RCHFSY")%>&E01RCHFLD=<%=request.getParameter("E01RCHFLD")%>&E01RCHFLM=<%=request.getParameter("E01RCHFLM")%>&E01RCHFLY=<%=request.getParameter("E01RCHFLY")%>&posicion=<%=request.getParameter("E01RCHREC")%>";
	 	document.forms[0].submit();
	 }
   }

	
  	
</script>


<H3 align="center">Desconciliaci�n
	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="reconciliacion_list.jsp, ERC2000"></H3>
<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC2000">
	<INPUT TYPE="HIDDEN" NAME="SCREEN" VALUE="650">
	<input type="hidden" name="codigo_lista" value="" id="codigo_lista">

	<input type="hidden" name="E01RCHSTN" value="<%=request.getAttribute("E01Cartola") %>">
	<input type="hidden" name="E01RCHAPF" value="<%=request.getAttribute("estado") %>"> 
	<input type="hidden" name="E01BRMEID" value="<%=session.getAttribute("codigo_banco") %>"> 

	<input type="hidden" name="E01RCHTFC" value="<%=request.getParameter("E01RCHTFC") %>" >
	<input type="hidden" name="E01BRMACC" value="<%=session.getAttribute("cuenta_ibs")%>">
	<input type="hidden" name="E01DSCRBK" value="<%=session.getAttribute("nombre_banco") %>">
 	<input type="hidden" name="E01BRMCTA" value="<%=session.getAttribute("cuenta_banco")%>">
  
  	<input type="hidden" name="E01RCHFSD" value="<%=request.getParameter("E01RCHFSD") %>"> 
	<input type="hidden" name="E01RCHFSM" value="<%=request.getParameter("E01RCHFSM") %>"> 
	<input type="hidden" name="E01RCHFSY" value="<%=request.getParameter("E01RCHFSY") %>"> 

	<input type="hidden" name="E01RCHFLD" value="<%=request.getParameter("E01RCHFLD") %>"> 
	<input type="hidden" name="E01RCHFLM" value="<%=request.getParameter("E01RCHFLM") %>"> 
	<input type="hidden" name="E01RCHFLY" value="<%=request.getParameter("E01RCHFLY") %>"> 

	<input type="hidden" name="E01RCHREC" value="<%=request.getParameter("E01RCHREC")%>">


<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" >
          <tr>
             <td nowrap width="10%" align="right">   Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input align="left" type="text" name="codigo_banco" value="<% out.print(session.getAttribute("codigo_banco")+"-"+session.getAttribute("nombre_banco"));%>" 
	  			size="60"  readonly>
             </td>          
             <td nowrap width="10%" align="right">   Cartola : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input align="left" type="text" name="codigo_banco" value="<%=Util.formatCell(msg.getE01CARSTN())%>" 
	  			size="15"  readonly>
             </td>          

         </tr>
 
         <tr>
            <td nowrap width="10%" align="right"> Cuenta Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<%=session.getAttribute("cuenta_banco")%>" 
	  			size="30"  readonly>
             </td>
                    <td nowrap width="10%" align="right"> Cuenta IBS : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<%=session.getAttribute("cuenta_ibs")%>" 
	  			size="30"  readonly>
             </td>  
          </tr>
        </table>
      </td>
    </tr>
    <tr><td height="10" ></td></tr>
 
  </table>

  <p>&nbsp;</p>



<%
	if (carList.getNoResult()) {


%>

<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No existen movimientos para desconciliar. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} 
	
	else{
%>

	 <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0">

  		<tr height="40" valign="middle" class="tableinfo">
  			<th align="center" bgcolor="#CCCCCC"  rowspan="2"> 
  			<b>Transaciones de Cartola</b></th>
 			<th width="2%" align="center"></th>
  			<th align="center"  bgcolor="#CCCCCC" rowspan="2"><b>Transaciones del IBS</b></th>
  		</tr>
  		
  		<tr height="40" valign="middle" class="tableinfo" >
  		
  		<th width="2%" align="center"></th>
  			
  		</tr>
  		<tr>
  			<td width="50%">
				<table id="headTable1" class="tbhead" cellspacing=0 cellpadding=2 width="100%" border="0">
					<tr id="trdark">
						<th align="center" width="5%">&nbsp;</th>
							<th align="center" width="15%">ID</th>
						<th align="center" width="20%">Fecha</th>
						<th align="center" width="15%">Referencia</th>
						<th align="center" width="35%">Descripci�n</th>
						<th align="center" width="25%">D�bito</th>
						<th align="center" widt	h="25%">Cr�dito</th>
					</tr>
				</table>
  			</td>
  			<td>
  			</td>
  			<td width="17%" colspan="3">
				<table id="headTable2" class="tbhead" cellspacing=0 cellpadding=2 width="100%" border="0">
					<tr id="trdark">
					
					<th align="center" width="20%">ID</th>
						<th align="center" width="20%">Fecha</th>
						<th align="center" width="15%">Referencia</th>
						<th align="center" width="35%">Descripci�n</th>
						<th align="center" width="25%">D�bito</th>
						<th align="center" width="25%">Cr�dito</th>
					</tr>
				</table>
  			</td>
  		</tr>
  		<tr>
  			<td width="50%" align="center" valign="top">
  				<table  id="dataTable1" class="tableinfo" ALIGN=CENTER style="width:'100%'">
					<tr>    
						<td NOWRAP width="100%">
							 <div id="dataDiv1" class="scbarcolor" style="width:100%; height:230px; overflow:auto;">  
							 	<table id="car" width="100%">      
<%
						carList.initRow();
								
								boolean firstTime = true;
				String codigo = "";
			    String nbr = "";
			    int flg = 0;
			    String chk = "";
			    
								while (carList.getNextRow()) {
									ERC004003Message car = (ERC004003Message) carList.getRecord();

			if (firstTime) {
						firstTime = false;
						
						chk = "checked";
						codigo = car.getE03RCSCNU();
					
		
					} else {
						chk = "";
					}
		 
		 if (!codigo.equals(car.getE03RCSCNU())) {
               flg = 0; 
      
			codigo = car.getE03RCSCNU();
		 }


			%>
				

										<tr>
											<td align="center" width="5%">
													<%if ( flg == 0) { %>
											<input type="radio" id="rowcar" name="rowcar" value="<%=carList.getCurrentRow()%>"<%=chk%> >
														<% } %>
											
											</td>
											
												<td align="center" width="15%">
												<div><%=car.getE03RCSCNU()%></div>
											</td>
											
											<td align="center" width="15%">
												<div><%=Util.formatDate(car.getE03RCSSDD(), car.getE03RCSSDM(), car.getE03RCSSDY())%></div>
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatCell(car.getE03RCSCKN())%></div>
											</td>
											<td align="center" width="35%">
												<div><%=car.getE03RCSGLO() %></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCCY(car.getE03RCSAMD())%></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCCY(car.getE03RCSAMC())%></div>
											</td>
										</tr> 
										
										
<%									
							
								if ( flg == 0) {
			flg = 1;
			}	
							
								} 
%>
								</table>
							</div>  				
						</td>
					</tr>	
  				</table>
  			</td>
  			<td></td>
  			<td align="center" valign="top" width="17%" colspan="3">
  				<table  id="dataTable2" class="tableinfo" ALIGN=CENTER style="width:'100%'">
					<tr>    
						<td NOWRAP width="100%">
							 <div id="dataDiv2" class="scbarcolor" style="width:100%; height:230px; overflow:auto;">  
							 	<table id="ibs" width="100%">      
<%
								ibsList.initRow();
								while (ibsList.getNextRow()) {
									ERC004002Message ibs = (ERC004002Message) ibsList.getRecord();
%>
										<tr>
											<td align="center" width="5%">
					<div><%=Util.formatCell(ibs.getE02RCTCNU())%></div>
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatDate(ibs.getE02RCTBDD(), ibs.getE02RCTBDM(), ibs.getE02RCTBDY())%></div>
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatCell(ibs.getE02RCTCKN())%></div>
											</td>
											<td align="center" width="35%">
												<div><%=ibs.getE02RCTNAR() %></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCCY(ibs.getE02RCTAMD())%></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCCY(ibs.getE02RCTAMC())%></div>
											</td>
										</tr> 
<%									
								} 
							
%>
								</table>
							</div>  				
						</td>
					</tr>	
  				</table>
  			</td>
  		</tr>
  		<tr>
			
			<td height="39"></td>
		<td colspan="3" height="39">
		
		</td>
	</tr>
  		<tr height="12">
  		</tr>
  	</table>
  	
	
	 	<div align="center"> 
    	<input id="EIBSBTN" type=button name="Submit" value="Atr�s" onClick="javascript:goAction2('1200')">
    	<input id="EIBSBTN" type=button name="Submit" value="Enviar" onClick="javascript:goAction('650')">	
  	</div>
	
  <%} //fin else sin resultados %>
</form>
</body>
</html>