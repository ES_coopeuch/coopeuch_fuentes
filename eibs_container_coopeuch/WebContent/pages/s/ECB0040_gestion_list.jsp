<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page import ="datapro.eibs.master.Util" %>
<HTML>
<HEAD>
<TITLE>
Lista de Gestiones de Cobros
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "mtList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "userPO" 	class= "datapro.eibs.beans.UserPos"  		scope="session"/>
<jsp:useBean id= "clientInfo" class= "datapro.eibs.beans.ECB004001Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<SCRIPT language="javascript" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1">
  
function cerrarVentana(){
	window.open('','_parent','');
	window.close(); 
}

function showInq(num) {
	page = webapp + "/servlet/datapro.eibs.products.JSECB0040?SCREEN=300&CURRCODE=" + num;
	CenterWindow(page,600,500,2);
}	 	
</script>

</HEAD>

<BODY>


<h3 align="center">Gestion de Cobranzas
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="gestion_list.jsp,ECB0040">
</h3>
<hr size="4">
<FORM Method="post" Action="<%=request.getContextPath()%>/pages/s/ECB0040_gestion_enter_inq.jsp" >
<INPUT type=hidden name="OLDROW" value="">
<INPUT TYPE=HIDDEN NAME="CURRENTROW" VALUE="0"> 
 <% if (!userPO.getOption().equals("CL")) { %>
   <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="14%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="9%" > 
              <div align="left"> 
                <input type="text" name="E01GCMCUN" size="10" maxlength="9" readonly value="<%= userPO.getCusNum().trim()%>"readonly>
              </div>
            </td>
            <td nowrap width="12%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E01GCMNAM" size="45" maxlength="45" readonly value="<%= userPO.getCusName().trim()%>" readonly>
              </div>
            </td>
            <td nowrap > 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap ><b> 
              <input type="text" name="E01GCMPRO" size="4" maxlength="4" readonly value="<%= userPO.getProdCode().trim()%>" readonly>
              </b></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="14%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="9%"> 
              <div align="left"> 
                <input type="text" name="E01GCMACC" size="13" maxlength="12" value="<%= userPO.getAccNum().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="12%"> 
              <div align="right"><b>Oficial :</b></div>
            </td>
            <td nowrap width="33%"> 
              <div align="left"><b> 
                <input type="text" name="D01GCMOFC" size="45" maxlength="45" readonly value="<%= userPO.getHeader1().trim()%>" readonly>
                </b> </div>
            </td>
            <td nowrap width="11%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="21%"> 
              <div align="left"><b> 
                <input type="text" name="E01GCMCCY" size="3" maxlength="3" value="<%= userPO.getCurrency().trim()%>" readonly>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
 <% } else { %>
   <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="14%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="9%" > 
              <div align="left"> 
                <input type="text" name="E01GCMCUN" size="10" maxlength="9" readonly value="<%= userPO.getCusNum().trim()%>"readonly>
              </div>
            </td>
            <td nowrap width="12%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap  width="20%" > 
              <div align="left"> 
                <input type="text" name="E01GCMNAM" size="45" maxlength="45" readonly value="<%= userPO.getCusName().trim()%>" readonly>
              </div>
            </td>
            <td nowrap > </td>
            <td nowrap > </td>            
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <% } %> 
 
<%
	if ( mtList.getNoResult() ) {
%> 
  <TABLE class="tbenter" width=100% height=40%>
   	<TR>
      <TD> 
        <div align="center"> 
          <p>
            <b>No existen registros por gestiones de cobranzas.</b>
          </p>          
        </div>
      </TD>
     </TR>
   </TABLE>
<%   		
   }else { 
	
%> 
&nbsp; 
 <TABLE  id="mainTable" class="tableinfo" >
  <TR> 
    <TD NOWRAP valign=top width="100%">
  	 <TABLE id="dataTable" width="100%">
  		<TR id="trdark"> 
  			<TH ALIGN=CENTER >Cuenta</TH>
 			<TH ALIGN=CENTER >Secuencia</TH>
  			<TH ALIGN=CENTER >Fecha</TH>
    		<TH ALIGN=CENTER >Hora</TH>
    		<TH ALIGN=CENTER >Usuario</TH>
    		<TH ALIGN=CENTER >Cobrador</TH>  
    		<TH ALIGN=CENTER >Descripcion</TH>     		
  		</TR>
     <%
        mtList.initRow();            
        while (mtList.getNextRow()) {
           datapro.eibs.beans.ECB004001Message msgMT = (datapro.eibs.beans.ECB004001Message) mtList.getRecord();
     %>               
        <TR>
			<TD NOWRAP ALIGN="CENTER"><a href="javascript:showInq('<%= mtList.getCurrentRow() %>');"><%=msgMT.getE01GCMACC()%></a></TD>
			<TD NOWRAP ALIGN="CENTER"><a href="javascript:showInq('<%= mtList.getCurrentRow() %>');"><%=msgMT.getE01GCMSEQ()%></a></TD>			
			<TD NOWRAP ALIGN="CENTER"><a href="javascript:showInq('<%= mtList.getCurrentRow() %>');"><%=Util.formatDate(msgMT.getE01GCMFGD(),msgMT.getE01GCMFGM(),msgMT.getE01GCMFGY())%></a></TD>
			<TD NOWRAP ALIGN="CENTER"><a href="javascript:showInq('<%= mtList.getCurrentRow() %>');"><%=msgMT.getE01GCMTIM()%></a></TD>			
			<TD NOWRAP ALIGN="LEFT"><a href="javascript:showInq('<%= mtList.getCurrentRow() %>');"><%=Util.formatCell(msgMT.getE01GCMUSR())%></a></TD>
			<TD NOWRAP ALIGN="CENTER"><a href="javascript:showInq('<%= mtList.getCurrentRow() %>');"><%=Util.formatCell(msgMT.getE01GCMCOD() + "-" + msgMT.getE01GCMCNAM())%></a></TD>
			<TD NOWRAP ALIGN="CENTER"><a href="javascript:showInq('<%= mtList.getCurrentRow() %>');"><%= msgMT.getE01GCMMA0().trim()+ " " + msgMT.getE01GCMMA1().trim()%></a></TD>
		</TR>    		
    <%              
        }
    %>    
     </TABLE>
    </TD>
</TR>  
</TABLE>

<%   		
	} 
 %>
 
<p align="center"> 
<%if (!("S").equals(request.getAttribute("PC"))){//mostramos Cancelar si no viene de Posicion Cliente %>
	<input id="EIBSBTN" type=submit name="Submit" value="Salir">
<%}else{ %>
	<input id="EIBSBTN" type="button" name="cancel" value="Salir" onclick="cerrarVentana()">
<%} %>
</p>
</FORM>

</BODY>
</HTML>
