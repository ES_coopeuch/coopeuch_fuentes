<%@ page import="datapro.eibs.master.Util"%>
<%@ page import="cl.coopeuch.core.nexus.*"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<title>Movimientos No Facturados Tarjetas de Creditos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv ="Cache-Control" content ="no-cache"/>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="RespMov" class="cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">

</script>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) 
 {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Movimientos No Facturados<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="cc_mov_no_fact_list_nexus.jsp,ECC0150"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150">
<input type="hidden" name="SCREEN" value="201"> 
 <% int row = 0;%>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCUN" size="13" maxlength="12" readonly value="<%= userPO.getCusNum().trim()%>">                  
               </div>               
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre : </b></div>
            </td>
            <td nowrap width="20%"  > 
              <div align="left"> 
                <input type="text" name="E01CCMNME" size="35" maxlength="35" readonly value="<%= userPO.getCusName().trim()%>">                                 
              </div>
            </td>            
            <td nowrap width="16%" > 
              <div align="right"><b>Identif. Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCID" size="15" maxlength="15" readonly value="<%= userPO.getIdentifier().trim()%>">                
               </div>               
            </td>            
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta IBS : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCMACC" size="13" maxlength="12" readonly value="<%= userPO.getAccNum().trim() %>">                                 
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMCCY" size="5" maxlength="4" readonly value="<%= userPO.getCurrency().trim() %>">                              
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Oficial : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMOFC" size="5" maxlength="4" readonly value="<%= userPO.getOfficer().trim() %>">                               
                </b> </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
          <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMPRO" size="5" maxlength="4" readonly value="<%= userPO.getProdCode().trim() %>">                  
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Desc. Producto : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="D01CCMPRO" size="35" maxlength="35" readonly value="<%= userPO.getHeader20().trim()%>">                              
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Nro. Cuenta : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCRNXN" size="20" maxlength="20" readonly value="<%= userPO.getHeader21().trim() %>">                              
              </div>
            </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>

 <TABLE class="tbenter" width="100%">
	<TR>
	<TD ALIGN=CENTER class=TDBKG><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
	</TD>
	</TR>
</TABLE>

 <%			
 
 int nTrans = RespMov.getTransacciones_c();
 
 if (nTrans <= 0) {

%>
<TABLE class="tbenter" width=100% height=30%>
	<TR>
		<TD>
		<div align="center"><font size="3"><b> No hay resultados que correspondan a su criterio de b�squeda. </b></font></div>
		</TD>
	</TR>	
</TABLE>
<%
	} else {
%>
<TABLE  class="tableinfo" width="100%">
	<TR>
		<TD>
		 <table cellpadding=0 cellspacing=0 width="100%" border=0>		
			<TR id="trclear">
				<td align="center" rowspan=0 ><h3>Movimientos Nacionales sin Facturar</h3></Td>			
			</TR>
		</TABLE>
		</TD>
	</TR>

	<TR>
		<TD>
		 <table border=0 CELLPADDING=0 CELLSPACING=0 width="100%">
				<TR id="trclear">
				<td width="25%" ALIGN=CENTER nowrap><b>No Documento</b></Td>
				<td width="20%" ALIGN=CENTER nowrap><b>Fecha</b></Td>
				<td width="35%" ALIGN=CENTER nowrap><b>Descripcion</b></Td>	
				<td width="20%" ALIGN=CENTER nowrap><b>Monto $</b></Td>			
			</TR>
			<%
			ListaTransacciones Lista = null;
			Lista = RespMov.getTransacciones();
			
			nTrans = RespMov.getTransacciones_c();
			RECORD registro = null;
			String FechaMov = "";
			
			for(int ciclo=0; ciclo<nTrans; ciclo=ciclo +1)
				{
		            registro = Lista.getRECORD(ciclo);					
					if(registro.getOrigen_trans().equals("NAC"))
					{	
					FechaMov = Integer.toString(registro.getFec_trans());
					
			%>
						<tr id="trdark">
							<td nowrap align="left"><%=registro.getMicrofilm23()%></td>
							<td nowrap align="center"><%=((FechaMov.substring(6,8).concat("/")).concat(FechaMov.substring(4,6).concat("/"))).concat(FechaMov.substring(0,4))%></td>		
							<td nowrap align="left"><%=registro.getGlosa_trans()%></td>				
							<td nowrap align="right"><%=Util.formatCCY(registro.getMonto_compra())%></td>			
						</tr>
			<%
					}
				}
			%>
		</TABLE>
    </TD>
</TR>
</TABLE>

<br>
<TABLE  class="tableinfo" width="100%">
	<TR>
		<TD>
		 <table cellpadding=0 cellspacing=0 width="100%" border=0>		
			<TR id="trclear">
				<td align="center" rowspan=0 ><h3>Movimientos Internacionales sin Facturar</h3></Td>			
			</TR>
		</TABLE>
		</TD>
	</TR>
	<TR>
		<TD>
		 <table border=0 CELLPADDING=0 CELLSPACING=0 width="100%">
				<TR id="trclear">
				<td width="20%" ALIGN=CENTER nowrap><b>No Documento</b></Td>
				<td width="10%" ALIGN=CENTER nowrap><b>Fecha</b></Td>
				<td width="30%" ALIGN=CENTER nowrap><b>Descripcion</b></Td>	
				<td width="15%" ALIGN=CENTER nowrap><b>Ciudad</b></Td>		
				<td width="5%" ALIGN=CENTER nowrap><b>Pais</b></Td>
				<td width="10%" ALIGN=CENTER nowrap><b>Monto<br> Moneda Origen</b></Td>				
				<td width="10%" ALIGN=CENTER nowrap><b>Monto USD$</b></Td>			
			</TR>
			<%
			for(int ciclo=0; ciclo<nTrans; ciclo=ciclo +1)
				{
		            registro = Lista.getRECORD(ciclo);					
					if(registro.getOrigen_trans().equals("EXT"))
					{	
					FechaMov = Integer.toString(registro.getFec_trans());
					
			%>
				<tr id="trdark">
				<td nowrap align="left"><%=registro.getMicrofilm23()%></td>
				<td nowrap align="center"><%=((FechaMov.substring(6,8).concat("/")).concat(FechaMov.substring(4,6).concat("/"))).concat(FechaMov.substring(0,4))%></td>			
				<td nowrap align="left"><%=registro.getGlosa_trans()%></td>				
				<td nowrap align="left"><%=registro.getCiudad()%></td>
				<td nowrap align="center"><%=registro.getCodigo_pais_iata()%></td>	
				<td nowrap align="right"><%=Util.formatCCY(registro.getMonto_mon_origen())%></td>
				<td nowrap align="right"><%=Util.formatCCY(registro.getMonto_compra())%></td>																					
			</tr>
			<%
			    }
				}
			%>
		</TABLE>
    </TD>
</TR>
</TABLE>
<%
	}
%>
</form>
</body>
</html>
