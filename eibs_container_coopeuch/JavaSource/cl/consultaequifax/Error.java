/**
 * Error.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.consultaequifax;

public class Error  implements java.io.Serializable {
    private java.lang.String formato;
    private java.lang.String identificador;
    private java.lang.String correlativo;
    private java.lang.String numeroErrores;
    private cl.consultaequifax.ListErrores[] listErrores;

    public Error() {
    }

    public java.lang.String getFormato() {
        return formato;
    }

    public void setFormato(java.lang.String formato) {
        this.formato = formato;
    }

    public java.lang.String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(java.lang.String identificador) {
        this.identificador = identificador;
    }

    public java.lang.String getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(java.lang.String correlativo) {
        this.correlativo = correlativo;
    }

    public java.lang.String getNumeroErrores() {
        return numeroErrores;
    }

    public void setNumeroErrores(java.lang.String numeroErrores) {
        this.numeroErrores = numeroErrores;
    }

    public cl.consultaequifax.ListErrores[] getListErrores() {
        return listErrores;
    }

    public void setListErrores(cl.consultaequifax.ListErrores[] listErrores) {
        this.listErrores = listErrores;
    }

    public cl.consultaequifax.ListErrores getListErrores(int i) {
        return listErrores[i];
    }

    public void setListErrores(int i, cl.consultaequifax.ListErrores value) {
        this.listErrores[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        Error other = (Error) obj;
        boolean _equals;
        _equals = true
            && ((this.formato==null && other.getFormato()==null) || 
             (this.formato!=null &&
              this.formato.equals(other.getFormato())))
            && ((this.identificador==null && other.getIdentificador()==null) || 
             (this.identificador!=null &&
              this.identificador.equals(other.getIdentificador())))
            && ((this.correlativo==null && other.getCorrelativo()==null) || 
             (this.correlativo!=null &&
              this.correlativo.equals(other.getCorrelativo())))
            && ((this.numeroErrores==null && other.getNumeroErrores()==null) || 
             (this.numeroErrores!=null &&
              this.numeroErrores.equals(other.getNumeroErrores())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        Error history = (Error) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.listErrores==null && other.getListErrores()==null) || 
             (this.listErrores!=null &&
              java.util.Arrays.equals(this.listErrores, other.getListErrores())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        Error history = (Error) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getFormato() != null) {
            _hashCode += getFormato().hashCode();
        }
        if (getIdentificador() != null) {
            _hashCode += getIdentificador().hashCode();
        }
        if (getCorrelativo() != null) {
            _hashCode += getCorrelativo().hashCode();
        }
        if (getNumeroErrores() != null) {
            _hashCode += getNumeroErrores().hashCode();
        }
        if (getListErrores() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListErrores());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListErrores(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
