package com.datapro.eibs.internet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.apache.log4j.Category;

import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.internet.databeans.DC_USER_OPASS;
import com.datapro.generic.access.DAOSuper;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.BeanParser;

import datapro.eibs.beans.JBObjList;


/**
 * @author fhernandez
 *
 */
public class DAOUser extends DAOSuper {

	private static Category cat =
		Category.getInstance(DAOUser.class.getName());

	/**
	 * Constructor for DAOUserEibs.
	 * @param dbid
	 */
	public DAOUser(String dbid) {
		super(dbid);
	}
	
	public int deleteUserPassword(String user) throws DAOException {
		int rt;
		try {
			String sql = "DELETE FROM DC_USER_OPASS WHERE USERID=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, user.toUpperCase());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
		
	}

}
