<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<META HTTP-EQUIV="Pragma" CONTENT="No-cache">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%@ page import = "java.io.*,java.net.*,datapro.eibs.sockets.*,datapro.eibs.beans.*,datapro.eibs.master.*" %>
<script language="javascript">
//<!-- Hide from old browsers
function enter(tipo, desc, valvdu, valvhu, valsdu, valshu, valamp) {

var formLength= top.opener.document.forms[0].elements.length;

for(n=0;n<formLength;n++){
var elementName= top.opener.document.forms[0].elements[n].name;
	if(elementName == top.opener.fieldTipo){
  		top.opener.document.forms[0].elements[n].value = tipo;
  		break;
	}
 }
for(n=0;n<formLength;n++){
var elementName= top.opener.document.forms[0].elements[n].name;
	if(elementName == top.opener.fieldDesc){
  		top.opener.document.forms[0].elements[n].value = desc;
  		break;
	}
 }
for(n=0;n<formLength;n++){
var elementName= top.opener.document.forms[0].elements[n].name;
	if(elementName == top.opener.fieldVdu){
  		top.opener.document.forms[0].elements[n].value = valvdu;
  		break;
	}
 }
 for(n=0;n<formLength;n++){
var elementName= top.opener.document.forms[0].elements[n].name;
	if(elementName == top.opener.fieldVhu){
  		top.opener.document.forms[0].elements[n].value = valvhu;
  		break;
	}
 }
 for(n=0;n<formLength;n++){
var elementName= top.opener.document.forms[0].elements[n].name;
	if(elementName == top.opener.fieldSdu){
  		top.opener.document.forms[0].elements[n].value = valsdu;
  		break;
	}
 } 
 for(n=0;n<formLength;n++){
var elementName= top.opener.document.forms[0].elements[n].name;
	if(elementName == top.opener.fieldShu){
  		top.opener.document.forms[0].elements[n].value = valshu;
  		break;
	}
 }  
 for(n=0;n<formLength;n++){
var elementName= top.opener.document.forms[0].elements[n].name;
	if(elementName == top.opener.fieldAmp){
  		top.opener.document.forms[0].elements[n].value = valamp;
  		break;
	}
 } 
top.close();
 }
//-->
</script>
<TITLE></TITLE>
</head>
<jsp:useBean id= "EDD2000Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id="EDD2000" class="datapro.eibs.beans.EDD200001Message" scope="session" />
<body>

 <SCRIPT SRC="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<FORM>
<%
	if ( EDD2000Help.getNoResult() ) {
%>
 		<TABLE class="tbenter" width=100% height=100%>
 		<TR>
      <TD>

      <div align="center"> <font size="3"><b> No hay datos que correspondan con su criterio de busqueda</b></font>
      </div>
      </TD></TR>
   		</TABLE>
<%
	}
	else {
%>

  <TABLE class="tableinfo" style="width:40%" ALIGN=CENTER>
    <TR id="trdark">
      <TH ALIGN=CENTER  nowrap width="5%">Tipo</TH>
      <TH ALIGN=CENTER  nowrap width="15%">Descripción</TH>
                     
    </TR>
<%
				EDD2000Help.initRow();
					boolean firstTime = true;
					String chk = "";
					while (EDD2000Help.getNextRow()) {
						if (firstTime) {
							firstTime = false;
							chk = "checked";
						} else {
							chk = "";
						}
						EDD200001Message convObj = (EDD200001Message) EDD2000Help
								.getRecord();
			%>
			<tr id="trdark">				
				<td nowrap align="center"><a href="javascript:enter('<%= convObj.getE01TPSTTR() %>','<%= convObj.getE01TPSDSC() %>','<%= convObj.getE01TPSVDU() %>','<%= convObj.getE01TPSVHU() %>','<%= convObj.getE01TPSSDU() %>','<%= convObj.getE01TPSSHU() %>','<%= convObj.getE01TPSAMP() %>')"><%= convObj.getE01TPSTTR() %></td>				
				<td nowrap align="center"><%= convObj.getE01TPSDSC()%></td>		
			</tr>
			<%
				}
			%>
		</table>
			<%
				}
			%>		

</FORM>
</BODY>
</HTML>

