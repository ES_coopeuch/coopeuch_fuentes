<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.EPV121601Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script type="text/javascript">

function validaCampos(){

	if(document.getElementById("E01TPRREN1").checked==true){
		var avanceCre = null;
		var cantCuotas = null;
		var montReliq = null;
		var descTasaReliq = null;
		
		//Validacion % Avance Crédito
		if(document.getElementById("E01TPRAVC").value !== ""){
			if (isNaN(parseInt(document.getElementById("E01TPRAVC").value))) {			
				alert('Valor % de Avance de Crédito debe ser un valor numerico');
				return false;											
			}else if(document.getElementById("E01TPRAVC").value<0){			
				alert('Valor % de Avance de Crédito no puede ser negativo');
				return false;			
			}else if(document.getElementById("E01TPRAVC").value>100){
				alert('Valor % de Avance de Crédito no puede ser mayor a 100');
				return false;		 	
			}else if(dos_decimales(document.getElementById("E01TPRAVC").value) !== true){			
				alert('Formato invalido % de Avance de Crédito, permitido XXX.XX');
				return false;			
			}else{
				avanceCre = true;
			}	
		}else{
			avanceCre = true;
		}		
		
		//Validacion cantidad de cuotas
		if(document.getElementById("E01TPRCCU").value !== ""){
			if (!/^([0-9])*$/.test(document.getElementById("E01TPRCCU").value)){		   
	           alert("Valor cantidad de cuotas debe ser numerico y entero");
	           return false;
	    	}else if(document.getElementById("E01TPRCCU").value<0){
			 	alert('Valor cantidad de cuotas no puede ser negativo');
			 	return false;		 	
			}else if(document.getElementById("E01TPRCCU").value>100){
				alert('Valor cantidad de cuotas no puede ser mayor a 100');
				return false;		 	
			}else{
				cantCuotas = true;
			}
		}else{
			cantCuotas = true;
		}		
		
		//Validacion % Monto Reliquidación
		if(document.getElementById("E01TPRMRE").value !== ""){
			if (!/^([0-9])*$/.test(document.getElementById("E01TPRMRE").value)){		   
	           alert("Valor % Monto Reliquidación debe ser numerico y entero");
	           return false;
	    	}else if(document.getElementById("E01TPRMRE").value<0){
			 	alert('Valor % Monto Reliquidación no puede ser negativo');
			 	return false;		 	
			}else if(document.getElementById("E01TPRMRE").value>100){
				alert('Valor % Monto Reliquidación no puede ser mayor a 100');
				return false;		 	
			}else{
				montReliq = true;
			}
		}else{
			montReliq = true;
		}
		
		
		
		//Validacion % Desc. Tasa Reliquidacion
		if(document.getElementById("E01TPRDTA").value !== ""){
			if (isNaN(parseInt(document.getElementById("E01TPRDTA").value))) {			
				alert('Valor % Desc. Tasa Reliquidacion debe ser un valor numerico');
				return false;								
			}else if(document.getElementById("E01TPRDTA").value<0){			
				alert('Valor % Desc. Tasa Reliquidacion no puede ser negativo');
				return false;
			}else if(document.getElementById("E01TPRDTA").value>100){
				alert('Valor % Desc. Tasa Reliquidacion no puede ser mayor a 100');
				return false;		 	
			}else if(dos_decimales(document.getElementById("E01TPRDTA").value) !== true){			
				alert('Formato invalido % Desc. Tasa Reliquidacion, permitido XXX.XX');
				return false;
			}else{
				descTasaReliq = true;
			}
		}else{
			descTasaReliq = true;
		}
		
		
		if(avanceCre==true && cantCuotas==true && montReliq==true && descTasaReliq==true){		
			document.getElementById("form1").submit();
		}
	
	}else{
			document.getElementById("form1").submit();
	}

}

function dos_decimales(cadena){
	var expresion=/^\d+(\.\d{0,2})?$/;
	var resultado=expresion.test(cadena);
	return resultado;
}


	function habilita(hab){
		if (hab=='S'){		
			document.forms[0].E01TPRBOB.disabled = false;
			document.forms[0].E01TPRBOR.disabled = false;
			document.forms[0].E01TPRBOA.disabled = false;						
		}else{
			document.forms[0].E01TPRBOB.value ="";		
			document.forms[0].E01TPRBOB.disabled = true;
			document.forms[0].E01TPRBOR.value ="";		
			document.forms[0].E01TPRBOR.disabled = true;
			document.forms[0].E01TPRBOA.value ="";		
			document.forms[0].E01TPRBOA.disabled = true;			
		}
		
				
	}
	
function habilitar(value){
	if(value=="Y")	{		
		document.getElementById("E01TPRAVC").disabled = false;
		document.getElementById("E01TPRCCU").disabled = false;
		document.getElementById("E01TPRMRE").disabled = false;
		document.getElementById("E01TPRDTA").disabled = false;		
	}else if(value=="N"){                         
		document.getElementById("E01TPRAVC").disabled = true;
		document.getElementById("E01TPRAVC").value = "";
		document.getElementById("E01TPRCCU").disabled = true;
		document.getElementById("E01TPRCCU").value = "";
		document.getElementById("E01TPRMRE").disabled = true;
		document.getElementById("E01TPRMRE").value = "";
		document.getElementById("E01TPRDTA").disabled = true;
		document.getElementById("E01TPRDTA").value = "";			
	}		
}
	
	function showHiddenField(){
		if (document.forms[0].E01TPRDOP.value=='C'){//cesante dejar los campos disponibles.		
			document.getElementById('f1').style.display	= 'none';
			document.getElementById('f2').innerHTML = '% Cuotas Renegociadas :';
			document.getElementById('f3').style.display	= 'none';
			document.getElementById('f4').style.display	= 'none';
			document.getElementById('f5').style.display	= 'none';
			document.getElementById('f6').style.display ='none';
			document.getElementById('f7').style.display ='none';
			document.getElementById('f8').style.display ='none';
			document.getElementById('f9').style.display ='none';
			document.getElementById('f10').style.display ='none';			
										
		}else{
			document.getElementById('f1').style.display ='';
			document.getElementById('f2').innerHTML = '% Cobertura garantia :';
			document.getElementById('f3').style.display ='';
			document.getElementById('f4').style.display ='';
			document.getElementById('f5').style.display ='';		
			document.getElementById('f6').style.display ='';
			document.getElementById('f7').style.display ='';
			document.getElementById('f8').style.display ='';
			document.getElementById('f9').style.display ='';
			document.getElementById('f10').style.display ='';					
		}						
	}	
	
	
	function showHiddenField_FlagGarant(){
	
		var value = getElementChecked("E01TPRFLG").value;
		
		if (value=='V'){//Variable colocamo los titulos corrrespondientes		
			document.getElementById('f2').innerHTML = '% Cobertura garantia minima :';
			document.getElementById('f21').innerHTML = '% Cobertura garantia Maxima :';							
		    document.forms[0].E01TPRCUO.disabled = true;
			document.forms[0].E01TPRMOR.disabled = true;		    			
		}else{			
			document.getElementById('f2').innerHTML = (document.forms[0].E01TPRDOP.value=='C')?'% Cuotas Renegociadas :':'% Cobertura garantia :';	
			document.getElementById('f21').innerHTML = '% Comision Apertura :';
		    document.forms[0].E01TPRCUO.disabled = false;
			document.forms[0].E01TPRMOR.disabled = false;		    											
		}						
	}	
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body onload="showHiddenField();">
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	Consulta Tipos de Evaluacion
<%} else if (maintenance){ %>
	Mantención Tipos de Evaluacion
<%} else { %>
	Nuevo  Tipos de Evaluacion
<%} %>

 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PVPRD_maintenance.jsp, EPV1216"></h3>
<hr size="4">
<form id="form1" method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1216" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01UBK" value="<%= currUser.getE01UBK().trim()%>">
  
 <% int row = 0;%>
 
    
  <table  class="tableinfo" width="100%" border="0">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr > 
            <td width="40%" nowrap="nowrap"> 
              <div align="right">Tipo Producto :</div>
            </td>
            <td width="60%" nowrap="nowrap"> 
            <%if (readOnly){%>
            	<eibsinput:text property="E01TPRTPR" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" size="2" disabled="true"/>
            <%}else{%>
            	<eibsinput:text property="E01TPRTPR" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" size="2"/>
            <%} %>            
	             
	        </td>
          </tr>

          <tr > 
            <td > 
              <div align="right">Descripción :</div>
            </td>
            <td  > 
            <%if (readOnly){%>
            	<eibsinput:text property="E01TPRDES" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" disabled="true"/>
            <%}else{%>
            	<eibsinput:text property="E01TPRDES" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>"/>
            <%} %>
	             
	        </td>
          </tr>
          
          <tr > 
            <td > 
              <div align="right">Tipo Evaluacion :</div>
            </td>
			<td  align="left">
				<select name="E01TPRDOP" <%=readOnly?"disabled":""%> onchange="showHiddenField();">          
		             <option value="P" <%if (cnvObj.getE01TPRDOP().equals("P")) out.print("selected"); %>>P: Planilla</option>
		             <option value="D" <%if (cnvObj.getE01TPRDOP().equals("D"))out.print("selected"); %>>D: Pago Directo</option>    
		             <option value="S" <%if (cnvObj.getE01TPRDOP().equals("S")) out.print("selected"); %>>S: Evaluacion Especial</option>   
		             <option value="M" <%if (cnvObj.getE01TPRDOP().equals("M"))out.print("selected"); %>>M: Microcredito</option>   
		             <option value="E" <%if (cnvObj.getE01TPRDOP().equals("E")) out.print("selected"); %>>E: Compra Cartera</option> 
		             <option value="T" <%if (cnvObj.getE01TPRDOP().equals("T")) out.print("selected"); %>>T: Tarjeta de Credito</option>  
		             <option value="A" <%if (cnvObj.getE01TPRDOP().equals("A")) out.print("selected"); %>>A: Automotriz</option>
		             <option value="C" <%if (cnvObj.getE01TPRDOP().equals("C")) out.print("selected"); %>>C: Cesante</option>   		                                                         
		          </select>  
			</td>										           
          </tr>
          <tr> 
            <td > 
              <div align="right">Moneda :</div>
            </td>
            <td  > 
            
            <%if (readOnly){%>
            	<eibsinput:help name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" property="E01TPRCCY" fn_param_one="E01TPRCCY"  readonly="true" disabled="true"/>
            <%}else{%>
            	<eibsinput:help name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" property="E01TPRCCY" fn_param_one="E01TPRCCY" />
            <%} %>
                        
					             
	        </td>
          </tr>
                    
          <tr> 
            <td > 
              <div align="right">Producto Corto Plazo :</div>
            </td>
            <td  > 
              <input type="text" name="E01TPRPRC" size="5" maxlength="4" value="<%= cnvObj.getE01TPRPRC().trim()%>" <%=readOnly?"disabled":""%> >
			<%if  (!readOnly) { %>
              <a href="javascript:GetProduct('E01TPRPRC','10','<%=currUser.getE01UBK() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absmiddle" border="0"></a> 
             <%} %>                             
	        </td>
          </tr>
          <tr> 
            <td > 
              <div align="right">Producto Largo Plazo :</div>
            </td>
            <td  > 
              <input type="text" name="E01TPRPRL" size="5" maxlength="4" value="<%= cnvObj.getE01TPRPRL().trim()%>" <%=readOnly?"disabled":""%> >
			<%if  (!readOnly) { %>
              <a href="javascript:GetProduct('E01TPRPRL','10','<%=currUser.getE01UBK() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absmiddle" border="0"></a> 
             <%} %>                             
	        </td>
          </tr>          
          <tr id="f1"> 
            <td> 
              <div align="right">Flag Garantia :</div>
            </td>
            <td> 
 		       <p> 
                 <input type="radio" name="E01TPRFLG"  value="Y" <%if (cnvObj.getE01TPRFLG().equals("Y")) out.print("checked"); %>  <%=readOnly?"disabled":""%> onclick="showHiddenField_FlagGarant();" >
                  Si 
                 <input type="radio" name="E01TPRFLG"  value="N" <%if (cnvObj.getE01TPRFLG().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%> onclick="showHiddenField_FlagGarant();" >
                  No
                 <input type="radio" name="E01TPRFLG"  value="V" <%if (cnvObj.getE01TPRFLG().equals("V")) out.print("checked"); %>  <%=readOnly?"disabled":""%> onclick="showHiddenField_FlagGarant();" >
                  Variable
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
                               
	        </td>
          </tr>
          <tr> 
            <td > 
              <div align="right" id="f2">% Cobertura garantia :</div>
            </td>
            <td  > 
            <%if (readOnly){%>
            	<eibsinput:text name="cnvObj" property="E01TPRCOB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>" disabled="true"/>
            <%}else{%>
            	<eibsinput:text name="cnvObj" property="E01TPRCOB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>"/>
            <%} %>            
                 
	        </td>
          </tr>

          <tr id="f3"> 
            <td > 
              <div align="right" id="f21">% Comision Apertura :</div>
            </td>
            <td  > 
                 
            <%if (readOnly){%>
            	<eibsinput:text name="cnvObj" property="E01TPRAPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>" disabled="true"/>
            <%}else{%>
            	<eibsinput:text name="cnvObj" property="E01TPRAPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>"/>
            <%} %>                   
	        </td>
          </tr>          

          <tr id="f4"> 
            <td > 
              <div align="right">% Comision Cuota :</div>
            </td>
            <td  > 
                 
            <%if (readOnly){%>
            	<eibsinput:text name="cnvObj" property="E01TPRCUO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>" disabled="true"/>
            <%}else{%>
            	<eibsinput:text name="cnvObj" property="E01TPRCUO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>"/>
            <%} %>                   
                 
	        </td>
          </tr>          

          <tr id="f5"> 
            <td > 
              <div align="right">% Comision Mora :</div>
            </td>
            <td  > 
                 
            <%if (readOnly){%>
            	<eibsinput:text name="cnvObj" property="E01TPRMOR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>" disabled="true"/>
            <%}else{%>
            	<eibsinput:text name="cnvObj" property="E01TPRMOR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>"/>
            <%} %>                   
                 
	        </td>
          </tr>          

          <tr> 
            <td > 
              <div align="right">Meses de Plazo Maximo Normal :</div>
            </td>
            <td  > 
                 
            <%if (readOnly){%>
            	<eibsinput:text name="cnvObj" property="E01TPRPMN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" disabled="true"/>
            <%}else{%>
            	<eibsinput:text name="cnvObj" property="E01TPRPMN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>"/>
            <%} %>                   
                 
	        </td>
          </tr>
         
          <tr id="f6"> 
            <td > 
              <div align="right">Meses de Plazo Maximo Renegociado :</div>
            </td>
            <td  >                 
            <%if (readOnly){%>
            	<eibsinput:text name="cnvObj" property="E01TPRPMR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" disabled="true"/>
            <%}else{%>
            	<eibsinput:text name="cnvObj" property="E01TPRPMR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" />
            <%} %>                   
                 
	        </td>
          </tr>

          <tr> 
            <td > 
              <div align="right">Dias de Plazo Minimo Cuota 1 :</div>
            </td>
            <td  >                  
            <%if (readOnly){%>
            	<eibsinput:text name="cnvObj" property="E01TPRPMI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" disabled="true"/>
            <%}else{%>
            	<eibsinput:text name="cnvObj" property="E01TPRPMI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>"/>
            <%} %>                   
                 
	        </td>
          </tr>

          <tr> 
            <td > 
              <div align="right">Dias de Plazo Maximo Cuota 1 :</div>
            </td>
            <td  > 
                
            <%if (readOnly){%>
            	 <eibsinput:text name="cnvObj" property="E01TPRPMA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" disabled="true"/>
            <%}else{%>
            	 <eibsinput:text name="cnvObj" property="E01TPRPMA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>"/>
            <%} %>                   
                 
	        </td>
          </tr>
          
          
          <tr id="f7"> 
            <td > 
              <div align="right">Dias de Mora Minima Pago Directo:</div>
            </td>
            <td  > 
                
            <%if (readOnly){%>
            	 <eibsinput:text name="cnvObj" property="E01TPRMMD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" disabled="true"/>
            <%}else{%>
            	 <eibsinput:text name="cnvObj" property="E01TPRMMD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>"/>
            <%} %>                   
                 
	        </td>
          </tr>
          <tr id="f8"> 
            <td > 
              <div align="right">Dias de Mora Minima Pago Planilla:</div>
            </td>
            <td  > 
                
            <%if (readOnly){%>
            	 <eibsinput:text name="cnvObj" property="E01TPRMMP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" disabled="true"/>
            <%}else{%>
            	 <eibsinput:text name="cnvObj" property="E01TPRMMP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>"/>
            <%} %>                   
                 
	        </td>
          </tr>          
          <tr id="f9"> 
            <td > 
              <div align="right">Acepta Liquido :</div>
            </td>
            <td  > 
                
 		       <p> 
                 <input type="radio" name="E01TPRLIQ"  value="Y" <%if (cnvObj.getE01TPRLIQ().equals("Y")) out.print("checked"); %>  <%=readOnly?"disabled":""%> >
                  Si 
                 <input type="radio" name="E01TPRLIQ"  value="N" <%if (cnvObj.getE01TPRLIQ().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%>>
                  No
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
	        </td>
          </tr>   
          <tr id="f10"> 
            <td > 
              <div align="right">Flag Renegociado :</div>
            </td>
            <td  > 
                
 		       <p> 
                 <input type="radio" id="E01TPRREN1" name="E01TPRREN"  value="Y" <%if (cnvObj.getE01TPRREN().equals("Y")) out.print("checked"); %>  <%=readOnly?"disabled":""%> onchange="habilitar(this.value);">
                  Si 
                 <input type="radio" id="E01TPRREN2" name="E01TPRREN"  value="N" <%if (cnvObj.getE01TPRREN().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%> onchange="habilitar(this.value);">
                  No
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
	        </td>
          </tr>  
          <tr> 
            <td > 
              <div align="right">% Avance Crédito :</div>
            </td>
            <td> 
            <%if (readOnly){%>
               <input type="text" id="E01TPRAVC" name="E01TPRAVC" size="6" maxlength="6" value="<%if (cnvObj.getE01TPRREN().equals("Y")){ out.print(cnvObj.getE01TPRAVC().trim());}else{out.print("");}%>" disabled="true" onkeyup=" checkDecimal(2) " onkeypress=" enterSignDecimal(2) " onchange="setRecalculate3();" >
            <%}else{%>
            	<input type="text" id="E01TPRAVC" name="E01TPRAVC" size="6" maxlength="6" value="<%if (cnvObj.getE01TPRREN().equals("Y")){ out.print(cnvObj.getE01TPRAVC().trim());}else{out.print("");}%>" <%if (cnvObj.getE01TPRREN().equals("N")) { %>disabled="true"<%}else{ %> enabled="true"<%} %> onkeyup=" checkDecimal(2) " onkeypress=" enterSignDecimal(2) " onchange="setRecalculate3();" >
            <%} %> 
	        </td>
          </tr> 
          <tr> 
            <td > 
              <div align="right">Cantidad de Cuotas :</div>
            </td>
            <td> 
            <%if (readOnly){%>
               <input type="text" id="E01TPRCCU" name="E01TPRCCU" size="4" maxlength="4" value="<%if (cnvObj.getE01TPRREN().equals("Y")){ out.print(cnvObj.getE01TPRCCU().trim());}else{out.print("");}%>" disabled="true" >
            <%}else{%>
            	<input type="text" id="E01TPRCCU" name="E01TPRCCU" size="4" maxlength="4" value="<%if (cnvObj.getE01TPRREN().equals("Y")){ out.print(cnvObj.getE01TPRCCU().trim());}else{out.print("");}%>" <%if (cnvObj.getE01TPRREN().equals("N")) { %>disabled="true"<%}else{ %> enabled="true"<%} %> >
            <%} %>    
	        </td>
          </tr>
          <tr> 
            <td > 
              <div align="right">% Monto Reliquidación :</div>
            </td>
            <td> 
            <%if (readOnly){%>
               <input type="text" id="E01TPRMRE" name="E01TPRMRE" size="3" maxlength="3" value="<%if (cnvObj.getE01TPRREN().equals("Y")){ out.print((int)Double.parseDouble(cnvObj.getE01TPRMRE().trim()));}else{out.print("");}%>" disabled="true" >
            <%}else{%>
            	<input type="text" id="E01TPRMRE" name="E01TPRMRE" size="3" maxlength="3" value="<%if (cnvObj.getE01TPRREN().equals("Y")){ out.print((int)Double.parseDouble(cnvObj.getE01TPRMRE().trim()));}else{out.print("");}%>" <%if (cnvObj.getE01TPRREN().equals("N")) { %>disabled="true"<%}else{ %> enabled="true"<%} %> >
            <%}%>   
	        </td>
          </tr>
          <tr> 
            <td > 
              <div align="right">% Desc. Tasa Reliquidación :</div>
            </td>
            <td>
            <%if (readOnly){%> 
               <input type="text" id="E01TPRDTA" name="E01TPRDTA" size="6" maxlength="6" value="<%if (cnvObj.getE01TPRREN().equals("Y")){ out.print(cnvObj.getE01TPRDTA().trim());}else{out.print("");}%>" disabled="true" onkeyup=" checkDecimal(2) " onkeypress=" enterSignDecimal(2) " onchange="setRecalculate3();" >
            <%}else{%>
            	<input type="text" id="E01TPRDTA" name="E01TPRDTA" size="6" maxlength="6" value="<%if (cnvObj.getE01TPRREN().equals("Y")){ out.print(cnvObj.getE01TPRDTA().trim());}else{out.print("");}%>" <%if (cnvObj.getE01TPRREN().equals("N")) { %>disabled="true"<%}else{ %> enabled="true"<%} %> onkeyup=" checkDecimal(2) " onkeypress=" enterSignDecimal(2) " onchange="setRecalculate3();" >
            <%} %>     
	        </td>
          </tr> 
		 <tr> 
            <td > 
              <div align="right">Retencion de Clientes :</div>
            </td>
            <td  > 
                
 		       <p> 
                 <input type="radio" name="E01TPRRCL"  value="Y" <%if (cnvObj.getE01TPRRCL().equals("Y")) out.print("checked"); %>  <%=readOnly?"disabled":""%> >
                  Si 
                 <input type="radio" name="E01TPRRCL"  value="N" <%if (cnvObj.getE01TPRRCL().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%>>
                  No
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
	        </td>
          </tr>                     
                             
          <tr> 
            <td > 
				<div align="right"><b>Parametros Motor&nbsp;&nbsp;&nbsp;&nbsp;</b></div>
            </td>
            <td  >               
              &nbsp;
              </td>
          </tr>                    
          <tr> 
            <td > 
              <div align="right">Evaluación Motor :</div>
            </td>
            <td  > 
                
 		       <p> 
                 <input type="radio" name="E01TPRFEV"  value="Y" onclick="habilita('S')" <%if (cnvObj.getE01TPRFEV().equals("Y")) out.print("checked"); %>  <%=readOnly?"disabled":""%> >
                  Si 
                 <input type="radio" name="E01TPRFEV"  value="N" onclick="habilita('N')" <%if (cnvObj.getE01TPRFEV().equals("N") || cnvObj.getE01TPRFEV().equals("")) out.print("checked"); %>  <%=readOnly?"disabled":""%>>
                  No
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
	        </td>
          </tr>   
          <tr> 
            <td > 
              <div align="right">Tipo Evaluación (Bussines Object) :</div>
            </td>
            <td  > 
                
            <%if (readOnly){%>
            	 <eibsinput:text name="cnvObj" property="E01TPRBOB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" maxlength="20" size="20"  disabled="true"/>
            <%}else{%>
            	 <eibsinput:text name="cnvObj" property="E01TPRBOB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" maxlength="20" size="20"/>
            <%} %>                   
                 
	        </td>
          </tr>
          
          <tr > 
            <td > 
              <div align="right">Tipo Evaluación (Bussines Object Renegociado) :</div>
            </td>
            <td  > 
                
            <%if (readOnly){%>
            	 <eibsinput:text name="cnvObj" property="E01TPRBOR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" maxlength="20" size="20"  disabled="true"/>
            <%}else{%>
            	 <eibsinput:text name="cnvObj" property="E01TPRBOR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" maxlength="20" size="20"/>
            <%} %>                   
                 
	        </td>
          </tr>
          <tr > 
            <td > 
              <div align="right">Tipo Evaluación (Bussines Object AVAL) :</div>
            </td>
            <td  > 
                
            <%if (readOnly){%>
            	 <eibsinput:text name="cnvObj" property="E01TPRBOA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" maxlength="20" size="20"  disabled="true"/>
            <%}else{%>
            	 <eibsinput:text name="cnvObj" property="E01TPRBOA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" maxlength="20" size="20"/>
            <%} %>                   
                 
	        </td>
          </tr>                               
          <tr > 
            <td > 
              <div align="right">Activar en Plataforma de ventas :</div>
            </td>
            <td  > 
                 
 		       <p> 
                 <input type="radio" name="E01TPRFAC"  value="Y" <%if (cnvObj.getE01TPRFAC().equals("Y")) out.print("checked"); %>  <%=readOnly?"disabled":""%> >
                  Si 
                 <input type="radio" name="E01TPRFAC"  value="N" <%if (cnvObj.getE01TPRFAC().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%>>
                  No
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
	        </td>
          </tr>  
        </table>
      </td>
    </tr>
  </table>

<%if  (!readOnly) { %>
    <div align="center"> 
        <input id="EIBSBTN" type=button name="Submit" value="Enviar" onclick="validaCampos()">
    </div>
<% } %>  

<%if (cnvObj.getE01TPRFEV().equals("N") || cnvObj.getE01TPRFEV().equals("")){%>
<script type="text/javascript">
	habilita('N');
</script>	
<%} %>
  </form>
</body>
</HTML>
