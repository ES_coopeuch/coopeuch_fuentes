package datapro.eibs.salesplatform;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV108001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;

public class JSEPV1080 extends JSEIBSServlet {

	protected static final int R_STATUS_INQUIRY = 100;
	protected static final int A_STATUS_INQUIRY = 101;
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_STATUS_INQUIRY:
				procReqStatusList(user, req, res, session);
				break;
			case A_STATUS_INQUIRY:
				procActStatusList(user, req, res, session);
				break;
			default:
				forward(devPage, req, res);
				break;
		}
	}

	private void procActStatusList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		UserPos userPO = getUserPos(session);
		String customer = req.getParameter("E01CUN") == null ? "0" : req.getParameter("E01CUN").trim();
		userPO.setCusNum(customer);
		String rut = req.getParameter("E01IDN") == null ? "0" : req.getParameter("E01IDN").trim();
		userPO.setCusType(rut);
		String name = req.getParameter("E01NME") == null ? "" : req.getParameter("E01NME").trim();
		userPO.setCusName(name);
		
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1080", req);
			EPV108001Message msgList = (EPV108001Message) mp.getMessageRecord("EPV108001", user.getH01USR(), "0015");
			msgList.setE01SELCUN(customer);
			
			mp.sendMessage(msgList);
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			//Header
			msgList = (EPV108001Message) mp.receiveMessageRecord("EPV108001");
			
			if (mp.hasError(msgError)) {
				session.setAttribute("error", msgError);
				forward("EPV1000_client_enter.jsp", req, res);
			} else {
				session.setAttribute("header", msgList);
				//Outstanding Debts
				JBObjList debts = mp.receiveMessageRecordList("H02FLGMAS");
				session.setAttribute("EPV108002List", debts);
				//Overdue
				JBObjList overdue = mp.receiveMessageRecordList("H03FLGMAS");
				session.setAttribute("EPV108003List", overdue);
				//Credit Cards
				JBObjList cards = mp.receiveMessageRecordList("H04FLGMAS");
				session.setAttribute("EPV108004List", cards);
				//Collateral
				JBObjList collateral = mp.receiveMessageRecordList("H05FLGMAS");
				session.setAttribute("EPV108005List", collateral);
				
				JBObjList mortgage = mp.receiveMessageRecordList("H06FLGMAS");
				session.setAttribute("EPV108006List", mortgage);

				forward("EPV1080_salesplatorm_status_inquiry.jsp", req, res);
			}
			
		} finally {
			if (mp != null)	mp.close();
		}
	}

	private void procReqStatusList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		userPO.setRedirect("/servlet/datapro.eibs.salesplatform.JSEPV1080?SCREEN=101");
		userPO.setHeader1("Plataforma de Ventas - Estado de Situaci�n");
		session.setAttribute("userPO", userPO);
		forward("EPV1000_client_enter.jsp", req, res);
	}

}
