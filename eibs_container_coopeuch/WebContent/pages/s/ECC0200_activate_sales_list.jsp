<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Transacciones PostVenta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%@ page import = "datapro.eibs.master.Util,datapro.eibs.beans.*" %>

<jsp:useBean id="actSaleslist" class="datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">
function PrintPreview() {

	var pg = '<%=request.getContextPath()%>/pages/s/ECC0200_activate_sales_list_print.jsp';
	CenterWindow(pg,720,500,2);

}
</SCRIPT>

</head>

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>


<h3 align="center">Transacciones PostVenta<br/>Tarjetas Credito/Debito<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="activate_sales_list.jsp,ECC0200"></h3>
<hr size="4">
  <br/>
  <br/>
  <% if (actSaleslist.getNoResult()) {%>
  <br/>
  <br/>
  <br/>  
      <h3 align=center>No existen Movimientos Para La Agencia y la fecha seleccionada.</h3>
  <% } else { %>
  
  <%
  	actSaleslist.initRow();   	
	actSaleslist.getNextRow();  	
    ECC020001Message hMesg = (ECC020001Message) actSaleslist.getRecord();
     
   %>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" class="tbhead" align="center">
          <tr id=trdark> 
            <td nowrap width="10%" align="right"> 
              <div align="right">Fecha : </div>
            </td>
            <td nowrap width="10%" align="left">
	            <%=(hMesg.getE01SPVISD().length()>1?"":"0")+hMesg.getE01SPVISD()+"/"+(hMesg.getE01SPVISM().length()>1?"":"0")+hMesg.getE01SPVISM()+"/"+hMesg.getE01SPVISY()%>
            </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Sucursal : </div>
            </td>
			<td nowrap width="10%" align="left">
			<%= hMesg.getE01SPVINB()%>
			</td>            
          </tr>          
        </table>
      </td>
    </tr>
  </table>
  <table class="tableinfo">
    <tr > 
      <td nowrap>
        <table id="headTable" >
		    <tr id="trdark">  
		      <th align="center" nowrap width="15%">Rut Usuario</TH>
		      <th align="center" nowrap width="15%">Rut Socio</TH>			      	      
		      <th align="center" nowrap width="20%">Nombre Socio</TH>
		      <th align="center" nowrap width="15%">N�mero Tarjeta</TH>
		      <th align="center" nowrap width="20%">Transacci�n</TH>
		      <th align="center" nowrap width="10%">Tipo</TH>
   		      <th align="center" nowrap width="5%"></TH>		      		      
		    </TR>
		</table>  
   		<div id="dataDiv1" class="scbarcolor" style="padding:0" nowrap>
    		<table id="dataTable"  >
               <%          		
          		actSaleslist.initRow();
   		  		while (actSaleslist.getNextRow()) {
          			ECC020001Message message = (ECC020001Message) actSaleslist.getRecord();
          		%>    		
          			  <tr> 
				         <td nowrap align="center"> <%= message.getE01SPVRID()%></td>				         
				         <td nowrap align="center">	<%= message.getE01SPVCID()%></td>
				         <td nowrap align="left"> <%= message.getE01DESCID()%></td>
				         <td nowrap align="center">  <%= message.getE01SPVNUM()%></td>
				         <td nowrap align="center">  <%= message.getE01DESTCD()%></td>
						 <td  nowrap align="center"><% if(message.getE01SPVTDC().equals("C")) out.print("Credito"); else out.print("Debito"); %></TD>
						 <td nowrap align="center"><%= message.getE01SPVTPI()%></td>							 				         			         
				     </tr>
				 <%
          		   }         
                %>         
		     </table>   
  		</div>	
          		 
      </td>
    </tr>
  </table>

  
   <div align="center"> 
    <input id="EIBSBTN" type=button name="Submit" OnClick="PrintPreview()" value="Imprimir">
  </div>
  
  <SCRIPT Language="Javascript">
    function resizeDoc() {
       adjustEquTables(headTable, dataTable, dataDiv1,1,false);
    }
  	resizeDoc();
  	window.onresize=resizeDoc;
  </SCRIPT>
  
  <% } %>
   
    
</body>
</html>
