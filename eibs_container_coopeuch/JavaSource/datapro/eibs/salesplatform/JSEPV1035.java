package datapro.eibs.salesplatform;

/**
 * Visado Senior
 * Creation date: (19/11/12)
 * @author: EV
 */
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.coopeuch.core.tiendavirtual.ActualizaStockEntrada;
import cl.coopeuch.core.tiendavirtual.RespActualizaStock;
import cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.FaultMsg;
import cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtualSOAPProxy;
import cl.coopeuch.creditos.predictorriesgo.sinacofi.DatosConsulta;
import cl.coopeuch.creditos.predictorriesgo.sinacofi.Salida;
import cl.coopeuch.creditos.predictorriesgo.sinacofi.SinacofiWSPortProxy;


import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV103501Message;
import datapro.eibs.beans.EPV103502Message;
import datapro.eibs.beans.EPV117001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSEPV1035 extends JSEIBSServlet {

	/**
	 * comentario 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_PLATFORM_LIST_SEARCH_CLIENT = 150;
	protected static final int R_PLATFORM_LIST = 100;	
	protected static final int A_PLATFORM_LIST = 101;
	
	protected static final int R_PLATFORM_MAINT = 201;
	protected static final int A_PLATFORM_MAINTENANCE = 600;
	protected static final int R_PLATFORM_SINACOFI = 500;		
	

	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		switch (screen) {
		case R_PLATFORM_LIST_SEARCH_CLIENT:
			procReqPlatformList(user, req, res, session);
			break;
		case A_PLATFORM_LIST:
		case R_PLATFORM_LIST:			
			procActionPlatformList(user, req, res, session);
			break;
		case R_PLATFORM_MAINT:
			procReqPlatform(user, req, res, session);
			break;
		case A_PLATFORM_MAINTENANCE:
			procActionMaintenance(user, req, res, session);
			break;
		case R_PLATFORM_SINACOFI:
			procReqPlatformConsultaSinacofi(user, req, res, session);
			break;				
		default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}
	/**
	 * procReqPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
			UserPos userPO = getUserPos(ses);
			userPO.setRedirect("/servlet/datapro.eibs.salesplatform.JSEPV1035?SCREEN=101");
			userPO.setHeader1("Plataforma de Ventas - Visado Senior de Documentos");
			ses.setAttribute("userPO", userPO);
			forward("EPV1000_client_enter.jsp", req, res);

	}
	
	
	/**
	 * procActionPlatformList: find the list of forms depending on status, the program will epvl1005
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		
		String customer = req.getParameter("E01CUN") == null ? "0" : req.getParameter("E01CUN").trim();
		
		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV1035", req);
			EPV103502Message msgList = (EPV103502Message) mp.getMessageRecord("EPV103502");
			msgList.setH02USERID(user.getH01USR());
			msgList.setE02SELCUN(customer);

			// Sends message
			mp.sendMessage(msgList);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			// Receive salesPlatform list for visado senior
			JBObjList list = mp.receiveMessageRecordList("H02FLGMAS");
			
			if (mp.hasError(error)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", error);
				forward("EPV1000_client_enter.jsp", req, res);
			} else {								
				userPO.setHeader9(" ");
				session.setAttribute("EPV103502List", list);
				session.setAttribute("userPO", userPO);										
				forward("EPV1035_salesplatform_list.jsp", req, res);					
				
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	/**
	 * procReqPlatform: This Method show a single Sales Platform for evaluation process.
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatform(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		UserPos userPO = getUserPos(session);
		userPO.setPurpose("MAINTENANCE");
		
		JBObjList list = (JBObjList) session.getAttribute("EPV103502List");
		
		int row = 0;
		try {
			row = Integer.parseInt(req.getParameter("row"));
		} catch (Exception e) {
			row = 0;
		}
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1035", req);
			
			EPV103502Message senior = (EPV103502Message) list.get(row);

			EPV103501Message msg = (EPV103501Message) mp.getMessageRecord("EPV103501", user.getH01USR(), "0002");
			msg.setE01PVMCUN(senior.getE02PVMCUN());
			msg.setE01PVMNUM(senior.getE02PVMNUM());
			
			mp.sendMessage(msg);
			
			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV103501Message) mp.receiveMessageRecord();

			session.setAttribute("error", msgError);
			session.setAttribute("document", msg);// set header page
			
			if (!mp.hasError(msgError)) {
				// if there are no errors go to maintenance page
				req.setAttribute("row", row);//save selected row
				forward("EPV1035_salesplatform_documentos_senior.jsp", req, res);
			} else {
				// if there are errors go back to list page
				forward("EPV1035_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	
	/**
	 * Process the Maintenance
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1035", req);

			EPV103501Message msg = (EPV103501Message) mp.getMessageRecord("EPV103501", user.getH01USR(), "0005");

			String ope = req.getParameter("E01FLGOPE");
			// Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);
			
			//Sending message
			mp.sendMessage(msg);
			
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV103501Message) mp.receiveMessageRecord();
			
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			
			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and sho errors
				session.setAttribute("document", msg);
				forward("EPV1035_salesplatform_documentos_senior.jsp", req, res);			
			} else {
				// if there are no errors and ope = R then go Tienda Virtual
				if ("R".equals(ope)){
					boolean ok = quitarReservaProductosTiendaVirtual(req,session,user,msg.getE01PVMNUM());
				}
				
				// if there are no errors go back to list
				procActionPlatformList(user, req, res, session);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	private boolean quitarReservaProductosTiendaVirtual(HttpServletRequest req, HttpSession session,
			ESS0030DSMessage user,String nroSol) throws ServletException, IOException{
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		boolean procesado=false;
		try {
			mp = getMessageProcessor("EPV1170", req);

			EPV117001Message msg = (EPV117001Message) mp.getMessageRecord("EPV117001", user.getH01USR(), "0030");//todos los productos a rechazar

			// Sets message with page fields
			msg.setE01PTVCUN(userPO.getCusNum());
			msg.setE01PTVNUM(nroSol);

			//Sending message
			mp.sendMessage(msg);			

			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			if (!list.getNoResult()){//data exists 
				list.initRow();	 
				TiendaVirtualSOAPProxy tvp = new TiendaVirtualSOAPProxy();	
				ArrayList<String[]> err = new ArrayList<String[]>();
				while (list.getNextRow()) {
					EPV117001Message prod = (EPV117001Message) list.getRecord();
					if (!"".equals(prod.getE01PTVCAM()) && !"".equals(prod.getE01PTVPRD()) && !"".equals(prod.getE01TVIFLG())){
						//llenamos servicio tienda virtual...
						ActualizaStockEntrada ent = new ActualizaStockEntrada();
						ent.setCodCampana(Integer.valueOf(prod.getE01PTVCAM())); 
						ent.setCodProducto(Integer.valueOf(prod.getE01PTVPRD()));						
						ent.setCantidad(Integer.valueOf(prod.getE01PTVCAN()));
						ent.setTipoOperacion(prod.getE01TVIFLG());//Se envia R=rechazo reserva
						ent.setIdEvaluacion(Integer.valueOf(prod.getE01PTVNUM()));																							
						RespActualizaStock resp = tvp.actualizaStock(ent);					
						if (resp.getCodRespuesta()==1){//0=OK y 1=noOK
							String[] valores={prod.getE01PTVCAM(),prod.getE01PTVPRD(),prod.getE01PTVCAN(),prod.getE01TVIFLG()};
							err.add(valores);
						}else{
							//nos conectamos al as4000 para informar que el registro fue rechazado satisfactoriamente
							mp = getMessageProcessor("EPV1170", req);
							EPV117001Message msgResult = (EPV117001Message) mp.getMessageRecord("EPV117001", user.getH01USR(), "0031");
							assignValue(prod,msgResult);
							//Sending message
							mp.sendMessage(msgResult);	
							//Receive error and data
							ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();	
							if (mp.hasError(msgError)) {
								// if there are errors go back to maintenance page and sho errors
								session.setAttribute("error", msgError);								
								return procesado;
							}							
						}							
					}
					
				}//EoW
				
				if (err.size()>0){
					String numErr = (err.size()>10)?"10":String.valueOf(err.size());
		            ELEERRMessage msgError = new ELEERRMessage();
		            msgError.setERRNUM(numErr);
		            int fil=1;
		            for (Iterator<String[]> iterator = err.iterator(); iterator.hasNext();) {
		            	String fila = (fil>9)?""+fil:"0"+fil;
		            	String[] val = (String[]) iterator.next();
		            	String des = "Error Actualizar Stock Tienda Virtual Prod:"+val[1]+" Cant:"+val[2]+" Oper:"+val[3];//55
		            	msgError.getField("ERNU"+fila).setString(fila);//numero error
		            	msgError.getField("ERDS"+fila).setString(des);//descripcion error
					}
		            session.setAttribute("error", msgError);
				}else{
					//existen productos a actualizar y los proceso bien.
					procesado=true;
				}
				
			}else{
				//no existen productos a actualizar y proceso bien.
				procesado=true;
			}
			  


		}
		catch (Exception e) {
			String className = e.getClass().getName();
			String description = e.getMessage() == null ? "Exception General" : e.getMessage();					
            ELEERRMessage msgError = new ELEERRMessage();				
			msgError.setERRNUM("3");
            msgError.setERNU01("01");//4
            msgError.setERDS01("ERROR AL ACCEDER SERVICIO DE TIENDA VIRTUAL. VERIFIQUE COMUNICACION.");
            msgError.setERNU02("02");//4
            msgError.setERDS02(className);
            msgError.setERNU03("03");//4
            msgError.setERDS03(description.length() > 70 ? description.substring(0, 70) : description);	
			session.setAttribute("error", msgError);						
		}		
		finally {
			if (mp != null)
				mp.close();
		}		
		return procesado;
		
	}
	
	private void assignValue(EPV117001Message source, EPV117001Message destination){
		destination.setE01PTVCUN(source.getE01PTVCUN());
		destination.setE01PTVNUM(source.getE01PTVNUM());
		destination.setE01PTVSEQ(source.getE01PTVSEQ());
		destination.setE01PTVCAM(source.getE01PTVCAM());
		destination.setE01PTVNCA(source.getE01PTVNCA());
		destination.setE01PTVPRD(source.getE01PTVPRD());
		destination.setE01PTVCTV(source.getE01PTVCTV());
		destination.setE01PTVNME(source.getE01PTVNME());
		destination.setE01PTVMAR(source.getE01PTVMAR());
		destination.setE01PTVMOD(source.getE01PTVMOD());
		destination.setE01PTVVAU(source.getE01PTVVAU());
		destination.setE01PTVCAN(source.getE01PTVCAN());
		destination.setE01PTVSTO(source.getE01PTVSTO());
		destination.setE01PTVCEN(source.getE01PTVCEN());
		destination.setE01PTVPTO(source.getE01PTVPTO());
		destination.setE01PTVOPE(source.getE01PTVOPE());
		destination.setE01PTVRSP(source.getE01PTVRSP());
		destination.setE01NUMREC(source.getE01NUMREC());
		destination.setE01TVINUM(source.getE01TVINUM());
		destination.setE01TVIFLG(source.getE01TVIFLG());               
	}
	
	protected void procReqPlatformConsultaSinacofi(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {
		

		try {
				//borramos el anterior objeto Sinacofi de la session
				session.removeAttribute("objSinac");	
				//
				String rutP = req.getParameter("E01CUSIDE");					
				String rut_s = rutP.substring(0, rutP.length()-1);				
				String dv_s = rutP.substring(rutP.length()-1);				

				String serie = req.getParameter("E01CUSIDS");											
				//
				DatosConsulta parameters = new DatosConsulta();			
				parameters.setRut(rut_s);
				parameters.setDv(dv_s);
				parameters.setSerie(serie);
				parameters.setConsultas("1,2,16,82");
				//				
				SinacofiWSPortProxy wsp = new SinacofiWSPortProxy();//inicializamos el servicio	
				Salida resp = wsp.sinacofiWS(parameters);	
				if (resp.getCodError()==null || "0".equals(resp.getCodError()) || "".equals(resp.getCodError().trim()) ){//no hay errores..
					session.setAttribute("objSinac", resp);	
				}else{
					String c = resp.getCodError();
					c =(c==null)?"N/D":c; 
					String d = resp.getDesError();
					d =(d==null)?"SIN RESPUESTA SERVICIO BUREAU":d;					
					ELEERRMessage msgError = new ELEERRMessage();
		            msgError.setERRNUM("1");
		            msgError.setERNU01((c.length() >4?(c.substring(0,4)):c));		//4		                
		            msgError.setERDS01((d.length() >70?(d.substring(0,70)):d));		//70
					session.setAttribute("error", msgError);										
				}
		
		}catch (Exception e1){
			e1.printStackTrace();
			String d = e1.getMessage();
			d=(d==null)?d=e1.toString():d;					
	        ELEERRMessage msgError = new ELEERRMessage();
            msgError.setERRNUM("2");
            msgError.setERNU01("01");										//4		                
            msgError.setERDS01("ERROR AL ACCEDER AL SERVICIO DE BUREAU. VERIFIQUE COMUNICACION.");		//70
	        msgError.setERNU02("02");
	        msgError.setERDS02((d.length() >70?(d.substring(0,70)):d));
			session.setAttribute("error", msgError);			
		}	
		
		forward("EPV1035_consulta_sinacofi.jsp", req, res);				
	}	


 }	



