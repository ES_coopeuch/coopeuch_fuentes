<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>


<%@page import="datapro.eibs.beans.UserPos"%><html>
<head>
<title>Consulta Plan Socio</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ESD404001List" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "userPOAux" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<jsp:useBean id= "planSocio" class= "datapro.eibs.beans.ESD404001Message"  scope="session" />
<jsp:useBean id= "servlet" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "paginaPS" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "operation" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "IDuser" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "NOBuser" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "banderaCV" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "banderaLH" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "tc" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "numCuentaCV" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "numCuentaTC" class= "java.lang.String"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<SCRIPT Language="Javascript">



//  Process according with user selection
 var bandera;	

function nada(){
	return;
}


</SCRIPT>

</head>

<body bgcolor="#FFFFFF" onload="">

<h3 align="center">Formularios<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="plan_socio_new, ESD4040"  ></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4040" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
  <INPUT TYPE=HIDDEN NAME="APPROVAL" VALUE="N">
  <INPUT TYPE=HIDDEN NAME="tarejetaCredito" VALUE="N">
  <input type="hidden" name="PlanPago" size="2" maxlength="1" value="0">
  <input type="hidden" name="E01FL5" size="2" maxlength="1" value="<%= client.getE01FL5().trim()%>">
  <input type="hidden" name="E01LGT" size="2" maxlength="1" value="<%= client.getE01LGT().trim()%>">
  <input type="hidden" name="cuentaVista" size="2" maxlength="1" value="N">
  <input type="hidden" name="libretaAhorro" size="2" maxlength="1" value="N">
  <input type="hidden" name="TDC" size="2" maxlength="1" value="N">  
  <input type="hidden" name="E01PSPRODUC" size="2" maxlength="1" value="">
  <input type="hidden" name="contTD" size="10" maxlength="2" value="">  
  <input type="hidden" name="contLH" size="2" maxlength="2" value="">
  <input type="hidden" name=paginaPS value="<%= paginaPS %>" size=15 maxlength=9 >
  <input type="hidden" name=IDuser value="<%= IDuser %>" size=15 maxlength=9 >
  <input type="hidden" name=NOBuser value="<%= NOBuser %>" size=15 maxlength=9 >
  <input type="hidden" name=operation value="<%= NOBuser %>" size=15 maxlength=9 >
  <input type="hidden" name=banderaLH value="<%= banderaLH %>" size=15 maxlength=9 >
  <input type="hidden" name=banderaCV value="<%= banderaCV %>" size=15 maxlength=9 >
  <input type="hidden" name="servlet" value="<%= servlet %>" size=15 maxlength=9 >
  <input type="hidden" name="E01CUN" value="<%= userPO.getCusNum() %>" size=15 maxlength=9 >
  <input type="hidden" name="E01IDN" value="<%= userPO.getHeader2() %>" size=15 maxlength=9 >
  <input type="hidden" name="nombreCliente" value="<%= userPO.getHeader3() %>" size=15 maxlength=9 >
  <input type="hidden" name="TC" value="<%= tc %>" size=15 maxlength=9 >
  <input type="hidden" name="TIENEPLAN" value="<%= planSocio.getL01FILLE4() %>" size=15 maxlength=9 >
  <input type="hidden" name="numCuentaCV" value="<%= numCuentaCV %>" size=15 maxlength=9 >
  <input type="hidden" name="numCuentaTC" value="<%= numCuentaTC %>" size=15 maxlength=9 >
  <input type="hidden" name="estadoTC" value="<%= planSocio.getE01PSPATC() %>" size=15 maxlength=9 >
  
  <br>
  <h4 align="center">Datos del Socio</h4>
  <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">Id Plan: </td>
					<td nowrap width="12%" aling="left" > <%=planSocio.getE01PSNPL() %> </td>
						
					<td nowrap width="6%" aling="right">Cliente: </td>
					<td nowrap width="14%" aling="left" > <%=userPOAux.getCusNum() %> </td>
					
					<td nowrap width="8%" aling="right">Nombre: </td>					
					<td nowrap width="20%" aling="left" > <%=userPOAux.getHeader3() %> </td>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
	<br>
    <h4 align="center">Recuerde imprimir formularios y solicitar al socio su firma</h4>
    <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center" border = "0">
				<tr>
					<td nowrap width="10%" aling="right">Cuenta Coopeuch </td>
					<% //if( planSocio.getE01PSPACV().equals("S") || planSocio.getE01PSPACV().equals("s") ){
						if( planSocio.getE01PSPACV().equals("S")){
						%>
						<td nowrap name="planSocio" property="E01PSPACV" width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
						<input type="hidden" name="TIENECV" value="S" size=15 maxlength=9 >
						</td> 
					<% 
						
					}else{
						%>
					 	<td nowrap name="planSocio" property="E01PSPACV" width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" > 
					 	<input type="hidden" name="TIENECV" value="N" size=15 maxlength=9 >
					 	</td> 
					<% 
					}
					%>
						
					<td nowrap width="6%" aling="right">Cuenta Ahorro </td>
					<% //if(planSocio.getE01PSPALA().equals("S") || planSocio.getE01PSPALA().equals("s")){ 
					   if( planSocio.getE01PSPALA().equals("S")){
						%>
						<td nowrap name="planSocio" property="E01PSPALA" width="12%" aling="left" ><img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
						<input type="hidden" name="TIENELA" value="S" size=15 maxlength=9 >
						</td>
						
					<%
					} else {
					%>
						<td nowrap name="planSocio" property="E01PSPALA" width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" > 
						<input type="hidden" name="TIENELA" value="N" size=15 maxlength=9 >
						</td>
					<%} %>
					
					<td nowrap width="8%" aling="right">Tarjeta de Credito </td>
					<% if(planSocio.getE01PSPATC().equals("S")){ %>
						<td nowrap name="planSocio" property="E01PSPATC" width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >  
						<input type="hidden" name="TIENETC" value="S" size=15 maxlength=9 >
						</td>
					<%} else {
					%>
						<td nowrap name="planSocio" property="E01PSPATC" width="12%" aling="left" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" > 
						<input type="hidden" name="TIENETC" value="N" size=15 maxlength=9 >
						</td>
					<%} %>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
    <br>
    <h4>Cobro</h4>
    <table  class="tableinfo">
  		<tr bordercolor="#FFFFFF">
  			<td nowrap>
	  			<table cellspacing="0" cellpadding="2" width="100%" border="0">
 					<tr id="trclear">
  						<td nowrap width="20%">
  							<div align="right">Plan Tarifario:</div>
  						</td>
  						<td nowrap width="20%" colspan="5">
  							<%=planSocio.getE01PSDCCP() %>
                  		</td>
                  		<td width="20%">
  							<div align="right">Valor a Pagar :</div>
  						</td>
  						<td width="20%">
  							<%=planSocio.getE01PSVBP() %> UF
                  		</td>
  					</tr>
  				</table>
  			</td>
  		</tr>
  	</table>
  	
  	<br>
    
	<table width="100%">		
  	<tr>
	  		<td width="15%">
	  		  <div align="center"> 
	     		<!--<input id="EIBSBTN" type="button" name="Submit" value="Cancelar" onClick="javascript:goAction(3);"> -->
	     		
	     	  </div>	
	  		</td>
			<td width="75%">
  		  		<div align="center">
				<input id="EIBSBTN" type="button" name="Submit2" value="Cancelar" onClick="javascript:goAction(3);">
     	  	 	</div>	
	  		</td>
		
  	</tr>	
</table>	

</form>
</body>
</html>

