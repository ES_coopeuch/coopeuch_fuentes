package datapro.eibs.salesplatform;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.coopeuch.core.tiendavirtual.ActualizaStockEntrada;
import cl.coopeuch.core.tiendavirtual.RespActualizaStock;
import cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtualSOAPProxy;

import com.adobe.fdf.FDFDoc;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.FdfReader;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100500Message;
import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV103801Message;
import datapro.eibs.beans.EPV117001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.reports.JSEFRM000PDF;
import datapro.eibs.sockets.MessageRecord;

public class JSEPV1038E extends JSEFRM000PDF {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;
	 private static final String nombreBucket = "credito-consumo-planilla";
	    private static final String accessKeyId = "AKIAIXSQFZ6EKSKROA7A";
	    private static final String accessKeySecret = "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";

	protected static final int A_PLATFORM_PDF = 100;
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		switch (screen) {
			case A_PLATFORM_PDF:
				procActionPDF(user, req, res, session);
				break;

			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}
	
	protected void procActionPDF(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		MessageProcessor mp = null;
		try 
		{
			mp = getMessageProcessor("EPV1038", req);

			EPV103801Message msg = (EPV103801Message) mp.getMessageRecord("EPV103801", user.getH01USR(), "0005");

			msg.setH01SCRCOD("01");
			msg.setH01USERID("SMALDONADO");
			msg.setH01FLGWK1("A");
			//DATOS VARIABLES
			msg.setE01PVMCUN("22409");
			msg.setE01PVMNUM("2921727");
			msg.setE01OFFOP1("04"); // CAJA
			msg.setE01OFFGL1("9910210110011200"); // CUENTA
			msg.setE01OFFBK1("01"); // BANCO
			msg.setE01OFFBR1("1"); // SUCURSAL
			msg.setE01OFFCY1("CLP"); 
			msg.setE01OFFAM1("1,000,000.00");
			 
			mp.sendMessage(msg);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			
			if (mp.hasError(msgError)) 
			{
				msg = (EPV103801Message) mp.receiveMessageRecord();
			} 
			else 
			{
				JBObjList list = mp.receiveMessageRecordList("E01MORFRM");
				if (!list.isEmpty()) 
				{
					FDFDoc outputFDF = new FDFDoc();
					initDataStructure();
						
                    int index = 0;
                    String prefix = "";
					// add General Information like User ID....
    				getFormData(outputFDF, (String)formatNames.get("ESS0030DS") + ".", user);

    				// add forms information...
					MessageRecord newmsg = mp.receiveMessageRecord();

					String ddsName = newmsg.getFormatName();
					while (true) 
					{
						if (!ddsName.equals(newmsg.getFormatName())) 
						{
							index = (hasIndex(newmsg.getFormatName())) ? 1 : 0;
						}
						prefix = getPrefix(newmsg);
						ddsName = newmsg.getFormatName();
                        if (ddsName.equals("EFRM00001"))
                            break;
                        else                             	
                    		buildFormList(outputFDF, newmsg, "." + prefix, index++);
                        
                        newmsg = mp.receiveMessageRecord();
					}
					
					
					//
					String urlPDF = JSEIBSProp.getFORMPDFURL() + "SET_FIRMA_CONS.PDF";

					try 
					{

					outputFDF = new FDFDoc(((FDFDoc)outputFDF).Save());
					outputFDF.SetFile(urlPDF);

					res.setContentType("application/vnd.fdf");

					UUID uuid = UUID.randomUUID();
					String filename = uuid.toString();
					
					OutputStream file = new FileOutputStream("C:\\" + filename + ".fdf");

					outputFDF.Save(file);
					
					PdfReader pdfreader = new PdfReader(urlPDF);
				    PdfStamper stamp = new PdfStamper(pdfreader, new FileOutputStream("C:\\registered_fdf.pdf"));
				    FdfReader fdfreader = new FdfReader("C:\\" + filename + ".fdf");
				    AcroFields form = stamp.getAcroFields();
				    form.setFields(fdfreader);
				    stamp.close();

				    File archivoGuardado = new File("C:\\registered_fdf.pdf");
				    String rut = "109878057";
				    AmazonS3 amazonS3 = new AmazonS3Client(new BasicAWSCredentials(accessKeyId, accessKeySecret));
			        PutObjectResult resultado = amazonS3.putObject(nombreBucket, "documentos-socio/" + rut + "/set-firmas/3043042433.pdf", archivoGuardado);

				    
				    
				    
					} catch(Exception e) {
						flexLog("Exception: " + e.getClass().getName() +  " --> Cause: " + e.getMessage());
						throw new RuntimeException("PDF Generator Error");
					}	
				}		
			}
		} finally {
			if (mp != null)  
				mp.close();
		}
	}	
	
//	
}	



