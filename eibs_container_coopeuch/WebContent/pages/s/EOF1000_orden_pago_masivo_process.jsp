<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>orden de pago masivo</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "opMasivo" class= "datapro.eibs.beans.EOF100001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

</head>

<body>
 <% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
<H3 align="center">Resultado Satifactorio - Carga de archivo <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="orden_pago_masivo_process.jsp, EOF1000"></H3>

<hr size="4">
<br>
  
<table class="tableinfo">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">     


		    <tr id="trdark">
		      <td width="30%"> 
		        <div align="right"> Codigo de Archivo Cargado : </div>
		      </td>
		      <td width="70%"> 
		        <div align="left">
		        <b>
		        <%= opMasivo.getE01CTCEID().trim()%> - <%= opMasivo.getE01CTCNME().trim()%> 
		        </b>
		        </div>
		      </td>
		    </tr>
		    <tr id="trclear">
		      <td width="30%"> 
		        <div align="right">Nombre del Archivo Cargado : </div>
		      </td>
		      <td width="70%"> 
		        <div align="left"> 
		        <b>
		        	<%= opMasivo.getE01CTCFNM().trim()%>
				</b>		        			       
		        </div>
		      </td>
		    </tr> 
        </table>                       
        </td>
    </tr>
  </table>

<br>
<table class="tableinfo" width="100%">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">     
		    <tr>
				<div align="center">
  					<h3>
  						<br>Archivo Cargado con exito.<br><br>Ahora Ejecute la opcion de Validacion de la Informacion
  					</h3>
  				 </div>		    
		      <td > 
		      </td>
		    </tr>
		 </table>
	  </td>
	</tr>
</table>

<br>

</body>
</html>
