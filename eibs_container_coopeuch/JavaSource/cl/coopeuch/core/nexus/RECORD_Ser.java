/**
 * RECORD_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * q0834.18 v82708152146
 */

package cl.coopeuch.core.nexus;

public class RECORD_Ser extends com.ibm.ws.webservices.engine.encoding.ser.BeanSerializer {
    /**
     * Constructor
     */
    public RECORD_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        RECORD bean = (RECORD) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_93;
          propValue = bean.getNum_tarjeta_s();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_36;
          propValue = new java.lang.Integer(bean.getCorrelativo());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_94;
          propValue = bean.getOrigen_trans();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_95;
          propValue = new java.lang.Integer(bean.getFec_trans());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_96;
          propValue = new java.lang.Integer(bean.getFec_posteo());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_97;
          propValue = new java.lang.Integer(bean.getCodigo_trans());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_98;
          propValue = bean.getTipo_trans();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_99;
          propValue = new java.lang.Double(bean.getMonto_compra());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_100;
          propValue = new java.lang.Integer(bean.getNumero_cuota());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_101;
          propValue = new java.lang.Integer(bean.getTotal_de_cuotas());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_102;
          propValue = new java.lang.Double(bean.getTasa());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_103;
          propValue = bean.getDescripcion_trans();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_104;
          propValue = bean.getGlosa_trans();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_105;
          propValue = bean.getMicrofilm23();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_106;
          propValue = new java.lang.Long(bean.getCdgo_comercio_tbk());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_107;
          propValue = bean.getCdgo_comercio_int();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_108;
          propValue = new java.lang.Integer(bean.getRubro());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_109;
          propValue = bean.getNombre_comercio();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_110;
          propValue = new java.lang.Long(bean.getRut_comercio());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_111;
          propValue = bean.getDvrut_comercio();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_112;
          propValue = new java.lang.Long(bean.getNro_solicitud());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_113;
          propValue = bean.getDesc_rubro();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_114;
          propValue = bean.getCodigo_pais_iata();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_115;
          propValue = bean.getCiudad();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_116;
          propValue = new java.lang.Integer(bean.getMoneda_fact_9());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_117;
          propValue = new java.lang.Integer(bean.getMoneda_origen_9());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_118;
          propValue = new java.lang.Double(bean.getMonto_mon_origen());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_119;
          propValue = bean.getCodaut();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_120;
          propValue = new java.lang.Integer(bean.getFecha_autorizador_s());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_121;
          propValue = new java.lang.Integer(bean.getHora_autorizador_s());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_122;
          propValue = new java.lang.Integer(bean.getEstado_trans());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_123;
          propValue = new java.lang.Double(bean.getMonto11());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_124;
          propValue = new java.lang.Double(bean.getMonto21());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_125;
          propValue = new java.lang.Double(bean.getMonto31());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_126;
          propValue = new java.lang.Long(bean.getNumero11());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_127;
          propValue = new java.lang.Long(bean.getNumero21());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_128;
          propValue = new java.lang.Long(bean.getNumero31());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_129;
          propValue = bean.getFlag11();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_130;
          propValue = bean.getFlag21();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_131;
          propValue = bean.getFlag31();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_132;
          propValue = bean.getString11();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_133;
          propValue = bean.getString21();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_134;
          propValue = bean.getString31();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_135;
          propValue = bean.getFiller_03();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
        }
    }
    private final static javax.xml.namespace.QName QName_0_125 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto31");
    private final static javax.xml.namespace.QName QName_0_132 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "string11");
    private final static javax.xml.namespace.QName QName_0_115 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "ciudad");
    private final static javax.xml.namespace.QName QName_0_108 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "rubro");
    private final static javax.xml.namespace.QName QName_0_119 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "codaut");
    private final static javax.xml.namespace.QName QName_0_130 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "flag21");
    private final static javax.xml.namespace.QName QName_0_114 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "codigo_pais_iata");
    private final static javax.xml.namespace.QName QName_0_113 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "desc_rubro");
    private final static javax.xml.namespace.QName QName_0_94 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "origen_trans");
    private final static javax.xml.namespace.QName QName_0_112 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "nro_solicitud");
    private final static javax.xml.namespace.QName QName_0_109 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "nombre_comercio");
    private final static javax.xml.namespace.QName QName_0_96 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "fec_posteo");
    private final static javax.xml.namespace.QName QName_0_111 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "dvrut_comercio");
    private final static javax.xml.namespace.QName QName_0_128 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero31");
    private final static javax.xml.namespace.QName QName_1_22 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "double");
    private final static javax.xml.namespace.QName QName_0_116 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "moneda_fact_9");
    private final static javax.xml.namespace.QName QName_0_95 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "fec_trans");
    private final static javax.xml.namespace.QName QName_0_120 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "fecha_autorizador_s");
    private final static javax.xml.namespace.QName QName_0_124 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto21");
    private final static javax.xml.namespace.QName QName_0_118 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto_mon_origen");
    private final static javax.xml.namespace.QName QName_0_97 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "codigo_trans");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "string31");
    private final static javax.xml.namespace.QName QName_0_101 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "total_de_cuotas");
    private final static javax.xml.namespace.QName QName_0_100 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero_cuota");
    private final static javax.xml.namespace.QName QName_0_129 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "flag11");
    private final static javax.xml.namespace.QName QName_1_7 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_117 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "moneda_origen_9");
    private final static javax.xml.namespace.QName QName_0_105 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "microfilm23");
    private final static javax.xml.namespace.QName QName_0_106 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "cdgo_comercio_tbk");
    private final static javax.xml.namespace.QName QName_0_98 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "tipo_trans");
    private final static javax.xml.namespace.QName QName_0_110 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "rut_comercio");
    private final static javax.xml.namespace.QName QName_0_135 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "filler_03");
    private final static javax.xml.namespace.QName QName_0_127 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero21");
    private final static javax.xml.namespace.QName QName_0_99 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto_compra");
    private final static javax.xml.namespace.QName QName_1_87 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "long");
    private final static javax.xml.namespace.QName QName_1_21 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_0_104 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "glosa_trans");
    private final static javax.xml.namespace.QName QName_0_123 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto11");
    private final static javax.xml.namespace.QName QName_0_133 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "string21");
    private final static javax.xml.namespace.QName QName_0_103 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "descripcion_trans");
    private final static javax.xml.namespace.QName QName_0_131 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "flag31");
    private final static javax.xml.namespace.QName QName_0_102 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "tasa");
    private final static javax.xml.namespace.QName QName_0_107 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "cdgo_comercio_int");
    private final static javax.xml.namespace.QName QName_0_122 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "estado_trans");
    private final static javax.xml.namespace.QName QName_0_121 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "hora_autorizador_s");
    private final static javax.xml.namespace.QName QName_0_126 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero11");
    private final static javax.xml.namespace.QName QName_0_93 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "num_tarjeta_s");
    private final static javax.xml.namespace.QName QName_0_36 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "correlativo");
}
