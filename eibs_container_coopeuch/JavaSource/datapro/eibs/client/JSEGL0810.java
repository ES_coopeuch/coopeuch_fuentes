package datapro.eibs.client;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EGL081001Message;
import datapro.eibs.beans.EGL081002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.Util;

/**
 * @author cmoran
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JSEGL0810 extends JSEIBSServlet {

	protected static final int R_SELECTION = 100;
	protected static final int A_SELECTION = 200;
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_SELECTION:
				procReqSelection(user, req, res, session);
				break;
			case A_SELECTION:
				procActionSelection(user, req, res, session);
				break;
			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

	private void procActionSelection(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		UserPos userPO = getUserPos(session);
		userPO.setBank(req.getParameter("E01TRABNK") == null ? "" : req.getParameter("E01TRABNK"));
		userPO.setBranch(req.getParameter("E01TRABRN") == null ? "" : req.getParameter("E01TRABRN"));
		userPO.setCurrency(req.getParameter("E01TRACCY") == null ? "" : req.getParameter("E01TRACCY").toUpperCase());
		userPO.setAccNum(req.getParameter("E01TRAGLN") == null ? "" : req.getParameter("E01TRAGLN"));
		userPO.setIdentifier(req.getParameter("E01TRAACC") == null ? "" : req.getParameter("E01TRAACC"));
		
		procReqList(user, req, res, session);
	}

	private void procReqList(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		UserPos userPO = getUserPos(session);
		
		String pageName = "";
		int startPos = 0;
		try {
			startPos = Integer.parseInt(req.getParameter("Pos"));
		} catch (Exception e) {
			startPos = 0;
		}
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EGL0810", req);
			EGL081001Message msgSearch = (EGL081001Message) mp.getMessageRecord("EGL081001", user.getH01USR(), "0004");
			msgSearch.setH01SCRCOD("01");
			msgSearch.setE01NUMREC("" + startPos);
			msgSearch.setE01TRABNK(userPO.getBank());
			msgSearch.setE01TRABRN(userPO.getBranch());
			msgSearch.setE01TRACCY(userPO.getCurrency());
			msgSearch.setE01TRAGLN(userPO.getAccNum());
			msgSearch.setE01TRAACC(userPO.getIdentifier());
			
			mp.sendMessage(msgSearch);
			
			JBObjList list = mp.receiveMessageRecordList("E01INDOPE");
			ELEERRMessage msgError = new ELEERRMessage();
			
			if (mp.hasError(list)) {
				msgError = (ELEERRMessage) mp.getError(list);
				pageName = "EGL0810_st_selection.jsp";
			} else {
				list.initRow();
				if (list.getNextRow()) {
					EGL081002Message msgHeader = (EGL081002Message) list.getRecord();
					session.setAttribute("stGLBal", msgHeader);
				}
				JBList beanList = new JBList();
				boolean firstTime = true;
				StringBuffer myRow = null;
				String strDebit = "";
				String strCredit = "";
				BigDecimal debit = new BigDecimal("0.00");
				BigDecimal credit = new BigDecimal("0.00");
				while (list.getNextRow()) {
					EGL081001Message msgList = (EGL081001Message) list.getRecord();
					if (firstTime) {
						firstTime = false;
						int pos = 0;
						try {
							pos = Integer.parseInt(msgList.getE01NUMREC());
						} catch (Exception e) {
							pos = 0;
						}
						beanList.setFirstRec(pos);
					}
					if (msgList.getE01TRADCC().equals("0")) {
						debit = debit.add(msgList.getBigDecimalE01TRAAMT());
						strDebit = Util.fcolorCCY(msgList.getE01TRAAMT());
						strCredit = "&nbsp;";
					} else if (msgList.getE01TRADCC().equals("5")) {
						credit = credit.add(msgList.getBigDecimalE01TRAAMT());
						strCredit = Util.fcolorCCY(msgList.getE01TRAAMT());
						strDebit = "&nbsp;";
					}
					myRow = new StringBuffer("<TR>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\">"
							+ Util.formatDate(msgList
									.getE01DATE11(), msgList
									.getE01DATE12(), msgList
									.getE01DATE13()) + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\">"
							+ Util.formatDate(msgList
									.getE01DATE21(), msgList
									.getE01DATE22(), msgList
									.getE01DATE23()) + "</TD>");

					myRow.append("<TD NOWRAP ALIGN=\"CENTER\">"
							+ Util.formatCell(msgList.getE01TRABTH())
							+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\">"
							+ Util.formatCell(msgList.getE01TRACDE())
							+ "</TD>");

					myRow.append("<TD NOWRAP ALIGN=\"LEFT\">"
							+ Util.formatCell(msgList.getE01TRANAR())
							+ "</TD>");

					myRow.append("<TD NOWRAP ALIGN=RIGHT>" + strDebit
							+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>" + strCredit
							+ "</TD>");
					myRow.append("</TR>");

					beanList.addRow("", myRow.toString());
				}
				beanList.setShowNext(list.getShowNext());
				userPO.setHeader19(Util.fcolorCCY(debit.toString()));
				userPO.setHeader20(Util.fcolorCCY(credit.toString()));
				
				session.setAttribute("glList", beanList);
				
				pageName = "EGL0810_st_list_fp.jsp";
			}
			
			flexLog("Putting java beans into the session");
			session.setAttribute("userPO", userPO);
			session.setAttribute("error", msgError);
			
			forward(pageName, req, res);
		
		} finally {
			if (mp != null)	mp.close();
		}
	}

	private void procReqSelection(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = new UserPos();
		userPO.setOption("GL");
		userPO.setPurpose("STATEMENT");
		session.setAttribute("userPO", userPO);
		session.setAttribute("error", new ELEERRMessage());
		
		forward("EGL0810_st_selection.jsp", req, res);
	}

}
