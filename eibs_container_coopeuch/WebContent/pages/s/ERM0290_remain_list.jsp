<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<html>
<head>
<title>Informacion Basica</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="JavaScript">
function addDate(){
date = new Date();
var month = date.getMonth()+1;
var day = date.getDate();
var year = date.getFullYear();


if (document.getElementById('fecha1').value == ''){
document.getElementById('fecha1').value = day;
}

if (document.getElementById('fecha2').value == ''){
document.getElementById('fecha2').value = month;
}


if (document.getElementById('fecha3').value == ''){
document.getElementById('fecha3').value = year;
}

if (document.getElementById('fecha4').value == ''){
document.getElementById('fecha4').value = day;
}


if (document.getElementById('fecha5').value == ''){
document.getElementById('fecha5').value = month;
}

if (document.getElementById('fecha6').value == ''){
document.getElementById('fecha6').value = year;
}


}

</script>


<jsp:useBean id= "RteList" class= "datapro.eibs.beans.ERM029001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" 	scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" 	scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage"  scope="session" />


</head>

<body bgcolor="#FFFFFF"  >

<%if (!error.getERRNUM().equals("0")) {
	error.setERRNUM("0");
	out.println("<SCRIPT Language=\"Javascript\">");
	out.println("       showErrors()");
	out.println("</SCRIPT>");
	}
 
%>





<h3 align="center">Parámetros Generales de Control Remanentes<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="remain_list, ERM0290"></h3>
<hr size="4">
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERM0290">
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="300"> 


 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="left" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">   Banco : 
              </td>
             <td nowrap width="5%" align="left">
	  			<input type="text" name="E01REMBNK" value="<%=RteList.getE01REMBNK()%>" 
	  			size="2"  readonly>
             </td>          
          <td nowrap width="10%" align="left">
	  			<input type="text" name="D01REMBNK" value="<%=RteList.getD01REMBNK()%>" 
	  			size="50"  readonly>
             </td> 
         
         </tr>
         
      
        </table>
      </td>
    </tr>
  </table>




<h4>Cuentas Contables Tránsito</h4>

<table class="tableinfo" cellspacing="0" cellpadding="2" width="100%"border="0">


	<tr id="trclear">		
		<td nowrap width="30%">
<div align="right">Capitalización:</div>
	  </td>
		<td nowrap width="30%">
		
	     <input type="text" name="E01REMGL1" onKeypress="enterInteger()" size="17" maxlength="16" value="<%= RteList.getE01REMGL1().trim()%>">
          <a href="javascript:GetLedger('E01REMGL1',document.forms[0].E01REMBNK.value,'CLP','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absbottom" border="0" ></a> 
	    
	
	<eibsinput:text  property="D01REMGL1" name="RteList" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>"/>
		</td>
        <td nowrap width="25%">
          		
       
        </td> 
        		<td nowrap width="5%">
		</td>
	</tr>
	<tr id="trdark">
			<td nowrap width="30%">
<div align="right">Abono Morosos:</div>
	  </td>
		<td nowrap width="30%">
	          	
	          	   <input type="text" name="E01REMGL2" onKeypress="enterInteger()" size="17" maxlength="16" value="<%= RteList.getE01REMGL2().trim()%>">
          <a href="javascript:GetLedger('E01REMGL2',document.forms[0].E01REMBNK.value,'CLP','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absbottom" border="0" ></a> 
	          	
	          	
			<eibsinput:text  property="D01REMGL2" name="RteList" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>"  />
		
		</td>
        <td nowrap width="25%">
          	
       
        </td> 
        		<td nowrap width="5%">

		</td>
	</tr>
	
	


	<tr id="trclear">
			<td nowrap width="30%">
<div align="right">Abono Cuenta Vista:</div>
	  </td>
		<td nowrap width="30%">

   <input type="text" name="E01REMGL3" onKeypress="enterInteger()" size="17" maxlength="16" value="<%= RteList.getE01REMGL3().trim()%>">
          <a href="javascript:GetLedger('E01REMGL3',document.forms[0].E01REMBNK.value,'CLP','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absbottom" border="0" ></a> 

		<eibsinput:text  property="D01REMGL3" name="RteList" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>"  />
		
		</td>
        <td nowrap width="25%">
          		
       
        </td> 
        		<td nowrap width="5%">

		</td>
	</tr>
	
	
	
	
	
		<tr id="trdark">
			<td nowrap width="30%">
<div align="right">Transferencia Electrónica:</div>
	  </td>
		<td nowrap width="30%">
	          	
	          	   <input type="text" name="E01REMGL4" onKeypress="enterInteger()" size="17" maxlength="16" value="<%= RteList.getE01REMGL4().trim()%>">
          <a href="javascript:GetLedger('E01REMGL4',document.forms[0].E01REMBNK.value,'CLP','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absbottom" border="0" ></a> 
		
		<eibsinput:text  property="D01REMGL4" name="RteList" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>"  />
		</td>
        <td nowrap width="25%">
          		
       
        </td> 
        		<td nowrap width="5%">

		</td>
	</tr>
	
		<tr id="trclear">
			<td nowrap width="30%">
<div align="right">Pago otro Banco:</div>
	  </td>
		<td nowrap width="30%">
	          		
	          		   <input type="text" name="E01REMGL5" onKeypress="enterInteger()" size="17" maxlength="16" value="<%= RteList.getE01REMGL5().trim()%>">
          <a href="javascript:GetLedger('E01REMGL5',document.forms[0].E01REMBNK.value,'CLP','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absbottom" border="0" ></a> 
		
		<eibsinput:text  property="D01REMGL5" name="RteList" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>"  />
  
		
		</td>
		
		
        <td nowrap width="25%">
          		     
        </td> 
        		<td nowrap width="5%">

		</td>
	</tr>	
	

	</table>
	
	
	
	<h4>Capitalización Voluntaria</h4>

<table class="tableinfo" cellspacing="0" cellpadding="2" width="100%"border="0">


	<tr id="trclear">		
		<td nowrap width="30%">
  Fecha Inicio:
  			
                <input type="text" name="E01REMIDD" id="fecha1" size="3" maxlength="2" value="<%=RteList.getE01REMIDD()%>">
                <input type="text" name="E01REMIDM" id="fecha2" size="3" maxlength="2" value="<%=RteList.getE01REMIDM()%>">
                <input type="text" name="E01REMIDY" id="fecha3" size="5" maxlength="4" value="<%=RteList.getE01REMIDY()%>">
                <a href="javascript:DatePicker(document.forms[0].E01REMIDD,document.forms[0].E01REMIDM,document.forms[0].E01REMIDY)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
	  </td>
		<td nowrap width="30%">
	          		
	       
	          		
	          	
	          		</td>
		
        <td nowrap width="25%">
     		Fecha Hasta:
                <input type="text" name="E01REMFDD" id="fecha4" size="3" maxlength="2" value="<%=RteList.getE01REMFDD()%>">
                <input type="text" name="E01REMFDM" id="fecha5" size="3" maxlength="2" value="<%=RteList.getE01REMFDM()%>">
                <input type="text" name="E01REMFDY" id="fecha6" size="5" maxlength="4" value="<%=RteList.getE01REMFDY()%>">
                <a href="javascript:DatePicker(document.forms[0].E01REMFDD,document.forms[0].E01REMFDM,document.forms[0].E01REMFDY)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
          	
        </td> 
        		<td nowrap width="5%">
		</td>
	</tr>

	

	</table>
	  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
	</FORM>
	
	
	</body>
	
	</html>
	
	