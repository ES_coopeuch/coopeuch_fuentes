package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Creado por              :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  14/02/2013                                                                                      **/
/**  Objetivo                :  Proceso de Orden de Pago Masivo                                                                 **/
/*********************************************************************************************************************************/

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.PropertyResourceBundle;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jspsmart.upload.SmartUpload;
import com.jspsmart.upload.SmartUploadException;


import datapro.eibs.beans.EOF100001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.ServiceLocator;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;

public class JSEOF1000 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_OP_MASIVO_ENTER = 100;	
	protected static final int A_OP_MASIVO_ENTER = 200;
	
	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		screen =  A_OP_MASIVO_ENTER;
		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}		
		switch (screen) {
		case R_OP_MASIVO_ENTER:
			procReqOPMasivoEnter(user, req, res, session);
			break;
		case A_OP_MASIVO_ENTER:
			procActionOPMasivoEnter(user, req, res, session);
			break;		
			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}
	/**
	 * procReqOPMasivoEnter
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqOPMasivoEnter(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: EOF1000_orden_pago_masivo_enter.jsp");
			forward("EOF1000_orden_pago_masivo_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	
	protected void procReqAplicaOPMasivoEnter(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: EOF1000_aplica_orden_pago_masivo_enter.jsp");
			forward("EOF1000_aplica_orden_pago_masivo_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	
	/**
	 * procActionOPMasivoEnter: find the list of forms depending on status, the program will epvl1005
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionOPMasivoEnter(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		boolean ok = false;
		try {
			SmartUpload mySmartUpload = new SmartUpload();
			com.jspsmart.upload.File myFile = null;
			try {
				mySmartUpload.initialize(config, req, res);
				mySmartUpload.upload();
				myFile = mySmartUpload.getFiles().getFile(0);
				if (myFile.getSize() > 0) {
					//El archivo tiene datos buscamos el nombre para subir el archivo.					
					mp = getMessageProcessor("EOF1000", req);					
					EOF100001Message msg = (EOF100001Message) mp.getMessageRecord("EOF100001");
					//seteamos las propiedades
					msg.setH01USERID(user.getH01USR());
					msg.setH01OPECOD("0002");
					msg.setH01TIMSYS(getTimeStamp());					
					msg.setH01SCRCOD("01");	
					String E01CTCEID = mySmartUpload.getRequest().getParameter("E01CTCEID");
			//		String E01BRMCTA = mySmartUpload.getRequest().getParameter("E01BRMCTA");					
					msg.setE01CTCEID(E01CTCEID);//concepto
			//		msg.setE01BRMCTA(E01BRMCTA);//cuenta
					
					//Sending message
					mp.sendMessage(msg);
		
					//Receive error and data
					msgError = (ELEERRMessage) mp.receiveMessageRecord();
					msg = (EOF100001Message) mp.receiveMessageRecord();		
					
					session.setAttribute("opMasivo", msg);
					
					//havent errors i get the field
					if (!mp.hasError(msgError)) {
						String fileName=  msg.getE01CTCFNM();//nombre del archivo					
						byte[] bd = new byte[myFile.getSize()];
						for (int i = 0; i < myFile.getSize(); i++) {
							bd[i] = myFile.getBinaryData(i);
						}
						InputStream  input = new ByteArrayInputStream(bd);													
						String userid = ServiceLocator.getSlInfo().getString("jdbc.cnx.userid.eibs-server");
						String password = ServiceLocator.getSlInfo().getString("jdbc.cnx.password.eibs-server");												
						FTPWrapper ftp = new FTPStdWrapper(JSEIBSProp.getHostIP(), userid, password, "");
						if (ftp.open()) {
								//Mandamos todos los archivos de IMAGENES
								if (ftp.cdRemotePath(user.getH01USR().substring(0, 3)+"CYFILES")) {
									ftp.setFileType(FTPWrapper.ASCII);
									ftp.upload(input,fileName); 
									ok=true;//Aqui nos seguramos que todo esta Bien..
								}
						} else {
							msgError = new ELEERRMessage();
							msgError.setERRNUM("1");
							msgError.setERNU01("01");		                
							msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
						}
					}
					
				}else{
					//mandamos error en session 
					msgError = new ELEERRMessage();
					msgError.setERRNUM(new BigDecimal(1));
					msgError.setERDS01("Archivo Vacio...");							
				}
				
			}catch (Exception e) {
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();	
				msgError = new ELEERRMessage();			
				msgError.setERRNUM("3");
	            msgError.setERNU01("01");
	            msgError.setERDS01(className);
	            msgError.setERNU02("02");
	            msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
	            msgError.setERNU03("03");
	            msgError.setERDS03("Para mas informacion revizar los archivos de log.");
				e.printStackTrace();			
			} 	
			
			if (ok){									
				forward("EOF1000_orden_pago_masivo_process.jsp", req, res);
			}else{
				session.setAttribute("error", msgError);				
				forward("EOF1000_orden_pago_masivo_enter.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
 }	



