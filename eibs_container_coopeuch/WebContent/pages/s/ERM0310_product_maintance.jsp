<!-- Hecho por Alonso Arana ------Datapro-----15/05/2014 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<html>
<head>
<title>Formulario de Mantenci�n de Par�metros de control de Acreencias </title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="JavaScript">
function goAction2(op) {


if(op=='666'){
	document.forms[0].SCREEN.value = op;
			document.forms[0].submit();	
}

	}


</script>


<jsp:useBean id= "cnvObj" class= "datapro.eibs.beans.ERM031001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" 	scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" 	scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage"  scope="session" />


</head>
<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>

<body bgcolor="#FFFFFF"  >

<%if (!error.getERRNUM().equals("0")) {
	error.setERRNUM("0");
	out.println("<SCRIPT Language=\"Javascript\">");
	out.println("       showErrors()");
	out.println("</SCRIPT>");
	}
	
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
 
%>





<h3 align="center">  <%if (readOnly){ %>
	Consulta de Productos de Acreencias
<%} else if (maintenance){ %>
	Mantenci�n de  Productos de Acreencias
<%} else { %>
	Nuevo Producto de Acreencias <%} %> <img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="enter_inform, ERM0310"></h3>
<hr size="4">
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERM0310">
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="<%
 if(readOnly){
 out.print("999");
 } 
 
 if(maintenance){
 out.print("750");
 } 
  else{
   out.print("650");
  
  }
  
  
   %>"> 

 <input type="hidden" name="E01CHGACD"  value="97">
 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="left" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">   Banco : 
              </td>
             <td nowrap width="5%" align="left">
	  			<input type="text" name="E01ACRBNK" value="<%=session.getAttribute("codigo_banco")%>" 
	  			size="2"  readonly>
             </td>          
          <td nowrap width="10%" align="left">
	  			<input type="text" name="E01DESBNK" value="<%=session.getAttribute("desc_banco")%>" 
	  			size="50"  readonly>
             </td> 
         
         </tr>
         
      
        </table>
      </td>
    </tr>
  </table>



<h4>Par�metros</h4>

<table class="tableinfo" cellspacing="0" cellpadding="2" width="100%"border="0">


	
	<tr id="trdark">
			<td nowrap width="30%">
<div align="right">Producto:</div>
	  </td>
		<td nowrap width="30%">
	        
	     
	     	<% if (maintenance){  %>
	     	   <input type="text"  name="E01ACRCDE" readonly  size="7" maxlength="4" value="<%=cnvObj.getE01ACRCDE().trim()%>">
        
     
               
        
	     	<% }
	     	
	     	else {
	     	%>
	     	<input type="text"  name="E01ACRCDE" readonly  size="7" maxlength="4" value="<%=cnvObj.getE01ACRCDE().trim()%>">
         <a href="javascript:GetProduct('E01ACRCDE',document.forms[0].E01CHGACD.value,'','E01DESCDE')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0" ></a> 
        
	     	<% 
	     	}
	     	
	     	%>
	          
		
	          	  
	          	  
		</td>
        <td nowrap width="30%">
  Descripci�n:
  <input type="text" name="E01DESCDE" value="<%=cnvObj.getE01DESCDE()%>" 
	  			size="50"  readonly>
        </td> 
        		<td nowrap width="30%">
     
		</td>
	</tr>
	
	


	
	
		<tr id="trclear">
			<td nowrap width="30%">
<div align="right">Dias Retenci�n:</div>
	  </td>
		<td nowrap width="30%">

   <input type="text" name="E01ACRDRT" onKeypress="enterInteger()" size="6" maxlength="5" value="<%=cnvObj.getE01ACRDRT().trim()%>">
        
                
 		</td>
		
        <td nowrap width="25%">
        <div>
          		Dias Mora:
          <input type="text" name="E01ACRDMO"  onKeypress="enterInteger()" size="6" maxlength="5" value="<%=cnvObj.getE01ACRDMO().trim()%>">
       </div>
        </td> 
        		<td nowrap width="5%">

		</td>
	</tr>
	
			
	
	<tr id="trdark">
			<td nowrap width="30%">
<div align="right">Cargo Autom�tico:</div>
	  </td>
		<td nowrap width="30%">
	<select name="E01ACRAUT">
	    <% boolean flag = false; %>
                       <option value="S" <%if (cnvObj.getE01ACRAUT().equals("S")) { flag = true; out.print("selected"); }%>>SI</option>
                <option value="N" <%if (cnvObj.getE01ACRAUT().equals("N")) { flag = true; out.print("selected"); }%>>NO</option>
             
              </select>
             

		
		</td>
        <td nowrap width="25%">
          		
             <div>

    
     </div>
        </td> 
        		<td nowrap width="5%">

		</td>
	</tr>

	</table>
	
	
	<% if (maintenance){  %>
	<h4>Datos �ltima actualizaci�n</h4>

<table class="tableinfo" cellspacing="0" cellpadding="2" width="100%"border="0">


	<tr id="trclear">		
		<td nowrap width="30%">
  Fecha:
  			
                <input type="text" name="E01ACRLMD" readonly id="fecha1" size="3" maxlength="2" value="<%=cnvObj.getE01ACRLMD()%>">
                <input type="text" name="E01ACRLMM" readonly id="fecha2" size="3" maxlength="2" value="<%=cnvObj.getE01ACRLMM()%>">
                <input type="text" name="E01ACRLMY" readonly id="fecha3" size="5" maxlength="4" value="<%=cnvObj.getE01ACRLMY()%>">
               
	  </td>
		<td nowrap width="30%">
	          		
	       
	          		
	          	
	          		</td>
		
        <td nowrap width="25%">
     		Usuario:
                <input type="text" name="E01ACRLMU"  size="13" readonly  value="<%=cnvObj.getE01ACRLMU()%>">
               
	
          	
        </td> 
        		<td nowrap width="5%">
		</td>
	</tr>

	

	</table>
  <%} %>


	  <p align="center">
	    <input id="EIBSBTN" type=button name="Submit" value="Atr�s" onClick="javascript:goAction2('666')">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
  

	</FORM>
	
	
	</body>
	
	</html>
	
	