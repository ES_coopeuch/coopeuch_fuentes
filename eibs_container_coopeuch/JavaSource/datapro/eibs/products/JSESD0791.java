package datapro.eibs.products;

/**
 * Insert the type's description here.
 */
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import datapro.eibs.beans.*;

import datapro.eibs.sockets.*;

public class JSESD0791 extends datapro.eibs.master.SuperServlet {

	// entering options
	protected static final int R_CHG_DEAL_STATUS_ENTER 		= 100;
	protected static final int A_CHG_DEAL_STATUS_ENTER	 	= 200;
	protected static final int C_CHG_DEAL_STATUS_ENTER	 	= 300;

	protected String LangPath = "S";

/**
 * JSEGL0034 constructor comment.
 */
public JSESD0791() {
	super();
}
/**
 * This method was created by Orestes Garcia.
 */
public void destroy() {

	flexLog("free resources used by JSESD0790");
	
}
/**
 * This method was created by David Mavilla.
 */
public void init(ServletConfig config) throws ServletException {
	super.init(config);
}

/**
 * This method was created in VisualAge.
 */
protected void procReqChgDealStatusEnter(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

	ELEERRMessage msgError = null;
	UserPos	userPO = null;	
	ESD079101Message msgDeal = null;
	try {

		msgError = new datapro.eibs.beans.ELEERRMessage();
		userPO = new datapro.eibs.beans.UserPos();
		msgDeal = new datapro.eibs.beans.ESD079101Message();

		userPO.setOption("TRANSACTION");
		userPO.setPurpose("MAINTENANCE");
		
		ses.setAttribute("error", msgError);
		ses.setAttribute("userPO", userPO);
		ses.setAttribute("deal", msgDeal);
		
  	} catch (Exception ex) { 
		flexLog("Error: " + ex); 
  	}

	try {
		flexLog("About to call Page: " + LangPath + "ESD0791_req_change_deal_status_enter.jsp");
		callPage(LangPath + "ESD0791_req_change_deal_status_enter.jsp", req, res);	
	}
	catch (Exception e) {
		flexLog("Exception calling page " + e);
	}

}

/**
 * This method was created in VisualAge.
 */

protected void procActionChgDealStatusEnter(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
	
	MessageRecord newmessage = null;
	ESD079101Message msgDeal = null;
	ELEERRMessage msgError = null;
	UserPos	userPO = null;	
	boolean IsNotError = false;
	 
	try {
		msgError = new datapro.eibs.beans.ELEERRMessage();
	  	} 
	catch (Exception ex) {
		flexLog("Error: " + ex); 
  	}

	userPO = (datapro.eibs.beans.UserPos)ses.getAttribute("userPO");

	// Send Initial data
	try
	{
	 	msgDeal = (ESD079101Message)mc.getMessageRecord("ESD079101");
	 	msgDeal.setH01USERID(user.getH01USR());
	 	msgDeal.setH01PROGRM("ESD0791");
	 	msgDeal.setH01TIMSYS(getTimeStamp());
	 	msgDeal.setH01OPECOD("0001");
	 	try {msgDeal.setE01COTCDE(req.getParameter("E01COTCDE"));} 
		catch (Exception ex) {msgDeal.setE01COTCDE("");}
		
	 	try {msgDeal.setE01COTNUM(req.getParameter("E01COTNUM"));} 
		catch (Exception ex) {msgDeal.setE01COTNUM("0");}

	 	try {req.setAttribute("E01COTECU", req.getParameter("E01COTECU"));} 
		catch (Exception ex) {req.setAttribute("E01COTECU", req.getParameter("0"));}

		
		msgDeal.send();	

	}		
	catch (Exception e)	{
		e.printStackTrace();
		flexLog("Error: " + e);
	  	throw new RuntimeException("Socket Communication Error");
	}
	
	try
	{
	

		//Receive error and data
 	 	msgError = (ELEERRMessage) mc.receiveMessage();
	 	msgDeal = (ESD079101Message) mc.receiveMessage();
		IsNotError = msgError.getERRNUM().equals("0");

	 	//Sets session with required data
		try {
			if(!msgDeal.getE01COTNUM().equals("0") && IsNotError)
				msgDeal.setE01COTSTS("X");
				
		} 
		catch (Exception ex) {msgDeal.setE01COTSTS("X");}

		ses.setAttribute("userPO", userPO);
		ses.setAttribute("deal", msgDeal);

	 	if (IsNotError) {
	 		//if there are no errors go to maintenance page
			flexLog("About to call Page: " + LangPath + "ESD0791_req_change_deal_status_new.jsp");
	 		callPage(LangPath + "ESD0791_req_change_deal_status_new.jsp", req, res);	
	 	} 
	 	else 
	 	{
	 		//if there are errors go back to list page
			ses.setAttribute("error", msgError);
	 		flexLog("About to call Page: " + LangPath + "ESD0791_req_change_deal_status_enter.jsp");
	 		callPage(LangPath + "ESD0791_req_change_deal_status_enter.jsp", req, res);	
	 	}
 	}
	catch (Exception e)	{
		e.printStackTrace();
		flexLog("Error: " + e);
	  	throw new RuntimeException("Socket Communication Error");
	}
}

/**
 * This method was created in VisualAge.
 */
protected void procConChgDealStatusEnter(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
	
	MessageRecord newmessage = null;
	ESD079101Message msgDeal = null;
	ELEERRMessage msgError = null;
	UserPos	userPO = null;	
	boolean IsNotError = false;
	
	try {
		msgError = new datapro.eibs.beans.ELEERRMessage();
	  	} 
	catch (Exception ex) {
		flexLog("Error: " + ex); 
  	}

	userPO = (datapro.eibs.beans.UserPos)ses.getAttribute("userPO");

	// Send Initial data
	try
	{
	 	msgDeal = (ESD079101Message)mc.getMessageRecord("ESD079101");
	 	msgDeal.setH01USERID(user.getH01USR());
	 	msgDeal.setH01PROGRM("ESD0791");
	 	msgDeal.setH01TIMSYS(getTimeStamp());
	 	msgDeal.setH01OPECOD("0002");
	 	
	 	try {msgDeal.setE01COTCDE(req.getParameter("E01COTCDE"));} 
		catch (Exception ex) {msgDeal.setE01COTCDE("");}
	 	
		try {msgDeal.setE01COTNUM(req.getParameter("E01COTNUM"));} 
		catch (Exception ex) {msgDeal.setE01COTNUM("0");}
	

		try {msgDeal.setE01COTSTS(req.getParameter("E01COTSTS"));} 
		catch (Exception ex) {msgDeal.setE01COTSTS("X");}
		
		try {msgDeal.setE01COTMTV(req.getParameter("E01COTMTV"));} 
		catch (Exception ex) {msgDeal.setE01COTMTV("0");}

		try {msgDeal.setE01COTDMT(req.getParameter("E01COTDMT"));} 
		catch (Exception ex) {msgDeal.setE01COTDMT("");}
			
		try {msgDeal.setE01COTDET(req.getParameter("E01COTDET"));} 
		catch (Exception ex) {msgDeal.setE01COTDET("");}
			
		msgDeal.send();	

	}		
	catch (Exception e)	{
		e.printStackTrace();
		flexLog("Error: " + e);
	  	throw new RuntimeException("Socket Communication Error");
	}
	
	try
	{
	 	//Receive error and data
 	 	msgError = (ELEERRMessage) mc.receiveMessage();
	 	msgDeal = (ESD079101Message) mc.receiveMessage();
		IsNotError = msgError.getERRNUM().equals("0");

	 	//Sets session with required data
		ses.setAttribute("userPO", userPO);
		ses.setAttribute("deal", msgDeal);

	 	if (IsNotError) {
	 		//if there are no errors go to maintenance page
			flexLog("About to call Page: " + LangPath + "ESD0791_con_change_deal_status_new.jsp");
	 		callPage(LangPath + "ESD0791_con_change_deal_status_new.jsp", req, res);	
	 	} 
	 	else 
	 	{
	 		//if there are errors go back to list page
	 		ses.setAttribute("error", msgError);
	 		flexLog("About to call Page: " + LangPath + "ESD0791_req_change_deal_status_new.jsp");
	 		callPage(LangPath + "ESD0791_req_change_deal_status_new.jsp", req, res);	
	 	}
 	}
	catch (Exception e)	{
		e.printStackTrace();
		flexLog("Error: " + e);
	  	throw new RuntimeException("Socket Communication Error");
	}
}



public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
   
	MessageContext mc = null;

	ESS0030DSMessage msgUser = null;
  	HttpSession session = null;

	session = (HttpSession)req.getSession(false); 
	
	if (session == null) {
		try {
			res.setContentType("text/html");
			printLogInAgain(res.getWriter());
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception ocurred. Exception = " + e);
		}
	}
	else {

		int screen = R_CHG_DEAL_STATUS_ENTER;
		
		try {

			flexLog("Screen  Number: " + screen);
		
			msgUser = (datapro.eibs.beans.ESS0030DSMessage)session.getAttribute("currUser");

			// Here we should get the path from the user profile
			LangPath = super.rootPath + msgUser.getE01LAN() + "/";

			try
			{
				flexLog("Opennig Socket Connection");
				mc = new MessageContext(super.getMessageHandler("ESD0790", req));
			
			try {
				screen = Integer.parseInt(req.getParameter("SCREEN"));
			}
			catch (Exception e) {
				flexLog("Screen set to default value");
			}
		
			switch (screen) {
				case R_CHG_DEAL_STATUS_ENTER : 
					procReqChgDealStatusEnter(msgUser, req, res, session);
					break;
				case A_CHG_DEAL_STATUS_ENTER : 
					procActionChgDealStatusEnter(mc, msgUser, req, res, session);
					break;
				case C_CHG_DEAL_STATUS_ENTER : 
					procConChgDealStatusEnter(mc, msgUser, req, res, session);
					break;
				default :
					res.sendRedirect(super.srctx + LangPath + super.devPage);
					break;
			}

			} catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
					//return;
			} finally {
					mc.close();
			}

		}
		catch (Exception e) {
			flexLog("Error: " + e);
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}

	}
	
}
      
}