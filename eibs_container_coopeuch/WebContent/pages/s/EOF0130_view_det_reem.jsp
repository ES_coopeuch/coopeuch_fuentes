<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import = "java.lang.Object" %>
<HTML>
<HEAD>
<TITLE>
Vista detalle de reemplazo de cheques de terceros</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "VieDetReem" 		class="datapro.eibs.beans.EOF013501Message"  scope="session" />
<jsp:useBean id= "userPO"   class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error"    class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT Language="javascript">
</SCRIPT>

</HEAD>
<BODY>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
 	  error.setERRNUM("0");
      out.println("<SCRIPT Language=\"Javascript\">");
      out.println("       showErrors()");
      out.println("</SCRIPT>");
 }
%> 

<h3 align="center">Reemplazo de Cheques de Terceros<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="view_det_reem.jsp, EOF0130"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEOF0130">
   <table class="tableinfo">
   <tr > 
    	<td nowrap> 
        	<table cellspacing="0" cellpadding="2" width="100%" border="0"	class="tbhead">
				<tr id="trclear">
			    	<td nowrap width="10%"></td >
			        <td nowrap width="10%"><h4>CHEQUE # 1</h4></td >
			       	<td nowrap width="40%"><div align="right">Referencia :</div></td>
				    <td nowrap width="10%"><%= VieDetReem.getE01RMPRE1().trim()%></td >
			   	</tr>
			    
			   <tr id="trdark">
			   		<td></td >
			        <td></td >
			        <td><div align="right">Fecha :</div></td>
				    <td><%= VieDetReem.getE01RMPDVE().trim() + "/"+ VieDetReem.getE01RMPMVE().trim() + "/" + VieDetReem.getE01RMPAVE().trim() %></td >
			   </tr>
			   
			   <tr id="trdark">
			   		<td></td >
			        <td></td >
			        <td><div align="right">Monto :</div></td>
				    <td>$*****<%= VieDetReem.getE01RMPMT1().trim()%></td >
				</tr>
			    
			    <tr id="trdark"> 
			    	<td></td >
				    <td><div align="right">Pagar a la orden de :</div></td>
				    <td><%= VieDetReem.getE01RMPBN1().trim()%> </td>
				    <td></td>
			    </tr>
			    
			    <tr id="trdark">
			    	<td></td >
				    <td><div align="right">La suma de :</div></td>
				    <td><%= VieDetReem.getE01RMPLE1().trim()%></td>
				    <td></td>
			    </tr>
			          
			    <tr id="trdark">
			    	<td></td >
				    <td><div align="right">Concepto :</div></td>
		            <td><%= VieDetReem.getE01RMPCO1().trim()%></td>
		            <td></td>
		        </tr>
		        
		        <tr id="trdark"> 
		        	<td></td >
					<td><div align="right">Banco Emisor :</div></td>
		        	<td><%= VieDetReem.getE01RMPAD1().trim()%></td>
		        	<td></td>
		        </tr>

		        <tr id="trdark"> 
		        	<td></td >
		        	<td></td >
		        	<td></td >
		        	<td></td>
		        </tr>

		    </table>
		</td>
	</tr>	    		
	</table>

	<%if(VieDetReem.getE01RMPTYP().trim().equals("RME")){%>
	<table class="tableinfo">
    <tr > 
    	<td nowrap> 
        	<table cellspacing="0" cellpadding="2" width="100%" border="0"	class="tbhead">
				<tr id="trclear">
			    	<td nowrap width="10%"></td >
			        <td nowrap width="10%"><h4>CHEQUE # 2</h4></td >
			       	<td nowrap width="40%"><div align="right">Referencia :</div></td>
				    <td nowrap width="10%"><%= VieDetReem.getE01RMPRE2().trim()%></td >
			   	</tr>
			    
			   <tr id="trdark">
			   		<td></td >
			        <td></td >
			        <td><div align="right">Fecha :</div></td>
				    <td><%= VieDetReem.getE01RMPDVE().trim() + "/"+ VieDetReem.getE01RMPMVE().trim() + "/" + VieDetReem.getE01RMPAVE().trim() %></td >
			   </tr>
			   
			   <tr id="trdark">
			   		<td></td >
			        <td></td >
			        <td><div align="right">Monto :</div></td>
				    <td>$*****<%= VieDetReem.getE01RMPMT2().trim()%></td >
				</tr>
			    
			    <tr id="trdark"> 
			    	<td></td >
				    <td><div align="right">Pagar a la orden de :</div></td>
				    <td><%= VieDetReem.getE01RMPBN2().trim()%> </td>
				    <td></td>
			    </tr>
			    
			    <tr id="trdark">
			    	<td></td >
				    <td><div align="right">La suma de :</div></td>
				    <td><%= VieDetReem.getE01RMPLE2().trim()%></td>
				    <td></td>
			    </tr>
			          
			    <tr id="trdark">
			    	<td></td >
				    <td><div align="right">Concepto :</div></td>
		            <td><%= VieDetReem.getE01RMPCO2().trim()%></td>
		            <td></td>
		        </tr>
		        
		        <tr id="trdark"> 
		        	<td></td >
					<td><div align="right">Banco Emisor :</div></td>
		        	<td><%= VieDetReem.getE01RMPAD2().trim()%></td>
		        	<td></td>
		        </tr>
		        <tr id="trdark"> 
		        	<td></td >
		        	<td></td >
		        	<td></td >
		        	<td></td>
		        </tr>
		    </table>
		</td>
	</tr>	    		
	</table>
	<%}%>

	<%if(VieDetReem.getE01RMPTYP().trim().equals("RMA")){%>
   <table class="tableinfo">
    <tr > 
    	<td nowrap> 
        	<table cellspacing="0" cellpadding="2" width="100%" border="0"	class="tbhead">
				<tr id="trclear">
			    	<td nowrap width="10%"></td >
			        <td nowrap width="10%"><h4>EFECTIVO CAJA</h4></td >
			       	<td nowrap width="40%"><div align="right"></div></td>
				    <td nowrap width="10%"></td >
			   	</tr>
			    
 			   <tr id="trdark">
			   		<td></td >
			        <td></td >
			        <td><div align="right">Monto :</div></td>
				    <td>$*****<%= VieDetReem.getE01MTOEFE().trim()%></td >
				</tr>
		    </table>
		</td>
	</tr>	    		
	</table>
	<%}%>
</FORM>
</BODY>
</HTML>
