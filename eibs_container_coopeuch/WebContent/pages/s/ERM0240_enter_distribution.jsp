<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Sistema Bancario:Remanentes</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script language="JavaScript">

</script>

</head>

<body>
 
 <% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>
<p></p>
<H3 align="center">Distribución Utilidades<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="enter_distribution.jsp, ERM0240"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERM0240" 
onsubmit="" >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">

  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0"> 
    <tr id="trdark">
      <td width="50%"> 
   
      </td>
    </tr>

  	<tr id="trdark"> 
        <td align=CENTER width="50%"> 
          <div align="right">Tipo a Distribuir :</div>
          
        </td>
        <td align=left width="50%"> 
      <select name="E01FLGPRC">
                       <option value="FA">Fallecidos</option>
                <option value="CP" >Capitalización Obligatoria</option>
             <option value="MO" >Pago Crédito Moroso</option>
             <option value="PB" >Pago a Cuenta Vista</option>
             <option value="PM" >Transferencia Electrónica</option>
            <option value="OB" >Pago efectivo Otro Banco</option>
            <option value="AL" >Todas las Anteriores</option>
             
              </select>
        </td>
      </tr>

        
  </table>
  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>

</form>
</body>
</html>
