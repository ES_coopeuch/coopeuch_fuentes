<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="datapro.eibs.master.Util"%>
<HTML>
<HEAD>
<TITLE>
Marca/Desmarca Boletin Comercial
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V4.0 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "EBC0010Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id="BolBasic" class="datapro.eibs.beans.EBC001001Message" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

</HEAD>

<BODY>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %> 
<SCRIPT Language="Javascript">
            showErrors();
     </SCRIPT>
 <%
 }
%>
<SCRIPT Language="Javascript">

function ChequearTodos(checkbox) 
	{ 
	  var maxrow = document.all["dataTable"].rows.length;
	  var x=0;
	  for (var i=0;x < maxrow;i++) 
	  { 	
		var elemento = document.forms[0].elements[i]; 
		if (elemento.type == "checkbox") 
			{ 
			   eval("document.forms[0].E01BCMPFL"+x+".value=''");
			   elemento.checked = "true"
			   x++;
			} 
		} 
	}
	

function colocaMarca(chkbox,pos)
	{ 
		if (chkbox.checked) { 
			eval("document.forms[0].E01BCMPFL"+pos+".value=''");
			eval("document.forms[0].E01BCMGMD"+pos+".style.display = 'none'");	
			eval("document.forms[0].ayudaMarca"+pos+".style.display = 'none'");	
		}else{
			eval("document.forms[0].E01BCMPFL"+pos+".value='D'");
            var Nombre_Campo = "E01BCMCNV"+pos;
 			if(document.getElementById(Nombre_Campo).value == "") 
 				eval("document.forms[0].E01BCMGMD"+pos+".value='NO DEFINIDO'");
 			else
			 	eval("document.forms[0].E01BCMGMD"+pos+".value='MORA EMPLEADOR'");
			eval("document.forms[0].E01BCMCMD"+pos+".value=''");
			eval("document.forms[0].E01BCMGMD"+pos+".style.display = ''");	 
			eval("document.forms[0].ayudaMarca"+pos+".style.display = ''");	 
		} 		
	}
function mensaje()
   {
		alert("Al Cambiarse de pagina perdera los cambios efectuados en esta..")
		return;
	}
</SCRIPT>
<h3 align="center">Marcar/Desmarcar Bolet&iacute;n Comercial<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="boletin_comercial_list.jsp,EBC0010"></h3>
<hr size="4">
<FORM Method="post" Action="<%=request.getContextPath()%>/servlet/datapro.eibs.cleaning.JSEBC0010">
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="5">

<table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			class="tbhead">
			<tr id="trclear">
			<td nowrap width="16%">
			<div align="right"><b>Fecha :</b></div>
			</td>
            <td nowrap width="20%" > 
              <div align="left"> 
              <input type="text" name="E01BCMIND" size="3" maxlength="2" value="<%= userPO.getHeader19().trim()%>" readonly>
              <input type="text" name="E01BCMINM" size="3" maxlength="2" value="<%= userPO.getHeader20().trim()%>" readonly>
              <input type="text" name="E01BCMINY" size="5" maxlength="4" value="<%= userPO.getHeader21().trim()%>" readonly>
			</div>
            </td>
			</tr>
			<tr id="trdark">
			<td nowrap width="16%">
			<div align="right"><b>Oficina :</b></div>
			</td>
			<td nowrap width="20%">
			<div align="left"><input type="text" name="D01SELBRN" size="25" maxlength="25" value="<%=userPO.getHeader22().trim()%>" readonly></div>
			</td>
			</tr>
			<tr id="trclear">
			<td nowrap width="16%">
			<div align="right"><b>Cliente :</b></div>
			</td>
			<td nowrap width="20%">
			<div align="left"><input type="text" name="D01SELCUN" size="13" maxlength="12" value="<%=userPO.getHeader23().trim()%>" readonly></div>
			</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<%
	int recnum = 0;
	if ( EBC0010Help.getNoResult() ) {
 %>
   		<TABLE class="tbenter" width=100% height=90% >
   		<TR>
      <TD> 
        
      <div align="center"> <font size="3"><b> No hay datos que correspondan con su criterio de busqueda 
        </b></font> </div>
      </TD></TR>
   		</TABLE>
<%   		
	}
	else 
	{
%>

  <TABLE class="tbenter" width="100%">
      <TR>
			<td nowrap width="16%">
			<div align="right"><b> </b></div>
			</td>
	</TR>
    <TR>
 	 <TD ALIGN=left ><input type="checkbox" name="checkboxTodos" value="checkbox" onClick="ChequearTodos(this);"><b>Marcar Todos</b></TD>
	</TR>
	    <TR>
			<td nowrap width="16%">
			<div align="right"><b> </b></div>
			</td>
	</TR>
  </TABLE>      

  <TABLE  id="mainTable" class="tableinfo" ALIGN=CENTER style="width:'100%'" height="72%" >
		<tr height="95%">    
			<td NOWRAP width="100%">       
   			    <div id="dataDiv1" class="scbarcolor" style="width:100%; height:100%; overflow:auto;">
    				<table id="dataTable" width="100%" > 
			    		<TR id="trdark">  
      						<TH ALIGN="left" width="5%" NOWRAP>Marca</TH>
      						<TH ALIGN="left" width="23%" NOWRAP>Motivos Desmarca</TH>
      						<TH ALIGN="left" width="7%" NOWRAP>Rut</TH>
				      		<TH ALIGN="left" width="25%" NOWRAP>Nombre</TH>
							<TH ALIGN="left" width="10%" NOWRAP>Cuenta</TH>     
			      			<TH ALIGN="left" width="10%" NOWRAP>Vencimiento</TH>
     			 			<TH ALIGN="left" width="5%" NOWRAP>Mora</TH>
      						<TH ALIGN="left" width="5%" NOWRAP>Cnv</TH>
							<TH ALIGN="left" width="10%" NOWRAP>Monto</TH>
     		</TR>
    				
    				
<%
                EBC0010Help.initRow();
				boolean firstTime = true;
				String chk = "";
				int idx = 0;
				int fil = 0;
				String valStyle = "";
        		while (EBC0010Help.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					if (idx++ % 2 != 0)
						valStyle = "trdark";
					else
						valStyle = "trclear";
                  	datapro.eibs.beans.EBC001001Message msgList = (datapro.eibs.beans.EBC001001Message) EBC0010Help.getRecord();
		 %>
				<TR id="<%= valStyle %>">
           		 	<td align="left" > 
	               	 	<input type="checkbox" name="CE01BCMPFL<%=fil%>" value="<%=msgList.getE01BCMPFL()%>"  <%if (!msgList.getE01BCMPFL().equals("D")) out.print("checked"); %> onclick="colocaMarca(this,<%=fil%>);"> 
						<input type="hidden" name="E01BCMPFL<%=fil%>" value="<%= msgList.getE01BCMPFL() %>">					               	 	
					</td>
           		 	<td align="left" width="23%"> 
        	            <input type="text" name="E01BCMGMD<%=fil%>" size="30" maxlength="30" value="<%if (msgList.getE01BCMCMD().equals("")){if(msgList.getE01BCMCNV().equals("")) out.print("NO DEFINIDO"); else out.print("MORA EMPLEADOR"); } else out.print(msgList.getE01BCMGMD());%>" <%if (!msgList.getE01BCMPFL().equals("D")) out.print("style='display:none;'"); else out.print("style='display:;'"); %>>
            	   	 	<a href="javascript:GetCodeDescCNOFC('E01BCMCMD<%=fil%>','E01BCMGMD<%=fil%>','BC')"><img name="ayudaMarca<%=fil%>" src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0" <%if (!msgList.getE01BCMPFL().equals("D")) out.print("style='display:none;'"); else out.print("style='display:;'"); %> ></a> 
						<input type="hidden" name="E01BCMCMD<%=fil%>" value="<%= msgList.getE01BCMCMD() %>">
					</td>
					<TD NOWRAP  ALIGN="right" ><%= msgList.getE01BCMIDN() %></td>
					<TD NOWRAP  ALIGN=left ><%= msgList.getE01BCMSHN() %></td>
					<TD NOWRAP  ALIGN=center ><%= msgList.getE01BCMACC() %></td>
			        <TD NOWRAP align=center ><%= Util.formatDate(msgList.getE01BCMPDD(),msgList.getE01BCMPDM(),msgList.getE01BCMPDY())%></td>
 					<TD NOWRAP  ALIGN=center ><%= msgList.getE01BCMMOR() %></td>
 				    <TD NOWRAP  ALIGN=center ><%= msgList.getE01BCMCNV() %>
						<input type="hidden" name="E01BCMCNV<%=fil%>" value="<%= msgList.getE01BCMCNV() %>">
 				    </td>
 			    	<TD NOWRAP  ALIGN=right ><%= msgList.getE01BCMAMT() %></td>
 				</TR>
		 <%
		 		fil++;
                }
         %>
					</table>
   				</div>
   			</TD>
		</TR>	
	</TABLE>

  <TABLE  class="tbenter" WIDTH="98%" ALIGN=CENTER>
    <TR>
      <TD WIDTH="50%" ALIGN=LEFT height="25"> <%
        if ( EBC0010Help.getShowPrev() ) {
      			int pos =EBC0010Help.getFirstRec() - 51;
      			   out.print("<A HREF=\"" + request.getContextPath() + "/servlet/datapro.eibs.cleaning.JSEBC0010?SCREEN=3&FromRecord=" + pos +  "\" ><img src=\""+request.getContextPath()+"/images/s/previous_records.gif\" border=0></A>");
        }
%> </TD>
 	  <TD WIDTH="50%" ALIGN=RIGHT height="25"> <%       
        if ( EBC0010Help.getShowNext() ) {
      			int pos = EBC0010Help.getLastRec();
      			out.print("<A HREF=\"" + request.getContextPath()+ "/servlet/datapro.eibs.cleaning.JSEBC0010?SCREEN=3&FromRecord=" + pos +  "\" ><img src=\""+ request.getContextPath()+"/images/s/next_records.gif\" border=0></A>");

        }
%> </TD>
	 </TR>
	 </TABLE>
	 
  <SCRIPT language="JavaScript">
		showChecked("CURRCODE");
		
			function resizeDoc() {
      		 	divResize();
     		    adjustEquTables(headTable, dataTable, dataDiv1,1,false);
      		}
	 		resizeDoc();   			
     		window.onresize=resizeDoc;        
     </SCRIPT>
<BR>


       <p align="center"> 
    	  <input id="EIBSBTN" type=Submit name="Submit" value="Enviar" onClick="SendInfo()">
    	</p>
  
<%           
  }
%>

  <SCRIPT language="JavaScript">
	 	document.getElementById("hiddenDivSearch").onclick=cancelBub;
		if(document.getElementById("eibsSearch"))
			document.getElementById("eibsSearch").onclick=showHiddenDivSearch; 
     </SCRIPT>

</FORM>


</BODY>
</HTML>
