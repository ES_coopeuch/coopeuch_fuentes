package datapro.eibs.beans;

import datapro.eibs.sockets.*;
import java.io.*;
import java.math.*;

import java.util.*;

/**
* Class generated by AS/400 CRTCLASS command from EREC10001 physical file definition.
* 
* File level identifier is 1160203160248.
* Record format level identifier is 4DC1DB0088110.
*/

public class EREC10001Message extends MessageRecord
{
  final static String fldnames[] = {
                                     "H01USERID",
                                     "H01PROGRM",
                                     "H01TIMSYS",
                                     "H01SCRCOD",
                                     "H01OPECOD",
                                     "H01FLGMAS",
                                     "H01FLGWK1",
                                     "H01FLGWK2",
                                     "H01FLGWK3",
                                     "E01NROREG",
                                     "E01RERIDE",
                                     "E01RERCOR",
                                     "E01RERCON",
                                     "E01RERMIN",
                                     "E01RERMAX",
                                     "E01RERIND"
                                   };
  final static String tnames[] = {
                                   "H01USERID",
                                   "H01PROGRM",
                                   "H01TIMSYS",
                                   "H01SCRCOD",
                                   "H01OPECOD",
                                   "H01FLGMAS",
                                   "H01FLGWK1",
                                   "H01FLGWK2",
                                   "H01FLGWK3",
                                   "E01NROREG",
                                   "E01RERIDE",
                                   "E01RERCOR",
                                   "E01RERCON",
                                   "E01RERMIN",
                                   "E01RERMAX",
                                   "E01RERIND"
                                 };
  final static String fid = "1160203160248";
  final static String rid = "4DC1DB0088110";
  final static String fmtname = "EREC10001";
  final int FIELDCOUNT = 16;
  private static Hashtable tlookup = new Hashtable();
  private CharacterField fieldH01USERID = null;
  private CharacterField fieldH01PROGRM = null;
  private CharacterField fieldH01TIMSYS = null;
  private CharacterField fieldH01SCRCOD = null;
  private CharacterField fieldH01OPECOD = null;
  private CharacterField fieldH01FLGMAS = null;
  private CharacterField fieldH01FLGWK1 = null;
  private CharacterField fieldH01FLGWK2 = null;
  private CharacterField fieldH01FLGWK3 = null;
  private DecimalField fieldE01NROREG = null;
  private DecimalField fieldE01RERIDE = null;
  private DecimalField fieldE01RERCOR = null;
  private CharacterField fieldE01RERCON = null;
  private DecimalField fieldE01RERMIN = null;
  private DecimalField fieldE01RERMAX = null;
  private CharacterField fieldE01RERIND = null;

  /**
  * Constructor for EREC10001Message.
  */
  public EREC10001Message()
  {
    createFields();
    initialize();
  }

  /**
  * Create fields for this message.
  * This method implements the abstract method in the MessageRecord superclass.
  */
  protected void createFields()
  {
    recordsize = 133;
    fileid = fid;
    recordid = rid;
    message = new byte[getByteLength()];
    formatname = fmtname;
    fieldnames = fldnames;
    tagnames = tnames;
    fields = new MessageField[FIELDCOUNT];

    fields[0] = fieldH01USERID =
    new CharacterField(message, HEADERSIZE + 0, 10, "H01USERID");
    fields[1] = fieldH01PROGRM =
    new CharacterField(message, HEADERSIZE + 10, 10, "H01PROGRM");
    fields[2] = fieldH01TIMSYS =
    new CharacterField(message, HEADERSIZE + 20, 12, "H01TIMSYS");
    fields[3] = fieldH01SCRCOD =
    new CharacterField(message, HEADERSIZE + 32, 2, "H01SCRCOD");
    fields[4] = fieldH01OPECOD =
    new CharacterField(message, HEADERSIZE + 34, 4, "H01OPECOD");
    fields[5] = fieldH01FLGMAS =
    new CharacterField(message, HEADERSIZE + 38, 1, "H01FLGMAS");
    fields[6] = fieldH01FLGWK1 =
    new CharacterField(message, HEADERSIZE + 39, 1, "H01FLGWK1");
    fields[7] = fieldH01FLGWK2 =
    new CharacterField(message, HEADERSIZE + 40, 1, "H01FLGWK2");
    fields[8] = fieldH01FLGWK3 =
    new CharacterField(message, HEADERSIZE + 41, 1, "H01FLGWK3");
    fields[9] = fieldE01NROREG =
    new DecimalField(message, HEADERSIZE + 42, 8, 0, "E01NROREG");
    fields[10] = fieldE01RERIDE =
    new DecimalField(message, HEADERSIZE + 50, 5, 0, "E01RERIDE");
    fields[11] = fieldE01RERCOR =
    new DecimalField(message, HEADERSIZE + 55, 3, 0, "E01RERCOR");
    fields[12] = fieldE01RERCON =
    new CharacterField(message, HEADERSIZE + 58, 40, "E01RERCON");
    fields[13] = fieldE01RERMIN =
    new DecimalField(message, HEADERSIZE + 98, 17, 2, "E01RERMIN");
    fields[14] = fieldE01RERMAX =
    new DecimalField(message, HEADERSIZE + 115, 17, 2, "E01RERMAX");
    fields[15] = fieldE01RERIND =
    new CharacterField(message, HEADERSIZE + 132, 1, "E01RERIND");

    synchronized (tlookup)
    {
      if (tlookup.isEmpty())
      {
        for (int i = 0; i < tnames.length; i++)
          tlookup.put(tnames[i], new Integer(i));
      }
    }

    taglookup = tlookup;
  }

  /**
  * Set field H01USERID using a String value.
  */
  public void setH01USERID(String newvalue)
  {
    fieldH01USERID.setString(newvalue);
  }

  /**
  * Get value of field H01USERID as a String.
  */
  public String getH01USERID()
  {
    return fieldH01USERID.getString();
  }

  /**
  * Set field H01PROGRM using a String value.
  */
  public void setH01PROGRM(String newvalue)
  {
    fieldH01PROGRM.setString(newvalue);
  }

  /**
  * Get value of field H01PROGRM as a String.
  */
  public String getH01PROGRM()
  {
    return fieldH01PROGRM.getString();
  }

  /**
  * Set field H01TIMSYS using a String value.
  */
  public void setH01TIMSYS(String newvalue)
  {
    fieldH01TIMSYS.setString(newvalue);
  }

  /**
  * Get value of field H01TIMSYS as a String.
  */
  public String getH01TIMSYS()
  {
    return fieldH01TIMSYS.getString();
  }

  /**
  * Set field H01SCRCOD using a String value.
  */
  public void setH01SCRCOD(String newvalue)
  {
    fieldH01SCRCOD.setString(newvalue);
  }

  /**
  * Get value of field H01SCRCOD as a String.
  */
  public String getH01SCRCOD()
  {
    return fieldH01SCRCOD.getString();
  }

  /**
  * Set field H01OPECOD using a String value.
  */
  public void setH01OPECOD(String newvalue)
  {
    fieldH01OPECOD.setString(newvalue);
  }

  /**
  * Get value of field H01OPECOD as a String.
  */
  public String getH01OPECOD()
  {
    return fieldH01OPECOD.getString();
  }

  /**
  * Set field H01FLGMAS using a String value.
  */
  public void setH01FLGMAS(String newvalue)
  {
    fieldH01FLGMAS.setString(newvalue);
  }

  /**
  * Get value of field H01FLGMAS as a String.
  */
  public String getH01FLGMAS()
  {
    return fieldH01FLGMAS.getString();
  }

  /**
  * Set field H01FLGWK1 using a String value.
  */
  public void setH01FLGWK1(String newvalue)
  {
    fieldH01FLGWK1.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK1 as a String.
  */
  public String getH01FLGWK1()
  {
    return fieldH01FLGWK1.getString();
  }

  /**
  * Set field H01FLGWK2 using a String value.
  */
  public void setH01FLGWK2(String newvalue)
  {
    fieldH01FLGWK2.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK2 as a String.
  */
  public String getH01FLGWK2()
  {
    return fieldH01FLGWK2.getString();
  }

  /**
  * Set field H01FLGWK3 using a String value.
  */
  public void setH01FLGWK3(String newvalue)
  {
    fieldH01FLGWK3.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK3 as a String.
  */
  public String getH01FLGWK3()
  {
    return fieldH01FLGWK3.getString();
  }

  /**
  * Set field E01NROREG using a String value.
  */
  public void setE01NROREG(String newvalue)
  {
    fieldE01NROREG.setString(newvalue);
  }

  /**
  * Get value of field E01NROREG as a String.
  */
  public String getE01NROREG()
  {
    return fieldE01NROREG.getString();
  }

  /**
  * Set numeric field E01NROREG using a BigDecimal value.
  */
  public void setE01NROREG(BigDecimal newvalue)
  {
    fieldE01NROREG.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01NROREG as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01NROREG()
  {
    return fieldE01NROREG.getBigDecimal();
  }

  /**
  * Set field E01RERIDE using a String value.
  */
  public void setE01RERIDE(String newvalue)
  {
    fieldE01RERIDE.setString(newvalue);
  }

  /**
  * Get value of field E01RERIDE as a String.
  */
  public String getE01RERIDE()
  {
    return fieldE01RERIDE.getString();
  }

  /**
  * Set numeric field E01RERIDE using a BigDecimal value.
  */
  public void setE01RERIDE(BigDecimal newvalue)
  {
    fieldE01RERIDE.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RERIDE as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RERIDE()
  {
    return fieldE01RERIDE.getBigDecimal();
  }

  /**
  * Set field E01RERCOR using a String value.
  */
  public void setE01RERCOR(String newvalue)
  {
    fieldE01RERCOR.setString(newvalue);
  }

  /**
  * Get value of field E01RERCOR as a String.
  */
  public String getE01RERCOR()
  {
    return fieldE01RERCOR.getString();
  }

  /**
  * Set numeric field E01RERCOR using a BigDecimal value.
  */
  public void setE01RERCOR(BigDecimal newvalue)
  {
    fieldE01RERCOR.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RERCOR as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RERCOR()
  {
    return fieldE01RERCOR.getBigDecimal();
  }

  /**
  * Set field E01RERCON using a String value.
  */
  public void setE01RERCON(String newvalue)
  {
    fieldE01RERCON.setString(newvalue);
  }

  /**
  * Get value of field E01RERCON as a String.
  */
  public String getE01RERCON()
  {
    return fieldE01RERCON.getString();
  }

  /**
  * Set field E01RERMIN using a String value.
  */
  public void setE01RERMIN(String newvalue)
  {
    fieldE01RERMIN.setString(newvalue);
  }

  /**
  * Get value of field E01RERMIN as a String.
  */
  public String getE01RERMIN()
  {
    return fieldE01RERMIN.getString();
  }

  /**
  * Set numeric field E01RERMIN using a BigDecimal value.
  */
  public void setE01RERMIN(BigDecimal newvalue)
  {
    fieldE01RERMIN.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RERMIN as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RERMIN()
  {
    return fieldE01RERMIN.getBigDecimal();
  }

  /**
  * Set field E01RERMAX using a String value.
  */
  public void setE01RERMAX(String newvalue)
  {
    fieldE01RERMAX.setString(newvalue);
  }

  /**
  * Get value of field E01RERMAX as a String.
  */
  public String getE01RERMAX()
  {
    return fieldE01RERMAX.getString();
  }

  /**
  * Set numeric field E01RERMAX using a BigDecimal value.
  */
  public void setE01RERMAX(BigDecimal newvalue)
  {
    fieldE01RERMAX.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RERMAX as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RERMAX()
  {
    return fieldE01RERMAX.getBigDecimal();
  }

  /**
  * Set field E01RERIND using a String value.
  */
  public void setE01RERIND(String newvalue)
  {
    fieldE01RERIND.setString(newvalue);
  }

  /**
  * Get value of field E01RERIND as a String.
  */
  public String getE01RERIND()
  {
    return fieldE01RERIND.getString();
  }

}
