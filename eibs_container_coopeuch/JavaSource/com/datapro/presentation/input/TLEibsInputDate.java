package com.datapro.presentation.input;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.master.SuperServlet;

/**
 * @author fhernandez
 *
 */
public class TLEibsInputDate extends EibsInputDate {

	private static final long serialVersionUID = -4657478425528295659L;
	private String fn_date = null;
	private String fn_month = null;
	private String fn_day = null;
	private String fn_year = null;
	private String identificador = null;

	public int doEndTag() throws JspException {
		try {
			JspWriter out = pageContext.getOut();
			
			String contextPath = SuperServlet.webAppPath;
			StringBuffer str = new StringBuffer();
				
			
	    	if (!getReadonly())
	    		if (fn_date == null && identificador==null) {
		    		str.append(
			    		getDateHelpLink(
							contextPath,
							"",
							fn_year,
							fn_month,
							fn_day));
				}else if(fn_date == null && identificador!=null){
					str.append(
		    		getDateHelpLink(
						contextPath,
						"",
						fn_year,
						fn_month,
						fn_day,
						identificador)); 
				}
			str.append("</a>");
			
	    	if (isRequired())
	    		str.append(
	    			getMandatoryIcon(
						contextPath,
						""));	

			out.print(str);
			
		} catch (Exception ex) {
		}
		return (super.doEndTag());
	}
	
    protected String renderInputElement() throws JspException {
    	ESS0030DSMessage currUser = (ESS0030DSMessage) pageContext.getSession().getAttribute("currUser");
		setFormat(currUser.getE01DTF());
		
        StringBuffer results = new StringBuffer();
        
        if (fn_date == null) {
            results.append(
            	getDateInput(
            		getFn_year(), 
    				getFn_month(), 
    				getFn_day(),
    				lookupProperty(getName(), getFn_year()),
    				lookupProperty(getName(), getFn_month()),
    				lookupProperty(getName(), getFn_day()),
					getReadonly() ? "readonly" : ""));
		} else {
	        results.append(
	        	getDateInput(
	        		getFn_date(), 
					lookupProperty(getName(), getFn_date()),
					getReadonly() ? "readonly" : ""));
		}

        return results.toString();
    }
	
	public String getFn_day() {
		return fn_day;
	}
	public void setFn_day(String fn_day) {
		this.fn_day = fn_day;
	}
	public String getFn_month() {
		return fn_month;
	}
	public void setFn_month(String fn_month) {
		this.fn_month = fn_month;
	}
	public String getFn_year() {
		return fn_year;
	}
	public void setFn_year(String fn_year) {
		this.fn_year = fn_year;
	}
	public String getFn_date() {
		return fn_date;
	}
	public void setFn_date(String fn_date) {
		this.fn_date = fn_date;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	
}
