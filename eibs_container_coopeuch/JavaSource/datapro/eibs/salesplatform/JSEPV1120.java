package datapro.eibs.salesplatform;

/**
 * Consulta Sinacofi
 * @author evargas
 */
import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession; 

import org.apache.log4j.Logger;

import cl.coopeuch.creditos.predictorriesgo.sinacofi.DatosConsulta;
import cl.coopeuch.creditos.predictorriesgo.sinacofi.Salida;
import cl.coopeuch.creditos.predictorriesgo.sinacofi.SinacofiWSPortProxy;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.SuperServlet;

public class JSEPV1120 extends JSEIBSServlet {

	static Logger logger = Logger.getLogger(JSEPV1120.class); 
	
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int R_SINACOFI_LIST = 100;
	protected static final int A_SINACOFI_LIST = 101;	
	protected static final int R_SINACOFI_RETURN_LIST = 102;	 

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_SINACOFI_LIST:
			procReqSinacofiEnterClient(user, req, res, session,"P");
			break;
		case R_SINACOFI_RETURN_LIST:
			procReqSinacofiEnterClient(user, req, res, session,"");
			break;			
		case A_SINACOFI_LIST:
			procActionSinacofi(user, req, res, session);
			break;		
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}



	protected void procReqSinacofiEnterClient(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses, String vez)
			throws ServletException, IOException {

		try {	
			if ("P".equals(vez)){
				String nivel = req.getParameter("nivel");
				String ni="B";//si viene algo distinto por defecto Asume Basico.
				if ("M".equalsIgnoreCase(nivel)){
					ni="M";
				}else if ("A".equalsIgnoreCase(nivel)){
					ni="A";
				}
				ses.setAttribute("NIVEL", ni);	// B=Basico, M=Medio , A=avanzado				
			}			
			flexLog("About to call Page: EPV1120_clients_sinacofi_enter_search.jsp");
			forward("EPV1120_clients_sinacofi_enter_search.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}



	protected void procActionSinacofi(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {
		
		UserPos userPO = new UserPos();
		try {
				
				String ni = (String)session.getAttribute("NIVEL"); 
				ni = (ni==null?"":ni);
			  
			
				//borramos el anterior objeto Sinacofi de la session
				session.removeAttribute("objSinac");	
				//
				String rutP = req.getParameter("rut");
				userPO.setIdentifier(rutP);
				String rut_s = rutP.substring(0, rutP.length()-1);				
				String dv_s = rutP.substring(rutP.length()-1);				
				String serie = req.getParameter("serie");
				userPO.setHeader1(serie);
				//
				DatosConsulta parameters = new DatosConsulta();			
				parameters.setRut(rut_s);
				parameters.setDv(dv_s);
				parameters.setSerie(serie);
				//armar la consulta segun los check seleccionados.
				String flat01 = req.getParameter("flat01");//	X	Estado C�dula
				req.setAttribute("flat01", flat01==null?"":flat01);
				flat01 = (flat01==null?"":flat01+",");

				String flat02 = req.getParameter("flat02");//	X	Morosidad
				req.setAttribute("flat02", flat02==null?"":flat02);
				flat02 = (flat02==null?"":flat02+",");				
 
				String flat82 = req.getParameter("flat82");//	X	Protestos
				req.setAttribute("flat82", flat82==null?"":flat82);
				flat82 = (flat82==null?"":flat82+",");				

				String flat07 = req.getParameter("flat07");//		Infracciones Laborales del empleador
				req.setAttribute("flat07", flat07==null?"":flat07);
				flat07 = (flat07==null?"":flat07+",");			
				
				String flat08 = req.getParameter("flat08");//		Prendas
				req.setAttribute("flat08", flat08==null?"":flat08);
				flat08 = (flat08==null?"":flat08+",");				

				String flat14 = req.getParameter("flat14");//		Multados por el Banco Central de Chile
				req.setAttribute("flat14", flat14==null?"":flat14);
				flat14 = (flat14==null?"":flat14+",");				

				String flat16 = req.getParameter("flat16");//	X	deudores sistema financiero
				req.setAttribute("flat16", flat16==null?"":flat16);
				flat16 = (flat16==null?"":flat16+",");				

				String flat18 = req.getParameter("flat18");//		Direcciones Asociadas al Rut
				req.setAttribute("flat18", flat18==null?"":flat18);
				flat18 = (flat18==null?"":flat18+",");				

				String flat26 = req.getParameter("flat26");//		Directorio de personas
				req.setAttribute("flat26", flat26==null?"":flat26);
				flat26 = (flat26==null?"":flat26+",");			
				
				String flat32 = req.getParameter("flat32");//		Bienes ra�ces 
				req.setAttribute("flat32", flat32==null?"":flat32);
				flat32 = (flat32==null?"":flat32+",");
				
				String consulta =  flat01 +	flat02 + flat82 + flat07 + flat08 + flat14 +  flat16 + flat18 + flat26 + flat32;
				
				consulta = consulta.substring(0,consulta.length()-1);
					
				parameters.setConsultas(consulta);
				//
				
				logger.info("PARAMETROS CONSULTA DE  BUREAU : RUT="+rut_s+"; DV="+dv_s+"; SERIE="+serie+"; consultas="+consulta);	
				SinacofiWSPortProxy wsp = new SinacofiWSPortProxy();//inicializamos el servicio	
				Salida resp = wsp.sinacofiWS(parameters);	

				if (resp!=null)			
					session.setAttribute("objSinac", resp); 		      		

				if (!(resp.getCodError()==null) && !"0".equals(resp.getCodError())){			
					String c = resp.getCodError();
					c =(c==null)?"N/D":c; 
					String d = resp.getDesError();
					d =(d==null)?"SIN RESPUESTA SERVICIO  BUREAU ":d;					
					req.setAttribute("ERNU01", c.toString());
					req.setAttribute("ERDS01", d.toString());    
				}
				else 
				{		
					req.setAttribute("ERNU01", "");
					req.setAttribute("ERDS01", "");     
				}
		
		}catch (Exception e1){
			e1.printStackTrace();
			String d = e1.getMessage();
			d=(d==null)?d=e1.toString():d;					

			req.setAttribute("ERNU01", "");
			req.setAttribute("ERDS01", " ERROR AL ACCEDER AL SERVICIO DE BUREAU. VERIFIQUE COMUNICACION. - " + d.toString());     
		}	
		session.setAttribute("userPO",userPO);
		forward("EPV1120_consulta_sinacofi.jsp", req, res);				
	}		

}


