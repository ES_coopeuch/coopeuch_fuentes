<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<jsp:useBean id="ufivpMsg" class="datapro.eibs.beans.ESD040001Message"  scope="session" />
<jsp:useBean id="error"  class="datapro.eibs.beans.ELEERRMessage"  scope="session"/>
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" 	scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<%!
 
	boolean readOnly=false;
	String READ_ONLY_TAG=""; 
%> 

<%
	// Determina si es solo lectura
	if (userPO.getOption().equals("C"))
		{
			readOnly=true;
			READ_ONLY_TAG="readonly";
		} else {
			readOnly=false;
			READ_ONLY_TAG="";
		}
%>
<script language="JavaScript">
function enter(){
	  document.forms[0].submit();
	 }
</script> 

</head>
<body>

 <% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     error.setERRNUM("0");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
    }
%>
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSESD0400" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="3">
  <input type=HIDDEN name="E01SELMONT" value="<%= ufivpMsg.getE01SELMONT().trim()%>">
  <input type=HIDDEN name="E01SELYEAR" value="<%= ufivpMsg.getE01SELYEAR().trim()%>">
  <input type=HIDDEN name="E01SELMONE" value="<%= ufivpMsg.getE01SELMONE().trim()%>">  

  <h3 align="center"><% if (userPO.getOption().equals("M")) out.print("Mantenimiento Valores UF, IVP");
                 else out.print("Consulta Valores UF, IVP");%> <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="UF_IVP.jsp,ESD0400"> 
  </h3>
  <hr size="4">
<BR> 
<TABLE class="tableinfo" align="center" width="85%">
	<TBODY>
		<TR>
			<TD>
			<TABLE width="100%" border="0" cellspacing="1" cellpadding="0">
				<TBODY>
         	 <TR id="trdark">
            <td nowrap width="50%" align="right">Moneda :</td>
            <TD nowrap width="10%" align="left"> 
              <select name="E01SELMONE" disabled>
                <option value=" " <% if (!(ufivpMsg.getE01SELMONE().equals("2") ||ufivpMsg.getE01SELMONE().equals("3"))) out.print("selected"); %>></option>
                <option value="2" <% if (ufivpMsg.getE01SELMONE().equals("2")) out.print("selected"); %>>UF</option>
                <option value="3" <% if (ufivpMsg.getE01SELMONE().equals("3")) out.print("selected"); %>>IVP</option>
              </select>
            </TD>
			 <td nowrap width="40%" align="left"></td>	
		  	</TR>
		 	 <tr id="trclear">
            <td nowrap width="50%" align="right">Mes :</td>
            <td nowrap width="10%" align="left"> 
                <eibsinput:text name="ufivpMsg" property="E01SELMONT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="5" maxlength="2" required="false" readonly = "true"/>
            </td>
			 <td nowrap width="40%" align="left"></td>            
 		  </tr>
 		  <tr id="trdark">
		   <TD nowrap width="50%" align="right">A�o :</TD>
			<TD nowrap width="10%" align="left">
               <eibsinput:text name="ufivpMsg" property="E01SELYEAR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="5" maxlength="4" required="false"/>
            </TD>
			 <td nowrap width="40%" align="left"></td>                
		     </tr>
				</TBODY>
			</TABLE>
			</TD>
		</TR>
	</TBODY>
</TABLE>
<BR>
<%	if (!READ_ONLY_TAG.equals("") &&   ufivpMsg.getE01UFIVM01().equals("0"))  { %>  
          <p>
            <b>No existen registros para su seleccion.</b>
          </p>
<%	} else {	%>
<TABLE class="tableinfo" align="center" >
	<TBODY>
		<TR align="center">
			<TD>
			<TABLE width="100%" align="center" cellspacing="1" cellpadding="0">
				<tr id="trdark">
					<td  align="center" valign="top" width="40%">Fecha &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Valor</td>
				</tr>
				<tr>
					<td  align="center" width="50%">
				<div style="height:400px; overflow-y: scroll">
					<%
					String index = "0" ;
					for(int i = 1; i <= 31; i++)
					{
					   if (i < 10) 
					   {
					     index = "0" + i;
					   }
					   else
					   {
					   	 index = "" + i;
					   }
					   if (!ufivpMsg.getField("E01UFIVD" + index).getString().trim().equals("0")) {
						%>
						<input size="3" maxlength="2" name="<%="E01UFIVD" + index %>" value="<%= ufivpMsg.getField("E01UFIVD" + index).getString().trim()%>" class="TXTRIGHT" readonly>
						<input size="3" maxlength="2" name=E01SELMONT  value="<%=ufivpMsg.getE01SELMONT()%>" class="TXTRIGHT" readonly>
						<input size="5" maxlength="4" name=E01SELYEAR  value="<%=ufivpMsg.getE01SELYEAR()%>" class="TXTRIGHT" readonly> 
						<%if (ufivpMsg.getField("E01UFIVF" + index).getString().trim().equals("B")) {%>
						<input size="18" maxlength="12" name="<%= "E01UFIVM" + index %>"   value="<%= ufivpMsg.getField("E01UFIVM" + index).getString().trim()%>" class="TXTRIGHT" <%= READ_ONLY_TAG %> readonly>
						<br>	
						<% } else { %>	
						 <input size="18" maxlength="12" name="<%= "E01UFIVM" + index %>"   value="<%= ufivpMsg.getField("E01UFIVM" + index).getString().trim()%>" class="TXTRIGHT" <%= READ_ONLY_TAG %> onkeypress="enterDecimal()">
						<br>
						<% } %>				
					<% }}%>
				</div>
				</td>
				</tr>
			</TABLE>
			</TD>
		</TR>
	</TBODY>
</TABLE>

<BR>

<%	if (READ_ONLY_TAG.equals("")) { %>  
  <p align="center"> 
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
<%	} 	%> 
<%	} 	%>     
</form>
</body>
</html>
