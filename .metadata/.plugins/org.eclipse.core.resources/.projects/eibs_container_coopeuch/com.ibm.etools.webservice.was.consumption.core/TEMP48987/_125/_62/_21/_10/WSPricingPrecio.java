/**
 * WSPricingPrecio.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public interface WSPricingPrecio extends javax.xml.rpc.Service {
    public java.lang.String getPricingPrecioPortAddress();
    public _125._62._21._10.PricingPrecioSOAPPortType getPricingPrecioPort() throws javax.xml.rpc.ServiceException;
    public _125._62._21._10.PricingPrecioSOAPPortType getPricingPrecioPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;

}
