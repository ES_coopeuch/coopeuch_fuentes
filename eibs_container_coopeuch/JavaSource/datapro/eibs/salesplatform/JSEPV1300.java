package datapro.eibs.salesplatform;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */ 
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV130001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.ServiceLocator;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;

public class JSEPV1300 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_CAM_LOAD_ENTER = 100;	
	protected static final int A_CAM_LOAD_ENTER = 200;
	
	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
 		screen =  R_CAM_LOAD_ENTER;

		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}		
		
		switch (screen) {
		case R_CAM_LOAD_ENTER:
			procReqCamEnter(user, req, res, session);
			break;
		case A_CAM_LOAD_ENTER:
			procActionCamEnter(user, req, res, session);
			break; 


			
		}		
	}
	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqCamEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: EPV1300_campaigns_load_enter.jsp");
			forward("EPV1300_campaigns_load_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 * 
	 */
	protected void procActionCamEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
	
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		boolean ok = false;
		String idCampana = "";
		
		try {
			SmartUpload mySmartUpload = new SmartUpload();
			com.jspsmart.upload.File myFile = null;
			try {
				mySmartUpload.initialize(config, req, res);
				mySmartUpload.upload();
				myFile = mySmartUpload.getFiles().getFile(0);
				if (myFile.getSize() > 0) {
					//El archivo tiene datos buscamos el id asignado.					
					mp = getMessageProcessor("EPV1300", req);					
					EPV130001Message msg = (EPV130001Message) mp.getMessageRecord("EPV130001");
					//seteamos las propiedades
					msg.setH01USERID(user.getH01USR());
					msg.setH01OPECOD("0001");
					msg.setH01TIMSYS(getTimeStamp());					
					
					//Sending message
					mp.sendMessage(msg);
		
					//Receive error and data
					msgError = (ELEERRMessage) mp.receiveMessageRecord();
					msg = (EPV130001Message) mp.receiveMessageRecord();		
					
					
					//havent errors i get the field
					if (!mp.hasError(msgError)) {
					try {
						    String fileName =  "CMPFTP";//nombre del archivo	
							idCampana = msg.getE01CMPIDC();
						
							byte[] bd = new byte[myFile.getSize()];
							for (int i = 0; i < myFile.getSize(); i++) {
								bd[i] = myFile.getBinaryData(i);
							}
						
							InputStream  input = new ByteArrayInputStream(bd);													
							String userid = ServiceLocator.getSlInfo().getString("ftp.cnx.userid.eibs-server");
							String password = ServiceLocator.getSlInfo().getString("ftp.cnx.password.eibs-server");												
						
							FTPWrapper ftp = new FTPStdWrapper(JSEIBSProp.getHostIP(), userid, password, "");
						
							if (ftp.open()) 
							{
								ftp.setFileType(FTPWrapper.ASCII);
								ftp.upload(input,fileName); 
								ok=true;
							}
							else
							{	
								msgError = new ELEERRMessage();
								msgError.setERRNUM("1");
								msgError.setERNU01("01");		                
								msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
							}	

						} catch (Exception e) {
							msgError = new ELEERRMessage();
							msgError.setERRNUM("1");
							msgError.setERNU01("01");		                
							msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
						}
					}
					
				}else{
					//mandamos error en session 
					msgError = new ELEERRMessage();
					msgError.setERRNUM(new BigDecimal(1));
					msgError.setERDS01("Archivo Vacio...");							
				}
				
			}catch (Exception e) {
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();	
				msgError = new ELEERRMessage();			
				msgError.setERRNUM("1");
	            msgError.setERNU01("01");
	            msgError.setERDS01(className);
	            msgError.setERNU02("02");
	            msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
	            msgError.setERNU03("03");
	            msgError.setERDS03("Para mas informacion revizar los archivos de log.");
				e.printStackTrace();			
			} 	
			
			if (ok){									
				msgError = null;
				mp.close();
				mp = null;

				mp = getMessageProcessor("EPV1300", req);					
				EPV130001Message msg = (EPV130001Message) mp.getMessageRecord("EPV130001");
				msg.setH01USERID(user.getH01USR());
				msg.setH01OPECOD("0002");
				msg.setH01TIMSYS(getTimeStamp());
				//Sets message with page fields
				msg.setE01CMPIDC(idCampana);

				//Sending message
				mp.sendMessage(msg);

				//Receive error and data
				msgError = (ELEERRMessage) mp.receiveMessageRecord();
				msg = (EPV130001Message) mp.receiveMessageRecord();

				//Sets session with required data
				session.setAttribute("userPO", userPO);
				session.setAttribute("bank", msg);

				if (!mp.hasError(msgError)) {
					forward("EPV1300_campaigns_load_procces.jsp", req, res);
				} else {
					//if there are errors go back to maintenance page and show errors
					session.setAttribute("error", msgError);
					forward("EPV1300_campaigns_load_enter.jsp", req, res);
				}

			
			}else{
				session.setAttribute("error", msgError);				
				forward("EPV1300_campaigns_load_enter.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	//END
}
	



