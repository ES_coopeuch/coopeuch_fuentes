<html>
<head>
<title>Certificado Prepago Prestamo</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT Language="javascript">

function CheckNum(){
	if(isNaN(document.forms[0].E01DEAACC.value)||(document.forms[0].E01DEAACC.value.length < 1))
	{
		alert("Debe ingresar un n�mero de Prestamo valido");
		document.forms[0].E01DEAACC.value='';
		document.forms[0].E01DEAACC.focus();
  		return false;
	}
	else if(isNaN(document.forms[0].E01PAGDIA.value)||(document.forms[0].E01PAGDIA.value.length < 1) ||
	        isNaN(document.forms[0].E01PAGMES.value)||(document.forms[0].E01PAGMES.value.length < 1) ||
	        isNaN(document.forms[0].E01PAGANO.value)||(document.forms[0].E01PAGANO.value.length < 1))
		{
		alert("Debe ingresar una fecha valida");
		document.forms[0].E01PAGDIA.value='';
		document.forms[0].E01PAGDIA.focus();
  		return false;
  		}
	else {
  		return true;
	}	
}

</SCRIPT>


</head>


<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session"/>




<body  bgcolor="#FFFFFF">

<h3 align="center">Certificado Prepago Pr&eacute;stamo<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cer_ln_enter_payment, ECE0030"></h3>
<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECE0030" onsubmit="return(CheckNum())">
  <p> 
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="10">
  </p>
  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0" bordercolor="#000000">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0">
            <tr><td>&nbsp;</td></tr>
			  <tr><td>&nbsp;</td></tr>
			    <tr><td>&nbsp;</td></tr>
				  <tr><td>&nbsp;</td></tr>
		  <tr> 
            <td nowrap width="50%"> 
              <div align="right"><b>Ingrese el N&uacute;mero del Pr&eacute;stamo 
                :</b></div>
            </td>
            <td nowrap width="50%"> 
              <input type="text" name="E01DEAACC" size="13" maxlength="12" onKeypress="enterInteger()">
              <a href="javascript:GetAccount('E01DEAACC','','10','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0" ></a> 
            </td>
          </tr>
        	<tr id="trdark">
		    <td nowrap width="20%">
			<div align="right"><b>Ingrese Fecha de Liquidacion :</b></div>
			</td>
            <td nowrap width="60%"> 
              <input type="text" name="E01PAGDIA" size="3" maxlength="2" onKeypress="enterInteger()">
              <input type="text" name="E01PAGMES" size="3" maxlength="2" onKeypress="enterInteger()">
              <input type="text" name="E01PAGANO" size="5" maxlength="4" onKeypress="enterInteger()">
             </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>

	<div align="center"> 
    	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  	</div>


<script language="JavaScript">
  document.forms[0].E01DEAACC.focus();
  document.forms[0].E01DEAACC.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
