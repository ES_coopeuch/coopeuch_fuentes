<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="datarec" class="datapro.eibs.beans.EPV118001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

	
  	 function cerrarVentana(){
		window.open('','_parent','');
		window.close(); 
  	}
  	
//  Process according with user selection
 function goAction(op) {
	
   	switch (op){
	//Cancel
	case 1:  {
 		//document.forms[0].SCREEN.value = "101";
 		cerrarVentana();
       	break;
		}
	}
	//document.forms[0].submit();
 }
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
	boolean newOnly=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<%
	// Determina si es nuevo o mantencion
	if (userPO.getPurpose().equals("NEW")){
			newOnly=false;
		} else if (userPO.getPurpose().equals("MAINTENANCE")) {
    		newOnly=false;
		} else {
    		newOnly=true;		
		}
%>

<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	CONSULTA DE SEGURO
<%} else if (maintenance){ %>
	MANTENIMIENTO DE SEGURO
<%} else { %>
	NUEVO SEGURO
<%} %>
 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="seguros_maintenance.jsp, EPV1180"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1180" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01PSGBNK"  value="<%= currUser.getE01UBK().trim()%>">
  <input type=HIDDEN name="H01FLGMAS"  value="<%= datarec.getH01FLGMAS().trim()%>">

 <% int row = 0;%>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="datarec" property="E01PSGCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Propuesta : 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="datarec" property="E01PSGNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">Sequencia :  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="datarec" property="E01PSGSEQ" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="3" maxlength="2" readonly="true"/>
             </td>
         </tr>
       </table>
      </td>
    </tr>
  </table>
  
  <h4>Poliza de Seguro </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" >

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="15%" > 
              <div align="right">Seguro :</div>
            </td>
            <td width="40%" > 
                 <eibsinput:cnofc name="datarec" property="E01PSGSCD" required="false" flag="IT" fn_code="E01PSGSCD" fn_description="E01PSGSNM" readonly="true" size="4" maxlength="4"/>
                 <eibsinput:text property="E01PSGSNM" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="true" size="40" maxlength="40" readonly="true"/>
	        </td>
            <td width="15%" > 
              <div align="right">Poliza Numero :</div>
            </td>
            <td width="30%" > 
                 <eibsinput:text name="datarec" property="E01PSGPLZ" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_REFERENCE%>" required="false" readonly="true"/>
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td > 
              <div align="right">Plan :</div>
            </td>
            <td > 
              <input type="text" name="E01PSGPLN" size="3" maxlength="2" value="<%= datarec.getE01PSGPLN().trim()%>">
              <a href="javascript:GetTablaSeguros('E01PSGPLN',document.forms[0].E01PSGSCD.value,'E01DSCPLN')" ><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"  ></a>
                 <eibsinput:text name="datarec" property="E01DSCPLN"  eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="true" size="40" maxlength="40" readonly="true"/>
	        </td>
           <td> 
              <div align="right">Numero de Meses :</div>
            </td>
            <td > 
 		        <eibsinput:text name="datarec" property="E01PSGNPM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="4" maxlength="3" readonly="<%=newOnly%>"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td > 
              <div align="right">Fecha Incicio :</div>
            </td>
            <td > 
    	        <eibsinput:date name="datarec" fn_year="E01PSGFIY" fn_month="E01PSGFIM" fn_day="E01PSGFID" readonly="<%=newOnly%>"/>
	        </td>
           <td> 
              <div align="right">Fecha Vencimiento :</div>
            </td>
            <td > 
    	        <eibsinput:date name="datarec" fn_year="E01PSGFVY" fn_month="E01PSGFVM" fn_day="E01PSGFVD" readonly="<%=newOnly%>"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td > 
              <div align="right">Moneda Seguro :</div>
            </td>
            <td> 
 	 			<eibsinput:help name="datarec" property="E01PSGCYS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" fn_param_one="E01PSGCYS" fn_param_two="document.forms[0].E01PSGBNK.value"  required="false" readonly="true" />
	        </td>
           <td > 
              <div align="right">Monto Asegurado :</div>
            </td>
            <td > 
 		        <eibsinput:text name="datarec" property="E01PSGMTS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td  > 
              <div align="right">Moneda Prima :</div>
            </td>
            <td > 
 	 			<eibsinput:help name="datarec" property="E01PSGCYP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" fn_param_one="E01PSGCYP" fn_param_two="document.forms[0].E01PSGBNK.value"  required="false" readonly="true"/>
	        </td>
           <td > 
              <div align="right">Monto Prima :</div>
            </td>
            <td > 
 		        <eibsinput:text name="datarec" property="E01PSGMTP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td > 
              <div align="right">Compania :</div>
            </td>
            <td > 
				<eibsinput:help name="datarec" property="E01PSGCCD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER %>" 
					fn_param_one="E01PSGCCD" fn_param_two="E01PSGCNM" fn_param_three="I" readonly="true"/>
                 <eibsinput:text property="E01PSGCNM" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="true"/>
	        </td>
           <td > 
              <div align="right">Prima Total :</div>
            </td>
            <td > 
 		        <eibsinput:text name="datarec" property="E01PSGPRT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true"/>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>
  
  
  <h4>Beneficiarios </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">Nro</div>
           </td>
           <td width="15%" > 
              <div align="center">Rut</div>
           </td>
           <td width="30%" > 
              <div align="center">Nombre</div>
           </td>
           <td width="10%" > 
              <div align="center">Telefono</div>
           </td>
           <td width="10%" > 
              <div align="Center">Relacion</div>
           </td>
           <td width="5%" > 
              <div align="center">%</div>
           </td>
           <td width="10%" > 
           </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">1</div>
           </td>
           <td width="15%" > 
 		        <eibsinput:text name="datarec" property="E01PSGRT1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="30%" > 
 		        <eibsinput:text name="datarec" property="E01PSGNM1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
 		        <eibsinput:text name="datarec" property="E01PSGPH1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
                  <select name="E01PSGRL1" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(datarec.getE01PSGRL1().equals("1")||datarec.getE01PSGRL1().equals("2") || datarec.getE01PSGRL1().equals("3")||datarec.getE01PSGRL1().equals("4")||datarec.getE01PSGRL1().equals("5")||datarec.getE01PSGRL1().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (datarec.getE01PSGRL1().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (datarec.getE01PSGRL1().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (datarec.getE01PSGRL1().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (datarec.getE01PSGRL1().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (datarec.getE01PSGRL1().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (datarec.getE01PSGRL1().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
           <td width="5%" > 
 		        <eibsinput:text name="datarec" property="E01PSGPR1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
           </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">2</div>
           </td>
           <td width="15%" > 
 		        <eibsinput:text name="datarec" property="E01PSGRT2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="30%" > 
 		        <eibsinput:text name="datarec" property="E01PSGNM2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
 		        <eibsinput:text name="datarec" property="E01PSGPH2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
                  <select name="E01PSGRL2" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(datarec.getE01PSGRL2().equals("1")||datarec.getE01PSGRL2().equals("2") || datarec.getE01PSGRL2().equals("3")||datarec.getE01PSGRL2().equals("4")||datarec.getE01PSGRL2().equals("5")||datarec.getE01PSGRL2().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (datarec.getE01PSGRL2().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (datarec.getE01PSGRL2().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (datarec.getE01PSGRL2().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (datarec.getE01PSGRL2().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (datarec.getE01PSGRL2().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (datarec.getE01PSGRL2().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
           <td width="5%" > 
 		        <eibsinput:text name="datarec" property="E01PSGPR2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
           </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">3</div>
           </td>
           <td width="15%" > 
 		        <eibsinput:text name="datarec" property="E01PSGRT3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="30%" > 
 		        <eibsinput:text name="datarec" property="E01PSGNM3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
 		        <eibsinput:text name="datarec" property="E01PSGPH3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
                  <select name="E01PSGRL3" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(datarec.getE01PSGRL3().equals("1")||datarec.getE01PSGRL3().equals("2") || datarec.getE01PSGRL3().equals("3")||datarec.getE01PSGRL3().equals("4")||datarec.getE01PSGRL3().equals("5")||datarec.getE01PSGRL3().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (datarec.getE01PSGRL3().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (datarec.getE01PSGRL3().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (datarec.getE01PSGRL3().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (datarec.getE01PSGRL3().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (datarec.getE01PSGRL3().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (datarec.getE01PSGRL3().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
           <td width="5%" > 
 		        <eibsinput:text name="datarec" property="E01PSGPR3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
           </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">4</div>
           </td>
           <td width="15%" > 
 		        <eibsinput:text name="datarec" property="E01PSGRT4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="30%" > 
 		        <eibsinput:text name="datarec" property="E01PSGNM4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
 		        <eibsinput:text name="datarec" property="E01PSGPH4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
                  <select name="E01PSGRL4" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(datarec.getE01PSGRL4().equals("1")||datarec.getE01PSGRL4().equals("2") || datarec.getE01PSGRL4().equals("3")||datarec.getE01PSGRL4().equals("4")||datarec.getE01PSGRL4().equals("5")||datarec.getE01PSGRL4().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (datarec.getE01PSGRL4().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (datarec.getE01PSGRL4().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (datarec.getE01PSGRL4().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (datarec.getE01PSGRL4().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (datarec.getE01PSGRL4().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (datarec.getE01PSGRL4().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
           <td width="5%" > 
 		        <eibsinput:text name="datarec" property="E01PSGPR4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
           </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
           <td width="10%" > 
           </td>
           <td width="5%" > 
              <div align="center">5</div>
           </td>
           <td width="15%" > 
 		        <eibsinput:text name="datarec" property="E01PSGRT5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="30%" > 
 		        <eibsinput:text name="datarec" property="E01PSGNM5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
 		        <eibsinput:text name="datarec" property="E01PSGPH5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
                  <select name="E01PSGRL5" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(datarec.getE01PSGRL5().equals("1")||datarec.getE01PSGRL5().equals("2") || datarec.getE01PSGRL5().equals("3")||datarec.getE01PSGRL5().equals("4")||datarec.getE01PSGRL5().equals("5")||datarec.getE01PSGRL5().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (datarec.getE01PSGRL5().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (datarec.getE01PSGRL5().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (datarec.getE01PSGRL5().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (datarec.getE01PSGRL5().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (datarec.getE01PSGRL5().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (datarec.getE01PSGRL5().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
           <td width="5%" > 
 		        <eibsinput:text name="datarec" property="E01PSGPR5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>"/>
           </td>
           <td width="10%" > 
           </td>
          </tr>
          
        </table>
      </td>
    </tr>
  </table>
 
  
  <h4>Propiedad </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="trdark"> 
            <td width="10%" > 
            </td>
           <td width="20%"> 
              <div align="right">Descripcion :</div>
            </td>
            <td width="40%"> 
			<%if (readOnly){ %>
                <textarea name="E01PSGRMK" cols="50" rows="5" readonly ><%= datarec.getE01PSGRMK()%> </textarea>
			<%} else { %>
                <textarea name="E01PSGRMK" cols="50" rows="5" ><%= datarec.getE01PSGRMK()%> </textarea>
		    <% } %>  
            </td>
            <td width="10%" > 
	        </td>
          </tr>


          <tr id="trdark"> 
            <td width="10%" > 
            </td>
           <td width="20%"> 
              <div align="right">Direcci�n :</div>
            </td>
            <td width="40%"> 
 		        <eibsinput:text name="datarec" property="E01PSGMA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>"  readonly="<%=readOnly %>"/>
            </td>
            <td width="10%" > 
	        </td>
          </tr>

          <tr id="trdark"> 
            <td width="10%" > 
            </td>
           <td width="20%"> 
              <div align="right"> </div>
            </td>
            <td width="40%"> 
 		        <eibsinput:text name="datarec" property="E01PSGMA2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>"  readonly="<%=readOnly %>"/>
            </td>
            <td width="10%" > 
	        </td>
          </tr>

          <tr id="trdark"> 
            <td width="10%" > 
            </td>
           <td width="20%"> 
              <div align="right"> </div>
            </td>
            <td width="40%"> 
 		        <eibsinput:text name="datarec" property="E01PSGMA3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>"  readonly="<%=readOnly %>"/>
            </td>
            <td width="10%" > 
	        </td>
          </tr>

          <tr id="trdark"> 
            <td width="10%" > 
            </td>
           <td width="20%"> 
              <div align="right"> </div>
            </td>
            <td width="40%"> 
 		        <eibsinput:text name="datarec" property="E01PSGMA4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>"  readonly="<%=readOnly %>"/>
            </td>
            <td width="10%" > 
	        </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>
  
   <%if  (!readOnly) { %>
      <div align="center"> 
          <input id="EIBSBTN" type=submit name="Submit" value="Enviar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>
    <% } else { %>
      <div align="center"> 
          <input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>     
    <% } %>  
  </form>

 <%if ("S".equals(request.getAttribute("ACT"))){%>
 <script type="text/javascript">
 	  //recargamos la pagina que nos llamo..	  
	  window.opener.location.href =  '<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1180?SCREEN=101&E01PSGCUN=<%=userPO.getCusNum()%>&E01PSGNUM=<%=userPO.getHeader23()%>';	   	   
     <%//NOTA: solo para activar el check de la pagina integral%>
     <%  String re =(String) session.getAttribute("EMPSG");%>
	 <%  if ("S".equals(re)){%>
			window.opener.parent.setRecalculate3();	        
	 <% } %>   	 
  	  //cerramos la ventana
  	  cerrarVentana();	  
 </script>
 <% } %>  
   
</body>
</HTML>
