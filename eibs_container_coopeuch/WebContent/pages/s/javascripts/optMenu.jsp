<%@ page contentType="application/x-javascript" %>

var menuImgRef;
var menuRef;
var hPopupRef;
var mnuHelpRef;

// Help Menu Global variable for english

var accountHelp=1;
var ledgerHelp=2;
var branchHelp=3;
var custdescidHelp=4;
var currencyHelp=5;
var conceptHelp=6;
var cnofHelp=7;
var costcenterHelp=8;
var accholdersHelp=9;
var codeDescCNOFC=10;
var causeHelp=11;
var conceptTransacHelp=12;
var staticFactHelp=13;
var staticFrecHelp=14;
var documentsFrecuencyHelp = 15;
var documentsTypeHelp = 16;
var documentsFeeCharges = 17;
var documentsFrecCharges = 18;
var concepttypeHelp = 19;
var lnreferHelp=20;
var openingWireHelp=21
var accountCustomerHelp=25;
var accountCustomerNameHelp=26;

var fieldName;
var fieldHlp1;
var fieldHlp2;
var fieldBank;
var fieldCCY;
var fieldOPCode;
var optHelp;
var menuactive = false;
var animactive = false;
var ontime = "";
var outtime = "";
var outtime2 = "";
var curlen = 0;
var popupStatus = "open"


//Help menu definitions
var language = "s/";
var prefix = "<%=request.getContextPath()%>/pages/";

function IncludeJavaScript(jsFile) {
	document.write('<script type="text/javascript" src="' + '<%=request.getContextPath()%>/pages/s/javascripts/' + jsFile + '"></script>');
}


IncludeJavaScript('Ajax.js');
IncludeJavaScript('optMenu.js');
IncludeJavaScript('graphical.js');

