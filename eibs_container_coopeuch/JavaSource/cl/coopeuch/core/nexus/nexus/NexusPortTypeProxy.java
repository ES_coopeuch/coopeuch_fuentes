package cl.coopeuch.core.nexus.nexus;

public class NexusPortTypeProxy implements cl.coopeuch.core.nexus.nexus.NexusPortType {
  private boolean _useJNDI = true;
  private String _endpoint = null;
  private cl.coopeuch.core.nexus.nexus.NexusPortType __nexusPortType = null;
  
  public NexusPortTypeProxy() {
    _initNexusPortTypeProxy();
  }
  
  private void _initNexusPortTypeProxy() {
  
    if (_useJNDI) {
      try {
        javax.naming.InitialContext ctx = new javax.naming.InitialContext();
        __nexusPortType = ((cl.coopeuch.core.nexus.nexus.NexusService)ctx.lookup("java:comp/env/service/NexusService")).getNexusSOAP();
      }
      catch (javax.naming.NamingException namingException) {}
      catch (javax.xml.rpc.ServiceException serviceException) {}
    }
    if (__nexusPortType == null) {
      try {
        __nexusPortType = (new cl.coopeuch.core.nexus.nexus.NexusServiceLocator()).getNexusSOAP();
        
      }
      catch (javax.xml.rpc.ServiceException serviceException) {}
    }
    if (__nexusPortType != null) {
      if (_endpoint != null)
        ((javax.xml.rpc.Stub)__nexusPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
      else
        _endpoint = (String)((javax.xml.rpc.Stub)__nexusPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
    }
    
  }
  
  
  public void useJNDI(boolean useJNDI) {
    _useJNDI = useJNDI;
    __nexusPortType = null;
    
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (__nexusPortType != null)
      ((javax.xml.rpc.Stub)__nexusPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.coopeuch.core.nexus.nexus.NexusPortType getNexusPortType() {
    if (__nexusPortType == null)
      _initNexusPortTypeProxy();
    return __nexusPortType;
  }
  
  public cl.coopeuch.core.nexus.RespuestaEstadosDeCuentaPDF consultaEstadosDeCuentaPDF(cl.coopeuch.core.nexus.DatosEstadosDeCuentaPDF entrada) throws java.rmi.RemoteException{
    if (__nexusPortType == null)
      _initNexusPortTypeProxy();
    return __nexusPortType.consultaEstadosDeCuentaPDF(entrada);
  }
  
  public cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados consultaMovimientosNoFacturados(cl.coopeuch.core.nexus.DatosMovimientosNoFacturados entrada) throws java.rmi.RemoteException{
    if (__nexusPortType == null)
      _initNexusPortTypeProxy();
    return __nexusPortType.consultaMovimientosNoFacturados(entrada);
  }
  
  
}