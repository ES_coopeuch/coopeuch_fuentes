<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import = "datapro.eibs.master.*,datapro.eibs.beans.*" %>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Tarjetas Creditos/Debitos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id= "ECC0040Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script type="text/javascript">

function setInfo(idx){  
 //  alert("index = " + idx); 
   document.forms[0].actRow.value = idx;      
}

function goAction(op) {
    var page = "";
   	var row = parseInt(document.forms[0].actRow.value);   
 	params = "&CURRCODE=" + row;
 	page = webapp + "/servlet/datapro.eibs.products.JSECC0040?SCREEN=300" + params;		
	CenterWindow(page,800,800,2);	 
}

</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Tarjetas del Cliente<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cc_inq_card_list.jsp,ECC0040"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0040">
<input type="hidden" name="SCREEN" value="201"> 
<INPUT TYPE=HIDDEN NAME="CURRCODE" VALUE="0"> 
 <INPUT TYPE=HIDDEN NAME="actRow" VALUE="0">
<%
	if (ECC0040Help.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>

<%
	} else { %>
	
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCMNUM" size="15" maxlength="15" readonly value="<%= userPO.getCusNum().trim()%>"> 
               </div>               
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre : </b></div>
            </td>
            <td nowrap width="20%"  > 
              <div align="left"> 
                <input type="text" name="E01CCMNME" size="35" maxlength="35" readonly value="<%= userPO.getCusName().trim()%>">                                 
              </div>
            </td>            
            <td nowrap width="16%" > 
              <div align="right"><b>Identif. Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCMCID" size="12" maxlength="12" readonly value="<%= userPO.getHeader1().trim()%>">                 
               </div>               
            </td>   
            </tr>         
        </table>
      </td>
    </tr>
  </table>
 <TABLE class="tbenter" width="100%">
	<TR>
		<TD ALIGN=CENTER class=TDBKG><a href="javascript:goAction(1)"><b>Consulta <br>Detalle</b></a>
		</TD>
		<TD ALIGN=CENTER class=TDBKG><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</TD>
	</TR>
</TABLE>
	<table class="tableinfo" id="dataTable" width="100%">
		<tr id="trclear">
			<th align="center" nowrap width="5%"></th>
			<th align="center" nowrap width="15%">Tarjeta</th>
			<th align="center" nowrap width="15%">Nro. Tarjeta</th>
    		<th align="center" nowrap width="15%">Cobranding Afinidad</th>
    		<th align="center" nowrap width="10%">Estado</th>    		
    		<th align="center" nowrap width="15%">BLoqueo</th>
       		<th align="center" nowrap width="10%">Tit./<br>Adic.</th>
       		<th align="center" nowrap width="5%">Tipo<br>Tarj.</th>       		
       		<th align="center" nowrap width="5%">Nro. Cuenta</th>
       		<th align="center" nowrap width="5%">Producto</th>
		</tr>
            <%
               ECC0040Help.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (ECC0040Help.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  datapro.eibs.beans.ECC004001Message msgList = (datapro.eibs.beans.ECC004001Message) ECC0040Help.getRecord();
		 %>
			<tr id="trdark">
            <td NOWRAP  align=CENTER> 
              <input type="radio" name="CURRCODE" value="<%= ECC0040Help.getCurrentRow() %>"  <%=chk%> onClick="setInfo(<%= ECC0040Help.getCurrentRow()%>)" >
             </td>											
			<td nowrap align="left"><%=msgList.getD01CCMPRO().trim()%></td>	
			<td nowrap align="left"><%=Util.formatCell(msgList.getE01CCRNUM())%></td>
			<td nowrap align="center"><%=msgList.getD01CCRFIN().trim()%></td>	
			<td nowrap align="center"><%=msgList.getD01CCRSTS().trim()%></td> 				
			<td nowrap align="center"><%=msgList.getD01CCRLKC().trim()%></td> 	
			<td nowrap align="center"><% if (msgList.getE01CCRTPI().equals("T")) {out.print("Titular");} else {out.print("Adicional");}%></td>				
			<td nowrap align="center"><%=msgList.getE01CCRTDC().trim()%> </td>	
			<td nowrap align="right" ><%=msgList.getE01CCRCRA().trim()%> </td>					
			<% if (msgList.getE01CCRTDC().equals("C")) {  %>
			<td nowrap align="center"><%=msgList.getE01CCMPRO().trim()%> </td>
			<% } else {  %>	
			<td nowrap align="center"><%=msgList.getE01ACMPRO().trim()%> </td>
			<% } %>						
		</tr>
		<%
			}
		%>
	</table>
<%
	}
%>
</form>

</body>
</html>

