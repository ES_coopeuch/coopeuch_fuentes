package com.datapro.eibs.parameters.access.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.General;
import com.datapro.eibs.exception.CommonCodeNotFoundException;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.parameters.access.jdbc.bean.CNOFC;
import com.datapro.generic.access.DAOSuper;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.BeanParser;
import com.datapro.sql.manager.DBConnector;

/**
 * @author ogarcia
 *
 */
public class DAOCommonCodes extends DAOSuper {

	private static Category cat =
		Category.getInstance(DAOCommonCodes.class.getName());

	/**
	 * Constructor for DAOCommonCodes.
	 * @param dbid
	 */
	public DAOCommonCodes(String dbid) {
		super(dbid);
	}

	/**
	 * Constructor for DAOCommonCodes.
	 * @param connection
	 */
	public DAOCommonCodes(Connection connection) {
		super(connection);
	}

	/**
	 * Constructor for DAOCommonCodes.
	 */
	public DAOCommonCodes() {
		super();
	}

	/**
	 * Get common code's record info.
	 */
	public CNOFC getCode(String flg, String cde)
		throws CommonCodeNotFoundException, DAOException {

		CNOFC bean = null;

		try {
			String sql = "SELECT * FROM CNOFC WHERE CNOCFL=? AND CNORCD=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, flg);
			ps.setString(2, cde);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNOFC();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} else {
				throw new CommonCodeNotFoundException(
					"Common Code not found exception(flag:" + flg + " - code:" + cde + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return bean;

	}

	/**
	 * Get common code's record info.
	 */
	public BeanList getCodeList(String flg)
		throws CommonCodeNotFoundException, DAOException {

		BeanList list = null;

		try {
			String sql = "SELECT * FROM CNOFC WHERE CNOCFL=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, flg);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				CNOFC bean = new CNOFC();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new CommonCodeNotFoundException(
					"Common Code not found exception(flag:" + flg + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}
}
