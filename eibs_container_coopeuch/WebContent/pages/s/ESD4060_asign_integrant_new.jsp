<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page import="datapro.eibs.beans.ESD406001Message"%>

<html>
<head>
<title>Nuevo Plan Socio</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<SCRIPT Language="Javascript">

function goAsignarProducto() {

	if(document.forms[0].CANTIDAD.value != "1")
	{
		if (document.forms[0].IDCLIENTEFILA.value != "")
		{
			if (document.forms[0].DESCRIPCION[document.forms[0].IDCLIENTEFILA.value].value == "R")
			{
				alert('Esta persona es PERSONA RELACIONADA para ser parte de un plan debe hacerlo SOCIO primero');
			}
			else if (document.forms[0].DESCRIPCION[document.forms[0].IDCLIENTEFILA.value].value == "C")
			{
				alert('Esta persona es CLIENTE para ser parte de un plan debe hacerlo SOCIO primero');
			}
			else if (document.forms[0].PERTENECEPLAN[document.forms[0].IDCLIENTEFILA.value].value == "S")
			{
				alert('Esta persona pertenece a otro plan');
			}
			else if (document.forms[0].AUTORIZAPLAN[document.forms[0].IDCLIENTEFILA.value].value == "1")
			{
				alert('Esta persona NO esta autorizado para tomar el plan');
			}
			else
			{
				document.forms[0].E01AIIDA.value = document.forms[0].IDCLIENTE[document.forms[0].IDCLIENTEFILA.value].value
				document.forms[0].SCREEN.value="300";
				document.forms[0].submit();
			}
		}
		else
			alert('Debe seleccionar una PERSONA RELACIONADA primero');
	}
	else 
	{
		if (document.forms[0].IDCLIENTEFILA.value != "")
		{
			if (document.forms[0].DESCRIPCION.value == "R")
			{
				alert('Esta persona es PERSONA RELACIONADA para ser parte de un plan debe hacerlo SOCIO primero');
			}
			else if (document.forms[0].DESCRIPCION.value == "C")
			{
				alert('Esta persona es CLIENTE para ser parte de un plan debe hacerlo SOCIO primero');
			}
			else if (document.forms[0].PERTENECEPLAN.value == "S")
			{
				alert('Esta persona pertenece a otro plan');
			}
			else if (document.forms[0].AUTORIZAPLAN.value == "1")
			{
				alert('Esta persona NO esta autorizado para tomar el plan');
			}
			else
			{
				document.forms[0].E01AIIDA.value = document.forms[0].IDCLIENTE.value
				document.forms[0].SCREEN.value="300";
				document.forms[0].submit();
			}
		}
		else
			alert('Debe seleccionar una PERSONA RELACIONADA primero');
	}
}

//  Process according with user selection
 var bandera;	
 function goAction(op) {
	
	
	switch (op){
	// Validate & Write 
  	case 1:  {
  		self.window.location.href = "<%=request.getContextPath()%>/pages/background.jsp";
       	break;
        }
	// Validate and Approve
	case 2:  {
 		self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4070?SCREEN=200";
       	break;
		}
	}
	
	if(bandera == 1){
	    bandera = 0;
		self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4060?SCREEN=500";
	}
}

function guardarID(obj){
	document.forms[0].IDCLIENTEFILA.value = obj.value;
}

function tienePlanSocio(){
	if (document.forms[0].PLANSOCIO.value == "N"){
		alert("El cliente ingresado no posee plan asociado");
		self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4060?SCREEN=100";
	}
}

</SCRIPT>

 
<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "userPOAux" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<jsp:useBean id= "msgPlanSocio" class= "datapro.eibs.beans.ESD406001Message"  scope="session" />
<jsp:useBean id= "ESD406001List" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "rut" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "planSocio" class= "java.lang.String"  scope="session" />
</head>

<body bgcolor="#FFFFFF" onload = "tienePlanSocio();">
<h3 align="center">Asignar Integrante<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="asign_integrant_new, ESD4060"  ></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4060" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
  <input type="hidden" name="E01FL5" size="2" maxlength="1" value="<%= client.getE01FL5().trim()%>">
  <input type="hidden" name="E01LGT" size="2" maxlength="1" value="<%= client.getE01LGT().trim()%>">
  <input type="hidden" name="IDCLIENTEFILA" size="2" maxlength="2">
  <input type="hidden" name="NUEVO" size="2" maxlength="2" value = "0">
  <input type="hidden" name="E01CUN" value="<%= userPO.getCusNum() %>" size=15 maxlength=9 >
  <input type="hidden" name="E01IDN" value="<%= userPO.getHeader2() %>" size=15 maxlength=9 >
  <input type="hidden" name="E01AIIDA" size=15 maxlength=9 >
  <input type="hidden" name="PLANSOCIO" value="<%= planSocio %>" size=15 maxlength=9 >
  <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">Cliente: </td>
					<td nowrap width="12%" aling="left" > <%=userPOAux.getCusNum() %> </td>
						
					<td nowrap width="6%" aling="right">RUT: </td>
					<td nowrap width="14%" aling="left" > <%=userPOAux.getHeader2() %> </td>
					
					<td nowrap width="8%" aling="right">Nombre: </td>
					<td nowrap width="20%" aling="left" > <%=userPOAux.getHeader3() %> </td>
					
					<td nowrap width="8%" aling="right">Estado Plan: </td>
					<td nowrap width="20%" aling="left" > <% if (msgPlanSocio.getE01AIED().equals("Y")) out.print("Activo"); else out.print("Inactivo"); %> </td> 
				</tr>
			</table> 
		</td>
      </tr>
    </table>
	<br>
	<table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">ID: </td>
					<td nowrap width="12%" aling="left" > <%=msgPlanSocio.getE01AIID() %> </td>
						
					<td nowrap width="6%" aling="right">Plan: </td>
					<td nowrap width="14%" aling="left" > <%=msgPlanSocio.getE01AINM() %> </td>
					
					<td nowrap width="8%" aling="right">Descripcion: </td>
					<td nowrap width="20%" aling="left" > <%=msgPlanSocio.getE01AIDC() %> </td>
					
					<td nowrap width="8%" aling="right">Status: </td>
					<td nowrap width="20%" aling="left" > <% if (msgPlanSocio.getE01AIES().equals("1")) out.print("Activo"); else out.print("Inactivo"); %> </td>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
    <table class="tbenter" width=100% align=center>
		<tr>
			<td class=TDBKG width="30%"> 
		    	<div align="center"><a href="javascript:goAsignarProducto()"><b>Agregar</b></a></div>              
		    </td>
		</tr>
  	</table>
  	<br>
  		<h4 align="center">Asociar Integrantes al Plan Familia Socio Titular</h4>
     	<table  id=cfTable class="tableinfo">
     		<tr>
     			<td NOWRAP valign="top" width="100%">
     				<table id="headTable" width="100%" border = "0">
     					<tr id="trdark">
     						<th align=center nowrap width="5%">&nbsp;</th>
     						<th align=center nowrap width="10%">RUT</th>
     						<th align=center nowrap width="25%"> Nombre Completo</th>
     						<th align=center nowrap width="10%"> Descripción</th>
     						<th align=center nowrap width="10%"> Estado</th>
     						<th align=center nowrap width="10%"> Agregado</th>
     					</tr>
     					<%
    	  					int row = 0;
    	  					int i = 0;
    	  					ESD406001List.initRow();
          					while (ESD406001List.getNextRow()) {
            					ESD406001Message msgList = (ESD406001Message) ESD406001List.getRecord();%>
     							<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>" >
     								<td NOWRAP align="center" width="5%">
     								<% if (!msgList.getE01AIIDA().equals(rut)) {%>
     									<input type="radio" name="ROW" value="<%= ESD406001List.getCurrentRow()%>" onclick="guardarID(this);">
     								<%} else { %>
     									<input type="radio" name="ROW" value="<%= ESD406001List.getCurrentRow()%>" onclick="guardarID(this);" disabled="true">
     								<% } %> 
     									<input type="hidden" name="IDCLIENTE" size="2" maxlength="10" value="<%= msgList.getE01AIIDA().trim()%>">
  										<input type="hidden" name="DESCRIPCION" size="2" maxlength="1" value="<%= msgList.getE01AIDAC().trim()%>">
  										<input type="hidden" name="PERTENECEPLAN" size="2" maxlength="1" value="<%= msgList.getE01AIAUX2().trim()%>">
  										<input type="hidden" name="AUTORIZAPLAN" size="2" maxlength="1" value="<%= msgList.getE01AIAUX3().trim()%>">
     								</td>
     								<td NOWRAP  align=center> <%= msgList.getE01AIIDA() %> </td>
     								<td NOWRAP  align=center > <%= msgList.getE01AINMA() %> </td>
     								<td NOWRAP align=center>  <% if( msgList.getE01AIDAC().equals("R")) out.print("PERSONA RELACIONADA"); %>
     														  <% if( msgList.getE01AIDAC().equals("C")) out.print("CLIENTE"); %>
     														  <% if( msgList.getE01AIDAC().equals("S")) out.print("SOCIO"); %>
     								 </td>
     								<td NOWRAP  align=center> <% if ( msgList.getE01AIST().equals("Y")) out.print("Activo"); else out.print("Inactivo");%> </td>
     								<% if (!msgList.getE01AIIDA().equals(rut)) {
     									if (msgList.getE01AIAUX4().equals("1")) {%>
											<td nowrap width="12%" align="center" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > </td>
											<%} else { %>
												<td nowrap width="12%" align="center" > <img src="<%=request.getContextPath()%>/images/CheckNO.gif" alt="mandatory field" align="bottom" border="0" > </td>
											<% } 
									  } else {%>
									  <td nowrap width="12%" align="center" > <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > </td>
									  <% } %> 
     							</tr>
     							
     							<%  i++; 
     						} %>
     					</table>
     					<input type="hidden" name="CANTIDAD" size="2" maxlength="10" value="<%= row%>">
     				</td>
     			</tr>
     		</table>
	<table width="100%">		
  	<tr>
			<td width="15%">
	  		  <div align="center"> 
	     		<input id="EIBSBTN" type="button" name="Submit" value="Imprimir" onClick="javascript:goAction(1);">
	     	  </div>	
	  		</td>
	  		<td width="15%">
	  		  <div align="center"> 
	     		<input id="EIBSBTN" type="button" name="Submit" value="Cancelar" onClick="javascript:goAction(1);">
	     	  </div>	
	  		</td>
			<td width="75%">
  		  		<div align="center">
				<input id="EIBSBTN" type="button" name="Submit2" value="Enviar" onClick="javascript:goAction(2);">
     	  	 	</div>	
	  		</td>
		
  	</tr>	
</table>	
</form>
</body>
</html>

