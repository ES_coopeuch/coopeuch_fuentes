package datapro.eibs.salesplatform;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV121701Message;


import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.sockets.MessageContext;


/**
 * hecho por Alonso Arana
 * 
 * Servlet implementation class JSEPV1217
 */
public class JSEPV1217 extends JSEIBSServlet {
	
	private static final long serialVersionUID = 1L;
	protected static final int A_PVPRD_LIST = 100;
	protected static final int R_PVPRD_NEW = 200;
	protected static final int R_PVPRD_MAINT = 201;
	protected static final int R_PVPRD_DELETE = 202;	
	protected static final int R_PVPRD_INQUIRY = 203;
	protected static final int A_PVPRD_MAINT = 600;
 
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case A_PVPRD_LIST:
			procActionTablaPVPRDList(user, req, res, session, null);
			break;
		case R_PVPRD_NEW:
			procReqTablaPVPRD(user, req, res, session, "NEW");
			break;
		case R_PVPRD_MAINT:
			procReqTablaPVPRD(user, req, res, session, "MAINTENANCE");
			break;
		case R_PVPRD_INQUIRY:
			procReqTablaPVPRD(user, req, res, session, "INQUIRY");
		//	procReqView(user, req, res, session);
			break;
		case A_PVPRD_MAINT:
			procActionMaintenance(user, req, res, session);
			break;
		case R_PVPRD_DELETE:
			procReqDelete(user, req, res, session);
			break;
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	/**
	 * procActionTablaPVPRDList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionTablaPVPRDList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String option)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EPV1217", req);

			EPV121701Message msg = (EPV121701Message) mp.getMessageRecord("EPV121701");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());
			//Sends message
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
 
			session.setAttribute("userPO", userPO);
			session.setAttribute("EPV121701List", list);
			forwardOnSuccess("EPV1217_credit_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqTablaPVPRD: This Method show a single  TablaPVPRD either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTablaPVPRD(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {

			
			mp = getMessageProcessor("EPV1217", req);
			userPO.setPurpose(option);			
			//Creates the message with operation code depending on the option
			EPV121701Message msg = (EPV121701Message) mp.getMessageRecord("EPV121701");
			msg.setH01USERID(user.getH01USR());
			msg.setH01TIMSYS(getTimeStamp());
			if (option.equals("NEW")) {

				flexLog("About to call Page: EPV1217_credit_maintenance.jsp");
				forward("EPV1217_credit_maintance.jsp", req, res);		
			
			
			} else if (option.equals("MAINTENANCE")) {
				msg.setH01OPECOD("0002");
			} else {
				msg.setH01OPECOD("0004");
			}
			
			//Sets the number for maintenance and inquiry options
			if (!option.equals("NEW") && req.getParameter("E01LISCOD") != null) {
				msg.setE01LISCOD(req.getParameter("E01LISCOD"));
			}

			//Send message
			flexLog("mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV121701Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go to maintenance page
				flexLog("About to call Page: EPV1217_credit_maintance.jsp");
				if (option.equals("INQUIRY")) {
					// if the request is an inquiry sets the readOlnly attribute 'true'
					forward("EPV1217_credit_maintance.jsp?readOnly=true", req, res);
				} else {
					forward("EPV1217_credit_maintance.jsp", req, res);
				}
			} else {
				//if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1217_credit_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
		
		
	}

	/**
	 * procActionMaintenance
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1217", req);

			EPV121701Message msg = (EPV121701Message) mp.getMessageRecord("EPV121701");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0005");
			msg.setH01TIMSYS(getTimeStamp());
			//Sets message with page field
			msg.setH01SCRCOD("01");			
			setMessageRecord(req, msg);
			
			
			
			if(req.getParameter("E01LISCNU") != null) {
				String num=req.getParameter("E01LISCNU");
				
				
				msg.setE01LISCNU(num);
			}
			
			
			//Sending message
			mp.sendMessage(msg);
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV121701Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go back to list
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1217?SCREEN=100", res);
			} else {
				//if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				forward("EPV1217_credit_maintance.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqDelete
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDelete(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("MAINTENANCE");

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1217", req);
			
			//Creates message with the 'Delete'operation code
			EPV121701Message msg = (EPV121701Message) mp.getMessageRecord("EPV121701");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0009");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets required values
			msg.setH01SCRCOD("01");
			//Sets the Code for delete options
			if (req.getParameter("E01LISCOD") != null) {	
				msg.setE01LISCOD(req.getParameter("E01LISCOD"));
			}
			
			//Send message
			mp.sendMessage(msg);

			//Receive Error and Data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV121701Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//If there are no errors request the list again
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1217?SCREEN=100", res);
			} else {
				//if there are errors show the list without updating
				session.setAttribute("error", msgError);
				forward("EPV1217_maintance_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	protected void procReqView(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			EPV121701Message msgDoc = null;
			UserPos userPO = null;

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			// Receive Data
			try {
				JBObjList bl = (JBObjList) ses.getAttribute("EPV121701List");
				int idx = Integer.parseInt(req.getParameter("COL"));
				bl.setCurrentRow(idx);

				msgDoc = (EPV121701Message) bl.getRecord();
			
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("cnvObj", msgDoc);

				try {
					forward("EPV1216_PVPRD_maintenance.jsp?readOnly=true", req, res);

				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}

}
