package datapro.eibs.tefmulti;

import com.adobe.fdf.FDFDoc;
import com.datapro.generics.Util;
import com.ibm.crypto.provider.Seal;

import datapro.eibs.beans.*;
import datapro.eibs.master.*;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class JSETE0200 extends JSEIBSServlet {

	private static final long serialVersionUID = 1L;

	protected static final int R_ENTER_CONTRACT = 100;
	protected static final int A_ENTER_CONTRACT = 200;

	protected static final int A_SIGNING_CONTRACT = 300;
	protected static final int A_ASSIGN_CARD = 400;


	
	public JSETE0200() {
	}

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_ENTER_CONTRACT: 
			procReqEnterContract(user, req, res, session);
			break;

		case A_ENTER_CONTRACT: 
			procActEnterContract(user, req, res, session);
			break;

		case A_SIGNING_CONTRACT : 
			procSigningContract(user, req, res, session);
			break;

		case A_ASSIGN_CARD:
			procAssignCard(user, req, res, session);
			break;
	
		

//			
			
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	
	protected void procReqEnterContract(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException 
	{
		
		ESD008001Message msgClient = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	

		try {
			msgClient = new datapro.eibs.beans.ESD008001Message(); 
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos(); 
			userPO.setOption("CLIENT");
 			userPO.setPurpose("CTR");
			userPO.setRedirect("/servlet/datapro.eibs.tefmulti.JSETE0200?SCREEN=200");
			ses.setAttribute("client", msgClient);
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

	  	} catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		try {
			flexLog("About to call Page: ESD0080_client_both_enter.jsp");
			forward("ESD0080_client_both_enter.jsp", req, res);
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}	
	}

	protected void procActEnterContract(ESS0030DSMessage user,	HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException 
			{
		
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("ETE0200", req);

			ETE020001Message msg = (ETE020001Message) mp.getMessageRecord("ETE020001", user.getH01USR(), "0001");
			
			if(!userPO.getCusNum().equals(""))
			{	
				String a =  userPO.getCusNum();
				msg.setE01ETCUN(userPO.getCusNum());
			}	
			else
			{	
				try 
		 		{
					if (req.getParameter("E01CUN") != null)
						msg.setE01ETCUN(req.getParameter("E01CUN"));
		 		}
				catch (Exception e)
				{
					msg.setE01ETCUN("0");
					flexLog("Input data error " + e);
				}
				try 
				{
					if (req.getParameter("E01IDN") != null && req.getParameter("E01IDN").indexOf("-") != -1	)
						msg.setE01ETRUT(req.getParameter("E01IDN"));			 		
					else 
						msg.setE01ETRUT(req.getParameter("E01IDN"));
				}
				catch (Exception e)
				{
					msg.setE01ETRUT("");
					flexLog("Input data error " + e);
				}
			}
			
			mp.sendMessage(msg);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) 
			{
				userPO.setOption("CLIENT");
	 			userPO.setPurpose("CTR");
				userPO.setRedirect("/servlet/datapro.eibs.tefmulti.JSETE0200?SCREEN=200");
				session.setAttribute("error", error);
				session.setAttribute("userPO", userPO);

				flexLog("About to call Page: ESD0080_client_both_enter.jsp");
				forward("ESD0080_client_both_enter.jsp", req, res);
			} 
			else 
			{
				msg = (ETE020001Message) mp.receiveMessageRecord();

				userPO.setCusNum(msg.getE01ETCUN());
				session.setAttribute("userPO", userPO);
				session.setAttribute("consig", msg);

				forwardOnSuccess("ETE0200_contract_signing_channels.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procSigningContract(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
	
		try {
			mp = getMessageProcessor("ETE0200", req);

			ETE020001Message msg = (ETE020001Message) mp.getMessageRecord("ETE020001", user.getH01USR(), "0002");

			setMessageRecord(req, msg);

			mp.sendMessage(msg);
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ETE020001Message) mp.receiveMessageRecord();

			if (mp.hasError(msgError)) {
				session.setAttribute("consig", msg);
				session.setAttribute("error", msgError);
				forwardOnSuccess("ETE0200_contract_signing_channels.jsp", req, res);
			} 
			else 
			{
				procActEnterContract(user,req,res,session);
			
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	
	protected void procAssignCard(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
	
		try {
			mp = getMessageProcessor("ETE0200", req);

			ETE020001Message msg = (ETE020001Message) mp.getMessageRecord("ETE020001", user.getH01USR(), "0003");

			setMessageRecord(req, msg);

			mp.sendMessage(msg);
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ETE020001Message) mp.receiveMessageRecord();

			if (mp.hasError(msgError)) {
				session.setAttribute("consig", msg);
				session.setAttribute("error", msgError);
				forward("ETE0200_contract_signing_channels.jsp", req, res);
			} 
			else 
			{
				procActEnterContract(user,req,res,session);
			
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	

//
}
