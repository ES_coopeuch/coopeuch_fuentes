<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.List"%>
<%@page import="datapro.eibs.beans.EDET05002Message"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datapro.eibs.beans.EDET05001Message"%><html>
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mantenedor Deterioro</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR"
	content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<TITLE>Conexi�n</TITLE>
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<script language="javascript">
  var reason = '';
  var accOfac = '';
  var accWarn = '';

  function goAction(opt) {
	document.forms[0].SCREEN.value = opt;
	document.forms[0].submit();
  }
 
 function goExit(){
    window.location.href="<%=request.getContextPath()%>/pages/background.jsp";
  }
  
  function showInqWarn(fieldValue){
	page = webapp + "/servlet/datapro.eibs.client.JSEDET050?SCREEN=900&E01IDN=" + fieldValue;
	CenterWindow(page,800,500,2);  
	showAddInfo(fieldValue);
}

function showAddInfo(idxRow){
   if (document.forms[0]["STSOFAC"+idxRow].value == "3") {
      var formLength = document.forms[0].elements.length;
      for (var n=0;n<formLength;n++){
        var elemt = document.forms[0].elements[n];
        if ( elemt.checked ) {
              accOfac=elemt.value;
              break;
        }
      }      
   } else {
      accOfac = "";
   }
   if (document.forms[0]["STSWARN"+idxRow].value == "A") {
      var formLength = document.forms[0].elements.length;
      for (var n=0;n<formLength;n++){
        var elemt = document.forms[0].elements[n];
        if ( elemt.checked ) {
              accWarn=elemt.value;
              break;
        }
      }      
   } else {
      accWarn = "";
   }

   tbAddInfo.rows[0].cells[1].style.color="white";
   tbAddInfo.rows[0].cells[1].innerHTML=document.forms[0]["TXTDATA"+idxRow].value;
   tbAddInfo.rows[0].cells[1].style.color="";
   if (tbAddInfo.rows[0].cells[1].filters[0])
	   tbAddInfo.rows[0].cells[1].filters[0].Play();
   for ( var i=0; i<dataTable.rows.length; i++ ) {
       dataTable.rows[i].className="TRNORMAL";
    }
    document.getElementById(idxRow).className = "TRHIGHLIGHT";
    document.getElementById("E01IDN"+idxRow).checked=1;
   	adjustTables(headTable, dataTable, dataDiv1,2,1);
  }   

</script>
<style type="text/css">
.cabecera {
	background-color: #ffffff;
	text-align: center;
}

.info {
	text-align: right;
	font-size: 14px;
}

body {
	font-size: 14px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}

.TRNORMAL {
	color: #990000;
	background-color: #EFF0F1;
}

.TRHIGHLIGHT {
	color: #990000;
	background-color: white;
}

.trdark {
	background-color: #FFFFFF;
	font-family: "Verdana, Arial, Helvetica, sans-serif";
	font-size: 8pt;
	color: #990000;
	height: 20pt;
}

.SCBARCOLOR {
	scrollbar-3dlight-color: #F5F5F5;
	scrollbar-arrow-color: navy;
	scrollbar-base-color: lightgray;
	scrollbar-darkshadow-color: darkblue;
	scrollbar-face-color: #D1D1D1;
	scrollbar-highlight-color: #DCDCDC;
	scrollbar-shadow-color: darkgray;
	scrollbar-track-color: #EFF0F1;
}
</style>
</head>
<body onload="adjustTables(headTable, dataTable, dataDiv1,2,1);">
<%
	List<EDET05001Message> detalles = new ArrayList<EDET05001Message>();
	detalles = (List<EDET05001Message>) session.getAttribute("EDET05001Message");
%>
<form
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSEDET050">
<input type="hidden" name="SCREEN" value="300">
<h3 align="center">Bandeja Aprobaci&oacute;n Mantenedor Deterioro<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"	name="EIBS_GIF" alt="list_client_details, EDET050"></h3>
<hr size="4">
<%
	if (detalles.size() == 0) {
%>
<div align="center" style="text-align: center;"><h3>No hay resultados para su b&uacute;squeda</h3><br> 
	<a href="javascript:goExit()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5','','<%=request.getContextPath()%>/images/s/exit_over.gif',1)">
		<img name="Image5" border="0" src="<%=request.getContextPath()%>/images/s/EXIT.gif">
	</a>
</div>
<%
	} else {
%>
<TABLE class="tbenter" style="width: 100%; background-color: #ffffff">
	<TR>
		<TD ALIGN=CENTER width="33%">
			<a href="javascript:goAction('500')" id="linkApproval" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','<%=request.getContextPath()%>/images/s/approve_over.gif',1)">
				<img name="Image1" alt="Approval" border="0" src="<%=request.getContextPath()%>/images/s/approve.gif">
			</a>
		</TD>
		<TD ALIGN=CENTER width="33%">
			<a href="javascript:goAction('600')" id="linkReject" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image2','','<%=request.getContextPath()%>/images/s/reject_over.gif',1)">
				<img name="Image2" alt="Reject" border="0" src="<%=request.getContextPath()%>/images/s/reject.gif">
			</a>
		</TD>
		<TD ALIGN=CENTER width="34%">
			<a href="javascript:goExit()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5','','<%=request.getContextPath()%>/images/s/exit_over.gif',1)">
				<img name="Image5" border="0" src="<%=request.getContextPath()%>/images/s/EXIT.gif">
			</a>
		</TD>
	</TR>
</TABLE>

<!-- TABLA COPIADA -->
<TABLE id="mainTable" class="tableinfo" width="100%">
	<TR>
		<TD NOWRAP valign="top" width="100%">
			<TABLE id="headTable" width="100%">
				<TR class="trdark">
					<TH ALIGN=CENTER rowspan="2" nowrap>&nbsp;</TH>
					<TH ALIGN=CENTER rowspan="2" nowrap>RUT</TH>
					<TH ALIGN=CENTER colspan="2" nowrap>Cliente</TH>
					<TH ALIGN=CENTER rowspan="2" nowrap>Accion</TH>
					<TH ALIGN=CENTER rowspan="2" nowrap>Motivo</TH>
				</TR>
				<TR class="trdark">
					<TH ALIGN=CENTER nowrap>Numero</TH>
					<TH ALIGN=CENTER nowrap>Nombre</TH>
				</TR>
			</table>
			<div id="dataDiv1" class="SCBARCOLOR" style="padding:0" NOWRAP>
    			<table id="dataTable" cellspacing="0"  >
	    			<%
						for (EDET05001Message message : detalles) {
					%>
					<tr id="<%=message.getE01NRORUT()%>" <% if (detalles.indexOf(message) == 0){out.print("class=\"TRHIGHLIGHT\"");}else{out.print("class=\"TRNORMAL\"");} %>>
						<td NOWRAP><input type="radio" id="E01IDN<%=message.getE01NRORUT()%>" name="E01IDN" <% if (detalles.indexOf(message) == 0){out.print("checked");} %> value="<%=message.getE01NRORUT()%>" onclick="showAddInfo(<%=message.getE01NRORUT()%>)"></td>
						<td NOWRAP><a href="javascript:showInqWarn(<%=message.getE01NRORUT()%>);"><%=message.getE01NRORUT()%></a></td>
						<td NOWRAP align="left"><a href="javascript:showInqWarn(<%=message.getE01NRORUT()%>);"><%=message.getE01NUMCLI()%></a></td>
						<td NOWRAP><a href="javascript:showInqWarn(<%=message.getE01NRORUT()%>);"><%=message.getE01CNA1()%></a></td>
						<td NOWRAP><a href="javascript:showInqWarn(<%=message.getE01NRORUT()%>);">
						<%
							if (message.getE01FLGMAR().trim().equalsIgnoreCase("m")) {
						%>MARCA<%
							} else {
						%>DESMARCA<%
							}
						%>
							<INPUT TYPE=HIDDEN NAME="TXTDATA<%=message.getE01NRORUT()%>" VALUE="<%=message.getE01USER()%><br><%=message.getE01FCHITI().substring(0,10).substring(8, 10) + "/" + message.getE01FCHITI().substring(0,10).substring(5, 7) + "/" + message.getE01FCHITI().substring(0,10).substring(0, 4)%><br><%=message.getE01FCHITI().substring(11,19).replace('.', ':')%><br>Ingresada">
							<INPUT TYPE=HIDDEN NAME="STSOFAC<%=message.getE01NRORUT()%>" VALUE="">
							<INPUT TYPE=HIDDEN NAME="STSWARN<%=message.getE01NRORUT()%>" VALUE="">
							</a>
						</td>
						<td NOWRAP><a href="javascript:showInqWarn(<%=message.getE01NRORUT()%>);"><%=message.getE01TXTMOT()%></a></td>
					</tr>
					<%
						}
					%>
    			</table>
   			</div>
			
		</TD>
		<TD nowrap ALIGN="RIGHT" valign="top">
			<Table id="tbAddInfoH" width="100%">
				<tr id="trdark" height="58px;">
					<TH ALIGN=CENTER nowrap class="cabecera">Informaci�n B�sica</TH>
				</tr>
			</Table>
			<Table id="tbAddInfo">
				<tr id="trclear">
					<TD ALIGN="RIGHT" valign="center" nowrap><b>
					Usuario : <br>
					Fecha : <br>
					Hora :<br>
					Observaci&oacute;n : <br></b></TD>
					<TD ALIGN="LEFT" valign="center" nowrap class="tdaddinfo">
						<%=detalles.get(0).getE01USER()%><br><%=detalles.get(0).getE01FCHITI().substring(0,10).substring(8, 10) + "/" + detalles.get(0).getE01FCHITI().substring(0,10).substring(5, 7) + "/" + detalles.get(0).getE01FCHITI().substring(0,10).substring(0, 4)%><br><%=detalles.get(0).getE01FCHITI().substring(11,19).replace('.', ':')%><br>Ingresada
					</TD>
				</tr>
			</Table>
		</TD>
	</TR>
</TABLE>
<br>

<!-- FIN TABLA COPIADA -->

<%
	int reinli = 0;
		try {
			reinli = Integer.parseInt(session.getAttribute("reinli")
					.toString());
			System.out.println(reinli);
		} catch (Exception e) {
		}
%>
<table width="98%" align="center" style="background-color: #ffffff;">
	<tr>
		<td width="50%" height="25%" align="left">
		<input type="hidden" name="E01REINLI" value="<%=reinli%>">
		<%
			boolean first = Boolean.parseBoolean(session.getAttribute(
						"first").toString());
		%>
		<%
			if (!first) {
		%>
			<a
			href="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSEDET050?SCREEN=400&E01REINLI=<%=reinli
							- Integer.parseInt(session.getAttribute("normal")
									.toString())%>&normal=<%=session.getAttribute("normal")%>">
		<img alt=""
			src="<%=request.getContextPath()%>/images/s/previous_records.gif" border="0">
		</a>
		<%
			}
		%>
		</td>
		<td width="50%" height="25%" align="right">
		<%
			boolean mas = Boolean.parseBoolean(session.getAttribute("more")
						.toString());
		%>
		<%
			if (mas) {
		%>
			<a href="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSEDET050?SCREEN=400&E01REINLI=<%=reinli
							+ Integer.parseInt(session.getAttribute("normal")
									.toString())%>&normal=<%=session.getAttribute("normal")%>">
				<img alt="" src="<%=request.getContextPath()%>/images/s/next_records.gif" border=0>
			</a>
		<%
			}
		%>
		</td>
	</tr>
</table>
<SCRIPT language="JavaScript">
  function resizeDoc() {
       resize(true);
       adjustTables(headTable, dataTable, dataDiv1,2,1);
  }
  showChecked("ACCNUM");
  resizeDoc();
  tbAddInfoH.rows[0].cells[0].height = headTable.rows[0].cells[0].clientHeight;
  window.onresize=resizeDoc;
  
  function resize(addInfo) {
		var minValue = mainTable.offsetTop + dataDiv1.offsetTop + 30;
		var h = document.body.clientHeight - minValue;
		var totalrow = parseInt(document.forms[0].totalRow.value);
		var maxHeight = totalrow * 20; // dataDiv1.childNode.offsetHeight;
	
		if (addInfo) {
			minValue = mainTable.offsetTop + dataDiv1.offsetTop
					+ tbAddInfo.offsetHeight + 4;
		}
		h = (h <= 0) ? maxHeight : h;
		if (totalrow > 10 && document.body.clientHeight > minValue) {
			if (h < maxHeight) {
				dataDiv1.style.height = h + "";
				dataDiv1.style.overflowY = "scroll";
			} else {
				dataDiv1.style.height = maxHeight + "";
				dataDiv1.style.overflowY = "";
			}
		} else if (totalrow > 10 && document.body.clientHeight <= minValue) {
			dataDiv1.style.height = (addInfo) ? "" + tbAddInfo.offsetHeight : "200";
			dataDiv1.style.overflowY = "scroll";
		}
	}
  function adjustTables(Table1, Table2, Div1, column, rb) {
	var T1 = Table1.rows[0];
	var T11 = Table1.rows[1];
	var maxT11Col = T11.cells.length - 1;
	var wT1 = 0;
	var wT2 = 0;
	var n = column;
	var maxWidth = 0;
	var adjPerCol = 0;
	var adjust = 0;
	var mainTb = Table1.parentNode; // TD

	if (Table2.rows.length >= 1) {

		var T2 = Table2.rows[0];
		var maxCol = Table2.rows[0].cells.length;
		var incr = maxCol * 2;

		for (i = 0; i < n; i++) {
			T1.cells[i].style.pixelWidth = 0;
			T2.cells[i].style.pixelWidth = 0;
			wT1 = T1.cells[i].offsetWidth;
			wT2 = T2.cells[i].offsetWidth;
			if (wT1 > wT2) {
				T1.cells[i].style.pixelWidth = wT1;
				T2.cells[i].style.pixelWidth = wT1;
			} else {
				T1.cells[i].style.pixelWidth = wT2;
				T2.cells[i].style.pixelWidth = wT2;
			}
			maxWidth = (wT1 > wT2) ? maxWidth + wT1 : maxWidth + wT2;
		}

		for (i = n; i < maxCol; i++) {

			if (i >= n && i <= (n + maxT11Col)) {
				T11.cells[i - n].style.pixelWidth = 0;
				wT1 = T11.cells[i - n].offsetWidth;
			} else {
				T1.cells[i - maxT11Col].style.pixelWidth = 0;
				wT1 = T1.cells[i - maxT11Col].offsetWidth;
			}

			T2.cells[i].style.pixelWidth = 0;
			wT2 = T2.cells[i].offsetWidth;

			if (wT1 > wT2) {
				T2.cells[i].style.pixelWidth = wT1;
				if (i >= n && i <= (n + maxT11Col)) {
					T11.cells[i - n].style.pixelWidth = wT1;
				} else {
					T1.cells[i - maxT11Col].style.pixelWidth = wT1;
				}
			} else {
				T2.cells[i].style.pixelWidth = wT2;
				if (i >= n && i <= (n + maxT11Col)) {
					T11.cells[i - n].style.pixelWidth = wT2;
				} else {
					T1.cells[i - maxT11Col].style.pixelWidth = wT2;
				}
			}
			maxWidth = (wT1 > wT2) ? maxWidth + wT1 : maxWidth + wT2;
		}

		maxWidth += incr;
		Table1.width = maxWidth;
		Table2.Width = Table1.Width;

		if (Table1.clientWidth < mainTb.clientWidth) {
			incr = (Div1.style.overflowY == "scroll") ? Table1.clientWidth + 18
					: Table1.clientWidth;
			adjust = mainTb.clientWidth - incr;
			adjPerCol = adjust / (maxCol - rb);
			adjPerCol = Math.round(adjPerCol);

			for (k = 0; k < n; k++) {
				wT1 = T1.cells[k].style.pixelWidth;
				T1.cells[k].style.pixelWidth = (k == (rb - 1)) ? wT1 : wT1
						+ adjPerCol;
				wT2 = T2.cells[k].style.pixelWidth;
				T2.cells[k].style.pixelWidth = (k == (rb - 1)) ? wT2 : wT2
						+ adjPerCol;
			}

			for (k = n; k < maxCol; k++) {
				if (k >= n && k <= (n + maxT11Col)) {
					wT1 = T11.cells[k - n].style.pixelWidth;
					T11.cells[k - n].style.pixelWidth = (k == (rb - 1)) ? wT1
							: wT1 + adjPerCol;
				} else {
					wT1 = T1.cells[k - maxT11Col].style.pixelWidth;
					T1.cells[k - maxT11Col].style.pixelWidth = (k == (rb - 1)) ? wT1
							: wT1 + adjPerCol;
				}
				wT2 = T2.cells[k].style.pixelWidth;
				T2.cells[k].style.pixelWidth = (k == (rb - 1)) ? wT2 : wT2
						+ adjPerCol;
			}

		}

	} else {
		Table1.width = "100%"
	}
}
  
  
</SCRIPT>
 <%
 }
%>
</form>
</body>
</html>