/**
 * SinacofiWSPortType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public interface SinacofiWSPortType extends java.rmi.Remote {
    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.Salida sinacofiWS(cl.Sinacofi.coopeuch.creditos.predictorriesgo.DatosConsulta defaultInput) throws java.rmi.RemoteException;
    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResumenAntecedentesComercialesResponse resumenAntecedentesComerciales(cl.Sinacofi.coopeuch.creditos.predictorriesgo.DatosConsultaSum defaultInput) throws java.rmi.RemoteException;
}
