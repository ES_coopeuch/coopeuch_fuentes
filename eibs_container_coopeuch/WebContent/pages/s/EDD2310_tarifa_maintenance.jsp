<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Condiciones de Cuenta Vista</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<%@ page import = "java.io.*,java.net.*,datapro.eibs.beans.*,datapro.eibs.master.*,java.math.*" %>

<jsp:useBean id="rtBasic" class="datapro.eibs.beans.EDD231001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

</head>
<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
   
%> 
<H3 align="center">Condiciones de Cuenta Vista<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="tarifa_maintenance, EDD2310"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2310">
  <INPUT TYPE=HIDDEN NAME="SCREEN" value="300">
  <INPUT TYPE=HIDDEN NAME="E01ACMATY" VALUE="<%= rtBasic.getE01ACMATY().trim()%>">
  <INPUT TYPE=HIDDEN NAME="E01ACMPRO" VALUE="<%= rtBasic.getE01ACMPRO().trim()%>">
  <INPUT TYPE=HIDDEN NAME="H01FLGWK1" VALUE="<%= rtBasic.getH01FLGWK1().trim()%>">
  
  
   
  
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01ACMCUN" size="10" maxlength="9" value="<%= rtBasic.getE01ACMCUN().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"> 
                <input type="text" name="E01CUSNA1" size="45" maxlength="45" readonly value="<%= userPO.getHeader3().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Producto :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"> 
                <input type="text" name="E01ACMPRO" size="4" maxlength="4" readonly value="<%= rtBasic.getE01ACMPRO().trim()%>" readonly>
              </div>
            </td>
            
            
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
                <input type="text" name="E01ACMACC" size="13" maxlength="12" value="<%= userPO.getIdentifier().trim()%>" readonly >
              </div>
            </td>
             <td nowrap width="16%" > 
              <div align="right"><b>Descripción :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><b>
                <input type="text" name="E01ACMDSC" size="45" maxlength="45" value="<%= rtBasic.getE01ACMDSC().trim()%>" readonly>
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
                <input type="text" name="E01ACMCCY" size="3" maxlength="3" value="<%= userPO.getCurrency().trim()%>" readonly>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <H4>Datos B&aacute;sicos de la Cuenta</H4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
         <tr id="trdark"> 
            <td nowrap width="29%"> 
              <div align="right">Fecha de Apertura Cuenta Vista:</div>
            </td>
            <td nowrap width="19%">
              <input type="text" name="E01ACMOPD" size="3" maxlength="2" value="<%= rtBasic.getE01ACMOPD().trim()%>" readonly> 
              <input type="text" name="E01ACMOPM" size="3" maxlength="2" value="<%= rtBasic.getE01ACMOPM().trim()%>" readonly>
              <input type="text" name="E01ACMOPY" size="5" maxlength="4" value="<%= rtBasic.getE01ACMOPY().trim()%>" readonly>
            </td>
            <td nowrap width="26%"> 
              <div align="right">Saldo Disponible :
              </div>
            </td>
            <td nowrap width="26%">
				<input type="text" name="E01ACMMNB" size="15" maxlength="12" value="<%= rtBasic.getE01ACMMNB().trim()%>" readonly class="TXTRIGHT"> 			   
            </td>
          </tr>
         </table>
      </td>
    </tr>
  </table>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
  		<table cellspacing="0" cellpadding="2" width="100%" border="0">
	  		<tr id="trclear"> 
	            <td nowrap width="16%"> 
	              <div align="right">Tipo de Plan :</div>
	            </td>
	            <td nowrap width="34%"> 
	               <input type="text" name="E01ACMACL" size="3" maxlength="2" value="<%= rtBasic.getE01ACMACL().trim()%>" readonly>              
	               <input type="text" name="E01ACMACLG" size="47" maxlength="80" value="<%= rtBasic.getE01ACMACLG().trim()%>" readonly>              
	            </td>
	            <%if(rtBasic.getH01FLGWK1().equals("1")){ %>
	            <td nowrap width="16%">
	            	<div align="right">Nuevo Plan :
	              	</div>
	            </td>
	            <td nowrap width="34%"> 
	               <input type="text" name="E01CMSPPTN" size="3" maxlength="2" value="<%= rtBasic.getE01CMSPPTN().trim()%>" readonly>              
	               <input type="text" name="E01CMSPCLG" size="50" maxlength="80" value="<%= rtBasic.getE01CMSPCLG().trim()%>" readonly>              
	            </td>
	            <%} else {%>
	            <td nowrap width="16%">
	            </td>
	            <td nowrap width="34%"> 
	            </td>
	            <%}%>
	        </tr>        
	        <tr id="trdark"> 
	            <td nowrap> 
	              <div align="right">Fecha de Modificaci&oacute;n Plan :</div>
	            </td>
	            <td nowrap width>
	              <input type="text" name="E01ACMUPD" size="3" maxlength="2" value="<%= rtBasic.getE01ACMUPD().trim()%>" readonly>
	              <input type="text" name="E01ACMUPM" size="3" maxlength="2" value="<%= rtBasic.getE01ACMUPM().trim()%>" readonly>
	              <input type="text" name="E01ACMUPY" size="5" maxlength="4" value="<%= rtBasic.getE01ACMUPY().trim()%>" readonly>
	            </td>
	            <%if(rtBasic.getH01FLGWK1().equals("1")){ %>
	            <td nowrap> 
	              <div align="right">F. Cambio Nuevo Plan :
	              </div>
	            </td>
	            <td nowrap >
	              <input type="text" name="E01CMSPFSD" size="3" maxlength="2" value="<%= rtBasic.getE01CMSPFSD().trim()%>" readonly>
	              <input type="text" name="E01CMSPFSM" size="3" maxlength="2" value="<%= rtBasic.getE01CMSPFSM().trim()%>" readonly>
	              <input type="text" name="E01CMSPFSY" size="5" maxlength="4" value="<%= rtBasic.getE01CMSPFSY().trim()%>" readonly>
	            </td>
	            <%} else {%>
	            <td nowrap>
	            </td>
	            <td nowrap > 
	            </td>
	            <%}%>
	          </tr>

            <%if(rtBasic.getH01FLGWK1().equals("1")){ %>
	        <tr id="trclear"> 
	            <td nowrap > 
	              <div align="right"></div>
	            </td>
	            <td nowrap>
	            </td>
	            <td nowrap> 
	              <div align="right">F. Vigencia Nuevo Plan :
	              </div>
	            </td>
	            <td nowrap >
	              <input type="text" name="E01CMSPFID" size="3" maxlength="2" value="<%= rtBasic.getE01CMSPFID().trim()%>" readonly>
	              <input type="text" name="E01CMSPFIM" size="3" maxlength="2" value="<%= rtBasic.getE01CMSPFIM().trim()%>" readonly>
	              <input type="text" name="E01CMSPFIY" size="5" maxlength="4" value="<%= rtBasic.getE01CMSPFIY().trim()%>" readonly>
	            </td>
	          </tr>

	        <tr id="trclear"> 
	            <td nowrap > 
	              <div align="right"></div>
	            </td>
	            <td nowrap>
	            </td>
	            <td nowrap> 
	              <div align="right">Vendedor :
	              </div>
	            </td>
	            <td nowrap >
	               <input type="text" name="E01CMSPUC1" size="3" maxlength="2" value="<%= rtBasic.getE01CMSPUC1().trim()%>" readonly>              
	               <input type="text" name="E01CMSPNOV" size="50" maxlength="80" value="<%= rtBasic.getE01CMSPNOV().trim()%>" readonly>              
	            </td>
	          </tr>



            <%}%>
  		</table>
       </td>
    </tr>
  </table>
  <H4>Cambio de Tarifa</H4>
  <table  class="tableinfo" width="685">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap>
		<table cellspacing="0" cellpadding="2" width="132%" border="0">
			<tr id="trdark">
				<td nowrap height="38" width="23%">
					<div align="right">Tarifa :</div>
				</td>
				<td nowrap height="38" width="77%">
				<input type="text" name="E01ACMACLN" size="3" maxlength="2" value="<%= rtBasic.getE01ACMACLN().trim()%>" <% out.print("readonly");%>>
								
				<%if(rtBasic.getE01ACMATY().trim().equals("CVIS")){%>
					<a href="javascript:GetRetCod('E01ACMACLN',document.forms[0].E01ACMATY.value,'','<%= rtBasic.getE01ACMPRO().trim()%>')">
				<% }else{%>
					<a href="javascript:GetRetCod('E01ACMACLN',document.forms[0].E01ACMATY.value)"/>				
				<% }%> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="middle" border="0"></a> 
					<img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0"></td>
			</tr>
		</table>
		<div align="center"> 
				<input id="EIBSBTN" type="button" name="Submit" value="Enviar" onClick="document.forms[0].submit();">
     	</div>	
</table>
</form>
</body>
</html>
