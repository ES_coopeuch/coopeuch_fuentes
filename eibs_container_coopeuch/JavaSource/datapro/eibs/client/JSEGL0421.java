package datapro.eibs.client;

/**
 * Insert the type's description here.
 * Creation date: (1/19/00 6:08:55 PM)
 * @author: Orestes Garcia
 */
import java.io.*;
import java.math.BigDecimal;
import java.util.Iterator;
import javax.servlet.*;
import javax.servlet.http.*;
import datapro.eibs.beans.*;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.Util;
 

public class JSEGL0421 extends JSEIBSServlet {

	// GL Statement options
	protected static final int R_LIST 				= 1;
	protected static final int R_PRINT 				= 3;

	// entering options
	protected static final int R_SELECTION			= 100;
	protected static final int A_SELECTION 			= 200;
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException, IOException {
		
		switch (screen) {
		case R_LIST :
			procReqList(user, req, res, session);
			break;
		case R_PRINT :
			procReqPrintList(user, req, res, session);
			break;
		// Entering Options
		case R_SELECTION :
			procReqSelection(user, req, res, session);
			break;
		case A_SELECTION :
			procActionSelection(user, req, res, session);
			break;
		default :
			forward(devPage, req, res);
			break;
		}
	}

	private void procActionSelection(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		try {
			userPO.setBank(req.getParameter("E01TRABNK").trim());
		} catch (Exception e) {
		}
		try {
			userPO.setBranch(req.getParameter("E01TRABRN").trim());
		} catch (Exception e) {
		}
		try {
			userPO.setCurrency(req.getParameter("E01TRACCY").toUpperCase());
		} catch (Exception e) {
		}
		try {
			userPO.setAccNum(req.getParameter("E01TRAGLN").trim());
		} catch (Exception e) {
		}
		try {
			userPO.setHeader7(req.getParameter("E01HISCYC").trim());
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			userPO.setHeader8(req.getParameter("E01VALBTH").trim());
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			userPO.setHeader9(req.getParameter("E01FRDTE1").trim());
		 	userPO.setHeader10(req.getParameter("E01FRDTE2").trim());
		 	userPO.setHeader11(req.getParameter("E01FRDTE3").trim());
		} catch (Exception e){
		}
		try {
		 	userPO.setHeader12(req.getParameter("E01TODTE1").trim());
		 	userPO.setHeader13(req.getParameter("E01TODTE2").trim());
		 	userPO.setHeader14(req.getParameter("E01TODTE3").trim());
		} catch (Exception e){
		}
		try {
		 	userPO.setHeader15(req.getParameter("E01FRREFN").trim());
		} catch (Exception e){
		}
		try {
		 	userPO.setHeader16(req.getParameter("E01TOREFN").trim());
		} catch (Exception e){
		}
		try {
		 	userPO.setHeader17(req.getParameter("E01FRAMNT").trim());
		} catch (Exception e){
		}
		try {
		 	userPO.setHeader18(req.getParameter("E01TOAMNT").trim());
		} catch (Exception e){
		}

		session.setAttribute("userPO", userPO);
		
		procReqList(user, req, res, session);
	}

	private void procReqSelection(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = new UserPos();
		userPO.setOption("GL");
		userPO.setPurpose("STATEMENT");
		session.setAttribute("userPO", userPO);
		
		session.setAttribute("error", new ELEERRMessage());

		forward("EGL0420_st_selection.jsp", req, res);
	}

	private void procReqPrintList(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		String pageName = "";
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EGL0420", req);
			EGL042001Message msgSearch = (EGL042001Message) mp.getMessageRecord("EGL042001");
			msgSearch.setH01USERID(user.getH01USR());
		 	msgSearch.setH01PROGRM("EGL0420");
		 	msgSearch.setH01TIMSYS(getTimeStamp());
		 	msgSearch.setH01SCRCOD("01");
		 	msgSearch.setH01OPECOD("0004");
		 	msgSearch.setH01FLGWK1("P");
			try {
			 	msgSearch.setE01NUMREC(req.getParameter("Pos").trim());
			} catch (Exception e){
			}
		 	msgSearch.setE01TRABNK(userPO.getBank());
		 	msgSearch.setE01TRABRN(userPO.getBranch());
		 	msgSearch.setE01TRACCY(userPO.getCurrency());
		 	msgSearch.setE01TRAGLN(userPO.getAccNum());
			msgSearch.setE01HISCYC(userPO.getHeader7());
			msgSearch.setE01VALBTH(userPO.getHeader8());
			msgSearch.setE01FRDTE1(userPO.getHeader9());
			msgSearch.setE01FRDTE2(userPO.getHeader10());
			msgSearch.setE01FRDTE3(userPO.getHeader11());
		 	msgSearch.setE01TODTE1(userPO.getHeader12());
		 	msgSearch.setE01TODTE2(userPO.getHeader13());
		 	msgSearch.setE01TODTE3(userPO.getHeader14());
		 	msgSearch.setE01FRREFN(userPO.getHeader15());
		 	msgSearch.setE01TOREFN(userPO.getHeader16());
		 	msgSearch.setE01FRAMNT(userPO.getHeader17());
		 	msgSearch.setE01TOAMNT(userPO.getHeader18());

			mp.sendMessage(msgSearch);
			JBObjList list = mp.receiveMessageRecordList("E01INDOPE");

			if (mp.hasError(list)) {
				session.setAttribute("error", mp.getError(list));
				pageName = "EGL0420_st_selection.jsp";
			} else {
				JBList beanList = new JBList();
				StringBuffer myRow = null;
				boolean firstTime = true;
				String strDebit = "";
				String strCredit = "";
				BigDecimal debit = new BigDecimal("0.00");
				BigDecimal credit = new BigDecimal("0.00");
				int countDebit = 0;
				int countCredit = 0;
				
				EGL042001Message msgList = null;
				Iterator iterator = list.iterator();
				while (iterator.hasNext()) {
					msgList = (EGL042001Message) iterator.next();
					if (firstTime) {
						firstTime = false;
						userPO.setHeader5(msgList.getE01BEGBAL());
					}
					strDebit = "&nbsp;";
					strCredit = "&nbsp;";
					if(msgList.getE01TRADCC().equals("0")){
						debit = debit.add(msgList.getBigDecimalE01TRAAMT());
						countDebit ++;
						strDebit = Util.fcolorCCY(msgList.getE01TRAAMT());
					}
					else if(msgList.getE01TRADCC().equals("5")){
						credit = credit.add(msgList.getBigDecimalE01TRAAMT());
						countCredit ++;
						strCredit = Util.fcolorCCY(msgList.getE01TRAAMT());
					}

					myRow = new StringBuffer("<TR>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\">" + Util.formatDate(msgList.getE01DATE11(), msgList.getE01DATE12() , msgList.getE01DATE13()) + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\">" + Util.formatCell(msgList.getE01TRAACC()) + "</TD>");
					if(msgList.getE01NUMNAR().equals("0")){
					   myRow.append("<TD NOWRAP>" + Util.formatCell(msgList.getE01TRANAR()) + "</TD>");}
					else {
					    if(msgList.getE01NUMNAR().trim().equals("1")) {
					       myRow.append("<TD NOWRAP>" + Util.formatCell(msgList.getE01TRANAR()) + "<BR>"+ Util.formatCell(msgList.getE01TRANA1()) + "</TD>");
					       }				   		
					    else if(msgList.getE01NUMNAR().trim().equals("2")) {
						     myRow.append("<TD NOWRAP>" + Util.formatCell(msgList.getE01TRANAR()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA1()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA2()) + "</TD>");
						 	}
						else if(msgList.getE01NUMNAR().trim().equals("3")) {
						     myRow.append("<TD NOWRAP>" + Util.formatCell(msgList.getE01TRANAR()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA1()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA2()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA3()) + "</TD>");
						 	}
						else if(msgList.getE01NUMNAR().trim().equals("4")) {
						     myRow.append("<TD NOWRAP>" + Util.formatCell(msgList.getE01TRANAR()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA1()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA2()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA3()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA4()) + "</TD>");
						 	}
						else if(msgList.getE01NUMNAR().trim().equals("5")) {
						     myRow.append("<TD NOWRAP>" + Util.formatCell(msgList.getE01TRANAR()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA1()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA2()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA3()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA4()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA5()) + "</TD>");
						 	}
						else if(msgList.getE01NUMNAR().trim().equals("6")) {
						     myRow.append("<TD NOWRAP>" + Util.formatCell(msgList.getE01TRANAR()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA1()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA2()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA3()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA4()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA5()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA6()) + "</TD>");
						 	}
						else if(msgList.getE01NUMNAR().trim().equals("7")) {
						     myRow.append("<TD NOWRAP>" + Util.formatCell(msgList.getE01TRANAR()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA1()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA2()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA3()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA4()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA5()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA6()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA7()) + "</TD>");
						 	}
						else if(msgList.getE01NUMNAR().trim().equals("8")) {
						     myRow.append("<TD NOWRAP>" + Util.formatCell(msgList.getE01TRANAR()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA1()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA2()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA3()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA4()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA5()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA6()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA7()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA8()) + "</TD>");
						 	}
						else if(msgList.getE01NUMNAR().trim().equals("9")) {
						     myRow.append("<TD NOWRAP>" + Util.formatCell(msgList.getE01TRANAR()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA1()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA2()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA3()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA4()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA5()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA6()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA7()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA8()) + "<BR>"
							 + Util.formatCell(msgList.getE01TRANA9()) + "</TD>");
						 	}
					}

					myRow.append("<TD NOWRAP ALIGN=RIGHT>" + strDebit + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>" + strCredit + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>" + Util.fcolorCCY(msgList.getE01ENDBAL()) + "</TD>");
					myRow.append("</TR>");
					beanList.addRow("", myRow.toString());
				}
				beanList.setShowNext(list.getShowNext());
				beanList.setShowPrev(list.getShowPrev());
				beanList.setFirstRec(list.getFirstRec());
				
				userPO.setHeader19(Util.fcolorCCY(debit.toString()));
				userPO.setHeader20(Util.fcolorCCY(credit.toString()));
				userPO.setHeader21(msgList.getE01ENDBAL());
				userPO.setHeader22(countDebit +"");
				userPO.setHeader23(countCredit + "");
				
				session.setAttribute("glList", beanList);
				
				if (msgList.getE01VALBTH().equals("V")) {
					pageName = "EGL0420_st_list_print_fv.jsp";
				} else {
					pageName = "EGL0420_st_list_print_fp.jsp";
				}
			}

			session.setAttribute("userPO", userPO);

			forward(pageName, req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
		
	}

	private void procReqList(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		DataNavTotals dataDC = null;
		
		try {
			int idx = Integer.parseInt(req.getParameter("FlagMov"));
			dataDC = (DataNavTotals) session.getAttribute("dataBTH");
			if (idx == 0) dataDC.setIndex(dataDC.getIndex() - 1);
		} catch (Exception e) {
			dataDC = new DataNavTotals();
		}
		
		String pageName = "";
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EGL0421", req);
			EGL042101Message msgSearch = (EGL042101Message) mp.getMessageRecord("EGL042101");
			msgSearch.setH01USERID(user.getH01USR());
		 	msgSearch.setH01PROGRM("EGL0421");
		 	msgSearch.setH01TIMSYS(getTimeStamp());
		 	msgSearch.setH01SCRCOD("01");
		 	msgSearch.setH01OPECOD("0004");
		


 		 	int startPos = 0;
 			try {
 				msgSearch.setE01NUMREC(req.getParameter("Pos"));
 				startPos = 	Integer.parseInt(req.getParameter("Pos"));
 			} catch (Exception e) {
 			}
			
			
			try {
			 	msgSearch.setE01SELBTH(req.getParameter("BTH"));
			} catch (Exception e){
			}
			try {
				msgSearch.setE01SELDT1(req.getParameter("DT1"));
			 	msgSearch.setE01SELDT2(req.getParameter("DT2"));
			 	msgSearch.setE01SELDT3(req.getParameter("DT3"));
			} catch (Exception e){
			}
			try {
				msgSearch.setE01SELACR(req.getParameter("ACR"));
			} catch (Exception e){
			}

			mp.sendMessage(msgSearch);
			JBObjList list = mp.receiveMessageRecordList("E01INDOPE", "E01NUMREC");

			if (mp.hasError(list)) {
				session.setAttribute("error", mp.getError(list));
				pageName = "EGL0421_st_selection.jsp";
			} else {
				JBList beanList = new JBList();
				StringBuffer myRow = null;

				String strDebit = "";
				String strCredit = "";
				BigDecimal debit = new BigDecimal("0.00");
				BigDecimal credit = new BigDecimal("0.00");
				int countTrans = 0;
				
				Iterator iterator = list.iterator();
				while (iterator.hasNext()) {
					EGL042101Message msgList = (EGL042101Message) iterator.next();
					
					strCredit = "&nbsp;";
					strDebit = "&nbsp;";
					
					if(msgList.getE01TRADCC().equals("0")){
						debit = debit.add(msgList.getBigDecimalE01TRAAMT());
						strDebit = Util.fcolorCCY(msgList.getE01TRAAMT());
					}
					else if(msgList.getE01TRADCC().equals("5")){
						credit = credit.add(msgList.getBigDecimalE01TRAAMT());
						strCredit = Util.fcolorCCY(msgList.getE01TRAAMT());
					}

					myRow = new StringBuffer("<TR>");
					myRow.append("<TD NOWRAP ALIGN=CENTER>" + Util.formatCell(msgList.getE01TRAOBK()) + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=CENTER>" + Util.formatCell(msgList.getE01TRABRN()) + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=CENTER>" + Util.formatCell(msgList.getE01TRACCY()) + "</TD>");						
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\">" + Util.formatCell(msgList.getE01TRAGLN()) + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\">" + Util.formatCell(msgList.getE01TRAACC()) + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\">" + Util.formatCell(msgList.getE01TRACCN()) + "</TD>");	
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\">" + Util.formatCell(msgList.getE01TRACDE()) + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=\"LEFT\">" + Util.formatCell(msgList.getE01TRANAR()) + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>" + strDebit + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>" + strCredit + "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>" + Util.formatDate(msgList.getE01TRAVD1(),msgList.getE01TRAVD2(),msgList.getE01TRAVD3()) + "</TD>");
					myRow.append("</TR>");
					beanList.addRow("", myRow.toString());
					countTrans++;				
				}
				beanList.setShowNext(list.getShowNext());
				beanList.setShowPrev(list.getShowPrev());
				beanList.setFirstRec(list.getFirstRec());
				beanList.setLastRec(list.getLastRec());
				
				

				if (countTrans > 0) {
					if (dataDC.getMaxRow() == 0){
						dataDC.addRow(""+debit,""+credit,""+countTrans);
				    } else if (req.getParameter("FlagMov").equals("1")) {
				 	 	dataDC.setIndex(dataDC.getIndex() +1); 
				    	if (dataDC.getIndex() >= dataDC.getMaxRow()) dataDC.addRow(""+debit,""+credit,""+countTrans);
				    }
			    } 
				
				userPO.setHeader19(Util.fcolorCCY(debit.toString()));
				userPO.setHeader20(Util.fcolorCCY(credit.toString()));
				
				session.setAttribute("bthList", beanList);
				session.setAttribute("dataBTH", dataDC);
				
				String params = "?Batch="+req.getParameter("BTH")+"&Date1="+req.getParameter("DT1")+"&Date2="+req.getParameter("DT2")+"&Date3="+req.getParameter("DT3")+"&Ref="+req.getParameter("ACR");
				
				pageName = "EGL0421_batch_detail.jsp" + params;
			}

			session.setAttribute("userPO", userPO);

			forward(pageName, req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
		
	}

}