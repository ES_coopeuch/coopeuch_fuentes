package datapro.eibs.products;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDET05101Message;
import datapro.eibs.beans.EDL006001Message;
import datapro.eibs.beans.EDL006501Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ETG000000Message;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.sockets.MessageRecord;

public class JSEDL0065 extends JSEIBSServlet {

	protected static final int R_LINEAS_ESTATALES_PASSWORD = 100;
	protected static final int R_LINEAS_ESTATALES_LIST_APPROVAL = 200;
	protected static final int R_APPROVAL_LINEAS_ESTATALES = 300;

		protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_LINEAS_ESTATALES_PASSWORD :
				procReqPassword(req, res, session);
				break;
			case R_LINEAS_ESTATALES_LIST_APPROVAL :
				procReqLinEstAppList(user, req, res, session);
				break;
			case R_APPROVAL_LINEAS_ESTATALES :
				procAprMainLinEst(user, req, res, session);
				break;
			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

		
	protected void procReqPassword(HttpServletRequest req, HttpServletResponse res, HttpSession ses)
	throws ServletException, IOException {

		UserPos	userPO = null;

		try {
			int screen = R_LINEAS_ESTATALES_LIST_APPROVAL ;

			userPO = (datapro.eibs.beans.UserPos)ses.getAttribute("userPO");
			userPO.setRedirect("/servlet/datapro.eibs.products.JSEDL0065?SCREEN=" + screen );
			ses.setAttribute("userPO", userPO);
			res.sendRedirect(super.srctx + "/servlet/datapro.eibs.menu.JSESS0030?SCREEN=7");
			}
		catch (Exception e)	
		{
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}	

	}
		
	
	
	private void procReqLinEstAppList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDL0065", req);
			EDL006501Message msgList = (EDL006501Message) mp.getMessageRecord("EDL006501", user.getH01USR(), "0001");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("LN");
			
			mp.sendMessage(msgList);
			
		 	ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			
	 		if (mp.hasError(msgError)) {
	 			flexLog("Putting java beans into the session");
	 			session.setAttribute("error", msgError);
 			session.setAttribute("userPO", userPO);
	 			forward("error_viewer.jsp", req, res);
	 		
	 		} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("listAppr", list);
				session.setAttribute("userPO", userPO);
				
				forward("EDL0065_lineas_estatales_approval_list.jsp", req, res);
	 		}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

	private void procAprMainLinEst(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //Approval
				userPO.setPurpose("APP");
				session.setAttribute("userPO", userPO);
				procReqApp(user, req, res, session);
				break;
			case 2 : //Rechazo
				userPO.setPurpose("REC");
				session.setAttribute("userPO", userPO);
				procReqApp(user, req, res, session);
				break;
			case 3 : //Consulta
				userPO.setPurpose("");
				session.setAttribute("userPO", userPO);
				procReqInq(user, req, res, session);
				break;
				
			default : 
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

	private void procReqApp(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		BigDecimal pos;
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		EDL006501Message msg = null;
	
		try 
		{
			JBObjList bl = (JBObjList) session.getAttribute("listAppr");
			int idx = 0;
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
			bl.setCurrentRow(idx);
			msg = (EDL006501Message) bl.getRecord();

			//Lectura de descripción de Cabeceras.	

			mp = getMessageProcessor("EDL0065", req);
			msg.setH01USERID(user.getH01USR());
			msg.setH01PROGRM("EDL0065");
			msg.setH01TIMSYS(getTimeStamp());
			if(userPO.getPurpose().equals("APP"))
				msg.setH01OPECOD("0003");
			else 
				msg.setH01OPECOD("0005");
				
	
			if(msg.getE01MLNDSC().equals("LINEA"))
				msg.setH01SCRCOD("LN");
			else if (msg.getE01MLNDSC().equals("COMUNAS"))
					msg.setH01SCRCOD("CM");
				  else
					msg.setH01SCRCOD("SL");
			
			mp.sendMessage(msg);
	
			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EDL006501Message) mp.receiveMessageRecord("EDL006501");

			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);

			if (mp.hasError(msgError)) 
				forward("EDL0065_lineas_estatales_approval_list.jsp", req, res);
			else 
				procReqLinEstAppList(user, req, res, session);

		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

	private void procReqInq(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		EDL006501Message msg = null;
	
		try 
		{
			JBObjList bl = (JBObjList) session.getAttribute("listAppr");
			int idx = 0;
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
			bl.setCurrentRow(idx);
			msg = (EDL006501Message) bl.getRecord();
			
			if (msg.getE01MLNDSC().equals("COMUNAS"))
			{
				mp = getMessageProcessor("EDL0060", req);
				EDL006001Message msgList = (EDL006001Message) mp.getMessageRecord("EDL006001", user.getH01USR(), "0006");
				msgList.setH01TIMSYS(getTimeStamp());
				msgList.setE01MLNSEQ(msg.getE01MLNSEQ());
				msgList.setE01MLNIND(msg.getE01MLNIND());
				msgList.setH01SCRCOD("CM");
				
				mp.sendMessage(msgList);
				
				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");

				
				session.setAttribute("msgLinEst", msg);
				session.setAttribute("listComunas", list);
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);

				if (!mp.hasError(msgError)) 
					forward("EDL0065_comunas_lineas_estatales_inq.jsp", req, res);
			}	
			else 
			{	
				if(msg.getE01MLNDSC().equals("LINEA"))
				{
					session.setAttribute("msgLinEst", msg);
					session.setAttribute("error", msgError);
					session.setAttribute("userPO", userPO);

					forward("EDL0065_lineas_estatales_inq.jsp", req, res);

				}
				else 
				{
					session.setAttribute("msgSubLinEst", msg);
					session.setAttribute("error", msgError);
					session.setAttribute("userPO", userPO);

					forward("EDL0065_sub_lineas_estatales_inq.jsp", req, res);

				}
			}	
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

//	
}
