<html>
<head>
<title>Tabla de Cargos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="lnServCharge" class="datapro.eibs.beans.ESD0713DSMessage"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

<%
if ( userPO.getHeader23().equals("G") ||  userPO.getHeader23().equals("V")){
%>
	builtNewMenu(ln_i_1_opt);
<%   
}
else  {
%>
	builtNewMenu(ln_i_2_opt);
<%   
}
%>

</SCRIPT>

</head>

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
   if (userPO.getPurpose().equals("INQUIRY")){ 
   out.println("<SCRIPT> initMenu(); </SCRIPT>");}
%> 


<h3 align="center">Consulta de Tabla de Cargos de Pr&eacute;stamos
	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="ln_inq_ren_rate, EDL0160"></h3>
<hr size="4">
<form >
  <p></p>
  
  <table class="tableinfo">
    <tr> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="20%" align=right> 
              <b>Banco :</b>
            </td>
            <td nowrap > 
               
              <input type="text" name="E01SELBNK" size="4" maxlength="2" value="<%= lnServCharge.getE01SELBNK()%>" readonly>
            </td>
            <td nowrap width="20%" align=right> 
              <b>Producto :</b>
            </td>
            <td nowrap > 
               <input type="text" name="E01SELTYP" size="5" maxlength="4" value="<%= lnServCharge.getE01SELTYP()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="20%" align=right> 
              <b>Tabla :</b>
            </td>
            <td nowrap colspan=3>  
               <input type="text" name="E01SELTBL" size="4" maxlength="2" value="<%= lnServCharge.getE01SELTLN()%>" readonly>
               <input type="text" name="E01DLSDSC" size="48" maxlength="45" value="<%= lnServCharge.getE01DLSDSC()%>">
            </td>
          </tr>
		  <%if(!lnServCharge.getE01DLSCUN().equals("0")){%>
          <tr id="trdark"> 
            <td nowrap width="20%" align=right> 
              <b>Cliente :</b>
            </td>
            <td nowrap colspan=3>  
               <input type="text" name="E01SELCUN" size="10" maxlength="9" value="<%= lnServCharge.getE01DLSCUN()%>" readonly>
               <input type="text" name="E01DSCCUN" size="48" maxlength="45" value="<%= lnServCharge.getE01CUSNA1()%>" readonly>
            </td>
          </tr>
          <%}%>
        </table>
      </td>
    </tr>
  </table>
  
  <H4>Datos B&aacute;sicos</H4>
  <table  class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark">
            <td nowrap> 
            </td>
            <td nowrap> 
            </td>
            <td nowrap> 
            </td>
            <td nowrap> 
            </td>
            <td nowrap colspan=3> 
              Factor	        
            </td>
          </tr>
          <tr id="trdark">
            <td nowrap> 
              <div align="right">Periodo Base (360/65/66) :</div>
            </td>
            <td nowrap> 
              <input type="text" name="E01DLSDAB" size="4" maxlength="3" value="<%= lnServCharge.getE01DLSDAB()%>" readonly>	        
            </td>
            <td nowrap> 
              <div align="right">Tasa Base o Puntos :</div>
            </td>
            <td nowrap> 
              <input type="text" name="E01DLSRTF" size="10" maxlength="10"	value="<%= lnServCharge.getE01DLSRTF()%>" readonly>	        
            </td>
            <td nowrap colspan=3> 
              <input type="text" name="E01DLSRFT" size="2" maxlength="1"	value="<%= lnServCharge.getE01DLSRFT()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Tasa Minima Permitida :</div>
            </td>
            <td nowrap> 
 	          <input type="text" name="E01DLSMIN" size="10" maxlength="10"	value="<%= lnServCharge.getE01DLSMIN()%>" readonly>
 	        </td>
 	        <td nowrap> 
              <div align="right">Tasa Maxima Permitida :</div>
            </td>
            <td nowrap> 
 	          <input type="text" name="E01DLSMAX" size="10" maxlength="10"	value="<%= lnServCharge.getE01DLSMAX()%>" readonly>
 	        </td>
 	        <td nowrap colspan=3> 
              <input type="text" name="E01DLSMMT" size="2" maxlength="1"	value="<%= lnServCharge.getE01DLSMMT()%>" readonly>
            </td>
          </tr>
          <tr id="trdark">
            <td nowrap>
              <div align="right">Tipo de Interes :</div>
            </td>
            <td nowrap>
            	<input type="text" name="E01DLSICT" size="2" maxlength="1"	value="<%= lnServCharge.getE01DLSICT()%>" readonly>
            </td>
            <td nowrap> 
              <div align="right">Cargo o Tasa x Mora :</div>
            </td>
            <td nowrap> 
 	          <input type="text" name="E01DLSIR1" size="10" maxlength="10"	value="<%= lnServCharge.getE01DLSIR1()%>" readonly>
 	        </td>
            <td nowrap colspan=3>        
              <input type="text" name="E01DLSFL2" size="2" maxlength="1" value="<%= lnServCharge.getE01DLSFL2()%>" readonly>
            </td>
          </tr>
			<tr id="trclear">
            <td nowrap>
              <div align="right">Permite Pagos Parciales :</div>
            </td>
            <td nowrap>
              <input type="text" name="E01DLSPPR" size="2" maxlength="1"	value="<%= lnServCharge.getE01DLSPPR()%>" readonly>
            </td>
            <td nowrap> 
              <div align="right">Cargo M�nimo de Mora :</div>
            </td>
            <td nowrap colspan=4> 
 	          <input type="text" name="E01DLSMPA" size="15" maxlength="15"	value="<%= lnServCharge.getE01DLSMPA()%>" readonly>
 	        </td>            
          </tr>
          <tr id="trdark">
            <td nowrap>
              <div align="right">Control. Adelanto Capital :</div>
            </td>
			<td nowrap>
  				<input type="radio" name="E01DLSFL1" value="Y" <%if(lnServCharge.getE01DLSFL1().equals("Y")) out.print("checked");%> disabled>Si
  				<input type="radio" name="E01DLSFL1" value="N" <%if(lnServCharge.getE01DLSFL1().equals("N")) out.print("checked");%> disabled>No
			</td>
			<td nowrap>
              <div align="right">Gracia Mora :</div>
            </td>
			<td nowrap colspan=4>
  				1- <input type="text" name="E01DLSGR1" size="3" maxlength="2" value="<%=lnServCharge.getE01DLSGR1()%>" readonly> 2-
  			<INPUT type="text" name="E01DLSGR2" size="3" maxlength="2"
					value="<%=lnServCharge.getE01DLSGR2()%>" readonly> 3- <INPUT type="text"
					name="E01DLSGR3" size="3" maxlength="2"
					value="<%=lnServCharge.getE01DLSGR3()%>" readonly></td>
		  </tr>
		  <tr id="trclear">
			<td nowrap>
              <div align="right">Vencimiento Feriados :</div>
            </td>
			<td nowrap>
  				<input type="text" name="E01DLSFCO" size="2" maxlength="1" value="<%=lnServCharge.getE01DLSFCO()%>" readonly>
  			<td nowrap>
              <div align="right">Dias de Paso a Vencido :</div>
            </td>
			<td nowrap colspan=4>
  				<input type="text" name="E01DLSPMD" size="4" maxlength="4" value="<%=lnServCharge.getE01DLSPMD()%>" readonly>
  			</td>
          </tr>
          <tr id="trdark">
			<td nowrap>
              <div align="right">Forma Cambio Contable :</div>
            </td>
			<td nowrap>
  				<input type="text" name="E01DLSTCG" size="2" maxlength="1" value="<%=lnServCharge.getE01DLSTCG()%>" readonly>
  			<td nowrap>
              <div align="right">Paso Castigo sin Garantia :</div>
            </td>
			<td nowrap colspan=4>
  				<input type="text" name="E01DLSPC1" size="4" maxlength="4" value="<%=lnServCharge.getE01DLSPC1()%>" readonly>
  			</td>
          </tr>
          <tr id="trclear">
			<td nowrap>
              <div align="right">Moneda Cobro Cargos :</div>
            </td>
			<td nowrap>
  				<input type="text" name="E01DLSCCR" size="4" maxlength="3" value="<%=lnServCharge.getE01DLSCCR()%>" readonly>
            </td>
  			<td nowrap>
              <div align="right">Paso Castigo con Garantia :</div>
            </td>
			<td nowrap colspan=4>
  				<input type="text" name="E01DLSPC2" size="4" maxlength="4" value="<%=lnServCharge.getE01DLSPC2()%>" readonly>
  			</td>
          </tr>
          <tr id="trdark">
			<td nowrap> 
              <div align="right">Fecha Validez :</div>
              
            </td>
			<td nowrap> 
              	<input type="text" name="E01DLSMAM" size="3" maxlength="2" value="<%=lnServCharge.getE01DLSMAM()%>" readonly>
  				<input type="text" name="E01DLSMAD" size="3" maxlength="2" value="<%=lnServCharge.getE01DLSMAD()%>" readonly>
  				<input type="text" name="E01DLSMAY" size="5" maxlength="4" value="<%=lnServCharge.getE01DLSMAY()%>" readonly>
            </td>
			<td nowrap>
              <div align="right">Retiro Giro Custodia. :</div>
            </td>
			<td nowrap colspan=4>
  				<input type="text" name="E01DLSCUP" size="3" maxlength="2" value="<%=lnServCharge.getE01DLSCUP()%>" readonly>
  			</td>
          </tr>
         <tr id="trclear">
			<td nowrap colspan=9>
			   <table cellspacing="0" cellpadding="2" width="100%" border="0">
		          <tr id="trclear">
		            <td nowrap> 
		            	<div align="right">Dias Cambio Contable :</div>
		            </td>
		            <td nowrap> 
		                (1)<input type="text" name="E01DLSPL1" size="4" maxlength="4" value="<%=lnServCharge.getE01DLSPL1()%>" readonly>
		            </td>
		            <td nowrap> 
		            	(2)<input type="text" name="E01DLSPL2" size="4" maxlength="4" value="<%=lnServCharge.getE01DLSPL2()%>" readonly>
		            </td>
		            <td nowrap> 
		            	(3)<input type="text" name="E01DLSPL3" size="4" maxlength="4" value="<%=lnServCharge.getE01DLSPL3()%>" readonly>
		            </td>
		            <td nowrap> 
		              	(4)<input type="text" name="E01DLSPL4" size="4" maxlength="4" value="<%=lnServCharge.getE01DLSPL4()%>" readonly>
		            </td>
		            <td nowrap> 
		              	(5)<input type="text" name="E01DLSPL5" size="4" maxlength="4" value="<%=lnServCharge.getE01DLSPL5()%>" readonly>
		            </td>
		          </tr>
		          <tr id="trdark">
		            <td nowrap> 
		            	<div align="right">% Capital Vencido :</div>
		            </td>
		            <td nowrap> 
		                <input type="text" name="E01DLSPR1" size="10" maxlength="10" value="<%=lnServCharge.getE01DLSPR1()%>" readonly>
		            </td>
		            <td nowrap> 
		            	<input type="text" name="E01DLSPR2" size="10" maxlength="10" value="<%=lnServCharge.getE01DLSPR2()%>" readonly>
		            </td>
		            <td nowrap> 
		            	<input type="text" name="E01DLSPR3" size="10" maxlength="10" value="<%=lnServCharge.getE01DLSPR3()%>" readonly>
		            </td>
		            <td nowrap> 
		              	<input type="text" name="E01DLSPR4" size="10" maxlength="10" value="<%=lnServCharge.getE01DLSPR4()%>" readonly>
		            </td>
		            <td nowrap> 
		              	<input type="text" name="E01DLSPR5" size="10" maxlength="10" value="<%=lnServCharge.getE01DLSPR5()%>" readonly>
		            </td>
		          </tr>
			   </table>
            </td>			
          </tr>
         </table>
       </td>
    </tr>
  </table>
  
<h4>Prioridad de Pagos </h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="14%"  > 
              <div align="center"> 
                <input type="text" name="E01DLSPP1" size="2" maxlength="1" value="<%= lnServCharge.getE01DLSPP1().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="14%"  > 
              <div align="center"> 
                <input type="text" name="E01DLSPP2" size="2" maxlength="1" value="<%= lnServCharge.getE01DLSPP2().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="14%" > 
              <div align="center"> 
                <input type="text" name="E01DLSPP3" size="2" maxlength="1" value="<%= lnServCharge.getE01DLSPP3().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="14%"  > 
              <div align="center"> 
                <input type="text" name="E01DLSPP4" size="2" maxlength="1" value="<%= lnServCharge.getE01DLSPP4().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="14%" > 
              <div align="center"> 
                <input type="text" name="E01DLSPP5" size="2" maxlength="1" value="<%= lnServCharge.getE01DLSPP5().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="14%"  > 
              <div align="center"> 
                <input type="text" name="E01DLSPP6" size="2" maxlength="1" value="<%= lnServCharge.getE01DLSPP6().trim()%>" readonly>
              </div>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="14%" > 
              <div align="center">Principal</div>
            </td>
            <td nowrap width="14%" > 
              <div align="center">Intereses</div>
            </td>
            <td nowrap width="14%" > 
              <div align="center">Mora</div>
            </td>
            <td nowrap width="14%" > 
              <div align="center">Comisiones</div>
            </td>
            <td nowrap width="14%" > 
              <div align="center">Impuestos</div>
            </td>
            <td nowrap width="14%" > 
              <div align="center">Deducciones</div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <H4>Comisiones o Impuestos</H4>
  <table  class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <th nowrap >Descripcion</th>
            <th nowrap >Fac</th>
            <th nowrap >Monto</th>
            <th nowrap >Fre</th>
            <th nowrap >M�nimo</th>
            <th nowrap >M�ximo</th>
            <th nowrap >Mda</th>
            <th nowrap >Cta/Contable</th>
            <th nowrap >Iva</th>
          </tr>
          <%
  				   int amount = 9;
 				   String name; 				   
  					for ( int i=1; i<=amount; i++ ) {
   					 name = i + "";
   			%> 
          <tr id="trclear"> 
            <td nowrap > 
              <div align="center" > 
                <input type="text" name="E01DLSDE<%= name %>" value="<%= lnServCharge.getField("E01DLSDE"+name).getString() %>" size="30" maxlength="30" readonly>
              </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E01DLSFA<%= name %>" size="2" maxlength="1" 
				 value="<%= lnServCharge.getField("E01DLSFA"+name).getString()%>" readonly>
              </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E01DLSAM<%= name %>" size="15" maxlength="15" value="<%= lnServCharge.getField("E01DLSAM"+name).getString()%>" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01DLSAP<%= name %>" size="2" maxlength="1" 
				value="<%= lnServCharge.getField("E01DLSAP"+name).getString()%>" readonly>
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01DLSMN<%= name %>" size="15" maxlength="15" value="<%= lnServCharge.getField("E01DLSMN"+name).getString()%>" readonly>
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01DLSMX<%= name %>" size="15" maxlength="15"  value="<%= lnServCharge.getField("E01DLSMX"+name).getString()%>" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01DLSGB<%= name %>" size="3" maxlength="3" 
				value="<%=lnServCharge.getField("E01DLSGB"+name).getString()%>" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01DLSGL<%= name %>" size="18" maxlength="17"  
				value="<%= lnServCharge.getField("E01DLSGL"+name).getString()%>" readonly>
              </div>
            </td>
            
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01DLSWH<%= name %>" size="2" maxlength="1"  value="<%= lnServCharge.getField("E01DLSWH"+name).getString()%>" readonly>
              </div>
            </td>
          </tr>
          <%
    		}
    		%> 
    </table>
    </td>
    </tr>
  </table>
  <p align="center">&nbsp;</p>
  </form>
</body>
</html>
