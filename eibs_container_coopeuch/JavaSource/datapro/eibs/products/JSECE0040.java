package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Modificado por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  17/01/2013                                                                                      **/
/**  Objetivo                :  Servicio para Generacion y Emision de certificados de posision efectiva                         **/
/*********************************************************************************************************************************/

import java.io.*;
import java.net.*;
import java.beans.Beans;
import javax.servlet.*;
import javax.servlet.http.*;

import datapro.eibs.beans.*;

import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.Util;
 
import datapro.eibs.sockets.*;
import java.util.Hashtable;
import java.util.ArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.PropertyResourceBundle;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generic.beanutil.BeanList;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.sockets.DecimalField;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

import datapro.eibs.generic.SimpleAthenticator;
public class JSECE0040 extends datapro.eibs.master.SuperServlet {


	protected static final int R_ENTER_ID	 	 = 100;
	protected static final int R_LIST            = 1;
	protected static final int A_LIST            = 2;
	private String LangPath = "S";

/**
 * JSECLI001 constructor comment.
 */
public JSECE0040() {
	super();
}
/**
 * 
 */
public void destroy() {

	flexLog("free resources used by JSECE0000");
	
}
/**
 * 
 */
public void init(ServletConfig config) throws ServletException {
	super.init(config);
}

/**
 * This method was created in VisualAge.
 */
protected void procReqEnterID(
		ESS0030DSMessage user, 
		HttpServletRequest req, 
		HttpServletResponse res, 
		HttpSession ses)
		throws ServletException, IOException {

	ESD008001Message msgClient = null;
	ELEERRMessage msgError = null;
	UserPos	userPO = null;	

	try {

		msgClient = new datapro.eibs.beans.ESD008001Message(); 
		msgError = new datapro.eibs.beans.ELEERRMessage();
		userPO = new datapro.eibs.beans.UserPos(); 
		userPO.setOption("CLIENT");
		userPO.setPurpose("SOL");
		userPO.setRedirect("/servlet/datapro.eibs.products.JSECE0040?SCREEN=1");
		ses.setAttribute("client", msgClient);
		ses.setAttribute("error", msgError);
		ses.setAttribute("userPO", userPO);

  	} catch (Exception ex) {
		flexLog("Error: " + ex); 
  	}

	try {
		flexLog("About to call Page: " + LangPath + "ESD0080_client_both_enter.jsp");
		callPage(LangPath + "ESD0080_client_both_enter.jsp", req, res);
	}
	catch (Exception e) {
		e.printStackTrace();
		flexLog("Exception calling page " + e);
	}

}

protected void procReqCerConMPRD(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECE004001Message msgList = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError =
				(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
					getClass().getClassLoader(),
					"datapro.eibs.beans.ELEERRMessage");
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		// Send Initial data
		try {
			msgList = (ECE004001Message) mc.getMessageRecord("ECE004001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ECE0040");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			msgList.setH01OPECOD("0001");
			try {
			if (msgList.getE01SELCUN().equals("0") || msgList.getE01SELCUN().equals(" "))
			{
				if (userPO.getHeader1().equals("") || userPO.getHeader1().equals("0"))
				{
					msgList.setE01SELCUN(req.getParameter("E01CUN"));
				} 
				else 
				{
					msgList.setE01SELCUN(userPO.getHeader1());
			    }
			}
			} catch (Exception e) {

			}
			flexLog("cliente ..." + msgList.getE01SELCUN());
			flexLog("Cliente para crear = " + req.getParameter("E01CUN"));
			flexLog("userPO = " + userPO.getHeader1());
			msgList.send();
			msgList.destroy();
			flexLog("ECE0040010 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError =
						(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
							getClass().getClassLoader(),
							"datapro.eibs.beans.ELEERRMessage");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else {
				flexLog("Message " + newmessage.getFormatName() + " received.");				
			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e + newmessage);
			throw new RuntimeException("Socket Communication Error Receiving");
		}

		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECE004001")) {

				JBObjList beanList = new JBObjList();

				boolean firstTime = true;
				String marker = "";
				String chk = "";
					while (true) {

					msgList = (ECE004001Message) newmessage;
					marker = msgList.getH01FLGMAS();

					if (firstTime) {
						userPO.setHeader1(msgList.getE01SELCUN());
						userPO.setHeader2(msgList.getE01CERNAM());
						userPO.setHeader3(msgList.getE01CERIDN());
						userPO.setHeader4(msgList.getE01CERISD());
						userPO.setHeader5(msgList.getE01CERISM());
						userPO.setHeader6(msgList.getE01CERISA());
						firstTime = false;
						chk = "checked";

					} else {
						chk = "";
					}

					if (marker.equals("*")) {
						beanList.setShowNext(false);
						break;
					} else {
						beanList.addRow(msgList);

						if (marker.equals("+")) {
							beanList.setShowNext(true);

							break;
						}
					}
					newmessage = mc.receiveMessage();
				}

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("ECE0040Help", beanList);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog(
							"About to call Page: "
								+ LangPath
								+ "ECE0040_cer_posesion_consulta.jsp");
						callPage(
							LangPath + "ECE0040_cer_posesion_consulta.jsp",
							req,
							res);					
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				
				} else {
					try {
						flexLog("About to call Page: " + LangPath + "ESD0080_client_both_enter.jsp");
						callPage(LangPath + "ESD0080_client_both_enter.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}				

			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Data Receiving");
		}

	}
protected void procActionPos(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		UserPos userPO = null;

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		flexLog(".....  = " + userPO.getOption()); 
		int inptOPT = 0;

		inptOPT = Integer.parseInt(req.getParameter("opt"));
        flexLog("Opcion = " + inptOPT);
		switch (inptOPT) {
			case 1 : //Imprimir Certificado
				procReqPDF(mc, user, req, res, ses);
				break;				
			default :
				res.sendRedirect(
					super.srctx
						+ "/servlet/datapro.eibs.products.JSECE0040?SCREEN=1");
				break;
		}
	}


protected void procReqPDF(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
		throws ServletException, IOException {
		DocumentException ex = null;
		ByteArrayOutputStream baosPDF = null;

		boolean ACT = false;
		try {
			baosPDF =
				generatePDFDocumentBytes(
					req,
					this.getServletContext(),
					ses,
					ACT);

				StringBuffer sbFilename = new StringBuffer();
				String fn = com.datapro.generic.tool.Util.getTimestamp().toString();
				fn = Util.replace(fn,":","-");
				fn = Util.replace(fn,".","-");
				sbFilename.append(fn);
				sbFilename.append(".pdf");

				res.setHeader("Cache-Control", "max-age=30");

				res.setContentType("application/pdf");

				StringBuffer sbContentDispValue = new StringBuffer();
				sbContentDispValue.append("inline");
				sbContentDispValue.append("; filename=");
				sbContentDispValue.append(sbFilename);

				res.setHeader(
					"Content-disposition",
					sbContentDispValue.toString());

				res.setContentLength(baosPDF.size());

				ServletOutputStream sos;

				sos = res.getOutputStream();

				baosPDF.writeTo(sos);

				sos.flush();

		} catch (DocumentException dex) {
			res.setContentType("text/html");
			PrintWriter writer = res.getWriter();
			writer.println(
				this.getClass().getName()
					+ " caught an exception: "
					+ dex.getClass().getName()
					+ "<br>");
			writer.println("<pre>");
			dex.printStackTrace(writer);
			writer.println("</pre>");
		} finally {
			if (baosPDF != null) {
				baosPDF.reset();
			}
		}

		return;

	}
protected ByteArrayOutputStream generatePDFDocumentBytes(
		final HttpServletRequest req,
		final ServletContext ctx,
		HttpSession session,
		boolean FLG)
		throws DocumentException 		{
	String TIT1     = "                                     CERTIFICADO DE POSESION EFECTIVA";
	String detail01 = "      La Cooperativa del Personal de la Universidad de Chile Ltda., Coopeuch Ltda., ";
	String detail02 = " certifica que el Sr(a) ";
	String detail03 = " Rut ";
	String detail04 = "  ha sido socio de nuestra Institucion desde el ";
	String detail05 = " y que al dia de hoy ";
	String detail06 = " registra en su cuenta";
	String detail07 = " los siguientes valores:";
	String detail08 = "      Se extiende el presente certificado a peticion de los familiares para tramitar ";
	String detail09 = "      posesion efectiva, sin ulterior responsabilidad de la Cooperativa ";
	String detail11 = " ______________________________________                ";
	String detail13 = "              COOPEUCH LTDA.                           ";
	String detail14 = " los siguientes";
	String detail15 = " valores:";
    ECE004001Message msgDoc = null;
	JBObjList bl = (JBObjList) session.getAttribute("ECE0040Help");
	int idx = 0;
	bl.setCurrentRow(idx);
	msgDoc = (ECE004001Message) bl.getRecord();
    flexLog("Mensaje rescatado de la lista " + "index  " + idx + " " + msgDoc);
    //Datos generales 
	String selcun = Util.unformatHTML(msgDoc.getE01SELCUN());       //Numero del cliente
	String selidn = Util.unformatHTML(msgDoc.getE01CERIDN()); 
	String nombre = Util.unformatHTML(msgDoc.getE01CERNAM());    //Nombre del Cliente	  
	String nomeje = Util.unformatHTML(msgDoc.getE01CERNUS());    //Nombre del Usuario
	String rut = Util.unformatHTML(msgDoc.getE01CERIDN());       //Rut del Cliente
    String mantisa= "";
    String digito="";
    String formattedRUT="";
    
	if (rut.length() > 0)
		{ 
        mantisa=rut.substring(0,rut.length()-1);
        digito=rut.substring(rut.length()-1);
        DecimalFormat nf = new DecimalFormat("##,###,###"); 
        formattedRUT=nf.format(Integer.valueOf(mantisa))+"-"+digito;
		}
	
	Document doc = new Document(PageSize.A4, 36, 36, 36, 36);           //setaer tamano de hoja
    DecimalFormat nf = new DecimalFormat("##,###,###"); 
	ByteArrayOutputStream baosPDF = new ByteArrayOutputStream();
	PdfWriter docWriter = null;

	try {
		docWriter = PdfWriter.getInstance(doc, baosPDF);

		if (FLG) {
			docWriter.setEncryption(
				PdfWriter.STRENGTH128BITS,
				selcun,
				selidn,
				PdfWriter.AllowCopy | PdfWriter.AllowPrinting);
		}

		doc.addAuthor("eIBS");
		doc.addCreationDate();
		doc.addProducer();
		doc.addCreator(msgDoc.getE01SELCUN());
		doc.addKeywords("pdf, itext, Java, open source, http");

		Font normalBoldFont = FontFactory.getFont(FontFactory.COURIER, 10, Font.BOLD);
		Font normalBoldFont7 = FontFactory.getFont(FontFactory.COURIER, 7, Font.BOLD);
		Font normalBoldFont6 = FontFactory.getFont(FontFactory.COURIER, 6, Font.BOLD);
		Font headerBoldFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD);
		Font headerBoldFont7 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);

		Paragraph BLANK = new Paragraph(" ", headerBoldFont);
		//Header principal
		Paragraph HEADER00  = new Paragraph("                             " + TIT1, headerBoldFont);
		Paragraph HEADER06B = new Paragraph("PRODUCTOS", headerBoldFont7);
		Paragraph HEADER06C = new Paragraph("MONTO EN $", headerBoldFont7);

		Paragraph DETAIL01 = new Paragraph(detail01, normalBoldFont);
		Paragraph DETAIL02 = new Paragraph(detail02 + nombre + ", ", normalBoldFont);
		Paragraph DETAIL03 = new Paragraph(detail03 + formattedRUT + ", " + detail04 + msgDoc.getE01CERISD() + " de " + msgDoc.getD01CERISM() + " de " + msgDoc.getE01CERISA()+ ",", normalBoldFont);
		Paragraph DETAIL04 = new Paragraph(detail05 + msgDoc.getE01CERCDD() + " de " + msgDoc.getD01CERCMM() + " de " + msgDoc.getE01CERCAA() + ", " + detail06 + detail14, normalBoldFont);
		Paragraph DETAIL05 = new Paragraph(detail03 + formattedRUT + ", " + detail05 + msgDoc.getE01CERCDD() + " de " + msgDoc.getD01CERCMM() + " de " + msgDoc.getE01CERCAA() + ", " + detail06, normalBoldFont);
		Paragraph DETAIL06 = new Paragraph(detail07, normalBoldFont);
		Paragraph DETAIL07 = new Paragraph(detail15, normalBoldFont);		
		
		Paragraph DETAIL08 = new Paragraph(detail08, normalBoldFont);
		Paragraph DETAIL09 = new Paragraph(detail09, normalBoldFont);
		Paragraph DETAIL11 = new Paragraph(detail11, normalBoldFont);
		Paragraph DETAIL12 = new Paragraph(msgDoc.getE01CERNUS() + "                              ", normalBoldFont);
		Paragraph DETAIL13 = new Paragraph(msgDoc.getE01CERUCI() + "                              ", normalBoldFont);
		Paragraph DETAIL14 = new Paragraph(detail13, normalBoldFont);
		Paragraph DETAIL15 = new Paragraph(msgDoc.getE01CERUCI()+", " + msgDoc.getE01CERCDD() + " de " + msgDoc.getD01CERCMM() + " de " + msgDoc.getE01CERCAA(), normalBoldFont);
		
		Image image = null;
		try {
			image = Image.getInstance(JSEIBSProp.getImgTempPath() + "/images/logo1_coopeuch.JPG");
		    //image = Image.getInstance(req.getContextPath() + "/images/logo1_coopeuch.JPG");			
			image.setAlignment(Element.ALIGN_LEFT);
			image.scalePercent(75);
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		doc.open();
		if (image != null) doc.add(image);
		Table table = new Table(1, 20);
		table.setBorderWidth(0);
		table.setCellsFitPage(true);
		table.setPadding(1);
		table.setSpacing(1);
		table.setWidth(100);
		
		Cell cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);                                          //Lineas en blanco
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
	
		cell = new Cell(HEADER00);
		cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		
		cell = new Cell(DETAIL01);
		cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		cell = new Cell(DETAIL02);
		cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
 
		if (!msgDoc.getD01CERISM().equals(""))
		{
			cell = new Cell(DETAIL03);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
	
			cell = new Cell(DETAIL04);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL07);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);			
		}
		else
		{
			cell = new Cell(DETAIL05);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);

			cell = new Cell(DETAIL06);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
		}
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		table.addCell(cell);	
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		doc.add(table);                                     //Agrego Lineas a la tabla
		//--------------------------------------------------------------------------------
		
		table = new Table(4, 4);
		table.setBorderWidth(0);
		table.setCellsFitPage(true);
		table.setPadding(1);
		table.setSpacing(1);
		table.setWidth(100);	
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		cell = new Cell(HEADER06B);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(HEADER06C);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
	
		// --------------------------------------------------------------------------------		
		table = new Table(4, 4);
		table.setBorderWidth(0);
		table.setCellsFitPage(true);
		table.setPadding(1);
		table.setSpacing(1);
		table.setWidth(100);		
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		cell = new Cell(HEADER06B);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(HEADER06C);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
	
        bl.initRow();
        long  monto = 0;        
        while (bl.getNextRow()) 
        {
            msgDoc = (datapro.eibs.beans.ECE004001Message) bl.getRecord();
            if (msgDoc.getE01CERSEC().equals("1"))
            {
           		monto = monto + Long.parseLong(msgDoc.getE01CERMTO());	              	
            }
        } 
		Paragraph DETAIL01B = new Paragraph(msgDoc.getE01CERCPA()+ " Cuotas de Participacion ", normalBoldFont6);
		Paragraph DETAIL01C = new Paragraph(nf.format(monto) + "      ", normalBoldFont6);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		cell = new Cell(DETAIL01B);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(DETAIL01C);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);

		// --------------------------------------------------------------------------------

        bl.initRow();
        monto = 0; 
        while (bl.getNextRow()) 
        {
        	msgDoc = (datapro.eibs.beans.ECE004001Message) bl.getRecord();
            if (msgDoc.getE01CERSEC().equals("2"))
               {
            		monto = monto + Long.parseLong(msgDoc.getE01CERMTO());	              	
               }
         }                
		Paragraph DETAIL02B = new Paragraph(" Remanentes ", normalBoldFont6);
		Paragraph DETAIL02C = new Paragraph(nf.format(monto) + "      ", normalBoldFont6);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		cell = new Cell(DETAIL02B);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(DETAIL02C);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);        			
		// --------------------------------------------------------------------------------
	
        bl.initRow();
        monto = 0; 
        while (bl.getNextRow()) 
        {
        	    msgDoc = (datapro.eibs.beans.ECE004001Message) bl.getRecord();
                if (msgDoc.getE01CERSEC().equals("3"))
                {
        			monto = monto + Long.parseLong(msgDoc.getE01CERMTO());	              	
                }
        }                
		Paragraph DETAIL03B = new Paragraph("Libretas de Ahorro Unipersonal", normalBoldFont6);
		Paragraph DETAIL03C = new Paragraph(nf.format(monto) + "      ", normalBoldFont6);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		cell = new Cell(DETAIL03B);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(DETAIL03C);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);

		// --------------------------------------------------------------------------------
    	
        bl.initRow();
        monto = 0; 
        while (bl.getNextRow()) 
        {
        	    msgDoc = (datapro.eibs.beans.ECE004001Message) bl.getRecord();
                if (msgDoc.getE01CERSEC().equals("4"))                    
                {
        			monto = monto + Long.parseLong(msgDoc.getE01CERMTO());	              	
                }
        }
		Paragraph DETAIL04B = new Paragraph("Libretas de Ahorro Bipersonal", normalBoldFont6);
		Paragraph DETAIL04C = new Paragraph(nf.format(monto) + "      ", normalBoldFont6);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		cell = new Cell(DETAIL04B);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(DETAIL04C);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
        
		// --------------------------------------------------------------------------------
    	
        bl.initRow();
        monto = 0; 
        while (bl.getNextRow()) 
        {
        	    msgDoc = (datapro.eibs.beans.ECE004001Message) bl.getRecord();
                if (msgDoc.getE01CERSEC().equals("5"))
                    {
            			monto = monto + Long.parseLong(msgDoc.getE01CERMTO());	              	
                    }
        }        
		Paragraph DETAIL05B = new Paragraph("Depositos a Plazo Unipersonal", normalBoldFont6);
		Paragraph DETAIL05C = new Paragraph(nf.format(monto) + "      ", normalBoldFont6);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		cell = new Cell(DETAIL05B);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(DETAIL05C);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);

		// --------------------------------------------------------------------------------
    	
        bl.initRow();
        monto = 0;
        while (bl.getNextRow()) 
        {
        	    msgDoc = (datapro.eibs.beans.ECE004001Message) bl.getRecord();
                if (msgDoc.getE01CERSEC().equals("6"))
                {
        			monto = monto + Long.parseLong(msgDoc.getE01CERMTO());	              	
                }
        }        
		Paragraph DETAIL06B = new Paragraph("Depositos a Plazo Bipersonal", normalBoldFont6);
		Paragraph DETAIL06C = new Paragraph(nf.format(monto) + "      ", normalBoldFont6);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		cell = new Cell(DETAIL06B);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(DETAIL06C);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		// --------------------------------------------------------------------------------
    	
        bl.initRow();
        long saldoAcre = 0;
        long saldoDeu = 0;
        long saldoOtros = 0;
        while (bl.getNextRow()) {
        	    msgDoc = (datapro.eibs.beans.ECE004001Message) bl.getRecord();
                if (msgDoc.getE01CERSEC().equals("0"))
                {
        			saldoAcre = saldoAcre + Long.parseLong(msgDoc.getE01CERMTO());	              	
                }
                if (msgDoc.getE01CERSEC().equals("1") || 
                	msgDoc.getE01CERSEC().equals("3") || 
                	msgDoc.getE01CERSEC().equals("4")) 
                {
        			saldoDeu = saldoDeu + Long.parseLong(msgDoc.getE01CERMTO());	              	
                }
                if (msgDoc.getE01CERSEC().equals("2") || 
                    msgDoc.getE01CERSEC().equals("5") ||                 		
                   	msgDoc.getE01CERSEC().equals("6")) 
                    {
            			saldoOtros = saldoOtros + Long.parseLong(msgDoc.getE01CERMTO());	              	
                    }
        }
		long saldoTotal = saldoDeu + saldoOtros;	
		Paragraph DETAIL07B = new Paragraph("Saldo a Favor", normalBoldFont6);
		Paragraph DETAIL07C = new Paragraph(nf.format(saldoAcre) + "      ", normalBoldFont6);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		cell = new Cell(DETAIL07B);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new Cell(DETAIL07C);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		Paragraph DETAIL09B = new Paragraph("TOTAL", normalBoldFont6);
		Paragraph DETAIL09C = new Paragraph(nf.format(saldoTotal) + "      ", normalBoldFont6);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		cell = new Cell(DETAIL09B);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
		cell = new Cell(DETAIL09C);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell); 		
		// --------------------------------------------------------------------------------
       
		doc.add(table);
		table = new Table(1, 4);
		table.setBorderWidth(0);
		table.setCellsFitPage(true);
		table.setPadding(1);
		table.setSpacing(1);
		table.setWidth(100);
		cell = new Cell(BLANK);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		doc.add(table);
		
		
		cell = new Cell(DETAIL08);
		cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		cell = new Cell(DETAIL09);
		cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
	
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);	
		
		cell = new Cell(DETAIL11);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		cell = new Cell(DETAIL12);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		cell = new Cell(DETAIL13);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);

		cell = new Cell(DETAIL14);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		
		cell = new Cell(BLANK);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);
		table.addCell(cell);		
			
		cell = new Cell(DETAIL15);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		doc.add(table);
		
		
	} catch (DocumentException dex) {
		baosPDF.reset();
		throw dex;
	} finally {
		if (doc != null) {
			doc.close();
		}
		if (docWriter != null) {
			docWriter.close();
		}
	}

	if (baosPDF.size() < 1) {
		throw new DocumentException(
			"document has " + baosPDF.size() + " bytes");
	}

	return baosPDF;
	}



public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
   
	Socket s = null;
	MessageContext mc = null;

	ESS0030DSMessage msgUser = null;
  	HttpSession session = null;

	session = (HttpSession)req.getSession(false); 
	
	if (session == null) {
		try {
			res.setContentType("text/html");
			printLogInAgain(res.getWriter());
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception ocurred. Exception = " + e);
		}
	}
	else {

		int screen = R_ENTER_ID;
		
		try {
		
			msgUser = (datapro.eibs.beans.ESS0030DSMessage)session.getAttribute("currUser");

			// Here we should get the path from the user profile
			LangPath = super.rootPath + msgUser.getE01LAN() + "/";

			try
			{
				flexLog("Opennig Socket Connection");
				s = new Socket(super.hostIP, getInitSocket(req) + 1);
				s.setSoTimeout(super.sckTimeOut);
			  	mc = new MessageContext(new DataInputStream(new BufferedInputStream(s.getInputStream())),
							      	    new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
									    "datapro.eibs.beans");
			
			try {
				screen = Integer.parseInt(req.getParameter("SCREEN"));
			}
			catch (Exception e) {
				flexLog("Screen set to default value");
			}
		    flexLog("Entre con Screen = " + screen);
			switch (screen) {
	
			    case R_ENTER_ID : 
			    	procReqEnterID(msgUser, req, res, session);
				break;
				case R_LIST :
					procReqCerConMPRD(mc, msgUser, req, res, session);
					break;
				case A_LIST :
					procActionPos(mc, msgUser, req, res, session);
					break;	
				default :
					res.sendRedirect(super.srctx +LangPath + super.devPage);
					break;
			}
			}
			catch (Exception e) {
				e.printStackTrace();
				int sck = getInitSocket(req) + 1;
				flexLog("Socket not Open(Port " + sck + "). Error: " + e);
				res.sendRedirect(super.srctx +LangPath + super.sckNotOpenPage);
			//	return;
			}
			finally {
				s.close();
			}
			

		}
		catch (Exception e) {
			flexLog("Error: " + e);
			res.sendRedirect(super.srctx +LangPath + super.sckNotRespondPage);
		}
		
	}
	
}
protected void showERROR(ELEERRMessage m) {
	if (logType != NONE) {

		flexLog("ERROR received.");

		flexLog("ERROR number:" + m.getERRNUM());
		flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
		flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
		flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
		flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
		flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
		flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
		flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
		flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
		flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
		flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

	}
}

}