package com.datapro.eibs.security.access.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.UserNotFoundException;
import com.datapro.eibs.security.access.jdbc.bean.CNTRLEUP;
import com.datapro.eibs.security.access.jdbc.bean.USRGRP;
import com.datapro.generic.access.DAOSuper;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.BeanParser;
import com.datapro.generic.beanutil.BeanSql; 


/**
 * @author ogarcia
 *
 */
public class DAOUserEibs extends DAOSuper {

	private static Category cat =
		Category.getInstance(DAOUserEibs.class.getName());

	/**
	 * Constructor for DAOUserEibs.
	 * @param dbid
	 */
	public DAOUserEibs(String dbid) {
		super(dbid);
	}

	/**
	 * Constructor for DAOUserEibs.
	 */
	public DAOUserEibs() {
		super();
	}

	/**
	 * Constructor for DAOUserEibs.
	 * @param connection
	 */
	public DAOUserEibs(Connection connection) {
		super(connection);
	}

	/**
	 * Method getUser.
	 * @param uid
	 * @return CNTRLEUP
	 * @throws UserNotFoundException
	 * @throws DAOException
	 * Get user record.
	 */
	public CNTRLEUP getUser(String uid)
		throws UserNotFoundException, DAOException {

		CNTRLEUP bean = null;

		try {
			String sql = "SELECT * FROM CNTRLEUP WHERE EUPUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNTRLEUP();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} else {
				throw new UserNotFoundException(
					"User not found exception(uid:" + uid + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return bean;

	}

	/**
	 * Method delete.
	 * @param uid
	 * @return int
	 * @throws DAOException
	 * Delete user record.
	 */
	public int delete(String uid) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM CNTRLEUP WHERE EUPUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

	/**
	 * Method insert.
	 * @param bean
	 * @return int
	 * @throws DAOException
	 * Insert user record.
	 */
	public int insert(CNTRLEUP bean) throws DAOException {

		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.insertPrepareStatement();
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

	/**
	 * Method update.
	 * @param bean
	 * @return int
	 * @throws DAOException
	 * Update user record.
	 */
	public int update(CNTRLEUP bean) throws DAOException {

		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.updatePrepareStatement() + " WHERE EUPUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			ps.setString(bs.getNextParamIndex(), bean.getEUPUSR());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

	/**
	 * Method insert.
	 * @param uid
	 * @param sts
	 * @return int
	 * @throws DAOException
	 * Insert user and status record.
	 */
	public int insert(String uid, String sts) throws DAOException {

		int rt;

		try {
			String sql = "INSERT INTO CNTRLEUP (EUPUSR,EUPSTS) VALUES(?,?)";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, sts);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

	/**
	 * Method deleteByGroup.
	 * @param gid
	 * @return int
	 * @throws DAOException
	 * Delete all eibs user for a group.
	 */
	public int deleteByGroup(String gid) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM CNTRLEUP EUP WHERE EUP.EUPUSR IN (SELECT EUP.EUPUSR FROM CNTRLEUP EUP JOIN CNTRLBTH BTH ON BTH.BTHKEY=EUP.EUPUSR WHERE BTH.BTHF03=?)";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, gid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

	/**
	 * Method update.
	 * @param uid
	 * @param sts
	 * @return int
	 * @throws DAOException
	 * Change status for a given user record.
	 */
	public int update(String uid, String sts) throws DAOException {

		int rt;

		try {
			String sql = "UPDATE CNTRLEUP SET EUPSTS=? WHERE EUPUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, sts);
			ps.setString(2, uid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

	/**
	 * Method getGroupsAndUsers.
	 * @return BeanList
	 * @throws DAOException
	 * Get eibs user record plus status sorted by group id and user id.
	 */
	public BeanList getGroupsAndUsers()
		throws DAOException {

		BeanList list = null;

		try {
			String sql = "SELECT CBTH.BTHF03 AS GRPID, CASE WHEN TRIM(CBTH.BTHKEY)='' THEN '" + Security.USER_GROUP_UNASSIGNED + "' ELSE CBTH.BTHKEY END AS USRID, EUP.EUPSTS AS STATUS FROM CNTRLBTH CBTH LEFT OUTER JOIN CNTRLEUP EUP ON EUP.EUPUSR=CBTH.BTHKEY ORDER BY CBTH.BTHF03,CBTH.BTHKEY";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				USRGRP bean = new USRGRP();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}


}
