package datapro.eibs.products;

import java.io.IOException; 
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERC005001Message;
import datapro.eibs.beans.ERC005002Message;
import datapro.eibs.beans.ERC005003Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSERC0050 extends JSEIBSServlet {
	
	private static final long serialVersionUID = 1L;
	protected final int ENTER_SCREEN = 100;
	protected final int SEARCH_DATA = 200;
	protected final int ACTION_CONCILIACION = 300;
	protected final int ACTION_BACK = 400;
	
	
	public JSERC0050() { 
	}

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case ENTER_SCREEN:
			procEnterScreen(user, req, res, session);
			break;
		case SEARCH_DATA:
			procSearchData(user, req, res, session);
			break;
		case ACTION_CONCILIACION:
			procActionContabilizacion(user, req, res, session); 
			break;
		case ACTION_BACK:
			forward("ERC0050_search_movimientos_cartola.jsp", req, res);
			break;
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	protected void procEnterScreen(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		UserPos userPO = (UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		try {
			flexLog("About to call Page: ERC0050_search_movimientos_cartola.jsp");
			forward("ERC0050_search_movimientos_cartola.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
		}
	}
	
	protected void procSearchData(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		try {
			mp = getMessageProcessor("ERC0050", req);
			ERC005001Message message = new ERC005001Message();
			message.setH01USERID(user.getH01USR());
			message.setH01PROGRM("ERC0050");
			message.setH01TIMSYS(getTimeStamp());
				
			message.setH01FLGWK3(req.getParameter("H01FLGWK3"));
			message.setE01RCHRBK(req.getParameter("E01RCHRBK"));
			message.setE01BRMACC(req.getParameter("E01BRMACC"));
			message.setE01BRMCTA(req.getParameter("E01BRMCTA"));
			
			message.setE01DESDDM(req.getParameter("E01DESDDM"));
			message.setE01DESDDD(req.getParameter("E01DESDDD"));
			message.setE01DESDDY(req.getParameter("E01DESDDY"));
			message.setE01HASDDM(req.getParameter("E01HASDDM"));
			message.setE01HASDDD(req.getParameter("E01HASDDD"));
			message.setE01HASDDY(req.getParameter("E01HASDDY"));
			
			message.setE01RCSSTN(req.getParameter("E01RCSSTN"));
					
			
			mp.sendMessage(message);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord(); 
			flexLog(error.toString());
			if (mp.hasError(error)) {
				ses.setAttribute("error", error);
				flexLog("About to call Page: ERC0050_search_movimientos_cartola.jsp");
				forward("ERC0050_search_movimientos_cartola.jsp", req, res);
			}else {
				ERC005001Message receive = (ERC005001Message) mp.receiveMessageRecord();

				JBObjList listCredito = mp.receiveMessageRecordList("H02FLGMAS");
				
		   	    String cartola = "";
			   	int elementos = listCredito.size();
			    if(elementos > 0){
				   	  ERC005002Message msg = (ERC005002Message) listCredito.get(0);
				   	  cartola = msg.getE02RCSSTN().toString();
				  	  receive.setE01RCSSTN(cartola);
			    }
		      	ses.setAttribute("data", receive);  
		   	    ses.setAttribute("listCredito", listCredito);
				ses.setAttribute("descBanco", req.getParameter("E01DSCRBK"));
				flexLog("About to call Page: ERC0050_list_movimientos_cartola.jsp");
				forward("ERC0050_list_movimientos_cartola.jsp", req, res);
			}
			 
		} catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
			flexLog("About to call Page: ERC0050_search_movimientos_cartola.jsp");
			forward("ERC0050_search_movimientos_cartola.jsp", req, res);
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	protected void procActionContabilizacion(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session){
		MessageProcessor mp;
		mp = null;
		@SuppressWarnings("unused")
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try{
			mp = getMessageProcessor("ERC0050",req);
			ERC005003Message message = new ERC005003Message();
			message.setH03USERID(user.getH01USR());
			message.setH03PROGRM("ERC0050");
			message.setH03TIMSYS(getTimeStamp());
					
			Method campo = null;
			String[] listCredito = req.getParameterValues("rowHaber");
			Class classCredito = Class.forName(ERC005003Message.class.getName());
			int indCredito = 1;
			for (String credito : listCredito){
				if (indCredito < 10){
					campo = classCredito.getMethod("setE03SUID0" + indCredito, String.class);
				}else {
					campo = classCredito.getMethod("setE03SUID" + indCredito, String.class);
				}
				campo.invoke(message, credito);
				indCredito++;
			}
			mp.sendMessage(message);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC0050_search_movimientos_cartola.jsp");
				forward("ERC0050_search_movimientos_cartola.jsp", req, res);
			}else {
	
				req.setAttribute("H01FLGWKK", req.getParameter("H01FLGWKK"));
				req.setAttribute("E01RCHRBK", req.getParameter("E01RCHRBK"));
				req.setAttribute("E01BRMACC", req.getParameter("E01BRMACC"));
				req.setAttribute("E01BRMCTA", req.getParameter("E01BRMCTA"));
				
				req.setAttribute("E01DESDDM", req.getParameter("E01DESDDM"));
				req.setAttribute("E01DESDDD", req.getParameter("E01DESDDD"));
				req.setAttribute("E01DESDDY", req.getParameter("E01DESDDY"));
				
				req.setAttribute("E01HASDDM", req.getParameter("E01HASDDM"));
				req.setAttribute("E01HASDDD", req.getParameter("E01HASDDD"));
				req.setAttribute("E01HASDDY", req.getParameter("E01HASDDY"));
				
				procSearchData(user, req, res, session);
	
			}
		}catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
		}
	}
	
	
}
