<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import = "datapro.eibs.beans.EDL006001Message" %>

<html>
<head>
<title>Mantenedor de L&iacute;neas Estatales MYPE</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "listComunas" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="msgLinEst" class="datapro.eibs.beans.EDL006501Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
<script language="JavaScript">


</SCRIPT>  

</head>

<BODY>
<h3 align="center">Consulta L&iacute;neas Estatales MYPE<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="comunas_lineas_estatales_inq, EDL0065"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0060" >
 <input type=HIDDEN name="SCREEN" value="">


  

  <h4>
  		<%if(msgLinEst.getE01MLNDSC().equals("LINEA"))
		{
			out.print("Datos L&iacute;nea");
		}else{
			out.print("Datos SubL&iacute;nea");
		}				
		%>
  </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">L&iacute;nea :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01MLNSEQ" size="7"  value="<%= msgLinEst.getE01MLNSEQ().trim()%>" readonly >
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">
              <div align="right"><%if(userPO.getHeader2().equals("SUB")){out.print("SubL&iacute;nea :");} %>
              </div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
               <%if(userPO.getHeader2().equals("SUB")){ %>
              	<input type="text" name="E01MLNIND" size="7"  value="<%= msgLinEst.getE01MLNIND().trim()%>" readonly >
 	           <%} %>	
              </div>
            </td>
          </tr>

           <tr id="trclear"> 
            <td nowrap width="20%"> 
              <div align="right">Garantizador :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01MLNGTZ" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNGTZ().trim()%>" readonly  style="text-align:right;">
                <input type="text" name="E01MLNDGR" size="41"  value="<%= msgLinEst.getE01MLNDGR().trim()%>" readonly  >
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Programa :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E01MLNPGM" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNPGM().trim()%>" readonly >
                <input type="text" name="E01MLNDPG" size="41"  value="<%= msgLinEst.getE01MLNDPG().trim()%>" readonly  >
              </div>
            </td>
          </tr>
          
                 <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Estado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSTS1" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSTS().trim()%>" readonly  >
                <input type="text" name="E01MLNDST1" size="41"  value="<%= msgLinEst.getE01MLNDST().trim()%>" readonly  >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Tasa :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTAS1" size="11" maxlength="10" value="<%= msgLinEst.getE01MLNTAS().trim()%>" readonly style="text-align:right;" >
              </div>
            </td>
          </tr> 

          <tr id="trclear"> 
            <td height="23"> 
              <div align="right">% Garant&iacute;a :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNGRT" size="11" maxlength="10" value="<%= msgLinEst.getE01MLNGRT().trim()%>" onkeypress="enterDecimal()" style="text-align:right;"  >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Segmento 1 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG1" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG1().trim()%>"  readonly>
                <input type="text" name="E01MLNDG1" size="41"  value="<%= msgLinEst.getE01MLNDG1().trim()%>" readonly  >
             </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Cupo L&iacute;nea :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCLI" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNCLI().trim())%>"  readonly style="text-align:right;">
             </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 2 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG2" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG2().trim()%>"  readonly>
                <input type="text" name="E01MLNDG2" size="41"  value="<%= msgLinEst.getE01MLNDG2().trim()%>" readonly  >
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Cupo L&iacute;nea Pesos :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCMN" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNCMN().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 3 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG3" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG3().trim()%>"  readonly>
                <input type="text" name="E01MLNDG3" size="41"  value="<%= msgLinEst.getE01MLNDG3().trim()%>" readonly  >
             </div>
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Saldo Disponible :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSDO" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNSDO().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 4 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG4" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG4().trim()%>"  readonly>
                <input type="text" name="E01MLNDG4" size="41"  value="<%= msgLinEst.getE01MLNDG4().trim()%>" readonly  >
            </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Saldo Retenido :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
              	<input type="text" name="E01MLNSDR" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNSDR().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 5 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG5" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG5().trim()%>"  readonly>
                <input type="text" name="E01MLNDG5" size="41"  value="<%= msgLinEst.getE01MLNDG5().trim()%>" readonly  >
              </div>
            </td>
          </tr>
          
		</table>
	  </td>	
	</tr>
  </table>	 
   
<h4>Datos Comuna</h4>   
<%
	if ( listComunas.getNoResult() ) {
%>

  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p>&nbsp;</p>
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
        </div>
	  </TD>
	</TR>
    </TABLE>
	
<%}else {
 
		 if ( !error.getERRNUM().equals("0")  ) {
     			error.setERRNUM("0");
    			out.println("<SCRIPT Language=\"Javascript\">");
     			out.println("       showErrors()");
     			out.println("</SCRIPT>");
    		 }
%> 
 
          
  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
 			<th align=LEFT nowrap width="5%">Comuna</th>
 			<th align=LEFT nowrap width="30%"></th>
 			<th align=LEFT nowrap width="65%"></th>
          </tr>
 
           <%
                listComunas.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (listComunas.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	
               		datapro.eibs.beans.EDL006001Message msgList = (datapro.eibs.beans.EDL006001Message) listComunas.getRecord();
					 %>
					<tr id="dataTable"> 
						<td align="right" nowrap>
							<%=msgList.getE01MLNCCM() %>
						</td>
						<td align="left" nowrap>
							<%=msgList.getE01MLNDCM() %>
						</td>						
					
         			</tr>
          	<% } %>
              </table>
              </td>
              </tr>
  </table>
  

<%}%>

  </form>

</body>
</html>
