<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import = "datapro.eibs.master.Util" %>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>

<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="datarec" class="datapro.eibs.beans.EPV115001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

  	 function cerrarVentana(){
		window.open('','_parent','');
		window.close(); 
  	}
  	
//  Process according with user selection
 function goAction(op) {

 	switch (op){
	//Cancel
	case 0:  {
				var campo = document.forms[0].E01PVCVAL;
				var campoInt = parseInt(campo.value);
				campo.value = campoInt.toString();

				document.forms[0].submit();

		       	break;
		}
	case 1:  {
 		//document.forms[0].SCREEN.value = "101";
 		cerrarVentana();
       	break;
		}
	}
	//document.forms[0].submit();
 }
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
	boolean newOnly=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<%
	// Determina si es nuevo o mantencion
	if (userPO.getPurpose().equals("NEW")){
			newOnly=false;
		} else if (userPO.getPurpose().equals("MAINTENANCE")) {
    		newOnly=false;
		} else {
    		newOnly=true;		
		}
%>

<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	CONSULTA DE CHEQUE
A TERCEROS<%} else if (maintenance){ %>
	MANTENIMIENTO DE CHEQUE
A TERCEROS
<%} else { %>
	NUEVO CHEQUE
A TERCEROS
<%} %>
 <% String emp = (String)session.getAttribute("EMPCT");
 	emp = (emp==null)?"":emp;//si es blanco viene llamado por menu, sino viene llamdo desde la pantalla EPV1010
 %>
 <%if ("".equals(emp)){ %> 
 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="checks_third_maintenance.jsp, EPV1150"></h3>
<hr size="4">
<%}%>
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1150" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01UBK"  value="<%= currUser.getE01UBK().trim()%>">
  <input type=HIDDEN name="H01FLGMAS"  value="<%= datarec.getH01FLGMAS().trim()%>">
  <input type=HIDDEN name="E01PVCBC1"  value="<%= datarec.getE01PVCBC1().trim()%>">
  <input type=HIDDEN name="E01PVCBC2"  value="<%= datarec.getE01PVCBC2().trim()%>">
  <input type=HIDDEN name="E01PVCBD1"  value="<%= datarec.getE01PVCBD1().trim()%>">
  <input type=HIDDEN name="E01PVCBD2"  value="<%= datarec.getE01PVCBD2().trim()%>">

 <% int row = 0;%>
<%if ("".equals(emp)){ %> 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="datarec" property="E01PVCCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Propuesta : 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="datarec" property="E01PVCNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">Sequencia :  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="datarec" property="E01PVCSEQ" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="3" maxlength="2" readonly="true"/>
             </td>
         </tr>
       </table>
      </td>
    </tr>
  </table>
  <%}else{%>
  	<input  type="hidden" name="E01PVCCUN" value="<%=datarec.getE01PVCCUN()%>">
  	<input  type="hidden" name="E01PVCNUM" value="<%=datarec.getE01PVCNUM()%>">
  	<input  type="hidden" name="E01PVCSEQ" value="<%=datarec.getE01PVCSEQ()%>"> 
  <%} %>

<%if ("".equals(emp)){ %>      
  <h4>Datos del Cheque</h4>
<%} %>      
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
             <td width="15%"> 
              <div align="right">Rut Tercero:</div>
            </td>
            <td width="35%">
			<%if (readOnly){ %>
	            <eibsinput:text property="E01PVCRUT" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>"  readonly="true" />
		    <% } else { %>
	            <eibsinput:text property="E01PVCRUT" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>" required="true" />
	              <a href="javascript:GetBeneficiaryId('E01PVCRUT')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a> 
		    <% } %>  
            </td>
           <td width="15%"> 
              <div align="right">Cliente :</div>
            </td>
            <td width="35%">             
				<eibsinput:help name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" property="E01PVCCCU" 
					fn_param_one="E01PVCCCU" fn_param_two="E01PVCNME" fn_param_three="E01PVCRUT"   readonly="<%=readOnly %>"/>
            </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td > 
              <div align="right">Nombre :</div>
            </td>
            <td colspan="3">
            	<eibsinput:text name="datarec" property="E01PVCNME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" required="true" readonly="<%=readOnly%>"/>
            </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td > 
              <div align="right">Direccion   :</div>
            </td>
            <td colspan="3">
            	<eibsinput:text name="datarec" property="E01PVCAD1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="<%=readOnly%>"/> <br>
            	<eibsinput:text name="datarec" property="E01PVCAD2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="<%=readOnly%>"/> <br>
            	<eibsinput:text name="datarec" property="E01PVCAD3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="<%=readOnly%>"/>
            </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td > 
              <div align="right">Monto del Cheque:</div>
            </td>
            <td >
                <% 
                 int MonCheque = 0; 
    			 try {
    				 String StrMonCheque = datarec.getE01PVCVAL().replaceAll(",","");
     				 StrMonCheque = StrMonCheque.replace(".","");

					 StrMonCheque = StrMonCheque.substring(0,StrMonCheque.length()-2);

	    			 MonCheque = Integer.parseInt(StrMonCheque);
     				}
  					  catch(Exception e )
  					{
      				  MonCheque = 0;
    				 }
                %>
                <input type="text" name="E01PVCVAL" value="<%=MonCheque%>" onkeypress=" enterInteger()" class="TXTRIGHT" <%if(readOnly) out.print("readonly");%>>  
	        </td> 
           <td > 
            </td><td > 
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Cheque del Banco :</div>
            </td>
            <td nowrap  width="30%"> 
              <input type="radio" name="E01PVCBSL" value="1" <%if (!datarec.getE01PVCBSL().equals("2")) out.print("checked"); %>>
              <%= datarec.getE01PVCBC1() + "-" + datarec.getE01PVCBD1().toUpperCase()%> <br>
              <input type="radio" name="E01PVCBSL" value="2" <%if (datarec.getE01PVCBSL().equals("2")) out.print("checked"); %>>
              <%= datarec.getE01PVCBC2() + "-" + datarec.getE01PVCBD2().toUpperCase()%>
            </td>
            <td nowrap width="20%" > 
              <div align="right">Sucursal :</div>
            </td>
            <td nowrap  width="30%"> 
            	<eibsinput:text name="datarec" property="E01PVCBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
           </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>
  
   <%if  (!readOnly) { %>
      <div align="center"> 
          <input id="EIBSBTN" type=button name="button1" value="Enviar" onclick="javascript:goAction(0);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>
    <% } else { %>
      <div align="center"> 
          <input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>     
    <% } %>  
    
  </form>
 <%if ("S".equals(request.getAttribute("ACT"))){%>
 <script type="text/javascript">
 	  //recargamos la pagina que nos llamo..	  
	  window.opener.location.href =  '<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1150?SCREEN=101&E01PVCCUN=<%=userPO.getCusNum()%>&E01PVCNUM=<%=userPO.getHeader23()%>';	   	   
     <%//NOTA: solo para activar el check de la pagina integral%>
     <%  String re =(String) session.getAttribute("EMPCT");%>
	 <%  if ("S".equals(re)){%>
			window.opener.parent.setRecalculate3();	       
	 <% } %>     	 
  	  //cerramos la ventana
  	  cerrarVentana();	  
 </script>
 <% } %>  
</body> 
</HTML>