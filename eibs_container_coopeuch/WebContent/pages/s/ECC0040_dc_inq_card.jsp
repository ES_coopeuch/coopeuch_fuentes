<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Debit Card Inquiry</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id="dcInq" class="datapro.eibs.beans.ECC004001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<SCRIPT LANGUAGE="javascript">
 
	//builtNewMenu(cc_i_opt);
  function goAction(op)
   {
      if (op == 1) 
      {
		pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150?SCREEN=100";
	  }
	  else if (op == 2)
      {
		pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150?SCREEN=200";
	  }
	  else if (op == 3)
	  {
		pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150?SCREEN=1000";
	  } 
	  else if (op == 4)
	  {
		pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150?SCREEN=2000";
	  } 	   	      
	   CenterWindow(pg,800,800,2);
   }  
</SCRIPT>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0"); 
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
  
   out.println("<SCRIPT> initMenu();  </SCRIPT>");

%> 

</head>
<body>
<h3 align="center">Consulta Tarjeta de D�bito
 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="dc_inq_card.jsp,ECC0040"> 
</h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0040" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="">  
 <% int row = 0;%>

  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">N�mero Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
		        <eibsinput:text name="dcInq" property="E01CCRNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CARD_NUMBER %>" readonly="true" />            
            </td>
            <td nowrap width="25%"> 
              <div align="right">Tipo de Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCRTPI" disabled>
                <option value=" " <% if (!(
		                	dcInq.getE01CCRTPI().equals("T") || 
                			dcInq.getE01CCRTPI().equals("A"))) 
                			out.print("selected"); %> selected></option>
                <option value="T" <% if(dcInq.getE01CCRTPI().equals("T")) out.print("selected");%>>Titular</option>
                <option value="A" <% if(dcInq.getE01CCRTPI().equals("A")) out.print("selected");%>>Adicional</option>                			
              </select>
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Nombre TarjetaHabiente : </div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="E01CCRNAM" size="35" maxlength="35" readonly value="<%= dcInq.getE01CCRNAM().trim()%>">               
            </td>
            <td nowrap width="25%"> 
              <div align="right">Rut TarjetaHabiente : </div>
            </td>
            <td nowrap width="23%"> 
                <input type="text" name="E01CCRCI2" size="15" maxlength="15" readonly value="<%= dcInq.getE01CCRCI2().trim()%>">               
            </td>
          </tr>   
        </table>
      </td>
    </tr>
  </table>  
  <h4>Tarjeta</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Estado Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCRSTS" disabled>
                <option value=" " <% if (!(
		                	dcInq.getE01CCRSTS().equals("00") || 
                			dcInq.getE01CCRSTS().equals("01"))) 
                			out.print("selected"); %> selected></option>
                <option value="0" <% if(dcInq.getE01CCRSTS().equals("00")) out.print("selected");%>>Inactiva</option>
                <option value="1" <% if(dcInq.getE01CCRSTS().equals("01")) out.print("selected");%>>Activa</option>  
              </select>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Activaci�n : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="dcInq" fn_year="E01CCRATY" fn_month="E01CCRATM" fn_day="E01CCRATD" readonly="true"/> 
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Bloqueo Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="D01CCRLKC" size="35" maxlength="35" readonly value="<%= dcInq.getD01CCRLKC().trim()%>">  		              
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Bloqueo : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="dcInq" fn_year="E01CCRBKY" fn_month="E01CCRBKM" fn_day="E01CCRBKD" readonly="true"/>             
            </td>
          </tr>   
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%">                
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Creaci�n : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="dcInq" fn_year="E01CCRISY" fn_month="E01CCRISM" fn_day="E01CCRISD" readonly="true"/>             
            </td>
          </tr>   
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%">                
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Expiraci�n : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="dcInq" fn_year="E01CCREXY" fn_month="E01CCREXM" fn_day="E01CCREXD" readonly="true"/>             
            </td>
          </tr>    
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%">                
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Cambio Estado : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="dcInq" fn_year="E01CCRLSY" fn_month="E01CCRLSM" fn_day="E01CCRLSD" readonly="true"/>             
            </td>
          </tr>                                                   
        </table>
      </td>
    </tr>
  </table>  
  <h4>Cuenta</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">N�mero Cuenta : </div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" name="E01CCRCRA" size="12" maxlength="12" readonly value="<%= dcInq.getE01CCRCRA().trim()%>">             
            </td>
            <td nowrap width="25%"> 
              <div align="right">Producto : </div>
            </td>
            <td nowrap width="23%">
              <input type="text" name="E01ACMPRO" size="4" maxlength="4" readonly value="<%= dcInq.getE01ACMPRO().trim()%>"> 
              <input type="text" name="D01ACMPRO" size="35" maxlength="35" readonly value="<%= dcInq.getD01ACMPRO().trim()%>">                                                        
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Estado de la Cuenta : </div>
            </td>
            <td nowrap width="23%"> 
             <select name="E01ACMAST" disabled>
                <option value=" " <% if (!(dcInq.getE01ACMAST().equals("A") ||dcInq.getE01ACMAST().equals("C")
				||dcInq.getE01ACMAST().equals("I")||dcInq.getE01ACMAST().equals("D")
				||dcInq.getE01ACMAST().equals("O")||dcInq.getE01ACMAST().equals("E")
				||dcInq.getE01ACMAST().equals("T"))) out.print("selected"); %>></option>
                <option value="A" <% if (dcInq.getE01ACMAST().equals("A")) out.print("selected"); %>>Activa</option>
                <option value="C" <% if (dcInq.getE01ACMAST().equals("C")) out.print("selected"); %>>Cancelada</option>
                <option value="I" <% if (dcInq.getE01ACMAST().equals("I")) out.print("selected"); %>>Inactiva 1</option>
                <option value="D" <% if (dcInq.getE01ACMAST().equals("D")) out.print("selected"); %>>Inactiva 2</option>
                <option value="O" <% if (dcInq.getE01ACMAST().equals("O")) out.print("selected"); %>>Controlada</option>
				<option value="E" <% if (dcInq.getE01ACMAST().equals("E")) out.print("selected"); %>>Embargada</option>
				<option value="T" <% if (dcInq.getE01ACMAST().equals("T")) out.print("selected"); %>>Acepta S�lo Dep�sitos</option>
              </select>
            </td>
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%"> 
            </td>
          </tr>           
        </table>
      </td>
    </tr>
  </table>
  <br>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>  
  </form>
</body>
</html>
