<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="datapro.eibs.master.Util"%>
<html>
<head>
<title>Certificados</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ECE0040Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<jsp:useBean id="cerBasic" class="datapro.eibs.beans.ECE004001Message" scope="session" />

<script language="JavaScript">


function goAction(op) {

	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}

</SCRIPT>  

</head>

<BODY>
<h3 align="center">Consulta Informacion Posesion Efectiva<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cer_posesion_consulta.jsp,ECE0040"></h3> 
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECE0040" >
  <input type=HIDDEN name="SCREEN" value="2">
  <input type=HIDDEN name="opt" value="1">  
  <table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			class="tbhead">
			<tr id="trdark">
				<td nowrap width="16%">
				<div align="right"><b>Cliente :</b></div>
				</td>
				<td nowrap width="20%">
				<div align="left"><input type="text" name="E01SELCUN"
					size="10" maxlength="9" value="<%=userPO.getHeader1()%>">
				</div>
				</td>
				<td nowrap width="16%">
				<div align="right"><b>Nombre :</b></div>
				</td>
				<td nowrap colspan="3">
				<div align="left"><font face="Arial"><font face="Arial"><font
					size="2"> <input type="text" name="E01CERNAM" size="45"
					maxlength="45" value="<%=userPO.getHeader2()%>">
				</font></font></font></div>
				</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="16%">
				<div align="right"><b>Identificación :</b></div>
				</td>
				<td nowrap width="20%">
				<div align="left"><input type="text" name="E01CERIDN"
					size="10" maxlength="9" value="<%=userPO.getHeader3()%>">
				</div>
				</td>
				<td nowrap width="16%">
				<div align="right"><b>Socio desde :</b></div>
				</td>
		        <TD NOWRAP width="16%">		        
		        <input type="text" name="Header4" size="3" maxlength="2" value="<%=userPO.getHeader4()%>" readonly>
		        <input type="text" name="Header5" size="3" maxlength="2" value="<%=userPO.getHeader5()%>" readonly>
		        <input type="text" name="Header6" size="5" maxlength="4" value="<%=userPO.getHeader6()%>" readonly>
		        </TD>
			</tr>			
		</table>
		</td>
	</tr>
</table>
  
  
  <%
	if ( ECE0040Help.getNoResult()) {
 %>
  
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>Cliente No cuenta con Productos</b></p>
          <table class="tbenter" width=100% align=center>
           <tr>
              <td class=TDBKG>
                <div align="center"><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
              </td>
           </tr>
         </table>
	  </div>

	  </TD>
	</TR>
    </TABLE>
	
  <%  
}
else {
%> <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }
%> 
	
<h4>Valores :</h4>
  <TABLE  id=cfTable class="tableinfo">
 <TR > 
    <TD NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="20%"> 
              <div align="center"></div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Tipo de Cuenta</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Monto en $</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center"> </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center"> </div>
            </th>
          </tr>
          <%
                ECE0040Help.initRow();
				int idx = 0;
				String valStyle = "";
				String titulo = "";
        		while (ECE0040Help.getNextRow()) {
                datapro.eibs.beans.ECE004001Message msgList = (datapro.eibs.beans.ECE004001Message) ECE0040Help.getRecord();
                if (msgList.getE01CERSEC().equals("0")) titulo = "Cuenta Vista                 "; 
                if (msgList.getE01CERSEC().equals("1")) titulo = "Cuotas de Participacion      ";  
                if (msgList.getE01CERSEC().equals("2")) titulo = "Remanentes                   ";
                if (msgList.getE01CERSEC().equals("3")) titulo = "Ahorro Unipersonal           ";
                if (msgList.getE01CERSEC().equals("4")) titulo = "Ahorro Bipersonal            "; 
                if (msgList.getE01CERSEC().equals("5")) titulo = "Deposito a Plazo Unipersonal ";
                if (msgList.getE01CERSEC().equals("6")) titulo = "Deposito a Plazo Bipersonal  ";                                                                      
               	if (idx++ % 2 != 0)
					valStyle = "trdark";
				else
					valStyle = "trclear";
		    %>
		    	<TR id="<%= valStyle %>">
				<TD NOWRAP  ALIGN=CENTER width=\"20%\"></td>
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= titulo %></td>				
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"><%= Util.formatCCY(msgList.getE01CERMTO()) %></td>	
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"></td>	
				<TD NOWRAP  ALIGN=CENTER width=\"15%\"></td>												
 				</TR>
		     <%
               }
             %>
        </table>
        </TD>
        </TR>
    </table>

		     <%
               }
             %>    
  <table class="tbenter" width=100% align=center>
    <tr> 
      <td class=TDBKG width="33%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Imprimir<br>Certificado</b></a></div>
      </td>
      <td class=TDBKG width="33%"> 
        <div align="center"><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
      </td>
    </tr>
  </table>
<SCRIPT language="JavaScript">
  
  showChecked("CURRCODE");
  
 function tableresize() {
    adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  tableresize();
  window.onresize=tableresize;   
</SCRIPT>
</form>

</body>
</html>
