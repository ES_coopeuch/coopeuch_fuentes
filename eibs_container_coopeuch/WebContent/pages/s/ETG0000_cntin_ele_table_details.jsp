<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Table de Codigos del Sistema</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="refCodes" class="datapro.eibs.beans.ETG000000Message"  scope="session" />
<jsp:useBean id="msgF1" class="datapro.eibs.beans.ETG000001Message"  scope="session" />
<jsp:useBean id="msgF2" class="datapro.eibs.beans.ETG000002Message"  scope="session" />
<jsp:useBean id="msgF3" class="datapro.eibs.beans.ETG000003Message"  scope="session" />
<jsp:useBean id="msgF4" class="datapro.eibs.beans.ETG000004Message"  scope="session" />
<jsp:useBean id="msgF5" class="datapro.eibs.beans.ETG000005Message"  scope="session" />
<jsp:useBean id="msgF6" class="datapro.eibs.beans.ETG000006Message"  scope="session" />
<jsp:useBean id="msgF7" class="datapro.eibs.beans.ETG000007Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

</head>

<BODY>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<script language="JavaScript">



function goAction(op) {

	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}

function cancel() {
	document.forms[0].SCREEN.value = 1100;
	document.forms[0].submit();
}

function goDelete() {

	if(confirm("Esta seguro que desea borrar este codigo?")){
		document.forms[0].opt.value = 3;
		document.forms[0].submit();
	}
}

</SCRIPT>  

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "NEW".equals(userPO.getPurpose()) ? "" : "readonly";     
%>



<h3 align="center">Elementos Tablas de C&oacute;digos del Sistema<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cntin_ele_table_details.jsp, ETG0000"></h3>
<hr size="4">
<form name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSETG0000" >
  <p> 
    <input type=HIDDEN name="SCREEN" value="1600">
    
    <input type=HIDDEN name="ETG00FLD01" value="<%=refCodes.getETG00FLD01().trim()%>">
    <input type=HIDDEN name="ETG00NOM01" value="<%= refCodes.getETG00NOM01().trim()%>">
    <input type=HIDDEN name="ETG00FLD02" value="<%=refCodes.getETG00FLD02().trim()%>">
    <input type=HIDDEN name="ETG00NOM02" value="<%= refCodes.getETG00NOM02().trim()%>">
    <input type=HIDDEN name="ETG00FLD03" value="<%=refCodes.getETG00FLD03().trim()%>">
    <input type=HIDDEN name="ETG00NOM03" value="<%= refCodes.getETG00NOM03().trim()%>">
    <input type=HIDDEN name="ETG00FLD04" value="<%=refCodes.getETG00FLD04().trim()%>">
    <input type=HIDDEN name="ETG00NOM04" value="<%= refCodes.getETG00NOM04().trim()%>">
    <input type=HIDDEN name="ETG00FLD05" value="<%=refCodes.getETG00FLD05().trim()%>">
    <input type=HIDDEN name="ETG00NOM05" value="<%= refCodes.getETG00NOM05().trim()%>">
    <input type=HIDDEN name="ETG00FLD06" value="<%=refCodes.getETG00FLD06().trim()%>">
    <input type=HIDDEN name="ETG00NOM06" value="<%= refCodes.getETG00NOM06().trim()%>">
    <input type=HIDDEN name="ETG00FLD07" value="<%=refCodes.getETG00FLD07().trim()%>">
    <input type=HIDDEN name="ETG00NOM07" value="<%= refCodes.getETG00NOM07().trim()%>">
    <input type=HIDDEN name="ETG00FLD08" value="<%=refCodes.getETG00FLD08().trim()%>">
    <input type=HIDDEN name="ETG00NOM08" value="<%= refCodes.getETG00NOM08().trim()%>">
    <input type=HIDDEN name="ETG00FLD09" value="<%=refCodes.getETG00FLD09().trim()%>">
    <input type=HIDDEN name="ETG00NOM09" value="<%= refCodes.getETG00NOM09().trim()%>">
    <input type=HIDDEN name="ETG00FLD10" value="<%=refCodes.getETG00FLD10().trim()%>">
    <input type=HIDDEN name="ETG00NOM10" value="<%= refCodes.getETG00NOM10().trim()%>">
  </p>
  
  <p> 
  <h4>Par&aacute;metros de Definici&oacute;n de Tablas</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Tabla :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="ETG00ELE" size="9" maxlength="8" value="<%= refCodes.getETG00ELE().trim()%>" readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Nombre o T&iacute;tulo :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="ETG00DESCL" size="66" maxlength="58" value="<%= refCodes.getETG00DESCL().trim()%>"  readonly>
	              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Tipo C&oacute;digo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG00TCOD" size="15" value="<%if(refCodes.getETG00TCOD().equals("N")) out.print("N&uacute;merico"); else out.print("Alfan&uacute;merico"); %>" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="right">C&oacute;digo Menor Aceptado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG00NROM" size="3" maxlength="2" value="<%= refCodes.getETG00NROM().trim()%>" readonly style="text-align:right;">
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Es Tabla Usuario :</div>
            </td>
            <td nowrap> 
              <div align="left">
              	<input type="text" name="ETG00TUSR" size="2" maxlength="1" value="<%= refCodes.getETG00TUSR().trim()%>" readonly >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">C&oacute;digo de Grupo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG00TGRP" size="4" maxlength="3" value="<%= refCodes.getETG00TGRP().trim()%>" readonly style="text-align:right;">
                <input type="text" name="ETG00TGRD" size="50" maxlength="3" value="<%= refCodes.getETG00TGRD().trim()%> "  readonly >
              </div>
            </td>
          </tr> 
          
          <tr id="trclear"> 
            <td nowrap  height="23"> 
              <div align="right">Formato :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG00FMTO" size="2" maxlength="1" value="<%= refCodes.getETG00FMTO().trim()%>" readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap height="23"> 
	             <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

<% int fila=1;%>

  <h4>Elementos Tabla</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

		<%if(refCodes.getETG00FMTO().trim().equals("1")){ %>

          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right"><%= refCodes.getETG00NOMCD().trim()%>&ensp;:&ensp;</div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <%if(refCodes.getETG00TCOD().equals("N")){%>
	                <input type="text" name="ETG01ELE" size="9" maxlength="8" value="<% if(msgF1.getETG01ELE().equals("")) out.print("0"); else  out.print(Integer.valueOf(msgF1.getETG01ELE().trim()));%>" <%=readonly%> style="text-align:right;" onKeyPress="enterInteger()">
                <%}else{%>
	                <input type="text" name="ETG01ELE" size="9" maxlength="8" value="<%= msgF1.getETG01ELE().trim()%>" <%=readonly%>>
                <%}%>
              </div>
            </td>
          </tr>

		  <% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM01()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG01DESCL" size="41" maxlength="40" value="<%= msgF1.getETG01DESCL().trim()%>"  >
              </div>
            </td>
          </tr>
          <% } %> 

         <% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM02()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG01VAL01" size="13" maxlength="12" value="<%= msgF1.getETG01VAL01().trim()%>"  style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
          </tr>
         <% } %> 

         <% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM03()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG01VAL02" size="10" maxlength="9" value="<%= msgF1.getETG01VAL02().trim()%>" style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
          </tr>
        <% } %> 

	    <% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right"><%= refCodes.getETG00NOM04()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG01ALFA1" size="6" maxlength="5" value="<%= msgF1.getETG01ALFA1().trim()%>"  >
              </div>
            </td>
          </tr>
        <% } %> 

	    <% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM05()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG01ALFA2" size="16" maxlength="15" value="<%= msgF1.getETG01ALFA2().trim()%>"  >
              </div>
            </td>
          </tr>
        <% } %> 

        <% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right"><%= refCodes.getETG00NOM06()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG01VAL03" size="5" maxlength="4" value="<%= msgF1.getETG01VAL03().trim()%>" style="text-align:right;" onKeyPress="enterInteger()" >
              </div>
            </td>
          </tr>
      <% } %> 
	<% } %> 
	
		<%if(refCodes.getETG00FMTO().trim().equals("2")){ %>

          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right"><%= refCodes.getETG00NOMCD().trim()%>&ensp;:&ensp;</div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <%if(refCodes.getETG00TCOD().equals("N")){%>
	                <input type="text" name="ETG02ELE" size="9" maxlength="8" value="<% if(msgF2.getETG02ELE().equals("")) out.print("0"); else  out.print(Integer.valueOf(msgF2.getETG02ELE().trim()));%>" <%=readonly%> style="text-align:right;" onKeyPress="enterInteger()">
                <%}else{%>
	                <input type="text" name="ETG02ELE" size="9" maxlength="8" value="<%= msgF2.getETG02ELE().trim()%>" <%=readonly%>>
                <%}%>
              </div>
          </tr>

          <% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM01()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
              <% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
                <input type="text" name="ETG02DESCL" size="25" maxlength="24" value="<%= msgF2.getETG02DESCL().trim()%>"  >
	          <% } %> 
              </div>
            </td>
          </tr>
          <% } %> 

          <% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
          <tr id="trclear">            
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM02()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG02ALFA1" size="6" maxlength="5" value="<%= msgF2.getETG02ALFA1().trim()%>"  >
              </div>
            </td>
          </tr>
	      <% } %> 
          
          <% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap height="28"> 
              <div align="right"><%= refCodes.getETG00NOM03()%> :</div>
            </td>
            <td nowrap height="28"> 
              <div align="left"> 
                <input type="text" name="ETG02ALFA2" size="6" maxlength="5" value="<%= msgF2.getETG02ALFA2().trim()%>"  >
              </div>
            </td>
         </tr>
         <% } %> 
            
        <% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap height="28"> 
              <div align="right"><%= refCodes.getETG00NOM04()%> :</div>
            </td>
            <td nowrap height="28"> 
              <div align="left"> 
                <input type="text" name="ETG02ALFA3" size="6" maxlength="5" value="<%= msgF2.getETG02ALFA3().trim()%>"  >
              </div>
            </td>
         </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right"><%= refCodes.getETG00NOM05()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG02ALFA4" size="6" maxlength="5" value="<%= msgF2.getETG02ALFA4().trim()%>"  >
              </div>
            </td>
         </tr>
         <% } %> 

         <% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM06()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG02VAL01" size="6" maxlength="5" value="<%= msgF2.getETG02VAL01().trim()%>" style="text-align:right;" onKeyPress="enterInteger()"  >
              </div>
            </td>
          </tr>
          <% } %> 

          <% if (refCodes.getETG00FLD07().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM07()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG02VAL02" size="6" maxlength="5" value="<%= msgF2.getETG02VAL02().trim()%>" style="text-align:right;" onKeyPress="enterInteger()">
              </div>
            </td>
        </tr>
		<% } %> 

        <% if (refCodes.getETG00FLD08().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM08()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG02ALFA5" size="4" maxlength="3" value="<%= msgF2.getETG02ALFA5().trim()%>"  >
              </div>
            </td>
        </tr>
        <% } %> 
         <% if (refCodes.getETG00FLD09().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap height="23"> 
               <div align="right"><%= refCodes.getETG00NOM09()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG02ALFA6" size="4" maxlength="3" value="<%= msgF2.getETG02ALFA6().trim()%>"  >
              </div>
            </td>
        </tr>
        <% } %> 

        <% if (refCodes.getETG00FLD10().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM10()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG02VAL03" size="23" maxlength="22" value="<%= msgF2.getETG02VAL03().trim()%>" style="text-align:right;" onKeyPress="enterDecimal()">
              </div>
            </td>
        </tr>
        <% } %> 

          <% if (refCodes.getETG00FLD11().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM11()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG02VAL04" size="4" maxlength="3" value="<%= msgF2.getETG02VAL04().trim()%>"  style="text-align:right;" onKeyPress="enterInteger()">
              </div>
            </td>
        </tr>
        <% } %> 

      <% } %> 


		<%if(refCodes.getETG00FMTO().trim().equals("3")){ %>

        <tr id="trclear">  
            <td nowrap width="20%"> 
              <div align="right"><%= refCodes.getETG00NOMCD().trim()%>&ensp;:&ensp;</div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <%if(refCodes.getETG00TCOD().equals("N")){%>
	                <input type="text" name="ETG03ELE" size="9" maxlength="8" value="<% if(msgF3.getETG03ELE().equals("")) out.print("0"); else  out.print(Integer.valueOf(msgF3.getETG03ELE().trim()));%>" <%=readonly%> style="text-align:right;" onKeyPress="enterInteger()">
                <%}else{%>
	                <input type="text" name="ETG03ELE" size="9" maxlength="8" value="<%= msgF3.getETG03ELE().trim()%>" <%=readonly%>>
                <%}%>
              </div>
            </td>
          </tr>

          <% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM01()%> :</div>
            </td>
            <td nowrap > 
              <div align="left" > 
                <input type="text" name="ETG03DATO" size="81" maxlength="80" value="<%= msgF3.getETG03DATO().trim()%>"  >
	          </div>
            </td>
          </tr>
          <% } %> 
      <% } %> 

		<%if(refCodes.getETG00FMTO().trim().equals("4")){ %>
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right"><%= refCodes.getETG00NOMCD().trim()%>&ensp;:&ensp;</div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <%if(refCodes.getETG00TCOD().equals("N")){%>
	                <input type="text" name="ETG04ELE" size="9" maxlength="8" value="<% if(msgF4.getETG04ELE().equals("")) out.print("0"); else  out.print(Integer.valueOf(msgF4.getETG04ELE().trim()));%>" <%=readonly%> style="text-align:right;" onKeyPress="enterInteger()">
                <%}else{%>
	                <input type="text" name="ETG04ELE" size="9" maxlength="8" value="<%= msgF4.getETG04ELE().trim()%>" <%=readonly%>>
                <%}%>
              </div>
            </td>
          </tr>

          <% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM01()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG04DESCL" size="29" maxlength="28" value="<%= msgF4.getETG04DESCL().trim()%>"  >
              </div>
            </td>
          </tr>
          <% } %> 

          <% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM02()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG04VAL01" size="15" maxlength="14" value="<%= msgF4.getETG04VAL01().trim()%>" style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM03()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG04VAL02" size="15" maxlength="14" value="<%= msgF4.getETG04VAL02().trim()%>"  style="text-align:right;" onKeyPress="enterDecimal()">
              </div>
            </td>
          </tr>
		<% } %> 

          <% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM04()%> :</div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="ETG04VAL03" size="15" maxlength="14" value="<%= msgF4.getETG04VAL03().trim()%>" style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
		  </TR>
        <% } %> 

        <% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM05()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG04VAL04" size="23" maxlength="22" value="<%= msgF4.getETG04VAL04().trim()%>" style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
          </tr>
        <% } %> 

        <% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM06()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG04VAL05" size="3" maxlength="2" value="<%= msgF4.getETG04VAL05().trim()%>" style="text-align:right;" onKeyPress="enterInteger()" >
              </div>
            </td>
          </tr>
         <% } %> 
    <% } %> 

	<%if(refCodes.getETG00FMTO().trim().equals("5")){ %>
          <tr id="trdark"> 
            <td nowrap width="20%" > 
              <div align="right"><%= refCodes.getETG00NOMCD().trim()%>&ensp;:&ensp;</div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <%if(refCodes.getETG00TCOD().equals("N")){%>
	                <input type="text" name="ETG05ELE" size="9" maxlength="8" value="<% if(msgF5.getETG05ELE().equals("")) out.print("0"); else  out.print(Integer.valueOf(msgF5.getETG05ELE().trim()));%>" <%=readonly%> style="text-align:right;" onKeyPress="enterInteger()">
                <%}else{%>
	                <input type="text" name="ETG05ELE" size="9" maxlength="8" value="<%= msgF5.getETG05ELE().trim()%>" <%=readonly%>>
                <%}%>
              </div>
            </td>
          </tr>

          <% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM01()%> :</div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="ETG05DESCL" size="29" maxlength="28" value="<%= msgF5.getETG05DESCL().trim()%>"  >
              </div>
            </td>
          </tr>
          <% } %> 

          <% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM02()%> :</div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="ETG05DESCC" size="13" maxlength="12" value="<%= msgF5.getETG05DESCC().trim()%>"  >
              </div>
            </td>
          </tr>
          <% } %> 

          <% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM03()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG05ALFA1" size="3" maxlength="2" value="<%= msgF5.getETG05ALFA1().trim()%>"  >
              </div>
            </td>
          </tr>
          <% } %> 

          <% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
          <tr id="trclear">             <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM04()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG05ALFA2" size="5" maxlength="4" value="<%= msgF5.getETG05ALFA2().trim()%>"  >
              </div>
            </td>
          </tr>
         <% } %> 
          
          <% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM05()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG05ALFA3" size="5" maxlength="4" value="<%= msgF5.getETG05ALFA3().trim()%>"  >
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM06()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG05VAL01" size="6" maxlength="5" value="<%= msgF5.getETG05VAL01().trim()%>" style="text-align:right;" onKeyPress="enterInteger()" >
              </div>
            </td>
          </tr>
          <% } %> 

          <% if (refCodes.getETG00FLD07().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM07()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG05VAL02" size="6" maxlength="5" value="<%= msgF5.getETG05VAL02().trim()%>" style="text-align:right;" onKeyPress="enterInteger()">
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD08().trim().equals("Y")){ %>
         <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM08()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG05VAL03" size="23" maxlength="22" value="<%= msgF5.getETG05VAL03().trim()%>" style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
          </tr>
         <% } %> 

         <% if (refCodes.getETG00FLD09().trim().equals("Y")){ %>
         <tr id="trclear"> 
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM09()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG05VAL04" size="4" maxlength="3" value="<%= msgF5.getETG05VAL04().trim()%>" style="text-align:right;" onKeyPress="enterInteger()" >
              </div>
            </td>
          </tr>
         <% } %> 
          
      <% } %> 

		<%if(refCodes.getETG00FMTO().trim().equals("6")){ %>
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right"><%= refCodes.getETG00NOMCD().trim()%>&ensp;:&ensp;</div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <%if(refCodes.getETG00TCOD().equals("N")){%>
	                <input type="text" name="ETG06ELE" size="9" maxlength="8" value="<% if(msgF6.getETG06ELE().equals("")) out.print("0"); else  out.print(Integer.valueOf(msgF6.getETG06ELE().trim()));%>" <%=readonly%> style="text-align:right;" onKeyPress="enterInteger()">
                <%}else{%>
	                <input type="text" name="ETG06ELE" size="9" maxlength="8" value="<%= msgF6.getETG06ELE().trim()%>" <%=readonly%>>
                <%}%>
              </div>
            </td>
          </tr>

          <% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM01()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG06VAL01"  size="11" maxlength="10" value="<%= msgF6.getETG06VAL01().trim()%>"  style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM02()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG06VAL02" size="14" maxlength="13" value="<%= msgF6.getETG06VAL02().trim()%>"  style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM03()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG06VAL03" size="14" maxlength="13" value="<%= msgF6.getETG06VAL03().trim()%>" style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM04()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG06VAL04" size="14" maxlength="13" value="<%= msgF6.getETG06VAL04().trim()%>"  style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
          </tr>
         <% } %> 

         <% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM05()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG06VAL05" size="13" maxlength="12" value="<%= msgF6.getETG06VAL05().trim()%>" style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM06()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG06VAL06" size="13" maxlength="12" value="<%= msgF6.getETG06VAL06().trim()%>" style="text-align:right;" onKeyPress="enterDecimal()" >
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD07().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM07()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG06VAL07" size="13" maxlength="12" value="<%= msgF6.getETG06VAL07().trim()%>" style="text-align:right;" onKeyPress="enterInteger()" >
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD08().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM08()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG06ALFA1" size="12" maxlength="11" value="<%= msgF6.getETG06ALFA1().trim()%>">
              </div>
            </td>
          </tr>
         <% } %> 
      <% } %> 

		<%if(refCodes.getETG00FMTO().trim().equals("7")){ %>
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right"><%= refCodes.getETG00NOMCD().trim()%>&ensp;:&ensp;</div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <%if(refCodes.getETG00TCOD().equals("N")){%>
	                <input type="text" name="ETG07ELE" size="9" maxlength="8" value="<% if(msgF7.getETG07ELE().equals("")) out.print("0"); else  out.print(Integer.valueOf(msgF7.getETG07ELE().trim()));%>" <%=readonly%> style="text-align:right;" onKeyPress="enterInteger()">
                <%}else{%>
	                <input type="text" name="ETG07ELE" size="9" maxlength="8" value="<%= msgF7.getETG07ELE().trim()%>" <%=readonly%>>
                <%}%>
              </div>
            </td>
          </tr>

          <% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM01()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG07DESCC" size="33" maxlength="32" value="<%= msgF7.getETG07DESCC().trim()%>"  >
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM02()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG07CAM01" size="5" maxlength="4" value="<%= msgF7.getETG07CAM01().trim()%>"  >
              </div>
            </td>
          </tr>
         <% } %> 
          
          <% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
          <tr id="trclear">  
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM03()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG07VAL01" size="13" maxlength="12" value="<%= msgF7.getETG07VAL01().trim()%>" style="text-align:right;"  onKeyPress="enterInteger()"  >
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right"><%= refCodes.getETG00NOM04()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG07VAL02" size="17" maxlength="16" value="<%= msgF7.getETG07VAL02().trim()%>" style="text-align:right;"  onKeyPress="enterInteger()"  >
              </div>
            </td>
          </tr>
         <% } %> 

          <% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right"><%= refCodes.getETG00NOM05()%> :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG07VAL03" size="17" maxlength="16" value="<%= msgF7.getETG07VAL03().trim()%>"  style="text-align:right;"  onKeyPress="enterInteger()">
              </div>
            </td>
          </tr>
         <% } %> 
      <% } %> 

		</table>
		</td>
	</tr>
  </table>
  
    <div align="center">
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel()">
  </div>
  
</form>

</body>
</html>
