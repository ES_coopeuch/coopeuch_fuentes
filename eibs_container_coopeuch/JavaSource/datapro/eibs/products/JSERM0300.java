package datapro.eibs.products;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERM029001Message;
import datapro.eibs.beans.ERM030001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

/**
 * Servlet implementation class JSERM0300
 */
public class JSERM0300 extends JSEIBSServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */	
	
	protected static final int A_enter_capitalizacion=100;
	protected static final int reps=200;
	protected static final int capPRocess=300;
	
			protected void processRequest(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res,
					HttpSession session, int screen) throws ServletException,
					IOException {
			
				switch (screen) {
				case A_enter_capitalizacion:
					procReqCapitalizacion(user, req, res, session);
					break;
				case reps:
					
					CapProcess(user, req, res, session);
					break;
				case capPRocess:
					CapResponse(user, req, res, session);
					
					break;
					
					
				default:
					forward(SuperServlet.devPage, req, res);
					break;
				}
				
				
			}
	
	
	
			protected void procReqCapitalizacion(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession ses)
					throws ServletException, IOException {
				UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
				ses.setAttribute("userPO", userPO);
				try {
					flexLog("About to call Page: ERM0300_enter_capitalizacion.jsp");
					forward("ERM0300_enter_capitalizacion.jsp", req, res);
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Exception calling page " + e);
				}
			}
			
			
			protected void CapProcess(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					mp = getMessageProcessor("ERM0300", req);

					ERM030001Message msgList = (ERM030001Message) mp.getMessageRecord("ERM030001", user.getH01USR(), "0001");
					//Sets the employee  number from either the first page or the maintenance page
			
					if (req.getParameter("E01RMMCUN")!= null){
										
				
						msgList.setE01RMMCUN(req.getParameter("E01RMMCUN"));
						
						
					
					} 

					//Sends message
					
					flexLog("ERM030001: "+msgList);
					
					mp.sendMessage(msgList);
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
					msgList = (ERM030001Message) mp.receiveMessageRecord();
				
				
					session.setAttribute("RteList", msgList);
					
					if (mp.hasError(error)) { // if there are errors go back to first page
						
				
						session.setAttribute("error", error);
						
						flexLog("About to call Page: ERM0300_enter_capitalizacion.jsp");
						forward("ERM0300_enter_capitalizacion.jsp", req, res);
					} else {
					
						forward("ERM0300_capitalizacion_detalle.jsp", req, res);

				
					}


				} finally {
					if (mp != null)
						mp.close();
				}
			}
	
	
	
			
			
			
			protected void CapResponse(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {					
					mp = getMessageProcessor("ERM0300", req);
					ERM030001Message msg = (ERM030001Message) mp.getMessageRecord("ERM030001", user.getH01USR(), "0002");
					//Sets the employee  number from either the first page or the maintenance page
			
					setMessageRecord(req, msg);
					
					if(req.getParameter("E01PAGOPC")!=null){
						
						msg.setE01OFFOPC(req.getParameter("E01PAGOPC"));
						
					}
					
					
					if(req.getParameter("E01PAGCON")!=null){
						
						
					msg.setE01OFFCON(req.getParameter("E01PAGCON"));
					
						
					}
					
					if(req.getParameter("E01PAGOBK")!=null){
						
						msg.setE01OFFBNK(req.getParameter("E01PAGOBK"));
						
					}
					
					
					
					if(req.getParameter("E01PAGOGL")!=null){
						
						msg.setE01OFFGLN(req.getParameter("E01PAGOGL"));
					}
					
				
					if(req.getParameter("E01PAGOBR")!=null){
						
						msg.setE01OFFBRN(req.getParameter("E01PAGOBR"));
					}
					
					
					if(req.getParameter("E01PAGOCY")!=null){
						
						msg.setE01OFFCCY(req.getParameter("E01PAGOCY"));
					}
					
					if(req.getParameter("E01PAGOAC")!=null){
						
						msg.setE01OFFREF(req.getParameter("E01PAGOAC"));
						
					}
					
					session.removeAttribute("RteList");
					session.setAttribute("RteList",msg);
					
					
					flexLog("mensaje ida"+msg);
					
					mp.sendMessage(msg);
					
					//session.removeAttribute("RteList");
				
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();		
					
					msg = (ERM030001Message) mp.receiveMessageRecord();
					
					
					session.setAttribute("RteList", msg);
					if (mp.hasError(error)) { // if there are errors go back to first page
						
						session.setAttribute("error", error);
						flexLog("About to call Page: ERM0300_capitalizacion_detalle.jsp");
						forward("ERM0300_capitalizacion_detalle.jsp", req, res);
					} else {
				
						session.removeAttribute("RteList");
						redirectToPage("/servlet/datapro.eibs.products.JSERM0300?SCREEN=100", res);
					    
					} 


				} finally {
					if (mp != null)
						mp.close();
				}
			}

			
			
			
}
