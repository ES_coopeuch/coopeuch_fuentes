// CrossBrowserFunctions.js

// newFunction
function EventUtils() {

   throw 'RuntimeException: EventUtils is a static utility class ' +
            ' and may not be instantiated';

}

 EventUtils.addEventListener = function (target,type,callback,captures) {
	    if (target.addEventListener) {
                // EOMB
	        target.addEventListener(type,callback,captures);
	    } else if (target.attachEvent) {
	        // IE
	        target.attachEvent('on'+type,callback,captures);
	    } else {
	    	// IE 5 Mac and some others
	    	target['on'+type] = callback;
	    }
	};


	  function hidediv(id) { 
		   if (document.getElementById) { // DOM3 = IE5, NS6 
		    document.getElementById(id).style.display = 'none'; 
		   } 
		   else { 
		    if (document.layers) { // Netscape 4 
		      document.layers[id].display = 'none'; 
		    } 
		    else { // IE 4 
		      eval( "document.all." + id + ".style.display = 'none'");  
		    } 
		   } 
		  } 

		  function showdiv(id) { 
		    if (document.getElementById) { // DOM3 = IE5, NS6 
		      document.getElementById(id).style.display= 'block'; 
		    } 
		    else { 
		      if (document.layers) { // Netscape 4 
		        document.layers[id].display = 'block'; 
		      } 
		      else { // IE 4 
		        eval( "document.all." + id + ".style.display = 'block'");   
		      } 
		    } 
		   } 
