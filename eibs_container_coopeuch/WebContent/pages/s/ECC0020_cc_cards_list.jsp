<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<HTML>
<HEAD>
<TITLE>
Tarjetas de Creditos
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ECC0040Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">

function typeClick(value) {
		document.forms[0].OPT.value = value;		
}

function goAction() {
 	var page = "";
   	var row = parseInt(document.forms[0].actRow.value); 
 	params = "&OPT="+document.forms[0].OPT.value+"&CURRCODE=" + row;
		if (document.forms[0].OPT.value == "")
		 	{ 	    
				alert("Debe Seleccionar un Tipo de Solicitud");
 	    	}    
 	   else
 	       {
 			page = webapp + "/servlet/datapro.eibs.products.JSECC0020?SCREEN=400" + params;		
			CenterWindow(page,800,600,2);	
          //  document.forms[0].actRow.value = row;  		
	 	  //  document.forms[0].submit(); 	    
 	       }
  }
function setInfo(idx){  
   document.forms[0].actRow.value = idx;     
   var tipo = document.getElementById('Tipo'+idx).value
   if (tipo=='C'){
   		document.forms[0].Type[0].disabled = false;
   		document.forms[0].Type[1].disabled = false;
   		document.forms[0].Type[2].disabled = false;
   		document.forms[0].Type[3].disabled = false;
   		document.forms[0].Type[4].disabled = false;
   		document.forms[0].Type[5].disabled = false;
   		document.forms[0].Type[6].disabled = false;
   		document.forms[0].Type[7].disabled = false;
   		document.forms[0].Type[8].disabled = false;
   		document.forms[0].Type[9].disabled = false;
   		document.forms[0].Type[10].disabled = false;  
   		document.forms[0].Type[11].disabled = false;     		    		
   }else{
   		document.forms[0].Type[0].disabled = false;
   		document.forms[0].Type[1].disabled = false;
   		document.forms[0].Type[2].disabled = true;
   		document.forms[0].Type[3].disabled = false;
   		document.forms[0].Type[4].disabled = false;
   		document.forms[0].Type[5].disabled = true;
   		document.forms[0].Type[6].disabled = true;
   		document.forms[0].Type[7].disabled = false;
   		document.forms[0].Type[8].disabled = true;
   		document.forms[0].Type[9].disabled = false;
   		document.forms[0].Type[10].disabled = true;   		
   		document.forms[0].Type[11].disabled = true;   		
   }
}  

</script>
</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }

%> 
<h3 align="center">Plataforma PosVenta Tarjetas<BR>Ingreso de Solicitudes <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cc_cards_list.jsp , ECC0020"></h3>
<hr size="4">
<FORM Method="post" Action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0020" id="form1" >
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="400">
<INPUT TYPE=HIDDEN NAME="CURRCODE" VALUE="0"> 
 <INPUT TYPE=HIDDEN NAME="actRow" VALUE="0">
 <INPUT TYPE=HIDDEN NAME="OPT" VALUE=""> 
<TABLE class="tableinfo" width=100%>
     <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trclear"> 
              <td nowrap width="16%"> 
              <div align="right"><b>Identificaci&oacute;n :</b></div>
              </td>
              <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCRCID" size="15" maxlength="15" value="<%= userPO.getIdentifier() %>" readonly>
               </div>
               </td>
               <td nowrap width="16%" > 
               <div align="right"><b>Cliente :</b></div>
                </td>
                <td nowrap width="20%" > 
                <div align="left"><b> 
                 <input type="text" readonly name="E01CCRCUN" size="10" maxlength="9" value="<%= userPO.getCusNum().trim()%>" >
                 </b> </div>
                 </td>
                 <td nowrap width="16%" > 
                 <div align="right"><b>Nombre :</b> </div>
                 </td>
                 <td nowrap > 
                 <div align="left"><font face="Arial"><font face="Arial"><font size="2"> 
                 <input type="text" name="E01CUSNA1" size="45" maxlength="45" readonly value="<%= userPO.getCusName().trim()%>">
                 </font></font></font></div>
                 </td>
                </tr>
              </table>
            </td>
          </tr>
 </table>      
 <%
	if (ECC0040Help.getNoResult()){
 		%> 
          <p><font size="3"><b>Cliente no tiene tarjetas de Creditos </b></font></p>
          <table class="tbenter" width="100%">
            <tr> 
		      <TD class=TDBKG> 
		        <div align="center"><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
		      </TD>            
            </tr>
          </table>          			          
 <%   		
	} else {
 %>        			          

<H4>Tarjetas</H4>
	<table class="tableinfo" id="dataTable" width="100%">
        <tr id="trclear"> 
              <TH align=CENTER nowrap width="10%">&nbsp;</TH>
              <TH align=CENTER nowrap width="25%">Numero</TH>
              <TH align=CENTER nowrap width="25%">Tipo </TH>
              <TH align=CENTER nowrap width="10%">Estado</TH>
              <TH align=CENTER nowrap width="10%">Bloqueo</TH>              
              <TH align=CENTER nowrap width="10%">Titular<br>Adicional</TH> 
              <TH align=CENTER nowrap width="10%">Fecha<br>Expiracion</TH>            
            </tr>
            <%
               ECC0040Help.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (ECC0040Help.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  datapro.eibs.beans.ECC004001Message msgList = (datapro.eibs.beans.ECC004001Message) ECC0040Help.getRecord();
		 %>
			<tr id="trdark">
            <td NOWRAP  align=CENTER> 
              <input type="radio" name="CURRCODE" value="<%= ECC0040Help.getCurrentRow() %>"  <%=chk%> onClick="setInfo(<%= ECC0040Help.getCurrentRow()%>)" >
              <input type="hidden" name="Tipo<%= ECC0040Help.getCurrentRow()%>" id="Tipo<%= ECC0040Help.getCurrentRow()%>" value="<%= msgList.getE01CCRTDC() %>" >              
             </td>											
	      <TD ALIGN=LEFT NOWRAP><%= Util.formatCell(msgList.getE01CCRNUM()) %></TD>
	      <TD ALIGN=CENTER NOWRAP><%= msgList.getD01CCMPRO() %></TD>
   	      <TD ALIGN=CENTER NOWRAP> <%=msgList.getD01CCRSTS()%></TD>
   	      <TD ALIGN=CENTER NOWRAP> <%=msgList.getD01CCRLKC()%></TD>   	      
	      <TD ALIGN=CENTER NOWRAP><% if(msgList.getE01CCRTPI().equals("T")) out.print("Titular"); else out.print("Adicional"); %></TD>
	      <TD ALIGN=CENTER NOWRAP><%= msgList.getE01CCREXM()%>/<%= msgList.getE01CCREXY()%></TD>
		</tr>
		<%
			}
		%>
	</table>		        
     
   <table  id="TBHELP" width="100%" cellspacing="0" cellpadding="0">
   <tr> 
        <td id="THHELP">&nbsp;</td>
     </tr>
      <tr> 
        <td id="THHELP">&nbsp;</td>
     </tr>
     <tr> 
        <td id="THHELP">&nbsp;</td>
     </tr>
     <tr> 
        <td id="THHELP">&nbsp;</td>
     </tr>
     <tr> 
        <th id="THHELP"><H4><b>Tipo de Solicitud</b></H4></th>
     </tr>
     <tr> 
       <td> 
        <input type="radio" name="Type" value="1" onclick="typeClick('1')"><b>ACTIVACION TARJETA</b></td>
       <td> 
        <input type="radio" name="Type" value="2" onclick="typeClick('2')"><b>INGRESO ADICIONALES</b></td>        
     </tr>
     <tr> 
       <td> 
        <input type="radio" name="Type" value="3" onclick="typeClick('3')"><b>CAMBIO CUPO</b></td>
       <td> 
        <input type="radio" name="Type" value="4" onclick="typeClick('4')"><b>BLOQUEOS/DESBLOQUEOS</b></td>       
      </tr>
      <tr> 
       <td> 
        <input type="radio" name="Type" value="5" onclick="typeClick('5')"><b>REEMISION TARJETA TITULAR</b></td>
       <td> 
        <input type="radio" name="Type" value="6" onclick="typeClick('6')"><b>CAMBIO CODIGO VENCIMIENTO</b></td>       

       </tr>    
       <tr> 
       <td> 
        <input type="radio" name="Type" value="7" onclick="typeClick('7')"><b>CAMBIO PAGO MINIMO/PAC MULTIBANCO</b></td>
       <td> 
        <input type="radio" name="Type" value="8" onclick="typeClick('8')"><b>RESETEO DE CLAVE</b></td>
       </tr> 
       <tr> 
       <td> 
        <input type="radio" name="Type" value="9" onclick="typeClick('9')"><b>DIRECCION ENVIO ESTADO CUENTA</b></td>
	    <td >
		    <input type="radio" name="Type" value="10" onclick="typeClick('10')"><b>ELIMINACION TARJETA</b>
	    </TD>
       </tr>
       <tr> 
       <td>
       		<input type="radio" name="Type" value="11" onclick="typeClick('11')"><b>RENUNCIA TARJETA DE CREDITO</b>	
       </td>
	    <td >
			<input type="radio" name="Type" value="12" onclick="typeClick('12')"><b> ACTIVACION DE RENOVACION TARJETA DE CREDITO</b>	    
	    </TD>
       </tr>                
     </table> 
     <tr> 
     <td id="THHELP">&nbsp;</td>
     </tr>
      <tr> 
        <td id="THHELP">&nbsp;</td>
     </tr>
  <div align="center"> 
      <input id="EIBSBTN" type=button name="Submit" value="Enviar" onClick="javascript:goAction()">	   
  </div>  
  <%
  }
%> 

<SCRIPT language="JavaScript">
  showChecked("CURRCODE");
</SCRIPT>

</FORM>

</BODY>


</HTML>

