<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV100501Message"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV100506Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV100501List" class="datapro.eibs.beans.JBObjList"
	scope="session" />
<jsp:useBean id="EPV100506List" class="datapro.eibs.beans.JBObjList"
	scope="session" />	
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">

  function goAction(op) {
	var ok = false;
	var cun = "";
	var pg = "";

	if (op != '200'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01PVMNUM") 
	      	{	
	      		if (element.checked == true) {
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	if (op == '301' && !confirm('Se Dispone a Rechazar la Solicitud Seleccionada, Desea Contiinuar?')){
      		return;
      	}
        document.forms[0].E06CODCAM.value="";   
		document.forms[0].SCREEN.value = op;
		document.forms[0].submit();
     } else {
		alert("Debe seleccionar una Solicitud para continuar.");	   
	 }
      
	}
	
 	function goAction2(op, cusnum) 
  	{
	    document.forms[0].E06CODCAM.value="ACTIVA"; 
		document.forms[0].SCREEN.value = op;
		document.forms[0].E06PCACUN.value = cusnum;
		document.forms[0].submit();		
      
	}	

</script>

</head>

<body>
<% 

boolean warnig = false;
 if ( !error.getERRNUM().equals("0")  ) {
     if ("Y".equals(error.getERWRNG()))warnig=true; 
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");     
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center"> Plataforma de Ventas - Ingreso<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_list.jsp,JSEPV1005"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1005">
<input type="hidden" name="SCREEN" value=""> 
<input type="hidden" name="WARNING" value="<%=(warnig?"A":"") %>">
<input type="hidden" name="E06PCACUN" value=""> 
<input type="hidden" name="E06CODCAM" value="">
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Numero: 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">RUT:  
             </td>
             <td nowrap width="10%" align="left">            
	  			<eibsinput:text name="userPO" property="cusType" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>  			
             </td>
             <td nowrap width="10%" align="right"> Nombre: 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="userPO" property="cusName" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table> 
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="15%"><a href="#"
			onclick="goAction('200')"> <b>Crear</b> </a></td>
		<td align="center" class="tdbkg" width="15%"><a
			href="javascript:goAction('201')"> <b>Modificar</b> </a>
		</td>
		<td align="center" class="tdbkg" width="15%"><a
			href="javascript:goAction('301')"> <b>Rechazar</b> </a>
		</td>		
		<td align="center" class="tdbkg" width="15%">
			<a	href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>

<%
	if (EPV100501List.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=50%>
	<TR>
		<TD>
		<div align="center">
		<font size="3"><b> 
			No hay resultados que correspondan a su criterio de b�squeda. 
		</b></font></div>
		</TD>
	</TR>
</TABLE>
<%
	} else {
%>

	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="5%">&nbsp;</th>
			<th align="center" nowrap width="15%">Solicitud</th>
			<th align="center" nowrap width="20%">Medio de Evaluaci�n</th>
			<th align="center" nowrap width="20%">Producto/Descripcion</th>
			<th align="center" nowrap width="20%">Canal de Venta</th>						
			<th align="center" nowrap width="10%">Fecha Ingreso</th>			
			<th align="center" nowrap width="10%">Estado</th>
		</tr>
		<%
			EPV100501List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EPV100501List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";				
					}
					EPV100501Message convObj = (EPV100501Message) EPV100501List.getRecord();
		%>
		<tr>
			<td nowrap><input type="radio" name="E01PVMNUM"	value="<%=convObj.getE01PVMNUM()%>" <%=chk%>/></td>
			<td nowrap align="center"><%=Util.formatCell(convObj.getE01PVMNUM())%></td>
			<td nowrap align="left"><%=convObj.getE01DSCEVA()%></td>
			<td nowrap align="left"><%=convObj.getE01DSPROD()%></td>			
			<td nowrap align="left"><%=convObj.getE01DSCSLC()%></td>
			<td nowrap align="center"><%=Util.formatCell(convObj.getE01PVMOPD() + "/" + convObj.getE01PVMOPM() + "/"+ convObj.getE01PVMOPY())%> </td>
			<td nowrap align="center"><%=convObj.getE01DSCSTS()%></td>
		</tr>
		<%
			}
		%>
	</table>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV100501List.getShowPrev()) {
					int pos = EPV100501List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1005?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV100501List.getShowNext()) {
					int pos = EPV100501List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1005?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>


<%
	if (!EPV100506List.getNoResult()) {
%>

<h3 align="center">Lista de Campa�as</h3>


 
  <table  class="tbenter" width="100%">
  	<tr>
  		<td width="50%">
			<table class="tbenter" width="100%">
				<tr>	
					<td align="center" class="tdbkg" width="50%">
					<a href="javascript:goAction2('200', '<%= userPO.getCusNum() %>')"> <b>Crear</b> </a></td>
				</tr>
			</table>  		
  		</td>  
  			<td width="50%"  >
  			&nbsp;
  		</td>		
  	</tr>
  </table>  
	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="5%">&nbsp;</th>
			<th align="center" nowrap width="10%">Campa�a</th>
			<th align="center" nowrap width="25%">Medio de Evaluaci�n</th>			
			<th align="center" nowrap width="30%">Fecha Vigencia</th>
			<th align="center" nowrap width="15%">Tipo Campa�a</th>
			<th align="center" nowrap width="15%">Monto</th>			
		</tr>
		<%
			EPV100506List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EPV100506List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";				
					}
					EPV100506Message convObjCam = (EPV100506Message) EPV100506List.getRecord();
		%>
		<tr>
			<td nowrap><input type="radio" name="CURRCODE" value="<%= EPV100506List.getCurrentRow() %> " <%=chk%> /></td>
			<td nowrap align="center"><%=convObjCam.getE06PCACCA()%></td>
			<td nowrap align="left"><%=convObjCam.getE06DESTEV()%></td>			
			<td nowrap align="center"><%=Util.formatCell(convObjCam.getE06PCAVHD() + "/" + convObjCam.getE06PCAVHM() + "/"+ convObjCam.getE06PCAVHY())%> </td>
			<td nowrap align="right"><%=convObjCam.getE06DESTYP()%></td>
			<td nowrap align="right"><%=convObjCam.getE06PCAAMT()%></td>
		</tr>
		<%
			}
		%>
	</table>
<% } %>
 
</form>

</body>
</html>
