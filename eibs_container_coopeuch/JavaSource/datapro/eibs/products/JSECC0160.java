package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Creado    por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  27/11/2012                                                                                      **/
/**  Objetivo                :  Aprovacion de solicitudes Pos Venta de Tarjetas                                                 **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/

import java.io.*;
import java.net.*;
import java.beans.Beans;
import javax.servlet.*;
import javax.servlet.http.*;

import datapro.eibs.beans.*;

import datapro.eibs.master.Util;
 
import datapro.eibs.sockets.*;
import java.util.Hashtable;
import java.util.ArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.PropertyResourceBundle;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generic.beanutil.BeanList;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;

import datapro.eibs.beans.DataNavTotals;
import datapro.eibs.beans.ECIF03001Message;
import datapro.eibs.beans.ECIF03002Message;
import datapro.eibs.beans.EDD009001Message;
import datapro.eibs.beans.EGL042101Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.sockets.DecimalField;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

import datapro.eibs.generic.SimpleAthenticator;
public class JSECC0160 extends datapro.eibs.master.SuperServlet {

	protected static final int R_ENTER_SEARCH_AP = 100;
	protected static final int R_LIST_APROB      = 200;	
	protected static final int A_LIST            = 300;
	private String LangPath = "S";

/**
 *  constructor comment.
 */
public JSECC0160() {
	super();
}
/**
 * 
 */
public void destroy() {

	flexLog("free resources used by JSECC0160");
	
}
/**
 * 
 */
public void init(ServletConfig config) throws ServletException {
	super.init(config);
}

protected void procSearchApp(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		ECC016001Message msgRT = null;
		UserPos userPO = null;
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		userPO.setPurpose(" ");
		msgRT = new datapro.eibs.beans.ECC016001Message();
		msgRT.setE01SELISD(user.getBigDecimalE01RDD());
		msgRT.setE01SELISM(user.getBigDecimalE01RDM());
		msgRT.setE01SELISY(user.getBigDecimalE01RDY());
		
		userPO.setHeader1(msgRT.getE01SELISD());
		userPO.setHeader2(msgRT.getE01SELISM());
		userPO.setHeader3(msgRT.getE01SELISY());
		ses.setAttribute("userPO", userPO);
		ses.setAttribute("ccPvta", msgRT);
		try {
			flexLog(
				"About to call Page: "
					+ LangPath
					+ "ECC0160_cc_posventa_approval_enter_search.jsp");
			callPage(LangPath + "ECC0160_cc_posventa_approval_enter_search.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}
protected void procReqListAprob(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECC016001Message msgList = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError =
				(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
					getClass().getClassLoader(),
					"datapro.eibs.beans.ELEERRMessage");
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		// Send Initial data
		try {
			msgList = (ECC016001Message) mc.getMessageRecord("ECC016001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ECC0160");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			msgList.setH01OPECOD("0015");
			msgList.setE01SELINB(userPO.getBranch());
			msgList.setE01SELISD(userPO.getHeader1());
			msgList.setE01SELISM(userPO.getHeader2());
			msgList.setE01SELISY(userPO.getHeader3());

			// all the fields here
			java.util.Enumeration enu = msgList.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}
			flexLog("mensaje enviado ..." + msgList);
			msgList.send();
			msgList.destroy();
			flexLog("ECC016001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError =
						(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
							getClass().getClassLoader(),
							"datapro.eibs.beans.ELEERRMessage");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else {
				flexLog("Message " + newmessage.getFormatName() + " received.");				
			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e + newmessage);
			throw new RuntimeException("Socket Communication Error Receiving");
		}

		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECC016001")) {

				JBObjList beanList = new JBObjList();

				boolean firstTime = true;
				String marker = "";
				String chk = "";

				while (true) {

					msgList = (ECC016001Message) newmessage;

					marker = msgList.getH01FLGMAS();
					if (firstTime) {
						firstTime = false;
						chk = "checked";

					} else {
						chk = "";
					}

					if (marker.equals("*")) {
						beanList.setShowNext(false);
						break;
					} else {
						beanList.addRow(msgList);

						if (marker.equals("+")) {
							beanList.setShowNext(true);

							break;
						}
					}
					newmessage = mc.receiveMessage();
				}

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("ECC0160Help", beanList);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog(
							"About to call Page: "
								+ LangPath
								+ "ECC0160_cc_posventa_approval_list.jsp");
						callPage(
							LangPath + "ECC0160_cc_posventa_approval_list.jsp",
							req,
							res);					
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				
				} else {
					try {
						flexLog("About to call Page: " + LangPath + "ECC0160_cc_posventa_approval_enter_search.jsp");
						callPage(LangPath + "ECC0160_cc_posventa_approval_enter_search.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}				

			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Data Receiving");
		}

	}

protected void procActionPos(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
 
		int inptOPT = 0;

		inptOPT = Integer.parseInt(req.getParameter("OPT"));
		switch (inptOPT) {

			case 1 : //Consulta
				procReqView(mc, user, req, res, ses);
				break;
			case 5 : //Aprobar
				procActionAprob(mc, user, req, res, ses, "AP");
				break;
			case 6 : //Rechazar
				procActionAprob(mc, user, req, res, ses, "RE" );
				break;			
			default :
				res.sendRedirect(
					super.srctx
						+ "/servlet/datapro.eibs.products.JSECC0160?SCREEN=100");
				break;
		}
	}

protected void procActionAprob(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses,
		String Option)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECC016001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		// Send Initial data
		try {
			flexLog("Send Initial Data");
			JBObjList bl = (JBObjList) ses.getAttribute("ECC0160Help");
			int idx = Integer.parseInt(req.getParameter("CURRCODE"));
			bl.setCurrentRow(idx);
			msgRT = (ECC016001Message) bl.getRecord();
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("ECC016001");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD(Option);
			msgRT.setH01OPECOD("0020");


			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}
            flexLog("Mensaje enviado..." + msgRT);
			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("ECC016001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECC016001")) {
				try {
					msgRT = new datapro.eibs.beans.ECC016001Message();
					flexLog("ECC016001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ECC016001Message) newmessage;
				ses.setAttribute("error", msgError);
				ses.setAttribute("ccPvta", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					res.sendRedirect(super.srctx + "/servlet/datapro.eibs.products.JSECC0160?SCREEN=200");
					flexLog(super.srctx + "/servlet/datapro.eibs.products.JSECC0160?SCREEN=200");
				} else { // There are errors
					try {
						flexLog(
							"About to call Page: "
								+ LangPath
								+ "ECC0160_cc_posventa_approval_list.jsp");
						callPage(
							LangPath + "ECC0160_cc_posventa_approval_list.jsp",
							req,
							res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

protected void procReqView(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		ECC016001Message msgDoc = null;
		UserPos userPO = null;

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Receive Data
		try {
			JBObjList bl = (JBObjList) ses.getAttribute("ECC0160Help");
			int idx = Integer.parseInt(req.getParameter("COL"));
			bl.setCurrentRow(idx);

			msgDoc = (ECC016001Message) bl.getRecord();

			String option=(msgDoc.getE01SPVTCD()).substring(3);		
			userPO.setOption(option);
			
			flexLog("option..." + option);			
			ses.setAttribute("userPO", userPO);
			

			try {
				 if (msgDoc.getE01SPVTDC().equals("C")) 
				 {
					 ses.setAttribute("ccPvta", msgDoc);
					flexLog(
							"About to call Page: "
								+ LangPath
								+ "ECC0160_cc_posventa_approval_inq.jsp");
					callPage(
							LangPath + "ECC0160_cc_posventa_approval_inq.jsp",
							req,
							res);					 
				 } else
				 {
					 ses.setAttribute("dcPvta", msgDoc);
					flexLog(
							"About to call Page: "
								+ LangPath
								+ "ECC0160_dc_posventa_approval_inq.jsp");
					callPage(
							LangPath + "ECC0160_dc_posventa_approval_inq.jsp",
							req,
							res);					 
				 }

			} catch (Exception e) {
				flexLog("Exception calling page " + e);
			}

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}
/**
 * This method was created in VisualAge.
 */

public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
   
	Socket s = null;
	MessageContext mc = null;

	ESS0030DSMessage msgUser = null;
  	HttpSession session = null;

	session = (HttpSession)req.getSession(false); 
	
	if (session == null) {
		try {
			res.setContentType("text/html");
			printLogInAgain(res.getWriter());
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception ocurred. Exception = " + e);
		}
	}
	else {

		int screen = R_ENTER_SEARCH_AP ;
		
		try {
		
			msgUser = (datapro.eibs.beans.ESS0030DSMessage)session.getAttribute("currUser");

			// Here we should get the path from the user profile
			LangPath = super.rootPath + msgUser.getE01LAN() + "/";

			try
			{
				flexLog("Opennig Socket Connection");
				s = new Socket(super.hostIP, getInitSocket(req) + 1);
				s.setSoTimeout(super.sckTimeOut);
			  	mc = new MessageContext(new DataInputStream(new BufferedInputStream(s.getInputStream())),
							      	    new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
									    "datapro.eibs.beans");
			
			try {
				screen = Integer.parseInt(req.getParameter("SCREEN"));
			}
			catch (Exception e) {
				flexLog("Screen set to default value");
			}
		    flexLog("Entre con Screen = " + screen);
			switch (screen) {
	
			case R_ENTER_SEARCH_AP :
				procSearchApp(mc, msgUser, req, res, session);
				break;
			case R_LIST_APROB :
				procReqListAprob(mc, msgUser, req, res, session);
				break;				
			case A_LIST :
				procActionPos(mc, msgUser, req, res, session);
				break;
			
			default :
					res.sendRedirect(super.srctx +LangPath + super.devPage);
					break;
			}
			}
			catch (Exception e) {
				e.printStackTrace();
				int sck = getInitSocket(req) + 1;
				flexLog("Socket not Open(Port " + sck + "). Error: " + e);
				res.sendRedirect(super.srctx +LangPath + super.sckNotOpenPage);
			//	return;
			}
			finally {
				s.close();
			}
			

		}
		catch (Exception e) {
			flexLog("Error: " + e);
			res.sendRedirect(super.srctx +LangPath + super.sckNotRespondPage);
		}
		
	}
	
}
protected void showERROR(ELEERRMessage m) {
	if (logType != NONE) {

		flexLog("ERROR received.");

		flexLog("ERROR number:" + m.getERRNUM());
		flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
		flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
		flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
		flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
		flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
		flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
		flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
		flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
		flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
		flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());
	}
}

}