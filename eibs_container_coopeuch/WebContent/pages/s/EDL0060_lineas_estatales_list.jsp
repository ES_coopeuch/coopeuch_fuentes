<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import = "datapro.eibs.beans.EDL006001Message" %>

<html>
<head>
<title>Mantenedor de L&iacute;neas Estatales MYPE</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "listLinEst" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
<script language="JavaScript">
function goAction(op) {

	document.forms[0].opt.value = op;
	
	if(op==2) //MAINT
	{
		var indice = document.form1.CURRCODEL.selectedIndex; 
   		for(n=0; n<document.forms[0].elements.length; n++)
	  	{
	     var element = document.forms[0].elements[n];
	     if(element.name == "CURRCODEL") 
	     {	
	        if (element.checked == true) {
	      	   var indice = element.value; 
        	   break;
			}
	      }
	   	}
	 
		var id="E01MLNSTS" + indice; 
		var estado = document.getElementById(id);
		if(estado.value=="A" || estado.value=="I" )
			document.forms[0].submit();
		else 
			alert("el estado de la linea no permite mantencion");	
	}
	else
	{
		if(op==6)
		{
			if(confirm("Esta seguro que desea borrar esta linea?"))
						document.forms[0].submit();
		}	
		else
		{
			document.forms[0].submit();
		}
	}	
}





function goFilter(estado) {
	document.forms[0].SCREEN.value = 120;
	document.forms[0].filestado.value = estado.value;
	document.forms[0].submit();
	
	
}

</SCRIPT>  

</head>

<BODY>
<h3 align="center"><% if(userPO.getHeader1().equals("INQ")) out.print("Consulta "); else out.print("Mantenedor de "); %> L&iacute;neas Estatales MYPE<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="lineas_estatales_list.jsp, EDL0060"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0060" >
  <p> 
    <input type=HIDDEN name="SCREEN" value="200">
    <input type=HIDDEN name="opt"> 
    <input type=HIDDEN name="filestado"> 
	<input type=HIDDEN name="CURRCODESL" value="0"> 
  </p>
  

<%
	
	boolean nohayregistros = true;
	if ( !listLinEst.getNoResult() ) 
	{
		listLinEst.initRow();

        while (listLinEst.getNextRow()) 
		{
        	datapro.eibs.beans.EDL006001Message msgList = (datapro.eibs.beans.EDL006001Message) listLinEst.getRecord();
			if(!(!userPO.getHeader1().equals("INQ") && !msgList.getE01MLNSTS().equals("A") && !msgList.getE01MLNSTS().equals("I")))
			{
				nohayregistros = false;
				break;	
			}	
 		} 
	 }  


	if ( listLinEst.getNoResult() || nohayregistros ) {
%>

  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 

          <table class="tbenter" width=100% align=center>
          		<% if(userPO.getHeader1().equals("INQ")){ %>
          <tr id="trclear"> 
            <th align=CENTER  colspan="2">
	              <div align="right">Estado :</div>
            </th>

            <th align=CENTER colspan="9">
             <div align="left">
                <input type="text" name="E01MLNST1" size="5" maxlength="4" value="<%=request.getAttribute("E01MLNST1") %>" readonly onchange="javascript:goFilter(this);" onfocus="javascript:goFilter(this);" >
                <input type="text" name="E01MLNDS1" size="41"  value="<%=request.getAttribute("E01MLNDS1") %>" readonly  >
					<a href="javascript:GetCodeDescCNOFC('E01MLNST1','E01MLNDS1','VE')"> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
			</div>
            </th>
          </tr>
          
		<% }%>
          
            <tr> 
			<% if(!userPO.getHeader1().equals("INQ")){ %>              
              <td class=TDBKG width="100%"> 
                	<div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
                	   <input type=HIDDEN name="CURRCODEL" value="0"> 
              </td>
			<% } %>
            </tr>
          </table>
          <p>&nbsp;</p>
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
    	</div>

	  </TD>
	</TR>
    </TABLE>

	
<%}else {
 
 
 
 
		 if ( !error.getERRNUM().equals("0")  ) {
     			error.setERRNUM("0");
    			out.println("<SCRIPT Language=\"Javascript\">");
     			out.println("       showErrors()");
     			out.println("</SCRIPT>");
    		 }
%> 
 
          
	<table class="tbenter" width=100% align=center height="8%">
	<tr> 
		<% if(!userPO.getHeader1().equals("INQ")){ %>
	    	<td class=TDBKG width="20%"> 
   		     	<div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
   		   	</td>
			<td class=TDBKG width="20%"> 
      	 	 	<div align="center"><a href="javascript:goAction(2)"><b>Mantenci&oacute;n</b></a></div>
      		</td>      
		<% } %>
			<td class=TDBKG width="20%"> 
      	 	 	<div align="center"><a href="javascript:goAction(5)"><b>Consulta</b></a></div>
      		</td>      
	    	<td class=TDBKG width="20%"> 
    	    	<div align="center"><a href="javascript:goAction(3)"><b>Sub L&iacute;neas</b></a></div>
  	 	   	</td>
			<td class=TDBKG width="20%"> 
    	    	<div align="center"><a href="javascript:goAction(4)"><b>Comunas</b></a></div>
    	  	</td>      
    </tr>
  	</table>
	<br>

  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
		<% if(userPO.getHeader1().equals("INQ")){ %>
          <tr id="trclear"> 
            <th align=CENTER  colspan="2">
	              <div align="right">Estado :</div>
            </th>

            <th align=CENTER colspan="9">
             <div align="left">
                <input type="text" name="E01MLNST1" size="5" maxlength="4" value="<%=request.getAttribute("E01MLNST1") %>" readonly onchange="javascript:goFilter(this);" onfocus="javascript:goFilter(this);" >
                <input type="text" name="E01MLNDS1" size="41"  value="<%=request.getAttribute("E01MLNDS1") %>" readonly  >
					<a href="javascript:GetCodeDescCNOFC('E01MLNST1','E01MLNDS1','VE')"> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
			</div>
            </th>
          </tr>
          
		<% }%>
		
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
            <th align=LEFT nowrap width="5%">L&iacute;nea</th>
 			<th align=LEFT nowrap width="10%">Garantizador</th>
 			<th align=LEFT nowrap width="10%">Programa</th>
 			<th align=LEFT nowrap width="10%">Inicio</th>
 			<th align=LEFT nowrap width="10%">T&eacute;rmino Final</th>
 			<th align="center" nowrap width="10%">Moneda</th>
 			<th align=LEFT nowrap width="10%">Cupo L&iacute;nea</th>
			<th align=LEFT nowrap width="10%">Saldo</th>
			<th align=LEFT nowrap width="10%">Monto Retenido</th>
			<th align=LEFT nowrap width="10%">Tasa Comisi&oacute;n</th>
			<th align=LEFT nowrap width="5%">Estado</th>
			<th align=LEFT nowrap width="5%"> </th>
          </tr>
 
           <%
                listLinEst.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (listLinEst.getNextRow()) {
                  	
               		datapro.eibs.beans.EDL006001Message msgList = (datapro.eibs.beans.EDL006001Message) listLinEst.getRecord();
					 %>
					 
					<% if(!(!userPO.getHeader1().equals("INQ") && !msgList.getE01MLNSTS().equals("A") && !msgList.getE01MLNSTS().equals("I"))){
					
						if (firstTime) {
							firstTime = false;
							chk = "checked";
						} else {
							chk = "";
						}
					
					 %>
					 
					<tr id="dataTable"> 
           				<td NOWRAP align=CENTER>
           					<input type="radio" name="CURRCODEL" value="<%= listLinEst.getCurrentRow() %>"  <%=chk%> onClick="highlightRow('dataTable', this.value)"> 
           				</td>

						<td align="center" nowrap>
							<%=msgList.getE01MLNSEQ() %>
						</td>
						
						<td align="left" nowrap>
							<%=msgList.getE01MLNDGR() %>
						</td>
						<td align="left" nowrap>
							<%=msgList.getE01MLNDPG() %>
						</td>						
						<td align="center" nowrap>
						    <%= Util.formatDate(msgList.getE01MLNFVD(),msgList.getE01MLNFVM(),msgList.getE01MLNFVY())%>
						</td>
						<td align="center" nowrap>
						    <%= Util.formatDate(msgList.getE01MLNFTD(),msgList.getE01MLNFTM(),msgList.getE01MLNFTY())%>
						</td>
						<td align="center" nowrap>
							<%=msgList.getE01MLNMDA() %>
						</td>						
						 
						<td align="right" nowrap>
							<%=Util.formatCCY(msgList.getE01MLNCLI()) %>
						</td>						
						<td align="right" nowrap >
						<% if(msgList.getE01MLNMAX().trim().equals("1")) out.print("<FONT COLOR=#FF0000> <B>");%>
							<%=Util.formatCCY(msgList.getE01MLNSDO()) %> 
						<% if(msgList.getE01MLNMAX().trim().equals("1")) out.print("</B>");%>
						</td>						
						<td align="right" nowrap>
							<%=Util.formatCCY(msgList.getE01MLNSDR()) %>
						</td>						
						<td align="right" nowrap>
							<%=msgList.getE01MLNTAS () %>
						</td>						
						<td align="left" nowrap>
							<%=msgList.getE01MLNDST() %>
						</td>
						<td align="left" nowrap>
							<% if(msgList.getH01FLGWK3().trim().equals("1")) out.print("<FONT COLOR=#FF0000> <B>No Aprobado</B>");%>
						</td>
						<input type="hidden" name="E01MLNSTS<%=listLinEst.getCurrentRow()%>" id="E01MLNSTS<%=listLinEst.getCurrentRow()%>" value="<%=msgList.getE01MLNSTS() %>">						
         			   
         			   <% } %>
         			</tr>
          	<% } %>
              </table>
              </td>
              </tr>
  </table>
  
   

<SCRIPT language="JavaScript">
	showChecked("CURRCODEL");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

</body>
</html>
