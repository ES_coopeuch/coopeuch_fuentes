<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.EPV119001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

 builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
	init(opth,field,bank,ccy,field1,field2,opcod);
	showPopupHelp();
}

function validaCampos(){

	if(document.getElementById("E01PVPARL1").checked==true){
		var avanceCre = null;
		var cantCuotas = null;
		var montReliq = null;
		var descTasaReliq = null;
		
		//Validacion % Avance Cr�dito
		if(document.getElementById("E01PVPAVC").value !== ""){
			if (isNaN(parseInt(document.getElementById("E01PVPAVC").value))) {			
				alert('Valor % de Avance de Cr�dito debe ser un valor numerico');
				return false;											
			}else if(document.getElementById("E01PVPAVC").value<0){			
				alert('Valor % de Avance de Cr�dito no puede ser negativo');
				return false;			
			}else if(document.getElementById("E01PVPAVC").value>100){
				alert('Valor % de Avance de Cr�dito no puede ser mayor a 100');
				return false;		 	
			}else if(dos_decimales(document.getElementById("E01PVPAVC").value) !== true){			
				alert('Formato invalido % de Avance de Cr�dito, permitido XXX.XX');
				return false;			
			}else{
				avanceCre = true;
			}
		}else{
			avanceCre = true;
		}
		
		
		//Validacion cantidad de cuotas
		if(document.getElementById("E01PVPCCU").value !== ""){
			if (!/^([0-9])*$/.test(document.getElementById("E01PVPCCU").value)){		   
           		alert("Valor cantidad de cuotas debe ser numerico y entero");
           		return false;
	    	}else if(document.getElementById("E01PVPCCU").value<0){
			 	alert('Valor cantidad de cuotas no puede ser negativo');
			 	return false;		 	
			}else if(document.getElementById("E01PVPCCU").value>100){
				alert('Valor cantidad de cuotas no puede ser mayor a 100');
				return false;		 	
			}else{
				cantCuotas = true;
			}	
		}else{
			cantCuotas = true;
		}		
		
		//Validacion % Monto Reliquidaci�n
		if(document.getElementById("E01PVPMRE").value !== ""){
			if (!/^([0-9])*$/.test(document.getElementById("E01PVPMRE").value)){		   
	           alert("Valor % Monto Reliquidaci�n debe ser numerico y entero");
	           return false;
	    	}else if(document.getElementById("E01PVPMRE").value<0){
			 	alert('Valor % Monto Reliquidaci�n no puede ser negativo');
			 	return false;		 	
			}else if(document.getElementById("E01PVPMRE").value>100){
				alert('Valor % Monto Reliquidaci�n no puede ser mayor a 100');
				return false;		 	
			}else{
				montReliq = true;
			}
		}else{
			montReliq = true;
		}		
		
		
		//Validacion % Desc. Tasa Reliquidacion
		if(document.getElementById("E01PVPDTA").value !== ""){
			if (isNaN(parseInt(document.getElementById("E01PVPDTA").value))) {			
				alert('Valor % Desc. Tasa Reliquidacion debe ser un valor numerico');
				return false;								
			}else if(document.getElementById("E01PVPDTA").value<0){			
				alert('Valor % Desc. Tasa Reliquidacion no puede ser negativo');
				return false;
			}else if(document.getElementById("E01PVPDTA").value>100){
				alert('Valor % Desc. Tasa Reliquidacion no puede ser mayor a 100');
				return false;		 	
			}else if(dos_decimales(document.getElementById("E01PVPDTA").value) !== true){			
				alert('Formato invalido % Desc. Tasa Reliquidacion, permitido XXX.XX');
				return false;
			}else{
				descTasaReliq = true;
			}	
		}else{
			descTasaReliq = true;
		}		
		
		if(avanceCre==true && cantCuotas==true && montReliq==true && descTasaReliq==true){		
			document.getElementById("form1").submit();
		}
	
	}else{
			document.getElementById("form1").submit();
	}

}

function dos_decimales(cadena){
	var expresion=/^\d+(\.\d{0,2})?$/;
	var resultado=expresion.test(cadena);
	return resultado;
}


function habilitar(value){
	if(value=="Y")	{		
		document.getElementById("E01PVPAVC").disabled = false;
		document.getElementById("E01PVPCCU").disabled = false;
		document.getElementById("E01PVPMRE").disabled = false;
		document.getElementById("E01PVPDTA").disabled = false;		
	}else if(value=="N"){                         
		document.getElementById("E01PVPAVC").disabled = true;
		document.getElementById("E01PVPAVC").value = "";
		document.getElementById("E01PVPCCU").disabled = true;
		document.getElementById("E01PVPCCU").value = "";
		document.getElementById("E01PVPMRE").disabled = true;
		document.getElementById("E01PVPMRE").value = "";
		document.getElementById("E01PVPDTA").disabled = true;
		document.getElementById("E01PVPDTA").value = "";			
	}		
}


 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	Consulta Promoci�n en Plataforma de Venta
<%} else if (maintenance){ %>
	Mantenci�n Promoci�n en Plataforma de Venta
<%} else { %>
	Nuevo  Promoci�n en Plataforma de Venta
<%} %>

 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PVPRO_maintenance.jsp, EPV1190"></h3>
<hr size="4">
<form id="form1" method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1190" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01UBK" value="<%= currUser.getE01UBK().trim()%>">
  
 <% int row = 0;%>
 
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">C�digo de Promoci�n :</div>
            </td>
            <td width="70%" > 
	             <eibsinput:text property="E01PVPCOD" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" readonly="true"/>
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Descripci�n :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01PVPDES" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL%>" readonly="<%=readOnly %>" />
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">C�digo Tarifa :</div>
            </td>
            <td width="70%" > 
              <input type="text" name="E01PVPTLN" size="3" maxlength="2" value="<%= cnvObj.getE01PVPTLN().trim()%>" <%=readOnly?"disabled":""%> >
			<%if  (!readOnly) { %>
              <a href="javascript:GetRateTable('E01PVPTLN')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absmiddle" border="0"></a> 
             <%} %>                
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Vigencia Desde :</div>
            </td>
            <td width="70%" > 
   	        <eibsinput:date name="cnvObj" fn_year="E01PVPVDY" fn_month="E01PVPVDM" fn_day="E01PVPVDD" readonly="<%=readOnly %>"/>
	        </td>
          </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Vigencia Hasta :</div>
            </td>
            <td width="70%" > 
   	        <eibsinput:date name="cnvObj" fn_year="E01PVPVHY" fn_month="E01PVPVHM" fn_day="E01PVPVHD" readonly="<%=readOnly %>" />
	        </td>
          </tr>            
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="40%" > 
              <div align="right">Tipo de Recaudaci�n :</div>
            </td>
            <td width="60%" > 
               <select name="E01PVPREC" <%=readOnly?"disabled":""%>>
                    <option value="P" <% if (cnvObj.getE01PVPREC().equals("P")) out.print("selected"); %>>Planilla</option>
                    <option value="D" <% if (cnvObj.getE01PVPREC().equals("D")) out.print("selected"); %>>Pago Directo</option>  
                    <option value="A" <% if (cnvObj.getE01PVPREC().equals("A")) out.print("selected"); %>>Ambos</option>                                      
                  </select>
	        </td>
           </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="40%" > 
              <div align="right">Acepta Prepago :</div>
            </td>
            <td width="60%" > 
 		       <p> 
                 <input type="radio" name="E01PVPAPR"  value="Y" <%if (cnvObj.getE01PVPAPR().equals("Y")) out.print("checked"); %>  <%=readOnly?"disabled":""%> >
                  Si 
                 <input type="radio" name="E01PVPAPR"  value="N" <%if (cnvObj.getE01PVPAPR().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%>>
                  No
               </p>                              
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="40%" > 
              <div align="right">Retenci�n :</div>
            </td>
            <td width="60%" > 
 		       <p> 
				<input type="checkbox" name="flat_E01PVPFRT" onclick="if (document.forms[0].flat_E01PVPFRT.checked ){document.forms[0].E01PVPFRT.value='Y'}else{document.forms[0].E01PVPFRT.value='N'}" 
            		<%=("Y".equals(cnvObj.getE01PVPFRT())?"checked='checked'":"")%> >
            	<input type="hidden" name="E01PVPFRT" value="<%= (!"Y".equals( cnvObj.getE01PVPFRT().trim())?"N": cnvObj.getE01PVPFRT().trim())%>" >
               </p>                               
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="40%" > 
              <div align="right">Acepta Reliquidaci�n :</div>
            </td>
            <td width="60%" > 
 		       <p> 
                 <input type="radio" id="E01PVPARL1" name="E01PVPARL"  value="Y" <%if (cnvObj.getE01PVPARL().equals("Y")) out.print("checked"); %>  <%=readOnly?"disabled":""%> onchange="habilitar(this.value);">
                  Si 
                 <input type="radio" id="E01PVPARL2" name="E01PVPARL"  value="N" <%if (cnvObj.getE01PVPARL().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%> onchange="habilitar(this.value);">
                  No
               </p>                              
	        </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
           <td width="40%" > 
              <div align="right">% Avance Cr�dito :</div>
            </td>
            <td width="70%" > 
            <%if (readOnly){%> 
              	<input type="text" id="E01PVPAVC" name="E01PVPAVC" size="6" maxlength="6" value="<%if (cnvObj.getE01PVPARL().equals("Y")){ out.print(cnvObj.getE01PVPAVC().trim());}else{out.print("");}%>" disabled="true" onkeyup=" checkDecimal(2) " onkeypress=" enterSignDecimal(2) " onchange="setRecalculate3();">
            <%}else{ %>
            	<input type="text" id="E01PVPAVC" name="E01PVPAVC" size="6" maxlength="6" value="<%if (cnvObj.getE01PVPARL().equals("Y")){ out.print(cnvObj.getE01PVPAVC().trim());}else{out.print("");}%>" <%if (cnvObj.getE01PVPARL().equals("N")) { %>disabled="true"<%}else{ %> enabled="true"<%} %>onkeyup=" checkDecimal(2) " onkeypress=" enterSignDecimal(2) " onchange="setRecalculate3();">
            <%} %>  
            </td> 
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
           <td width="40%" > 
              <div align="right">Cantidad Cuotas :</div>
           </td>
           <td width="70%" >
           <%if (readOnly){%>  
              <input type="text" id="E01PVPCCU" name="E01PVPCCU" size="4" maxlength="4" value="<%if (cnvObj.getE01PVPARL().equals("Y")){ out.print(cnvObj.getE01PVPCCU().trim());}else{out.print("");}%>" disabled="true" >
           <%}else{ %>
           	  <input type="text" id="E01PVPCCU" name="E01PVPCCU" size="4" maxlength="4" value="<%if (cnvObj.getE01PVPARL().equals("Y")){ out.print(cnvObj.getE01PVPCCU().trim());}else{out.print("");}%>" <%if (cnvObj.getE01PVPARL().equals("N")) { %>disabled="true"<%}else{ %> enabled="true"<%} %> >
           <%} %>   
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
           <td width="40%" > 
              <div align="right">% Monto Reliquidaci�n :</div>
           </td>
           <td width="70%" > 
           <%if (readOnly){%>
              	<input type="text" id="E01PVPMRE" name="E01PVPMRE" size="3" maxlength="3" value="<%if (cnvObj.getE01PVPARL().equals("Y")){ out.print((int)Double.parseDouble(cnvObj.getE01PVPMRE().trim()));}else{out.print("");}%>" disabled="true">
           <%}else{ %>
           		<input type="text" id="E01PVPMRE" name="E01PVPMRE" size="3" maxlength="3" value="<%if (cnvObj.getE01PVPARL().equals("Y")){ out.print((int)Double.parseDouble(cnvObj.getE01PVPMRE().trim()));}else{out.print("");}%>" <%if (cnvObj.getE01PVPARL().equals("N")) { %>disabled="true"<%}else{ %> enabled="true"<%} %>>
           <%} %>   
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
           <td width="40%" > 
              <div align="right">% Desc. Tasa Reliquidaci�n :</div>
           </td>
           <td width="70%" >
           <%if (readOnly){%> 
                <input type="text" id="E01PVPDTA" name="E01PVPDTA" size="6" maxlength="6" value="<%if (cnvObj.getE01PVPARL().equals("Y")){ out.print(cnvObj.getE01PVPDTA().trim());}else{out.print("");}%>" disabled="true" onkeyup=" checkDecimal(2) " onkeypress=" enterSignDecimal(2) " onchange="setRecalculate3();">
           <%}else{ %>
           		<input type="text" id="E01PVPDTA" name="E01PVPDTA" size="6" maxlength="6" value="<%if (cnvObj.getE01PVPARL().equals("Y")){ out.print(cnvObj.getE01PVPDTA().trim());}else{out.print("");}%>" <%if (cnvObj.getE01PVPARL().equals("N")) { %>disabled="true"<%}else{ %> enabled="true"<%} %> onkeyup=" checkDecimal(2) " onkeypress=" enterSignDecimal(2) " onchange="setRecalculate3();">
           <%} %>   
            </td>
          </tr>         
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="40%" > 
              <div align="right">Acepta SobreDescuentos :</div>
            </td>
            <td width="60%" > 
 		       <p> 
                 <input type="radio" name="E01PVPADS"  value="Y" <%if (cnvObj.getE01PVPADS().equals("Y")) out.print("checked"); %>  <%=readOnly?"disabled":""%> >
                  Si 
                 <input type="radio" name="E01PVPADS"  value="N" <%if (cnvObj.getE01PVPADS().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%>>
                  No
               </p>                              
	        </td>
          </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="40%" > 
              <div align="right">Tipo de Promoci&oacute;n :</div>
            </td>
            <td width="60%" > 
 		       <p> 
                 <input type="radio" name="E01PVPTDP"  value="N" <%if (cnvObj.getE01PVPTDP().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%> >
                  Normal  
                 <input type="radio" name="E01PVPTDP"  value="E" <%if (cnvObj.getE01PVPTDP().equals("E")) out.print("checked"); %>  <%=readOnly?"disabled":""%>>
                  Especial
               </p>                              
	        </td>
          </tr>          

        </table>
      </td>
    </tr>
  </table>

<%if  (!readOnly) { %>
    <div align="center"> 
        <input id="EIBSBTN" type=button name="Submit" value="Enviar" onclick="validaCampos()">
    </div>
<% } %>  

  </form>
</body>
</HTML>
