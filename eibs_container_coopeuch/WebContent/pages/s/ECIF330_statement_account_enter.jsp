<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" 	prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<jsp:useBean id="headerTrans" class="datapro.eibs.beans.ECIF33003Message"  scope="session" />
<html>
<head>
<title>Consulta de Cartolas</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT Language="javascript">

  function mostrar(tipo){
	if (tipo=='C'){
		document.getElementById("tab1").style.visibility="hidden";	
	}else{
		document.getElementById("tab1").style.visibility="visible";	
	}
  }
 
  function validateSelection(forma){
  	//validar fecha desde...
	if (forma.E03FRDTEY.value=='0' || forma.E03FRDTEM.value=='0' || forma.E03FRDTED.value=='0'){
		alert('Debe colocar fecha desde.');
		document.forms[0].E03FRDTEY.focus();
		return false;	
	}else{
		return true;
	}		
  }
    
function checkNum(){
	var ok = false;
	if(isNaN(document.forms[0].E01SELACC.value)||(document.forms[0].E01SELACC.value.length < 1)){
		alert("Debe introducir un valor valido de cuenta");
		document.forms[0].E01SELACC.value='';
		document.forms[0].E01SELACC.focus();
		ok=false;
	}else {
		var value = getElementChecked("Type").value;		
		if (value!='C' && !validateSelection(document.forms[0])){
			ok=false;		
		}else{
			document.forms[0].SCREEN.value = value=='C' ? '200' : '250';		
			ok=true;		
		}
		  	
	}	
	return ok;
}


</SCRIPT>

</head>



<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<body bgcolor="#FFFFFF">

<h3 align="center">Consulta de Cartolas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="statement_account_enter, ECIF330"></h3>
<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECIF330" onsubmit="return checkNum();">
  <p> 
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="">
  </p>
  
  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0" bordercolor="#000000">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0">          
		   <tr><td colspan="4">&nbsp;</td></tr>
		    <tr><td colspan="4">&nbsp;</td></tr>
			 <tr><td colspan="4">&nbsp;</td></tr>
		  <tr> 
			<td width="20%">&nbsp;</td>		  
            <td nowrap width="15%"> 
              <div align="left">
				&nbsp;&nbsp;&nbsp;&nbsp;N&uacute;mero de cuenta :
			   </div>
            </td>
            <td nowrap width="50%"> 
              <input type="text" name="E01SELACC" size="13" maxlength="12" onKeypress="enterInteger()">
              <a href="javascript:GetAccount('E01SELACC','','RT','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absmiddle" border="0"  ></a> 
            </td>
			<td width="15%">&nbsp;</td>            
          </tr>

		<tr>
			<td align="left">&nbsp;</td>
			<td align="left" nowrap>
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" checked="checked" name="Type" value="C" onclick="javascript:mostrar(this.value);">Cartola
			</td>
			<td align="left" >&nbsp;</td>
			<td align="left">&nbsp;</td>			
		</tr>
		<tr>
			<td align="left">&nbsp;</td>
			<td align="left"  nowrap>
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="Type" value="S" onclick="mostrar(this.value);">Selecci�n Transacciones
			</td>
			<td align="left">
			<table class="tbenter" border="0" id="tab1" style="visibility:  hidden">
				<tr>
					<td align="right" nowrap >Fecha Desde :</td>				
					<td align="left" nowrap  ><eibsinput:date name="headerTrans" fn_year="E03FRDTEY" fn_month="E03FRDTEM" fn_day="E03FRDTED"/></td>
					<td align="right" nowrap > Hasta :</td>				
					<td align="left" nowrap  ><eibsinput:date name="headerTrans" fn_year="E03TODTEY" fn_month="E03TODTEM" fn_day="E03TODTED"/></td>
				</tr>
				<tr>
					<td align="right" nowrap >Monto Desde :</td>				
					<td align="left" nowrap  ><eibsinput:text name="headerTrans" property="E03FRAMNT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"/></td>
					<td align="right" nowrap > Hasta :</td>				
					<td align="left" nowrap  ><eibsinput:text name="headerTrans" property="E03TOAMNT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"/></td>
				</tr>	
				<tr>
					<td align="right" nowrap ># Ref :</td>				
					<td align="left" nowrap  ><eibsinput:text name="headerTrans" property="E03FRCHKN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="11" maxlength="11"/></td>
					<td align="right" nowrap > Hasta :</td>				
					<td align="left" nowrap  ><eibsinput:text name="headerTrans" property="E03TOCHKN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>"  size="11" maxlength="11"/></td>
				</tr>											
			</table>					
			
			</td>			
			<td align="left">&nbsp;</td>			
		</tr>				         	         
        </table>
      </td>
    </tr>
  </table>
  
 <br>
 <div align="center"> 
     <input id="EIBSBTN" type="submit" name="submit" value="Enviar">
 </div>

<script language="JavaScript">
  document.forms[0].E01SELACC.focus();
  document.forms[0].E01SELACC.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>;
 <%
 }
%>

 <%if ("S".equals(request.getAttribute("TYPE"))){%>
 <script type="text/javascript">
	document.forms[0].Type[1].checked = true; 
  	mostrar('S');
 </script>
 <% } %> 
</form>
</body>
</html>
