/**
 * ResultsAvaldoBienesRaices.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResultsAvaldoBienesRaices  implements java.io.Serializable {
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.AvaldoBienesRaices[] avaldoBienesRaices;

    public ResultsAvaldoBienesRaices() {
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.AvaldoBienesRaices[] getAvaldoBienesRaices() {
        return avaldoBienesRaices;
    }

    public void setAvaldoBienesRaices(cl.Sinacofi.coopeuch.creditos.predictorriesgo.AvaldoBienesRaices[] avaldoBienesRaices) {
        this.avaldoBienesRaices = avaldoBienesRaices;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.AvaldoBienesRaices getAvaldoBienesRaices(int i) {
        return avaldoBienesRaices[i];
    }

    public void setAvaldoBienesRaices(int i, cl.Sinacofi.coopeuch.creditos.predictorriesgo.AvaldoBienesRaices value) {
        this.avaldoBienesRaices[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsAvaldoBienesRaices history = (ResultsAvaldoBienesRaices) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ResultsAvaldoBienesRaices other = (ResultsAvaldoBienesRaices) obj;
        boolean _equals;
        _equals = true
            && ((this.avaldoBienesRaices==null && other.getAvaldoBienesRaices()==null) || 
             (this.avaldoBienesRaices!=null &&
              java.util.Arrays.equals(this.avaldoBienesRaices, other.getAvaldoBienesRaices())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsAvaldoBienesRaices history = (ResultsAvaldoBienesRaices) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getAvaldoBienesRaices() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAvaldoBienesRaices());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAvaldoBienesRaices(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
