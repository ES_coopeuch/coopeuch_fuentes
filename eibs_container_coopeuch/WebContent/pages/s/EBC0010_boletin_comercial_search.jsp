<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="datapro.eibs.master.Util"%>
<HTML>
<HEAD>
<TITLE>
Marca/Desmarca Boletin Comercial
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V4.0 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script type="text/javascript">
function enter()
{
   	if ((document.forms[0].E01SELBRN == null) && (document.forms[0].E01SELCUN == null)) 
	{
		alert("Debe Ingresar algun criterio de selecci�n");
	  	document.forms[0].E01SELBRN.focus();
  		return false;
  	} 
   	else if((document.forms[0].E01SELBRN.value.length <1) && (document.forms[0].E01SELCUN.value.length <1)) 
  	  		{
				alert("Debe Ingresar algun criterio de selecci�n");
  				document.forms[0].E01SELBRN.focus();
  				return false;
  	  		}
  		else if ((document.forms[0].E01SELBRN.value == 0) && (document.forms[0].E01SELCUN.value == 0))
  			{
  				alert("Debe Ingresar algun criterio de selecci�n");
  				document.forms[0].E01SELBRN.focus();
  				return false;
  			}
   return true;
}
</script>

</head>

<body>
   
 <% 
 if ( !error.getERRNUM().equals("0")  ) {
 	 error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Marca/Desmarca Bolet&iacute;n Comercial<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="boletin_comercial_search.jsp,EBC0010"></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.cleaning.JSEBC0010" onsubmit="return(enter())"> 
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="3">


<h3 align="center">Seleccion por</h3>
<br>
  <table class="tableinfo">
    <tr> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trclear"> 
            <td nowrap align="right" valign="middle" width="49%"> 
              <div align="right">C�digo Oficina :</div>
            </td>
            <td nowrap align="left" valign="middle" colspan="2"> 
              <input type="text" name="E01SELBRN" maxlength="4" size="5" >
              <a href="javascript:GetBranch('E01SELBRN','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="help" align="absmiddle" border="0" ></a> 
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap align="right" valign="middle" width="49%"> 
              <div align="right">O</div>
            </td>
            <td nowrap align="left" valign="middle" colspan="2"> 
               <div align="right"> </div>
           </td>       
          </tr>  
          <tr id="trclear"> 
            <td nowrap align="right" valign="middle" width="49%"> 
              <div align="right">Cliente :</div>
            </td>
            <td nowrap align="left" valign="middle" colspan="2"> 
              <input type="text" name="E01SELCUN" maxlength="10" size="10" >
              <a href="javascript:GetCustomerDescId('E01SELCUN','','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="help" align="absmiddle" border="0" ></a> 
            </td>       
          </tr>                
        </table>
      </td>
    </tr>
  </table>
        <p align="center"> 
    	  <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    	</p>
   
   <script language="JavaScript">
  document.forms[0].E01SELBRN.focus();
  document.forms[0].E01SELBRN.select();
</script>
</form>
</body>
</html>
