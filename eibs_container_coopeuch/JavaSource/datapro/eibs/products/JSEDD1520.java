package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Creado por              :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  07/05/2013                                                                                      **/
/**  Objetivo                :  Proceso de carga de nomina de fallecidos                                                       **/
/*********************************************************************************************************************************/



import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.ECO100002Message;
import datapro.eibs.beans.EDD152001Message;
import datapro.eibs.beans.EDD152002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.master.Util;
import datapro.eibs.services.ExcelResultSet;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.services.ExcelXLSXResultSet;
public class JSEDD1520 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_FALLECIDOS_ENTER = 100;	
	protected static final int A_FALLECIDOS_ENTER = 200;
	protected static final int A_CARGADOS_LIST = 300;	
	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	/**
	 * 
	 */
	protected void processRequest(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res,
			HttpSession session, 
			int screen) 
	throws ServletException, IOException {
		screen =  A_FALLECIDOS_ENTER;
		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}			
	    	flexLog("Screen..." + screen);
		switch (screen) {
		case R_FALLECIDOS_ENTER:
			procReqFallecidosEnter(user, req, res, session);
			break;
		case A_FALLECIDOS_ENTER:
			procActionFallecidosEnter(user, req, res, session);
			break;			
		case A_CARGADOS_LIST:
			procActionCargadosList(user, req, res, session);
			break;				
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}
	/**
	 * procReqFallecidosEnter
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqFallecidosEnter(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession ses)
			throws ServletException, IOException {
		UserPos userPO = getUserPos(ses);
		ELEERRMessage msgError = new ELEERRMessage();
		
		ses.setAttribute("error", msgError);
		ses.setAttribute("userPO", userPO);
		
		forward("EDD1520_fallecidos_enter.jsp", req, res);

	}
	
	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionFallecidosEnter(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		String PageToCall = "";
		int rows = 0;
		//read file
		try {
			mp = getMessageProcessor("ECO1000", req);
			
			SmartUpload mySmartUpload = new SmartUpload();
			mySmartUpload.initialize(config, req, res);
			mySmartUpload.upload();
			com.jspsmart.upload.File file =  mySmartUpload.getFiles().getFile(0);
			
			InputStream xls = Util.getStreamFromObject(file);													
			
			ResultSet rs = null;
			if (ExcelUtils.isXLSXVersion(file.getFilePathName())) {
				rs = new ExcelXLSXResultSet(xls, 0);
				((ExcelXLSXResultSet)rs).init();
			} else {
				rs = new ExcelResultSet(xls, 0);
				((ExcelResultSet)rs).init();
			}
			
			boolean error = false;
			while (rs.next()) {
				EDD152001Message msg = (EDD152001Message) mp.getMessageRecord("EDD152001");
	            msg.setH01USER(user.getH01USR());
				ExcelUtils.populate(rs, msg);
				flexLog("Mensaje enviado..." + msg);
				mp.sendMessage(msg);
				rows++;
			}
			if (!error) {
				EDD152001Message msg = (EDD152001Message) mp.getMessageRecord("EDD152001");
	            msg.setH01USER(user.getH01USR());
				msg.setH01FLGMAS("*"); //Last Element List
				mp.sendMessage(msg);
				//	Receive Error Message
				msgError = (ELEERRMessage) mp.receiveMessageRecord();
				if (mp.hasError(msgError)) {
					PageToCall = "EDD1520_fallecidos_enter.jsp";
				} else {
					req.setAttribute("rows", String.valueOf(rows));
					PageToCall = "EDD1520_fallecidos_process.jsp";
				}
			}
			
			flexLog("Putting java beans into the session");
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			forward(PageToCall, req, res);
			
		} catch (Exception e) {
			msgError = new ELEERRMessage();
			msgError.setERRNUM("2");
			msgError.setERNU01("0001");		//4		                
			msgError.setERDS01("Error en Record: " + (rows + 1));//70
			msgError.setERNU02("0002");		//4		                
			msgError.setERDS02(e.getMessage().trim().length() > 70 ? e.getMessage().substring(0, 69) : e.getMessage().trim());//70
			session.setAttribute("error", msgError);						
			forward("EDD1520_fallecidos_enter.jsp", req, res);
		} finally {
			if (mp != null)	mp.close();
		}

}
	
	/**
	 * procActionCargadosList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionCargadosList(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EDD1520", req);

			EDD152002Message msg = (EDD152002Message) mp.getMessageRecord("EDD152002");
			msg.setH02USERID(user.getH01USR());
			msg.setH02OPECOD("0015");
			msg.setH02TIMSYS(getTimeStamp());
 
			//Sends message
		//	flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H02FLGMAS");
            userPO.setPurpose("CARGADOS");
			session.setAttribute("userPO", userPO);
			session.setAttribute("EDD152002List", list);
			forwardOnSuccess("EDD1520_fallecidos_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}	
}
