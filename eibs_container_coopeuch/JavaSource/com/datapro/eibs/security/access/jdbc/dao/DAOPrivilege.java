package com.datapro.eibs.security.access.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.General;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.PrivilegeNotFoundException;
import com.datapro.eibs.security.access.jdbc.bean.CNTRLPVL;
import com.datapro.eibs.security.bean.CNTRLPVLNME;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.BeanParser;
import com.datapro.generic.beanutil.BeanSql;
import com.datapro.sql.manager.DBConnector;

/**
 * @author ogarcia
 *
 */
public class DAOPrivilege {

	private static Category cat =
		Category.getInstance(DAOPrivilege.class.getName());

	/**
	 * Constructor for DAOPrivilege.
	 */
	public DAOPrivilege() {
		super();
	}

	/**
	 * Delete privilege for a given user.
	 */
	public int delete(String uid) throws DAOException {

		int rt;
		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;

		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			String sql = "DELETE FROM CNTRLPVL WHERE PVLUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		} finally {
			dbCnx.close(null);
		}

		return rt;
	}

	/**
	 * Delete privilege for a given user.
	 */
	public int delete(String uid, String cde) throws DAOException {

		int rt;
		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;

		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			String sql = "DELETE FROM CNTRLPVL WHERE PVLUSR=? AND PVLPVL=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, cde);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		} finally {
			dbCnx.close(null);
		}

		return rt;
	}

	/**
	 * Insert user's privilege.
	 */
	public int insert(CNTRLPVL bean) throws DAOException {

		int rt;
		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;

		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			BeanSql bs = new BeanSql(bean);
			String sql = bs.insertPrepareStatement();
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		} finally {
			dbCnx.close(null);
		}

		return rt;

	}

	/**
	 * Insert user's privileges.
	 */
	public void insert(BeanList list) throws DAOException {

		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;

		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			list.initRow();
			int result = 0;
			while (list.getNextRow()) {
				CNTRLPVL bean = (CNTRLPVL) list.getRecord();
			
				BeanSql bs = new BeanSql(bean);
				String sql = bs.insertPrepareStatement();
				PreparedStatement ps = cnx.prepareStatement(sql);
				bs.parsePrepareStatement(ps);
				ps.executeUpdate();
				ps.close();
			}

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		} finally {
			dbCnx.close(null);
		}

	}

	/**
	 * Update user's privilege record.
	 */
	public int update(CNTRLPVL bean) throws DAOException {

		int rt;
		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;

		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			BeanSql bs = new BeanSql(bean);
			String sql = bs.updatePrepareStatement() + " WHERE PVLUSR=? AND PVLPVL=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			ps.setString(bs.getNextParamIndex(), bean.getPVLUSR());
			ps.setString(bs.getNextParamIndex() + 1, bean.getPVLPVL());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		} finally {
			dbCnx.close(null);
		}

		return rt;

	}

	/**
	 * Get user record.
	 */
	public CNTRLPVL getPrivilege(String uid, String cde)
		throws PrivilegeNotFoundException, DAOException {

		CNTRLPVL bean = null;
		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;

		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			String sql = "SELECT * FROM CNTRLPVL WHERE PVLUSR=? AND PVLPVL=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, cde);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNTRLPVL();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} else {
				throw new PrivilegeNotFoundException(
					"User privilege not found exception(uid:" + uid + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		} finally {
			dbCnx.close(null);
		}

		return bean;

	}

	/**
	 * Get privilege name and type per user.
	 */
	public BeanList getUserPrivilegesName(String uid)
		throws PrivilegeNotFoundException, DAOException {

		BeanList list = null;
		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;

		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			String sql = "SELECT a.*, b.CNODSC, b.CNOF04 FROM CNTRLPVL a, CNOFC b " +
			             "WHERE a.PVLUSR=? AND b.CNOCFL=? AND a.PVLPVL=b.CNORCD";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, "PV");
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				CNTRLPVLNME bean = new CNTRLPVLNME();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new PrivilegeNotFoundException(
					"User privilege not found exception(uid:" + uid + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		} finally {
			dbCnx.close(null);
		}

		return list;

	}

}
