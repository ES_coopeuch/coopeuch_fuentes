<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="datapro.eibs.master.Util"%>
<html>
<head>
<title>PosVenta Tarjetas</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ECC0160Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<jsp:useBean id="ccPvta" class="datapro.eibs.beans.ECC016001Message" scope="session" />

<script language="JavaScript">

function goAction(op) {
 	var page = "";
 	document.forms[0].OPT.value = op;
		if (document.forms[0].OPT.value == "")
		 	{ 	    
				alert("Debe Seleccionar una Solicitud");
 	    	}    
 	   else
 	       {
			document.forms[0].submit();
 	      	}		
  }

function GetInqDet(col)
{
	var param1="&COL="+col+"&OPT=1";
	page = webapp + "/servlet/datapro.eibs.products.JSECC0160?SCREEN=300" + param1;
	CenterWindow(page,900,500,2);
} 

</SCRIPT>  

</head>

<BODY>
<h3 align="center">Aprobacion Solicitudes <br> Post-Venta Tarjetas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cc_posventa_approval_list.jsp,ECC0160"></h3> 
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0160" >
  <input type=HIDDEN name="SCREEN" value="300">
 <INPUT TYPE=HIDDEN NAME="OPT" VALUE=""> 
   
    <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trclear"> 
   	        <td nowrap width="10%">&nbsp;</td>
            <td nowrap width="10%"> 
              <div align="right"><b>B&uacute;squeda por: </b></div>
            </td>
            <td nowrap width="5%">&nbsp;</td>
            <td nowrap width="10%">&nbsp;</td>
            <td nowrap width="40%">&nbsp;</td>
                      
            </tr>
		    <tr id="trclear">
	         <td nowrap>&nbsp;</td>            
            <td nowrap> 
              <div align="right"><b>Sucursal : </b></div>
            </td>
            <td nowrap width="5%">
                <input type="text" name="E01SELINB" size="5" maxlength="4" value="<%= userPO.getBranch() %>" readonly>(en blanco para todas)               
             </td>
             </tr>
             <tr id="trclear">
             <td nowrap>&nbsp;</td> 
             <td nowrap> 
              <div align="right"><b>Fecha de Solicitud : </b></div>
            </td>
            <td nowrap width="40%">
                <input type="text" name="E01SELISD" size="3" maxlength="2" value="<%= userPO.getHeader1() %>" readonly>
                <input type="text" name="E01SELISM" size="3" maxlength="2" value="<%= userPO.getHeader2() %>" readonly>
                <input type="text" name="E01SELISY" size="5" maxlength="4" value="<%= userPO.getHeader3() %>" readonly>                                
				</td>
			</tr> 
		</table>
      </td>
    </tr>
  </table>
  <br>
   
  <%
	if ( ECC0160Help.getNoResult() ) {
 %>
  
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
          <table class="tbenter" width=100% align=center>
           <tr>
              <td class=TDBKG>
                <div align="center"><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
              </td>
           </tr>
         </table>
	  </div>

	  </TD>
	</TR>
    </TABLE>
	
  <%  
		}
	else {
%> <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }

%> 


          
  <table class="tbenter" width=100% align=center>
    <tr> 
      <td class=TDBKG width="15%"> 
        <div align="center" style="cursor:hand" ><a href="javascript:goAction(5)"><b>Aprobar</b></a></div>
      </td>
      <td class=TDBKG width="15%"> 
        <div align="center" style="cursor:hand" ><a href="javascript:goAction(6)"><b>Rechazar</b></a></div>
      </td>
      <td class=TDBKG width="15%"> 
        <div align="center"><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
      </td>
    </tr>
  </table>
  
  <br>  
  
<TABLE  id=cfTable class="tableinfo">
 <tr> 
    <td NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Sucursal</div>
            </th>
            <th align=CENTER nowrap width="10%"> 
              <div align="center">Fecha Ingreso</div>
            </th>
            <th align=CENTER nowrap width="10%"> 
              <div align="center">No Solicitud </div>
            </th>
            <th align=CENTER nowrap width="20%"> 
              <div align="center">Cliente </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Cuenta </div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center"> Tarjeta </div>
            </th>            
            <th align=CENTER nowrap width="10%"> 
              <div align="center"> Solicitud </div>
            </th>
          </tr>
          <%
                ECC0160Help.initRow();
				boolean firstTime = true;
				String chk = "";
				int idx = 0;
				String valStyle = "";
        		while (ECC0160Help.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					if (idx++ % 2 != 0)
						valStyle = "trdark";
					else
						valStyle = "trclear";
                  	datapro.eibs.beans.ECC016001Message msgList = (datapro.eibs.beans.ECC016001Message) ECC0160Help.getRecord();                
		 %>
				<TR id="<%= valStyle %>">			
					<TD NOWRAP  ALIGN=CENTER > 
					<input type="radio" name="CURRCODE" value="<%= ECC0160Help.getCurrentRow() %>"  <%=chk%> ></TD>
					<TD NOWRAP  ALIGN=left ><%= msgList.getD01SPVINB()%></td>
			        <TD NOWRAP align="center" ><%= Util.formatDate(msgList.getE01SPVISD(),msgList.getE01SPVISM(),msgList.getE01SPVISY())%></td>
					<td NOWRAP align="center"><a href="javascript:GetInqDet('<%= ECC0160Help.getCurrentRow() %>')"><%=Util.formatCell(msgList.getE01SPVNSO())%></a></td>		        					
					<TD NOWRAP  ALIGN=left ><a href="javascript:GetInqDet('<%= ECC0160Help.getCurrentRow() %>')"><%= msgList.getE01CCRNAM() %> </a></td>					
					<TD NOWRAP  ALIGN=left ><%= msgList.getE01CCRNXN() %></td>
					<TD NOWRAP  ALIGN=left ><%= msgList.getE01SPVNUM() %></td>	 				
			        <TD nowrap width= align="left">
				        <INPUT type="HIDDEN" name="E01SPVTCD" size="4" value="<%= msgList.getE01SPVTCD().trim()%>">
							<% if (msgList.getE01SPVTCD().equals("0001")) out.print("Activacion de Tarjeta");
			   					 else if (msgList.getE01SPVTCD().equals("0002")) out.print("Ingreso Adicionales");
			   					 else if (msgList.getE01SPVTCD().equals("0003")) out.print("Cambio Cupo");
			   					 else if (msgList.getE01SPVTCD().equals("0004")) out.print("Bloque/Desbloqueo");
			   					 else if (msgList.getE01SPVTCD().equals("0005")) out.print("Reemision Tarjeta de Credito");
		    					 else if (msgList.getE01SPVTCD().equals("0006")) out.print("Cambio Codigo FV");
			    				 else if (msgList.getE01SPVTCD().equals("0007")) out.print("Pago Minimo/Pac Multibanco");
			    				 else if (msgList.getE01SPVTCD().equals("0008")) out.print("Reseteo de Clave"); 
			    				 else if (msgList.getE01SPVTCD().equals("0009")) out.print("Cambio Direccion envio EC"); 
			    				 else if (msgList.getE01SPVTCD().equals("0010")) out.print("Eliminación Tarjeta"); 
			    				 else if (msgList.getE01SPVTCD().equals("0011")) out.print("Renuncia");
			    				 else if (msgList.getE01SPVTCD().equals("0012")) out.print("Activación de Renovación");  
			    				 %>			    				 		    				 
	 		       </TD>
 				</TR>
		 <%
                }
              %>
        </table>
        </td>
        </tr>
    </table>
     
  <%}%>

<SCRIPT language="JavaScript">
  
//  showChecked("CURRCODE"); 

// function tableresize() {
//    adjustEquTables(headTable,dataTable,dataDiv,0,true);
//  }
//  tableresize();
//  window.onresize=tableresize;  

</SCRIPT>

</form>

</body>
</html>
