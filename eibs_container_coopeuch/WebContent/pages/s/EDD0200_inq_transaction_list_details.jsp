<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Transacciones PostVenta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%@ page import = "datapro.eibs.master.Util,datapro.eibs.beans.*" %>

<% datapro.eibs.beans.JBObjList actInqTrxlist = (datapro.eibs.beans.JBObjList)session.getAttribute("actInqTrxlist"); %>

<jsp:useBean id="msgSerch" class= "datapro.eibs.beans.EDD020001Message"  scope="session" />
<jsp:useBean id="Resumen" class= "datapro.eibs.beans.EDD020002Message"  scope="session" />
<jsp:useBean id="userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/CrossBrowserFunctions.js"> </SCRIPT>
<SCRIPT Language="Javascript">
function PrintPreview(fecha, hora,trxn,dipositivo,numRec,numTarj,monto,codAut,typTrxn,state) {

	var pg = '<%=request.getContextPath()%>/pages/s/ECC0200_activate_sales_list_print.jsp';
	CenterWindow(pg,720,500,2);

}
   
</SCRIPT>

</head>

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>


<h3 align="center">Detalle Transaccion<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="inq_transaction_list_details.jsp,EDD0200"></h3>
<hr size="4">
  <br/>
  <br/>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" class="tbhead" align="center">
          <tr id=trdark> 
            <td nowrap width="10%" align="right"> 
              <div align="right">Fecha Contable: </div>
            </td>
            <td nowrap width="10%" align="left">
	            <%=(msgSerch.getE01LOGFCD().length()>1?"":"0")+msgSerch.getE01LOGFCD()+"/"+(msgSerch.getE01LOGFCM().length()>1?"":"0")+msgSerch.getE01LOGFCM()+"/"+msgSerch.getE01LOGFCY()%>
            </td>
            <td nowrap width="10%" align="right"> 
              <div align="right">Canal : </div>
            </td>
			<td nowrap width="10%" align="left">
			<%=("ATMBECH".equals(msgSerch.getE01LOGCAJ())?"Banco Estado":("ATMTRBK".equals(msgSerch.getE01LOGCAJ())?"Transbank":("Redbanc"))) %>
			</td> 
            <td nowrap width="10%" align="right"> 
              <div align="right">Tipo de Consulta  : </div>
            </td>
			<td nowrap width="10%" align="left">
			<%= "D".equals(msgSerch.getE01FLGSUP())?"Detalle":"Resumen"%>
			</td> 			           
          </tr>          
        </table>
      </td>
    </tr>
  </table>  
<br>
    <TABLE class="tableinfo" >
       <tr id="trdark">
         <td colspan="2"><b>Detalle Transacción</b></td>
       </tr> 
       <TR id="trclear"> 
         <td align="right">
           Fecha Transacción : 
         </td>
         <td>
            <input type="text" id="fecha" name="fecha" size="12" alt="Fecha Transacción" readOnly="readonly" value="<%=request.getParameter("fecha")%>"/>
         </td> 
        </tr>
        <tr id="trdark" > 
         <td align="right">Hora Transacción :  </td>
         <td>
            <input type="text" id="hora" name="hora" size="12" alt="Hora Transacción" readOnly="readonly" value="<%=request.getParameter("hora")%>"/> (HHMMSS)
         </td>
         </tr>
       <TR id="trclear"> 
         <td align="right">
           Transacción : 
         </td>
         <td>
            <input type="text" id="trxn" name="trxn" size="30"  alt="Transacción" readOnly="readonly" value="<%=request.getParameter("trxn")%>" />
         </td> 
        </tr>
        <tr id="trdark" > 
         <td align="right">
            Dispositivo : 
         </td>
         <td >
            <input type="text" id="dipositivo" name="dipositivo" size="30" alt="Dispositivo" readOnly="readonly" value="<%=request.getParameter("dipositivo")%>"/>
         </td>
         </tr>

		<TR id="trclear"> 
         <td align="right">
           Numero Recibo : 
         </td>
         <td>
            <input type="text" id="numRec" name="numRec" size="30"  alt="Numero Recibo" readOnly="readonly" value="<%=request.getParameter("numRec")%>"/>
         </td> 
        </tr>
        <tr id="trdark" > 
         <td align="right">
            Numero Tarjeta : 
         </td>
         <td >
            <input type="text" id="numTarj" name="numTarj" size="30" alt="Numero Tarjeta" readOnly="readonly" value="<%=request.getParameter("numTarj")%>"/>
         </td>
         </tr>

		<TR id="trclear"> 
         <td align="right">
           Monto : 
         </td>
         <td>
            <input type="text" id="monto" name="monto" size="30"  alt="Monto" readOnly="readonly" value="<%=request.getParameter("monto")%>"/>
         </td> 
        </tr>
        <tr id="trdark" > 
         <td align="right">
            Codigo Autorización : 
         </td>
         <td >
            <input type="text" id="codAut" name="codAut" size="30" alt="Codigo Autorización" readOnly="readonly" value="<%=request.getParameter("codAut")%>"/>
         </td>
         </tr>
         
		<TR id="trclear"> 
         <td align="right">
           Tipo de Transacción : 
         </td>
         <td>
            <input type="text" id="typTrxn" name="typTrxn" size="30"  alt="Tipo de Transacción" readOnly="readonly" value="<%=request.getParameter("typTrxn")%>" />
         </td> 
        </tr>
        <tr id="trdark" > 
         <td align="right">
            Estado : 
         </td>
         <td >
            <input type="text" id="state" name="state" size="30" alt="Estado" readOnly="readonly" value="<%=request.getParameter("state")%>"/>
         </td>
         </tr>
		<tr id="trclear" > 
         <td align="right">
            Indicador de Pareo : 
         </td>
         <td >
            <input type="text" id="ipareo" name="ipareo" size="30" alt="Indicador de Pareo" readOnly="readonly" value="<%=request.getParameter("ipareo")%>"/>
         </td>
         </tr>         
		<TR id="trdark"> 
         <td align="right">
          &nbsp;
         </td>
         <td>
          &nbsp;
         </td> 
        </tr>         
      </table>
</body>
</html>
