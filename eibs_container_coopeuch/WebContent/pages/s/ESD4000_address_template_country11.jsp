<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@page import="com.datapro.constants.EibsFields"%>

<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<jsp:useBean id="entity" class="datapro.eibs.beans.ESD400001Message"  scope="session" />

<%!
	boolean readOnly=false;
%> 
          
<%      
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
     <tr id="trdark"> 
        <td nowrap width="40%"> 
           <div align="right">Direccion Principal :</div>
        </td>
        <td nowrap width="60%" > 
           <%	if (readOnly) {	%>
            <input type="text" name="E01MA2" readonly size="47" maxlength="45" value="<%= entity.getE01MA2().trim()%>" > 
           <% } else { %>  
              <input type="text" name="E01MA2" size="47" maxlength="45" value="<%= entity.getE01MA2().trim()%>" > 
           <% } 	%>
        </td>
     </tr> 
     <tr id="trclear"> 
        <td nowrap width="40%"> 
        </td>
        <td nowrap width="60%" > 
            <%	if (readOnly) {	%>
            <input type="text" name="E01MA3" readonly size="47" maxlength="45" value="<%= entity.getE01MA3().trim()%>" > 
           <% } else { %>  
              <input type="text" name="E01MA3" size="47" maxlength="45" value="<%= entity.getE01MA3().trim()%>" > 
           <% } 	%>
        </td>
     </tr>       
     <tr id="trdark"> 
        <td nowrap width="40%"> 
        </td>
        <td nowrap width="60%" > 
            <%	if (readOnly) {	%>
            <input type="text" name="E01MA4" readonly size="47" maxlength="45" value="<%= entity.getE01MA4().trim()%>" > 
           <% } else { %>  
              <input type="text" name="E01MA4" size="47" maxlength="45" value="<%= entity.getE01MA4().trim()%>" > 
           <% } 	%>
        </td>
     </tr>      
     <tr id="trclear">
        <td nowrap width="40%" > 
           <div align="right">Ciudad :</div>
        </td>
        <td nowrap  width="60%" > 
           <eibsinput:text name="entity" property="E01CTY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" required="false"  readonly="<%=readOnly %>"/>
        </td>
     </tr>    
     <tr id="trdark"> 
        <td nowrap width="40%" > 
           <div align="right">Estado :</div>
        </td>
        <td nowrap width="60%" > 
            <% if ( !readOnly ) { %>
               <eibsinput:cnofc name="entity" property="E01STE" required="false" flag="27" fn_code="E01STE" fn_description="D01STE"/>
            <% } %>
           <eibsinput:text property="D01STE" name="entity" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly %>" />
        </td>
     </tr>     
     <tr id="trclear">
        <td nowrap width="40%" > 
          <div align="right">Pa&iacute;s :</div>
        </td>
        <td nowrap  width="60%" >
	       <eibsinput:text name="entity" property="E01CTR" required="false" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" readonly="<%=readOnly %>"/>
        </td>
     </tr>     
     <tr id="trclear"> 
        <td nowrap width="40%" > 
           <div align="right">Apartado Postal :</div>
        </td>
        <td nowrap  width="60%" > 
           <eibsinput:text name="entity" property="E01POB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PO_BOX %>" readonly="<%=readOnly %>"/>
        </td>
     </tr>
     <tr id="trdark"> 
        <td nowrap width="40%" > 
          <div align="right">C&oacute;digo Postal : </div>
        </td>
        <td nowrap  width="60%" > 
          <eibsinput:text name="entity" property="E01ZPC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ZIPCODE %>" readonly="<%=readOnly %>"/>
        </td>   
     </tr>
          