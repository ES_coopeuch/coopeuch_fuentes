package com.datapro.eibs.security.servlet;

/**
 * Insert the type's description here.
 * Creation date: (7/19/00 6:55:55 PM)
 * @author: Enrique Almonte
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Set;
import java.util.Iterator;

import com.datapro.eibs.security.facade.*;
//import com.datapro.eibs.security.access.jdbc.bean.*;
import com.datapro.eibs.security.bean.*;
import com.datapro.eibs.security.object.*;
import com.datapro.generic.beanutil.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;


public class JSMenuProfile extends datapro.eibs.master.SuperServlet {

    //Requests
	protected static final int R_USERS_INFO = 1;  //CTRLBTH + CTRLEUP
	protected static final int R_USERS_PRIVILEGES = 3; //CTRLPVL
	protected static final int R_USERS_BRANCH = 5; //CTRLDBA
	
	//Actions
	protected static final int A_USERS_INFO = 2;  //CTRLBTH + CTRLEUP
	protected static final int A_USERS_PRIVILEGES = 4; //CTRLPVL
	protected static final int A_USERS_BRANCH = 6; //CTRLDBA
	
	protected static final int R_UPDATE_USER = 10;
	protected static final int A_UPDATE_USER = 20;
	
	protected static final int A_GROUP_USER = 8;
	
	protected static final String PAGE_USER_MAINT = "UserProfile_maint.jsp";
	protected static final String PAGE_USER_BRANCH = "UserProfile_branch.jsp";
	protected static final String PAGE_USER_PRIVILEGES = "UserProfile_privileges.jsp";
	
	protected String LangPath = "S";
	
	/**
	 * JSEODPDF constructor comment.
	 */
	public JSMenuProfile() {
		super();
	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		//dbProp = new DBConnectorProperty();
	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {

			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}

		} else {
			try {

				msgUser = (ESS0030DSMessage) session.getAttribute("currUser");

				LangPath = super.rootPath + msgUser.getE01LAN() + "/";
				
				int screen = R_USERS_INFO;
				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}

				switch (screen) {
					case R_USERS_INFO :
						procReqUsers(req, res, session);
						break;
					case R_USERS_BRANCH :
						procReqUsersBranch(req, res, session);
						break;
					case R_USERS_PRIVILEGES :
						procReqUsersPrivileges(req, res, session);
						break;
					case A_UPDATE_USER :
						procActionUpdateUser(req, res, session);
						break;
					case A_USERS_BRANCH :
						procActionUsersBranch(req, res, session);
						break;
					case A_USERS_PRIVILEGES :
						procActionUsersPrivileges(req, res, session);
						break;
					case A_GROUP_USER :
						procActionUsers(req, res, session);
						break;
					default :
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}
		}

	}
	
	protected void procReqUsers(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		USERBean userInfo = new USERBean();
		UserProfile userMng = new UserProfile();
		//DynamicDTO dto = null;
		try {
			
				String userID= req.getParameter("userid");
				//dto = userMng.getUser(userID);
				//dto.unload(userInfo);
				userInfo = userMng.getUser(userID);
				if (userInfo != null) {
					ses.setAttribute("userInfo",userInfo);
					try {
						flexLog("About to call Page: " + LangPath + PAGE_USER_MAINT);
						callPage(LangPath + PAGE_USER_MAINT, req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else {
					
				}

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	protected void procReqUsersBranch(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		JBObjList userBranchList = new JBObjList();
		UserProfile userMng = new UserProfile();
		BranchProfile branchMng = new BranchProfile();
		CNTRLBRN branch = new CNTRLBRN();
		try {			
				String userID= req.getParameter("userid");				
				String defbnk= req.getParameter("defbank");
				String defbrn= req.getParameter("defbranch");
				String defnme="";
				userBranchList = userMng.getUserBranch(userID);
				if (userBranchList != null) {
					ses.setAttribute("branchList",userBranchList);
					branch = branchMng.getBranch(defbnk,defbrn);
					if (branch !=null) defnme=branch.getBRNNME();
					try {
						flexLog("About to call Page: " + LangPath + PAGE_USER_BRANCH);
						res.sendRedirect(super.srctx + LangPath + PAGE_USER_BRANCH + "?defBNK=" + defbnk + "&defBRN=" + defbrn + "&defNME=" + defnme);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else {
					
				}

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	protected void procActionUsersBranch(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		JBObjList userBranchList = new JBObjList();
		UserProfile userMng = new UserProfile();
		BranchProfile branchMng = new BranchProfile();
		CNTRLDBANME branch = null;
		JBObjList branchList = new JBObjList();
		USERBean userBean = null;
		try {			
				String action= req.getParameter("ACTION");
				String userID= req.getParameter("DBAUSR");
				String defbnk= req.getParameter("DDBABNK");
				String defbrn= req.getParameter("DDBABRN");				
				if (action.equals("D") || action.equals("S")) { //Delete || Set default
					int row = Integer.parseInt(req.getParameter("ROW"));					
					
					String defnme="";
					userBranchList = (JBObjList)ses.getAttribute("branchList");
					userBranchList.setCurrentRow(row);					
					branch = (CNTRLDBANME) userBranchList.getRecord();
					if (action.equals("D")) {
						int result = userMng.deleteUserBranch(branch.getDBAUSR(),branch.getDBABNK(),branch.getDBABRN().toString(),branch.getDBAALL());
						if (result != 0) {
							res.sendRedirect(super.srctx + "/servlet/com.datapro.eibs.security.servlet.JSUserProfile?SCREEN=5&userid="+ userID + "&defbank=" + defbnk + "&defbranch=" + defbrn);
						}
					} else {
						defbnk= branch.getDBABNK();
						
						if (branch.getDBAALL().equals("*")) {
							defbrn = req.getParameter("NEWBRANCH");
						} else {
							defbrn = branch.getDBABRN().toString();					 
						}
						
						CNTRLBRN brnBean = branchMng.getBranch(defbnk,defbrn);
						defnme = brnBean.getBRNNME();
						//get Cost Center
						//String ccost= brnBean.getBRNF01();
						//Update CNTRLBTH
						userBean = (USERBean) ses.getAttribute("userInfo");
						userBean.setBTHUBK(defbnk);
						userBean.setBTHUBR(new BigDecimal(defbrn));
						int result= userMng.update(userBean,new CNTRLBTH());
						if (result !=0) {
							ses.setAttribute("userInfo",userBean);
						}
						try {
							flexLog("About to call Page: " + LangPath + PAGE_USER_BRANCH);
							res.sendRedirect(super.srctx + LangPath + PAGE_USER_BRANCH + "?defBNK=" + defbnk + "&defBRN=" + defbrn + "&defNME=" + defnme);	
						}
						catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
						
					}
				} else { //Add branch
					String newBank= req.getParameter("NEWBANK");
					String newBranch= req.getParameter("NEWBRANCH");
					String newFlag= req.getParameter("NEWFLAG");
					int result= 0;
					if ((userMng.getUserBranch(userID,newBank,newBranch,newFlag) == null) && (userMng.getUserBranch(userID,newBank,"0","*") == null)){
						if (newFlag.equals("*")) {
							result = userMng.deleteUserBranch(userID,newBank);
						}
						result= userMng.addUserBranch(userID,newBank,newBranch,newFlag);		
					} else {
						//The branch already exists
					}
					res.sendRedirect(super.srctx + "/servlet/com.datapro.eibs.security.servlet.JSUserProfile?SCREEN=5&userid="+ userID + "&defbank=" + defbnk + "&defbranch=" + defbrn);
						
				}			

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	protected void procActionUpdateUser(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
        
        USERBean formBean = null;        
        UserProfile userMng = new UserProfile();
        
        formBean = (USERBean) ses.getAttribute("userInfo");
        
		BeanParser bp = new BeanParser(formBean);
		
		int result = 0;
		
		Set ks = bp.getTypes().keySet();
		for (Iterator iter = ks.iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			bp.setString(name, req.getParameter(name));
			//System.out.println(name + "(" + bp.getTypes().get(name) + ") =" + bp.get(name));
		}		
		
		
        result = userMng.update(formBean);
        
        if (result !=0) {
        	res.sendRedirect(super.srctx + "/servlet/com.datapro.eibs.security.servlet.JSUserProfile?SCREEN=1&userid=" + formBean.getEUPUSR());
        }

	}

	protected void procReqUsersPrivileges(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		JBObjList userPriviList = new JBObjList();
		JBObjList grpPriviList = new JBObjList();
		UserProfile userMng = new UserProfile();
		USERBean userInfo = null;
		try {			
				String userID= req.getParameter("userid");
								
				userInfo = (USERBean) ses.getAttribute("userInfo");
				userPriviList = userMng.getUserPrivilege(userID);
				grpPriviList = userMng.getUserPrivilege(userInfo.getBTHF03());				
				ses.setAttribute("priviList",userPriviList);
				ses.setAttribute("grpPriviList",grpPriviList);
				try {
						flexLog("About to call Page: " + LangPath + PAGE_USER_PRIVILEGES);
						callPage(LangPath + PAGE_USER_PRIVILEGES,req,res);
					}
				catch (Exception e) {
						flexLog("Exception calling page " + e);
				}
				

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	protected void procActionUsersPrivileges(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		JBObjList userPriviList = new JBObjList();
		UserProfile userMng = new UserProfile();
		CNTRLPVLNME privilege = null;
		USERBean userBean = null;
		try {			
				String action= req.getParameter("ACTION");
				String userID= req.getParameter("PVLUSR");
							
				if (action.equals("D") || action.equals("M")) { //Delete || Modify
					int row = Integer.parseInt(req.getParameter("ROW"));					
					
					userPriviList = (JBObjList)ses.getAttribute("priviList");
					userPriviList.setCurrentRow(row);
					privilege = (CNTRLPVLNME)	userPriviList.getRecord();
					int result=0;			
					if (action.equals("D")) {
						result = userMng.deleteUserPrivilege(userID,privilege.getPVLPVL());
					} else {												
						String newVal = req.getParameter("NEWVAL");
						result = userMng.updateUserPrivilege(userID,privilege.getPVLPVL(),newVal);					
					}																			
					if (result != 0) {
						res.sendRedirect(super.srctx + "/servlet/com.datapro.eibs.security.servlet.JSUserProfile?SCREEN=3&userid="+ userID);
					}	
					
				} else { //Add Privilege
					String newVal= req.getParameter("NEWVAL");
					String newPvl= req.getParameter("NEWPVL");
					int result= 0;
					if (userMng.getUserPrivilege(userID,newPvl) == null ){
						result= userMng.addUserPrivilege(userID,newPvl,newVal);		
					} else {
						//The privilege already exists
					}
					res.sendRedirect(super.srctx + "/servlet/com.datapro.eibs.security.servlet.JSUserProfile?SCREEN=3&userid="+ userID);
						
				}			

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	protected void procActionUsers(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		JBObjList userList = new JBObjList();
		UserProfile userMng = new UserProfile();
		CNTRLBTH bthBean = null;
		CNTRLEUP eupBean = null;
		ESS0030DSMessage user = null;
		try {			
				String action= req.getParameter("ACTION");
				String userID= req.getParameter("USERID");
				String groupID= req.getParameter("GROUPID");
				String newURL = "";
				int row = Integer.parseInt(req.getParameter("ROW"));
				int rowGrp = Integer.parseInt(req.getParameter("ROWGRP"));
				int result=0;
							
				if (action.equals("1") ) { //Delete 
										
					result = userMng.delete(userID);
					newURL = "/servlet/com.datapro.eibs.security.servlet.JSGroupProfile?SCREEN=5&GROUPID=" + groupID;
					
				} else if (action.equals("2")) { //Move Group
					
					userList = (JBObjList) ses.getAttribute("userList"); 
					userList.setCurrentRow(row);
   		 			bthBean = (CNTRLBTH) userList.getRecord();
					bthBean.setBTHF03(groupID);
					
					result = userMng.update(bthBean);
					newURL = "/pages/s/GroupProfile_grouplist.jsp?ROW=" + rowGrp;
				
				} else { //new user
					String err = "&ERR=0";
					if (userMng.getUser(userID) == null) {
						bthBean = new CNTRLBTH();
						eupBean = new CNTRLEUP();
						user = (ESS0030DSMessage) ses.getAttribute("currUser");
						bthBean.setBTHF03(groupID); //group
						bthBean.setBTHKEY(userID);  //id
						bthBean.setBTHUBK(user.getE01UBK());
						bthBean.setBTHUBR(user.getBigDecimalE01UBR());
						bthBean.setBTHCCN(user.getBigDecimalE01CCN());
						eupBean.setEUPUSR(userID);  //id
						eupBean.setEUPSTS("1"); //Status 1 = Active
						result = userMng.addUser(bthBean,eupBean);
					} else {
						result = 1;
						err = "&ERR=1";
					}
					newURL = "/pages/s/GroupProfile_grouplist.jsp?ROW=" + rowGrp + err;
					
				}
				
				if (result != 0) {
					res.sendRedirect(super.srctx + newURL);
				}			
							

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
}
