<!-- Hecho por Alonso Arana ------Datapro-----15/05/2014 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<html>
<head>
<title>Informacion Basica</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="JavaScript">
function addDate(){
date = new Date();
var month = date.getMonth()+1;
var day = date.getDate();
var year = date.getFullYear();


if (document.getElementById('fecha1').value == ''){
document.getElementById('fecha1').value = day;
}

if (document.getElementById('fecha2').value == ''){
document.getElementById('fecha2').value = month;
}


if (document.getElementById('fecha3').value == ''){
document.getElementById('fecha3').value = year;
}

if (document.getElementById('fecha4').value == ''){
document.getElementById('fecha4').value = day;
}


if (document.getElementById('fecha5').value == ''){
document.getElementById('fecha5').value = month;
}

if (document.getElementById('fecha6').value == ''){
document.getElementById('fecha6').value = year;
}


}



function goAction(op) {
var ok = false;

if(op=='300'){
	document.forms[0].SCREEN.value = op;
			document.forms[0].submit();	
}
	if (op != '300'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01RCCTCD") 
	      	{	
	      		if (element.checked == true) {
	      			document.getElementById("codigo_lista").value = element.value; 
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = true;
      	
      	if (op =='500'){
      		confirm1 = confirm("�Desea eliminar el registro seleccionado?");
      	}
      	
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		

     } else {
		alert("Debe seleccionar un banco para continuar.");	   
	 }   
	
	}
	




</script>


<jsp:useBean id= "AcreList" class= "datapro.eibs.beans.ERM031001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" 	scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" 	scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage"  scope="session" />


</head>

<body bgcolor="#FFFFFF"  >

<%if (!error.getERRNUM().equals("0")) {
	error.setERRNUM("0");
	out.println("<SCRIPT Language=\"Javascript\">");
	out.println("       showErrors()");
	out.println("</SCRIPT>");
	}
 
%>





<h3 align="center">Control Acreencias<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="list, ERM0310"></h3>
<hr size="4">
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERM0310">
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="300"> 
<input type="hidden" name="codigo_lista" value="" id="codigo_lista">  

 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="left" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">   Banco : 
              </td>
             <td nowrap width="5%" align="left">
	  			<input type="text" name="E01ACRBNK" value="<%=AcreList.getE01ACRBNK()%>" 
	  			size="2"  readonly>
             </td>          
          <td nowrap width="10%" align="left">
	  			<input type="text" name="E01DESBNK" value="<%=AcreList.getE01DESBNK()%>" 
	  			size="50"  readonly>
             </td> 
         
         </tr>
         
      
        </table>
      </td>
    </tr>
  </table>
	      <p>&nbsp;</p>
	<table class="tbenter" width="100%">
	<tr>
	
	
	
	
	
		<td align="center" class="tdbkg" width="10%"> 
		<a href="javascript:goAction('400')"><b>Modificar</b></a>
		</td>
	
		
		
		
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('500')"><b>Eliminar</b></a>
		</td>
		
		
	</tr>
</table>
	

<%
	if (AcreList.isEmpty()) {
%>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>



<%
	
	
	} else {
%>

  <p>&nbsp;</p>
<table id="headTable"  width="" align="center">
		<tr id="trdark">
			<th align="center" nowrap width="30"></th>
			<th align="center" nowrap width="300">C�digo Banco</th>
			<th align="center" nowrap width="500">Nombre Banco</th>
		
		</tr>
		
		<%
			
		%>
		<tr>
		
		<td nowrap><input type="radio" name="E01RCCTCD" id="radio_key" value="1" /></td>
		<td nowrap align="left"><%=AcreList.getE01ACRBNK() %></td>
 	 		<td nowrap align="left"><%=AcreList.getE01DESBNK() %></td>
	
		</tr>
		<%
			}
		%>
	</table>
	</FORM>
	
	</body>
	
	</html>
	
	