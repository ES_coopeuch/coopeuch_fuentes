//
// Generated By:JAX-WS RI IBM 2.1.1 in JDK 6 (JAXB RI IBM JAXB 2.1.3 in JDK 1.6)
//


package cl.coopeuch.core.tiendavirtual.serviciotiendavirtual;

import javax.xml.ws.WebFault;
import cl.coopeuch.core.tiendavirtual.FaultMessage;

@WebFault(name = "faultMessage", targetNamespace = "http://tiendavirtual.core.coopeuch.cl")
public class FaultMsg
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private FaultMessage faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public FaultMsg(String message, FaultMessage faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param message
     * @param cause
     */
    public FaultMsg(String message, FaultMessage faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: cl.coopeuch.core.tiendavirtual.FaultMessage
     */
    public FaultMessage getFaultInfo() {
        return faultInfo;
    }

}
