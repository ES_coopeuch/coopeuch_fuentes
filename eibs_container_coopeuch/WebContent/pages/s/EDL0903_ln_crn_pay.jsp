<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EDL090302Message"%>

<HTML>
<HEAD>
<TITLE>Pr&eacute;stamos - Consulta de Plan de Pagos Original</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<jsp:useBean id="header" class="datapro.eibs.beans.EDL090301Message"  scope="session" />
<jsp:useBean id= "list" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
 
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 

<SCRIPT Language="Javascript">

function goAction(op) {

     document.forms[0].opt.value = op;
     var formLength= document.forms[0].elements.length;
     var ok = false;
     for(n=0;n<formLength;n++)
     {
      	var elementName= document.forms[0].elements[n].name;
      	if(elementName == "ACCNUM") 
      	{
        		ok = true;
        		break;
      	}
      }
	 if ( ok || (op == 3) ) {
          document.forms[0].submit();
     }
     else {
			alert("Seleccione una cuenta antes de realizar esta operación");	   
     }

  }
</SCRIPT>

</HEAD>

<BODY>

<SCRIPT>
<%if (!userPO.getOption().equals("PROP")){%>
	initMenu(); 
<%   
}
%>
</SCRIPT>

<form>
  <input TYPE=HIDDEN name="SCREEN" value="500">
  <INPUT TYPE=HIDDEN NAME="opt" VALUE="1">


  
  <h3 align="center">Pr&eacute;stamos - Consulta de Plan de Pagos Original<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="ln_crn_pay.jsp,EDL0903"> 
  </h3>
  <hr size="4">

<h4></h4>

  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trclear"> 
            <td nowrap width="14%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="9%" > 
              <div align="left"> 
                <input type="text" name="E02CUN2" size="10" maxlength="9" readonly value="<%= header.getE01DEACUN().trim()%>">
              </div>
            </td>
            <td nowrap width="12%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E02NA12" size="45" maxlength="45" readonly value="<%= header.getE01CUSNA1().trim()%>">
              </div>
            </td>
            <td nowrap > 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap ><b> 
              <input type="text" name="E02PRO2" size="4" maxlength="4" readonly value="<%= header.getE01DEAPRO().trim()%>">
              </b></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="14%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="9%"> 
              <div align="left"> 
                <input type="text" name="E02ACC" size="13" maxlength="12" value="<%= header.getE01DEAACC().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="12%"></td>
            <td nowrap width="33%"></td>
            <td nowrap width="11%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="21%"> 
              <div align="left"><b> 
                <input type="text" name="E01DEACCY" size="3" maxlength="3" value="<%= header.getE01DEACCY().trim()%>" readonly>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>&nbsp;</h4>

  <TABLE class="tableinfo"  cellpading=2 cellspacing=3>
    <TR id=trdark> 
      <TH ALIGN=CENTER nowrap   >Nro<br>Cuota</TH>
      <TH ALIGN=CENTER nowrap   >Fecha <br>a Pagar</TH>
      <TH ALIGN=CENTER nowrap   >Principal</TH>
      <TH ALIGN=CENTER nowrap   >Inter&eacute;s</TH>
      <TH ALIGN=CENTER nowrap   >Otros<br>Cargos</TH>
      <TH ALIGN=CENTER nowrap   >Total<br>Cuota</TH>
	 <%if (session.getAttribute("TP").equals("CV")) {%>
      <TH ALIGN=CENTER nowrap  >Saldo</TH>
   	 <%}%>
      <TH ALIGN=CENTER nowrap  >Est.</TH>
      <TH ALIGN=CENTER nowrap   >Dias<br>Ven.</TH>
      <TH ALIGN=CENTER nowrap  >Fecha<br>Pago</TH>
      <TH ALIGN=CENTER nowrap  >Monto<br>Pagado</TH>
    </TR>
    <%
                list.initRow();
                while (list.getNextRow()) {
                					EDL090302Message regplan = (EDL090302Message) list.getRecord();
                
 	%> 
    <TR id=trclear> 
      <TD ALIGN=CENTER  nowrap   >
      		<div align="center"><%= regplan.getE02DLPPNU() %></div>
      </TD>

      <TD ALIGN=CENTER  nowrap  >
      		<div align="right"><%= Util.formatDate(regplan.getE02DLPPDD(),regplan.getE02DLPPDM(),regplan.getE02DLPPDY()) %></div>
      </TD>

      <TD ALIGN=CENTER  nowrap > 
        <div align="right"><%= Util.formatCCY(regplan.getE02DLPPPM()) %></div>
      </TD>
      
      <TD ALIGN=CENTER  nowrap > 
        <div align="right"><%= Util.formatCCY(regplan.getE02DLPIPM()) %></div>
      </TD>

      <TD ALIGN=CENTER  nowrap > 
        <div align="right"><%= Util.formatCCY(regplan.getE02DLPPIA()) %></div>
      </TD>

      <TD ALIGN=CENTER  nowrap > 
        <div align="right"><%= Util.formatCCY(regplan.getE02DLPTOT()) %></div>
      </TD>
	 <%if (session.getAttribute("TP").equals("CV")) {%>
      <TD ALIGN=CENTER  nowrap > 
        <div align="right"><%= Util.formatCCY(regplan.getE02SALCRE()) %></div>
      </TD>
   	 <%}%>

      <TD ALIGN=CENTER  nowrap > 
        <div align="right"><%= Util.formatCCY(regplan.getE02DLPFL2()) %></div>
      </TD>

      <TD ALIGN=CENTER  nowrap > 
        <div align="right"><%= Util.formatCCY(regplan.getE02DLPVEN()) %></div>
      </TD>

      <TD ALIGN=CENTER  nowrap  >
      		<div align="right"><%= Util.formatDate(regplan.getE02DLPDTD(),regplan.getE02DLPDTM(),regplan.getE02DLPDTY()) %></div>
      </TD>

      <TD ALIGN=CENTER  nowrap > 
        <div align="right"><%= Util.formatCCY(regplan.getE02DLPPAG()) %></div>
      </TD>

    </TR>
    <%
 		}
    %> 
  </TABLE>
  

</FORM>

</BODY>
</HTML>
