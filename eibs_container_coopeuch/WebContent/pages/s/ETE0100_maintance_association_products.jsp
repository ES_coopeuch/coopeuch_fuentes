<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ETE010003Message"%>
<%@page import="com.datapro.constants.EibsFields"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Transferencias - Seguridad y Parámetros</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="msgtransa" class= "datapro.eibs.beans.ETE010002Message"  scope="session" />
<jsp:useBean id="AprObj" class= "datapro.eibs.beans.ETE010003Message"  scope="session" />
<jsp:useBean id="listproductos" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">
function goAction3(op) {

 	document.forms[0].SCREEN.value = op;
	document.forms[0].submit();		
}
</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Trasferencias - Seguridad y Par&aacute;metros
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="maintance_association_products.jsp, ETE0100">
</h3>

<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.tefmulti.JSETE0100">
<input type="hidden" name="SCREEN" value="100">
<input type="hidden" name="E03ICTIND" value="<%=msgtransa.getH02FLGWK1().trim()%>" size="5" maxlength="4" readonly >


<table  class="tableinfo">
	<tr bordercolor="#FFFFFF"> 
		<td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Tipo de Transacci&oacute;n : 
              </td>
             <td nowrap width="10%" align="left">
               <eibsinput:text name="msgtransa" property="E02LIFCOD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>"  readonly="true" />	
               <eibsinput:text name="msgtransa" property="E02LIFDCO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>"  readonly="true" />	
             </td>
         </tr>
        </table>
      </td>
    </tr>
</table>

<table  class="tableinfo">
	<tr bordercolor="#FFFFFF"> 
		<td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">Producto Origen : 
              </td>
             <td nowrap width="90%" align="left" >
       			 <div align="left"> 
 	     			  <input type="hidden" name="E03ICTCOD" value="<%if(!userPO.getPurpose().equals("NEW")) out.print(AprObj.getE03ICTCOD()); else out.print(msgtransa.getE02LIFCOD());%>" size="5" maxlength="4" readonly >
 	     			  <input type="text" name="E03ICTPRO" value="<%= AprObj.getE03ICTPRO()%>" size="5" maxlength="4" readonly >
        			   <input type="text" name="E03ICTDRO" value="<%= AprObj.getE03ICTDRO()%>" size="50" maxlength="45"  readonly >
 <%if(!userPO.getPurpose().equals("MAINTENANCE")) 
 {%>               
	   				    <a href="javascript:GetCodeDescCNOFC('E03ICTPRO','E03ICTDRO','TM')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
	     			   <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >  
 <%}%>               
 
        		</div>
             </td>
         </tr>
          <tr>
             <td nowrap width="10%" align="right"> Producto Destino : 
              </td>
             <td nowrap width="90%" align="left" >
       			 <div align="left"> 
 	     			  <input type="text" name="E03ICTPRD" value="<%= AprObj.getE03ICTPRD()%>" size="5" maxlength="4" readonly>
        			   <input type="text" name="E03ICTDRD" value="<%= AprObj.getE03ICTDRD()%>" size="50" maxlength="45"  readonly >
        			   <%if(msgtransa.getH02FLGWK1().equals("0")) { %>
	   				    	<a href="javascript:GetCodeDescCNOFC('E03ICTPRD','E03ICTDRD','TO')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
	   				   <% } else { %>
	   				    	<a href="javascript:GetCodeDescCNOFC('E03ICTPRD','E03ICTDRD','X3')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
	   				   <% } %> 	
	     			   <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >  
        		</div>
             </td>
         </tr>
	</table>
	</td>
	</tr>
</table>

<p align="center">
	<input id="EIBSBTN" type="button" name="Enviar" value="Enviar" onclick="goAction3('1100')">
	<input id="EIBSBTN" type="button" name="Volver" value="Volver" onclick="goAction3('200')">
	
</p>

<h4>Relaci&oacute;n Productos Actuales</h4>
<hr style="  border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));  "/>

	<table id="headTable"  width="100%" align="left">
		<tr id="trdark">
			<th align="center" nowrap width="30%">Producto Origen</th>
			<th align="center" nowrap width="30%">Producto / Instituci&oacute;n Financiera Destino</th>
			<th align="center" nowrap width="40%"></th>
		</tr>
		<%
			listproductos.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (listproductos.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ETE010003Message regtrans = (ETE010003Message) listproductos.getRecord();
		%>
		<tr>
			<td nowrap align="left">
				<%=regtrans.getE03ICTPRO()%> - <%=regtrans.getE03ICTDRO() %>
			</td>

			<td nowrap align="left">
				<%=regtrans.getE03ICTPRD()%> - <%=regtrans.getE03ICTDRD()  %>
			</td>
		    
		    <td>
		    </td>
		</tr>
		<%
			}
		%>
	</table>

</form>
</body>
</html>
