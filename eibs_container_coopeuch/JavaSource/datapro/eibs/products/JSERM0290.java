package datapro.eibs.products;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERC200001Message;
import datapro.eibs.beans.ERC200002Message;
import datapro.eibs.beans.ERM029001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

/**
 * Servlet implementation class JSERM0290
 * 
 * hecho por Alonso Arana ---------------28/02/2014-------------------Datapro-----------------------------
 * 
 */
public class JSERM0290 extends JSEIBSServlet {
	private static final long serialVersionUID = 1L;

	protected static final int A_enter_remain=100;
	protected static final int A_list_remain=200;
   protected static final int  A_grab_remain=300;
	
			protected void processRequest(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res,
					HttpSession session, int screen) throws ServletException,
					IOException {
			
				switch (screen) {
				case A_enter_remain:
					procReqRemain(user, req, res, session);
					break;
					
				case A_list_remain:
					RemainProcess(user, req, res, session);
				
					break;
					
				case A_grab_remain:
					
					procActionRemainResponse(user, req, res, session);
					break;
					
					
				default:
					forward(SuperServlet.devPage, req, res);
					break;
				}
				
				
			}
				
			
			protected void procReqRemain(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession ses)
					throws ServletException, IOException {
				UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
				ses.setAttribute("userPO", userPO);
				try {
					flexLog("About to call Page: ERM0290_enter_remain.jsp");
					forward("ERM0290_enter_remain.jsp", req, res);
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Exception calling page " + e);
				}
			}
			
			
			
			
			protected void RemainProcess(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					mp = getMessageProcessor("ERM0290", req);

					ERM029001Message msgList = (ERM029001Message) mp.getMessageRecord("ERM029001", user.getH01USR(), "0002");
					//Sets the employee  number from either the first page or the maintenance page
			
					if (req.getParameter("E01REMBNK")!= null){
										
				
						msgList.setE01REMBNK(req.getParameter("E01REMBNK"));
						
						
					
					} 

					//Sends message
					mp.sendMessage(msgList);
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
					msgList = (ERM029001Message) mp.receiveMessageRecord();
				
				
					session.setAttribute("RteList", msgList);
					
					if (mp.hasError(error)) { // if there are errors go back to first page
						
				
						session.setAttribute("error", error);
						
						flexLog("About to call Page: ERM0290_enter_remain.jsp");
						forward("ERM0290_enter_remain.jsp", req, res);
					} else {
					
						forward("ERM0290_remain_list.jsp", req, res);

				
					}


				} finally {
					if (mp != null)
						mp.close();
				}
			}
			
			
			
			protected void procActionRemainResponse(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {					
					mp = getMessageProcessor("ERM0290", req);
					ERM029001Message msg = (ERM029001Message) mp.getMessageRecord("ERM029001", user.getH01USR(), "0004");
					//Sets the employee  number from either the first page or the maintenance page
			
					setMessageRecord(req, msg);
					session.removeAttribute("RteList");
					session.setAttribute("RteList",msg);
					
					
					flexLog("mensaje ida"+msg);
					
					mp.sendMessage(msg);
					
					//session.removeAttribute("RteList");
				
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();		
					
					msg = (ERM029001Message) mp.receiveMessageRecord();
					
					
					session.setAttribute("RteList", msg);
					if (mp.hasError(error)) { // if there are errors go back to first page
						
						session.setAttribute("error", error);
						flexLog("About to call Page: ERM0290_remain_list.jsp");
						forward("ERM0290_remain_list.jsp", req, res);
					} else {
				
						session.removeAttribute("RteList");
						redirectToPage("/servlet/datapro.eibs.products.JSERM0290?SCREEN=100", res);
					    
					} 


				} finally {
					if (mp != null)
						mp.close();
				}
			}

			
			
			
			
			
	}
	

