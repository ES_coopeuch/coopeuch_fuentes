<html> 
<head>
<title>Cambio de Producto</title>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session"/>
<jsp:useBean id="brnDetails" class="datapro.eibs.beans.ESD090001Message"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript">

</script>

</head>
<body>

 
 <% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     error.setERRNUM("0");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
    }
%>
  <h3 align="center">Cambio de Producto<BR>Cambio Masivo<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="product_change_massive_inq.jsp, ESD0900">
  </h3>
  <hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0900" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <INPUT TYPE=HIDDEN NAME="CHANGE" VALUE="M">
  <INPUT TYPE=HIDDEN NAME="E01CHGTYP" VALUE="<% if (!brnDetails.getE01CHGTYP().equals("")) { out.print(brnDetails.getE01CHGTYP()); } else { out.print("2"); } %>">
  <INPUT TYPE=HIDDEN NAME="E01CHGACC" VALUE="<%= brnDetails.getE01CHGACC() %>">

  <table class="tableinfo">
      <tr> 
        <td nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap width="10%"> 
              </td>
              <td nowrap width="15%"> 
                <div align="right">C�digo de Banco :</div>
              </td>
              <td nowrap width="75%">  
                <input type="text" name="E01CHGBNK" size="3" maxlength="2" value="<%= brnDetails.getE01CHGBNK() %>" readonly>
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="10%"> 
              </td>
              <td nowrap width="15%"> 
                <div align="right">M�dulo :</div>
              </td>
              <td nowrap width="75%">  
               	<input type="text" name="E01CHGACD" size="3" maxlength="2" value="<%= brnDetails.getE01CHGACD() %>" readonly>
               	<input type="text" name="E01MODDSC" size="45" maxlength="45" value="<%= brnDetails.getE01MODDSC() %>" readonly>
              </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="10%"> 
              </td>
              <td nowrap width="15%"> 
                <div align="right">Tipo de Producto :</div>
              </td>
              <td nowrap width="75%">  
               	<input type="text" name="E01CHGPRT" size="5" maxlength="4" value="<%= brnDetails.getE01CHGPRT() %>" readonly>
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="10%" rowspan="2" align="right"> 
                <div align="right">
                <input type="radio" name="MASSIVE" value="P" onClick="javascript:optionClick('P')" <% if (brnDetails.getE01CHGTYP().equals("2")) { out.print("checked"); } %> <% if (!brnDetails.getE01CHGTYP().equals("2") && userPO.getPurpose().equals("MAINTENANCE")) { out.print("disabled"); } %>>
                Producto</div>
              </td>
              <td nowrap width="15%"> 
                <div align="right">Producto Actual :</div>
              </td>
              <td nowrap width="75%">  
                <div id="field01">
                	<input type="text" name="E01CHGPRO" size="5" maxlength="4" value="<%= brnDetails.getE01CHGPRO() %>" readonly>
                	<input type="text" name="E01PRDDSC" size="45" maxlength="45" value="<%= brnDetails.getE01PRDDSC() %>" readonly>
              	</div>
              </td>
            </tr>
            <tr id="trclear">
              <td nowrap width="15%"> 
                <div align="right">Nuevo Producto :</div>
              </td>
              <td nowrap width="75%">  
                <div id="field02">
                	<input type="text" name="E01CHGPRC" size="5" maxlength="4" value="<%= brnDetails.getE01CHGPRC() %>" readonly>
                	<input type="text" name="E01NEWPRD" size="45" maxlength="45" value="<%= brnDetails.getE01NEWPRD() %>" readonly>
              	</div>
              </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="10%" rowspan="2" align="right"> 
                <div align="right">
                <input type="radio" name="MASSIVE" value="B" onClick="javascript:optionClick('B')" <% if (brnDetails.getE01CHGTYP().equals("3")) { out.print("checked"); } %> <% if (!brnDetails.getE01CHGTYP().equals("3") && userPO.getPurpose().equals("MAINTENANCE")) { out.print("disabled"); } %>>
                Sucursal</div>
              </td>
              <td nowrap width="15%"> 
                <div align="right">Sucursal Anterior :</div>
              </td>
              <td nowrap width="75%">  
                <div id="field03">
                	<input type="text" name="E01CHGOBR" size="5" maxlength="4" value="<%= brnDetails.getE01CHGOBR() %>" readonly>
                	<input type="text" name="E01OLDBRN" size="45" maxlength="45" value="<%= brnDetails.getE01OLDBRN() %>" readonly>
              	</div>
              </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="15%"> 
                <div align="right">Nueva Sucursal :</div>
              </td>
              <td nowrap width="75%">  
                <div id="field04">
                	<input type="text" name="E01CHGNBR" size="5" maxlength="4" value="<%= brnDetails.getE01CHGNBR() %>" readonly>
                	<input type="text" name="E01NEWBRN" size="45" maxlength="45" value="<%= brnDetails.getE01NEWBRN() %>" readonly>
              	</div>
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="10%" rowspan="2" align="right"> 
                <div align="right">
                <input type="radio" name="MASSIVE" value="O" onClick="javascript:optionClick('O')" <% if (brnDetails.getE01CHGTYP().equals("4")) { out.print("checked"); } %> <% if (!brnDetails.getE01CHGTYP().equals("4") && userPO.getPurpose().equals("MAINTENANCE")) { out.print("disabled"); } %>>
                Oficial</div>
              </td>
              <td nowrap width="15%"> 
                <div align="right">Oficial Anterior :</div>
              </td>
              <td nowrap width="75%">  
                <div id="field05">
                	<input type="text" name="E01CHGOFC" size="5" maxlength="4" value="<%= brnDetails.getE01CHGOFC() %>" readonly>
                	<input type="text" name="E01OLDOFN" size="45" maxlength="45" value="<%= brnDetails.getE01OLDOFN() %>" readonly>
              	</div>
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="15%"> 
                <div align="right">Nuevo Oficial :</div>
              </td>
              <td nowrap width="75%">  
                <div id="field06">
                	<input type="text" name="E01CHGNFC" size="5" maxlength="4" value="<%= brnDetails.getE01CHGNFC() %>" readonly>
                	<input type="text" name="E01NEWOFN" size="45" maxlength="45" value="<%= brnDetails.getE01NEWOFN() %>" readonly>
              	</div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  <BR>

<SCRIPT language="JavaScript">
	<% if (userPO.getPurpose().equals("NEW")) { %>
		optionClick('P');
	<% } %>
	<% if (brnDetails.getE01CHGTYP().equals("2") && userPO.getPurpose().equals("MAINTENANCE")) { %>
		optionClick('P');
	<% } %>
	<% if (brnDetails.getE01CHGTYP().equals("3") && userPO.getPurpose().equals("MAINTENANCE")) { %>
		optionClick('B');
	<% } %>
	<% if (brnDetails.getE01CHGTYP().equals("4") && userPO.getPurpose().equals("MAINTENANCE")) { %>
		optionClick('O');
	<% } %>
</SCRIPT>
  
<p align="center"> 
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar" >
    <input id="EIBSBTN" type=button name="Cancel" value="Regresar" onClick="javascript:cancel()">
  </p>
      
</form>
</body>
</html>
