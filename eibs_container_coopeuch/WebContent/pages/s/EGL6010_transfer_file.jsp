<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css"> 
<TITLE>Carga de Cuadraturas automaticas</TITLE>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"  scope="session" />

<SCRIPT LANGUAGE="JavaScript" SRC="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

function CheckFile(){ 
	if (!isNumeric(document.forms[0].E01TRABNK.value))
	    {
	    alert("Debe Ingresar Banco Origen");
	  	document.forms[0].E01TRABNK.focus();
	    }
	else if (!isNumeric(document.forms[0].E01TRABRN.value))
	     {
	      	alert("Debe Ingresar Sucursal Origen");
	  		document.forms[0].E01TRABRN.focus();	     
	     }
	else if (!isNumeric(document.forms[0].E01TRABTH.value.length))
	     {
	      	alert("Debe Ingresar Numero de Lote");
	  		document.forms[0].E01TRABTH.focus();	     
	     }
	else if (document.forms[0].FILENAME.value.length < 1)
	     {
	      	alert("Debe Ingresar Direccion donde reside archivo excel");
	  		document.forms[0].FILENAME.focus();	     
		} 
	else {	
	  	alert("Este procedimiento puede tardar varios minutos.\r\nPor favor espere que muestre la pantalla de confirmación.");
	  	document.getElementById("wait").style.visibility = 'visible';
	  	document.getElementById("EIBSBTN").disabled = true;	
		document.forms[0].submit();
	}
}

</SCRIPT>
</HEAD>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%> 

<h3 align="center"> Carga de Movimientos
 	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="transfer_file.jsp, EGL6010"></h3>
<hr size="4">
 <FORM METHOD="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEGL6010" ENCTYPE="multipart/form-data">	
    <p>
    	<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="1">	   	
    </p>

  <table class="tableinfo">
    <tr > 
      <td nowrap height="100"> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
          <tr id="trclear"> 
            <td nowrap width="30%"> 
              <div align="right">Banco Origen : </div>
            </td>
            <td nowrap width="70%">
              <input type="text" name="E01TRABNK" size="3" maxlength="2" value="<%= userPO.getBank() %>" >              
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="30%"> 
              <div align="right">Sucursal Origen : </div>
            </td>
            <td nowrap width="70%">
              <div align="left"> 
                <input type="text" name="E01TRABRN" size="05" maxlength="04" value="<%= userPO.getBranch() %>">
                <a href="javascript:GetBranch('E01TRABRN','<%= userPO.getBank() %>',document.forms[0].E01TRABRN.value)"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0"></a> 
              </div>              
            </td>
          </tr> 
          <tr id="trclear"> 
            <td nowrap width="30%"> 
              <div align="right">Numero de Lote : </div>
            </td>
            <td nowrap width="70%">
              <input type="text" name="E01TRABTH" size="6" maxlength="5" value="<%= userPO.getHeader1() %>" >
            </td>
          </tr>                   
          <tr id="trdark"> 
            <td nowrap width="30%"> 
              <div align="right">Direccion de archivo a Cargar</div>
            </td>
            <td nowrap width="70%">
              <input type=file name="FILENAME" size="80" maxlength="255" >
            </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>
  <p align="center"> 
	<input id="EIBSBTN" type="button" name="Submit" value="Enviar" onClick="CheckFile()"> 
  </p> 

  	<br>
  	<br>
  	
  	<div id="wait" align="center" style="visibility: hidden">
  		<img align="bottom" border="0" src="<%=request.getContextPath()%>/images/gears7.gif"/>
  		<span><font color="blue" size="2"><i>Cargando Movimientos .... </i></font></span>
  	</div> 

<script language="JavaScript">
  document.forms[0].E01TRABNK.focus();
</script>
 </FORM>
</BODY>
</HTML>
