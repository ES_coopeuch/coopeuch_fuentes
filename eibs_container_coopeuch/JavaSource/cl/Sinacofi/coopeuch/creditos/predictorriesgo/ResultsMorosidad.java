/**
 * ResultsMorosidad.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResultsMorosidad  implements java.io.Serializable {
    private javax.xml.soap.SOAPElement[] morosidad;

    public ResultsMorosidad() {
    }

    public javax.xml.soap.SOAPElement[] getMorosidad() {
        return morosidad;
    }

    public void setMorosidad(javax.xml.soap.SOAPElement[] morosidad) {
        this.morosidad = morosidad;
    }

    public javax.xml.soap.SOAPElement getMorosidad(int i) {
        return morosidad[i];
    }

    public void setMorosidad(int i, javax.xml.soap.SOAPElement value) {
        this.morosidad[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsMorosidad history = (ResultsMorosidad) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ResultsMorosidad other = (ResultsMorosidad) obj;
        boolean _equals;
        _equals = true
            && ((this.morosidad==null && other.getMorosidad()==null) || 
             (this.morosidad!=null &&
              java.util.Arrays.equals(this.morosidad, other.getMorosidad())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsMorosidad history = (ResultsMorosidad) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getMorosidad() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMorosidad());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMorosidad(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
