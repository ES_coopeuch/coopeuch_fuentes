package datapro.eibs.products;

/**
 * Insert the type's description here.
 * Creation date: (1/19/00 6:08:55 PM)
 * @author: David Mavilla
 */
import java.io.*;
import java.math.BigDecimal;
import java.net.*;
import java.beans.Beans;
import javax.servlet.*;
import javax.servlet.http.*;

import com.datapro.generic.beanutil.BeanList;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import datapro.eibs.beans.*;

import datapro.eibs.master.Util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import datapro.eibs.sockets.*;

public class JSEGL0216 extends datapro.eibs.master.SuperServlet {

	// entering options
	protected static final int R_ENTER = 100;
	protected static final int A_ENTER = 200;
	protected static final int R_PDFE  = 300;	

	
	protected String LangPath = "S";

	/**
	 * JSEGL0034 constructor comment.
	 */
	public JSEGL0216() {
		super();
	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

		flexLog("free resources used by JSEGL0216");

	}
	/**
	 * This method was created by David Mavilla.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionEnter(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		EGL021601Message msgGL = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Sending header");
			flexLog("A_ENTER ");

			String opCode = "0002";
			
			msgGL = (EGL021601Message) mc.getMessageRecord("EGL021601");
			msgGL.setH01USERID(user.getH01USR());
			msgGL.setH01PROGRM("EGL0216");
			msgGL.setH01TIMSYS(getTimeStamp());
			msgGL.setH01SCRCOD("01");
			msgGL.setH01OPECOD(opCode);

			// all the fields here
			java.util.Enumeration enu = msgGL.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			msgGL.send();
			msgGL.destroy();
			flexLog("EGL021601 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EGL021601")) {
				try {
					msgGL = new datapro.eibs.beans.EGL021601Message();
					flexLog("EGL021601 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgGL = (EGL021601Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("gLedger", msgGL);

				if (IsNotError) { // There are no errors
					try {
							flexLog("About to call Page: "+ LangPath + "EGL0216_gledger_enter.jsp");
							callPage(LangPath + "EGL0216_gledger_enter.jsp",req,res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} 
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}


	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqEnter(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			MessageRecord newmessage = null;
			EGL021601Message msgGL = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			boolean IsNotError = false;

			try {
				msgError = new datapro.eibs.beans.ELEERRMessage();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			// Send Initial data
			try {
				flexLog("Sending header");
				flexLog("header R_ENTER ");
				String opCode = "0001";

				msgGL = (EGL021601Message) mc.getMessageRecord("EGL021601");
				msgGL.setH01USERID(user.getH01USR());
				msgGL.setH01PROGRM("EGL0216");
				msgGL.setH01TIMSYS(getTimeStamp());
				msgGL.setH01SCRCOD("01");
				msgGL.setH01OPECOD(opCode);

				// all the fields here
				java.util.Enumeration enu = msgGL.fieldEnumeration();
				MessageField field = null;
				String value = null;
				while (enu.hasMoreElements()) {
					field = (MessageField) enu.nextElement();
					try {
						value = req.getParameter(field.getTag()).toUpperCase();
						if (value != null) {
							field.setString(value);
						}
					} catch (Exception e) {
					}
				}

				msgGL.send();
				msgGL.destroy();
				flexLog("EGL021601 Message Sent");
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Error Message
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("EGL021601")) {
					try {
						msgGL = new datapro.eibs.beans.EGL021601Message();
						flexLog("EGL021601 Message Received");
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					msgGL = (EGL021601Message) newmessage;

					flexLog("Putting java beans into the session");
					ses.setAttribute("error", msgError);
					ses.setAttribute("userPO", userPO);
					ses.setAttribute("gLedger", msgGL);

					if (IsNotError) { // There are no errors
						try {
								flexLog("About to call Page: "+ LangPath + "EGL0216_gledger_enter.jsp");
								callPage(LangPath + "EGL0216_gledger_enter.jsp",req,res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else { // There are errors
						try {
							flexLog(
								"About to call Page: "
									+ LangPath
									+ "EGL0216_gledger_enter.jsp");
							callPage(
								LangPath + "EGL0216_gledger_enter.jsp",
								req,
								res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}

	// START PDF
	public void procReqPDFE(
	MessageContext mc,
	ESS0030DSMessage user,
	HttpServletRequest req,
	HttpServletResponse res,
	HttpSession ses)
	throws ServletException, IOException, DocumentException {


		MessageRecord newmessage = null;
		EGL021601Message msgGL = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		
		flexLog("EGL021601 a");

		DocumentException ex = null;
		ByteArrayOutputStream baosPDF = null;
		boolean ACT = false;
		
		
		baosPDF =
			generatePDFDocumentBytes(
				req,
				this.getServletContext(),
				ses,
				ACT);
				
	}
		

	
	protected ByteArrayOutputStream generatePDFDocumentBytes(
			final HttpServletRequest req,
			final ServletContext ctx,
			HttpSession session,
			boolean FLG)
			throws DocumentException {
	
		
		ESS0030DSMessage msgUser = null;
		msgUser = (ESS0030DSMessage) session.getAttribute("currUser");
		String LNG = msgUser.getE01LAN();

		EGL021601Message msgHeader = null;
		msgHeader = (EGL021601Message) session.getAttribute("msgHeader");

		
		
		String title = "";

		String header01 = "";
		String header02 = "";
		String header03 = "";
		String header04 = "";
		String header05 = "";
		String header06 = "";
		String header07 = "";
		String header08 = "";
		String header09 = "";

		String page = "";
		
		title = "Correccion monetaria de Capital Cuotas de Participacion ($)";

		header01 = "Mes";
		header02 = "Saldo Capital ($)";
		header03 = "Saldo CM ($)"; //Address
		header04 = "Saldo Reajustes ($)";
		header05 = "Saldo Nro. Cuotas";
		header06 = "Aporte Neto ($)";
		header07 = "%CM";
		header08 = "Monto CM ($)";
		header09 = "Capital Actualizado ($)";
		
		Document doc = new Document(PageSize.A4, 36, 36, 36, 36);

		ByteArrayOutputStream baosPDF = new ByteArrayOutputStream();
		PdfWriter docWriter = null;

		docWriter = PdfWriter.getInstance(doc, baosPDF);
		
		doc.addAuthor("eIBS");
		doc.addCreationDate();
		doc.addProducer();
		doc.addCreator(msgHeader.getH01USERID());
		doc.addTitle(title);
		doc.addKeywords("pdf, itext, Java, open source, http");


		Font normalFont =
			FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL);
		Font normalBoldFont =
			FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD);
		Font normalBoldUnderFont =
			FontFactory.getFont(
				FontFactory.HELVETICA,
				8,
				Font.BOLD | Font.UNDERLINE);
		Font headerFont =
			FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL);
		Font headerBoldFont =
			FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD);
		Font headerBoldUnderFont =
			FontFactory.getFont(
				FontFactory.HELVETICA,
				10,
				Font.BOLD | Font.UNDERLINE);
		
		Paragraph TITLE = new Paragraph(title, headerBoldFont);

		Paragraph RAYA =
			new Paragraph(
				"____________________________________________________________________________________________________________________",
				normalBoldFont);

		Paragraph HEADER01 =
			new Paragraph(header01 , headerBoldFont);
		Paragraph HEADER02 =
			new Paragraph(header02 , headerBoldFont);
		Paragraph HEADER03 =
			new Paragraph(header03 , headerBoldFont);
		Paragraph HEADER031 =
			new Paragraph(header03 , headerBoldFont);
		Paragraph HEADER032 =
			new Paragraph(header03 , headerBoldFont);
		Paragraph HEADER04 =
			new Paragraph(header04 , headerBoldFont);
		Paragraph HEADER05 =
			new Paragraph(header05 , headerBoldFont);
		Paragraph HEADER07 =
			new Paragraph(header07 , headerBoldFont);
		Paragraph HEADER08 =
			new Paragraph(header08 , headerBoldFont);
		Paragraph HEADER09 =
			new Paragraph(header09 , headerBoldFont);

		HeaderFooter header = new HeaderFooter(TITLE, false);
		header.setBorder(Rectangle.NO_BORDER);
		header.setAlignment(Element.ALIGN_CENTER);
		doc.setHeader(header);

		HeaderFooter footer = new HeaderFooter(new Phrase(page), false);
		footer.setBorder(Rectangle.NO_BORDER);
		doc.setFooter(footer);

		doc.open();

		Table table = new Table(2, 5);
		table.setBorderWidth(0);
		table.setCellsFitPage(true);
		table.setPadding(1);
		table.setSpacing(1);
		table.setWidth(100);

		Cell cell = new Cell(HEADER01);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);


		cell = new Cell(HEADER02);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);

		cell = new Cell(HEADER03);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);

		cell = new Cell(HEADER04);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);
		

		
		
		cell = new Cell(HEADER05);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);


		cell = new Cell(HEADER07);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);


		cell = new Cell(HEADER08);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);



		cell = new Cell(HEADER09);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setBorder(Rectangle.NO_BORDER);
		table.addCell(cell);

		doc.add(table);
		
		
		if (doc != null) {
			doc.close();
		}
		if (docWriter != null) {
			docWriter.close();
		}

	if (baosPDF.size() < 1) {
		throw new DocumentException(
			"document has " + baosPDF.size() + " bytes");
	}

	return baosPDF;
}
	

	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		Socket s = null;
		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			int screen = R_ENTER;

			try {

				flexLog("Screen  Number: " + screen);

				msgUser =
					(datapro.eibs.beans.ESS0030DSMessage) session.getAttribute(
						"currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					s = new Socket(super.hostIP, getInitSocket(req) + 1);
					s.setSoTimeout(super.sckTimeOut);
					mc =
						new MessageContext(
							new DataInputStream(
								new BufferedInputStream(s.getInputStream())),
							new DataOutputStream(
								new BufferedOutputStream(s.getOutputStream())),
							"datapro.eibs.beans");

					try {
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					} catch (Exception e) {
						flexLog("Screen set to default value");
					}

					switch (screen) {
						// Presenta formulario Inicial
						case R_ENTER :
							procReqEnter(mc, msgUser, req, res, session);
							break;
						// Calcula cuota de participacion 
						case A_ENTER :
							procActionEnter(mc, msgUser, req, res, session);
							break;
							// Genera PDF
						case R_PDFE :
							procReqPDFE(mc, msgUser, req, res, session);
							break;

						default :
							res.sendRedirect(super.srctx + LangPath + super.devPage);
							break;
					}

				} catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
					//return;
				} finally {
					s.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}

		}

	}

	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

		}
	}



}

