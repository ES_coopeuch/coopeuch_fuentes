/**
 * RespuestaEstadosDeCuentaPDF.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.coopeuch.core.nexus;

public class RespuestaEstadosDeCuentaPDF  implements java.io.Serializable {
    private int coderror;
    private java.lang.String msgerror;
    private java.lang.String idwsst247;
    private java.lang.String filler_02;
    private java.lang.String variable_pdf;
    private cl.coopeuch.core.nexus.Error error;

    public RespuestaEstadosDeCuentaPDF() {
    }

    public int getCoderror() {
        return coderror;
    }

    public void setCoderror(int coderror) {
        this.coderror = coderror;
    }

    public java.lang.String getMsgerror() {
        return msgerror;
    }

    public void setMsgerror(java.lang.String msgerror) {
        this.msgerror = msgerror;
    }

    public java.lang.String getIdwsst247() {
        return idwsst247;
    }

    public void setIdwsst247(java.lang.String idwsst247) {
        this.idwsst247 = idwsst247;
    }

    public java.lang.String getFiller_02() {
        return filler_02;
    }

    public void setFiller_02(java.lang.String filler_02) {
        this.filler_02 = filler_02;
    }

    public java.lang.String getVariable_pdf() {
        return variable_pdf;
    }

    public void setVariable_pdf(java.lang.String variable_pdf) {
        this.variable_pdf = variable_pdf;
    }

    public cl.coopeuch.core.nexus.Error getError() {
        return error;
    }

    public void setError(cl.coopeuch.core.nexus.Error error) {
        this.error = error;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        RespuestaEstadosDeCuentaPDF other = (RespuestaEstadosDeCuentaPDF) obj;
        boolean _equals;
        _equals = true
            && this.coderror == other.getCoderror()
            && ((this.msgerror==null && other.getMsgerror()==null) || 
             (this.msgerror!=null &&
              this.msgerror.equals(other.getMsgerror())))
            && ((this.idwsst247==null && other.getIdwsst247()==null) || 
             (this.idwsst247!=null &&
              this.idwsst247.equals(other.getIdwsst247())))
            && ((this.filler_02==null && other.getFiller_02()==null) || 
             (this.filler_02!=null &&
              this.filler_02.equals(other.getFiller_02())))
            && ((this.variable_pdf==null && other.getVariable_pdf()==null) || 
             (this.variable_pdf!=null &&
              this.variable_pdf.equals(other.getVariable_pdf())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        RespuestaEstadosDeCuentaPDF history = (RespuestaEstadosDeCuentaPDF) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        RespuestaEstadosDeCuentaPDF history = (RespuestaEstadosDeCuentaPDF) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        _hashCode += getCoderror();
        if (getMsgerror() != null) {
            _hashCode += getMsgerror().hashCode();
        }
        if (getIdwsst247() != null) {
            _hashCode += getIdwsst247().hashCode();
        }
        if (getFiller_02() != null) {
            _hashCode += getFiller_02().hashCode();
        }
        if (getVariable_pdf() != null) {
            _hashCode += getVariable_pdf().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
