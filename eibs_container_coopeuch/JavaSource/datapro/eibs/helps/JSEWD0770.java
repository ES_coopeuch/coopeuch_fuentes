package datapro.eibs.helps;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.EWD0770DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;

public class JSEWD0770 extends JSEIBSServlet{


	/**
	 * 
	 */
	private static final long serialVersionUID = -7145733848431775203L;

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		//EWD0770
		String customer =	req.getParameter("CustNum");
		if (customer!=null && !"".equals(customer)){
			MessageProcessor mp = null;		
			try {
				mp = getMessageProcessor("EWD0770", req);
		
				EWD0770DSMessage msgHelp = null;
		               
				// Send Request
				msgHelp = (EWD0770DSMessage)mp.getMessageRecord("EWD0770DS");
				msgHelp.setEWDCUN(customer);

				mp.sendMessage(msgHelp);
				
				JBObjList list = mp.receiveMessageRecordList("EWDOPE");
				
				session.setAttribute("helpList", list);
				forward("EWD0770_AccountRolloverTimeDeposit.jsp", req, res);
				
				
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
}