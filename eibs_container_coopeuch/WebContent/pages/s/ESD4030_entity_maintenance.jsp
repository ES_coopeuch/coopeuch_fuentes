<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="datapro.eibs.master.Util"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="datapro.eibs.sockets.MessageRecord"%>
<%@page import="datapro.eibs.beans.ESD403001Message"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Entity</title>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
		<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
	</head>

	<jsp:useBean id="ESD403001List" class="datapro.eibs.beans.JBObjList" scope="session" />
	<jsp:useBean id="entity" class="datapro.eibs.beans.ESD403001Message" scope="session" />
	<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"  	 scope="session" />
	<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"  	     scope="session" />
	<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" 	scope="session" />
	<jsp:useBean id= "ListaVinculo" class= "datapro.eibs.beans.JBList"  scope="session" />
	<jsp:useBean id= "tipoTransaccion" class= "java.lang.String"  scope="session" />
	
	<body onload="deshabilitaRetroceso(); borrarFono();  fecha_actual(document.forms[0].TIPOTRANSACCION.value);"  onpaste="return false">
	
	<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
	<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

	<script type="text/javascript">
	
	<%
	boolean isApprovalInquiry = userPO.getPurpose().equals("APPROVAL_INQ");
	boolean isInquiry = userPO.getPurpose().equals("INQUIRY");
	boolean isReadOnly = isApprovalInquiry || isInquiry;
	%>
	
	function deshabilitaRetroceso(){
		if (document.forms[0].TIPOTRANSACCION.value == "M")
			document.forms[0].E01PRR.disabled = "true";
	}
	
	function borrarFono(){
		if (document.forms[0].TIPOTRANSACCION.value == "I")
			document.forms[0].E01PRF.value = "";
	}
	
	function validarRut(){
		//Valida si es un rut valido
		var busqueda;
		var busqueda2;
		
		if (document.forms[0].E01PRR.value != ""){
			busqueda = rutChile(document.forms[0].E01PRR.value.toUpperCase());
			busqueda2 = Numeros(busqueda);
			document.forms[0].E01PRR.value = busqueda2;		
		
			if (busqueda == 0) {
				document.forms[0].E01PRR.value = "";
				alert("!! Error: El Rut no es valido. !!");
				document.forms[0].E01PRR.focus();
				return;
		    }else{
		    	//Valida si esta ingresando el RUT del socio Titular
				if(document.forms[0].rutTitular.value == document.forms[0].E01PRR.value.toUpperCase() ){
					alert("!! Error: No se puede ingresar el socio como un relacionado");
					document.forms[0].E01PRR.value = "";
					document.forms[0].E01PRR.focus();
					return;
				}else{				
					//Valida si esta ingresando una persona relacionada ya existente
					if(parseInt(document.forms[0].cantidadRut.value) > 1){
						for(var i=0; i< document.forms[0].rut.length ; i++ ){
							var rutMayuscula = document.forms[0].rut[i].value.toUpperCase();
							if( (busqueda2)  == rutMayuscula && document.forms[0].TIPOTRANSACCION.value == "I"){
								alert("!! Error: Relacionado YA Existe!!!");
								document.forms[0].E01PRR.value = "";	
								document.forms[0].E01PRR.focus();
								break;
							}else{
								//Valida si es una persona juridica
								if(document.forms[0].E01PRR.value.substring(0,busqueda2.length-1) > 50000000){
									alert("!! Error: No se puede ingresar una persona Juridica");
						   			document.forms[0].E01PRR.value = "";
						   			document.forms[0].E01PRR.focus();
						   			return;
								}
								else{
									parent.results.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4030?SCREEN=400&RUT=" + document.forms[0].E01PRR.value;
								}								
							}
						}
					}else if (parseInt(document.forms[0].cantidadRut.value) == 1){
						var rutMayuscula = document.forms[0].rut.value.toUpperCase();
						if( (busqueda2)  == rutMayuscula && document.forms[0].TIPOTRANSACCION.value == "I"){
							alert("!! Error: Relacionado YA Existe!!!");
							document.forms[0].E01PRR.value = "";	
							document.forms[0].E01PRR.focus();
							return;
						}else{
							//Valida si es una persona juridica
							if(document.forms[0].E01PRR.value.substring(0,busqueda2.length-1) > 50000000){
								alert("!! Error: No se puede ingresar una persona Juridica");
					   			document.forms[0].E01PRR.value = "";
					   			document.forms[0].E01PRR.focus();
					   			return;
							}
							else{
								parent.results.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4030?SCREEN=400&RUT=" + document.forms[0].E01PRR.value;
							}
						}
					}else{
						//Valida si es una persona juridica
						if(document.forms[0].E01PRR.value.substring(0,busqueda2.length-1) > 50000000){
							alert("!! Error: No se puede ingresar una persona Juridica");
				   			document.forms[0].E01PRR.value = "";
				   			document.forms[0].E01PRR.focus();
				   			return;
						}
						else{
							parent.results.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4030?SCREEN=400&RUT=" + document.forms[0].E01PRR.value;
						}
					}
				}
		    }
		}
	}
	
	function fecha_actual(tipo){
		var today = new Date();
		var dia_today = today.getDate();
		var mes_today = (today.getMonth() + 1);
		var year_today = today.getFullYear();
		if(dia_today <= 9 && dia_today > 0){
			dia_today = "0"+dia_today;
		}
		else{
			dia_today = dia_today;
		}
		
		if(mes_today <= 9 && mes_today > 0){
			mes_today = "0"+mes_today;
		}
		else{
			mes_today = mes_today;
		}
		
		if (tipo != "M")
		{
			document.forms[0].aceptar.value = "A�adir";
			document.forms[0].E01PRD.value = dia_today;
			document.forms[0].E01PRM.value = mes_today; 			
	 		document.forms[0].E01PRA.value = year_today;
 		}else{
 			document.forms[0].aceptar.value = "Aceptar";
 		}	
	}
	
	//solo permite ingresar numeros y la letra K y k
	function validaIngRut(e){
		tecla = (document.all) ? e.keyCode : e.which;

		if(tecla == 75 || tecla == 107){
				return true;
			}
			if(tecla == 8 ){
				return false;
			}
			patron = /[0-9]/;
			tecla_final = String.fromCharCode(tecla);
			return patron.test(tecla_final);
	}
	
	function goAction(op){
		switch (op){
			case 2:{
				if( document.forms[0].E01PRR.value == "" ){
			 		alert("!! Error: No ingresaste el RUT es Obligatorio !!");
			 		document.forms[0].E01PRR.focus();
			 	}else{
			 		if(document.forms[0].E01PRN.value == ""){
			 			alert("!! Error: Falto el Nombre del Relacionado. !!");
			 			document.forms[0].E01PRN.focus();
			 		}else{
			 			if (!validarFechaMenorActual())
			 				alert("Fecha es mayor a la actual");
			 			else{
			 				if(document.forms[0].E01PRC.value == ""){
	 							alert("!! Error: Email no Ingresado. !!");
	 							document.forms[0].E01PRC.focus();
	 						}else{			 			
				 				if(document.forms[0].E01PRF.value == ""){
									alert("!! Error: falta. ingresar el numero de t�lefono !!");
									document.forms[0].E01PRF.focus();
								}else{
									if(document.forms[0].E01PRTV.value == "" ){
 										alert("!! Error: Vinculo  falta. !!");
 									}else{
 										if(document.forms[0].TIPOTRANSACCION.value == "I" ){
 											document.forms[0].SCREEN.value="900";
											document.forms[0].submit();
											alert('Persona Relacionada Ingresado con Exito');
 										}else{
 											document.forms[0].SCREEN.value="600";
 											document.getElementById("E01PRR").disabled = false; 
											document.forms[0].submit();
											alert('Persona Relacionada Modificado con Exito');
 										}
 									}
								}
							}
			 			}
			 		}
			 	}
				break;	
			}
			case 6:{
				document.forms[0].SCREEN.value="500";
				document.forms[0].submit();
				break;	
			}
		}
	}
	
	function keyPressed(tipo, fecha){
		var retroceso = 8;
		if (tipo == "M" || fecha == "S"){
			if(document.all){
				if(event.keyCode == retroceso){
					event.keyCode = 0;
					window.event.returnValue = false;
				}else if(event.ctrlKey){
					if(event.keyCode == retroceso){
						event.keyCode = 0;
						window.event.returnValue = false;
					}
				}
			}
		}
	}
	
	function Numeros(string){ 			
		var out = '', invalido = 0;
		var filtro = '1234567890'; 
		var caracterK = 'K';
		var caracter_k = 'k';
		
		for(var i=0; i< string.length; i++){
			if( filtro.indexOf(string.charAt(i)) != -1  || caracterK.indexOf(string.charAt(i)) != -1 || caracter_k.indexOf(string.charAt(i)) != -1 ) {
				out += string.charAt(i);
			}
		}
		var pivote = out.toUpperCase();
		if(out == ""){
			//alert("Error: Caracteres invalidos en el rut"+out);
			document.forms[0].E01PRR.value = "";
		}
		else{
			if(pivote.length <= 9){
				return pivote;	
			} 				
		}
		return invalido;
	}
	
	function validarFechaMenorActual(){
      var x=new Date();
      var date = document.forms[0].E01PRD.value + "/" + document.forms[0].E01PRM.value + "/" + document.forms[0].E01PRA.value;
      
      var fecha = date.split("/");
      x.setFullYear(fecha[2],fecha[1]-1,fecha[0]);
      var today = new Date();
      if (x > today){
        return false;
     }else{
     	return true;
     }
	}
	
	//funcion donde se valida el ingreso de un correo electronico valido
	function isValidEmail() { 
		if(document.forms[0].E01PRC.value != ""){
			if(!(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(document.forms[0].E01PRC.value))){
				alert("!! Error: Email mal ingresado. !!");
				document.forms[0].E01PRC.value = "";
				document.forms[0].E01PRC.focus();
			}
		}
	}
	
	//funcion de validar el numero de telefono
	function validaFono(){
		if (document.forms[0].E01PRF.value != "")
		{
			if(document.forms[0].E01PRF.value.length < 9){
				alert("N�mero de tel�fono Invalido, n�mero menor a nueve digitos");
				document.forms[0].E01PRF.value = "";
	 			document.forms[0].E01PRF.focus();
			}
			else{
				var aux = document.forms[0].E01PRF.value;
				for(var num = 0; num < aux.length ; num++){
					if(num == 0){
						var caracter = aux.charAt(num);
						if(caracter == 0){
							alert("El Tel�fono no puede comenzar con Cero (0) ");
							document.forms[0].E01PRF.value = "";
	 						document.forms[0].E01PRF.focus();
						}
					}
				}
			}
		}
	}
	
	function cambiarFoco()
	{
		if (document.forms[0].E01PRF.value != "")
			document.forms[0].descripcion.focus();
	}
	
	//solo permite ingresar numeros
	function validaIngFono(e){
		tecla = (document.all) ? e.keyCode : e.which;
		if(tecla == 8 ){
				return true;
			}
			patron = /[0-9]/;
			tecla_final = String.fromCharCode(tecla);
			return patron.test(tecla_final);
	}
	
	</script>

	<H3 align="center">Personas Relacionadas
		<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="entity_maintenance.jsp, ESD4030">
	</H3>
	
	<hr size="4">
	<form name="datos" method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4030">
	<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600"> 
	<INPUT TYPE=HIDDEN NAME="APPROVAL" VALUE="N"> 
	<INPUT TYPE=HIDDEN NAME="vinculo" VALUE=""> 
    <input type=HIDDEN name="L01FILLE2" value="<%=entity.getL01FILLE2().trim()%>"> 
    <input type=HIDDEN name="TIPOTRANSACCION" value="<%=tipoTransaccion%>">
    <input type=HIDDEN name="IDCLIENTE" value="<%=userPO.getHeader1()%>">
	<table class="tableinfo">
		<tr bordercolor="#FFFFFF">
			<td nowrap>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
					<tr>
						<td nowrap width="10%" align="right">Cliente:</td>
						<td nowrap width="12%" align="left"><%=userPO.getHeader1()%></td>
						<td nowrap width="6%" align="right">RUT:</td>
						<td nowrap width="14%" align="left"><%=userPO.getHeader2()%></td>
						<td nowrap width="8%" align="right">Nombre:</td>
						<td nowrap width="50%" align="left"><%=userPO.getHeader3()%></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="rutTitular" size="10" maxlength="10" value="<%=userPO.getHeader2()%>">
	<h4><%="       " %></h4>
	<table class="tableinfo">
		<tr bordercolor="#FFFFFF">
			<td nowrap>
				<table cellspacing="0" cellpadding="2" width="100%" border="0">
					<tr id="trdark">
						<td nowrap width="40%">
							<div align="right">RUT :</div>
						</td>
						<td nowrap width="60%" colspan="3">
							<input name="E01PRR" value="<%=entity.getE01PRR()%>" size="10"
							maxlength="10" onblur="validarRut();" onkeypress="return validaIngRut(event)" onkeydown="keyPressed(document.forms[0].TIPOTRANSACCION.value, 'N')" />
							<img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom">
						</td>						
					</tr>
					<tr id="trclear">
						<td nowrap width="40%">
							<div align="right">Nombre Completo :</div>
						</td>
						<td nowrap width="60%" colspan="3">
							<eibsinput:text name="entity" property="E01PRN" eibsType="<%=EibsFields.EIBS_FIELD_TYPE_NAME_FULL%>"
							required="true" readonly="false" />
						</td>
					</tr>
					<tr id="trdark">
						<td nowrap width="40%">
							<div align="right">Fecha Nacimiento :</div>
						</td>
						<td nowrap width="60%" colspan="3">
							<input type="text" name="E01PRD" size="3" maxlength="2" value="<%=entity.getE01PRD() %> " readonly onkeydown="keyPressed(document.forms[0].TIPOTRANSACCION.value, 'S')" onkeypress=" enterInteger()" readonly / > 
							<input type="text" name="E01PRM" size="3" maxlength="2" value="<%=entity.getE01PRM() %>" readonly onkeydown="keyPressed(document.forms[0].TIPOTRANSACCION.value, 'S')" onkeypress=" enterInteger()" readonly  /> 
							<input type="text" name="E01PRA" size="5" maxlength="4" value="<%=entity.getE01PRA() %>" readonly onkeydown="keyPressed(document.forms[0].TIPOTRANSACCION.value, 'S')" onkeypress=" enterInteger()" readonly /> 
							<a id="linkHelp" href="javascript:DatePicker(document.forms[0].E01PRD,document.forms[0].E01PRM,document.forms[0].E01PRA);">
								<img src="/eibs_FUN/images/calendar.gif"  align="bottom" border="0" />
							</a><img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom">
						</td>		
					</tr>
					<tr id="trclear">
						<td nowrap width="40%">
							<div align="right">Correo Electr&oacutenico :</div>
						</td>
						<td nowrap width="60%" colspan="3">
							<eibsinput:text name="entity" property="E01PRC" eibsType="<%=EibsFields.EIBS_FIELD_TYPE_EMAIL%>" onblur="isValidEmail();" required="false" readonly="false" /> 
							<img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom">
						</td>
					</tr>
					<tr id="trdark">
						<td nowrap width="40%">
							<div align="right">Tel&eacutefono :</div>
						</td>
						<td nowrap width="60%" colspan="3">
							<eibsinput:text name="entity" property="E01PRF" eibsType="<%=EibsFields.EIBS_FIELD_TYPE_PHONE%>" size="10" 
							maxlength="9" onblur="validaFono(); cambiarFoco();" onkeypress="return validaIngFono(event)" required="false" readonly="false" /> 
							
							
							<img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom">
						</td>
					</tr>
					<tr id="trclear">
						<td nowrap width="40%">
							<div align="right">V&iacutenculo :</div>
						</td>
						<td nowrap width="60%" colspan="3">
							<%
							String aux = entity.getE01PRTV().toString();
							String descripcion2="";
							String cod2="";
							ListaVinculo.initRow();
             						int k=1;
             						while (ListaVinculo.getNextRow()) {                							
             							String [] cadenas = ListaVinculo.getRecord().split(",");
                 						if (ListaVinculo.getFlag().equals("")) {
                 							
                 							if(cadenas[0].equals(aux)){
                 								//out.println( cadenas[1] );
                 								descripcion2 = cadenas[1];
                 								cod2 = cadenas[0];
                 								break;
                 							}
                 							k++;
                 						}        
             						}
								%>
						
							<input type="hidden" name="E01PRTV" size="6" value ="<%=cod2 %>"  maxlength="4"  />
					        <input type="text" name="descripcion" size="25" value="<%=descripcion2 %>"  maxlength="4" disabled  />
           						<a href="javascript:GetCodeDescCNOFC('E01PRTV','descripcion','FL')">
           						  <img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0" >
           						</a>
           						<img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<%
		int row = 0;
    	int i = 0;
    	
    	if (!ESD403001List.isEmpty()) {
    		ESD403001List.initRow();
       		while (ESD403001List.getNextRow()) {
         			ESD403001Message msgList = (ESD403001Message) ESD403001List.getRecord();	
         	%> 
         	<input type="hidden" name="rut" size="2" maxlength="1" value="<%=msgList.getE01PRR()%>">
         	<%
			i++;
  			}
    	}
	 %>
	<input type="hidden" name="cantidadRut" size="2" maxlength="1" value="<%=i%>">	
  	<br>
  	<div align="center">
		<input id="EIBSBTN" type=button name="cancelar" value="Cancelar" onClick="javascript:goAction(6);"> 
		<input id="EIBSBTN" type=button name="aceptar" value="A�adir" onClick="javascript:goAction(2);">
	</div>
	
  	
  	
  	
	</form>
</body>
</html>