package datapro.eibs.products;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERC004701Message;
import datapro.eibs.beans.ERC004704Message;
import datapro.eibs.beans.ERC004705Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSERC0047 extends JSEIBSServlet {
	
	private static final long serialVersionUID = 1L;
	protected final int ENTER_SCREEN = 100;
	protected final int SEARCH_DATA = 200;
	protected final int ACTION_CONCILIACION = 300;
	protected final int ACTION_DESCONCILIACION = 400;

	public JSERC0047() {
	}

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case ENTER_SCREEN:
			procEnterScreen(user, req, res, session);
			break;
		case SEARCH_DATA:
			procSearchData(user, req, res, session);
			break;
		case ACTION_CONCILIACION:
			procActionConciliacion(user, req, res, session);
			break;
		case ACTION_DESCONCILIACION:
			procActionDesconciliacion(user, req, res, session);
			break;
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	protected void procEnterScreen(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		UserPos userPO = (UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		try {
			flexLog("About to call Page: ERC0047_enter.jsp");
			forward("ERC0047_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
		}
	}
	
	protected void procSearchData(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		String Tipo_Pro = "";    //PRO 0041 INI
		
		try {
			mp = getMessageProcessor("ERC0047", req);
			ERC004701Message message = new ERC004701Message();
			message.setH01USERID(user.getH01USR());
			message.setH01PROGRM("ERC0047");
			message.setH01TIMSYS(getTimeStamp());
			message.setH01OPECOD("0001");
			message.setH01SCRCOD("47");
//PRO 0041 INI	
			if(req.getAttribute("H01FLGWKK")!=null)
			{
				Tipo_Pro = req.getAttribute("H01FLGWKK").toString();
				message.setH01FLGWK3(req.getAttribute("H01FLGWKK").toString());
				message.setE01RCHRBK(req.getAttribute("E01RCHRBK").toString());
				message.setE01BRMACC(req.getAttribute("E01BRMACC").toString());
				message.setE01BRMCTA(req.getAttribute("E01BRMCTA").toString());
				
				message.setE01DESDDM(req.getAttribute("E01DESDDM").toString());
				message.setE01DESDDD(req.getAttribute("E01DESDDD").toString());
				message.setE01DESDDY(req.getAttribute("E01DESDDY").toString());
				message.setE01HASDDM(req.getAttribute("E01HASDDM").toString());
				message.setE01HASDDD(req.getAttribute("E01HASDDD").toString());
				message.setE01HASDDY(req.getAttribute("E01HASDDY").toString());
			}
			else
			{
//PRO 0041 FIN	
				Tipo_Pro = 	req.getParameter("H01FLGWK3");
				message.setH01FLGWK3(req.getParameter("H01FLGWK3"));
				message.setE01RCHRBK(req.getParameter("E01RCHRBK"));
				message.setE01BRMACC(req.getParameter("E01BRMACC"));
				message.setE01BRMCTA(req.getParameter("E01BRMCTA"));
			
				message.setE01DESDDM(req.getParameter("E01DESDDM"));
				message.setE01DESDDD(req.getParameter("E01DESDDD"));
				message.setE01DESDDY(req.getParameter("E01DESDDY"));
				message.setE01HASDDM(req.getParameter("E01HASDDM"));
				message.setE01HASDDD(req.getParameter("E01HASDDD"));
				message.setE01HASDDY(req.getParameter("E01HASDDY"));
			}
			mp.sendMessage(message);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				ses.setAttribute("error", error);
				flexLog("About to call Page: ERC0047_enter.jsp");
				forward("ERC0047_enter.jsp", req, res);
			}else {
				ERC004701Message receive = (ERC004701Message) mp.receiveMessageRecord();
				JBObjList listDebito = mp.receiveMessageRecordList("H02FLGMAS");
				JBObjList listCredito = mp.receiveMessageRecordList("H03FLGMAS");
				ses.setAttribute("data", receive);
				ses.setAttribute("listDebito", listDebito);
				ses.setAttribute("listCredito", listCredito);
				ses.setAttribute("descBanco", req.getParameter("E01DSCRBK"));
//PRO 0041
				if (Tipo_Pro.toString().trim().equalsIgnoreCase("c")){
					flexLog("About to call Page: ERC0047_search_data_conciliacion.jsp");
					forward("ERC0047_search_data_conciliacion.jsp", req, res);
				}else {
					flexLog("About to call Page: ERC0047_search_data_desconciliacion.jsp");
					forward("ERC0047_search_data_desconciliacion.jsp", req, res);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
			flexLog("About to call Page: ERC0047_enter.jsp");
			forward("ERC0047_enter.jsp", req, res);
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	protected void procActionConciliacion(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session){
		MessageProcessor mp;
		mp = null;
		@SuppressWarnings("unused")
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try{
			mp = getMessageProcessor("ERC0047",req);
			ERC004704Message message = new ERC004704Message();
			message.setH04USERID(user.getH01USR());
			message.setH04PROGRM("ERC0047");
			message.setH04TIMSYS(getTimeStamp());
			message.setH04OPECOD("0022");
			message.setH04SCRCOD("47");
			String[] listDebito = req.getParameterValues("rowHaber");
			String[] listCredito = req.getParameterValues("rowDebe");
			Class classDebito = Class.forName(ERC004704Message.class.getName());
			int indDebito = 1;
			Method campo = null;
			for (String debito : listDebito){
				if (indDebito < 10){
					campo = classDebito.getMethod("setE04TUID0" + indDebito, String.class);
				}else {
					campo = classDebito.getMethod("setE04TUID" + indDebito, String.class);
				}
				campo.invoke(message, debito);
				indDebito++;
			}
			int indCredito = 1;
			for (String credito : listCredito){
				if (indCredito < 10){
					campo = classDebito.getMethod("setE04SUID0" + indCredito, String.class);
				}else {
					campo = classDebito.getMethod("setE04SUID" + indCredito, String.class);
				}
				campo.invoke(message, credito);
				indCredito++;
			}
			mp.sendMessage(message);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC0047_search_data_conciliacion.jsp");
				forward("ERC0047_search_data_conciliacion.jsp", req, res);
			}else {
//PRO 0041 INI	
				req.setAttribute("H01FLGWKK", req.getParameter("H01FLGWKK"));
				req.setAttribute("E01RCHRBK", req.getParameter("E01RCHRBK"));
				req.setAttribute("E01BRMACC", req.getParameter("E01BRMACC"));
				req.setAttribute("E01BRMCTA", req.getParameter("E01BRMCTA"));
				
				req.setAttribute("E01DESDDM", req.getParameter("E01DESDDM"));
				req.setAttribute("E01DESDDD", req.getParameter("E01DESDDD"));
				req.setAttribute("E01DESDDY", req.getParameter("E01DESDDY"));
				
				req.setAttribute("E01HASDDM", req.getParameter("E01HASDDM"));
				req.setAttribute("E01HASDDD", req.getParameter("E01HASDDD"));
				req.setAttribute("E01HASDDY", req.getParameter("E01HASDDY"));
				
				procSearchData(user, req, res, session);
//PRO 0041 FIN	
			}
		}catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
		}
	}
	
	protected void procActionDesconciliacion(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session){
		MessageProcessor mp;
		mp = null;
		@SuppressWarnings("unused")
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try{
			mp = getMessageProcessor("ERC0047",req);
			ERC004705Message message = new ERC004705Message();
			message.setH05USERID(user.getH01USR());
			message.setH05PROGRM("ERC0047");
			message.setH05TIMSYS(getTimeStamp());
			message.setH05OPECOD("0024");
			message.setH05SCRCOD("47");
			message.setE05NROCON(req.getParameter("rowDebe"));
			message.setE05BRMACC(req.getParameter("E01BRMACC"));
			mp.sendMessage(message);
			
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC0047_search_data_desconciliacion.jsp");
				forward("ERC0047_search_data_desconciliacion.jsp", req, res);
			}else {
//PRO 0041 INI	
				req.setAttribute("H01FLGWKK", req.getParameter("H01FLGWKK"));
				req.setAttribute("E01RCHRBK", req.getParameter("E01RCHRBK"));
				req.setAttribute("E01BRMACC", req.getParameter("E01BRMACC"));
				req.setAttribute("E01BRMCTA", req.getParameter("E01BRMCTA"));
				
				req.setAttribute("E01DESDDM", req.getParameter("E01DESDDM"));
				req.setAttribute("E01DESDDD", req.getParameter("E01DESDDD"));
				req.setAttribute("E01DESDDY", req.getParameter("E01DESDDY"));
				
				req.setAttribute("E01HASDDM", req.getParameter("E01HASDDM"));
				req.setAttribute("E01HASDDD", req.getParameter("E01HASDDD"));
				req.setAttribute("E01HASDDY", req.getParameter("E01HASDDY"));
				
				procSearchData(user, req, res, session);
//PRO 0041 FIN	
			}
		}catch(Exception e){
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
		}
	}
}
