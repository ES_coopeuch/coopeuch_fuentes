<%@ page import = "datapro.eibs.master.Util" %>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "RteList" class= "datapro.eibs.beans.ERM030001Message"  scope="session" />
<title>Capitalización Voluntaria</title>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>


<SCRIPT LANGUAGE="JavaScript">
 builtHPopUp();

 function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
 }

</script>
</head>
<body>

<%
	if (!error.getERRNUM().equals("0")) {
		out.println("<script type=\"text/javascript\">");
		error.setERRNUM("0");
		out.println("showErrors()");
		out.println("</script>");
	}
%>

<h3 align="center">Capitalización Voluntaria<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="capitalizacion_detalle.jsp,ERM0300"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERM0300">
<input type="hidden" name="SCREEN" value="300">

  <INPUT TYPE=HIDDEN NAME="E01DEABNK" VALUE="01">

<br>



  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left">
                <input type="text" size="10" maxlength="9" name="E01RMMCUN" value="<%=RteList.getE01RMMCUN() %>" readonly>
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left">
                <input type="text" size="45" maxlength="45" name="E01RMMNA1" value="<%=RteList.getE01RMMNA1() %>" readonly>
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left">
                <input type="text" size="13" maxlength="12" name="E01RMMACC" value="<%=RteList.getE01RMMACC() %>" readonly>
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
                <input type="text" name="E01RMMCCY" size="4" maxlength="3" value="<%=RteList.getE01RMMCCY() %>" readonly>
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b>
                <input type="text" size="5" maxlength="4" name="E01RMMPRD" value="<%=RteList.getE01RMMPRD() %>" readonly>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  

<h4>Información de la cuenta</h4>

<table class="tableinfo" cellspacing="0" cellpadding="2" width="100%"border="0">


	<tr id="trclear">		
		<td nowrap width="30%">
<div align="right">Saldo Cuota:</div>
	  </td>
		<td nowrap width="30%" align="left">
		
	     <input type="text" name="E01RMMCDP" readonly size="17" maxlength="16" value="<%=RteList.getE01RMMCDP() %>">
          
	    
	
		</td>
        <td nowrap align="center" width="25%">
    

        </td> 
        		<td nowrap width="5%">
		</td>
	</tr>
	
	
		<tr id="trdark">		
		<td nowrap width="30%">
<div align="right">Saldo Cuota Pesos:</div>
	  </td>
		<td nowrap width="30%" align="left">
		
	        
	<eibsinput:text  property="E01RMMCLP" name="RteList" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>"/>      		
       
          
	    
	
		</td>
        <td nowrap align="center" width="25%">

        </td> 
        		<td nowrap width="5%">
		</td>
	</tr>
	<tr id="trclear">
			<td nowrap width="30%">
<div align="right">Valor Remanente:</div>
	  </td>
		<td nowrap width="30%">
	          	
	          	   <input type="text" name="E01RMMAMT" value="<%=RteList.getE01RMMAMT()%>" readonly>
        
	     
		</td>
        <td nowrap width="10%">
          
      
        </td> 
        		<td nowrap width="30%">

		</td>
	</tr>
	
	<tr id="trdark">

        <td nowrap width="10%">
          	<div align="right">Valor Cuota:</div>
      
        </td> 
        		<td nowrap width="30%">
 <eibsinput:text  property="E01RMMVCU" name="RteList" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>"  />
       
		</td>
		
		 <td nowrap width="10%">
          
      
        </td> 
        		<td nowrap width="30%">

		</td>
	</tr>
	

	</table>
 
<h4>Transacción</h4>

  <table class="tableinfo" cellspacing=0  width="100%" border="0"> 
  
  
    <tr id="trclear">
      <td nowrap> 
        <div align="right">Nº Cuotas a Comprar :</div>
      </td>
      <td width="50%"> 
        <div align="left"> 	
       
       <div align="left"> 
 	 <input type="text" name="E01RMMNCU" maxlength="9"  value="<%=RteList.getE01RMMNCU()  %>" onKeypress="enterInteger()" >
        </div>
        </div>
      </td>
      <td width="50%"> 
   
      </td>
      
    </tr>
 
  	<tr id="trdark"> 
        <td align=CENTER width="50%"> 
          <div align="right">Monto   en pesos :</div>
        </td>
        <td align=CENTER width="50%"> 
          <div align="left"> 
 	         <input type="text" name="E01RMMMCO" maxlength="20"  onKeypress="enterInteger()"  size="19" maxlength="19" value="<%=RteList.getE01RMMMCO() %>">
        
         
          </div>
        </td>
           <td > 
   
      </td>
      </tr>
<tr id="trclear"> 
        <td align=CENTER width="50%"> 
          <div align="right">Cuenta de Pago :</div>
        </td>
        <td nowrap> 
              <div align="center">Concepto</div>
            </td>
            <td nowrap> 
              <div align="center">Banco</div>
            </td>
            <td nowrap> 
              <div align="center">Sucursal</div>
            </td>
            <td nowrap> 
              <div align="center">Moneda</div>
            </td>        
            <td nowrap> 
              <div align="center">Referencia</div>
            </td>
      </tr>
      
        
     <tr id="trclear">
    <td>
    </td>
        <td nowrap> 
              <div align="center"> 
                <input type=text name="E01PAGOPC" value="<%=RteList.getE01OFFOPC() %>" size="3" maxlength="2">
                <input type=HIDDEN name="E01PAGOGL" value="<%=RteList.getE01OFFGLN() %>">
                <input type="text" name="E01PAGCON" size="26" maxlength="25" readonly value="<%=RteList.getE01OFFCON() %>"
                 		oncontextmenu="showPopUp(conceptHelp,this.name,document.forms[0].E01DEABNK.value,'','E01PAGOGL','E01PAGOPC','10')">
			  </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01PAGOBK" size="3" maxlength="2" value="<%=RteList.getE01OFFBNK() %>">
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01PAGOBR" size="5" maxlength="4" value="<%=RteList.getE01OFFBRN() %>"
                    oncontextmenu="showPopUp(branchHelp,this.name,document.forms[0].E01DEABNK.value,'','','','')">
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01PAGOCY" size="4" maxlength="3" value="<%=RteList.getE01OFFCCY() %>"
                 oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E01DEABNK.value,'','','','')">
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01PAGOAC" size="13" maxlength="12" value="<%=RteList.getE01OFFREF() %>"
                 oncontextmenu="showPopUp(accountCustomerHelp,this.name,document.forms[0].E01DEABNK.value,'',document.forms[0].E01DEACUN.value,'','RT'); return false;">
              </div>
            </td>
        </tr>   
      
        
  </table>
  
   <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
  
  

</form>
</body>
</html>
 