<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Grupos Econůmicos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ESD009201Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="JavaScript">



function goAction(op) {

	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}

function getParameters(tablecode,tablename) {

	document.forms[0].TABLECODE.value = tablecode;
	document.forms[0].TABLENAME.value = tablename;  
}


</SCRIPT>  

</head>

<BODY>
<h3 align="center">Mantenimiento Grupos Econůmicos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="groups_enter_list.jsp, ESD0092"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0092" >
<p> 
  <input type=HIDDEN name="SCREEN" value="200">
  <input type=HIDDEN name="totalRow" value="0">
  <input type=HIDDEN name="opt">
  <input type=HIDDEN name="TABLECODE" value=" " >   
  <input type=HIDDEN name="TABLENAME" value=" " >   
</p>

<p><%	if ( ESD009201Help.getNoResult() ) { %></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda. </b></p>
          <p>&nbsp;</p>
        </div>
	  </TD>
	</TR>
    </TABLE>
	
<% } else { %>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }
%> 
<p> 

          
<table class="tbenter" width=100% align=center>
  <tr> 
    <td class=TDBKG width="50%"> 
      <div align="center"><a href="javascript:goAction(2)"><b>Integrantes</b></a></div>
    </td>
    <td class=TDBKG width="50%"> 
      <div align="center"><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
    </td>
  </tr>
</table>
   
<br>
<TABLE  id="mainTable" class="tableinfo" ALIGN=CENTER style="width:'95%'">
 			   <TR> 
   			     <TD NOWRAP width="100%" >
  				<TABLE id="headTable" >
  				   <TR id="trdark">  
            		<th align=CENTER nowrap width="5%">&nbsp;</th>
            		<th align=CENTER nowrap width="20%">C&oacute;digo</th>
            		<th align=CENTER nowrap width="75%"> Descripci&oacute;n </th>
       			   </TR>
       			</TABLE>
  
   			    <div id="dataDiv1" class="scbarcolor">
    				<table id="dataTable" > 
          <%
                ESD009201Help.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (ESD009201Help.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  datapro.eibs.beans.ESD009201Message msgList = (datapro.eibs.beans.ESD009201Message) ESD009201Help.getRecord();
		 %>
		 	<tr> 
            <td NOWRAP  align=CENTER width="5%"> 
              <input type="radio" name="CURRCODE" value="<%= msgList.getE01CNORCD() %> "  <%=chk%> 
				onClick="javascript:getParameters('<%= msgList.getE01CNORCD() %>','<%= msgList.getE01CNODSC() %>');">
            </td>
            <td NOWRAP  align=CENTER width="20%"><%= msgList.getE01CNORCD() %></td>
            <td NOWRAP  align=LEFT width="75%"><%= msgList.getE01CNODSC() %></td>
          </tr>
          <%
                }
              %>
			 </table>
   			</div>
   			</TD>
   		      </TR>	
		    </TABLE>

	  
    				 
<SCRIPT language="JavaScript">
		showChecked("CURRCODE");
		function resizeDoc() {
   		 	divResize();
   		    adjustEquTables(headTable, dataTable, dataDiv1,1,false);
      		}
		resizeDoc();   			
   		window.onresize=resizeDoc;        
</SCRIPT>

<%}%>
</form>
</body>
</html>
