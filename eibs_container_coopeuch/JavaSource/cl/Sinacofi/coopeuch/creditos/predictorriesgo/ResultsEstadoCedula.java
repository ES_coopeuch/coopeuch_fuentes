/**
 * ResultsEstadoCedula.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResultsEstadoCedula  implements java.io.Serializable {
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.EstadoCedula estadoCedula;

    public ResultsEstadoCedula() {
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.EstadoCedula getEstadoCedula() {
        return estadoCedula;
    }

    public void setEstadoCedula(cl.Sinacofi.coopeuch.creditos.predictorriesgo.EstadoCedula estadoCedula) {
        this.estadoCedula = estadoCedula;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsEstadoCedula history = (ResultsEstadoCedula) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ResultsEstadoCedula other = (ResultsEstadoCedula) obj;
        boolean _equals;
        _equals = true
            && ((this.estadoCedula==null && other.getEstadoCedula()==null) || 
             (this.estadoCedula!=null &&
              this.estadoCedula.equals(other.getEstadoCedula())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsEstadoCedula history = (ResultsEstadoCedula) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getEstadoCedula() != null) {
            _hashCode += getEstadoCedula().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
