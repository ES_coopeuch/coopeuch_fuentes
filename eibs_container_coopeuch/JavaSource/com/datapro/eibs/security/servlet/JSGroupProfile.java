package com.datapro.eibs.security.servlet;

/**
 * Insert the type's description here.
 * Creation date: (7/19/00 6:55:55 PM)
 * @author: Enrique Almonte
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

import com.datapro.eibs.security.facade.*;
//import com.datapro.eibs.security.access.jdbc.bean.*;
import com.datapro.eibs.security.bean.*;
import com.datapro.eibs.security.object.*;
import com.datapro.generic.beanutil.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;


public class JSGroupProfile extends datapro.eibs.master.SuperServlet {

    //Requests
	protected static final int R_GROUP_INFO = 1;  //CTRLBTH 
	protected static final int R_GROUP_PRIVILEGES = 3; //CTRLPVL
	protected static final int R_GROUP_USERS = 5; //CTRLDBA
	
	//Actions
	protected static final int A_GROUP_INFO = 2;  //CTRLBTH + CTRLEUP
	protected static final int A_GROUP_PRIVILEGES = 4; //CTRLPVL
	protected static final int A_GROUP_BRANCH = 6; //CTRLDBA
	protected static final int A_GROUP_OPT = 8;
	
	protected static final int R_UPDATE_USER = 10;
	protected static final int A_UPDATE_USER = 20;
	

	protected static final String PAGE_GROUP_MAINT = "GroupProfile_maint.jsp";
	protected static final String PAGE_USER_GROUP = "GroupProfile_userlist.jsp";
	protected static final String PAGE_GROUP_PRIVILEGES = "GroupProfile_privileges.jsp";
	
	protected String LangPath = "S";
	
	/**
	 * JSEODPDF constructor comment.
	 */
	public JSGroupProfile() {
		super();
	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		//dbProp = new DBConnectorProperty();
	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {

			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}

		} else {
			try {

				msgUser = (ESS0030DSMessage) session.getAttribute("currUser");

				LangPath = super.rootPath + msgUser.getE01LAN() + "/";
				
				int screen = R_GROUP_INFO;
				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}

				switch (screen) {
					case R_GROUP_INFO :
						procReqGroups(req, res, session);
						break;
					case R_GROUP_USERS :
						procReqUsersByGroups(req, res, session);
						break;					
					case R_GROUP_PRIVILEGES :
						procReqGroupPrivileges(req, res, session);
						break;					
					case A_GROUP_PRIVILEGES :
						procActionGroupPrivileges(req, res, session);
						break;
					case A_GROUP_OPT :
						procActionGroupOption(req, res, session);
						break;
					default :
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}
		}

	}
	
	protected void procReqGroups(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		GroupProfile groupMng = new GroupProfile();
		JBObjList groupList = new JBObjList();
		
		try {
			
				groupList = groupMng.getGroup();
				if (groupList != null) {					
					ses.setAttribute("groupList",groupList);
					try {
						flexLog("About to call Page: " + LangPath + PAGE_GROUP_MAINT);
						callPage(LangPath + PAGE_GROUP_MAINT, req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else {
					
				}

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	protected void procReqUsersByGroups(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		GroupProfile groupMng = new GroupProfile();
		JBObjList userList = null;
		
		try {
				
				String GROUPID = req.getParameter("GROUPID");
				userList = groupMng.getUserByGroup(GROUPID);
				if (userList == null) {														
					userList = new JBObjList();					
				}
				ses.setAttribute("userList",userList);
				ses.setAttribute("groupID",GROUPID);
				try {
						flexLog("About to call Page: " + LangPath + PAGE_USER_GROUP);
						callPage(LangPath + PAGE_USER_GROUP, req, res);	
				}
				catch (Exception e) {
						flexLog("Exception calling page " + e);
				}

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	

	protected void procReqGroupPrivileges(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		JBObjList groupPriviList = new JBObjList();
		UserProfile userMng = new UserProfile();
		
		try {			
				String groupID= req.getParameter("groupID");				
				
				groupPriviList = userMng.getUserPrivilege(groupID);				
				ses.setAttribute("grpPriviList",groupPriviList);
				ses.setAttribute("groupID",groupID);
				try {
						flexLog("About to call Page: " + LangPath + PAGE_GROUP_PRIVILEGES);
						callPage(LangPath + PAGE_GROUP_PRIVILEGES,req,res);
					}
				catch (Exception e) {
						flexLog("Exception calling page " + e);
				}
				

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	protected void procActionGroupPrivileges(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		JBObjList userPriviList = new JBObjList();
		GroupProfile groupMng = new GroupProfile();
		CNTRLPVLNME privilege = null;
		
		try {			
				String action= req.getParameter("ACTION");
				String groupID= req.getParameter("PVLUSR");
							
				if (action.equals("D") || action.equals("M")) { //Delete || Modify
					int row = Integer.parseInt(req.getParameter("ROW"));					
					
					userPriviList = (JBObjList)ses.getAttribute("grpPriviList");
					userPriviList.setCurrentRow(row);
					privilege = (CNTRLPVLNME) userPriviList.getRecord();
					int result=0;			
					if (action.equals("D")) {
						result = groupMng.deleteGroupPrivilege(groupID,privilege.getPVLPVL());
					} else {												
						String newVal = req.getParameter("NEWVAL");
						result = groupMng.updateGroupPrivilege(groupID,privilege.getPVLPVL(),newVal);					
					}																			
					if (result != 0) {
						res.sendRedirect(super.srctx + "/servlet/com.datapro.eibs.security.servlet.JSGroupProfile?SCREEN=3&groupID="+ groupID);
					}	
					
				} else { //Add Privilege
					String newVal= req.getParameter("NEWVAL");
					String newPvl= req.getParameter("NEWPVL");
					int result= 0;
					if (groupMng.getGroupPrivilege(groupID,newPvl) == null ){
						result= groupMng.addGroupPrivilege(groupID,newPvl,newVal);		
					} else {
						//The privilege already exists
					}
					res.sendRedirect(super.srctx + "/servlet/com.datapro.eibs.security.servlet.JSGroupProfile?SCREEN=3&groupID="+ groupID);
						
				}			

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	protected void procActionGroupOption(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		JBObjList groupList = new JBObjList();
		JBObjList userList = new JBObjList();
		GroupProfile groupMng = new GroupProfile();
		UserProfile userMng = new UserProfile();
		CNTRLBTH grpBean = null;
		
		try {			
				String action= req.getParameter("ACTION");
				String groupID= req.getParameter("GROUPID");
				int result=0;			
				if (action.equals("D")) { //Delete 
					result = groupMng.delete(groupID);
					if (result != 0) {
						  int result1 = 0;
						  if (req.getParameter("USERID").equals("")) { //set unassigned group
						  	 result1 = groupMng.updateUserByGroup(groupID);
						  } else { //delete all users group		  	 
						  	 userList = groupMng.getUserByGroup(groupID);
						  	 if (!userList.getNoResult()) { //group with users
						  	 	 userList.initRow();
   		 						 while (userList.getNextRow()) {
          							grpBean = (CNTRLBTH) userList.getRecord();
          							result1 = userMng.delete(grpBean.getBTHKEY());
   		 						 }
						  	 } 
						  }
											
					}																			
						
					
				} else { //Add Group
					if (groupMng.getGroup(groupID)==null) {
						grpBean = new CNTRLBTH();
						grpBean.setBTHKEY(groupID);
						grpBean.setBTHF03("GRPPRGGRPPRG");
					//result= 0;
						result = groupMng.add(grpBean);	
					} else result = 1;					
				}
				
				if (result != 0) {
						res.sendRedirect(super.srctx + "/servlet/com.datapro.eibs.security.servlet.JSGroupProfile?SCREEN=1");
				}			

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
}
