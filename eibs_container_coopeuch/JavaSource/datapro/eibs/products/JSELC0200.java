/*
 * Created on Apr 7, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package datapro.eibs.products;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELC020001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageContextHandler;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JSELC0200 extends SuperServlet {

	
	String LangPath = "s/";
	
	public JSELC0200() {
		super();
	}
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	public void destroy() {
		flexLog("free resources used by JSELC0200");
	}

	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = (HttpSession) req.getSession(false);
		if (session == null) {
			try {
				res.setContentType("text/html");
				super.printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {
			int screen = -1;

			ESS0030DSMessage user = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");
			// Here we should get the path from the user profile
			LangPath = rootPath + user.getE01LAN() + "/";
			
			Socket s = null;
			try {
				s = new Socket(hostIP, getInitSocket(req) + 1);
				s.setSoTimeout(sckTimeOut);
				MessageContext mc =
					new MessageContext(
						new DataInputStream(new BufferedInputStream(s.getInputStream())),
						new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
						"datapro.eibs.beans");
						
				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
					flexLog("Screen  Number: " + screen);
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}
				
				String PageToCall = "";

				switch (screen) {
					case 1 : // Enter (Control de Documentos Recibidos)
						PageToCall = "ELC0200_lc_doc_enter.jsp";
						callPage(PageToCall, req, res);
						break;
					case 2 : // Call list (submit 1st page)
						requestList(mc, user, req, res, screen);
						break;
					case 3 : // CALL LC DOCUMENTS NEW
						requestNew(mc, user, req, res, screen);
						break;
					case 4 : // CALL LC DOCUMENTS MAINT
						requestMaintenance(mc, user, req, res, screen);
						break;
					case 5 :
						requestAction(mc, user, req, res, screen);
						break;
					default :
						PageToCall = "MISC_not_available.jsp";
						callPage(PageToCall, req, res);
						break;
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				res.sendRedirect(srctx + LangPath + sckNotRespondPage);
			} finally {
				if (s != null) s.close();
				flexLog("Socket used by JSELC0200 closed.");
			}
		}	
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void requestAction(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		String PageToCall = "ELC0200_lc_doc_info.jsp";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ELC020001Message msg = (ELC020001Message) msgHandle.initMessage("ELC020001", user.getH01USR(), "0005");
			initTransaction(userPO, String.valueOf(screen), "");
			msgHandle.setFieldsFromPage(req, msg);
			msgHandle.sendMessage(msg);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			boolean isNotError = msgError.getERRNUM().equals("0");
			msg = (ELC020001Message) msgHandle.receiveMessage();
			putDataInSession(session, userPO, msgError, msg, null);
			if (isNotError) {
				requestList(mc, user, req, res, 2);
			} else
				callPage(PageToCall, req, res);
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void requestMaintenance(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		String PageToCall = "ELC0200_lc_doc_info.jsp";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ELC020001Message msg = (ELC020001Message) msgHandle.initMessage("ELC020001", user.getH01USR(), "0002");
			initTransaction(userPO, String.valueOf(screen), "");
			try {
				msg.setE01LCMACC(userPO.getAccNum());
				msg.setE01LCIDNO(req.getParameter("E01LCIDNO"));
			} catch (Exception e) {
				flexLog("E01LCIDNO not available from page");
			}
			msgHandle.sendMessage(msg);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			msg = (ELC020001Message) msgHandle.receiveMessage();
			putDataInSession(session, userPO, msgError, msg, null);
			callPage(PageToCall, req, res);
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void requestNew(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		String PageToCall = "ELC0200_lc_doc_info.jsp";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ELC020001Message msg = (ELC020001Message) msgHandle.initMessage("ELC020001", user.getH01USR(), "0001");
			initTransaction(userPO, String.valueOf(screen), "");
			msg.setE01LCMACC(userPO.getAccNum());
			msgHandle.sendMessage(msg);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			msg = (ELC020001Message) msgHandle.receiveMessage();
			putDataInSession(session, userPO, msgError, msg, null);
			callPage(PageToCall, req, res);
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void requestList(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		String PageToCall = "ELC0200_lc_doc_list.jsp";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ELC020001Message msg = (ELC020001Message) msgHandle.initMessage("ELC020001", user.getH01USR(), "0015");
			initTransaction(userPO, String.valueOf(screen), "");
			try {
				msg.setE01LCMACC(req.getParameter("E01LCMACC"));
				userPO.setAccNum(msg.getE01LCMACC());
			} catch (Exception e) {
				flexLog("E01LCMACC not available from page");
			}
			try {
				msg.setE01LCIDNO(req.getParameter("E01LCIDNO"));
			} catch (Exception e) {
				flexLog("E01LCIDNO not available from page");
			}
			msgHandle.sendMessage(msg);
			JBObjList jbList = msgHandle.receiveMessageList("H01FLGMAS");
			putDataInSession(session, userPO, null, msg, jbList);
			callPage(PageToCall, req, res);
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}
				
	private void initTransaction(UserPos userPO, String optMenu, String purpose) {
		try {
			userPO.setOption(optMenu);
			userPO.setPurpose(purpose);
			
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}
		try {
		} catch (Exception ex) {
			flexLog("Error getting userPO from session: " + ex);
		}
	}
	
	public void callPage(String page, HttpServletRequest req, HttpServletResponse res) {
		try {
			super.callPage(LangPath + page, req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e.toString() + e.getMessage());
		}
		return; 
	}
	
	private void putDataInSession(HttpSession session, UserPos userPO, ELEERRMessage msgError, ELC020001Message msg, JBObjList jbList) {
		flexLog("Putting java beans into the session");

		if (msgError == null) {
			msgError = new ELEERRMessage(); 
		}
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		if (msg != null) session.setAttribute("msg01", msg);
		if (jbList != null) session.setAttribute("jbList", jbList);
	}

}
