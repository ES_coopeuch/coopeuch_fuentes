/**
 * NivelDescuento.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public class NivelDescuento  implements java.io.Serializable {
    private int orden;
    private int idRol;
    private java.lang.String descripcionRol;
    private java.math.BigDecimal valor;

    public NivelDescuento() {
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public int getIdRol() {
        return idRol;
    }

    public void setIdRol(int idRol) {
        this.idRol = idRol;
    }

    public java.lang.String getDescripcionRol() {
        return descripcionRol;
    }

    public void setDescripcionRol(java.lang.String descripcionRol) {
        this.descripcionRol = descripcionRol;
    }

    public java.math.BigDecimal getValor() {
        return valor;
    }

    public void setValor(java.math.BigDecimal valor) {
        this.valor = valor;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        NivelDescuento other = (NivelDescuento) obj;
        boolean _equals;
        _equals = true
            && this.orden == other.getOrden()
            && this.idRol == other.getIdRol()
            && ((this.descripcionRol==null && other.getDescripcionRol()==null) || 
             (this.descripcionRol!=null &&
              this.descripcionRol.equals(other.getDescripcionRol())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        NivelDescuento history = (NivelDescuento) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.valor==null && other.getValor()==null) || 
             (this.valor!=null &&
              this.valor.equals(other.getValor())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        NivelDescuento history = (NivelDescuento) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        _hashCode += getOrden();
        _hashCode += getIdRol();
        if (getDescripcionRol() != null) {
            _hashCode += getDescripcionRol().hashCode();
        }
        if (getValor() != null) {
            _hashCode += getValor().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
