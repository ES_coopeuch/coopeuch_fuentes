<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV100503Message,datapro.eibs.beans.EPV101003Message"%>

<html>    
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="platfObj" class="datapro.eibs.beans.EPV101001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<jsp:useBean id="EPV100503List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="EPV100504List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="EPV101003List" class="datapro.eibs.beans.JBObjList" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

 function setRecalculate() {
	  document.forms[0].RECALC.checked = false;
	  UpdateFlag(false);  
 }
 

 
 function UpdateFlag(val) {
  document.forms[0].H02FLGWK2.value = (val==true)?"X":"";
 }

function UpdateFlag3(val) {
  document.forms[0].H02FLGWK3.value = (val==true)?"X":"";
}

  function introducirAval() {
   <% session.setAttribute("EMPTI","I"); %>  
	var pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1140?SCREEN=101&E01PVTCUN=<%=platfObj.getE01PVMCUN()%>&E01PVTNUM=<%=platfObj.getE01PVMNUM()%>";			  
	CenterWindow(pg,750,600,2);	
 }

 
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">CONSULTA EVALUACION SOLICITUD DE CREDITO <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_valuacion_inquiry.jsp,EPV1010"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1010">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="">
  <input type=HIDDEN name="customer_number"  value="<%= userPO.getCusNum()%>">
  <input type=HIDDEN name="E02PVMBNK"  value="<%= currUser.getE01UBK().trim()%>"> 
  <INPUT TYPE=HIDDEN NAME="H02FLGWK2" VALUE="<%=platfObj.getH01FLGWK2()%>">
  <INPUT TYPE=HIDDEN NAME="H02FLGWK3" VALUE="<%=platfObj.getH01FLGWK3()%>">
  <INPUT TYPE=HIDDEN NAME="E01LNTERM" VALUE="<%=platfObj.getE01LNTERM()%>">
  <INPUT TYPE=HIDDEN NAME="E01LNTERC" VALUE="<%=platfObj.getE01LNTERC()%>">
  <INPUT TYPE=HIDDEN NAME="E01LNTYPE" VALUE="<%=platfObj.getE01LNTYPE()%>">
 <% int row = 0;%>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="platfObj" property="E01PVMCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Nombre :</td>
             <td nowrap align="left" width="40%">
	  			<eibsinput:text name="platfObj" property="E01CUSNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="platfObj" property="E01PVMNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="40%">
    	        <eibsinput:date name="platfObj" fn_year="E01PVMOPY" fn_month="E01PVMOPM" fn_day="E01PVMOPD" readonly="true" readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Sucursal :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="platfObj" property="E01PVMBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
             </td>
             <td nowrap align="right" width="20%"> Ejecutivo :</td>
             <td nowrap align="left" width="40%">
	  			<eibsinput:text name="platfObj" property="E01PVMOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>" readonly="true"/>
             </td>
         </tr>         
        </table>
      </td>
    </tr>
  </table>

<br/>

  <table class=tbenter>
   <tr > 
      <td nowrap> 
		  <h4>Valores Maximo Permitidos (Pesos)</h4>
      </td>
      <td nowrap align=right> 
   		<b>Estado :</b>
      </td>
      <td nowrap> 
		  <INPUT TYPE=HIDDEN NAME="E01DSCSTS" VALUE="<%=platfObj.getE01DSCSTS()%>">  
   		<b><font color="#ff6600"><%= platfObj.getE01DSCSTS().trim()%></font></b>
      </td>
    </tr>
  </table>

  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
 		<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Evaluaci�n :</div>
            </td>
            <td nowrap width="30%">
			  <INPUT TYPE=HIDDEN NAME="E01LNTYPG" VALUE="<%=platfObj.getE01LNTYPG()%>">  
            	<b>
                <%=platfObj.getE01LNTYPG()%> 
					<% if (platfObj.getE01LNTYPG().equals("D")) out.print("- PAGO DIRECTO");%>
					<% if (platfObj.getE01LNTYPG().equals("P")) out.print("- DESCUENTO POR PLANILLA");%>
					<% if (platfObj.getE01LNTYPG().equals("T")) out.print("- TARJETA DE CREDITO");%>
					<% if (platfObj.getE01LNTYPG().equals("E")) out.print("- COMPRA CARTERA");%>
					<% if (platfObj.getE01LNTYPG().equals("M")) out.print("- MICROCR�DITO");%>
					<% if (platfObj.getE01LNTYPG().equals("S")) out.print("- EVALUACION ESPECIAL");%>
				</b>					
             </td>
            <td nowrap width="20%"> 
              <div align="right">Plazo Maximo :</div>
           </td>
            <td nowrap width="30%">            
				<eibsinput:text name="platfObj" property="E01MXTERM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" readonly="true" />	            
            </td>
          </tr>        
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Maximo Valor Cuota :</div>
            </td>
            <td nowrap width="30%">             
            	<eibsinput:text name="platfObj" property="E01MXVACU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/> 
			</td>
     <%--    <td nowrap width="20%"> 
              <div align="right">Monto Maximo :</div>
            </td>
            <td nowrap width="30%">
				<eibsinput:text name="platfObj" property="E01MXVADE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>            
            </td> --%>
           <td nowrap width="20%"> 
              &nbsp;
            </td>   
           <td nowrap width="30%"> 
              &nbsp;
            </td>             
          </tr>
        </table>
        </td>
    </tr>
  </table> 
  
  <h4>Valores Negociados</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%"> 
              <div align="right">Producto :</div>
            </td>            
    <%--       <td nowrap width="20%">
				<select name="E01LNPROD"  >
					<option value="">Ninguno</option>
					<%	String primerp="";
	            	if (EPV100504List!=null && !EPV100504List.getNoResult()) {
	            		EPV100504List.initRow();
	            		while (EPV100504List.getNextRow()) {
							EPV100504Message convObj = (EPV100504Message) EPV100504List.getRecord();							
								out.println("<option value='"+convObj.getE04PVMPRO()+"'>"+convObj.getE04DSCPRO()+"</option>");
    		        	}
    		        }
            	 %>
				</select> 
				<script type="text/javascript">
					document.forms[0].E01LNPROD.value='<%=platfObj.getE01LNPROD()%>'
				</script>
            </td>   --%>
            <td nowrap width="20%">
				<eibsinput:text name="platfObj" property="E01LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>
 				 <b><%=platfObj.getE01DSPROD()%></b> 
				 <input type="hidden" name="E01DSPROD" value="<%=platfObj.getE01DSPROD()%>">					            
            </td>            
            <td nowrap width="10%"> 
              <div align="right">Tasa :</div>
            </td>
            <td nowrap width="20%">
				<eibsinput:text name="platfObj" property="E01LNTSPR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>            
            </td>
            <td nowrap width="10%"> 
              <div align="right">PV   M�ximo :</div>
           </td>
            <td nowrap width="20%">            
	            <eibsinput:text name="platfObj" property="E01LNOAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"   readonly="true"/> 
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%" > 
              <div align="right">Promociones :</div>
            </td>
            <td nowrap width="20%" >
				<select name="E01LNPROM"  disabled="disabled">
					<option value="">Ninguno</option>            
            	<%
	            	if (EPV100503List!=null && !EPV100503List.getNoResult()) {
	            		EPV100503List.initRow();
	            		while (EPV100503List.getNextRow()) {
							EPV100503Message convObj = (EPV100503Message) EPV100503List.getRecord();
							if ("P".equals(convObj.getE03PVCTYP().trim())){ 
								out.println("<option value='"+convObj.getE03PVCCOD()+"'>"+convObj.getE03PVCDES()+"</option>");							
							}
    		        	}
    		        }
            	 %>
				</select> 
				<script type="text/javascript">
					document.forms[0].E01LNPROM.value='<%=platfObj.getE01LNPROM()%>'
				</script>			        	 	
            </td>
            <td nowrap width="10%"> 
              <div align="right"></div>
            </td>
            <td nowrap width="20%">
				<eibsinput:text name="platfObj" property="E01LNTSPO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>            
            </td>
            <td nowrap width="10%"> 
              <div align="right">Valor de la Cuota :</div>
            </td>
            <td nowrap width="20%">
            <eibsinput:text name="platfObj" property="E01LNCUAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"    readonly="true"/>	
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%" >
            	<div align="right">Descuentos :</div>
            </td>
            <td nowrap width="20%" >
				<select name="E01LNDESC" disabled="disabled">
					<option value="">Ninguno</option>
					<%
	            	if (EPV100503List!=null && !EPV100503List.getNoResult()) {
	            		EPV100503List.initRow();
	            		while (EPV100503List.getNextRow()) {
							EPV100503Message convObj = (EPV100503Message) EPV100503List.getRecord();
							if ("D".equals(convObj.getE03PVCTYP().trim())){ 
								out.println("<option value='"+convObj.getE03PVCCOD()+"'>"+convObj.getE03PVCDES()+"</option>");							
							}
    		        	}
    		        }
            	 %>
				</select> 
				<script type="text/javascript">
					document.forms[0].E01LNDESC.value='<%=platfObj.getE01LNDESC()%>'
				</script>
            </td>
            <td nowrap width="10%"> 
              <div align="right"></div>
            </td>
            <td nowrap width="20%">
				<eibsinput:text name="platfObj" property="E01LNTSDS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>            
            </td>
            <td nowrap width="10%"> 
              <div align="right"><!-- Renegociar : --></div>
            </td>
            <td nowrap width="20%">
            <!-- 
				<input type="checkbox" name="flat_E01LNFRNG" onclick="if (document.forms[0].flat_E01LNFRNG.checked ){document.forms[0].E01LNFRNG.value='Y'}else{document.forms[0].E01LNFRNG.value='N'}" 
            		<%=("Y".equals(platfObj.getE01LNFRNG())?"checked='checked'":"")%> disabled="disabled">
             -->	
				<input type="hidden" name="E01LNFRNG" value="<%= (!"Y".equals( platfObj.getE01LNFRNG().trim())?"N": platfObj.getE01LNFRNG().trim())%>" >            	
            </td>
          </tr>                 
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="10%"> 
              <div align="right">C&oacute;digo de Convenio :</div>
            </td>
            <td nowrap width="20%">
             <eibsinput:text name="platfObj" property="E01LNCONV" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true" />
            </td>
            <td nowrap width="10%"> 
              <div align="right"></div>
            </td>
            <td nowrap width="20%">
			<eibsinput:text name="platfObj" property="E01LNDRAT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>            
            </td>
            <td nowrap width="10%"> 
              <div align="right">Tasa de Interes :</div>
            </td>
            <td nowrap width="20%">
            	<eibsinput:text name="platfObj" property="E01LNRATE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>"     readonly="true"/> 
            </td>
          </tr> 
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="27%"> 
              <div align="right">Tipo Campa�a :</div>    
             </td>
            <td nowrap width="29%"> 
              <eibsinput:text property="E01DSCTYP" name="platfObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="true" />
            </td>
            <td nowrap width="23%"> 
              &nbsp;
            </td>
           <td nowrap width="23%"> 
              &nbsp;
            </td> 
           <td nowrap width="23%"> 
              &nbsp;
            </td>   
           <td nowrap width="23%"> 
              &nbsp;
            </td>                                  
          </tr>                                                           
        </table>
      </td>
    </tr>
  </table>   

  <h4>Reliquidaciones de Creditos</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <th nowrap width="5%">&nbsp;</th>
            <th nowrap width="5%" align="left">Prod.</th>
            <th nowrap width="20%" align="center">Descripcion</th>
            <th nowrap width="10%" align="right">Prestamo</th>
            <th nowrap width="10%" align="center">Convenio</th>
            <th nowrap width="10%" align="right">Cuotas</th>
            <th nowrap width="10%" align="right">Pagas</th>
            <th nowrap width="15%" align="right">Valor Cuota</th>        
            <th nowrap width="15%" align="right">Dev. Primas</th>  
            <th nowrap width="15%" align="right">Com. Prepago</th>                                                   
            <th nowrap width="15%" align="right">Saldo</th>
          </tr>
 
          <%
           for (int ix=1;ix<10;ix++) {
		    if(!platfObj.getField("E01RPPRD"+ix).getString().trim().equals("")){
          %> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="5%">
            	<input type="checkbox" name="flat<%=ix%>"  onclick="setRecalculate();if (document.forms[0].flat<%=ix%>.checked ){document.forms[0].<%="E01RPSEL"+ix%>.value='Y'}else{document.forms[0].<%="E01RPSEL"+ix%>.value='N'}" 
            		<%=("Y".equals(platfObj.getFieldString("E01RPSEL"+ix))?"checked='checked'":"")%> disabled="disabled">
            	<input type="hidden" name="E01RPSEL<%=ix%>" value="<%= (!"Y".equals( platfObj.getFieldString("E01RPSEL"+ix).trim())?"N": platfObj.getFieldString("E01RPSEL"+ix).trim())%>" size="2" maxlength="1">
            </td>
            <td nowrap width="5%" align="left"> <%=platfObj.getFieldString("E01RPPRD"+ix)%> <input type=HIDDEN name="<%="E01RPPRD"+ix%>"  value="<%=platfObj.getFieldString("E01RPPRD"+ix) %>"> </td>
            <td nowrap width="20%" align="left">  <%=platfObj.getFieldString("E01RPDSC"+ix)%> <input type=HIDDEN name="<%="E01RPDSC"+ix%>"  value="<%=platfObj.getFieldString("E01RPDSC"+ix) %>"> </td>
            <td nowrap width="10%" align="right"> <%=platfObj.getFieldString("E01RPACC"+ix)%> <input type=HIDDEN name="<%="E01RPACC"+ix%>"  value="<%=platfObj.getFieldString("E01RPACC"+ix) %>"> </td>
            <td nowrap width="10%" align="center"> <%=platfObj.getFieldString("E01RPCNV"+ix)%> <input type=HIDDEN name="<%="E01RPCNV"+ix%>"  value="<%=platfObj.getFieldString("E01RPCNV"+ix) %>"> </td>
            <td nowrap width="10%" align="right"> <%=platfObj.getFieldString("E01RPTCU"+ix)%> <input type=HIDDEN name="<%="E01RPTCU"+ix%>"  value="<%=platfObj.getFieldString("E01RPTCU"+ix) %>"> </td>
            <td nowrap width="10%" align="right"> <%=platfObj.getFieldString("E01RPCPA"+ix)%> <input type=HIDDEN name="<%="E01RPCPA"+ix%>"  value="<%=platfObj.getFieldString("E01RPCPA"+ix) %>"> </td>
            <td nowrap width="15%" align="right"> <%=platfObj.getFieldString("E01RPVAC"+ix)%> <input type=HIDDEN name="<%="E01RPVAC"+ix%>"  value="<%=platfObj.getFieldString("E01RPVAC"+ix) %>"> </td>
            <td nowrap width="15%" align="right"> <%=platfObj.getFieldString("E01RPDEV"+ix)%> <input type=HIDDEN name="<%="E01RPDEV"+ix%>"  value="<%=platfObj.getFieldString("E01RPDEV"+ix) %>"> </td>
            <td nowrap width="15%" align="right"> <%=platfObj.getFieldString("E01RPCOM"+ix)%> <input type=HIDDEN name="<%="E01RPCOM"+ix%>"  value="<%=platfObj.getFieldString("E01RPCOM"+ix) %>"> </td>                        
            <td nowrap width="15%" align="right"> <%=platfObj.getFieldString("E01RPSAL"+ix)%> <input type=HIDDEN name="<%="E01RPSAL"+ix%>"  value="<%=platfObj.getFieldString("E01RPSAL"+ix) %>"> </td>
          </tr> 
   		  <%}
   		   }%>
			          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
          	<td> </td>
          	<td> </td>
          	<td> </td>
            <td nowrap align="right" colspan="7"><b>TOTAL A RELIQUIDAR :</b></td>
            <td nowrap align="right" colspan="1"><b> <%=platfObj.getFieldString("E01RPVTOT")%></b></td>
          </tr>                                                                                                                   
        </table>
        </td>
    </tr>
  </table>    
 
    <h4>Liquido Socio</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Monto Liquido :</div>
            </td>
            <td nowrap width="30%">
				<eibsinput:text name="platfObj" property="E01LNNAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"  readonly="true"/> 			           
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
           </td>
            <td nowrap width="30%">            
	           &nbsp;
            </td>
          </tr> 
        </table>
      </td>
    </tr>
  </table>   
   
 <h4>Cheques a Terceros</h4> 
 <% session.setAttribute("EMPCT","I"); %>
  <table class="tableinfo">
    <tr > 
      <td nowrap >     
		<iframe frameborder="0" scrolling="auto" width="100%" height="290" 
			src="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1150?SCREEN=101&E01PVCCUN=<%=platfObj.getE01PVMCUN()%>&E01PVCNUM=<%=platfObj.getE01PVMNUM()%>">		
		</iframe>  
       </td>
    </tr>
  </table>
  
  <h4>Tarjetas Alianzas</h4>
  <% session.setAttribute("EMPTA","I"); %>
  <table class="tableinfo">
    <tr > 
      <td nowrap >     
		<iframe frameborder="0" scrolling="auto" width="100%" height="290" 
			src="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1160?SCREEN=101&E01PVTCUN=<%=platfObj.getE01PVMCUN()%>&E01PVTNUM=<%=platfObj.getE01PVMNUM()%>">		
		</iframe>  
       </td>
    </tr>
  </table>  
  
  <h4>Tienda Virtual</h4>
  <% session.setAttribute("EMPTV","I"); %>
  <table class="tableinfo">
    <tr > 
      <td nowrap >     
		<iframe frameborder="0" scrolling="auto" width="100%" height="290" 
			src="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1170?SCREEN=101&E01PTVCUN=<%=platfObj.getE01PVMCUN()%>&E01PTVNUM=<%=platfObj.getE01PVMNUM()%>">		
		</iframe>  
       </td>
    </tr>
  </table> 

 <h4>Seguros Financiados</h4> 
 <% session.setAttribute("EMPSG","I"); %>
  <table class="tableinfo">
    <tr > 
      <td nowrap >     
		<iframe frameborder="0" scrolling="auto" width="100%" height="290" 
			src="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1180?SCREEN=101&E01PSGCUN=<%=platfObj.getE01PVMCUN()%>&E01PSGNUM=<%=platfObj.getE01PVMNUM()%>">		
		</iframe>  
       </td>
    </tr>
  </table>
  
  <h4>Cargos Adicionales</h4>
  <% session.setAttribute("EMPCA","I"); %>
  <table class="tableinfo">
    <tr > 
      <td nowrap >     
		<iframe frameborder="0" scrolling="auto" width="100%" height="290" 
			src="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1165?SCREEN=101&E01PVHCUN=<%=platfObj.getE01PVMCUN()%>&E01PVHNUM=<%=platfObj.getE01PVMNUM()%>">		
		</iframe>  
       </td>
    </tr>
  </table>  
  
  <h4>Montos que Componen el Cr�dito</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Liquido :</div>
            </td>
            <td nowrap width="30%">
	            <input type="text" size="22" value="<%=platfObj.getE01LNNAMT() %>" name="E01LNNAMT_Display" readonly="readonly" class="TXTRIGHT">
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
           </td>
            <td nowrap width="30%">             
				&nbsp;				      
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Reliquidaci�n :</div>					             
            </td>
            <td nowrap width="30%" >
           	 	<eibsinput:text name="platfObj" property="E01SIRELI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Monto Bruto :</div>
            </td>
            <td nowrap width="30%">	
					<eibsinput:text name="platfObj" property="E01SIBAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>            
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Cheques Terceros :</div>
            </td>
            <td nowrap width="30%" >
				<eibsinput:text name="platfObj" property="E01SICHKT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>  
            </td>
            <td nowrap width="20%"> 
              <div align="right">Plazo :</div>
            </td>
            <td nowrap width="30%">
			<eibsinput:text name="platfObj" property="E01SICUPA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" readonly="true" />  
			<input type="hidden" name="E01SIPTPG" value="M">
       		<input type="text" name="E01SIPTPG_desc" value="Mes(es)" size="7" readonly="readonly">				
            </td>
          </tr>                 
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Tarjeta Alianzas :</div>
            </td>
            <td nowrap width="30%">
					<eibsinput:text name="platfObj" property="E01SITALI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>            
            </td>
            <td nowrap width="20%"> 
              <div align="right">1er   Vencimiento :</div>
            </td>
            <td nowrap width="30%">            	
				<input type="text"  name="E01SIPXPD" size="3" maxlength="2" value="<%=platfObj.getE01SIPXPD()%>" onkeypress=" enterInteger()"  readonly="readonly"/>
				<input type="text"  name="E01SIPXPM" size="3" maxlength="2" value="<%=platfObj.getE01SIPXPM()%>" onkeypress=" enterInteger()"  readonly="readonly"/>
				<input type="text"  name="E01SIPXPY" size="5" maxlength="4" value="<%=platfObj.getE01SIPXPY()%>" onkeypress=" enterInteger()"  readonly="readonly"/>  
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Tienda  Virtual :</div>
            </td>
            <td nowrap width="30%">
				<eibsinput:text name="platfObj" property="E01SITVIR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>            
            </td>
            <td nowrap width="20%"></td>
            <td nowrap width="30%">
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Seguro Financiado :</div>
            </td>
            <td nowrap width="30%">
					<eibsinput:text name="platfObj" property="E01SISEGU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>           
            </td>
            <td nowrap width="20%"> 
              <div align="right">Valor Cuota :</div>
            </td>
            <td nowrap width="30%">	
					<eibsinput:text name="platfObj" property="E01SICUAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>		            
            </td>  
          </tr> 
          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cargos Adicionales :</div>
            </td>
            <td nowrap width="30%">
					<eibsinput:text name="platfObj" property="E01SICARA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>          
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tasa de Interes :</div>
            </td>
            <td nowrap width="30%">
            	<%if ("M".equals(platfObj.getE01LNTYPG())){ %>
            		<eibsinput:text name="platfObj" property="E01SIRATE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" />
            	<%}else{%>
					<eibsinput:text name="platfObj" property="E01SIRATE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>            	            
            	<%} %>
			</td>  
          </tr>
 		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td> 
            <td nowrap width="30%">
	            <select name="E01LNPVIA" disabled="disabled">
					<option value=""
						<% if (platfObj.getE01LNPVIA().equals("")) out.print("selected");%>>Caja</option>
					<option value="1"
						<% if (platfObj.getE01LNPVIA().equals("1")) out.print("selected");%>>PAC</option>							            
					<option value="2"
						<% if (platfObj.getE01LNPVIA().equals("2")) out.print("selected");%>>Convenio</option>
					<option value="4"
						<% if (platfObj.getE01LNPVIA().equals("4")) out.print("selected");%>>PAC/Multibanco</option>	
				</select>
			</td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:help name="platfObj" property="E01LNPACC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" fn_param_one="E01LNPACC" fn_param_two="document.forms[0].E02PVMBNK.value" fn_param_three="RT"  readonly="true" />
            </td>

          </tr>
                    
          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td nowrap colspan="2" align="center"> 
 				&nbsp;
            </td>
            <td nowrap colspan="2" align="center"> 
            	&nbsp;
            </td>
          </tr>   
                     
        </table>
      </td>
    </tr>
  </table> 

  <h4>Indicadores</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Indice de Liquidez / Carga Financiera :</div>					              
            </td>
            <td nowrap width="30%">				           
				<eibsinput:text name="platfObj" property="E01SIINDL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>            
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cae   Mensual :</div>
           </td>
            <td nowrap width="30%"> 
				<eibsinput:text name="platfObj" property="E01SICAEM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>            
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Endeudamiento Global :</div>
            </td>
            <td nowrap width="30%" >
           	 	<eibsinput:text name="platfObj" property="E01SIENDG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cae Anual :</div>
            </td>
            <td nowrap width="30%">	
	            <eibsinput:text name="platfObj" property="E01SICAEA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Costo Total del Credito :</div>
            </td>
            <td nowrap width="30%" >
            	<eibsinput:text name="platfObj" property="E01SICTOT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="20%">
            </td>
            <td nowrap width="30%">
          	             
            </td>
          </tr>                                       
        </table>
      </td>
    </tr>
  </table> 
    
  <br/>
   <div align="center">
		<input id="EIBSBTN" type=button name="Aval" value="Aval"  onclick="introducirAval();">   
   </div>
                     
  </form>
</body>
</HTML>
