package com.datapro.eibs.images.vo;

import com.datapro.eibs.access.VOSuper;

public class SCNIMGViewSignature extends VOSuper {
	
	private String TBLDSC = "";
	private byte[] IMGBIN = null;
	private int IMGLEN = 0;
	
	public byte[] getIMGBIN() {
		return IMGBIN;
	}
	public void setIMGBIN(byte[] imgbin) {
		IMGBIN = imgbin;
	}
	public int getIMGLEN() {
		return IMGLEN;
	}
	public void setIMGLEN(int imglen) {
		IMGLEN = imglen;
	}
	public String getTBLDSC() {
		return TBLDSC;
	}
	public void setTBLDSC(String tbldsc) {
		TBLDSC = tbldsc;
	}
	

}
