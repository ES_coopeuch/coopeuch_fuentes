<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV101003Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="liquidacion" class="datapro.eibs.beans.EPV101002Message"  scope="session" />
<jsp:useBean id="EPV101003List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "servlet" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "pagOperation" class= "java.lang.String"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

</head>

<%if(!liquidacion.getE02LNTYPG().equals("T")){%>
<body onload="tableresize();">
<%}else{%>
<body>
<%} %> 

<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<SCRIPT LANGUAGE="javascript">
builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   if (field.substring(0,8) == "E02OFFAC"){
     var index = field.substr(8,1);
     var concepto=document.getElementById("E02OFFOP"+index).value;
     if (concepto == "01")
       Client=document.getElementById("E02PVMCUN").value;
     else
       Client="";                          
   }
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }
</SCRIPT>

<h3 align="center"> Plataforma de Ventas - Liquidacion<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_liquidacion.jsp,JSEPV1010"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1010">
<input type="hidden" name="SCREEN" value="701"> 
  <input type=HIDDEN name="E02PVMBNK"  value="<%= liquidacion.getE02PVMBNK().trim()%>">
  <input type=HIDDEN name="E02PVMCCY"  value="<%= liquidacion.getE02PVMCCY().trim()%>">    
  <input type=HIDDEN name="E02SELBC1"  value="<%= liquidacion.getE02SELBC1().trim()%>">
  <input type=HIDDEN name="E02SELBC2"  value="<%= liquidacion.getE02SELBC2().trim()%>">
  <input type=HIDDEN name="E02SELBD1"  value="<%= liquidacion.getE02SELBD1().trim()%>">
  <input type=HIDDEN name="E02SELBD2"  value="<%= liquidacion.getE02SELBD2().trim()%>">
  <input type=HIDDEN name="H02FLGWK1"  value="<%= liquidacion.getH02FLGWK1().trim()%>">
  
  <input type="hidden" name="servlet" value="<%= servlet %>" size=15 maxlength=9 >
  <input type="hidden" name="pagOperation" value="<%= pagOperation %>" size=15 maxlength=9 >
  
 <% int row = 0;%>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="liquidacion" property="E02PVMCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Nombre :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="liquidacion" property="E02CUSNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="liquidacion" property="E02PVMNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="30%">
    	        <eibsinput:date name="liquidacion" fn_year="E02PVMOPY" fn_month="E02PVMOPM" fn_day="E02PVMOPD" readonly="true" readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Sucursal :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="liquidacion" property="E02PVMBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
             </td>
             <td nowrap align="right" width="20%"> Ejecutivo :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="liquidacion" property="E02PVMOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>" readonly="true"/>
             </td>
         </tr>         
        </table>
      </td>
    </tr>
  </table>

<%if(liquidacion.getE02LNTYPG().equals("T")){%> 

  <h4>Informacion Tarjeta de Credito </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="liquidacion" property="E02LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="liquidacion" property="E02DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Direccion :</div>
            </td>
            <td nowrap width="30%">
              <input type="text" name="E02TCCMLA" size="3" maxlength="2" value="<%= liquidacion.getE02TCCMLA().trim()%>" readonly >
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">  
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Local :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="liquidacion" property="E02TCCUMB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Extranjera :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="liquidacion" property="E02TCCUME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
          </tr>                  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Dia de Pago :</div>
            </td>
            <td nowrap width="30%">             
				<select name="E02TCDYPG" disabled>
					<option></option>
					<option value="3" <% if (liquidacion.getE02TCDYPG().equals("3")) out.print("selected");%>>3</option>
					<option value="23" <% if (liquidacion.getE02TCDYPG().equals("23")) out.print("selected");%>>23</option>
				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Porcentaje de Pago :</div>
            </td>
            <td nowrap width="30%">            
				<select name="E02TCPRPG" disabled>
					<option></option>
					<option value="5" <% if (liquidacion.getE02TCPRPG().equals("5")) out.print("selected");%>>El 5%</option>
					<option value="100" <% if (liquidacion.getE02TCPRPG().equals("100")) out.print("selected");%>>El 100%</option>
				</select>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">
				<select name="E02TCMEPG" disabled>
					<option></option>
					<option value="1" <% if (liquidacion.getE02TCMEPG().equals("1")) out.print("selected");%>>Con Cuenta</option>
					<option value="2" <% if (liquidacion.getE02TCMEPG().equals("2")) out.print("selected");%>>Sin Cuenta</option>
				</select>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:text name="liquidacion" property="E02TCACPG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
            </td>
          </tr>  

        </table>
      </td>
    </tr>
  </table>

<%} else {%> 

  <h4>Informacion del Credito </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Tipo de Producto :</div>
            </td>
            <td nowrap width="30%" >
            	<eibsinput:text name="liquidacion" property="E02LNTYPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>	
            	<eibsinput:text name="liquidacion" property="E02DSTYPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Termino del Contrato :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="liquidacion" property="E02LNTERM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" readonly="true" /> 
	            <input type="text" readonly name="E02LNTERC" size="10" 
				  value="<% if (liquidacion.getE02LNTERC().equals("D")) out.print("D&iacute;a(s)");
							else if (liquidacion.getE02LNTERC().equals("M")) out.print("Mes(es)");
							else if (liquidacion.getE02LNTERC().equals("Y")) out.print("A&ntilde;o(s)");
							else out.print("");%>" >
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="liquidacion" property="E02LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="liquidacion" property="E02DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Monto del Credito :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="liquidacion" property="E02LNOAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
          </tr>                 
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Primer Pago :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:date name="liquidacion" fn_year="E02LNPXPY" fn_month="E02LNPXPM" fn_day="E02LNPXPD" readonly="true" />
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tasa de Interes :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="liquidacion" property="E02LNRATE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/> 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Convenio :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="liquidacion" property="E02LNCONV" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Valor de la Cuota :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="liquidacion" property="E02LNCUAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%">  
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">             
				<select name="E02LNPVIA" <%="disabled"%>>
					<option value=""
						<% if (liquidacion.getE02LNPVIA().equals("")) out.print("selected");%>>Caja</option>
					<option value="1"
						<% if (liquidacion.getE02LNPVIA().equals("1")) out.print("selected");%>>PAC</option>							            
					<option value="2"
						<% if (liquidacion.getE02LNPVIA().equals("2")) out.print("selected");%>>Convenio</option>
					<option value="4"
						<% if (liquidacion.getE02LNPVIA().equals("4")) out.print("selected");%>>PAC/Multibanco</option>	
				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:text name="liquidacion" property="E02LNPACC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>

<% if (EPV101003List.getNoResult()) {%>
<TABLE class="tbenter" width=100% height=50%>
	<TR>
		<TD>
		<div align="center">
		<font size="3"><b> 
			No hay resultados que correspondan a su criterio de b�squeda. 
		</b></font></div>
		</TD>
	</TR>
</TABLE>
<%
	} else {
%>

  <h4>Distribucion del Prestamo </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

		<%
			EPV101003List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EPV101003List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EPV101003Message convObj = (EPV101003Message) EPV101003List.getRecord();
		%>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			<td nowrap width="25%" align="Left"></td>
			<td nowrap width="30%" align="Left"><%=convObj.getE03PLDESC()%></td>
			<td nowrap width="20%"align="right"><%=convObj.getE03PLAMOU()%></td>
			<td nowrap width="25%" align="Left"></td>
         </tr>
		<%
			}
		%>
        </table>
      </td>
    </tr>
  </table>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV101003List.getShowPrev()) {
					int pos = EPV101003List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1005?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV101003List.getShowNext()) {
					int pos = EPV101003List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1005?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<% } %>

<%} %>   

  <div align="center"> 
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
<%if(!liquidacion.getE02LNTYPG().equals("T")){%>
 <SCRIPT language="javascript">
    function tableresize() {
     adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  //tableresize();
  window.onresize=tableresize;
 </SCRIPT>
<%}%> 
</form>

</body>
</html>
