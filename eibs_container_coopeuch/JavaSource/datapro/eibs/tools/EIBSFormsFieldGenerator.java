package datapro.eibs.tools;

/**
 * Reads the Message and generates the XML containing the FIELDS for the EIBS Forms.
 * Creation date: (03/05/12 2:57 PM)
 * @author: Catalina Sepulveda
 */

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.datapro.xml.DOMUtil;

import datapro.eibs.beans.DataFormsDDS;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.LocateMe;

public class EIBSFormsFieldGenerator {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4920275636255318082L;
	private String LangPath = "S";
	private static String sClassesRoot = "";
	boolean isDataSource = false;
	private static org.w3c.dom.Document myXMLDoc = null;
	private Map params = null; 
	private Map paramsByName = null; 

/**
 * JSECLI001 constructor comment.
 */
public EIBSFormsFieldGenerator() {
	super();
}

/**
 * This method was created by Catalina Sepulveda.
 */
public void init(ServletConfig config) throws ServletException {
	isDataSource = JSEIBSProp.getEODDataSource();
	sClassesRoot = new LocateMe().getClassesRoot();
}

public String getFormatName(String ddsName){
	if (paramsByName == null){
		sClassesRoot = new LocateMe().getClassesRoot();
		readXMLParameters();
	}
	String name = (String)paramsByName.get(ddsName);
	return name;
}

public boolean isTableGroup(String ddsName){
	boolean result = false;
	if (params == null){
		sClassesRoot = new LocateMe().getClassesRoot();
		readXMLParameters();
	}
	
	for (int i = 0; i < params.size(); i++) {
		DataFormsDDS dds = (DataFormsDDS)params.get(i+"");
		if (dds.getDdsName().equals(ddsName)){
			result =  dds.isTable;
			break;
		}
	}
	return result;
}

protected Connection getConnection() throws Exception {
	Connection cnx = null;
	try {
	
		String driver = "com.ibm.as400.access.AS400JDBCDriver";
		String url = "jdbc:as400://134.177.251.90;naming=sql;prompt=false;libraries=R71CYFILES,IBSR07M01,R71BUFILES,EIBSR07M01";
		String userid = "R71JDBC";
		String password = "R71JDBC";
		
		Class.forName(driver).newInstance();
		cnx =
			DriverManager.getConnection(url, userid, password);
		return cnx;

	} catch (Exception e) {
		throw e;
	}
}

/**
 * @param args
 */
public static void main(String[] args) {
	EIBSFormsFieldGenerator a = new EIBSFormsFieldGenerator();
	sClassesRoot = new LocateMe().getClassesRoot();
	a.readXMLParameters();
	a.generateXML();
}

private void readXMLParameters(){
	String file = sClassesRoot + "eIBSFormsParams.xml";
	DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder docBuilder;
	
	try {
		docBuilder = docBuilderFactory.newDocumentBuilder();
		myXMLDoc = docBuilder.parse(new File(file));
		
	} catch (ParserConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	// normalize text representation
	myXMLDoc.getDocumentElement().normalize();
	
	NodeList rootList = myXMLDoc.getElementsByTagName("eIBSForms");
	Element root = (Element)rootList.item(0);
	
	NodeList children = root.getElementsByTagName("eIBSFormType");
	params = new HashMap();
	paramsByName = new HashMap();
	for (int i = 0; i < children.getLength(); i++) {
		org.w3c.dom.Node tableNode = children.item(i);
		org.w3c.dom.Element tableElement = (org.w3c.dom.Element) tableNode;
		DataFormsDDS ddsInfo = new DataFormsDDS();
		
		ddsInfo.setDdsName(tableElement.getAttribute("DDSClass"));
		ddsInfo.setDescription(tableElement.getAttribute("label"));
		ddsInfo.setId(tableElement.getAttribute("formName"));
		ddsInfo.setTable(tableElement.getAttribute("isTable").equals("Y"));
		
		params.put(i+"", ddsInfo);	
		paramsByName.put(tableElement.getAttribute("DDSClass"), tableElement.getAttribute("formName"));
	}
	
}
/**
 * readXMLParameters * 
 * @param result
 */
public static Map readXMLParameters(Map result){
	String file = sClassesRoot + "eIBSFormsParams.xml";
	DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder docBuilder;
	
	try {
		docBuilder = docBuilderFactory.newDocumentBuilder();
		myXMLDoc = docBuilder.parse(new File(file));
		
	} catch (ParserConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	// normalize text representation
	myXMLDoc.getDocumentElement().normalize();
	
	NodeList rootList = myXMLDoc.getElementsByTagName("eIBSForms");
	Element root = (Element)rootList.item(0);
	
	result = new HashMap();
	NodeList children = root.getElementsByTagName("eIBSFormType");
	for (int i = 0; i < children.getLength(); i++) {
		org.w3c.dom.Node tableNode = children.item(i);
		org.w3c.dom.Element tableElement = (org.w3c.dom.Element) tableNode;
		result.put(tableElement.getAttribute("DDSClass"), tableElement.getAttribute("formName"));		
	}
	
	return result;
	
}

			

private synchronized void generateXML() {

	Connection cnx = null;
	try {
		cnx = getConnection();
		Statement st = cnx.createStatement();
		
		
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
 
		// root elements xls
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("xsd:schema");
		rootElement.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
		doc.appendChild(rootElement);
		
		// root element xml
		Document xml = docBuilder.newDocument();
		Element xmlRootElement = xml.createElement("eIBSWordDataStructure");
		xml.appendChild(xmlRootElement);
		
		//xls elements
		Element fieldsType = doc.createElement("xsd:element");
		fieldsType.setAttribute("name", "eIBSPDFDataStructure");
		rootElement.appendChild(fieldsType);
		
		Element cmplTyp = doc.createElement("xsd:complexType");
		fieldsType.appendChild(cmplTyp);
		
		Element sequenceTypeRoot = doc.createElement("xsd:sequence");
		cmplTyp.appendChild(sequenceTypeRoot);

		for (int i =0; i <params.size(); i++){
			DataFormsDDS form = (DataFormsDDS)params.get(i+"");
			
			ResultSet rs = st.executeQuery("select COLUMN_HEADING, COLUMN_TEXT from QSYS2.SYSCOLUMNS where SYSTEM_TABLE_NAME = '"+form.getDdsName()+"' and SYSTEM_TABLE_SCHEMA = 'EIBSR07M01'");
			
			//xls document
			Element elementGroup = doc.createElement("xsd:element");
			elementGroup.setAttribute("name", form.getId());
			elementGroup.setAttribute("nillable", "true");
			rootElement.appendChild(elementGroup);
			
			Element dataGroup = doc.createElement("xsd:complexType");
			elementGroup.appendChild(dataGroup);
			
			Element sequenceType = doc.createElement("xsd:sequence");
			dataGroup.appendChild(sequenceType);
			
			//xml document
			Element xmlElementGroup = xml.createElement("eIBSDataGroup");
			xmlElementGroup.setAttribute("eIBSCode", form.getId());
			xmlElementGroup.setAttribute("e-Label",  form.getDescription());
			xmlRootElement.appendChild(xmlElementGroup);
			
			int index = 0;
			while (rs.next()) {
				String dataName = rs.getString("COLUMN_HEADING");
				if (dataName != null && !dataName.substring(0, 1).equals("*")){
					//xls document
					Element dataField = doc.createElement("xsd:element");
					dataField.setAttribute("name", rs.getString("COLUMN_HEADING"));
					dataField.setAttribute("type", "xsd:string");
					dataField.setAttribute("nillable", "true");
					sequenceType.appendChild(dataField);
					
					//xml document
					Element xmlDataField = xml.createElement("eIBSDataField");
					xmlDataField.setAttribute("DataType", "String" );
					xmlDataField.setAttribute("PublicName", form.getId()+"."+rs.getString("COLUMN_HEADING"));
					xmlDataField.setAttribute("eIBSCode", rs.getString("COLUMN_HEADING"));
					xmlDataField.setAttribute("s-Label", " ");
					
					String documentation = rs.getString("COLUMN_TEXT");
					if (documentation != null && !documentation.equals("")) {
						Element anotationField = doc.createElement("xsd:annotation");
						dataField.appendChild(anotationField);
						
						Element documentationField = doc.createElement("xsd:documentation");				
						anotationField.appendChild(documentationField);
						
						documentationField.appendChild(doc.createTextNode(documentation));
						xmlDataField.setAttribute("s-Label", documentation);
					}
					xmlElementGroup.appendChild(xmlDataField);
				}
				
			}
			
			if (form.isTable()){
				Element tableElement = doc.createElement("xsd:element");
				tableElement.setAttribute("name", form.getId()+"Table");
				tableElement.setAttribute("nillable", "true");
				rootElement.appendChild(tableElement);
				
				Element complex = doc.createElement("xsd:complexType");
				tableElement.appendChild(complex);
				
				Element sequence = doc.createElement("xsd:sequence");
				complex.appendChild(sequence);
				
				Element lastElem = doc.createElement("xsd:element");
				lastElem.setAttribute("maxOccurs", "unbounded");
				lastElem.setAttribute("ref", form.getId());				
				sequence.appendChild(lastElem);		
				
			}
			
			Element attrb = doc.createElement("xsd:element");
			if (form.isTable()){
				attrb.setAttribute("ref", form.getId()+"Table");
			} else {
				attrb.setAttribute("ref", form.getId());
			}
			sequenceTypeRoot.appendChild(attrb);

			/*attrb = doc.createElement("xsd:attribute");
			attrb.setAttribute("name", "Label");
			attrb.setAttribute("type", "string");
			attrb.setAttribute("default", form.getDescription());
			dataGroup.appendChild(attrb);*/
			
		}
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("eIBSFormsFields.xsd"));
		
		DOMSource xmlSource = new DOMSource(xml);
		StreamResult xmlResult = new StreamResult(new File("eIBSForms_DST.xml"));
 
		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);
 
		transformer.transform(source, result);
		transformer.transform(xmlSource, xmlResult);
 
		System.out.println("File saved!");
		
	}catch (Exception e) {
		e.printStackTrace();
	}finally {
		try{
			cnx.close();
		} catch (Exception e) {
			// TODO: handle exception
		}		
	}
}
}