<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import = "datapro.eibs.beans.EDL006501Message" %>

<html>
<head>
<title>Aprobaci&oacute;n de L&iacute;neas Estatales MYPE</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "listAppr" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
<script language="JavaScript">
function goAction(op,pos) {

 	document.forms[0].opt.value = op;

	if(op==3)//consulta
	{
		var pg = '<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0065?SCREEN=300&opt=' + op + '&CURRCODE=' + pos;
		CenterWindow(pg,1200,600,4);
	}
	else
		document.forms[0].submit();
}

</SCRIPT>  

</head>

<BODY>
<h3 align="center">Aprobaci&oacute;n de L&iacute;neas Estatales MYPE<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="lineas_estatales_approval_list, EDL0065"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0065" >
  <p> 
    <input type=HIDDEN name="SCREEN" value="300">
    <input type=HIDDEN name="opt"> 
  </p>
  

<%
	if ( listAppr.getNoResult() ) {
%>

  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
          <p>&nbsp;</p>
          
        </div>

	  </TD>
	</TR>
    </TABLE>
	
<%}else {
 
		 if ( !error.getERRNUM().equals("0")  ) {
     			error.setERRNUM("0");
    			out.println("<SCRIPT Language=\"Javascript\">");
     			out.println("       showErrors()");
     			out.println("</SCRIPT>");
    		 }
%> 
 
          
	<table class="tbenter" width=100% align=center height="8%">
	<tr> 
    	<td class=TDBKG width="50%"> 
        	<div align="center"><a href="javascript:goAction(1,0)"><b>Aprobar</b></a></div>
      	</td>
    	<td class=TDBKG width="50%"> 
        	<div align="center"><a href="javascript:goAction(2,0)"><b>Rechazar</b></a></div>        	
      	</td>
    </tr>
  	</table>
	<br>

  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
            <th align=LEFT nowrap width="5%">Tipo</th>
            <th align=LEFT nowrap width="5%">Acci&oacute;n</th>
            <th align=LEFT nowrap width="5%">L&iacute;nea</th>
			<th align=LEFT nowrap width="5%">Sub L&iacute;nea</th>
			<th align=LEFT nowrap width="5%">Comuna</th>
 			<th align=LEFT nowrap width="5%">Garantizador</th>
 			<th align=LEFT nowrap width="5%">Programa</th>
 			<th align=LEFT nowrap width="8%">Inicio</th>
 			<th align=LEFT nowrap width="8%">T&eacute;rmino Final</th>
 			<th align="center" nowrap width="8%">Moneda</th>
 			<th align=LEFT nowrap width="9%">Cupo L&iacute;nea</th>
			<th align=LEFT nowrap width="9%">Saldo</th>
			<th align=LEFT nowrap width="9%">Estado</th>
          </tr>
 
           <%
                listAppr.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (listAppr.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	
               		datapro.eibs.beans.EDL006501Message msgList = (datapro.eibs.beans.EDL006501Message) listAppr.getRecord();
					 %>
					<tr id="dataTable"> 
           				<td NOWRAP align=CENTER>
           					<input type="radio" name="CURRCODE" value="<%= listAppr.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"> 
           				</td>

						<td align="left" nowrap>
							<a href="javascript:goAction(3,<%= listAppr.getCurrentRow() %>)"><%=msgList.getE01MLNDSC() %></a>
						</td>
						<td align="left" nowrap>
							<FONT COLOR=#CC0000><%if(msgList.getE01MLNMAN().equals("I"))	out.print("CREAR"); else if (msgList.getE01MLNMAN().equals("M")) out.print("MODIFICAR");	else out.print("ELIMINAR");%>
							</FONT>
						</td>
						<td align="center" nowrap>
							<%=msgList.getE01MLNSEQ() %>
						</td>
						<td align="right" nowrap>
							<%=msgList.getE01MLNIND() %>
						</td>		
						<td align="center" nowrap>
							<%=msgList.getE01MLNCCM() %>
						</td>											
						<td align="left" nowrap>
							<%=msgList.getE01MLNDGR() %>
						</td>
						<td align="left" nowrap>
							<%=msgList.getE01MLNDPG() %>
						</td>						
						<td align="center" nowrap>
							<%= Util.formatDate(msgList.getE01MLNFVD(),msgList.getE01MLNFVM(),msgList.getE01MLNFVY())%>
						</td>
						<td align="center" nowrap>
							<%= Util.formatDate(msgList.getE01MLNFTD(),msgList.getE01MLNFTM(),msgList.getE01MLNFTY())%>
						</td>
						<td align="center" nowrap>
							<%=msgList.getE01MLNMDA() %>
						</td>						
						<td align="right" nowrap>
							<%=Util.formatCCY(msgList.getE01MLNCLI()) %>
						</td>						
						<td align="right" nowrap>
							<%=Util.formatCCY(msgList.getE01MLNSDO()) %>
						</td>						
						
						<td align="left" nowrap>
							<%=msgList.getE01MLNDST() %>
						</td>						
         			</tr>
          	<% } %>
              </table>
              </td>
              </tr>
  </table>
  
   

<SCRIPT language="JavaScript">
	showChecked("CURRCODE");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

</body>
</html>
