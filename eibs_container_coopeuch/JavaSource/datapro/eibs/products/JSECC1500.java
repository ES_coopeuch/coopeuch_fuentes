package datapro.eibs.products;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.PropertyResourceBundle;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jspsmart.upload.SmartUpload;
import com.jspsmart.upload.SmartUploadException;


import datapro.eibs.beans.ECC150101Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.ServiceLocator;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;

public class JSECC1500 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_ECC_ENTER = 100;	
	protected static final int A_ECC_ENTER = 200;
	
	protected static final int R_BANK_PROCESS_ENTER = 300;	
	protected static final int A_BANK_PROCESS_ENTER = 400;
	
	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		screen =  A_ECC_ENTER;
		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}		
		switch (screen) {
		case R_ECC_ENTER:
			procReqECCEnter(user, req, res, session);
			break;
		case A_ECC_ENTER:
			procActionECCEnter(user, req, res, session);
			break; 
		}		
	}
	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqECCEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: ECC1500_ECC_enter.jsp");
			forward("ECC1500_ECC_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 * 
	 * modify by Alonso Arana 					Datapro						September 31th
	 */
	protected void procActionECCEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		boolean ok = false;
		String idCartola = "";
		
		try {
			SmartUpload mySmartUpload = new SmartUpload();
			com.jspsmart.upload.File myFile = null;
			try {
				mySmartUpload.initialize(config, req, res);
				mySmartUpload.upload();
				myFile = mySmartUpload.getFiles().getFile(0);
				if (myFile.getSize() > 0) {
					//El archivo tiene datos buscamos el n�mero de Interfaz asignada.					
					mp = getMessageProcessor("ECC1501", req);					
					ECC150101Message msg = (ECC150101Message) mp.getMessageRecord("ECC150101");
					//seteamos las propiedades
					msg.setH01USERID(user.getH01USR());
					msg.setH01OPECOD("0002");
					msg.setH01TIMSYS(getTimeStamp());					
					msg.setH01SCRCOD("01");	
					
					//Sending message
					mp.sendMessage(msg);
		
					//Receive error and data
					msgError = (ELEERRMessage) mp.receiveMessageRecord();
					msg = (ECC150101Message) mp.receiveMessageRecord();		
					
					
					//havent errors i get the field
					if (!mp.hasError(msgError)) {
						String fileName =  "CCPINT";//nombre del archivo	
						idCartola = msg.getE01CCHIDC();
						
						byte[] bd = new byte[myFile.getSize()];
						for (int i = 0; i < myFile.getSize(); i++) {
							bd[i] = myFile.getBinaryData(i);
						}
						
						InputStream  input = new ByteArrayInputStream(bd);													
						String userid = ServiceLocator.getSlInfo().getString("ftp.cnx.userid.eibs-server");
						String password = ServiceLocator.getSlInfo().getString("ftp.cnx.password.eibs-server");												
						
						FTPWrapper ftp = new FTPStdWrapper(JSEIBSProp.getHostIP(), userid, password, "");
						
						if (ftp.open()) {
								//Mandamos todos los archivos de IMAGENES
								if (ftp.cdRemotePath(userid.substring(0, 3).toUpperCase()+"CYFILES")) {
									ftp.setFileType(FTPWrapper.ASCII);
									ftp.upload(input,fileName); 
									ok=true;//Aqui nos seguramos que todo esta Bien..
								}
						} else {
							msgError = new ELEERRMessage();
							msgError.setERRNUM("1");
							msgError.setERNU01("01");		                
							msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
						}
					}
					
				}else{
					//mandamos error en session 
					msgError = new ELEERRMessage();
					msgError.setERRNUM(new BigDecimal(1));
					msgError.setERDS01("Archivo Vacio...");							
				}
				
			}catch (Exception e) {
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();	
				msgError = new ELEERRMessage();			
				msgError.setERRNUM("3");
	            msgError.setERNU01("01");
	            msgError.setERDS01(className);
	            msgError.setERNU02("02");
	            msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
	            msgError.setERNU03("03");
	            msgError.setERDS03("Para mas informacion revizar los archivos de log.");
				e.printStackTrace();			
			} 	
			
			if (ok){									
				ECC150101Message msg = (ECC150101Message) mp.getMessageRecord("ECC150101");
				msg.setH01USERID(user.getH01USR());
				msg.setH01OPECOD("0003");
				msg.setH01TIMSYS(getTimeStamp());
				//Sets message with page fields
				
				
				msg.setE01CCHIDC(idCartola);

				//Sending message
				mp.sendMessage(msg);

				//Receive error and data
				ELEERRMessage msgError2 = (ELEERRMessage) mp.receiveMessageRecord();
				msg = (ECC150101Message) mp.receiveMessageRecord();

				//Sets session with required data
				session.setAttribute("userPO", userPO);
				session.setAttribute("bank", msg);

				if (!mp.hasError(msgError2)) {
					forward("ECC1500_ECC_Procces.jsp", req, res);
				} else {
					//if there are errors go back to maintenance page and show errors
					session.setAttribute("error", msgError2);
					forward("ECC1500_ECC_enter.jsp", req, res);
				}

			
			}else{
				session.setAttribute("error", msgError);				
				forward("ECC1500_ECC_enter.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
}
	



