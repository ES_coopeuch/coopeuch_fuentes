<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<title>Carga de Promociones</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "prom" class= "datapro.eibs.beans.EPV132001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script type="text/javascript">

function goAction(op) {
	document.forms[0].SCREEN.value = op;
	document.forms[0].submit();	
}

</SCRIPT>  


</head>

<body>
<H3 align="center">Carga de Promociones<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" 
ALT="promociones_load_procces.jsp, EPV1320"></H3>

<hr size="4">
<p>&nbsp;</p>

<br>
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1320" target="main">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="100">

<table class="tableinfo" width="100%">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">     
		    <tr>
				<div align="center">
  					<h3>
  					
  					<br>Se ha realizado la carga de la interfaz de promociones con n&uacute;mero de id : 
  					<% out.print( " " +  prom.getE01PVHIDC() + " "); %>
					<br>Para su gesti&oacute;n ingrese a opci&oacute;n Consulta de Cargas 					
  					</h3>
  				 </div>		    
		      <td > 
		      </td>
		    </tr>
		 </table>
	  </td>
	</tr>
</table>

<br>
  <p align="center">
      <input id="EIBSBTN" type="button" onclick="javascript:goAction('100');"   name=Volver value="Volver">
  </p>
  
 </form> 
</body>
</html>
