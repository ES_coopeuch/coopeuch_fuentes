<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>

<%@ page  import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Capitalizacion de Cuentas de Ahorro</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="svCapitaliza" class="datapro.eibs.beans.EDD260001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

   builtHPopUp();

  function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }	

</SCRIPT>
</head>
<BODY>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0"); 
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
 String blocked = "";
//	if (!currUser.getE01AUT().equals("B")) {
//		blocked = "readonly disabled";
//	} 

%> 
<H3 align="center">Capitalizacion de Cuentas de Detalle<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ap_capitaliza_basic_sv.jsp, EDD2600"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2600">
  <INPUT TYPE=HIDDEN NAME="SCREEN" value="500">
  
 <% int row = 0;%>
 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01ACMCUN" size="10" maxlength="9" value="<%= svCapitaliza.getE01ACMCUN().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"> 
                <input type="text"  name="E01CUSNA1" size="45" maxlength="45" readonly value="<%= svCapitaliza.getE01CUSNA1().trim()%>">
              </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
                <input type="text"  name="E01ACMACC" size="13" maxlength="12" value="<%= svCapitaliza.getE01ACMACC().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda :</b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
                <input type="text"  name="E01ACMCCY" size="3" maxlength="3" value="<%= svCapitaliza.getE01ACMCCY().trim()%>" readonly>
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
                <input type="text"  name="E01ACMPRO" size="4" maxlength="4" readonly value="<%= svCapitaliza.getE01ACMPRO().trim()%>">
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Informaci&oacute;n B&aacute;sica</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="28%" height="23"> 
              <div align="right">Fecha de Apertura :</div>
            </td>
            <td nowrap width="20%" height="23"> 
              <input type="text" readonly name="E01OPNDT1" size="3" maxlength="2" value="<%= svCapitaliza.getE01OPNDT1().trim()%>" onKeyPress="enterInteger()">
              <input type="text" readonly name="E01OPNDT2" size="3" maxlength="2" value="<%= svCapitaliza.getE01OPNDT2().trim()%>" onKeyPress="enterInteger()">
              <input type="text" readonly name="E01OPNDT3" size="5" maxlength="4" value="<%= svCapitaliza.getE01OPNDT3().trim()%>" onKeyPress="enterInteger()">
            </td>
            <td nowrap width="24%" height="23"> 
              <div align="right">Fecha de Capitalizaci&oacute;n :</div>
            </td>
            <td nowrap width="28%" height="23"> 
              <input type="text" readonly name="E01RUNDT2" size="2" maxlength="2" value="<%= svCapitaliza.getE01RUNDT2().trim()%>" onKeyPress="enterInteger()">
              <input type="text" readonly name="E01RUNDT3" size="5" maxlength="4" value="<%= svCapitaliza.getE01RUNDT3().trim()%>" onKeyPress="enterInteger()">
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Saldos</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="40%"> 
              <div align="right"> <b>Saldo Contable :</b></div>
            </td>
            <td nowrap width="60%"> 
 		        <eibsinput:text name="svCapitaliza" property="E01GRSBAL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="40%"> 
              <div align="right"> <b>Saldo Disponible :</b></div>
            </td>
            <td nowrap width="60%"> 
 		        <eibsinput:text name="svCapitaliza" property="E01NETBAL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="40%" height="19"> 
              <div align="right">Intereses a Capitalizar :</div>
            </td>
            <td nowrap width="60%" height="19"> 
				<eibsinput:text name="svCapitaliza" property="E01INTAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" onblur="CalculateAvail()" readonly="true"/>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="40%" height="19"> 
              <div align="right">Reajsutes a Capitalizar :</div>
            </td>
            <td nowrap width="60%" height="19"> 
				<eibsinput:text name="svCapitaliza" property="E01REAAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" onblur="CalculateAvail()" readonly="true"/>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="40%" height="19">
              <div align="right"><b>Saldo Final :</b></div>
            </td>
            <td nowrap width="60%" height="19">
 		        <eibsinput:text name="svCapitaliza" property="E01PAGAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
   <h4> </h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap > 
              <div align="right">Razón de Capitalización :</div>
            </td>
            <td nowrap >
 		        <eibsinput:text name="svCapitaliza" property="E01CAPRSN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE%>" readonly="true"/>  
		        <eibsinput:text name="svCapitaliza" property="D01CAPRSN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" readonly="true"/>   		                  
            </td> 
          </tr>
        </table>
      </td>
    </tr>
  </table>
 </form>
</body>
</html>
