
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Simulador Ahorro Vivienda</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="Microsoft FrontPage 4.0">

<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
</head>

<jsp:useBean id= "simula" class= "datapro.eibs.beans.EDD250001Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="JavaScript">
function goAction(op) {
	document.forms[0].E01SAVTTR.value = 0;
	document.forms[0].E01SAVDSC.value = '';
	document.forms[0].E01SAVSHU.value = 0,0000;
	document.forms[0].E01SAVAMP.value = 0;
	document.forms[0].E01SAVDIP.value = 0;	
	document.forms[0].E01SAVDIU.value = 0,0000;	
	document.forms[0].E01SAVPZO.value = 0;
	document.forms[0].E01SAVAME.value = 0;
	document.forms[0].E01SAVAMU.value = 0,0000;	
	document.forms[0].E01SAVATP.value = 0,0000;			
}
</SCRIPT>  
<% 
 if ( !error.getERRNUM().equals("0")  ) 
 {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%> 
<H3 align="center">Simulador Ahorro Vivienda<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="simulador_ahorro_vivienda.jsp, EDD2500"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2500">
  <INPUT TYPE=HIDDEN NAME="SCREEN" value="2">

  <h4>Datos de la Postulacion </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%" >
              <div align="right">Programa Habitacional :</div>
            </td>
            <td nowrap width="30%">
               <div align="left"> 
               <input type="text" name="E01SAVTTR" size="2" maxlength="2" value="<%= simula.getE01SAVTTR() %>">
               <input type="text" name="E01SAVDSC" size="30" maxlength="30" value="<%= simula.getE01SAVDSC() %>">
        		<a href="javascript:GetAhorroVivienda('E01SAVTTR','E01SAVDSC','','','','E01SAVSHU','E01SAVAMP')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absbottom" border="0" ></a>
                <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >         			 
                </div>
            </td>
           </tr>
          <tr id="trclear">  
            <td nowrap width="20%" >
              <div align="right">Monto Subsidio :</div>
            </td>
            <td nowrap width="30%" >
              <eibsinput:text property="E01SAVSHU" size="14" maxlength="13" name="simula" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT_FEE%>" readonly="true" required="true"/>UF            
            </td>
           </tr>
            <tr  id="trdark">   
            <td nowrap width="20%" >
              <div align="right">Monto Ahorro Minimo :</div>
            </td>
            <td nowrap width="30%" >
              <eibsinput:text property="E01SAVAMP" size="14" maxlength="13" name="simula" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT_FEE%>" readonly="true" required="true" />UF            
             </td>
          </tr>   
         </table>
      </td>
    </tr>
  </table>
  <h4>Datos Simulacion </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%" >
              <div align="right">Deposito Inicial :</div>
            </td>
            <td nowrap width="30%" >
              <eibsinput:text property="E01SAVDIP" size="14" maxlength="13" name="simula" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="false" required="true"/>(en pesos)            
            </td>
            <td nowrap width="30%" >
              <eibsinput:text property="E01SAVDIU" size="14" maxlength="13" name="simula" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT_FEE%>" readonly="true" />(en UF)            
            </td>            
           </tr>
          <tr id="trclear">  
            <td nowrap width="20%" >
              <div align="right">Plazo :</div>
            </td>
            <td nowrap width="30%" >
              <eibsinput:text property="E01SAVPZO" size="4" maxlength="4" name="simula" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE%>" readonly="false" required="true"/>(en meses)                
            </td>
            <td nowrap width="30%" > </td>            
           </tr>
            <tr  id="trdark">   
            <td nowrap width="20%" >
              <div align="right">Ahorro Mensual :</div>
            </td>
            <td nowrap width="30%" >
              <eibsinput:text property="E01SAVAME" size="14" maxlength="13" name="simula" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true" />(en pesos)            
                </td>
            <td nowrap width="30%" >
              <eibsinput:text property="E01SAVAMU" size="14" maxlength="13" name="simula" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT_FEE%>" readonly="true" />(en UF)             
             </td>                
          </tr>
          <tr id="trclear">  
            <td nowrap width="20%" >
              <div align="right">Ahorro Total Pactado :</div>
            </td>
            <td nowrap width="30%" >
              <eibsinput:text property="E01SAVATP" size="14" maxlength="13" name="simula" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT_FEE%>" readonly="true" />UF            
            </td>
            <td nowrap width="30%" > </td>            
           </tr>                                  
         </table>
      </td>
    </tr>
  </table>
    
  <SCRIPT Language="Javascript">

  tableresize();
  window.onresize=tableresize; 

</SCRIPT>
  <br>
          <div align="center"> 
            <input id="EIBSBTN" type=submit name="Submit" value="Calcular">
     		<input id="EIBSBTN" type=button name="Limpiar" value="Limpiar" onClick="goAction(4);">         
          </div>
  
  </form>
</body>
</html>
