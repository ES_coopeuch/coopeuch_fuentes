package datapro.eibs.client;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ESD010001Message;
import datapro.eibs.beans.ESD010002Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.Util;
import datapro.eibs.products.JOActionRedirect;
import datapro.eibs.sockets.MessageRecord;

public class JSESD0100 extends JSEIBSServlet {

	protected static final int R_PASSWORD 		= 1;
	protected static final int A_APPROVAL 		= 2;
	protected static final int R_APPROVAL_INQ 	= 3;
	protected static final int R_APPROVAL 		= 5;
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_PASSWORD:
				procReqPassword(req, res, session);
				break;
			case R_APPROVAL:
				procReqApproval(user, req, res, session);
				break;
			case A_APPROVAL:
				procActionApproval(user, req, res, session);
				break;
			case R_APPROVAL_INQ:
				procReqApprovalInq(user, req, res, session);
				break;
			default:
				forward(devPage, req, res);
				break;
		}
	}

	private void procReqApprovalInq(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException {
		
		String accNum = req.getParameter("ACCNUM") == null ? "" : req.getParameter("ACCNUM");
		
		JOActionRedirect red = new JOActionRedirect(ACC_APPROVAL_INQ, 0, accNum, user.getE01LAN(), session);
		res.sendRedirect(srctx + red.action());
	}

	private void procActionApproval(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("ESD0100", req);
			ESD010002Message msgList = (ESD010002Message) mp.getMessageRecord("ESD010002");
			msgList.setH02USERID(user.getH01USR());
			msgList.setH02PROGRM("EDD1000");
			msgList.setH02TIMSYS(getTimeStamp());
			try {
				msgList.setE02CUSCUN(req.getParameter("ACCNUM").trim());
			} catch (Exception e) {
			}
			try {
				msgList.setE02ACTION(req.getParameter("action").trim());
			} catch (Exception e) {
			}
			try {
				msgList.setE02MSGTXT(req.getParameter("reason").trim());
			} catch (Exception e) {
			}
			
			mp.sendMessage(msgList);
			MessageRecord msg = mp.receiveMessageRecord();
			
			if (mp.hasError(msg)) {
				session.setAttribute("error", msg);
				forward("ESD0100_approval_list.jsp", req, res);
			} else {
				procReqPassword(req, res, session);
			}
			
		} finally {
			if (mp != null) mp.close();
		}
	}

	private void procReqApproval(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("ESD0100", req);
			ESD010001Message msgList = (ESD010001Message) mp.getMessageRecord("ESD010001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ESD0100");
			msgList.setH01TIMSYS(getTimeStamp());
			
			mp.sendMessage(msgList);
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			
			if (mp.hasError(list)) {
				session.setAttribute("error", mp.getError(list));
				forward("error_viewer.jsp", req, res);
			} else if (list.isEmpty()) {
				forward("MISC_no_result.jsp", req, res);
			} else {	
				JBList beanList = new JBList();
				StringBuffer myRow = null;
				String chk = "";
				String chkOfac = "";
				String accNum = req.getParameter("ACCNUM");
				boolean firstTime = accNum == null;
				list.initRow();
				while (list.getNextRow()) {
					msgList = (ESD010001Message) list.getRecord();
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						if (msgList.getE01CUSCUN().trim().equals(accNum))
							chk = "checked";
						else
							chk = "";
					}
					myRow = new StringBuffer("<TR>");
					// mod EMAT 10/01/2001
					// add ofac status : H01FLGWK3 = '3'
					chkOfac = (msgList.getH01FLGWK3().equals("3") ? "<a href=\"javascript:showInqOFAC('"
							+ msgList.getE01CUSCUN()
							+ "')\"><img src=\"../images/warning_16.jpg\" alt=\"OFAC Match List\" align=\"absmiddle\" border=\"0\" ></a>"
							: "");
					// chkOfac = "<a href=\"javascript:showInqOFAC('" +
					// msgList.getE01CUSCUN() + "')\"><img
					// src=\"/eIBS_R04M03/images/warning_16.jpg\" alt=\"OFAC
					// Match List\" align=\"absmiddle\" border=\"0\" ></a>";
					//
					myRow.append("<TD NOWRAP><input type=\"radio\" name=\"ACCNUM\" value=\""
									+ msgList.getE01CUSCUN()
									+ "\" "
									+ chk
									+ " onclick=\"showAddInfo("
									+ list.getCurrentRow()
									+ ")\"></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"LEFT\"><A HREF=\"javascript:showInqApprovalClient('"
									+ msgList.getE01CUSCUN()
									+ "')\">"
									+ Util.formatCell(msgList
											.getE01CUSCUN())
									+ "</A>"
									+ chkOfac + "</TD>");
					myRow.append("<TD NOWRAP><A HREF=\"javascript:showInqApprovalClient('"
									+ msgList.getE01CUSCUN()
									+ "')\">"
									+ Util.formatCell(msgList
											.getE01CUSNA1()) + "</A></TD>");
					myRow.append("<TD NOWRAP><A HREF=\"javascript:showInqApprovalClient('"
									+ msgList.getE01CUSCUN()
									+ "')\">"
									+ Util.formatCell(msgList
											.getE01CUSIDN()) + "</A>");
					myRow.append("<INPUT TYPE=HIDDEN NAME=\"STSOFAC"
							+ list.getCurrentRow() + "\" VALUE=\""
							+ msgList.getH01FLGWK3() + "\">");
					myRow.append("<INPUT TYPE=HIDDEN NAME=\"TXTDATA"
							+ list.getCurrentRow() + "\" VALUE=\""
							+ Util.formatCell(msgList.getE01CUSRMK())
							+ "<br>");

					myRow.append(Util.formatCell(msgList.getE01CUSTID())
							+ "<br>");
					myRow.append(Util.formatCell(msgList.getE01CUSPID())
							+ "<br>");
					myRow.append(Util.formatCell(msgList.getE01CUSDIB())
							+ "<br>");
					myRow.append(Util.formatCell(msgList.getE01CUSUSR())
							+ "\"></TD>");
					myRow.append("</TR>");
					beanList.addRow("", myRow.toString());
				}
				beanList.setShowNext(list.getShowNext());
				beanList.setShowPrev(list.getShowPrev());
				
				userPO.setOption("CLIENT");
				userPO.setPurpose("APPROVAL");
				
				flexLog("Putting java beans into the session");
				session.setAttribute("userPO", userPO);
				session.setAttribute("appList", beanList);
				
				forward("ESD0100_approval_list.jsp", req, res);
			}
			
		} finally {
			if (mp != null) mp.close();
		}
	}

	private void procReqPassword(HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws IOException {
		
		UserPos userPO = getUserPos(session);
		userPO.setRedirect("/servlet/datapro.eibs.client.JSESD0100?SCREEN="
				+ R_APPROVAL
				+ (req.getParameter("ACCNUM") == null ? "" : "&ACCNUM="
						+ req.getParameter("ACCNUM")));
		session.setAttribute("userPO", userPO);
		redirect("/servlet/datapro.eibs.menu.JSESS0030?SCREEN=7", res);	
	}

}
