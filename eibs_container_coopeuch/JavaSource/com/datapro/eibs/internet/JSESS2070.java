package com.datapro.eibs.internet;

/**
 * Insert the type's description here.
 * Creation date: (8/28/2000 4:02:17 PM)
 * @author: Orestes Garcia
**/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ESS204001Message;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

import com.datapro.eibs.internet.beans.JBCards;
import com.datapro.eibs.internet.databeans.DC_CARDS;
import com.datapro.eibs.internet.generics.Util;
import com.datapro.exception.ServiceLocatorException;
import com.datapro.generics.BeanList;
//import com.datapro.generics.Util;
import com.datapro.security.SHA1;
import com.datapro.services.ServiceLocator;

public class JSESS2070 extends datapro.eibs.master.SuperServlet {

	protected static final int R_GEN_CARDS = 1;
	protected static final int A_GEN_CARDS = 11;
	protected static final int R_INQ_CARDS = 2;
	protected static final int A_INQ_CARDS = 21;
	protected String LangPath = "S";

	Random generator = new Random();
	
	public JSESS2070() {
		super();
	}

	public JSESS2070(int logType) {
		super(logType);

	}

	public void service(HttpServletRequest req, HttpServletResponse res) throws javax.servlet.ServletException, java.io.IOException {
		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			}
			catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		}
		else {

			int screen = R_INQ_CARDS;

			try {
				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					try {
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					}
					catch (Exception e) {
						flexLog("Screen set to default value");
					}

					switch (screen) {
						case R_GEN_CARDS :
							res.sendRedirect(super.srctx + LangPath + "ESS2070_cards_generate_number.jsp");
							break;
						case A_GEN_CARDS :
							procGenerate_Cards(msgUser,req,res,session);
							break;							
						case R_INQ_CARDS :
							reqInquiry_Cards(msgUser,req,res,session);
							break;
						case A_INQ_CARDS :
							actInquiry_Cards(msgUser,req,res,session);
							break;
						default :
							res.sendRedirect(super.srctx + LangPath + super.devPage);
							break;
					}

				}
				catch (Exception e) {
					e.printStackTrace();
					int sck = super.iniSocket + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
					//return;
				}
			}
			catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}

		}

	}
	//OK
	protected void procGenerate_Cards(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
		throws ServletException, IOException {

		DC_CARDS cardsdatabean = new DC_CARDS();
		DC_CARDS cardsdatabeanreport = new DC_CARDS();
		JBCards cardbean = new JBCards();
		BeanList lsCards = new BeanList();

		Connection cnx = null;
		try {
			cnx = ServiceLocator.getInstance().getDBConn("MSS");
		}
		catch (ServiceLocatorException e) {
			System.out.println("Exception e : " + e);
		}

		SHA1 secpsw = new SHA1();
		if (!secpsw.selfTest()) {
			res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
			return;
		}

		int nCards = Integer.parseInt(req.getParameter("NCARDS").toUpperCase());

		String USRKEYEncrip = "";

		for (int w = 0; w < nCards; w++) {

			cardsdatabean = new DC_CARDS();
			cardsdatabeanreport = new DC_CARDS();

			int k = 1;
			while (k < 51) {

				String USRKEY = "";
				String KEYVAL = "";

				String NumKey = String.valueOf(k);

				if (NumKey.length() < 2) {
					NumKey = "0" + NumKey;
				}

				for (int i = 0; i < 2; i++) {
					int NUM = generator.nextInt(34) + 1; // random number for letter(A-Z) and numbers(1-9)
					KEYVAL = Util.getKEYVAL(NUM); // getting the letter or number for that random number
					USRKEY = USRKEY + KEYVAL; // this is the key
				}

				secpsw.clear();
				secpsw.update(USRKEY.trim());
				secpsw.finalize();
				USRKEYEncrip = secpsw.toString().trim();

				switch (k) {
					case 1 :
						cardsdatabean.setKEY01(USRKEYEncrip);
						cardsdatabeanreport.setKEY01(USRKEY.trim());
						break;
					case 2 :
						cardsdatabean.setKEY02(USRKEYEncrip);
						cardsdatabeanreport.setKEY02(USRKEY.trim());
						break;
					case 3 :
						cardsdatabean.setKEY03(USRKEYEncrip);
						cardsdatabeanreport.setKEY03(USRKEY.trim());
						break;
					case 4 :
						cardsdatabean.setKEY04(USRKEYEncrip);
						cardsdatabeanreport.setKEY04(USRKEY.trim());
						break;
					case 5 :
						cardsdatabean.setKEY05(USRKEYEncrip);
						cardsdatabeanreport.setKEY05(USRKEY.trim());
						break;
					case 6 :
						cardsdatabean.setKEY06(USRKEYEncrip);
						cardsdatabeanreport.setKEY06(USRKEY.trim());
						break;
					case 7 :
						cardsdatabean.setKEY07(USRKEYEncrip);
						cardsdatabeanreport.setKEY07(USRKEY.trim());
						break;
					case 8 :
						cardsdatabean.setKEY08(USRKEYEncrip);
						cardsdatabeanreport.setKEY08(USRKEY.trim());
						break;
					case 9 :
						cardsdatabean.setKEY09(USRKEYEncrip);
						cardsdatabeanreport.setKEY09(USRKEY.trim());
						break;
					case 10 :
						cardsdatabean.setKEY10(USRKEYEncrip);
						cardsdatabeanreport.setKEY10(USRKEY.trim());
						break;
					case 11 :
						cardsdatabean.setKEY11(USRKEYEncrip);
						cardsdatabeanreport.setKEY11(USRKEY.trim());
						break;
					case 12 :
						cardsdatabean.setKEY12(USRKEYEncrip);
						cardsdatabeanreport.setKEY12(USRKEY.trim());
						break;
					case 13 :
						cardsdatabean.setKEY13(USRKEYEncrip);
						cardsdatabeanreport.setKEY13(USRKEY.trim());
						break;
					case 14 :
						cardsdatabean.setKEY14(USRKEYEncrip);
						cardsdatabeanreport.setKEY14(USRKEY.trim());
						break;
					case 15 :
						cardsdatabean.setKEY15(USRKEYEncrip);
						cardsdatabeanreport.setKEY15(USRKEY.trim());
						break;
					case 16 :
						cardsdatabean.setKEY16(USRKEYEncrip);
						cardsdatabeanreport.setKEY16(USRKEY.trim());
						break;
					case 17 :
						cardsdatabean.setKEY17(USRKEYEncrip);
						cardsdatabeanreport.setKEY17(USRKEY.trim());
						break;
					case 18 :
						cardsdatabean.setKEY18(USRKEYEncrip);
						cardsdatabeanreport.setKEY18(USRKEY.trim());
						break;
					case 19 :
						cardsdatabean.setKEY19(USRKEYEncrip);
						cardsdatabeanreport.setKEY19(USRKEY.trim());
						break;
					case 20 :
						cardsdatabean.setKEY20(USRKEYEncrip);
						cardsdatabeanreport.setKEY20(USRKEY.trim());
						break;
					case 21 :
						cardsdatabean.setKEY21(USRKEYEncrip);
						cardsdatabeanreport.setKEY21(USRKEY.trim());
						break;
					case 22 :
						cardsdatabean.setKEY22(USRKEYEncrip);
						cardsdatabeanreport.setKEY22(USRKEY.trim());
						break;
					case 23 :
						cardsdatabean.setKEY23(USRKEYEncrip);
						cardsdatabeanreport.setKEY23(USRKEY.trim());
						break;
					case 24 :
						cardsdatabean.setKEY24(USRKEYEncrip);
						cardsdatabeanreport.setKEY24(USRKEY.trim());
						break;
					case 25 :
						cardsdatabean.setKEY25(USRKEYEncrip);
						cardsdatabeanreport.setKEY25(USRKEY.trim());
						break;
					case 26 :
						cardsdatabean.setKEY26(USRKEYEncrip);
						cardsdatabeanreport.setKEY26(USRKEY.trim());
						break;
					case 27 :
						cardsdatabean.setKEY27(USRKEYEncrip);
						cardsdatabeanreport.setKEY27(USRKEY.trim());
						break;
					case 28 :
						cardsdatabean.setKEY28(USRKEYEncrip);
						cardsdatabeanreport.setKEY28(USRKEY.trim());
						break;
					case 29 :
						cardsdatabean.setKEY29(USRKEYEncrip);
						cardsdatabeanreport.setKEY29(USRKEY.trim());
						break;
					case 30 :
						cardsdatabean.setKEY30(USRKEYEncrip);
						cardsdatabeanreport.setKEY30(USRKEY.trim());
						break;

					case 31 :
						cardsdatabean.setKEY31(USRKEYEncrip);
						cardsdatabeanreport.setKEY31(USRKEY.trim());
						break;
					case 32 :
						cardsdatabean.setKEY32(USRKEYEncrip);
						cardsdatabeanreport.setKEY32(USRKEY.trim());
						break;
					case 33 :
						cardsdatabean.setKEY33(USRKEYEncrip);
						cardsdatabeanreport.setKEY33(USRKEY.trim());
						break;
					case 34 :
						cardsdatabean.setKEY34(USRKEYEncrip);
						cardsdatabeanreport.setKEY34(USRKEY.trim());
						break;
					case 35 :
						cardsdatabean.setKEY35(USRKEYEncrip);
						cardsdatabeanreport.setKEY35(USRKEY.trim());
						break;
					case 36 :
						cardsdatabean.setKEY36(USRKEYEncrip);
						cardsdatabeanreport.setKEY36(USRKEY.trim());
						break;
					case 37 :
						cardsdatabean.setKEY37(USRKEYEncrip);
						cardsdatabeanreport.setKEY37(USRKEY.trim());
						break;
					case 38 :
						cardsdatabean.setKEY38(USRKEYEncrip);
						cardsdatabeanreport.setKEY38(USRKEY.trim());
						break;
					case 39 :
						cardsdatabean.setKEY39(USRKEYEncrip);
						cardsdatabeanreport.setKEY39(USRKEY.trim());
						break;
					case 40 :
						cardsdatabean.setKEY40(USRKEYEncrip);
						cardsdatabeanreport.setKEY40(USRKEY.trim());
						break;
					case 41 :
						cardsdatabean.setKEY41(USRKEYEncrip);
						cardsdatabeanreport.setKEY41(USRKEY.trim());
						break;
					case 42 :
						cardsdatabean.setKEY42(USRKEYEncrip);
						cardsdatabeanreport.setKEY42(USRKEY.trim());
						break;
					case 43 :
						cardsdatabean.setKEY43(USRKEYEncrip);
						cardsdatabeanreport.setKEY43(USRKEY.trim());
						break;
					case 44 :
						cardsdatabean.setKEY44(USRKEYEncrip);
						cardsdatabeanreport.setKEY44(USRKEY.trim());
						break;
					case 45 :
						cardsdatabean.setKEY45(USRKEYEncrip);
						cardsdatabeanreport.setKEY45(USRKEY.trim());
						break;
					case 46 :
						cardsdatabean.setKEY46(USRKEYEncrip);
						cardsdatabeanreport.setKEY46(USRKEY.trim());
						break;
					case 47 :
						cardsdatabean.setKEY47(USRKEYEncrip);
						cardsdatabeanreport.setKEY47(USRKEY.trim());
						break;
					case 48 :
						cardsdatabean.setKEY48(USRKEYEncrip);
						cardsdatabeanreport.setKEY48(USRKEY.trim());
						break;
					case 49 :
						cardsdatabean.setKEY49(USRKEYEncrip);
						cardsdatabeanreport.setKEY49(USRKEY.trim());
						break;
					case 50 :
						cardsdatabean.setKEY50(USRKEYEncrip);
						cardsdatabeanreport.setKEY50(USRKEY.trim());
						break;
				} // end switch

				k++;

			} //end while

			cardsdatabean.setSTS("P");
			String newcardnum = cardbean.getLastCard();
			if(newcardnum.trim().equals("")){
				newcardnum = "10000";
			}
			String Serial = String.valueOf(Integer.parseInt(newcardnum) + 1);
			cardsdatabean.setSERIAL(Serial);
			cardbean.setAddCard(cardsdatabean, cnx);
			cardsdatabeanreport.setSERIAL(Serial);
			lsCards.addRow(cardsdatabeanreport);
		} //end for loop

		try {
			cnx.close();
		}
		catch (SQLException e) {
			System.out.println("Exception e : " + e);
		}

		String PageToCall = "ESS2070_cards_generate_report.jsp";
		ses.setAttribute("lsCards", lsCards);
		res.sendRedirect(super.srctx + LangPath + PageToCall);
		return;
	}
	//OK

	protected void reqInquiry_Cards(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
	throws ServletException, IOException {
		com.datapro.generic.beanutil.BeanList lsInqEntity = new com.datapro.generic.beanutil.BeanList();	
		JBCards cardbean = new JBCards();	
		lsInqEntity = cardbean.getListEntity();
		ses.setAttribute("lsInqEntity", lsInqEntity);
		res.sendRedirect(super.srctx + LangPath + "ESS2070_cards_inq_entity_list.jsp");
	}
	
	protected void actInquiry_Cards(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
	throws ServletException, IOException {
		com.datapro.generic.beanutil.BeanList lsCardsInq = new com.datapro.generic.beanutil.BeanList();	
		JBCards cardbean = new JBCards();
		
		String ENTITYID = req.getParameter("ENTITYID").toUpperCase();
		lsCardsInq = cardbean.getListCardsforEntity(ENTITYID);		
		ses.setAttribute("lsCardsInq", lsCardsInq);
		res.sendRedirect(super.srctx + LangPath + "ESS2070_cards_inq_entity_asigned.jsp");
	}
	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01() + " code : " + m.getERDF01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02() + " code : " + m.getERDF02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03() + " code : " + m.getERDF03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04() + " code : " + m.getERDF04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05() + " code : " + m.getERDF05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06() + " code : " + m.getERDF06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07() + " code : " + m.getERDF07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08() + " code : " + m.getERDF08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09() + " code : " + m.getERDF09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10() + " code : " + m.getERDF10());

		}
	}
}