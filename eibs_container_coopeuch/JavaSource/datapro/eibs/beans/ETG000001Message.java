package datapro.eibs.beans;

import datapro.eibs.sockets.*;
import java.io.*;
import java.math.*;

import java.util.*;

/**
* Class generated by AS/400 CRTCLASS command from ETG000001 physical file definition.
* 
* File level identifier is 1160525133431.
* Record format level identifier is 29824CC0C1145.
*/

public class ETG000001Message extends MessageRecord
{
  final static String fldnames[] = {
                                     "H01USERID",
                                     "H01PROGRM",
                                     "H01TIMSYS",
                                     "H01SCRCOD",
                                     "H01OPECOD",
                                     "H01FLGMAS",
                                     "H01FLGWK1",
                                     "H01FLGWK2",
                                     "H01FLGWK3",
                                     "ETG01TAB",
                                     "ETG01ELE",
                                     "ETG01DESCL",
                                     "ETG01VAL01",
                                     "ETG01VAL02",
                                     "ETG01ALFA1",
                                     "ETG01ALFA2",
                                     "ETG01VAL03",
                                     "ETG01NREC",
                                     "ETG01NPAG"
                                   };
  final static String tnames[] = {
                                   "H01USERID",
                                   "H01PROGRM",
                                   "H01TIMSYS",
                                   "H01SCRCOD",
                                   "H01OPECOD",
                                   "H01FLGMAS",
                                   "H01FLGWK1",
                                   "H01FLGWK2",
                                   "H01FLGWK3",
                                   "ETG01TAB",
                                   "ETG01ELE",
                                   "ETG01DESCL",
                                   "ETG01VAL01",
                                   "ETG01VAL02",
                                   "ETG01ALFA1",
                                   "ETG01ALFA2",
                                   "ETG01VAL03",
                                   "ETG01NREC",
                                   "ETG01NPAG"
                                 };
  final static String fid = "1160525133431";
  final static String rid = "29824CC0C1145";
  final static String fmtname = "ETG000001";
  final int FIELDCOUNT = 19;
  private static Hashtable tlookup = new Hashtable();
  private CharacterField fieldH01USERID = null;
  private CharacterField fieldH01PROGRM = null;
  private CharacterField fieldH01TIMSYS = null;
  private CharacterField fieldH01SCRCOD = null;
  private CharacterField fieldH01OPECOD = null;
  private CharacterField fieldH01FLGMAS = null;
  private CharacterField fieldH01FLGWK1 = null;
  private CharacterField fieldH01FLGWK2 = null;
  private CharacterField fieldH01FLGWK3 = null;
  private DecimalField fieldETG01TAB = null;
  private CharacterField fieldETG01ELE = null;
  private CharacterField fieldETG01DESCL = null;
  private DecimalField fieldETG01VAL01 = null;
  private DecimalField fieldETG01VAL02 = null;
  private CharacterField fieldETG01ALFA1 = null;
  private CharacterField fieldETG01ALFA2 = null;
  private DecimalField fieldETG01VAL03 = null;
  private DecimalField fieldETG01NREC = null;
  private DecimalField fieldETG01NPAG = null;

  /**
  * Constructor for ETG000001Message.
  */
  public ETG000001Message()
  {
    createFields();
    initialize();
  }

  /**
  * Create fields for this message.
  * This method implements the abstract method in the MessageRecord superclass.
  */
  protected void createFields()
  {
    recordsize = 155;
    fileid = fid;
    recordid = rid;
    message = new byte[getByteLength()];
    formatname = fmtname;
    fieldnames = fldnames;
    tagnames = tnames;
    fields = new MessageField[FIELDCOUNT];

    fields[0] = fieldH01USERID =
    new CharacterField(message, HEADERSIZE + 0, 10, "H01USERID");
    fields[1] = fieldH01PROGRM =
    new CharacterField(message, HEADERSIZE + 10, 10, "H01PROGRM");
    fields[2] = fieldH01TIMSYS =
    new CharacterField(message, HEADERSIZE + 20, 12, "H01TIMSYS");
    fields[3] = fieldH01SCRCOD =
    new CharacterField(message, HEADERSIZE + 32, 2, "H01SCRCOD");
    fields[4] = fieldH01OPECOD =
    new CharacterField(message, HEADERSIZE + 34, 4, "H01OPECOD");
    fields[5] = fieldH01FLGMAS =
    new CharacterField(message, HEADERSIZE + 38, 1, "H01FLGMAS");
    fields[6] = fieldH01FLGWK1 =
    new CharacterField(message, HEADERSIZE + 39, 1, "H01FLGWK1");
    fields[7] = fieldH01FLGWK2 =
    new CharacterField(message, HEADERSIZE + 40, 1, "H01FLGWK2");
    fields[8] = fieldH01FLGWK3 =
    new CharacterField(message, HEADERSIZE + 41, 1, "H01FLGWK3");
    fields[9] = fieldETG01TAB =
    new DecimalField(message, HEADERSIZE + 42, 4, 0, "ETG01TAB");
    fields[10] = fieldETG01ELE =
    new CharacterField(message, HEADERSIZE + 46, 8, "ETG01ELE");
    fields[11] = fieldETG01DESCL =
    new CharacterField(message, HEADERSIZE + 54, 40, "ETG01DESCL");
    fields[12] = fieldETG01VAL01 =
    new DecimalField(message, HEADERSIZE + 94, 11, 2, "ETG01VAL01");
    fields[13] = fieldETG01VAL02 =
    new DecimalField(message, HEADERSIZE + 105, 9, 2, "ETG01VAL02");
    fields[14] = fieldETG01ALFA1 =
    new CharacterField(message, HEADERSIZE + 114, 5, "ETG01ALFA1");
    fields[15] = fieldETG01ALFA2 =
    new CharacterField(message, HEADERSIZE + 119, 15, "ETG01ALFA2");
    fields[16] = fieldETG01VAL03 =
    new DecimalField(message, HEADERSIZE + 134, 5, 0, "ETG01VAL03");
    fields[17] = fieldETG01NREC =
    new DecimalField(message, HEADERSIZE + 139, 8, 0, "ETG01NREC");
    fields[18] = fieldETG01NPAG =
    new DecimalField(message, HEADERSIZE + 147, 8, 0, "ETG01NPAG");

    synchronized (tlookup)
    {
      if (tlookup.isEmpty())
      {
        for (int i = 0; i < tnames.length; i++)
          tlookup.put(tnames[i], new Integer(i));
      }
    }

    taglookup = tlookup;
  }

  /**
  * Set field H01USERID using a String value.
  */
  public void setH01USERID(String newvalue)
  {
    fieldH01USERID.setString(newvalue);
  }

  /**
  * Get value of field H01USERID as a String.
  */
  public String getH01USERID()
  {
    return fieldH01USERID.getString();
  }

  /**
  * Set field H01PROGRM using a String value.
  */
  public void setH01PROGRM(String newvalue)
  {
    fieldH01PROGRM.setString(newvalue);
  }

  /**
  * Get value of field H01PROGRM as a String.
  */
  public String getH01PROGRM()
  {
    return fieldH01PROGRM.getString();
  }

  /**
  * Set field H01TIMSYS using a String value.
  */
  public void setH01TIMSYS(String newvalue)
  {
    fieldH01TIMSYS.setString(newvalue);
  }

  /**
  * Get value of field H01TIMSYS as a String.
  */
  public String getH01TIMSYS()
  {
    return fieldH01TIMSYS.getString();
  }

  /**
  * Set field H01SCRCOD using a String value.
  */
  public void setH01SCRCOD(String newvalue)
  {
    fieldH01SCRCOD.setString(newvalue);
  }

  /**
  * Get value of field H01SCRCOD as a String.
  */
  public String getH01SCRCOD()
  {
    return fieldH01SCRCOD.getString();
  }

  /**
  * Set field H01OPECOD using a String value.
  */
  public void setH01OPECOD(String newvalue)
  {
    fieldH01OPECOD.setString(newvalue);
  }

  /**
  * Get value of field H01OPECOD as a String.
  */
  public String getH01OPECOD()
  {
    return fieldH01OPECOD.getString();
  }

  /**
  * Set field H01FLGMAS using a String value.
  */
  public void setH01FLGMAS(String newvalue)
  {
    fieldH01FLGMAS.setString(newvalue);
  }

  /**
  * Get value of field H01FLGMAS as a String.
  */
  public String getH01FLGMAS()
  {
    return fieldH01FLGMAS.getString();
  }

  /**
  * Set field H01FLGWK1 using a String value.
  */
  public void setH01FLGWK1(String newvalue)
  {
    fieldH01FLGWK1.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK1 as a String.
  */
  public String getH01FLGWK1()
  {
    return fieldH01FLGWK1.getString();
  }

  /**
  * Set field H01FLGWK2 using a String value.
  */
  public void setH01FLGWK2(String newvalue)
  {
    fieldH01FLGWK2.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK2 as a String.
  */
  public String getH01FLGWK2()
  {
    return fieldH01FLGWK2.getString();
  }

  /**
  * Set field H01FLGWK3 using a String value.
  */
  public void setH01FLGWK3(String newvalue)
  {
    fieldH01FLGWK3.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK3 as a String.
  */
  public String getH01FLGWK3()
  {
    return fieldH01FLGWK3.getString();
  }

  /**
  * Set field ETG01TAB using a String value.
  */
  public void setETG01TAB(String newvalue)
  {
    fieldETG01TAB.setString(newvalue);
  }

  /**
  * Get value of field ETG01TAB as a String.
  */
  public String getETG01TAB()
  {
    return fieldETG01TAB.getString();
  }

  /**
  * Set numeric field ETG01TAB using a BigDecimal value.
  */
  public void setETG01TAB(BigDecimal newvalue)
  {
    fieldETG01TAB.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG01TAB as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG01TAB()
  {
    return fieldETG01TAB.getBigDecimal();
  }

  /**
  * Set field ETG01ELE using a String value.
  */
  public void setETG01ELE(String newvalue)
  {
    fieldETG01ELE.setString(newvalue);
  }

  /**
  * Get value of field ETG01ELE as a String.
  */
  public String getETG01ELE()
  {
    return fieldETG01ELE.getString();
  }

  /**
  * Set field ETG01DESCL using a String value.
  */
  public void setETG01DESCL(String newvalue)
  {
    fieldETG01DESCL.setString(newvalue);
  }

  /**
  * Get value of field ETG01DESCL as a String.
  */
  public String getETG01DESCL()
  {
    return fieldETG01DESCL.getString();
  }

  /**
  * Set field ETG01VAL01 using a String value.
  */
  public void setETG01VAL01(String newvalue)
  {
    fieldETG01VAL01.setString(newvalue);
  }

  /**
  * Get value of field ETG01VAL01 as a String.
  */
  public String getETG01VAL01()
  {
    return fieldETG01VAL01.getString();
  }

  /**
  * Set numeric field ETG01VAL01 using a BigDecimal value.
  */
  public void setETG01VAL01(BigDecimal newvalue)
  {
    fieldETG01VAL01.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG01VAL01 as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG01VAL01()
  {
    return fieldETG01VAL01.getBigDecimal();
  }

  /**
  * Set field ETG01VAL02 using a String value.
  */
  public void setETG01VAL02(String newvalue)
  {
    fieldETG01VAL02.setString(newvalue);
  }

  /**
  * Get value of field ETG01VAL02 as a String.
  */
  public String getETG01VAL02()
  {
    return fieldETG01VAL02.getString();
  }

  /**
  * Set numeric field ETG01VAL02 using a BigDecimal value.
  */
  public void setETG01VAL02(BigDecimal newvalue)
  {
    fieldETG01VAL02.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG01VAL02 as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG01VAL02()
  {
    return fieldETG01VAL02.getBigDecimal();
  }

  /**
  * Set field ETG01ALFA1 using a String value.
  */
  public void setETG01ALFA1(String newvalue)
  {
    fieldETG01ALFA1.setString(newvalue);
  }

  /**
  * Get value of field ETG01ALFA1 as a String.
  */
  public String getETG01ALFA1()
  {
    return fieldETG01ALFA1.getString();
  }

  /**
  * Set field ETG01ALFA2 using a String value.
  */
  public void setETG01ALFA2(String newvalue)
  {
    fieldETG01ALFA2.setString(newvalue);
  }

  /**
  * Get value of field ETG01ALFA2 as a String.
  */
  public String getETG01ALFA2()
  {
    return fieldETG01ALFA2.getString();
  }

  /**
  * Set field ETG01VAL03 using a String value.
  */
  public void setETG01VAL03(String newvalue)
  {
    fieldETG01VAL03.setString(newvalue);
  }

  /**
  * Get value of field ETG01VAL03 as a String.
  */
  public String getETG01VAL03()
  {
    return fieldETG01VAL03.getString();
  }

  /**
  * Set numeric field ETG01VAL03 using a BigDecimal value.
  */
  public void setETG01VAL03(BigDecimal newvalue)
  {
    fieldETG01VAL03.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG01VAL03 as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG01VAL03()
  {
    return fieldETG01VAL03.getBigDecimal();
  }

  /**
  * Set field ETG01NREC using a String value.
  */
  public void setETG01NREC(String newvalue)
  {
    fieldETG01NREC.setString(newvalue);
  }

  /**
  * Get value of field ETG01NREC as a String.
  */
  public String getETG01NREC()
  {
    return fieldETG01NREC.getString();
  }

  /**
  * Set numeric field ETG01NREC using a BigDecimal value.
  */
  public void setETG01NREC(BigDecimal newvalue)
  {
    fieldETG01NREC.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG01NREC as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG01NREC()
  {
    return fieldETG01NREC.getBigDecimal();
  }

  /**
  * Set field ETG01NPAG using a String value.
  */
  public void setETG01NPAG(String newvalue)
  {
    fieldETG01NPAG.setString(newvalue);
  }

  /**
  * Get value of field ETG01NPAG as a String.
  */
  public String getETG01NPAG()
  {
    return fieldETG01NPAG.getString();
  }

  /**
  * Set numeric field ETG01NPAG using a BigDecimal value.
  */
  public void setETG01NPAG(BigDecimal newvalue)
  {
    fieldETG01NPAG.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG01NPAG as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG01NPAG()
  {
    return fieldETG01NPAG.getBigDecimal();
  }

}
