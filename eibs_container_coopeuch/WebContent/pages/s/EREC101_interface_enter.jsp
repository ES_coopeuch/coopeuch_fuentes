<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Validaci&oacute;n Interfaces Locales Motor de Pago</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "Inter" class= "datapro.eibs.beans.EREC10101Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">
 function enterCode(){
	
	if (trim(document.forms[0].E01REOIDE.value).length > 0) 
	{
 		if (trim(document.forms[0].file.value).length > 0) 
 		{
 			document.forms[0].submit();
 		}
 		else
 		{
 			alert("Debe Seleccionar Archivo");
 			document.forms[0].file.focus();
 			return false;
 		}
	}
	else
	{
		alert("Debe ingresar Id de Interfaz");
		document.forms[0].E01REOIDE.focus();
		return false;
	}
 }
</script>

</head>

<body>
 
<H3 align="center">Validaci&oacute;n Interfaz Motor de Pago <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="interface_enter.jsp, { EREC101"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.motorpago.JSEREC101" enctype="multipart/form-data" >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">


  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0"> 
    <tr id="trdark">
      <td width="50%"> 
        <div align="right">Ingrese Id de Interfaz :</div>
      </td>
      <td width="50%"> 
        <div align="left"> 
              <input type="text" name="E01REOIDE" size="5" maxlength="4" readonly value="">
              <input type="text" name="E01REODES" size="51" maxlength="50" readonly value="">
			  <a href="javascript:GetCodeDescCNOFC('E01REOIDE','E01REODES','CX')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a> 
	          <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
        </div>
      </td>
    </tr>

  	<tr id="trdark"> 
        <td align=CENTER width="50%"> 
          <div align="right">Seleccione Archivo :</div>
        </td>
        <td align=CENTER width="50%"> 
          <div align="left"> 
 	         <input type="file" name="file" size="50" >
          </div>
        </td>
      </tr>
        
        
  </table>
  <p align="center">
      <input id="EIBSBTN" type=button name="Submit" value="Enviar" onclick="enterCode()">
  </p>
<script language="JavaScript">
  document.forms[0].E01REOIDE.focus();
  document.forms[0].E01REOIDE.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
