<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV107001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV107001List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id="total" class="datapro.eibs.beans.EPV107002Message"  scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">

	function goInquiry(row) {
        page = webapp + "/servlet/datapro.eibs.salesplatform.JSEPV1070?SCREEN=203&ROW="+row;
        CenterWindow(page,850,700,2);      
	}
</script>

</head>

<body>
<% 


 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<% int row = 0;%>

<h3 align="center">Reporte de Ventas<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_list.jsp,JSEPV1070"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath() + userPO.getRedirect()%>">
<input type="hidden" name="SCREEN" value=""> 

<h4>Seleccion </h4>

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="10%"> Banco/Oficina :</td>
             <td nowrap align="left" width="12%">
	  			<eibsinput:text name="total" property="E02SELBNK" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BANK %>" readonly="true" />
 	 			<eibsinput:text name="total" property="E02SELBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true" />
             </td>
             <td nowrap align="right" width="10%"> Fecha Inicial :</td>
             <td nowrap align="left" width="12%">
    	        <eibsinput:date name="total" fn_year="E02SELFIY" fn_month="E02SELFIM" fn_day="E02SELFID" readonly="true"/>
             </td>
             <td nowrap align="right" width="10%"> Fecha Final :</td>
             <td nowrap align="left" width="12%">
    	        <eibsinput:date name="total" fn_year="E02SELFFY" fn_month="E02SELFFM" fn_day="E02SELFFD" readonly="true"/>
             </td>
             <td nowrap align="right" width="10%"> Fecha Reporte :</td>
             <td nowrap align="left" width="12%">
	          <input type="text" name="E02TIMERP" size="30" value="<%= total.getE02TIMERP().trim()%>" readonly>
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="10%"> Supervisor :</td>
             <td nowrap align="left" width="12%">
	            <eibsinput:cnofc name="total" property="E02SELSUP" required="false" flag="CA" fn_code="E02SELSUP" readonly="true"/>
             </td>
             <td nowrap align="right" width="10%"> Vendedor :</td>
             <td nowrap align="left" width="12%">
	            <eibsinput:cnofc name="total" property="E02SELVEN" required="false" flag="CA" fn_code="E02SELVEN" readonly="true"/>
             </td>
             <td nowrap align="right" width="10%"> Convenio :</td>
             <td nowrap align="left" width="12%">
	          <input type="text" name="E02SELCNV" size="5" maxlength="4" value="<%= total.getE02SELCNV().trim()%>" readonly>
             </td>
             <td nowrap align="right" width="10%"> </td>
             <td nowrap align="left" width="12%">
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>

<br>

<% if ( userPO.getHeader8().equals("Y")  ) { %> 
<% if (EPV107001List.getNoResult()) { %>
<TABLE class="tbenter" width=100% height=50%>
	<TR>
		<TD>
		<div align="center">
		<font size="3"><b> 
			No hay resultados que correspondan a su criterio de b�squeda. 
		</b></font></div>
		</TD>
	</TR>
</TABLE>

<% } else { %>

	<table id="headTable" width="100%">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			<th align="center" nowrap width="10%">Solicitud</th>
			<th align="center" nowrap width="10%">Cliente</th>
			<th align="center" nowrap width="20%">Nombre</th>
			<th align="center" nowrap width="5%">Producto</th>
			<th align="center" nowrap width="5%">Oficina</th>
			<th align="center" nowrap width="5%">Fecha</th>			
			<th align="center" nowrap width="5%">Tasa</th>
			<th align="center" nowrap width="5%">Monto</th>
			<th align="center" nowrap width="5%">Cuotas</th>
			<th align="center" nowrap width="5%">Convenio</th>
			<th align="center" nowrap width="20%">Vendedor</th>
		</tr>
		<%
			EPV107001List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EPV107001List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EPV107001Message convObj = (EPV107001Message) EPV107001List.getRecord();
		%>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			<td nowrap align="center"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=Util.formatCell(convObj.getE01PVVNUM())%></a></td>
			<td nowrap align="center"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=convObj.getE01PVVCUN()%></a></td>
			<td nowrap align="left"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=convObj.getE01PVVNME()%></a></td>
			<td nowrap align="center"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=convObj.getE01PVVPRO()%> </a></td>
			<td nowrap align="center"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=convObj.getE01PVVBRN()%> </a></td>
			<td nowrap align="center"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=Util.formatCell(convObj.getE01PVVODD() + "/" + convObj.getE01PVVODM() + "/"+ convObj.getE01PVVODY())%> </a></td>
			<td nowrap align="right"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=convObj.getE01LNSRTE()%></a></td>
			<td nowrap align="right"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=convObj.getE01PVVAPA()%></a></td>
			<td nowrap align="right"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=convObj.getE01PVVPNU()%></a></td>
			<td nowrap align="right"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=convObj.getE01PVVCNV()%></a></td>
			<td nowrap align="left"><a href="javascript:goInquiry('<%= EPV107001List.getCurrentRow() %>');"><%=Util.formatCell(convObj.getE01PVVVCD() + "-" + convObj.getE01NMEVCD())%> </a></td>
		</tr>

		<% } %>
	</table>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV107001List.getShowPrev()) {
					int pos = EPV107001List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1070?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV107001List.getShowNext()) {
					int pos = EPV107001List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1070?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<% } %>
<% } %>

<% if ( userPO.getHeader9().equals("Y")  ) { %> 
  <h4>Totales </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Total de Operaciones :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02TOTREC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="8"  readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Monto Capital :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02TOTAPA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Monto Liquido :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02TOTNET" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Reliquidaciones :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02TOTPLR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Seguros Financiados :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02TOTSOM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Tienda Virtual :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02TOTTVI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Tarjeta Alianza :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02TOTTRA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Otros :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02TOTOTH" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Tasa ponderada :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02RTEPON" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Spread Ponderada :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02SPRPON" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%" > </td>
            <td nowrap width="25%" >
            	<div align="right">Costo de Fondos ponderada :</div>
            </td>
            <td nowrap width="25%" >
	            <eibsinput:text name="total" property="E02CSTPON" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
            <td nowrap width="25%" > </td>
          </tr>                 

        </table>
      </td>
    </tr>
  </table>
<% } %>

</form>

</body>
</html>
