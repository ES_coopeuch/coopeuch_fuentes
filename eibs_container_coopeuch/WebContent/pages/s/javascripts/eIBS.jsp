<%@ page language="java" %>
<%-- set document type to Javascript (addresses a bug in Netscape according to a web resource --%>

<%@ page contentType="application/x-javascript" %>

<%@ page import = "datapro.eibs.sockets.DecimalField" %>

<%-- Global Variables --%>

	var errorTitle = "Errores";
	var msgRightClick = "Todos los derechos reservados";
	var fieldName;
	var fieldDesc;
	var fieldId;
	var fieldAux1;
	var fieldAux2;
	var option;

	var language = "s/";
	var webapp = "<%=request.getContextPath()%>";
	var context = "<%=request.getContextPath()%>";
	var prefix = context + "/pages/";
	var imgPath = context + "/images/";
	var javascriptsPath = prefix + language + "javascripts/";


	var amountformat = true;
	var decSeparator = '<%= DecimalField.getDecimalSeparator() %>';
	var grpSeparator = '<%= DecimalField.getGroupingSeparator()%>';
	var decimalPartDefaultLength = 2;
	
	var applet = null;
	var model;


	function IncludeJavaScript(jsFile) {
		document.write('<script type="text/javascript" src="' + '<%=request.getContextPath()%>/pages/s/javascripts/' + jsFile + '"></script>');
	}

	IncludeJavaScript('eIBS.js');
	IncludeJavaScript('help.js');

