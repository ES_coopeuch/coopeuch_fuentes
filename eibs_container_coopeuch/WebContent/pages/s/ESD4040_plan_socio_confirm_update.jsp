<html>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">

<head>
<title>Confirmación de actualizacion del Plan Socio </title>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD404001Message"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "pagOperation" class= "java.lang.String"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT LANGUAGE="javascript">
 function finish(){
 	//document.forms[0].submit();
 	self.window.location.href = "<%=request.getContextPath()%>/pages/background.jsp";
  }
  
 setTimeout("finish();", 3000);
 

 
</SCRIPT>

</head>

<body>

 <h3 align="center">Confirmación  <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="plan_socio_confirm, ESD4040" ></h3>
 <hr size="4">
 <FORM METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4040?SCREEN=100" >
 
  <table width="100%" height="100%" border="1" bordercolor="#000000">
    <tr > 
      <td> 
        <table width="100%" height="100%">
          <tr> 
            <td align=center>
		       	El socio <b><%= userPO.getHeader3()%></b> ha modificado satisfactoriamente un plan socio,
		       	y se le  ha asignado el numero <b><%=session.getAttribute("IPplanSocio") %></b> dentro del sistema.

            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
 </FORM>

</body>
</html>
