
<%

  datapro.eibs.sockets.MessageRecord mr;
 
  String READ="";
  String color1="trdark";
  String color2="trclear";
 
  // PARAMETROS
  String suffix, suffixDsc, suffixMod ;
  String messageName;
  String title="";
  boolean readOnly=false ;  
  boolean basicData=false ; 
  String paisCode; 
  
  
  // Esta variable se usa para saber si se consultan los datos basicos y asi construir 
  // dinamicamente los nombres de los campos, ya que el template es el mismo para 
  // las direcciones de clientes personales, corporativos , beneficiarios y representantes legales
  // asi como para la consulta de aprobaci�n.
  // Ver mas abajo donde se arman los nombres de los campos.
  if( request.getParameter("basic") != null ){
		basicData = Boolean.valueOf(request.getParameter("basic").toString()).booleanValue() ;
  }
   
   //Obtiene el titulo del segmento de direccion
   if (request.getParameter("title") != null)
     title = request.getParameter("title");
   
   // Determina si es solo lectura
   if (request.getParameter("readOnly") != null ){
     readOnly=Boolean.valueOf( request.getParameter("readOnly").toString() ).
     					booleanValue() ;
     READ = readOnly?"readonly":"";
   }
   // Obtiene el mensaje a desplegar
   messageName = request.getParameter("messageName");
   mr= (datapro.eibs.sockets.MessageRecord) session.getAttribute(messageName);      
   
   if (request.getParameter("suffix") != null){
   		suffix = request.getParameter("suffix") ;
   }else{
   		suffix = "E01" ;
   }
   suffixDsc = "D" + suffix.substring(1) ;
   suffixMod = "F" + suffix.substring(1) ;

   //Obtiene el color de la primera fila
   if(request.getParameter("firstColor")!= null){
      color1=request.getParameter("firstColor");
      if (color1.equals("trclear")) color2="trdark";
   }

  // NOMBRES DE CAMPOS
	String linea2 = suffix + (basicData?"NA2":"MA2") ;
	String linea3 = suffix + (basicData?"NA3":"MA3") ;
	String linea4 = suffix + (basicData?"NA4":"MA4") ;	
	String ciudad = suffix + (basicData?"STE":"STE") ;
	String ciudadDsc = suffixDsc + (basicData?"STE":"STE") ;
	String departamento = suffix + (basicData?"MLC":"MLC") ;
	String departamentoDsc = suffixDsc + (basicData?"MLC":"MLC") ;
	String pais = suffix + "CTR" ;
	String apartadoPostal = suffix + "POB" ;
	String codigoCorreo = suffix + "ZPC" ;
	String eMail = suffix + "IAD" ;

	// Flag Modificacion para consulta de aprobacion  
	String linea2F = suffixMod + (basicData?"NA2":"MA2") ;
	String linea3F = suffixMod + (basicData?"NA3":"MA4") ;
	String linea4F = suffixMod + (basicData?"NA3":"MA4") ;
	String ciudadF = suffixMod + (basicData?"STE":"STE") ;
	String departamentoF = suffixMod + (basicData?"MLC":"MLC") ;
	String paisF = suffixMod + "CTR" ;
	String apartadoPostalF = suffixMod + "POB" ;
	String codigoCorreoF = suffixMod + "ZPC" ;
	String eMailF = suffixMod + "IAD" ;

   // Obtiene pais de la direccion
   	if (pais.equals("COLOMBIA")){
   		paisCode = "CO";
	} else {
		paisCode = "";
   	}
  
%>
<SCRIPT Language="Javascript">

 function chgPais(p) {
 	if (p.toUpperCase() == 'COLOMBIA') {
		document.getElementById('IDCOL').style.display='block' ;
		document.getElementById('IDOTHER').style.display='none' ;
		chgData(1);
	} else {
		document.getElementById('IDCOL').style.display='none' ;
		document.getElementById('IDOTHER').style.display='block' ;
		chgData(2);
	}
 }
 
 function chgData(type) {
 	switch (type){
	// Direccion de Colombia
	case 1:  {
		document.getElementById('CCTR').value = 'COLOMBIA';
		document.getElementById('CCTRCOD').value = 'CO';
		document.getElementById('CDIR').value = document.getElementById('CNA2').value + "\n" +
												document.getElementById('CNA3').value + "\n" +
												document.getElementById('CNA4').value + "\n" +
												document.getElementById('CCTY').value + ', ' +
												document.getElementById('CDMLC').value + ", " +
												document.getElementById('CCTR').value; 
		break;
		}	
	// Direccion Generica para otro pais	
	case 2:  {
		document.getElementById('OCTR').value = document.getElementById('CCTR').value;
		document.getElementById('ODIR').value = document.getElementById('ONA2').value + "\n" +
												document.getElementById('ONA3').value + "\n" +
												document.getElementById('ONA4').value + "\n" +
												document.getElementById('OCTY').value + ', ' +
												document.getElementById('ODMLC').value + ", " +
												document.getElementById('OCTR').value; 
		break;
		}
	}		
 }	
 
 function PasNomenclatura() {
	document.getElementById('CNA2').value = document.getElementById('CLSVIA').value + " " +
											document.getElementById('NROV').value + " " +
											document.getElementById('LETRAV').value;
	chgData(1);											  	
 }
</SCRIPT>
    
<%-- Inicio del bloque de direccion Colombia --%> 
<div id="IDCOL" style="position:relative; display:none;">
<% if( basicData){ %>
	<h4><%=title%></h4>
   
   <table class="tableinfo" width="100%" >
    	<tr > 
        	<td nowrap >
        		<table cellspacing="0" cellpadding="2" width="100%" border="0">
<% } %>
          			<tr id="<%=color1%>"> 
            			<td nowrap align="right">Pa�s : </td>
            			<td nowrap > 
            				<input type="hidden" name="CCTRCOD" value="">
              				<input type="text" name="CCTR" size="21" maxlength="20" value="<%= mr.getField(pais).getString()%>" readonly
              					<%=(basicData && mr.getField(paisF).getString().equals("Y"))?"id=\"txtchanged\"":"" %> 
              					onChange="chgPais(this.value)" onBlur="chgPais(this.value)">
              				<% if ( !readOnly ) {%>
								<a href="javascript:GetCodeDescCNOFC('CCTRCOD','CCTR','03')">
								<img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0">
								</a>
								<img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0">	
              				<% } %>
            			</td>
            			<td nowrap>
            			</td>
           			</tr>
          			<tr id="<%=color2%>"> 
            			<td nowrap align="right">Departamento : </td>
            			<td nowrap > 
              				<input type="hidden" name="CMLC"  size="3" maxlength="2" value="<%= mr.getField(departamento).getString()%>" <%=READ %> >
              				<input type="text" name="CDMLC"  size="37" maxlength="35" value="<%= mr.getField(departamentoDsc).getString()%>" readonly 
              				<%=(basicData && mr.getField(departamentoF).getString().equals("Y"))?"id=\"txtchanged\"":"" %>
              				onChange="chgData(1)" onBlur="chgData(1)">
              				<% if ( !readOnly ) { %>
              					<A href="javascript:GetCodeDescCNOFC('CMLC','CDMLC','27')" >
									<img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0" style="cursor:hand" >
	              				</a>
	              				<img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0">	
              				<% } %>
            			</td>
            			<td nowrap>
            			</td>
           			</tr>
           			<tr id="<%=color1%>">
            			<td nowrap align="right">Ciudad : </td>
            			<td nowrap > 
              				<input type="hidden" name="CCTYCOD" size="6" maxlength="5" value="<%= mr.getField(ciudad).getString()%>">
              				<input type="text" name="CCTY" size="36" maxlength="35" value="<%= mr.getField(ciudadDsc).getString()%>" readonly
              				<% if(basicData){ out.print((mr.getField(ciudadF).getString().equals("Y"))?"id=\"txtchanged\"":"");} %>
              				onChange="chgData(1)" onBlur="chgData(1)">
              				<% if ( !readOnly ) { %>
              					<a href="javascript:Get2FilterCodes('CCTYCOD','CCTY','84', ' ',document.forms[0]['CMLC'].value)" >
              					<img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0" style="cursor:hand" >
	          					</a>
	          				<img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0"  >	
              				<% } %>
            			</td>
            			<td nowrap>
            			</td>
          			</tr>
					<tr id="<%=color2%>">
            			<td nowrap align="right">Nomenclatura : </td>
            			<td nowrap> 
              				<input type="text" name="CNA2"  size="46" maxlength="45" value="<%=mr.getField(linea2).getString()%>" <%=READ%> 
              				<% if(basicData){ out.print((mr.getField(linea2F).getString().equals("Y"))?"id=\"txtchanged\"":""); } %>
              				onChange="chgData(1)" onBlur="chgData(1)">
              				<% if( !readOnly){ %>
              					<img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0">
              				<% } %>
            			</td>
            			<td nowrap>
            				<% if( !readOnly){ %>
            					<a href="javascript:PasNomenclatura()">
              						<img src="<%=request.getContextPath()%>/images/bistro007ic.gif" alt="pasar nomenclatura" align="bottom" border="0">
              					</a>
              					<fieldset><legend>Via Principal / Numero / Letra / Prefijo / Cuadrante :</legend>
              						<SELECT class="inputs" name="CLSVIA" size="1"></SELECT>
              						<INPUT type="text" name="NROV" maxlength="15" size="16">
              						<div style="border:solid 1px #000000">
										<input type="text" name="LETRAV" value="" size="2" /><br />
										<select name="LETRAVS" size="1" 
											onChange="document.getElementById('LETRAV').value = document.getElementById('LETRAVS').options[document.getElementById('LETRAVS').selectedIndex].value;document.getElementById('LETRAVS').value=''">
											<option value=" " selected="selected">
										</select>
									</div> 
              						<SELECT class="inputs" name="BIS" size="1"></SELECT>
              					</fieldset>
              					<BR>
              					<fieldset><legend>Numero Placa / Letra /Sufijo / Letra / Numero / Cuadrante :</legend>
              						<INPUT type="text" name="NROG1" maxlength="3" size="4" onkeypress="enterInteger()">
              						<SELECT class="inputs" name="LETRAG" size="1"></SELECT>
              						<SELECT class="inputs" name="BISG" size="1"></SELECT>
              						<INPUT type="text" name="NROG2" maxlength="3" size="4" onkeypress="enterInteger()">
              					</fieldset>
            				<% } %>
            			</td>
           			</tr>
           			<tr id="<%=color1%>">
            			<td nowrap align="right">Complemento : </td>
            			<td nowrap> 
              				<input type="text" name="CNA3"  size="46" maxlength="45" value="<%=mr.getField(linea3).getString()%>" <%=READ%> 
              				<% if(basicData){ out.print((mr.getField(linea3F).getString().equals("Y"))?"id=\"txtchanged\"":""); } %>
              				onChange="chgData(1)" onBlur="chgData(1)"><br>
            			</td>
            			<td nowrap>
            			</td>
           			</tr>
          			<tr id="<%=color2%>">
            			<td nowrap align="right"> </td>
            			<td nowrap> 
              				<input type="text" name="CNA4"  size="46" maxlength="45" value="<%=mr.getField(linea4).getString()%>" <%=READ%> 
              				<% if(basicData){ out.print((mr.getField(linea4F).getString().equals("Y"))?"id=\"txtchanged\"":""); } %>
              				onChange="chgData(1)" onBlur="chgData(1)">
            			</td>
            			<td nowrap>
            			</td>
           			</tr>
           			<tr id="<%=color1%>">
            			<td nowrap align="right">Apartado Postal : </td>
            			<td nowrap > 
              				<input type="text" name="CPOB"  size="11" maxlength="10" value="<%= mr.getField(apartadoPostal).getString()%>" <%=READ %> 
              				<%=(basicData && mr.getField(apartadoPostalF).getString().equals("Y"))?"id=\"txtchanged\"":"" %> >
            			</td>
            			<td nowrap>
            			</td>
          			</tr>
           			<tr id="<%=color2%>">
            			<td nowrap align="right">C�digo de Correo : </td>
            			<td nowrap > 
              				<input type="text" name="CZPC"  size="16" maxlength="15" value="<%= mr.getField(codigoCorreo).getString()%>" <%=READ %> 
              				<%= (basicData && mr.getField(codigoCorreoF).getString().equals("Y"))?"id=\"txtchanged\"":"" %> >
            			</td>
            			<td nowrap>
            			</td>
          			</tr>
          			<tr id="<%=color1%>">
       					<td nowrap align="right"> </td>
       					<td nowrap > 
       						<textarea name="CDIR" cols="50" rows="6" readonly>
        					</textarea>
       					</td>
       					<td nowrap>
            			</td>
   					</tr>
<% if ( basicData ) { //Este campo solo aplica para los datos basicos  %>
          			<tr id="<%=color2%>"> 
            			<td nowrap align="right">Direcci�n E-Mail : </td>
            			<td nowrap > 
              				<input type="text" name="CIAD" size="61" maxlength="60" value="<%= mr.getField(eMail).getString()%>" <%=READ %> 
              				<%=(basicData && mr.getField(eMailF).getString().equals("Y"))?"id=\"txtchanged\"":"" %> >
            			</td>
            			<td nowrap>
            			</td>
          			</tr>
			</table>
		</td>
	</tr>
</table>
<% } %>
</div>    

<%-- Inicio del bloque de direccion diferente de Colombia --%> 
<div id="IDOTHER" style="position:relative; display:none;">
<% if( basicData){ %>
	<h4><%=title%></h4>
   
   <table class="tableinfo" width="100%" >
    	<tr > 
        	<td nowrap >
        		<table cellspacing="0" cellpadding="2" width="100%" border="0">
<% } %>
			<tr id="<%=color1%>"> 
              <td nowrap align="right">Pa�s : </td>
              <td nowrap>  
                <input type="hidden" name="OCTRCOD" value="">
                <input type="text" name="OCTR" size="21" maxlength="20" value="<%= mr.getField(pais).getString()%>" <%=READ %> 
                 <%=(basicData && mr.getField(suffixMod +  "CTR").getString().equals("Y"))?"id=\"txtchanged\"":"" %> 
                 onChange="chgPais(this.value)" onBlur="chgPais(this.value)">
              <% if( !readOnly ){ %>
              	<a href="javascript:GetCodeDescCNOFC('OCTRCOD','OCTR','03')">
				<img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              	<img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom"> 
              <% } %>
              </td>
            </tr>
            <tr id="<%=color2%>"> 
              <td nowrap align="right">Ciudad : </td>
              <td nowrap>  
              	<input type="text" name="OCTY" size="36" maxlength="35" value="<%= mr.getField(ciudadDsc).getString()%>" <%=READ %>
              				<% if(basicData){ out.print((mr.getField(ciudadF).getString().equals("Y"))?"id=\"txtchanged\"":"");} %>
              				onChange="chgData(2)" onBlur="chgData(2)">
                <% if( !readOnly ){ %>
                <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" >
                <% } %>
              </td>
            </tr>
            <tr id="<%=color1%>"> 
              <td nowrap align="right">Estado/Provincia/Municipio/...  : </td>
              <td nowrap>  
					<input type="hidden" name="OMLC"  size="3" maxlength="2" value="<%= mr.getField(departamento).getString()%>">
              		<input type="text" name="ODMLC"  size="37" maxlength="35" value="<%= mr.getField(departamentoDsc).getString()%>"
              		<% if(basicData){ out.print((mr.getField(ciudadF).getString().equals("Y"))?"id=\"txtchanged\"":"");} %>
              		onChange="chgData(2)" onBlur="chgData(2)">
              		<% if ( !readOnly ) { %>
	                <a href="javascript:GetCodeDescCNOFC('OMLC','ODMLC','27')">
	                <img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a> 
    	            <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" >
    	      </td>
    	        <% } %>
            </tr>
            <tr id="<%=color2%>"> 
              <td nowrap align="right">Direcci�n Principal : </td>
              <td nowrap>  
   				<input type="text" name="ONA2"  size="46" maxlength="45" value="<%=mr.getField(linea2).getString()%>" <%=READ%> 
       				<% if(basicData){ out.print((mr.getField(linea2F).getString().equals("Y"))?"id=\"txtchanged\"":""); } %>
       				onChange="chgData(2)" onBlur="chgData(2)">
                <% if( !readOnly){ %>
                	<img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" > 
                <% } %>
			  </td>
            </tr>
            <tr id="<%=color1%>"> 
              <td nowrap align="right"></td>
              <td nowrap>  
   				<input type="text" name="ONA3"  size="46" maxlength="45" value="<%=mr.getField(linea3).getString()%>" <%=READ%> 
       				<% if(basicData){ out.print((mr.getField(linea3F).getString().equals("Y"))?"id=\"txtchanged\"":""); } %> 
       				onChange="chgData(2)" onBlur="chgData(2)">
			  </td>
            </tr>
            <tr id="<%=color2%>"> 
              <td nowrap align="right"></td>
              <td nowrap>  
   				<input type="text" name="ONA4"  size="46" maxlength="45" value="<%=mr.getField(linea4).getString()%>" <%=READ%> 
       				<% if(basicData){ out.print((mr.getField(linea4F).getString().equals("Y"))?"id=\"txtchanged\"":""); } %>
       				onChange="chgData(2)" onBlur="chgData(2)">
			  </td>
            </tr>
            <tr id="<%=color1%>"> 
              <td nowrap align="right">Apartado Postal : </td>
              <td nowrap>  
                <input type="text" name="OPOB" size="11" maxlength="10" value="<%= mr.getField(suffix +  "POB").getString().trim()%>" <%=READ %> >
              </td>
            </tr>
   			<tr id="<%=color2%>">
       			<td nowrap align="right">C�digo de Correo : </td>
       			<td nowrap > 
       				<input type="text" name="OZPC"  size="16" maxlength="15" value="<%= mr.getField(codigoCorreo).getString()%>" <%=READ %> 
           				<%= (basicData && mr.getField(codigoCorreoF).getString().equals("Y"))?"id=\"txtchanged\"":"" %> >
       			</td>
   			</tr>
   			<tr id="<%=color1%>">
       			<td nowrap align="right"> </td>
       			<td nowrap > 
       				<textarea name="ODIR" cols="50" rows="6" readonly>
        			</textarea>
       			</td>
   			</tr>
<% if ( basicData ) { //Este campo solo aplica para los datos basicos  %>
   			<tr id="<%=color2%>"> 
      			<td nowrap align="right">Direcci�n E-Mail : </td>
       			<td nowrap > 
       				<input type="text" name="OIAD" size="61" maxlength="60" value="<%= mr.getField(eMail).getString()%>" <%=READ %> 
           				<%=(basicData && mr.getField(eMailF).getString().equals("Y"))?"id=\"txtchanged\"":"" %> >
       			</td>
   			</tr>
		</table>
	</td>
  </tr>
</table>
<% } %>
</div>

<SCRIPT Language="Javascript">

<% 	if (mr.getField(suffix +  "CTR").getString().trim().contains("COLOMBIA")) {  
		paisCode = "CO"; %>
		document.getElementById('IDCOL').style.display='block' ;
		document.getElementById('IDOTHER').style.display='none' ;
<% } else { %>
		document.getElementById('IDCOL').style.display='none' ;
		document.getElementById('IDOTHER').style.display='block' ;
<% } %>	    
// Clases de vias
  var tobox = document.forms[0].CLSVIA;
  tobox.options.length = 0;
  tobox.options[tobox.options.length] = new Option(' ',' ',true,'selected');
  tobox.options[tobox.options.length] = new Option('Autopista','AU');
  tobox.options[tobox.options.length] = new Option('Avenida','AV');
  tobox.options[tobox.options.length] = new Option('Avenida Calle','AC');
  tobox.options[tobox.options.length] = new Option('Avenida Carrera','AK');
  tobox.options[tobox.options.length] = new Option('Bulevar','BL');
  tobox.options[tobox.options.length] = new Option('Calle','CL');
  tobox.options[tobox.options.length] = new Option('Carrera','KR');
  tobox.options[tobox.options.length] = new Option('Carretera','CT');
  tobox.options[tobox.options.length] = new Option('Circular','CQ');
  tobox.options[tobox.options.length] = new Option('Circunvalar','CV');
  tobox.options[tobox.options.length] = new Option('Cuentas Corridas','CC');
  tobox.options[tobox.options.length] = new Option('Diagonal','DG');
  tobox.options[tobox.options.length] = new Option('Pasaje','PJ');
  tobox.options[tobox.options.length] = new Option('Paseo','PS');
  tobox.options[tobox.options.length] = new Option('Peatonal','PT');
  tobox.options[tobox.options.length] = new Option('Transversal','TV');
  tobox.options[tobox.options.length] = new Option('Troncal','TC');
  tobox.options[tobox.options.length] = new Option('Variante','VT');
  tobox.options[tobox.options.length] = new Option('Via','VI');
  tobox = document.forms[0].LETRAVS;
  tobox.options.length = 1;
  tobox.options[tobox.options.length] = new Option('A','A');
  tobox.options[tobox.options.length] = new Option('B','B');
  tobox.options[tobox.options.length] = new Option('C','C');
  tobox.options[tobox.options.length] = new Option('D','D');
  tobox.options[tobox.options.length] = new Option('E','E');
  tobox.options[tobox.options.length] = new Option('F','F');
  tobox.options[tobox.options.length] = new Option('G','G');
  tobox.options[tobox.options.length] = new Option('H','H');
  tobox.options[tobox.options.length] = new Option('I','I');
  tobox.options[tobox.options.length] = new Option('J','J');
  tobox.options[tobox.options.length] = new Option('K','K');
  tobox.options[tobox.options.length] = new Option('L','L');
  tobox.options[tobox.options.length] = new Option('M','M');
  tobox.options[tobox.options.length] = new Option('N','N');
  tobox.options[tobox.options.length] = new Option('O','O');
  tobox.options[tobox.options.length] = new Option('P','P');
  tobox.options[tobox.options.length] = new Option('Q','Q');
  tobox.options[tobox.options.length] = new Option('R','R');
  tobox.options[tobox.options.length] = new Option('S','S');
  tobox.options[tobox.options.length] = new Option('T','T');
  tobox.options[tobox.options.length] = new Option('V','V');
  tobox.options[tobox.options.length] = new Option('W','W');
  tobox.options[tobox.options.length] = new Option('X','X');
  tobox.options[tobox.options.length] = new Option('Y','Y');
  tobox.options[tobox.options.length] = new Option('Z','Z');
  tobox = document.forms[0].BIS;
  tobox.options.length = 0;
  tobox.options[tobox.options.length] = new Option(' ',' ',true,'selected');
  tobox.options[tobox.options.length] = new Option('BIS','BIS');
</SCRIPT>