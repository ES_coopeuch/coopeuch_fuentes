<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@ page import="datapro.eibs.master.Util"%>
<%@page import="com.datapro.constants.Entities"%>

<html>
<head>
<title>Credit Cards Maintenance</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id="ccNew" class="datapro.eibs.beans.ECC004001Message"  scope="session" />
<jsp:useBean id="ccPvta" class="datapro.eibs.beans.ECC002001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<SCRIPT LANGUAGE="javascript">
 builtHPopUp();  
  function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }        
</SCRIPT>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0"); 
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
  
 if (!userPO.getPurpose().equals("NEW")){
   out.println("<SCRIPT> initMenu();  </SCRIPT>");}

%> 

</head>
<body>
<h3 align="center" >Plataforma PosVenta<BR> Ingreso Solicitudes Tarjetas de Credito <br>
<%if (userPO.getOption().equals("1")) out.print("A C T I V A C I O N");%>
<%if (userPO.getOption().equals("2")) out.print("A D I C I O N A L E S");%>
<%if (userPO.getOption().equals("3")) out.print("Cambio Cupo");%>
<%if (userPO.getOption().equals("4")) out.print("Bloqueo/Desbloqueo");%>
<%if (userPO.getOption().equals("5")) out.print("Reemision Tarjeta Titular");%>
<%if (userPO.getOption().equals("6")) out.print("Cambio de Codigo FV");%>
<%if (userPO.getOption().equals("7")) out.print("Pago Minimo/Pac Multibanco");%>
<%if (userPO.getOption().equals("8")) out.print("Reseteo Clave");%>
<%if (userPO.getOption().equals("9")) out.print("Cambio Direcci�n envio Estado Cuenta");%>
<%if (userPO.getOption().equals("10")) out.print("Eliminaci�n Tarjeta");%>
<%if (userPO.getOption().equals("11")) out.print("Renuncia Tarjeta");%>  
<%if (userPO.getOption().equals("12")) out.print("Activacion de Renovacion de Tarjeta");%>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    																	    

<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cc_maint_posventa.jsp,ECC0020"> 
</h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0020" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="500">
  <INPUT TYPE=HIDDEN NAME="E01CCRBNK" VALUE=<%= ccNew.getE01CCMBNK().trim() %>>  
 <%  String mando = "SI"; %>
   <TABLE class="tbenter" width=100%>
   	<TR>
      <TD> 
        <div align="center">
   	      <table class="tableinfo">
            <tr > 
              <td nowrap> 
                <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
                  <tr id="trclear"> 
                    <td nowrap width="16%"> 
                      <div align="right"><b>Identificaci&oacute;n :</b></div>
                    </td>
                    <td nowrap width="20%"> 
                      <div align="left"> 
                        <input type="text" name="E01CCRCID" size="15" maxlength="15" value="<%= userPO.getIdentifier() %>" readonly>
                      </div>
                    </td>
                    <td nowrap width="16%" > 
                      <div align="right"><b>Cliente :</b></div>
                    </td>
                    <td nowrap width="20%" > 
                      <div align="left"><b> 
                        <input type="text" readonly name="E01CCRCUN" size="10" maxlength="9" value="<%= userPO.getCusNum().trim()%>" >
                        </b> </div>
                    </td>
                    <td nowrap width="16%" > 
                      <div align="right"><b>Nombre :</b> </div>
                    </td>
                    <td nowrap > 
                      <div align="left"><font face="Arial"><font face="Arial"><font size="2"> 
                        <input type="text" name="E01CCRNAM" size="35" maxlength="35" readonly value="<%= userPO.getCusName().trim()%>">
                        </font></font></font></div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>      
        </div>
      </td>
    </tr>
 </table>
 
  <h4>Informaci&oacute;n de la Cuenta</h4> 
 <% int row = 0;%>  
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">N�mero Cuenta : </div>
            </td>
            <td nowrap width="23%"> 
               <eibsinput:text name="ccNew" property="E01CCMNXN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CARD_NUMBER %>" readonly="true"/>              
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Apertura : </div>
            </td>
            <td nowrap width="23%"> 
              <eibsinput:date name="ccNew" fn_year="E01CCMOPY" fn_month="E01CCMOPM" fn_day="E01CCMOPD" readonly="true"/>
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Estado de la Cuenta : </div>
            </td>
            <td nowrap width="23%">             
              <select name="E01CCMAST" disabled>
                <option value=" " <% if (!(
		                	ccNew.getE01CCMAST().equals("0") || 
                			ccNew.getE01CCMAST().equals("1"))) 
                			out.print("selected"); %> selected></option>
                <option value="0" <% if(ccNew.getE01CCMAST().equals("0")) out.print("selected");%>>Inactiva</option>
                <option value="1" <% if(ccNew.getE01CCMAST().equals("1")) out.print("selected");%>>Activa</option>                			
              </select>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Activaci�n : </div>
            </td>
            <td nowrap width="23%"> 
              <eibsinput:date name="ccNew" fn_year="E01CCMACY" fn_month="E01CCMACM" fn_day="E01CCMACI" readonly="true"/>
            </td>
          </tr>           
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Bloqueo 1 Cuenta :</div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="D01CCMCBK" size="35" maxlength="35" readonly value="<%= ccNew.getD01CCMCBK().trim()%>">                             
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha 1 Bloqueo : </div>
            </td>
            <td nowrap width="27%"> 
              <eibsinput:date name="ccNew" fn_year="E01CCMB1Y" fn_month="E01CCMB1M" fn_day="E01CCMB1D" readonly="true"/>   			
 			</td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Bloqueo 2 Cuenta :</div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="D01CCMCB2" size="35" maxlength="35" readonly value="<%= ccNew.getD01CCMCB2().trim()%>">                  
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha 2 Bloqueo : </div>
            </td>
 	      <td nowrap width="27%"> 
             <eibsinput:date name="ccNew" fn_year="E01CCMB2Y" fn_month="E01CCMB2M" fn_day="E01CCMB2D" readonly="true"/>  	
 			</td>    
	      </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Forma de Pago : </div>
            </td>
             <td nowrap width="27%"> 
               <input type="text" name="D01CCMFPA" size="35" maxlength="35" readonly value="<%= ccNew.getD01CCMFPA().trim()%>">                  
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cuenta Corriente : </div>
            </td>
            <td nowrap width="27%"> 
             <input type="text" name="E01CCMRPA" maxlength="13" size="13" value="<%= ccNew.getE01CCMRPA().trim() %>" readonly="readonly" class="TXTRIGHT">		     		        			
 			</td>
          </tr>           
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Codigo FV :</div>
            </td>
            <td nowrap width="23%"> 
		        <eibsinput:text name="ccNew" property="E01CCMCFA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />            
            </td>
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="27%">             
 			</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
         
<h4>Informaci&oacute;n de la Tarjeta</h4>       
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">      
          <tr id="trdark"> 
            <td nowrap width="17%"> 
              <div align="right">Tarjeta : </div>
            </td>
            <td nowrap width="41%">
              <div align="left" >
                <input type="text" name="E01CCRNUM" size="20" maxlength="20" value="<%= userPO.getHeader22().trim() %>" readonly >
                <input type="text" name="D01CCMPRO" size="35" maxlength="35" value="<%= userPO.getHeader20().trim() %>" readonly >
              </div>              
            </td>                
            <td nowrap width="20%"> 
              <div align="right">Tipo : </div>
            </td>
            <td nowrap width="22%">
              <div align="left" >                
                <input type="text" name="E01CCRTPI" size="10" maxlength="10" value="<% if (userPO.getHeader23().trim().equals("T")) out.print("TITULAR"); else out.print("ADICIONAL"); %>" readonly > 
              </div>              
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="17%"> 
              <div align="right">Rut Titular : </div>
            </td>
            <td nowrap width="41%"> 
              <div align="left"> 
                <input type="text" readonly name="E01CCRCID" size="12" maxlength="12" value="<%= userPO.getIdentifier()%>">
                <input type="text" name="E01CCRNAM" size="35" maxlength="35" value="<%= userPO.getCusName()%>" readonly>
               </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Fecha Activaci&oacute;n : </div>
            </td>
            <td nowrap width="22%"> 
              <input type="text" name="E01CCRATD" size="3" maxlength="2" value="<%= ccNew.getE01CCRATD().trim()%>" readonly>
              <input type="text" name="E01CCRATM" size="3" maxlength="2" value="<%= ccNew.getE01CCRATM().trim()%>" readonly>
              <input type="text" name="E01CCRATY" size="5" maxlength="4" value="<%= ccNew.getE01CCRATY().trim()%>" readonly>              
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="17%"> 
              <div align="right">Estado de la Tarjeta : </div>
            </td>
            <td nowrap width="41%"> 
              <input type="text" name="E01CCRSTS" size="25" maxlength="25" value="<% if(ccNew.getE01CCRSTS().equals("01")) out.print("Activa"); else out.print("Inactiva");%>" readonly>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Fecha de Apertura : </div>
            </td>
            <td nowrap width="22%"> 
              <input type="text" name="E01CCMOPD" size="3" maxlength="2" value="<%= ccNew.getE01CCMOPD().trim()%>" readonly>
              <input type="text" name="E01CCMOPM" size="3" maxlength="2" value="<%= ccNew.getE01CCMOPM().trim()%>" readonly>
              <input type="text" name="E01CCMOPY" size="5" maxlength="4" value="<%= ccNew.getE01CCMOPY().trim()%>" readonly>
            </td>
            </tr>  
           <tr id="trclear"> 
            <td nowrap width="17%"> 
              <div align="right">Bloqueo : </div>
            </td>
            <td nowrap width="41%"> 
              <input type="text" name="D01CCRLKC" size="25" maxlength="25" value="<%= ccNew.getD01CCRLKC()%>" readonly>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cupo compras Nacional : </div>
            </td>
            <td nowrap width="22%">            
              <eibsinput:text property="E01CCRCDN" size="17" maxlength="16" name="ccNew" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly ="true"/>
              </td>
            </tr> 
          <tr id="trdark"> 
            <td nowrap width="17%"> 
              <div align="right">Codigo FV : </div>
            </td>
            <td nowrap width="41%"> 
              <input type="text" name="E01CCMCFA" size="3" maxlength="2" value="<%= ccNew.getE01CCMCFA()%>" readonly>
            </td>
            <td nowrap width="20%">  
              <div align="right">Cupo compras Internacional : </div>
            </td>
            <td nowrap width="22%"> 
              <eibsinput:text property="E01CCRCDR" size="17" maxlength="16" name="ccNew" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly ="true"/> 				 
            </td>
          </tr>                          
          <tr id="trclear"> 
            <td nowrap width="17%"> 
              <div align="right">Pago M&iacute;nimo : </div>
            </td>
            <td nowrap width="41%"> 
              <input type="text" name="D01CCMFPA" size="35" maxlength="35" value="<%= ccNew.getD01CCMFPA().trim()%>" readonly> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Direcc. Estado Cuenta : </div>
            </td>
            <td nowrap width="22%"> 
              <input type="text" name="E01CCMMLA" size="2" maxlength="2" value="<%= ccNew.getE01CCMMLA().trim()%>" readonly>                
 			</td>
          </tr>                     
        </table>
      </td>
    </tr>
  </table>
<br>
<%-----------------------------------------------------------------------------------------Activacion de la Tarjeta ---------%>
<% if (userPO.getOption().equals("1")) { %>
 <h4>Activaci&oacute;n de la Tarjeta</h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>  
<%  if (ccNew.getE01CCRSTS().equals("00") && (ccNew.getE01CCRLKC().equals(""))) { %>                
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Nuevo Estado de Tarjeta :</div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01SPVAST">
                <option value="1" <% if(ccNew.getE01CCRSTS().equals("00")) out.print("selected");%>>Activar</option>                			
              </select>
				<img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="absbottom" border="0" > 
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr> 
<% } else { mando  = "NO"; %>    
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="23%"> 
              <div align="Center"><b> Tarjeta ya esta Activa / Bloqueada </b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr> 
 <%} %>                  
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                            
        </table>
      </td>
    </tr>
  </table> 
<%}%> 
<%-----------------------------------------------------------------------------------------Ingreso Adicionales ---------%>  
<% if (userPO.getOption().equals("2")) { %>
 <h4>Ingreso de Adicionales</h4> 
 <%  if (ccNew.getE01CCRTPI().equals("T") && 
         ccNew.getE01CCMAST().equals("1") && 
         ccNew.getE01CCMCBK().equals("") && 
         ccNew.getE01CCMCB2().equals("")) { %>         
   <table class="tableinfo">  
   <tr>
   <td >     
    <table cellspacing="0" cellpadding="2" width="100%" border="0" >
           <tr id="trclear"> 
            <td> 
              <div align="left"><h4>Adicional 1</h4> </div>
            </td>        
          </tr>                           
           <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Rut Adicional : </div>
            </td>
            <td nowrap width="32%"> 
             <input type="text" name="E01SPVRA1" size="25" maxlength="25" value="<%= ccPvta.getE01SPVRA1() %>" >
            </td>
             <td nowrap width="25%"> </td>  
             <td nowrap width="20%"> </td>                        
          </tr>        
          <tr id="trclear">
            <td nowrap width="22%"> 
              <div align="right">Apellido Paterno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPA1" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPA1()%>" >
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
          <tr id="trdark">
            <td nowrap width="22%"> 
              <div align="right">Apellido Materno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVSA1" size="13" maxlength="13" value="<%=ccPvta.getE01SPVSA1()%>" >
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>Cupos Diferenciados</b> </div>
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Nombres : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPN1" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPN1()%>" >
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Nacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVCN1" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                
            </td>            
          </tr> 
         <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Fecha de Nacimiento : </div>
            </td>
             <td nowrap width="32%">
                <eibsinput:date name="ccPvta" fn_year="E01SPVBY1" fn_month="E01SPVBM1" fn_day="E01SPVBD1" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Internacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVIN1" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                
            </td>            
          </tr> 
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Sexo : </div>
            </td>
             <td nowrap width="32%">
              <input type="radio" name="E01SPVSX1" value="F" <% if (ccPvta.getE01SPVSX1().equals("F")) out.print("checked"); %>>
              Femenino 
              <input type="radio" name="E01SPVSX1" value="M" <% if (ccPvta.getE01SPVSX1().equals("M")) out.print("checked"); %>>
              Masculino </td> 
            <td nowrap width="25%"> 
              <div align="right">Parentesco : </div>
            </td>
             <td nowrap width="25%">
              <select name="E01SPVRE1">
                <option value=" " <% if (!(ccPvta.getE01SPVRE1().equals("1") ||
                                           ccPvta.getE01SPVRE1().equals("2") ||
                                           ccPvta.getE01SPVRE1().equals("3") ||
                                           ccPvta.getE01SPVRE1().equals("4") ||
                                           ccPvta.getE01SPVRE1().equals("5") ||
                                           ccPvta.getE01SPVRE1().equals("6") ||
                                           ccPvta.getE01SPVRE1().equals("7") ||
                                           ccPvta.getE01SPVRE1().equals("8") ||
                                           ccPvta.getE01SPVRE1().equals("9") ||
                                           ccPvta.getE01SPVRE1().equals("A") ||
                                           ccPvta.getE01SPVRE1().equals("B") ||   
                                           ccPvta.getE01SPVRE1().equals("C") ||
                                           ccPvta.getE01SPVRE1().equals("D") ||
                                           ccPvta.getE01SPVRE1().equals("E") ||  
                                           ccPvta.getE01SPVRE1().equals("F")))  out.print("selected"); %>></option>
   	     	    <option value="1" <% if (ccPvta.getE01SPVRE1().equals("1")) out.print("selected"); %>>Ninguna</option>
	   	   	    <option value="2" <% if (ccPvta.getE01SPVRE1().equals("2")) out.print("selected"); %>>Padre</option>
                <option value="3" <% if (ccPvta.getE01SPVRE1().equals("3")) out.print("selected"); %>>Madre</option>
                <option value="4" <% if (ccPvta.getE01SPVRE1().equals("4")) out.print("selected"); %>>Hermano(a)</option>
                <option value="5" <% if (ccPvta.getE01SPVRE1().equals("5")) out.print("selected"); %>>Abuelo(a)</option>
                <option value="6" <% if (ccPvta.getE01SPVRE1().equals("6")) out.print("selected"); %>>Tio(a)</option>
                <option value="7" <% if (ccPvta.getE01SPVRE1().equals("7")) out.print("selected"); %>>Hijo(a)</option>
   	     	    <option value="8" <% if (ccPvta.getE01SPVRE1().equals("8")) out.print("selected"); %>>Conyuge</option>
	   	   	    <option value="9" <% if (ccPvta.getE01SPVRE1().equals("9")) out.print("selected"); %>>Primo(a)</option>
                <option value="A" <% if (ccPvta.getE01SPVRE1().equals("A")) out.print("selected"); %>>Cunado(a)</option>
                <option value="B" <% if (ccPvta.getE01SPVRE1().equals("B")) out.print("selected"); %>>Nieto(a)</option>
                <option value="C" <% if (ccPvta.getE01SPVRE1().equals("C")) out.print("selected"); %>>Sobrino(a)</option>
                <option value="D" <% if (ccPvta.getE01SPVRE1().equals("D")) out.print("selected"); %>>Suegro(a)</option>
                <option value="E" <% if (ccPvta.getE01SPVRE1().equals("E")) out.print("selected"); %>>Nuera(a)</option>  
                <option value="F" <% if (ccPvta.getE01SPVRE1().equals("F")) out.print("selected"); %>>Yerno</option>                               
              </select>                           
            </td>            
          </tr>                              
        </table>
   </td>
   </tr>     
</table>
   <table class="tableinfo">  
   <tr>
   <td >     
    <table cellspacing="0" cellpadding="2" width="100%" border="0" >
           <tr id="trclear"> 
            <td> 
              <div align="left"><h4>Adicional 2</h4> </div>
            </td>        
          </tr>                           
           <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Rut Adicional : </div>
            </td>
            <td nowrap width="32%"> 
             <input type="text" name="E01SPVRA2" size="25" maxlength="25" value="<%= ccPvta.getE01SPVRA2() %>" >
            </td>
             <td nowrap width="25%"> </td>  
             <td nowrap width="20%"> </td>                        
          </tr>        
          <tr id="trclear">
            <td nowrap width="22%"> 
              <div align="right">Apellido Paterno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPA2" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPA2()%>" >
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
          <tr id="trdark">
            <td nowrap width="22%"> 
              <div align="right">Apellido Materno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVSA2" size="13" maxlength="13" value="<%=ccPvta.getE01SPVSA2()%>" >
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>Cupos Diferenciados</b> </div>
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Nombres : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPN2" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPN2()%>" >
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Nacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVCN2" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                
            </td>            
          </tr> 
         <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Fecha de Nacimiento : </div>
            </td>
             <td nowrap width="32%">
                <eibsinput:date name="ccPvta" fn_year="E01SPVBY2" fn_month="E01SPVBM2" fn_day="E01SPVBD2" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Internacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVIN2" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                  
            </td>            
          </tr> 
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Sexo : </div>
            </td>
             <td nowrap width="32%">
              <input type="radio" name="E01SPVSX2" value="F" <% if (ccPvta.getE01SPVSX2().equals("F")) out.print("checked"); %>>
              Femenino 
              <input type="radio" name="E01SPVSX2" value="M" <% if (ccPvta.getE01SPVSX2().equals("M")) out.print("checked"); %>>
              Masculino </td> 
            <td nowrap width="25%"> 
              <div align="right">Parentesco : </div>
            </td>
             <td nowrap width="25%">
              <select name="E01SPVRE2">
                <option value=" " <% if (!(ccPvta.getE01SPVRE2().equals("1") ||
                                           ccPvta.getE01SPVRE2().equals("2") ||
                                           ccPvta.getE01SPVRE2().equals("3") ||
                                           ccPvta.getE01SPVRE2().equals("4") ||
                                           ccPvta.getE01SPVRE2().equals("5") ||
                                           ccPvta.getE01SPVRE2().equals("6") ||
                                           ccPvta.getE01SPVRE2().equals("7") ||
                                           ccPvta.getE01SPVRE2().equals("8") ||
                                           ccPvta.getE01SPVRE2().equals("9") ||
                                           ccPvta.getE01SPVRE2().equals("A") ||
                                           ccPvta.getE01SPVRE2().equals("B") ||   
                                           ccPvta.getE01SPVRE2().equals("C") ||
                                           ccPvta.getE01SPVRE2().equals("D") ||
                                           ccPvta.getE01SPVRE2().equals("E") ||  
                                           ccPvta.getE01SPVRE2().equals("F")))  out.print("selected"); %>></option>
   	     	    <option value="1" <% if (ccPvta.getE01SPVRE2().equals("1")) out.print("selected"); %>>Ninguna</option>
	   	   	    <option value="2" <% if (ccPvta.getE01SPVRE2().equals("2")) out.print("selected"); %>>Padre</option>
                <option value="3" <% if (ccPvta.getE01SPVRE2().equals("3")) out.print("selected"); %>>Madre</option>
                <option value="4" <% if (ccPvta.getE01SPVRE2().equals("4")) out.print("selected"); %>>Hermano(a)</option>
                <option value="5" <% if (ccPvta.getE01SPVRE2().equals("5")) out.print("selected"); %>>Abuelo(a)</option>
                <option value="6" <% if (ccPvta.getE01SPVRE2().equals("6")) out.print("selected"); %>>Tio(a)</option>
                <option value="7" <% if (ccPvta.getE01SPVRE2().equals("7")) out.print("selected"); %>>Hijo(a)</option>
   	     	    <option value="8" <% if (ccPvta.getE01SPVRE2().equals("8")) out.print("selected"); %>>Conyuge</option>
	   	   	    <option value="9" <% if (ccPvta.getE01SPVRE2().equals("9")) out.print("selected"); %>>Primo(a)</option>
                <option value="A" <% if (ccPvta.getE01SPVRE2().equals("A")) out.print("selected"); %>>Cunado(a)</option>
                <option value="B" <% if (ccPvta.getE01SPVRE2().equals("B")) out.print("selected"); %>>Nieto(a)</option>
                <option value="C" <% if (ccPvta.getE01SPVRE2().equals("C")) out.print("selected"); %>>Sobrino(a)</option>
                <option value="D" <% if (ccPvta.getE01SPVRE2().equals("D")) out.print("selected"); %>>Suegro(a)</option>
                <option value="E" <% if (ccPvta.getE01SPVRE2().equals("E")) out.print("selected"); %>>Nuera(a)</option>  
                <option value="F" <% if (ccPvta.getE01SPVRE2().equals("F")) out.print("selected"); %>>Yerno</option>                               
              </select>
               
            </td>            
          </tr>  
        </table>
   </td>
   </tr>     
</table>        
   <table class="tableinfo">  
   <tr>
   <td >     
    <table cellspacing="0" cellpadding="2" width="100%" border="0" >
           <tr id="trclear"> 
            <td> 
              <div align="left"><h4>Adicional 3</h4> </div>
            </td>        
          </tr>                           
           <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Rut Adicional : </div>
            </td>
            <td nowrap width="32%"> 
             <input type="text" name="E01SPVRA3" size="25" maxlength="25" value="<%= ccPvta.getE01SPVRA3() %>" >
            </td>
             <td nowrap width="25%"> </td>  
             <td nowrap width="20%"> </td>                        
          </tr>        
          <tr id="trclear">
            <td nowrap width="22%"> 
              <div align="right">Apellido Paterno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPA3" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPA3()%>" >
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
          <tr id="trdark">
            <td nowrap width="22%"> 
              <div align="right">Apellido Materno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVSA3" size="13" maxlength="13" value="<%=ccPvta.getE01SPVSA3()%>" >
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>Cupos Diferenciados</b> </div>
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Nombres : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPN3" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPN3()%>" >
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Nacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVCN3" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                 
            </td>            
          </tr> 
         <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Fecha de Nacimiento : </div>
            </td>
             <td nowrap width="32%">
                <eibsinput:date name="ccPvta" fn_year="E01SPVBY3" fn_month="E01SPVBM3" fn_day="E01SPVBD3" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Internacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVIN3" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                 
            </td>            
          </tr> 
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Sexo : </div>
            </td>
             <td nowrap width="32%">
              <input type="radio" name="E01SPVSX3" value="F" <% if (ccPvta.getE01SPVSX3().equals("F")) out.print("checked"); %>>
              Femenino 
              <input type="radio" name="E01SPVSX3" value="M" <% if (ccPvta.getE01SPVSX3().equals("M")) out.print("checked"); %>>
              Masculino </td>             
            <td nowrap width="25%"> 
              <div align="right">Parentesco : </div>
            </td>
             <td nowrap width="25%">
              <select name="E01SPVRE3">
                <option value=" " <% if (!(ccPvta.getE01SPVRE3().equals("1") ||
                                           ccPvta.getE01SPVRE3().equals("2") ||
                                           ccPvta.getE01SPVRE3().equals("3") ||
                                           ccPvta.getE01SPVRE3().equals("4") ||
                                           ccPvta.getE01SPVRE3().equals("5") ||
                                           ccPvta.getE01SPVRE3().equals("6") ||
                                           ccPvta.getE01SPVRE3().equals("7") ||
                                           ccPvta.getE01SPVRE3().equals("8") ||
                                           ccPvta.getE01SPVRE3().equals("9") ||
                                           ccPvta.getE01SPVRE3().equals("A") ||
                                           ccPvta.getE01SPVRE3().equals("B") ||   
                                           ccPvta.getE01SPVRE3().equals("C") ||
                                           ccPvta.getE01SPVRE3().equals("D") ||
                                           ccPvta.getE01SPVRE3().equals("E") ||  
                                           ccPvta.getE01SPVRE3().equals("F")))  out.print("selected"); %>></option>
   	     	    <option value="1" <% if (ccPvta.getE01SPVRE3().equals("1")) out.print("selected"); %>>Ninguna</option>
	   	   	    <option value="2" <% if (ccPvta.getE01SPVRE3().equals("2")) out.print("selected"); %>>Padre</option>
                <option value="3" <% if (ccPvta.getE01SPVRE3().equals("3")) out.print("selected"); %>>Madre</option>
                <option value="4" <% if (ccPvta.getE01SPVRE3().equals("4")) out.print("selected"); %>>Hermano(a)</option>
                <option value="5" <% if (ccPvta.getE01SPVRE3().equals("5")) out.print("selected"); %>>Abuelo(a)</option>
                <option value="6" <% if (ccPvta.getE01SPVRE3().equals("6")) out.print("selected"); %>>Tio(a)</option>
                <option value="7" <% if (ccPvta.getE01SPVRE3().equals("7")) out.print("selected"); %>>Hijo(a)</option>
   	     	    <option value="8" <% if (ccPvta.getE01SPVRE3().equals("8")) out.print("selected"); %>>Conyuge</option>
	   	   	    <option value="9" <% if (ccPvta.getE01SPVRE3().equals("9")) out.print("selected"); %>>Primo(a)</option>
                <option value="A" <% if (ccPvta.getE01SPVRE3().equals("A")) out.print("selected"); %>>Cunado(a)</option>
                <option value="B" <% if (ccPvta.getE01SPVRE3().equals("B")) out.print("selected"); %>>Nieto(a)</option>
                <option value="C" <% if (ccPvta.getE01SPVRE3().equals("C")) out.print("selected"); %>>Sobrino(a)</option>
                <option value="D" <% if (ccPvta.getE01SPVRE3().equals("D")) out.print("selected"); %>>Suegro(a)</option>
                <option value="E" <% if (ccPvta.getE01SPVRE3().equals("E")) out.print("selected"); %>>Nuera(a)</option>  
                <option value="F" <% if (ccPvta.getE01SPVRE3().equals("F")) out.print("selected"); %>>Yerno</option>                               
              </select>               
            </td>            
          </tr>  
        </table>
   </td>
   </tr>     
</table> 
<% } else { mando = "NO";%>  
   <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>           
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="23%"> 
              <div align="Center"><b> Cuenta se encuentra Inactiva o Bloqueada </b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                   
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                            
        </table>
      </td>
    </tr>
  </table> 
 <%} %>          
         
<%}%>   
  <%-----------------------------------------------------------------------------------------Cambio Cupo ---------%> 
<% if (userPO.getOption().equals("3")) { %>  
 <h4>Cambio Cupo </h4>
 <%  if (ccNew.getE01CCRSTS().equals("01") && (ccNew.getE01CCRLKC().equals(""))) { %>  
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">       
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Cupo Compras Nacional : </div>
            </td>
             <td nowrap>
              <eibsinput:text property="E01SPVCNC" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"required="true"/>              				 
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Compras Internacional : </div>
            </td>
             <td nowrap>
              <eibsinput:text property="E01SPVCIC" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" required="true"/>              				 
            </td>
          </tr>                                              
        </table>
      </td>
    </tr>
  </table>   
<% } else { mando = "NO";%>  
   <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>           
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="23%"> 
              <div align="Center"><b> Tarjeta se encuentra Inactiva o Bloqueada </b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                   
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                            
        </table>
      </td>
    </tr>
  </table> 
 <%} %>          
 <%}%> 
<%-----------------------------------------------------------------------------------------Bloqueos y Desbloqueos ---------%> 
<% if (userPO.getOption().equals("4")) { %>  	
	<table cellpadding="2" width="100%" border="0" cellspacing="0">
		<tr id="trdark">
			<td nowrap align="left" width="50%">
				<h4>Bloqueos - Desbloqueos </h4>  					
			</td>
			<%if (ccNew.getH01FLGWK1().equals("F") ){ %>
				<td nowrap align="right" width="50%">
					<h2>Estado: Bloqueo Fallecido</h2>
				</td>
			<%}else{ %>
				<td nowrap align="right" width="50%">
				</td>
			<%} %>
		</tr>
	</table>        
 	
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr> 
<%  if (ccNew.getE01CCRLKC().equals("")) { %>                    
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Bloqueo : </div>
            </td>
             <td nowrap>
              <select name="E01SPVBLN">
                <option value=" "></option> 
	   	   	    <option value="N" <% if (ccPvta.getE01SPVBLN().equals("N")) out.print("selected"); %>>Cliente</option> 
	   	   	    <option value="O" <% if (ccPvta.getE01SPVBLN().equals("O")) out.print("selected"); %>>Robo/Perdida</option> 
	   	   	    <option value="S" <% if (ccPvta.getE01SPVBLN().equals("S")) out.print("selected"); %>>Emisor</option>                                             
              </select>             
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div> 
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr> 
<%  } else { %>
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Desbloqueo : </div>
            </td>
             <td nowrap>
              <select name="E01SPVBLN">
                <option value=" " >Sin Bloqueo</option>                             
              </select>             
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr> 
<% } %>      
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                                       
        </table>
      </td>
    </tr>
  </table>   
 <%} %>
 <%-----------------------------------------------------------------------------------------Reemision de la Tarjeta ---------%> 
<% if (userPO.getOption().equals("5")) { %>  
 <h4>Reemisi&oacute;n</h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">       
          <tr id="trdark"> 
            <td nowrap width="15%"> 
              <div align="right">Motivo Remisi&oacute;n :</div>
            </td>
             <td nowrap>
                <input type="text" name="E01SPVMOT" size="4" maxlength="4" value="<%= ccPvta.getE01SPVMOT() %>" readonly >
                <input type="text" name="E01SPVRMK"  size="30" maxlength="30" value="<%= ccPvta.getE01SPVRMK()%>" readonly>
              <a href="javascript:GetCodeDescCNOFC('E01SPVMOT','E01SPVRMK','TG')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0"></a>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Sucursal de Env�o de Tarjeta :</div>
            </td>
             <td nowrap>
              <input type="text" name="E01SPVSEC" size="4" maxlength="4" value="<%= ccPvta.getE01SPVSEC() %>" readonly >
                <input type="text" name="E01SPVBEC"  size="30" maxlength="30" value="<%= ccPvta.getE01SPVBEC() %>" readonly>
              <a href="javascript:GetBranch('E01SPVSEC','01','E01SPVBEC')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0">
            </td>
          </tr>                                              
        </table>
      </td>
    </tr>
  </table> 
 <%} %>
 <%-----------------------------------------------------------------------------------------Cambio de cargo FV ---------%> 
<% if (userPO.getOption().equals("6")) { %>  
 <h4>Cambio de Codigo FV </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">  
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>  
 <%  if (ccNew.getE01CCRTPI().equals("T") && 
         ccNew.getE01CCMAST().equals("1") && 
         ccNew.getE01CCMCBK().equals("") && 
         ccNew.getE01CCMCB2().equals("")) { %> 
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Nuevo FV :</div>
            </td>
             <td nowrap>
              <select name="E01SPVCFC">
                <option value='0' <% if (!(ccPvta.getField("E01SPVCFC").getString().equals("3") || ccPvta.getField("E01SPVCFC").getString().equals("23"))) out.print("selected"); %> selected></option>
                <option value='3' <% if(ccPvta.getField("E01SPVCFC").getString().equals("3")) out.print("selected");%>>3</option>
                <option value='23' <% if(ccPvta.getField("E01SPVCFC").getString().equals("23")) out.print("selected");%>>23</option>                            			
              </select>	   	   	                              			 
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>           
 <% } else {  mando = "NO";%>                              
           <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="23%"> 
              <div align="Center"><b> Tarjeta no es Titular o Cuenta Inactiva o Bloqueada </b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>
 <% } %>             
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                          
        </table>
      </td>
    </tr>
  </table>   
 <%}%> 
 <%-----------------------------------------------------------------------------------------Cambio Pago Minimo/MultiBanco---------%> 
<% if (userPO.getOption().equals("7")) { %>  
 <h4>Pago M&iacute;nimo </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>     
 <%  if (ccNew.getE01CCRTPI().equals("T") && 
         ccNew.getE01CCMAST().equals("1") && 
         ccNew.getE01CCMCBK().equals("") && 
         ccNew.getE01CCMCB2().equals("")) { %>              
           <tr id="trdark">
            <td nowrap width="27%"> 
              <div align="right">Pago M&iacute;nimo : </div> 			
             </td> 
             <td> 
              <select name="E01SPVCPM">
                <option value=0 <% if (!(ccPvta.getField("E01SPVCPM").getString().equals("0")) || ccPvta.getField("E01SPVCPM").getString().equals("1")) out.print("selected"); %> selected></option>
                <option value=0 <% if (ccPvta.getField("E01SPVCPM").getString().equals("0")) out.print("selected");%>>   5%</option>
                <option value=1 <% if (ccPvta.getField("E01SPVCPM").getString().equals("1")) out.print("selected");%>> 100%</option>                			
              </select>				  
             </td>               
            <td> 
              <div align="right">Cuenta Corriente Actual :</div> 
             </td>               
            <td nowrap width="27%">        
             <div align="center"> 
             <input type="text" name="E01SPVCCP" size="16" maxlength="13"  value="<%= ccPvta.getE01SPVCCP().trim() %>" 
              oncontextmenu="showPopUp(accountCustomerHelp,this.name,<%=ccNew.getE01CCMBNK().trim() %>,'',document.forms[0].E01CCRCUN.value,'','RT'); return false;"> 
              </div>               
          </td>                
           </tr>
 <% } else {  mando = "NO";%>           
           <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="23%"> 
              <div align="Center"><b> Tarjeta no es Titular o Cuenta Inactiva o Bloqueada </b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>            
<%} %>           
                  
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>           
        </table>
      </td>
    </tr>
  </table>   
 	<%-------------------------------- Cambio PAC Multibanco ---------%>   
 <h4> PAC Multibanco</h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>     
 <%  if (ccNew.getE01CCRTPI().equals("T") && 
         ccNew.getE01CCMAST().equals("1") && 
         ccNew.getE01CCMCBK().equals("") && 
         ccNew.getE01CCMCB2().equals("")) { %>              
           <tr id="trdark">
            <td nowrap width="27%"> 
              <div align="right"> PAC Multibanco : </div> 			
             </td> 
             <td> 
              <select name="E01SPVBLN">
                <option value='N' <% if (ccNew.getField("E01CCMCLT").getString().trim().equals("") || ccNew.getField("E01CCMCLT").getString().equals("N")) out.print("selected"); %> selected>NO</option>
                <option value='Y' <% if (ccNew.getField("E01CCMCLT").getString().equals("Y")) out.print("selected");%>>SI</option>
                			
              </select>				  
             </td>               
            <td> 
              <div align="right">&nbsp;</div> 
             </td>               
            <td nowrap width="27%"> 
				&nbsp;			
             </td>
           </tr>
 <% } else {  mando = "NO";%>           
           <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="23%"> 
              <div align="Center"><b> Tarjeta no es Titular o Cuenta Inactiva o Bloqueada </b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>            
<%} %>           
                  
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>           
        </table>
      </td>
    </tr>
  </table>   
  
 <%}%> 

<%-----------------------------------------------------------------------------------------Reseteo Clave ---------%> 
<% if (userPO.getOption().equals("8")) { %>   
<%  if (ccNew.getE01CCRSTS().equals("01") && ccNew.getE01CCRLKC().equals("")) { 
       mando = "SI";
} else { mando = "NO";%>
 <h4>Reseteo Clave </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">      
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>   
           <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="23%"> 
              <div align="Center"><b> Tarjeta Inactiva o Bloqueada </b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>             
            <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                           
        </table>
      </td>
    </tr>
  </table>   
 <%}%> 
 <%}%>  
 <%-----------------------------------------------------------------------------------------Cambio Direccion envio estado cuenta ---------%> 
<% if (userPO.getOption().equals("9")) { %>  
 <h4>Direcci�n Envio Estado Cuenta </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>  
 <%  if (ccNew.getE01CCRTPI().equals("T") && 
         ccNew.getE01CCRTDC().equals("C")) { %>           
           <tr id="trdark">
            <td nowrap width="27%"> 
              <div align="right">Nuevo correlativo Direcci�n : </div> 			
             </td> 
              <td nowrap width="61%">  
               <input type="text" name="E01SPVMAN" size="3" maxlength="2" value="<%= ccPvta.getE01SPVMAN().trim()%>">
               <a href="javascript:GetMailing('E01SPVMAN','<%= userPO.getCusNum().trim()%>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Direcciones de Correo del Cliente" align="absmiddle" border="0"></a> 
	            <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
             </td> 
             <td> 
              <div align="right"> </div> 
             </td> 
             <td> 
              <div align="right"> </div> 
             </td>               
            </tr> 
 <% } else {  mando = "NO";%>           
           <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="23%"> 
              <div align="Center"><b> Tarjeta no es Titular o No es Tarjeta de Credito </b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>            
<%} %>            
            <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                        
        </table>
      </td>
    </tr>
  </table>   
  <% } %>
 
 <h4>Identificaci�n de Usuario </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">      
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right">Rut Usuario : </div>
            </td>
            <td nowrap width="25%"> 
              <div align="left">
              <eibsinput:text property="E01SPVRID" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>"/>
              </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                
            <tr id="trdark"> 
            <td nowrap> 
              <div align="right"> </div>
            </td>
            <td nowrap> 
              <div align="right"> </div>
            </td>
             <td nowrap> 
              <div align="right"> </div>
            </td>
            <td nowrap> 
              <div align="right"> </div> 			
             </td>
          </tr>                                           
        </table>
      </td>
    </tr>
  </table>   
    
  <br>
 
 <% if (mando == "SI") { %>
	 <%String valor ="Enviar";%>
	 <%if (userPO.getOption().equals("10")) valor ="Eliminar";%>
	 <%if (userPO.getOption().equals("11")) valor ="Renuncia";%>
	 <%if (userPO.getOption().equals("12")) valor ="Activar";%>  	   
   <div align="center">   
       <input id="EIBSBTN" type=submit name="Submit" value="<%=valor%>" >
   </div>        
<% } else { %>
   <table class="tbenter" width="100%">
      <tr> 
	<TD class=TDBKG> 
	   <div align="center"><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
	</TD>            
      </tr>
    </table>  
<% } %>        

  </form>
</body>
</html>
