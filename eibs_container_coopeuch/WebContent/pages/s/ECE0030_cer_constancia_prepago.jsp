<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="datapro.eibs.master.Util"%>
<html>
<head>
<title>Certificados</title>
<!-- 
	Modificacion:	04/02/2016
	Autor:			MCastroC
	Detalle:		Incorporacion de nuevo campo doc. D4-B Fogape_Prepagos
					Otros Cargos.			
 -->
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ECE0030Help" class="datapro.eibs.beans.JBObjList"
	scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"
	scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"
	scope="session" />
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<jsp:useBean id="cerBasic" class="datapro.eibs.beans.ECE003001Message"
	scope="session" />

<script language="JavaScript">


function goAction_OLD(op) {

	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}

function  goAction(op) {
	var pg = '<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECE0030?SCREEN=2&opt=1';	
	CenterWindow(pg,720,500,2);

}

</SCRIPT>

</head>

<BODY>
<h3 align="center">Consulta Constancia Prepago<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" alt="cer_constancia_prepago.jsp,ECE0030"></h3>
<hr size="4">
<FORM name="form1" METHOD="post"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECE0030">
<input type=HIDDEN name="SCREEN" value="2"> <input type=HIDDEN
	name="opt" value="1">
<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			class="tbhead">
			<tr id="trdark">
				<td nowrap width="16%">
				<div align="right"><b>Cliente :</b></div>
				</td>
				<td nowrap width="20%">
				<div align="left"><input type="text" size="10" maxlength="9"
					name="E02DEACUN" value="<%= userPO.getHeader1().trim()%>" readonly>
				</div>
				</td>
				<td nowrap width="16%">
				<div align="right"><b>Nombre :</b></div>
				</td>
				<td nowrap colspan="3">
				<div align="left"><font face="Arial"><font face="Arial"><font
					size="2"> <input type="text" size="45" maxlength="45"
					name="E02CUSNA1" value="<%= userPO.getHeader2().trim()%>" readonly>
				</font></font></font></div>
				</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="16%">
				<div align="right"><b>Cuenta :</b></div>
				</td>
				<td nowrap width="20%">
				<div align="left"><input type="text" size="13" maxlength="12"
					name="E02DEAACC" value="<%= userPO.getHeader3().trim()%>" readonly>
				</div>
				</td>
				<td nowrap width="16%">
				<div align="right"><b>Moneda : </b></div>
				</td>
				<td nowrap width="16%">
				<div align="left"><b> <input type="text" name="E02DEACCY"
					size="3" maxlength="3" value="<%= userPO.getHeader4().trim()%>"
					readonly> </b></div>
				</td>
				<td nowrap width="16%">
				<div align="right"><b>Producto : </b></div>
				</td>
				<td nowrap width="16%">
				<div align="left"><b> <input type="text" size="4"
					maxlength="4" name="E02DEAPRO"
					value="<%= userPO.getHeader5().trim()%>" readonly> </b></div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<h4>Calculo Prepago para los proximos 22 dias</h4>
<TABLE id=cfTable class="tableinfo">
	<TR>
		<TD NOWRAP valign="top" width="100%">
		<table id="headTable" width="100%">
			<tr id="trdark">
				<th align=CENTER nowrap width="20%">
				<div align="center">Fecha Vigencia</div>
				</th>
				<th align=CENTER nowrap width="15%">
				<div align="center">Interes Devengado</div>
				</th>
				<th align=CENTER nowrap width="15%">
				<div align="center">Otros Cargos</div>
				</th>
				<th align=CENTER nowrap width="15%">
				<div align="center">Comision Prep.</div>
				</th>
				<th align=CENTER nowrap width="15%">
				<div align="center">Dev. Seguro</div>
				</th>
				<th align=CENTER nowrap width="15%">
				<div align="center">Fec. Expira Seg.</div>
				</th>
				<th align=CENTER nowrap width="15%">
				<div align="center">Monto Prepago</div>
				</th>
				<th align=CENTER nowrap width="15%">
				<div align="center">Saldo Prepago</div>
				</th>
			</tr>
			<%
                ECE0030Help.initRow();
				int idx = 0;
				String valStyle = "";
        		while (ECE0030Help.getNextRow()) {
                datapro.eibs.beans.ECE003001Message msgList = (datapro.eibs.beans.ECE003001Message) ECE0030Help.getRecord();
               	if (idx++ % 2 != 0)
					valStyle = "trdark";
				else
					valStyle = "trclear";
		    %>
			<TR id="<%= valStyle %>">
				<TD NOWRAP ALIGN=CENTER width=\"20%\"><%= Util.formatDate(msgList.getE01DEAVDD(),msgList.getE01DEAVMM(),msgList.getE01DEAVAA()) %></td>
				<TD NOWRAP ALIGN=CENTER width=\"15%\"><%= Util.formatCCY(msgList.getE01DEAIND())%></td>
				<TD NOWRAP ALIGN=CENTER width=\"15%\"><%= Util.formatCCY(msgList.getE01DEAINM())%></td>
				<!-- Otros Cargos -->
				<TD NOWRAP ALIGN=CENTER width=\"15%\"><%= Util.formatCCY(msgList.getE01DEACPR())%></td>
				<TD NOWRAP ALIGN=CENTER width=\"15%\"><%= Util.formatCCY(msgList.getE01DEADSE())%></td>
				<TD NOWRAP ALIGN=CENTER width=\"20%\"><%= Util.formatDate(msgList.getE01DEAEDD(),msgList.getE01DEAEMM(),msgList.getE01DEAEAA()) %></td>
				<TD NOWRAP ALIGN=CENTER width=\"15%\"><%= Util.formatCCY(msgList.getE01DEAMTO())%></td>
				<TD NOWRAP ALIGN=CENTER width=\"15%\"><%= Util.formatCCY(msgList.getE01DEASDO())%></td>
			</TR>
			<%
         }
             %>
		</table>
		</TD>
	</TR>
</table>

<table class="tbenter" width=100% align=center>
	<tr>
		<td class=TDBKG width="33%">
		<div align="center"><a href="javascript:goAction(1)"><b>Imprimir<br>
		Constancia</b></a></div>
		</td>
		<td class=TDBKG width="33%">
		<div align="center"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
		</td>
	</tr>
</table>
<SCRIPT language="JavaScript">
  
  showChecked("CURRCODE");
  
 function tableresize() {
    adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  tableresize();
  window.onresize=tableresize;   
</SCRIPT></form>

</body>
</html>
