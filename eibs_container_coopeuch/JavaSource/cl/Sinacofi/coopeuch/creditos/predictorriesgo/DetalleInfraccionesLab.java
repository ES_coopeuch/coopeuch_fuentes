/**
 * DetalleInfraccionesLab.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class DetalleInfraccionesLab  implements java.io.Serializable {
    private java.math.BigInteger numBoletin;
    private java.math.BigInteger paginaBoletin;
    private java.lang.String IFeBoletin;
    private java.lang.String tipoInfraccion;
    private java.math.BigInteger numMesesAdeu;
    private java.math.BigDecimal montoDeuImp;
    private java.math.BigInteger regInspec;
    private java.math.BigInteger numInspec;
    private java.math.BigInteger anoResol;
    private java.math.BigInteger numResol;
    private java.lang.String motivoInfrac;
    private java.lang.String nombreAFP;

    public DetalleInfraccionesLab() {
    }

    public java.math.BigInteger getNumBoletin() {
        return numBoletin;
    }

    public void setNumBoletin(java.math.BigInteger numBoletin) {
        this.numBoletin = numBoletin;
    }

    public java.math.BigInteger getPaginaBoletin() {
        return paginaBoletin;
    }

    public void setPaginaBoletin(java.math.BigInteger paginaBoletin) {
        this.paginaBoletin = paginaBoletin;
    }

    public java.lang.String getIFeBoletin() {
        return IFeBoletin;
    }

    public void setIFeBoletin(java.lang.String IFeBoletin) {
        this.IFeBoletin = IFeBoletin;
    }

    public java.lang.String getTipoInfraccion() {
        return tipoInfraccion;
    }

    public void setTipoInfraccion(java.lang.String tipoInfraccion) {
        this.tipoInfraccion = tipoInfraccion;
    }

    public java.math.BigInteger getNumMesesAdeu() {
        return numMesesAdeu;
    }

    public void setNumMesesAdeu(java.math.BigInteger numMesesAdeu) {
        this.numMesesAdeu = numMesesAdeu;
    }

    public java.math.BigDecimal getMontoDeuImp() {
        return montoDeuImp;
    }

    public void setMontoDeuImp(java.math.BigDecimal montoDeuImp) {
        this.montoDeuImp = montoDeuImp;
    }

    public java.math.BigInteger getRegInspec() {
        return regInspec;
    }

    public void setRegInspec(java.math.BigInteger regInspec) {
        this.regInspec = regInspec;
    }

    public java.math.BigInteger getNumInspec() {
        return numInspec;
    }

    public void setNumInspec(java.math.BigInteger numInspec) {
        this.numInspec = numInspec;
    }

    public java.math.BigInteger getAnoResol() {
        return anoResol;
    }

    public void setAnoResol(java.math.BigInteger anoResol) {
        this.anoResol = anoResol;
    }

    public java.math.BigInteger getNumResol() {
        return numResol;
    }

    public void setNumResol(java.math.BigInteger numResol) {
        this.numResol = numResol;
    }

    public java.lang.String getMotivoInfrac() {
        return motivoInfrac;
    }

    public void setMotivoInfrac(java.lang.String motivoInfrac) {
        this.motivoInfrac = motivoInfrac;
    }

    public java.lang.String getNombreAFP() {
        return nombreAFP;
    }

    public void setNombreAFP(java.lang.String nombreAFP) {
        this.nombreAFP = nombreAFP;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        DetalleInfraccionesLab other = (DetalleInfraccionesLab) obj;
        boolean _equals;
        _equals = true
            && ((this.IFeBoletin==null && other.getIFeBoletin()==null) || 
             (this.IFeBoletin!=null &&
              this.IFeBoletin.equals(other.getIFeBoletin())))
            && ((this.tipoInfraccion==null && other.getTipoInfraccion()==null) || 
             (this.tipoInfraccion!=null &&
              this.tipoInfraccion.equals(other.getTipoInfraccion())))
            && ((this.motivoInfrac==null && other.getMotivoInfrac()==null) || 
             (this.motivoInfrac!=null &&
              this.motivoInfrac.equals(other.getMotivoInfrac())))
            && ((this.nombreAFP==null && other.getNombreAFP()==null) || 
             (this.nombreAFP!=null &&
              this.nombreAFP.equals(other.getNombreAFP())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        DetalleInfraccionesLab history = (DetalleInfraccionesLab) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.numBoletin==null && other.getNumBoletin()==null) || 
             (this.numBoletin!=null &&
              this.numBoletin.equals(other.getNumBoletin())))
            && ((this.paginaBoletin==null && other.getPaginaBoletin()==null) || 
             (this.paginaBoletin!=null &&
              this.paginaBoletin.equals(other.getPaginaBoletin())))
            && ((this.numMesesAdeu==null && other.getNumMesesAdeu()==null) || 
             (this.numMesesAdeu!=null &&
              this.numMesesAdeu.equals(other.getNumMesesAdeu())))
            && ((this.montoDeuImp==null && other.getMontoDeuImp()==null) || 
             (this.montoDeuImp!=null &&
              this.montoDeuImp.equals(other.getMontoDeuImp())))
            && ((this.regInspec==null && other.getRegInspec()==null) || 
             (this.regInspec!=null &&
              this.regInspec.equals(other.getRegInspec())))
            && ((this.numInspec==null && other.getNumInspec()==null) || 
             (this.numInspec!=null &&
              this.numInspec.equals(other.getNumInspec())))
            && ((this.anoResol==null && other.getAnoResol()==null) || 
             (this.anoResol!=null &&
              this.anoResol.equals(other.getAnoResol())))
            && ((this.numResol==null && other.getNumResol()==null) || 
             (this.numResol!=null &&
              this.numResol.equals(other.getNumResol())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        DetalleInfraccionesLab history = (DetalleInfraccionesLab) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getNumBoletin() != null) {
            _hashCode += getNumBoletin().hashCode();
        }
        if (getPaginaBoletin() != null) {
            _hashCode += getPaginaBoletin().hashCode();
        }
        if (getIFeBoletin() != null) {
            _hashCode += getIFeBoletin().hashCode();
        }
        if (getTipoInfraccion() != null) {
            _hashCode += getTipoInfraccion().hashCode();
        }
        if (getNumMesesAdeu() != null) {
            _hashCode += getNumMesesAdeu().hashCode();
        }
        if (getMontoDeuImp() != null) {
            _hashCode += getMontoDeuImp().hashCode();
        }
        if (getRegInspec() != null) {
            _hashCode += getRegInspec().hashCode();
        }
        if (getNumInspec() != null) {
            _hashCode += getNumInspec().hashCode();
        }
        if (getAnoResol() != null) {
            _hashCode += getAnoResol().hashCode();
        }
        if (getNumResol() != null) {
            _hashCode += getNumResol().hashCode();
        }
        if (getMotivoInfrac() != null) {
            _hashCode += getMotivoInfrac().hashCode();
        }
        if (getNombreAFP() != null) {
            _hashCode += getNombreAFP().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
