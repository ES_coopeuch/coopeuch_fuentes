package datapro.eibs.salesplatform;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.coopeuch.core.tiendavirtual.ActualizaStockEntrada;
import cl.coopeuch.core.tiendavirtual.RespActualizaStock;
import cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtualSOAPProxy;

import com.adobe.fdf.FDFDoc;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100500Message;
import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV103801Message;
import datapro.eibs.beans.EPV117001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.reports.JSEFRM000PDF;
import datapro.eibs.sockets.MessageRecord;

public class JSEPV1038 extends JSEFRM000PDF {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_PLATFORM_LIST = 100;	
	protected static final int A_PLATFORM_LIST = 101;
	
	protected static final int R_PLATFORM_MAINT = 201;
	protected static final int A_PLATFORM_MAINTENANCE = 600;
	protected static final int A_PLATFORM_PRINT_FORMATS = 700;
	
	protected static final int A_PLATFORM_EXTERNAL = 9999;
	
	
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		switch (screen) {
			case R_PLATFORM_LIST:
				procReqPlatformList(user, req, res, session);
				break;
			case A_PLATFORM_LIST:
				procActionPlatformList(user, req, res, session);
				break;
			case R_PLATFORM_MAINT:
				procReqPlatform(user, req, res, session);
				break;
			case A_PLATFORM_MAINTENANCE:
				procActionMaintenance(user, req, res, session);
				break;
//				
			case A_PLATFORM_EXTERNAL:
				procActionExternal(user, req, res, session);
				break;
//				
			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}
	
	/**
	 * procReqPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		UserPos userPO = getUserPos(ses);
		userPO.setRedirect("/servlet/datapro.eibs.salesplatform.JSEPV1038?SCREEN=101");
		userPO.setHeader1("Plataforma de Ventas - Formalizacion");
		ses.setAttribute("userPO", userPO);
		forward("EPV1000_client_enter.jsp", req, res);
		
	}
	
	
	/**
	 * procActionPlatformList: find the list of forms depending on status, the program will epvl1005
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);		
		String customer = req.getParameter("E01CUN") == null ? "0" : req.getParameter("E01CUN").trim();
		
		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV1005", req);
			EPV100501Message msgList = (EPV100501Message) mp.getMessageRecord("EPV100501", user.getH01USR(), "0015");
			msgList.setE01SELCUN(customer);
			msgList.setE01SELSTS("4");

			// Sends message
			mp.sendMessage(msgList);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			//
			if (mp.hasError(error)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", error);
				forward("EPV1000_client_enter.jsp", req, res);
			} else {
				EPV100500Message header = (EPV100500Message) list.get(0);	
				userPO.setCusNum(header.getE00CUSCUN());
				userPO.setCusType(header.getE00CUSIDN());
				userPO.setCusName(header.getE00CUSNA1());
				session.setAttribute("userPO", userPO);
				list.remove(0);
				// save customer number
				userPO.setPurpose("FORMALIZATION");
				userPO.setRedirect(srctx +  "/servlet/datapro.eibs.salesplatform.JSEPV1038?SCREEN=101");
				// if there are NO errors display list
				session.setAttribute("userPO", userPO);						
				session.setAttribute("EPV100501List", list);
				
				if (list.isEmpty()){//then no occurrences
					if (!"S".equals(req.getParameter("fin"))){//Only show when not fin
						error.setERRNUM("1");
						error.setERNU01("0001");		//4		                
						error.setERDS01("No existen solicitudes pendiente para Formalizar de este Cliente");//70					
						session.setAttribute("error", error);						
					}
					procReqPlatformList(user,req,res,session);					
				} else if (list.size() > 1) {//then at least 2 occurrences
					forward("EPV1038_salesplatform_list.jsp", req, res);
				} else {
					redirect("datapro.eibs.salesplatform.JSEPV1038?SCREEN=201&E01PVMNUM="+ msgList.getE01PVMNUM(), res);	
				}
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	/**
	 * procReqPlatform: This Method show a single Sales Platform for evaluation process.
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatform(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		UserPos userPO = getUserPos(session);
		userPO.setPurpose("MAINTENANCE");
		String pageName = "";
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1038", req);

			EPV103801Message msg = (EPV103801Message) mp.getMessageRecord("EPV103801", user.getH01USR(), "0002");
			// Sets the client number
			msg.setE01PVMCUN(userPO.getCusNum());

			// Sets the platform number for maintenance
			if (req.getParameter("E01PVMNUM") != null) {
				msg.setE01PVMNUM(req.getParameter("E01PVMNUM"));
			}

			// Send message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV103801Message) mp.receiveMessageRecord();

			session.setAttribute("formalizacion", msg);// set header page
			session.setAttribute("error", msgError);

			if (mp.hasError(msgError)) {
				// if there are errors go back to list page
				pageName = "EPV1038_salesplatform_list.jsp";
			} else {
				// if there are no errors go to maintenance page
				pageName = "EPV1038_salesplatform_formalizacion.jsp";
			}
			
			
			forward(pageName, req, res);
		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	
	/**
	 * Process the Maintenance
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		boolean bsalto = true;

		try {
			mp = getMessageProcessor("EPV1038", req);

			EPV103801Message msg = (EPV103801Message) mp.getMessageRecord("EPV103801", user.getH01USR(), "0005");

			// Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//save for later the flag R(rechazar) o (A)aprobar
			String oper =  msg.getH01FLGWK1();
			
			//Sending message
			mp.sendMessage(msg);
			
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			
			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and show errors
				msg = (EPV103801Message) mp.receiveMessageRecord();
				session.setAttribute("formalizacion", msg);
				forward("EPV1038_salesplatform_formalizacion.jsp", req, res);			
			} else {
				
				if ("R".equals(oper)){//Rechazar, then go initial page
					quitarReservaProductosTiendaVirtual(req,session,user,msg.getE01PVMNUM());//se rechazo la solicitud, si hay tienda virtual se rechaza la reserva	
					procActionPlatformList(user, req, res, session);
				}else{
					// if there are no errors go print formats
					JBObjList list = mp.receiveMessageRecordList("E01MORFRM");
					if (!list.isEmpty()) {
						FDFDoc outputFDF = new FDFDoc();
						initDataStructure();
						
	                    int index = 0;
	                    String prefix = "";
						// add General Information like User ID....
	    				getFormData(outputFDF, (String)formatNames.get("ESS0030DS") + ".", user);

	    				// add forms information...
						MessageRecord newmsg = mp.receiveMessageRecord();

		                if(bsalto){
		                	String StrOpe = newmsg.getFieldString("E60LNSACC").toString();
		                	String StrNum = req.getParameter("E01PVMNUM").trim();
		                	String StrPrd = newmsg.getFieldString("E60PVEPRD").toString();
		                	session.setAttribute("StrOpe", StrOpe);
		                	session.setAttribute("StrNum", StrNum);
		                	session.setAttribute("StrPrd", StrPrd);
		                	bsalto = false;
		                }
						String ddsName = newmsg.getFormatName();
						while (true) {
							if (!ddsName.equals(newmsg.getFormatName())) {
								index = (hasIndex(newmsg.getFormatName())) ? 1 : 0;
							}
							prefix = getPrefix(newmsg);
							ddsName = newmsg.getFormatName();
	                        if (ddsName.equals("EFRM00001"))
	                            break;
	                        else                             	
	                    		buildFormList(outputFDF, newmsg, "." + prefix, index++);
	                        newmsg = mp.receiveMessageRecord();
						}
	                	String StrId = newmsg.getFieldString("E01APFDDS").toString(); 
	                	session.setAttribute("StrId", StrId);
						session.setAttribute("pdfData", outputFDF);							
					}
					session.setAttribute("pdfList", list);
					forward("EPV1038_pdf_list.jsp", req, res);	
				}//EoIe
					
			}//EoIe

		} finally {
			if (mp != null)  
				mp.close();
		}
	}
	
	private boolean quitarReservaProductosTiendaVirtual(HttpServletRequest req, HttpSession session,
			ESS0030DSMessage user,String nroSol) throws ServletException, IOException{
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		boolean procesado=false;
		try {
			mp = getMessageProcessor("EPV1170", req);

			EPV117001Message msg = (EPV117001Message) mp.getMessageRecord("EPV117001", user.getH01USR(), "0030");//todos los productos a rechazar

			// Sets message with page fields
			msg.setE01PTVCUN(userPO.getCusNum());
			msg.setE01PTVNUM(nroSol);

			//Sending message
			mp.sendMessage(msg);			

			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			if (!list.getNoResult()){//data exists 
				list.initRow();	 
				TiendaVirtualSOAPProxy tvp = new TiendaVirtualSOAPProxy();	
				ArrayList<String[]> err = new ArrayList<String[]>();
				while (list.getNextRow()) {
					EPV117001Message prod = (EPV117001Message) list.getRecord();
					if (!"".equals(prod.getE01PTVCAM()) && !"".equals(prod.getE01PTVPRD()) && !"".equals(prod.getE01TVIFLG())){
						//llenamos servicio tienda virtual...
						ActualizaStockEntrada ent = new ActualizaStockEntrada();
						ent.setCodCampana(Integer.valueOf(prod.getE01PTVCAM())); 
						ent.setCodProducto(Integer.valueOf(prod.getE01PTVPRD()));						
						ent.setCantidad(Integer.valueOf(prod.getE01PTVCAN()));
						ent.setTipoOperacion(prod.getE01TVIFLG());//Se envia R=rechazo reserva
						ent.setIdEvaluacion(Integer.valueOf(prod.getE01PTVNUM()));																							
						RespActualizaStock resp = tvp.actualizaStock(ent);					
						if (resp.getCodRespuesta()==1){//0=OK y 1=noOK
							String[] valores={prod.getE01PTVCAM(),prod.getE01PTVPRD(),prod.getE01PTVCAN(),prod.getE01TVIFLG()};
							err.add(valores);
						}else{
							//nos conectamos al as4000 para informar que el registro fue rechazado satisfactoriamente
							mp = getMessageProcessor("EPV1170", req);
							EPV117001Message msgResult = (EPV117001Message) mp.getMessageRecord("EPV117001", user.getH01USR(), "0031");
							assignValue(prod,msgResult);
							//Sending message
							mp.sendMessage(msgResult);	
							//Receive error and data
							ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();	
							if (mp.hasError(msgError)) {
								// if there are errors go back to maintenance page and sho errors
								session.setAttribute("error", msgError);								
								return procesado;
							}							
						}							
					}
					
				}//EoW
				
				if (err.size()>0){
					String numErr = (err.size()>10)?"10":String.valueOf(err.size());
		            ELEERRMessage msgError = new ELEERRMessage();
		            msgError.setERRNUM(numErr);
		            int fil=1;
		            for (Iterator<String[]> iterator = err.iterator(); iterator.hasNext();) {
		            	String fila = (fil>9)?""+fil:"0"+fil;
		            	String[] val = (String[]) iterator.next();
		            	String des = "Error Actualizar Stock Tienda Virtual Prod:"+val[1]+" Cant:"+val[2]+" Oper:"+val[3];//55
		            	msgError.getField("ERNU"+fila).setString(fila);//numero error
		            	msgError.getField("ERDS"+fila).setString(des);//descripcion error
					}
		            session.setAttribute("error", msgError);
				}else{
					//existen productos a actualizar y los proceso bien.
					procesado=true;
				}
				
			}else{
				//no existen productos a actualizar y proceso bien.
				procesado=true;
			}
			  


		}
		catch (Exception e) {
			String className = e.getClass().getName();
			String description = e.getMessage() == null ? "Exception General" : e.getMessage();					
            ELEERRMessage msgError = new ELEERRMessage();				
			msgError.setERRNUM("3");
            msgError.setERNU01("01");//4
            msgError.setERDS01("ERROR AL ACCEDER SERVICIO DE TIENDA VIRTUAL. VERIFIQUE COMUNICACION.");
            msgError.setERNU02("02");//4
            msgError.setERDS02(className);
            msgError.setERNU03("03");//4
            msgError.setERDS03(description.length() > 70 ? description.substring(0, 70) : description);	
			session.setAttribute("error", msgError);						
		}		
		finally {
			if (mp != null)
				mp.close();
		}		
		return procesado;
		
	}
	
	private void assignValue(EPV117001Message source, EPV117001Message destination){
		destination.setE01PTVCUN(source.getE01PTVCUN());
		destination.setE01PTVNUM(source.getE01PTVNUM());
		destination.setE01PTVSEQ(source.getE01PTVSEQ());
		destination.setE01PTVCAM(source.getE01PTVCAM());
		destination.setE01PTVNCA(source.getE01PTVNCA());
		destination.setE01PTVPRD(source.getE01PTVPRD());
		destination.setE01PTVCTV(source.getE01PTVCTV());
		destination.setE01PTVNME(source.getE01PTVNME());
		destination.setE01PTVMAR(source.getE01PTVMAR());
		destination.setE01PTVMOD(source.getE01PTVMOD());
		destination.setE01PTVVAU(source.getE01PTVVAU());
		destination.setE01PTVCAN(source.getE01PTVCAN());
		destination.setE01PTVSTO(source.getE01PTVSTO());
		destination.setE01PTVCEN(source.getE01PTVCEN());
		destination.setE01PTVPTO(source.getE01PTVPTO());
		destination.setE01PTVOPE(source.getE01PTVOPE());
		destination.setE01PTVRSP(source.getE01PTVRSP());
		destination.setE01NUMREC(source.getE01NUMREC());
		destination.setE01TVINUM(source.getE01TVINUM());
		destination.setE01TVIFLG(source.getE01TVIFLG());               
	}	
	
//
	protected void procActionExternal(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		MessageProcessor mp = null;

		try 
		{
			mp = getMessageProcessor("EPV1038", req);

			EPV103801Message msg = (EPV103801Message) mp.getMessageRecord("EPV103801", user.getH01USR(), "0005");

			msg.setH01SCRCOD("01");
			msg.setH01USERID("SMALDONADO");
			msg.setH01FLGWK1("A");
			//DATOS VARIABLES
			msg.setE01PVMCUN("22409");
			msg.setE01PVMNUM("2921727");

			mp.sendMessage(msg);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			
			if (mp.hasError(msgError)) 
			{
				msg = (EPV103801Message) mp.receiveMessageRecord();
			} 
			else 
			{
				JBObjList list = mp.receiveMessageRecordList("E01MORFRM");
				if (!list.isEmpty()) 
				{
					FDFDoc outputFDF = new FDFDoc();
					initDataStructure();
						
                    int index = 0;
                    String prefix = "";
					// add General Information like User ID....
    				getFormData(outputFDF, (String)formatNames.get("ESS0030DS") + ".", user);

    				// add forms information...
					MessageRecord newmsg = mp.receiveMessageRecord();

					String ddsName = newmsg.getFormatName();
					while (true) 
					{
						if (!ddsName.equals(newmsg.getFormatName())) 
						{
							index = (hasIndex(newmsg.getFormatName())) ? 1 : 0;
						}
						prefix = getPrefix(newmsg);
						ddsName = newmsg.getFormatName();
                        if (ddsName.equals("EFRM00001"))
                            break;
                        else                             	
                    		buildFormList(outputFDF, newmsg, "." + prefix, index++);
                        
                        newmsg = mp.receiveMessageRecord();
					}
					
					session.setAttribute("pdfData", outputFDF);	
					
					procReqFormExternal(req, res, session);
				}		
					
			}
		} finally {
			if (mp != null)  
				mp.close();
		}
	}	
	
//	
	
	
	
 }	



