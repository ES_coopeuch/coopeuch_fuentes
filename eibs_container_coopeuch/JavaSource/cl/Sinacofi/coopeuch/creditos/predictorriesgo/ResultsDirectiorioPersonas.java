/**
 * ResultsDirectiorioPersonas.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResultsDirectiorioPersonas  implements java.io.Serializable {
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.DirectiorioPersonas[] directiorioPersonas;

    public ResultsDirectiorioPersonas() {
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.DirectiorioPersonas[] getDirectiorioPersonas() {
        return directiorioPersonas;
    }

    public void setDirectiorioPersonas(cl.Sinacofi.coopeuch.creditos.predictorriesgo.DirectiorioPersonas[] directiorioPersonas) {
        this.directiorioPersonas = directiorioPersonas;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.DirectiorioPersonas getDirectiorioPersonas(int i) {
        return directiorioPersonas[i];
    }

    public void setDirectiorioPersonas(int i, cl.Sinacofi.coopeuch.creditos.predictorriesgo.DirectiorioPersonas value) {
        this.directiorioPersonas[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsDirectiorioPersonas history = (ResultsDirectiorioPersonas) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ResultsDirectiorioPersonas other = (ResultsDirectiorioPersonas) obj;
        boolean _equals;
        _equals = true
            && ((this.directiorioPersonas==null && other.getDirectiorioPersonas()==null) || 
             (this.directiorioPersonas!=null &&
              java.util.Arrays.equals(this.directiorioPersonas, other.getDirectiorioPersonas())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsDirectiorioPersonas history = (ResultsDirectiorioPersonas) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getDirectiorioPersonas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDirectiorioPersonas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDirectiorioPersonas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
