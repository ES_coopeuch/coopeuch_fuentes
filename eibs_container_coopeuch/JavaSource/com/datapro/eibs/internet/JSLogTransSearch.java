

package com.datapro.eibs.internet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.eibs.internet.beans.JBLog;
import com.datapro.eibs.internet.databeans.DC_SEARCH;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;

public class JSLogTransSearch extends SuperServlet
{

	public JSLogTransSearch()
	{
		LangPath = "S";
	}

	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		Socket s = null;
		MessageContext mc = null;
		ESS0030DSMessage msgUser = null;
		HttpSession session = null;
		LangPath = SuperServlet.rootPath + "s/";
		session = req.getSession(false);
		if(session == null)
		{
			try
			{
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			}
			catch(Exception e)
			{
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else
		{
			int screen = 1;
			try
			{
				msgUser = (ESS0030DSMessage)session.getAttribute("currUser");
				LangPath = SuperServlet.rootPath + msgUser.getE01LAN() + "/";
				try
				{
					flexLog("Opennig Socket Connection");
					s = new Socket(SuperServlet.hostIP, SuperServlet.iniSocket + 1);
					s.setSoTimeout(SuperServlet.sckTimeOut);
					mc = new MessageContext(new DataInputStream(new BufferedInputStream(s.getInputStream())), new DataOutputStream(new BufferedOutputStream(s.getOutputStream())), "datapro.eibs.beans");
					try
					{
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					}
					catch(Exception e)
					{
						flexLog("Screen set to default value");
					}
					switch(screen)
					{
					case 1: // '\001'
						RequestTransactions(req, res, session);
						break;

					case 2: // '\002'
						ActionTransactions(req, res, session);
						break;

					case 3: // '\003'
						RequestUsersHelp(req, res, session);
						break;

					default:
						res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.devPage);
						break;
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
					int sck = SuperServlet.iniSocket + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.sckNotOpenPage);
				}
				finally
				{
					s.close();
				}
			}
			catch(Exception e)
			{
				flexLog("Error: " + e);
				res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.sckNotRespondPage);
			}
		}
	}

	private void RequestUsersHelp(HttpServletRequest req, HttpServletResponse res, HttpSession session)
		throws ServletException, IOException
	{
		ELEERRMessage msgError = null;
		UserPos userPO = new UserPos();
		JBObjList lstEntities = null;
		JBLog logbean = new JBLog();
		JBList beanList = null;
		try
		{
			String myFlag = "";
			StringBuffer myRow = null;
			beanList = new JBList();
			lstEntities = logbean.getAllEntities();
			lstEntities.initRow();
			for(; lstEntities.getNextRow(); beanList.addRow(myFlag, myRow.toString()))
			{
				myRow = new StringBuffer("<TR>");
				myRow.append("<td nowrap><a href=\"javascript:enter('" + lstEntities.getRecord() + "')\">" + lstEntities.getRecord() + "</a></td>");
				myRow.append("</TR>");
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
			msgError.setERRNUM("Database SQL Error");
			throw new RuntimeException("Database SQL Error");
		}
		flexLog("Putting java beans into the session");
		session.setAttribute("userPO", userPO);
		session.setAttribute("lstEntities", beanList);
		session.setAttribute("error", msgError);
		try
		{
			flexLog("About to call Page: " + LangPath + "DCIBS_users_help.jsp");
			callPage(LangPath + "DCIBS_users_help.jsp", req, res);
		}
		catch(Exception e)
		{
			flexLog("Exception calling page " + e);
		}
	}

	private void ActionTransactions(HttpServletRequest req, HttpServletResponse res, HttpSession session)
		throws ServletException, IOException
	{
		ELEERRMessage msgError = null;
		UserPos userPO = new UserPos();
		JBObjList lstTransactions = new JBObjList();
		JBObjList lstTransactions2 = new JBObjList();
		JBLog logbean = new JBLog();
		String ENTITY = "";
		String SEARCH = "";
		String TRANSCODE = "";
		String GTRANSCODE = "";
		String FD1 = "";
		String FD2 = "";
		String FD3 = "";
		String ED1 = "";
		String ED2 = "";
		String ED3 = "";
		String RTYPE = "";
		int FromRecord = 0;
		DC_SEARCH paramters = new DC_SEARCH();
		ENTITY = req.getParameter("ENTITY");
		paramters.setENTITYID(ENTITY);
		SEARCH = req.getParameter("SEARCH");
		paramters.setSEARCH(SEARCH);
		TRANSCODE = req.getParameter("TRANSCODE");
		paramters.setTRANSCODE(TRANSCODE);
		GTRANSCODE = req.getParameter("GTRANSCODE");
		if(GTRANSCODE == null)
			GTRANSCODE = "";
		RTYPE = req.getParameter("RTYPE");
		if(RTYPE == null)
			RTYPE = "R";
		FromRecord = 0;
		FD1 = req.getParameter("FD1") != null ? req.getParameter("FD1") : "0";
		paramters.setFD1(FD1);
		FD2 = req.getParameter("FD2") != null ? req.getParameter("FD2") : "0";
		paramters.setFD2(FD2);
		FD3 = req.getParameter("FD3") != null ? req.getParameter("FD3") : "0";
		paramters.setFD3(FD3);
		ED1 = req.getParameter("ED1") != null ? req.getParameter("ED1") : "0";
		paramters.setED1(ED1);
		ED2 = req.getParameter("ED2") != null ? req.getParameter("ED2") : "0";
		paramters.setED2(ED2);
		ED3 = req.getParameter("ED3") != null ? req.getParameter("ED3") : "0";
		paramters.setED3(ED3);
		String FDATE = String.valueOf(Integer.parseInt("20" + FD3) * 10000 + Integer.parseInt(FD1) * 100 + Integer.parseInt(FD2));
		String EDATE = String.valueOf(Integer.parseInt("20" + ED3) * 10000 + Integer.parseInt(ED1) * 100 + Integer.parseInt(ED2));
		if(RTYPE.equals("R"))
		{
			try
			{
				if(TRANSCODE.equals("00000") && SEARCH.equals("D"))
					lstTransactions = logbean.getLogListAllUsersAllTransacByDate(ENTITY, FDATE, EDATE, FromRecord);
				else
				if(TRANSCODE.equals("00000") && SEARCH.equals("A"))
					lstTransactions = logbean.getLogListAllUsersAll(ENTITY, FromRecord);
				else
				if(!TRANSCODE.equals("00000") && SEARCH.equals("D"))
					lstTransactions = logbean.getLogListAllUserTransAllDate(ENTITY, TRANSCODE, FDATE, EDATE, FromRecord);
				else
				if(!TRANSCODE.equals("00000") && SEARCH.equals("A"))
					lstTransactions = logbean.getLogListAllUserTransNoDate(ENTITY, TRANSCODE, FromRecord);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				flexLog("Error: " + e);
				msgError.setERRNUM("Database SQL Error");
				throw new RuntimeException("Database SQL Error");
			}
			flexLog("Putting java beans into the session");
			session.setAttribute("paramters", paramters);
			session.setAttribute("userPO", userPO);
			session.setAttribute("error", msgError);
			session.setAttribute("lstTransactions", lstTransactions);
			try
			{
				flexLog("About to call Page: " + LangPath + "DCIBS_log_transaction_list.jsp");
				callPage(LangPath + "DCIBS_log_transaction_list.jsp", req, res);
			}
			catch(Exception e)
			{
				flexLog("Exception calling page " + e);
			}
		} else
		{
			try
			{
				if(GTRANSCODE.equals("00000"))
					lstTransactions = logbean.getGLogListAllUsersAll1(ENTITY, FromRecord);
				else
					lstTransactions = logbean.getGLogListAllUsersAll2(GTRANSCODE, FromRecord);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				flexLog("Error: " + e);
				msgError.setERRNUM("Database SQL Error");
				throw new RuntimeException("Database SQL Error");
			}
			flexLog("Putting java beans into the session");
			session.setAttribute("GTTYPE", paramters);
			session.setAttribute("userPO", userPO);
			session.setAttribute("error", msgError);
			session.setAttribute("lstTransactions", lstTransactions);
			try
			{
				flexLog("About to call Page: " + LangPath + "DCIBS_log_transaction_graph.jsp");
				callPage(LangPath + "DCIBS_log_transaction_graph.jsp", req, res);
			}
			catch(Exception e)
			{
				flexLog("Exception calling page " + e);
			}
		}
	}

	private void RequestTransactions(HttpServletRequest req, HttpServletResponse res, HttpSession session)
		throws ServletException, IOException
	{
		ELEERRMessage msgError = null;
		UserPos userPO = new UserPos();
		JBObjList lstEntities = null;
		JBObjList lstTransactions = null;
		JBLog logbean = new JBLog();
		String ENTITYID = req.getParameter("ENTITY");
		try
		{
			msgError = new ELEERRMessage();
		}
		catch(Exception ex)
		{
			flexLog("Error: " + ex);
		}
		try
		{
			lstEntities = logbean.getAllEntities();
			lstTransactions = logbean.getListTrans();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
			msgError.setERRNUM("Database SQL Error");
			throw new RuntimeException("Database SQL Error");
		}
		flexLog("Putting java beans into the session");
		session.setAttribute("userPO", userPO);
		session.setAttribute("lstEntities", lstEntities);
		session.setAttribute("lstTransactions", lstTransactions);
		session.setAttribute("error", msgError);
		try
		{
			flexLog("About to call Page: " + LangPath + "DCIBS_log_transaction.jsp");
			callPage(LangPath + "DCIBS_log_transaction.jsp", req, res);
		}
		catch(Exception e)
		{
			flexLog("Exception calling page " + e);
		}
	}

	protected String LangPath;
	protected static final int R_TRANSACTION = 1;
	protected static final int A_TRANSACTION = 2;
	protected static final int R_USERS_HELP = 3;
}
