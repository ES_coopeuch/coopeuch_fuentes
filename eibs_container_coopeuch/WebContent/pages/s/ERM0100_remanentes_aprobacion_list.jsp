<%@ page import="datapro.eibs.beans.ERM010001Message"%>
<%@ page import="datapro.eibs.master.Util"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<title>Remanentes</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="ERM010001List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">


  function goAction(op) {
	var ok = false;
	
    for(n=0; n<document.forms[0].elements.length; n++)
     {
     	var elementName= document.forms[0].elements[n].name;
     	if(elementName == "key") 
     	{
			if (document.forms[0].elements[n].checked == true) {
      			ok = true;
      			break;
			}
     	}
     }      
     if ( ok ) { 
		document.forms[0].SCREEN.value = op;
						  					
		if (op == '900'){	//Inquiry
			dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERM0100?SCREEN="+document.forms[0].SCREEN.value+
				  "&key="+document.forms[0].keysel.value;			
			CenterWindow(dir,780,500,2);
		} else {
			document.forms[0].submit();	
		}
		
     } else {
		alert("Debe seleccionar un convenio para continuar.");	   
	 }
		
	}

  
 function showAddInfo(idxRow){
    document.forms[0].keysel.value=idxRow;
     
   tbAddInfo.rows[0].cells[1].style.color="blue";   
   tbAddInfo.rows[0].cells[1].innerHTML=document.forms[0]["TXTDATA"+idxRow].value;//extraInfo(document.forms[0]["TXTDATA"+idxRow].value,4);

   }
      
   
 function extraInfo(textfields,noField) {
	 var pos=0
	 var s= textfields;
	 for ( var i=0; i<noField ; i++ ) {
	   pos=textfields.indexOf("<br>",pos+1);
	  }
	 s=textfields.substring(0,pos);
	 return(s);
 }  

</script>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Aprobacion Remanenetes<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_curse_list.jsp,ERM0100"></h3>

<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERM0100">
<input type="hidden" name="SCREEN" value="200"> 
<input type="hidden" name="keysel" value="">
<input type="hidden" name="totalRow" value="">
<input type="hidden" name="selected_customer">


<table class="tbenter" width=100% align=center>
	<tr>
		<td class=TDBKG width="35%">
			<div align="center"><a href="javascript:goAction('200')" id="linkApproval"><b>Aprobar</b></a></div>
		</td>
		<td class=TDBKG width="35%">
			<div align="center"><a href="javascript:goAction('400')" id="linkReject"><b>Eliminar</b></a></div>
		</td>
		<td class=TDBKG width="30%">
		<div align="center"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a></div>
		</td>
	</tr>
</table>

<% 
 int k = 0;
	if (ERM010001List.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=90%>
	<TR>
		<TD>
		<div align="center"><font size="3"><b> No hay Operaciones pendientes de Aprobar. </b></font></div>
		</TD>
	</TR>
</TABLE>
<%
	} else {
%>
 <table id="mainTable" class="tableinfo" align="center">
	<tr>
		<td nowrap valign="top">

		<TABLE id="headTable" >
    		<TR id="trdark">  		
				<th align="center" nowrap></th>
				<th align="center" nowrap>Cuenta</th>
				<th align="center" nowrap>Cliente</th>
				<th align="center" nowrap>Emision</th>
				<th align="center" nowrap>Monto Origen</th>
				<th align="center" nowrap>Saldo</th>

    		</TR>
       </TABLE>
<div id="dataDiv1" class="scbarcolor" >
    <table id="dataTable" >       		
			<%
				ERM010001List.initRow();
					k = 0;
					boolean firstTime = true;
					String chk = "";
					while (ERM010001List.getNextRow()) {
						if (firstTime) {
							firstTime = false;
							chk = "checked";
						} else {
							chk = "";
						}
						ERM010001Message convObj = (ERM010001Message) ERM010001List
								.getRecord();
			%>
			<tr>
				<td nowrap><input type="radio" name="key"
					value="<%=k%>" onclick="showAddInfo(<%=k%>); //setValue('<%=convObj.getE01RMMACC()%>');"
					<%=chk%>></td>
				<td nowrap align="center"><a href="javascript:goAction('900');"><%= Util.formatCell(convObj.getE01RMMACC())%></a></td>
				<td nowrap align="left"><a href="javascript:goAction('900');"><%= Util.formatCell(convObj.getE01CUSNA1())%></a></td>
				<td nowrap align="center"><a href="javascript:goAction('900');"><%=Util.formatCell(convObj.getE01RMMOPD() + "/" + convObj.getE01RMMOPM() + "/"+ convObj.getE01RMMOPY())%></a></td>
				<td nowrap align="right"><a href="javascript:goAction('900');"><%= Util.formatCell(convObj.getE01RMMOAM())%></a></td>
				<td nowrap align="right"><a href="javascript:goAction('900');"><%= Util.formatCell(convObj.getE01RMMAMT())%></a>
				<%
					String ls = "";
							ls += Util.formatCell(convObj.getE01RMMCUN()) + "<br>";
							ls += Util.formatCell(convObj.getE01RMMUSR()) + "<br>";
							ls += Util.fcolorCCY(convObj.getE01RMMRMK()) + "<br>";
				%>
				<input type="hidden" id="TXTDATA<%=k%>"  name="TXTDATA<%=k%>" value="<%=ls%>">
				</td>
			</tr>
			<%
			k++;
				}
			%>
 </table>
   </div>
		</td>

      <TD nowrap ALIGN="RIGHT" valign="top" > 
	       <Table id="tbAddInfoH"  width="100%" >
	        <tr id="trdark">
	            <TH ALIGN=CENTER nowrap > Informati&oacute;n B&aacute;sica</TH>
	        </tr>
	      </Table>
      
			<Table id="tbAddInfo" >
		      <tr id="trclear" >
		            <TD  ALIGN="RIGHT"  valign="center" nowrap >
		              <b>Numero de Cliente : <br>
		              Usuario : <br>
		              Remark :<br> </b>		              
		            </TD>
		         <TD ALIGN="LEFT" valign="center" nowrap class="tdaddinfo">hola</TD>
		      </tr>
		     </Table>
			
	</td>
	</tr>
</table>

<script type="text/javascript">
     //showAddInfo(0);          
</script> 
<br>
<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (ERM010001List.getShowPrev()) {
					int pos = ERM010001List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSECIF010?SCREEN=3&NameSearch="
									//+ ERM010001List.getSearchText() + "&Type="
									//+ ERM010001List.getSearchType() + "&Pos=" + pos
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (ERM010001List.getShowNext()) {
					int pos = ERM010001List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSECIF010?SCREEN=3&NameSearch="
									//	+ cifList.getSearchText() + "&Type="
									//	+ cifList.getSearchType() + "&Pos=" + pos
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/next_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>

</form>

<SCRIPT language="JavaScript">
  document.forms[0].totalRow.value="<%= k %>";
   function resizeDoc() {
       divResize(true);
       //adjustDifTables(headTable, dataTable, dataDiv1,2,1);
	   adjustEquTables(headTable, dataTable, dataDiv1,1,false);
       
  }
  showChecked("ACCNUM");
  resizeDoc();
  tbAddInfoH.rows[0].cells[0].height = headTable.rows[0].cells[0].clientHeight;
  window.onresize=resizeDoc;
   showAddInfo(0);   
</SCRIPT>
</body>
</html>
