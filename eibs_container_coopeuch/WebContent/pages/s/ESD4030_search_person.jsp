<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="datapro.eibs.master.Util"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="datapro.eibs.sockets.MessageRecord"%>
<%@page import="datapro.eibs.beans.ESD403001Message"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Entity</title>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
		<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
	</head>

	<jsp:useBean id="ESD403001ListLoad" class="datapro.eibs.beans.JBObjList" scope="session" />
	<jsp:useBean id="entity" class="datapro.eibs.beans.ESD403001Message" scope="session" />
	<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"  	 scope="session" />
	<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"  	     scope="session" />
	<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" 	scope="session" />
	<jsp:useBean id= "ListaVinculo" class= "datapro.eibs.beans.JBList"  scope="session" />
	<jsp:useBean id= "tipoTransaccion" class= "java.lang.String"  scope="session" />
	
	<body onload="cargarDatos();">
	
	<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
	<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

	<script type="text/javascript">
	
	function cargarDatos(){
		if (document.forms[0].TOTALCLIENTE.value != "0")
		{	
			parent.frames['query'].document.datos.E01PRN.value = document.forms[0].NOMBRE.value;
			parent.frames['query'].document.datos.E01PRD.value = document.forms[0].DIA.value;
			parent.frames['query'].document.datos.E01PRM.value = document.forms[0].MES.value;
			parent.frames['query'].document.datos.E01PRA.value = document.forms[0].ANO.value;
			parent.frames['query'].document.datos.E01PRC.value = document.forms[0].CORREO.value;
			parent.frames['query'].document.datos.E01PRF.value = document.forms[0].TELEFONO.value;
		}else{
			parent.frames['query'].document.datos.E01PRN.value = "";			
			parent.frames['query'].document.datos.E01PRC.value = "";
			parent.frames['query'].document.datos.E01PRF.value = "";
			fecha_actual();
			parent.frames['query'].document.datos.E01PRN.focus();
		}
	}
	
	function fecha_actual(){
		var today = new Date();
		var dia_today = today.getDate();
		var mes_today = (today.getMonth() + 1);
		var year_today = today.getFullYear();
		if(dia_today <= 9 && dia_today > 0){
			dia_today = "0"+dia_today;
		}
		else{
			dia_today = dia_today;
		}
		
		if(mes_today <= 9 && mes_today > 0){
			mes_today = "0"+mes_today;
		}
		else{
			mes_today = mes_today;
		}
		
		parent.frames['query'].document.datos.E01PRD.value = dia_today;
		parent.frames['query'].document.datos.E01PRM.value = mes_today; 			
 		parent.frames['query'].document.datos.E01PRA.value = year_today;
 		
	}
	
	</script>

	<H3 align="center"> 
		<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="entity_maintenance.jsp, ESD4030">
	</H3>
	
	<hr size="4">
	<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4030">
	<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600"> 
	<INPUT TYPE=HIDDEN NAME="APPROVAL" VALUE="N"> 
	<INPUT TYPE=HIDDEN NAME="vinculo" VALUE=""> 
    <input type=HIDDEN name="L01FILLE2" value="<%=entity.getL01FILLE2().trim()%>"> 
    <input type=HIDDEN name="TIPOTRANSACCION" value="<%=tipoTransaccion%>">
    
    <%
		int row = 0;
		int i = 0;
		ESD403001ListLoad.initRow();
		while (ESD403001ListLoad.getNextRow()) {
			String cadena ="";
	        ESD403001Message msgList = (ESD403001Message) ESD403001ListLoad.getRecord();%>
	        <input type=HIDDEN name="NOMBRE" value="<%=msgList.getE01PRN()%>">
	        <input type=HIDDEN name="DIA" value="<%=msgList.getE01PRD()%>">
	        <input type=HIDDEN name="MES" value="<%=msgList.getE01PRM()%>">
	        <input type=HIDDEN name="ANO" value="<%=msgList.getE01PRA()%>">
	        <input type=HIDDEN name="CORREO" value="<%=msgList.getE01PRC()%>">
	        <input type=HIDDEN name="TELEFONO" value="<%=msgList.getE01PRF()%>">
	    <%
	        
	      i++;  
		}    
     %>
    <input type=HIDDEN name="TOTALCLIENTE" value="<%=i%>">
	</form>
</body>
</html>