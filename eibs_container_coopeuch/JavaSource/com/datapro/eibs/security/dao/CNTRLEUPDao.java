package com.datapro.eibs.security.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.UserNotFoundException;
import com.datapro.generic.access.DAOSuper;
import com.datapro.eibs.security.bean.CNTRLEUP;
import com.datapro.generic.beanutil.*;
import datapro.eibs.beans.JBObjList;

public class CNTRLEUPDao extends DAOSuper {


    private static Category cat =
		Category.getInstance(CNTRLEUPDao.class.getName());
			
	/**
	 * CNTRLBTHDao constructor comment.
	 */

	public CNTRLEUPDao(String DBID) {
		super(DBID);
	}
	
	/**
	 * Constructor for DAOUserEibs.
	 */
	public CNTRLEUPDao() {
		super();
	}

	/**
	 * Constructor for DAOUserEibs.
	 * @param connection
	 */
	public CNTRLEUPDao(Connection connection) {
		super(connection);
	}

	public CNTRLEUP select(String USERID) throws UserNotFoundException,DAOException {
		
		CNTRLEUP bean = null;
		
		try {
			String sql = "SELECT * FROM CNTRLEUP WHERE EUPUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, USERID.toUpperCase());
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNTRLEUP();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} 
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return bean;
		
	}		
		
	
	public int delete (String USERID) throws DAOException {
		int rt;

		try {
			String sql = "DELETE FROM cntrleup WHERE eupusr=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, USERID.toUpperCase());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
		
	}
	
	/**
	 * Method deleteByGroup.
	 * @param gid
	 * @return int
	 * @throws DAOException
	 * Delete all eibs user for a group.
	 */
	public int deleteByGroup(String gid) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM CNTRLEUP EUP WHERE EUP.EUPUSR IN (SELECT EUP.EUPUSR FROM CNTRLEUP EUP JOIN CNTRLBTH BTH ON BTH.BTHUSR=EUP.EUPUSR WHERE BTH.BTHGRP=?)";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, gid.toUpperCase());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}
	
	public int update (CNTRLEUP bean) throws DAOException {
		
		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.updatePrepareStatement() + " WHERE EUPUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			ps.setString(bs.getNextParamIndex(), bean.getEUPUSR().toUpperCase());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
		
		
	}

	public int insert (CNTRLEUP bean) throws DAOException {
		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.insertPrepareStatement();
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

}