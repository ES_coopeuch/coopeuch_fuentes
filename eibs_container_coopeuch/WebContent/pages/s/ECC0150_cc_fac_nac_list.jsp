<%@ page import="datapro.eibs.beans.ECC015001Message"%>
<%@ page import="datapro.eibs.master.Util"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<title>Resumen de facturaciones Nacionales</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ECC015001List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">


function goAction(op) {
      if (op == 1)       
      {
      	var valueCURR = getElementChecked("CURR150").value;
		pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150?SCREEN=500&CURR150="+valueCURR;
	  }
	   CenterWindow(pg,950,600,2);
   }
  
 function showAddInfo(idxRow){
   tbAddInfo.rows[0].cells[1].style.color="blue";   
   tbAddInfo.rows[0].cells[1].innerHTML=extraInfo(document.forms[0]["TXTDATA"+idxRow].value,4);
   } 
   
 function extraInfo(textfields,noField) {
	 var pos=0
	 var s= textfields;
	 for ( var i=0; i<noField ; i++ ) {
	   pos=textfields.indexOf("<br>",pos+1);
	  }
	 s=textfields.substring(0,pos);
	 return(s);
 }  
</script>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Resumen de Facturas Nacionales<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="cc_fac_nac_list.jsp,ECC0150"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150">
<input type="hidden" name="SCREEN" value="201"> 
 <% int row = 0;%>
<h4>Cliente</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCUN" size="13" maxlength="12" readonly value="<%= userPO.getCusNum().trim()%>">                  
               </div>               
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre : </b></div>
            </td>
            <td nowrap width="20%"  > 
              <div align="left"> 
                <input type="text" name="E01CCMNME" size="35" maxlength="35" readonly value="<%= userPO.getCusName().trim()%>">                                 
              </div>
            </td>            
            <td nowrap width="16%" > 
              <div align="right"><b>Identif. Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCID" size="15" maxlength="15" readonly value="<%= userPO.getIdentifier().trim()%>">                
               </div>               
            </td>            
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta IBS : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCMACC" size="13" maxlength="12" readonly value="<%= userPO.getAccNum().trim() %>">                                 
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMCCY" size="5" maxlength="4" readonly value="<%= userPO.getCurrency().trim() %>">                              
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Oficial : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMOFC" size="5" maxlength="4" readonly value="<%= userPO.getOfficer().trim() %>">                               
                </b> </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
          <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMPRO" size="5" maxlength="4" readonly value="<%= userPO.getProdCode().trim() %>">                  
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Desc. Producto : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="D01CCMPRO" size="35" maxlength="35" readonly value="<%= userPO.getHeader20().trim()%>">                              
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Nro. Cuenta : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCRNXN" size="20" maxlength="20" readonly value="<%= userPO.getHeader21().trim() %>">                              
              </div>
            </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>
  <p>&nbsp;</p>

<%
	if (ECC015001List.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=90%>
	<TR>
		<TD>
		<div align="center"><font size="3"><b> No hay
		resultados que correspondan a su criterio de b�squeda. </b></font></div>
		</TD>
	</TR>
	<TR>
	<TD ALIGN=CENTER class=TDBKG><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
	</TD>
	</TR>	
</TABLE>
<%
	} else {
%>
 <TABLE class="tbenter" width="100%">
	<TR>
		<TD ALIGN=CENTER class=TDBKG><a href="javascript:goAction(1)"><b>Consulta <br>Detalle</b></a>
		</TD>
		<TD ALIGN=CENTER class=TDBKG><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</TD>
	</TR>
</TABLE>
<table class="tableinfo">
	<tr>
		<td nowrap valign="top">
		 <table cellpadding=2 cellspacing=0 width="100%" border="1">
			<tr id="trclear">
				<th align="center" nowrap width="2%">&nbsp;</th>
				<th align="center" nowrap width="9%">Fecha<br>Vencimiento</th>
				<th align="center" nowrap width="9%">Fecha<br>Facturacion</th>
				<th align="center" nowrap width="10%">Saldo<br>Anterior</th>
				<th align="center" nowrap width="10%">Total<br> Pagos</th>
				<th align="center" nowrap width="10%">Total Cargos<br>Automaticos</th>
				<th align="center" nowrap width="10%">Total Compras<br>Cuotas/Avances</th>
				<th align="center" nowrap width="10%">Total Cargos<br>y Abonos</th>
				<th align="center" nowrap width="10%">Deuda Total<br>Facturada</th>		
				<th align="center" nowrap width="10%">Saldo En <br>Mora</th>	
				<th align="center" nowrap width="10%">Pago<br>Minimo</th>													
			</tr>
			<% 
				ECC015001List.initRow();
					boolean firstTime = true;
					String chk = "";
					while (ECC015001List.getNextRow()) {
						if (firstTime) {
							firstTime = false;
							chk = "checked";
						} else {
							chk = "";
						}
						ECC015001Message convObj = (ECC015001Message) ECC015001List
								.getRecord();
			%>
			<tr id="trdark">
				<td nowrap><input type="radio" name="CURR150"
					value="<%=ECC015001List.getCurrentRow()%>" onclick="showAddInfo(<%=ECC015001List.getCurrentRow()%>);"
					<%=chk%>></td>
				<td nowrap align="center"><%=Util.formatDate(convObj.getE01CCHVTD(),convObj.getE01CCHVTM(),convObj.getE01CCHVTA())%></td>
				<td nowrap align="center"><%=Util.formatDate(convObj.getE01CCHFFD(),convObj.getE01CCHFFM(),convObj.getE01CCHFFA())%></td>				
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE01CCHSAN())%></td>				
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE01CCHTPG())%></td>
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE01CCHTAU())%></td>
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE01CCHTAC())%></td>
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE01CCHTCA())%></td>
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE01CCHDTP())%></td>
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE01CCHSEM())%></td>
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE01CCHPMI())%></td>				
			</tr>
			<%
				}
			%>
		</table>
		</td>
	</tr>
</table>

<script type="text/javascript">
     showAddInfo(0);     
     
</script> <br>
<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (ECC015001List.getShowPrev()) {
					int pos = ECC015001List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.products.JSECC0150?SCREEN=3&NameSearch="
									//+ ECC015001List.getSearchText() + "&Type="
									//+ ECC015001List.getSearchType() + "&Pos=" + pos
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (ECC015001List.getShowNext()) {
					int pos = ECC015001List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.products.JSECC0150?SCREEN=3&NameSearch="
									//	+ cifList.getSearchText() + "&Type="
									//	+ cifList.getSearchType() + "&Pos=" + pos
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/next_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>
</form>

</body>
</html>
