package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Modificado por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  01/12/2011                                                                                      **/
/**  Objetivo                :  Incorporacion manejo de productos de Cuotas de Participacion Nuevo ACD = 06                     **/
/**                                                                                                                             **/
/**  Modificado por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL02                                                                                           **/
/**  Fecha                   :  29/01/2013                                                                                      **/
/**  Objetivo                :  Incorporacion manejo de ahorro Vivienda                                                         **/
/*********************************************************************************************************************************/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.Socket;
import java.text.DecimalFormat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.exception.ServiceLocatorException;

import datapro.eibs.beans.ECO050001Message;
import datapro.eibs.beans.EDD000001Message;
import datapro.eibs.beans.EDD000002Message;
import datapro.eibs.beans.EDD000003Message;
import datapro.eibs.beans.EDD000004Message;
import datapro.eibs.beans.EDD000005Message;
import datapro.eibs.beans.EDD000010Message;
import datapro.eibs.beans.EDD009001Message;
import datapro.eibs.beans.EDD009002Message;
import datapro.eibs.beans.EDD009201Message;
import datapro.eibs.beans.EDL090001Message;
import datapro.eibs.beans.EDL090002Message;
import datapro.eibs.beans.EDL090003Message;
import datapro.eibs.beans.EFT000015Message;
import datapro.eibs.beans.ELD000001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD000002Message;
import datapro.eibs.beans.ESD000004Message;
import datapro.eibs.beans.ESD000005Message;
import datapro.eibs.beans.ESD000006Message;
import datapro.eibs.beans.ESD009001Message;
import datapro.eibs.beans.ESD071103Message;
import datapro.eibs.beans.EDD210001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBListRec;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;



public class JSEDD0000 extends datapro.eibs.master.SuperServlet {
	// RETAIL ACCOUNTS
	protected static final int R_RT_NEW = 1;
	protected static final int A_RT_NEW = 2;
	protected static final int R_RT_MAINTENANCE = 3;
	protected static final int A_RT_MAINTENANCE = 4;
	protected static final int R_RT_OVERDRAFT = 5;
	protected static final int A_RT_OVERDRAFT = 6;
	protected static final int R_RT_LIN_CRED = 7;
	protected static final int A_RT_LIN_CRED = 8;
	protected static final int R_RT_MONEY = 11;
	protected static final int A_RT_MONEY = 12;
	protected static final int R_RT_CODES = 13;
	protected static final int A_RT_CODES = 14;
	protected static final int R_RT_ENTER_STATUS = 150;
	protected static final int R_RT_STATUS = 15;
	protected static final int A_RT_STATUS = 16;
	protected static final int R_RT_TITULARES = 17;
	protected static final int A_RT_TITULARES = 18;
	protected static final int R_RT_FIRMANTES = 19;
	protected static final int A_RT_FIRMANTES = 20;
	protected static final int R_RT_SPECIAL_INST = 21;
	protected static final int A_RT_SPECIAL_INST = 22;
	protected static final int R_RT_BENEFICIARIES = 23;
	protected static final int A_RT_BENEFICIARIES = 24;
	protected static final int R_RT_PRINT_NEW = 43;
	protected static final int A_RT_PRINT_NEW = 44;
	protected static final int R_RT_OVERNIGHT = 45;
	protected static final int A_RT_OVERNIGHT = 46;
	protected static final int R_RT_LEGAL_REP = 47;
	protected static final int A_RT_LEGAL_REP = 48;
	protected static final int R_RT_ACC_ANALYSIS = 101;
	protected static final int A_RT_ACC_ANALYSIS = 102;
	protected static final int R_RT_ACC_TELLER_MSG = 103;
	protected static final int A_RT_ACC_TELLER_MSG = 104;

	protected static final int R_INQ_ACCOUNT_ANALYSIS = 105;

	protected static final int R_SV_NEW = 25;
	protected static final int R_SV_MAINTENANCE = 27;

	protected static final int A_RT_BASIC = 29;
	protected static final int A_SV_BASIC = 30;

	// Entering options
	protected static final int R_RT_ENTER_NEW = 100;
	protected static final int R_RT_ENTER_MAINT = 300;
	protected static final int R_SV_ENTER_NEW = 500;
	protected static final int R_SV_ENTER_MAINT = 700;
	protected static final int R_RT_ENTER_PRINT = 1100;
	protected static final int R_RT_ENTER_INQUIRY = 1300;
	protected static final int R_SV_ENTER_INQUIRY = 1500;
	protected static final int R_SV_ENTER_PRINT = 1700;

	protected static final int A_RT_ENTER_NEW = 200;
	protected static final int A_RT_ENTER_MAINT = 400;
	protected static final int A_SV_ENTER_NEW = 600;
	protected static final int A_SV_ENTER_MAINT = 800;
	protected static final int A_RT_ENTER_PRINT = 1200;
	protected static final int A_RT_ENTER_INQUIRY = 1400;
	protected static final int A_SV_ENTER_INQUIRY = 1600;
	protected static final int A_SV_ENTER_PRINT = 1800;
	protected static final int R_SV_SALDOS_AHORRO_VIV = 817;
	protected static final int A_SV_SALDOS_AHORRO_VIV = 818;

	protected static final int R_RT_ENTER_SOBREGIRO = 2000;
	protected static final int R_RT_OVERDRAFT_OPC = 2005;
	protected static final int A_RT_OVERDRAFT_OPC = 2006;
	/**
	 * Indicador de Solicitud de Ex Convenios
	 * */
	protected static final int A_SV_CONVENIOS = 2014;
	/**
	 * Indicador de Ver Ex Convenios
	 * */
	protected static final int A_INTER_CONVENIOS = 987;
	/**
	 * Indicador Para Consulta de IFRS
	 * */
	protected static final int A_SV_IFRS = 3001;
	/**
	 * Indicador Para Ver IFRS
	 * */
	protected static final int A_INTER_IFRS = 3002;
	
	protected static final int R_RT_ENTER_LINEACREDITO = 3000;
	protected static final int R_RT_LINEACREDITO = 3007;
	protected static final int A_RT_LINEACREDITO = 3008;

	// Inquiry Options
	protected static final int R_INQ_BALANCES = 31;
	protected static final int R_INQ_BASIC = 32;
	protected static final int R_INQ_MONEY = 33;
	protected static final int R_INQ_AVERAG = 34;
	protected static final int R_INQ_CODES = 35;
	protected static final int R_INQ_SIGNERS = 36;
	protected static final int R_INQ_SP_INST = 37;
	protected static final int R_INQ_TIT = 38;
	protected static final int R_INQ_BENE = 39;
	protected static final int R_PRODUCT = 40;
	protected static final int R_RELATED_ACC = 41;
	protected static final int R_DAILY_BAL = 42;
	protected static final int R_INQ_LEGAL_REP = 50;

	// inquiry CHECKBOOK option
	protected static final int R_INQ_CHECKBOOK = 51;

	protected static final int R_ACCOUNT_TITLE = 57;
	protected static final int A_ACCOUNT_TITLE = 58;

	// Cuotas de Participacion PCL01
	protected static final int R_CP_NEW = 60;
	protected static final int R_CP_MAINTENANCE = 61;
	protected static final int A_CP_BASIC = 62;
	protected static final int R_CP_ENTER_NEW = 4500;
	protected static final int R_CP_ENTER_MAINT = 4700;
	protected static final int R_CP_ENTER_INQUIRY = 5500;
	protected static final int R_CP_ENTER_PRINT = 5700;
	protected static final int A_CP_ENTER_NEW = 4600;
	protected static final int A_CP_ENTER_MAINT = 4800;
	protected static final int A_CP_ENTER_INQUIRY = 5600;
	protected static final int A_CP_ENTER_PRINT = 5800;
	// -----------------------------
	protected String LangPath = "S";

	/**
	 * This method was created by David Mavilla.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procActionEnterInquiry(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD009001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgRT = (EDD009001Message) mc.getMessageRecord("EDD009001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			flexLog("Option " + userPO.getOption());
			msgRT.setH01SCRCOD(userPO.getProdCode());
			msgRT.setH01OPECOD("0002");
			try {
				msgRT.setE01ACMACC(req.getParameter("E01ACMACC"));
			} catch (Exception e) {
				msgRT.setE01ACMACC("0");
			}
			flexLog("EDD009001 enviado = " + msgRT);
			msgRT.send();
			msgRT.destroy();

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD009001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD009001Message();
					flexLog("EDD009001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD009001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				if (IsNotError) { // There are no errors
					if (msgRT.getE01ACMACD().equals("04")) {
						userPO.setOption("SV");
					} else if (msgRT.getE01ACMACD().equals("06")) // Cuotas de
																	// Participacion
																	// PCL01
					{
						userPO.setOption("CP");
					} else {
						userPO.setOption("RT");
					}

					userPO.setAccNum(msgRT.getE01ACMACC());
					userPO.setIdentifier(msgRT.getE01ACMACC());
					userPO.setBank(msgRT.getE01ACMBNK());
					userPO.setCusNum(msgRT.getE01ACMCUN());
					userPO.setHeader1(msgRT.getE01ACMPRO());
					userPO.setHeader2(msgRT.getE01ACMCCY());
					userPO.setHeader3(msgRT.getE01CUSNA1());
					userPO.setCusType(msgRT.getH01FLGWK3());

					ses.setAttribute("userPO", userPO);
					ses.setAttribute("rtBalances", msgRT);

					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_inq_balances.jsp");
						callPage(LangPath + "EDD0000_rt_inq_balances.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_enter_inquiry.jsp");
							callPage(LangPath + "EDD0000_sv_enter_inquiry.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else { // Cuotas de Participacion
						if (userPO.getOption().equals("CP")) {
							try {
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_cp_enter_inquiry.jsp");
								callPage(LangPath
										+ "EDD0000_cp_enter_inquiry.jsp", req,
										res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						} else {
							try {
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_enter_inquiry.jsp");
								callPage(LangPath
										+ "EDD0000_rt_enter_inquiry.jsp", req,
										res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procActionEnterMaint(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01FLGWK1("R");
			msgRT.setH01OPECOD("0002");
			try {
				msgRT.setE01ACMACC(req.getParameter("E01ACMACC"));
			} catch (Exception e) {
				msgRT.setE01ACMACC("0");
			}

			msgRT.send();
			msgRT.destroy();
			flexLog("EDD000001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
					flexLog("EDD000001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				if (IsNotError) { // There are no errors
					if (msgRT.getE01ACMACD().equals("04")) {
						userPO.setOption("SV");
						userPO.setAccNum(msgRT.getE01ACMACC());
						userPO.setIdentifier(msgRT.getE01ACMACC());
						userPO.setApplicationCode(msgRT.getE01ACMACD());
						userPO.setBank(msgRT.getE01ACMBNK());
						userPO.setCusNum(msgRT.getE01ACMCUN());
						userPO.setHeader1(msgRT.getE01ACMPRO());
						userPO.setHeader2(msgRT.getE01ACMCUN());
						userPO.setHeader3(msgRT.getE01CUSNA1());
						userPO.setCurrency(msgRT.getE01ACMCCY());
						userPO.setCusType(msgRT.getH01FLGWK3());

						ses.setAttribute("userPO", userPO);
						ses.setAttribute("svBasic", msgRT);

						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (msgRT.getE01ACMACD().equals("06")) { // Cuotas de
																	// Participacion
																	// PCL01
						userPO.setOption("CP");
						userPO.setAccNum(msgRT.getE01ACMACC());
						userPO.setIdentifier(msgRT.getE01ACMACC());
						userPO.setApplicationCode(msgRT.getE01ACMACD());
						userPO.setBank(msgRT.getE01ACMBNK());
						userPO.setCusNum(msgRT.getE01ACMCUN());
						userPO.setHeader1(msgRT.getE01ACMPRO());
						userPO.setHeader2(msgRT.getE01ACMCUN());
						userPO.setHeader3(msgRT.getE01CUSNA1());
						userPO.setCurrency(msgRT.getE01ACMCCY());
						userPO.setCusType(msgRT.getH01FLGWK3());
						ses.setAttribute("userPO", userPO);
						ses.setAttribute("cpBasic", msgRT);
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_cp_basic.jsp");
							callPage(LangPath + "EDD0000_cp_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}

					} else {
						userPO.setOption("RT");
						userPO.setAccNum(msgRT.getE01ACMACC());
						userPO.setIdentifier(msgRT.getE01ACMACC());
						userPO.setApplicationCode(msgRT.getE01ACMACD());
						userPO.setBank(msgRT.getE01ACMBNK());
						userPO.setCusNum(msgRT.getE01ACMCUN());
						userPO.setHeader1(msgRT.getE01ACMPRO());
						userPO.setHeader2(msgRT.getE01ACMCUN());
						userPO.setHeader3(msgRT.getE01CUSNA1());
						userPO.setCurrency(msgRT.getE01ACMCCY());

						ses.setAttribute("userPO", userPO);
						ses.setAttribute("rtBasic", msgRT);

						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else { // There are errors
					if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_enter_maint.jsp");
							callPage(LangPath + "EDD0000_sv_enter_maint.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else { // Cuotas de Participacion
						if (userPO.getOption().equals("CP")) {
							try {
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_cp_enter_maint.jsp");
								callPage(LangPath
										+ "EDD0000_cp_enter_maint.jsp", req,
										res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						} else {
							try {
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_enter_maint.jsp");
								callPage(LangPath
										+ "EDD0000_rt_enter_maint.jsp", req,
										res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionEnterPrint(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD009002Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {

			msgRT = (EDD009002Message) mc.getMessageRecord("EDD009002");
			msgRT.setH02USERID(user.getH01USR());
			msgRT.setH02PROGRM("EDD0000");
			msgRT.setH02TIMSYS(getTimeStamp());
			msgRT.setH02SCRCOD("01");
			msgRT.setH02OPECOD("0002");
			try {
				msgRT.setE02ACMACC(req.getParameter("E02ACMACC"));
			} catch (Exception e) {
				msgRT.setE02ACMACC("0");
			}

			msgRT.send();
			msgRT.destroy();
			flexLog("EDD009002 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD009002")) {
				try {
					msgRT = new datapro.eibs.beans.EDD009002Message();
					flexLog("EDD009002 Message Sent");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD009002Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				if (IsNotError) { // There are no errors
					userPO.setIdentifier(msgRT.getE02ACMACC());

					ses.setAttribute("userPO", userPO);
					ses.setAttribute("rtFinish", msgRT);
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_finish.jsp");
						callPage(LangPath + "EDD0000_rt_finish.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_enter_print.jsp");
							callPage(LangPath + "EDD0000_rt_enter_print.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else { // Cuotas de Participacion
						if (userPO.getOption().equals("CP")) {
							try {
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_cp_enter_print.jsp");
								callPage(LangPath
										+ "EDD0000_cp_enter_print.jsp", req,
										res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						} else {
							try {
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_enter_print.jsp");
								callPage(LangPath
										+ "EDD0000_sv_enter_print.jsp", req,
										res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procActionEnterSVMaint(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("04");
			msgRT.setH01FLGWK1("S");
			msgRT.setH01OPECOD("0002");
			try {
				msgRT.setE01ACMACC(req.getParameter("E01ACMACC"));
			} catch (Exception e) {
				msgRT.setE01ACMACC("0");
			}
			flexLog("Enviado... = " + msgRT);
			msgRT.send();
			msgRT.destroy();
			flexLog("EDD000001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
					flexLog("EDD000001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;
				flexLog("Recibido... = " + msgRT);
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				if (IsNotError) { // There are no errors
					userPO.setOption("SV");
					userPO.setAccNum(msgRT.getE01ACMACC());
					userPO.setIdentifier(msgRT.getE01ACMACC());
					userPO.setApplicationCode(msgRT.getE01ACMACD());
					userPO.setBank(msgRT.getE01ACMBNK());
					userPO.setCusNum(msgRT.getE01ACMCUN());
					userPO.setHeader1(msgRT.getE01ACMPRO());
					userPO.setHeader2(msgRT.getE01ACMCUN());
					userPO.setHeader3(msgRT.getE01CUSNA1());
					userPO.setCurrency(msgRT.getE01ACMCCY());

					ses.setAttribute("userPO", userPO);
					ses.setAttribute("svBasic", msgRT);

					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_sv_basic.jsp");
						callPage(LangPath + "EDD0000_sv_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_enter_maint.jsp");
							callPage(LangPath + "EDD0000_sv_enter_maint.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * Cuentas de Cuotas de Participacion by Patricia Cataldo on 12/01/2011
	 */
	protected void procActionEnterCPMaint(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("06");
			msgRT.setH01FLGWK1("S");
			msgRT.setH01OPECOD("0002");
			try {
				msgRT.setE01ACMACC(req.getParameter("E01ACMACC"));
			} catch (Exception e) {
				msgRT.setE01ACMACC("0");
			}
			flexLog("Enviado... = " + msgRT);
			msgRT.send();
			msgRT.destroy();
			flexLog("EDD000001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
					flexLog("EDD000001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				if (IsNotError) { // There are no errors
					userPO.setOption("CP");
					userPO.setAccNum(msgRT.getE01ACMACC());
					userPO.setIdentifier(msgRT.getE01ACMACC());
					userPO.setApplicationCode(msgRT.getE01ACMACD());
					userPO.setBank(msgRT.getE01ACMBNK());
					userPO.setCusNum(msgRT.getE01ACMCUN());
					userPO.setHeader1(msgRT.getE01ACMPRO());
					userPO.setHeader2(msgRT.getE01ACMCUN());
					userPO.setHeader3(msgRT.getE01CUSNA1());
					userPO.setCurrency(msgRT.getE01ACMCCY());

					ses.setAttribute("userPO", userPO);
					ses.setAttribute("cpBasic", msgRT);

					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_cp_basic.jsp");
						callPage(LangPath + "EDD0000_cp_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("CP")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_cp_enter_maint.jsp");
							callPage(LangPath + "EDD0000_cp_enter_maint.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionFinish(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		MessageRecord newmessage = null;
		EDD009002Message msgCD = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgCD = (EDD009002Message) ses.getAttribute("cdFinish");
			msgCD = (EDD009002Message) mc.getMessageRecord("EDD009002");
			msgCD.setH02USERID(user.getH01USR());
			msgCD.setH02PROGRM("EDD0000");
			msgCD.setH02TIMSYS(getTimeStamp());
			msgCD.setH02SCRCOD("01");
			msgCD.setH02OPECOD("0005");
			try {
				msgCD.setE02ACMACC(userPO.getIdentifier());
			} catch (Exception e) {
				flexLog("E01CUMACC");
			}

			mc.sendMessage(msgCD);
			msgCD.destroy();
			flexLog("EDD009002 Message Sent");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("EDD009002")) {
				try {
					msgCD = new datapro.eibs.beans.EDD009002Message();
					flexLog("EDD009002 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
				msgCD = (EDD009002Message) newmessage;

				userPO.setIdentifier(msgCD.getE02ACMACC());
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtFinish", msgCD);
				ses.setAttribute("userPO", userPO);
				if (IsNotError) { // There are no errors
					try {

						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_enter_maint.jsp");
						callPage(LangPath + "EDD0000_rt_enter_maint.jsp", req,
								res);

					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_finish.jsp");
						callPage(LangPath + "EDD0000_rt_finish.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionFirm(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000004Message msgBene = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String country = null;

		country = req.getParameter("COUNTRY");

		// Send Initial data
		try {
			msgBene = (ESD000004Message) ses.getAttribute("rtFirm");
			msgBene.setH04USR(user.getH01USR());
			msgBene.setH04PGM("EDD0000");
			msgBene.setH04TIM(getTimeStamp());
			msgBene.setH04SCR("01");
			msgBene.setH04OPE("0005");
			msgBene.setE04RTP("S");
			msgBene.setE04CUN(userPO.getIdentifier());

			// all the fields here
			java.util.Enumeration enu = msgBene.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			flexLog("JSEDD0000:procActionFirm, ESD000004 Message Sent: "
					+ msgBene.toString());
			mc.sendMessage(msgBene);
			msgBene.destroy();

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000004")) {
				try {
					msgBene = new datapro.eibs.beans.ESD000004Message();
					flexLog("ESD000004 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgBene = (ESD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtFirm", msgBene);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else { // There are errors
					if (country.equals("PA")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_firm_pa.jsp");
							callPage(LangPath + "EDD0000_rt_firm_pa.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (country.equals("VE")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_firm_ve.jsp");
							callPage(LangPath + "EDD0000_rt_firm_ve.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_firm_generic.jsp");
							callPage(LangPath + "EDD0000_rt_firm_generic.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procActionPrintNew(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD("0002");
			if (userPO.getOption().equals("SV")) {
				msgRT.setH01SCRCOD("04");
			} else if (userPO.getOption().equals("CP")) {
				msgRT.setH01SCRCOD("06");
			}
			try {
				msgRT.setE01ACMACC(userPO.getIdentifier());
			} catch (Exception e) {
				msgRT.setE01ACMACC("0");
			}
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				if (IsNotError) { // There are no errors
					if (msgRT.getE01ACMACD().equals("04")) {
						userPO.setOption("SV");
						userPO.setAccNum(msgRT.getE01ACMACC());
						userPO.setIdentifier(msgRT.getE01ACMACC());
						userPO.setApplicationCode(msgRT.getE01ACMACD());
						userPO.setBank(msgRT.getE01ACMBNK());
						userPO.setCusNum(msgRT.getE01ACMCUN());
						userPO.setHeader1(msgRT.getE01ACMPRO());
						userPO.setHeader2(msgRT.getE01ACMCUN());
						userPO.setHeader3(msgRT.getE01CUSNA1());
						userPO.setCurrency(msgRT.getE01ACMCCY());

						ses.setAttribute("userPO", userPO);
						ses.setAttribute("svBasic", msgRT);

						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (msgRT.getE01ACMACD().equals("06")) {
						userPO.setOption("CP");
						userPO.setAccNum(msgRT.getE01ACMACC());
						userPO.setIdentifier(msgRT.getE01ACMACC());
						userPO.setBank(msgRT.getE01ACMBNK());
						userPO.setCusNum(msgRT.getE01ACMCUN());
						userPO.setHeader1(msgRT.getE01ACMPRO());
						userPO.setHeader2(msgRT.getE01ACMCUN());
						userPO.setHeader3(msgRT.getE01CUSNA1());
						userPO.setCurrency(msgRT.getE01ACMCCY());

						ses.setAttribute("userPO", userPO);
						ses.setAttribute("cpBasic", msgRT);

						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_cp_basic.jsp");
							callPage(LangPath + "EDD0000_cp_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						
						userPO.setOption("RT");
						userPO.setAccNum(msgRT.getE01ACMACC());
						userPO.setIdentifier(msgRT.getE01ACMACC());
						userPO.setBank(msgRT.getE01ACMBNK());
						userPO.setCusNum(msgRT.getE01ACMCUN());
						userPO.setHeader1(msgRT.getE01ACMPRO());
						userPO.setHeader2(msgRT.getE01ACMCUN());
						userPO.setHeader3(msgRT.getE01CUSNA1());
						userPO.setCurrency(msgRT.getE01ACMCCY());

						ses.setAttribute("userPO", userPO);
						ses.setAttribute("rtBasic", msgRT);

						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}

					}
				} else { // There are errors
					if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_enter_maint.jsp");
							callPage(LangPath + "EDD0000_sv_enter_maint.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("CP")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_cp_enter_maint.jsp");
							callPage(LangPath + "EDD0000_cp_enter_maint.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_enter_maint.jsp");
							callPage(LangPath + "EDD0000_rt_enter_maint.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procActionRTBasic(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		EFT000015Message msgFinish = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (EDD000001Message) ses.getAttribute("rtBasic");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD("0005");
			try {
				if (req.getParameter("APPROVAL").equals("Y"))
					msgRT.setH01OPECOD("0006");
			} catch (Exception e) {
			}
			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("EDD000001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EFT000015")) {
				try {
					msgFinish = new datapro.eibs.beans.EFT000015Message();
					flexLog("EFT000015 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgFinish = (EFT000015Message) newmessage;
				userPO.setIdentifier(msgFinish.getE15ACCNUM());
				userPO.setHeader2(msgFinish.getE15CUSCUN());
				userPO.setHeader3(msgFinish.getE15CUSNA1());
				userPO.setCurrency(msgFinish.getE15CCYCDE());
				userPO.setHeader1(msgFinish.getE15PROCDE());

				flexLog("Putting java beans into the session");
				ses.setAttribute("userPO", userPO);
				try {
					ses.setAttribute("error", msgError);
					ses.setAttribute("rtFinish", msgFinish);
					flexLog("About to call Page1: " + LangPath
							+ "EDD0000_rt_confirm.jsp");
					callPage(LangPath + "EDD0000_rt_confirm.jsp", req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			} else if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
					flexLog("EDD000001 Message Sent");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;

				if (userPO.getPurpose().equals("NEW")) {
					userPO.setIdentifier(msgRT.getE01ACMPRO());
					userPO.setApplicationCode(msgRT.getE01ACMACD());
					userPO.setHeader1(msgRT.getE01ACMCUN());
					userPO.setHeader2(msgRT.getE01CUSNA1());
					userPO.setCurrency(msgRT.getE01ACMCCY());
				}

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtBasic", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) {
					if (userPO.getPurpose().equals("MAINTENANCE")) {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_rt_enter_maint.jsp");
							callPage(LangPath + "EDD0000_rt_enter_maint.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getPurpose().equals("NEW")) {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "background.jsp");
							res.sendRedirect(super.srctx
									+ "/pages/background.jsp");
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						flexLog("Error Unknown");
					}
				} else {

					if (userPO.getPurpose().equals("NEW")) {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_rt_new.jsp");
							callPage(LangPath + "EDD0000_rt_new.jsp", req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionRTBene(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000004Message msgBene = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgBene = (ESD000004Message) ses.getAttribute("bene");
			msgBene.setH04USR(user.getH01USR());
			msgBene.setH04PGM("EDD0000");
			msgBene.setH04TIM(""); 
			msgBene.setH04SCR("01");
			msgBene.setH04OPE("0005");
			msgBene.setE04CUN(userPO.getIdentifier());
			msgBene.setE04RTP("J");

			// all the fields here
			java.util.Enumeration enu = msgBene.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgBene);
			msgBene.destroy();
			flexLog("ESD000004 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
				if (!IsNotError)
					userPO.setAccOpt("LEGALREP");
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000004")) {
				try {
					msgBene = new datapro.eibs.beans.ESD000004Message();
					flexLog("ESD000004 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgBene = (ESD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("bene", msgBene);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_bene.jsp");
						callPage(LangPath + "EDD0000_rt_bene.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionRTLegalRep(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ESD000004Message msgBene = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		String type = "I";
		type = userPO.getHeader10();

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgBene = (ESD000004Message) ses.getAttribute("legalRep");
			msgBene.setH04USR(user.getH01USR());
			msgBene.setH04PGM("EDD0000");
			msgBene.setH04TIM(""); 
			msgBene.setH04SCR("01");
			msgBene.setH04OPE("0005");
			msgBene.setE04CUN(userPO.getIdentifier());
			msgBene.setE04RTP(type);

			// all the fields here
			java.util.Enumeration enu = msgBene.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgBene);
			msgBene.destroy();
			flexLog("ESD000004 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
				if (!IsNotError)
					userPO.setAccOpt("LEGALREP");
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000004")) {
				try {
					msgBene = new datapro.eibs.beans.ESD000004Message();
					flexLog("ESD000004 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgBene = (ESD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("legalRep", msgBene);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_legal_rep.jsp");
						callPage(LangPath + "EDD0000_rt_legal_rep.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionRTCredit(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000004Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (EDD000004Message) ses.getAttribute("rtCredit");
			msgRT.setH04USERID(user.getH01USR());
			msgRT.setH04PROGRM("EDD0000");
			msgRT.setH04TIMSYS(getTimeStamp());
			msgRT.setH04SCRCOD("01");
			msgRT.setH04OPECOD("0005");

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("EDD000004 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000004")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000004Message();
					flexLog("EDD000004 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000004Message) newmessage;

				userPO.setIdentifier(msgRT.getE04ACMACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtCredit", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_credit.jsp");
						callPage(LangPath + "EDD0000_rt_credit.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionRTCreditLine(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000004Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (EDD000004Message) ses.getAttribute("rtCredit");
			msgRT.setH04USERID(user.getH01USR());
			msgRT.setH04PROGRM("EDD0000");
			msgRT.setH04TIMSYS(getTimeStamp());
			msgRT.setH04SCRCOD("01");
			msgRT.setH04OPECOD("0005");

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("EDD000004 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000004")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000004Message();
					flexLog("EDD000004 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000004Message) newmessage;

				userPO.setIdentifier(msgRT.getE04ACMACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtCredit", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors

					try {
						{
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_enter_lineacredito.jsp");
							callPage(LangPath
									+ "EDD0000_rt_enter_lineacredito.jsp", req,
									res);
						}
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_credit_line.jsp");
						callPage(LangPath + "EDD0000_rt_credit_line.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procActionRTEnterNew(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {
		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD("0001");
			try {
				msgRT.setE01ACMPRO(req.getParameter("E01ACMPRO"));
			} catch (Exception e) {
				msgRT.setE01ACMPRO("");
			}
			try {
				msgRT.setE01ACMACC(req.getParameter("E01ACMACC"));
			} catch (Exception e) {
			}

			try {
				msgRT.setE01OFFAC1(req.getParameter("E01OFFAC1"));
			} catch (Exception e) {
			}

			try {
				msgRT.setE01OFFAM1(req.getParameter("E01ACMAMT"));
				msgRT.setH01FLGWK3("T");
			} catch (Exception e) {
			}

			msgRT.send();
			msgRT.destroy();
			flexLog("EDD000001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
					flexLog("EDD000001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
				msgRT = (EDD000001Message) newmessage;
				userPO.setProdCode(msgRT.getE01ACMPRO());
				userPO.setApplicationCode(msgRT.getE01ACMACD());
				userPO.setHeader1(msgRT.getE01ACMCUN());
				userPO.setHeader2(msgRT.getE01CUSNA1());
				userPO.setCurrency(msgRT.getE01ACMCCY());
				userPO.setHeader4("N");

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("rtBasic", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_new.jsp");
						callPage(LangPath + "EDD0000_rt_new.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_enter_new.jsp");

						String firstLink = super.webAppPath + LangPath
								+ "ESD0711_products_detail.jsp?appcode="
								+ req.getParameter("appcode").trim()
								+ "&typecode="
								+ req.getParameter("typecode").trim()
								+ "&generic="
								+ req.getParameter("generic").trim()
								+ "&title=" + req.getParameter("title").trim()
								+ "&bank=" + req.getParameter("bank").trim();
						res.setContentType("text/html");
						PrintWriter out = res.getWriter();
						printProdFrame(out, firstLink, LangPath);

					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionRTMoney(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ELD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (ELD000001Message) ses.getAttribute("rtMoney");
			msgRT.setH06USERID(user.getH01USR());
			msgRT.setH06PROGRM("EDD0000");
			msgRT.setH06TIMSYS(getTimeStamp());
			msgRT.setH06SCRCOD("01");
			msgRT.setH06OPECOD("0005");

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("ELD000001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELD000001")) {
				try {
					msgRT = new datapro.eibs.beans.ELD000001Message();
					flexLog("ELD000001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ELD000001Message) newmessage;

				userPO.setIdentifier(msgRT.getE06LDMACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtMoney", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_money.jsp");
						callPage(LangPath + "EDD0000_rt_money.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionAccountAnalysis(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000010Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (EDD000010Message) ses.getAttribute("accAnalysis");
			msgRT.setH10USERID(user.getH01USR());
			msgRT.setH10PROGRM("EDD000010");
			msgRT.setH10TIMSYS(getTimeStamp());
			msgRT.setH10SCRCOD("01");
			msgRT.setH10OPECOD("0005");

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("EDD000010 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000010")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000010Message();
					flexLog("EDD000010 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000010Message) newmessage;

				userPO.setIdentifier(msgRT.getE10ACMACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("accAnalysis", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_money.jsp");
						callPage(LangPath + "EDD0000_rt_account_analysis.jsp",
								req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	protected void procReqInqAccountAnalysis(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000010Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000010Message) mc.getMessageRecord("EDD000010");
			msgRT.setH10USERID(user.getH01USR());
			msgRT.setH10PROGRM("EDD0000");
			msgRT.setH10TIMSYS(getTimeStamp());
			msgRT.setH10SCRCOD("01");
			msgRT.setH10OPECOD(opCode);
			msgRT.setE10ACMACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000010")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000010Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000010Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("accAnalysis", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_inq_account_analysis.jsp");
						callPage(LangPath
								+ "EDD0000_rt_inq_account_analysis.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page4: " + LangPath
								+ "EDD0000_rt_inq_balances.jsp");
						callPage(LangPath + "EDD0000_rt_inq_balances.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionTellerMessages(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ESD009001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (ESD009001Message) ses.getAttribute("instructions");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD000010");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD("0005");
			msgRT.setE01SPIACD("R4");
			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("ESD009001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD009001")) {
				try {
					msgRT = new datapro.eibs.beans.ESD009001Message();
					flexLog("ESD009001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ESD009001Message) newmessage;

				userPO.setIdentifier(msgRT.getE01SPIACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("instructions", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_teller_instructions.jsp");
						callPage(LangPath
								+ "EDD0000_rt_teller_instructions.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionRTOverdraft(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000003Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (EDD000003Message) ses.getAttribute("rtOverdraft");
			msgRT.setH03USERID(user.getH01USR());
			msgRT.setH03PROGRM("EDD0000");
			msgRT.setH03TIMSYS(getTimeStamp());
			msgRT.setH03SCRCOD("01");
			msgRT.setH03OPECOD("0005");

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("EDD000003 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000003")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000003Message();
					flexLog("EDD000003 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000003Message) newmessage;

				userPO.setIdentifier(msgRT.getE03ACMACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtOverdraft", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_overdraft.jsp");
						callPage(LangPath + "EDD0000_rt_overdraft.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionRTOverdraftOpc(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000003Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (EDD000003Message) ses.getAttribute("rtOverdraft");
			msgRT.setH03USERID(user.getH01USR());
			msgRT.setH03PROGRM("EDD0000");
			msgRT.setH03TIMSYS(getTimeStamp());
			msgRT.setH03SCRCOD("01");
			msgRT.setH03OPECOD("0005");

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("EDD000003 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000003")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000003Message();
					flexLog("EDD000003 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000003Message) newmessage;

				userPO.setIdentifier(msgRT.getE03ACMACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtOverdraft", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors

					try {
						{
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_enter_sobregiro.jsp");
							callPage(LangPath
									+ "EDD0000_rt_enter_sobregiro.jsp", req,
									res);
						}
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_overdraft_opc.jsp");
						callPage(LangPath + "EDD0000_rt_overdraft_opc.jsp",
								req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionRTOvernight(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000005Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (EDD000005Message) ses.getAttribute("rtOvernight");
			msgRT.setH05USERID(user.getH01USR());
			msgRT.setH05PROGRM("EDD0000");
			msgRT.setH05TIMSYS(getTimeStamp());
			msgRT.setH05SCRCOD("01");
			msgRT.setH05OPECOD("0005");
			msgRT.setE05ACMACC(userPO.getIdentifier());

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("EDD000005 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000005")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000005Message();
					flexLog("EDD000005 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000005Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtOvernight", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_basic.jsp");
						callPage(LangPath + "EDD0000_rt_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_overnight.jsp");
						callPage(LangPath + "EDD0000_rt_overnight.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionStatus(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000002Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (EDD000002Message) ses.getAttribute("rtStatus");
			msgRT.setH02USERID(user.getH01USR());
			msgRT.setH02PROGRM("EDD0000");
			msgRT.setH02TIMSYS(getTimeStamp());
			msgRT.setH02SCRCOD("01");
			msgRT.setH02OPECOD("0005");

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("EDD000002 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000002")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000002Message();
					flexLog("EDD000002 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000002Message) newmessage;

				userPO.setIdentifier(msgRT.getE02ACMACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtStatus", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("CP")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_cp_basic.jsp");
								callPage(LangPath + "EDD0000_cp_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_enter_chg_sts.jsp");
						callPage(LangPath + "EDD0000_enter_chg_sts.jsp", req,
								res);
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_chg_status.jsp");
						callPage(LangPath + "EDD0000_chg_status.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionSpcInst(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000005Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (ESD000005Message) ses.getAttribute("rtInst");
			msgRT.setH05USR(user.getH01USR());
			msgRT.setH05PGM("EDD0000");
			msgRT.setH05TIM(""); 
			msgRT.setH05SCR("01");
			msgRT.setH05OPE("0005");
			msgRT.setE05ACC(userPO.getIdentifier());
			msgRT.setE05ACD(userPO.getApplicationCode());

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("ESD000005 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000005")) {
				try {
					msgRT = new datapro.eibs.beans.ESD000005Message();
					flexLog("ESD000005 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ESD000005Message) newmessage;

				userPO.setIdentifier(msgRT.getE05ACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtInst", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_special_inst.jsp");
							callPage(LangPath + "EDD0000_rt_special_inst.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_special_inst.jsp");
							callPage(LangPath + "EDD0000_sv_special_inst.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionSpecialCodes(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ESD000002Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (ESD000002Message) ses.getAttribute("rtCodes");
			msgRT.setH02USR(user.getH01USR());
			msgRT.setH02PGM("EDD0000");
			msgRT.setH02TIM(getTimeStamp());
			msgRT.setH02SCR("01");
			msgRT.setH02OPE("0005");

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("ESD000002 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000002")) {
				try {
					msgRT = new datapro.eibs.beans.ESD000002Message();
					flexLog("ESD000002 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ESD000002Message) newmessage;

				userPO.setIdentifier(msgRT.getE02ACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtCodes", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("CP")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_cp_basic.jsp");
								callPage(LangPath + "EDD0000_cp_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_codes.jsp");
						callPage(LangPath + "EDD0000_rt_codes.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procActionSVBasic(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		EFT000015Message msgFinish = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (EDD000001Message) ses.getAttribute("svBasic");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD("0005");
			try {
				if (req.getParameter("APPROVAL").equals("Y"))
					msgRT.setH01OPECOD("0006");
			} catch (Exception e) {
			}
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			flexLog("mensaje a enviar : " + msgRT);
			msgRT.destroy();
			flexLog("EDD000001 Message Sent");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("EFT000015")) {
				try {
					msgFinish = new datapro.eibs.beans.EFT000015Message();
					flexLog("EFT000015 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
				msgFinish = (EFT000015Message) newmessage;
				userPO.setIdentifier(msgFinish.getE15ACCNUM());
				flexLog("Putting java beans into the session");
				ses.setAttribute("userPO", userPO);
				try {
					ses.setAttribute("error", msgError);
					ses.setAttribute("rtFinish", msgFinish);
					flexLog("About to call Page1: " + LangPath
							+ "EDD0000_rt_confirm.jsp");
					callPage(LangPath + "EDD0000_rt_confirm.jsp", req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			} else if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
					flexLog("EDD000001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
				msgRT = (EDD000001Message) newmessage;

				userPO.setAccNum(msgRT.getE01ACMACC());
				userPO.setIdentifier(msgRT.getE01ACMACC());
				userPO.setApplicationCode(msgRT.getE01ACMACD());
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("svBasic", msgRT);
				ses.setAttribute("userPO", userPO);
				if (IsNotError) {
					if (userPO.getPurpose().equals("MAINTENANCE")) {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_sv_enter_maint.jsp");
							callPage(LangPath + "EDD0000_sv_enter_maint.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getPurpose().equals("NEW")) {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_sv_enter_new.jsp");
							res.sendRedirect(super.srctx
									+ "/pages/background.jsp");
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						flexLog("Error Unknown");
					}
				} else {
					if (userPO.getPurpose().equals("NEW")) {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_sv_new.jsp");
							callPage(LangPath + "EDD0000_sv_new.jsp", req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * Metodo creado para Cuotas de Participacion Patricia Cataldo L on
	 * 12/02/11.
	 */
	protected void procActionCPBasic(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		EFT000015Message msgFinish = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgRT = (EDD000001Message) ses.getAttribute("cpBasic");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD("0005");
			try {
				if (req.getParameter("APPROVAL").equals("Y"))
					msgRT.setH01OPECOD("0006");
			} catch (Exception e) {
			}
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("EDD000001 Message Sent");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("EFT000015")) {
				try {
					msgFinish = new datapro.eibs.beans.EFT000015Message();
					flexLog("EFT000015 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
				msgFinish = (EFT000015Message) newmessage;
				userPO.setIdentifier(msgFinish.getE15ACCNUM());
				flexLog("Putting java beans into the session");
				ses.setAttribute("userPO", userPO);
				try {
					ses.setAttribute("error", msgError);
					ses.setAttribute("rtFinish", msgFinish);
					flexLog("About to call Page1: " + LangPath
							+ "EDD0000_rt_confirm.jsp");
					callPage(LangPath + "EDD0000_rt_confirm.jsp", req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			} else if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
					flexLog("EDD000001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
				msgRT = (EDD000001Message) newmessage;

				userPO.setAccNum(msgRT.getE01ACMACC());
				userPO.setIdentifier(msgRT.getE01ACMACC());
				userPO.setApplicationCode(msgRT.getE01ACMACD());
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("cpBasic", msgRT);
				ses.setAttribute("userPO", userPO);
				if (IsNotError) {
					if (userPO.getPurpose().equals("MAINTENANCE")) {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_cp_enter_maint.jsp");
							callPage(LangPath + "EDD0000_cp_enter_maint.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getPurpose().equals("NEW")) {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_cp_enter_new.jsp");
							res.sendRedirect(super.srctx
									+ "/pages/background.jsp");
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						flexLog("Error Unknown");
					}
				} else {
					if (userPO.getPurpose().equals("NEW")) {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_cp_new.jsp");
							callPage(LangPath + "EDD0000_cp_new.jsp", req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						try {
							flexLog("About to call Page2: " + LangPath
									+ "EDD0000_cp_basic.jsp");
							callPage(LangPath + "EDD0000_cp_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procActionSVEnterNew(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {
		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Sending header");
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD("0001");
			try {
				if (req.getParameter("E01ACMPRO") != null)
					msgRT.setE01ACMPRO(req.getParameter("E01ACMPRO"));
			} catch (Exception e) {
			}
			try {
				if (req.getParameter("E01ACMACC") != null)
					msgRT.setE01ACMACC(req.getParameter("E01ACMACC"));
			} catch (Exception e) {
			}

			try {
				msgRT.setE01OFFAC1(req.getParameter("E01OFFAC1"));
			} catch (Exception e) {
			}

			try {
				msgRT.setE01OFFAM1(req.getParameter("E01ACMAMT"));
				msgRT.setH01FLGWK3("T");
			} catch (Exception e) {
			}

			msgRT.send();
			msgRT.destroy();
			flexLog("EDD000001 Message Sent");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
					flexLog("EDD000001 Message Sent");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
				msgRT = (EDD000001Message) newmessage;
				userPO.setProdCode(msgRT.getE01ACMPRO());
				userPO.setApplicationCode(msgRT.getE01ACMACD());
				userPO.setHeader1(msgRT.getE01ACMCUN());
				userPO.setHeader2(msgRT.getE01CUSNA1());
				userPO.setCurrency(msgRT.getE01ACMCCY());
				userPO.setHeader4("N");
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("svBasic", msgRT);
				ses.setAttribute("nombreCliente", (String) ses.getAttribute("nombreCliente"));
				if (IsNotError) { // There are no errors

					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_sv_new.jsp");
						callPage(LangPath + "EDD0000_sv_new.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_sv_enter_new.jsp");

						String firstLink = super.webAppPath + LangPath
								+ "ESD0711_products_detail.jsp?appcode="
								+ req.getParameter("appcode").trim()
								+ "&typecode="
								+ req.getParameter("typecode").trim()
								+ "&generic="
								+ req.getParameter("generic").trim()
								+ "&title=" + req.getParameter("title").trim()
								+ "&bank=" + req.getParameter("bank").trim();
						res.setContentType("text/html");
						PrintWriter out = res.getWriter();
						printProdFrame(out, firstLink, LangPath);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * Cuotas de Participacion Patricia Cataldo L. on 12/02/11
	 */
	protected void procActionCPEnterNew(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {
		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Sending header");
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD("0001");
			try {
				if (req.getParameter("E01ACMPRO") != null)
					msgRT.setE01ACMPRO(req.getParameter("E01ACMPRO"));
			} catch (Exception e) {
			}
			try {
				if (req.getParameter("E01ACMACC") != null)
					msgRT.setE01ACMACC(req.getParameter("E01ACMACC"));
			} catch (Exception e) {
			}

			try {
				msgRT.setE01OFFAC1(req.getParameter("E01OFFAC1"));
			} catch (Exception e) {
			}

			try {
				msgRT.setE01OFFAM1(req.getParameter("E01ACMAMT"));
				msgRT.setH01FLGWK3("T");
			} catch (Exception e) {
			}

			msgRT.send();
			msgRT.destroy();
			flexLog("EDD000001 Message Sent");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
					flexLog("EDD000001 Message Sent");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}
				msgRT = (EDD000001Message) newmessage;
				userPO.setProdCode(msgRT.getE01ACMPRO());
				userPO.setApplicationCode(msgRT.getE01ACMACD());
				userPO.setHeader1(msgRT.getE01ACMCUN());
				userPO.setHeader2(msgRT.getE01CUSNA1());
				userPO.setCurrency(msgRT.getE01ACMCCY());
				userPO.setHeader4("N");
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("cpBasic", msgRT);
				if (IsNotError) { // There are no errors

					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_cp_new.jsp");
						callPage(LangPath + "EDD0000_cp_new.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_cp_enter_new.jsp");
						String firstLink = super.webAppPath + LangPath
								+ "ESD0711_products_detail.jsp?appcode="
								+ req.getParameter("appcode").trim()
								+ "&typecode="
								+ req.getParameter("typecode").trim()
								+ "&generic="
								+ req.getParameter("generic").trim()
								+ "&title=" + req.getParameter("title").trim()
								+ "&bank=" + req.getParameter("bank").trim();
						res.setContentType("text/html");
						PrintWriter out = res.getWriter();
						printProdFrame(out, firstLink, LangPath);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionTit(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000006Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgRT = (ESD000006Message) mc.getMessageRecord("ESD000006");
			msgRT.setH06USR(user.getH01USR());
			msgRT.setH06PGM("EDD0000");
			msgRT.setH06TIM(getTimeStamp());
			msgRT.setH06SCR("01");
			msgRT.setH06OPE("0005");
			msgRT.setE06ACC(userPO.getIdentifier());
			msgRT.setE06RTP("H");

			// all the fields here
			java.util.Enumeration enu = msgRT.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			mc.sendMessage(msgRT);
			msgRT.destroy();
			flexLog("EDD000006 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000006")) {
				try {
					msgRT = new datapro.eibs.beans.ESD000006Message();
					flexLog("EDD000006 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ESD000006Message) newmessage;

				userPO.setIdentifier(msgRT.getE06ACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtTit", msgRT);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_rt_basic.jsp");
								callPage(LangPath + "EDD0000_rt_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							{
								flexLog("About to call Page: " + LangPath
										+ "EDD0000_sv_basic.jsp");
								callPage(LangPath + "EDD0000_sv_basic.jsp",
										req, res);
							}
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_tit.jsp");
						callPage(LangPath + "EDD0000_rt_tit.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqDayBal(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ELEERRMessage msgError = null;
		EDD009201Message msgSearch = null;
		EDD009201Message msgList = null;
		JBList beanList = null;
		UserPos userPO = null;

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		int type = 0;
		String num = "";
		int posi = 0;
		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgSearch = (EDD009201Message) mc.getMessageRecord("EDD009201");
			msgSearch.setH01USERID(user.getH01USR());
			msgSearch.setH01PROGRM("EDL0300");
			msgSearch.setH01TIMSYS(getTimeStamp());
			msgSearch.setH01SCRCOD("01");
			msgSearch.setH01OPECOD("0004");

			try {
				try {
					posi = Integer.parseInt(req.getParameter("Pos"));
				} catch (Exception e) {
					posi = 0;
					flexLog("E01NUMPOS");
				}

				try {
					msgSearch.setE01ACMACC(userPO.getIdentifier());
				} catch (Exception e) {
					flexLog("E01ACMACC");
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Input data error " + e);
			}

			msgSearch.send();
			msgSearch.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError = new datapro.eibs.beans.ELEERRMessage();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

			}

			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD009201")) {

				try {
					beanList = new datapro.eibs.beans.JBList();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				try {
					beanList.setSearchText(num);
					beanList.setSearchType(type + "");
				} catch (Exception e) {
					e.printStackTrace();
					beanList.setSearchText("A");
					beanList.setSearchType("3");
					flexLog("Input data error " + e);
				}

				boolean firstTime = true;
				String marker = "";
				String myFlag = "";
				String chk = "";

				while (true) {

					msgList = (EDD009201Message) newmessage;

					marker = msgList.getH01FLGMAS();

					if (marker.equals("*")) {
						beanList.setShowNext(false);
						break;
					} else {
						if (firstTime) {
							firstTime = false;
							chk = "checked";
						} else {
							chk = "";
						}
						StringBuffer myRow = null;
						myRow = new StringBuffer("<TR>");
						myRow.append("<TD NOWRAP ALIGN=\"CENTER\">"
								+ Util.formatDate(msgList.getE01BALDT1(),
										msgList.getE01BALDT2(), msgList
												.getE01BALDT3()) + "</TD>");
						myRow.append("<TD NOWRAP ALIGN=RIGHT>"
								+ Util.fcolorCCY(msgList.getE01GRSBAL())
								+ "</TD>");
						myRow.append("<TD NOWRAP ALIGN=RIGHT>"
								+ Util.fcolorCCY(msgList.getE01UNCOLL())
								+ "</TD>");
						myRow.append("<TD NOWRAP ALIGN=RIGHT>"
								+ Util.formatCell(msgList.getE01HOLDIN())
								+ "</TD>");
						myRow.append("<TD NOWRAP ALIGN=RIGHT>"
								+ Util.fcolorCCY(msgList.getE01DISPON())
								+ "</TD>");
						myRow.append("</TR>");
						beanList.addRow(myFlag, myRow.toString());

						if (marker.equals("+")) {
							beanList.setShowNext(true);
							break;
						}
					}

					newmessage = mc.receiveMessage();
				}

				flexLog("Putting java beans into the session");
				ses.setAttribute("cifList", beanList);

				try {
					flexLog("About to call Page: " + LangPath
							+ "EDD0092_rt_inq_day_bal.jsp");
					callPage(LangPath + "EDD0092_rt_inq_day_bal.jsp", req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqEnterPrint(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("RT");
			userPO.setPurpose("PRINT");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_rt_enter_print.jsp");
			callPage(LangPath + "EDD0000_rt_enter_print.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqFinish(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD009002Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD009002Message) mc.getMessageRecord("EDD009002");
			msgRT.setH02USERID(user.getH01USR());
			msgRT.setH02PROGRM("EDL0130");
			msgRT.setH02TIMSYS(getTimeStamp());
			msgRT.setH02SCRCOD("01");
			msgRT.setH02OPECOD(opCode);
			msgRT.setE02ACMACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD009002")) {
				try {
					msgRT = new datapro.eibs.beans.EDD009002Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD009002Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtFinish", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_finish.jsp");
						callPage(LangPath + "EDD0000_rt_finish.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page4: " + LangPath
								+ "EDD0000_rt_enter_print.jsp");
						callPage(LangPath + "EDD0000_rt_enter_print.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqFirm(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000004Message msgBene = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		userPO.setAccOpt("SIGNERS");

		String opCode = null;
		String country = null;

		opCode = "0002";
		country = req.getParameter("COUNTRY");

		// Send Initial data
		try {
			msgBene = (ESD000004Message) mc.getMessageRecord("ESD000004");
			msgBene.setH04USR(user.getH01USR());
			msgBene.setH04PGM("EDD0000");
			msgBene.setH04TIM(getTimeStamp());
			try {
				msgBene.setH04SCR(req.getParameter("H04SCR"));
			} catch (Exception e) {
				msgBene.setH04SCR("07");
			}
			msgBene.setH04OPE(opCode);
			msgBene.setE04CUN(userPO.getIdentifier());
			msgBene.setE04RTP("S");

			msgBene.send();
			msgBene.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000004")) {
				try {
					msgBene = new datapro.eibs.beans.ESD000004Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgBene = (ESD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtFirm", msgBene);

				if (IsNotError) { // There are no errors
					if (country.equals("PA")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_firm_pa.jsp");
							callPage(LangPath + "EDD0000_rt_firm_pa.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (country.equals("VE")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_firm_ve.jsp");
							callPage(LangPath + "EDD0000_rt_firm_ve.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_firm_generic.jsp");
							callPage(LangPath + "EDD0000_rt_firm_generic.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqInqTit(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000006Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (ESD000006Message) mc.getMessageRecord("ESD000006");
			msgRT.setH06USR(user.getH01USR());
			msgRT.setH06PGM("EDD0000");
			msgRT.setH06TIM(getTimeStamp());
			msgRT.setH06SCR("01");
			msgRT.setH06OPE(opCode);
			msgRT.setE06ACC(userPO.getIdentifier());
			msgRT.setE06RTP("H");
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000006")) {
				try {
					msgRT = new datapro.eibs.beans.ESD000006Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ESD000006Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtTit", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_inq_tit.jsp");
						callPage(LangPath + "EDD0000_rt_inq_tit.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page4: " + LangPath
								+ "EDD0000_rt_inq_balances.jsp");
						callPage(LangPath + "EDD0000_rt_inq_balances.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created by Orestes Garcia.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 */
	protected void procReqProductInfo(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD071103Message msgProdDDA = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String bank = userPO.getBank();
		String prodCode = userPO.getHeader1();

		String opCode = "0004";

		// Send Initial data
		try {
			msgProdDDA = (ESD071103Message) mc.getMessageRecord("ESD071103");
			msgProdDDA.setH03USERID(user.getH01USR());
			msgProdDDA.setH03PROGRM("ESD0711");
			msgProdDDA.setH03TIMSYS(getTimeStamp());
			msgProdDDA.setH03SCRCOD("01");
			msgProdDDA.setH03OPECOD(opCode);
			msgProdDDA.setE03APCCDE(prodCode);
			msgProdDDA.setE03APCBNK(bank);
			msgProdDDA.send();
			msgProdDDA.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD071103")) {
				try {
					msgProdDDA = new datapro.eibs.beans.ESD071103Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgProdDDA = (ESD071103Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("ddaProdInq", msgProdDDA);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "ESD0711_products_inq_dda.jsp");
						callPage(LangPath + "ESD0711_products_inq_dda.jsp",
								req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTBene(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000004Message msgBene = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		userPO.setAccOpt("BENE");

		String opCode = "";
		if (userPO.getPurpose().equals("NEW"))
			opCode = "0001";
		else
			opCode = "0002";

		// Send Initial data
		try {
			msgBene = (ESD000004Message) mc.getMessageRecord("ESD000004");
			msgBene.setH04USR(user.getH01USR());
			msgBene.setH04PGM("EDD0000");
			msgBene.setH04TIM(getTimeStamp());
			msgBene.setH04SCR("01");
			msgBene.setH04OPE(opCode);
			msgBene.setE04CUN(userPO.getIdentifier());
			msgBene.setE04RTP("J");
			msgBene.send();
			msgBene.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000004")) {
				try {
					msgBene = new datapro.eibs.beans.ESD000004Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgBene = (ESD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("bene", msgBene);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_bene.jsp");
						callPage(LangPath + "EDD0000_rt_bene.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTLegalRep(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000004Message msgBene = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		userPO.setAccOpt("LEGALREP");

		String type = "I";
		if (type == null)
			type = "";
		userPO.setHeader10(type);

		String opCode = "";
		if (userPO.getPurpose().equals("NEW"))
			opCode = "0001";
		else
			opCode = "0002";

		// Send Initial data
		try {
			msgBene = (ESD000004Message) mc.getMessageRecord("ESD000004");
			msgBene.setH04USR(user.getH01USR());
			msgBene.setH04PGM("EDD0000");
			msgBene.setH04TIM(getTimeStamp());
			msgBene.setH04SCR("01");
			msgBene.setH04OPE(opCode);
			msgBene.setE04CUN(userPO.getIdentifier());
			msgBene.setE04RTP(type);
			msgBene.send();
			msgBene.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000004")) {
				try {
					msgBene = new datapro.eibs.beans.ESD000004Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgBene = (ESD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("legalRep", msgBene);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_legal_rep.jsp");
						callPage(LangPath + "EDD0000_rt_legal_rep.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqInqRTBene(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000004Message msgBene = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		if (userPO.getPurpose().equals("NEW"))
			opCode = "0001";
		else
			opCode = "0002";

		// Send Initial data
		try {
			msgBene = (ESD000004Message) mc.getMessageRecord("ESD000004");
			msgBene.setH04USR(user.getH01USR());
			msgBene.setH04PGM("EDD0000");
			msgBene.setH04TIM(getTimeStamp());
			msgBene.setH04SCR("01");
			msgBene.setH04OPE(opCode);
			msgBene.setE04CUN(userPO.getIdentifier());
			msgBene.setE04RTP("J");
			msgBene.send();
			msgBene.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000004")) {
				try {
					msgBene = new datapro.eibs.beans.ESD000004Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgBene = (ESD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("bene", msgBene);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_inq_bene.jsp");
						callPage(LangPath + "EDD0000_rt_inq_bene.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_inq_basic.jsp");
							callPage(LangPath + "EDD0000_rt_inq_basic.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqInqRTLegalRep(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ESD000004Message msgBene = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String type = null;
		type = req.getParameter("Type");
		userPO.setHeader10(type);

		String opCode = null;
		if (userPO.getPurpose().equals("NEW"))
			opCode = "0001";
		else
			opCode = "0002";

		// Send Initial data
		try {
			msgBene = (ESD000004Message) mc.getMessageRecord("ESD000004");
			msgBene.setH04USR(user.getH01USR());
			msgBene.setH04PGM("EDD0000");
			msgBene.setH04TIM(getTimeStamp());
			msgBene.setH04SCR("01");
			msgBene.setH04OPE(opCode);
			msgBene.setE04CUN(userPO.getIdentifier());
			msgBene.setE04RTP(type);
			msgBene.send();
			msgBene.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000004")) {
				try {
					msgBene = new datapro.eibs.beans.ESD000004Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgBene = (ESD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("legalRep", msgBene);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_inq_legal_rep.jsp");
						callPage(LangPath + "EDD0000_rt_inq_legal_rep.jsp",
								req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_inq_basic.jsp");
							callPage(LangPath + "EDD0000_rt_inq_basic.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTCredit(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000004Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000004Message) mc.getMessageRecord("EDD000004");
			msgRT.setH04USERID(user.getH01USR());
			msgRT.setH04PROGRM("EDD0000");
			msgRT.setH04TIMSYS(getTimeStamp());
			msgRT.setH04SCRCOD("01");
			msgRT.setH04OPECOD(opCode);
			msgRT.setE04ACMACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000004")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000004Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtCredit", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_credit.jsp");
						callPage(LangPath + "EDD0000_rt_credit.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTCreditLine(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000004Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000004Message) mc.getMessageRecord("EDD000004");
			msgRT.setH04USERID(user.getH01USR());
			msgRT.setH04PROGRM("EDD0000");
			msgRT.setH04TIMSYS(getTimeStamp());
			msgRT.setH04SCRCOD("01");
			msgRT.setH04OPECOD(opCode);

			try {
				msgRT.setE04ACMACC(req.getParameter("E04ACMACC"));
			} catch (Exception e) {
				msgRT.setE04ACMACC("0");
			}

			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000004")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000004Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtCredit", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_credit_line.jsp");
						callPage(LangPath + "EDD0000_rt_credit_line.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors

					try {
						flexLog("About to call Page4: " + LangPath
								+ "EDD0000_rt_enter_lineacredito.jsp");
						callPage(
								LangPath + "EDD0000_rt_enter_lineacredito.jsp",
								req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTEnterInquiry(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("RT");
			userPO.setPurpose("INQUIRY");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_rt_enter_inquiry.jsp");
			callPage(LangPath + "EDD0000_rt_enter_inquiry.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTSobregiro(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_rt_enter_sobregiro.jsp");
			callPage(LangPath + "EDD0000_rt_enter_sobregiro.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTLineacredito(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_rt_enter_lineacredito.jsp");
			callPage(LangPath + "EDD0000_rt_enter_lineacredito.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procReqRTEnterMaint(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("RT");
			userPO.setPurpose("MAINTENANCE");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_rt_enter_maint.jsp");
			callPage(LangPath + "EDD0000_rt_enter_maint.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procReqRTEnterNew(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("RT");
			userPO.setPurpose("NEW");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_rt_enter_new.jsp");
			callPage(LangPath + "EDD0000_rt_enter_new.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTEnterPrint(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("RT");
			userPO.setPurpose("PRINT");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_rt_enter_print.jsp");
			callPage(LangPath + "EDD0000_rt_enter_print.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTMaintenance(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD(opCode);
			msgRT.setE01ACMACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtBasic", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_basic.jsp");
						callPage(LangPath + "EDD0000_rt_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page4: " + LangPath
								+ "EDD0000_rt_enter_maint.jsp");
						callPage(LangPath + "EDD0000_rt_enter_maint.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTMoney(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ELD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (ELD000001Message) mc.getMessageRecord("ELD000001");
			msgRT.setH06USERID(user.getH01USR());
			msgRT.setH06PROGRM("EDD0000");
			msgRT.setH06TIMSYS(getTimeStamp());
			msgRT.setH06SCRCOD("01");
			msgRT.setH06OPECOD(opCode);
			msgRT.setE06LDMACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELD000001")) {
				try {
					msgRT = new datapro.eibs.beans.ELD000001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ELD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtMoney", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_money.jsp");
						callPage(LangPath + "EDD0000_rt_money.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}

					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procReqSVNew(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0001";

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD(opCode);
			msgRT.setE01ACMPRO(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("svNew", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_sv_basic.jsp");
						callPage(LangPath + "EDD0000_sv_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: /servlet/datapro.eibs.products.JSESD0711?TYPE=04");
						res
								.sendRedirect(super.srctx
										+ "/servlet/datapro.eibs.products.JSESD0711?TYPE=04");
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqAccountAnalysis(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000010Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000010Message) mc.getMessageRecord("EDD000010");
			msgRT.setH10USERID(user.getH01USR());
			msgRT.setH10PROGRM("EDD0000");
			msgRT.setH10TIMSYS(getTimeStamp());
			msgRT.setH10SCRCOD("01");
			msgRT.setH10OPECOD(opCode);
			msgRT.setE10ACMACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000010")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000010Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000010Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("accAnalysis", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_account_analysis.jsp");
						callPage(LangPath + "EDD0000_rt_account_analysis.jsp",
								req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqTellerMessages(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ESD009001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (ESD009001Message) mc.getMessageRecord("ESD009001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("ESD009001");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD(opCode);
			msgRT.setE01SPIACC(userPO.getIdentifier());
			msgRT.setE01SPIACD("R4");
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD009001")) {
				try {
					msgRT = new datapro.eibs.beans.ESD009001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ESD009001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("instructions", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_teller_instructions.jsp");
						callPage(LangPath
								+ "EDD0000_rt_teller_instructions.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procReqRTNew(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgCD = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0001";

		// Send Initial data
		try {
			msgCD = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgCD.setH01USERID(user.getH01USR());
			msgCD.setH01PROGRM("EDD0000");
			msgCD.setH01TIMSYS(getTimeStamp());
			msgCD.setH01SCRCOD("01");
			msgCD.setH01OPECOD(opCode);
			msgCD.setE01ACMPRO(userPO.getIdentifier());
			msgCD.send();
			msgCD.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgCD = new datapro.eibs.beans.EDD000001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgCD = (EDD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtNew", msgCD);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_basic.jsp");
						callPage(LangPath + "EDD0000_rt_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: /servlet/datapro.eibs.products.JSESD0711?TYPE=RT");
						res
								.sendRedirect(super.srctx
										+ "/servlet/datapro.eibs.products.JSESD0711?TYPE=RT");
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTOverdraft(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000003Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000003Message) mc.getMessageRecord("EDD000003");
			msgRT.setH03USERID(user.getH01USR());
			msgRT.setH03PROGRM("EDD0000");
			msgRT.setH03TIMSYS(getTimeStamp());
			msgRT.setH03SCRCOD("01");
			msgRT.setH03OPECOD(opCode);
			msgRT.setE03ACMACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000003")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000003Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000003Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtOverdraft", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_overdraft.jsp");
						callPage(LangPath + "EDD0000_rt_overdraft.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTOverdraftOpc(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000003Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000003Message) mc.getMessageRecord("EDD000003");
			msgRT.setH03USERID(user.getH01USR());
			msgRT.setH03PROGRM("EDD0000");
			msgRT.setH03TIMSYS(getTimeStamp());
			msgRT.setH03SCRCOD("01");
			msgRT.setH03OPECOD(opCode);

			try {
				msgRT.setE03ACMACC(req.getParameter("E03ACMACC"));
			} catch (Exception e) {
				msgRT.setE03ACMACC("0");
			}

			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000003")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000003Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000003Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtOverdraft", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_overdraft_opc.jsp");
						callPage(LangPath + "EDD0000_rt_overdraft_opc.jsp",
								req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors

					try {
						flexLog("About to call Page4: " + LangPath
								+ "EDD0000_rt_enter_sobregiro.jsp");
						callPage(LangPath + "EDD0000_rt_enter_sobregiro.jsp",
								req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}

			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqRTOvernight(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000005Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000005Message) mc.getMessageRecord("EDD000005");
			msgRT.setH05USERID(user.getH01USR());
			msgRT.setH05PROGRM("EDD0000");
			msgRT.setH05TIMSYS(getTimeStamp());
			msgRT.setH05SCRCOD("01");
			msgRT.setH05OPECOD(opCode);
			msgRT.setE05ACMACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000005")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000005Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000005Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtOvernight", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_overnight.jsp");
						callPage(LangPath + "EDD0000_rt_overnight.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page4: " + LangPath
								+ "EDD0000_rt_basic.jsp");
						callPage(LangPath + "EDD0000_rt_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqStatus(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000002Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000002Message) mc.getMessageRecord("EDD000002");
			msgRT.setH02USERID(user.getH01USR());
			msgRT.setH02PROGRM("EDD0000");
			msgRT.setH02TIMSYS(getTimeStamp());
			msgRT.setH02SCRCOD("01");
			msgRT.setH02OPECOD(opCode);
			String acc = userPO.getIdentifier();
			acc = ((acc == null || "".equals(acc.trim())) ? req
					.getParameter("E01ACMACC") : acc);
			msgRT.setE02ACMACC(acc);
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000002")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000002Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000002Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtStatus", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_chg_status.jsp");
						callPage(LangPath + "EDD0000_chg_status.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					flexLog("About to call Page: " + LangPath
							+ "EDD0000_enter_chg_sts.jsp");
					callPage(LangPath + "EDD0000_enter_chg_sts.jsp", req, res);
				}

			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqSpcInst(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000005Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = "";
		if (userPO.getPurpose().equals("MAINTENANCE"))
			opCode = "0002";
		else
			opCode = "0004";

		// Send Initial data
		try {
			msgRT = (ESD000005Message) mc.getMessageRecord("ESD000005");
			msgRT.setH05USR(user.getH01USR());
			msgRT.setH05PGM("EDD0000");
			msgRT.setH05TIM(getTimeStamp());
			msgRT.setH05SCR("01");
			msgRT.setH05OPE(opCode);
			msgRT.setE05ACC(userPO.getIdentifier());
			msgRT.setE05ACD(userPO.getApplicationCode());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000005")) {
				try {
					msgRT = new datapro.eibs.beans.ESD000005Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ESD000005Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtInst", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_rt_special_inst.jsp");
						callPage(LangPath + "EDD0000_rt_special_inst.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqSpecialCodes(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ESD000002Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = "";
		if (userPO.getPurpose().equals("MAINTENANCE"))
			opCode = "0002";
		else
			opCode = "0004";

		// Send Initial data
		try {
			msgRT = (ESD000002Message) mc.getMessageRecord("ESD000002");
			msgRT.setH02USR(user.getH01USR());
			msgRT.setH02PGM("EDD0000");
			msgRT.setH02TIM(getTimeStamp());
			msgRT.setH02SCR("01");
			msgRT.setH02OPE(opCode);
			msgRT.setE02ACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000002")) {
				try {
					msgRT = new datapro.eibs.beans.ESD000002Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ESD000002Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtCodes", msgRT);

				if (IsNotError) { // There are no errors
					try {
						if (userPO.getPurpose().equals("MAINTENANCE")) {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_codes.jsp");
							callPage(LangPath + "EDD0000_rt_codes.jsp", req,
									res);
						} else {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_codes.jsp");
							callPage(LangPath + "EDD0000_rt_codes.jsp", req,
									res);
						}
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("CP")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_cp_basic.jsp");
							callPage(LangPath + "EDD0000_cp_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqSVEnterInquiry(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("SV");
			userPO.setPurpose("INQUIRY");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_sv_enter_inquiry.jsp");
			callPage(LangPath + "EDD0000_sv_enter_inquiry.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * Cuotas de Participacion
	 */
	protected void procReqCPEnterInquiry(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("CP");
			userPO.setPurpose("INQUIRY");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_cp_enter_inquiry.jsp");
			callPage(LangPath + "EDD0000_cp_enter_inquiry.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */

	protected void procReqSVEnterMaint(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("SV");
			userPO.setPurpose("MAINTENANCE");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_sv_enter_maint.jsp");
			callPage(LangPath + "EDD0000_sv_enter_maint.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * Cuotas de Participacion by Patricia Cataldo L on 12/01/11
	 */
	protected void procReqCPEnterMaint(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("CP");
			userPO.setPurpose("MAINTENANCE");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_cp_enter_maint.jsp");
			callPage(LangPath + "EDD0000_cp_enter_maint.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procReqSVEnterNew(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("SV");
			userPO.setPurpose("NEW");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_sv_enter_new.jsp");
			callPage(LangPath + "EDD0000_sv_enter_new.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * Metodo para llamar a la pagina inicial de busqueda de cuentas para cambio
	 * status
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqSVEnterStatus(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_enter_chg_sts.jsp");
			callPage(LangPath + "EDD0000_enter_chg_sts.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * Metodo Creado para Cuotas de Participacion by Patricia Cataldo L on
	 * 12/01/11
	 */
	protected void procReqCPEnterNew(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("CP");
			userPO.setPurpose("NEW");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_cp_enter_new.jsp");
			callPage(LangPath + "EDD0000_cp_enter_new.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqSVEnterPrint(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("SV");
			userPO.setPurpose("PRINT");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_sv_enter_print.jsp");
			callPage(LangPath + "EDD0000_sv_enter_print.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}
	}

	/**
	 * Metodo Creado para Cuotas de Participacion
	 * 
	 */
	protected void procReqCPEnterPrint(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("CP");
			userPO.setPurpose("PRINT");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDD0000_cp_enter_print.jsp");
			callPage(LangPath + "EDD0000_cp_enter_print.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqSVMaintenance(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("04");
			msgRT.setH01OPECOD(opCode);
			msgRT.setE01ACMACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("svBasic", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_sv_basic.jsp");
						callPage(LangPath + "EDD0000_sv_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page4: " + LangPath
								+ "EDD0000_sv_enter_maint.jsp");
						callPage(LangPath + "EDD0000_sv_enter_maint.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * Metodo Creado Para Cuotas de Participacion Por Patricia Cataldo en
	 * 12/02/11
	 */
	protected void procReqCPMaintenance(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("06");
			msgRT.setH01OPECOD(opCode);
			msgRT.setE01ACMACC(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("cpBasic", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_cp_basic.jsp");
						callPage(LangPath + "EDD0000_cp_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page4: " + LangPath
								+ "EDD0000_cp_enter_maint.jsp");
						callPage(LangPath + "EDD0000_cp_enter_maint.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * Metodo para cuotas de Particiapcion PCL01 by Patricia Cataldo L. on
	 * 12/02/11.
	 */
	protected void procReqCPNew(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0001";

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01OPECOD(opCode);
			msgRT.setE01ACMPRO(userPO.getIdentifier());
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("cpNew", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_cp_basic.jsp");
						callPage(LangPath + "EDD0000_cp_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: /servlet/datapro.eibs.products.JSESD0711?TYPE=06");
						res
								.sendRedirect(super.srctx
										+ "/servlet/datapro.eibs.products.JSESD0711?TYPE=06");
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqTit(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000006Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgRT = (ESD000006Message) mc.getMessageRecord("ESD000006");
			msgRT.setH06USR(user.getH01USR());
			msgRT.setH06PGM("EDD0000");
			msgRT.setH06TIM(getTimeStamp());
			msgRT.setH06SCR("01");
			msgRT.setH06OPE(opCode);
			msgRT.setE06ACC(userPO.getIdentifier());
			msgRT.setE06RTP("H");
			msgRT.send();
			msgRT.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000006")) {
				try {
					msgRT = new datapro.eibs.beans.ESD000006Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (ESD000006Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("rtTit", msgRT);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_rt_tit.jsp");
						callPage(LangPath + "EDD0000_rt_tit.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page4: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * Ingreso del Saldos Promedios traspasados desde Otras Intituciones Ahorro
	 * Vivienda P.Cataldo L.
	 */
	protected void procReqSaldosAhorroVivienda(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD210001Message msgSV = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgSV = (EDD210001Message) mc.getMessageRecord("EDD210001");
			msgSV.setH01USERID(user.getH01USR());
			msgSV.setH01PROGRM("EDD2100");
			msgSV.setH01TIMSYS(getTimeStamp());
			msgSV.setH01SCRCOD("01");
			msgSV.setH01OPECOD("0001");
			msgSV.setE01AVPACC(userPO.getIdentifier());
			flexLog("mensaje enviado..." + msgSV);
			msgSV.send();
			msgSV.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD210001")) {
				try {
					msgSV = new datapro.eibs.beans.EDD210001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgSV = (EDD210001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("svSaldos", msgSV);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page3: " + LangPath
								+ "EDD0000_sv_saldos_prom.jsp");
						callPage(LangPath + "EDD0000_sv_saldos_prom.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page4: " + LangPath
								+ "EDD0000_sv_basic.jsp");
						callPage(LangPath + "EDD0000_sv_basic.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * Ingreso del Saldos Promedios traspasados desde Otras Intituciones Ahorro
	 * Vivienda P.Cataldo L.
	 */
	protected void procActionSaldosAhorroVivienda(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD210001Message msgSV = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgSV = (EDD210001Message) mc.getMessageRecord("EDD210001");
			msgSV.setH01USERID(user.getH01USR());
			msgSV.setH01PROGRM("EDD2100");
			msgSV.setH01TIMSYS(getTimeStamp());
			msgSV.setH01SCRCOD("01");
			msgSV.setH01OPECOD("0002");
			msgSV.setE01AVPACC(userPO.getIdentifier());

			// all the fields here
			java.util.Enumeration enu = msgSV.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			flexLog("Mensaje enviado ..." + msgSV);
			mc.sendMessage(msgSV);
			msgSV.destroy();
			flexLog("EDD210001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD210001")) {
				try {
					msgSV = new datapro.eibs.beans.EDD210001Message();

				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgSV = (EDD210001Message) newmessage;
				flexLog("EDD210001 Message Received" + msgSV);

				userPO.setIdentifier(msgSV.getE01AVPACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("svSaldos", msgSV);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_sv_saldos_prom.jsp");
						callPage(LangPath + "EDD0000_sv_saldos_prom.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath
								+ "EDD0000_sv_saldos_prom.jsp");
						callPage(LangPath + "EDD0000_sv_saldos_prom.jsp", req,
								res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	public void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		Socket s = null;
		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			int screen = R_RT_ENTER_MAINT;

			try {

				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session
						.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					s = new Socket(super.hostIP, getInitSocket(req) + 4);
					s.setSoTimeout(super.sckTimeOut);
					mc = new MessageContext(new DataInputStream(
							new BufferedInputStream(s.getInputStream())),
							new DataOutputStream(new BufferedOutputStream(s
									.getOutputStream())), "datapro.eibs.beans");

					try {
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					} catch (Exception e) {
						flexLog("Screen set to default value");
					}
					flexLog("Entre con Screen...= " + screen);
					switch (screen) {
					// BEGIN RETAIL ACCOUNT
					// Request
					case R_RT_NEW:
						procReqRTNew(mc, msgUser, req, res, session);
						break;
					case R_RT_MAINTENANCE:
						procReqRTMaintenance(mc, msgUser, req, res, session);
						break;
					case R_RT_OVERDRAFT:
						procReqRTOverdraft(mc, msgUser, req, res, session);
						break;
					case R_RT_OVERDRAFT_OPC:
						procReqRTOverdraftOpc(mc, msgUser, req, res, session);
						break;
					case R_RT_OVERNIGHT:
						procReqRTOvernight(mc, msgUser, req, res, session);
						break;
					case R_RT_LIN_CRED:
						procReqRTCredit(mc, msgUser, req, res, session);
						break;
					case R_RT_LINEACREDITO:
						procReqRTCreditLine(mc, msgUser, req, res, session);
						break;
					case R_RT_MONEY:
						procReqRTMoney(mc, msgUser, req, res, session);
						break;
					case R_RT_STATUS:
						procReqStatus(mc, msgUser, req, res, session);
						break;
					case R_RT_CODES:
						procReqSpecialCodes(mc, msgUser, req, res, session);
						break;
					case R_ACCOUNT_TITLE:
						procReqAccountTitle(mc, msgUser, req, res, session);
						break;
					case R_RT_TITULARES:
						procReqTit(mc, msgUser, req, res, session);
						break;
					case R_RT_FIRMANTES:
						procReqFirm(mc, msgUser, req, res, session);
						break;
					case R_RT_SPECIAL_INST:
						procReqSpcInst(mc, msgUser, req, res, session);
						break;
					case R_RT_BENEFICIARIES:
						procReqRTBene(mc, msgUser, req, res, session);
						break;
					case R_RT_LEGAL_REP:
						procReqRTLegalRep(mc, msgUser, req, res, session);
						break;
					case R_INQ_BENE:
						procReqInqRTBene(mc, msgUser, req, res, session);
						break;
					case R_INQ_LEGAL_REP:
						procReqInqRTLegalRep(mc, msgUser, req, res, session);
						break;
					case R_RT_ACC_ANALYSIS:
						procReqAccountAnalysis(mc, msgUser, req, res, session);
						break;
					case R_RT_ACC_TELLER_MSG:
						procReqTellerMessages(mc, msgUser, req, res, session);
						break;
					// Savings
					case R_SV_NEW:
						procReqSVNew(mc, msgUser, req, res, session);
						break;
					case R_SV_MAINTENANCE:
						procReqSVMaintenance(mc, msgUser, req, res, session);
						break;
					// Cuotas de Participacion PCL01
					case R_CP_NEW:
						procReqCPNew(mc, msgUser, req, res, session);
						break;
					case R_CP_MAINTENANCE:
						procReqCPMaintenance(mc, msgUser, req, res, session);
						break;

					// Action
					case A_RT_BASIC:
						procActionRTBasic(mc, msgUser, req, res, session);
						break;
					case A_RT_OVERDRAFT:
						procActionRTOverdraft(mc, msgUser, req, res, session);
						break;
					case A_RT_OVERDRAFT_OPC:
						procActionRTOverdraftOpc(mc, msgUser, req, res, session);
						break;
					case A_RT_OVERNIGHT:
						procActionRTOvernight(mc, msgUser, req, res, session);
						break;
					case A_RT_LIN_CRED:
						procActionRTCredit(mc, msgUser, req, res, session);
						break;
					case A_RT_LINEACREDITO:
						procActionRTCreditLine(mc, msgUser, req, res, session);
						break;
					case A_RT_MONEY:
						procActionRTMoney(mc, msgUser, req, res, session);
						break;
					case A_RT_STATUS:
						procActionStatus(mc, msgUser, req, res, session);
						break;
					case A_ACCOUNT_TITLE:
						procActionAccountTitle(mc, msgUser, req, res, session);
						break;
					case A_RT_CODES:
						procActionSpecialCodes(mc, msgUser, req, res, session);
						break;
					case A_RT_TITULARES:
						procActionTit(mc, msgUser, req, res, session);
						break;
					case A_RT_FIRMANTES:
						procActionFirm(mc, msgUser, req, res, session);
						break;
					case A_RT_SPECIAL_INST:
						procActionSpcInst(mc, msgUser, req, res, session);
						break;
					case A_RT_BENEFICIARIES:
						procActionRTBene(mc, msgUser, req, res, session);
						break;
					case A_RT_LEGAL_REP:
						procActionRTLegalRep(mc, msgUser, req, res, session);
						break;
					case A_RT_PRINT_NEW:
						procActionPrintNew(mc, msgUser, req, res, session);
						break;
					case A_RT_ACC_ANALYSIS:
						procActionAccountAnalysis(mc, msgUser, req, res,
								session);
						break;
					case A_RT_ACC_TELLER_MSG:
						procActionTellerMessages(mc, msgUser, req, res, session);
						break;

					// Savings
					case A_SV_BASIC:
						procActionSVBasic(mc, msgUser, req, res, session);
						break;

					// END RETAIL ACCOUNT
					// BEGIN EnterinG
					// Request
					// Cuotas de Participacion
					case A_CP_BASIC:
						procActionCPBasic(mc, msgUser, req, res, session);
						break;
					case R_RT_ENTER_NEW:
						procReqRTEnterNew(msgUser, req, res, session);
						break;
					case R_SV_ENTER_NEW:
						procReqSVEnterNew(msgUser, req, res, session);
						break;
					// llamada de inicial cambio status
					case R_RT_ENTER_STATUS:
						procReqSVEnterStatus(msgUser, req, res, session);
						break;
					// Cuotas de participacion
					case R_CP_ENTER_NEW:
						procReqCPEnterNew(msgUser, req, res, session);
						break;
					case R_RT_ENTER_MAINT:
						procReqRTEnterMaint(msgUser, req, res, session);
						break;
					case R_SV_ENTER_MAINT:
						procReqSVEnterMaint(msgUser, req, res, session);
						break;
					case R_CP_ENTER_MAINT:
						procReqCPEnterMaint(msgUser, req, res, session);
						break;
					case R_RT_ENTER_INQUIRY:
						procReqRTEnterInquiry(msgUser, req, res, session);
						break;
					case R_RT_ENTER_SOBREGIRO:
						procReqRTSobregiro(msgUser, req, res, session);
						break;
					case R_RT_ENTER_LINEACREDITO:
						procReqRTLineacredito(msgUser, req, res, session);
						break;
					case R_SV_ENTER_INQUIRY:
						procReqSVEnterInquiry(msgUser, req, res, session);
						break;
					case R_CP_ENTER_INQUIRY:
						procReqCPEnterInquiry(msgUser, req, res, session);
						break;
					case R_RT_ENTER_PRINT:
						procReqRTEnterPrint(msgUser, req, res, session);
						break;
					case R_SV_ENTER_PRINT:
						procReqSVEnterPrint(msgUser, req, res, session);
						break;
					case R_CP_ENTER_PRINT:
						procReqCPEnterPrint(msgUser, req, res, session);
						break;
					case R_INQ_ACCOUNT_ANALYSIS:
						procReqInqAccountAnalysis(mc, msgUser, req, res,
								session);
						break;

					// Action
					case A_RT_ENTER_NEW:
						procActionRTEnterNew(mc, msgUser, req, res, session);
						break;
					case A_SV_ENTER_NEW:
						procActionSVEnterNew(mc, msgUser, req, res, session);
						break;
					// Cuotas de Participacion
					case A_CP_ENTER_NEW:
						procActionCPEnterNew(mc, msgUser, req, res, session);
						break;

					case A_RT_ENTER_MAINT:
						procActionEnterMaint(mc, msgUser, req, res, session);
						break;
					case A_SV_ENTER_MAINT:
						procActionEnterSVMaint(mc, msgUser, req, res, session);
						break;
					case A_CP_ENTER_MAINT:
						procActionEnterCPMaint(mc, msgUser, req, res, session);
						break;
					case A_RT_ENTER_INQUIRY:
					case A_SV_ENTER_INQUIRY:
					case A_CP_ENTER_INQUIRY:
						procActionEnterInquiry(mc, msgUser, req, res, session);
						break;
					case A_RT_ENTER_PRINT:
					case A_SV_ENTER_PRINT:
					case A_CP_ENTER_PRINT:
						procActionEnterPrint(mc, msgUser, req, res, session);
						break;

					case R_INQ_CHECKBOOK:
						procReqInqCheckbook(mc, msgUser, req, res, session);
						break;
					// Ahorro Vivienda Ingreso, Mantencion Saldos Promedios
					// P.Cataldo
					case R_SV_SALDOS_AHORRO_VIV:
						procReqSaldosAhorroVivienda(mc, msgUser, req, res,
								session);
						break;
					case A_SV_SALDOS_AHORRO_VIV:
						procActionSaldosAhorroVivienda(mc, msgUser, req, res,
								session);
						break;
					// END Entering

					case A_SV_CONVENIOS:
						procReqSVEnterInquiryConvenios(msgUser, req, res,
								session);
						break;
					case A_INTER_CONVENIOS:
						ProductList(msgUser, req, res, session);
						break;
					case A_SV_IFRS:
						procReqSVEnterInquiryIFRS(msgUser, req, res,
								session);
						break;
					case A_INTER_IFRS:
						procReqPaymPlan(mc, msgUser, req, res, session);
						break;
					default:
						res
								.sendRedirect(super.srctx + LangPath
										+ super.devPage);
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 4;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath
							+ super.sckNotOpenPage);
				} finally {
					s.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath
						+ super.sckNotRespondPage);
			}

		}

	}
	
	protected void procReqPaymPlan(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
	throws ServletException, IOException {


		MessageRecord newmessage = null;
		EDL090001Message msgHeader = null;
		ELEERRMessage msgError = null;
		EDL090002Message msgList = null;
		EDL090003Message msgListUF = null;
		JBListRec beanList = null;
		JBListRec beanListUF = null;
		UserPos	userPO = null;	
		boolean IsNotError = false;
		String sType = "";

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} 
		catch (Exception ex) {
			flexLog("Error: " + ex); 
		}

		userPO = (datapro.eibs.beans.UserPos)ses.getAttribute("userPO");

		String opCode = "0004";

		// Send Initial data
		try
		{
			msgHeader = (EDL090001Message)mc.getMessageRecord("EDL090001");
			msgHeader.setH01USERID(user.getH01USR());
			msgHeader.setH01PROGRM("EDL0900");
			msgHeader.setH01TIMSYS(getTimeStamp());
			msgHeader.setH01OPECOD(opCode);
			try {
				msgHeader.setE01DEAACC("140904120821");
			}
			catch (Exception e) {
				msgHeader.setE01DEAACC("0");
			}
			try {
				String s = req.getParameter("Type");
				if (s.equals("A")) {
					sType = "APPROVAL";
				} else {
					sType = "INQUIRY";
				}
			}
			catch (Exception e) {
				sType = userPO.getPurpose().trim();
			}
			
			msgHeader.send();	
			msgHeader.destroy();

			flexLog("EDL090001 Message Sent");
		}		
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
			
		// Receive Data
		try
		{
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage)newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
			}
			else if (newmessage.getFormatName().equals("EDL090001")) {
				try {
					msgHeader = new datapro.eibs.beans.EDL090001Message();
					
					flexLog("EDL090001 Message Received");
					
					msgHeader = (EDL090001Message)newmessage;
					
					userPO.setPurpose(sType);
					userPO.setHeader10(Util.formatDate(msgHeader.getE01OPEND1(),msgHeader.getE01OPEND2(),msgHeader.getE01OPEND3())); //Opening Date
					userPO.setHeader11(Util.formatDate(msgHeader.getE01MATUR1(),msgHeader.getE01MATUR2(),msgHeader.getE01MATUR3())); //Maturity Date
					userPO.setHeader12(Util.formatCCY(msgHeader.getE01DEAOAM()));  // Original Amount
					userPO.setHeader13(Util.formatCell(msgHeader.getE01DEARTE())); // Rate
					userPO.setHeader14(Util.formatCell(msgHeader.getE01DEABAS())); // Basis
					userPO.setHeader15(Util.formatCell(msgHeader.getE01DEAICT())); // Interest Type
				}
				catch (Exception ex) {
					flexLog("Error: " + ex); 
				}

				msgHeader = (EDL090001Message)newmessage;


				// Fill List bean
				int colNum = 94;
				try {
					beanList = new datapro.eibs.beans.JBListRec();
					beanList.init(colNum);
				} 
				catch (Exception ex) {
					flexLog("Error: " + ex); 
				}
				
				char sel = ' ';
				String marker = "";
				String myFlag = "";
				String myRow[] = new String[colNum];
				for (int i=0; i<colNum; i++) {
					myRow[i] = "";
				}
				
				BigDecimal principal = new BigDecimal("0");
				BigDecimal interest = new BigDecimal("0");
				BigDecimal other = new BigDecimal("0");
				BigDecimal payments = new BigDecimal("0");
				while (true) {

					newmessage = mc.receiveMessage();
					msgList = (EDL090002Message)newmessage;

					marker = msgList.getE02ENDFLD();

					if (marker.equals("*")) {
						break;
					}
					else {
						//Quote List
						principal = principal.add(msgList.getBigDecimalE02DLPPRN());
						interest = interest.add(msgList.getBigDecimalE02DLPINT());
						other = other.add(msgList.getBigDecimalE02DLPOTH());
						payments = payments.add(msgList.getBigDecimalE02DLPPAG());
						
						myRow[0] =  Util.formatCell(msgList.getE02DLPPNU());	// Quote Number
						myRow[1] =  Util.formatDate(msgList.getE02DLPPD1(),msgList.getE02DLPPD2(),msgList.getE02DLPPD3());	// Fecha a Pagar
						myRow[2] =  Util.formatCCY(msgList.getE02DLPPRN());	// Principal Pymt
						myRow[3] =  Util.formatCCY(msgList.getE02DLPINT());	// Interest Pymnt
						myRow[4] =  Util.formatCCY(msgList.getE02DLPOTH());	// Other Charges
						myRow[5] =  Util.formatCell(msgList.getE02DLPTOT());	// Total Quotes
						myRow[6] =  Util.formatCell(msgList.getE02DLPBAL());	// Balance
						myRow[7] =  Util.formatCell(msgList.getE02DLPSTS());	// Status
						myRow[8] =  Util.formatCell(msgList.getE02DLPVEN());	// Mature
						myRow[9] =  Util.formatDate(msgList.getE02DLPDT1(),msgList.getE02DLPDT2(),msgList.getE02DLPDT3());	// Fecha a Pagar
						myRow[10] =  Util.formatCell(msgList.getE02DLPPAG());	// Total Payment
						//Quote Detail
						myRow[11] =  Util.formatCell(msgList.getE02DESC01());	// Description
						myRow[12] =  Util.formatCell(msgList.getE02DESC02());
						myRow[13] =  Util.formatCell(msgList.getE02DESC03());
						myRow[14] =  Util.formatCell(msgList.getE02DESC04());
						myRow[15] =  Util.formatCell(msgList.getE02DESC05());
						myRow[16] =  Util.formatCell(msgList.getE02DESC06());
						myRow[17] =  Util.formatCell(msgList.getE02DESC07());
						myRow[18] =  Util.formatCell(msgList.getE02DESC08());
						myRow[19] =  Util.formatCell(msgList.getE02DESC09());
						myRow[20] =  Util.formatCell(msgList.getE02DESC10());
						myRow[21] =  Util.formatCell(msgList.getE02DESC11());
						myRow[22] =  Util.formatCell(msgList.getE02DESC12());
						myRow[23] =  Util.formatCell(msgList.getE02DESC13());
						myRow[24] =  Util.formatCell(msgList.getE02DESC14());
						myRow[25] =  Util.formatCell(msgList.getE02DESC15());
						myRow[26] =  Util.formatCell(msgList.getE02DESC16());
						myRow[27] =  Util.formatCell(msgList.getE02DESC17());
						myRow[28] =  Util.formatCell(msgList.getE02DESC18());
						myRow[29] =  Util.formatCell(msgList.getE02DESC19());
						myRow[30] =  Util.formatCell(msgList.getE02DESC20());
						myRow[31] =  Util.formatCCY(msgList.getE02AMNT01().trim()); 	//Amount
						myRow[32] =  Util.formatCCY(msgList.getE02AMNT02().trim());
						myRow[33] =  Util.formatCCY(msgList.getE02AMNT03().trim());
						myRow[34] =  Util.formatCCY(msgList.getE02AMNT04().trim());
						myRow[35] =  Util.formatCCY(msgList.getE02AMNT05().trim());
						myRow[36] =  Util.formatCCY(msgList.getE02AMNT06().trim());
						myRow[37] =  Util.formatCCY(msgList.getE02AMNT07().trim());
						myRow[38] =  Util.formatCCY(msgList.getE02AMNT08().trim());
						myRow[39] =  Util.formatCCY(msgList.getE02AMNT09().trim());
						myRow[40] =  Util.formatCCY(msgList.getE02AMNT10().trim());
						myRow[41] =  Util.formatCCY(msgList.getE02AMNT11().trim());
						myRow[42] =  Util.formatCCY(msgList.getE02AMNT12().trim());
						myRow[43] =  Util.formatCCY(msgList.getE02AMNT13().trim());
						myRow[44] =  Util.formatCCY(msgList.getE02AMNT14().trim());
						myRow[45] =  Util.formatCCY(msgList.getE02AMNT15().trim());
						myRow[46] =  Util.formatCCY(msgList.getE02AMNT16().trim());
						myRow[47] =  Util.formatCCY(msgList.getE02AMNT17().trim());
						myRow[48] =  Util.formatCCY(msgList.getE02AMNT18().trim());
						myRow[49] =  Util.formatCCY(msgList.getE02AMNT19().trim());
						myRow[50] =  Util.formatCCY(msgList.getE02AMNT20().trim());
						myRow[51] =  Util.formatCCY(msgList.getE02PAID01().trim());	//Paid	
						myRow[52] =  Util.formatCCY(msgList.getE02PAID02().trim());
						myRow[53] =  Util.formatCCY(msgList.getE02PAID03().trim());
						myRow[54] =  Util.formatCCY(msgList.getE02PAID04().trim());
						myRow[55] =  Util.formatCCY(msgList.getE02PAID05().trim());
						myRow[56] =  Util.formatCCY(msgList.getE02PAID06().trim());
						myRow[57] =  Util.formatCCY(msgList.getE02PAID07().trim());
						myRow[58] =  Util.formatCCY(msgList.getE02PAID08().trim());
						myRow[59] =  Util.formatCCY(msgList.getE02PAID09().trim());
						myRow[60] =  Util.formatCCY(msgList.getE02PAID10().trim());
						myRow[61] =  Util.formatCCY(msgList.getE02PAID11().trim());
						myRow[62] =  Util.formatCCY(msgList.getE02PAID12().trim());
						myRow[63] =  Util.formatCCY(msgList.getE02PAID13().trim());
						myRow[64] =  Util.formatCCY(msgList.getE02PAID14().trim());
						myRow[65] =  Util.formatCCY(msgList.getE02PAID15().trim());
						myRow[66] =  Util.formatCCY(msgList.getE02PAID16().trim());
						myRow[67] =  Util.formatCCY(msgList.getE02PAID17().trim());
						myRow[68] =  Util.formatCCY(msgList.getE02PAID18().trim());
						myRow[69] =  Util.formatCCY(msgList.getE02PAID19().trim());
						myRow[70] =  Util.formatCCY(msgList.getE02PAID20().trim());
						myRow[71] =  Util.formatCCY(msgList.getE02COND01().trim());	//Condonacion	
						myRow[72] =  Util.formatCCY(msgList.getE02COND02().trim());
						myRow[73] =  Util.formatCCY(msgList.getE02COND03().trim());
						myRow[74] =  Util.formatCCY(msgList.getE02COND04().trim());
						myRow[75] =  Util.formatCCY(msgList.getE02COND05().trim());
						myRow[76] =  Util.formatCCY(msgList.getE02COND06().trim());
						myRow[77] =  Util.formatCCY(msgList.getE02COND07().trim());
						myRow[78] =  Util.formatCCY(msgList.getE02COND08().trim());
						myRow[79] =  Util.formatCCY(msgList.getE02COND09().trim());
						myRow[80] =  Util.formatCCY(msgList.getE02COND10().trim());
						myRow[81] =  Util.formatCCY(msgList.getE02COND11().trim());
						myRow[82] =  Util.formatCCY(msgList.getE02COND12().trim());
						myRow[83] =  Util.formatCCY(msgList.getE02COND13().trim());
						myRow[84] =  Util.formatCCY(msgList.getE02COND14().trim());
						myRow[85] =  Util.formatCCY(msgList.getE02COND15().trim());
						myRow[86] =  Util.formatCCY(msgList.getE02COND16().trim());
						myRow[87] =  Util.formatCCY(msgList.getE02COND17().trim());
						myRow[88] =  Util.formatCCY(msgList.getE02COND18().trim());
						myRow[89] =  Util.formatCCY(msgList.getE02COND19().trim());
						myRow[90] =  Util.formatCCY(msgList.getE02COND20().trim());
						myRow[91] =  Util.formatCCY(msgList.getE02TOAMNT().trim());
						myRow[92] =  Util.formatCCY(msgList.getE02TOPAID().trim());
						myRow[93] =  Util.formatCCY(msgList.getE02TOCOND().trim());
							
						beanList.addRow(myFlag, myRow);
										
					}

				}
				
				userPO.setHeader20(principal.toString());
				userPO.setHeader21(interest.toString());
				userPO.setHeader22(other.toString());
				userPO.setHeader23(payments.toString());
				userPO.setHeader19(principal.add(interest.add(other)).toString());
				// Fill List bean
				colNum = 94;
				try {
					beanListUF = new datapro.eibs.beans.JBListRec();
					beanListUF.init(colNum);
			  	} 
				catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}
				String markerUF = "";
				String myFlagUF = "";
				String myRowUF[] = new String[colNum];
				for (int i=0; i<colNum; i++) {
					myRowUF[i] = "";
				}
				float  principalUF = 0;
				float interestUF = 0;
				float otherUF = 0;
				float paymentsUF = 0;
				
				while (true) {             // registro en UF
					newmessage = mc.receiveMessage();
					msgListUF = (EDL090003Message)newmessage;

					markerUF = msgListUF.getE03ENDFLD();

					if (markerUF.equals("*")) {
						break;
					} else {
						principalUF = principalUF + Float.parseFloat(msgListUF.getE03DLPPRN().replaceAll(",","."));					
						interestUF = interestUF + Float.parseFloat(msgListUF.getE03DLPINT().replaceAll(",","."));
						otherUF = otherUF + Float.parseFloat(msgListUF.getE03DLPOTH().replaceAll(",","."));
						paymentsUF = paymentsUF + Float.parseFloat(msgListUF.getE03DLPPAG().replaceAll(",","."));

						
						//Quote List				
						myRowUF[0] =  Util.formatCell(msgListUF.getE03DLPPNU());	// Quote Number
						myRowUF[1] =  Util.formatDate(msgListUF.getE03DLPPD1(),msgList.getE02DLPPD2(),msgList.getE02DLPPD3());	// Fecha a Pagar
						myRowUF[2] =  Util.formatCell(msgListUF.getE03DLPPRN());	// Principal Pymt
						myRowUF[3] =  Util.formatCell(msgListUF.getE03DLPINT());	// Interest Pymnt
						myRowUF[4] =  Util.formatCell(msgListUF.getE03DLPOTH());	// Other Charges
						myRowUF[5] =  Util.formatCell(msgListUF.getE03DLPTOT());	// Total Quotes
						myRowUF[6] =  Util.formatCell(msgListUF.getE03DLPBAL());	// Balance
						myRowUF[7] =  Util.formatCell(msgListUF.getE03DLPSTS());	// Status
						myRowUF[8] =  Util.formatCell(msgListUF.getE03DLPVEN());	// Mature
						myRowUF[9] =  Util.formatDate(msgListUF.getE03DLPDT1(),msgListUF.getE03DLPDT2(),msgListUF.getE03DLPDT3());	// Fecha a Pagar
						myRowUF[10] =  Util.formatCell(msgListUF.getE03DLPPAG());	// Total Pymnt
						//Quote Detail
						myRowUF[11] =  Util.formatCell(msgListUF.getE03DESC01());	// Description
						myRowUF[12] =  Util.formatCell(msgListUF.getE03DESC02());
						myRowUF[13] =  Util.formatCell(msgListUF.getE03DESC03());
						myRowUF[14] =  Util.formatCell(msgListUF.getE03DESC04());
						myRowUF[15] =  Util.formatCell(msgListUF.getE03DESC05());
						myRowUF[16] =  Util.formatCell(msgListUF.getE03DESC06());
						myRowUF[17] =  Util.formatCell(msgListUF.getE03DESC07());
						myRowUF[18] =  Util.formatCell(msgListUF.getE03DESC08());
						myRowUF[19] =  Util.formatCell(msgListUF.getE03DESC09());
						myRowUF[20] =  Util.formatCell(msgListUF.getE03DESC10());
						myRowUF[21] =  Util.formatCell(msgListUF.getE03DESC11());
						myRowUF[22] =  Util.formatCell(msgListUF.getE03DESC12());
						myRowUF[23] =  Util.formatCell(msgListUF.getE03DESC13());
						myRowUF[24] =  Util.formatCell(msgListUF.getE03DESC14());
						myRowUF[25] =  Util.formatCell(msgListUF.getE03DESC15());
						myRowUF[26] =  Util.formatCell(msgListUF.getE03DESC16());
						myRowUF[27] =  Util.formatCell(msgListUF.getE03DESC17());
						myRowUF[28] =  Util.formatCell(msgListUF.getE03DESC18());
						myRowUF[29] =  Util.formatCell(msgListUF.getE03DESC19());
						myRowUF[30] =  Util.formatCell(msgListUF.getE03DESC20());
						myRowUF[31] =  Util.formatCell(msgListUF.getE03AMNT01().trim()); 	//Amount
						myRowUF[32] =  Util.formatCell(msgListUF.getE03AMNT02().trim());
						myRowUF[33] =  Util.formatCell(msgListUF.getE03AMNT03().trim());
						myRowUF[34] =  Util.formatCell(msgListUF.getE03AMNT04().trim());
						myRowUF[35] =  Util.formatCell(msgListUF.getE03AMNT05().trim());
						myRowUF[36] =  Util.formatCell(msgListUF.getE03AMNT06().trim());
						myRowUF[37] =  Util.formatCell(msgListUF.getE03AMNT07().trim());
						myRowUF[38] =  Util.formatCell(msgListUF.getE03AMNT08().trim());
						myRowUF[39] =  Util.formatCell(msgListUF.getE03AMNT09().trim());
						myRowUF[40] =  Util.formatCell(msgListUF.getE03AMNT10().trim());
						myRowUF[41] =  Util.formatCell(msgListUF.getE03AMNT11().trim());
						myRowUF[42] =  Util.formatCell(msgListUF.getE03AMNT12().trim());
						myRowUF[43] =  Util.formatCell(msgListUF.getE03AMNT13().trim());
						myRowUF[44] =  Util.formatCell(msgListUF.getE03AMNT14().trim());
						myRowUF[45] =  Util.formatCell(msgListUF.getE03AMNT15().trim());
						myRowUF[46] =  Util.formatCell(msgListUF.getE03AMNT16().trim());
						myRowUF[47] =  Util.formatCell(msgListUF.getE03AMNT17().trim());
						myRowUF[48] =  Util.formatCell(msgListUF.getE03AMNT18().trim());
						myRowUF[49] =  Util.formatCell(msgListUF.getE03AMNT19().trim());
						myRowUF[50] =  Util.formatCell(msgListUF.getE03AMNT20().trim());
						myRowUF[51] =  Util.formatCell(msgListUF.getE03PAID01().trim());	//Paid	
						myRowUF[52] =  Util.formatCell(msgListUF.getE03PAID02().trim());
						myRowUF[53] =  Util.formatCell(msgListUF.getE03PAID03().trim());
						myRowUF[54] =  Util.formatCell(msgListUF.getE03PAID04().trim());
						myRowUF[55] =  Util.formatCell(msgListUF.getE03PAID05().trim());
						myRowUF[56] =  Util.formatCell(msgListUF.getE03PAID06().trim());
						myRowUF[57] =  Util.formatCell(msgListUF.getE03PAID07().trim());
						myRowUF[58] =  Util.formatCell(msgListUF.getE03PAID08().trim());
						myRowUF[59] =  Util.formatCell(msgListUF.getE03PAID09().trim());
						myRowUF[60] =  Util.formatCell(msgListUF.getE03PAID10().trim());
						myRowUF[61] =  Util.formatCell(msgListUF.getE03PAID11().trim());
						myRowUF[62] =  Util.formatCell(msgListUF.getE03PAID12().trim());
						myRowUF[63] =  Util.formatCell(msgListUF.getE03PAID13().trim());
						myRowUF[64] =  Util.formatCell(msgListUF.getE03PAID14().trim());
						myRowUF[65] =  Util.formatCell(msgListUF.getE03PAID15().trim());
						myRowUF[66] =  Util.formatCell(msgListUF.getE03PAID16().trim());
						myRowUF[67] =  Util.formatCell(msgListUF.getE03PAID17().trim());
						myRowUF[68] =  Util.formatCell(msgListUF.getE03PAID18().trim());
						myRowUF[69] =  Util.formatCell(msgListUF.getE03PAID19().trim());
						myRowUF[70] =  Util.formatCell(msgListUF.getE03PAID20().trim());
						myRowUF[71] =  Util.formatCell(msgListUF.getE03COND01().trim());	//Condnoacion	
						myRowUF[72] =  Util.formatCell(msgListUF.getE03COND02().trim());
						myRowUF[73] =  Util.formatCell(msgListUF.getE03COND03().trim());
						myRowUF[74] =  Util.formatCell(msgListUF.getE03COND04().trim());
						myRowUF[75] =  Util.formatCell(msgListUF.getE03COND05().trim());
						myRowUF[76] =  Util.formatCell(msgListUF.getE03COND06().trim());
						myRowUF[77] =  Util.formatCell(msgListUF.getE03COND07().trim());
						myRowUF[78] =  Util.formatCell(msgListUF.getE03COND08().trim());
						myRowUF[79] =  Util.formatCell(msgListUF.getE03COND09().trim());
						myRowUF[80] =  Util.formatCell(msgListUF.getE03COND10().trim());
						myRowUF[81] =  Util.formatCell(msgListUF.getE03COND11().trim());
						myRowUF[82] =  Util.formatCell(msgListUF.getE03COND12().trim());
						myRowUF[83] =  Util.formatCell(msgListUF.getE03COND13().trim());
						myRowUF[84] =  Util.formatCell(msgListUF.getE03COND14().trim());
						myRowUF[85] =  Util.formatCell(msgListUF.getE03COND15().trim());
						myRowUF[86] =  Util.formatCell(msgListUF.getE03COND16().trim());
						myRowUF[87] =  Util.formatCell(msgListUF.getE03COND17().trim());
						myRowUF[88] =  Util.formatCell(msgListUF.getE03COND18().trim());
						myRowUF[89] =  Util.formatCell(msgListUF.getE03COND19().trim());
						myRowUF[90] =  Util.formatCell(msgListUF.getE03COND20().trim());
						myRowUF[91] =  Util.formatCell(msgListUF.getE03TOAMNT().trim());
						myRowUF[92] =  Util.formatCell(msgListUF.getE03TOPAID().trim());
						myRowUF[93] =  Util.formatCell(msgListUF.getE03TOCOND().trim());
							
						beanListUF.addRow(myFlagUF, myRowUF);
										
					}

				}
		        DecimalFormat nuf = new DecimalFormat("##,###.####");
				userPO.setHeader4(nuf.format(principalUF));
				userPO.setHeader5(nuf.format(interestUF));
				userPO.setHeader6(nuf.format(otherUF));
				userPO.setHeader7(nuf.format(paymentsUF));
				float TotalUF = principalUF + interestUF + otherUF;
				userPO.setHeader8(nuf.format(TotalUF));
				
				ses.setAttribute("list", beanList);
				ses.setAttribute("listUF", beanListUF);
				ses.setAttribute("header", msgHeader);			
				ses.setAttribute("userPO", userPO);		

				try {
					flexLog("About to call Page: " + LangPath + "EDL0900_ln_ap_crn_pay.jsp");
					callPage(LangPath + "EDL0900_ln_ap_crn_pay.jsp", req, res);
				}
				catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			}
			else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		}
		catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}	

}
	
	protected void procReqSVEnterInquiryIFRS(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			ELEERRMessage msgError = null;
			UserPos userPO = null;

			try {

				msgError = new datapro.eibs.beans.ELEERRMessage();
				userPO = new datapro.eibs.beans.UserPos();
				userPO.setOption("IF");
				userPO.setPurpose("REQUEST");
				userPO.setRedirect("/servlet/datapro.eibs.products.JSEXEDL0160?SCREEN=3002&Type=I");
				userPO.setProdCode("10");
				//Others Parameters
				userPO.setHeader1("E01CUN");
				userPO.setHeader3("E01IDN");
				
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
				
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			try {
				flexLog("About to call Page: " + LangPath + "GENERIC_account_enter.jsp");
				flexLog("procReqSVEnterInquiryConvenios");
				callPage(LangPath + "GENERIC_account_enter.jsp", req, res);
			} catch (Exception e) {
				flexLog("Exception calling page " + e);
			}
	}

	protected void procReqSVEnterInquiryConvenios(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			ELEERRMessage msgError = null;
			UserPos userPO = null;

			try {

				msgError = new datapro.eibs.beans.ELEERRMessage();
				userPO = new datapro.eibs.beans.UserPos();
				userPO.setOption("EC");
				userPO.setPurpose("REQUEST");
				userPO.setRedirect("/servlet/datapro.eibs.products.JSEXEDD0000?SCREEN=987");
				userPO.setProdCode("EC");
				//Others Parameters
				userPO.setHeader1("E01CUN");
				userPO.setHeader3("E01IDN");
				
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
				
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			try {
				flexLog("About to call Page: " + LangPath + "GENERIC_account_enter.jsp");
				flexLog("procReqSVEnterInquiryConvenios");
				callPage(LangPath + "GENERIC_account_enter.jsp", req, res);
			} catch (Exception e) {
				flexLog("Exception calling page " + e);
			}
	}
	
	protected void ProductList(ESS0030DSMessage user, 
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
	throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		try {
			mp = getMessageProcessor("ECO0500", req);

			ECO050001Message msgList = (ECO050001Message) mp.getMessageRecord("ECO050001", user.getH01USR(), "0001");
			//Sets the employee  number from either the first page or the maintenance page
	
			if (req.getParameter("SELCLIENT")!= null){
				String parameter = req.getParameter("SELCLIENT");
				msgList.setE01COHCUN(req.getParameter("SELCLIENT"));
			} 
			
			if (req.getParameter("E01IDN")!= null){
				msgList.setE01CUSIDN(req.getParameter("E01IDN"));
			} 
			
			
			flexLog("ECO050001: list"+msgList);
			
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	

			if (mp.hasError(error)) { // if there are errors go back to first page
						
					ses.setAttribute("error", error);
					flexLog("About to call Page: " + LangPath + "GENERIC_account_enter.jsp");
					callPage(LangPath + "GENERIC_account_enter.jsp", req, res);
			} else {
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
				
				ses.setAttribute("ConvenioList", list);
				
				 if (!list.isEmpty())
			        {
					 ECO050001Message listMessage = (ECO050001Message)list.get(0);
			          
					 
					 ses.setAttribute("cabezera_convenio", listMessage);
					 
			          
			        	}
				
			
				forward("ECO0500_product_list.jsp", req, res);

		
					}
			
		
		}
		

		finally {
			if (mp != null)
			mp.close();
			}

		}
	
	protected void forward(String pageName,
			HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
			callPage(getLangPath(req, res) + pageName, req, res);
		}
	
	protected String getLangPath(HttpServletRequest req, HttpServletResponse res) {
		HttpSession session = getSession(req, res);
		if (session != null) {
			ESS0030DSMessage msgUser =
				(ESS0030DSMessage) session.getAttribute("currUser");
			return getLangPath(msgUser);			
		} else {
			return SuperServlet.rootPath + "s/";
		}
	}
	
	protected String getLangPath(ESS0030DSMessage msgUser) {
		return SuperServlet.rootPath + (msgUser != null ? msgUser.getE01LAN().toLowerCase() : "s") + "/";
	}
	
	public MessageProcessor getMessageProcessor(String targetProgram, HttpServletRequest request) 
	throws IOException {
	try {
		return new MessageProcessor(
			getMessageHandler(targetProgram, request));
	} catch (ServiceLocatorException e) {
		throw new IOException(e.getMessage());
	}
}
	
	
	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

		}
	}

	protected void procReqAccountTitle(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ESD000004Message msgMailA = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = null;
		if (userPO.getPurpose().equals("NEW"))
			opCode = "0001";
		else
			opCode = "0002";

		// Send Initial data
		try {
			msgMailA = (ESD000004Message) mc.getMessageRecord("ESD000004");
			msgMailA.setH04USR(user.getH01USR());
			msgMailA.setH04PGM("ESD0080");
			msgMailA.setH04TIM(getTimeStamp());
			msgMailA.setH04SCR("01");
			msgMailA.setH04OPE(opCode);
			msgMailA.setE04CUN(userPO.getIdentifier());
			msgMailA.setE04RTP("1");
			msgMailA.setH04WK1("T");
			msgMailA.setH04WK3("1");
			msgMailA.send();
			msgMailA.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000004")) {
				try {
					msgMailA = new datapro.eibs.beans.ESD000004Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgMailA = (ESD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("mailA", msgMailA);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_account_title.jsp");
							callPage(LangPath + "EDD0000_rt_account_title.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_account_title.jsp");
							callPage(LangPath + "EDD0000_sv_account_title.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	protected void procActionAccountTitle(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ESD000004Message msgMailAddress = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgMailAddress = (ESD000004Message) mc
					.getMessageRecord("ESD000004");
			msgMailAddress.setH04USR(user.getH01USR());
			msgMailAddress.setH04PGM("ESD0080");
			msgMailAddress.setH04TIM(getTimeStamp());
			msgMailAddress.setH04SCR("01");
			msgMailAddress.setH04OPE("0005");
			msgMailAddress.setE04CUN(userPO.getIdentifier());
			msgMailAddress.setE04RTP("1");
			msgMailAddress.setH04WK1("T");
			msgMailAddress.setH04WK3("1");

			// all the fields here
			java.util.Enumeration enu = msgMailAddress.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			msgMailAddress.send();
			msgMailAddress.destroy();
			flexLog("ESD000004 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000004")) {
				try {
					msgMailAddress = new datapro.eibs.beans.ESD000004Message();
					flexLog("ESD000004 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgMailAddress = (ESD000004Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("mailA", msgMailAddress);

				if (IsNotError) { // There are no errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_basic.jsp");
							callPage(LangPath + "EDD0000_rt_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else { // There are errors
					if (userPO.getOption().equals("RT")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_account_title.jsp");
							callPage(LangPath + "EDD0000_rt_account_title.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_account_title.jsp");
							callPage(LangPath + "EDD0000_sv_account_title.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge. by David Mavilla. on 5/17/00.
	 */
	protected void procReqInqCheckbook(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		EDD000001Message msgRT = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgRT = (EDD000001Message) mc.getMessageRecord("EDD000001");
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD0000");
			msgRT.setH01TIMSYS(getTimeStamp());
			msgRT.setH01SCRCOD("01");
			msgRT.setH01FLGWK1("R");
			msgRT.setH01OPECOD("0002");
			try {
				msgRT.setE01ACMACC(userPO.getIdentifier());
			} catch (Exception e) {
				msgRT.setE01ACMACC("0");
			}

			msgRT.send();
			msgRT.destroy();
			flexLog("EDD000001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDD000001")) {
				try {
					msgRT = new datapro.eibs.beans.EDD000001Message();
					flexLog("EDD000001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgRT = (EDD000001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				if (IsNotError) { // There are no errors
					if (msgRT.getE01ACMACD().equals("04")) {
						userPO.setOption("SV");
						userPO.setAccNum(msgRT.getE01ACMACC());
						userPO.setIdentifier(msgRT.getE01ACMACC());
						userPO.setApplicationCode(msgRT.getE01ACMACD());
						userPO.setBank(msgRT.getE01ACMBNK());
						userPO.setCusNum(msgRT.getE01ACMCUN());
						userPO.setHeader1(msgRT.getE01ACMPRO());
						userPO.setHeader2(msgRT.getE01ACMCUN());
						userPO.setHeader3(msgRT.getE01CUSNA1());
						userPO.setCurrency(msgRT.getE01ACMCCY());
						userPO.setCusType(msgRT.getH01FLGWK3());

						ses.setAttribute("userPO", userPO);
						ses.setAttribute("svBasic", msgRT);

						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_basic.jsp");
							callPage(LangPath + "EDD0000_sv_basic.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						userPO.setOption("RT");
						userPO.setAccNum(msgRT.getE01ACMACC());
						userPO.setIdentifier(msgRT.getE01ACMACC());
						userPO.setApplicationCode(msgRT.getE01ACMACD());
						userPO.setBank(msgRT.getE01ACMBNK());
						userPO.setCusNum(msgRT.getE01ACMCUN());
						userPO.setHeader1(msgRT.getE01ACMPRO());
						userPO.setHeader2(msgRT.getE01ACMCUN());
						userPO.setHeader3(msgRT.getE01CUSNA1());
						userPO.setCurrency(msgRT.getE01ACMCCY());

						ses.setAttribute("userPO", userPO);
						ses.setAttribute("rtBasic", msgRT);

						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD1000_rt_inq_checkbook.jsp");
							callPage(LangPath + "EDD1000_rt_inq_checkbook.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else { // There are errors
					if (userPO.getOption().equals("SV")) {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_sv_enter_inquiry.jsp");
							callPage(LangPath + "EDD0000_sv_enter_inquiry.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else {
						try {
							flexLog("About to call Page: " + LangPath
									+ "EDD0000_rt_enter_inquiry.jsp");
							callPage(LangPath + "EDD0000_rt_enter_inquiry.jsp",
									req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}
}