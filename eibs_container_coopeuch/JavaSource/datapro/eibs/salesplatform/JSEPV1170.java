package datapro.eibs.salesplatform;

/**
 * Registros  de PACSG
 * Archivo de tienda_virtual para plataforma de venta
 * @author: Jose M. Buitrago 
 */
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.coopeuch.core.tiendavirtual.ConsultaProductoCampanaEntrada;
import cl.coopeuch.core.tiendavirtual.ConsultaProductosEntrada;
import cl.coopeuch.core.tiendavirtual.DetalleProductos;
import cl.coopeuch.core.tiendavirtual.Error;
import cl.coopeuch.core.tiendavirtual.RespConsultaProductoCampana;
import cl.coopeuch.core.tiendavirtual.RespConsultaProductos;
import cl.coopeuch.core.tiendavirtual.Error.ListadoErrores;
import cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.FaultMsg;
import cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtualSOAPProxy;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV117001Message;
import datapro.eibs.beans.EPV117002Message;
import datapro.eibs.beans.EPV117005Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSEPV1170 extends JSEIBSServlet {

	/**
	 *  
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int R_TIENDA_VIRTUAL_LIST = 100;
	protected static final int A_TIENDA_VIRTUAL_LIST = 101;
	protected static final int R_TIENDA_VIRTUAL_NEW = 200;
	protected static final int R_TIENDA_VIRTUAL_MAINT = 201;
	protected static final int R_TIENDA_VIRTUAL_DELETE = 202;
	protected static final int R_TIENDA_VIRTUAL_INQUIRY = 203;
	protected static final int A_TIENDA_VIRTUAL_MAINT = 600;
	protected static final int R_TIENDA_VIRTUAL_WS = 300;//Llamada a la patalla de tienda virtual
	protected static final int A_TIENDA_VIRTUAL_WS_MAINT = 400;	//llamada al guardar la tienda virtual

	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_TIENDA_VIRTUAL_LIST:
			procReqTiendaVirtualList(user, req, res, session);
			break;
		case A_TIENDA_VIRTUAL_LIST:
			procActionTiendaVirtualList(user, req, res, session, null);
			break;
		case R_TIENDA_VIRTUAL_NEW:
			procReqTiendaVirtual(user, req, res, session, "NEW");
			break;
		case R_TIENDA_VIRTUAL_MAINT:
			procReqTiendaVirtual(user, req, res, session, "MAINTENANCE");
			break;
		case R_TIENDA_VIRTUAL_INQUIRY:
			procReqTiendaVirtual(user, req, res, session, "INQUIRY");
			break;
		case A_TIENDA_VIRTUAL_MAINT:
			procActionMaintenance(user, req, res, session);
			break;
		case R_TIENDA_VIRTUAL_DELETE:
			procReqDelete(user, req, res, session);
			break;
		case R_TIENDA_VIRTUAL_WS:
			procReqTiendaVirtualWebService(user, req, res, session, "NEW");
			break;	
		case A_TIENDA_VIRTUAL_WS_MAINT:
			procActionWebServiceMaintenance(user, req, res, session);
			break;				
			
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	/**
	 * procReqTabla Tienda Virtual List
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException 
	 */
	protected void procReqTiendaVirtualList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {		
		try {
			flexLog("About to call Page: EPV1170_tienda_virtual_enter_search.jsp");
			forward("EPV1170_tienda_virtual_enter_search.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}

	/**
	 * procActionTablatienda_virtualList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionTiendaVirtualList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			mp = getMessageProcessor("EPV1170", req);

			EPV117001Message msg = (EPV117001Message) mp.getMessageRecord("EPV117001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());
			msg.setE01PTVCUN(req.getParameter("E01PTVCUN").trim());
			msg.setE01PTVNUM(req.getParameter("E01PTVNUM").trim());

			userPO.setCusNum(req.getParameter("E01PTVCUN").trim());
			userPO.setHeader23(req.getParameter("E01PTVNUM").trim());

			// Sends message
			mp.sendMessage(msg);

			// Receive header 
			EPV117002Message msgHeader = (EPV117002Message) mp.receiveMessageRecord();
			
			// Receive insurance list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			
			//si faltan datos llamar al webservice y completar informacion faltante.
			if (!list.getNoResult() && ("".equals(((EPV117001Message) list.get(0)).getE01PTVNME()))){
				list = completarPedido(list,session,req,user,userPO);
			}
			

			session.setAttribute("EPV117002", msgHeader);
			session.setAttribute("userpPO", userPO);
			session.setAttribute("EPV117001List", list);
			forwardOnSuccess("EPV1170_tienda_virtual_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	@SuppressWarnings("unchecked")
	private JBObjList completarPedido(JBObjList list,HttpSession session,HttpServletRequest req,
									ESS0030DSMessage user,UserPos userPO) throws IOException{
		//recorremos la lista
		TiendaVirtualSOAPProxy tvp = new TiendaVirtualSOAPProxy();	
		JBObjList listNew = new JBObjList();
		boolean error = false;
		MessageProcessor mp = null;		
		try {
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				EPV117001Message obj = (EPV117001Message) iterator.next();
				ConsultaProductoCampanaEntrada entrada = new ConsultaProductoCampanaEntrada();
				entrada.setCodCampana(Integer.valueOf(obj.getE01PTVCAM()));
				entrada.setCodProducto(Integer.valueOf(obj.getE01PTVPRD()));
				RespConsultaProductoCampana resp = tvp.consultaProductoCampana(entrada);
				obj.setE01PTVNCA(resp.getNomCampana());//nombre campa�a
				obj.setE01PTVNME(resp.getNombProd());//nombre producto
				obj.setE01PTVMAR(resp.getMarca());//marca
				obj.setE01PTVMOD(resp.getModelo());//modelo
				obj.setE01PTVVAU(new BigDecimal(resp.getPrecioUnitario()));//precio unitario.
				obj.setE01PTVSTO(new BigDecimal(resp.getDisponible()));//cant. disponible
				//obj.setE01PTVCAM();//codigo campa�a 		NO REEMPLAZAR	
				//obj.setE01PTVPRD();//ccodigo producto		NO REEMPLAZAR
				//obj.setE01PTVCEN();//costo envio.			NO VIENE
				//obj.setE01PTVCAN();//cantidad solicitada	NO REEMPLAZAR 
				//obj.setE01PTVCUN();//cutomer number		NO REEMPLAZAR
				//obj.setE01PTVNUM();//numero de solicitud	NO REEMPLAZAR	
				//obj.setE01PTVSEQ();// Numero de secuencia	NO REEMPLAZAR				
				listNew.add(obj);				
			}				
		}catch (Exception e) {
			error=true;
			String className = e.getClass().getName();
			String description = e.getMessage() == null ? "Exception General" : e.getMessage();	
			ELEERRMessage msgError = new ELEERRMessage();			
			msgError.setERRNUM("3");
            msgError.setERNU01("01");//4
            msgError.setERDS01(className);
            msgError.setERNU02("02");//4
            msgError.setERDS02("ERROR AL ACCEDER SERVICIO DE TIENDA VIRTUAL. VERIFIQUE COMUNICACION.");
            msgError.setERNU03("03");//4
            msgError.setERDS03(description.length() > 70 ? description.substring(0, 70) : description);
			session.setAttribute("error", msgError);            
			System.out.println("ERROR TIENDA VIRTUAL:"+e);
			e.printStackTrace();										
		}
		
		if (!error){
			listNew.initRow();
			listNew.getNextRow();
			//TODO:
			EPV117001Message prod1 = (EPV117001Message) listNew.getRecord();					
			mp = getMessageProcessor("EPV1170", req);
			EPV117001Message msg = (EPV117001Message) mp.getMessageRecord("EPV117001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0021");
			msg.setH01TIMSYS(getTimeStamp());
			msg.setE01PTVCUN(prod1.getE01PTVCUN());				
			msg.setE01PTVNUM(prod1.getE01PTVNUM());						
			// Sending message
			mp.sendMessage(msg);
			// Receive error
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();				
			if (mp.hasError(msgError)){
				// if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				return list;
			}
			listNew.initRow();	    	
			while (listNew.getNextRow()) {					
				EPV117001Message prod = (EPV117001Message) listNew.getRecord();
				prod.setH01USERID(user.getH01USR());
				prod.setH01OPECOD("0022");
				prod.setH01TIMSYS(getTimeStamp());
				//Sending message
				mp.sendMessage(prod);
				// Receive error
				ELEERRMessage msgErr = (ELEERRMessage) mp.receiveMessageRecord();
				if (mp.hasError(msgErr)){
					// if there are errors go back to maintenance page and show errors
					session.setAttribute("error", msgErr);
					return list;
				}						
		 	}//EoW
			
			//buscamos la nueva lista
			EPV117001Message msg1 = (EPV117001Message) mp.getMessageRecord("EPV117001");
			msg1.setH01USERID(user.getH01USR());
			msg1.setH01OPECOD("0015");
			msg1.setH01TIMSYS(getTimeStamp());
			msg1.setE01PTVCUN(userPO.getCusNum());
			msg1.setE01PTVNUM(userPO.getHeader23());

			// Sends message
			mp.sendMessage(msg);

			// Receive header 
			EPV117002Message msgHeader = (EPV117002Message) mp.receiveMessageRecord();
			
			// Receive insurance list
			listNew = mp.receiveMessageRecordList("H01FLGMAS");			
			
		}else{
			return list;
		}
		
		return listNew;
	}
	
	/**
	 * procReqTablatienda_virtual: This Method show a single Tabla Seguros either for a
	 * new register, a maintenance or an inquiry.
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTiendaVirtual(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("EPV1170", req);
			userPO.setPurpose(option);

			// Creates the message with operation code depending on the option
			EPV117005Message msg = (EPV117005Message) mp.getMessageRecord("EPV117005");
			msg.setH05USERID(user.getH01USR());
			msg.setH05TIMSYS(getTimeStamp());
			if (option.equals("NEW")) {
				msg.setH05OPECOD("0001");
			} else if (option.equals("MAINTENANCE")) {
				msg.setH05OPECOD("0002");
			} else {
				msg.setH05OPECOD("0004");
			}

			// Sets the number for maintenance and inquiry options
			msg.setE05PTVCUN(userPO.getCusNum());
			msg.setE05PTVNUM(userPO.getHeader23());

			// Send message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV117005Message) mp.receiveMessageRecord();

			// Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				// if there are no errors go to maintenance page
				flexLog("About to call Page: EPV1170_tienda_virtual_maintenance.jsp");
				if (option.equals("INQUIRY")) {
					// if the request is an inquiry sets the readOlnly attribute
					// 'true'
					forward("EPV1170_tienda_virtual_maintenance.jsp?readOnly=true",
							req, res);
				} else {
					forward("EPV1170_tienda_virtual_maintenance.jsp", req, res);
				}
			} else {
				// if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1170_tienda_virtual_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procActionMaintenance
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session
				.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1170", req);

			EPV117005Message msg = (EPV117005Message) mp.getMessageRecord("EPV117005");
			msg.setH05USERID(user.getH01USR());
			msg.setH05OPECOD("0005");
			msg.setH05TIMSYS(getTimeStamp());

			// Sets message with page fields
			msg.setH05SCRCOD("01");
			setMessageRecord(req, msg);
			msg.setE05PTVCUN(req.getParameter("E05PTVCUN").trim());
			msg.setE05PTVNUM(req.getParameter("E05PTVNUM").trim());

			// Sending message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV117005Message) mp.receiveMessageRecord();

			// Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				// if there are no errors go back to list
				//redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1170?SCREEN=101&E01PTVCUN="+ userPO.getCusNum()+"&E01PTVNUM="+ userPO.getHeader23(), res);
				req.setAttribute("ACT","S");
				forward("EPV1170_tienda_virtual_maintenance.jsp", req, res);				
			} else {
				// if there are errors go back to maintenance page and show
				// errors
				session.setAttribute("error", msgError);
				forward("EPV1170_tienda_virtual_maintenance.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqDelete
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDelete(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("MAINTENANCE");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1170", req);

			// Creates message with the 'Delete'operation code
			EPV117001Message msg = (EPV117001Message) mp.getMessageRecord("EPV117001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0009");
			msg.setH01TIMSYS(getTimeStamp());

			// Sets required values
			msg.setH01SCRCOD("01");
			// Sets the Code for delete options
			msg.setE01PTVCUN(userPO.getCusNum());
			msg.setE01PTVNUM(userPO.getHeader23());
			msg.setE01PTVSEQ(req.getParameter("E01PTVSEQ").trim());

			// Send message
			mp.sendMessage(msg);

			// Receive Error and Data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV117001Message) mp.receiveMessageRecord();

			// Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				// If there are no errors request the list again
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1170?SCREEN=101&E01PTVCUN="+ userPO.getCusNum()+"&E01PTVNUM="+ userPO.getHeader23(), res);				
			} else {
				// if there are errors show the list without updating
				session.setAttribute("error", msgError);
				forward("EPV1170_tienda_virtual_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	protected void procReqTiendaVirtualWebService(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			JBObjList listSel = (JBObjList)session.getAttribute("EPV117001List");
			JBObjList list = new JBObjList();			
 
			EPV117002Message msgHeader = (EPV117002Message) session.getAttribute("EPV117002");
			
			//set enter parameters
			ConsultaProductosEntrada entrada = new ConsultaProductosEntrada();
			
			String dd =(msgHeader.getE02RUNDTD().length()==1)?"0"+msgHeader.getE02RUNDTD():msgHeader.getE02RUNDTD();
			String mm =(msgHeader.getE02RUNDTM().length()==1)?"0"+msgHeader.getE02RUNDTM():msgHeader.getE02RUNDTM();
			String aaaa= msgHeader.getE02RUNDTY();
			entrada.setCodOficina(Integer.parseInt(msgHeader.getE02NUMOFI()));
			entrada.setRutSocio(Integer.parseInt(msgHeader.getE02RUTCLI().substring(0, msgHeader.getE02RUTCLI().length()-1)));
			entrada.setCodConvenio(msgHeader.getE02CNVCLI());		
			entrada.setIFecProceso(Integer.parseInt(aaaa+mm+dd));				
			
			try {
				TiendaVirtualSOAPProxy tvp = new TiendaVirtualSOAPProxy();				
				RespConsultaProductos resp = tvp.consultaProductos(entrada);
				
				if (resp.getError()==null){
					List<DetalleProductos> dpList = resp.getDetalleProductos();												
					for (Iterator<DetalleProductos> iterator = dpList.iterator(); iterator.hasNext();) {
						DetalleProductos dp = (DetalleProductos) iterator.next();
						EPV117001Message msg = new EPV117001Message();
						msg.setE01PTVCUN(msgHeader.getE02PTVCUN());
						msg.setE01PTVNUM(msgHeader.getE02PTVNUM());						
						msg.setE01PTVCAM(String.valueOf(dp.getCodCampana()));
						msg.setE01PTVNCA(dp.getNomCampana());
						msg.setE01PTVPRD(String.valueOf(dp.getCodProducto()));
						msg.setE01PTVNME(dp.getNombProd());
						msg.setE01PTVMAR(dp.getMarca());						
						msg.setE01PTVMOD(dp.getModelo());
						msg.setE01PTVVAU(String.valueOf(dp.getPrecioUnitario()));
						msg.setE01PTVCEN(String.valueOf(dp.getCostoEnvio()));
						msg.setE01PTVSTO(String.valueOf(dp.getDisponible())); 
						//buscamos la cantidad solicitada
						if(!listSel.getNoResult()){
							listSel.initRow();
							while (listSel.getNextRow()) {
								EPV117001Message prodSel = (EPV117001Message) listSel.getRecord();
								if (msg.getE01PTVCAM().trim().equals(prodSel.getE01PTVCAM().trim())&&
										msg.getE01PTVPRD().trim().equals(prodSel.getE01PTVPRD().trim()) &&
										msg.getE01PTVMAR().trim().equals(prodSel.getE01PTVMAR().trim())&&
										msg.getE01PTVMOD().trim().equals(prodSel.getE01PTVMOD().trim())){
										msg.setE01PTVCAN(prodSel.getE01PTVCAN());
										break;//exit while
								}
							}
						}							
						list.add(msg);				 
					}//EoF.
					
				}else{
					//send error message standart ibs
					List<Error.ListadoErrores> err = resp.getError().getListadoErrores();
	                ELEERRMessage msgError = new ELEERRMessage();
	                int numError = 0;

	                for (Iterator<Error.ListadoErrores> iterator = err.iterator(); iterator.hasNext();) {
						ListadoErrores e = (ListadoErrores) iterator.next();
						
						if (numError<9){//show the first 9
							numError++;
							String c = e.getErrores().getValue().getCodigoError();
							String d = e.getErrores().getValue().getDescripcionError();	
							msgError.getField("ERNU0"+numError).setString((c.length() >4?(c.substring(0,4)):c));//4
							msgError.getField("ERDS0"+numError).setString((d.length() >70?(d.substring(0,70)):d));//70					
						}else{
							break;
						}
						
					}//EoF
					msgError.setERRNUM(String.valueOf(numError));//number errors of the object				
					session.setAttribute("error", msgError);							
				}
				
			}
			catch (Exception e) {
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();					
                ELEERRMessage msgError = new ELEERRMessage();				
				msgError.setERRNUM("3");
	            msgError.setERNU01("01");//4
	            msgError.setERDS01("ERROR AL ACCEDER SERVICIO DE TIENDA VIRTUAL. VERIFIQUE COMUNICACION.");
	            msgError.setERNU02("02");//4
	            msgError.setERDS02(className);
	            msgError.setERNU03("03");//4
	            msgError.setERDS03(description.length() > 70 ? description.substring(0, 70) : description);	
	  			session.setAttribute("error", msgError);				
			}
			
			// Sets session with required data
			session.setAttribute("listaProd", list);				
			session.setAttribute("userPO", userPO);			
			forward("EPV1170_tienda_virtual_maintenance_ws.jsp", req, res);
		} finally {
			
		}
	}
	
	protected void procActionWebServiceMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			JBObjList list = (JBObjList)session.getAttribute("listaProd");
			if (!list.getNoResult()){
				list.initRow();
				list.getNextRow();
				//TODO:
				EPV117001Message prod1 = (EPV117001Message) list.getRecord();				
				//first send opecod 0021 (delete all)
				mp = getMessageProcessor("EPV1170", req);
				EPV117001Message msg = (EPV117001Message) mp.getMessageRecord("EPV117001");
				msg.setH01USERID(user.getH01USR());
				msg.setH01OPECOD("0021");
				msg.setH01TIMSYS(getTimeStamp());
				msg.setE01PTVCUN(prod1.getE01PTVCUN());				
				msg.setE01PTVNUM(prod1.getE01PTVNUM());								
				// Sending message
				mp.sendMessage(msg);
				// Receive error
				ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();				
				if (mp.hasError(msgError)){
					// if there are errors go back to maintenance page and show errors
					session.setAttribute("error", msgError);
					forward("EPV1170_tienda_virtual_maintenance_ws.jsp", req, res);
					return;
				}
				//if no errors continue, opecod 0022 insert either one
				list.initRow();
		    	int fil = 0;//rows arraylist
		    	int seq = 0;//number seq.		    	
				while (list.getNextRow()) {					
					fil++;
					if (req.getParameter("flat"+fil)!=null){//just checked
						EPV117001Message prod = (EPV117001Message) list.getRecord();
						String cantSol = req.getParameter("cant_sol"+fil);
						prod.setE01PTVCAN(cantSol);						
						seq++;
						prod.setE01PTVSEQ(String.valueOf(seq));
						prod.setH01USERID(user.getH01USR());
						prod.setH01OPECOD("0022");
						prod.setH01TIMSYS(getTimeStamp());
						//Sending message
						mp.sendMessage(prod);
						// Receive error
						ELEERRMessage msgErr = (ELEERRMessage) mp.receiveMessageRecord();
						if (mp.hasError(msgErr)){
							// if there are errors go back to maintenance page and show errors
							session.setAttribute("error", msgErr);
							forward("EPV1170_tienda_virtual_maintenance_ws.jsp", req, res);
							return;
						}						
					} 
				}
				
				//si esta en este punto actualizo bien todo..
				req.setAttribute("ACT","S");
				
			}else{
				ELEERRMessage msgError = new ELEERRMessage();
                msgError.setERRNUM("1");
                msgError.setERNU01("01");							 //4		                
                msgError.setERDS01("NO EXISTEN PRODUCTOS ASOCIADOS");//70				
				session.setAttribute("error", msgError);
			}
						
			forward("EPV1170_tienda_virtual_maintenance_ws.jsp", req, res);	

		} finally {
			if (mp != null)
				mp.close();
		}
	}	

}
