<!-- Hecho por Alonso Arana ------Datapro-----16/05/2014 -->
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ECO050001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Aprobaci�n de Ex-Convenios</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="AproveList" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

   $(function(){
				
					$("#radio_key").attr("checked", false);
                
				});


  function goAction(op,index) {
	var ok = false;
	var cun = "";
	var pg = "";

	if (op != '3000'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01RCCTCD") 
	      	{	
	      		if (element.checked == true) {
	      			document.getElementById("codigo_lista").value = element.value; 
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = false;
      	var confirm2=false;
      	
      	
      	if (op =='202'){
      		confirm1 = confirm("�Desea Aprobar la renuncia del convenio?");
      	}
      	
      	if(op=='203'){
      	
      	confirm2=confirm("�Desea Rechazar la renuncia del convenio?");
      	
      	}
      	
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		
		
		if(confirm2){
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		
		
		}
		
		
		

     } else {
		alert("Debe seleccionar un registro para continuar.");	   
	 }    
	}



</SCRIPT>  

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Aprobaci�n de Ex-Convenios<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ECO0500_aprovettion_list.jsp,ECO0500"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0500">
<input type="hidden" name="SCREEN" value="201">
<input type="hidden" name="codigo_lista" value="" id="codigo_lista">  


  

 <table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('202')"><b>Aprobar</b></a>
		</td>
	
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('203')"><b>Rechazar</b></a>
		</td>
		
		
	
		
		
	</tr>
</table>


<%
	if (AproveList.getNoResult()) {
%>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>



<%
	
	
	} else {
%>

  <p>&nbsp;</p>
<table id="headTable" width="1342" align="center">
	<tr id="trdark">
		<th align="center" nowrap width="30"></th>
		<th align="center" nowrap width="100">Cliente</th>
		<th align="center" nowrap width="268">Nombre Cliente</th>
		<th align="center" nowrap width="110">Convenio</th>
		<th align="center" nowrap width="268">Descripci�n</th>

		<th align="center" nowrap width="100">Motivo</th>
		<th align="center" nowrap width="150">Usuario Ingreso</th>
		<th align="center" nowrap width="150">Fecha Solicitud</th>
		<th align="center" nowrap width="150">Sucursal Ingreso</th>
	</tr>

	<%
			AproveList.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (AproveList.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
		ECO050001Message pvprd = (ECO050001Message) AproveList.getRecord();
		%>
	<tr>

		<td nowrap><input type="radio" name="E01RCCTCD" id="radio_key"
			value="<%= AproveList.getCurrentRow()%>" <%=chk%> /></td>
		<td nowrap align="center"><%=pvprd.getE01CUSIDN() %></td>
		<td nowrap align="center" width="268"><%=pvprd.getE01CUSSHN() %></td>
		<td nowrap align="center" width="110"><%=pvprd.getE01COHCDE() %></td>
		<td nowrap align="left" width="268"><%=pvprd.getE01DESCDE()%></td>

		<td nowrap align="left"><%=pvprd.getE01DESMOT() %></td>
		<td nowrap align="left"><%=pvprd.getE01COHUSO() %></td>
		<td nowrap align="center">
		<%out.print(pvprd.getE01COHFSD()+"/"+pvprd.getE01COHFSM()+"/"+pvprd.getE01COHFSY()
				
				);%>
		</td>
		<td nowrap align="left"><%=pvprd.getE01COHBRS() %></td>
	</tr>
	<%
			}
		%>
</table>
<%
	}
%>
</form>
</body>
</html>
