package com.datapro.eibs.images.key;

import java.math.BigDecimal;

import com.datapro.eibs.access.KeySuper;

public class SCNIMGKEYViewSignature extends KeySuper {
	
	private BigDecimal TBLNUM = null;
	private String TBLTBN = "";
	private BigDecimal TBLSEQ = null;
	
	public BigDecimal getTBLNUM() {
		return TBLNUM;
	}
	public void setTBLNUM(BigDecimal tblnum) {
		TBLNUM = tblnum;
	}
	public BigDecimal getTBLSEQ() {
		return TBLSEQ;
	}
	public void setTBLSEQ(BigDecimal tblseq) {
		TBLSEQ = tblseq;
	}
	public String getTBLTBN() {
		return TBLTBN;
	}
	public void setTBLTBN(String tbltbn) {
		TBLTBN = tbltbn;
	}
	

}
