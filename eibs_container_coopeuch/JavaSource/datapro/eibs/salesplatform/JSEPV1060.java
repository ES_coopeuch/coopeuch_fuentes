package datapro.eibs.salesplatform;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import datapro.eibs.beans.EPV100500Message;
import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV100502Message;
import datapro.eibs.beans.EPV100503Message;
import datapro.eibs.beans.EPV101001Message;
import datapro.eibs.beans.EPV103001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV103502Message;
import datapro.eibs.beans.EPV121601Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageRecord;

public class JSEPV1060 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_PLATFORM_LIST = 100;	
	protected static final int A_PLATFORM_LIST = 101;
	
	protected static final int R_PLATFORM_INGRESO = 200;
	protected static final int R_PLATFORM_VALUACION = 300;
	protected static final int R_PLATFORM_VISADO = 400;
	
	protected static final int R_PLATFORM_INGRESO_VI_SE = 250;
	protected static final int R_PLATFORM_VALUACION_VI_SE = 350;
	protected static final int R_PLATFORM_VISADO_VI_SE = 450;	
	

	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		switch (screen) {
			case R_PLATFORM_LIST:
				procReqPlatformList(user, req, res, session);
				break;
			case A_PLATFORM_LIST:
				procActionPlatformList(user, req, res, session);
				break;
			case R_PLATFORM_INGRESO:
				procReqPlatformIngreso(user, req, res, session);
				break;
			case R_PLATFORM_INGRESO_VI_SE:
				procReqPlatformIngresoVisadoSenior(user, req, res, session);
				break;				
			case R_PLATFORM_VALUACION:
				procReqPlatformValuacion(user, req, res, session);
				break;
			case R_PLATFORM_VALUACION_VI_SE:
				procReqPlatformValuacionVisadoSenior(user, req, res, session);
				break;				
			case R_PLATFORM_VISADO:
				procReqPlatformVisado(user, req, res, session);
				break;
			case R_PLATFORM_VISADO_VI_SE:
				procReqPlatformVisadoVisadoSenior(user, req, res, session);
				break;				
			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}
	/**
	 * procReqPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		
		
		UserPos userPO = getUserPos(ses);
		userPO.setRedirect("/servlet/datapro.eibs.salesplatform.JSEPV1060?SCREEN=101");
		userPO.setHeader1("Plataforma de Ventas - Consulta");
		ses.setAttribute("userPO", userPO);
		forward("EPV1000_client_enter.jsp", req, res);
		
	}
	
	
	/**
	 * procActionPlatformList: find the list of forms depending on status, the program will epvl1005
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		String customer = req.getParameter("E01CUN") == null ? "0" : req.getParameter("E01CUN").trim();
		
		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV1005", req);
			EPV100501Message msgList = (EPV100501Message) mp.getMessageRecord("EPV100501", user.getH01USR(), "0015");
			msgList.setE01SELCUN(customer);
			msgList.setE01SELSTS(" ");

			// Sends message
			mp.sendMessage(msgList);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			//
			if (mp.hasError(error)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", error);
				forward("EPV1000_client_enter.jsp", req, res);
			} else {				
				EPV100500Message header = (EPV100500Message) list.get(0);	
				userPO.setCusNum(header.getE00CUSCUN());
				userPO.setCusType(header.getE00CUSIDN());
				userPO.setCusName(header.getE00CUSNA1());
				session.setAttribute("userPO", userPO);
				list.remove(0);
				// if there are NO errors display list
				session.setAttribute("EPV100501List", list);
				session.setAttribute("userPO", userPO);						
				forwardOnSuccess("EPV1060_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	/**
	 * procReqPlatform: This Method show a single Sales Platform for evaluation process.
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformIngreso(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		UserPos userPO = getUserPos(session);
		userPO.setPurpose("INQUIRY");
		
		String pageName = "";
		
		MessageProcessor mp = null;
		try {
			
			JBObjList list = (JBObjList) session.getAttribute("EPV100501List");
			
			if (!list.isEmpty()) {
				int row = 0;
				try {
					row = Integer.parseInt(req.getParameter("row"));
				} catch (Exception e) {
					row = 0;
				}
				
				mp = getMessageProcessor("EPV1005", req);

				EPV100501Message solicitud = (EPV100501Message) list.get(row);
				
				EPV100502Message msg = (EPV100502Message) mp.getMessageRecord("EPV100502", user.getH01USR(), "0002");

				// Sets the client number
				msg.setE02PVMCUN(solicitud.getE01PVMCUN());
				msg.setE02PVMNUM(solicitud.getE01PVMNUM());

				// Send message
				mp.sendMessage(msg);

				// Receive error and data
				ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
				msg = (EPV100502Message) mp.receiveMessageRecord();
				msg.setH02FLGWK1("I");				

				session.setAttribute("platfObj", msg);// set header page
				
				if (!mp.hasError(msgError)) {
					// if there are no errors go to maintenance page
					//buscamos los productos para el combo
					mp = getMessageProcessor("EPV1216", req);
					EPV121601Message msg1 = (EPV121601Message) mp.getMessageRecord("EPV121601");
					msg1.setH01USERID(user.getH01USR());
					msg1.setH01OPECOD("0015");
					msg1.setH01TIMSYS(getTimeStamp());
					//Sends message
					mp.sendMessage(msg1);
					//Receive insurance  list
					JBObjList list1 = mp.receiveMessageRecordList("H01FLGMAS");		 
					session.setAttribute("EPV121601List", list1);
					
					pageName = "EPV1005_salesplatform_ingreso_inqury.jsp";
				} else {
					// if there are errors go back to list page
					session.setAttribute("error", msgError);
					pageName = "EPV1060_salesplatform_list.jsp";
				}
			} else {
				pageName = "EPV1060_salesplatform_list.jsp";
			}
			
			forward(pageName, req, res);

		} finally {
			if (mp != null)
				mp.close();
		}

	}	

	protected void procReqPlatformIngresoVisadoSenior(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		UserPos userPO = getUserPos(session);
		userPO.setPurpose("INQUIRY");
		
		String pageName = "";
		
		MessageProcessor mp = null;
		try {
			
			JBObjList list = (JBObjList) session.getAttribute("EPV103502List");
			
			if (!list.isEmpty()) {
				int row = 0;
				try {
					row = Integer.parseInt(req.getParameter("row"));
				} catch (Exception e) {
					row = 0;
				}
				
				mp = getMessageProcessor("EPV1005", req);

				EPV103502Message solicitud = (EPV103502Message) list.get(row);
				
				EPV100502Message msg = (EPV100502Message) mp.getMessageRecord("EPV100502", user.getH01USR(), "0002");

				// Sets the client number
				msg.setE02PVMCUN(solicitud.getE02PVMCUN());
				msg.setE02PVMNUM(solicitud.getE02PVMNUM());

				// Send message
				mp.sendMessage(msg);

				// Receive error and data
				ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
				msg = (EPV100502Message) mp.receiveMessageRecord();
				msg.setH02FLGWK1("I");				

				session.setAttribute("platfObj", msg);// set header page
				
				if (!mp.hasError(msgError)) {
					// if there are no errors go to maintenance page
					//buscamos los productos para el combo
					mp = getMessageProcessor("EPV1216", req);
					EPV121601Message msg1 = (EPV121601Message) mp.getMessageRecord("EPV121601");
					msg1.setH01USERID(user.getH01USR());
					msg1.setH01OPECOD("0015");
					msg1.setH01TIMSYS(getTimeStamp());
					//Sends message
					mp.sendMessage(msg1);
					//Receive insurance  list
					JBObjList list1 = mp.receiveMessageRecordList("H01FLGMAS");		 
					session.setAttribute("EPV121601List", list1);
					
					pageName = "EPV1005_salesplatform_ingreso_inqury.jsp";
				} else {
					// if there are errors go back to list page
					session.setAttribute("error", msgError);
					pageName = "EPV1060_salesplatform_list.jsp";
				}
			} else {
				pageName = "EPV1060_salesplatform_list.jsp";
			}
			
			forward(pageName, req, res);

		} finally {
			if (mp != null)
				mp.close();
		}

	}
	
	protected void procReqPlatformValuacionVisadoSenior(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		UserPos userPO = getUserPos(session);
		userPO.setPurpose("INQUIRY");
		
		JBObjList list = (JBObjList) session.getAttribute("EPV103502List");
		
		int row = 0;
		try {
			row = Integer.parseInt(req.getParameter("row"));
		} catch (Exception e) {
			row = 0;
		}
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1010", req);

			EPV103502Message solicitud = (EPV103502Message) list.get(row);
			
			EPV101001Message msg = (EPV101001Message) mp.getMessageRecord("EPV101001", user.getH01USR(), "0004");

			// Sets the client number
			msg.setE01PVMCUN(solicitud.getE02PVMCUN());
			msg.setE01PVMNUM(solicitud.getE02PVMNUM());

			// Send message
			mp.sendMessage(msg);
   
			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			MessageRecord newmessage = mp.receiveMessageRecord();			

			session.setAttribute("platfObj", newmessage);// set header page
			if (!mp.hasError(msgError)) {
				if (newmessage instanceof EPV101001Message) {					
					// if there are no errors go to maintenance page
					msgError = buscarPromocionesDescuentos(user,mp,session,((EPV101001Message)newmessage).getE01LNTYPG(), solicitud.getE02PVMCUN());					
					forward("EPV1010_salesplatform_valuacion_inquiry.jsp", req, res);
				} else {
					forward("EPV1010_salesplatform_cards_maintenance.jsp?readOnly=true", req, res);
				}
				
			} else {
				// if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1060_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}	

	protected void procReqPlatformValuacion(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		UserPos userPO = getUserPos(session);
		userPO.setPurpose("INQUIRY");
		
		JBObjList list = (JBObjList) session.getAttribute("EPV100501List");
		
		int row = 0;
		try {
			row = Integer.parseInt(req.getParameter("row"));
		} catch (Exception e) {
			row = 0;
		}
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1010", req);

			EPV100501Message solicitud = (EPV100501Message) list.get(row);
			
			EPV101001Message msg = (EPV101001Message) mp.getMessageRecord("EPV101001", user.getH01USR(), "0004");

			// Sets the client number
			msg.setE01PVMCUN(solicitud.getE01PVMCUN());
			msg.setE01PVMNUM(solicitud.getE01PVMNUM());

			// Send message
			mp.sendMessage(msg);
   
			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			MessageRecord newmessage = mp.receiveMessageRecord();			

			session.setAttribute("platfObj", newmessage);// set header page
			if (!mp.hasError(msgError)) {
				if (newmessage instanceof EPV101001Message) {					
					// if there are no errors go to maintenance page
					msgError = buscarPromocionesDescuentos(user,mp,session,((EPV101001Message)newmessage).getE01LNTYPG(), solicitud.getE01PVMCUN());					
					forward("EPV1010_salesplatform_valuacion_inquiry.jsp", req, res);
				} else {
					forward("EPV1010_salesplatform_cards_maintenance.jsp?readOnly=true", req, res);
				}
				
			} else {
				// if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1060_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	/**
	 * metodo que busca la lista de productos y promociones existentes programa epv1005 
	 * @param user
	 * @param mp
	 * @param session
	 * @throws IOException
	 */
	private ELEERRMessage buscarPromocionesDescuentos(ESS0030DSMessage user,MessageProcessor mp, 
						HttpSession session, String type, String Socio) throws IOException {
		
		//Buscar Listas de Promociones y Descuentos
		EPV100503Message msg1 = null;				
		msg1 = (EPV100503Message) mp.getMessageRecord("EPV100503", user.getH01USR(), "0001");
		type = ("E".equals(type)?"D":type);
		msg1.setE03PVCREC(type);
		msg1.setE03PVCCUN(Socio);
		mp.sendMessage(msg1);
		JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");	
		if (!mp.hasError(list)) {
			session.setAttribute("EPV100503List", list);					
			return new ELEERRMessage();
		} else {
			return (ELEERRMessage) mp.getError(list);
		}
	}
	
	protected void procReqPlatformVisado(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		UserPos userPO = getUserPos(session);
		userPO.setPurpose("INQUIRY");
		
		JBObjList list = (JBObjList) session.getAttribute("EPV100501List");
		
		int row = 0;
		try {
			row = Integer.parseInt(req.getParameter("row"));
		} catch (Exception e) {
			row = 0;
		}
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1030", req);

			EPV100501Message solicitud = (EPV100501Message) list.get(row);
			
			EPV103001Message msg = (EPV103001Message) mp.getMessageRecord("EPV103001", user.getH01USR(), "0004");

			// Sets the client number
			msg.setE01PVMCUN(solicitud.getE01PVMCUN());
			msg.setE01PVMNUM(solicitud.getE01PVMNUM());

			// Send message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV103001Message) mp.receiveMessageRecord();
			session.setAttribute("document", msg);// set header page

			if (!mp.hasError(msgError)) {
				// if there are no errors go to maintenance page
				forward("EPV1030_salesplatform_documentos_inquiry.jsp", req, res);
			} else {
				// if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1060_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}	

	protected void procReqPlatformVisadoVisadoSenior(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		UserPos userPO = getUserPos(session);
		userPO.setPurpose("INQUIRY");
		
		JBObjList list = (JBObjList) session.getAttribute("EPV103502List");
		
		int row = 0;
		try {
			row = Integer.parseInt(req.getParameter("row"));
		} catch (Exception e) {
			row = 0;
		}
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1030", req);

			EPV103502Message solicitud = (EPV103502Message) list.get(row);
			
			EPV103001Message msg = (EPV103001Message) mp.getMessageRecord("EPV103001", user.getH01USR(), "0004");

			// Sets the client number
			msg.setE01PVMCUN(solicitud.getE02PVMCUN());
			msg.setE01PVMNUM(solicitud.getE02PVMNUM());

			// Send message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV103001Message) mp.receiveMessageRecord();
			session.setAttribute("document", msg);// set header page

			if (!mp.hasError(msgError)) {
				// if there are no errors go to maintenance page
				forward("EPV1030_salesplatform_documentos_inquiry.jsp", req, res);
			} else {
				// if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1060_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

 }	



