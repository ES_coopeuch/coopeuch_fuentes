<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" 	scope="session" />

<html>
<head>
<title>Nuevo Cliente</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" 	rel="stylesheet">

<script language="Javascript"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp">
	
	
</SCRIPT>

<%
   //Define variables request
  String country = currUser.getE01CTR();
  String countryDsc = currUser.getE01CTD();

  String cun="";
  String idn="";
  String tid="";
   //Define variables de la identificacion Paname�a
  String cProvincia="";
  String cLetra="";
  String cFolio="";
  String cAsiento="";
  String rRollo="";
  String rFolio="";
  String rAsiento=""; 
  String digVer="";
  
  // Define variables de la identificacion Hondure�a
  String rPlace="";
  String rYear="";
  String rSecuence="";
  
  int indexI=0;
  int indexF=0;
  
  //Obtiene los campos del request
 //Obtiene los campos del request
   if (request.getParameter("COUNTRY") != null){
        country = request.getParameter("COUNTRY");
        if (request.getParameter("COUNTRYDSC") != null) countryDsc = request.getParameter("COUNTRYDSC");        
        if (country.toUpperCase().equals("PA")){ // Panama
	        if (idn != null && !idn.equals("")){
            	idn = request.getParameter("IDN");
            	//Obtiene la direccion paname�a dependiendo del Legal Type
	      		indexI=idn.indexOf("-",0);
	      		indexF=idn.indexOf("-",indexI+1);
	      		if ((request.getParameter("TYPE").equals("PERSONAL"))&&(idn!= null)){
				     //                 09-PI-00001-001277 70         
				     //                 Pr Lt Folio Asien  DV         
				     //             Provincia  letra Folio  Asiento DV 
				    cProvincia=idn.substring(0,indexI );
				    cLetra=idn.substring(indexI+1,indexF );
				    indexI=indexF;
				    indexF=idn.indexOf("-",indexI+1);
				    cFolio=idn.substring(indexI+1,indexF);
				    indexI=indexF;
				    indexF=idn.indexOf(" ",indexI+1);
				    if (indexF>0 ){
					    cAsiento=idn.substring(indexI+1,indexF);
					    digVer=idn.substring(indexF+1);			    
				    }else{cAsiento=idn.substring(indexI+1);}  
	
			       System.out.println("cProvincia:" + cProvincia);
			       System.out.println("cLetra:" + cLetra);
			       System.out.println("cFolio: "+ cFolio);
			       System.out.println("cAsiento: "+ cAsiento);
			       //System.out.println("Digito verif: "+ digVer);

	      		}else if ((request.getParameter("TYPE").equals("CORPORATIVE"))&&(idn!= null)){
				    //                 0055496-0012-00304533 50
				    //                 Rollo  Folio  Asiento DV      
				    rRollo=idn.substring(0,indexI);
				    rFolio=idn.substring(indexI+1,indexF);
				    indexI=indexF;
				    indexF=idn.indexOf(" ",indexI+1);
				    if (indexF>0 ){
					    rAsiento=idn.substring(indexI+1,indexF);
					    digVer=idn.substring(indexF+1);			    
				    }else{rAsiento=idn.substring(indexI+1);}  
				       System.out.println("rRollo:" + rRollo);
				       System.out.println("rFolio: "+ rFolio);
				       System.out.println("rAsiento: "+ rAsiento);
				       System.out.println("Digito verif: "+ digVer);    
			    }
			}			
		}else
		
		 if (country.toUpperCase().equals("HN")){ // Honduras
	        idn = request.getParameter("IDN");
	        if (idn != null && !idn.equals("")){
            	idn = request.getParameter("IDN");
            	//Obtiene la direccion paname�a dependiendo del Legal Type
	      		indexI=idn.indexOf("-",0);
	      		indexF=idn.indexOf("-",indexI+1);
	      		 if ((request.getParameter("TYPE").equals("PERSONAL"))&&(idn!= null)){
				    //                 0801-1900-00304 
				    //                 Lugar Nacimiento Ano  Consecutivo     
				    rPlace=idn.substring(0,indexI);
				    rYear=idn.substring(indexI+1,indexF);
				    indexI=indexF;
				    indexF=idn.indexOf(" ",indexI+1);
				    if (indexF>0 ){
					    rSecuence=idn.substring(indexI+1,indexF);			    
				    }else{rSecuence=idn.substring(indexI+1);}  
				    System.out.println("rPlace:" + rPlace);
				    System.out.println("rYear: "+ rYear);
				    System.out.println("rSecuence: "+ rSecuence);
				   
			    }
			    else {  // Corporative
                  if (request.getParameter("IDN")!= null) idn = request.getParameter("IDN");               
                  }		
			    
			
			}else {  // venezuela y otros
            if (request.getParameter("IDN")!= null) idn = request.getParameter("IDN");               
        }		
		
		}	
        
   }
 %>



<script language="JavaScript">

 function enterInteger1 (idType) {

	 if (idType.value != 'P') enterInteger();
 }

 function joinID(idField, idType, idNum){

	  var strID = "";
	    while ((idNum.value.length < 9) && (idType.value!='P'))
	      idNum.value='0'+idNum.value;
	  strID = trim(idType.value) + trim(idNum.value);
      idField.value=strID.toUpperCase();
}

 function getIdTypeHelp(){
   // Get the client type selected
   var clientTypeSelected="CORPORATIVE";
   for (counter = 0; counter < document.forma.TYPE.length; counter++)
   {
      if (document.forma.TYPE[counter].checked)
        clientTypeSelected = document.forma.TYPE[counter].value; 
   }
   // Display the id screen help 
   if (clientTypeSelected == "CORPORATIVE") 
     GetCode('VIDN0','STATIC_client_help_id_type.jsp?clientType=CORPORATIVE');
   else
     GetCode('VIDN0','STATIC_client_help_id_type.jsp?clientType=PERSONAL');
 }
 
function showIdFields(countryCode,institution) {

	var cusType = getCheckedValue(document.forms[0].elements["TYPE"]) ;
	
	if ( institution != "05" ) {  // Not Colombia
		if ( countryCode.toUpperCase() == "VE" ) {
				document.getElementById('IDVEN').style.display='block' ;
				document.getElementById('IDCOL').style.display='none' ;
				document.getElementById('IDOTHER').style.display='none' ;
				document.getElementById('IDPAPERS').style.display='none' ;
				document.getElementById('IDPACORP').style.display='none' ;
				document.getElementById('IDHONPERS').style.display='none' ; 
		} else if( countryCode.toUpperCase() == "PA" && cusType.toUpperCase() == "CORPORATIVE" ){
				document.getElementById('IDCOL').style.display='none' ;
				document.getElementById('IDVEN').style.display='none' ;
				document.getElementById('IDOTHER').style.display='none' ;
				document.getElementById('IDPAPERS').style.display='none' ;
				document.getElementById('IDPACORP').style.display='block' ;
				document.getElementById('IDHONPERS').style.display='none' ;
		} else if( countryCode.toUpperCase() == "PA" && cusType.toUpperCase() == "PERSONAL" ){
				document.getElementById('IDCOL').style.display='none' ;
				document.getElementById('IDVEN').style.display='none' ;
				document.getElementById('IDOTHER').style.display='none' ;
				document.getElementById('IDPAPERS').style.display='block' ;
				document.getElementById('IDPACORP').style.display='none' ;
				document.getElementById('IDHONPERS').style.display='none' ;
		} else if( countryCode.toUpperCase() == "HN" && cusType.toUpperCase() == "PERSONAL" ){
				document.getElementById('IDCOL').style.display='none' ;
				document.getElementById('IDVEN').style.display='none' ;
				document.getElementById('IDOTHER').style.display='none' ;
				document.getElementById('IDPAPERS').style.display='none' ;
				document.getElementById('IDPACORP').style.display='none' ;
				document.getElementById('IDHONPERS').style.display='block' ;
		} else if( countryCode.toUpperCase() != "" ){
				document.getElementById('IDCOL').style.display='none' ;
				document.getElementById('IDVEN').style.display='none' ;
				document.getElementById('IDOTHER').style.display='block' ;
				document.getElementById('IDPAPERS').style.display='none' ;
				document.getElementById('IDPACORP').style.display='none' ;
				document.getElementById('IDHONPERS').style.display='none' ;
		}
	}	
}

function fill0Left(idField){

	  //alert("**idFieldNAME: "+idField.name + " **Longitud: " + idField.value.length + " **MaxLongitud: " + idField.maxLength);
	    while (idField.value.length < idField.maxLength )
	      idField.value='0'+idField.value;	  	 
}

function joinPersonalIDFieldsPA(){

	document.forms[0].IDN.value=document.forms[0].PIDN0.value + "-" +
								document.forms[0].PIDN1.value + "-" +
								document.forms[0].PIDN2.value + "-" +
								document.forms[0].PIDN3.value ;

								
}

function joinCorpIDFieldsPA(){

    var dv = "" ;
	dv = document.forms[0].elements["HIDN3"].value;

	if( dv == "" || dv == "undefined" || dv == null ){
		dv = "  " ;
	} else if (dv.length < 2) {
		dv = " " + dv ;
	}

	document.forms[0].IDN.value=document.forms[0].CIDN0.value + "-" +
								document.forms[0].CIDN1.value + "-" +
								document.forms[0].CIDN2.value + " " +
							    dv ;
}

function joinPersonalIDFieldsHON(){

	document.forms[0].IDN.value=document.forms[0].HIDN0.value + "-" +
								document.forms[0].HIDN1.value + "-" +
								document.forms[0].HIDN2.value ;
								
}



 // return the value of the radio button that is checked
 // return an empty string if none are checked, or
 // there are no radio buttons
function getCheckedValue(radioObj) {
	if(!radioObj)
		return "";
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
			return radioObj[i].value;
		}
	}
	return "";
}

function stripZerosAtLeft( valObj ){

	if( valObj.value != "" && !isNaN(valObj.value) ){
		valObj.value = parseInt(valObj.value , 10) ;
	}
}

	function validate() {
		if (document.getElementById("OTIDO").value == "RUT") {
			var rut = validateRut(document.getElementById("OIDNO").value, <%=currUser.getE01INT()%>);
			if (rut == 0) {
				alert("!! Error: El Rut no es valido. !!");
				return;
			} else {
				document.getElementById("OIDNO").value = rut;
				document.getElementById("IDN").value = rut;
			}	
		}
		document.forms[0].submit();
	}

</script>

</head>

<body bgcolor="#FFFFFF">

<h3 align="center">Nuevo Cliente<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="client_both_enter_new, ESD0080"></h3>
<hr size="4">
<form name="forma" method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080">
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="400">

<% if (currUser.getE01INT().equals("05")) {   // Colombia %>
	<h4 align="center">Ingrese Pa�s, Tipo de Cliente, Tipo de Identificaci�n e Identificaci�n.</h4>
<% } else { %>
<% 	if (currUser.getH01WK2().equals("1")) { // Access by Customer Number  %>  
	<h4 align="center">Ingrese N�mero del Cliente IBS.</h4>
<% } else { %>
	<h4 align="center">Ingrese Identificaci�n del Cliente. BBBB</h4>
<% } %>
<% } %>
<p>&nbsp;</p>
<table class="tableinfo">

	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trdark">
				<td nowrap align="right"  width="30%">Pa�s : </td>
				<td colspan="2">
				<INPUT TYPE=HIDDEN NAME="INT" value=""> 
				<INPUT TYPE="text" NAME="COUNTRY" value="<%=country%>" size="5" maxlength="4" 
					onfocus="showIdFields(document.forms[0].COUNTRY.value,'<%=currUser.getE01INT()%>');" 
					onblur="showIdFields(document.forms[0].COUNTRY.value,'<%=currUser.getE01INT()%>');"
				>		  
				<INPUT TYPE="text" NAME="COUNTRYDSC" value="<%=countryDsc%>" readonly size="45"
					onfocus="showIdFields(document.forms[0].COUNTRY.value,'<%=currUser.getE01INT()%>');"
				> 
				<a href="javascript:GetCodeDescCNOFC('COUNTRY','COUNTRYDSC','03')"> 
				<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0">
				</a>
				<% 	if (currUser.getE01INT().equals("05")) {   // Colombia  %>			
						<IMG src="<%=request.getContextPath()%>/images/Check.gif" alt="Campo Obligatorio" align="bottom">	
				<% 	} %>
				 
				</td>
			</tr>

			<tr id="trclear">
				<td nowrap align="right" width="30%">Tipo de Cliente :</td>
				<td colspan="2">
				<% 	if (currUser.getE01INT().equals("18")) {   		
					tid="RUT";
				} %>
				<INPUT TYPE=HIDDEN NAME="IDN" value=""> 
				<label for="corp" >
				<input type="radio" id="corp" name="TYPE" value="CORPORATIVE" onclick="showIdFields(document.forms[0].COUNTRY.value,'<%=currUser.getE01INT()%>');" >Empresa 
				</label>
				<label for="pers" >
				<input type="radio" id="pers" name="TYPE" value="PERSONAL" checked="checked" onclick="showIdFields(document.forms[0].COUNTRY.value,'<%=currUser.getE01INT()%>');" >Persona
				</label>
				<% 	if (!currUser.getE01INT().equals("05")) { // Not Colombia%>
				<label for="other" >
				<input type="radio" id="other" name="TYPE" value="OTHER" onclick="showIdFields(document.forms[0].COUNTRY.value,'<%=currUser.getE01INT()%>');" >Otro
				</label>
				<% 	} else { %> 
				<label for="userC" >
				<input type="radio" id="other" name="TYPE" value="USERC" onclick="showIdFields(document.forms[0].COUNTRY.value,'<%=currUser.getE01INT()%>');" >Usuario Empresa
				</label>	
				<label for="userP" >
				<input type="radio" id="other" name="TYPE" value="USERP" onclick="showIdFields(document.forms[0].COUNTRY.value,'<%=currUser.getE01INT()%>');" >Usuario Persona 
				</label>	
				<%	}%>
				</td>
			</tr>
		</table>

		<div id="IDVEN" style="position:relative; display:none;">
		<table  cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trdark">
				<td nowrap align="right" width="30%">N&uacute;mero de Cliente :</td>
				<td nowrap align="left" colspan="2">
				<input type="text" name="CUN" value="0" maxlength="10" size="11" onKeypress="enterInteger()">
				</td>
			</tr>		
			<tr id="trclear">
				<td nowrap align="right" width="30%">Identificaci�n :
				</td>
				<td nowrap align="left" valign="middle" colspan="2">
				<input type="text"  name="VIDN0" value="" maxlength="1" size="1" readonly
					onchange="joinID(document.forms[0].IDN, document.forms[0].VIDN0, document.forms[0].VIDN1);">
				<a href="javascript:getIdTypeHelp()"> 
				<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ."
					align="bottom" border="0">
				</a> 
				<input type="text" name="VIDN1" value="<%=idn%>" maxlength="25" size="28"
					onKeypress="enterInteger1(document.forms[0].VIDN0)"
					onchange="joinID(document.forms[0].IDN, document.forms[0].VIDN0, document.forms[0].VIDN1);">

				</td>
			</tr>
		</table>
		</div>

		<div id="IDCOL" style="position:relative; display:none;">
		<table  cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trdark">
				<td nowrap align="right" valign="middle" width="30%">Tipo e Identificaci�n : </td>
				<td nowrap align="left" valign="middle" colspan="2">
					<input type="text" name="TID" value="<%=tid%>" maxlength="4" size="5" readonly>
					<a href="javascript:GetCodeDescCNOFC('TID','','34')"> 
						<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0">
					</a> 
					<input type="text" name="IDNCOL" value="" maxlength="25" size="28" onblur="document.forms[0].IDN.value=this.value">
					<IMG src="<%=request.getContextPath()%>/images/Check.gif" alt="Campo Obligatorio" align="bottom">
				</td>
			</tr>
			<% 	if (currUser.getH01WK2().equals("1")) { // Access by Customer Number  %>  
				<tr id="trclear">
					<td nowrap align="right" valign="middle" width="30%">N�mero de Cliente :</td>
					<td nowrap align="left" valign="middle" colspan="2">
						<input type="text" name="CUN" value="0" maxlength="10" size="11" onKeypress="enterInteger()">
					</td>
				</tr>
			<%  } %>	
		</table>
		</div>

		<div id="IDOTHER" style="position:relative; display:none;">
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
		<% 	if (currUser.getH01WK2().equals("1")) { // Access by Customer Number  %>  
			<tr id="trdark">
				<td nowrap align="right" valign="middle">N�mero de Cliente :</td>
				<td nowrap align="left" valign="middle" colspan="3">
				<input type="text" name="OCUNO" value="<%=cun%>" maxlength="10" size="11" 
				 onblur="document.forms[0].IDN.value=this.value" onKeypress="enterInteger()">
				</td>
			</tr>
			<%  } else { %>	
		
			<tr id="trclear">
				 <td nowrap align="right" width="30%">Identificaci�n : </td> 
				<% 	if (currUser.getE01INT().equals("11")) {   // Colombia  %>	
				 <td nowrap align="left" width="10%">
					  <input type="text" name="OIDNO" value="<%=idn%>" maxlength="15" size="16"
					  onblur="document.forms[0].IDN.value=this.value" >
  	  	         </td>
				<% } else { %>
				  <td nowrap align="left" width="10%">
					  <input type="text" name="OIDNO" value="<%=idn%>" maxlength="25" size="28"
					  onblur="document.forms[0].IDN.value=this.value" >
				  </td>
				 <%  } %> 
				<td nowrap align="right" width="20%">Tipo : </td> 
				<td nowrap align="left" width="40%">
					<input id="OTIDO" type="text" name="OTIDO" value="<%=tid%>" maxlength="4" size="5" onblur="document.forms[0].TID.value=this.value" >
					<a href="javascript:GetCodeDescCNOFC('OTIDO','','34')"> 
						<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0">
					</a> 
				</td>
			</tr>
		 <%  } %> 
			
		</table>
		</div>

        <div id="IDPACORP" style="position:relative; display:none;">
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trclear">
				<td nowrap align="right" valign="middle" width="30%">Identificaci�n:
				</td>
				<td nowrap align="left" valign="middle" colspan="2">
				<input type="text" name="CIDN0" value="<%=rRollo%>" maxlength="7" size="8" onKeyPress="enterInteger();"
				onblur="fill0Left(this);joinCorpIDFieldsPA();" > - 
				<input type="text" name="CIDN1" value="<%=rFolio%>" maxlength="4" size="5" onKeyPress="enterInteger();"
				onblur="fill0Left(this);joinCorpIDFieldsPA();" > - 
				<input type="text" name="CIDN2" value="<%=rAsiento%>" maxlength="8" size="9" onKeyPress="enterInteger();"
				onblur="fill0Left(this);joinCorpIDFieldsPA();" >
				<input type="hidden" name="CIDN3" value="00" maxlength="2" size="3" onKeyPress="enterInteger();"
				onblur="joinCorpIDFieldsPA();document.forms[0].EIBSBTN.focus();" >
				</td>
			</tr>
		</table>
		</div>
		
		<div id="IDPAPERS" style="position:relative; display:none;">
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trclear">
				<td nowrap align="right" valign="middle" width="30%">Identificaci�n:
				</td>
				<td nowrap align="left" valign="middle" colspan="2">
				<input type="text" name="PIDN0" value="<%=cProvincia %>" maxlength="2" size="3" onKeyPress="enterInteger();"
				onblur="fill0Left(this);joinPersonalIDFieldsPA();" > - 
				<input type="text" name="PIDN1" value="<%=cLetra  %>" maxlength="2" size="3"
				onblur="this.value=this.value.replace(/ /ig,'');joinPersonalIDFieldsPA();" > - 
				<input type="text" name="PIDN2" value="<%=cFolio  %>" maxlength="5" size="6" onKeyPress="enterInteger();"
				onblur="fill0Left(this);joinPersonalIDFieldsPA();" > - 
				<input type="text" name="PIDN3" value="<%=cAsiento  %>" maxlength="6" size="7" onKeyPress="enterInteger();"
				onblur="fill0Left(this);joinPersonalIDFieldsPA();" >
				<%-- 
				<input type="text" name="PIDN4" value="  " maxlength="2" size="4"
				onblur="joinPersonalIDFieldsPA();" readonly="readonly" >
				--%>
				</td>
			</tr>
		</table>
		</div>
  
        <div id="IDHONPERS" style="position:relative; display:none;">
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trclear">
				<td nowrap align="right" valign="middle" width="30%">Identificaci�n:
				</td>
				<td nowrap align="left" valign="middle" colspan="2">
				<input type="text" name="HIDN0" value="<%=rPlace%>" maxlength="4" size="5" onKeyPress="enterInteger();"
				onblur="fill0Left(this);joinPersonalIDFieldsHON();" > - 
				<input type="text" name="HIDN1" value="<%=rYear%>" maxlength="4" size="5" onKeyPress="enterInteger();"
				onblur="fill0Left(this);joinPersonalIDFieldsHON();" > - 
				<input type="text" name="HIDN2" value="<%=rSecuence%>" maxlength="5" size="6" onKeyPress="enterInteger();"
				onblur="fill0Left(this);joinPersonalIDFieldsHON();" >
				</td>
			</tr>
		</table>
		</div>
		
		
		</td>
	</tr>

</table>
<p align="center">
<% 	if (currUser.getE01INT().equals("18")) {   // Chile  %>	
<input id="EIBSBTN" type=button name="Submit" value="Enviar" onclick="validate()">
<%}else{ %>
<input id="EIBSBTN" type=Submit name="Submit" value="Enviar"">
<%}%>
</p>
</form>

<SCRIPT Language="Javascript">;
<%if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");	
%>
      showErrors();
<% } %>

<% 	if (currUser.getE01INT().equals("05")) {   // Colombia   %>
		document.getElementById('IDCOL').style.display='block' ;
		document.getElementById('IDVEN').style.display='none' ;
		document.getElementById('IDOTHER').style.display='none' ;
		document.getElementById('IDPAPERS').style.display='none' ;
		document.getElementById('IDPACORP').style.display='none' ;
		document.getElementById('IDHONPERS').style.display='none' ;
		
<% } else { %>
		showIdFields('<%= country %>','<%=currUser.getE01INT()%>');
<% } %>		
		
</SCRIPT>
</body>
</html>
