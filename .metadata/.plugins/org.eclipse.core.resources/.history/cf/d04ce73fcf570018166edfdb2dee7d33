package datapro.eibs.salesplatform;

/**
 * Plataforma de Ventas -ingreso / mantenimiento y simulacion de planillas
 * @author evargas
 * modify by Alonso Arana 16/04/2014---------Datapro------------------------l�gica de medio de evaluaci�n   
 */
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.coopeuch.creditos.predictorriesgo.sinacofi2.DatosConsultaSum;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenAntecedentesComercialesResponse;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiPortType;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiSOAPProxy;
import cl.coopeuch.creditos.predictorriesgo.sinacofi2.ResumenSinacofiTypeType;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100500Message;
import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV100502Message;
import datapro.eibs.beans.EPV100505Message;
import datapro.eibs.beans.EPV100506Message;
import datapro.eibs.beans.EPV121601Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSEPV1005 extends JSEIBSServlet { 
 
	private static final long serialVersionUID = -5013250952357918505L;
	 
	protected static final int R_PLATFORM_LIST = 100;  
	protected static final int A_PLATFORM_LIST = 101;
	protected static final int R_PLATFORM_NEW = 200;
	protected static final int R_PLATFORM_MAINT = 201;
	protected static final int R_PLATFORM_MAINT_REJECT = 301;	
	protected static final int A_PLATFORM_MAINTENANCE = 600;
	protected static final int A_PLATFORM_SIMULATE = 601;
	protected static final int A_PLATFORM_HIST_INCOME = 700;		
	protected static final int R_PLATFORM_SINACOFI = 850;	
	protected static final int R_PLATFORM_LIST_CAMP = 1000;
	protected static final int A_PLATFORM_LIST_CAMP = 1001;
	
	/**
	 *  
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_PLATFORM_LIST:
			procReqPlatformList(user, req, res, session);
			break;
		case A_PLATFORM_LIST:
			procActionPlatformList(user, req, res, session);
			break;
		case R_PLATFORM_NEW:
			procReqPlatform(user, req, res, session, "NEW");
			break;
		case R_PLATFORM_MAINT:
			procReqPlatform(user, req, res, session, "MAINTENANCE");
			break;
		case R_PLATFORM_MAINT_REJECT:
			procReqPlatformReject(user, req, res, session);
			break;			
		case A_PLATFORM_MAINTENANCE:
			procActionMaintenance(user, req, res, session);
			break;
		case A_PLATFORM_SIMULATE:
			procActionSimulation(user, req, res, session);
			break;
		case R_PLATFORM_SINACOFI:
			procReqPlatformSinacofi(user, req, res, session);
			break;
		case A_PLATFORM_HIST_INCOME:
			procActionPlatformHistoricalIncome(user, req, res, session);
			break;				
		case A_PLATFORM_LIST_CAMP:
			procActionPlatformListCamp(user, req, res, session);
			break;		
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	
	/**
	 * procReqPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
		UserPos userPO = getUserPos(ses);

		//solo para  retencion de cliente
		ses.removeAttribute("rentencion");
		String reten = req.getParameter("rentencion");
		if ("S".equals(reten)){
			ses.setAttribute("rentencion", "S");
		}									
		userPO.setRedirect("/servlet/datapro.eibs.salesplatform.JSEPV1005?SCREEN=101");
		userPO.setHeader1("Plataforma de Ventas - Ingreso");
		ses.setAttribute("userPO", userPO);
		forward("EPV1000_client_enter.jsp", req, res);
	}
	
	
	/**
	 * procActionPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		
		String customer = req.getParameter("E01CUN") == null ? "0" : req.getParameter("E01CUN").trim();
		
		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV1005", req);
			EPV100501Message msgList = (EPV100501Message) mp.getMessageRecord("EPV100501", user.getH01USR(), "0015");
			msgList.setE01SELCUN(customer);
			msgList.setE01SELSTS("1");
			// Sends message
			mp.sendMessage(msgList);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			//
			if (mp.hasError(error)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", error);
				forward("EPV1000_client_enter.jsp", req, res);
			} else {				
				EPV100500Message header = (EPV100500Message) list.get(0);	
				userPO.setCusNum(header.getE00CUSCUN());
				userPO.setCusType(header.getE00CUSIDN());
				userPO.setCusName(header.getE00CUSNA1());
				session.setAttribute("userPO", userPO);
				list.remove(0);
				// if there are NO errors display list
				mp = getMessageProcessor("EPV1005", req);
				EPV100506Message msgListCam = (EPV100506Message) mp.getMessageRecord("EPV100506", user.getH01USR(), "0001");
				msgListCam.setE06PCACUN(userPO.getCusNum());
				// Sends message
				mp.sendMessage(msgListCam);

				ELEERRMessage error1 = (ELEERRMessage) mp.receiveMessageRecord();
				JBObjList listCam = mp.receiveMessageRecordList("H06FLGMAS");
				// save customer number
				
				session.setAttribute("EPV100501List", list);
				session.setAttribute("EPV100506List", listCam);
				session.setAttribute("userPO", userPO);							
				forward("EPV1005_salesplatform_list.jsp", req, res);
			}
			
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	protected void procActionPlatformHistoricalIncome(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV1005", req);
			EPV100505Message msgList = (EPV100505Message) mp.getMessageRecord("EPV100505", user.getH01USR(), "0016");
			msgList.setE05PVMCUN(req.getParameter("E05PVMCUN"));
			// Sends message
			mp.sendMessage(msgList);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", error);
				flexLog("About to call Page: EPV1005_salesplatform_historical_income.jsp");
				forward("EPV1005_salesplatform_historical_income.jsp", req, res);
			} else {
				// Receive salesPlatform list
				JBObjList list = mp.receiveMessageRecordList("H05FLGMAS");
				// if there are NO errors display list
				session.setAttribute("EPV100505List", list);
				forwardOnSuccess("EPV1005_salesplatform_historical_income.jsp", req, res);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	
	protected void procReqPlatformReject(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		MessageProcessor mp = null;		
		try {
			UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
			mp = getMessageProcessor("EPV1005", req);

			EPV100501Message msg = null;

			// Creates the message with operation code depending on the option			
			msg = (EPV100501Message) mp.getMessageRecord("EPV100501", user.getH01USR(), "0007");
			
			// Sets the client number
            
			// Sets the platform number for maintenance
			if (req.getParameter("E01PVMNUM") != null) {
				msg.setE01PVMNUM(req.getParameter("E01PVMNUM"));
			}
          
			if (userPO.getCusNum() != null){
				msg.setE01PVMCUN(userPO.getCusNum());
			}	
			// Send message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();			
			session.setAttribute("error", msgError);
			redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1005?SCREEN=101&E01CUN="+ userPO.getCusNum(), res);			

		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	
	/**
	 * procReqPlatform: This Method show a single Sales Platform either for new
	 * registry or maintenance.
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatform(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		MessageProcessor mp = null;	
		EPV100506Message  msgSel = null;
		
		try {
			UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
			userPO.setPurpose(option);
//-------------------------
			if (req.getParameter("E06CODCAM") != null && !req.getParameter("E06CODCAM").equals("")) 
			{
				JBObjList bl = (JBObjList) session.getAttribute("EPV100506List");
				int idx = 0;
				try {
					idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
				} catch (Exception e) {
					throw new ServletException(e);
				}
				bl.setCurrentRow(idx);
				msgSel = (EPV100506Message) bl.getRecord();
			}
//-------------------------			
			mp = getMessageProcessor("EPV1005", req);

			EPV100502Message msg = null;

			// Creates the message with operation code depending on the option
			if (option.equals("NEW")) {
				// New
				msg = (EPV100502Message) mp.getMessageRecord("EPV100502", user.getH01USR(), "0001");
			} else if (option.equals("MAINTENANCE")) {
				// Maintenance
				msg = (EPV100502Message) mp.getMessageRecord("EPV100502", user.getH01USR(), "0002");
			}           
			// Sets the client number
            
			// Sets the platform number for maintenance
			if (req.getParameter("E01PVMNUM") != null) {
				msg.setE02PVMNUM(req.getParameter("E01PVMNUM"));
			}

			if (userPO.getCusNum() != null)
			{
				msg.setE02PVMCUN(userPO.getCusNum());
			}
			else
			{
			  if (msgSel.getE06PCACUN() != null && req.getParameter("E06CODCAM") != null) {
				msg.setE02PVMCUN(msgSel.getE06PCACUN());				
			  }else{
				msg.setE02PVMCUN("0");
			  }
			}	

			if (req.getParameter("E06CODCAM") != null) 
			{
				if (!req.getParameter("E06CODCAM").equals("")) 
				{
					msg.setE02PVMIDN(userPO.getCusType());
					msg.setE02PVMCAM(msgSel.getE06PCACCA());
					msg.setE02PCACUN(userPO.getCusNum());
					msg.setE02PCACCA(msgSel.getE06PCACCA());
					msg.setE02PCAVHD(msgSel.getE06PCAVHD());
					msg.setE02PCAVHM(msgSel.getE06PCAVHM());
					msg.setE02PCAVHY(msgSel.getE06PCAVHY());
					msg.setE02PCATYP(msgSel.getE06PCATYP());
					msg.setE02PCATEV(msgSel.getE06PCATEV());
					msg.setE02PCAPLZ(msgSel.getE06PCAPLZ());
					msg.setE02PCAAMT(msgSel.getE06PCAAMT());
				}	
			}
			//set only warning 
			if (req.getParameter("WARNING") != null && "A".equals(req.getParameter("WARNING"))) {
				msg.setH02FLGWK2(req.getParameter("WARNING"));//set A value
			}

			// Send message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV100502Message) mp.receiveMessageRecord();
			
			//just to know when maintenance
			if (option.equals("MAINTENANCE")) {
				msg.setH02FLGWK1("M");
				msg.setH02FLGWK2("X");//ya es mantenimiento, debe haber simulado				
			}

			session.setAttribute("platfObj", msg);// set header page
			if (!mp.hasError(msgError)) {
				// if there are no errors go to maintenance page				 
			
				//buscamos los productos para el combo
				try {
					mp = getMessageProcessor("EPV1216", req);
					EPV121601Message msg1 = (EPV121601Message) mp.getMessageRecord("EPV121601");
					msg1.setH01USERID(user.getH01USR());
					//vemos que opecod mandamos: nomal=0016 retencion=0018
					String reten = (String)session.getAttribute("rentencion");
					String opecod = ("S".equals(reten))?"0018":"0016";
					msg1.setH01OPECOD(opecod);
					msg1.setH01TIMSYS(getTimeStamp());
					//Sends message
		
					mp.sendMessage(msg1);
					//Receive insurance  list
					
					
					JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");		 
					session.setAttribute("EPV121601List", list);
				} catch (Exception e) {
					JBObjList list = new JBObjList();		 
					session.setAttribute("EPV121601List", list);					
					e.printStackTrace();
				}				
				
				flexLog("About to call Page: EPV1005_salesplatform_maintenance.jsp");
				forward("EPV1005_salesplatform_maintenance.jsp", req, res);
			} else {
				// if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1005_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	
	/**
	 *  Process the Maintenance
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			req.setAttribute("SINACOFI",req.getParameter("SINACOFI")) ;//dejar guardado el estado para controlar consulta cedula
		}
		catch (Exception e) {
			req.setAttribute("SINACOFI","");
		}
		
		try {
			mp = getMessageProcessor("EPV1005", req);

			EPV100502Message msg = (EPV100502Message) mp.getMessageRecord("EPV100502", user.getH01USR(), "0003");

			// Sets message with page fields
			msg.setH02SCRCOD("01");
			setMessageRecord(req, msg);
			
			// Sending message
			flexLog("Mensaje enviado...procActionMaintenance .." + msg);
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV100502Message) mp.receiveMessageRecord();

			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				session.setAttribute("platfObj", msg);
				forward("EPV1005_salesplatform_maintenance.jsp", req, res);
				return;
			}			
		} finally {
			if (mp != null)
				mp.close();
		}
		
		
		try {
			mp = getMessageProcessor("EPV1005", req);

			EPV100502Message msg = (EPV100502Message) mp.getMessageRecord("EPV100502", user.getH01USR(), "0005");

			// Sets message with page fields
			msg.setH02SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);
			
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV100502Message) mp.receiveMessageRecord();
			
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			
			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and sho errors
				session.setAttribute("platfObj", msg);
				forward("EPV1005_salesplatform_maintenance.jsp", req, res);			
			} else {
				//go to evaluation
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1010?SCREEN=201&E01PVMNUM="+ msg.getE02PVMNUM()+"&DOP="+msg.getE02PVMDOP(), res);				
				
			}

		} finally {
			if (mp != null)
				mp.close();
		}		
	}
	
	
	/**
	 * Process the simulation for request
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionSimulation(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;

		try {	
			mp = getMessageProcessor("EPV1005", req);
			EPV100502Message msg = (EPV100502Message) mp.getMessageRecord("EPV100502", user.getH01USR(), "0012");			

			// Sets message with page fields
			setMessageRecord(req, msg);


			
			// Sending message
			flexLog("Mensaje enviado...procActionSimulation .." + msg);

			mp.sendMessage(msg);

			// Receive error and data
			msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV100502Message) mp.receiveMessageRecord();
			
			if (mp.hasError(msgError)) {
				msg.setH02FLGWK2("");//si tiene error sigo simulando	
			}else{
				msg.setH02FLGWK2("X");//NO hubo error simulo
			}
			req.setAttribute("ESTADO",req.getParameter("ESTADO")) ;//dejar guardado el estado para mostrar deuda SBIF
			req.setAttribute("SINACOFI",req.getParameter("SINACOFI")) ;//dejar guardado el estado para controlar consulta cedula
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("platfObj", msg);// seteamos nuevamente con la informacion obtenida
			req.setAttribute("SIMULAR", "S");
			forward("EPV1005_salesplatform_maintenance.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	/**
	 * Metodo que invoca el webservice de sinacofi expuesto por coopeuch
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformSinacofi(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			EPV100502Message msg = (EPV100502Message) session.getAttribute("platfObj");
			setMessageRecord(req, msg);		
						
			
			try {
				String rut_s = msg.getE02PVMIDN().substring(0, msg.getE02PVMIDN().length()-1);
				String dv_s = msg.getE02PVMIDN().substring(msg.getE02PVMIDN().length()-1);
				BigInteger rut = new BigInteger(rut_s);
				String serie = msg.getE02PVMIDS();				
				String rutEmp_s = msg.getE02PVMCPR().substring(0, msg.getE02PVMCPR().length()-1);
				String dvEmp_s = msg.getE02PVMCPR().substring(msg.getE02PVMCPR().length()-1);
				BigInteger rutEmp = new BigInteger(rutEmp_s);
				
				String medio_evaluacion=msg.getE02PVMTPR();
				
				DatosConsultaSum parameters = new DatosConsultaSum();			
				parameters.setRut(rut);
				parameters.setDv(dv_s);
				parameters.setSerie(serie);
				parameters.setRutEmpleador(rutEmp);
				parameters.setDvEmpleador(dvEmp_s);	
		
				
				// logica medio de evaluacion
				
					if(medio_evaluacion.equals("PP")){//pago planilla
						
						parameters.setConsultas("A3,A2,A1");
						
					}
					
					else if(medio_evaluacion.equals("PD")){//pago directo
						
						parameters.setConsultas("A3,A2,A1,B");	
					}
					
					else if(medio_evaluacion.equals("CC")){// cartera directo
						
						parameters.setConsultas("A3,A2,A1,B");	
					}
					
					else if(medio_evaluacion.equals("FA")){// credito fach
						
						parameters.setConsultas("A3,A2,A1");	
					}
		
					else if(medio_evaluacion.equals("RD")){//renogociado directo
						
						
						parameters.setConsultas("A3");	
					}	
					
					else if(medio_evaluacion.equals("RP")){//renogociado planilla
						
						parameters.setConsultas("A3");	
					}
					
					else if(medio_evaluacion.equals("RS")){//renogociado cesante
						
						parameters.setConsultas("A3");	
					}
					
					
					else if(medio_evaluacion.equals("TC")){ // tarjeta de credito
						
						parameters.setConsultas("A3,A2,A1,B");	
					}
					
					
					else{
						parameters.setConsultas("A3,A2,A1,B");		
						
					}
					
				
				ResumenSinacofiSOAPProxy wsp = new ResumenSinacofiSOAPProxy();//inicializamos el servicio					
				ResumenAntecedentesComercialesResponse respWrapper  = wsp.resumenAntecedentesComerciales(parameters);
				if (respWrapper.getCodError()==null || "0".equals(respWrapper.getCodError()) 
						|| "".equals(respWrapper.getCodError().trim())){//no hay errores..
					ResumenSinacofiTypeType resp = respWrapper.getResumenSinacofiType();
					//seteamos las variables que responde
					msg.setE02PVPU12(new BigDecimal(resp.getCantidadProtestoAnual())); //Inf - cantidad protesto 12 meses
					msg.setE02PVP12A(new BigDecimal(resp.getCantidadProtestoAnual())); //Ajus			
					msg.setE02PVPU6I(new BigDecimal(resp.getCantidadProtestoSemestral())); //Inf cantidad protesto 6 meses
					msg.setE02PVPU6A(new BigDecimal(resp.getCantidadProtestoSemestral())); //Ajus	
					msg.setE02PVPMOT(new BigDecimal(resp.getMontoMoraTotal())); //Inf - monto de mora total					
					msg.setE02PVPMOA(new BigDecimal((resp.getMontoMoraTotal()))); //Ajus
					msg.setE02PVPPRO(new BigDecimal(resp.getMontoTotalProtesto())); //Inf 																			
					msg.setE02PVPPRA(new BigDecimal(resp.getMontoTotalProtesto())); //Ajus				
					msg.setE02PVPSCO(Integer.toString(resp.getScore()));
					String dscCed = (resp.getEstadoCedula()==null)?"NO RESPONSE":resp.getEstadoCedula();
					msg.setE02DSCCED(dscCed); //descripcion del edo cedula // codigo de 30
					msg.setE02PVPCED((resp.getCedulaVigente().length()>0)?resp.getCedulaVigente().substring(0,1):resp.getCedulaVigente()); //solo primera letra
					String f = resp.getFechaVencimientoCedula();//dd-mm-yyyy
					if (f!=null && !"".equals(f.trim())){						
						msg.setE02PVPVCD(f.substring(0,2));
						msg.setE02PVPVCM(f.substring(3,5));			
						msg.setE02PVPVCY(f.substring(6));
					}
				}else{
					String c = respWrapper.getCodError();
					c =(c==null)?"N/D":c; 
					String d = respWrapper.getDesError();
					d =(d==null)?"SIN RESPUESTA SERVICIO BUREAU":d;					
					ELEERRMessage msgError = new ELEERRMessage();
	                msgError.setERRNUM("1");
	                msgError.setERNU01((c.length() >4?(c.substring(0,4)):c));		//4		                
	                msgError.setERDS01((d.length() >70?(d.substring(0,70)):d));		//70
					session.setAttribute("error", msgError);									
				}
							
			}catch (Exception e1){
				e1.printStackTrace();
				String d = e1.getMessage();
				d=(d==null)?d=e1.toString():d;					
				//si hay error mandarlos en la pantalla
                ELEERRMessage msgError = new ELEERRMessage();
                msgError.setERRNUM("1");
                msgError.setERNU01("01");
                msgError.setERDS01((d.length() >70?(d.substring(0,70)):d));
				session.setAttribute("error", msgError);			
			}		
			req.setAttribute("SINACOFI", "S");			
			session.setAttribute("platfObj", msg);// seteamos nuevamente con la informacion obtenida
			forward("EPV1005_salesplatform_maintenance.jsp", req, res);

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
	/**
	 * procActionPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformListCamp(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = getUserPos(session);

		try {

			mp = getMessageProcessor("EPV1005", req);
			EPV100506Message msgList = (EPV100506Message) mp.getMessageRecord("EPV100506", user.getH01USR(), "0001");
			msgList.setE06PCACUN(userPO.getCusNum());
			// Sends message
			mp.sendMessage(msgList);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", error);
				forward("EPV1000_client_enter.jsp", req, res);				
			} else {
				// Receive salesPlatform list
				JBObjList list = mp.receiveMessageRecordList("H06FLGMAS");
				// save customer number
				session.setAttribute("EPV100506List", list);
				session.setAttribute("userPO", userPO);				
				forwardOnSuccess("EPV1005_salesplatform_list_campana.jsp", req, res);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
}
