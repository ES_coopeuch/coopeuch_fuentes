/**
 * DatosRespuestaSRut.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.consultaequifax;

public class DatosRespuestaSRut  implements java.io.Serializable {
    private java.lang.String score;
    private java.lang.String glosa;
    private java.lang.String usu_co_sexo;
    private java.lang.String usu_co_renta;
    private java.lang.String usu_co_niveleduc;
    private java.lang.String usu_co_edad;
    private java.lang.String usu_co_diffsocio;
    private java.lang.String usu_co_antig_lab;
    private java.lang.String rellcnup12;
    private java.lang.String relconsp_3;
    private java.lang.String mesddamo12;
    private java.lang.String efx_ratio_leverage;
    private java.lang.String efx_antes_lag_apariciones;
    private java.lang.String dic_totdocs_02;
    private java.lang.String dic_totcons3_38;
    private java.lang.String dic_totavaluo_32;
    private java.lang.String dic_totacreed_12;
    private java.lang.String dic_tot_clqveh_10;
    private java.lang.String dic_r04_creds_m01_16;
    private java.lang.String dic_nrodirecciones_18;
    private java.lang.String baja_dda_cons_3M;
    private java.lang.String estadoServicio;
    private java.lang.String timeOut;
    private java.lang.String baseProactiva;
    private cl.consultaequifax.Error error;

    public DatosRespuestaSRut() {
    }

    public java.lang.String getScore() {
        return score;
    }

    public void setScore(java.lang.String score) {
        this.score = score;
    }

    public java.lang.String getGlosa() {
        return glosa;
    }

    public void setGlosa(java.lang.String glosa) {
        this.glosa = glosa;
    }

    public java.lang.String getUsu_co_sexo() {
        return usu_co_sexo;
    }

    public void setUsu_co_sexo(java.lang.String usu_co_sexo) {
        this.usu_co_sexo = usu_co_sexo;
    }

    public java.lang.String getUsu_co_renta() {
        return usu_co_renta;
    }

    public void setUsu_co_renta(java.lang.String usu_co_renta) {
        this.usu_co_renta = usu_co_renta;
    }

    public java.lang.String getUsu_co_niveleduc() {
        return usu_co_niveleduc;
    }

    public void setUsu_co_niveleduc(java.lang.String usu_co_niveleduc) {
        this.usu_co_niveleduc = usu_co_niveleduc;
    }

    public java.lang.String getUsu_co_edad() {
        return usu_co_edad;
    }

    public void setUsu_co_edad(java.lang.String usu_co_edad) {
        this.usu_co_edad = usu_co_edad;
    }

    public java.lang.String getUsu_co_diffsocio() {
        return usu_co_diffsocio;
    }

    public void setUsu_co_diffsocio(java.lang.String usu_co_diffsocio) {
        this.usu_co_diffsocio = usu_co_diffsocio;
    }

    public java.lang.String getUsu_co_antig_lab() {
        return usu_co_antig_lab;
    }

    public void setUsu_co_antig_lab(java.lang.String usu_co_antig_lab) {
        this.usu_co_antig_lab = usu_co_antig_lab;
    }

    public java.lang.String getRellcnup12() {
        return rellcnup12;
    }

    public void setRellcnup12(java.lang.String rellcnup12) {
        this.rellcnup12 = rellcnup12;
    }

    public java.lang.String getRelconsp_3() {
        return relconsp_3;
    }

    public void setRelconsp_3(java.lang.String relconsp_3) {
        this.relconsp_3 = relconsp_3;
    }

    public java.lang.String getMesddamo12() {
        return mesddamo12;
    }

    public void setMesddamo12(java.lang.String mesddamo12) {
        this.mesddamo12 = mesddamo12;
    }

    public java.lang.String getEfx_ratio_leverage() {
        return efx_ratio_leverage;
    }

    public void setEfx_ratio_leverage(java.lang.String efx_ratio_leverage) {
        this.efx_ratio_leverage = efx_ratio_leverage;
    }

    public java.lang.String getEfx_antes_lag_apariciones() {
        return efx_antes_lag_apariciones;
    }

    public void setEfx_antes_lag_apariciones(java.lang.String efx_antes_lag_apariciones) {
        this.efx_antes_lag_apariciones = efx_antes_lag_apariciones;
    }

    public java.lang.String getDic_totdocs_02() {
        return dic_totdocs_02;
    }

    public void setDic_totdocs_02(java.lang.String dic_totdocs_02) {
        this.dic_totdocs_02 = dic_totdocs_02;
    }

    public java.lang.String getDic_totcons3_38() {
        return dic_totcons3_38;
    }

    public void setDic_totcons3_38(java.lang.String dic_totcons3_38) {
        this.dic_totcons3_38 = dic_totcons3_38;
    }

    public java.lang.String getDic_totavaluo_32() {
        return dic_totavaluo_32;
    }

    public void setDic_totavaluo_32(java.lang.String dic_totavaluo_32) {
        this.dic_totavaluo_32 = dic_totavaluo_32;
    }

    public java.lang.String getDic_totacreed_12() {
        return dic_totacreed_12;
    }

    public void setDic_totacreed_12(java.lang.String dic_totacreed_12) {
        this.dic_totacreed_12 = dic_totacreed_12;
    }

    public java.lang.String getDic_tot_clqveh_10() {
        return dic_tot_clqveh_10;
    }

    public void setDic_tot_clqveh_10(java.lang.String dic_tot_clqveh_10) {
        this.dic_tot_clqveh_10 = dic_tot_clqveh_10;
    }

    public java.lang.String getDic_r04_creds_m01_16() {
        return dic_r04_creds_m01_16;
    }

    public void setDic_r04_creds_m01_16(java.lang.String dic_r04_creds_m01_16) {
        this.dic_r04_creds_m01_16 = dic_r04_creds_m01_16;
    }

    public java.lang.String getDic_nrodirecciones_18() {
        return dic_nrodirecciones_18;
    }

    public void setDic_nrodirecciones_18(java.lang.String dic_nrodirecciones_18) {
        this.dic_nrodirecciones_18 = dic_nrodirecciones_18;
    }

    public java.lang.String getBaja_dda_cons_3M() {
        return baja_dda_cons_3M;
    }

    public void setBaja_dda_cons_3M(java.lang.String baja_dda_cons_3M) {
        this.baja_dda_cons_3M = baja_dda_cons_3M;
    }

    public java.lang.String getEstadoServicio() {
        return estadoServicio;
    }

    public void setEstadoServicio(java.lang.String estadoServicio) {
        this.estadoServicio = estadoServicio;
    }

    public java.lang.String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(java.lang.String timeOut) {
        this.timeOut = timeOut;
    }

    public java.lang.String getBaseProactiva() {
        return baseProactiva;
    }

    public void setBaseProactiva(java.lang.String baseProactiva) {
        this.baseProactiva = baseProactiva;
    }

    public cl.consultaequifax.Error getError() {
        return error;
    }

    public void setError(cl.consultaequifax.Error error) {
        this.error = error;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        DatosRespuestaSRut other = (DatosRespuestaSRut) obj;
        boolean _equals;
        _equals = true
            && ((this.score==null && other.getScore()==null) || 
             (this.score!=null &&
              this.score.equals(other.getScore())))
            && ((this.glosa==null && other.getGlosa()==null) || 
             (this.glosa!=null &&
              this.glosa.equals(other.getGlosa())))
            && ((this.usu_co_sexo==null && other.getUsu_co_sexo()==null) || 
             (this.usu_co_sexo!=null &&
              this.usu_co_sexo.equals(other.getUsu_co_sexo())))
            && ((this.usu_co_renta==null && other.getUsu_co_renta()==null) || 
             (this.usu_co_renta!=null &&
              this.usu_co_renta.equals(other.getUsu_co_renta())))
            && ((this.usu_co_niveleduc==null && other.getUsu_co_niveleduc()==null) || 
             (this.usu_co_niveleduc!=null &&
              this.usu_co_niveleduc.equals(other.getUsu_co_niveleduc())))
            && ((this.usu_co_edad==null && other.getUsu_co_edad()==null) || 
             (this.usu_co_edad!=null &&
              this.usu_co_edad.equals(other.getUsu_co_edad())))
            && ((this.usu_co_diffsocio==null && other.getUsu_co_diffsocio()==null) || 
             (this.usu_co_diffsocio!=null &&
              this.usu_co_diffsocio.equals(other.getUsu_co_diffsocio())))
            && ((this.usu_co_antig_lab==null && other.getUsu_co_antig_lab()==null) || 
             (this.usu_co_antig_lab!=null &&
              this.usu_co_antig_lab.equals(other.getUsu_co_antig_lab())))
            && ((this.rellcnup12==null && other.getRellcnup12()==null) || 
             (this.rellcnup12!=null &&
              this.rellcnup12.equals(other.getRellcnup12())))
            && ((this.relconsp_3==null && other.getRelconsp_3()==null) || 
             (this.relconsp_3!=null &&
              this.relconsp_3.equals(other.getRelconsp_3())))
            && ((this.mesddamo12==null && other.getMesddamo12()==null) || 
             (this.mesddamo12!=null &&
              this.mesddamo12.equals(other.getMesddamo12())))
            && ((this.efx_ratio_leverage==null && other.getEfx_ratio_leverage()==null) || 
             (this.efx_ratio_leverage!=null &&
              this.efx_ratio_leverage.equals(other.getEfx_ratio_leverage())))
            && ((this.efx_antes_lag_apariciones==null && other.getEfx_antes_lag_apariciones()==null) || 
             (this.efx_antes_lag_apariciones!=null &&
              this.efx_antes_lag_apariciones.equals(other.getEfx_antes_lag_apariciones())))
            && ((this.dic_totdocs_02==null && other.getDic_totdocs_02()==null) || 
             (this.dic_totdocs_02!=null &&
              this.dic_totdocs_02.equals(other.getDic_totdocs_02())))
            && ((this.dic_totcons3_38==null && other.getDic_totcons3_38()==null) || 
             (this.dic_totcons3_38!=null &&
              this.dic_totcons3_38.equals(other.getDic_totcons3_38())))
            && ((this.dic_totavaluo_32==null && other.getDic_totavaluo_32()==null) || 
             (this.dic_totavaluo_32!=null &&
              this.dic_totavaluo_32.equals(other.getDic_totavaluo_32())))
            && ((this.dic_totacreed_12==null && other.getDic_totacreed_12()==null) || 
             (this.dic_totacreed_12!=null &&
              this.dic_totacreed_12.equals(other.getDic_totacreed_12())))
            && ((this.dic_tot_clqveh_10==null && other.getDic_tot_clqveh_10()==null) || 
             (this.dic_tot_clqveh_10!=null &&
              this.dic_tot_clqveh_10.equals(other.getDic_tot_clqveh_10())))
            && ((this.dic_r04_creds_m01_16==null && other.getDic_r04_creds_m01_16()==null) || 
             (this.dic_r04_creds_m01_16!=null &&
              this.dic_r04_creds_m01_16.equals(other.getDic_r04_creds_m01_16())))
            && ((this.dic_nrodirecciones_18==null && other.getDic_nrodirecciones_18()==null) || 
             (this.dic_nrodirecciones_18!=null &&
              this.dic_nrodirecciones_18.equals(other.getDic_nrodirecciones_18())))
            && ((this.baja_dda_cons_3M==null && other.getBaja_dda_cons_3M()==null) || 
             (this.baja_dda_cons_3M!=null &&
              this.baja_dda_cons_3M.equals(other.getBaja_dda_cons_3M())))
            && ((this.estadoServicio==null && other.getEstadoServicio()==null) || 
             (this.estadoServicio!=null &&
              this.estadoServicio.equals(other.getEstadoServicio())))
            && ((this.timeOut==null && other.getTimeOut()==null) || 
             (this.timeOut!=null &&
              this.timeOut.equals(other.getTimeOut())))
            && ((this.baseProactiva==null && other.getBaseProactiva()==null) || 
             (this.baseProactiva!=null &&
              this.baseProactiva.equals(other.getBaseProactiva())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        DatosRespuestaSRut history = (DatosRespuestaSRut) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        DatosRespuestaSRut history = (DatosRespuestaSRut) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getScore() != null) {
            _hashCode += getScore().hashCode();
        }
        if (getGlosa() != null) {
            _hashCode += getGlosa().hashCode();
        }
        if (getUsu_co_sexo() != null) {
            _hashCode += getUsu_co_sexo().hashCode();
        }
        if (getUsu_co_renta() != null) {
            _hashCode += getUsu_co_renta().hashCode();
        }
        if (getUsu_co_niveleduc() != null) {
            _hashCode += getUsu_co_niveleduc().hashCode();
        }
        if (getUsu_co_edad() != null) {
            _hashCode += getUsu_co_edad().hashCode();
        }
        if (getUsu_co_diffsocio() != null) {
            _hashCode += getUsu_co_diffsocio().hashCode();
        }
        if (getUsu_co_antig_lab() != null) {
            _hashCode += getUsu_co_antig_lab().hashCode();
        }
        if (getRellcnup12() != null) {
            _hashCode += getRellcnup12().hashCode();
        }
        if (getRelconsp_3() != null) {
            _hashCode += getRelconsp_3().hashCode();
        }
        if (getMesddamo12() != null) {
            _hashCode += getMesddamo12().hashCode();
        }
        if (getEfx_ratio_leverage() != null) {
            _hashCode += getEfx_ratio_leverage().hashCode();
        }
        if (getEfx_antes_lag_apariciones() != null) {
            _hashCode += getEfx_antes_lag_apariciones().hashCode();
        }
        if (getDic_totdocs_02() != null) {
            _hashCode += getDic_totdocs_02().hashCode();
        }
        if (getDic_totcons3_38() != null) {
            _hashCode += getDic_totcons3_38().hashCode();
        }
        if (getDic_totavaluo_32() != null) {
            _hashCode += getDic_totavaluo_32().hashCode();
        }
        if (getDic_totacreed_12() != null) {
            _hashCode += getDic_totacreed_12().hashCode();
        }
        if (getDic_tot_clqveh_10() != null) {
            _hashCode += getDic_tot_clqveh_10().hashCode();
        }
        if (getDic_r04_creds_m01_16() != null) {
            _hashCode += getDic_r04_creds_m01_16().hashCode();
        }
        if (getDic_nrodirecciones_18() != null) {
            _hashCode += getDic_nrodirecciones_18().hashCode();
        }
        if (getBaja_dda_cons_3M() != null) {
            _hashCode += getBaja_dda_cons_3M().hashCode();
        }
        if (getEstadoServicio() != null) {
            _hashCode += getEstadoServicio().hashCode();
        }
        if (getTimeOut() != null) {
            _hashCode += getTimeOut().hashCode();
        }
        if (getBaseProactiva() != null) {
            _hashCode += getBaseProactiva().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
