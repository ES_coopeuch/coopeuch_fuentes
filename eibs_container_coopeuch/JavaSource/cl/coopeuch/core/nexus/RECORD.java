/**
 * RECORD.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.coopeuch.core.nexus;

public class RECORD  implements java.io.Serializable {
    private java.lang.String num_tarjeta_s;
    private int correlativo;
    private java.lang.String origen_trans;
    private int fec_trans;
    private int fec_posteo;
    private int codigo_trans;
    private java.lang.String tipo_trans;
    private double monto_compra;
    private int numero_cuota;
    private int total_de_cuotas;
    private double tasa;
    private java.lang.String descripcion_trans;
    private java.lang.String glosa_trans;
    private java.lang.String microfilm23;
    private long cdgo_comercio_tbk;
    private java.lang.String cdgo_comercio_int;
    private int rubro;
    private java.lang.String nombre_comercio;
    private long rut_comercio;
    private java.lang.String dvrut_comercio;
    private long nro_solicitud;
    private java.lang.String desc_rubro;
    private java.lang.String codigo_pais_iata;
    private java.lang.String ciudad;
    private int moneda_fact_9;
    private int moneda_origen_9;
    private double monto_mon_origen;
    private java.lang.String codaut;
    private int fecha_autorizador_s;
    private int hora_autorizador_s;
    private int estado_trans;
    private double monto11;
    private double monto21;
    private double monto31;
    private long numero11;
    private long numero21;
    private long numero31;
    private java.lang.String flag11;
    private java.lang.String flag21;
    private java.lang.String flag31;
    private java.lang.String string11;
    private java.lang.String string21;
    private java.lang.String string31;
    private java.lang.String filler_03;

    public RECORD() {
    }

    public java.lang.String getNum_tarjeta_s() {
        return num_tarjeta_s;
    }

    public void setNum_tarjeta_s(java.lang.String num_tarjeta_s) {
        this.num_tarjeta_s = num_tarjeta_s;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public java.lang.String getOrigen_trans() {
        return origen_trans;
    }

    public void setOrigen_trans(java.lang.String origen_trans) {
        this.origen_trans = origen_trans;
    }

    public int getFec_trans() {
        return fec_trans;
    }

    public void setFec_trans(int fec_trans) {
        this.fec_trans = fec_trans;
    }

    public int getFec_posteo() {
        return fec_posteo;
    }

    public void setFec_posteo(int fec_posteo) {
        this.fec_posteo = fec_posteo;
    }

    public int getCodigo_trans() {
        return codigo_trans;
    }

    public void setCodigo_trans(int codigo_trans) {
        this.codigo_trans = codigo_trans;
    }

    public java.lang.String getTipo_trans() {
        return tipo_trans;
    }

    public void setTipo_trans(java.lang.String tipo_trans) {
        this.tipo_trans = tipo_trans;
    }

    public double getMonto_compra() {
        return monto_compra;
    }

    public void setMonto_compra(double monto_compra) {
        this.monto_compra = monto_compra;
    }

    public int getNumero_cuota() {
        return numero_cuota;
    }

    public void setNumero_cuota(int numero_cuota) {
        this.numero_cuota = numero_cuota;
    }

    public int getTotal_de_cuotas() {
        return total_de_cuotas;
    }

    public void setTotal_de_cuotas(int total_de_cuotas) {
        this.total_de_cuotas = total_de_cuotas;
    }

    public double getTasa() {
        return tasa;
    }

    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    public java.lang.String getDescripcion_trans() {
        return descripcion_trans;
    }

    public void setDescripcion_trans(java.lang.String descripcion_trans) {
        this.descripcion_trans = descripcion_trans;
    }

    public java.lang.String getGlosa_trans() {
        return glosa_trans;
    }

    public void setGlosa_trans(java.lang.String glosa_trans) {
        this.glosa_trans = glosa_trans;
    }

    public java.lang.String getMicrofilm23() {
        return microfilm23;
    }

    public void setMicrofilm23(java.lang.String microfilm23) {
        this.microfilm23 = microfilm23;
    }

    public long getCdgo_comercio_tbk() {
        return cdgo_comercio_tbk;
    }

    public void setCdgo_comercio_tbk(long cdgo_comercio_tbk) {
        this.cdgo_comercio_tbk = cdgo_comercio_tbk;
    }

    public java.lang.String getCdgo_comercio_int() {
        return cdgo_comercio_int;
    }

    public void setCdgo_comercio_int(java.lang.String cdgo_comercio_int) {
        this.cdgo_comercio_int = cdgo_comercio_int;
    }

    public int getRubro() {
        return rubro;
    }

    public void setRubro(int rubro) {
        this.rubro = rubro;
    }

    public java.lang.String getNombre_comercio() {
        return nombre_comercio;
    }

    public void setNombre_comercio(java.lang.String nombre_comercio) {
        this.nombre_comercio = nombre_comercio;
    }

    public long getRut_comercio() {
        return rut_comercio;
    }

    public void setRut_comercio(long rut_comercio) {
        this.rut_comercio = rut_comercio;
    }

    public java.lang.String getDvrut_comercio() {
        return dvrut_comercio;
    }

    public void setDvrut_comercio(java.lang.String dvrut_comercio) {
        this.dvrut_comercio = dvrut_comercio;
    }

    public long getNro_solicitud() {
        return nro_solicitud;
    }

    public void setNro_solicitud(long nro_solicitud) {
        this.nro_solicitud = nro_solicitud;
    }

    public java.lang.String getDesc_rubro() {
        return desc_rubro;
    }

    public void setDesc_rubro(java.lang.String desc_rubro) {
        this.desc_rubro = desc_rubro;
    }

    public java.lang.String getCodigo_pais_iata() {
        return codigo_pais_iata;
    }

    public void setCodigo_pais_iata(java.lang.String codigo_pais_iata) {
        this.codigo_pais_iata = codigo_pais_iata;
    }

    public java.lang.String getCiudad() {
        return ciudad;
    }

    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }

    public int getMoneda_fact_9() {
        return moneda_fact_9;
    }

    public void setMoneda_fact_9(int moneda_fact_9) {
        this.moneda_fact_9 = moneda_fact_9;
    }

    public int getMoneda_origen_9() {
        return moneda_origen_9;
    }

    public void setMoneda_origen_9(int moneda_origen_9) {
        this.moneda_origen_9 = moneda_origen_9;
    }

    public double getMonto_mon_origen() {
        return monto_mon_origen;
    }

    public void setMonto_mon_origen(double monto_mon_origen) {
        this.monto_mon_origen = monto_mon_origen;
    }

    public java.lang.String getCodaut() {
        return codaut;
    }

    public void setCodaut(java.lang.String codaut) {
        this.codaut = codaut;
    }

    public int getFecha_autorizador_s() {
        return fecha_autorizador_s;
    }

    public void setFecha_autorizador_s(int fecha_autorizador_s) {
        this.fecha_autorizador_s = fecha_autorizador_s;
    }

    public int getHora_autorizador_s() {
        return hora_autorizador_s;
    }

    public void setHora_autorizador_s(int hora_autorizador_s) {
        this.hora_autorizador_s = hora_autorizador_s;
    }

    public int getEstado_trans() {
        return estado_trans;
    }

    public void setEstado_trans(int estado_trans) {
        this.estado_trans = estado_trans;
    }

    public double getMonto11() {
        return monto11;
    }

    public void setMonto11(double monto11) {
        this.monto11 = monto11;
    }

    public double getMonto21() {
        return monto21;
    }

    public void setMonto21(double monto21) {
        this.monto21 = monto21;
    }

    public double getMonto31() {
        return monto31;
    }

    public void setMonto31(double monto31) {
        this.monto31 = monto31;
    }

    public long getNumero11() {
        return numero11;
    }

    public void setNumero11(long numero11) {
        this.numero11 = numero11;
    }

    public long getNumero21() {
        return numero21;
    }

    public void setNumero21(long numero21) {
        this.numero21 = numero21;
    }

    public long getNumero31() {
        return numero31;
    }

    public void setNumero31(long numero31) {
        this.numero31 = numero31;
    }

    public java.lang.String getFlag11() {
        return flag11;
    }

    public void setFlag11(java.lang.String flag11) {
        this.flag11 = flag11;
    }

    public java.lang.String getFlag21() {
        return flag21;
    }

    public void setFlag21(java.lang.String flag21) {
        this.flag21 = flag21;
    }

    public java.lang.String getFlag31() {
        return flag31;
    }

    public void setFlag31(java.lang.String flag31) {
        this.flag31 = flag31;
    }

    public java.lang.String getString11() {
        return string11;
    }

    public void setString11(java.lang.String string11) {
        this.string11 = string11;
    }

    public java.lang.String getString21() {
        return string21;
    }

    public void setString21(java.lang.String string21) {
        this.string21 = string21;
    }

    public java.lang.String getString31() {
        return string31;
    }

    public void setString31(java.lang.String string31) {
        this.string31 = string31;
    }

    public java.lang.String getFiller_03() {
        return filler_03;
    }

    public void setFiller_03(java.lang.String filler_03) {
        this.filler_03 = filler_03;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        RECORD other = (RECORD) obj;
        boolean _equals;
        _equals = true
            && ((this.num_tarjeta_s==null && other.getNum_tarjeta_s()==null) || 
             (this.num_tarjeta_s!=null &&
              this.num_tarjeta_s.equals(other.getNum_tarjeta_s())))
            && this.correlativo == other.getCorrelativo()
            && ((this.origen_trans==null && other.getOrigen_trans()==null) || 
             (this.origen_trans!=null &&
              this.origen_trans.equals(other.getOrigen_trans())))
            && this.fec_trans == other.getFec_trans()
            && this.fec_posteo == other.getFec_posteo()
            && this.codigo_trans == other.getCodigo_trans()
            && ((this.tipo_trans==null && other.getTipo_trans()==null) || 
             (this.tipo_trans!=null &&
              this.tipo_trans.equals(other.getTipo_trans())))
            && this.monto_compra == other.getMonto_compra()
            && this.numero_cuota == other.getNumero_cuota()
            && this.total_de_cuotas == other.getTotal_de_cuotas()
            && this.tasa == other.getTasa()
            && ((this.descripcion_trans==null && other.getDescripcion_trans()==null) || 
             (this.descripcion_trans!=null &&
              this.descripcion_trans.equals(other.getDescripcion_trans())))
            && ((this.glosa_trans==null && other.getGlosa_trans()==null) || 
             (this.glosa_trans!=null &&
              this.glosa_trans.equals(other.getGlosa_trans())))
            && ((this.microfilm23==null && other.getMicrofilm23()==null) || 
             (this.microfilm23!=null &&
              this.microfilm23.equals(other.getMicrofilm23())))
            && this.cdgo_comercio_tbk == other.getCdgo_comercio_tbk()
            && ((this.cdgo_comercio_int==null && other.getCdgo_comercio_int()==null) || 
             (this.cdgo_comercio_int!=null &&
              this.cdgo_comercio_int.equals(other.getCdgo_comercio_int())))
            && this.rubro == other.getRubro()
            && ((this.nombre_comercio==null && other.getNombre_comercio()==null) || 
             (this.nombre_comercio!=null &&
              this.nombre_comercio.equals(other.getNombre_comercio())))
            && this.rut_comercio == other.getRut_comercio()
            && ((this.dvrut_comercio==null && other.getDvrut_comercio()==null) || 
             (this.dvrut_comercio!=null &&
              this.dvrut_comercio.equals(other.getDvrut_comercio())))
            && this.nro_solicitud == other.getNro_solicitud()
            && ((this.desc_rubro==null && other.getDesc_rubro()==null) || 
             (this.desc_rubro!=null &&
              this.desc_rubro.equals(other.getDesc_rubro())))
            && ((this.codigo_pais_iata==null && other.getCodigo_pais_iata()==null) || 
             (this.codigo_pais_iata!=null &&
              this.codigo_pais_iata.equals(other.getCodigo_pais_iata())))
            && ((this.ciudad==null && other.getCiudad()==null) || 
             (this.ciudad!=null &&
              this.ciudad.equals(other.getCiudad())))
            && this.moneda_fact_9 == other.getMoneda_fact_9()
            && this.moneda_origen_9 == other.getMoneda_origen_9()
            && this.monto_mon_origen == other.getMonto_mon_origen()
            && ((this.codaut==null && other.getCodaut()==null) || 
             (this.codaut!=null &&
              this.codaut.equals(other.getCodaut())))
            && this.fecha_autorizador_s == other.getFecha_autorizador_s()
            && this.hora_autorizador_s == other.getHora_autorizador_s()
            && this.estado_trans == other.getEstado_trans()
            && this.monto11 == other.getMonto11()
            && this.monto21 == other.getMonto21()
            && this.monto31 == other.getMonto31()
            && this.numero11 == other.getNumero11()
            && this.numero21 == other.getNumero21()
            && this.numero31 == other.getNumero31()
            && ((this.flag11==null && other.getFlag11()==null) || 
             (this.flag11!=null &&
              this.flag11.equals(other.getFlag11())))
            && ((this.flag21==null && other.getFlag21()==null) || 
             (this.flag21!=null &&
              this.flag21.equals(other.getFlag21())))
            && ((this.flag31==null && other.getFlag31()==null) || 
             (this.flag31!=null &&
              this.flag31.equals(other.getFlag31())))
            && ((this.string11==null && other.getString11()==null) || 
             (this.string11!=null &&
              this.string11.equals(other.getString11())))
            && ((this.string21==null && other.getString21()==null) || 
             (this.string21!=null &&
              this.string21.equals(other.getString21())))
            && ((this.string31==null && other.getString31()==null) || 
             (this.string31!=null &&
              this.string31.equals(other.getString31())))
            && ((this.filler_03==null && other.getFiller_03()==null) || 
             (this.filler_03!=null &&
              this.filler_03.equals(other.getFiller_03())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        RECORD history = (RECORD) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        RECORD history = (RECORD) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getNum_tarjeta_s() != null) {
            _hashCode += getNum_tarjeta_s().hashCode();
        }
        _hashCode += getCorrelativo();
        if (getOrigen_trans() != null) {
            _hashCode += getOrigen_trans().hashCode();
        }
        _hashCode += getFec_trans();
        _hashCode += getFec_posteo();
        _hashCode += getCodigo_trans();
        if (getTipo_trans() != null) {
            _hashCode += getTipo_trans().hashCode();
        }
        _hashCode += new Double(getMonto_compra()).hashCode();
        _hashCode += getNumero_cuota();
        _hashCode += getTotal_de_cuotas();
        _hashCode += new Double(getTasa()).hashCode();
        if (getDescripcion_trans() != null) {
            _hashCode += getDescripcion_trans().hashCode();
        }
        if (getGlosa_trans() != null) {
            _hashCode += getGlosa_trans().hashCode();
        }
        if (getMicrofilm23() != null) {
            _hashCode += getMicrofilm23().hashCode();
        }
        _hashCode += new Long(getCdgo_comercio_tbk()).hashCode();
        if (getCdgo_comercio_int() != null) {
            _hashCode += getCdgo_comercio_int().hashCode();
        }
        _hashCode += getRubro();
        if (getNombre_comercio() != null) {
            _hashCode += getNombre_comercio().hashCode();
        }
        _hashCode += new Long(getRut_comercio()).hashCode();
        if (getDvrut_comercio() != null) {
            _hashCode += getDvrut_comercio().hashCode();
        }
        _hashCode += new Long(getNro_solicitud()).hashCode();
        if (getDesc_rubro() != null) {
            _hashCode += getDesc_rubro().hashCode();
        }
        if (getCodigo_pais_iata() != null) {
            _hashCode += getCodigo_pais_iata().hashCode();
        }
        if (getCiudad() != null) {
            _hashCode += getCiudad().hashCode();
        }
        _hashCode += getMoneda_fact_9();
        _hashCode += getMoneda_origen_9();
        _hashCode += new Double(getMonto_mon_origen()).hashCode();
        if (getCodaut() != null) {
            _hashCode += getCodaut().hashCode();
        }
        _hashCode += getFecha_autorizador_s();
        _hashCode += getHora_autorizador_s();
        _hashCode += getEstado_trans();
        _hashCode += new Double(getMonto11()).hashCode();
        _hashCode += new Double(getMonto21()).hashCode();
        _hashCode += new Double(getMonto31()).hashCode();
        _hashCode += new Long(getNumero11()).hashCode();
        _hashCode += new Long(getNumero21()).hashCode();
        _hashCode += new Long(getNumero31()).hashCode();
        if (getFlag11() != null) {
            _hashCode += getFlag11().hashCode();
        }
        if (getFlag21() != null) {
            _hashCode += getFlag21().hashCode();
        }
        if (getFlag31() != null) {
            _hashCode += getFlag31().hashCode();
        }
        if (getString11() != null) {
            _hashCode += getString11().hashCode();
        }
        if (getString21() != null) {
            _hashCode += getString21().hashCode();
        }
        if (getString31() != null) {
            _hashCode += getString31().hashCode();
        }
        if (getFiller_03() != null) {
            _hashCode += getFiller_03().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
