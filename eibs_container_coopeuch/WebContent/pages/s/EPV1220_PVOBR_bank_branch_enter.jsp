<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"></SCRIPT> 

<script Language="Javascript1.1">

function save() {
		document.forms[0].SCREEN.value="150";	
 		document.forms[0].E01OPETYP.value='0001';
  		document.forms[0].submit();

}

function disable() {

     document.getElementById('E01PVGBRN').value= "0";
     document.getElementById('E01PVGBRN').disabled= true;
     document.getElementById('lupa').style.display='none';

}

function enable() {

     //document.getElementById('E01DBABRN').readOnly= false;
     document.getElementById('lupa').style.display='';
     document.getElementById('E01PVGBRN').disabled= false;
}

</SCRIPT>

</head>

<jsp:useBean id="pvobr" class="datapro.eibs.beans.EPV122001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />

<body bgcolor="#FFFFFF">

<H3 align="center">Mantención Holguras por Sucursal Plataforma de Venta<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PVOBR_bank_branch_enter, EPV1220"></H3>

<hr size="4">
<%
	String chkAll = "";
	String chkSlt = "";
	if (pvobr.getE01PVGBRN().equals("*")) {
		chkAll = "checked";
		chkSlt = "";
	} else {
		chkAll = "";
		chkSlt = "checked";	
	}
%>
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1220">
  <p> 
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="150">
  </p>
  <table class="tableinfo"  cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr id="trclear"> 
      <td nowrap width="40%"> 
      	<div align="right"> Banco :</div>
      </td> 
      <td nowrap width="60%"> 
        <INPUT type="text" name="E01PVGBNK" size="3" maxlength="2" value="01" readonly>
      </td>
    </tr>       
    <tr id="trdark"> 
      <td nowrap width="40%"> 
      	<div align="right">  Sucursal :</div>
      </td> 
      <td nowrap width="60%"> 
        <input type="radio" name="E01DBAALL" value="S" <%= chkAll%> onclick="disable();" > Todas las Sucursales
      </td>
    </tr>       
    <tr id="trclear"> 
      <td nowrap width="40%"> 
      	<div align="right"> </div>
      </td> 
      <td nowrap width="60%"> 
        <input type="radio" name="E01DBAALL" value="N" <%= chkSlt%> onclick="enable();" > Seleccione Sucursal
    	<input type="text" name="E01PVGBRN" id="E01PVGBRN" size="5" maxlength="4" value="<%= pvobr.getE01PVGBRN().trim()%>" onkeypress="enterInteger()" >
    	<a id="lupa" href="javascript:GetBranch('E01PVGBRN','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="help" align="absbottom" border="0"  ></a>   

      </td>
    </tr>       
          
  </table>
 
 <p align="center"> 
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar" onclick="save()">
  </p>  
 
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
  <%
 }
%> 
</form>
</body>
</html>
