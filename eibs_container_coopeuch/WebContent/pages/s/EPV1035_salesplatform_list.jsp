<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV103502Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV103502List" class="datapro.eibs.beans.JBObjList"
	scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">

function goAction(op) {
	document.forms[0].SCREEN.value = op;
	document.forms[0].submit();
}
	
function setRow(pos){
	document.getElementById('pos').value = pos;
}	
</script>

</head>

<body>
<% 


 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center"> Plataforma de Ventas - Visado Senior de Documentos<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_list.jsp,EPV1035"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1035">
<input type="hidden" name="SCREEN" value=""> 
 <input type="hidden" id="pos" name="row" value="">
 
    <table  class="tbenter" width="100%">
  	<tr>
  		<td width="50%"  >
  			&nbsp;
  		</td>
  		<td width="50%">
			<table class="tbenter" width="100%">
				<tr>	
						<%
							if (!EPV103502List.getNoResult()) {
						%>					
				
					<td align="center" class="tdbkg" width="15%">&nbsp;
							<a href="javascript:goAction('201')"> <b>Visado Senior</b> </a>
					</td>
						<%
							}
						%>															
					<td align="center" class="tdbkg" width="15%">
						<a	href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
					</td>
				</tr>
			</table>  		
  		</td>  		
  	</tr>
  </table>
  


<%
	if (EPV103502List.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=50%>
	<TR>
		<TD>
		<div align="center">
		<font size="3"><b> 
			No existen solicitudes pendiente para Visado Senior. 
		</b></font></div>
		</TD>
	</TR>
</TABLE>
<%
	} else {
%>

	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="5%" >&nbsp;</th>
			<th align="center" nowrap width="5%">Solicitud</th>
			<th align="center" nowrap width="10%">RUT</th>
			<th align="center" nowrap width="15%">Nombre</th>			
			<!-- <th align="center" nowrap width="15%"><small>Medio de Evaluación</small></th> -->
						
			<th align="center" nowrap width="15%">Producto/Descripcion</th>
			<th align="center" nowrap width="15%">Canal de Venta</th>					
			<th align="center" nowrap width="10%">Monto Negociado</th>
			<th align="center" nowrap width="5%">Fecha Ingreso</th>			
			<th align="center" nowrap width="5%">Estado</th>
		</tr>
		<%
			EPV103502List.initRow();
				boolean firstTime = true;
				String chk = "";
				while (EPV103502List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EPV103502Message convObj = (EPV103502Message) EPV103502List.getRecord();
		%>
		<tr>
			<td nowrap>
			<input type="radio" name="ITEMS" value="<%=EPV103502List.getCurrentRow()%>" <%=chk%> onclick="setRow(this.value)"/>
			</td>
			<td nowrap align="center"><%=Util.formatCell(convObj.getE02PVMNUM())%></td>
			<td nowrap align="center"><%=convObj.getE02PVMIDN()%></td>
			<td nowrap align="left" ><%=convObj.getE02PVMNME()%></td>
			<!--<td nowrap align="left"><%=convObj.getE02DSCEVA()%></td> -->			
			<td nowrap align="left"><%=convObj.getE02DSPROD()%></td>
			<td nowrap align="left"><%=convObj.getE02DSCSLC()%></td>			
			<td nowrap align="right"><%=convObj.getE02LNOAMT()%></td>
			<td nowrap align="center"><%=Util.formatCell(convObj.getE02PVMOPD() + "/" + convObj.getE02PVMOPM() + "/"+ convObj.getE02PVMOPY())%> </td>
			<td nowrap align="center"><%=convObj.getE02DSCSTS()%></td>
		</tr>
		<%
			}
		%>
	</table>

<% } %>

</form>

</body>
</html>
