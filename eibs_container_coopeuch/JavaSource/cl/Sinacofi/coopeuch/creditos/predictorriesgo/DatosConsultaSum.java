/**
 * DatosConsultaSum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class DatosConsultaSum  implements java.io.Serializable {
    private java.math.BigInteger rut;
    private java.lang.String dv;
    private java.lang.String serie;
    private java.math.BigInteger rutEmpleador;
    private java.lang.String dvEmpleador;

    public DatosConsultaSum() {
    }

    public java.math.BigInteger getRut() {
        return rut;
    }

    public void setRut(java.math.BigInteger rut) {
        this.rut = rut;
    }

    public java.lang.String getDv() {
        return dv;
    }

    public void setDv(java.lang.String dv) {
        this.dv = dv;
    }

    public java.lang.String getSerie() {
        return serie;
    }

    public void setSerie(java.lang.String serie) {
        this.serie = serie;
    }

    public java.math.BigInteger getRutEmpleador() {
        return rutEmpleador;
    }

    public void setRutEmpleador(java.math.BigInteger rutEmpleador) {
        this.rutEmpleador = rutEmpleador;
    }

    public java.lang.String getDvEmpleador() {
        return dvEmpleador;
    }

    public void setDvEmpleador(java.lang.String dvEmpleador) {
        this.dvEmpleador = dvEmpleador;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        DatosConsultaSum other = (DatosConsultaSum) obj;
        boolean _equals;
        _equals = true
            && ((this.dv==null && other.getDv()==null) || 
             (this.dv!=null &&
              this.dv.equals(other.getDv())))
            && ((this.serie==null && other.getSerie()==null) || 
             (this.serie!=null &&
              this.serie.equals(other.getSerie())))
            && ((this.dvEmpleador==null && other.getDvEmpleador()==null) || 
             (this.dvEmpleador!=null &&
              this.dvEmpleador.equals(other.getDvEmpleador())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        DatosConsultaSum history = (DatosConsultaSum) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.rut==null && other.getRut()==null) || 
             (this.rut!=null &&
              this.rut.equals(other.getRut())))
            && ((this.rutEmpleador==null && other.getRutEmpleador()==null) || 
             (this.rutEmpleador!=null &&
              this.rutEmpleador.equals(other.getRutEmpleador())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        DatosConsultaSum history = (DatosConsultaSum) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getRut() != null) {
            _hashCode += getRut().hashCode();
        }
        if (getDv() != null) {
            _hashCode += getDv().hashCode();
        }
        if (getSerie() != null) {
            _hashCode += getSerie().hashCode();
        }
        if (getRutEmpleador() != null) {
            _hashCode += getRutEmpleador().hashCode();
        }
        if (getDvEmpleador() != null) {
            _hashCode += getDvEmpleador().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
