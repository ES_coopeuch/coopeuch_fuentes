/**
 * ResumenSinacofiType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResumenSinacofiType  implements java.io.Serializable {
    private int cantidadProtestoAnual;
    private int cantidadProtestoSemestral;
    private java.lang.String estadoCedula;
    private double montoMoraTotal;
    private double montoTotalProtesto;
    private int score;
    private double montoProtestoEmpresa;
    private java.lang.String cedulaVigente;
    private java.lang.String fechaVencimientoCedula;

    public ResumenSinacofiType() {
    }

    public int getCantidadProtestoAnual() {
        return cantidadProtestoAnual;
    }

    public void setCantidadProtestoAnual(int cantidadProtestoAnual) {
        this.cantidadProtestoAnual = cantidadProtestoAnual;
    }

    public int getCantidadProtestoSemestral() {
        return cantidadProtestoSemestral;
    }

    public void setCantidadProtestoSemestral(int cantidadProtestoSemestral) {
        this.cantidadProtestoSemestral = cantidadProtestoSemestral;
    }

    public java.lang.String getEstadoCedula() {
        return estadoCedula;
    }

    public void setEstadoCedula(java.lang.String estadoCedula) {
        this.estadoCedula = estadoCedula;
    }

    public double getMontoMoraTotal() {
        return montoMoraTotal;
    }

    public void setMontoMoraTotal(double montoMoraTotal) {
        this.montoMoraTotal = montoMoraTotal;
    }

    public double getMontoTotalProtesto() {
        return montoTotalProtesto;
    }

    public void setMontoTotalProtesto(double montoTotalProtesto) {
        this.montoTotalProtesto = montoTotalProtesto;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public double getMontoProtestoEmpresa() {
        return montoProtestoEmpresa;
    }

    public void setMontoProtestoEmpresa(double montoProtestoEmpresa) {
        this.montoProtestoEmpresa = montoProtestoEmpresa;
    }

    public java.lang.String getCedulaVigente() {
        return cedulaVigente;
    }

    public void setCedulaVigente(java.lang.String cedulaVigente) {
        this.cedulaVigente = cedulaVigente;
    }

    public java.lang.String getFechaVencimientoCedula() {
        return fechaVencimientoCedula;
    }

    public void setFechaVencimientoCedula(java.lang.String fechaVencimientoCedula) {
        this.fechaVencimientoCedula = fechaVencimientoCedula;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        ResumenSinacofiType other = (ResumenSinacofiType) obj;
        boolean _equals;
        _equals = true
            && this.cantidadProtestoAnual == other.getCantidadProtestoAnual()
            && this.cantidadProtestoSemestral == other.getCantidadProtestoSemestral()
            && ((this.estadoCedula==null && other.getEstadoCedula()==null) || 
             (this.estadoCedula!=null &&
              this.estadoCedula.equals(other.getEstadoCedula())))
            && this.montoMoraTotal == other.getMontoMoraTotal()
            && this.montoTotalProtesto == other.getMontoTotalProtesto()
            && this.score == other.getScore()
            && this.montoProtestoEmpresa == other.getMontoProtestoEmpresa()
            && ((this.cedulaVigente==null && other.getCedulaVigente()==null) || 
             (this.cedulaVigente!=null &&
              this.cedulaVigente.equals(other.getCedulaVigente())))
            && ((this.fechaVencimientoCedula==null && other.getFechaVencimientoCedula()==null) || 
             (this.fechaVencimientoCedula!=null &&
              this.fechaVencimientoCedula.equals(other.getFechaVencimientoCedula())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResumenSinacofiType history = (ResumenSinacofiType) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResumenSinacofiType history = (ResumenSinacofiType) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        _hashCode += getCantidadProtestoAnual();
        _hashCode += getCantidadProtestoSemestral();
        if (getEstadoCedula() != null) {
            _hashCode += getEstadoCedula().hashCode();
        }
        _hashCode += new Double(getMontoMoraTotal()).hashCode();
        _hashCode += new Double(getMontoTotalProtesto()).hashCode();
        _hashCode += getScore();
        _hashCode += new Double(getMontoProtestoEmpresa()).hashCode();
        if (getCedulaVigente() != null) {
            _hashCode += getCedulaVigente().hashCode();
        }
        if (getFechaVencimientoCedula() != null) {
            _hashCode += getFechaVencimientoCedula().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
