<!-- Hecho por Alonso Arana ------Datapro-----06/02/2014 -->
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC210001Message,datapro.eibs.beans.ERC210002Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Conciliaci�n Bancos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.ERC210002Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "ERCSeleccion" class= "datapro.eibs.beans.ERC210001Message"  scope="session" />


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">


</SCRIPT>  

</head>





<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	Consulta de Movimientos de Bancos
<%} else if (maintenance){ %>
	Mantenci�n de Movimientos de Bancos
<%} else { %>
	Nuevo Movimientos de Bancos
<%} %>





 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ERC2100_statement_movement.jsp, ERC2100"></h3>

<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC2100">


  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="<%
 if(readOnly){
 out.print("600");
 } 
 
 if(maintenance){
 out.print("700");
 } 
  else{
   out.print("800");
  
  }
  
  
   %>">

<input type="hidden" name="codigo_lista" value="" id="codigo_lista">  
<input type="hidden" name="codigo_banco" value="<%=session.getAttribute("codigo_banco") %>">
 <input type="hidden" name="E02WCSUID" value="<%=cnvObj.getE02WCSUID()%>">
 
   <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="banco" value="<% out.print(session.getAttribute("codigo_banco")+"-"+session.getAttribute("nombre_banco"));%>" 
	  			size="60"  readonly>
             </td>    
         </tr>
         
          <tr>
             <td nowrap width="10%" align="right">Cuenta Banco : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCTA" size="23" maxlength="20" value="<%=session.getAttribute("cuenta_banco")%>" readonly>
             </td>
        <td nowrap width="10%" align="right">Cuenta IBS : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E02WCSACC" size="23" maxlength="20" value="<%=session.getAttribute("cuenta_ibs")%>" readonly>
             </td>
             </tr>

          <tr>
             <td nowrap width="10%" align="right">N� Cartola :</td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E02WCSSTN" size="23" maxlength="20" value="<%=ERCSeleccion.getE01WCHSTN()%>" readonly>
             </td>
        	 <td nowrap width="10%" align="right">Fecha de Carga :</td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="fecha_carga" size="23" maxlength="20" value="<% out.print(ERCSeleccion.getE01WCHSDD()+"/"+ERCSeleccion.getE01WCHSDM()+"/"+ERCSeleccion.getE01WCHSDY());%>" readonly>
             </td>
             </tr>


      
           
        </table>
      </td>
    </tr>
  </table>
  <p>&nbsp;</p>
  
  <table class="tableinfo" cellspacing=0  width="100%" border="0"> 
    <tr id="trdark">
      <td nowrap> 
        <div align="right">Fecha : </div>
      </td>
      <td width="50%"> 
        <div align="left"> 	
       
       <div align="left"> 
                <input type="text" name="E02WCSSDD" size="3" maxlength="2" value="<%=cnvObj.getE02WCSSDD() %>">
                <input type="text" name="E02WCSSDM" size="3" maxlength="2" value="<%=cnvObj.getE02WCSSDM() %>">
                <input type="text" name="E02WCSSDY" size="5" maxlength="4" value="<%=cnvObj.getE02WCSSDY() %>">
                <a href="javascript:DatePicker(document.forms[0].E02WCSSDD,document.forms[0].E02WCSSDM,document.forms[0].E02WCSSDY)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
        </div>
        </div>
      </td>
    </tr>

  	<tr id="trclear"> 
        <td align=CENTER width="50%"> 
          <div align="right">Referencia   :</div>
        </td>
        <td align=CENTER width="50%"> 
          <div align="left"> 
 	         <input type="text" name="E02WCSCKN"  size="6" maxlength="6" value="<%=cnvObj.getE02WCSCKN() %>">
        
         
          </div>
        </td>
      </tr>
<tr id="trdark"> 
        <td align=CENTER width="50%"> 
          <div align="right">C�digo   de Movimiento:</div>
        </td>
        <td align=CENTER width="50%"> 
          <div align="left"> 
 	         <input type="text" name="E02WCSCDE" size="7" maxlength="7" value="<%=cnvObj.getE02WCSCDE() %>">
        
   
          </div>
        </td>
      </tr>
      <tr id="trclear"> 
        <td align=CENTER width="50%"> 
          <div align="right">Monto  :</div>
        </td>
        <td align=CENTER width="50%"> 
          <div align="left"> 
                   <% 
    String s = cnvObj.getE02WCSAMT();
    double d = Double.parseDouble(s.replace(",", ""));
    int irt = (int) d;  
 	          %>
 	         <input type="text" name="E02WCSAMT" size="10"  onkeypress=" enterInteger()" maxlength="10" value="<%=irt %>">
        
        
          </div>
        </td>
      </tr>
        
        
      
        
  </table>
  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
 
 
 
 </form>
</body>
</html>