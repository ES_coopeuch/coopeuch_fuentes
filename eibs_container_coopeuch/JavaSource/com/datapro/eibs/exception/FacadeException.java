package com.datapro.eibs.exception;

/**
 * @author ogarcia
 *
 */
public class FacadeException extends Exception {

	/**
	 * Constructor for FacadeException.
	 */
	public FacadeException() {
		super();
	}

	/**
	 * Constructor for FacadeException.
	 * @param s
	 */
	public FacadeException(String s) {
		super(s);
	}

}
