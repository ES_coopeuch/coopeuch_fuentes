package com.datapro.eibs.security.logic;

import com.datapro.eibs.constants.General;
import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.LogicException;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.math.BigDecimal;
import com.datapro.sql.manager.*;
//import com.datapro.eibs.security.access.jdbc.bean.*;
import com.datapro.eibs.security.access.jdbc.bean.*;
import com.datapro.eibs.security.access.jdbc.dao.*;
import com.datapro.eibs.security.facade.*;
import com.datapro.generic.beanutil.*;

import java.sql.Connection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;

/**
 * @author ogarcia
 *
 */
public class LOMenuBasic {

	/**
	 * Constructor for LOMenuBasic.
	 */
	public LOMenuBasic() {
		super();
	}

	/**
	 * Method getGroupsAndUsers.
	 * @return BeanList
	 * @throws DAOException
	 */
	public BeanList getMenu() {

		DAOMenu menuDao = new DAOMenu("AS400");
		
		BeanList menuBean = new BeanList();
		try {
			
			menuBean = menuDao.getMenu();
			//userDBADao.close();
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return menuBean;
	}
	
	/**
	 * 
	 */
	public SECMNU getMenuItem(int key) {

		DAOMenu menuDao = new DAOMenu("AS400");
		
		SECMNU menuBean = null;
		try {
			
			menuBean = menuDao.getMenuItem(key);
			//userDBADao.close();
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return menuBean;
	}
	
	/**
	 * 
	 */
	public int addMenuItem(SECMNU bean) {

		DAOMenu menuDao = new DAOMenu("AS400");
		
		int result = 0;
		
		try {
			
			result = menuDao.insert(bean);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
	
	public int updateMenuItem(SECMNU bean) {

		DAOMenu menuDao = new DAOMenu("AS400");
		int result = 0;
		
		try {
			result = menuDao.update(bean);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}

}
