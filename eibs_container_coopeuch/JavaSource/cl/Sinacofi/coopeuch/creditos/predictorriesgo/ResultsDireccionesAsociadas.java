/**
 * ResultsDireccionesAsociadas.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResultsDireccionesAsociadas  implements java.io.Serializable {
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.DireccionesAsociadas[] direccionesAsociadas;

    public ResultsDireccionesAsociadas() {
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.DireccionesAsociadas[] getDireccionesAsociadas() {
        return direccionesAsociadas;
    }

    public void setDireccionesAsociadas(cl.Sinacofi.coopeuch.creditos.predictorriesgo.DireccionesAsociadas[] direccionesAsociadas) {
        this.direccionesAsociadas = direccionesAsociadas;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.DireccionesAsociadas getDireccionesAsociadas(int i) {
        return direccionesAsociadas[i];
    }

    public void setDireccionesAsociadas(int i, cl.Sinacofi.coopeuch.creditos.predictorriesgo.DireccionesAsociadas value) {
        this.direccionesAsociadas[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsDireccionesAsociadas history = (ResultsDireccionesAsociadas) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ResultsDireccionesAsociadas other = (ResultsDireccionesAsociadas) obj;
        boolean _equals;
        _equals = true
            && ((this.direccionesAsociadas==null && other.getDireccionesAsociadas()==null) || 
             (this.direccionesAsociadas!=null &&
              java.util.Arrays.equals(this.direccionesAsociadas, other.getDireccionesAsociadas())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsDireccionesAsociadas history = (ResultsDireccionesAsociadas) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getDireccionesAsociadas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDireccionesAsociadas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDireccionesAsociadas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
