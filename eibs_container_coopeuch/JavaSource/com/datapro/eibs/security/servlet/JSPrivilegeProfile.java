package com.datapro.eibs.security.servlet;

/**
 * Insert the type's description here.
 * 
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import com.datapro.eibs.security.facade.*;
import com.datapro.eibs.security.bean.*;
import com.datapro.eibs.security.object.PrivilegeProfile;
import com.datapro.generic.beanutil.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;


public class JSPrivilegeProfile extends datapro.eibs.master.SuperServlet {

    //Requests
	protected static final int R_PVL = 1;  
	protected static final String PAGE_PRIVIL_LIST = "PrivilProfile_list.jsp";
	
	protected String LangPath = "S";
	
	/**
	 * 
	 */
	public JSPrivilegeProfile() {
		super();
	}
	/**
	 * 
	 */
	public void destroy() {

	}
	/**
	 * 
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		//dbProp = new DBConnectorProperty();
	}
	/**
	 * 
	 */
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {

			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}

		} else {
			try {

				msgUser = (ESS0030DSMessage) session.getAttribute("currUser");

				LangPath = super.rootPath + msgUser.getE01LAN() + "/";
				
				int screen = R_PVL;
				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}

				switch (screen) {
					case R_PVL :
						procReqPrivilege(req, res, session);
						break;
					default :
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}
		}

	}
	
	protected void procReqPrivilege(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		PrivilegeProfile privilMng = new PrivilegeProfile();
		JBObjList pvlList = null;
		
		try {							
				pvlList = privilMng.getPrivilege();				
				if (pvlList == null) pvlList = new JBObjList(); 
				ses.setAttribute("pvlList",pvlList);
				try {
						flexLog("About to call Page: " + LangPath + PAGE_PRIVIL_LIST);
						callPage(LangPath + PAGE_PRIVIL_LIST, req, res);
				}
				catch (Exception e) {
						flexLog("Exception calling page " + e);
				}

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}	
	

}
