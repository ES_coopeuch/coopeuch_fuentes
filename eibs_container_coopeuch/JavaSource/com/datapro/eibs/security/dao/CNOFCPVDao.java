package com.datapro.eibs.security.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.PrivilegeNotFoundException;
import com.datapro.generic.access.DAOSuper;
import com.datapro.generic.beanutil.*;
import datapro.eibs.beans.JBObjList;

import com.datapro.eibs.security.bean.CNOFCPVNME;


public class CNOFCPVDao extends DAOSuper {

		
	private static Category cat =
		Category.getInstance(CNOFCPVDao.class.getName());

	/**
	 * Constructor forCNOFCPVDao.
	 */
	public CNOFCPVDao() {
		super();
	}
	
	/**
	 * CNOFCPVDao constructor comment.
	 */
	public CNOFCPVDao(String DBID) {
		super(DBID);
	}
	
	public CNOFCPVDao(Connection connection) {
		super(connection);
	}
	
	public JBObjList select() throws DAOException {
		
		
		JBObjList list = new JBObjList();
    	
		try {
			//st.setString(1, USERID);
			String sql = "SELECT cnorcd,cnodsc,cnof04 FROM cnofc WHERE cnocfl='PV' ORDER BY cnorcd";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();			
			ResultSetMetaData md = rs.getMetaData();
			list = new JBObjList();
			while (rs.next()) {
				CNOFCPVNME bean = new CNOFCPVNME();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		} 

		return list;
	}
	

	
	public CNOFCPVNME select(String cde) throws PrivilegeNotFoundException,DAOException {
		
		CNOFCPVNME bean = null;
		
		try {
			
			String sql = "SELECT cnorcd,cnodsc,cnof04 FROM cnofc WHERE cnocfl='PV' AND cnorcd = ?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, cde);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNOFCPVNME();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} else {
				throw new PrivilegeNotFoundException(
					"User privilege not found exception(uid:" + cde + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}
		
		return bean;
		
	}

}