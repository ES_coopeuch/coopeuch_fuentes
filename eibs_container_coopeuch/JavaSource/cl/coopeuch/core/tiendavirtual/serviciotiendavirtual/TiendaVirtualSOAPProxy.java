package cl.coopeuch.core.tiendavirtual.serviciotiendavirtual;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import cl.coopeuch.core.tiendavirtual.ActualizaStockEntrada;
import cl.coopeuch.core.tiendavirtual.ConsultaProductoCampanaEntrada;
import cl.coopeuch.core.tiendavirtual.ConsultaProductosEntrada;
import cl.coopeuch.core.tiendavirtual.EliminaProductoFinanciadoEntrada;
import cl.coopeuch.core.tiendavirtual.GrabaDetalleProductoEntrada;
import cl.coopeuch.core.tiendavirtual.GrabaProductoFinanciadoEntrada;
import cl.coopeuch.core.tiendavirtual.ObjectFactory;
import cl.coopeuch.core.tiendavirtual.RespActualizaStock;
import cl.coopeuch.core.tiendavirtual.RespConsultaProductoCampana;
import cl.coopeuch.core.tiendavirtual.RespConsultaProductos;
import cl.coopeuch.core.tiendavirtual.RespEliminaProducto;
import cl.coopeuch.core.tiendavirtual.RespGrabaDetalleProducto;
import cl.coopeuch.core.tiendavirtual.RespGrabaProducto;

public class TiendaVirtualSOAPProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtual _service = null;
        private cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtualPortType _proxy = null;
        private Dispatch<Source> _dispatch = null;

        public Descriptor() {
            _service = new cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtual();
            initCommon();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtual(wsdlLocation, serviceName);
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getTiendaVirtualSOAP();
        }

        public cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtualPortType getProxy() {
            return _proxy;
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("tiendavirtual.core.coopeuch.cl/serviciotiendavirtual", "TiendaVirtualSOAP");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }
    }

    public TiendaVirtualSOAPProxy() {
        _descriptor = new Descriptor();
    }

    public TiendaVirtualSOAPProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public RespConsultaProductos consultaProductos(ConsultaProductosEntrada entrada) throws FaultMsg {
        return _getDescriptor().getProxy().consultaProductos(entrada);
    }

    public RespActualizaStock actualizaStock(ActualizaStockEntrada entrada) throws FaultMsg {
        return _getDescriptor().getProxy().actualizaStock(entrada);
    }

    public RespGrabaProducto grabaProductoFinanciado(GrabaProductoFinanciadoEntrada entrada) throws FaultMsg {
        return _getDescriptor().getProxy().grabaProductoFinanciado(entrada);
    }

    public RespGrabaDetalleProducto grabaDetalleProductoFinanciado(GrabaDetalleProductoEntrada entrada) throws FaultMsg {
        return _getDescriptor().getProxy().grabaDetalleProductoFinanciado(entrada);
    }

    public RespEliminaProducto eliminaProductoFinanciado(EliminaProductoFinanciadoEntrada entrada) throws FaultMsg {
        return _getDescriptor().getProxy().eliminaProductoFinanciado(entrada);
    }

    public RespConsultaProductoCampana consultaProductoCampana(ConsultaProductoCampanaEntrada entrada) throws FaultMsg {
        return _getDescriptor().getProxy().consultaProductoCampana(entrada);
    }

}