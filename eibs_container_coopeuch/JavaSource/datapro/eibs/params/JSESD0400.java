package datapro.eibs.params;
 
/*********************************************************************************************************************************/
/**  Creado     por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  19/04/2013                                                                                      **/
/**  Objetivo                :  Servicio para ingreso/Mantenimiento de UFs y IVP                                                **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/

import java.beans.Beans;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.*;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSESD0400 extends datapro.eibs.master.SuperServlet {

	protected static final int R_ENTER = 1;
	protected static final int R_LIST = 2;
	protected static final int A_MAINT = 3;
	protected static final int A_PROCESS = 4;
	protected static final int A_DELETE = 5;

	protected String LangPath = "S";

	/**
	 * JSECLI001 constructor comment.
	 */
	public JSESD0400() {
		super();
	}

	/**
	 * 
	 */
	public void destroy() {

		flexLog("free resources used by JSESD0400");

	}
	/**
	 * This method was created 
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	/**
 * This method was created in VisualAge.
 */


	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqEnter(
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		ESD040001Message ufivpMsg = null;

		try {
			ufivpMsg = new datapro.eibs.beans.ESD040001Message();
			ufivpMsg.setE01SELMONE("2");
			ufivpMsg.setE01SELMONT(user.getE01RDM());
			ufivpMsg.setE01SELYEAR(user.getE01RDY());			
			ses.setAttribute("ufivpMsg", ufivpMsg);
			flexLog(
				"About to call Page: "
					+ LangPath
					+ "ESD0400_UF_IVP_enter.jsp");
			callPage(
				LangPath + "ESD0400_UF_IVP_enter.jsp",
				req,
				res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}

	}
	
	protected void procReqList(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD040001Message ufivpMsg = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError =
				(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
					getClass().getClassLoader(),
					"datapro.eibs.beans.ELEERRMessage");
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
        userPO.setOption("C"); 
		// Send Initial data
		try {
			ufivpMsg = (ESD040001Message) mc.getMessageRecord("ESD040001");
			ufivpMsg.setH01USERID(user.getH01USR());
			ufivpMsg.setH01PROGRM("ESD040001");
			ufivpMsg.setH01TIMSYS(getTimeStamp());
			ufivpMsg.setH01SCRCOD("01");
			ufivpMsg.setH01OPECOD("0015");

			try {
				ufivpMsg.setE01SELMONE(req.getParameter("E01SELMONE"));
			}
			catch  (Exception ex) {
				ufivpMsg.setE01SELMONE("");		
			}
			
			try {
				ufivpMsg.setE01SELMONT(req.getParameter("E01SELMONT"));
			}
			catch  (Exception ex) {
				ufivpMsg.setE01SELMONT("");		
			}
			
			try {
				ufivpMsg.setE01SELYEAR(req.getParameter("E01SELYEAR"));
			}
			catch  (Exception ex) {
				ufivpMsg.setE01SELYEAR("");		
			}	
			int Nfechasel = (Integer.parseInt(ufivpMsg.getE01SELYEAR()) * 12) + Integer.parseInt(ufivpMsg.getE01SELMONT());
			int Nfechacur = (Integer.parseInt(user.getE01RDY()) * 12) + Integer.parseInt(user.getE01RDM());			
			if (Nfechasel >= Nfechacur)
			{
		        userPO.setOption("M"); 
			}
			flexLog("fechas  selecc, y la de hoy..." + Nfechasel + "  " + Nfechacur );
			flexLog("Mensaje enviado..." + ufivpMsg);
			ufivpMsg.send();
			ufivpMsg.destroy();
			flexLog("ESD040001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError =
						(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
							getClass().getClassLoader(),
							"datapro.eibs.beans.ELEERRMessage");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;

				// showERROR(msgError);
				//beanList.setNoResult(true);

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
				IsNotError = msgError.getERRNUM().equals("0");

			} 
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e + newmessage);
			throw new RuntimeException("Socket Communication Error Receiving");
		}

		try {
			  newmessage = mc.receiveMessage();
								
				if (newmessage.getFormatName().equals("ESD040001")) {
					try {
						ufivpMsg = new ESD040001Message();
						flexLog("ESD040001 Message Received");
						} catch (Exception ex) {
							flexLog("Error: " + ex);
						}
						
					ufivpMsg = (ESD040001Message) newmessage;
					flexLog("ESD040001 Message Received " + ufivpMsg);
					ses.setAttribute("error", msgError);
					ses.setAttribute("ufivpMsg", ufivpMsg);
					ses.setAttribute("userPO", userPO);

					if (IsNotError) { // There are no errors
						try {
							flexLog("userPO " + userPO.getOption());
							flexLog("About to call Page: " + LangPath + "ESD0400_UF_IVP_maint.jsp");
							callPage(LangPath + "ESD0400_UF_IVP_maint.jsp", req,	res);	
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else { // There are errors
						try {
							flexLog("About to call Page: " + LangPath
									+ "ESD0400_UF_IVP_enter.jsp");
							callPage(LangPath + "ESD0400_UF_IVP_enter.jsp", req,	res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}

				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");
					
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Data Receiving");
		}
	}
	
	protected void procActMaint(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD040001Message ufivpMsg = null;
		ELEERRMessage msgError = null;
		JBList beanList = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError =
				(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
					getClass().getClassLoader(),
					"datapro.eibs.beans.ELEERRMessage");
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			ufivpMsg = (ESD040001Message) ses.getAttribute("ufivpMsg");
			ufivpMsg.setH01USERID(user.getH01USR());
			ufivpMsg.setH01PROGRM("ESD0400");
			ufivpMsg.setH01TIMSYS(getTimeStamp());
			ufivpMsg.setH01SCRCOD("01");
			ufivpMsg.setH01OPECOD("0005");
			
//			all the fields here
			java.util.Enumeration enu = ufivpMsg.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
						}
					} catch (Exception e) {
				}
			}
			flexLog("Mensaje de enviado..." + ufivpMsg);
            mc.sendMessage(ufivpMsg);
			ufivpMsg.destroy();
			flexLog("ESD040001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError =
						(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
							getClass().getClassLoader(),
							"datapro.eibs.beans.ELEERRMessage");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;

				// showERROR(msgError);
				//beanList.setNoResult(true);

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
				IsNotError = msgError.getERRNUM().equals("0");


			} 
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e + newmessage);
			throw new RuntimeException("Socket Communication Error Receiving");
		}

		try {
			  newmessage = mc.receiveMessage();
								
				if (newmessage.getFormatName().equals("ESD040001")) {
					try {
						ufivpMsg = new ESD040001Message();
						flexLog("ESD040001 Message Received");
						} catch (Exception ex) {
							flexLog("Error: " + ex);
						}
						
						ufivpMsg = (ESD040001Message) newmessage;
						
						flexLog("Putting java beans into the session");
						ses.setAttribute("error", msgError);
						ses.setAttribute("ufivpMsg", ufivpMsg);
						ses.setAttribute("userPO", userPO);


						if (IsNotError) { // There are no errors
							try {
								flexLog("About to call Page: " + LangPath
										+ "ESD0400_UF_IVP_enter.jsp");
								callPage(LangPath + "ESD0400_UF_IVP_enter.jsp", req,	res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						} else { // There are errors
							try {
								flexLog("About to call Page: " + LangPath
										+ "ESD0400_UF_IVP_maint.jsp");
								callPage(LangPath + "ESD0400_UF_IVP_maint.jsp", req, res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						}

				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");
					
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Data Receiving");
		}
		}

	
/*
 * 
 */	
	
	protected void procActDelete(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			MessageRecord newmessage = null;
			ESD040001Message ufivpMsg = null;
			ELEERRMessage msgError = null;
			JBList beanList = null;
			UserPos userPO = null;
			boolean IsNotError = false;

			try {
				msgError =
					(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
						getClass().getClassLoader(),
						"datapro.eibs.beans.ELEERRMessage");
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			// Send Initial data
			try {
				ufivpMsg = (ESD040001Message) mc.getMessageRecord("ESD040001");
				ufivpMsg.setH01USERID(user.getH01USR());
				ufivpMsg.setH01PROGRM("ESD040001");
				ufivpMsg.setH01TIMSYS(getTimeStamp());
				ufivpMsg.setH01SCRCOD("01");
				ufivpMsg.setH01OPECOD("0005");
				
//				all the fields here
				java.util.Enumeration enu = ufivpMsg.fieldEnumeration();
				MessageField field = null;
				String value = null;
				while (enu.hasMoreElements()) {
					field = (MessageField) enu.nextElement();
					try {
						value = req.getParameter(field.getTag()).toUpperCase();
						if (value != null) {
							field.setString(value);
							}
						} catch (Exception e) {
					}
				}
				
				ufivpMsg.send();
				ufivpMsg.destroy();
				flexLog("ESD040001 Message Sent");
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {

					try {
						msgError =
							(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
								getClass().getClassLoader(),
								"datapro.eibs.beans.ELEERRMessage");
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					msgError = (ELEERRMessage) newmessage;

					// showERROR(msgError);
					//beanList.setNoResult(true);

					flexLog("Putting java beans into the session");
					ses.setAttribute("error", msgError);
					ses.setAttribute("userPO", userPO);


				} 
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e + newmessage);
				throw new RuntimeException("Socket Communication Error Receiving");
			}

			try {
				  newmessage = mc.receiveMessage();
									
					if (newmessage.getFormatName().equals("ESD040001")) {
						try {
							ufivpMsg = new ESD040001Message();
							flexLog("ESD040001 Message Received");
							} catch (Exception ex) {
								flexLog("Error: " + ex);
							}
							
							ufivpMsg = (ESD040001Message) newmessage;
							
							flexLog("Putting java beans into the session");
							ses.setAttribute("error", msgError);
							ses.setAttribute("ufivpMsg", ufivpMsg);
							ses.setAttribute("userPO", userPO);

						try {
							flexLog("About to call Page: " + LangPath + "ESD0400_UF_IVP_enter.jsp");
							callPage(LangPath + "ESD0400_UF_IVP_enter.jsp", req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}

					} else
						flexLog("Message " + newmessage.getFormatName() + " received.");
						
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Data Receiving");
			}
	}
	
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		Socket s = null;
		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			int screen = R_ENTER;

			try {

				msgUser =
					(datapro.eibs.beans.ESS0030DSMessage) session.getAttribute(
						"currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					s = new Socket(super.hostIP, getInitSocket(req) + 1);
					s.setSoTimeout(super.sckTimeOut);
					mc =
						new MessageContext(
							new DataInputStream(
								new BufferedInputStream(s.getInputStream())),
							new DataOutputStream(
								new BufferedOutputStream(s.getOutputStream())),
							"datapro.eibs.beans");

					try {
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					} catch (Exception e) {
						flexLog("Screen set to default value");
					}

					switch (screen) {
						case R_ENTER :
							procReqEnter(msgUser, req, res, session );
							break;
						case R_LIST :
							procReqList( mc, msgUser, req, res, session );
							break;
						case A_MAINT :
							procActMaint( mc, msgUser, req, res, session );
							break;
						case A_DELETE :
							procActDelete( mc, msgUser, req, res, session);
							break;	
						default :
							res.sendRedirect(super.srctx + LangPath + super.devPage);
							break;
					}

				} catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
					//return;
				} finally {
					s.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}

		}

	}
}