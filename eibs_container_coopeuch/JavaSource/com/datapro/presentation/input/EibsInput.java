package com.datapro.presentation.input;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import com.datapro.eibs.constants.StyleID;
import com.datapro.generic.tool.UDecimalFormat;
import com.datapro.generic.tool.Util;
import com.datapro.presentation.taglib.TextTag;


/**
 * @author fhernandez
 *
 */

public class EibsInput extends TextTag implements EibsInputConstants, StyleID {
	
	private static final long serialVersionUID = 9131298943157246497L;
	int type = INPUT_TYPE_TEXT;
	private int decimals = 0;
	private String onkeypress;
	private String onkeyup;
    
	/**
	 * 
	 */
	public EibsInput(int type) {
		super();
		this.type = type;
	}
	/**
	 * 
	 */
	public EibsInput() {
		super();
	}
	public int getDecimals() {
		return decimals;
	}
	public void setDecimals(int decimals) {
		this.decimals = decimals;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getOnkeypress() {
		String rt = null;
		if (!getReadonly() && onkeypress == null) {
			switch (getType()) {
				case INPUT_TYPE_INTEGER:
					rt = " enterInteger()";
					break;
				case INPUT_TYPE_DECIMAL:
					rt = " enterDecimal("
						+ decimals
						+ ") ";
					break;
				case INPUT_TYPE_SIGN_DECIMAL:
					rt = 
						" enterSignDecimal("
						+ decimals
						+ ") ";
					break;
				default:
					break;
			}			
		} else {
			rt = onkeypress;
		}

		return rt;
	}

	public String getOnkeyup() {
		String rt = null;
		if (onkeyup == null) {
			switch (getType()) {
				case INPUT_TYPE_DECIMAL:
				case INPUT_TYPE_SIGN_DECIMAL:
					rt = " checkDecimal("
							+ decimals
							+ ") ";
					break;
				default:
					break;
			}
		} else {
			rt = onkeyup;
		}
		return rt;
	}
	
	public String getKeyEvents() {
		
		StringBuffer sb = new StringBuffer();
		prepareKeyEvents(sb);
		return sb.toString();
	}
	
    protected String formatValue(Object value) {
		
        if(value == null)
            value = "";
       	String valueToFormat = value.toString();
		switch (getType()) {
			case INPUT_TYPE_INTEGER:
				if (valueToFormat.trim().equals(""))
					value = "0";
				break;
			case INPUT_TYPE_DECIMAL:
			case INPUT_TYPE_SIGN_DECIMAL:
				if (valueToFormat.trim().equals(""))
					valueToFormat = "0." + Util.addLeftChar('0', decimals, valueToFormat);
				if (decimals == UDecimalFormat.DEFAULT_DECIMAL_PART_LENGTH) {
					value = UDecimalFormat.formatCCY(valueToFormat);
				} else if (decimals > 0) {
					value = UDecimalFormat.formatCCY(valueToFormat, decimals);
				} else if (decimals == 0) {
					value = "0";
				}
				break;
			default:
				break;
		}
		
		return value.toString().trim();
	}
	
	public String getInputText(String options) {

		StringBuffer sb = new StringBuffer();
		String id = "";
		if(getId()!= null && !getId().equals("")){
			id = "id=\""+ getId()+ "\"";
		}
		sb.append(
				"<input type=\"text\" "
					+ id
					+ " name=\""
					+ getName()
					+ "\" size=\""
					+ getSize()
					+ "\" maxlength=\""
					+ getMaxlength()
					+ "\" value=\""
					+ formatValue(getValue())
					+ "\" "
					+ getKeyEvents()
					+ options
					+ " />");
		
		return sb.toString();
	}
	
	public String getInputText(
			int type,
			String fldName,
			String fldValue,
			String fldSize,
			String fldLength,
			int decimals,
			String options) {
		
		this.type = type;
		this.name = fldName; 
		this.value = fldValue;
		this.decimals = decimals;
		setSize(fldSize);
		setMaxlength(fldLength);
		
		return getInputText(options);
	}
	
	public String getInputText(
			int type,
			String fldName,
			String fldValue,
			String fldSize,
			String fldLength,
			String decimals,
			String options) {
		
		this.type = type;
		this.name = fldName; 
		this.value = fldValue; 

		setSize(fldSize);
		setMaxlength(fldLength);
		try {
			this.decimals = Integer.parseInt(decimals);
		} catch (Exception e) {}
		
		return getInputText(options);
	}	
	
	public String getInputText(
			int type,
			String fldName,
			String fldValue,
			int fldSize,
			int fldLength,
			int decimals,
			String options) {
		
		this.type = type;
		this.name = fldName; 
		this.value = fldValue; 
		setSize("" + fldSize);
		setMaxlength("" + fldLength);
		this.decimals = decimals;
		
		return getInputText(options);
	}
	
	public String getInputText(
			int type,
			String fldName,
			String fldValue,
			int fldSize,
			int fldLength,
			String options) {
		
		return getInputText(type, fldName, fldValue, "" + fldSize, "" + fldLength, 0, options);
	}
	
	public String getInputText(
			int type,
			String fldName,
			String fldValue,
			int fldSize,
			int fldLength) {
		
		return getInputText(type, fldName, fldValue, "" + fldSize, "" + fldLength, 0, "");
	}	

}