package com.datapro.eibs.facade;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import com.datapro.eibs.access.DAOFactoryKeys;
import com.datapro.eibs.access.KeySuper;
import com.datapro.eibs.access.VOSuper;
import com.datapro.eibs.constants.EOD;
import com.datapro.eibs.constants.General;
import com.datapro.eibs.exception.DatabaseAccessException;
import com.datapro.eibs.exception.FacadeException;
import com.datapro.eibs.exception.ItemNotFoundException;
import com.datapro.eibs.security.dao.HOLYDDAOView;
import com.datapro.eibs.security.dao.USERSDAOViewByGroup;
import com.datapro.eibs.security.dao.WEBCODAO;
import com.datapro.eibs.security.dao.WEBCODAOViewByUser;
import com.datapro.eibs.security.dao.WEBCODAOViewByUserSecurity;
import com.datapro.eibs.security.dao.WEBCODAOViewTreeByGroup;
import com.datapro.eibs.security.dao.WEBCODAOViewTreeByUser;
import com.datapro.eibs.security.dao.WEBMGRDAO;
import com.datapro.eibs.security.dao.WEBMMDAO;
import com.datapro.eibs.security.dao.WEBMMDAOViewByUser;
import com.datapro.eibs.security.dao.WEBMMDAOViewByUserSecurity;
import com.datapro.eibs.security.dao.WEBMMDAOViewTreeByGroup;
import com.datapro.eibs.security.dao.WEBMMDAOViewTreeByUser;
import com.datapro.eibs.security.dao.WEBMODAO;
import com.datapro.eibs.security.dao.WEBMUSDAO;
import com.datapro.eibs.security.dao.WEBOGDAO;
import com.datapro.eibs.security.dao.WEBOUDAO;
import com.datapro.eibs.security.key.CNTRLBTHKEYViewUsers;
import com.datapro.eibs.security.key.HOLYDKEYView;
import com.datapro.eibs.security.key.USERSKEYViewByGroup;
import com.datapro.eibs.security.key.USRRPTKEY;
import com.datapro.eibs.security.key.USRRPTKEYViewEODPDF;
import com.datapro.eibs.security.key.USRRPTKEYViewEndOfDayReports;
import com.datapro.eibs.security.key.USRRPTKEYViewGeneratedOnDates;
import com.datapro.eibs.security.key.USRRPTKEYViewModules;
import com.datapro.eibs.security.key.USRRPTKEYViewModulesByPeriod;
import com.datapro.eibs.security.key.USRRPTKEYViewReports;
import com.datapro.eibs.security.key.USRRPTKEYViewReportsChecked;
import com.datapro.eibs.security.key.WEBCOKEY;
import com.datapro.eibs.security.key.WEBCOKEYViewByUser;
import com.datapro.eibs.security.key.WEBCOKEYViewByUserSecurity;
import com.datapro.eibs.security.key.WEBCOKEYViewTreeByGroup;
import com.datapro.eibs.security.key.WEBCOKEYViewTreeByUser;
import com.datapro.eibs.security.key.WEBMGRKEY;
import com.datapro.eibs.security.key.WEBMMKEY;
import com.datapro.eibs.security.key.WEBMMKEYViewByUser;
import com.datapro.eibs.security.key.WEBMMKEYViewByUserSecurity;
import com.datapro.eibs.security.key.WEBMMKEYViewTreeByGroup;
import com.datapro.eibs.security.key.WEBMMKEYViewTreeByUser;
import com.datapro.eibs.security.key.WEBMOKEY;
import com.datapro.eibs.security.key.WEBMUSKEY;
import com.datapro.eibs.security.key.WEBOGKEY;
import com.datapro.eibs.security.key.WEBOUKEY;
import com.datapro.eibs.security.vo.HOLYDView;
import com.datapro.eibs.security.vo.USRRPT;
import com.datapro.eibs.security.vo.USRRPTExtAddAllReports;
import com.datapro.eibs.security.vo.WEBCO;
import com.datapro.eibs.security.vo.WEBMGR;
import com.datapro.eibs.security.vo.WEBOG;
import com.datapro.eibs.setup.key.CNTRLCNTKEYExtRunDate;
import com.datapro.eibs.setup.vo.CNTRLCNTExtRunDate;
import com.datapro.generic.beanutil.BeanList;


public class FASecurity extends FACommon {

	public static FASecurity getInstance() {
		return (FASecurity) new FASecurity();		
	}
	
	public List getOptionsMenu(String user, String codeName) throws FacadeException {
		try {
			setDbConnectionKey(General.DBID_CURRENT_YEAR);
			WEBCOKEYViewByUser key = null;
			if(getSessionUser() != null 
				&& "Y".equals(getSessionUser().getE01SEC())){
				initDao(WEBCODAOViewByUserSecurity.class);
				key = new WEBCOKEYViewByUserSecurity();
			} else {
				initDao(WEBCODAOViewByUser.class);
				key = new WEBCOKEYViewByUser();
			}
			key.setBTHUSR(user);
			key.setCMOCDE(codeName);
			return (List) getList(key).getList();

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		} 
	}
	
	public void newOptionsMenu(String user, WEBCO bean) throws FacadeException {
		try {
			setDbConnectionKey(General.DBID_CURRENT_YEAR);
			initDao(WEBCODAO.class);
			insert(bean);

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		} 
	}
	
	public void insertOptionsMenu(String user, List list) throws FacadeException {
		try {
			open(General.DBID_CURRENT_YEAR);
			initDao(WEBCODAO.class);
			Iterator iter = list.listIterator();
			while (iter.hasNext()) {
				WEBCO vo = (WEBCO) iter.next();
				WEBCOKEY key = new WEBCOKEY();
				key.setCMOSID(vo.getCMOSID());
				key.setCMODEN(vo.getCMODEN());
				key.setCMOSEQ(vo.getCMOSEQ());
				key.setCMOLIF(vo.getCMOLIF());
				try {
					getValue(key);
				} catch (ItemNotFoundException e1) {
					insert(vo);
				}
			}

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		} finally {
			close();
		}
		 
	}
	
	/**
	 * @param user id
	 * @param reportPeriod
	 * @return
	 * @throws FacadeException
	 */
	public BeanList getUserReportsModules(String uid, String reportPeriod)
		throws FacadeException {

		super.setDbConnectionKey(General.DBID_CURRENT_YEAR);
		super.setDaoCreationKey(
			DAOFactoryKeys.SECURITY_REPORTS_VIEW_MODULES_BY_PERIOD);

		USRRPTKEYViewModulesByPeriod key = new USRRPTKEYViewModulesByPeriod();
		key.setUSRUID(uid);
		key.setIBSPER(reportPeriod);

		return super.getList(key);
	}
	
	/**
	 * @param user id
	 * @param reportPeriod
	 * @return
	 * @throws FacadeException
	 */
	public BeanList getUserReportsModules(String uid)
		throws FacadeException {

		super.setDbConnectionKey(General.DBID_CURRENT_YEAR);
		super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_VIEW_MODULES);

		USRRPTKEYViewModules key = new USRRPTKEYViewModules();
		key.setUSRUID(uid);

		return super.getList(key);
	}	
	
	/**
	 * @param group id
	 * @param reportPeriod
	 * @param module
	 * @return
	 * @throws FacadeException
	 */
	public BeanList getUserReports(String uid, String reportPeriod, String module)
		throws FacadeException {

		super.setDbConnectionKey(General.DBID_CURRENT_YEAR);
		super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_VIEW_REPORTS);

		USRRPTKEYViewReports key = new USRRPTKEYViewReports();
		key.setUSRUID(uid);
		key.setIBSLIF(getSessionUser().getE01LAN());
		key.setIBSPER(reportPeriod);
		key.setIBSMOD(module);

		return super.getList(key);
	}
	
	public BeanList getEodGeneratedReports(USRRPTKEYViewReports keyReports)
		throws FacadeException {

		super.setDbConnectionKey(General.DBID_CURRENT_YEAR);
		
		//ESS0030DSMessage sessionUser = getSessionUser();
		CNTRLCNTExtRunDate cntrlibs = getRunDate();
		
		BeanList list = new BeanList();
		
		keyReports.setUSRUID(getUser());
		keyReports.setIBSLIF(getSessionUser().getE01LAN());
		if (keyReports instanceof USRRPTKEYViewGeneratedOnDates) {
			USRRPTKEYViewGeneratedOnDates key = (USRRPTKEYViewGeneratedOnDates) keyReports;
			key.setEDPFLG(keyReports.getIBSPER());
			
			if (EOD.END_OF_MONTH_REPROCESS.equals(keyReports.getIBSPER())
				|| EOD.END_OF_YEAR_REPROCESS.equals(keyReports.getIBSPER())) {
				if (key.getFROMDTY()== null){
					//Returning the reprocess report list
					BigDecimal month = cntrlibs.getCNTRDM();
					BigDecimal year = cntrlibs.getCNTRDY();
					if (cntrlibs.getCNTRDM().intValue() == 1) {
						month = TWELVE;
						year = year.subtract(ONE);
					} else {
						if (EOD.END_OF_YEAR_REPROCESS.equals(keyReports.getIBSPER())) {
							month = TWELVE;
							year = year.subtract(ONE);
						} else {
							month = month.subtract(ONE);
						}
					}
					key.setFROMDTY(year);
					key.setFROMDTM(month);
					key.setFROMDTD(ONE);
					key.setTODTY(year);
					key.setTODTM(month);
					key.setTODTD(new BigDecimal("31"));
				}
				super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_VIEW_GENERATED);
			} else {
				USRRPTKEYViewEODPDF keyByName = (USRRPTKEYViewEODPDF) keyReports;
				if (keyByName.getEDPRPN() == null || "%".equals(keyByName.getEDPRPN())){
					//Report name was not included as a key
					super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_VIEW_GENERATED);
				} else {
					super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_VIEW_EODPDF);
				}
			}
			list = super.getList(key);
		} else {
			USRRPTKEYViewEndOfDayReports key = (USRRPTKEYViewEndOfDayReports) keyReports;
			key.setEDPFLG(keyReports.getIBSPER());
			if (!EOD.END_OF_YEAR_PROCESS.equals(key.getIBSPER())) {
				//If End Of year the End Of Month Process doesn't changes the Run Date
				key.setEDPDTY(cntrlibs.getCNTRDY());
				key.setEDPDTM(cntrlibs.getCNTRDM());
				key.setEDPDTD(cntrlibs.getCNTRDD());
				super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_VIEW_EOD_REPORTS);
				list = super.getList(key);
			}
			if (list.isEmpty()) {
				if (!EOD.END_OF_YEAR_PROCESS.equals(key.getIBSPER())) {
					key.setEDPDTY(cntrlibs.getCNTPDY());
					key.setEDPDTM(cntrlibs.getCNTPDM());
					key.setEDPDTD(cntrlibs.getCNTPDD());
					super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_VIEW_EOD_REPORTS);
					list = super.getList(key);
				} else {
					key.setEDPDTY(cntrlibs.getCNTRDY().subtract(ONE));
					super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_VIEW_EOY_REPORTS);
					list = super.getList(key);
				}
			}
		}
		return list;
	}	
	
	public BeanList getEodGeneratedReports(String user, String flag)
		throws FacadeException {
		USRRPTKEYViewReports key = null;
		if (EOD.END_OF_MONTH_REPROCESS.equals(flag)
				|| EOD.END_OF_YEAR_REPROCESS.equals(flag)) {
			USRRPTKEYViewGeneratedOnDates gKey = new USRRPTKEYViewGeneratedOnDates();
			gKey.setUSRUID(user);
			gKey.setIBSPER(flag);
			key = gKey;
		} else {
			USRRPTKEYViewEndOfDayReports eKey = new USRRPTKEYViewEndOfDayReports();
			eKey.setUSRUID(user);
			eKey.setIBSPER(flag);
			key = eKey;
		}
		return getEodGeneratedReports(key);
	}
	
	public BeanList getEodGeneratedReports(
		String user,
		String reportName,
		BigDecimal fromYear,
		BigDecimal fromMonth,
		BigDecimal fromDay,
		BigDecimal toYear,
		BigDecimal toMonth,
		BigDecimal toDay)
		throws FacadeException {

		USRRPTKEYViewEODPDF key = new USRRPTKEYViewEODPDF(); 
		key.setUSRUID(user);
		key.setEDPRPN(reportName);
		key.setFROMDTY(fromYear);
		key.setFROMDTM(fromMonth);
		key.setFROMDTD(fromDay);
		key.setTODTY(toYear);
		key.setTODTM(toMonth);
		key.setTODTD(toDay);

		return getEodGeneratedReports(key);
	}
	
	public BeanList getUsers(CNTRLBTHKEYViewUsers key)
		throws FacadeException {
	
		super.setDbConnectionKey(General.DBID_CURRENT_YEAR);
		super.setDaoCreationKey(DAOFactoryKeys.SECURITY_VIEW_USERS);
	
		return super.getList(key);
	}
	
	public BeanList getUserAccessToReports(USRRPTKEYViewReportsChecked key)
		throws FacadeException {
	
		super.setDbConnectionKey(General.DBID_CURRENT_YEAR);
		super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_VIEW_USER_ACCESS);
	
		key.setIBSLIF(getSessionUser().getE01LAN());
		return super.getList(key);
	}
	
	public void removeAccessToReports(BeanList list)
		throws FacadeException {
	
		super.setDbConnectionKey(General.DBID_CURRENT_YEAR);
		super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_USER_ACCESS);
	
		list.initRow();
		while (list.getNextRow()) {
			USRRPTKEY key = (USRRPTKEY) list.getRecord();
			if (key.getUSRUID() == null) {
				throw new FacadeException("Operacion no permitida para todos los usuarios");
			}
			super.delete(key);
		}
	}
	
	public void addAccessToReports(BeanList list)
		throws FacadeException {
	
		super.setDbConnectionKey(General.DBID_CURRENT_YEAR);
		super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_USER_ACCESS);
	
		list.initRow();
		while (list.getNextRow()) {
			USRRPT bean = (USRRPT) list.getRecord();
			USRRPTKEY key = new USRRPTKEY();
			key.setUSRUID(bean.getUSRUID());
			key.setUSRRPN(bean.getUSRRPN());
			if (key.getUSRUID() == null) {
				throw new FacadeException("Operacion no permitida para todos los usuarios");
			}			
			super.delete(key);
			super.insert(bean);
		}
	}
	
	public void addAccessToAllReports(USRRPTExtAddAllReports bean)
		throws FacadeException {
	
		super.setDbConnectionKey(General.DBID_CURRENT_YEAR);
		
		if (bean.getUSRUID().equals(BLANK)) {
			throw new FacadeException("El usuario es mandatorio");
		}
	
		USRRPTKEY key = new USRRPTKEY();
		key.setUSRUID(bean.getUSRUID());
		super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_USER_ACCESS);
		super.delete(key);
		
		bean.setIBSLIF(getSessionUser().getE01LAN());
		super.setDaoCreationKey(DAOFactoryKeys.SECURITY_REPORTS_USER_ACCESS_TO_ALL);
		super.insert(bean);
	}

	public List getOptionsMainMenu(String lang) throws FacadeException {
		try {
			setDbConnectionKey(General.DBID_CURRENT_YEAR);
			initDao(WEBMODAO.class);
			WEBMOKEY key = new WEBMOKEY();
			key.setMOILIF(lang);
			return (List) getList(key).getList();

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		} 
	}
	
	public List getOptionsSubMenu(String user, String type) throws FacadeException {
		try {
			setDbConnectionKey(General.DBID_CURRENT_YEAR);
			KeySuper key;
			if ("G".equals(type)) {
				initDao(WEBCODAOViewTreeByGroup.class);
				key = new WEBCOKEYViewTreeByGroup();
			} else {
				initDao(WEBCODAOViewTreeByUser.class);
				key = new WEBCOKEYViewTreeByUser();
			}
			((WEBCOKEYViewTreeByUser) key).setBTHUSR(user);
			
			return (List) getList(key).getList();

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		} 
	}
	
	public void deleteOptionMenuByUser(String user, BeanList list) throws FacadeException {
		
		VOSuper vo = null;
		KeySuper key = null;
		try {
			setDbConnectionKey(General.DBID_CURRENT_YEAR);
			list.initRow();
			if (list.getNextRow()) {
				vo = (VOSuper) list.getRecord();
				if (vo instanceof WEBOG) {
					initDao(WEBOGDAO.class);
					key = new WEBOGKEY();
					((WEBOGKEY)key).setOGRUSR(user);
				} else {
					initDao(WEBOUDAO.class);
					key = new WEBOUKEY();
					((WEBOUKEY)key).setOUSUSR(user);
				}
				super.delete(key);
			}

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		}
	}
	
	public void updateOptionMenuByUser(String user, BeanList list) throws FacadeException {
		
		VOSuper vo = null;
		KeySuper key = null;
		try {
			open(General.DBID_CURRENT_YEAR);
			list.initRow();
			if (list.getNextRow()) {
				vo = (VOSuper) list.getRecord();
				if (vo instanceof WEBOG) {
					initDao(WEBOGDAO.class);
					key = new WEBOGKEY();
					((WEBOGKEY)key).setOGRUSR(user);
				} else {
					initDao(WEBOUDAO.class);
					key = new WEBOUKEY();
					((WEBOUKEY)key).setOUSUSR(user);
				}
				//super.delete(key);
				super.insertAll(list, true);
			}

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		} finally {
			close();
		}
	}
	
	public List getGroupList() throws FacadeException {
		try {
			setDbConnectionKey(General.DBID_CURRENT_YEAR);
			initDao(USERSDAOViewByGroup.class);
			return (List) super.getList(new USERSKEYViewByGroup());
		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		}
	}
	
	public List getMainMenu(String user) throws FacadeException {
		try {
			setDbConnectionKey(General.DBID_CURRENT_YEAR);
			WEBMMKEYViewByUser key = null;
			if(getSessionUser() != null 
				&& "Y".equals(getSessionUser().getE01SEC())){
				initDao(WEBMMDAOViewByUserSecurity.class);
				key = new WEBMMKEYViewByUserSecurity();
			} else {
				initDao(WEBMMDAOViewByUser.class);
				key = new WEBMMKEYViewByUser();
			}
			key.setBTHUSR(user);
			return (List) getList(key).getList();
		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		}
	}
	
	public List getMainMenuHeader(String lang) throws FacadeException {
		try {
			setDbConnectionKey(General.DBID_CURRENT_YEAR);
			initDao(WEBMMDAO.class);
			WEBMMKEY key = new WEBMMKEY();
			key.setMMILIF(lang);
			return (List) getList(key).getList();

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		} 
	}
	
	public List getMainMenuOptions(String user, String type) throws FacadeException {
		try {
			setDbConnectionKey(General.DBID_CURRENT_YEAR);
			KeySuper key;
			if ("G".equals(type)) {
				initDao(WEBMMDAOViewTreeByGroup.class);
				key = new WEBMMKEYViewTreeByGroup();
			} else {
				initDao(WEBMMDAOViewTreeByUser.class);
				key = new WEBMMKEYViewTreeByUser();
			}
			((WEBMMKEYViewTreeByUser) key).setBTHUSR(user);
			
			return (List) getList(key).getList();

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		} 
	}
	
	public void updateMainMenuByUser(String user, BeanList list) throws FacadeException {
		
		VOSuper vo = null;
		KeySuper key = null;
		try {
			open(General.DBID_CURRENT_YEAR);
			list.initRow();
			if (list.getNextRow()) {
				vo = (VOSuper) list.getRecord();
				if (vo instanceof WEBMGR) {
					initDao(WEBMGRDAO.class);
					key = new WEBMGRKEY();
					((WEBMGRKEY)key).setOGRUSR(user);
				} else {
					initDao(WEBMUSDAO.class);
					key = new WEBMUSKEY();
					((WEBMUSKEY)key).setOUSUSR(user);
				}
				//super.delete(key);
				super.insertAll(list, true);
			}

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		} finally {
			close();
		}
	}
	
	public void deleteMainMenuByUser(String user, BeanList list) throws FacadeException {
		VOSuper vo = null;
		KeySuper key = null;
		try {
			setDbConnectionKey(General.DBID_CURRENT_YEAR);
			list.initRow();
			if (list.getNextRow()) {
				vo = (VOSuper) list.getRecord();
				if (vo instanceof WEBMGR) {
					initDao(WEBMGRDAO.class);
					key = new WEBMGRKEY();
					((WEBMGRKEY)key).setOGRUSR(user);
				} else {
					initDao(WEBMUSDAO.class);
					key = new WEBMUSKEY();
					((WEBMUSKEY)key).setOUSUSR(user);
				}
				super.delete(key);
			}

		} catch (DatabaseAccessException e) {
			e.printStackTrace();
			throw new FacadeException(e.getMessage());
		}
	}
	
	private CNTRLCNTExtRunDate getPreviosEnd(boolean monthly) throws FacadeException {
		
		CNTRLCNTExtRunDate rDate = null;
		setDbConnectionKey(General.DBID_CURRENT_YEAR);
		super.setDaoCreationKey(DAOFactoryKeys.SETUP_CNTRLIBS_EXT_RUN_DATE);
		CNTRLCNTKEYExtRunDate key = new CNTRLCNTKEYExtRunDate();
		key.setCNTBNK(getSessionUser().getE01UBK());
		try {
			rDate = (CNTRLCNTExtRunDate) super.getValue(key);
			if (monthly) {
				rDate.setCNTRDM(rDate.getCNTRDM().subtract(ONE));
			} else {
				rDate.setCNTRDM(TWELVE);
				rDate.setCNTRDY(rDate.getCNTRDY().subtract(ONE));
			}
			Calendar cal = new GregorianCalendar(rDate.getCNTRDY().intValue(), rDate.getCNTRDM().intValue() - 1, 1);
			rDate.setCNTRDD(new BigDecimal(cal.getActualMaximum(Calendar.DAY_OF_MONTH)));
			
			HOLYDKEYView key1 = new HOLYDKEYView();
			while (true) {
				try {
					initDao(HOLYDDAOView.class);
					key1.setHOLDAD(rDate.getCNTRDD());
					key1.setHOLDAM(rDate.getCNTRDM());
					key1.setHOLDAY(rDate.getCNTRDY());
					super.getValue(key1);
					rDate.setCNTRDD(rDate.getCNTRDD().subtract(ONE));
				} catch (ItemNotFoundException e) {
					break;
				}	
			}
			
			return rDate;
			
		} catch (ItemNotFoundException e) {
			System.out.println("Error On Method getPreviosRunDate()");
			throw new FacadeException(e.toString());
		} catch (DatabaseAccessException e) {
			System.out.println("Error On Method getPreviosRunDate()");
			throw new FacadeException(e.toString());
		}
	}
	
	public CNTRLCNTExtRunDate getPreviosEndOfMonth() throws FacadeException {
		return getPreviosEnd(true);
	}

	public CNTRLCNTExtRunDate getPreviosEndOfYear() throws FacadeException {
		return getPreviosEnd(false);
	}

}
