package datapro.eibs.products;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDD020001Message;
import datapro.eibs.beans.EDD020002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;


/**
 * 
 * @author evargas
 *
 */
public class JSEDD0200 extends JSEIBSServlet {


	/**
	 *  
	 */
	private static final long serialVersionUID = -8810719221170280836L;
	
	protected static final int R_ENTER_INQ_TRANSACTION = 100;
	protected static final int R_LIST_INQ_TRANSACTION  = 200;	


	

	@Override
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
			case R_ENTER_INQ_TRANSACTION:
				procReqInqTransactionList(user, req, res, session);
				break;
			case R_LIST_INQ_TRANSACTION:
				procActionInqTransactionList(user, req, res, session);
				break;			
			default:
				forward(SuperServlet.devPage, req, res);
				break;
		}
	}
	
	protected void procReqInqTransactionList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
			//Only for actual date
			EDD020001Message msgSerch = new EDD020001Message();
			msgSerch.setE01LOGFCD(user.getBigDecimalE01RDD());
			msgSerch.setE01LOGFCM(user.getBigDecimalE01RDM());
			msgSerch.setE01LOGFCY(user.getBigDecimalE01RDY());			
			ses.setAttribute("msgSerch", msgSerch);
			
			forward("EDD0200_inq_transaction_enter_search.jsp", req, res);	
	}
	
	
	protected void procActionInqTransactionList(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession session)
			throws ServletException, IOException {
			
			MessageProcessor mp = null;
			String tipoConsulta = "";
			try {
				mp = getMessageProcessor("ECC0200", req);

				EDD020001Message msg = (EDD020001Message) mp.getMessageRecord("EDD020001");				
				setMessageRecord(req, msg);//caption  search value
				tipoConsulta = msg.getE01FLGSUP();
				msg.setH01USERID(user.getH01USR());				
				msg.setH01OPECOD("0001");
			 	mp.sendMessage(msg);
			
				// Receive salesPlatform list
				ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");//always  error read

			 	 
		        if(mp.hasError(msgError)){						
		        	 EDD020001Message hmsg = new EDD020001Message();
		        	 setMessageRecord(req, hmsg);//caption  search value	
		        	 session.setAttribute("msgSerch", hmsg);
		        	 session.setAttribute("error", msgError);
		        	 forward("EDD0200_inq_transaction_enter_search.jsp", req, res);
		        }else{
		        	//no errors...

		        	//put into session data search
		        	 EDD020001Message hmsg = new EDD020001Message();
		        	 setMessageRecord(req, hmsg);//caption  search value	
		        	 session.setAttribute("msgSerch", hmsg);		        	
		        	 
		        	 session.removeAttribute("actInqTrxlist");    	
		        	 if ("D".equals(tipoConsulta)){//read list EDD020001	
		        		JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
		        		session.setAttribute("actInqTrxlist", list);
		        	 }		        				        	
		        	 
		        	 EDD020002Message resumen = (EDD020002Message) mp.receiveMessageRecord("EDD020002");//always  error read
		        	 session.setAttribute("Resumen", resumen);		        	
		        	 forwardOnSuccess("EDD0200_inq_transaction_list.jsp", req, res);
		        }

			} finally {
				if (mp != null)	mp.close();
			}
		}	

}