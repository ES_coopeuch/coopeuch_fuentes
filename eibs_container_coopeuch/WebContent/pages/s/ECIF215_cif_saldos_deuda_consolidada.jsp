<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>

<html>

<head>

<style type="text/css">

.HRL{
	color: #804040;
	height:1pt;
}

.TRL{
	border-bottom-width : 1px;
	border-color: #D8DEE4;
	border-style : solid solid solid solid;
	width:100%;
}


 
  </style>



<jsp:useBean id="cifData" class="datapro.eibs.beans.ECIF21501Message"scope="session" />
<jsp:useBean id="cifDataSocio" class="datapro.eibs.beans.ECIF30001Message" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"
	scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"
	scope="session" />

<title>Saldos y Deuda Consolidada (M$) al <%=cifData.getE01FRF()%></title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">
<!--<link href="<%=request.getContextPath()%>/pages/print.css"
	rel="stylesheet" media=print>
--><script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<%if (!("S").equals(request.getAttribute("VS"))){//mostramos si no viene de Visado Senior %>
<SCRIPT Language="Javascript">
	 builtNewMenu(ecif10_i_opt); 	 
</SCRIPT>
<%} %>
<%!
	public String fmtNum(String i) {
		String fmt = "#,##0";
		java.text.DecimalFormat df = new java.text.DecimalFormat(fmt);
		return df.format(i);		
	}
%>

<SCRIPT Language="Javascript">
 function goQuery(screen) {
 	var dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECB0040?SCREEN="+screen+"&CUN="+document.forms[0].CUN.value; 	 	
 	CenterWindow(dir,850,650,2);
 }
 
 function goPDF() {
 	var dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECIF215?SCREEN=4&CUN=" + document.forms[0].RUT.value + "&opt=10&ID=" + document.forms[0].CUN.value; 	 	
 	CenterWindow(dir,850,650,2);
 }
</SCRIPT>
</head>

<body>

<SCRIPT> initMenu(); </SCRIPT>

<h3 align="center">Deuda Consolidada (M$) al <%=cifData.getE01FRF()%><img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="cif_saldos_deuda_consolidada.jsp,ECIF215"></h3>
<hr size="4">
<form method="post"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECIF010">
<INPUT TYPE=HIDDEN NAME="SCREEN" value="10"> <INPUT TYPE=HIDDEN
	NAME="opt" VALUE="1">

  <table class="tableinfo">
    <tr > 
        <td nowrap >       
        <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center">
          <tr id="trdark"> 
              <td nowrap width="60%">           
              <div align="left"><h4>Informaci&oacute;n del Cliente</h4></div>
              </td>
             <td nowrap width="40%" align="left"><h4> 
					 <%if (cifData.getE01FL5().equals("3")) out.print("   LISTA MAYOR RIESGO");
			 		else if (cifData.getE01FL5().equals("6")) out.print("   PEP");
			 		else out.print("     "); %> 
            </h4></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
<TABLE width= "100" class="tableinfo" ALIGN=CENTER>  
<tr id="trdark">
		<td nowrap>
		<table class=tbhead cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td nowrap  width= "02%" align="left">Cliente </td>
				<td nowrap  width= "10%" align="right">
				<input type="text" readonly size="10" name="CUN" value="<%=cifData.getE01CUN()%>">
				</td>
				<td nowrap  width= "30%" align="left">
				<input type="text" readonly size="60" name="DCUN" value="<%=cifData.getE01NOM()%>">
				</td>

				<td nowrap width="9%" align="right">
				</td>

				<td nowrap width="05%" align="left">Estado</td>

 		    <td nowrap width="10%" align="left">
				<input type="text" readonly size="11" name="EST" 
				          value="<% if (cifData.getE01EST().equals("1")) { out.print("Inactivo");}
				 	  	       else if (cifData.getE01EST().equals("2")) { out.print("Activo");  }
						       else if (cifData.getE01EST().equals("4")) { out.print("Fallecido"); }
						       else { out.print(""); }
			                    %>">
				</td>


               
				<td nowrap width="05%" align="left">Rut</td>

				<td nowrap width="10%" align="left">
				<input type="text"  readonly size="10" name="RUT" value="<%=cifData.getE01RUT()%>">
				</td>

				<td nowrap width="5%" align="right">
				</td>


				<td nowrap width="05%" align="left">Usuario</td>
				<td nowrap width="10%" align="left">
				<input type="text"  readonly size="15" name="USR" value="<%=cifData.getH01USR()%>">
				</td>

			
			
			</tr>
			<tr>
				
				<td nowrap width="02%" align="left">Convenio </td>
				<td nowrap width="10%" align="right">
				<input type="text" readonly size="10" name="CVN" value="<%=cifData.getE01CVN()%>">
				</td>
				
				<td nowrap width="30%" align="left">
				<input type="text" readonly size="60" name="DCVN" value="<%=cifData.getE01DSC()%>">
				</td>
				
				<td nowrap width="1%" align="left">
				</td>

           				
				<td nowrap width="05%" align="left">F.Socio</td>

   		        <td nowrap width="10%" align="left">
				<input type="text" readonly size="11" name="FSC" value="<%=cifData.getE01FSC()%>">
				</td>

			
				<td nowrap width="05%" align="left">Oficina </td>
				<td nowrap width="10%" align="left">
				<input type="text" readonly size="10" name="OFC" value="<%=cifData.getE01OFC()%>">
				</td>

				<td nowrap width="1%" align="left">
				</td>

	         
				<td nowrap width="05%" align="left">Fecha</td>
				<td nowrap width="10%" align="left">
				<input type="text" readonly size="15" name="HRA" value="<%=cifData.getH01TIM().substring(4,6)%>/<%=cifData.getH01TIM().substring(2,4)%>/<%=cifData.getH01TIM().substring(0,2)%> ">
				</td>	
			</tr>
		</table>
		</td>
	</tr>
</table>


<br>
 <table  class="tbenter" width="100%">
  	<tr>
		<table class="tbenter" width="90%">
			<tr>	
               <td nowrap align="center" width="20%" class="tdbkg" ><a href="javascript:goQuery('200')">Gestion Cobranza</a> </td>
               <td nowrap align="center" width="20%" >&nbsp;</td>
               <td nowrap align="center" width="20%" >&nbsp;</td>
			   <td nowrap align="center" width="20%" >&nbsp;</td>
 			   <td nowrap align="center" width="20%" >&nbsp;</td>			   
			</tr>			
		</table>  		
  	</tr>
  </table>

<br>
<h4>Indicadores Socio</h4>
<TABLE  width="100%" class="tableinfo" ALIGN=CENTER cellpadding=0 cellspacing=0 >
	<TR>
		<TD>
		<TABLE width=100% cellpadding=0 cellspacing=0  border=1>	
			<TR id="trclear">
				<Td   width="25%" ALIGN=CENTER nowrap>Calidad del Socio</Td>
				<Td   width="25%" ALIGN=CENTER nowrap>Comportamiento Pago Directo</Td>
				<Td   width="25%" ALIGN=CENTER nowrap>Comportamiento Pago Planilla</Td>
				<Td   width="25%" ALIGN=CENTER nowrap>Culpa</Td>						
			</TR>
			<TR id="trdark">
				<Td ALIGN=CENTER nowrap><%=cifDataSocio.getE01CSG()%>&nbsp;</Td>
				<Td ALIGN=CENTER nowrap><%=cifDataSocio.getE01CS3()%>&nbsp;</Td>
				<Td ALIGN=CENTER nowrap><%=cifDataSocio.getE01CS2()%>&nbsp;</Td>
				<Td ALIGN=CENTER nowrap><%=cifDataSocio.getE01CS4()%>&nbsp;</Td>		
			</TR>			
		</TABLE>
    	</TD>
	</TR>
		
</TABLE>
<br>
<!-- TABLA 2 -->
<h4>Deuda Coopeuch</h4>
<TABLE  width="100%" class="tableinfo" ALIGN=CENTER cellpadding=0 cellspacing=0 >
	<TR>
		<TD>
		<TABLE  width=100% cellpadding=0 cellspacing=0 border=1>
			<TR id="trdark">
				<Td  bgcolor="#F2F1F1"  align="left" rowspan="7">Deuda Directa x Mora</Td>	
				<Td width="20%" ALIGN=CENTER nowrap>Estado Deuda</Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF01()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF02()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF03()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF04()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF05()%></Td>				
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF06()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF07()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF08()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF09()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF10()%></Td>				
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF11()%></Td>				
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF12()%></Td>				
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF13()%></Td>					
				<Td width="8%" ALIGN=CENTER nowrap><%=cifData.getE01FRF14()%></Td>				
			</TR>
			<TR id="trclear">

				<TD ALIGN=left nowrap>Al d&iacute;a</TD>
				<TD ALIGN=right nowrap><%=cifData.getECDADIA01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDADIA02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDADIA03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDADIA04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDADIA05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDADIA06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDADIA07()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDADIA08()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDADIA09()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDADIA10()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDADIA11()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDADIA12()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDADIA13()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDADIA14()%></TD>				
			</TR>
			<TR id="trdark">
				<TD ALIGN=left nowrap>Mora 30-90 ds</TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR101()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR102()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR103()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR104()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR105()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR106()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDMOR107()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR108()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR109()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR110()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR111()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR112()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDMOR113()%></TD>		
				<TD ALIGN=right nowrap><%=cifData.getECDMOR114()%></TD>		
			</TR>
			<TR id="trclear">
				<TD ALIGN=left nowrap>Mora 90 a 180 ds</TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR201()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR202()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR203()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR204()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR205()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR206()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getECDMOR207()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR208()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR209()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR210()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR211()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR212()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getECDMOR213()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getECDMOR214()%></TD>	
			</TR>
			<TR id="trdark">
				<TD ALIGN=left nowrap>Mora 180 ds a 3 a&ntilde;os</TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR301()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR302()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR303()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR304()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR305()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR306()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDMOR307()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR308()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR309()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR310()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR311()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR312()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getECDMOR313()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR314()%></TD>	
			</TR>
			<TR id="trclear">
				<TD ALIGN=left nowrap>Mora &gt; 3 a&ntilde;os</TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR401()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR402()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR403()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR404()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR405()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR406()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDMOR407()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR408()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR409()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR410()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR411()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR412()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDMOR413()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDMOR414()%></TD>
			</TR>
			<TR id="trdark">
				<TD ALIGN=left nowrap>Total</TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL06()%></TD>		
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL12()%></TD>							
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDTOTL14()%></TD>

			</TR>



			<tr  >
				<td colspan="8"  style="height:1px;  background-color:#990000;">
     			</td>
			</tr>
			
			
			<TR id="trclear">
				<Td  bgcolor="#F2F1F1"   align="left" rowspan="4" nowrap>Deuda Indirecta x Mora</Td>
				<TD ALIGN="left" nowrap>Al d&iacute;a</TD>
				<TD align="right" nowrap><%=cifData.getECIADIA01()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA02()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA03()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA04()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA05()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA06()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA07()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA08()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA09()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA10()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA11()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA12()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA13()%></TD>
				<TD align="right" nowrap><%=cifData.getECIADIA14()%></TD>
												
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Mora 30 ds a 3 a&ntilde;os</td>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR101()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR102()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR103()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR104()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR105()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR106()%></TD>		
				<TD ALIGN=right nowrap><%=cifData.getECIMOR107()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR108()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR109()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR110()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR111()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR112()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getECIMOR113()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR114()%></TD>
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Mora &gt; 3 a&ntilde;os</td>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR201()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR202()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR203()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR204()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR205()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR206()%></TD>		
				<TD ALIGN=right nowrap><%=cifData.getECIMOR207()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR208()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR209()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR210()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR211()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR212()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getECIMOR213()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECIMOR214()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Total</td>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECITOTL07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL12()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getECITOTL13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECITOTL14()%></TD>
			</TR>




			<tr  >
				<td colspan="8"  style="height:1px;  background-color:#990000;">
     			</td>
			</tr>


			<TR id="trclear">
				<Td  bgcolor="#F2F1F1"   align="left" rowspan="4" nowrap>Deuda Directa x Tipo</Td>
				<td ALIGN=left nowrap>Consumo</td>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDPCON07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON12()%></TD>		
				<TD ALIGN=right nowrap><%=cifData.getECDPCON13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCON14()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Comercial</td>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM12()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPCOM14()%></TD>
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Hipotecario Vivienda</td>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP12()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPHIP14()%></TD>
			
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Total</td>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT06()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT12()%></TD>		
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getECDPTOT14()%></TD>
											
			</TR>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<!-- FIN TABLA 2 --> <br>




<br>
<!-- TABLA 1 -->
<h4>Sistema Financiero</h4>
<TABLE  width="100%" class="tableinfo" ALIGN=CENTER cellpadding=0 cellspacing=0 >
	<TR>
		<TD>
		<TABLE id="headTable1" width=100% cellpadding=0 cellspacing=0  border=1>
					<TR id="trdark">
				<Td bgcolor="#F2F1F1"  align="left" rowspan="7">Deuda Directa x Mora</Td>
				<Td width="20%" ALIGN=CENTER nowrap>Estado Deuda</Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF01()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF02()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF03()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF04()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF05()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF06()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF07()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF08()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF09()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF10()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF11()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF12()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF13()%></Td>
				<Td width="8%" ALIGN=CENTER nowrap><%=cifData.getE01FRF14()%></Td>				
			</TR>
			<TR id="trclear">
				<TD ALIGN=left nowrap>Al d&iacute;a</TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDADIA14()%></TD>
			</TR>
			<TR id="trdark">
				<TD ALIGN=left nowrap>Mora 30-90 ds</TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR101()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR102()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR103()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR104()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR105()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR106()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR107()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR108()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR109()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR110()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR111()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR112()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR113()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR114()%></TD>
			</TR>
			<TR id="trclear">
				<TD ALIGN=left nowrap>Mora 90 a 180 ds</TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR201()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR202()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR203()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR204()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR205()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR206()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR207()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR208()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR209()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR210()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR211()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR212()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR213()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR214()%></TD>
			</TR>
			<TR id="trdark">
				<TD ALIGN=left nowrap>Mora 180 ds a 3 a&ntilde;os</TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR301()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR302()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR303()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR304()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR305()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR306()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR307()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR308()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR309()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR310()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR311()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR312()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR313()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR314()%></TD>
			</TR>
			<TR id="trclear">
				<TD ALIGN=left nowrap>Mora &gt; 3 a&ntilde;os</TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR401()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR402()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR403()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR404()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR405()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR406()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR407()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR408()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR409()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR410()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR411()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR412()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR413()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDMOR414()%></TD>
			</TR>
			<TR id="trdark">
				<TD ALIGN=left nowrap>Total</TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDTOTL14()%></TD>
			</TR>



			<tr  >
				<td colspan="8"  style="height:1px;  background-color:#990000;">
     			</td>
			</tr>




			<TR id="trclear">
				<Td  bgcolor="#F2F1F1"  align="left" rowspan="4" nowrap>Deuda Indirecta x Mora</Td>
				<TD ALIGN="left" nowrap>Al d&iacute;a</TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA01()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA02()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA03()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA04()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA05()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA06()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA07()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA08()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA09()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA10()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA11()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA12()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA13()%></TD>
				<TD align="right" nowrap><%=cifData.getEDIADIA14()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Mora 30 ds a 3 a&ntilde;os</td>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR101()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR102()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR103()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR104()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR105()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR106()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR107()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR108()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR109()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR110()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR111()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR112()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR113()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR114()%></TD>
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Mora &gt; 3 a&ntilde;os</td>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR201()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR202()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR203()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR204()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR205()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR206()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR207()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR208()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR209()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR210()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR211()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR212()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR213()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDIMOR214()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Total</td>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDITOTL14()%></TD>
			</TR>

			<tr  >
				<td colspan="8"  style="height:1px;  background-color:#990000;">
     			</td>
			</tr>


			<TR id="trclear">
				<Td  bgcolor="#F2F1F1"  align="left" rowspan="5" nowrap>Deuda Directa x Tipo</Td>
				<td ALIGN=left nowrap>Nro. Int. c/Consumo</td>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPNIN14()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Consumo</td>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCON14()%></TD>
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Comercial</td>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPCOM14()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Hipotecario Vivienda</td>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPHIP14()%></TD>
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Total</td>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEDDPTOT14()%></TD>
			</TR>

			<tr  >
				<td colspan="8"  style="height:1px;  background-color:#990000;">
     			</td>
			</tr>



			<TR id="trdark">
				<Td  bgcolor="#F2F1F1"  align="left" rowspan="5" nowrap>Otras Deudas Directa</Td>
				<td ALIGN=left nowrap>Deuda Adquirida</td>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDADQ14()%></TD>
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Op. Financieras</td>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDOFI14()%></TD>				
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Prest. Contingentes</td>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDCTG14()%></TD>
			</TR>			
			<TR id="trclear">
				<td ALIGN=left nowrap>Leasing</td>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDLSG14()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Total</td>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEODDTOT14()%></TD>
			</TR>			


			<tr  >
				<td colspan="8"  style="height:1px;  background-color:#990000;">
     			</td>
			</tr>



			<TR id="trclear">
				<Td ALIGN=left nowrap>Linea Cred Disp.</Td>
				<td ALIGN=left nowrap>Total</td>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT06()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT12()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getELCPTOT14()%></TD>
			</TR>
		</TABLE>
    </TD>
</TR>
		
</TABLE>



<!-- FIN TABLA 1 --> <br>


<br>
<!--  TABLA 3 -->
<h4>Total General</h4>
<TABLE  width="100%" class="tableinfo" ALIGN=CENTER cellpadding=0 cellspacing=0 >
	<TR>
		<TD>
		<TABLE id="headTable1" width=100% cellpadding=0 cellspacing=0  border=1>
			<TR id="trdark">
				<Td  bgcolor="#F2F1F1"   align="left" rowspan="8" nowrap>Totales</Td>
				<Td width="20%" ALIGN=CENTER nowrap>Concepto</Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF01()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF02()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF03()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF04()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF05()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF06()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF07()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF08()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF09()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF10()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF11()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF12()%></Td>
				<Td width="6%" ALIGN=CENTER nowrap><%=cifData.getE01FRF13()%></Td>
				<Td width="8%" ALIGN=CENTER nowrap><%=cifData.getE01FRF14()%></Td>
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Consumo</td>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTCON07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON12()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTCON13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCON14()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Comercial</td>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM12()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTCOM14()%></TD>				
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Hipotecario Vivienda</td>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP12()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTHIP14()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap><b>Total</b></td>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL101()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL102()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL103()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL104()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL105()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL106()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTAL107()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL108()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL109()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL110()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL111()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL112()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTAL113()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL114()%></TD>
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Deuda Directa</td>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD001()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD002()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD003()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD004()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD005()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD006()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTDD007()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD008()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD009()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD010()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD011()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD012()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTDD013()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDD014()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Deuda Indirecta</td>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM12()%></TD>	
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTDIM14()%></TD>				
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap><b>Total</b></td>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL201()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL202()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL203()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL204()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL205()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL206()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTAL207()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL208()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL209()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL210()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL211()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL212()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETOTAL213()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETOTAL214()%></TD>
			</TR>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<!-- FIN TABLA 3 --> <br>
<br>

<%-- 
<!--  TABLA 4 -->
<h4>Acreencias Coopeuch</h4>
<TABLE  width="100%" class="tableinfo" ALIGN=CENTER cellpadding=0 cellspacing=0 >
	<TR>
		<TD>
		<TABLE id="headTable1" width=100% cellpadding=0 cellspacing=0  border=1 >
			<TR id="trdark">
				<Td  bgcolor="#F2F1F1"   width="20%" align="left" rowspan="7" nowrap>Saldos&nbsp;</Td>
				<Td width="20%" ALIGN=CENTER nowrap>Concepto</Td>
				<Td width="10%" ALIGN=CENTER nowrap><%=cifData.getE01FRF01()%></Td>
				<Td width="10%" ALIGN=CENTER nowrap><%=cifData.getE01FRF02()%></Td>
				<Td width="10%" ALIGN=CENTER nowrap><%=cifData.getE01FRF03()%></Td>
				<Td width="10%" ALIGN=CENTER nowrap>A&ntilde;o Anterior</Td>
				<Td width="10%" ALIGN=CENTER nowrap><%=cifData.getE01FRF05()%></Td>
				<Td width="10%" ALIGN=CENTER nowrap><%=cifData.getE01FRF06()%></Td>				
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Cuotas Participaci&oacute;n</td>
				<TD ALIGN=right nowrap><%=cifData.getEACRPAR01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRPAR02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRPAR03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRPAR04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRPAR05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRPAR06()%></TD>				
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Cuentas de Ahorro</td>
				<TD ALIGN=right nowrap><%=cifData.getEACRAHO01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRAHO02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRAHO03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRAHO04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRAHO05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRAHO06()%></TD>				
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Cuentas a la Vista</td>
				<TD ALIGN=right nowrap><%=cifData.getEACRCVI01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRCVI02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRCVI03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRCVI04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRCVI05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRCVI06()%></TD>				
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Captaciones</td>
				<TD ALIGN=right nowrap><%=cifData.getEACRCAP01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRCAP02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRCAP03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRCAP04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRCAP05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRCAP06()%></TD>				
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Varios Acreedores</td>
				<TD ALIGN=right nowrap><%=cifData.getEACRVAC01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRVAC02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRVAC03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRVAC04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRVAC05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRVAC06()%></TD>				
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap><b>Total</b></td>
				<TD ALIGN=right nowrap><%=cifData.getEACRTOT01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRTOT02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRTOT03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRTOT04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRTOT05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEACRTOT06()%></TD>				
			</TR>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<!-- FIN TABLA 4 --> 
<br>
--%>
<br>
<h4>T. Cr&eacute;dito</h4>
<TABLE  width="100%" class="tableinfo" ALIGN=CENTER cellpadding=0 cellspacing=0 >
	<TR>
		<TD>
		<TABLE id="headTable1" width=100% cellpadding=0 cellspacing=0  border=1>
		
			<TR id="trdark">
				<TD   bgcolor="#F2F1F1"  width="20%" align="left" rowspan="3" nowrap>Cupos&nbsp;</TD>
				<td  width="20%" ALIGN=left nowrap>Otorgado</td>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO01()%></TD>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO02()%></TD>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO03()%></TD>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO04()%></TD>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO05()%></TD>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO06()%></TD>				
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO07()%></TD>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO08()%></TD>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO09()%></TD>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO10()%></TD>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO11()%></TD>
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO12()%></TD>				
				<TD  width="6%" ALIGN=right nowrap><%=cifData.getETCROTO13()%></TD>
				<TD  width="8%" ALIGN=right nowrap><%=cifData.getETCROTO14()%></TD>
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Utilizado</td>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI12()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRUTI14()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap>Disponible</td>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS12()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getETCRDIS14()%></TD>
			</TR>
			</TABLE>			
		</TD>
	</TR>
</TABLE>

<br>
<h4>Garant&iacute;as</h4>
<TABLE  width="100%" class="tableinfo" ALIGN=CENTER cellpadding=0 cellspacing=0 >
	<TR>
		<TD>
		<TABLE id="headTable1" width=100% cellpadding=0 cellspacing=0  border=1>
			<TR id="trdark">
				<Td  bgcolor="rgb(239, 237, 237)"  width="20%" align="left" rowspan="3" nowrap>Valores</Td>
				<Td width="20%" ALIGN=left nowrap>Generales</TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR01()%></TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR02()%></TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR03()%></TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR04()%></TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR05()%></TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR06()%></TD>				
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR07()%></TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR08()%></TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR09()%></TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR10()%></TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR11()%></TD>
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR12()%></TD>				
				<Td width="6%" ALIGN=right nowrap><%=cifData.getEGARGNR13()%></TD>
				<Td width="8%" ALIGN=right nowrap><%=cifData.getEGARGNR14()%></TD>
			</TR>
			<TR id="trclear">
				<td ALIGN=left nowrap>Espec&iacute;ficas</td>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getEGARESP07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP12()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getEGARESP13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARESP14()%></TD>
			</TR>
			<TR id="trdark">
				<td ALIGN=left nowrap><b>Total</b></td>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT01()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT02()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT03()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT04()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT05()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT06()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT07()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT08()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT09()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT10()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT11()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT12()%></TD>				
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT13()%></TD>
				<TD ALIGN=right nowrap><%=cifData.getEGARTOT14()%></TD>
			</TR>
			</TABLE>			
		</TD>
	</TR>
</TABLE>
<br>
<br>
<form>
<table  class="tbenter" width="100%">
  	<tr>
		<table class="tbenter" width="90%">
			<tr>	
               <td nowrap align="center" width="20%" >&nbsp;</td>
               <td nowrap align="center" width="20%" >&nbsp;</td>
               <td nowrap align="center" width="20%" class="tdbkg" ><a href="javascript:goPDF()">Imprimir</a> </td>
			   <td nowrap align="center" width="20%" >&nbsp;</td>
 			   <td nowrap align="center" width="20%" >&nbsp;</td>			   
			</tr>			
		</table>  		
  	</tr>
  </table>
</form>
<br>

<SCRIPT language="javascript">
/*
   function tableResize() {
     adjustDifTables(headTable1,dataTable1,dataDiv1,1,1);
     adjustDifTables(headTable2,dataTable2,dataDiv2,1,1);
   }
  tableResize();
  window.onresize=tableResize;
 */ 
</SCRIPT>
</form>
</body>
</html>
