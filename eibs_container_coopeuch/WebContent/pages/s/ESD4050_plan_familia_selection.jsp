<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<%@page import="datapro.eibs.sockets.MessageRecord"%>
<%@page import="datapro.eibs.beans.ESD405001Message"%>

<html>
<head>
<title>Nuevo Plan Familia</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ESD405001List" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "planFamilia" class= "datapro.eibs.beans.ESD405001Message"  scope="session" />

<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<!--<jsp:useBean id= "servlet" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "paginaPS" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "operation" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "IDuser" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "NOBuser" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "banderaCV" class= "java.lang.String"  scope="session" />-->
<!--<jsp:useBean id= "banderaLH" class= "java.lang.String"  scope="session" />-->

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<SCRIPT Language="Javascript">

//  Process according with user selection
 var bandera;	
 function goAction(op) {
	
	
	switch (op){
	// Validate & Write 
  	case 1:  {
    	document.forms[0].APPROVAL.value = 'N';
       	break;
        }
	// Validate and Approve
	case 2:  {
		if (document.forms[0].PF[0].checked || document.forms[0].PF[1].checked){
	 		if (document.forms[0].PF[0].checked){
	 			document.forms[0].SCREEN.value = "300";
	 			document.forms[0].submit();
	 		}else{
	 			document.forms[0].SCREEN.value = "400";
	 			document.forms[0].submit();
	 		}
	 	}else{
	 		alert("Seleccione la modalidad del Plan");
	 	}
       	break;
		}
	case 3:{
		
		self.window.location.href = "<%=request.getContextPath()%>/pages/background.jsp";
		break;
		}
	}
}
</SCRIPT>
</head>

<body bgcolor="#FFFFFF">

<h3 align="center">Nuevo Plan Familia<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="plan_familia_new, ESD4050"  ></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4050" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
  <INPUT TYPE=HIDDEN NAME="APPROVAL" VALUE="N">
  <input type="hidden" name="E01FL5" size="2" maxlength="1" value="<%= client.getE01FL5().trim()%>">
  <input type="hidden" name="E01LGT" size="2" maxlength="1" value="<%= client.getE01LGT().trim()%>">
  <input type="hidden" name="E01CUN" size="2" maxlength="1" value="<%= userPO.getCusNum()%>">
  <input type="hidden" name="E01IDN" size="2" maxlength="1" value="<%= userPO.getHeader2()%>">
  <INPUT TYPE=HIDDEN NAME="IDPLANSOCIO" VALUE="<%= planFamilia.getE01PEST()%>">
  <br>
  <h4 align="center">Datos del socio Titular</h4>
  <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
				<tr>
					<td nowrap width="10%" aling="right">Cliente: </td>
					<td nowrap width="12%" aling="left" > <%=userPO.getCusNum() %> </td>
						
					<td nowrap width="6%" aling="right">RUT: </td>
					<td nowrap width="14%" aling="left" > <%=userPO.getHeader2() %> </td>
					
					<td nowrap width="6%" aling="right">ID Plan Socio: </td>
					<td nowrap width="14%" aling="left" > <%=planFamilia.getE01PEST() %> </td>
					
					<td nowrap width="8%" aling="right">Nombre: </td>
					<td nowrap width="20%" aling="left" > <%=userPO.getHeader3() %> </td>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
	<br>
    <h4 align="center">Seleccione Modalidad del Plan</h4>
    <table class="tableinfo">
    <tr > 
    	<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center" border = "0">
				<tr>
					<td nowrap width="10%" aling="left"></td>
					<td nowrap width="2%" aling="left"><input type="radio" name="PF" value=""></td>
					<td nowrap width="15%" aling="right">Titular Plan Familia </td>
					<td nowrap width="2%" aling="left"><input type="radio" name="PF" value=""></td>
					<td nowrap width="15%" aling="right">Integrante Plan Familia </td>
				</tr>
			</table> 
		</td>
      </tr>
    </table>
    <br>

  	<br>
  	
  	<br>
<table width="100%">		
  	<tr>

	  		<td width="15%">
	  		  <div align="center"> 
	     		<input id="EIBSBTN" type="button" name="Submit" value="Cancelar" onClick="javascript:goAction(3);">
	     		
	     	  </div>	
	  		</td>
			<td width="75%">
  		  		<div align="center">
				<input id="EIBSBTN" type="button" name="Submit2" value="Enviar" onClick="javascript:goAction(2);">
     	  	 	</div>	
	  		</td>
		
  	</tr>	
</table>	



</form>
</body>
</html>

