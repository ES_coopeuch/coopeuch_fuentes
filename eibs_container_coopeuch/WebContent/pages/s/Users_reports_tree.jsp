<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Seguridad de Reportes</title>
	
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
	
<style type="text/css">
  span.wait{
	font-family: "Verdana, Arial, Helvetica, sans-serif";
	font-size:10pt;
	border-color: #0b23b5;
	color: #06187F;
}
</style>

<script type="text/javascript" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.js""></script>	
<script type="text/javascript" src="<%=request.getContextPath()%>/pages/s/javascripts/Ajax.js""></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/pages/s/javascripts/json2.js""></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/plugins/jstree/_lib/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/plugins/jstree/_lib/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/plugins/jstree/_lib/jquery.hotkeys.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/plugins/jstree/jquery.jstree.js"></script>

<script type="text/javascript" class="source">

var user;

function changeLabel(value) {
	if (value == 'U') {
		document.getElementById("searchLabel").innerHTML = "Usuario :";
	} else {
	  	document.getElementById("searchLabel").innerHTML = "Grupo :";
	}  
}

function searchUsers() {
	var criteria = document.getElementById("searchCriteria").value;
    var type = getElementChecked("searchType").value;
    var userId = type == 'U' ? criteria : "";
	var group = type == 'G' ? criteria : "";    
	$("#userSpace").jstree({
		"json_data" : {
			"ajax" : {
				"url" : function (n) {
					return "<%=request.getContextPath()%>/servlet/datapro.eibs.security.JSUsersAccess?SCREEN=2";
				},
				"cache" : false, 
				"data" : function (n) {
					//Data to be sent to the server, could be key/value pairs
					return {
						"type" : type,
						"group" : group,
						"user" : userId
					};
				}
			}
		},
		"plugins" : [ "themes", "json_data", "ui" ]
		}).bind("select_node.jstree", function (event, data) {
			loadReports(data.rslt.obj.attr("id"));
		}); 
};

function loadReports(userId) {
	user = userId;
	callWaiting("reports", '<%=request.getContextPath()%>/images/gears7.gif', "Buscando reportes....");
	$("#reports").jstree({
		"json_data" : {
			"ajax" : {
				"url" : function (n) {
					return "<%=request.getContextPath()%>/servlet/datapro.eibs.security.JSUsersAccess?SCREEN=3";
				},
				"cache" : false, 
				"data" : function (n) {
					//Data to be sent to the server, could be key/value pairs
					return {
						"user" : userId,
						"type" : n.attr ? n.attr("type") : "",
						"id" : n.attr ? n.attr("id") : ""
					};
				}
			}
		},
		"plugins" : [ "themes", "json_data", "checkbox" ]
		}); 
};		

function submitChecked(){
	callWaiting("reportsDiv", '<%=request.getContextPath()%>/images/gears7.gif', "Actualizando menu de usuarios...");
	var added_ids = [];	
	$("#reports").jstree("get_checked", null, true).each 
		(function () {
			if(this.selected == 'false' && this.id.length > 2){
       			added_ids.push(this.id);
       		}
	});
	var removed_ids = [];
	$("#reports").jstree("get_unchecked", null, true).each 
		(function () {
			if(this.selected == "true" && this.id.length > 2){
       			removed_ids.push(this.id);
       		}
	});
	var url = "<%=request.getContextPath()%>/servlet/datapro.eibs.security.JSUsersAccess";
	PostXMLRequest(url, "user="+user+"&added="+added_ids+"&removed="+removed_ids+"&SCREEN=4", submitStatus, false);
}
  
function submitStatus(res){
    document.getElementById("reportsDiv").innerHTML = "";
	alert(res.responseText);
}
</script>
	
<title>Menu de usuarios</title>

</head>
<body>
<h3 align="center">Asignación de Reportes a los usuarios<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="Users_reportes_tree, UsersReports"></h3>
<hr size="4">

<div id="users" style="float: left; padding: 10px; width: 40%" >
	<div id="search">
    	<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.security.JSUsersAccess">
     		<table border="0" class="tableinfo" width="100%" >
	    		<tr align="center">
		   			<td align="right">Buscar por :&nbsp;&nbsp;</td>
		   			<td align="left">
		   				<input type="radio" name="searchType" value="U" onclick="changeLabel(this.value);" checked>Usuario&nbsp;&nbsp; 
		   				<input type="radio" name="searchType" value="G" onclick="changeLabel(this.value);">Grupo
		   			</td>
	   			</tr>
	   			<tr align="center">
		  			<td align="right"><span id="searchLabel">Usuario : </span>&nbsp;&nbsp;</td>
		  			<td align="left">
		  				<input type="text" id="searchCriteria" name="searchCriteria" value="" size="20" maxlength="10" />&nbsp;&nbsp; 
		  				<input type="button" name="Buscar" id="BuscarUser" value="Buscar" onclick="searchUsers();" />
		  			</td>
       			</tr>
    		</table>
    	</form>
   		<span id="waitDiv" class="wait"></span>
	</div>

   	<div id="userSpace" style="background-color: white;">
    	<ul id="listaUsuarios" style="background-color: white;"></ul>
   	</div>

</div>


<div id="menu" style="position: relative;  float: left; top: -25px; padding: 30px;" >
	<div id="updateMenu" style="left:200px;">
    	<div>
    		<a href="javascript:submitChecked()">
    		<img src="<%=request.getContextPath()%>/images/s/update.gif" alt="Actualizar reportes de usuario" border="0"></a>
			<span id="reportsDiv" class="wait"></span>
       </div>
    </div>
    
    <div id="reports" style="background-color: white; position: absolute;"></div>
</div>

</body>
</html>
