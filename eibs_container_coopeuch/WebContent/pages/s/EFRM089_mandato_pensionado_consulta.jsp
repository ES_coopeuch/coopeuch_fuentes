<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="datapro.eibs.master.Util"%>
<html>
<head>
<title>Consulta Mantato Pensionado</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<%@ page import="datapro.eibs.master.JSEIBSProp" %>
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id="msg" class="datapro.eibs.beans.EFRM08901Message" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">


function  goAction() {
	var pg = '<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEFRM089?SCREEN=300';	
	CenterWindow(pg,720,500,2);

}

function showPDF_NEW(url) {
	CenterNamedWindow(url,'pdf',600,500,7);
}

</SCRIPT>  

</head>
<BODY>

<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>

<h3 align="center">Consulta Mantato Pensionado<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="_mandato_pensionado_consulta, EFRM089"></h3> 
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEFRM089" >
  <input type=HIDDEN name="SCREEN" value="300">

  <table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			class="tbhead">
			<tr id="trdark">
				<td nowrap width="16%">
					<div align="right"><b>Cliente :</b></div>
				</td>
				<td nowrap width="20%">
					<div align="left">
						<input type="text" name="E89ENTCUN" size="10" maxlength="9" value="<%=msg.getE89ENTCUN()%>" readonly>
					</div>
				</td>

				<td nowrap width="16%">
					<div align="right"><b>Identificación :</b></div>
				</td>
				<td nowrap width="20%">
					<div align="left">
						<input type="text" name="E89NUMRUT" size="25" value="<%=msg.getE89NUMRUT()%>" readonly >
					</div>
				</td>

				<td nowrap width="16%">
					<div align="right"><b>Nombre :</b></div>
					</td>
				<td nowrap colspan="3">
					<div align="left">
						<input type="text" name="E89CUSNA1" size="45" maxlength="45" value="<%=msg.getE89CUSNA1()%>" readonly>
					</div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
  
<div align="center"> 
	<br>
	<%String urlPDF = JSEIBSProp.getFORMPDFURLNEW() + "PdfReporte?numCliente=" + msg.getE89ENTCUN() + "&tipoDoc=PAGAREPEN"; %>
	<input id="EIBSBTN" type="button" name="Mandato" value="Mandato" onclick="javascript:showPDF_NEW('<%= urlPDF %>')">
</div>
  

</form>
</body>
</html>
