<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.EPV120501Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

 builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
	init(opth,field,bank,ccy,field1,field2,opcod);
	showPopupHelp();
}

 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	Consulta Factor Carga Financiera en Plataforma de Venta
<%} else if (maintenance){ %>
	Mantenci�n Factor Carga Financiera en Plataforma de Venta
<%} else { %>
	Nuevo  Factor Carga Financiera en Plataforma de Venta
<%} %>

 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PVFAC_maintenance.jsp, EPV1205"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1205" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01UBK" value="<%= currUser.getE01UBK().trim()%>">
  
 <% int row = 0;%>
 
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Tipo Deuda SBIF :</div>
            </td>
            <td width="60%" > 
             <eibsinput:text property="E01PVFFL3" name="cnvObj" size="4" maxlength="4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true"/>
         	 <select name="E01PVFFL3" disabled > 
                    <option value=" " <% if (!(cnvObj.getE01PVFFL3().equals("C") ||
                                               cnvObj.getE01PVFFL3().equals("L") || 
                                               cnvObj.getE01PVFFL3().equals("H") ||
                                               cnvObj.getE01PVFFL3().equals("1") ||
                                               cnvObj.getE01PVFFL3().equals("2") ||
                                               cnvObj.getE01PVFFL3().equals("3") ||  
                                               cnvObj.getE01PVFFL3().equals("4") ||  
                                               cnvObj.getE01PVFFL3().equals("K"))) out.print("selected"); %>></option>       		        
            		<option <% if (cnvObj.getE01PVFFL3().equals("C")) out.print("selected"); %>>Deuda Consumo</option>
            		<option <% if (cnvObj.getE01PVFFL3().equals("L")) out.print("selected"); %>>Deuda Comercial</option>   
             		<option <% if (cnvObj.getE01PVFFL3().equals("H"))out.print("selected"); %>>Deuda Hipotecaria</option>   
             		<option <% if (cnvObj.getE01PVFFL3().equals("1"))out.print("selected"); %>>Lineas de Credito Disponibles</option>   
             		<option <% if (cnvObj.getE01PVFFL3().equals("2"))out.print("selected"); %>>Deuda Vencida entre 90-180</option> 
            		<option <% if (cnvObj.getE01PVFFL3().equals("3"))out.print("selected"); %>>Deuda Castigada entre 180 - 3 a�os</option> 
             		<option <% if (cnvObj.getE01PVFFL3().equals("4"))out.print("selected"); %>>Deuda Castigada > 3 a�os</option>   
             		<option <% if (cnvObj.getE01PVFFL3().equals("K"))out.print("selected"); %>>Contingente</option>              		                                    
         		 </select>         
       		 </td>    
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Porcentaje :</div>
            </td>
            <td width="70%" > 
          	<select name="E01PVFTYP" <%=readOnly?"disabled":""%>>    
             	<option value="%" <% if (cnvObj.getE01PVFTYP().equals("%"))out.print("selected"); %>>Porcentaje</option>   
             	<option value="F" <% if (cnvObj.getE01PVFTYP().equals("F"))out.print("selected"); %>>Factor</option>                                     
          	</select>                 
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="40%" > 
              <div align="right">Valor :</div>
            </td>
            <td width="60%" > 
             <eibsinput:text property="E01PVFFAC" name="cnvObj" size="4" maxlength="3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="<%=readOnly %>" />
	        </td>
           </tr>
        </table>
      </td>
    </tr>
  </table>

<%if  (!readOnly) { %>
    <div align="center"> 
        <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    </div>
<% } %>

  </form>
</body>
</HTML>
