/**
 * MultadosBcoCentral.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class MultadosBcoCentral  implements java.io.Serializable {
    private java.math.BigInteger id;  // attribute
    private java.math.BigInteger idConsulta;
    private java.math.BigInteger IFeCircular;
    private java.lang.String motivo;
    private java.math.BigDecimal monto;
    private java.math.BigInteger numeroCircular;
    private java.math.BigInteger numeroInfraccion;
    private java.lang.String idMoneda;
    private java.lang.String glosa;

    public MultadosBcoCentral() {
    }

    public java.math.BigInteger getId() {
        return id;
    }

    public void setId(java.math.BigInteger id) {
        this.id = id;
    }

    public java.math.BigInteger getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(java.math.BigInteger idConsulta) {
        this.idConsulta = idConsulta;
    }

    public java.math.BigInteger getIFeCircular() {
        return IFeCircular;
    }

    public void setIFeCircular(java.math.BigInteger IFeCircular) {
        this.IFeCircular = IFeCircular;
    }

    public java.lang.String getMotivo() {
        return motivo;
    }

    public void setMotivo(java.lang.String motivo) {
        this.motivo = motivo;
    }

    public java.math.BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(java.math.BigDecimal monto) {
        this.monto = monto;
    }

    public java.math.BigInteger getNumeroCircular() {
        return numeroCircular;
    }

    public void setNumeroCircular(java.math.BigInteger numeroCircular) {
        this.numeroCircular = numeroCircular;
    }

    public java.math.BigInteger getNumeroInfraccion() {
        return numeroInfraccion;
    }

    public void setNumeroInfraccion(java.math.BigInteger numeroInfraccion) {
        this.numeroInfraccion = numeroInfraccion;
    }

    public java.lang.String getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(java.lang.String idMoneda) {
        this.idMoneda = idMoneda;
    }

    public java.lang.String getGlosa() {
        return glosa;
    }

    public void setGlosa(java.lang.String glosa) {
        this.glosa = glosa;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        MultadosBcoCentral other = (MultadosBcoCentral) obj;
        boolean _equals;
        _equals = true
            && ((this.motivo==null && other.getMotivo()==null) || 
             (this.motivo!=null &&
              this.motivo.equals(other.getMotivo())))
            && ((this.idMoneda==null && other.getIdMoneda()==null) || 
             (this.idMoneda!=null &&
              this.idMoneda.equals(other.getIdMoneda())))
            && ((this.glosa==null && other.getGlosa()==null) || 
             (this.glosa!=null &&
              this.glosa.equals(other.getGlosa())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        MultadosBcoCentral history = (MultadosBcoCentral) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId())))
            && ((this.idConsulta==null && other.getIdConsulta()==null) || 
             (this.idConsulta!=null &&
              this.idConsulta.equals(other.getIdConsulta())))
            && ((this.IFeCircular==null && other.getIFeCircular()==null) || 
             (this.IFeCircular!=null &&
              this.IFeCircular.equals(other.getIFeCircular())))
            && ((this.monto==null && other.getMonto()==null) || 
             (this.monto!=null &&
              this.monto.equals(other.getMonto())))
            && ((this.numeroCircular==null && other.getNumeroCircular()==null) || 
             (this.numeroCircular!=null &&
              this.numeroCircular.equals(other.getNumeroCircular())))
            && ((this.numeroInfraccion==null && other.getNumeroInfraccion()==null) || 
             (this.numeroInfraccion!=null &&
              this.numeroInfraccion.equals(other.getNumeroInfraccion())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        MultadosBcoCentral history = (MultadosBcoCentral) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getIdConsulta() != null) {
            _hashCode += getIdConsulta().hashCode();
        }
        if (getIFeCircular() != null) {
            _hashCode += getIFeCircular().hashCode();
        }
        if (getMotivo() != null) {
            _hashCode += getMotivo().hashCode();
        }
        if (getMonto() != null) {
            _hashCode += getMonto().hashCode();
        }
        if (getNumeroCircular() != null) {
            _hashCode += getNumeroCircular().hashCode();
        }
        if (getNumeroInfraccion() != null) {
            _hashCode += getNumeroInfraccion().hashCode();
        }
        if (getIdMoneda() != null) {
            _hashCode += getIdMoneda().hashCode();
        }
        if (getGlosa() != null) {
            _hashCode += getGlosa().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
