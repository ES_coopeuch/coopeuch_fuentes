<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="ebl0110Msg" class="datapro.eibs.beans.EBL011001Message" 	scope="session" />
	
<html>
<head>
<link rel="stylesheet" href="../../theme/Master.css" type="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<title>Nuevo Cliente</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="GENERATOR" content="Rational Software Architect">
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
</head>
<body bgcolor="#FFFFFF">
<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
%>
<% 
	String opt = (String)session.getAttribute("OPT");
	if(opt.equals("2"))
	{
%>
<h3 align="center">Nuevo Bloqueo de Cliente<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="client_enter_new, EBL0110"></h3>
<%	
	}
	if(opt.equals("3"))
	{
 %>
 <h3 align="center">Mantenimiento Bloqueo de Cliente<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="client_enter_new, EBL0110"></h3>
<%
	}
	if(opt.equals("4"))
	{
 %>
 <h3 align="center">Consulta Bloqueo de Cliente<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="client_enter_new, EBL0110"></h3>	
<%
	}
 %>

<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSEBL0110">
<INPUT TYPE=HIDDEN NAME="SCREEN" value="<%=(String)session.getAttribute("OPT")%>">
<table class="tableinfo">
	<tr>
		<td nowrap width="100%">
			<table cellspacing="0" cellpadding="2"  border="0" width="100%">
				<tr id="trdark">
				<td nowrap width="17%">
					<div align="right" >País :</div>
				</td>
				<td width="923"><INPUT TYPE="text"
					NAME="E01CURPID" value="<%=ebl0110Msg.getE01CURPID()%>" size="5"
					maxlength="4"> <INPUT TYPE="text" NAME="COUNTRYDSC"
					readonly size="45" value="<%=ebl0110Msg.getD01CURPID()%>"> <IMG
					src="<%=request.getContextPath()%>/images/Check.gif"
					alt="Campo Obligatorio" align="bottom"> <a
					href="javascript:GetCodeDescCNOFC('E01CURPID','COUNTRYDSC','03')">
				<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ."
					align="bottom" border="0"></a></td>
				</tr>
			</table>
			<table cellspacing="0" cellpadding="2"  border="0" width="100%">
				<tr id="trclear">
					<td nowrap width="18%">
						<div align="right">Identificación :</div>
					</td>
					<td width="33%">
						<input type="text" name="E01CURIDN"
						value="<%=ebl0110Msg.getE01CURIDN()%>" maxlength="25" size="28">
					</td>
					<td nowrap width="12%">
						<div align="right">Tipo de Identificación :</div>
					 </td>
					<td width="36%">
						<input
						type="text" name="E01CURTID" value="<%=ebl0110Msg.getE01CURTID()%>" maxlength="4" size="5"> <a
						href="javascript:GetCodeDescCNOFC('E01CURTID','','34')"> <img
						src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ."
						align="bottom" border="0"></a> <IMG
						src="<%=request.getContextPath()%>/images/Check.gif"
						alt="Campo Obligatorio" align="bottom">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<div align="center"> 
  	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
</form>
</body>
</html>
