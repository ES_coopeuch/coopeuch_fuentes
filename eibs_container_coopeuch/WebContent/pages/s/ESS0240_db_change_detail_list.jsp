<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Historial de Mantenimiento</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "dbList" 	class= "datapro.eibs.beans.JBObjList"  			scope="session" />
<jsp:useBean id= "userPO" 	class= "datapro.eibs.beans.UserPos"  			scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  	scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

</SCRIPT>
</head>

<BODY>
<h3 align="center">Historial de Mantenimiento<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="db_change_modify_list.jsp, ESS0240"></h3>
<hr size="4">
<FORM  >
<% if (userPO.getOption().equals("CLIENT_P") || userPO.getOption().equals("CLIENT_C")) {%>
	<table class="tableinfo">
	  <tr > 
	    <td> 
	      <table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
	        <tr>
	             
	            <td nowrap width="10%" align="right"> Cliente: </td>
	          <td nowrap width="12%" align="left">
	      			<%= userPO.getHeader1()%>
	          </td>
	            <td nowrap width="6%" align="right">ID:  
	            </td>
	          <td nowrap width="14%" align="left">
	      			<%= userPO.getHeader2()%>
	          </td>
	            <td nowrap width="8%" align="right"> Nombre: </td>
	          <td nowrap width="50%"align="left">
	      			<%= userPO.getHeader3()%>
	          </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>
<%} else {%>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="14%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="9%" > 
              <div align="left"> 
                <input type="text" name="E02CUN2" size="9" maxlength="9" readonly value="<%= userPO.getHeader2().trim()%>">
              </div>
            </td>
            <td nowrap width="12%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3"> 
              <div align="left"> 
                <input type="text" name="E02NA12" size="45" maxlength="45" readonly value="<%= userPO.getHeader3().trim()%>">
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="14%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="9%"> 
              <div align="left"> 
                <input type="text" name="E02ACC" size="12" maxlength="12" value="<%= userPO.getIdentifier().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="12%"> 
              <div align="right">Producto :</div>
            </td>
            <td nowrap width="33%"> 
              <div align="left"><b> 
                <input type="text" name="E02PRO2" size="4" maxlength="4" readonly value="<%= userPO.getHeader1().trim()%>">
                </b> </div>
            </td>
            <td nowrap width="11%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="21%"> 
              <div align="left"><b> 
                <input type="text" name="E01DEACCY" size="3" maxlength="3" value="<%= userPO.getCurrency().trim()%>" readonly>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<%}%>  
  <p>
  <%if ( dbList.getNoResult() ) {%>
  </p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su criterio de b�squeda</b></p>
          <p>&nbsp;</p>
        </div>
	  </TD>
	</TR>
    </TABLE>
	<%} else {%> 
  <p> 
  <br>
         <%
          dbList.initRow();
          boolean firstTime = true;
          String chk = "";
          while (dbList.getNextRow()) {
                
                datapro.eibs.beans.ESS024003Message msgList = (datapro.eibs.beans.ESS024003Message) dbList.getRecord();
                if (firstTime){
                firstTime = false;
                 %>
				  <table  id=cfTable class="tableinfo">
				    <tr > 
				      <td NOWRAP valign="top" width="100%"> 
				        <table id="headTable" width="100%">
				          <tr> 
			                <td align="right" width="30%"> Usuario : </td>
				            <td NOWRAP  align=LEFT  width="10%"><%= msgList.getE03USERID() %></td>
			                <td align="right" width="30%"> Fecha : </td>
				            <td NOWRAP  align=LEFT  width="10%"><%= Util.formatDate(msgList.getE03DATER1(), msgList.getE03DATER2(), msgList.getE03DATER3()) %></td>
			                <td align="right" width="30%"> Hora : </td>
				            <td NOWRAP  align=LEFT  width="10%"><%= msgList.getE03VALTIM() %></td>
				          </tr>
				        </table>
			       </tr>
		  	     </table>
				          
				  <table  id=cfTable class="tableinfo">
				    <tr > 
				      <td NOWRAP valign="top" width="100%"> 
				        <table id="headTable" width="100%">
				          <tr id="trdark"> 
				            <th align=CENTER nowrap width="10%"> 
				              <div align="center">Programa</div>
				            </th>
				            <th align=CENTER nowrap width="10%"> 
				              <div align="center">Archivo</div>
				            </th>	
				            <th align=CENTER nowrap width="20%"> 
				              <div align="center">Nombre Campo </div>
				            </th>
				            <th align=CENTER nowrap width="40%"> 
				              <div align="center">Dato Anterior</div>
				            </th>
				            <th align=CENTER nowrap width="60%"> 
				              <div align="center">Nuevo Dato</div>
				            </th>
				          </tr>
                 <%
                }
                
		 %>
          <tr> 
            <td NOWRAP  align=LEFT  width="10%"><%= msgList.getE03MNPPRG() %></td>
            <td NOWRAP  align=LEFT  width="10%"><%= msgList.getE03MNPFIL() %></td>
            <td NOWRAP  align=LEFT  width="20%"><%= msgList.getE03FLDDSC() %></td>
            <td NOWRAP  align=LEFT  width="20%"><%= msgList.getE03VALBEF() %></td>
            <td NOWRAP  align=LEFT width="20%"><%= msgList.getE03VALAFT() %></td>
          </tr>
          <%}%>
        </table>
  </table>
     
<%}%>
</form>
</body>
</html>
