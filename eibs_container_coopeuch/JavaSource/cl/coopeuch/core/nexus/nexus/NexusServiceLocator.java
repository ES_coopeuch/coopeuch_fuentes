/**
 * NexusServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * q0834.18 v82708152146
 */

package cl.coopeuch.core.nexus.nexus;

public class NexusServiceLocator extends com.ibm.ws.webservices.multiprotocol.AgnosticService implements com.ibm.ws.webservices.multiprotocol.GeneratedService, cl.coopeuch.core.nexus.nexus.NexusService {

    public NexusServiceLocator() {
        super(com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
           "http://nexus.core.coopeuch.cl/nexus",
           "NexusService"));

        context.setLocatorName("cl.coopeuch.core.nexus.nexus.NexusServiceLocator");
    }

    public NexusServiceLocator(com.ibm.ws.webservices.multiprotocol.ServiceContext ctx) {
        super(ctx);
        context.setLocatorName("cl.coopeuch.core.nexus.nexus.NexusServiceLocator");
    }

    // Use to get a proxy class for nexusSOAP
    private final java.lang.String nexusSOAP_address = "http://";

    public java.lang.String getNexusSOAPAddress() {
        if (context.getOverriddingEndpointURIs() == null) {
            return nexusSOAP_address;
        }
        String overriddingEndpoint = (String) context.getOverriddingEndpointURIs().get("NexusSOAP");
        if (overriddingEndpoint != null) {
            return overriddingEndpoint;
        }
        else {
            return nexusSOAP_address;
        }
    }

    private java.lang.String nexusSOAPPortName = "NexusSOAP";

    // The WSDD port name defaults to the port name.
    private java.lang.String nexusSOAPWSDDPortName = "NexusSOAP";

    public java.lang.String getNexusSOAPWSDDPortName() {
        return nexusSOAPWSDDPortName;
    }

    public void setNexusSOAPWSDDPortName(java.lang.String name) {
        nexusSOAPWSDDPortName = name;
    }

    public cl.coopeuch.core.nexus.nexus.NexusPortType getNexusSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getNexusSOAPAddress());
        }
        catch (java.net.MalformedURLException e) {
            return null; // unlikely as URL was validated in WSDL2Java
        }
        return getNexusSOAP(endpoint);
    }

    public cl.coopeuch.core.nexus.nexus.NexusPortType getNexusSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        cl.coopeuch.core.nexus.nexus.NexusPortType _stub =
            (cl.coopeuch.core.nexus.nexus.NexusPortType) getStub(
                nexusSOAPPortName,
                (String) getPort2NamespaceMap().get(nexusSOAPPortName),
                cl.coopeuch.core.nexus.nexus.NexusPortType.class,
                "cl.coopeuch.core.nexus.nexus.NexusSOAPStub",
                portAddress.toString());
        if (_stub instanceof com.ibm.ws.webservices.engine.client.Stub) {
            ((com.ibm.ws.webservices.engine.client.Stub) _stub).setPortName(nexusSOAPWSDDPortName);
        }
        return _stub;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.coopeuch.core.nexus.nexus.NexusPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                return getNexusSOAP();
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("WSWS3273E: Error: There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        String inputPortName = portName.getLocalPart();
        if ("NexusSOAP".equals(inputPortName)) {
            return getNexusSOAP();
        }
        else  {
            throw new javax.xml.rpc.ServiceException();
        }
    }

    public void setPortNamePrefix(java.lang.String prefix) {
        nexusSOAPWSDDPortName = prefix + "/" + nexusSOAPPortName;
    }

    public javax.xml.namespace.QName getServiceName() {
        return com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "NexusService");
    }

    private java.util.Map port2NamespaceMap = null;

    protected synchronized java.util.Map getPort2NamespaceMap() {
        if (port2NamespaceMap == null) {
            port2NamespaceMap = new java.util.HashMap();
            port2NamespaceMap.put(
               "NexusSOAP",
               "http://schemas.xmlsoap.org/wsdl/soap/");
        }
        return port2NamespaceMap;
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            String serviceNamespace = getServiceName().getNamespaceURI();
            for (java.util.Iterator i = getPort2NamespaceMap().keySet().iterator(); i.hasNext(); ) {
                ports.add(
                    com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                        serviceNamespace,
                        (String) i.next()));
            }
        }
        return ports.iterator();
    }

    public javax.xml.rpc.Call[] getCalls(javax.xml.namespace.QName portName) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName should not be null.");
        }
        if  (portName.getLocalPart().equals("NexusSOAP")) {
            return new javax.xml.rpc.Call[] {
                createCall(portName, "consultaEstadosDeCuentaPDF", "null"),
                createCall(portName, "consultaMovimientosNoFacturados", "null"),
            };
        }
        else {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName should not be null.");
        }
    }
}
