/*
 * Created on Nov 24, 2009
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package datapro.eibs.products;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDD240001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EOF011501Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ETL051001Message;
import datapro.eibs.beans.ETL051002Message;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;



/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JSETL0510 extends JSEIBSServlet {

	// CIF options
	protected static final int R_LIST 				= 1;
	protected static final int R_DET	 			= 3;
	protected static final int R_STOP				= 5;
	protected static final int R_LIST_STOP			= 6;
	protected static final int R_DET_CHK 			= 7;	

	protected static final int R_LIST_CANCEL		= 10;	
	
	// entering options
	protected static final int R_ENTER 				= 100;
	protected static final int R_CHK 				= 300;
	
	protected static final int A_ENTER 				= 200;
	protected static final int A_CHK 				= 400;
	
	protected static final int A_CHK_2				= 500;   //new
	protected static final int R_DET_2 				= 600;   //new
	protected static final int R_CHK_SEARCH_ALL     = 350;   //new
	protected static final int R_LIST_2				= 2;	//new
	
	protected static final int R_LIST_HELP 			= 1000;
	
	protected String LangPath = "/pages/s/";//new
	
	
	/* (non-Javadoc)
	 * @see datapro.eibs.master.JSEIBSServlet#processRequest(datapro.eibs.beans.ESS0030DSMessage, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.http.HttpSession, int)
	 */
	protected void processRequest(
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession session,
		
		
		int screen)
		throws ServletException, IOException {
		
	
			switch (screen) {
				case R_LIST :
					procReqList(user, req, res, session);
					break;
				case R_LIST_2 :
					procReqList_2(user, req, res, session);
					break;
				case R_DET :
					procReqDocDet(user, req, res, session);  
					break;
					
				case R_DET_2 :
					procReqDocDet_2(user, req, res, session);  
					break;
					
				case R_CHK_SEARCH_ALL : 
					procReqChkReemplazo_all( user, req, res, session);
					break;
					
				case R_DET_CHK :
					procReqChkDet(user, req, res, session);
					break;
					
				//entering options
				case R_ENTER :
					procReqSTEnterSearch(user, req, res, session);
					break;
				case R_CHK :
					procReqOFEnterSearch(user, req, res, session);
					break;
				case R_STOP :
					procReqStopSel(user, req, res, session);
					break;
				case R_LIST_STOP :
					procReqListStop(user, req, res, session);
					break;
				case R_LIST_CANCEL :
					procReqListCancel(user, req, res, session);
					break;
				case R_LIST_HELP :
					procReqListHelp(user, req, res, session);
					break;
	
				case A_ENTER :
					procActionSTEnterSearch(user, req, res, session);
					break;
				case A_CHK :
					procActionOFEnterSearch(user, req, res, session);
					break;
					
				case A_CHK_2 :
					procActionOFEnterSearch_2(user, req, res, session);
					break;
				default :
					forward("MISC_not_available.jsp", req, res);
					break;
			}
	}
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procActionOFEnterSearch(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos	userPO = getUserPos(session);
		try {
			if (userPO.getOption() == null || userPO.getOption().equals("")){
				userPO.setHeader1("3");
			}
			else
			{
			userPO.setHeader1(userPO.getOption());
			}
		} catch (Exception e){
			userPO.setHeader1("3");			
		}
		
		try {
			if (req.getParameter("E01SELSCH") == null) {
				userPO.setHeader2("");
			}
			else
			{
				userPO.setHeader2(req.getParameter("E01SELSCH").toUpperCase());
			}
		} catch (Exception e){
			userPO.setHeader2("T");
		}
		try {
			if (req.getParameter("E01SELNCH") == null) {
				userPO.setHeader3("");
			}
			else
			{
				userPO.setHeader3(req.getParameter("E01SELNCH"));
			}
		} catch (Exception e){
			userPO.setHeader3("");
		}
		try {
			if (req.getParameter("E01SELACC") == null) {
				userPO.setHeader4("");
			}
			else
			{
				userPO.setHeader4(req.getParameter("E01SELACC"));
			}			
		} catch (Exception e){
			userPO.setHeader4("");
		}
		try {
			if (req.getParameter("E01SELBNF") == null) {
				userPO.setHeader5("");
			}
			else
			{
				userPO.setHeader5(req.getParameter("E01SELBNF").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader5("");
		}
		try {
			if (req.getParameter("E01SELAPL") == null) {
				userPO.setHeader6("");
			}
			else
			{
				userPO.setHeader6(req.getParameter("E01SELAPL").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader6("");
		}
		// seleccion por numero de cheque
		try {
			if (req.getParameter("E01SELRE2") == null) {
				userPO.setHeader7("");
			}
			else
			{
				userPO.setHeader7(req.getParameter("E01SELRE2").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader7("");
		}
		// seleccion por cuenta Banco
		try {
			if (req.getParameter("E01SELNPR") == null) {
				userPO.setHeader8("");
			}
			else
			{
				userPO.setHeader8(req.getParameter("E01SELNPR").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader8("");
		}
		// seleccion por operacion origen 
		try {
			if (req.getParameter("E01SELDAC") == null) {
				userPO.setHeader9("");
			}
			else
			{
				userPO.setHeader9(req.getParameter("E01SELDAC").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader9("");
		}		
		flexLog("Putting java beans into the session");
		session.setAttribute("userPO", userPO);


		procReqList(user, req, res, session);
	}
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procActionSTEnterSearch(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos	userPO = getUserPos(session);
		try {
			if (req.getParameter("E01SELDTY") == null || req.getParameter("E01SELDTY").equals("")) {
				userPO.setHeader1("3");
			}
			else
			{
			userPO.setHeader1(req.getParameter("E01SELDTY"));
			}
		} catch (Exception e){
			userPO.setHeader1("3");			
		}
		try {
			if (req.getParameter("E01SELSCH") == null) {
				userPO.setHeader2("");
			}
			else
			{
				userPO.setHeader2(req.getParameter("E01SELSCH").toUpperCase());
			}
		} catch (Exception e){
			userPO.setHeader2("");
		}
		try {
			if (req.getParameter("E01SELNCH") == null) {
				userPO.setHeader3("");
			}
			else
			{
				userPO.setHeader3(req.getParameter("E01SELNCH"));
			}
		} catch (Exception e){
			userPO.setHeader3("");
		}
		try {
			if (req.getParameter("E01SELACC") == null) {
				userPO.setHeader4("");
			}
			else
			{
				userPO.setHeader4(req.getParameter("E01SELACC"));
			}			
		} catch (Exception e){
			userPO.setHeader4("");
		}
		try {
			if (req.getParameter("E01SELBNF") == null) {
				userPO.setHeader5("");
			}
			else
			{
				userPO.setHeader5(req.getParameter("E01SELBNF").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader5("");
		}
		try {
			if (req.getParameter("E01SELAPL") == null) {
				userPO.setHeader6("");
			}
			else
			{
				userPO.setHeader6(req.getParameter("E01SELAPL").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader6("");
		}
		
		flexLog("Putting java beans into the session");
		session.setAttribute("userPO", userPO);

		procReqList(user, req, res, session);
	}
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqListHelp(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		String PageToCall = "";
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		try {
			mp = new MessageProcessor("ETL0510");
			ETL051001Message msg = (ETL051001Message) mp.getMessageRecord("ETL051001", user.getH01USR(), "0004");
			msg.setH01SCRCOD("01");
			msg.setE01OFMBNK(user.getE01UBK());			
			String type = (req.getParameter("E01SELDTY") == null || req.getParameter("E01SELDTY").equals("")) ? "3" : req.getParameter("E01SELDTY");
			msg.setE01SELDTY(type);
			String status = (req.getParameter("E01SELSCH") == null) ? " " : req.getParameter("E01SELSCH"); 	 	
			msg.setE01SELSCH(status);
			String position = (req.getParameter("Pos") == null || req.getParameter("Pos").equals("")) ? "0" : req.getParameter("Pos");
			msg.setE01NUMREC(position);
			String account = (req.getParameter("E01SELACC") == null) ? "" : req.getParameter("E01SELACC");
			msg.setE01SELACC(account);
			flexLog("mensaje enviado procReqListHelp " + msg);
			
			mp.sendMessage(msg);
		
			JBObjList list = mp.receiveMessageRecordList("E01INDOPE");
			list.initRow();
			if (mp.hasError(list)) {
				msgError = (ELEERRMessage) list.getRecord();
				PageToCall = "error_viewer.jsp";
			} else {
				session.setAttribute("dvList", list);
				req.setAttribute("E01SELDTY", type);
				req.setAttribute("E01SELSCH", status);
				PageToCall = "ETL0510_chk_off_help.jsp";
			}
				
			flexLog("Putting java beans into the session");
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			
			forward(PageToCall, req, res);
			
		} finally {
			if (mp != null)	mp.close();
		}
	}

	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqListCancel(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		String PageToCall = "";
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		try {
			mp = new MessageProcessor("ETL0510");
			ETL051001Message msg = (ETL051001Message) mp.getMessageRecord("ETL051001", user.getH01USR(), "0004");
			msg.setH01SCRCOD("01");
			msg.setE01OFMBNK(user.getE01UBK());
			msg.setE01SELDTY("3");				
			msg.setE01SELSCH("D");
			String position = (req.getParameter("Pos") == null) ? "0" : req.getParameter("Pos");
			msg.setE01NUMREC(position);
		    flexLog("Mensaje enviado procReqListCancel  " + msg);
			mp.sendMessage(msg);
			
			JBObjList list = mp.receiveMessageRecordList("E01INDOPE");
			list.initRow();
			if (mp.hasError(list)) {
				msgError = (ELEERRMessage) list.getRecord();
				PageToCall = "error_viewer.jsp";
			} else {
				list.initRow();
				JBList beanList = new JBList();
				boolean firstTime = true;
				String marker = "";
				String myFlag = "";
				StringBuffer myRow = null;
				String chk = "";
				while (list.getNextRow()) {
					msg = (ETL051001Message) list.getRecord();
					marker = msg.getE01INDOPE();
					if (firstTime) {
						firstTime = false;
						position = (msg.getE01NUMREC().equals("")) ? "0" : msg.getE01NUMREC();
						beanList.setFirstRec(Integer.parseInt(position));
						chk = "checked";
					} else {
						chk = "";
					}				
					String showStopOff = "showChkCanDet('" + msg.getE01OFMBNK() + "', '" + msg.getE01OFMBRN() + "', '" + msg.getE01OFMCCY() + "', '" + msg.getE01OFMMCH() + "', '" + msg.getE01OFMNCH() + "')";
					myRow = new StringBuffer("<TR>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMNCH()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMCCY()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMBRN()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMMCH()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMSTS()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatDate(msg.getE01OFMID1(), msg.getE01OFMID2(), msg.getE01OFMID3()) + "</A></TD>");
					myRow.append("<TD NOWRAP><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMBNF()) + "</A></TD>");
					myRow.append("</TR>");
					beanList.addRow(myFlag, myRow.toString());
				}
				beanList.setShowNext(marker.equals("+"));
				session.setAttribute("dvList", beanList);
				PageToCall = "ETL0510_of_chk_list_cancel.jsp";
			}
				
			flexLog("Putting java beans into the session");
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			
			forward(PageToCall, req, res);
			
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqListStop(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		String PageToCall = "";
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		try {
			mp = new MessageProcessor("ETL0510");
			ETL051001Message msg = (ETL051001Message) mp.getMessageRecord("ETL051001", user.getH01USR(), "0004");
			msg.setH01SCRCOD("01");
			msg.setE01OFMBNK(user.getE01UBK());
			msg.setE01SELDTY("3");			
			msg.setE01SELSCH("D");
			String position = (req.getParameter("Pos") == null) ? "0" : req.getParameter("Pos");
			msg.setE01NUMREC(position);
			flexLog("mensaje enviado procReqListStop " + msg);
			mp.sendMessage(msg);
			
			JBObjList list = mp.receiveMessageRecordList("E01INDOPE");
			list.initRow();
			if (mp.hasError(list)) {
				msgError = (ELEERRMessage) list.getRecord();
				PageToCall = "error_viewer.jsp";
			} else {
				list.initRow();
				JBList beanList = new JBList();
				boolean firstTime = true;
				String marker = "";
				String myFlag = "";
				StringBuffer myRow = null;
				String chk = "";
				while (list.getNextRow()) {
					msg = (ETL051001Message) list.getRecord();
					marker = msg.getE01INDOPE();
					if (firstTime) {
						firstTime = false;
						position = (msg.getE01NUMREC().equals("")) ? "0" : msg.getE01NUMREC();
						beanList.setFirstRec(Integer.parseInt(position));
						chk = "checked";
					} else {
						chk = "";
					}				
					String showStopOff = "setChkDet('" + msg.getE01OFMBNK() + "', '" + msg.getE01OFMBRN() + "', '" + msg.getE01OFMCCY() + "', '" + msg.getE01OFMMCH() + "', '" + msg.getE01OFMNCH() + "')";
					myRow = new StringBuffer("<TR>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMNCH()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMCCY()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMBRN()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMMCH()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMSTS()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMFTY()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatDate(msg.getE01OFMID1(), msg.getE01OFMID2(), msg.getE01OFMID3()) + "</A></TD>");
					myRow.append("<TD NOWRAP><A HREF=\"javascript:" + showStopOff + "\">" + Util.formatCell(msg.getE01OFMBNF()) + "</A></TD>");
					myRow.append("</TR>");
					beanList.addRow(myFlag, myRow.toString());
				}
				beanList.setShowNext(marker.equals("+"));
				session.setAttribute("dvList", beanList);
				PageToCall = "ETL0510_of_chk_list.jsp";
			}
			
			flexLog("Putting java beans into the session");
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			
			forward(PageToCall, req, res);
			
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqStopSel(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		userPO.setOption("OCK");
		userPO.setPurpose("STOP_PAYMENT");
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		forward("EFE01000_off_enter_stop_pay.jsp", req, res);
	}
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqOFEnterSearch(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		userPO.setOption("OF");
		userPO.setOption(req.getParameter("OPTION"));
		userPO.setPurpose("INQUIRY");
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		session.removeAttribute("checkFilter");
		forward("ETL0510_chk_off_inq_sel_identif.jsp", req, res);
	}
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqSTEnterSearch(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		userPO.setOption("DV");
		userPO.setPurpose("INQUIRY");
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		forward("ETL0510_dv_inq_sel.jsp", req, res);

	}
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqChkDet(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException {
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		try {
			mp = new MessageProcessor("ETL0510");
			ETL051002Message msg = (ETL051002Message) mp.getMessageRecord("ETL051002", user.getH01USR(), "0004");
		
			String bank = (req.getParameter("BNK") == null) ? "01" : req.getParameter("BNK");
			msg.setE02OFMBNK(bank);
			String branch = (req.getParameter("BRN") == null) ? "0" : req.getParameter("BRN");
			msg.setE02OFMBRN(branch);
			String currency = (req.getParameter("CCY") == null) ? "" : req.getParameter("CCY");
			msg.setE02OFMCCY(currency);
			String amount = (req.getParameter("AMT") == null) ? "0.00" : req.getParameter("AMT");
			msg.setE02OFMMCH(amount);
			String check = (req.getParameter("CHK") == null) ? "" : req.getParameter("CHK");
			msg.setE02OFMNCH(check);
			flexLog("mensaje enviado procReqChkDet " + msg);
			mp.sendMessage(msg);
			
			MessageRecord newmsg = mp.receiveMessageRecord();
			
			if (mp.hasError(newmsg)) {
				msgError = (ELEERRMessage) newmsg;
				session.setAttribute("error", msgError);
				res.setContentType("text/html");
				printClose(res.getWriter());
			} else {
				msg = (ETL051002Message) newmsg; 
				session.setAttribute("dvDocDet", msg);
				session.setAttribute("error", msgError);
				res.sendRedirect(super.srctx + "/servlet/datapro.eibs.products.JSEOF0100?SCREEN=7");	
			}
			
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqDocDet(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		String PageToCall = "";
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		try {
			mp = new MessageProcessor("ETL0510");
			ETL051002Message msg = (ETL051002Message) mp.getMessageRecord("ETL051002", user.getH01USR(), "0004");
			
			String bank = (req.getParameter("BNK") == null) ? "01" : req.getParameter("BNK");
			msg.setE02OFMBNK(bank);
			String branch = (req.getParameter("BRN") == null) ? "0" : req.getParameter("BRN");
			msg.setE02OFMBRN(branch);
			String currency = (req.getParameter("CCY") == null) ? "" : req.getParameter("CCY");
			msg.setE02OFMCCY(currency);
			String amount = (req.getParameter("AMT") == null) ? "0.00" : req.getParameter("AMT");
			msg.setE02OFMMCH(amount);
			String check = (req.getParameter("CHK") == null) ? "" : req.getParameter("CHK");
			msg.setE02OFMNCH(check);
			flexLog("mensaje enviado procReqDocDet " + msg);
			mp.sendMessage(msg);
			
			MessageRecord newmsg = mp.receiveMessageRecord();
			
			if (mp.hasError(newmsg)) {
				msgError = (ELEERRMessage) newmsg;
				session.setAttribute("error", msgError);
				res.setContentType("text/html");
				printClose(res.getWriter());
			} else {
				msg = (ETL051002Message) newmsg; 
				session.setAttribute("dvDocDet", msg);
				session.setAttribute("error", msgError);
				if (msg.getE02OFMDTY().equals("1")) { //Offical Check
					PageToCall = "ETL0510_chk_off_inq_det.jsp";	
				} else {			
					PageToCall = "ETL0510_dv_inq_doc_det.jsp";	
				}
				forward(PageToCall, req, res);
			}
			
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqList(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		String PageToCall = "";
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		try {
			mp = new MessageProcessor("ETL0510");
			ETL051001Message msg = (ETL051001Message) session.getAttribute("checkFilter");
			if (msg == null) {
				msg = (ETL051001Message) mp.getMessageRecord("ETL051001");
				try{
					// all the fields here
				 	java.util.Enumeration enu = msg.fieldEnumeration();
					MessageField field = null;
					String value = null;
					while (enu.hasMoreElements()) {
						field = (MessageField)enu.nextElement();
						try {
							value = req.getParameter(field.getTag()).toUpperCase();
							if (value != null) {
								field.setString(value);
							}
						} catch (Exception e) {
						}	
					}
				} catch (Exception e) {
				}
			}
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0004");
			
			msg.setE01SELDTY(userPO.getHeader1());
	        msg.setE01SELSCH(userPO.getHeader2());
	        msg.setE01SELNCH(userPO.getHeader3());
	        msg.setE01SELACC(userPO.getHeader4());
	        msg.setE01SELBNF(userPO.getHeader5());
	        msg.setE01SELAPL(userPO.getHeader6());	
	        msg.setE01SELRE2(userPO.getHeader7());
	        msg.setE01SELNPR(userPO.getHeader8());
	        msg.setE01SELDAC(userPO.getHeader9());	        
			String position = (req.getParameter("Pos") == null) ? "0" : req.getParameter("Pos");		
			msg.setE01NUMREC(position);
			flexLog("mensaje enviado procReqList " + msg);

			ETL051001Message msgaux = (ETL051001Message) msg;

			mp.sendMessage(msg);
			
			JBObjList list = (JBObjList) mp.receiveMessageRecordList("E01INDOPE", "E01NUMREC");
			list.initRow();
			if (mp.hasError(list)) {
				msgError = (ELEERRMessage) list.getRecord();
				if (userPO.getOption().equals("OF")) {
					PageToCall = "ETL0510_chk_off_inq_sel_identif.jsp";					
				} else { 
					PageToCall = "ETL0510_dv_inq_sel.jsp";
				}
			} else {
				list.initRow();
				JBList beanList = new JBList();
				boolean firstTime = true;
				String marker = "";
				String myFlag = "";
				StringBuffer myRow = null;
				String chk = "";
				while (list.getNextRow()) {
					msg = (ETL051001Message) list.getRecord();
					flexLog("mensaje recibido " + msg);
					marker = msg.getE01INDOPE();
					if (firstTime) {
						userPO.setHeader1(msg.getE01SELDTY());
				        userPO.setHeader2(msg.getE01SELSCH());
				        userPO.setHeader3(msg.getE01SELNCH());
				        userPO.setHeader4(msg.getE01SELACC());
				        userPO.setHeader5(msg.getE01SELBNF());
				        userPO.setHeader6(msg.getE01SELAPL());	
				        userPO.setHeader7(msg.getE01SELRE2());
				        userPO.setHeader8(msg.getE01SELNPR());
				        userPO.setHeader9(msg.getE01SELDAC());
						firstTime = false;
						position = (msg.getE01NUMREC().equals("")) ? "0" : msg.getE01NUMREC();
						beanList.setFirstRec(Integer.parseInt(position));
						chk = "checked";
					} else {
						chk = "";
					}				
					String showDocFunc = "showDocDet('" + msg.getE01OFMBNK() + "', '" + msg.getE01OFMBRN() + "', '" + msg.getE01OFMCCY() + "', '" + msg.getE01OFMMCH() + "', '" + msg.getE01OFMNCH() + "')";
					myRow = new StringBuffer("<TR>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMNCH()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMCCY()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMBRN()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMMCH()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMSTS()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMFTY()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatDate(msg.getE01OFMID1(), msg.getE01OFMID2(), msg.getE01OFMID3()) + "</A></TD>");
					myRow.append("<TD NOWRAP><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMBNF()) + "</A></TD>");
					myRow.append("</TR>");
					beanList.addRow(myFlag, myRow.toString());
				}
   			    beanList.setShowNext(marker.equals("+"));
					
				session.setAttribute("dvList", beanList);
				
				if (position.equals("0") && beanList.getLastRec() == 1) {
					
					String page = "/servlet/datapro.eibs.products.JSETL0510?SCREEN=3&BNK=" + msg.getE01OFMBNK() + "&BRN=" + msg.getE01OFMBRN() + "&CCY=" + msg.getE01OFMCCY() + "&AMT=" + msg.getE01OFMMCH() + "&CHK=" + msg.getE01OFMNCH();
					res.setContentType("text/html");
					PrintWriter  out = res.getWriter();
					out.println("<HTML>");
					out.println("<HEAD>");
					out.println("<TITLE>Close</TITLE>");
					out.println("</HEAD>");
					out.println("<BODY>");
					out.println("<SCRIPT LANGUAGE=\"JavaScript\">");
					out.println("		window.location.href='" + super.webAppPath + page + "'");					
					out.println("</SCRIPT>");
					out.println("</BODY>");
					out.println("</HTML>");
					out.close();
					
				} else {
					PageToCall = "ETL0510_dv_inq_list_doc.jsp";	
				}
			}
			
			flexLog("Putting java beans into the session");
			session.setAttribute("checkFilter", msgaux);
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			
			forward(PageToCall, req, res);
			
		} finally {
			if (mp != null)	mp.close();
		}
	}

	// new
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procActionOFEnterSearch_2(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos	userPO = getUserPos(session);

		try {
			if (userPO.getOption() == null || userPO.getOption().equals("")){
				userPO.setHeader1("3");
			}
			else
			{
			userPO.setHeader1(userPO.getOption());
			}
		} catch (Exception e){
			userPO.setHeader1("3");			
		}
		
		try {
			if (req.getParameter("E01SELSCH") == null) {
				userPO.setHeader2("");
			}
			else
			{
				userPO.setHeader2(req.getParameter("E01SELSCH").toUpperCase());
			}
		} catch (Exception e){
			userPO.setHeader2("T");
		}
		try {
			if (req.getParameter("E01SELNCH") == null) {
				userPO.setHeader3("");
			}
			else
			{
				userPO.setHeader3(req.getParameter("E01SELNCH"));
			}
		} catch (Exception e){
			userPO.setHeader3("");
		}
		try {
			if (req.getParameter("E01SELACC") == null) {
				userPO.setHeader4("");
			}
			else
			{
				userPO.setHeader4(req.getParameter("E01SELACC"));
			}			
		} catch (Exception e){
			userPO.setHeader4("");
		}
		try {
			if (req.getParameter("E01SELBNF") == null) {
				userPO.setHeader5("");
			}
			else
			{
				userPO.setHeader5(req.getParameter("E01SELBNF").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader5("");
		}
		try {
			if (req.getParameter("E01SELAPL") == null) {
				userPO.setHeader6("");
			}
			else
			{
				userPO.setHeader6(req.getParameter("E01SELAPL").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader6("");
		}
		// seleccion por numero de cheque
		try {
			if (req.getParameter("E01SELRE2") == null) {
				userPO.setHeader7("");
			}
			else
			{
				userPO.setHeader7(req.getParameter("E01SELRE2").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader7("");
		}
		// seleccion por cuenta Banco
		try {
			if (req.getParameter("E01SELNPR") == null) {
				userPO.setHeader8("");
			}
			else
			{
				userPO.setHeader8(req.getParameter("E01SELNPR").toUpperCase()); 
			}			
		} catch (Exception e){
			userPO.setHeader8("");
		}
		// seleccion por operacion origen 
		try {
			if (req.getParameter("E01SELDAC") == null) {
				userPO.setHeader9("");
			}
			else
			{
				userPO.setHeader9(req.getParameter("E01SELDAC").toUpperCase());
			}			
		} catch (Exception e){
			userPO.setHeader9("");
		}		
		flexLog("Putting java beans into the session");
		session.setAttribute("userPO", userPO);


		procReqList_2(user, req, res, session);
	}
	
	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqList_2(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		String PageToCall = "";
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		try {
			mp = new MessageProcessor("ETL0510");
			ETL051001Message msg = (ETL051001Message) session.getAttribute("checkFilter");
			if (msg == null) {
				msg = (ETL051001Message) mp.getMessageRecord("ETL051001");
				try{
					// all the fields here
				 	java.util.Enumeration enu = msg.fieldEnumeration();
					MessageField field = null;
					String value = null;
					while (enu.hasMoreElements()) {
						field = (MessageField)enu.nextElement();
						try {
							value = req.getParameter(field.getTag()).toUpperCase();
							if (value != null) {
								field.setString(value);
							}
						} catch (Exception e) {
						}	
					}
				} catch (Exception e) {
				}
			}
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0004");
			
			msg.setE01SELDTY(userPO.getHeader1());
	        msg.setE01SELSCH(userPO.getHeader2());
	        msg.setE01SELNCH(userPO.getHeader3());
	        msg.setE01SELACC(userPO.getHeader4());
	        msg.setE01SELBNF(userPO.getHeader5());
	        msg.setE01SELAPL(userPO.getHeader6());	
	        msg.setE01SELRE2(userPO.getHeader7());
	        msg.setE01SELNPR(userPO.getHeader8());
	        msg.setE01SELDAC(userPO.getHeader9());	        
			String position = (req.getParameter("Pos") == null) ? "0" : req.getParameter("Pos");		
			msg.setE01NUMREC(position);
			flexLog("mensaje enviado procReqList " + msg);
			mp.sendMessage(msg);
			
			JBObjList list = mp.receiveMessageRecordList("E01INDOPE");
			list.initRow();
			if (mp.hasError(list)) {
				msgError = (ELEERRMessage) list.getRecord();
				PageToCall = "EOF0130_of_chk_search_1.jsp"; 

			} else {
				list.initRow();
				JBList beanList = new JBList();
				boolean firstTime = true;
				String marker = "";
				String myFlag = "";
				StringBuffer myRow = null;
				String chk = "";
				while (list.getNextRow()) {
					msg = (ETL051001Message) list.getRecord();
					flexLog("mensaje recibido " + msg);
					marker = msg.getE01INDOPE();
					if (firstTime) {
						userPO.setHeader1(msg.getE01SELDTY());
				        userPO.setHeader2(msg.getE01SELSCH());
				        userPO.setHeader3(msg.getE01SELNCH());
				        userPO.setHeader4(msg.getE01SELACC());
				        userPO.setHeader5(msg.getE01SELBNF());
				        userPO.setHeader6(msg.getE01SELAPL());	
				        userPO.setHeader7(msg.getE01SELRE2());
				        userPO.setHeader8(msg.getE01SELNPR());
				        userPO.setHeader9(msg.getE01SELDAC());
						firstTime = false;
						position = (msg.getE01NUMREC().equals("")) ? "0" : msg.getE01NUMREC();
						beanList.setFirstRec(Integer.parseInt(position));
						chk = "checked";
					} else {
						chk = "";
					}				
					String showDocFunc = "showDocDet_Chq('" + msg.getE01OFMBNK() + "', '" + msg.getE01OFMBRN() + "', '" + msg.getE01OFMCCY() + "', '" + msg.getE01OFMMCH() + "', '" + msg.getE01OFMNCH() + "')";
					myRow = new StringBuffer("<TR>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMNCH()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMCCY()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMBRN()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMMCH()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMSTS()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMFTY()) + "</A></TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\"><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatDate(msg.getE01OFMID1(), msg.getE01OFMID2(), msg.getE01OFMID3()) + "</A></TD>");
					myRow.append("<TD NOWRAP><A HREF=\"javascript:" + showDocFunc + "\">" + Util.formatCell(msg.getE01OFMBNF()) + "</A></TD>");
					myRow.append("</TR>");
					beanList.addRow(myFlag, myRow.toString());
				}
				beanList.setShowNext(marker.equals("+"));
				session.setAttribute("dvList", beanList);
				
				if (position.equals("0") && beanList.getLastRec() == 1) {
					
					String page = "/servlet/datapro.eibs.products.JSETL0510?SCREEN=600&BNK=" + msg.getE01OFMBNK() + "&BRN=" + msg.getE01OFMBRN() + "&CCY=" + msg.getE01OFMCCY() + "&AMT=" + msg.getE01OFMMCH() + "&CHK=" + msg.getE01OFMNCH();
					res.setContentType("text/html");
					PrintWriter  out = res.getWriter();
					out.println("<HTML>");
					out.println("<HEAD>");
					out.println("<TITLE>Close</TITLE>");
					out.println("</HEAD>");
					out.println("<BODY>");
					out.println("<SCRIPT LANGUAGE=\"JavaScript\">");
					out.println("		window.location.href='" + super.webAppPath + page + "'");					
					out.println("</SCRIPT>");
					out.println("</BODY>");
					out.println("</HTML>");
					out.close();
					
				} else {
					PageToCall = "EOF0130_dv_inq_list_doc.jsp";
				}
			}
			
			flexLog("Putting java beans into the session");
			session.setAttribute("checkFilter", msg);
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			
			forward(PageToCall, req, res);
			
		} finally {
			if (mp != null)	mp.close();
		}
	}

	
	/**
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqDocDet_2(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session ) 
		throws ServletException, IOException {
		
		UserPos	userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		
		try {
			mp = new MessageProcessor("ETL0510"); 
			ETL051002Message msg = (ETL051002Message) mp.getMessageRecord("ETL051002", user.getH01USR(), "0004");
			
			String bank = (req.getParameter("BNK") == null) ? "01" : req.getParameter("BNK");
			msg.setE02OFMBNK(bank);
			String branch = (req.getParameter("BRN") == null) ? "0" : req.getParameter("BRN");
			msg.setE02OFMBRN(branch);
			String currency = (req.getParameter("CCY") == null) ? "" : req.getParameter("CCY");
			msg.setE02OFMCCY(currency);
			String amount = (req.getParameter("AMT") == null) ? "0.00" : req.getParameter("AMT");
			msg.setE02OFMMCH(amount);
			String check = (req.getParameter("CHK") == null) ? "" : req.getParameter("CHK");
			msg.setE02OFMNCH(check);

			flexLog("mensaje enviado procReqDocDet " + msg);
			mp.sendMessage(msg);
			
			MessageRecord newmsg = mp.receiveMessageRecord();
			
			if (mp.hasError(newmsg)) {
				msgError = (ELEERRMessage) newmsg;
				session.setAttribute("error", msgError);
				res.setContentType("text/html");
				printClose(res.getWriter());
			} else {
				msg = (ETL051002Message) newmsg; 
				session.setAttribute("dvDocDet", msg);
				session.setAttribute("error", msgError);

				mp = getMessageProcessor("EOF0115", req);
				EOF011501Message msgOffChk = (EOF011501Message) mp.getMessageRecord("EOF011501", user.getH01USR(), "0001");
				msgOffChk.setH01TIMSYS(getTimeStamp());
				msgOffChk.setH01SCRCOD("01");
				msgOffChk.setH01OPECOD("0001");

				try {msgOffChk.setE01OFMFTY("CT");}
				catch (Exception e)	{msgOffChk.setE01OFMFTY("0");}
			
				try {if (req.getParameter("E01OFMCKN") != null) msgOffChk.setE01OFMCKN(req.getParameter("E01OFMCKN").trim());}
				catch (Exception e)	{msgOffChk.setE01OFMCKN("");}
			
				try {msgOffChk.setE01OFMCCY("CLP");}
				catch (Exception e)	{msgOffChk.setE01OFMCCY("0");}
				
				mp.sendMessage(msgOffChk);
				
				msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
				msgOffChk = (EOF011501Message) mp.receiveMessageRecord("EOF011501");

				
				if (mp.hasError(msgError)) {
					session.setAttribute("error", msgError);
					res.setContentType("text/html");
					printClose(res.getWriter());
				} else {
					
					session.setAttribute("error", msgError);
					session.setAttribute("offBasic", msgOffChk);
					session.setAttribute("userPO", userPO);
					forward("EOF0130_dv_inq_doc_det.jsp", req, res);
				}
			}
			
		} finally {
			if (mp != null)	mp.close();
		}
	}	
	
	protected void procReqChkReemplazo_all(
			
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			
			throws ServletException, IOException {
			
		
			try {
				flexLog("About to call Page: /pages/s/EOF0130_of_chk_search_2.jsp");
				callPage("/pages/s/" + "EOF0130_of_chk_search_2.jsp", req, res);
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
			}

		}	
	
}
