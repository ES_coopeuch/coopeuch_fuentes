/**
 * ResultsMultadosBcoCentral.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResultsMultadosBcoCentral  implements java.io.Serializable {
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.MultadosBcoCentral[] multadosBcoCentral;

    public ResultsMultadosBcoCentral() {
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.MultadosBcoCentral[] getMultadosBcoCentral() {
        return multadosBcoCentral;
    }

    public void setMultadosBcoCentral(cl.Sinacofi.coopeuch.creditos.predictorriesgo.MultadosBcoCentral[] multadosBcoCentral) {
        this.multadosBcoCentral = multadosBcoCentral;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.MultadosBcoCentral getMultadosBcoCentral(int i) {
        return multadosBcoCentral[i];
    }

    public void setMultadosBcoCentral(int i, cl.Sinacofi.coopeuch.creditos.predictorriesgo.MultadosBcoCentral value) {
        this.multadosBcoCentral[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsMultadosBcoCentral history = (ResultsMultadosBcoCentral) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ResultsMultadosBcoCentral other = (ResultsMultadosBcoCentral) obj;
        boolean _equals;
        _equals = true
            && ((this.multadosBcoCentral==null && other.getMultadosBcoCentral()==null) || 
             (this.multadosBcoCentral!=null &&
              java.util.Arrays.equals(this.multadosBcoCentral, other.getMultadosBcoCentral())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsMultadosBcoCentral history = (ResultsMultadosBcoCentral) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getMultadosBcoCentral() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMultadosBcoCentral());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMultadosBcoCentral(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
