package com.datapro.eibs.security.logic;

import java.sql.Connection;

import com.datapro.eibs.constants.General;
import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.LogicException;
import com.datapro.eibs.exception.MenuOptionNotAssignedException;
import com.datapro.eibs.exception.UserNotFoundException;
import com.datapro.eibs.security.access.jdbc.dao.DAOGroupItem;
import com.datapro.eibs.security.access.jdbc.dao.DAOUserIbs;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.sql.manager.DBConnector;

/**
 * @author ogarcia
 *
 */
public class LOUserAccess {

	/**
	 * Constructor for LOUserAccess.
	 */
	public LOUserAccess() {
		super();
	}

	public BeanList getMenu(String uid)
		throws UserNotFoundException, MenuOptionNotAssignedException, LogicException {
			
		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;
		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			String gid = "";
			if (uid.equalsIgnoreCase(Security.SECURITY_ADMINISTRATOR)) {
				gid = uid;
			}
			else {
				DAOUserIbs uibs = new DAOUserIbs(cnx);
				gid = uibs.getGroupID(uid);
			}
			DAOGroupItem menu = new DAOGroupItem(cnx);
			BeanList menuList = menu.getMenuItems(gid);
			return menuList;
		}
		catch (DAOException e) {
			throw new LogicException(e.toString());
		}
		finally {
			dbCnx.close(null);
		}
	}
}
