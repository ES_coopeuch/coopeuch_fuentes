package datapro.eibs.products;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */ 
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Random;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;
import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.EDD800001Message;
import datapro.eibs.beans.EDD800002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.ServiceLocator;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;

public class JSEDD8000 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_EXC_ENTER = 100;	
	protected static final int A_EXC_ENTER = 200;
	
	protected static final int R_EXC_GES_ENTER = 1100;
	protected static final int A_EXC_GES_LIST  = 1200;
	
	protected static final int A_EXC_GES_LIST_DETAIL  = 1300;
	protected static final int A_EXC_PROCESS  = 1400;
	protected static final int A_EXC_DELETE  = 1500;

	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		screen =  A_EXC_ENTER;
			
		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}		
		
		switch (screen) {
		case R_EXC_ENTER:
			procReqECCEnter(user, req, res, session);
			break;
		case A_EXC_ENTER:
			procActionECCEnter(user, req, res, session);
			break; 

		case R_EXC_GES_ENTER:
			procReqExcGesEnter(user, req, res, session);
			break;

		case A_EXC_GES_LIST:
			procActionExcGesList(user, req, res, session);
			break; 

		case A_EXC_GES_LIST_DETAIL:
			procActionExcGesListDetail(user, req, res, session);
			break; 

		case A_EXC_PROCESS:
			procActionExcProcess(user, req, res, session);
			break; 

		case A_EXC_DELETE:
			procActionExcDelete(user, req, res, session);
			break; 
		}		
	}
	
	protected void procReqECCEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: EDD8000_exc_apro_enter.jsp");
			forward("EDD8000_exc_apro_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

	protected void procActionECCEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;

		boolean ok = false;
		String idCarga = "";
		String fileName = "";
		
		try 
		{
			SmartUpload mySmartUpload = new SmartUpload();
			com.jspsmart.upload.File myFile = null;
			try 
			{
				mySmartUpload.initialize(config, req, res);
				mySmartUpload.upload();
				myFile = mySmartUpload.getFiles().getFile(0);
				if (myFile.getSize() > 0) 
				{
					if (myFile.getFileExt().equals("TXT") || myFile.getFileExt().equals("txt") )
					{
						//El archivo tiene datos buscamos el n�mero de Interfaz asignada.					
						mp = getMessageProcessor("EDD8000", req);					
						EDD800001Message msg = (EDD800001Message) mp.getMessageRecord("EDD800001");
				
						//seteamos las propiedades
						msg.setH01USERID(user.getH01USR());
						msg.setH01OPECOD("0001");
						msg.setH01TIMSYS(getTimeStamp());					
				
						//Sending message
						mp.sendMessage(msg);
	
						//Receive error and data
						msgError = (ELEERRMessage) mp.receiveMessageRecord();
						msg = (EDD800001Message) mp.receiveMessageRecord();		
				
				
						//havent errors i get the field
						if (!mp.hasError(msgError)) 
						{
							try 
							{
								fileName = "EDD" + getCadenaAlfanumAleatoria (6); //nombre del archivo
								idCarga = msg.getE01IDCARGA();
					
								byte[] bd = new byte[myFile.getSize()];
								for (int i = 0; i < myFile.getSize(); i++) {
									bd[i] = myFile.getBinaryData(i);
								}
					
								InputStream  input = new ByteArrayInputStream(bd);													
								String userid = ServiceLocator.getSlInfo().getString("ftp.cnx.userid.eibs-server");
								String password = ServiceLocator.getSlInfo().getString("ftp.cnx.password.eibs-server");												
					
								FTPWrapper ftp = new FTPStdWrapper(JSEIBSProp.getHostIP(), userid, password, "");
							
								if (ftp.open()) 
								{
									ftp.setFileType(FTPWrapper.ASCII);
									ftp.upload(input,fileName); 
									ok=true;
								}
								else
								{	
									msgError = new ELEERRMessage();
									msgError.setERRNUM("1");
									msgError.setERNU01("01");		                
									msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
								}	
							} 
							catch (Exception e) 
							{
								msgError = new ELEERRMessage();
								msgError.setERRNUM("1");
								msgError.setERNU01("01");		                
								msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
							}
						}	
					}	
					else
					{
						msgError = new ELEERRMessage();
						msgError.setERRNUM("1");
						msgError.setERNU01("01");		                
						msgError.setERDS01("Extensi�n de archivo debe ser TXT.");	
					}	
				}
				else
				{
					//mandamos error en session 
					msgError = new ELEERRMessage();
					msgError.setERRNUM(new BigDecimal(1));
					msgError.setERDS01("El Archivo no contiene datos.");							
				}

				if (ok)
				{					
					
					EDD800001Message msg = (EDD800001Message) mp.getMessageRecord("EDD800001");
					msg.setH01USERID(user.getH01USR());
					msg.setH01OPECOD("0002");
					msg.setH01TIMSYS(getTimeStamp());
					//Sets message with page fields
					

					msg.setE01IDCARGA(idCarga);
					msg.setE01CMHCRU(fileName);

					//Sending message
					mp.sendMessage(msg);

					//Receive error and data
					msgError = (ELEERRMessage) mp.receiveMessageRecord();
					msg = (EDD800001Message) mp.receiveMessageRecord();

					//Sets session with required data
					session.setAttribute("userPO", userPO);
					session.setAttribute("ExCon", msg);

					if (!mp.hasError(msgError)) {
						forward("EDD8000_exc_apro_procces.jsp", req, res);
					} else {
						//if there are errors go back to maintenance page and show errors
						session.setAttribute("error", msgError);
						forward("EDD8000_exc_apro_enter.jsp", req, res);
					}
				}
				else
				{
					session.setAttribute("error", msgError);				
					forward("EDD8000_exc_apro_enter.jsp", req, res);
				}
			}
			catch (Exception e) 
			{
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();	
				msgError = new ELEERRMessage();			
				msgError.setERRNUM("1");
	            msgError.setERNU01("01");
	            msgError.setERDS01(className);
	            msgError.setERNU02("02");
	            msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
	            msgError.setERNU03("03");
	            msgError.setERDS03("Para mas informacion revisar los archivos de log.");
				e.printStackTrace();			

				session.setAttribute("error", msgError);				
				forward("EDD8000_exc_apro_enter.jsp", req, res);
			} 	
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**Rutina para crear un nombre de archivo aleatorio**/
	String getCadenaAlfanumAleatoria (int longitud){
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while ( i < longitud){
		char c = (char)r.nextInt(255);
		if ( (c >= '0' && c <='9') || (c >='A' && c <='M') ){
		cadenaAleatoria += c;
		i ++;
		}
		}
		return cadenaAleatoria;
		}
	
	
	protected void procReqExcGesEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: EDD8000_exc_ges_enter.jsp");
			forward("EDD8000_exc_ges_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}


	protected void procActionExcGesList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			
			mp = getMessageProcessor("EDD8000", req);
			EDD800001Message msgList = (EDD800001Message) mp.getMessageRecord("EDD800001", user.getH01USR(), "0003");
			
			if(req.getParameter("posicion")!=null)
				msgList.setE01NUMREG (req.getParameter("posicion"));
			else
				msgList.setE01NUMREG("0");
			
			setMessageRecord(req, msgList);

			mp.sendMessage(msgList);
						
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { // if there are errors go back to first page
				session.setAttribute("error", error);
				
				flexLog("About to call Page: EDD8000_exc_ges_enter.jsp");
				forward("EDD8000_exc_ges_enter.jsp", req, res);
			} else {
			
				
    		JBObjList list = mp.receiveMessageRecordList("H01FLGMAS","E01NUMREG");
			

			req.setAttribute("E01NUMREG", msgList.getE01NUMREG());
		
		
 		    session.setAttribute("ExCon", list);
			forward("EDD8000_exc_ges_list.jsp", req, res);
		
			}
		} finally { 
			if (mp != null)
				mp.close();
		}
	}

	
	protected void procActionExcGesListDetail(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			mp = getMessageProcessor("EDD8000", req);
			
			EDD800002Message msgList = (EDD800002Message) mp.getMessageRecord("EDD800002", user.getH01USR(), "0001");
			
			if (req.getParameter("codigo_lista")!= null){
				
				JBObjList list = (JBObjList)session.getAttribute("ExCon");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				EDD800001Message listMessage = (EDD800001Message)list.get(index);
				
				msgList.setE02IDCARGA(listMessage.getE01IDCARGA());
				req.setAttribute("codigo_lista", index);
		
				session.setAttribute("ExcSeleccion", listMessage);
			}
			
			if(req.getParameter("posicion")!=null)
				msgList.setE02NUMREG(req.getParameter("posicion"));
			else
				msgList.setE02NUMREG("0");
			
			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { 
				session.setAttribute("error", error);
				flexLog("About to call Page: EDD8000_exc_ges_list.jsp");
				JBObjList ExCon = (JBObjList)session.getAttribute("ExCon");
				forward("EDD8000_exc_ges_list.jsp", req, res);
			}
			else
			{
				JBObjList list = mp.receiveMessageRecordList("H02FLGMAS","E02NUMREG");
        	    session.setAttribute("ExConDetalle", list);
     			forwardOnSuccess("EDD8000_exc_ges_list_detail.jsp", req, res);
			}  

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	
	
	protected void procActionExcProcess(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
	throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {			
			mp = getMessageProcessor("EDD8000", req);
			EDD800001Message msgList = (EDD800001Message) mp.getMessageRecord("EDD800001", user.getH01USR(), "0004");
			
			if (req.getParameter("codigo_lista")!= null){
				JBObjList list = (JBObjList)session.getAttribute("ExCon");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				EDD800001Message listMessage = (EDD800001Message)list.get(index);
				
				msgList.setE01IDCARGA(listMessage.getE01IDCARGA());
				
				req.setAttribute("codigo_lista", index);
		
				session.setAttribute("ExcSeleccion", listMessage);
			}
			

			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) 
			{ // if there are errors go back to first page
				session.setAttribute("error", error);
				JBObjList ExCon = (JBObjList)session.getAttribute("ExCon");
				forward("EDD8000_exc_ges_list.jsp", req, res);
			} 
			else 
			{
				procActionExcGesList(user, req, res, session);
			} 
		} finally {
			if (mp != null)
				mp.close();
		}
	}	

	protected void procActionExcDelete(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
	throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try { 
			mp = getMessageProcessor("EDD8000", req);
			EDD800001Message msgList = (EDD800001Message) mp.getMessageRecord("EDD800001", user.getH01USR(), "0005");
			
			if (req.getParameter("codigo_lista")!= null){
				JBObjList list = (JBObjList)session.getAttribute("ExCon");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				EDD800001Message listMessage = (EDD800001Message)list.get(index);
				
				msgList.setE01IDCARGA(listMessage.getE01IDCARGA());
				
				req.setAttribute("codigo_lista", index);
		
				session.setAttribute("ExcSeleccion", listMessage);
			}
			
			
			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) 
			{ // if there are errors go back to first page
				session.setAttribute("error", error);
				JBObjList ExCon = (JBObjList)session.getAttribute("ExCon");				
				forward("EDD8000_exc_ges_list.jsp", req, res);
			} 
			else 
			{
				
				procActionExcGesList(user, req, res, session);
			} 
		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	//END
}
	



