<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.EPV122001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	Consulta Holguras por Sucursal Plataforma de Venta
<%} else if (maintenance){ %>
	Mantenci�n Holguras por Sucursal Plataforma de Venta
<%} else { %>
	Nuevo  Holguras por Sucursal Plataforma de Venta
<%} %>

 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PVOBR_maintenance.jsp, EPV1220"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1220" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  
 <% int row = 0;%>
 
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Sucursal :</div>
            </td>
            <td width="70%" > 
	             <eibsinput:text property="E01PVGBRN" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH%>" readonly="true"/>
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Tipo Producto :</div>
            </td>
            <td width="70%" > 
 	             <eibsinput:text property="E01PVGTPR" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE%>" readonly="true"/>
 	             <eibsinput:text property="E01TPRDES" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" readonly="true"/>           
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Tipo Limite :</div>
            </td>
            <td width="70%" >
          	<select name="E01PVGTYL" <%=readOnly?"disabled":""%>>  
			 <option value=" " <% if (!(cnvObj.getE01PVGTYL().equals("M")||cnvObj.getE01PVGTYL().equals("%"))) out.print("selected"); %>> </option>           	        
             <option value="M" <% if (cnvObj.getE01PVGTYL().equals("M"))out.print("selected"); %>>Monto</option>
             <option value="%" <% if (cnvObj.getE01PVGTYL().equals("%"))out.print("selected"); %>>Porcentaje</option>                                                      
          	</select>            	     
       	 </td> 
          </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Monto Limite Holgura :</div>
            </td>
            <td width="70%" > 
	             <eibsinput:text property="E01PVGAML" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="<%=readOnly %>"/>
	        </td>
          </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">% Limite :</div>
            </td>
            <td width="70%" > 
	             <eibsinput:text property="E01PVGPOL" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>" readonly="<%=readOnly %>"/>                           
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Holgura Aprobada :</div>
            </td>
            <td width="70%" > 
	             <eibsinput:text property="E01PVGAMA" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="<%=readOnly %>"/>                           
	        </td>
          </tr>        
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Holgura Utilizada :</div>
            </td>
            <td width="70%" > 
	             <eibsinput:text property="E01PVGAMU" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="false"/>                           
	        </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Frecuencia Holgura :</div>
            </td>
            <td width="70%" > 
               	<select name="E01PVGTPG" <%=readOnly?"disabled":""%>>  
			    <option value=" " <% if (!(cnvObj.getE01PVGTPG().equals("D")||cnvObj.getE01PVGTPG().equals("M") ||cnvObj.getE01PVGTPG().equals("Y"))) out.print("selected"); %>> </option>           	        
                <option value="D" <% if (cnvObj.getE01PVGTPG().equals("D"))out.print("selected"); %>>Dias</option>  
                <option value="M" <% if (cnvObj.getE01PVGTPG().equals("M"))out.print("selected"); %>>Meses</option> 
                <option value="Y" <% if (cnvObj.getE01PVGTPG().equals("Y"))out.print("selected"); %>>A�os</option>                                                  	                                                  
    			</select>                          
	        </td>
          </tr>    
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Term Holgura :</div>
            </td>
            <td width="70%" > 
	             <eibsinput:text property="E01PVGFPG" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="4" maxlength="3" readonly="<%=readOnly %>"/>                           
	        </td>
          </tr>      
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Reset Holgura :</div>
            </td>
            <td width="70%" > 
          <eibsinput:date name="cnvObj" fn_year="E01PVGREY" fn_month="E01PVGREM" fn_day="E01PVGRED" readonly="true"/>                          
	        </td>
          </tr>                            
        </table>
      </td>
    </tr>
  </table>

<%if  (!readOnly) { %>
    <div align="center"> 
        <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    </div>
<% } %>  

  </form>
</body>
</HTML>
