<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Informacion Basica</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008002Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<SCRIPT Language="Javascript">
   builtNewMenu(client_inq_corp_opt);
</SCRIPT>

</head>

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
 out.println("<SCRIPT> initMenu(); </SCRIPT>");
 
%> 

 <h3 align="center">Informaci&oacute;n Cliente Empresa  <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="client_inq_corp_basic, ESD0080" ></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="12">
  
<%
 if ( !userPO.getPurpose().equals("NEW") ) {
%>
  <table class="tableinfo">
    <tr > 
        <td nowrap >       
        <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center">
          <tr id="trdark"> 
              <td nowrap>           
              <div align="right"><h4>Estado : </h4></div>
              </td>
             <td nowrap width="30%"><h4> 
			 	<%if (client.getE02STS().equals("2")) out.print("   ACTIVO");
			 	else if (client.getE02STS().equals("4")) out.print("   DISUELTA"); %> 
            </h4></td>
            <td nowrap width="30%"><h4> 
			 <%if (client.getE02FL5().equals("3")) out.print("   LISTA MAYOR RIESGO");
			 else if (client.getE02FL5().equals("6")) out.print("   PEP");
			 else out.print("     "); %> 
            </h4></td>
              <%
              		if (client.getE02CCL().equals("0") ) {
              %> 
              <td nowrap>           
              <div align="right"><h4>Socio desde :    </h4></div>
              </td>
            	<td nowrap width="30%" ><h4> <%= client.getE02A4D().trim()%>  / <%= client.getE02A4M().trim()%> / <%= client.getE02A4Y().trim()%></h4>
              </td> 
                 <%
                 }
                 %>
            </tr>
          </table>
        </td>
      </tr>
    </table>
 <%
}
%>
  
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
  	<tr>
  		<td align="left" valign="top" width="30%"><h4>Raz&oacute;n Social</h4></td>  		
  		<td align="right" valign="top" width="85%" style="color:red;font-size:12;"><b><%=client.getE02PENDAP()%></b></td>
  		<td width="5%"></td>
  	</tr>
  </table>  
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td nowrap > 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap  width="23%"> 
                <div align="right">No Cliente :</div>
              </td>
              <td nowrap  colspan=3> 
                <input type="text" readonly name="E02CUN" size="10" maxlength="9" value="<%= client.getE02CUN()%>">
			  </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="44%"> 
                <div align="right">Raz&oacute;n Social :</div>
              </td>
              <td nowrap   width="56%">  
                <input type="text" readonly name="E02NA1" size="65" maxlength="60" value="<%= client.getE02NA1().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap  width="44%"> 
                <p align="right">Nombre de Fantasía :</p>
              </td>
              <td nowrap  width="56%">  
                <input type="text" readonly name="E02CP1" size="50" maxlength="45" value="<%= client.getE02CP1().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="44%"> 
                <div align="right">Nombre Corto :</div>
              </td>
              <td nowrap  width="56%">  
                <input type="text" readonly name="E02SHN" size="28" maxlength="25" value="<%= client.getE02SHN().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap  width="44%"> 
                <div align="right">Identificaci&oacute;n de Central de Riesgo :</div>
              </td>
              <td nowrap  width="56%">  
                <input type="text" readonly name="E02FN2" size="45" maxlength="45" value="<%= client.getE02FN2().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="44%"> 
                <div align="right"></div>
              </td>
              <td nowrap  width="56%">  
                <input type="text" readonly name="E02LN1" size="45" maxlength="45" value="<%= client.getE02LN1().trim()%>">
                </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <% System.out.println("Bloque Dirección H02SCR="+client.getH02SCR()); %>  
		<%	if( client.getH02SCR().equals("07")){%> 	
			<jsp:include page="ESD0080_address_template_basic_banesco_panama.jsp" flush="true">
				<jsp:param name="suffix" value="E02" />
				<jsp:param name="title" value="Dirección" />
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
				<jsp:param name="basic" value="true" />
			</jsp:include>

		<% } else if( client.getH02SCR().equals("03")){%> 	 
			<jsp:include page="ESD0080_address_template_venezuela.jsp" flush="true">				
				<jsp:param name="title" value="Dirección" />
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
			</jsp:include>
		
		<%} else if( client.getH02SCR().equals("18")){%> 	
			<jsp:include page="ESD0080_address_template_basic_chile.jsp" flush="true">
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
				<jsp:param name="title" value="Dirección Principal" />
				<jsp:param name="suffix" value="02" />
			</jsp:include>
	
		<% } else {%> 	
			<jsp:include page="ESD0080_address_template_basic_generic.jsp" flush="true">
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
				<jsp:param name="title" value="Dirección" />
				<jsp:param name="suffix" value="02" />
			</jsp:include>
	
		<%} %>   
    <% System.out.println("Bloque Identificación H02SCR="+client.getH02SCR()); %>  
    <%if( client.getH02SCR().equals("07")){%> 	
		<jsp:include page="ESD0080_ident_template_Banesco.jsp" flush="true">
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="true" />
			<jsp:param name="title" value="Identificación" />
			<jsp:param name="suffix" value="02" />
		</jsp:include>
	
	<%} else {%> 
		<jsp:include page="ESD0080_ident_template_generic.jsp" flush="true">
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="true" />
			<jsp:param name="title" value="Identificación" />
			<jsp:param name="suffix" value="02" />

		</jsp:include>
	<%} %>   

    <h4>Fechas</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Fecha de Fundaci&oacute;n :</div>
              </td>
              <td nowrap width="58%">  
	               <eibsinput:date name="client" fn_year="E02BDY" fn_month="E02BDM" fn_day="E02BDD" readonly="true"/>
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Fecha de Primer Contacto :</div>
              </td>
              <td nowrap width="58%">  
	               <eibsinput:date name="client" fn_year="E02IDY" fn_month="E02IDM" fn_day="E02IDD" readonly="true"/>
              </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Fecha Inicio de Operaciones :</div>
              </td>
              <td nowrap width="58%">  
	               <eibsinput:date name="client" fn_year="E02IEY" fn_month="E02IEM" fn_day="E02IED" readonly="true"/>
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Fecha de Registro :</div>
              </td>
              <td nowrap width="58%">  
	               <eibsinput:date name="client" fn_year="E02REY" fn_month="E02REM" fn_day="E02RED" readonly="true"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <h4>Tel&eacute;fonos</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Tel&eacute;fono Oficina 1 :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly name="E02HPN" size="16" maxlength="15" value="<%= client.getE02HPN().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="42%"> 
                <div align="right">Tel&eacute;fono Oficina 2 :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly name="E02PHN" size="16" maxlength="15" value="<%= client.getE02PHN().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Tel&eacute;fono de Fax :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly name="E02FAX" size="16" maxlength="15" value="<%= client.getE02FAX().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Tel&eacute;fono Celular :</div>
              </td>
              <td nowrap width="58%">  
                <input type="text" readonly name="E02PH1" size="16" maxlength="15" value="<%= client.getE02PH1().trim()%>">
                </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <h4>C&oacute;digos de Clasificaci&oacute;n</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left" >
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Ejecutivo Principal :</div>
              </td>
              <td nowrap width="58%" >
                <input type="text" readonly name="E02OFC" size="5" maxlength="4" value="<%= client.getE02OFC().trim()%>">  
                <input type="text" readonly name="D02OFC" size="45" maxlength="45" value="<%= client.getD02OFC().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Ejecutivo Secundario :</div>
              </td>
              <td nowrap width="58%" >
                <input type="text" readonly name="E02OF2" size="5" maxlength="4" value="<%= client.getE02OF2().trim()%>"> 
                <input type="text" readonly name="D02OF2" size="45" maxlength="45" value="<%= client.getD02OF2().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Sector Econ&oacute;mico :</div>
              </td>
              <td nowrap width="58%" >
                <input type="text" readonly name="E02INC" size="5" maxlength="4" value="<%= client.getE02INC().trim()%>">  
                <input type="text" readonly name="D02INC" size="45" maxlength="45" value="<%= client.getD02INC().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Actividad Econ&oacute;mica :</div>
              </td>
              <td nowrap width="58%" >
                <input type="text" readonly name="E02BUC" size="5" maxlength="4" value="<%= client.getE02BUC().trim()%>"> 
                <input type="text" readonly name="D02BUC" size="45" maxlength="45" value="<%= client.getD02BUC().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Pa&iacute;s de Residencia :</div>
              </td>
              <td nowrap width="58%" >
                <input type="text" readonly name="E02GEC" size="5" maxlength="4" value="<%= client.getE02GEC().trim()%>">  
                <input type="text" readonly name="D02GEC" size="45" maxlength="45" value="<%= client.getD02GEC().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Tipo de Relaci&oacute;n :</div>
              </td>
              <td nowrap  width="58%" >
                <input type="text" readonly name="E02UC1" size="5" maxlength="4" value="<%= client.getE02UC1().trim()%>"> 
                <input type="text" readonly name="D02UC1" size="45" maxlength="45" value="<%= client.getD02UC1().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Clasificaci&oacute;n :</div>
              </td>
              <td nowrap width="58%" >
                <input type="text" readonly name="E02UC2" size="5" maxlength="4" value="<%= client.getE02UC2().trim()%>">  
                <input type="text" readonly name="D02UC2" size="45" maxlength="45" value="<%= client.getD02UC2().trim()%>">
                </td>
            </tr>

            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Sub Clasificaci&oacute;n :</div>
              </td>
              <td nowrap  width="58%" >
                <input type="text" readonly name="E02SCL" size="5" maxlength="4" value="<%= client.getE02SCL().trim()%>"> 
                <input type="text" readonly name="D02SCL" size="45" maxlength="45" value="<%= client.getD02SCL().trim()%>">
                </td>
            </tr>

            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Nivel SocioEcon&oacute;mico :</div>
              </td>
              <td nowrap width="58%" >
                <input type="text" readonly name="E02UC3" size="5" maxlength="4" value="<%= client.getE02UC3().trim()%>"> 
                <input type="text" readonly name="D02UC3" size="45" maxlength="45" value="<%= client.getD02UC3().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Unidad de Negocio :</div>
              </td>
              <td nowrap width="58%" >
                <input type="text" readonly name="E02UC4" size="5" maxlength="4" value="<%= client.getE02UC4().trim()%>"> 
                <input type="text" readonly name="D02UC4" size="45" maxlength="45" value="<%= client.getD02UC4().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Segmento :</div>
              </td>
              <td nowrap width="58%" >
                <input type="text" readonly name="E02UC5" size="5" maxlength="4" value="<%= client.getE02UC5().trim()%>">  
                <input type="text" readonly name="D02UC5" size="45" maxlength="45" value="<%= client.getD02UC5().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">SubSegmento :</div>
              </td>
              <td nowrap width="58%" >
                <input type="text" readonly name="E02UC6" size="5" maxlength="4" value="<%= client.getE02UC6().trim()%>">  
                <input type="text" readonly name="D02UC6" size="45" maxlength="45" value="<%= client.getD02UC6().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap width="42%"> 
                <div align="right">Indicador Banco/Cooperativa :</div>
              </td>
              <td nowrap width="58%" >  
                <input type="text" readonly name="E02CRF_" size="5" maxlength="4" value="<%= client.getE02CRF().trim()%>">              
                <input type="text" readonly name="E02CRF" size="25" maxlength="25"
                  value="<% if (client.getE02CRF().equals("1")) { out.print("Banco Corresponsal"); }
                  			else if (client.getE02CRF().equals("2")) { out.print("Banco Regular"); }
                  			else if (client.getE02CRF().equals("3")) { out.print("Cooperativa"); }
							else if (client.getE02CRF().equals("0")) { out.print("Ninguno"); } 
							else { out.print(""); } %>" >
              </td>
            </tr>

            <tr id="trclear"> 
              <td nowrap width="42%"> 
                <div align="right">Con fin de lucro :</div>
              </td>
              <td nowrap width="58%" >
                 <input type="text" readonly name="E02FLC" size="2" maxlength="2" value="<%if (client.getE02FLC().equals("Y")) out.print("SI"); else out.print("NO"); %>">
              </td>
            </tr>
            
            
          </table>
        </td>
      </tr>
    </table>
    <h4>Datos Adicionales</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td nowrap> 
          <TABLE cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <TBODY><TR id="trdark"> 
              <TD nowrap width="22%"> 
                <DIV align="right">No. de Registro :</DIV>
              </TD>
              <TD nowrap width="28%"> 
                <INPUT type="text" readonly name="E02REN" size="16" maxlength="15" value="<%= client.getE02REN().trim()%>">
              </TD>
              <TD nowrap width="19%"> 
                <DIV align="right">A&ntilde;os Establecidos :</DIV>
              </TD>
              <TD nowrap width="31%"> 
                <INPUT type="text" readonly name="E02NSO" size="3" maxlength="2" value="<%= client.getE02NSO().trim()%>">
              </TD>
            </TR>
            <TR id="trclear"> 
              <TD nowrap width="22%"> 
                <DIV align="right">No. de Acciones :</DIV>
              </TD>
              <TD nowrap width="28%"> 
                <INPUT type="text" readonly name="E02NST" size="8" maxlength="7" value="<%= client.getE02NST().trim()%>">
              </TD>
              <TD nowrapwidth="25%" width="19%"> 
                <DIV align="right">No. de Accionistas :</DIV>
              </TD>
              <TD nowrap width="31%"> 
                <INPUT type="text" readonly name="E02NSH" size="8" maxlength="7" value="<%= client.getE02NSH().trim()%>">
              </TD>
            </TR>
			<TR id="trdark"> 
              <TD nowrapwidth="22%" width="22%"> 
                <DIV align="right">Capital Inicial :</DIV>
              </TD>
              <TD nowrap width="28%"> 
                <INPUT type="text" readonly name="E02CAI" size="16" maxlength="15" value="<%= client.getE02CAI().trim()%>">
              </TD>
              <TD nowrap width="19%"> 
                <DIV align="right">Capital Suscrito :</DIV>
              </TD>
              <TD nowrap width="31%"> 
                <INPUT type="text" readonly name="E02CAS" size="16" maxlength="15" value="<%= client.getE02CAS().trim()%>">
              </TD>
            </TR>
            <TR id="trclear"> 
              <TD nowrap width="22%"> 
                <DIV align="right">Capital Pagado :</DIV>
              </TD>
              <TD nowrap width="28%"> 
                <INPUT type="text" readonly name="E02CAP" size="16" maxlength="15" value="<%= client.getE02CAP().trim()%>">
              </TD>
              <TD nowrap width="19%"> 
                <DIV align="right">Nivel de Ventas :</DIV>
              </TD>
              <TD nowrap width="31%"> 
                <INPUT type="text" readonly name="E02INL" size="2" maxlength="1" value="<%= client.getE02INL().trim()%>">
              </TD>
            </TR>
            
			<TR id="trdark"> 
              <TD nowrapwidth="22%" width="22%"> 
                <DIV align="right">Total Activos :</DIV>
              </TD>
              <TD nowrap width="28%"> 
                <INPUT type="text" readonly name="E02TACT" size="16" maxlength="15" value="<%= client.getE02TACT().trim()%>">
              </TD>
              <TD nowrap width="19%"> 
                <DIV align="right">Total Pasivos :</DIV>
              </TD>
              <TD nowrap width="31%"> 
                <INPUT type="text" readonly name="E02TPAS" size="16" maxlength="15" value="<%= client.getE02TPAS().trim()%>">
              </TD>
            </TR>
            <TR id="trclear"> 
              <TD nowrap width="22%"> 
                <DIV align="right">Patrimonio :</DIV>
              </TD>
              <TD nowrap width="28%"> 
                <INPUT type="text" readonly name="E02TPAT" size="16" maxlength="15" value="<%= client.getE02TPAT().trim()%>">
              </TD>
              <TD nowrap width="19%"> 
                <DIV align="right">Resultado del Ejercicio :</DIV>
              </TD>
              <TD nowrap width="31%"> 
                <INPUT type="text" readonly name="E02RESU" size="16" maxlength="15" value="<%= client.getE02RESU().trim()%>">
              </TD>
            </TR>			<TR id="trdark"> 
              <TD nowrapwidth="22%" width="22%"> 
                <DIV align="right">Monto de Ingreso Renta :</DIV>
              </TD>
              <TD nowrap width="28%"> 
                <INPUT type="text" readonly name="E02MING" size="16" maxlength="15" value="<%= client.getE02MING().trim()%>">
              </TD>
              <TD nowrap width="19%"> 
                <DIV align="right"></DIV>
              </TD>
              <TD nowrap width="31%"> 
              </TD>
            </TR>

            
            <TR id="trdark"> 
              <TD nowrap n="" width="22%"> 
                <DIV align="right">Tama&ntilde;o del Negocio :</DIV>
              </TD>
              <TD nowrap width="28%"> 
                <INPUT type="text" readonly name="E02SEX" size="2" maxlength="1" value="<%= client.getE02SEX().trim()%>">
              </TD>
              <TD nowrap width="19%"> 
                <DIV align="right">Area de Negocio :</DIV>
              </TD>
              <TD nowrap width="31%"> 
                <INPUT type="text" readonly name="E02FL3" size="2" maxlength="1" value="<%= client.getE02FL3().trim()%>">
              </TD>
            </TR>
            <TR id="trclear"> 
              <TD nowrap width="22%"> 
                <DIV align="right">Fuentes de Ingresos : </DIV>
              </TD>
              <TD nowrap width="28%"> 
                <DIV align="left"> 
                  <INPUT type="text" readonly name="D02SOI" size="45" maxlength="45" value="<%= client.getD02SOI().trim()%>">
                </DIV>
              </TD>
              <TD nowrap width="19%">
                <DIV align="right">Tabla de Previsi&oacute;n :</DIV>
              </TD>
              <TD nowrap width="31%"> 
                <INPUT type="text" readonly name="E02PRV" size="4" maxlength="2" value="<%= client.getE02PRV().trim()%>">
              </TD>
            </TR>
            <TR id="trdark"> 
              <TD nowrap n="" width="22%"> 
                <DIV align="right">Nivel de Riesgo :</DIV>
              </TD>
              <TD nowrap width="28%"> 
                <INPUT type="text" readonly name="E02RSL" size="5" maxlength="4" value="<%= client.getE02RSL().trim()%>">
              </TD>
              <TD nowrap width="19%"> 
	              <div align="right">Bloqueo Correspondencia :</div>
              </TD>
              <TD nowrap width="31%"> 
                <INPUT type="text" readonly name="E02C18" size="5" maxlength="4" value="<%= client.getE02C18().trim()%>">
              </TD>
            </TR>
          </TBODY></TABLE>
        </td>
      </tr>
    </table>
    <h4>Datos Operativos</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td  nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap width="28%"> 
                <div align="right">Estado del Cliente :</div>
              </td>
              <td nowrap  width="28%" bordercolor="#FFFFFF">  
                <input type="text" readonly name="E02STS" size="13" maxlength="12" 
				value="<% if (client.getE02STS().equals("1")) { out.print("Inactivo"); }
                    	  else if (client.getE02STS().equals("2")) { out.print("Activo"); }
                          else if (client.getE02STS().equals("3")) { out.print("Lista Mayor Riesgo"); }
                          else if (client.getE02STS().equals("4")) { out.print("Disuelta"); }
                          else { out.print(""); } %>">
                </td>
              <td nowrap  width="22%"> 
                <div align="right">Clase de Cliente :</div>
              </td>
              <td nowrap  width="22%" >  
            <input type="text" readonly name="E02CCL size="13" maxlength="12"
			  value="<% if (client.getE02CCL().equals("0")) { out.print("Socio"); }
					  	else if (client.getE02CCL().equals("1")) { out.print("Emp/Gobierno");  }	
						else if (client.getE02CCL().equals("2")) { out.print("Emp/Privada"); }
						else if (client.getE02CCL().equals(" ")) { out.print("No Aplica"); }
						else { out.print(""); } %> " >
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="28%"> 
                <div align="right">Tipo de Cliente :</div>
              </td>
              <td nowrap width="28%" bordercolor="#FFFFFF">  
              <input type="text" readonly name="E02TYP" size="10" maxlength="10" 
    		  value="<% if (client.getE02TYP().equals("R")) { out.print("Regular"); }
						else if (client.getE02TYP().equals("M")) { out.print("Master"); }
		                else if (client.getE02TYP().equals("G")) { out.print("Grupo"); }
						else { out.print(""); } %>">
              </td>
			  	
              <td nowrap  width="22%"> 
                <div align="right">Numero Grupo :</div>
              </td>
              <td nowrap  width="22%">  
                <input type="text" readonly name="E02GRP" size="10" maxlength="9" value="<%= client.getE02GRP().trim()%>">
                </td>
            </tr>
            <tr id="trdark"> 
              <td nowrap  width="28%"> 
                <div align="right">Idioma :</div>
              </td>
              <td nowrap width="28%" bordercolor="#FFFFFF">  
                <input type="text" readonly name="E02LIF" size="10" maxlength="10" 
			  value="<% if (client.getE02LIF().equals("E")) { out.print("Ingles"); } 
                		else if (client.getE02LIF().equals("S")) { out.print("Español"); }
						else { out.print(""); } %>" >
              </td>
              <td nowrap  width="22%"> 
                <div align="right">Calificaci&oacute;n :</div>
              </td>
              <td nowrap width="22%">  
                <input type="text" readonly name="E02FL2" size="2" maxlength="1" value="<%= client.getE02FL2().trim()%>">
                </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap width="28%"> 
                <div align="right">Impuestos/Retenciones :</div>
              </td>
              <td nowrap  width="22%">  
                <input type="text" readonly name="E02TAX" size="2" maxlength="1" value="<%= client.getE02TAX().trim()%>">
              </td>
              <td nowrap width="18%"> 
	              <div align="right">Sucursal Administrativa :</div>
	          </td>
	          <td nowrap width="36%"> 
	            <eibsinput:text name="client" property="E02BRA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH%>" readonly="true" />
	          </td>

            </tr>

            <tr id="trclear"> 
              <td nowrap width="28%"> 
                <div align="right">Clasificaci&oacute;n Individual :</div>
              </td>
              <td nowrap  width="22%">  
                <input type="text" readonly name="E02CIN" size="2" maxlength="1" value="<%= client.getE02CIN().trim()%>">
              </td>
              <td nowrap width="18%"> 
	          </td>
	          <td nowrap width="36%"> 
	          </td>
            </tr>



          </table>
        </td>
      </tr>
    </table>
    <h4>Miscelaneos</h4>
  </div>
  <div align="center">
    <table class="tableinfo">
      <tr > 
        <td  nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
            <tr id="trdark"> 
              <td nowrap  width="18%"> 
                <div align="right">Referido por :</div>
              </td>
              <td nowrap  width="39%">  
                <input type="text" readonly name="E02RBY" size="5" maxlength="4" value="<%= client.getE02RBY().trim()%>">
                <input type="text" readonly name="E02RBN" size="40" maxlength="45" value="<%= client.getE02RBN().trim()%>">
                </td>
              <td nowrap  width="23%"> 
                <div align="right">Residente del Pa&iacute;s :</div>
              </td>
              <td nowrap   width="20%">  
              <input type="text" readonly name="E02FL1" size="5" maxlength="5" 
			  value="<%if (client.getE02FL1().equals("1")) { out.print("Si"); } 
						else {out.print("No"); } %>" >
              </td>
            </tr>
            <tr id="trclear"> 
              <td nowrap  width="18%" > 
                <div align="right">Nivel de Consulta :</div>
              </td>
              <td nowrap width="39%">  
                <input type="text" readonly name="E02ILV" size="1" maxlength="1" value="<%= client.getE02ILV().trim()%>">
                </td>
              <td nowrap width="23%" > 
                <div align="right">No. de Tarjetas ATM :</div>
              </td>
              <td nowrap width="20%" >  
                <input type="text" readonly name="E02ATM" size="2" maxlength="1" value="<%= client.getE02ATM().trim()%>">
                </td>
            </tr>
           <tr id="trdark"> 
            <td nowrap  width="20%"> 
              <div align="right">Poder Especial :</div>
              </td>             
            <td nowrap  width="20%"> 
              <input type="radio" disabled name="E02L01" value="Y" <%if (client.getE02L01().equals("Y")) out.print("checked"); %>>
                S&iacute; 
                <input type="radio" disabled name="E02L01" value="N" <%if (!client.getE02L01().equals("Y")) out.print("checked"); %>>
                No 
                </td>            
            <td nowrap  width="18%"> 
              <div align="right">Fecha Poder Especial :</div>
              </td>     
              <td nowrap> 
	               <eibsinput:date name="client" fn_year="E02A1Y" fn_month="E02A1M" fn_day="E02A1D" readonly="true"/>
              </td>
            </tr> 
            <tr id="trclear"> 
              
            <td nowrap width="18%"> 
              <div align="right">Fuente :</div>
              </td>
              
            <td nowrap  width="39%"> 
               <%
              	boolean bTesoreria = (client.getE02FL8().indexOf("T") > -1);
              	boolean bFideicomiso = (client.getE02FL8().indexOf("F") > -1);
              	boolean bFEM = (client.getE02FL8().indexOf("E") > -1);
              	boolean bTerceros = (client.getE02FL8().indexOf("R") > -1);
              %>
              <INPUT type="checkbox" name="E02FL8_TES" value="1" <% if (bTesoreria == true) out.print("checked"); %> readonly>Tesorer&iacute;a
              <INPUT type="checkbox" name="E02FL8_FID" value="1" <% if (bFideicomiso == true) out.print("checked"); %> readonly>Fideicomiso
              <INPUT type="checkbox" name="E02FL8_FEM" value="1" <% if (bFEM == true) out.print("checked"); %> readonly>FEM
              <INPUT type="checkbox" name="E02FL8_TER" value="1" <% if (bTerceros == true) out.print("checked"); %> readonly>Terceros
             </td>
              
            <td nowrap width="20%"> 
              
              </td>
              
            <td nowrap  width="18%"> 
              
              </td>
            </tr>            
          </table>
        </td>
      </tr>
    </table> 
    </div>
    
 	 <div align="center">
	 	<h4>Escritura P&uacute;blica</h4>
		<table class="tableinfo">
      	<tr > 
        	<td nowrap > 
          		<table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
		            <tr id="trdark"> 
        		      <td nowrap  width="18%" align="right"> 
        		      		<div align="right">Nombre de Notario :</div>
		              </td>
		              <td nowrap  width="39%"> 
                 		 <input type="text" name="E02NOT" size="61" maxlength="60" value="<%= client.getE02NOT().trim()%>" readonly>
             		  </td>
              		</tr>
		            <tr id="trclear"> 
        		      <td nowrap  width="18%" align="right"> 
        		      		<div align="right">Nombre Empresa por 1 d&iacute;a :</div>
		              </td>
					  <td nowrap  width="39%"> 
                   		<input type="text" name="E02NOME" size="61" maxlength="60" value="<%= client.getE02NOME().trim()%>" readonly>
		              </td>
              		</tr>
		            <tr id="trdark"> 
        			  <td nowrap width="18%" > 
               			 <div align="right">Ciudad Notar&iacute;a :</div>
              		  </td>
              		  <td nowrap width="39%" >  
               				 <input type="text" name="E02NCI" size="5" maxlength="4" value="<%= client.getE02NCI().trim()%>" readonly> 
                			 <input type="text" name="E02NCD" size="45" maxlength="45" value="<%= client.getE02NCD().trim()%>" readonly>
	                  </td>
      			    </tr>
            
           			<tr id="teclear"> 
            		  <td nowrap  width="20%"> 
             			 <div align="right">Fecha Escritura / Solicitud :</div>
		              </td>             
        		      <td nowrap  width="20%">
                		<eibsinput:date name="client" fn_year="E02NEY" fn_month="E02NEM" fn_day="E02NED" required="false" readonly="true"/>
            		  </td>            
	               </tr> 
            
          </table>
        </td>
      </tr>
    </table>
 
 
    </div>
 
    
  </form>
</body>
</html>
