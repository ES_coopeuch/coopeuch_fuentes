<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@page import="com.datapro.constants.EibsFields"%>
<%@ page import = "datapro.eibs.master.Util" %>

<html>
<head> 
<title>Liquidacion de Prestamos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="inqLoans" class="datapro.eibs.beans.EDL018001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<jsp:useBean id= "EDL0180Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="cerBasic" class="datapro.eibs.beans.EDL018002Message" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

<%
if ( userPO.getHeader23().equals("G") ||  userPO.getHeader23().equals("V")){
%>
	builtNewMenu(ln_i_1_opt);
<%   
}
else  {
%>
	builtNewMenu(ln_i_2_opt);
<%   
}
%>

</SCRIPT> 


</head>

<body nowrap>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }
    out.println("<SCRIPT> initMenu(); </SCRIPT>");
 
%>  
<div align="center"></div>
<h3 align="center">Liquidacion de Pr�stamo<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ln_liquidacion,EDL0160"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXEDL0160" >
  <input type=HIDDEN name="SCREEN" value="1">
  
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="14%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="9%" > 
              <div align="left"> 
                <input type="text" name="E01DEACUN" size="15" maxlength="15" value="<%= inqLoans.getE01DEACUN().trim()%>" readonly>
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01CUSNA1" size="45" maxlength="45" value="<%= inqLoans.getE01CUSNA1().trim()%>" readonly>
              </div>
            </td>
            <td nowrap > 
              <div align="right"><b>Fecha :</b></div>
            </td>
            <td nowrap >
             <div align="left"> 
              <input type="text" name="E01DEACUD" size="15" maxlength="15" value="<%= inqLoans.getE01DEACUD().trim()%> - <%= inqLoans.getE01DEACUM().trim()%> - <%= inqLoans.getE01DEACUY().trim()%>" readonly>
              </div></td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="14%"> 
              <div align="right"><b>Producto :</b></div>
            </td>
            <td nowrap width="9%"> 
              <div align="left"> 
                <input type="text" name="E01DEAPRO" size="15" maxlength="15" value="<%= inqLoans.getE01DEAPRO().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="33%"> 
              <div align="center"><b> 
                <input type="text" name="D01DEAPRO" size="45" maxlength="45" value="<%= inqLoans.getD01DEAPRO().trim()%>" readonly>
                </b> </div>
            </td>
            <td nowrap width="11%"> 
              <div align="right"><b>Usuario :</b></div>
            </td>
            <td nowrap width="21%"> 
              <div align="left"><b> 
                <input type="text" name="E01DEANAU" size="15" maxlength="15" value="<%= inqLoans.getE01DEANAU().trim()%>" readonly>  
                </b> </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="14%"> 
              <div align="right"><b>Operacion :</b></div>
            </td>
            <td nowrap width="9%"> 
              <div align="left"> 
                <input type="text" name="E01DEAACC" size="15" maxlength="15" value="<%= inqLoans.getE01DEAACC().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="33%"> 
              <div align="left"><b>  </b></div>
            </td>
            <td nowrap width="11%"> 
              <div align="right"><b>Rut :</b></div>
            </td>
            <td nowrap width="21%"> 
              <div align="left"><b> 
                <input type="text" name="E01CUSIDN" size="15" maxlength="15" value="<%= userPO.getHeader4().trim()%>" readonly>  
                </b> </div>
            </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>
  <h4>Otros Datos del Cliente</h4>
  <table class="tableinfo">
    <tr>
    <td nowrap>  
        <table cellspacing=0 cellpadding=2 width="100%" border="0" >
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Empleador :</div>
            </td>
            <td nowrap >
                <input align="left" type="text" name="E01CUSRUC" size="15" maxlength="15" value="<%= userPO.getHeader5().trim()%>" readonly>    
 		    </td> 
            <td nowrap > 
              <input align="middle" type="text" name="E01CUSCP1" size="45" maxlength="45" value="<%= inqLoans.getE01CUSCP1().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Fecha Socio :</div>
            </td>
            <td nowrap >               
              <input align="left" type="text" name="E01CUFA4D" size="15" maxlength="15" value="<%= inqLoans.getE01CUFA4D().trim()%> - <%= inqLoans.getE01CUFA4M().trim()%> - <%= inqLoans.getE01CUFA4Y().trim()%>" readonly>            
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Convenio :</div>
            </td>
            <td nowrap > 
              <input align="left" type="text" name="E01CUSCCO" size="15" maxlength="15" value="<%= inqLoans.getE01CUSCCO().trim()%>" readonly>
            </td>
            <td nowrap > 
              <input align="middle" type="text" name="D01CUSCCO" size="45" maxlength="45" value="<%= inqLoans.getD01CUSCCO().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right"> </div>
            </td>
            <td nowrap > 
              <div align="right"> </div>
            </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>  
  <h4>Detalle Venta</h4>
  <table class="tableinfo" width="735">
    <tr > 
      <td nowrap>  
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Oficina Venta :</div>
            </td>
            <td nowrap >
                <input align="left" type="text" name="E01DEABRN" size="15" maxlength="15" value="<%= inqLoans.getE01DEABRN().trim()%>" readonly>    
 		    </td> 
 		    <td nowrap > 
              <div align="right">Ejecutivo :</div>
            </td>
            <td nowrap > 
              <input align="left" type="text" name="E01DEAIDE" size="15" maxlength="15" value="<%= userPO.getHeader6().trim()%>" readonly>
            </td>
            <td nowrap >               
              <input align="middle" type="text" name="E01DEANAE" size="40" maxlength="40" value="<%= inqLoans.getE01DEANAE().trim()%>" readonly>            
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Canal de Venta :</div>
            </td>
            <td nowrap >
                <input align="left" type="text" name="E01DEASCH" size="15" maxlength="15" value="<%= inqLoans.getE01DEASCH().trim()%>" readonly>    
 		    </td> 
 		    <td nowrap > 
              <div align="right">Vendedor :</div>
            </td>
            <td nowrap > 
              <input align="left" type="text" name="E01DEAIDV" size="15" maxlength="15" value="<%= userPO.getHeader7().trim()%>" readonly>
            </td>
            <td nowrap >               
              <input align="middle" type="text" name="E01DEANAV" size="40" maxlength="40" value="<%= inqLoans.getE01DEANAV().trim()%>" readonly>            
            </td>
          </tr>  
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Oficina Curse :</div>
            </td>
            <td nowrap >
                <input align="left" type="text" name="E01DEABRC" size="15" maxlength="15" value="<%= inqLoans.getE01DEABRC().trim()%>" readonly>    
 		    </td> 
 		    <td nowrap > 
              <div align="right">Usuario Curse :</div>
            </td>
            <td nowrap > 
              <input align="left" type="text" name="E01DEAUSC" size="15" maxlength="15" value="<%= inqLoans.getE01DEAUSC().trim()%>" readonly>
            </td>
            <td nowrap >               
              <input align="middle" type="text" name="E01DEANAC" size="40" maxlength="40" value="<%= inqLoans.getE01DEANAC().trim()%>" readonly>            
            </td>
          </tr>  
        </table>
      </td>
    </tr> 
  </table>  
  <h4>Datos Operacion en Moneda Origen</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap>  
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Moneda :</div>
            </td>
            <td nowrap >
			 <eibsinput:text name="inqLoans" property="E01DEACCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" readonly="true" size="15" maxlength="15"/>                
 		    </td> 
 		    <td nowrap > 
              <div align="right">Fecha Origen :</div>
            </td>
            <td nowrap > 
              <input align="left" type="text" name="E01DEASDD" size="15" maxlength="15" value="<%= inqLoans.getE01DEASDD().trim()%> / <%= inqLoans.getE01DEASDM().trim()%> / <%= inqLoans.getE01DEASDY().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Ciclo Pago :</div>
            </td>
            <td nowrap >               
              <input align="left" type="text" name="E01DEAPPD" size="15" maxlength="15" value="<%= inqLoans.getE01DEAPPD().trim()%>" readonly>            
            </td>
          </tr>
          <tr id="trclear">  
            <td nowrap > 
              <div align="right">Monto MO :</div>
            </td>
            <td nowrap > 
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01GLROAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="15" maxlength="15"/>
 			 <% } else {%> 

			 <eibsinput:text name="inqLoans" property="E01DEAOAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" size="15" maxlength="15"/>                    
 		    </td> 
 			 <% } %>  		    
 		    <td nowrap > 
              <div align="right">Fecha Apertura :</div>
            </td>
            <td nowrap > 
              <input align="left" type="text" name="E01DEAODD" size="15" maxlength="15" value="<%= inqLoans.getE01DEAODD().trim()%> / <%= inqLoans.getE01DEAODM().trim()%> / <%= inqLoans.getE01DEAODY().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Tasa Interes :</div>
            </td>
            <td nowrap >               
			 <eibsinput:text name="inqLoans" property="E01DEARTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true" size="15" maxlength="15"/>%                          
            </td>
          </tr> 
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Monto en $ :</div>
            </td>
            <td nowrap >
			 <eibsinput:text name="inqLoans" property="E01DEAOAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" size="15" maxlength="15"/>                                    
 		    </td> 
 		    <td nowrap > 
              <div align="right">Fecha Termino :</div>
            </td>
            <td nowrap > 
              <input align="left" type="text" name="E01DEAMAD" size="15" maxlength="15" value="<%= inqLoans.getE01DEAMAD().trim()%> / <%= inqLoans.getE01DEAMAM().trim()%> / <%= inqLoans.getE01DEAMAY().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Dias Base :</div>
            </td>
            <td nowrap >               
 			 <eibsinput:text name="inqLoans" property="E01DEABAS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" readonly="true" size="15" maxlength="15"/>
            </td>
          </tr>  
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Nro Cuotas :</div>
            </td>
            <td nowrap >
 			 <eibsinput:text name="inqLoans" property="E01DLCNC1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" readonly="true" size="15" maxlength="15"/>
 		    </td> 
 		    <td nowrap > 
              <div align="right">1er Vencimiento :</div>
            </td>
            <td nowrap > 
              <input align="left" type="text" name="E01DEAPVD" size="15" maxlength="15" value="<%= inqLoans.getE01DEAPVD().trim()%> / <%= inqLoans.getE01DEAPVM().trim()%> / <%= inqLoans.getE01DEAPVY().trim()%>"" readonly>
            </td>
            <td nowrap > 
              <div align="right">Valor Cuota MO :</div>
            </td>
            <td nowrap >  
            <% 
 				if (inqLoans.getH01FLGWK3().equals("R")) {
 			%> 
 			 <eibsinput:text name="inqLoans" property="E01DLCUF1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEREST %>" readonly="true" size="15" maxlength="15"/>
 			 <% } else {%>                                     
			 <eibsinput:text name="inqLoans" property="E01DLCVA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" size="15" maxlength="15"/>                          
 			 <% }%>
            </td>
          </tr>  
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Frecuencia :</div>
            </td>
            <td nowrap >
                <input align="left" type="text" name="E01DLCTP1" size="15" maxlength="15" value="<%= inqLoans.getE01DLCTP1().trim()%>" readonly>    
 		    </td> 
 		    <td nowrap > 
              <div align="right">Recaudacion :</div>
            </td>
            <td nowrap > 
              <input align="left" type="text" name="E01DEAPVI" size="15" maxlength="15" value="<%= inqLoans.getE01DEAPVI().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Valor Cuota $ :</div>
            </td>
            <td nowrap >               
			 <eibsinput:text name="inqLoans" property="E01DLCVA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" size="15" maxlength="15"/>                                       
            </td>
          </tr>  
        </table>
      </td>
    </tr> 
  </table>

<h4>Liquidacion de Prestamo</h4>
  <TABLE  id=cfTable class="tableinfo">
 <TR > 
    <TD NOWRAP valign="top" width="100%">
        <table id="headTable" width="100%">
          <tr id="trdark"> 
          <th align=CENTER nowrap width="15%"> 
              <div align="center"> </div>
            </th>
          <th align=CENTER nowrap width="20%"> 
              <div align="center">Concepto</div>
            </th>
            <th align=CENTER nowrap width="15%"> 
              <div align="center">Monto ($)</div>
            </th>
            <th align=CENTER nowrap width="20%"> 
              <div align="center"> </div>
            </th>            
          </tr>
          <%
                EDL0180Help.initRow();
				int idx = 0;
				int x = 0;
				String valStyle = "";
        		while (EDL0180Help.getNextRow()) {
                datapro.eibs.beans.EDL018002Message msgList = (datapro.eibs.beans.EDL018002Message) EDL0180Help.getRecord();
               	if (idx++ % 2 != 0)
					valStyle = "trdark";
				else
					valStyle = "trclear";
		    %>
   		    	<TR id="<%= valStyle %>">
		    <% if  (x == 0) {
		        x = 1; %>
   				<TD NOWRAP  ALIGN=left width=\"15%\"><b> </b></td>	
				<TD NOWRAP  ALIGN=left width=\"20%\"><b><%= msgList.getE02DLSNAR()%></b></td>
		        <TD NOWRAP  ALIGN=right width=\"15%\"><b><%= Util.formatCCY(msgList.getE02DLSAMT())%></b></td>
   				<TD NOWRAP  ALIGN=left width=\"20%\"><b> </b></td>	
		    <%} else { %>  
		        <TD NOWRAP  ALIGN=left width=\"15%\"> </td>	
				<TD NOWRAP  ALIGN=left width=\"20%\"><%= msgList.getE02DLSNAR()%></td>
		        <TD NOWRAP  ALIGN=right width=\"15%\"><%= Util.formatCCY(msgList.getE02DLSAMT())%></td> 
		  		<TD NOWRAP  ALIGN=left width=\"20%\"> </td> 
		        <% } %>														
	 		  </TR>
		     <% } %>
        </table>
        </TD>
        </TR>
    </table> 
    
 <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0" bordercolor="#000000">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0">
	  <tr><td>&nbsp;</td></tr>
	    <tr><td>&nbsp;</td></tr>
		  <tr><td>&nbsp;</td></tr>  
		  <tr><td>&nbsp;</td></tr> 
		  <tr><td>&nbsp;</td></tr>   
          <tr id="trdark"> 
            <td nowrap > 
              <div align="center">_____________________________________________ </div>
            </td>
            <td nowrap > 
              <div align="right"></div>
            </td>
            <td nowrap > 
              <div align="right"> </div>
            </td>
            <td nowrap > 
              <div align="right"> </div>
            </td>
 		    <td nowrap > 
             <div align="center">_____________________________________________</div> 
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="center"><%= inqLoans.getE01DEANAE().trim()%></div>
            </td>
            <td nowrap > 
              <div align="right"> </div>
            </td>
            <td nowrap > 
              <div align="right"> </div>
            </td>
            <td nowrap > 
              <div align="right"> </div>
            </td>
 		    <td nowrap > 
             <div align="center"><%= inqLoans.getE01CUSNA1().trim() %></div> 
            </td>
          </tr>           
            <tr id="trdark"> 
            <td nowrap > 
              <div align="center">Ejecutivo</div>
            </td>
            <td nowrap > 
              <div align="right"> </div>
            </td>
           <td nowrap > 
              <div align="right"> </div>
            </td>
            <td nowrap > 
              <div align="right"> </div>
            </td>
 		    <td nowrap > 
             <div align="center">CI: <%= userPO.getHeader4().trim()%></div> 
            </td>
          </tr>  
          <tr id="trdark"> 
            <td nowrap > 
              <div align="center"> </div>
            </td>
            <td nowrap > 
              <div align="right"> </div>
            </td>
           <td nowrap > 
              <div align="right"> </div>
            </td>
           <td nowrap > 
              <div align="right"> </div>
            </td>
 		    <td nowrap > 
              <div align="center">Socio</div>
            </td>

          </tr>
          </table></td>
          </tr>
          </table>
          
  </form>
</body>
</html>
