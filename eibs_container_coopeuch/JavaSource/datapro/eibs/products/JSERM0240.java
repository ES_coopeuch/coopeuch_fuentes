package datapro.eibs.products;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERC001001Message;
import datapro.eibs.beans.ERM024001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.SuperServlet;

/**
 * Servlet implementation class JSERM0240
 */
public class JSERM0240 extends JSEIBSServlet {
	private static final long serialVersionUID = 1L;

	
	protected static final int A_enter_distribution=100;
	protected static final int A_process_distribution=200;
    /**
     * Default constructor. 
     */
  

	@Override
			protected void processRequest(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res,
					HttpSession session, int screen) throws ServletException,
					IOException {
				switch (screen) {
				case A_enter_distribution:
		
					procReqDistributionEnter(user, req, res, session);
					
					break;
				case A_process_distribution:
					procDistributionProcess(user, req, res, session);
					
					break;
			
					default:
					forward(SuperServlet.devPage, req, res);
					break;
				}
							
			}


	protected void procReqDistributionEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		try {
			flexLog("About to call Page: ERM0240_enter_distribution.jsp");
			forward("ERM0240_enter_distribution.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	
	
	protected void procDistributionProcess(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			mp = getMessageProcessor("ERM0240", req);

			ERM024001Message msgList = (ERM024001Message) mp.getMessageRecord("ERM024001", user.getH01USR(), "0004");
			//Sets the employee  number from either the first page or the maintenance page
	
			if (req.getParameter("E01FLGPRC")!= null){
				String op = req.getParameter("E01FLGPRC");								
		
				msgList.setE01FLGPRC(op);
			
				req.setAttribute("E01FLGPRC", op);
			} 

			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { // if there are errors go back to first page
				session.setAttribute("error", error);
				flexLog("About to call Page: ERM0240_enter_distrution.jsp");
				forward("ERM0240_enter_distribution.jsp", req, res);
			} else {
			
				forward("ERM0240_distribution_success.jsp", req, res);

		
			}


		} finally {
			if (mp != null)
				mp.close();
		}
	}
	


}
