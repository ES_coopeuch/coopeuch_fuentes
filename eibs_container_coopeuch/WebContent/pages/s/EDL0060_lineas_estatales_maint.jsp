<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<html>
<head>
<title>Mantenedor de L&iacute;neas Estatales MYPE</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="msgLinEst" class="datapro.eibs.beans.EDL006001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

function cancel(op) {
	document.forms[0].SCREEN.value = op;
	document.forms[0].submit();
}


function textCounter(field,maxlimit) 
{
	if (field.value.length > maxlimit) // if too long...trim it!
	{
		alert("Solo se permiten 200 caracteres en el comentario");
	 	field.value = field.value.substring(0, maxlimit);
	}

}
</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "";
    String readonly2 = "";
    String readonly3 = "";
        
    if(msgLinEst.getE01MLNSTS().equals("A"))
    	if(!userPO.getPurpose().equals("NEW"))
    		  readonly2 = "readonly";

    if(userPO.getPurpose().equals("INQ"))
    {
		readonly = "readonly";
    	readonly2 = "readonly";
    	readonly3 = "readonly";
    }
    
    if(userPO.getPurpose().equals("MAINT"))
    	readonly3 = "readonly";
    


%>


<H3 align="center">
<% if(readonly == "readonly") out.print("Consulta "); else out.print("Mantenedor de "); %>
 L&iacute;neas Estatales MYPE<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="lineas_estatales_maint, EDL0060"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0060" >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="300">
    <INPUT TYPE=HIDDEN NAME="readonly" VALUE="<%=readonly %>">
    <input type=HIDDEN name="CURRCODEL" value="<%=request.getParameter("CURRCODEL").trim()%>"> 
    
  
  <h4>Datos L&iacute;nea</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
<%if(readonly3.equals("readonly")){ %>
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">L&iacute;nea :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01MLNSEQ" size="7"  value="<%= msgLinEst.getE01MLNSEQ().trim()%>" readonly >
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
              </div>
            </td>
          </tr>
<%} %>
          <tr id="trclear"> 
            <td nowrap width="20%"> 
              <div align="right">Garantizador :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01MLNGTZ" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNGTZ().trim()%>" readonly  style="text-align:right;">
                <input type="text" name="E01MLNDGR" size="41"  value="<%= msgLinEst.getE01MLNDGR().trim()%>" readonly  >
				<%if(!readonly2.equals("readonly")) { %>
				<a href="javascript:GetCodeDescCNOFC('E01MLNGTZ','E01MLNDGR','MY')"> 
				<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%}%>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Programa :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E01MLNPGM" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNPGM().trim()%>" readonly>
                <input type="text" name="E01MLNDPG" size="41"  value="<%= msgLinEst.getE01MLNDPG().trim()%>" readonly  >
				<%if(!readonly2.equals("readonly")) { %>
				<a href="javascript:GetCodeDescCNOFC('E01MLNPGM','E01MLNDPG','VG')"> 
				<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%}%>
              </div>
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Licitaci&oacute;n :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNLIN" size="7" maxlength="6" value="<%= msgLinEst.getE01MLNLIN().trim()%>"  onkeypress="enterInteger()" style="text-align:right;" <%out.print(readonly2); %>>
              </div>
            </td>
            <td nowrap> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
          </tr>
          
          <tr id="trclear"> 
            <td height="23"> 
              <div align="right">Moneda :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNMDA" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNMDA().trim()%>" readonly  >
                <input type="text" name="E01MLNDMN" size="41"  value="<%= msgLinEst.getE01MLNDMN().trim()%>" readonly  >
				<%if(!readonly2.equals("readonly")) { %>
					<a href="javascript:GetCodeDescCNOFC('E01MLNMDA','E01MLNDMN','VM')"> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%}%>					
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Estado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSTS" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSTS().trim()%>" readonly  >
                <input type="text" name="E01MLNDST" size="41"  value="<%= msgLinEst.getE01MLNDST().trim()%>" readonly  >
				<%if(!readonly.equals("readonly")) { %>
					<a href="javascript:GetCodeDescCNOFC('E01MLNSTS','E01MLNDST','VE')"> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%} %>
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Cupo L&iacute;nea :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCLI" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNCLI().trim())%>"  onkeypress="enterDecimal()" style="text-align:right;" <% out.print(readonly); %> >
              </div>
            </td>
            <td nowrap> 
              <div align="right">Fecha de Inicio :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNFVD" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNFVD().trim()%>" <%out.print(readonly2);%>>
                <input type="text" name="E01MLNFVM" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNFVM().trim()%>" <%out.print(readonly2);%>>
                <input type="text" name="E01MLNFVY" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNFVY().trim()%>" <%out.print(readonly2);%>>
				<%if(!readonly2.equals("readonly")) { %>
	                <a href="javascript:DatePicker(document.forms[0].E01MLNFVD,document.forms[0].E01MLNFVM,document.forms[0].E01MLNFVY)">
    	            <img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
				<%}%>
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Cupo L&iacute;nea Pesos :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCMN" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNCMN().trim())%>"  
                onkeypress="enterDecimal()" style="text-align:right;" <% out.print(readonly); %>>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Fecha de T&eacute;rmino :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNFTD" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNFTD().trim()%>" <% out.print(readonly); %>>
                <input type="text" name="E01MLNFTM" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNFTM().trim()%>" <% out.print(readonly); %>>
                <input type="text" name="E01MLNFTY" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNFTY().trim()%>" <% out.print(readonly); %>>
				<%if(!readonly.equals("readonly")) { %>
	                <a href="javascript:DatePicker(document.forms[0].E01MLNFTD,document.forms[0].E01MLNFTM,document.forms[0].E01MLNFTY)">
    	            <img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
        		<%} %>
              </div>
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Saldo Disponible :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSDO" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNSDO().trim())%>"  
                onkeypress="enterDecimal()" style="text-align:right;" <% out.print(readonly); %>>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Fecha de T&eacute;rmino Final :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTRD" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNTRD().trim()%>" <% out.print(readonly); %>>
                <input type="text" name="E01MLNTRM" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNTRM().trim()%>" <% out.print(readonly); %>>
                <input type="text" name="E01MLNTRY" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNTRY().trim()%>" <% out.print(readonly); %>>
				<%if(!readonly.equals("readonly")) { %>
	                <a href="javascript:DatePicker(document.forms[0].E01MLNTRD,document.forms[0].E01MLNTRM,document.forms[0].E01MLNTRY)">
    	            <img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
				<%}%>
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Monto Retenido :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSDR" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNSDR().trim())%>"  
                onkeypress="enterDecimal()" style="text-align:right;" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
          </tr>

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Tasa :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTAS" size="11" maxlength="10" value="<%= msgLinEst.getE01MLNTAS().trim()%>" onkeypress="enterDecimal()" style="text-align:right;"  <%out.print(readonly2); %>>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">% Garant&iacute;a :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNGRT" size="11" maxlength="10" value="<%= msgLinEst.getE01MLNGRT().trim()%>" onkeypress="enterDecimal()" style="text-align:right;"  <%out.print(readonly2); %>>
              </div>
            </td>
          </tr> 

          
          <tr id="trclear"> 
            <td height="23"> 
              <div align="right">Segmento 1 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG1" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG1().trim()%>" readonly >
                <input type="text" name="E01MLNDG1" size="41"  value="<%= msgLinEst.getE01MLNDG1().trim()%>" readonly  >
				<%if(!readonly2.equals("readonly")) { %>
					<a href="javascript:GetCodeDescCNOFC('E01MLNSG1','E01MLNDG1','2E')"> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%}%>
              </div>
            </td>
            <td nowrap  height="23" rowspan="5"> 
              <div align="right">Comentarios :</div>
            </td>
            <td nowrap rowspan="5"> 
              <div align="left"> 
                <textarea name="E01MLNGLS"  cols="80" rows="8" onKeyDown="textCounter(this,200)" onKeyUp="textCounter(this,200)" onkeypress="textCounter(this,200)" <% out.print(readonly); %>><%=msgLinEst.getE01MLNGLS().trim()%></textarea>
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Segmento 2 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG2" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG2().trim()%>"  readonly>
                <input type="text" name="E01MLNDG2" size="41"  value="<%= msgLinEst.getE01MLNDG2().trim()%>" readonly  >
				<%if(!readonly2.equals("readonly")) { %>
				<a href="javascript:GetCodeDescCNOFC('E01MLNSG2','E01MLNDG2','2E')"> 
				<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%}%>
              </div>
            </td>
          </tr> 

          <tr id="trclear"> 
            <td height="23"> 
              <div align="right">Segmento 3 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG3" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG3().trim()%>"  readonly>
                <input type="text" name="E01MLNDG3" size="41"  value="<%= msgLinEst.getE01MLNDG3().trim()%>" readonly  >
				<%if(!readonly2.equals("readonly")) { %>
					<a href="javascript:GetCodeDescCNOFC('E01MLNSG3','E01MLNDG3','2E')"> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%}%>
              </div>
            </td>
          </tr> 
 
           <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Segmento 4 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG4" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG4().trim()%>"  readonly>
                <input type="text" name="E01MLNDG4" size="41"  value="<%= msgLinEst.getE01MLNDG4().trim()%>" readonly  >
				<%if(!readonly2.equals("readonly")) { %>
					<a href="javascript:GetCodeDescCNOFC('E01MLNSG4','E01MLNDG4','2E')"> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%}%>
              </div>
            </td>
          </tr> 
 
           <tr id="trclear"> 
            <td height="23"> 
              <div align="right">Segmento 5 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG5" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG5().trim()%>"  readonly>
                <input type="text" name="E01MLNDG5" size="41"  value="<%= msgLinEst.getE01MLNDG5().trim()%>" readonly  >
				<%if(!readonly2.equals("readonly")) { %>
					<a href="javascript:GetCodeDescCNOFC('E01MLNSG5','E01MLNDG5','2E')"> 
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a>
				<%}%>
              </div>
            </td>
          </tr> 
          
          
          
        </table>
      </td>
    </tr>
  </table>


  <div align="center">

   <%if(readonly.equals("readonly")) { %>
    	<%if(userPO.getHeader1().equals("INQ")){%>
    		<input id="EIBSBTN" type="button" name="Volver" value="Volver" onclick="cancel(110)">
	    <%}else{%>
    		<input id="EIBSBTN" type="button" name="Volver" value="Volver" onclick="cancel(100)">
  		<%}%>
    <% }else{%>
    	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    	<input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel(100)">
    <% }%>
  </div>
  </form>
  
</body>
</html>
