package datapro.eibs.salesplatform;

/**
 * Plataforma de Ventas - ingreso / mantenimiento y simulacion de planillas
 * @author evargas       
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.coopeuch.core.tiendavirtual.ActualizaStockEntrada;
import cl.coopeuch.core.tiendavirtual.RespActualizaStock;
import cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtualSOAPProxy;
import datapro.eibs.beans.EDD009001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100500Message;
import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV100503Message;
import datapro.eibs.beans.EPV101001Message;
import datapro.eibs.beans.EPV101002Message;
import datapro.eibs.beans.EPV101004Message;
import datapro.eibs.beans.EPV101010Message;
import datapro.eibs.beans.EPV117001Message;
import datapro.eibs.beans.EPV170001Message;
import datapro.eibs.beans.EPV200500Message;
import datapro.eibs.beans.EPV200501Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageRecord;

public class JSEPV1010 extends JSEIBSServlet {
 
	private static final long serialVersionUID = -5013250952357918505L;
	
	protected static final int R_PLATFORM_LIST = 100;	
	protected static final int A_PLATFORM_LIST = 101;
	
	protected static final int R_PLATFORM_MAINT = 201;
	protected static final int R_PLATFORM_MAINT_BACK = 202;
	
	protected static final int A_PLATFORM_CARD_MAINTENANCE = 500;
	protected static final int A_PLATFORM_MAINTENANCE = 600;
	protected static final int A_PLATFORM_SIMULATE = 601;
	protected static final int A_PLATFORM_SIMULATE2 = 602;
	protected static final int A_PLATFORM_SIMULATE3 = 603;		
	protected static final int A_PLATFORM_LIQUIDACION = 700;
	protected static final int A_PLATFORM_DESEMBOLSO = 701;
	protected static final int A_PLATFORM_RESPUESTA_ENGINE = 900;
		
	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
			case R_PLATFORM_LIST:
				procReqPlatformList(user, req, res, session);
				break;
			case A_PLATFORM_LIST:
				procActionPlatformList(user, req, res, session);
				break;
			case R_PLATFORM_MAINT:
				procReqPlatform(user, req, res, session);
				break;
			case R_PLATFORM_MAINT_BACK:
				procReqPlatform_Back(user, req, res, session);
				break;				
			case A_PLATFORM_CARD_MAINTENANCE:
				procActionCardMaintenance(user, req, res, session);
				break;
			case A_PLATFORM_MAINTENANCE:
				
				procActionMaintenance(user, req, res, session);
				break;
			case A_PLATFORM_LIQUIDACION:
				procActionPlatformLiquidacion(user, req, res, session);
				break;
			case A_PLATFORM_DESEMBOLSO:
				procActionDesembolso(user, req, res, session);
				break;
			case A_PLATFORM_RESPUESTA_ENGINE:
				procActionResponseEngine(user, req, res, session);
			break;			
			case A_PLATFORM_SIMULATE:
				procActionSimulation(user, req, res, session);
				break;
			case A_PLATFORM_SIMULATE2:
				procActionSimulation2(user, req, res, session);
				break;
			case A_PLATFORM_SIMULATE3:
				procActionSimulation3(user, req, res, session);
				break;				
			default:
				forward(SuperServlet.devPage, req, res);
				break;
		}
	}

	
	/**
	 * procReqPlatformList
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
		UserPos userPO = getUserPos(ses);
		userPO.setRedirect("/servlet/datapro.eibs.salesplatform.JSEPV1010?SCREEN=101");
		userPO.setHeader1("Plataforma de Ventas - Evaluación");
		ses.setAttribute("userPO", userPO);
		forward("EPV1000_client_enter.jsp", req, res);
	}
	
	
	/**
	 * procActionPlatformList: find the list of forms depending on status, the program will epvl1005
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		
		String customer = req.getParameter("E01CUN") == null ? "0" : req.getParameter("E01CUN").trim();
		
		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV2005", req);
			EPV200501Message msgList = (EPV200501Message) mp.getMessageRecord("EPV200501", user.getH01USR(), "0015");
			
			msgList.setE01SELCUN(customer);
			msgList.setE01SELSTS("2");
			// Sends message
			mp.sendMessage(msgList);

			// Receive salesPlatform list
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			//
			if (mp.hasError(msgError)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", msgError);
				forward("EPV1000_client_enter.jsp", req, res);								
			} else {
				EPV200500Message header = (EPV200500Message) list.get(0);	
				userPO.setCusNum(header.getE00CUSCUN());
				userPO.setCusType(header.getE00CUSIDN());
				userPO.setCusName(header.getE00CUSNA1());
				session.setAttribute("userPO", userPO);
				list.remove(0);
				// save customer number
				session.setAttribute("EPV100501List", list);
				
				if (list.size()==0){//then no occurrences
					if (!"S".equals(req.getParameter("fin"))){//Only show when not fin
						msgError = new ELEERRMessage();
						msgError.setERRNUM("1");
						msgError.setERNU01("0001");		//4		                
						msgError.setERDS01("No existen solicitudes pendiente para Evaluar de este Cliente");//70					
						session.setAttribute("error", msgError);						
					}
					procReqPlatformList(user,req,res,session);					
				}else if (list.size()>1){//then at least 2 occurrences
					forward("EPV2010_salesplatform_list.jsp", req, res);
				}else{
					redirect("datapro.eibs.salesplatform.JSEPV2010?SCREEN=201&E01PVMNUM="+ msgList.getE01PVMNUM(), res);	
				}
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	

	/**
	 * Process the simulation for request
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionSimulation(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;

		try {			
			mp = getMessageProcessor("EPV1010", req);
			EPV101001Message msg = (EPV101001Message) mp.getMessageRecord("EPV101001", user.getH01USR(), "0012");			

			// Sets message with page fields
			setMessageRecord(req, msg);

			// Sending message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV101001Message) mp.receiveMessageRecord("EPV101001");
			if (mp.hasError(msgError)) {
				msg.setH01FLGWK2("");//si tiene error sigo simulando	
			}else{
				msg.setH01FLGWK2("X");//NO hubo error simulo
			}
			msg.setH01FLGWK3("");//desmarco si hubo simulacion2, ya que tiene que volver a presionar simular1
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("platfObj", msg);// seteamos nuevamente con la informacion obtenida
			req.setAttribute("SIMULAR", "S");
			forward("EPV1010_salesplatform_maintenance.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * Process the simulation2 for request
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionSimulation2(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		MessageProcessor mp = null;

		try {		
			mp = getMessageProcessor("EPV1010", req);
			EPV101001Message msg = (EPV101001Message) mp.getMessageRecord("EPV101001", user.getH01USR(), "0014");			

			// Sets message with page fields
			setMessageRecord(req, msg);

			// Sending message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV101001Message) mp.receiveMessageRecord("EPV101001");
			msg.setH01FLGWK2("X");//marco que la simulacion 1 esta lista.
			
			if (mp.hasError(msgError)) {
				msg.setH01FLGWK3("");//si tiene error sigo simulando	
			}else{
				msg.setH01FLGWK3("X");//NO hubo error simulo
			}
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("platfObj", msg);// seteamos nuevamente con la informacion obtenida				
		
			req.setAttribute("SIMULAR3", "S");	
			forward("EPV1010_salesplatform_maintenance.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionSimulation3(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		MessageProcessor mp = null;

		try {		
			mp = getMessageProcessor("EPV1010", req);
			EPV101001Message msg = (EPV101001Message) mp.getMessageRecord("EPV101001", user.getH01USR(), "0008");			

			// Sets message with page fields
			setMessageRecord(req, msg);

			// Sending message
			mp.sendMessage(msg);

			// Receive list data
			JBObjList list = mp.receiveMessageRecordList("H11FLGMAS");
			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV101001Message) mp.receiveMessageRecord("EPV101001");			

			
			msg.setH01FLGWK2("X");//marco que la simulacion 1 esta lista.
			
			if (mp.hasError(msgError)) {
				msg.setH01FLGWK3("");//si tiene error sigo simulando	
			}else{
				msg.setH01FLGWK3("X");//NO hubo error simulo
			}
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("platfObj", msg);// seteamos nuevamente con la informacion obtenida				
			session.setAttribute("planPag", list);
			
			req.setAttribute("SIMULAR3", "S");	
			forward("EPV1010_salesplatform_maintenance.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	
	/**
	 * procReqPlatform: This Method show a single Sales Platform for evaluation process.
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatform(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		try {
			UserPos userPO = getUserPos(session);
			userPO.setPurpose("MAINTENANCE");
			mp = getMessageProcessor("EPV1010", req);

			// Sets the platform number for maintenance
			String dop = req.getParameter("DOP");
			dop  = (dop==null)?"":dop;
			
			String opecode = "C".equals(dop) ? "0007" : "0002"; // 0007: Maintenance credit cesante, 0002: Maintenance wherever credit 
			EPV101001Message msg = (EPV101001Message) mp.getMessageRecord("EPV101001", user.getH01USR(), opecode);				


			// Sets the client number
			msg.setE01PVMCUN(userPO.getCusNum());

			// Sets the platform number for maintenance
			if (req.getParameter("E01PVMNUM") != null) {
				msg.setE01PVMNUM(req.getParameter("E01PVMNUM"));
			}

			// Send message
			mp.sendMessage(msg);
			
			if ("C".equals(dop)) {
				// Receive list data
				JBObjList list = mp.receiveMessageRecordList("H11FLGMAS");
				session.setAttribute("planPag", list);				
			}
			
			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			MessageRecord newmessage = mp.receiveMessageRecord();
			
			if (!mp.hasError(msgError)) {
				session.setAttribute("platfObj", newmessage);// set header page
				// if there are no errors go to maintenance page
				if (newmessage instanceof EPV101001Message) {					
					msgError = buscarPromocionesDescuentos(user,mp,session,((EPV101001Message)newmessage).getE01LNTYPG(), userPO.getCusNum());
					if (mp.hasError(msgError)) {
						session.setAttribute("error", msgError);
						forward("EPV1010_salesplatform_list.jsp", req, res);
					} else {
						forward("EPV1010_salesplatform_maintenance.jsp", req, res);					
					}
				} else {
					forward("EPV1010_salesplatform_cards_maintenance.jsp", req, res);
				}
			} else {
				// if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1010_salesplatform_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}	

	protected void procReqPlatform_Back(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {
		
			MessageRecord newmessage = (MessageRecord)session.getAttribute("platfObj");				
			if (newmessage instanceof EPV101001Message) {									
				forward("EPV1010_salesplatform_maintenance.jsp", req, res);					
			} else {
				forward("EPV1010_salesplatform_cards_maintenance.jsp", req, res);
			}
	}
	
	/**
	 * metodo que busca la lista de productos y promociones existentes programa epv1005 
	 * @param user
	 * @param mp
	 * @param session
	 * @throws IOException
	 */
	private ELEERRMessage buscarPromocionesDescuentos(ESS0030DSMessage user,MessageProcessor mp, 
						HttpSession session, String type, String NumCli) throws IOException {
		
		//Buscar Listas de Promociones y Descuentos
		EPV100503Message msg1 = null;				
		msg1 = (EPV100503Message) mp.getMessageRecord("EPV100503", user.getH01USR(), "0001");
		type = ("E".equals(type)?"D":type);
		msg1.setE03PVCREC(type);
		msg1.setE03PVCCUN(NumCli);
		mp.sendMessage(msg1);
		JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");	
		if (!mp.hasError(list)) {
			session.setAttribute("EPV100503List", list);					
			return new ELEERRMessage();
		} else {
			return (ELEERRMessage) mp.getError(list);
		}
	}
	
	
	/**
	 * Process the Maintenance
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked", "null" })
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		MessageProcessor mp2 = null;
		boolean firstTime = true;
		StringBuffer myRow = null;
		
		EPV101001Message platfObj = (EPV101001Message)session.getAttribute("platfObj");
		
		try {
			mp = getMessageProcessor("EPV1010", req);
			String opecode = "C".equals(platfObj.getE01LNTYPG()) ? "0009" : "0005";
			EPV101001Message msg = (EPV101001Message) mp.getMessageRecord("EPV101001", user.getH01USR(), opecode);				

			// Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			msg.setH01FLGWK2("X");//marco que la simulacion 1 esta lista. Pcataldo
			msg.setH01FLGWK3("X");//NO hubo error simulo                  P.Cataldo
		
			mp.sendMessage(msg);
			
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV101001Message) mp.receiveMessageRecord("EPV101001");

			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("platfObj", msg);
			
			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and sho errors
				forward("EPV1010_salesplatform_maintenance.jsp", req, res);			
			} else {
				
				if (!"Y".equals(msg.getE01TPRFEV())){// type don't call engine			
					// if there are no errors go back to list
					redirect("datapro.eibs.salesplatform.JSEPV1010?SCREEN=700&customer_number="+ msg.getE01PVMCUN()+"&solicitud_number="+ msg.getE01PVMNUM(), res);					
				} else {					
					// if there are no errors  --> call desicion engine
					JMSMotorClient jms =  new JMSMotorClient();
					//choice target depending of flag 
					String target = ("Y".equals(msg.getE01LNFRNG()))?msg.getE01TPRBOR():msg.getE01TPRBOB();
					String planilla = msg.getE01PVMNUM();
					String cliente =  msg.getE01PVMCUN();
					String tieneAval=msg.getE01INDAVA();//A posee aval...
					
					ELEERRMessage msgError2 = jms.sendMessage(planilla,target,user.getH01USR());	
					
					if (!"0".equals(msgError2.getERRNUM())){	
						// if there are errors go back to maintenance page and show errors
						session.setAttribute("error", msgError2);					
						forward("EPV1010_salesplatform_maintenance.jsp", req, res);		
					}else{			
						// if there are no errors, parse engine response					
						List lista = jms.parseXMLResponse();
						ELEERRMessage msgError3 = (ELEERRMessage)lista.get(0);
						EPV101004Message m = (EPV101004Message)lista.get(1);
						
						if (!"0".equals(msgError3.getERRNUM())){ 
							// if there are errors go back to maintenance page and show errors
							session.setAttribute("error", msgError3);					
							forward("EPV1010_salesplatform_maintenance.jsp", req, res);		
						}else{
							
							//Motivos de rechazos comerciales
							JBList beanList = new JBList();
							
							mp2 = getMessageProcessor("EPV1700", req);
							EPV170001Message msg2 = (EPV170001Message) mp2.getMessageRecord("EPV170001", user.getH01USR(), "0001");
							
							msg2.setE01PVENUM(msg.getE01PVMNUM());
							
							mp2.sendMessage(msg2);	
							
							while (true) {							
								
								//ELEERRMessage msgError9 = (ELEERRMessage) mp2.receiveMessageRecord("ELEERR");
								msg2 = (EPV170001Message) mp2.receiveMessageRecord("EPV170001");
								
								if ("*".equals(msg2.getH60FLGMAS())) {
									break;
								}else{
									if (firstTime) {
										firstTime = false;										
									}
									myRow = new StringBuffer();
									myRow.append(msg2.getS01PVEMRE());
									beanList.addRow("", myRow.toString());
								}
							}							 
							
							session.setAttribute("EPV170001List", beanList);						
							
							//verificamos si requiere aval si lo poseemos enviamos segunda llamada motor 
							//de lo contrario enviamos error y devolvemos a la pagina de mantenimiento
							if ("S".equals(m.getE04INDAVA())){
								
								if (!"A".equals(tieneAval)){//has not guarantor								
									ELEERRMessage msgError4 = new ELEERRMessage();
									msgError4.setERRNUM("1");
									msgError4.setERNU01("01");//4
						            msgError4.setERDS01("Motor Requiere Aval, Favor Introducir!!!");//70	
									session.setAttribute("error", msgError4);					
									//anyway continue the flow.
									m.setE04PVMCUN(cliente);
									m.setE04PVMNUM(planilla);
									m.setE04CLIAVA("C");
									
									
									session.setAttribute("EPV101004resp", m);
									session.removeAttribute("EPV101004respAval");							
									forward("EPV1010_salesplatform_respuesta_motor.jsp", req, res);									
								}else{
									//find new target (guarantor include)
									target = msg.getE01TPRBOA();								
								    ELEERRMessage msgError5 = jms.sendMessage(planilla,target,user.getH01USR());
									if (!"0".equals(msgError5.getERRNUM())){ 
										// if there are errors go back to maintenance page and show errors
										session.setAttribute("error", msgError5);					
										forward("EPV1010_salesplatform_maintenance.jsp", req, res);		
									}else{
										// if there are no errors, parse engine response (guarantor)
										List listaAval = jms.parseXMLResponse();
										ELEERRMessage msgError6 = (ELEERRMessage)listaAval.get(0);
										EPV101004Message mAval = (EPV101004Message)listaAval.get(1);
										if (!"0".equals(msgError6.getERRNUM())){ 
											// if there are errors go back to maintenance page and show errors
											session.setAttribute("error", msgError6);					
											forward("EPV1010_salesplatform_maintenance.jsp", req, res);		
										}else{
											mAval.setE04PVMCUN(cliente);
											mAval.setE04PVMNUM(planilla);
											mAval.setE04CLIAVA("A");										
											session.setAttribute("EPV101004respAval", mAval);										
											m.setE04PVMCUN(cliente);
											m.setE04PVMNUM(planilla);
											m.setE04CLIAVA("C");										
											session.setAttribute("EPV101004resp", m);										
											forward("EPV1010_salesplatform_respuesta_motor.jsp", req, res);		
										}//EoIe
									}//EoIe
								}//EoIe							
							}else{
								// if there are no errors and  not guarantor required, go show response	
								m.setE04PVMCUN(cliente);
								m.setE04PVMNUM(planilla);
								m.setE04CLIAVA("C");
								session.setAttribute("EPV101004resp", m);
								session.removeAttribute("EPV101004respAval");							
								forward("EPV1010_salesplatform_respuesta_motor.jsp", req, res);								
							}
							
						}
					}
					
				}
				
			}

		} finally {
			if (mp != null)
				mp.close();			
		}
	}
	
	
	/**
	 * Process the Maintenance
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	
	protected void procActionCardMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		
		try {
			mp = getMessageProcessor("EPV1010", req);

			EPV101010Message msg = (EPV101010Message) mp.getMessageRecord("EPV101010", user.getH01USR(), "0005");

			// Sets message with page fields
			msg.setH10SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);
			
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV101010Message) mp.receiveMessageRecord("EPV101010");
			// Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("platfObj", msg);
			
			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and sho errors
				forward("EPV1010_salesplatform_cards_maintenance.jsp", req, res);			
			} else {
				
				
				if (!"Y".equals(msg.getE10TPRFEV())){// type don't call engine
					// if there are no errors go back to list
					redirect("datapro.eibs.salesplatform.JSEPV1010?SCREEN=700&customer_number="+ msg.getE10PVMCUN()+"&solicitud_number="+ msg.getE10PVMNUM(), res);					
				} else {
					// if there are no errors  --> call desicion engine
					JMSMotorClient jms =  new JMSMotorClient();				
					String target = msg.getE10TPRBOB();	
					String planilla = msg.getE10PVMNUM();
					String cliente =  msg.getE10PVMCUN();
					
					ELEERRMessage msgError2 = jms.sendMessage(planilla,target,user.getH01USR());	
					if (!"0".equals(msgError2.getERRNUM())){	
						// if there are errors go back to maintenance page and show errors
						session.setAttribute("error", msgError2);					
						forward("EPV1010_salesplatform_cards_maintenance.jsp", req, res);		
					}else{			
						// if there are no errors, parse engine response					
						List lista = jms.parseXMLResponse();
						ELEERRMessage msgError3 = (ELEERRMessage)lista.get(0);
						EPV101004Message m = (EPV101004Message)lista.get(1);
						if (!"0".equals(msgError3.getERRNUM())){ 
							// if there are errors go back to maintenance page and sho errors
							session.setAttribute("error", msgError3);					
							forward("EPV1010_salesplatform_cards_maintenance.jsp", req, res);		
						}else{
							// if there are no errors, go show response	
							m.setE04PVMCUN(cliente);
							m.setE04PVMNUM(planilla);
							session.setAttribute("EPV101004resp", m);
							forward("EPV1010_salesplatform_respuesta_motor.jsp", req, res);	
						}
					}
					
				}
				
			}

		} finally {
			if (mp != null)
				mp.close();			
		}
	}
	
	/**
	 * procActionPlatformList: find the list of forms depending on status, the program will epvl1005
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionPlatformLiquidacion(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV1010", req);

			EPV101002Message msg = (EPV101002Message) mp.getMessageRecord("EPV101002");
			msg.setH02USERID(user.getH01USR());
			msg.setH02OPECOD("0002");
			msg.setH02TIMSYS(getTimeStamp());
			try {
				msg.setE02PVMCUN(req.getParameter("customer_number").trim());
			} catch (Exception e) {
				msg.setE02PVMCUN("0");
			}
			try {
				msg.setE02PVMNUM(req.getParameter("solicitud_number").trim());
			} catch (Exception e) {
				msg.setE02PVMNUM("0");
			}

			try {
				msg.setH02FLGWK1(req.getParameter("InVisado").trim());
			} catch (Exception e) {
				msg.setH02FLGWK1("0");
			}
			// Sends message
			mp.sendMessage(msg);



			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV101002Message) mp.receiveMessageRecord("EPV101002");
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");
			session.setAttribute("liquidacion", msg);
			if (!mp.hasError(msgError)) {
				// if there are no errors go to liquidacion page
				session.setAttribute("EPV101003List", list);
				forward("EPV1010_salesplatform_liquidacion.jsp", req, res);					
			} else {
				// if there are errors go same page
				session.setAttribute("error", msgError);
				forward("EPV1010_salesplatform_maintenance.jsp", req, res);			
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	/**
	 * Process the Maintenance
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionDesembolso(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {


		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1010", req);

			EPV101002Message msg = (EPV101002Message) mp.getMessageRecord("EPV101002", user.getH01USR(), "0005");

			// Sets message with page fields
			msg.setH02SCRCOD("01");
			
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);
			
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EPV101002Message) mp.receiveMessageRecord("EPV101002");
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");
			
			// Sets session with required data
			if (mp.hasError(msgError)) {
				// if there are no errors go to maintenance page
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				session.setAttribute("liquidacion", msg);
				session.setAttribute("EPV101003List", list);
				forward("EPV1010_salesplatform_liquidacion.jsp", req, res);
			} else {
				boolean ok = reservarProductosTiendaVirtual(req,session,user,msg.getE02PVMNUM());
				if (ok){
					// if there are no errors go back to Search
					forward("EPV1000_client_enter.jsp", req, res);					
				}else{
					// if there are no errors go to maintenance page
					session.setAttribute("error", msgError);
					session.setAttribute("userPO", userPO);
					session.setAttribute("liquidacion", msg);
					session.setAttribute("EPV101003List", list);
					forward("EPV1010_salesplatform_liquidacion.jsp", req, res);			
				}
						
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	protected void procActionResponseEngine(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1010", req);

			EPV101004Message msg = (EPV101004Message) mp.getMessageRecord("EPV101004");
		
			EPV101004Message source = (EPV101004Message)session.getAttribute("EPV101004resp");

			//set values properties
			populate(source,msg);
            msg.setH04USERID(user.getH01USR());
            msg.setH04PROGRM("EPV1010");
            msg.setH04TIMSYS(getTimeStamp());
            msg.setH04OPECOD("0001");
			try {
				msg.setH04FLGWK1(req.getParameter("APR").trim());
			} catch (Exception e) {
				msg.setH04FLGWK1("");
			}
			
			//Sending message
			mp.sendMessage(msg);
			
			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			
			msg = (EPV101004Message) mp.receiveMessageRecord();
			
			if (mp.hasError(msgError)) {
				// if there are errors go back to maintenance page and sho errors
				session.setAttribute("error", msgError);								
				forward("EPV1010_salesplatform_maintenance.jsp", req, res);
			} else {
				//if guarantor is enabled then save
				EPV101004Message aval = (EPV101004Message)session.getAttribute("EPV101004respAval");
				if (aval!=null){//have data
					
					EPV101004Message msgaval = (EPV101004Message) mp.getMessageRecord("EPV101004");
																					
					//set values properties
					populate(aval, msgaval);
					msgaval.setH04USERID(user.getH01USR());
					msgaval.setH04PROGRM("EPV1010");
					msgaval.setH04TIMSYS(getTimeStamp());
					msgaval.setH04OPECOD("0001");
					try {
						msgaval.setH04FLGWK1(req.getParameter("APR"));
					} catch (Exception e) {
						msgaval.setH04FLGWK1(req.getParameter(""));
					}					
					//Sending message
					mp.sendMessage(msgaval);
					
					//Receive error and data
					ELEERRMessage msgError1 = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
					msgaval = (EPV101004Message) mp.receiveMessageRecord("EPV101004");
					
					if (mp.hasError(msgError1)) {
						// if there are errors go back to maintenance page and sho errors
						session.setAttribute("error", msgError);								
						forward("EPV1010_salesplatform_maintenance.jsp", req, res);
						return;
					}				
				}//EoI-->aval
				
				if ("R".equals(req.getParameter("APR"))){
					// if there are no errors go back to list
					forward("EPV1000_client_enter.jsp", req, res);					
				}else{
					// if there are no errors go back to list
					redirect("datapro.eibs.salesplatform.JSEPV1010?SCREEN=700&customer_number="+ source.getE04PVMCUN()+"&solicitud_number="+ source.getE04PVMNUM() + "&InVisado=" + req.getParameter("APR2").trim() , res);					
				}
			}
			
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	
	private boolean reservarProductosTiendaVirtual(HttpServletRequest req, HttpSession session,
			ESS0030DSMessage user,String nroSol) throws ServletException, IOException{
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		boolean procesado=false;		
        
		try {
			mp = getMessageProcessor("EPV1170", req);

			EPV117001Message msg = (EPV117001Message) mp.getMessageRecord("EPV117001", user.getH01USR(), "0023");//todas las solicitudes a reservar

			// Sets message with page fields
			msg.setE01PTVCUN(userPO.getCusNum());
			msg.setE01PTVNUM(nroSol);

			//Sending message
			mp.sendMessage(msg);						
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");		
			if (!list.isEmpty()){//data exists 
				list.initRow();	 
				TiendaVirtualSOAPProxy tvp = new TiendaVirtualSOAPProxy();	
				ArrayList<String[]> err = new ArrayList<String[]>();
				while (list.getNextRow()) {
					EPV117001Message prod = (EPV117001Message) list.getRecord();					
					if (!"".equals(prod.getE01PTVCAM()) && !"".equals(prod.getE01PTVPRD()) && !"".equals(prod.getE01TVIFLG())){
						//llenamos servicio tienda virtual...
						ActualizaStockEntrada ent = new ActualizaStockEntrada();
						ent.setCodCampana(Integer.valueOf(prod.getE01PTVCAM())); 
						ent.setCodProducto(Integer.valueOf(prod.getE01PTVPRD()));						
						ent.setCantidad(Integer.valueOf(prod.getE01PTVCAN()));
						ent.setTipoOperacion(prod.getE01TVIFLG());//Se envia R=rechazo reserva I:ingreso reserva
						ent.setIdEvaluacion(Integer.valueOf(prod.getE01PTVNUM()));								
						RespActualizaStock resp = tvp.actualizaStock(ent);								
						if (resp.getCodRespuesta()==1){//0=OK y 1=noOK
							String[] valores={prod.getE01PTVCAM(),prod.getE01PTVPRD(),prod.getE01PTVCAN(),prod.getE01TVIFLG()};
							err.add(valores);
						}else{						
							//nos conectamos al as4000 para informar que el registro fue reservado satisfactoriamente
							mp = getMessageProcessor("EPV1170", req);
							EPV117001Message msgResult = (EPV117001Message) mp.getMessageRecord("EPV117001");
							populate(prod,msgResult);
							msgResult.setH01USERID(user.getH01USR());
							msgResult.setH01PROGRM("EPV1170");
							msgResult.setH01TIMSYS(getTimeStamp());
							msgResult.setH01OPECOD("0025");
							//Sending message					            
							mp.sendMessage(msgResult);	
							//Receive error and data
							ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();							
							if (mp.hasError(msgError)) {
								// if there are errors go back to maintenance page and sho errors
								session.setAttribute("error", msgError);								
								return procesado;
							}							
						}							
					}else{
					}
					
				}//EoW
				
				if (err.size()>0){
					String numErr = (err.size()>10)?"10":String.valueOf(err.size());
		            ELEERRMessage msgError = new ELEERRMessage();
		            msgError.setERRNUM(numErr);
		            int fil=1;
		            for (Iterator<String[]> iterator = err.iterator(); iterator.hasNext();) {
		            	String fila = (fil>9)?""+fil:"0"+fil;
		            	String[] val = (String[]) iterator.next();
		            	String des = "Error Actualizar Stock Tienda Virtual Prod:"+val[1]+" Cant:"+val[2]+" Oper:"+val[3];//55
		            	msgError.getField("ERNU"+fila).setString(fila);//numero error
		            	msgError.getField("ERDS"+fila).setString(des);//descripcion error
					}		            
		            session.setAttribute("error", msgError);
				}else{
					//existen productos a actualizar y los proceso bien.						
					procesado=true;
				}
				
			}else{
				//no existen productos a actualizar y proceso bien.
				procesado=true;
			}
			  


		}
		catch (Exception e) {
			String className = e.getClass().getName();
			String description = e.getMessage() == null ? "Exception General" : e.getMessage();					
            ELEERRMessage msgError = new ELEERRMessage();				
			msgError.setERRNUM("3");
            msgError.setERNU01("01");//4
            msgError.setERDS01("ERROR AL ACCEDER SERVICIO DE TIENDA VIRTUAL. VERIFIQUE COMUNICACION.");
            msgError.setERNU02("02");//4
            msgError.setERDS02(className);
            msgError.setERNU03("03");//4
            msgError.setERDS03(description.length() > 70 ? description.substring(0, 70) : description);	
			session.setAttribute("error", msgError);				
		}		
		finally {
			if (mp != null)
				mp.close();
		}
		return procesado;		
	}
	
}
