package datapro.eibs.products;

/**
 * Insert the type's description here.
 * Creation date: (1/19/00 6:08:55 PM)
 * @author: Orestes Garcia
 */
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream; 
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EOF011501Message;
import datapro.eibs.beans.EOF012001Message;
import datapro.eibs.beans.EOF013001Message;
import datapro.eibs.beans.EOF013501Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSEOF0130 extends datapro.eibs.master.SuperServlet { 

	// Oficial Checks options
	protected static final int R_CHK_SEARCH 	   = 100; 
	protected static final int R_ENVIAR	   		   = 200;
	protected static final int R_CHK_SEARCH_ALL    = 300; 
	protected static final int A_CHK 			   = 400;
	protected static final int A_RESUMEN_TRX	   = 500;
	protected static final int A_VIEW_DET_REEM	   = 600;
	
	protected static final int R_ENTER_CANCEL	   = 11;
	protected static final int A_CHK_DET_CANCEL    = 14;
	protected static final int A_CHK_CANCEL 	   = 16;

	protected String LangPath = "S";


	public JSEOF0130() {
		super();
	}
	public void destroy() {

	}
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	public void service(
			HttpServletRequest req, 
			HttpServletResponse res)
			throws ServletException, IOException {

			Socket s = null;
			MessageContext mc = null;

			ESS0030DSMessage msgUser = null;
			HttpSession session = null;

			session = (HttpSession) req.getSession(false);

			if (session == null) {
				try {
					res.setContentType("text/html");
					printLogInAgain(res.getWriter());
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Error: " + e);
				}
			}else {
				int screen = A_CHK_DET_CANCEL;
				try {
					msgUser =(datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");

					// Get the path from the user profile
					LangPath = super.rootPath + msgUser.getE01LAN() + "/";

					try {
						flexLog("Opennig Socket Connection");
						s = new Socket(super.hostIP, getInitSocket(req) + 1);
						s.setSoTimeout(super.sckTimeOut);
						mc = new MessageContext(new DataInputStream(new BufferedInputStream(s.getInputStream())),new DataOutputStream(
							new BufferedOutputStream(s.getOutputStream())),"datapro.eibs.beans");
					} catch (Exception e) 
					{
						e.printStackTrace();
						int sck = getInitSocket(req) + 1;
						flexLog("Socket not Open(Port " + sck + "). Error: " + e);
						res.sendRedirect(LangPath + super.sckNotOpenPage);
						return;
					}

					try {
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					} catch (Exception e) 
					{
						flexLog("Screen set to default value");
					}

					switch (screen) {

					case R_CHK_SEARCH : 
						procReqOFEnterSearch(msgUser, req, res, session);
						break;
					case R_CHK_SEARCH_ALL : 
						procReqChkReemplazo_all(mc, msgUser, req, res, session);
						break;
					case A_RESUMEN_TRX : 
						procSendReemplazo(mc,msgUser,req,res,session);
						break;
					case A_VIEW_DET_REEM :
						procViewDetReem(mc, msgUser, req, res, session);
						break;
					default :
						res.sendRedirect(LangPath + super.devPage);
						break;
					}

					try {
						s.close();
					} catch (Exception e) {
						flexLog("Error: " + e);
					}

				} catch (Exception e) {
					flexLog("Error: " + e);
					res.sendRedirect(LangPath + super.sckNotRespondPage);
			}
		}
	}


	/**
	 * Objetivo 	: Presenta pagina de busqueda de cheque.
	 * Fecha 		: 03/2016
	 * Creador 		: VARELLANO COOPEUCH
	 */
	private void procReqOFEnterSearch(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		ELEERRMessage msgError = new ELEERRMessage();
		userPO.setOption("OF");
		userPO.setOption(req.getParameter("OPTION"));
		userPO.setPurpose("INQUIRY");
		
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		session.removeAttribute("checkFilter");

		
		callPage(LangPath + "EOF0130_of_chk_search_1.jsp", req, res);
	}
	

	
	private void procSendReemplazo(MessageContext mc,
						ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {

		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		try {
			mp = new MessageProcessor("EOF0130");
			EOF013001Message msg = (EOF013001Message) mp.getMessageRecord("EOF013001", user.getH01USR(), "0002");
			
			msg.setH01OPECOD("0002");//*
			msg.setE01RMPCHQ(req.getParameter("E01RMPCHQ").toString()); // * N° cheque
			msg.setE01RMPTYP(req.getParameter("TIPOREE")); 	// *tipo reemplazo
			
			msg.setE01RMPBID(req.getParameter("E01RMPBID")); // *rut benef ch original
			msg.setE01RMPBNF(req.getParameter("E01RMPBNF")); // *nom benef ch original

			msg.setE01BENID1(req.getParameter("E01RMPBID")); // rut benef ch original ch 1
			msg.setE01BENEF1(req.getParameter("E01RMPBNF")); // nom benef ch original ch 1
			
			msg.setE01MTOCH1(req.getParameter("E01BCOOP1")); // *monto cheque 1
			msg.setE01BCOOP1(req.getParameter("BCOSELCH1")); // *opcion de bco seleccionada( 1 o 2) para ch 1
			
			if(req.getParameter("CODDESBCO1").length() > 39)
				msg.setE01BCOE11(req.getParameter("CODDESBCO1").substring(0, 39)); // *cod +  desc  opcion bco 1 ch 1
			else
				msg.setE01BCOE11(req.getParameter("CODDESBCO1")); // *cod +  desc  opcion bco 1 ch 1
				
			
			if(req.getParameter("CODDESBCO2").length() > 39)
				msg.setE01BCOE12(req.getParameter("CODDESBCO2").substring(0, 39)); // *cod +  desc  opcion bco 2 ch 1
			else	
				msg.setE01BCOE12(req.getParameter("CODDESBCO2"));
			
			
			
			msg.setE01BENEF2(req.getParameter("E02OFMAPL")); // *nom aplicante ch original ch 2
			msg.setE01BENID2(req.getParameter("E02OFMAID")); // *rut aplicante ch original ch 2
			
			if (req.getParameter("E01BCOOP2") != null){
				msg.setE01MTOCH2(req.getParameter("E01BCOOP2")); // *monto cheque 2
			}else{
				msg.setE01MTOCH2("0"); //*monto cheque 2
			}
			
			if(msg.getE01RMPTYP().trim().equals("RME")){
				msg.setE01BCOOP2(req.getParameter("BCOSELCH2")); // *opcion de bco seleccionada( 1 o 2) para ch 2
				 // *cod +  desc  opcion bco 1 ch 2
				if(req.getParameter("CODDESBCO1").length() > 39)
					msg.setE01BCOE21(req.getParameter("CODDESBCO1").substring(0, 39));
				else
					msg.setE01BCOE21(req.getParameter("CODDESBCO1"));
				
				if(req.getParameter("CODDESBCO2").length() > 39)
					msg.setE01BCOE22(req.getParameter("CODDESBCO2").substring(0, 39));
				else
					msg.setE01BCOE22(req.getParameter("CODDESBCO2"));				
			}
			
			
			if (req.getParameter("E01MTOEFE") != null){
				msg.setE01MTOEFE(req.getParameter("E01MTOEFE")); // monto efectivo
			}else{
				msg.setE01MTOEFE("0"); // monto efectivo
			}
			
 			
			flexLog("mensaje enviado procSendReemplazo ==> " + msg);
			mp.sendMessage(msg);
			
			MessageRecord newmsg = mp.receiveMessageRecord();
			MessageRecord newmsg2 = mp.receiveMessageRecord();
			
			if (mp.hasError(newmsg)) {
			
				msgError = (ELEERRMessage) newmsg;
				session.setAttribute("error", msgError);
				callPage("/pages/s/EOF0130_dv_inq_doc_det.jsp", req, res);	
			} else {
			    session.setAttribute("dvDocDet", msg);
				session.setAttribute("error", msgError);
				session.setAttribute("Ree", newmsg2);
				callPage( "/pages/s/EOF0130_resumen_trx.jsp", req, res);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	
	protected void procReqChkReemplazo_all(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			
			throws ServletException, IOException {

			try {
				flexLog("About to call Page: " + LangPath + "EOF0130_of_chk_search_2.jsp");
				callPage(LangPath + "EOF0130_of_chk_search_2.jsp", req, res);
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
			}

		}
		
	
	
	

	

	
	//new***********
	
	/**
	 * This method was created in VisualAge.
	 */
	
/**
 * Objetivo 	: Presenta vista de detalle de reemplazo de cheques. Esta consulta es invocada desde la pantalla de aprobaci�n de cheques de terceros.
 * Fecha 		: 03/2016
 * Creador 		: JLSP
 */
	private void procViewDetReem
	(MessageContext mc,	ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {

		MessageProcessor mp = null;

		try {
			mp = new MessageProcessor("EOF0135");
			EOF013501Message msg = (EOF013501Message) mp.getMessageRecord("EOF013501", user.getH01USR(), "0001");

			msg.setE01RMPRE1(req.getParameter("ACCNUM").toString()); // Referencia de Cheque
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01PROGRM("EOF0135");

			mp.sendMessage(msg);

			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EOF013501Message) mp.receiveMessageRecord();
			
			if (mp.hasError(msgError)) {
				session.setAttribute("error", msgError);
			} 
			else 
			{
				session.setAttribute("VieDetReem", msg);
				session.setAttribute("error", msgError);
				callPage( "/pages/s/EOF0130_view_det_reem.jsp", req, res);
			}

		} catch (Exception e) {
				e.printStackTrace();
		}	
	}
	
// CIERRE	
}
