package datapro.eibs.products;


import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDL090001Message;
import datapro.eibs.beans.EDL090002Message;
import datapro.eibs.beans.EDL090003Message;
import datapro.eibs.beans.EDL096001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.JBListRec;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageRecord;


public class JSEDL0960 extends JSEIBSServlet {


	/**
	 *  
	 */
	private static final long serialVersionUID = 5774581979349998967L;
	
	protected static final int R_FORM_CAEV = 100;	
	protected static final int A_FORM_CAEV = 101;
	
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		switch (screen) {
			case R_FORM_CAEV:
				procReqEnterInquiry(user, req, res, session);
				break;
			case A_FORM_CAEV:
				procShowFormCAEV(user, req, res, session);
				break;
			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}
	


	protected void procReqEnterInquiry(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
				throws ServletException, IOException {

		ELEERRMessage msgError = new datapro.eibs.beans.ELEERRMessage();
		UserPos	userPO = new datapro.eibs.beans.UserPos();	

		userPO.setOption("CAEV");
		userPO.setPurpose("INQUIRY");
		userPO.setRedirect("/servlet/datapro.eibs.products.JSEDL0960?SCREEN=101");
		userPO.setProdCode("10");
		
		//Others Parameters
		userPO.setHeader1("E01CAEACC");
			
		ses.setAttribute("error", msgError);
		ses.setAttribute("userPO", userPO);

		forward("GENERIC_account_enter.jsp", req, res);
	}

	

	protected void procShowFormCAEV(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);		
		String account = req.getParameter("E01CAEACC") == null ? "0" : req.getParameter("E01CAEACC").trim();
		
		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EDL0960", req);
			EDL096001Message msg = (EDL096001Message) mp.getMessageRecord("EDL096001", user.getH01USR(), "0004");
			msg.setE01CAEACC(account);
			msg.setH01OPECOD("0004");

			// Sends message
			mp.sendMessage(msg);

			//Receive Error
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();			
			// Receive form CAEV
			EDL096001Message msgForm = (EDL096001Message) mp.receiveMessageRecord();
			
			if (mp.hasError(error)) {
				// if there are errors go back to firstpage
				session.setAttribute("error", error);
				forward("GENERIC_account_enter.jsp", req, res);
			} else {
				
//***********************************************************************************************************
//***********BUSCAMOS PLAN DE PAGO
//***********************************************************************************************************
				MessageRecord newmessage = null;
				EDL090001Message msgHeader = null;
				ELEERRMessage msgError = new datapro.eibs.beans.ELEERRMessage();
				EDL090002Message msgList = null;
				EDL090003Message msgListUF = null;
				JBListRec beanList = null;
				JBListRec beanListUF = null;

				
				// Send Initial data
				try
				{
					msgHeader = (EDL090001Message)mp.getMessageRecord("EDL090001");
				 	msgHeader.setH01USERID(user.getH01USR());
				 	msgHeader.setH01PROGRM("EDL0900");
				 	msgHeader.setH01TIMSYS(getTimeStamp());
				 	msgHeader.setH01OPECOD("0004");				 
				 	msgHeader.setE01DEAACC(account);
					msgHeader.send();	
				 	msgHeader.destroy();
				}		
				catch (Exception e)
				{
				  	throw new RuntimeException("Socket Communication Error");
				}
					
				// Receive Data
				try
				{
				    newmessage = mp.receiveMessageRecord();

					if (newmessage.getFormatName().equals("ELEERR")) {
						msgError = (ELEERRMessage)newmessage;
						session.setAttribute("error", msgError);
						forward("GENERIC_account_enter.jsp", req, res);						
					}
					else if (newmessage.getFormatName().equals("EDL090001")) {

						msgHeader = new datapro.eibs.beans.EDL090001Message();							
						msgHeader = (EDL090001Message)newmessage;												

						// Fill List bean
						int colNum = 94;
						beanList = new datapro.eibs.beans.JBListRec();
						beanList.init(colNum);

						String marker = "";
						String myFlag = "";
						String myRow[] = new String[colNum];
						for (int i=0; i<colNum; i++) {
							myRow[i] = "";
						}
						
						while (true) {
							newmessage =  mp.receiveMessageRecord();
							msgList = (EDL090002Message)newmessage;

							marker = msgList.getE02ENDFLD();

							if (marker.equals("*")) {
								break;
							} else {								
								//Quote List
								myRow[0] =  Util.formatCell(msgList.getE02DLPPNU());	// Quote Number
								myRow[1] =  Util.formatDate(msgList.getE02DLPPD1(),msgList.getE02DLPPD2(),msgList.getE02DLPPD3());	// Fecha a Pagar
								myRow[2] =  Util.formatCCY(msgList.getE02DLPPRN());	// Principal Pymt
								myRow[3] =  Util.formatCCY(msgList.getE02DLPINT());	// Interest Pymnt
								myRow[4] =  Util.formatCCY(msgList.getE02DLPOTH());	// Other Charges
								myRow[5] =  Util.formatCell(msgList.getE02DLPTOT());	// Total Quotes
								myRow[6] =  Util.formatCell(msgList.getE02DLPBAL());	// Balance
								myRow[7] =  Util.formatCell(msgList.getE02DLPSTS());	// Status
								myRow[8] =  Util.formatCell(msgList.getE02DLPVEN());	// Mature
								myRow[9] =  Util.formatDate(msgList.getE02DLPDT1(),msgList.getE02DLPDT2(),msgList.getE02DLPDT3());	// Fecha a Pagar
								myRow[10] =  Util.formatCell(msgList.getE02DLPPAG());	// Total Pymnt
								//Quote Detail
								myRow[11] =  Util.formatCell(msgList.getE02DESC01());	// Description
								myRow[12] =  Util.formatCell(msgList.getE02DESC02());
								myRow[13] =  Util.formatCell(msgList.getE02DESC03());
								myRow[14] =  Util.formatCell(msgList.getE02DESC04());
								myRow[15] =  Util.formatCell(msgList.getE02DESC05());
								myRow[16] =  Util.formatCell(msgList.getE02DESC06());
								myRow[17] =  Util.formatCell(msgList.getE02DESC07());
								myRow[18] =  Util.formatCell(msgList.getE02DESC08());
								myRow[19] =  Util.formatCell(msgList.getE02DESC09());
								myRow[20] =  Util.formatCell(msgList.getE02DESC10());
								myRow[21] =  Util.formatCell(msgList.getE02DESC11());
								myRow[22] =  Util.formatCell(msgList.getE02DESC12());
								myRow[23] =  Util.formatCell(msgList.getE02DESC13());
								myRow[24] =  Util.formatCell(msgList.getE02DESC14());
								myRow[25] =  Util.formatCell(msgList.getE02DESC15());
								myRow[26] =  Util.formatCell(msgList.getE02DESC16());
								myRow[27] =  Util.formatCell(msgList.getE02DESC17());
								myRow[28] =  Util.formatCell(msgList.getE02DESC18());
								myRow[29] =  Util.formatCell(msgList.getE02DESC19());
								myRow[30] =  Util.formatCell(msgList.getE02DESC20());
								myRow[31] =  Util.formatCCY(msgList.getE02AMNT01().trim()); 	//Amount
								myRow[32] =  Util.formatCCY(msgList.getE02AMNT02().trim());
								myRow[33] =  Util.formatCCY(msgList.getE02AMNT03().trim());
								myRow[34] =  Util.formatCCY(msgList.getE02AMNT04().trim());
								myRow[35] =  Util.formatCCY(msgList.getE02AMNT05().trim());
								myRow[36] =  Util.formatCCY(msgList.getE02AMNT06().trim());
								myRow[37] =  Util.formatCCY(msgList.getE02AMNT07().trim());
								myRow[38] =  Util.formatCCY(msgList.getE02AMNT08().trim());
								myRow[39] =  Util.formatCCY(msgList.getE02AMNT09().trim());
								myRow[40] =  Util.formatCCY(msgList.getE02AMNT10().trim());
								myRow[41] =  Util.formatCCY(msgList.getE02AMNT11().trim());
								myRow[42] =  Util.formatCCY(msgList.getE02AMNT12().trim());
								myRow[43] =  Util.formatCCY(msgList.getE02AMNT13().trim());
								myRow[44] =  Util.formatCCY(msgList.getE02AMNT14().trim());
								myRow[45] =  Util.formatCCY(msgList.getE02AMNT15().trim());
								myRow[46] =  Util.formatCCY(msgList.getE02AMNT16().trim());
								myRow[47] =  Util.formatCCY(msgList.getE02AMNT17().trim());
								myRow[48] =  Util.formatCCY(msgList.getE02AMNT18().trim());
								myRow[49] =  Util.formatCCY(msgList.getE02AMNT19().trim());
								myRow[50] =  Util.formatCCY(msgList.getE02AMNT20().trim());
								myRow[51] =  Util.formatCCY(msgList.getE02PAID01().trim());	//Paid	
								myRow[52] =  Util.formatCCY(msgList.getE02PAID02().trim());
								myRow[53] =  Util.formatCCY(msgList.getE02PAID03().trim());
								myRow[54] =  Util.formatCCY(msgList.getE02PAID04().trim());
								myRow[55] =  Util.formatCCY(msgList.getE02PAID05().trim());
								myRow[56] =  Util.formatCCY(msgList.getE02PAID06().trim());
								myRow[57] =  Util.formatCCY(msgList.getE02PAID07().trim());
								myRow[58] =  Util.formatCCY(msgList.getE02PAID08().trim());
								myRow[59] =  Util.formatCCY(msgList.getE02PAID09().trim());
								myRow[60] =  Util.formatCCY(msgList.getE02PAID10().trim());
								myRow[61] =  Util.formatCCY(msgList.getE02PAID11().trim());
								myRow[62] =  Util.formatCCY(msgList.getE02PAID12().trim());
								myRow[63] =  Util.formatCCY(msgList.getE02PAID13().trim());
								myRow[64] =  Util.formatCCY(msgList.getE02PAID14().trim());
								myRow[65] =  Util.formatCCY(msgList.getE02PAID15().trim());
								myRow[66] =  Util.formatCCY(msgList.getE02PAID16().trim());
								myRow[67] =  Util.formatCCY(msgList.getE02PAID17().trim());
								myRow[68] =  Util.formatCCY(msgList.getE02PAID18().trim());
								myRow[69] =  Util.formatCCY(msgList.getE02PAID19().trim());
								myRow[70] =  Util.formatCCY(msgList.getE02PAID20().trim());
								myRow[71] =  Util.formatCCY(msgList.getE02COND01().trim());	//Condnoacion	
								myRow[72] =  Util.formatCCY(msgList.getE02COND02().trim());
								myRow[73] =  Util.formatCCY(msgList.getE02COND03().trim());
								myRow[74] =  Util.formatCCY(msgList.getE02COND04().trim());
								myRow[75] =  Util.formatCCY(msgList.getE02COND05().trim());
								myRow[76] =  Util.formatCCY(msgList.getE02COND06().trim());
								myRow[77] =  Util.formatCCY(msgList.getE02COND07().trim());
								myRow[78] =  Util.formatCCY(msgList.getE02COND08().trim());
								myRow[79] =  Util.formatCCY(msgList.getE02COND09().trim());
								myRow[80] =  Util.formatCCY(msgList.getE02COND10().trim());
								myRow[81] =  Util.formatCCY(msgList.getE02COND11().trim());
								myRow[82] =  Util.formatCCY(msgList.getE02COND12().trim());
								myRow[83] =  Util.formatCCY(msgList.getE02COND13().trim());
								myRow[84] =  Util.formatCCY(msgList.getE02COND14().trim());
								myRow[85] =  Util.formatCCY(msgList.getE02COND15().trim());
								myRow[86] =  Util.formatCCY(msgList.getE02COND16().trim());
								myRow[87] =  Util.formatCCY(msgList.getE02COND17().trim());
								myRow[88] =  Util.formatCCY(msgList.getE02COND18().trim());
								myRow[89] =  Util.formatCCY(msgList.getE02COND19().trim());
								myRow[90] =  Util.formatCCY(msgList.getE02COND20().trim());
								myRow[91] =  Util.formatCCY(msgList.getE02TOAMNT().trim());
								myRow[92] =  Util.formatCCY(msgList.getE02TOPAID().trim());
								myRow[93] =  Util.formatCCY(msgList.getE02TOCOND().trim());	
								//add only if DLPSTS=P 
								if ("P".equals(msgList.getE02DLPSTS())){
									beanList.addRow(myFlag, myRow);	
								}																				
							}
						}//EoWhile
						
						
						// Fill List bean
						colNum = 94;
						beanListUF = new datapro.eibs.beans.JBListRec();
						beanListUF.init(colNum);

						String markerUF = "";
						String myFlagUF = "";
						String myRowUF[] = new String[colNum];
						for (int i=0; i<colNum; i++) {
							myRowUF[i] = "";
						}

						
						while (true) {             // registro en UF
							newmessage =  mp.receiveMessageRecord();
							msgListUF = (EDL090003Message)newmessage;

							markerUF = msgListUF.getE03ENDFLD();

							if (markerUF.equals("*")) {
								break;
							} else {
								//Quote List				
								myRowUF[0] =  Util.formatCell(msgListUF.getE03DLPPNU());	// Quote Number
								myRowUF[1] =  Util.formatDate(msgListUF.getE03DLPPD1(),msgListUF.getE03DLPPD2(),msgListUF.getE03DLPPD3());	// Fecha a Pagar
								myRowUF[2] =  Util.formatCell(msgListUF.getE03DLPPRN());	// Principal Pymt
								myRowUF[3] =  Util.formatCell(msgListUF.getE03DLPINT());	// Interest Pymnt
								myRowUF[4] =  Util.formatCell(msgListUF.getE03DLPOTH());	// Other Charges
								myRowUF[5] =  Util.formatCell(msgListUF.getE03DLPTOT());	// Total Quotes
								myRowUF[6] =  Util.formatCell(msgListUF.getE03DLPBAL());	// Balance
								myRowUF[7] =  Util.formatCell(msgListUF.getE03DLPSTS());	// Status
								myRowUF[8] =  Util.formatCell(msgListUF.getE03DLPVEN());	// Mature
								myRowUF[9] =  Util.formatDate(msgListUF.getE03DLPDT1(),msgListUF.getE03DLPDT2(),msgListUF.getE03DLPDT3());	// Fecha a Pagar
								myRowUF[10] =  Util.formatCell(msgListUF.getE03DLPPAG());	// Total Pymnt
								//Quote Detail
								myRowUF[11] =  Util.formatCell(msgListUF.getE03DESC01());	// Description
								myRowUF[12] =  Util.formatCell(msgListUF.getE03DESC02());
								myRowUF[13] =  Util.formatCell(msgListUF.getE03DESC03());
								myRowUF[14] =  Util.formatCell(msgListUF.getE03DESC04());
								myRowUF[15] =  Util.formatCell(msgListUF.getE03DESC05());
								myRowUF[16] =  Util.formatCell(msgListUF.getE03DESC06());
								myRowUF[17] =  Util.formatCell(msgListUF.getE03DESC07());
								myRowUF[18] =  Util.formatCell(msgListUF.getE03DESC08());
								myRowUF[19] =  Util.formatCell(msgListUF.getE03DESC09());
								myRowUF[20] =  Util.formatCell(msgListUF.getE03DESC10());
								myRowUF[21] =  Util.formatCell(msgListUF.getE03DESC11());
								myRowUF[22] =  Util.formatCell(msgListUF.getE03DESC12());
								myRowUF[23] =  Util.formatCell(msgListUF.getE03DESC13());
								myRowUF[24] =  Util.formatCell(msgListUF.getE03DESC14());
								myRowUF[25] =  Util.formatCell(msgListUF.getE03DESC15());
								myRowUF[26] =  Util.formatCell(msgListUF.getE03DESC16());
								myRowUF[27] =  Util.formatCell(msgListUF.getE03DESC17());
								myRowUF[28] =  Util.formatCell(msgListUF.getE03DESC18());
								myRowUF[29] =  Util.formatCell(msgListUF.getE03DESC19());
								myRowUF[30] =  Util.formatCell(msgListUF.getE03DESC20());
								myRowUF[31] =  Util.formatCell(msgListUF.getE03AMNT01().trim()); 	//Amount
								myRowUF[32] =  Util.formatCell(msgListUF.getE03AMNT02().trim());
								myRowUF[33] =  Util.formatCell(msgListUF.getE03AMNT03().trim());
								myRowUF[34] =  Util.formatCell(msgListUF.getE03AMNT04().trim());
								myRowUF[35] =  Util.formatCell(msgListUF.getE03AMNT05().trim());
								myRowUF[36] =  Util.formatCell(msgListUF.getE03AMNT06().trim());
								myRowUF[37] =  Util.formatCell(msgListUF.getE03AMNT07().trim());
								myRowUF[38] =  Util.formatCell(msgListUF.getE03AMNT08().trim());
								myRowUF[39] =  Util.formatCell(msgListUF.getE03AMNT09().trim());
								myRowUF[40] =  Util.formatCell(msgListUF.getE03AMNT10().trim());
								myRowUF[41] =  Util.formatCell(msgListUF.getE03AMNT11().trim());
								myRowUF[42] =  Util.formatCell(msgListUF.getE03AMNT12().trim());
								myRowUF[43] =  Util.formatCell(msgListUF.getE03AMNT13().trim());
								myRowUF[44] =  Util.formatCell(msgListUF.getE03AMNT14().trim());
								myRowUF[45] =  Util.formatCell(msgListUF.getE03AMNT15().trim());
								myRowUF[46] =  Util.formatCell(msgListUF.getE03AMNT16().trim());
								myRowUF[47] =  Util.formatCell(msgListUF.getE03AMNT17().trim());
								myRowUF[48] =  Util.formatCell(msgListUF.getE03AMNT18().trim());
								myRowUF[49] =  Util.formatCell(msgListUF.getE03AMNT19().trim());
								myRowUF[50] =  Util.formatCell(msgListUF.getE03AMNT20().trim());
								myRowUF[51] =  Util.formatCell(msgListUF.getE03PAID01().trim());	//Paid	
								myRowUF[52] =  Util.formatCell(msgListUF.getE03PAID02().trim());
								myRowUF[53] =  Util.formatCell(msgListUF.getE03PAID03().trim());
								myRowUF[54] =  Util.formatCell(msgListUF.getE03PAID04().trim());
								myRowUF[55] =  Util.formatCell(msgListUF.getE03PAID05().trim());
								myRowUF[56] =  Util.formatCell(msgListUF.getE03PAID06().trim());
								myRowUF[57] =  Util.formatCell(msgListUF.getE03PAID07().trim());
								myRowUF[58] =  Util.formatCell(msgListUF.getE03PAID08().trim());
								myRowUF[59] =  Util.formatCell(msgListUF.getE03PAID09().trim());
								myRowUF[60] =  Util.formatCell(msgListUF.getE03PAID10().trim());
								myRowUF[61] =  Util.formatCell(msgListUF.getE03PAID11().trim());
								myRowUF[62] =  Util.formatCell(msgListUF.getE03PAID12().trim());
								myRowUF[63] =  Util.formatCell(msgListUF.getE03PAID13().trim());
								myRowUF[64] =  Util.formatCell(msgListUF.getE03PAID14().trim());
								myRowUF[65] =  Util.formatCell(msgListUF.getE03PAID15().trim());
								myRowUF[66] =  Util.formatCell(msgListUF.getE03PAID16().trim());
								myRowUF[67] =  Util.formatCell(msgListUF.getE03PAID17().trim());
								myRowUF[68] =  Util.formatCell(msgListUF.getE03PAID18().trim());
								myRowUF[69] =  Util.formatCell(msgListUF.getE03PAID19().trim());
								myRowUF[70] =  Util.formatCell(msgListUF.getE03PAID20().trim());
								myRowUF[71] =  Util.formatCell(msgListUF.getE03COND01().trim());	//Condnoacion	
								myRowUF[72] =  Util.formatCell(msgListUF.getE03COND02().trim());
								myRowUF[73] =  Util.formatCell(msgListUF.getE03COND03().trim());
								myRowUF[74] =  Util.formatCell(msgListUF.getE03COND04().trim());
								myRowUF[75] =  Util.formatCell(msgListUF.getE03COND05().trim());
								myRowUF[76] =  Util.formatCell(msgListUF.getE03COND06().trim());
								myRowUF[77] =  Util.formatCell(msgListUF.getE03COND07().trim());
								myRowUF[78] =  Util.formatCell(msgListUF.getE03COND08().trim());
								myRowUF[79] =  Util.formatCell(msgListUF.getE03COND09().trim());
								myRowUF[80] =  Util.formatCell(msgListUF.getE03COND10().trim());
								myRowUF[81] =  Util.formatCell(msgListUF.getE03COND11().trim());
								myRowUF[82] =  Util.formatCell(msgListUF.getE03COND12().trim());
								myRowUF[83] =  Util.formatCell(msgListUF.getE03COND13().trim());
								myRowUF[84] =  Util.formatCell(msgListUF.getE03COND14().trim());
								myRowUF[85] =  Util.formatCell(msgListUF.getE03COND15().trim());
								myRowUF[86] =  Util.formatCell(msgListUF.getE03COND16().trim());
								myRowUF[87] =  Util.formatCell(msgListUF.getE03COND17().trim());
								myRowUF[88] =  Util.formatCell(msgListUF.getE03COND18().trim());
								myRowUF[89] =  Util.formatCell(msgListUF.getE03COND19().trim());
								myRowUF[90] =  Util.formatCell(msgListUF.getE03COND20().trim());
								myRowUF[91] =  Util.formatCell(msgListUF.getE03TOAMNT().trim());
								myRowUF[92] =  Util.formatCell(msgListUF.getE03TOPAID().trim());
								myRowUF[93] =  Util.formatCell(msgListUF.getE03TOCOND().trim());
								myRow[93] =  Util.formatCCY(msgList.getE02TOCOND().trim());	
								//add only if DLPSTS=P 
								if ("P".equals(msgListUF.getE03DLPSTS())){
									beanListUF.addRow(myFlagUF, myRowUF);	
								}	
							}

						}
						
						session.setAttribute("list", beanList);
						session.setAttribute("listUF", beanListUF);
						session.setAttribute("header", msgHeader);
					}
					else
						flexLog("Message " + newmessage.getFormatName() + " received.");

				}
				catch (Exception e)	{
				  	throw new RuntimeException("Socket Communication Error");
				}
//***********************************************************************************************************
//*********** FIN..				
//***********************************************************************************************************				
				session.setAttribute("formObj", msgForm);
				forward("EDL0960_show_form_CAEV.jsp", req, res);	
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

 }	



