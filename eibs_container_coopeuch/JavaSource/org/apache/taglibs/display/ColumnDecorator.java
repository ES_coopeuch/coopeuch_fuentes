/**
 * $Id: ColumnDecorator.java,v 1.1 2011/12/01 13:47:08 pcataldo Exp $
 *
 * Status: Ok
 **/

package org.apache.taglibs.display;

public abstract class ColumnDecorator extends Decorator
{
   public ColumnDecorator()
   {
      super();
   }

   public abstract String decorate( Object columnValue );
}

