<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV101003Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ticket" class="datapro.eibs.beans.EPV104003Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center"> Plataforma de Ventas - Curse<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_ticket.jsp,JSEPV1040"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1040">
  <input type="hidden" name="SCREEN" value=" "> 

 <% int row = 0;%>
 <% String name="";%>  
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="30%">
                <%=ticket.getE03PVDCUN()%> 
             </td>
             <td nowrap align="right" width="20%"> Nombre :</td>
             <td nowrap align="left" width="30%">
                <%=ticket.getE03CUSNA1()%> 
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="30%">
                <%=ticket.getE03PVDNUM()%> 
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="30%">
				<%=Util.formatCell(ticket.getE03PVMODD() + "/" + ticket.getE03PVMODM() + "/"+ ticket.getE03PVMODY())%> 
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Sucursal :</td>
             <td nowrap align="left" width="30%">
                <%=ticket.getE03PVMBRN()%> 
             </td>
             <td nowrap align="right" width="20%"> Ejecutivo :</td>
             <td nowrap align="left" width="30%">
				<%=Util.formatCell(ticket.getE03PVMOFC() + " - " + ticket.getE03DSCOFC())%> 
             </td>
         </tr>         


<%if(ticket.getE03LNTYPG().equals("T")){%> 

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Numero de Tarjeta :</div>
            </td>  
            <td nowrap width="30%">
                <%=ticket.getE03TCNUMT()%> 
            </td>
            <td nowrap width="20%"> 
              <div align="right"> </div>
            </td>  
            <td nowrap width="30%">
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
				<%=Util.formatCell(ticket.getE03LNPROD() + " - " + ticket.getE03DSPROD())%> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Direccion :</div>
            </td>  
            <td nowrap width="30%">
                <%=ticket.getE03TCCMLA()%> 
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">  
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Local :</div>
            </td>
            <td nowrap width="30%">
                <%=ticket.getE03TCCUMB()%> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Extranjera :</div>
            </td>
            <td nowrap width="30%">
                <%=ticket.getE03TCCUME()%> 
            </td>
          </tr>                  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Dia de Pago :</div>
            </td>
            <td nowrap width="30%">             
				 <% if (ticket.getE03TCDYPG().equals("3")) out.print(" 3");
					if (ticket.getE03TCDYPG().equals("23")) out.print("23");%>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Porcentaje de Pago :</div>
            </td>
            <td nowrap width="30%">            
				 <% if (ticket.getE03TCPRPG().equals("5")) out.print("El 5%");
					if (ticket.getE03TCPRPG().equals("100")) out.print("El 100%");%>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">
				 <% if (ticket.getE03TCMEPG().equals("1")) out.print("Con Cuenta");
					if (ticket.getE03TCMEPG().equals("2")) out.print("Sin Cuenta");%>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
                <%=ticket.getE03TCACPG()%> 
            </td>
          </tr>  

<%} else {%> 

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Numero del Prestamo :</div>
            </td>  
            <td nowrap width="30%"> <%=ticket.getField("E03LNNUMP").getString().trim()%> </td>
            <td nowrap width="20%"> 
              <div align="right"> </div>
            </td>  
            <td nowrap width="30%">
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Tipo de Producto :</div>
            </td>
            <td nowrap width="30%" >
				<%=Util.formatCell(ticket.getE03LNTYPE() + " - " + ticket.getE03DSTYPE())%> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Termino del Contrato :</div>
            </td>
            <td nowrap width="30%">
                <%=ticket.getE03LNTERM()%> 
				 <% if (ticket.getE03LNTERC().equals("D")) out.print("D&iacute;a(s)");
					if (ticket.getE03LNTERC().equals("M")) out.print("Mes(es)");
					if (ticket.getE03LNTERC().equals("Y")) out.print("A&ntilde;o(s)");%>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
				<%=Util.formatCell(ticket.getE03LNPROD() + " - " + ticket.getE03DSPROD())%> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Monto del prestamo :</div>
            </td>
            <td nowrap width="30%">
                <%=ticket.getE03LNOAMT()%> 
            </td>
          </tr>                 
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Primer Pago :</div>
            </td>
            <td nowrap width="30%">
				<%=Util.formatCell(ticket.getE03LNPXPD() + "/" + ticket.getE03LNPXPM() + "/"+ ticket.getE03LNPXPY())%> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tasa de Interes :</div>
            </td>
            <td nowrap width="30%">
                <%=ticket.getE03LNRATE()%> 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Convenio :</div>
            </td>
            <td nowrap width="30%">
                <%=ticket.getE03LNCONV()%> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Valor de la Cuota :</div>
            </td>
            <td nowrap width="30%">
                <%=ticket.getE03LNCUAM()%> 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">             
				 <% if (ticket.getE03LNTYPG().equals("")) out.print("Caja");
				 	if (ticket.getE03LNTYPG().equals("1")) out.print("PAC");
					if (ticket.getE03LNTYPG().equals("2")) out.print("Convenio");
					if (ticket.getE03LNTYPG().equals("4")) out.print("PAC/Multibanco");%>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
                <%=ticket.getE03LNPACC()%> 
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>

  <h4>Distribucion del Prestamo </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
		    <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		      <td nowrap width="15%" align="center" ></td>
		      <td nowrap width="10%" align="center" >Referencia</td>
		      <td nowrap width="30%" align="center" >Descripcion</td>
		      <td nowrap width="20%" align="right" >Monto</td>
		      <td nowrap width="25%" align="center" ></td>
		    </tr>
          <%
           for (int i=1;i<30;i++) {
            if (i<10) name="0"+i; else name=""+i;
		    if(!ticket.getField("E03PDSC"+name).getString().trim().equals("")){
		 %> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			<td nowrap width="15%" align="Left"></td>
			<td nowrap width="10%" align="center"><%=ticket.getField("E03PREF"+name).getString().trim()%></td>
			<td nowrap width="30%" align="Left"><%=ticket.getField("E03PDSC"+name).getString().trim()%></td>
			<td nowrap width="20%"align="right"><%=ticket.getField("E03PAMT"+name).getString().trim()%></td>
			<td nowrap width="25%" align="Left"></td>
          </tr>
          <%
		   	}
		   }
		  %> 
        </table>
      </td>
    </tr>
  </table>

  <h4>Desembolso del Monto Liquido</h4>
  <table id="mainTable" class="tableinfo">
  <tr>
   <td>
	<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead" id="headTable" >
    <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
      <td nowrap width="40%" align="center" >Concepto</td>
      <td nowrap width="5%" align="center" >Banco </td>
      <td nowrap width="5%" align="center" >Sucursal</td>
      <td nowrap width="5%" align="center" >Moneda</td>
      <td nowrap width="15%" align="right" >Referencia</td>
      <td nowrap width="15%" align="right" >Monto</td>
    </tr>
    </table> 
      
    <div id="dataDiv" style="height:60; overflow-y :scroll; z-index:0" >
     <table id="dataTable" cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead"> 
          <%
		   int amount = 9;
			for ( int i=1; i<=amount; i++ ) {
			  name = i + "";
 		    if(!ticket.getField("E03OFFCO"+name).getString().trim().equals("")){
   			%> 
	    <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td width="40%" align="center"> 
				<%=ticket.getField("E03OFFOP"+name).getString().trim()%>
				<%=ticket.getField("E03OFFCO"+name).getString().trim()%>
              </td>
				<td width="5%" align="center"><%=ticket.getField("E03OFFBK"+name).getString().trim()%></td>
				<td width="5%"align="center" ><%=ticket.getField("E03OFFBR"+name).getString().trim()%></td>
				<td width="5%" align="center"><%=ticket.getField("E03OFFCY"+name).getString().trim()%></td>
				<td width="15%" align="right"><%=ticket.getField("E03OFFAC"+name).getString().trim()%></td>
				<td width="15%" align="right"><%=ticket.getField("E03OFFAM"+name).getString().trim()%></td>
          </tr>
          <%
    		}
    		}
    		%> 
    	  </table>
        </div>
        
     <table id="footTable" > 
	    <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap  align="right"><b>Equivalente Moneda Credito :</b> 
            </td>
            <td nowrap align="center"><b><i><strong> 
                <%=ticket.getE03OFFEQV()%> 
                </strong></i></b>
            </td>
          </tr>
        </table>
        </td>
        </tr>
</table>  


 <h4>Cheques a Imprimir </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
		    <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		      <td nowrap width="15%" align="center" ></td>
		      <td nowrap width="10%" align="center" >Ref/Cheque</td>
		      <td nowrap width="30%" align="center" >Nombre</td>
		      <td nowrap width="20%" align="right" >Monto</td>
		      <td nowrap width="25%" align="center" ></td>
		    </tr>
          <%
           for (int i=1;i<15;i++) {
            if (i<10) name="0"+i; else name=""+i;
		    if(!ticket.getField("E03CNME"+name).getString().trim().equals("")){
		 %> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			<td nowrap width="15%" align="Left"></td>
			<td nowrap width="10%" align="center"><%=ticket.getField("E03CHEQ"+name).getString().trim()%></td>
			<td nowrap width="30%" align="Left"><%=ticket.getField("E03CNME"+name).getString().trim()%></td>
			<td nowrap width="20%"align="right"><%=ticket.getField("E03CAMT"+name).getString().trim()%></td>
			<td nowrap width="25%" align="Left"></td>
          </tr>
          <%
		   	}
		   }
		  %> 
        </table>
      </td>
    </tr>
  </table>

<%if(ticket.getE03SELFCK().equals("Y")){%>   
  <h4>Emision de Cheques </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Sucursal :</div>
            </td>
            <td nowrap  width="30%"> 
                <%=ticket.getE03SELBRN()%> 
           </td>
            <td nowrap width="20%" > 
              <div align="right">Cheque del Banco :</div>
            </td>
            <td nowrap  width="30%"> 
              <input type="radio" name="E03SELBSL" value="1" <%if (!ticket.getE03SELBSL().equals("2")) out.print("checked"); %>>              
              <font id="bco1"><%= ticket.getE03SELBC1() + "-" + ticket.getE03SELBD1().toUpperCase()%></font><br>
              <input type="radio" name="E03SELBSL" value="2" <%if (ticket.getE03SELBSL().equals("2")) out.print("checked"); %>>             
			  <font id="bco2"><%= ticket.getE03SELBC2() + "-" + ticket.getE03SELBD2().toUpperCase()%></font>              
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>
<% } %>   

<%} %>   

</form>

</body>
</html>
