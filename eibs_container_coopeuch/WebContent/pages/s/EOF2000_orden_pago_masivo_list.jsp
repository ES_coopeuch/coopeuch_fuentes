<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EOF200001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>

<%@page import="java.math.BigDecimal"%><html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EOF200001List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script type="text/javascript">

function showInq(num) {
	var page = webapp + "/servlet/datapro.eibs.products.JSEOF2000?SCREEN=800&ORDEN=" + num;
	CenterWindow(page,600,500,2);
}

function goAction(op) {
	document.forms[0].SCREEN.value = op;
   	if (op == "600") { // Borrar
   		if (confirm("!ATENCION, Ud. va a borrar la carga completa, esta seguro?")) {
			document.forms[0].submit();		
  		}
   	} else if (op == "700") { // Validar
   		if (confirm("!ATENCION, Se enviara a Validacion al Batch, revise resultado en reporte OF1410")) {
			document.forms[0].submit();		
   		}
   	} else if (op == "900") { // Aplicar
   		if (confirm("!ATENCION, Se enviara a Aplicacion al Batch, revise resultado en reporte OF1420")) {
			document.forms[0].submit();		
   		}
   	}
}

</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Orden de Pago Masivo <br> <% if (userPO.getPurpose().equals("CARGADOS")) out.print("Cargadas"); 
                                                else if (userPO.getPurpose().equals("VALIDADOS")) out.print("Aprobadas"); 
                                                else if (userPO.getPurpose().equals("RECHAZADOS")) out.print("Rechazadas");                                                
                                                else out.print("Aplicadas"); %>
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="orden_pago_masivo_list.jsp,EOF2000"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEOF2000">
<input type="hidden" name="SCREEN" value="201"> 


<%
	if (EOF200001List.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>
<% if(userPO.getPurpose().equals("CARGADOS")) { %>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('700')"> <b>Validar</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('600')"> <b>Borrar<br>Carga<br>Completa</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>
<%	} else if(userPO.getPurpose().equals("VALIDADOS")) { %>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('900')"> <b>Aplicar</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>
<%	} else  { %>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="100%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>

<%	} %>
 <br>
	<table  id="mainTable" class="tableinfo" ALIGN=CENTER style="width:'100%'" height="70%">
    	<tr height="5%"> 
    		<TD NOWRAP width="100%" >
  				<TABLE id="headTable" width="100%" >
  					<TR id="trdark">  
						<th align="center" nowrap width="5%">Numero</th>
						<th align="center" nowrap width="5%">Oficina</th>
						<th align="center" nowrap width="15%">Unidad <br>Generadora</th>	
						<th align="center" nowrap width="5%">Fecha <br> Ingreso</th>
						<th align="center" nowrap width="5%">Form</th>	
						<th align="center" nowrap width="10%">Rut<br> Tomador</th>	
						<th align="center" nowrap width="10%">Rut<br> Beneficiario</th>		
						<th align="center" nowrap width="15%">Nombre <br>Beneficiafio</th>	
						<th align="center" nowrap width="10%">Monto</th>
						<th align="center" nowrap width="5%">Motivo</th>	
						<th align="center" nowrap width="5%">Estado</th>																																
          			</TR>
       			</TABLE>
      		</td>
    	</tr>
		<tr height="95%">    
			<td NOWRAP width="100%">       
  
   			    <div id="dataDiv1" class="scbarcolor" style="width:100%; height:100%; overflow:auto;">
    				<table id="dataTable" width="100%" > 
		<%
			EOF200001List.initRow();
        	BigDecimal  monto = new BigDecimal("0"); 
        	int cantidad = 0;			
			while (EOF200001List.getNextRow()) {
				EOF200001Message opmst = (EOF200001Message) EOF200001List.getRecord();
		%>
		<tr>
			<td nowrap align="center" width="5%"><a href="javascript:showInq(<%= EOF200001List.getCurrentRow() %>)"><%=opmst.getE01OPMNUM()%></a></td>			
			<td nowrap align="center" width="5%"><a href="javascript:showInq(<%= EOF200001List.getCurrentRow() %>)"><%=opmst.getE01OPMBRN()%></a></td>																		   																		   
			<td nowrap align="left" width="15%"><a href="javascript:showInq(<%= EOF200001List.getCurrentRow() %>)"><%=opmst.getE01OPMGLO()%></a></td>
			<td nowrap align="center" width="5%"><%=opmst.getE01OPMFCU()%></td>
			<td nowrap align="center" width="5%"><%=opmst.getE01OPMCDF()%></td>
			<td nowrap align="center" width="10%"><%=opmst.getE01OPMRTO()%></td>
			<td nowrap align="center" width="10%"><%=opmst.getE01OPMRBE()%></td>	
			<td nowrap align="left" width="15%"><%=opmst.getE01OPMNBE()%></td>	
			<td nowrap align="right" width="10%"><%=opmst.getE01OPMAMT()%></td>	
			<td nowrap align="center" width="5%"><%=opmst.getE01OPMMOT()%></td>		
			<td nowrap align="center" width="5%"><% if(opmst.getE01OPMSTS().equals("")) out.print(" ");
              				else if(opmst.getE01OPMSTS().equals("A")) out.print("Aprob.");
              				else if(opmst.getE01OPMSTS().equals("R")) out.print("Rech.");
              				else if(opmst.getE01OPMSTS().equals("P")) out.print("Proc.");              				
							else out.print("");%></td>
																																						
		</tr>
		<%
		    	monto = monto.add(opmst.getBigDecimalE01OPMAMT()); 
		    	cantidad++; 		    
			}
		%>
	</table>
	</div>
	</td>
	</tr>
	</table>
  <table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			class="tbhead">
			<tr id="trdark">
				<td nowrap width="25%">
				<div align="right"><b>Total de Ordenes  :</b></div>
				</td>
				<td nowrap width="25%">
				<div align="left"><input type="text" 
					size="10" maxlength="9" value="<%=cantidad%>" readonly>
				</div>
				</td>
				<td nowrap width="25%">
				<div align="right"><b>Total en Monto :</b></div>
				</td>
				<td nowrap width="25%">
				<div align="left"><input type="text" 
					size="20" maxlength="20" value="<%=Util.formatCCY(monto.toString())%>" readonly>
				</div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
	  
    				 
  <SCRIPT language="JavaScript">
		
			function resizeDoc() {
      		 	divResize();
     		    adjustEquTables(headTable, dataTable, dataDiv1,1,false);
      		}
	 		resizeDoc();   			
     		window.onresize=resizeDoc;        
     </SCRIPT>
  
     


<%}%>

  </form>

</body>
</html>
