/**
 * ResultsDetalleInfraccionesLab.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResultsDetalleInfraccionesLab  implements java.io.Serializable {
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.DetalleInfraccionesLab[] detalleInfraccionesLab;

    public ResultsDetalleInfraccionesLab() {
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.DetalleInfraccionesLab[] getDetalleInfraccionesLab() {
        return detalleInfraccionesLab;
    }

    public void setDetalleInfraccionesLab(cl.Sinacofi.coopeuch.creditos.predictorriesgo.DetalleInfraccionesLab[] detalleInfraccionesLab) {
        this.detalleInfraccionesLab = detalleInfraccionesLab;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.DetalleInfraccionesLab getDetalleInfraccionesLab(int i) {
        return detalleInfraccionesLab[i];
    }

    public void setDetalleInfraccionesLab(int i, cl.Sinacofi.coopeuch.creditos.predictorriesgo.DetalleInfraccionesLab value) {
        this.detalleInfraccionesLab[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsDetalleInfraccionesLab history = (ResultsDetalleInfraccionesLab) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ResultsDetalleInfraccionesLab other = (ResultsDetalleInfraccionesLab) obj;
        boolean _equals;
        _equals = true
            && ((this.detalleInfraccionesLab==null && other.getDetalleInfraccionesLab()==null) || 
             (this.detalleInfraccionesLab!=null &&
              java.util.Arrays.equals(this.detalleInfraccionesLab, other.getDetalleInfraccionesLab())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsDetalleInfraccionesLab history = (ResultsDetalleInfraccionesLab) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getDetalleInfraccionesLab() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetalleInfraccionesLab());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetalleInfraccionesLab(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
