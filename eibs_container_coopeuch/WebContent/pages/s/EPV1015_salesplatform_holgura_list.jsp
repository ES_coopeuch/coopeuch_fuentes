<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV101501Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV101501List" class="datapro.eibs.beans.JBObjList"
	scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript"> 

 function goAction(op) {

	var ok = false;
	var cun = "";
	var pg = "";

	if (op != '200'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "PVMNUM") 
	      	{	
	      		if (element.checked == true) {
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {      
		document.forms[0].SCREEN.value = op;
		document.forms[0].submit();
     } else {
		alert("Debe seleccionar una Solicitud para continuar.");	   
	 }
      
	}

function showInqPV(num) {
	page = webapp + "/servlet/datapro.eibs.salesplatform.JSEPV1015?SCREEN=3&E01PVMNUM=" + num;
	CenterWindow(page,800,500,2);
}
	


</script>

</head>

<body>
<% 


 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center"> Plataforma de Ventas - Aprobacion Holgura<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_holgura_list.jsp,JSEPV1015"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1015">
<input type="hidden" name="SCREEN" value="">
 
  <table  class="tbenter" width="100%">
  	<tr>
  		<td width="97%">
			<table class="tbenter" width="100%">
				<tr>	
					<td align="center" class="tdbkg" width="15%">
					<a href="javascript:goAction('2')"> <b>Aprobar</b> </a>
					</td>
					<td align="center" class="tdbkg" width="15%">
					<a href="javascript:goAction('4')"> <b>Rechazar</b> </a>
					</td>
					<td align="center" class="tdbkg" width="15%">
						<a	href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
					</td>
				</tr>
			</table>  		
  		</td>  		
  	</tr>
  </table>
  


<%
	if (EPV101501List.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=50%>
	<TR>
		<TD>
		<div align="center">
		<font size="3"><b> 
			No hay resultados que correspondan a su criterio de b�squeda. 
		</b></font></div>
		</TD>
	</TR>
</TABLE>
<%
	} else {
%>

	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="5%">&nbsp;</th>
			<th align="center" nowrap width="10%">Solicitud</th>
			<th align="center" nowrap width="20%">Cliente</th>
			<th align="center" nowrap width="5%">Sucursal</th>		
			<th align="center" nowrap width="15%">Vendedor</th>	
			<th align="center" nowrap width="15%">Tipo de Producto</th>										
			<th align="center" nowrap width="10%">Fecha<br> Holgura</th>			
			<th align="center" nowrap width="10%">Monto<br> Holgura</th>
			<th align="center" nowrap width="10%">Monto<br> Negociado</th>			
		</tr>
		<%
			EPV101501List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EPV101501List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EPV101501Message convObj = (EPV101501Message) EPV101501List.getRecord();
		%>
		<tr>
			<td nowrap>
			<input type="radio" name="PVMNUM" value="<%=EPV101501List.getCurrentRow()%>" <%=chk%>>              
             </td>				
			<td nowrap align="center"><a href="javascript:showInqPV('<%= EPV101501List.getCurrentRow()%>');"><%= Util.formatCell(convObj.getE01PVMNUM())%></a></td>
			<td nowrap align="left"><a href="javascript:showInqPV('<%= EPV101501List.getCurrentRow()%>');"><%=convObj.getE01PVMSHN()%></a></td>
			<td nowrap align="center"><a href="javascript:showInqPV('<%= EPV101501List.getCurrentRow()%>');"><%=convObj.getE01PVMBRS()%></a></td>	
			<td nowrap align="center"><a href="javascript:showInqPV('<%= EPV101501List.getCurrentRow()%>');"><%=convObj.getE01PVMVCD()%> - <%=convObj.getE01PVMVCN()%></a></td>
			<td nowrap align="center"><a href="javascript:showInqPV('<%= EPV101501List.getCurrentRow()%>');"><%=convObj.getE01PVMTPR()%> - <%=convObj.getE01TPRDES()%></a></td>						
			<td nowrap align="center"><a href="javascript:showInqPV('<%= EPV101501List.getCurrentRow()%>');"><%=Util.formatCell(convObj.getE01PVMOLD() + "/" + convObj.getE01PVMOLM() + "/"+ convObj.getE01PVMOLY())%></a></td>
			<td nowrap align="right"><a href="javascript:showInqPV('<%= EPV101501List.getCurrentRow()%>');"><%=convObj.getE01PVMOLA()%></a></td>
			<td nowrap align="right"><a href="javascript:showInqPV('<%= EPV101501List.getCurrentRow()%>');"><%=convObj.getE01PVDOAM()%></a></td>

		</tr>
		<%
			}
		%>
	</table>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV101501List.getShowPrev()) {
					int pos = EPV101501List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1015?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV101501List.getShowNext()) {
					int pos = EPV101501List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1005?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>
</form>

</body>
</html>
