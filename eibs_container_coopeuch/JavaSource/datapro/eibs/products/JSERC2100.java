// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 12-02-2015 17:05:12
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   JSERC2100.java

package datapro.eibs.products;

import com.datapro.generics.Util;
import datapro.eibs.beans.*;
import datapro.eibs.master.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class JSERC2100 extends JSEIBSServlet {

	public JSERC2100() {
	}

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case 100: // 'd'
			procReqStatement(user, req, res, session);
			break;

		case 200:
			procActionStatementList(user, req, res, session);
			break;

		case 300:
			procActionStatementCrear(user, req, res, session);
			break;

		case 500:
			procActionStatementDetail(user, req, res, session);
			break;

		case 400:
			procSeleccionMov(user, req, res, session, "NEW");
			break;

		case 630:
			procSeleccionMov(user, req, res, session, "MAINTENANCE");
			break;

		case 700:
			procActionNEW(user, req, res, session);
			break;

		case 800:
			procActionNEW(user, req, res, session);
			break;

		case 750:
			procActionDelete(user, req, res, session);
			break;

		case 202:
			procActionStatementEliminar(user, req, res, session);
			break;

		case 600:
			procActionStatement_Aprove(user, req, res, session);
			break;

		case 1000:
			procActionStatementList(user, req, res, session);
			break;

		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	protected void procReqStatement(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		UserPos userPO = (UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		try {
			flexLog("About to call Page: ERC2100_statement_enter.jsp");
			forward("ERC2100_statement_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e)
					.toString());
		}
	}

	protected void procActionStatementList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2000", req);
			ERC210001Message msgList = (ERC210001Message) mp.getMessageRecord(
					"ERC210001", user.getH01USR(), "0015");
			if (req.getParameter("E01BRMEID") != null) {
				String codigo_banco = req.getParameter("E01BRMEID");
				msgList.setE01WCHRBK(codigo_banco);
			}
			if (req.getAttribute("E01BRMEID") != null) {
				String codigo_banco = (String) req.getAttribute("E01BRMEID");
				msgList.setE01WCHRBK(codigo_banco);
			}
			if (req.getParameter("E01BRMCTA") != null) {
				String cuenta_banco = req.getParameter("E01BRMCTA");
				msgList.setE01WCHCTA(cuenta_banco);
			}
			if (req.getAttribute("E01BRMCTA") != null) {
				String cuenta_banco = (String) req.getAttribute("E01BRMCTA");
				msgList.setE01WCHCTA(cuenta_banco);
			}
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC2100_statement_enter.jsp");
				forward("ERC2100_statement_enter.jsp", req, res);
			} else {
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
				session.setAttribute("ERClist", list);
				if (req.getParameter("E01BRMEID") != null) {
					session.setAttribute("codigo_banco", req
							.getParameter("E01BRMEID"));
					session.setAttribute("cuenta_banco", req
							.getParameter("E01BRMCTA"));
					session.setAttribute("nombre_banco", req
							.getParameter("E01DSCRBK"));
					session.setAttribute("cuenta_ibs", req
							.getParameter("E01BRMACC"));
				}
				forwardOnSuccess("ERC2100_statement_list.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionStatementCrear(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2100", req);
			ERC210001Message msgList = (ERC210001Message) mp.getMessageRecord(
					"ERC210001", user.getH01USR(), "0001");
			String codigo_banco = (String) session.getAttribute("codigo_banco");
			String cuenta_banco = (String) session.getAttribute("cuenta_banco");
			String nombre_banco = (String) session.getAttribute("nombre_banco");
			String cuenta_ibs = (String) session.getAttribute("cuenta_ibs");
			msgList.setE01WCHRBK(codigo_banco);
			msgList.setE01WCHCTA(cuenta_banco);
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC2000_statement_list.jsp");
				forward("ERC2000_statement_list.jsp", req, res);
			} else {
				req.setAttribute("E01BRMEID", codigo_banco);
				req.setAttribute("E01BRMCTA", cuenta_banco);
				procActionStatementList(user, req, res, session);
			}
		} finally {
			if (mp != null)
				mp.close();
		}

	}

	protected void procActionStatementEliminar(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		mp = getMessageProcessor("ERC2100", req);
		try {
			ERC210001Message msgList = (ERC210001Message) mp.getMessageRecord(
					"ERC210001", user.getH01USR(), "0009");
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC210001Message listMessage = (ERC210001Message) list
						.get(index);
				msgList.setE01WCHRBK(listMessage.getE01WCHRBK());
				msgList.setE01WCHCTA(listMessage.getE01WCHCTA());
				msgList.setE01WCHSTN(listMessage.getE01WCHSTN());
				req.setAttribute("E01BRMEID", listMessage.getE01WCHRBK());
				req.setAttribute("E01BRMCTA", listMessage.getE01WCHCTA());
			}
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC2000_statement_list.jsp");
				forward("ERC2100_statement_list.jsp", req, res);
			} else {
				procActionStatementList(user, req, res, session);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionStatement_Aprove(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2100", req);
			ERC210001Message msgList = (ERC210001Message) mp.getMessageRecord(
					"ERC210001", user.getH01USR(), "0002");
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC210001Message listMessage = (ERC210001Message) list
						.get(index);
				msgList.setE01WCHRBK(listMessage.getE01WCHRBK());
				msgList.setE01WCHCTA(listMessage.getE01WCHCTA());
				msgList.setE01WCHSTN(listMessage.getE01WCHSTN());
				req.setAttribute("E01BRMEID", listMessage.getE01WCHRBK());
				req.setAttribute("E01BRMCTA", listMessage.getE01WCHCTA());
			}
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC2000_statement_list.jsp");
				forward("ERC2100_statement_list.jsp", req, res);
			} else {
				procActionStatementList(user, req, res, session);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionStatementDetail(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2100", req);
			ERC210001Message msgList = (ERC210001Message) mp.getMessageRecord(
					"ERC210001", user.getH01USR(), "0004");
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC210001Message listMessage = (ERC210001Message) list
						.get(index);
				msgList.setE01WCHRBK(listMessage.getE01WCHRBK());
				msgList.setE01WCHCTA(listMessage.getE01WCHCTA());
				msgList.setE01WCHSTN(listMessage.getE01WCHSTN());
				session.setAttribute("ERCSeleccion", listMessage);
			}
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC2000_statement_list.jsp");
				forward("ERC2100_statement_list.jsp", req, res);
			} else {
				JBObjList list = mp.receiveMessageRecordList("H02FLGMAS");
				session.setAttribute("ERC", list);
				forwardOnSuccess("ERC2100_statement_maintance_list.jsp", req,
						res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionNEW(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		UserPos userPO;
		MessageProcessor mp;
		userPO = (UserPos) session.getAttribute("userPO");
		mp = null;
		try {
			String codigo_banco = (String) session.getAttribute("codigo_banco");
			String cuenta_banco = (String) session.getAttribute("cuenta_banco");
			String nombre_banco = (String) session.getAttribute("nombre_banco");
			String cuenta_ibs = (String) session.getAttribute("cuenta_ibs");
			mp = getMessageProcessor("ERC2100", req);
			ERC210002Message msg = (ERC210002Message) mp
					.getMessageRecord("ERC210002");
			msg.setH02USERID(user.getH01USR());
			msg.setH02OPECOD("0020");
			msg.setH02TIMSYS(getTimeStamp());
			msg.setE02WCSRBK(codigo_banco);
			msg.setE02WCSCTA(cuenta_banco);
			msg.setH02SCRCOD("01");
			setMessageRecord(req, msg);
			mp.sendMessage(msg);
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ERC210002Message) mp.receiveMessageRecord();
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);
			if (!mp.hasError(msgError)) {
				procActionStatementDetail_Aux(user, req, res, session);
			} else {
				session.setAttribute("error", msgError);
				forward("ERC2100_statement_movement.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procSeleccionMov(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {
		MessageProcessor mp;
		UserPos userPO;
		mp = null;
		userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2100", req);
			userPO.setPurpose(option);
			if (option.equals("NEW")) {
				session.removeAttribute("cnvObj");
				forward("ERC2100_statement_movement.jsp", req, res);
			}
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERC");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC210002Message listMessage = (ERC210002Message) list
						.get(index);
				if (option.equals("MAINTENANCE")) {
					session.setAttribute("userPO", userPO);
					session.setAttribute("cnvObj", listMessage);
					forward("ERC2100_statement_movement.jsp", req, res);
				}
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionStatementDetail_Aux(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2100", req);
			ERC210001Message msgList = (ERC210001Message) mp.getMessageRecord(
					"ERC210001", user.getH01USR(), "0004");
			if (req.getParameter("codigo_banco") != null)
				msgList.setE01WCHRBK(req.getParameter("codigo_banco"));
			if (req.getAttribute("codigo_banco") != null)
				msgList.setE01WCHRBK((String) req.getAttribute("codigo_banco"));
			if (req.getParameter("E01BRMCTA") != null)
				msgList.setE01WCHCTA(req.getParameter("E01BRMCTA"));
			if (req.getAttribute("numero_banco") != null)
				msgList.setE01WCHCTA((String) req.getAttribute("numero_banco"));
			if (req.getParameter("E02WCSSTN") != null)
				msgList.setE01WCHSTN(req.getParameter("E02WCSSTN"));
			if (req.getAttribute("cartola") != null)
				msgList.setE01WCHSTN((String) req.getAttribute("cartola"));
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC2000_statement_list.jsp");
				forward("ERC2100_statement_list.jsp", req, res);
			} else {
				JBObjList list = mp.receiveMessageRecordList("H02FLGMAS");
				session.setAttribute("ERC", list);
				forwardOnSuccess("ERC2100_statement_maintance_list.jsp", req,
						res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionDelete(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		UserPos userPO;
		MessageProcessor mp;
		userPO = (UserPos) session.getAttribute("userPO");
		mp = null;
		try {
			mp = getMessageProcessor("ERC2100", req);
			ERC210002Message msg = (ERC210002Message) mp
					.getMessageRecord("ERC210002");
			msg.setH02USERID(user.getH01USR());
			msg.setH02OPECOD("0009");
			msg.setH02TIMSYS(getTimeStamp());
			msg.setH02FLGWK3("D");
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERC");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC210002Message listMessage = (ERC210002Message) list
						.get(index);
				msg.setE02WCSUID(listMessage.getE02WCSUID());
				req.setAttribute("codigo_banco", listMessage.getE02WCSRBK());
				req.setAttribute("numero_banco", listMessage.getE02WCSCTA());
				req.setAttribute("cartola", listMessage.getE02WCSSTN());
				msg.setE02WCSRBK(listMessage.getE02WCSRBK());
				msg.setE02WCSCTA(listMessage.getE02WCSCTA());
				msg.setE02WCSSTN(listMessage.getE02WCSSTN());
			}
			mp.sendMessage(msg);
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ERC210002Message) mp.receiveMessageRecord();
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);
			if (!mp.hasError(msgError)) {
				procActionStatementDetail_Aux(user, req, res, session);
			} else {
				session.setAttribute("error", msgError);
				forward("ERC2100_statement_maintance_list.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	private static final long serialVersionUID = 1L;
	protected static final int A_enter_statement = 100;
	protected static final int A_list_statement = 200;
	protected static final int A_new_statement = 300;
	protected static final int A_TRANSACTION_LIST = 500;
	protected static final int N_statement_delete = 202;
	protected static final int N_statement_aprove = 600;
	protected static final int A_movement_new = 400;
	protected static final int N_movement_new = 800;
	protected static final int N_movement_delete = 750;
	protected static final int A_movement_update = 630;
	protected static final int N_movement_update = 700;
	protected static final int A_movemement_list = 1000;
}
