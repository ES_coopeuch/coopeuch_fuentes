package com.datapro.eibs.security.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.UserNotFoundException;
import com.datapro.generic.access.DAOSuper;
import com.datapro.eibs.security.bean.CNTRLBTH;
import com.datapro.generic.beanutil.*;
import datapro.eibs.beans.JBObjList;

public class CNTRLBTHDao extends DAOSuper {


    private static Category cat =
		Category.getInstance(CNTRLBTHDao.class.getName());
			
	/**
	 * CNTRLBTHDao constructor comment.
	 */

	public CNTRLBTHDao(String DBID) {
		super(DBID);
	}
	
	/**
	 * Constructor for DAOUserEibs.
	 */
	public CNTRLBTHDao() {
		super();
	}

	/**
	 * Constructor for DAOUserEibs.
	 * @param connection
	 */
	public CNTRLBTHDao(Connection connection) {
		super(connection);
	}
	
	public JBObjList selectGroup() throws DAOException {
				
		
    	JBObjList beanList = new JBObjList();
    	
		try {
			
			String sql = "SELECT * FROM CNTRLBTH WHERE BTHGRP='GRPPRGGRPPRG' ORDER BY BTHUSR";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
						
			while (true) {
				if (!rs.next ())
				 break;
				 
				ResultSetMetaData md = rs.getMetaData();				
				CNTRLBTH bean = new CNTRLBTH ();
				BeanParser bn = new BeanParser(bean);
				if (bn.parseResultSet(md,rs)) beanList.addRow(bean);
			}
			rs.close();
			ps.close();
			if (beanList.getNoResult()) beanList=null;
			

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}
		return beanList;
	}
	
	public JBObjList selectByGroup(String GROUPID) throws DAOException {
		
		JBObjList list = null;

		try {
			String sql = "SELECT * FROM CNTRLBTH WHERE BTHGRP=? ORDER BY BTHUSR";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, GROUPID.toUpperCase());
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new JBObjList();
			while (rs.next()) {
				CNTRLBTH bean = new CNTRLBTH();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;
				
	}
	
	public CNTRLBTH select (String USERID) throws UserNotFoundException,DAOException {
		
		CNTRLBTH bean = null;
		
		try {
			String sql = "SELECT * FROM CNTRLBTH WHERE BTHUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, USERID.toUpperCase());
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNTRLBTH();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} 
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return bean;
	}

	public int update (CNTRLBTH bean) throws DAOException {
		
		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.updatePrepareStatement() + " WHERE BTHUSR = '"+bean.getBTHKEY().toUpperCase()+"'";
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			//ps.setString(bs.getNextParamIndex(), bean.getBTHUSR());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
		
		
	}
	
	public int updateByGroup (String GROUPID) throws DAOException {
		
		int rt;

		try {
			String sql = "UPDATE CNTRLBTH SET BTHGRP='' WHERE BTHGRP=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, GROUPID.toUpperCase());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
		
	}
	
	public int insert (CNTRLBTH bean) throws DAOException {
		
		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.insertPrepareStatement();
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			rt = ps.executeUpdate();
			ps.close();
			//cnx.close();
			
		} catch (Exception e) {
			cat.error(e);			
			throw new DAOException(e.toString());
		}

		return rt;		
		
	}

	public int delete (String uid) throws DAOException {
		int rt;

		try {
			String sql = "DELETE FROM CNTRLBTH WHERE BTHUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid.toUpperCase());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}
	
	public int deleteByGroup (String gid) throws DAOException {
		
		int rt;

		try {
			String sql = "DELETE FROM CNTRLBTH WHERE BTHGRP=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, gid.toUpperCase());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

}