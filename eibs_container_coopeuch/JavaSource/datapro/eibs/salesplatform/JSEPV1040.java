package datapro.eibs.salesplatform;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.coopeuch.core.tiendavirtual.ActualizaStockEntrada;
import cl.coopeuch.core.tiendavirtual.GrabaDetalleProductoEntrada;
import cl.coopeuch.core.tiendavirtual.GrabaProductoFinanciadoEntrada;
import cl.coopeuch.core.tiendavirtual.RespActualizaStock;
import cl.coopeuch.core.tiendavirtual.RespGrabaDetalleProducto;
import cl.coopeuch.core.tiendavirtual.RespGrabaProducto;
import cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.FaultMsg;
import cl.coopeuch.core.tiendavirtual.serviciotiendavirtual.TiendaVirtualSOAPProxy;


import datapro.eibs.beans.EPV101002Message;
import datapro.eibs.beans.EPV103801Message;
import datapro.eibs.beans.EPV104001Message;
import datapro.eibs.beans.EPV104002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV104003Message;
import datapro.eibs.beans.EPV117001Message;
import datapro.eibs.beans.EPV117003Message;
import datapro.eibs.beans.EPV117004Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSEPV1040 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_COVENANT_APPROVAL_LIST = 100;
	
	protected static final int R_APROVE = 200;	
	protected static final int R_REJECT = 300;
	protected static final int R_DELETE = 400;	
	protected static final int R_INQUIRY = 900;
	
	protected static final String APPROVE = "A";
	protected static final String REJECT = "R";
	protected static final String DELETE = "D";
	

	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		switch (screen) {
			case R_COVENANT_APPROVAL_LIST:
				procReqCovenantApprovalList(user, req, res, session);
				break;		
			case R_APROVE:
				procActionApproveReject(user, req, res, session, APPROVE);
				break;
			case R_REJECT:
				procActionApproveReject(user, req, res, session, REJECT);
				break;	
			case R_DELETE:
				procActionApproveReject(user, req, res, session, DELETE);
				break;	
			case R_INQUIRY :
				procReqPlatformLiquidacionInquiry(user, req, res, session);
				break;		
			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}

	/**
	 * procActionApproveReject
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @param option
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionApproveReject(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession session, String option)
			throws ServletException, IOException {
			
			UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
			
			MessageProcessor mp = null;
			
			try {
				
				mp = getMessageProcessor("EPV1040", req);
				JBObjList list = (JBObjList)session.getAttribute("EPV104001List");
				int index = req.getParameter("key")==null?0:req.getParameter("key").equals("")?0:Integer.parseInt(req.getParameter("key"));
				
				EPV104001Message listMessage = (EPV104001Message)list.get(index);

				EPV104002Message msg = (EPV104002Message) mp.getMessageRecord("EPV104002", user.getH01USR(), "0005");
				msg.setH02SCRCOD("01");
			 	msg.setE02ACTION(option);
			 	msg.setE02PVDNUM(listMessage.getE01PVDNUM());
			 	msg.setE02PVDCUN(listMessage.getE01PVDCUN());
			 	
			 	mp.sendMessage(msg);
			 	
			 	ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
	            
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				
				if (!mp.hasError(msgError)){
					if (option.equals(APPROVE)){
						// Llamando a tienda virtual
						grabarProductosTiendaVirtual(req,session,user,listMessage.getE01PVDNUM(),listMessage.getE01PVDCUN());
						// Presentacion de ticket para imprimir
						EPV104003Message ticket = (EPV104003Message) mp.receiveMessageRecord();
						session.setAttribute("ticket", ticket);
						forward("EPV1040_salesplatform_ticket.jsp", req, res);			
					} else {
						//Rechazo, si habian productos reservados de tienda virtual, quitar la reserva
						quitarReservaProductosTiendaVirtual(req,session,user,listMessage.getE01PVDNUM());						
						procReqCovenantApprovalList(user, req, res, session);
					}
				} else {
					session.setAttribute("convObj", msg);
					forward("EPV1040_salesplatform_curse_list.jsp", req, res);
				}
				
				

			} finally {
				if (mp != null)	mp.close();
			}
	    }	
	
	@SuppressWarnings("unchecked")
	private boolean grabarProductosTiendaVirtual(HttpServletRequest req, HttpSession session,
			ESS0030DSMessage user,String nroSol,String nroCli) throws ServletException, IOException{
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		boolean procesado=false;
		try {
			mp = getMessageProcessor("EPV1170", req);

			EPV117001Message msg = (EPV117001Message) mp.getMessageRecord("EPV117001", user.getH01USR(), "0024");//get data to save on service
			
			// Sets message with page fields
			msg.setE01PTVCUN(nroCli);
			msg.setE01PTVNUM(nroSol);

			//Sending message
			mp.sendMessage(msg);			

			
			//receiving article list
			JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");

			//receiving header article list 
			EPV117004Message header = (EPV117004Message) mp.receiveMessageRecord();
			
			if (!list.getNoResult()){//data exists
				TiendaVirtualSOAPProxy tvp = new TiendaVirtualSOAPProxy();	
				
				//save header
				GrabaProductoFinanciadoEntrada entrada = new GrabaProductoFinanciadoEntrada();
				entrada.setIdEvaluacion(Integer.valueOf(header.getE04PTVNUM()));								
				entrada.setMontoFinanciado(header.getBigDecimalE04VALCOM().toBigInteger().intValue());
				entrada.setNumProductos(Integer.valueOf(header.getE04CANART()));
				entrada.setDireccionEnvio(header.getE04PTVMA1()+ " "+ header.getE04PTVMA2() + " " + header.getE04PTVMA3() + " " + header.getE04PTVMA4());
				
				int idcomuna= (header.getE04PTVCCO()!=null && !"".equals(header.getE04PTVCCO()))?Integer.valueOf(header.getE04PTVCCO()):0;
				entrada.setIdComuna(idcomuna);
				int idRegion= (header.getE04PTVCRG()!=null && !"".equals(header.getE04PTVCRG()))?Integer.valueOf(header.getE04PTVCRG()):0;
				entrada.setIdRegion(idRegion);
				int codPostal= (header.getE04PTVZPC()!=null && !"".equals(header.getE04PTVZPC()))?Integer.valueOf(header.getE04PTVZPC()):0;				
				entrada.setCodigoPostal(codPostal);
				
				entrada.setTelefono(header.getE04PTVHPN());
				entrada.setCelular(header.getE04PTVPHN());
				entrada.setEmailContacto(header.getE04PTVIAD());
				entrada.setObservacion(header.getE04PTVRMK());
				String rut = header.getE04RUTEJE();
				rut = (rut.length()>0)?rut.substring(0,rut.length()-1):"0";
				entrada.setRutIngreso(Integer.valueOf(rut));
				String mes  = (header.getE04FECPRM().length()>1)?header.getE04FECPRM():"0"+header.getE04FECPRM();
				String dia  = (header.getE04FECPRD().length()>1)?header.getE04FECPRD():"0"+header.getE04FECPRD();
				
				entrada.setFeProceso(header.getE04FECPRY() +  mes + dia);
				
				RespGrabaProducto respH = tvp.grabaProductoFinanciado(entrada);
				if (respH.getCodRespuesta()==1){
		            ELEERRMessage msgError = new ELEERRMessage();
		            msgError.setERRNUM("1");
		            msgError.setERNU01("01");//4		                
		            msgError.setERDS01("Retorno Falla: Tienda Virtual - grabaProductoFinanciado");		//70
					session.setAttribute("error", msgError);						
					return procesado;//return fails
				}
				
				//save details
				list.initRow();	 
				
				ArrayList errs = new ArrayList();
				while (list.getNextRow()) {
					EPV117003Message prod = (EPV117003Message) list.getRecord();

						GrabaDetalleProductoEntrada ent = new GrabaDetalleProductoEntrada();
						ent.setCodCampana(Integer.valueOf(prod.getE03PTVCAM()));
						ent.setCodProducto(Integer.valueOf(prod.getE03PTVPRD()));
						ent.setIdEvaluacion(Integer.valueOf(prod.getE03PTVNUM()));
						ent.setValor(prod.getBigDecimalE03PTVPTO().toBigInteger().intValue());//Monto total del items
						String rutDet = prod.getE03RUTEJE();
						rutDet = (rutDet.length()>0)?rutDet.substring(0,rutDet.length()-1):"0";
						ent.setRutIngreso(Integer.valueOf(rutDet));
						
						String mesD  = (prod.getE03FECPRM().length()>1)?prod.getE03FECPRM():"0"+prod.getE03FECPRM();
						String diaD  = (prod.getE03FECPRD().length()>1)?prod.getE03FECPRD():"0"+prod.getE03FECPRD();	
						
						ent.setFeProceso(prod.getE03FECPRY()+  mesD +  diaD);
						
						//graba detalle
						RespGrabaDetalleProducto resp = tvp.grabaDetalleProductoFinanciado(ent);					
						if (resp.getRetorno()==1){//0=OK y 1=noOK
							String[] valores={"D",prod.getE03PTVCAM(),prod.getE03PTVPRD(),prod.getE03PTVCAN()};
							errs.add(valores);
						}//EoI		
						
						//actualiza stock
						ActualizaStockEntrada actSt = new ActualizaStockEntrada();
						actSt.setCodCampana(Integer.valueOf(prod.getE03PTVCAM())); 
						actSt.setCodProducto(Integer.valueOf(prod.getE03PTVPRD()));						
						actSt.setCantidad(Integer.valueOf(prod.getE03PTVCAN()));
						actSt.setTipoOperacion("A");//Se envia A : aprobar
						actSt.setIdEvaluacion(Integer.valueOf(prod.getE03PTVNUM()));
						String nroCred = (header.getE04PTVACC()!=null)?header.getE04PTVACC().trim():"0";
						nroCred = (nroCred.length()>10)?nroCred.substring(2,nroCred.length()):nroCred;
						actSt.setIdProducto(Integer.valueOf(nroCred));//mandar ID del credito generado																				
						RespActualizaStock respStock = tvp.actualizaStock(actSt);	
						if (respStock.getCodRespuesta()==1){//0=OK y 1=noOK
							String[] valores={"S",prod.getE03PTVCAM(),prod.getE03PTVPRD(),prod.getE03PTVCAN()};
							errs.add(valores);
						}//EoI							
					
				}//EoW
				//actualizar stock..
				if (errs.size()>0){
					String numErr = (errs.size()>10)?"10":String.valueOf(errs.size());
		            ELEERRMessage msgError = new ELEERRMessage();
		            msgError.setERRNUM(numErr);
		            int fil=1;
		            for (Iterator iterator = errs.iterator(); iterator.hasNext();) {
		            	String fila = (fil>9)?""+fil:"0"+fil;
		            	String[] val = (String[]) iterator.next();//38 :37 
		            	String des = ("D".equals(val[0]))?"Error Grabar Det. Prod. Tienda Virtual":"Error Actualizar Stock Tienda Virtual";
		            	des = des + " Camp:"+val[1]+" Prod:"+val[2]+" Cant:"+val[3];//18+
		            	msgError.getField("ERNU"+fila).setString(fila);//numero error	            	
		            	msgError.getField("ERDS"+fila).setString((des.length() >70?(des.substring(0,70)):des));//descripcion error
					}
		            session.setAttribute("error", msgError);
				}else{
					//existen productos a actualizar y los proceso bien.
					procesado=true;
				}
				
			}else{
				//no existen productos a actualizar y proceso bien.
				procesado=true;
			}
			  


		}
		catch (Exception e) {
			String className = e.getClass().getName();
			String description = e.getMessage() == null ? "Exception General" : e.getMessage();					
            ELEERRMessage msgError = new ELEERRMessage();				
			msgError.setERRNUM("3");
            msgError.setERNU01("01");//4
            msgError.setERDS01("ERROR AL ACCEDER SERVICIO DE TIENDA VIRTUAL. VERIFIQUE COMUNICACION.");
            msgError.setERNU02("02");//4
            msgError.setERDS02(className);
            msgError.setERNU03("03");//4
            msgError.setERDS03(description.length() > 70 ? description.substring(0, 70) : description);	
			session.setAttribute("error", msgError);						
				
		}		
		finally {
			if (mp != null)
				mp.close();
		}
		return procesado;		
	}
	
	private boolean quitarReservaProductosTiendaVirtual(HttpServletRequest req, HttpSession session,
			ESS0030DSMessage user,String nroSol) throws ServletException, IOException{
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		boolean procesado=false;
		try {
			mp = getMessageProcessor("EPV1170", req);

			EPV117001Message msg = (EPV117001Message) mp.getMessageRecord("EPV117001", user.getH01USR(), "0030");//todos los productos a rechazar

			// Sets message with page fields
			msg.setE01PTVCUN(userPO.getCusNum());
			msg.setE01PTVNUM(nroSol);

			//Sending message
			mp.sendMessage(msg);			

			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			if (!list.getNoResult()){//data exists 
				list.initRow();	 
				TiendaVirtualSOAPProxy tvp = new TiendaVirtualSOAPProxy();	
				ArrayList<String[]> err = new ArrayList<String[]>();
				while (list.getNextRow()) {
					EPV117001Message prod = (EPV117001Message) list.getRecord();
					if (!"".equals(prod.getE01PTVCAM()) && !"".equals(prod.getE01PTVPRD()) && !"".equals(prod.getE01TVIFLG())){
						//llenamos servicio tienda virtual...
						ActualizaStockEntrada ent = new ActualizaStockEntrada();
						ent.setCodCampana(Integer.valueOf(prod.getE01PTVCAM())); 
						ent.setCodProducto(Integer.valueOf(prod.getE01PTVPRD()));						
						ent.setCantidad(Integer.valueOf(prod.getE01PTVCAN()));
						ent.setTipoOperacion(prod.getE01TVIFLG());//Se envia R=rechazo reserva
						ent.setIdEvaluacion(Integer.valueOf(prod.getE01PTVNUM()));																							
						RespActualizaStock resp = tvp.actualizaStock(ent);					
						if (resp.getCodRespuesta()==1){//0=OK y 1=noOK
							String[] valores={prod.getE01PTVCAM(),prod.getE01PTVPRD(),prod.getE01PTVCAN(),prod.getE01TVIFLG()};
							err.add(valores);
						}else{
							//nos conectamos al as4000 para informar que el registro fue rechazado satisfactoriamente
							mp = getMessageProcessor("EPV1170", req);
							EPV117001Message msgResult = (EPV117001Message) mp.getMessageRecord("EPV117001", user.getH01USR(), "0031");
							assignValue(prod,msgResult);
							//Sending message
							mp.sendMessage(msgResult);	
							//Receive error and data
							ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();	
							if (mp.hasError(msgError)) {
								// if there are errors go back to maintenance page and sho errors
								session.setAttribute("error", msgError);								
								return procesado;
							}							
						}							
					}
					
				}//EoW
				
				if (err.size()>0){
					String numErr = (err.size()>10)?"10":String.valueOf(err.size());
		            ELEERRMessage msgError = new ELEERRMessage();
		            msgError.setERRNUM(numErr);
		            int fil=1;
		            for (Iterator<String[]> iterator = err.iterator(); iterator.hasNext();) {
		            	String fila = (fil>9)?""+fil:"0"+fil;
		            	String[] val = (String[]) iterator.next();
		            	String des = "Error Actualizar Stock Tienda Virtual Prod:"+val[1]+" Cant:"+val[2]+" Oper:"+val[3];//55
		            	msgError.getField("ERNU"+fila).setString(fila);//numero error
		            	msgError.getField("ERDS"+fila).setString(des);//descripcion error
					}
		            session.setAttribute("error", msgError);
				}else{
					//existen productos a actualizar y los proceso bien.
					procesado=true;
				}
				
			}else{
				//no existen productos a actualizar y proceso bien.
				procesado=true;
			}
			  


		}
		catch (Exception e) {
			String className = e.getClass().getName();
			String description = e.getMessage() == null ? "Exception General" : e.getMessage();					
            ELEERRMessage msgError = new ELEERRMessage();				
			msgError.setERRNUM("3");
            msgError.setERNU01("01");//4
            msgError.setERDS01("ERROR AL ACCEDER SERVICIO DE TIENDA VIRTUAL. VERIFIQUE COMUNICACION.");
            msgError.setERNU02("02");//4
            msgError.setERDS02(className);
            msgError.setERNU03("03");//4
            msgError.setERDS03(description.length() > 70 ? description.substring(0, 70) : description);	
			session.setAttribute("error", msgError);						
		}		
		finally {
			if (mp != null)
				mp.close();
		}		
		return procesado;
		
	}
	
	private void assignValue(EPV117001Message source, EPV117001Message destination){
		destination.setE01PTVCUN(source.getE01PTVCUN());
		destination.setE01PTVNUM(source.getE01PTVNUM());
		destination.setE01PTVSEQ(source.getE01PTVSEQ());
		destination.setE01PTVCAM(source.getE01PTVCAM());
		destination.setE01PTVNCA(source.getE01PTVNCA());
		destination.setE01PTVPRD(source.getE01PTVPRD());
		destination.setE01PTVCTV(source.getE01PTVCTV());
		destination.setE01PTVNME(source.getE01PTVNME());
		destination.setE01PTVMAR(source.getE01PTVMAR());
		destination.setE01PTVMOD(source.getE01PTVMOD());
		destination.setE01PTVVAU(source.getE01PTVVAU());
		destination.setE01PTVCAN(source.getE01PTVCAN());
		destination.setE01PTVSTO(source.getE01PTVSTO());
		destination.setE01PTVCEN(source.getE01PTVCEN());
		destination.setE01PTVPTO(source.getE01PTVPTO());
		destination.setE01PTVOPE(source.getE01PTVOPE());
		destination.setE01PTVRSP(source.getE01PTVRSP());
		destination.setE01NUMREC(source.getE01NUMREC());
		destination.setE01TVINUM(source.getE01TVINUM());
		destination.setE01TVIFLG(source.getE01TVIFLG());               
	}	

	/**
	 *  Lista todos las solicitudes a cursar.
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqCovenantApprovalList(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession session)
			throws ServletException, IOException {
			
			MessageProcessor mp = null;
			try {
				mp = getMessageProcessor("EPV1040", req);

				EPV104001Message msgList = (EPV104001Message) mp.getMessageRecord("EPV104001", user.getH01USR(), "0015");
				mp.sendMessage(msgList);
			 	
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
				
				if (mp.hasError(list)) {
					session.setAttribute("error", mp.getError(list));
					flexLog("About to call Error Page: sckNotRespondPage");
					forward(sckNotRespondPage, req, res);	
				} else {
					session.setAttribute("EPV104001List", list);
					forward("EPV1040_salesplatform_curse_list.jsp", req, res);
				}	

			} finally {
				if (mp != null)	mp.close();
			}
		}	
	/**
	 * procActionPlatformList: find the list of forms depending on status, the program will epvl1005
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqPlatformLiquidacionInquiry(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV1010", req);
			JBObjList aplist = (JBObjList)session.getAttribute("EPV104001List");
			int index = req.getParameter("key")==null?0:req.getParameter("key").equals("")?0:Integer.parseInt(req.getParameter("key"));
			
			EPV104001Message listMessage = (EPV104001Message)aplist.get(index);

			EPV103801Message msg = (EPV103801Message) mp.getMessageRecord("EPV103801");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0002");
			msg.setH01TIMSYS(getTimeStamp());
			msg.setE01PVMCUN(listMessage.getE01PVDCUN());
			msg.setE01PVMNUM(listMessage.getE01PVDNUM());

			// Sends message
			mp.sendMessage(msg);

			// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV103801Message) mp.receiveMessageRecord();

			session.setAttribute("formalizacion", msg);// set header page
			session.setAttribute("error", msgError);
			
			if (!mp.hasError(msgError)) {
				// if there are no errors go to maintenance page
				flexLog("EPV1038_salesplatform_formalizacion.jsp");
				forward("EPV1038_salesplatform_formalizacion.jsp?readOnly=true", req, res);
			} else {
				// if there are errors go back to list page
				forward("EPV1040_salesplatform_curse_list.jsp", req, res);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mp != null)
				mp.close();
		}
	}

 }	



