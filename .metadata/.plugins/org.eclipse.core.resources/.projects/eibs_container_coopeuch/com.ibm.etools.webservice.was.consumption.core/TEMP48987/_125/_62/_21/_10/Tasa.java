/**
 * Tasa.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public class Tasa  implements java.io.Serializable {
    private java.lang.String subProducto;
    private java.lang.String campana;
    private java.lang.String convenio;
    private java.lang.String secuencia;
    private java.lang.String plazo;
    private java.lang.String monto;
    private java.lang.String moneda;
    private java.lang.String tipoPlazo;
    private java.lang.String valorPrimario;
    private java.lang.String valorSecundario;
    private java.lang.String vigenciaDesde;
    private java.lang.String vigentiaHasta;

    public Tasa() {
    }

    public java.lang.String getSubProducto() {
        return subProducto;
    }

    public void setSubProducto(java.lang.String subProducto) {
        this.subProducto = subProducto;
    }

    public java.lang.String getCampana() {
        return campana;
    }

    public void setCampana(java.lang.String campana) {
        this.campana = campana;
    }

    public java.lang.String getConvenio() {
        return convenio;
    }

    public void setConvenio(java.lang.String convenio) {
        this.convenio = convenio;
    }

    public java.lang.String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(java.lang.String secuencia) {
        this.secuencia = secuencia;
    }

    public java.lang.String getPlazo() {
        return plazo;
    }

    public void setPlazo(java.lang.String plazo) {
        this.plazo = plazo;
    }

    public java.lang.String getMonto() {
        return monto;
    }

    public void setMonto(java.lang.String monto) {
        this.monto = monto;
    }

    public java.lang.String getMoneda() {
        return moneda;
    }

    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }

    public java.lang.String getTipoPlazo() {
        return tipoPlazo;
    }

    public void setTipoPlazo(java.lang.String tipoPlazo) {
        this.tipoPlazo = tipoPlazo;
    }

    public java.lang.String getValorPrimario() {
        return valorPrimario;
    }

    public void setValorPrimario(java.lang.String valorPrimario) {
        this.valorPrimario = valorPrimario;
    }

    public java.lang.String getValorSecundario() {
        return valorSecundario;
    }

    public void setValorSecundario(java.lang.String valorSecundario) {
        this.valorSecundario = valorSecundario;
    }

    public java.lang.String getVigenciaDesde() {
        return vigenciaDesde;
    }

    public void setVigenciaDesde(java.lang.String vigenciaDesde) {
        this.vigenciaDesde = vigenciaDesde;
    }

    public java.lang.String getVigentiaHasta() {
        return vigentiaHasta;
    }

    public void setVigentiaHasta(java.lang.String vigentiaHasta) {
        this.vigentiaHasta = vigentiaHasta;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        Tasa other = (Tasa) obj;
        boolean _equals;
        _equals = true
            && ((this.subProducto==null && other.getSubProducto()==null) || 
             (this.subProducto!=null &&
              this.subProducto.equals(other.getSubProducto())))
            && ((this.campana==null && other.getCampana()==null) || 
             (this.campana!=null &&
              this.campana.equals(other.getCampana())))
            && ((this.convenio==null && other.getConvenio()==null) || 
             (this.convenio!=null &&
              this.convenio.equals(other.getConvenio())))
            && ((this.secuencia==null && other.getSecuencia()==null) || 
             (this.secuencia!=null &&
              this.secuencia.equals(other.getSecuencia())))
            && ((this.plazo==null && other.getPlazo()==null) || 
             (this.plazo!=null &&
              this.plazo.equals(other.getPlazo())))
            && ((this.monto==null && other.getMonto()==null) || 
             (this.monto!=null &&
              this.monto.equals(other.getMonto())))
            && ((this.moneda==null && other.getMoneda()==null) || 
             (this.moneda!=null &&
              this.moneda.equals(other.getMoneda())))
            && ((this.tipoPlazo==null && other.getTipoPlazo()==null) || 
             (this.tipoPlazo!=null &&
              this.tipoPlazo.equals(other.getTipoPlazo())))
            && ((this.valorPrimario==null && other.getValorPrimario()==null) || 
             (this.valorPrimario!=null &&
              this.valorPrimario.equals(other.getValorPrimario())))
            && ((this.valorSecundario==null && other.getValorSecundario()==null) || 
             (this.valorSecundario!=null &&
              this.valorSecundario.equals(other.getValorSecundario())))
            && ((this.vigenciaDesde==null && other.getVigenciaDesde()==null) || 
             (this.vigenciaDesde!=null &&
              this.vigenciaDesde.equals(other.getVigenciaDesde())))
            && ((this.vigentiaHasta==null && other.getVigentiaHasta()==null) || 
             (this.vigentiaHasta!=null &&
              this.vigentiaHasta.equals(other.getVigentiaHasta())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        Tasa history = (Tasa) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        Tasa history = (Tasa) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getSubProducto() != null) {
            _hashCode += getSubProducto().hashCode();
        }
        if (getCampana() != null) {
            _hashCode += getCampana().hashCode();
        }
        if (getConvenio() != null) {
            _hashCode += getConvenio().hashCode();
        }
        if (getSecuencia() != null) {
            _hashCode += getSecuencia().hashCode();
        }
        if (getPlazo() != null) {
            _hashCode += getPlazo().hashCode();
        }
        if (getMonto() != null) {
            _hashCode += getMonto().hashCode();
        }
        if (getMoneda() != null) {
            _hashCode += getMoneda().hashCode();
        }
        if (getTipoPlazo() != null) {
            _hashCode += getTipoPlazo().hashCode();
        }
        if (getValorPrimario() != null) {
            _hashCode += getValorPrimario().hashCode();
        }
        if (getValorSecundario() != null) {
            _hashCode += getValorSecundario().hashCode();
        }
        if (getVigenciaDesde() != null) {
            _hashCode += getVigenciaDesde().hashCode();
        }
        if (getVigentiaHasta() != null) {
            _hashCode += getVigentiaHasta().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
