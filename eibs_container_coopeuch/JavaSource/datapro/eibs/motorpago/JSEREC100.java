package datapro.eibs.motorpago;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EREC10001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;

public class JSEREC100 extends JSEIBSServlet {

	private static final long serialVersionUID 		= 1L;

	protected static final int R_INQUIRY			= 100;
	protected static final int R_INTER_LIST			= 200;
	protected static final int A_INTER 				= 300;
	protected static final int A_MAINTENANCE_INTER 	= 400;

	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_INQUIRY :
				procReqInquiry(user, req, res, session);
				break;
			case R_INTER_LIST :
				procReqInterList(user, req, res, session);
				break;
			case A_INTER :
				procActionInter(user, req, res, session);
				break;
			case A_MAINTENANCE_INTER :
				procActionMaintenanceInter(user, req, res, session);
				break;

				
			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}



	private void procReqInquiry(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		

		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
	
		try {		
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();

			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
		  	} catch (Exception ex) {
				flexLog("Error: " + ex);  
		  	}

			try {
				flexLog("About to call Page: EREC100_inquiry_interface.jsp");
				forward("EREC100_inquiry_interface.jsp", req, res);
			}
			catch (Exception e) {
				flexLog("Exception calling page " + e);
			}
		
	}
	
/***/
	
	private void procReqInterList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EREC100", req);
			EREC10001Message msgInter = (EREC10001Message) mp.getMessageRecord("EREC10001", user.getH01USR(), "0003");

			setMessageRecord(req, msgInter);
			userPO.setIdentifier(msgInter.getE01RERIDE().toString());
			userPO.setHeader1(req.getParameter("E01RERDES"));
			
			mp.sendMessage(msgInter);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList msgInterList = mp.receiveMessageRecordList("H01FLGMAS");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);

				forward("EREC100_inquiry_interface.jsp", req, res);
			} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("InterList", msgInterList);
				session.setAttribute("userPO", userPO);
				
				forward("EREC100_interface_list.jsp", req, res);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
/***/
	private void procActionInter(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNewInter(user, req, res, session);
				break;
			default : //Maintenance
				userPO.setPurpose("MAINTENANCE");
				session.setAttribute("userPO", userPO);
				procReqMaintenanceInter(user, req, res, session);
				break;
		}
	}

/***/
	private void procReqNewInter(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		flexLog("Putting java beans into the session");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("Inter", new EREC10001Message());
		
		forward("EREC100_inter_maitenance.jsp", req, res);
	}


	private void procReqMaintenanceInter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		
		JBObjList bl = (JBObjList) session.getAttribute("InterList");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		bl.setCurrentRow(idx);
		EREC10001Message msgInter = (EREC10001Message) bl.getRecord();

		flexLog("Putting java beans into the session");
		session.setAttribute("Inter", msgInter);
		session.setAttribute("userPO", userPO);
			
		forward("EREC100_inter_maitenance.jsp", req, res);
	}
	
/***/
	private void procActionMaintenanceInter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		
		try {
			mp = getMessageProcessor("EREC100", req);
			EREC10001Message msgInter = (EREC10001Message) new EREC10001Message() ;
			setMessageRecord(req, msgInter);
			msgInter.setH01USERID(user.getH01USR());
			msgInter.setH01PROGRM("EREC100");
			msgInter.setH01TIMSYS(getTimeStamp());

			if(userPO.getPurpose().equals("NEW"))
				msgInter.setH01OPECOD("0001");
			else
				msgInter.setH01OPECOD("0002");

			mp.sendMessage(msgInter);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgInter = (EREC10001Message) mp.receiveMessageRecord("EREC10001");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("Inter", msgInter);
				session.setAttribute("userPO", userPO);
				
				forward("EREC100_inter_maitenance.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				
				procReqInterList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}


	
//	
}
