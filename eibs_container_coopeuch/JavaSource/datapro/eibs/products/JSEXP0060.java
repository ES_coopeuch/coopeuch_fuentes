package datapro.eibs.products;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EXP006001Message;
import datapro.eibs.beans.EXP006002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JSEXP0060 extends JSEIBSServlet {

	//Consulta de Remanentes
	protected static final int R_ENTER_INQUIRY_REMANENTE = 200;
	protected static final int A_ENTER_INQUIRY_REMANENTE = 300;

	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_ENTER_INQUIRY_REMANENTE :
				procReqEnterInquiry(user, req, res, session);
				break;
			case A_ENTER_INQUIRY_REMANENTE :
				procActEnterInquiry(user, req, res, session);
				break;
			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}



	private void procReqEnterInquiry(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = new UserPos();
		userPO.setPurpose("MAINTENANCE");
		session.setAttribute("userPO", userPO);
		
		session.setAttribute("externos", new EXP006001Message());
		
		forward("EXP0060_productos_externos_enter_inquiry.jsp", req, res);
	}

	protected void procActEnterInquiry(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EXP0060", req);

			EXP006001Message msg = (EXP006001Message) mp.getMessageRecord("EXP006001");
			
			//Sets message with page fields
			msg.setH01SCRCOD("01");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0004");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EXP006001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("externos", msg);

			if (!mp.hasError(msgError)) {
				EXP006002Message msgCuotas = (EXP006002Message) mp.getMessageRecord("EXP006002");
				msgCuotas.setH02USERID(user.getH01USR());
			 	msgCuotas.setH02SCRCOD("01");
			 	msgCuotas.setH02OPECOD("0015");
			 	msgCuotas.setE02EXDACC(msg.getE01EXPACC());
			 	
				mp.sendMessage(msgCuotas);
			
				JBObjList list = mp.receiveMessageRecordList("H02FLGMAS");
				session.setAttribute("EXP0060List", list);
				forward("EXP0060_productos_externos_basic_inquiry.jsp", req, res);

			} else {
				//if there are errors go back to maintenance page and show errors
				forward("EXP0060_productos_externos_enter_inquiry.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

}
