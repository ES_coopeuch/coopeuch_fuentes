package com.datapro.presentation.input;


import java.text.ParseException;

import com.datapro.constants.EibsDate;
import com.datapro.generic.tool.UDateFormat;

/**
 * @author fhernandez
 *
 */

public class EibsInputDate extends EibsInputFields implements EibsDate {

	private static final long serialVersionUID = 2679387849717165817L;
	private String format = FORMAT_DATE_DMY;

	public EibsInputDate() {
		super();
	}
	
	public EibsInputDate(String format) {
		super();
		this.format = format;
	}
	
	public String getFormat() {
		return format;
	}
	
	public void setFormat(String format) {
		this.format = format;
	}	
	
	public String getDateInput(
			String fldDateName,
			String fldDateValue,
			String options) {
		
        try {
            String formatedDate = UDateFormat.format(
				UDateFormat.parse(
					fldDateValue,
					PATTERN_SQL_DATE),
				format,
				MEDIUM);
            
            return getInputText(
    				EIBS_FIELD_TYPE_DATE, 
    				fldDateName, 
    				formatedDate, 
    				options);
        } catch (ParseException e) {
            return "Invalid Date Format.";
        }

	}
	
	public String getDateInput(
			String fldYearName,
			String fldMonthName,
			String fldDayName,
			String fldYearValue,
			String fldMonthValue,
			String fldDayValue,
			String options) {
		
		StringBuffer sb = new StringBuffer();
		
		String prefix_id = "";
		if(getStyleId()!= null){				
			prefix_id = getStyleId()+"_";
		}else{
			setId("");
		}
		
		if (format.equals(FORMAT_DATE_DMY)) {
			if(getStyleId()!= null){				
				setId(prefix_id + fldDayName);
			}			
			sb.append(
					getInputText(EIBS_FIELD_TYPE_DAY, fldDayName, fldDayValue, options));
			if(getStyleId()!= null){				
				setId(prefix_id + fldMonthName);
			}
			sb.append(
					getInputText(EIBS_FIELD_TYPE_MONTH, fldMonthName, fldMonthValue, options));
			if(getStyleId()!= null){				
				setId(prefix_id + fldYearName);
			}
			sb.append(
					getInputText(EIBS_FIELD_TYPE_YEAR, fldYearName, fldYearValue, options));
			
		} else if (format.equals(FORMAT_DATE_MDY)) {
			if(getStyleId()!= null){				
				setId(prefix_id + fldMonthName);
			}
			sb.append(
					getInputText(EIBS_FIELD_TYPE_MONTH, fldMonthName, fldMonthValue, options));
			
			if(getStyleId()!= null){				
				setId(prefix_id + fldDayName);
			}
			sb.append(
					getInputText(EIBS_FIELD_TYPE_DAY, fldDayName, fldDayValue, options));
			
			if(getStyleId()!= null){				
				setId(prefix_id + fldYearName);
			}
			sb.append(
					getInputText(EIBS_FIELD_TYPE_YEAR, fldYearName, fldYearValue, options));
			
		} else if (format.equals(FORMAT_DATE_YMD)) {
			if(getStyleId()!= null){				
				setId(prefix_id + fldYearName);
			}
			sb.append(
					getInputText(EIBS_FIELD_TYPE_YEAR, fldYearName, fldYearValue, options));
			
			if(getStyleId()!= null){				
				setId(prefix_id + fldMonthName);
			}
			sb.append(
					getInputText(EIBS_FIELD_TYPE_MONTH, fldMonthName, fldMonthValue, options));
			
			if(getStyleId()!= null){				
				setId(prefix_id + fldDayName);
			}
			sb.append(
					getInputText(EIBS_FIELD_TYPE_DAY, fldDayName, fldDayValue, options));
			
		} else {
			sb.append("Invalid Date Format.");
		}
	
		return sb.toString();
	}
	
	public String getHelpLink(
			String contextPath,
			String skin,
			String function,
			String id) {
		
		StringBuffer sb = new StringBuffer();
		if (function != null) {
			if (id != null)
				sb.append(
					"<a id=\""
						+ id
						+ "\" href=\"javascript:"
						+ function
						+ "\">");
			else
				sb.append(
					"<a id=\"linkHelp\" href=\"javascript:" + function + "\">");
			
			sb.append(
				"<img src=\""
					+ contextPath
					+ "/images/calendar.gif\" alt=\""+ super.getAlt() 
					+ "\" align=\"bottom\" border=\"0\"/>");
		}
		return sb.toString();
	}
	
	public String getDateFunction(
			String fldYearName,
			String fldMonthName,
			String fldDayName) {

		if (fldYearName != null && fldMonthName != null && fldDayName != null) {
			if (format.equals(FORMAT_DATE_DMY)) {
				return "DatePicker(" 
				+ "document.forms[0]." + fldDayName + ","
				+ "document.forms[0]." + fldMonthName + ","
				+ "document.forms[0]." + fldYearName + ")";
				
			} else if (format.equals(FORMAT_DATE_MDY)) {
				return "DatePicker(" 
				+ "document.forms[0]." + fldMonthName + ","
				+ "document.forms[0]." + fldDayName + ","
				+ "document.forms[0]." + fldYearName + ")";
				
			} else if (format.equals(FORMAT_DATE_YMD)) {
				return "DatePicker(" 
				+ "document.forms[0]." + fldYearName + ","
				+ "document.forms[0]." + fldMonthName + ","
				+ "document.forms[0]." + fldDayName + ")";
			}
		}
		return "";
	}
	
	public String getDateHelpLink(
			String contextPath,
			String skin,
			String fldYearName,
			String fldMonthName,
			String fldDayName) {

		return getHelpLink(
				contextPath, 
				skin, 
				getDateFunction(fldYearName, fldMonthName, fldDayName), null);
	}
	
	public String getDateHelpLink(
			String contextPath,
			String skin,
			String fldYearName,
			String fldMonthName,
			String fldDayName,
			String id) {

		return getHelpLink(
				contextPath, 
				skin, 
				getDateFunction(fldYearName, fldMonthName, fldDayName), id);
	}
	
}