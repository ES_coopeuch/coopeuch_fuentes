package datapro.eibs.helps;

/**
 * This type was created by Gustavo Adolfo Villarroel.
 */
import java.io.*;
import java.net.*;
import java.beans.Beans;
import javax.servlet.*;
import javax.servlet.http.*;
import datapro.eibs.beans.*;

import datapro.eibs.master.Util;

import datapro.eibs.sockets.*;

public class JSEWD0700 extends datapro.eibs.master.SuperServlet {

	/**
	 * Insert the method's description here.
	 * Creation date: (1/14/00 12:29:44 PM)
	 */
	public JSEWD0700() {
		super();
	}
	/**
	 * This method was created by Gustavo Adolfo Villarroel.
	 */
	public void destroy() {

		flexLog("free resources used by JSEWD0700");

	}
	/**
	 * This method was created by Gustavo Adolfo Villarroel.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}
	/**
	 * This method was created by Gustavo Adolfo Villarroel.
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 */
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		
		MessageContext mc = null;
		HttpSession session;
		MessageRecord newmessage = null;
		EWD0700RMessage msgHelp = null;
		EWD0700SMessage msgHelpS = null;
		ESS0030DSMessage msgUser = null;
		JBList beanList = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			msgUser =
				(datapro.eibs.beans.ESS0030DSMessage) session.getAttribute(
					"currUser");
			String Language = msgUser.getE01LAN();
			String LangPath = super.rootPath + Language + "/";
			String Custype = "";
			try {
				flexLog("Opennig Socket Connection");
				mc = new MessageContext(super.getMessageHandler("EWD0700", req));

				try {
					msgHelp = (EWD0700RMessage) mc.getMessageRecord("EWD0700R");
					msgHelp.setRWDUSR(msgUser.getH01USR());
					msgHelp.setRWDSHR(
						req.getParameter("NameSearch").toUpperCase());
					msgHelp.setRWDTYP(req.getParameter("Type"));
					session.setAttribute("Type", msgHelp.getRWDTYP());
					msgHelp.setRWDFRC(req.getParameter("FromRecord"));
					try {
						Custype =
							(req.getParameter("CusType") == null)
								? ""
								: req.getParameter("CusType");
					} catch (Exception e) {
						Custype = "";
					}
					msgHelp.send();
					msgHelp.destroy();
					flexLog("EWD0700R Message Sent");
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
					return;
				}

				// Receiving
				try {
					newmessage = mc.receiveMessage();

					if (newmessage.getFormatName().equals("EWD0700S")) {

						try {
							beanList = new JBList();
							flexLog("EWD0700S Message Received");
						} catch (Exception ex) {
							flexLog("Error: " + ex);
						}

						boolean firstTime = true;
						String marker = "";
						String myFlag = "";
						StringBuffer myRow = null;
						String chk = "";
						String idn = "";

						while (true) {

							msgHelpS = (EWD0700SMessage) newmessage;

							marker = msgHelpS.getSWDOPE();

							if (marker.equals("*")) {
								beanList.setShowNext(false);
								break;
							} else {

								if (firstTime) {
									firstTime = false;
									beanList.setFirstRec(
										Integer.parseInt(msgHelpS.getSWDREC()));
									chk = "checked";
								} else {
									chk = "";
								}
								
								if (msgHelpS.getSWDIDN().equals(""))
									idn = msgHelpS.getSWDLN3();
								else
									idn = msgHelpS.getSWDIDN();
								
								myRow = new StringBuffer("<TR>");
								myRow.append(
									"<td nowrap><a href=\"javascript:enter('"
										+ msgHelpS.getSWDCUN()
										+ "','"
										+ msgHelpS.getSWDNA1()
										+ "','"
										+ idn
										+ "','"
										+ msgHelpS.getSWDTID()
										+ "','"
										+ msgHelpS.getSWDPID()
										+ "','"
										+ msgHelpS.getSWDCTR()
										+ "','"
										+ msgHelpS.getSWDSTE()
										+ "','"
										+ msgHelpS.getSWDUC8()
										+ "','"
										+ msgHelpS.getSWDMUN()
										+ "','"
										+ msgHelpS.getSWDCTY()
										+ "','"
										+ msgHelpS.getSWDPAR()
										+ "','"
										+ msgHelpS.getSWDZPC()
										+ "','"
										+ msgHelpS.getSWDNA2()
										+ "','"
										+ msgHelpS.getSWDNM4()
										+ "','"
										+ msgHelpS.getSWDNM5()
										+ "','"
										+ msgHelpS.getSWDNIV()
										+ "','"
										+ msgHelpS.getSWDAPT()
										+ "','"
										+ msgHelpS.getSWDNA4()
										+ "','"
										+ msgHelpS.getSWDPOB()
										+ "','"
										+ msgHelpS.getSWDCCS()
										+ "','"
										+ msgHelpS.getSWDEDL()
										+ "','"
										+ msgHelpS.getSWDSEX()
										+ "','"
										+ msgHelpS.getSWDMST()
										+ "','"
										+ msgHelpS.getSWDHPN()										
										+ "')\">"
										+ msgHelpS.getSWDCUN()
										+ "</a></td>");
								myRow.append(
									"<td nowrap><a href=\"javascript:enter('"
										+ msgHelpS.getSWDCUN()
										+ "','"
										+ msgHelpS.getSWDNA1()
										+ "','"
										+ idn
										+ "','"
										+ msgHelpS.getSWDTID()
										+ "','"
										+ msgHelpS.getSWDPID()
										+ "','"
										+ msgHelpS.getSWDCTR()
										+ "','"
										+ msgHelpS.getSWDSTE()
										+ "','"
										+ msgHelpS.getSWDUC8()
										+ "','"
										+ msgHelpS.getSWDMUN()
										+ "','"
										+ msgHelpS.getSWDCTY()
										+ "','"
										+ msgHelpS.getSWDPAR()
										+ "','"
										+ msgHelpS.getSWDZPC()
										+ "','"
										+ msgHelpS.getSWDNA2()
										+ "','"
										+ msgHelpS.getSWDNM4()
										+ "','"
										+ msgHelpS.getSWDNM5()
										+ "','"
										+ msgHelpS.getSWDNIV()
										+ "','"
										+ msgHelpS.getSWDAPT()
										+ "','"
										+ msgHelpS.getSWDNA4()
										+ "','"
										+ msgHelpS.getSWDPOB()
										+ "','"
										+ msgHelpS.getSWDCCS()
										+ "','"
										+ msgHelpS.getSWDEDL()
										+ "','"
										+ msgHelpS.getSWDSEX()
										+ "','"
										+ msgHelpS.getSWDMST()
										+ "','"
										+ msgHelpS.getSWDHPN()										
										+ "')\">"
										+ msgHelpS.getSWDNA1()
										+ "</a></td>");
								myRow.append(
									"<td nowrap><a href=\"javascript:enter('"
										+ msgHelpS.getSWDCUN()
										+ "','"
										+ msgHelpS.getSWDNA1()
										+ "','"
										+ idn
										+ "','"
										+ msgHelpS.getSWDTID()
										+ "','"
										+ msgHelpS.getSWDPID()
										+ "','"
										+ msgHelpS.getSWDCTR()
										+ "','"
										+ msgHelpS.getSWDSTE()
										+ "','"
										+ msgHelpS.getSWDUC8()
										+ "','"
										+ msgHelpS.getSWDMUN()
										+ "','"
										+ msgHelpS.getSWDCTY()
										+ "','"
										+ msgHelpS.getSWDPAR()
										+ "','"
										+ msgHelpS.getSWDZPC()
										+ "','"
										+ msgHelpS.getSWDNA2()
										+ "','"
										+ msgHelpS.getSWDNM4()
										+ "','"
										+ msgHelpS.getSWDNM5()
										+ "','"
										+ msgHelpS.getSWDNIV()
										+ "','"
										+ msgHelpS.getSWDAPT()
										+ "','"
										+ msgHelpS.getSWDNA4()
										+ "','"
										+ msgHelpS.getSWDPOB()
										+ "','"
										+ msgHelpS.getSWDCCS()
										+ "','"
										+ msgHelpS.getSWDEDL()
										+ "','"
										+ msgHelpS.getSWDSEX()
										+ "','"
										+ msgHelpS.getSWDMST()
										+ "','"
										+ msgHelpS.getSWDHPN()										
										+ "')\">"
										+ idn
										+ "</a></td>");
								myRow.append("</TR>");
								beanList.addRow(myFlag, myRow.toString());

								if (marker.equals("+")) {
									beanList.setShowNext(true);
									break;
								}
							}
							newmessage = mc.receiveMessage();
						}

						flexLog("Putting java beans into the session");
						session.setAttribute("EWD0700Help", beanList);

						try {
							req.setAttribute(
								"NameSearch",
								req.getParameter("NameSearch"));
							req.setAttribute("Type", req.getParameter("Type"));
							req.setAttribute("CusType", Custype);
							flexLog(
								"About to call Page: "
									+ LangPath
									+ "EWD0700_customer_details_help_helpmessage.jsp");
							callPage(
								LangPath
									+ "EWD0700_customer_details_help_helpmessage.jsp",
								req,
								res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
				}
			} catch (Exception e) {
				e.printStackTrace();
				int sck = getInitSocket(req) + 1;
				flexLog("Socket not Open(Port " + sck + "). Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
				//return;
			} finally {
				mc.close();
			}

		}

	}
}