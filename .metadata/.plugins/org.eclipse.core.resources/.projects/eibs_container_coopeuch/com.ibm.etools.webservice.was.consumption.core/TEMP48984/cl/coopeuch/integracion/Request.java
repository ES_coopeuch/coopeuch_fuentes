/**
 * Request.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.coopeuch.integracion;

public class Request  implements java.io.Serializable {
    private cl.coopeuch.integracion.Parametro[] parametro;

    public Request() {
    }

    public cl.coopeuch.integracion.Parametro[] getParametro() {
        return parametro;
    }

    public void setParametro(cl.coopeuch.integracion.Parametro[] parametro) {
        this.parametro = parametro;
    }

    public cl.coopeuch.integracion.Parametro getParametro(int i) {
        return parametro[i];
    }

    public void setParametro(int i, cl.coopeuch.integracion.Parametro value) {
        this.parametro[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        Request history = (Request) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        Request other = (Request) obj;
        boolean _equals;
        _equals = true
            && ((this.parametro==null && other.getParametro()==null) || 
             (this.parametro!=null &&
              java.util.Arrays.equals(this.parametro, other.getParametro())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        Request history = (Request) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getParametro() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getParametro());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getParametro(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
