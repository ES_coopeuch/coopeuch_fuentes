/*
 * Created on Apr 8, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package datapro.eibs.products;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELC051501Message;
import datapro.eibs.beans.ELC051502Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageContextHandler;
import datapro.eibs.sockets.MessageRecord;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JSELC0515 extends SuperServlet {

	String LangPath = "s/";
	
	private static final int R_SWIFT_LIST = 1;
	private static final int R_INQ_SWIFT = 2;
	private static final int A_APPROVAL_SWIFT = 3;
				
	public JSELC0515() {
		super();
	}
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = (HttpSession) req.getSession(false);
		if (session == null) {
			try {
				res.setContentType("text/html");
				super.printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {
			int screen = -1;

			ESS0030DSMessage user = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");
			// Here we should get the path from the user profile
			LangPath = rootPath + user.getE01LAN() + "/";
			
			Socket s = null;
			try {
				s = new Socket(hostIP, getInitSocket(req) + 1);
				s.setSoTimeout(sckTimeOut);
				MessageContext mc =
					new MessageContext(
						new DataInputStream(new BufferedInputStream(s.getInputStream())),
						new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
						"datapro.eibs.beans");
						
				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
					flexLog("Screen  Number: " + screen);
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}
				
				String PageToCall = "";

				switch (screen) {
					// Request
					case R_SWIFT_LIST :
						procReqIncomingSwiftList(mc, user, req, res, screen);
						break;
					case R_INQ_SWIFT :
						procReqIncomingSwiftInquiry(mc, user, req, res, screen);
						break;
					// Action
					case A_APPROVAL_SWIFT :
						procActionApprovalSwift(mc, user, req, res, screen);
						break;
					default :
						PageToCall = "MISC_not_available.jsp";
						callPage(PageToCall, req, res);
						break;
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				res.sendRedirect(srctx + LangPath + sckNotRespondPage);
			} finally {
				if (s != null) s.close();
				flexLog("Socket used by JSELC0515 closed.");
			}
		}	
	}
	
	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void procActionApprovalSwift(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ELC051501Message msg01 = (ELC051501Message) msgHandle.initMessage("ELC051501", user.getH01USR(), "0002");
			initTransaction(userPO, String.valueOf(screen), "");
			msg01.setH01SCRCOD("01");
			try {
				msg01.setE01LCMPRO(req.getParameter("PRODUCT"));
			} catch (Exception e) {
				msg01.setE01LCMPRO("");
			}
			try {
				msg01.setE01WAPFMT(req.getParameter("FMT"));
			} catch (Exception e) {
				msg01.setE01WAPFMT("");
			}
			try {
				msg01.setE01WAPSBK(req.getParameter("SBK"));
			} catch (Exception e) {
				msg01.setE01WAPSBK("");
			}
			try {
				msg01.setE01WAPRBK(req.getParameter("RBK"));
			} catch (Exception e) {
				msg01.setE01WAPRBK("");
			}
			try {
				msg01.setE01WAPRNO(req.getParameter("RNO"));
			} catch (Exception e) {
				msg01.setE01WAPRNO("");
			}
			msgHandle.sendMessage(msg01);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			boolean isNotError = msgError.getERRNUM().equals("0");
			msg01 = (ELC051501Message) msgHandle.receiveMessage();
			putDataInSession(session, userPO, msgError, "msgELC", msg01, null);
			if (isNotError) {
				if (msg01.getH01FLGWK2().equals("S") || msg01.getH01FLGWK2().equals("R")) {
					//stand by
					flexLog("About to redirect request to the procReqIncomingSwiftList with specific parameters: ");
					res.sendRedirect(srctx + "/servlet/datapro.eibs.products.JSELC0410?SCREEN=3&E01LCMACC=" + msg01.getE01LCMACC() + "&H01FLGMAS=N");							
				} else {
					//commercial
					flexLog("About to redirect request to the procReqIncomingSwiftList with specific parameters: ");
					res.sendRedirect(srctx + "/servlet/datapro.eibs.products.JSELC0510?SCREEN=3&E01LCMACC=" + msg01.getE01LCMACC() + "&H01FLGMAS=N");							
				}
			} else {
				procReqIncomingSwiftList(mc, user, req, res, screen);
			}
			return;
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void procReqIncomingSwiftInquiry(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		String PageToCall = "";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ELC051502Message msg02 = (ELC051502Message) msgHandle.initMessage("ELC051502", user.getH01USR(), "0005");
			initTransaction(userPO, String.valueOf(screen), "");
			try {
				msg02.setE02SWIRNO(req.getParameter("RNO"));
			} catch (Exception e) {
				msg02.setE02SWIRNO("");
			}
			try {
				msg02.setE02SWIRBK(req.getParameter("RBK"));
			} catch (Exception e) {
				msg02.setE02SWIRBK("");
			}
			msgHandle.sendMessage(msg02);
			ELEERRMessage msgError = new ELEERRMessage();
			JBObjList jbList = msgHandle.receiveMessageList("ELC051502", "H02FLGMAS", msgError);
			boolean isNotError = msgError.getERRNUM().equals("0");
			if (isNotError) {
				PageToCall = "ELC0515_inq_incoming_swift_message.jsp";
			} else {
				PageToCall = "error_viewer.jsp";
			}
			putDataInSession(session, userPO, msgError, "", null, jbList);
			callPage(PageToCall, req, res);
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void procReqIncomingSwiftList(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		String PageToCall = "";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ELC051501Message msg01 = (ELC051501Message) msgHandle.initMessage("ELC051501", user.getH01USR(), "0015");
			initTransaction(userPO, String.valueOf(screen), "");
			msg01.setH01SCRCOD("01");
			try {
				msg01.setE01NUMREC(req.getParameter("Pos"));
			} catch (Exception ex) {
				msg01.setE01NUMREC("0");
			}
			msgHandle.sendMessage(msg01);
			ELEERRMessage msgError = new ELEERRMessage();
			JBObjList jbList = msgHandle.receiveMessageList("ELC051501", "E01INDOPE", msgError);
			boolean isNotError = msgError.getERRNUM().equals("0");
			if (isNotError) {
				PageToCall = "ELC0515_incoming_swift_list.jsp";
			} else {
				PageToCall = "ELC0510_lc_enter_maint.jsp";
			}
			putDataInSession(session, userPO, msgError, "", null, jbList);
			callPage(PageToCall, req, res);
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	private void initTransaction(UserPos userPO, String optMenu, String purpose) {
		userPO.setOption(optMenu);
		userPO.setPurpose(purpose);
	}
	
	public void callPage(String page, HttpServletRequest req, HttpServletResponse res) {
		try {
			super.callPage(LangPath + page, req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e.toString() + e.getMessage());
		}
		return; 
	}
	
	private void putDataInSession(HttpSession session, UserPos userPO, ELEERRMessage msgError, String msg_name, MessageRecord msg, JBObjList jbList) {
		if (msgError == null) {
			msgError = new ELEERRMessage(); 
		}
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		if (msg != null) session.setAttribute(msg_name, msg);
		if (jbList != null) session.setAttribute("msgList", jbList);
	}

}
