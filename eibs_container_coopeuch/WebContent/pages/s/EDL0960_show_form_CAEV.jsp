<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV100503Message,datapro.eibs.beans.EPV101003Message,,datapro.eibs.beans.EPV101011Message"%>

<html>    
<head>
<title>Formulario CAEV </title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
 
<jsp:useBean id="formObj" class="datapro.eibs.beans.EDL096001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />
<jsp:useBean id= "list" class= "datapro.eibs.beans.JBListRec"  scope="session" />
	
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">
	function doPrint(){
		if(!window.print){
	       var msg ="Debe actualizar su navegador para imprimir";
		   alert(msg);
		   return;
		}
		
	    window.focus();
		window.print();
		window.close();
		return;
	}

 function goCancel() {
	document.forms[0].submit();
 }
 
 function goPrint() {
	var pg = '<%=request.getContextPath()%>/pages/s/EDL0960_show_form_CAEV_print.jsp';
	CenterWindow(pg,720,500,2);
	//window.print();	
 }  
 </script>
</head>

          
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>

<h3 align="center">INFORMACION HISTORICA DEL CREDITO<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="show_form_CAEV.jsp; EDL0960"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0960">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="100">


 <% int row = 0;%>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="30%"> Nombre Titular : </td>
             <td nowrap align="left" width="70%"> <%=formObj.getE01CAENA1()%></td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" > Fecha Informacion : </td>
             <td nowrap align="left" > <%=formObj.getE01CAEFID()%>/<%=formObj.getE01CAEFIM()%>/<%=formObj.getE01CAEFIY()%></td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> Id Producto : </td>
             <td nowrap align="left"><%=formObj.getE01CAEACC()%> </td>
         </tr>         
        </table>
      </td>
    </tr>
  </table><h4>I. Producto Principal</h4>
  
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="30%"> 
              <div align="right">Plazo del Credito : </div>
            </td>            
            <td nowrap width="70%"> <%=formObj.getE01CAECUT()%></td>            
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Saldo del Credito : </div>
            </td>            
            <td nowrap> <%=formObj.getE01CAESLD()%></td>            
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Valor de la Cuota (Pesos) : </div>
            </td>            
            <td nowrap> <%=formObj.getE01CAEVCU()%> </td>            
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Fecha Proximo Pago : </div>
            </td>            
            <td nowrap> <%=formObj.getE01CAEPPD()%>/<%=formObj.getE01CAEPPM()%>/<%=formObj.getE01CAEPPY()%></td>            
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Costo Total de Prepago : </div>
            </td>            
            <td nowrap> <%=formObj.getE01CAECTP()%> </td>            
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">CAEV% : </div>
            </td>            
            <td nowrap> <%=formObj.getE01CAEVAL()%></td>            
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Garantias Vigentes : </div>
            </td>            
            <td nowrap> <%=formObj.getE01CAEGAR()%> </td>            
          </tr>  
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Numero de Cuota : </div>
            </td>            
            <td nowrap> <%=formObj.getE01CAENCU()%> </td>            
          </tr>
        </table>
      </td>
    </tr>
  </table>  

  <h4>II. Historial</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="30%"> 
              <div align="right">Numero de Cuotas Pagadas : </div>
            </td>            
            <td nowrap width="70%"><%=formObj.getE01CAECUP()%>  </td>            
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Numero de Cuotas Vencidas No Pagadas : </div>
            </td>            
            <td nowrap> <%=formObj.getE01CAECVN()%> </td>            
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Monto Vencido No Pagado : </div>
            </td>            
            <td nowrap> <%=formObj.getE01CAEMVN()%> </td>            
          </tr> 
		 <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Historial Detalle : </div>
            </td>            
            <td nowrap>&nbsp;</td>                          
          </tr>  
        </table>
      </td>
    </tr>
  </table>
  
  <TABLE class="tableinfo"  cellpading=2 cellspacing=3>
    <TR id=trdark> 
      <TH ALIGN=CENTER nowrap   >Nro. Cuota</TH>
      <TH ALIGN=CENTER nowrap   >Monto Cuota</TH>
      <TH ALIGN=CENTER nowrap   >Fecha Vencimiento</TH>      
    </TR>
    <%
    			String couteDate = "";
                list.initRow();
                while (list.getNextRow()) {
                    if (couteDate.equals(list.getRecord(1))) {
                    } else {
                       couteDate = list.getRecord(1);
	%>
	 
    <TR> 
      <TD ALIGN=CENTER  nowrap   ><%= list.getRecord(0) %></TD>
      <TD ALIGN=CENTER  nowrap  ><div align="right"><%= list.getRecord(5) %></div></TD>
      <TD ALIGN=CENTER  nowrap  ><%= list.getRecord(1) %></TD>      
    </TR>
    
    <%
                    }
                }
    %> 
  </TABLE>
  
  <br>
      
   
  <h4>III. Gastos o Cargos por Productos o Servicios Voluntariamente Contratados</h4>

  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center"  class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="30%"> 
              <div align="right">Valor de la Cuotas Referencia :</div>
            </td>            
            <td nowrap width="70%"> </td>            
          </tr>  
        </table>
      </td>
    </tr>
  </table>
<table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">             
            <th nowrap width="20%" align="center">Tipo Seguro</th>
            <th nowrap width="20%" align="center">Costo Mensual ($)</th>
			<th nowrap width="20%" align="center">Costo Total ($)</th>
			<th nowrap width="20%" align="center">Proveedor del Servicio</th>
			<th nowrap width="20%" align="center">Cobertura</th>				
          </tr>
          
          <%
           for (int ix=1;ix<10;ix++) {
		    if(!formObj.getField("E01CAECS"+ix).getString().trim().equals("")){
          %> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap align="left"><%=formObj.getFieldString("E01CAECS"+ix)%> - <%=formObj.getFieldString("E01CAEDS"+ix)%> </td>	
			<td nowrap align="right">  <%=formObj.getFieldString("E01CAEMS"+ix)%> </td>
            <td nowrap align="right"> <%=formObj.getFieldString("E01CAETS"+ix)%>  </td>
            <td nowrap align="center"> <%=formObj.getFieldString("E01CAEES"+ix)%>  </td>		            
            <td nowrap align="right"> <%=formObj.getFieldString("E01CAECO"+ix)%>  </td>
            
          </tr> 
   		  <%}
   		   }%>                                                                                                                        
        </table>
        </td>
    </tr>
  </table>    
  
  <h4>IV. Condiciones de Prepago</h4>
  
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center"  class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="30%"> 
              <div align="right">Cargo de Prepago : </div>
            </td>            
            <td nowrap width="70%">Un Mes de Interes </td>            
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Plazo Aviso de Prepago : </div>
            </td>            
            <td nowrap> 3 dias h�biles </td>            
          </tr>          
        </table>
      </td>
    </tr>
  </table>
  <h4>V. Recargo por Atraso</h4>
  
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center"  class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="30%"> 
              <div align="right">Interes Moratorio : </div>
            </td>            
            <td nowrap width="70%"> Tasa M�xima Convencional</td>            
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Gastos de Cobranza : </div>
            </td>            
            <td nowrap> Un 9% para las deudas o cuotas de hasta 10UF</td>            
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">&nbsp;</div>
            </td>            
            <td nowrap> Un 6% para las deudas de 10 y hasta 50UF </td>            
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">&nbsp;</div>
            </td>            
            <td nowrap> Un 3% para deudas de mas de 50UF </td>            
          </tr>                      
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap colspan="2"> 
              <div align="left">El costo total de prepago no considera los recargos por atrasos en el pago del cr�dito</div>
            </td>                       
          </tr>                    
        </table>
      </td>
    </tr>
  </table>
    
  <br>
  <div align="center">  			   
	   <input id="EIBSBTN" type=button name="print" value="Imprimir"    onclick="goPrint();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
       <input id="EIBSBTN" type=button name="Cancel" value="Cancelar"  onclick="goCancel();">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   </div>
                     
  </form>
</body>
</HTML>
