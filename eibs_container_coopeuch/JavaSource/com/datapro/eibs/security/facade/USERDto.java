package com.datapro.eibs.security.facade;

import datapro.eibs.beans.JBObjList;

//import com.datapro.eibs.security.access.jdbc.bean.*;
import com.datapro.eibs.security.bean.*;

/**
 * @author ramaro
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class USERDto {
	
	private CNTRLBTH userBTH;
	private CNTRLEUP userEUP;
	//private CNTRLDBABean userDBA;
	private JBObjList userDBA;
	private JBObjList DTO;
	
	/**
	 * USERDto constructor comment.
	 */

	public USERDto() {
		
		userBTH = new CNTRLBTH();
		userEUP = new CNTRLEUP();
		userDBA = new JBObjList();
		DTO = new JBObjList();
		DTO.addRow(userBTH);
		DTO.addRow(userEUP);
		DTO.addRow(userDBA);
		
	}
	//getter
	public CNTRLBTH getBTH(){
		DTO.setCurrentRow(0);
		userBTH = (CNTRLBTH) DTO.getRecord();
		return userBTH;
	}
	
	public CNTRLEUP getEUP(){
		DTO.setCurrentRow(1);
		userEUP = (CNTRLEUP) DTO.getRecord();
		return userEUP;
	}
	
	public JBObjList getDBA(){
		DTO.setCurrentRow(2);
		userDBA = (JBObjList) DTO.getRecord();
		return userDBA;
	}
	
	public JBObjList getDTO(){		
		return DTO;
	}
	//setter
	public void setBTH( CNTRLBTH newBTH){
		
		DTO.setRecord(newBTH,0);
		
	}
	
	public void setEUP( CNTRLEUP newEUP){
		DTO.setRecord(newEUP,0);
	}
	
	public void setDBA(JBObjList newDBA){
		DTO.setRecord(newDBA,0);
		
	}
	
	public void setDTO(JBObjList newDTO){		
		DTO = newDTO;
	}
}
