<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import="datapro.eibs.beans.EDD240001Message"%>
<%@ page import="datapro.eibs.beans.EDD240001Message"%>
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Mantenimiento Cambio Valor Cuota y Reajuste</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="date" class="java.util.Date" scope="session" />      
<jsp:useBean id= "CapRea" class= "datapro.eibs.beans.EDD240001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">
  function goAction() {	
	document.forms[0].SCREEN.value = '100';	
	document.forms[0].submit();
 } 
 </script>

</head>
<body >
<H3 align="center">Mantenimiento Cambio Valor Cuota y Reajuste<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="EDD2400_capita_rea_enter_mantenance.jsp, EDD2400"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEDD2400">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="300"> 

<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trclear">
             <td nowrap width="30%" align="right"> 
             </td>
             <td  width="1"> 
             	<p>|</p>
			 </td>
             <td nowrap width="25%" align="left">
	  		    <div align="center">&Uacute;LTIMO PROCESO</div>
             </td>  
              <td  width="1"> 
             	<p>|</p>
			 </td>
             <td nowrap width="43%" align="right">
	  		    <div align="center">PR&Oacute;XIMO PROCESO</div>
             </td>
         </tr>

         <tr id="trdark">
             <td nowrap align="right"> 
             	Valor Cuota Participaci&oacute;n : 
             </td>
             <td> 
             	<p>|</p>
			 </td>
             <td nowrap align="left">
             	<%=CapRea.getE01VCMVCUP() %>
 			 </td>                 
             <td> 
             	<p>|</p>
			 </td>
             <td nowrap  align="left"> 
             	<span style="color:blue;"><%=CapRea.getE01VCMVCNP() %></span>
             </td>
         </tr>
    
         <tr id="trclear">
             <td nowrap align="right"> 
             	Fecha Cambio Cuota Participaci&oacute;n : 
             </td>
             <td> 
             	<p>|</p>
			 </td>
             <td nowrap align="left">
                <%=CapRea.getE01VCMFUCD() %> / <%=CapRea.getE01VCMFUCM() %> / <%=CapRea.getE01VCMFUCY() %>
             </td>                 
             <td> 
             	<p>|</p>
			 </td>
             <td nowrap  align="left"> 
                <span style="color:blue;"><%=CapRea.getE01VCMFNCD() %> / <%=CapRea.getE01VCMFNCM() %> / <%=CapRea.getE01VCMFNCY() %></span>
             </td>
         </tr>

         <tr id="trdark">
             <td nowrap align="right"> 
             	Motivo : 
             </td>
             <td> 
             	<p>|</p>
			 </td>
             <td nowrap align="left" valign="middle">
                <%= CapRea.getE01VCMCMUC().trim()%> - <%= CapRea.getE01VCMGMUC().trim()%>
             </td>                 
             <td> 
             	<p>|</p>
			 </td>
             <td nowrap  align="left"> 
                <span style="color:blue;"><%= CapRea.getE01VCMCMNC().trim()%> - <%= CapRea.getE01VCMGMNC().trim()%></span>
             </td>
         </tr>

         <tr id="trclear">
             <td nowrap align="right"> 
             	Fecha Reajustes : 
             </td>
             <td> 
             	<p>|</p>
			 </td>
             <td nowrap align="left">
                <%=CapRea.getE01VCMFURD() %> / <%=CapRea.getE01VCMFURM() %> / <%=CapRea.getE01VCMFURY() %>
             </td>                 
             <td> 
             	<p>|</p>
			 </td>
             <td nowrap  align="left"> 
                <span style="color:blue;"><%=CapRea.getE01VCMFNRD() %> / <%=CapRea.getE01VCMFNRM() %> / <%=CapRea.getE01VCMFNRY() %></span>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
 <br>	
 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trclear">
             <td nowrap align="left"> 
           		<b><p>Favor Confirmar la operaci&oacute;n de actualizaci&oacute;n <%if (!(error.getERNU01().equals("")))  out.print("(Con Advertencias)"); %></p></b>
             </td>
         </tr>
         <%if ( !(error.getERRNUM().equals("0"))  ) { %>
         
          <tr id="trdark">
             <td nowrap align="left" colspan="5">
                <%if (!(error.getERNU01().equals(""))) { %>
           		<p><span style="color:red"><%=error.getERNU01() + " - " + error.getERDS01() %></span></p>
                <%} %>
                <%if (!(error.getERNU02().equals(""))) { %>
           		<p><span style="color:red"><%=error.getERNU02() + " - " + error.getERDS02() %></span></p>
                <%} %>
             </td>
         </tr>
         <%} %>

        </table>
      </td>
    </tr>
  </table>



  <p align="center">
      <input id="EIBSBTN" type="button" name="Cancelar" value="Cancelar" onclick="goAction();">       <input id="EIBSBTN" type=submit name="Submit" value="Confirmar"> 
  </p>
<script language="JavaScript">
  document.forms[0].E01VCMFCAD.focus();
  document.forms[0].E01VCMFCAD.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
