<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="msgcli" class="datapro.eibs.beans.EDET05101Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<title>Mantenedor de Clasificación por Rut</title>


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<SCRIPT LANGUAGE="javascript">
function cancel() {
	document.forms[0].SCREEN.value = 200;
	document.forms[0].submit();
}
</SCRIPT>

</head>
<body>

<%
	if (!error.getERRNUM().equals("0")) {
		out.println("<script type=\"text/javascript\">");
		error.setERRNUM("0");
		out.println("showErrors()");
		out.println("</script>");
	}
%>

<h3 align="center">
Mantenedor de Clasificaci&oacute;n por Rut
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="clasrut_client_maintenance, EDET051"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSEDET051">
<input type="HIDDEN" name="SCREEN" value="400">
<br>

<table id="TBHELPN" width="100%" border="0" cellspacing="0"
	cellpadding="0" style="margin-left: center; margin-right: center;">
	<tr>
		<td align="right" width="25%" nowrap>Rut :&nbsp;</td>
		<td  width="25%">
           	<input type="text" id="rut" name="E01INDRUT" size="27"  value="<%= userPO.getID().trim()%>" readonly>
		</td>
		<td align="right" width="25%" nowrap>Clasificaci&oacute;n :&nbsp;</td>
		<td  width="25%">
			<select name="E01INDID1">   
            	<option value="A1" <% if (msgcli.getE01INDID1().equals("A1")) out.print("selected"); %>>A1</option>                   
           		<option value="A2" <% if (msgcli.getE01INDID1().equals("A2")) out.print("selected"); %>>A2</option>
          		<option value="A3" <% if (msgcli.getE01INDID1().equals("A3")) out.print("selected"); %>>A3</option>
          		<option value="A4" <% if (msgcli.getE01INDID1().equals("A4")) out.print("selected"); %>>A4</option>
          		<option value="A6" <% if (msgcli.getE01INDID1().equals("A6")) out.print("selected"); %>>A6</option>
          		<option value="B" <% if (msgcli.getE01INDID1().equals("B")) out.print("selected"); %>>B</option>
          		<option value="B1" <% if (msgcli.getE01INDID1().equals("B1")) out.print("selected"); %>>B1</option>
          		<option value="C1" <% if (msgcli.getE01INDID1().equals("C1")) out.print("selected"); %>>C1</option>
          		<option value="C2" <% if (msgcli.getE01INDID1().equals("C2")) out.print("selected"); %>>C2</option>
          		<option value="C3" <% if (msgcli.getE01INDID1().equals("C3")) out.print("selected"); %>>C3</option>
          		<option value="C4" <% if (msgcli.getE01INDID1().equals("C4")) out.print("selected"); %>>C4</option>
          		<option value="C5" <% if (msgcli.getE01INDID1().equals("C5")) out.print("selected"); %>>C5</option>
          		<option value="C6" <% if (msgcli.getE01INDID1().equals("C6")) out.print("selected"); %>>C6</option>
          		<option value="D1" <% if (msgcli.getE01INDID1().equals("D1")) out.print("selected"); %>>D1</option>
          		<option value="D2" <% if (msgcli.getE01INDID1().equals("D2")) out.print("selected"); %>>D2</option>
	  		</select>  
		</td>
	</tr>
</table>

  <p align="center">
  	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel()">  	
  </p>

</form>
</body>
</html>
 