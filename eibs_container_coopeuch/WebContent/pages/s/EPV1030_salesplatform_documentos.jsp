<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="platfObj" class="datapro.eibs.beans.EPV100502Message"  scope="session" />
<jsp:useBean id="document" class="datapro.eibs.beans.EPV103001Message"  scope="session" />
<jsp:useBean id="EPV101003List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<jsp:useBean id="EPV100503List" class="datapro.eibs.beans.JBObjList" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script type="text/javascript">
	function aprobarTodos(){
        <%
        String sx1="";
        for (int ix=1; ix<30; ix++) {
        	if (ix<10) sx1="0"+ix; else sx1=""+ix;
		    if(!document.getField("E01DCOD"+sx1).getString().trim().equals("")){
          %>
          		document.forms[0].<%="E01DSTS"+sx1%>[0].checked = true;             
   		  <%}
   		}%>
	}
	
	
function UpdateMotivo(check) 
{
   if(check.checked==0)
   {
	InpSinExc = document.getElementById('E01SINEXC');
	InpSinExc.value = "N";  
	
	fila = document.getElementById('listexep');
	fila.style.display = "";  
	limpiarMotivoAll();
   } 
	else 
   {
	InpSinExc = document.getElementById('E01SINEXC');
	InpSinExc.value = "S";  
	
	fila = document.getElementById('listexep');
	fila.style.display = "none";  
   } 
}  

function limpiarMotivo(cod, descod) 
{

	codmot = document.getElementById(cod);
	desmot = document.getElementById(descod);
	if(codmot.value=="0000" || codmot.value=="0060" )
	{
		codmot.value="";
		desmot.value="";
	}	
} 

function limpiarMotivoAll(cod, descod) 
{
	codmot = document.getElementById('E01EXEP01');
	desmot = document.getElementById('E01EXED01');
    codmot.value="";
    desmot.value="";

	codmot = document.getElementById('E01EXEP02');
	desmot = document.getElementById('E01EXED02');
    codmot.value="";
    desmot.value="";

	codmot = document.getElementById('E01EXEP03');
	desmot = document.getElementById('E01EXED03');
    codmot.value="";
    desmot.value="";

	codmot = document.getElementById('E01EXEP04');
	desmot = document.getElementById('E01EXED04');
    codmot.value="";
    desmot.value="";

	codmot = document.getElementById('E01EXEP05');
	desmot = document.getElementById('E01EXED05');
    codmot.value="";
    desmot.value="";
} 

</script>

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center"> Plataforma de Ventas - Visado de Documentos<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_documentos.jsp,JSEPV1030"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1030">
<input type="hidden" name="SCREEN" value="600"> 
<input type=HIDDEN name="E01PVMBNK"  value="<%= document.getE01PVMBNK().trim()%>">
<input type=HIDDEN name="E01CCNOFC"  value="<%= document.getE01CCNOFC().trim()%>">

 <% int row = 0;%>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="document" property="E01PVMCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Nombre :</td>
             <td nowrap align="left" width="40%">
	  			<eibsinput:text name="document" property="E01CUSNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="document" property="E01PVMNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="40%">
    	        <eibsinput:date name="document" fn_year="E01PVMODY" fn_month="E01PVMODM" fn_day="E01PVMODD" readonly="true" readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Sucursal :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="document" property="E01PVMBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
             </td>
             <td nowrap align="right" width="20%"> Ejecutivo :</td>
             <td nowrap align="left" width="40%">
	  			<eibsinput:text name="document" property="E01PVMOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>" readonly="true"/>
             </td>
         </tr>         
        </table>
      </td>
    </tr>
  </table>

<%if(document.getE01LNTYPG().equals("T")){%> 

  <table class=tbenter>
   <tr > 
      <td nowrap> 
		  <h4>Informacion Tarjeta de Credito</h4>
      </td>
      <td nowrap align=right> 
   		<b>Estado :</b>
      </td>
      <td nowrap> 
		  <INPUT TYPE=HIDDEN NAME="E01DSCSTS" VALUE="<%=document.getE01DSCSTS()%>">  
   		<b><font color="#ff6600"><%= document.getE01DSCSTS().trim()%></font></b>
      </td>
    </tr>
  </table>

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="document" property="E01LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="document" property="E01DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Direccion :</div>
            </td>
            <td nowrap width="30%">
              <input type="text" name="E01TCCMLA" size="3" maxlength="2" value="<%= document.getE01TCCMLA().trim()%>" readonly >
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Local :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01TCCUMB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Extranjera :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01TCCUME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
          </tr>                   
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Dia de Pago :</div>
            </td>
            <td nowrap width="30%">             
				<select name="E01TCDYPG" disabled>
					<option></option>
					<option value="3" <% if (document.getE01TCDYPG().equals("3")) out.print("selected");%>>3</option>
					<option value="23" <% if (document.getE01TCDYPG().equals("23")) out.print("selected");%>>23</option>
				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Porcentaje de Pago :</div>
            </td>
            <td nowrap width="30%">            
				<select name="E01TCPRPG" disabled>
					<option></option>
					<option value="5" <% if (document.getE01TCPRPG().equals("5")) out.print("selected");%>>El 5%</option>
					<option value="100" <% if (document.getE01TCPRPG().equals("100")) out.print("selected");%>>El 100%</option>
				</select>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">
				<select name="E01TCMEPG" disabled>
					<option></option>
					<option value="1" <% if (document.getE01TCMEPG().equals("1")) out.print("selected");%>>Con Cuenta</option>
					<option value="2" <% if (document.getE01TCMEPG().equals("2")) out.print("selected");%>>Sin Cuenta</option>
				</select>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:text name="document" property="E01TCACPG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
            </td>
          </tr>  

        </table>
      </td>
    </tr>
  </table>

<%} else {%> 

  <table class=tbenter>
   <tr > 
      <td nowrap> 
		  <h4>Informacion del Credito</h4>
      </td>
      <td nowrap align=right> 
   		<b>Estado :</b>
      </td>
      <td nowrap> 
		  <INPUT TYPE=HIDDEN NAME="E01DSCSTS" VALUE="<%=document.getE01DSCSTS()%>">  
   		<b><font color="#ff6600"><%= document.getE01DSCSTS().trim()%></font></b>
      </td>
    </tr>
  </table>

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Tipo de Producto :</div>
            </td>
            <td nowrap width="30%" >
            	<eibsinput:text name="document" property="E01LNTYPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>	
            	<eibsinput:text name="document" property="E01DSTYPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Termino del Contrato :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNTERM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" readonly="true" /> 
	            <input type="text" readonly name="E01LNTERC" size="10" 
				  value="<% if (document.getE01LNTERC().equals("D")) out.print("D&iacute;a(s)");
							else if (document.getE01LNTERC().equals("M")) out.print("Mes(es)");
							else if (document.getE01LNTERC().equals("Y")) out.print("A&ntilde;o(s)");
							else out.print("");%>" >
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="document" property="E01LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="document" property="E01DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Valor de la Cuota :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01LNCUAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
          </tr>                 
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Monto del Prestamo :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01LNOAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Monto Liquido :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="document" property="E01LNNAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
          </tr>  
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Primer Pago :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:date name="document" fn_year="E01LNPXPY" fn_month="E01LNPXPM" fn_day="E01LNPXPD" readonly="true" />
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
            </td>
            <td nowrap width="30%">
            	&nbsp; 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td> 
            <td nowrap width="30%">           
				<select name="E01LNPVIA" <%="disabled"%>>
					<option value=""
						<% if (document.getE01LNPVIA().equals("")) out.print("selected");%>>Caja</option>
					<option value="1"
						<% if (document.getE01LNPVIA().equals("1")) out.print("selected");%>>PAC</option>							            
					<option value="2"
						<% if (document.getE01LNPVIA().equals("2")) out.print("selected");%>>Convenio</option>
					<option value="4"
						<% if (document.getE01LNPVIA().equals("4")) out.print("selected");%>>PAC/Multibanco</option>				
				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:text name="document" property="E01LNPACC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
            </td>
          </tr>
          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Promociones :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01DSPROM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tasa Promoción:</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNTSPO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/>
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Descuentos :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01DSDESC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">% Descuento :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNTSDS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
          </tr>  
          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Codigo Convenio :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNCONV" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right"> % descuento del convenio : </div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01LNDRAT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Tasa de Interes :</div>
            </td>
            <td nowrap width="30%">
            	 <eibsinput:text name="document" property="E01LNRATE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
            </td>
            <td nowrap width="30%">
            	&nbsp;
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Holgura:</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="document" property="E01PVMOLA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">&nbsp;</div>
            </td>
            <td nowrap width="30%">
				&nbsp;            	
            </td>
          </tr>                       

        </table>
      </td>
    </tr>
  </table>
<%} %>   

 <h4>Rentas</h4>
  <%row=0; %>
  <table class="tableinfo">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">     
           <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'>  
            <td nowrap width="27%"> 
              <div align="right">Renta Bruta :</div>
            </td>
            <td nowrap width="21%"> 
			  <eibsinput:text name="platfObj" property="E02PVMRBR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" required="true"  onchange="setRecalculate()" />              
            </td>
            <td nowrap width="29%"><div align="right">Renta Liquida :</div></td>
            <td nowrap width="23%"><eibsinput:text name="platfObj"
					property="E02PVMICU"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" required="true"
					onchange="setRecalculate()" /></td>
          </tr>          
           <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td nowrap> 
              <div align="right">Renta Depurada :</div>
            </td>
            <td nowrap> 
				<eibsinput:text name="platfObj" property="E02PVMRDE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" required="true"  onchange="setRecalculate()" />            
            </td>
            <td nowrap> <div align="right">Otros Ingresos :</div></td>
            <td nowrap><eibsinput:text name="platfObj"
					property="E02PVMOIN"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" required="true"
					onchange="setRecalculate()" /></td>
          </tr>           
           <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'>  
            <td nowrap> 
              <div align="right">Renta Neta Conyugue :</div>
            </td>
            <td nowrap>			
				<eibsinput:text name="platfObj" property="E02PVMRNY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" required="false" readonly="true"/> 				
			</td>
            <td nowrap> <div align="right">Renta Total :</div></td>
            <td nowrap><eibsinput:text name="platfObj"
					property="E02PVMRTO"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					required="false" readonly="true" /></td>
            
          </tr>                                            
        </table>                       
        </td>
    </tr>
  </table>


  <h4>Documentos Requeridos</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <th nowrap width="5%" align="center">Codigo</th>
            <th nowrap width="30%" align="left">Descripcion </th>
            <th nowrap width="10%" align="center">Estatus  <input id="EIBSBTN" type="button" name="aprobarT" value="Aprobar Todos" onclick="aprobarTodos();"> </th>
            <th nowrap width="50%" align="center">Comentario</th>
          </tr>
 
          <%
           String sx="";
           for (int ix=1; ix<30; ix++) {
            if (ix<10) sx="0"+ix; else sx=""+ix;
		    if(!document.getField("E01DCOD"+sx).getString().trim().equals("")){
          %> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="5%" align="center"> <%=document.getFieldString("E01DCOD"+sx)%> <input type=HIDDEN name="<%="E01DCOD"+sx%>"  value="<%=document.getFieldString("E01DCOD"+sx) %>"> </td>
            <td nowrap width="30%" align="left">  <%=document.getFieldString("E01DDES"+sx)%> <input type=HIDDEN name="<%="E01DDES"+sx%>"  value="<%=document.getFieldString("E01DDES"+sx) %>"> </td>
            <td nowrap width="10%" align="left">
 		       <p> 
                 <input type="radio" name="<%="E01DSTS"+sx%>"  value="A" <%if (document.getField("E01DSTS"+sx).getString().trim().equals("A")) out.print("checked"); %>>
                  Aprobar <br>
                 <input type="radio" name="<%="E01DSTS"+sx%>"  value="R" <%if (document.getField("E01DSTS"+sx).getString().trim().equals("R")) out.print("checked"); %>>
                   Rechazar 
               </p> 
        	</td>
            <td nowrap width="50%" align="center">
            <textarea name="<%="E01DRMK"+sx%>"" cols="100" rows="3" ><%=document.getFieldString("E01DRMK"+sx)%> </textarea>
        	</td>
          </tr> 
   		  <%}
   		   }%>
        </table>
        </td>
    </tr>
  </table>    


  <h4>Excepciones</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" align="right">
		       <input type="checkbox" name="chk1" value="" onClick="UpdateMotivo(this)" <% if (document.getE01SINEXC().trim().equals("S")) out.print(" checked");%>>
			   <input type="hidden"  id="E01SINEXC" name="E01SINEXC"  value="<% if (document.getE01SINEXC().trim().equals("S")) out.print("S"); else out.print("N"); %>">
        	</td>
            <td nowrap width="30%" align="right">
				<div align="left"><b>Sin Excepciones</b/></div>        	</td>
  		    <td nowrap width="50%" align="left">       	 

        	</td>
          </tr> 
			
        </table>
        </td>
    </tr>
    
    <tr id='listexep' <% if (document.getE01SINEXC().trim().equals("S")) out.print(" style=\"display: none\"");%>> 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
            </td>
            <td nowrap width="30%"> 
              <div align="right">Motivo de Excepci&oacute;n :</div>
            </td>
            
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01EXEP01" size="4" maxlength="3" value="<%= document.getE01EXEP01().trim()%>" onfocus="limpiarMotivo('E01EXEP01','E01EXED01' );" readonly style="text-align:right;">
                <input type="text" name="E01EXED01" size="50" maxlength="3" value="<%= document.getE01EXED01().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNOFC('E01EXEP01','E01EXED01','<%=document.getE01CCNOFC() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
            </td>
            <td nowrap> 
              <div align="right">Motivo de Excepci&oacute;n :</div>
            </td>
            
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01EXEP02" size="4" maxlength="3" value="<%= document.getE01EXEP02().trim()%>" onfocus="limpiarMotivo('E01EXEP02','E01EXED02' );" readonly style="text-align:right;">
                <input type="text" name="E01EXED02" size="50" maxlength="3" value="<%= document.getE01EXED02().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNOFC('E01EXEP02','E01EXED02','<%=document.getE01CCNOFC() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
            </td>
            <td nowrap> 
              <div align="right">Motivo de Excepci&oacute;n :</div>
            </td>
            
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01EXEP03" size="4" maxlength="3" value="<%= document.getE01EXEP03().trim()%>" onfocus="limpiarMotivo('E01EXEP03','E01EXED03' );" readonly style="text-align:right;">
                <input type="text" name="E01EXED03" size="50" maxlength="3" value="<%= document.getE01EXED03().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNOFC('E01EXEP03','E01EXED03','<%=document.getE01CCNOFC() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
            </td>
            <td nowrap> 
              <div align="right">Motivo de Excepci&oacute;n :</div>
            </td>
            
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01EXEP04" size="4" maxlength="3" value="<%= document.getE01EXEP04().trim()%>" onfocus="limpiarMotivo('E01EXEP04','E01EXED04' );" readonly style="text-align:right;">
                <input type="text" name="E01EXED04" size="50" maxlength="3" value="<%= document.getE01EXED04().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNOFC('E01EXEP04','E01EXED04','<%=document.getE01CCNOFC() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
            </td>
            <td nowrap> 
              <div align="right">Motivo de Excepci&oacute;n :</div>
            </td>
            
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01EXEP05" size="4" maxlength="3" value="<%= document.getE01EXEP05().trim()%>" onfocus="limpiarMotivo('E01EXEP05','E01EXED05' );" readonly style="text-align:right;">
                <input type="text" name="E01EXED05" size="50" maxlength="3" value="<%= document.getE01EXED05().trim()%> "  readonly >
                <a href="javascript:GetCodeDescCNOFC('E01EXEP05','E01EXED05','<%=document.getE01CCNOFC() %>')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 
        </table>
        </td>
    </tr>
  </table>    



   <div align="center">
	   <input id="EIBSBTN" type="submit"" name="Enviar" value="Enviar"> 
	                     
   </div>
 
</form>

</body>
</html>
