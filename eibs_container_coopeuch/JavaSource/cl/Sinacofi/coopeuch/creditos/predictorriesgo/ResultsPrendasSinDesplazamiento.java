/**
 * ResultsPrendasSinDesplazamiento.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResultsPrendasSinDesplazamiento  implements java.io.Serializable {
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.PrendasSinDesplazamiento[] prendasSinDesplazamiento;

    public ResultsPrendasSinDesplazamiento() {
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.PrendasSinDesplazamiento[] getPrendasSinDesplazamiento() {
        return prendasSinDesplazamiento;
    }

    public void setPrendasSinDesplazamiento(cl.Sinacofi.coopeuch.creditos.predictorriesgo.PrendasSinDesplazamiento[] prendasSinDesplazamiento) {
        this.prendasSinDesplazamiento = prendasSinDesplazamiento;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.PrendasSinDesplazamiento getPrendasSinDesplazamiento(int i) {
        return prendasSinDesplazamiento[i];
    }

    public void setPrendasSinDesplazamiento(int i, cl.Sinacofi.coopeuch.creditos.predictorriesgo.PrendasSinDesplazamiento value) {
        this.prendasSinDesplazamiento[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsPrendasSinDesplazamiento history = (ResultsPrendasSinDesplazamiento) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ResultsPrendasSinDesplazamiento other = (ResultsPrendasSinDesplazamiento) obj;
        boolean _equals;
        _equals = true
            && ((this.prendasSinDesplazamiento==null && other.getPrendasSinDesplazamiento()==null) || 
             (this.prendasSinDesplazamiento!=null &&
              java.util.Arrays.equals(this.prendasSinDesplazamiento, other.getPrendasSinDesplazamiento())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsPrendasSinDesplazamiento history = (ResultsPrendasSinDesplazamiento) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getPrendasSinDesplazamiento() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPrendasSinDesplazamiento());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPrendasSinDesplazamiento(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
