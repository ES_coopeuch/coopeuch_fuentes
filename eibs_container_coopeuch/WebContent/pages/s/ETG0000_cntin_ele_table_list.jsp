<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Table de Codigos del Sistema</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ETG000000HelpEle" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "ETG000000Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="refCodes" class="datapro.eibs.beans.ETG000000Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<script language="JavaScript">



function goAction(op) {

	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}


function cancel() {
	document.forms[0].SCREEN.value = 100;
	document.forms[0].submit();
}


function goDelete() {

	if(confirm("Esta seguro que desea borrar este codigo?")){
		document.forms[0].opt.value = 3;
		document.forms[0].submit();
	}
}

</SCRIPT>  

</head>

<BODY>
<h3 align="center">Elementos Tablas de C&oacute;digos del Sistema<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cntin_ele_table_list.jsp, ETG0000"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSETG0000" >
  <p> 
    <input type=HIDDEN name="SCREEN" value="1800">
    <input type=HIDDEN name="totalRow" value="0">
    <input type=HIDDEN name="opt"> 
    
    <input type=HIDDEN name="ETG00FLD01" value="<%=refCodes.getETG00FLD01().trim()%>">
    <input type=HIDDEN name="ETG00NOM01" value="<%= refCodes.getETG00NOM01().trim()%>">
    <input type=HIDDEN name="ETG00FLD02" value="<%=refCodes.getETG00FLD02().trim()%>">
    <input type=HIDDEN name="ETG00NOM02" value="<%= refCodes.getETG00NOM02().trim()%>">
    <input type=HIDDEN name="ETG00FLD03" value="<%=refCodes.getETG00FLD03().trim()%>">
    <input type=HIDDEN name="ETG00NOM03" value="<%= refCodes.getETG00NOM03().trim()%>">
    <input type=HIDDEN name="ETG00FLD04" value="<%=refCodes.getETG00FLD04().trim()%>">
    <input type=HIDDEN name="ETG00NOM04" value="<%= refCodes.getETG00NOM04().trim()%>">
    <input type=HIDDEN name="ETG00FLD05" value="<%=refCodes.getETG00FLD05().trim()%>">
    <input type=HIDDEN name="ETG00NOM05" value="<%= refCodes.getETG00NOM05().trim()%>">
    <input type=HIDDEN name="ETG00FLD06" value="<%=refCodes.getETG00FLD06().trim()%>">
    <input type=HIDDEN name="ETG00NOM06" value="<%= refCodes.getETG00NOM06().trim()%>">
    <input type=HIDDEN name="ETG00FLD07" value="<%=refCodes.getETG00FLD07().trim()%>">
    <input type=HIDDEN name="ETG00NOM07" value="<%= refCodes.getETG00NOM07().trim()%>">
    <input type=HIDDEN name="ETG00FLD08" value="<%=refCodes.getETG00FLD08().trim()%>">
    <input type=HIDDEN name="ETG00NOM08" value="<%= refCodes.getETG00NOM08().trim()%>">
    <input type=HIDDEN name="ETG00FLD09" value="<%=refCodes.getETG00FLD09().trim()%>">
    <input type=HIDDEN name="ETG00NOM09" value="<%= refCodes.getETG00NOM09().trim()%>">
    <input type=HIDDEN name="ETG00FLD10" value="<%=refCodes.getETG00FLD10().trim()%>">
    <input type=HIDDEN name="ETG00NOM10" value="<%= refCodes.getETG00NOM10().trim()%>">
    
  </p>
  
  <p> 
  <h4>Par&aacute;metros de Definici&oacute;n de Tablas</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Tabla :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="ETG00ELE" size="9" maxlength="8" value="<%= refCodes.getETG00ELE().trim()%>" readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Nombre o T&iacute;tulo :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="ETG00DESCL" size="66" maxlength="58" value="<%= refCodes.getETG00DESCL().trim()%>"  readonly>
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Tipo C&oacute;digo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG00TCOD" size="15" value="<%if(refCodes.getETG00TCOD().equals("N")) out.print("N&uacute;merico"); else out.print("Alfan&uacute;merico"); %>" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="right">
              C&oacute;digo Menor Aceptado :
              </div>
            </td>
            <td nowrap> 
              <div align="left"> 
              	<input type="text" name="ETG00NROM" size="3" maxlength="2" value="<%= refCodes.getETG00NROM().trim()%>" readonly style="text-align:right;">
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">
              Es Tabla Usuario :
              </div>
            </td>
            <td nowrap> 
              <div align="left"> 
              	<input type="text" name="ETG00TUSR" size="2" maxlength="1" value="<%= refCodes.getETG00TUSR().trim()%>" readonly >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">C&oacute;digo de Grupo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG00TGRP" size="4" maxlength="3" value="<%= refCodes.getETG00TGRP().trim()%>" readonly style="text-align:right;">
                <input type="text" name="ETG00TGRD" size="50" maxlength="3" value="<%= refCodes.getETG00TGRD().trim()%> "  readonly >
              </div>
            </td>
          </tr> 
          
          <tr id="trclear"> 
            <td nowrap  height="23"> 
              <div align="right">
              Formato :
              </div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="ETG00FMTO" size="2" maxlength="1" value="<%= refCodes.getETG00FMTO().trim()%>" readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap height="23"> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

    <%
	if ( ETG000000HelpEle.getNoResult() ) {
 %>
  </p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
          <table class="tbenter" width=100% align=center>
            <tr> 
              <td class=TDBKG width="50%"> 
                <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
              </td>
              <td class=TDBKG width="50%"> 
                <div align="center"><a href="javascript:cancel()"><b>Volver</b></a></div>
              </td>
            </tr>
          </table>
          <p>&nbsp;</p>
          
        </div>

	  </TD>
	</TR>
    </TABLE>
	
  <%  
		}
	else {
%> <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }

%> 
 
          
  <table class="tbenter" width=100% align=center height="8%">
    <tr> 
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
      </td>
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(2)"><b>Modificar</b></a></div>
      </td>
	<td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goDelete(3)"><b>Borrar</b></a></div>
      </td>      
	<td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:cancel()"><b>Volver</b></a></div>
      </td>      
    </tr>
  </table>
  <br>
 
 
  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
            <th align=CENTER nowrap width="10%"><%= refCodes.getETG00NOMCD()%></th>
            <th align=CENTER nowrap width="2%"></th>

			<% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
           	 <th align=LEFT nowrap><%= refCodes.getETG00NOM01()%></th>
			<% } %>

			<% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
            <th align=LEFT nowrap ><%= refCodes.getETG00NOM02()%></th>
			<% } %>

			<% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
            <th align=LEFT nowrap ><%= refCodes.getETG00NOM03()%></th>
			<% } %>

			<% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
            <th align=LEFT nowrap ><%= refCodes.getETG00NOM04()%></th>
			<% } %>

			<% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
            <th align=LEFT nowrap ><%= refCodes.getETG00NOM05()%></th>
			<% } %>

			<% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
            <th align=LEFT nowrap ><%= refCodes.getETG00NOM06()%></th>
			<% } %>

			<% if (refCodes.getETG00FLD07().trim().equals("Y")){ %>
            <th align=LEFT nowrap ><%= refCodes.getETG00NOM07()%></th>
			<% } %>

			<% if (refCodes.getETG00FLD08().trim().equals("Y")){ %>
            <th align=LEFT nowrap ><%= refCodes.getETG00NOM08()%></th>
			<% } %>

			<% if (refCodes.getETG00FLD09().trim().equals("Y")){ %>
            <th align=LEFT nowrap ><%= refCodes.getETG00NOM09()%></th>
			<% } %>

			<% if (refCodes.getETG00FLD10().trim().equals("Y")){ %>
            <th align=LEFT nowrap ><%= refCodes.getETG00NOM10()%></th>
			<% } %>

			<% if (refCodes.getETG00FLD10().trim().equals("Y")){ %>
            <th align=LEFT nowrap ><%= refCodes.getETG00NOM10()%></th>
			<% } %>

          </tr>
 
           <%
                ETG000000HelpEle.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (ETG000000HelpEle.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	
                  	if(refCodes.getETG00FMTO().equals("1"))
                  	{
                  		datapro.eibs.beans.ETG000001Message msgList = (datapro.eibs.beans.ETG000001Message) ETG000000HelpEle.getRecord();
					 %>
        				  <tr id="dataTable<%= ETG000000HelpEle.getCurrentRow() %>"> 
           				  		<td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE2" value="<%= ETG000000HelpEle.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>
								
								
								<td align="<%if(refCodes.getETG00TCOD().equals("N")) out.print("RIGHT"); else  out.print("LEFT"); %>" nowrap width="10%">
									<%=msgList.getETG01ELE() %>
								</td>

								<td align=LEFT nowrap ></td>

								<% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG01DESCL()%></td>
								<% } %>
								<% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG01VAL01()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG01VAL02()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG01ALFA1()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG01ALFA2()%></td>
								<% } %>
								
								<% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG01VAL03()%></td>
								<% } %>
          				  </tr>
	          	<%  } 

                  	if(refCodes.getETG00FMTO().equals("2"))
                  	{
                  		datapro.eibs.beans.ETG000002Message msgList = (datapro.eibs.beans.ETG000002Message) ETG000000HelpEle.getRecord();
					 %>
        				  <tr id="dataTable<%= ETG000000HelpEle.getCurrentRow() %>"> 
           				  		<td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE2" value="<%= ETG000000HelpEle.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>

								<td align="<%if(refCodes.getETG00TCOD().equals("N")) out.print("RIGHT"); else  out.print("LEFT"); %>" nowrap width="10%">
									<%=msgList.getETG02ELE() %>
								</td>

								<td align=LEFT nowrap ></td>

								<% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG02DESCL()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG02ALFA1()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG02ALFA2()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG02ALFA3()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG02ALFA4()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG02VAL01()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD07().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG02VAL02()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD08().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG02ALFA5()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD09().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG02ALFA6()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD10().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG02VAL03()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD10().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG02VAL04()%></td>
								<% } %>

          				  </tr>
	          	<%  } 
	          	
                  	if(refCodes.getETG00FMTO().equals("3"))
                  	{
                  		datapro.eibs.beans.ETG000003Message msgList = (datapro.eibs.beans.ETG000003Message) ETG000000HelpEle.getRecord();
					 %>
        				  <tr id="dataTable<%= ETG000000HelpEle.getCurrentRow() %>"> 
           				  		<td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE2" value="<%= ETG000000HelpEle.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>

								<td align="<%if(refCodes.getETG00TCOD().equals("N")) out.print("RIGHT"); else  out.print("LEFT"); %>" nowrap width="10%">
									<%=msgList.getETG03ELE() %>
								</td>

								<td align=LEFT nowrap ></td>

								<% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG03DATO()%></td>
								<% } %>

          				  </tr>
	          	<%  } 
	          	
                  	if(refCodes.getETG00FMTO().equals("4"))
                  	{
                  		datapro.eibs.beans.ETG000004Message msgList = (datapro.eibs.beans.ETG000004Message) ETG000000HelpEle.getRecord();
					 %>
        				  <tr id="dataTable<%= ETG000000HelpEle.getCurrentRow() %>"> 
           				  		<td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE2" value="<%= ETG000000HelpEle.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>

								<td align="<%if(refCodes.getETG00TCOD().equals("N")) out.print("RIGHT"); else  out.print("LEFT"); %>" nowrap width="10%">
									<%=msgList.getETG04ELE() %>
								</td>

								<td align=LEFT nowrap ></td>

								<% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG04DESCL()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG04VAL01()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG04VAL02()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG04VAL03()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG04VAL04()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG04VAL05()%></td>
								<% } %>

          				  </tr>
	          	<%  } 
	          	
                  	if(refCodes.getETG00FMTO().equals("5"))
                  	{
                  		datapro.eibs.beans.ETG000005Message msgList = (datapro.eibs.beans.ETG000005Message) ETG000000HelpEle.getRecord();
					 %>
        				  <tr id="dataTable<%= ETG000000HelpEle.getCurrentRow() %>"> 
           				  		<td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE2" value="<%= ETG000000HelpEle.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>

								<td align="<%if(refCodes.getETG00TCOD().equals("N")) out.print("RIGHT"); else  out.print("LEFT"); %>" nowrap width="10%">
									<%=msgList.getETG05ELE() %>
								</td>

								<td align=LEFT nowrap ></td>

								<% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG05DESCL()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG05DESCC()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG05ALFA1()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG05ALFA2()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG05ALFA3()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG05VAL01()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD07().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG05VAL02()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD08().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG05VAL03()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD09().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG05VAL04()%></td>
								<% } %>

          				  </tr>
	          	<%  } 
	          	
                  	if(refCodes.getETG00FMTO().equals("6"))
                  	{
                  		datapro.eibs.beans.ETG000006Message msgList = (datapro.eibs.beans.ETG000006Message) ETG000000HelpEle.getRecord();
					 %>
        				  <tr id="dataTable<%= ETG000000HelpEle.getCurrentRow() %>"> 
           				  		<td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE2" value="<%= ETG000000HelpEle.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>

								<td align="<%if(refCodes.getETG00TCOD().equals("N")) out.print("RIGHT"); else  out.print("LEFT"); %>" nowrap width="10%">
									<%=msgList.getETG06ELE() %>
								</td>

								<td align=LEFT nowrap ></td>

								<% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG06VAL01()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG06VAL02()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG06VAL03()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG06VAL04()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
           							 <td align="right" nowrap ><%= msgList.getETG06VAL05()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD06().trim().equals("Y")){ %>
           							 <td align="right"  nowrap ><%= msgList.getETG06VAL06()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD07().trim().equals("Y")){ %>
           							 <td align="right"  nowrap ><%= msgList.getETG06VAL07()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD08().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG06ALFA1()%></td>
								<% } %>
          				  </tr>
	          	<%  } 
	          	
                  	if(refCodes.getETG00FMTO().equals("7"))
                  	{
                  		datapro.eibs.beans.ETG000007Message msgList = (datapro.eibs.beans.ETG000007Message) ETG000000HelpEle.getRecord();
					 %>
        				  <tr id="dataTable<%= ETG000000HelpEle.getCurrentRow() %>"> 
           				  		<td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE2" value="<%= ETG000000HelpEle.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>

								<td align="<%if(refCodes.getETG00TCOD().equals("N")) out.print("RIGHT"); else  out.print("LEFT"); %>" nowrap width="10%">
									<%=msgList.getETG07ELE() %>
								</td>

								<td align=LEFT nowrap ></td>

								<% if (refCodes.getETG00FLD01().trim().equals("Y")){ %>
           							 <td align=LEFT nowrap ><%= msgList.getETG07DESCC()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD02().trim().equals("Y")){ %>
           							 <td align="left"  nowrap ><%= msgList.getETG07CAM01()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD03().trim().equals("Y")){ %>
           							 <td align="right"  nowrap ><%= msgList.getETG07VAL01()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD04().trim().equals("Y")){ %>
           							 <td align="right"  nowrap ><%= msgList.getETG07VAL02()%></td>
								<% } %>

								<% if (refCodes.getETG00FLD05().trim().equals("Y")){ %>
           							 <td align="right"  nowrap ><%= msgList.getETG07VAL03()%></td>
								<% } %>

          				  </tr>
	          	<%  } %>

          	<% } %>
              </table>
              </td>
              </tr>
  </table>
  
  <TABLE  class="tbenter" WIDTH="98%" ALIGN="center"  height="10%">
  	<TR>
  		 <TD WIDTH="50%" ALIGN=LEFT height="25">
  		 <% 
        	if ( ETG000000HelpEle.getShowPrev() ) {
      			int pos = ETG000000HelpEle.getFirstRec() - 51;
      		    out.print("<A HREF=\"" + request.getContextPath() + "/servlet/datapro.eibs.params.JSETG0000?SCREEN=1100&FromRecord=" + pos + "\" ><img src=\""+request.getContextPath()+"/images/s/previous_records.gif\" border=0></A>");
        	}
  		 %>
  		 </TD>
  		 <TD WIDTH="50%" ALIGN=RIGHT height="25">
  		 <% 
        	if ( ETG000000HelpEle.getShowNext() ) {
      			int pos = ETG000000HelpEle.getLastRec();
      		    out.print("<A HREF=\"" + request.getContextPath() + "/servlet/datapro.eibs.params.JSETG0000?SCREEN=1100&FromRecord=" + pos + "\" ><img src=\""+request.getContextPath()+"/images/s/next_records.gif\" border=0></A>");
        	}
  		 %>
  		 </TD>
  	</TR>
  </TABLE>
   

<SCRIPT language="JavaScript">
	showChecked("CURRCODE2");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

</body>
</html>
