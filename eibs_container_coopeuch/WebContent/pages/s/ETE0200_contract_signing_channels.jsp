<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Informacion Basica</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "consig" class= "datapro.eibs.beans.ETE020001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<SCRIPT Language="Javascript">

 function goAction(op) {
	  document.forms[0].SCREEN.value = op;
      document.forms[0].submit();
 } 

  	function goView(pdf) {
  		var operacion = document.forms[0].E01ETCUN.value;
    	var page = '<%=request.getContextPath()%>/servlet/datapro.eibs.tefmulti.JSETE0200A?SCREEN=100&action=1&copies=1&E02ETCUN='+ operacion +'&pdfName=' + pdf;
		CenterNamedWindow(page,'pdf',600,500,7);
  	}

</SCRIPT>


</head>

<body bgcolor="#FFFFFF">

 <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Firma Contrato Canales Persona<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="contract_signing_channels, ETE0200"  ></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.tefmulti.JSETE0200" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="100">
 
  <h4>Datos Contrato</h4>
    <div align="left">         
    <table class="tableinfo" >
      <tr > 
          <td nowrap > 
          <div align="center"> 
            <table cellspacing="0" cellpadding="2" width="100%" border="0">
              <tr id="trdark"> 
                <td nowrap width="25%"> 
                  <div align="right">Numero Cliente :</div>
                </td>
                <td nowrap> 
                  <input type="text" name="E01ETCUN" size="15" maxlength="10" value="<%= consig.getE01ETCUN().trim()%>" readonly>
                </td>  
               </tr>
              <tr id="trclear"> 
                <td nowrap width="25%"> 
                  <div align="right">Identificaci&oacute;n :</div>
                </td>
                <td nowrap > 
                  <input type="text" name="E01ETRUT" size="30" maxlength="25" value="<%= consig.getE01ETRUT().trim()%>" readonly>
                </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="25%" > 
                  <div align="right">Nombre :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" name="E01ETNOM" size="65" maxlength="60" value="<%= consig.getE01ETNOM().trim()%>" readonly>
                </td>
              </tr>
              <% if(!consig.getE01ETNRO().trim().equals("0")) {  %>
              <tr id="trclear"> 
                <td nowrap width="25%"> 
                  <div align="right">Fecha Firma :</div>
                </td>
                <td nowrap colspan=3> 
	               <eibsinput:date name="consig" fn_year="E01ETAAF" fn_month="E01ETMMF" fn_day="E01ETDDF" readonly="true"/>
                </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="25%"> 
                  <div align="right">N&uacute;mero de Correlativo :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" name="E01ETNRO" size="17" maxlength="12" value="<%= consig.getE01ETNRO().trim()%>" readonly>
                </td>
              </tr>
              <%}%>
              <tr>
              	<td colspan="2">
  		  			<div align="center">
		                <% if(consig.getE01ETNRO().trim().equals("0")) {  %>

						<input id="EIBSBTN" type="button" name="Submit1" value="Firmar" onClick="javascript:goAction(300);">
		                <% }else {  %>
							<input id="EIBSBTN" type="button" name="Submit2" value="Imprimir" onClick="javascript:goView('FIRMA_CONTRATO_CANALES.pdf');">
			              <%}%>
     	  	 		</div>	
              	</td>
              </tr>	
          </table>
          </div>
          </td>
        </tr>
      </table>
   </div>
  <% if(!consig.getE01ETNRO().trim().equals("0")) {  %>

  <h4>Tarjeta de Coordenadas</h4>
    <div align="left">         
    <table class="tableinfo" >
      <tr > 
          <td nowrap > 
          <div align="center"> 
            <table cellspacing="0" cellpadding="2" width="100%" border="0">
              <tr id="trclear"> 
                <td nowrap width="25%"> 
                  <div align="right">N&uacute;mero de Tarjeta :</div>
                </td>
                <td nowrap > 
                  <input type="text" name="E01ETTAR" size="14" maxlength="9" value="<%= consig.getE01ETTAR().trim()%>" onkeypress="enterDecimal(1);">
                </td>
              </tr>
			  <% if(!consig.getE01ETFOL().trim().equals("0")) {  %>
              <tr id="trdark"> 
                <td nowrap width="25%"> 
                  <div align="right">N&uacute;mero de Flolio :</div>
                </td>
                <td nowrap> 
                  <input type="text" name="E01ETFOL" size="15" maxlength="10" value="<%= consig.getE01ETFOL().trim()%>" readonly>
                </td> 
              </tr>
              <tr id="trdark"> 
                <td nowrap width="25%"> 
                  <div align="right">Fecha Firma :</div>
                </td>
                <td nowrap colspan=3> 
	               <eibsinput:date name="consig" fn_year="E01ETAAC" fn_month="E01ETMMC" fn_day="E01ETDDC" readonly="true"/>
                </td>
              </tr>
			  <%}%>

              <tr id="trclear">
              	<td colspan="2">
  		  			<div align="center">
  		  			<% if(!consig.getE01ETFOL().trim().equals("0")) {  %>
						<input id="EIBSBTN" type="button" name="Submit3" value="Actualizar" onClick="javascript:goAction(400);">
 						<input id="EIBSBTN" type="button" name="Submit3" value="Imprimir" onClick="javascript:goView('COMPROBANTE_TARJETA_COORDENADA.pdf');">
 					  <% }else {  %>	
						<input id="EIBSBTN" type="button" name="Submit3" value="Asignar" onClick="javascript:goAction(400);">
 					  <%}%>
     	  	 		</div>	
              	</td>
              </tr>	
          </table>
          </div>
          </td>
        </tr>
      </table>
   </div>
  <%}%>

 
</form>
</body>
</html>

