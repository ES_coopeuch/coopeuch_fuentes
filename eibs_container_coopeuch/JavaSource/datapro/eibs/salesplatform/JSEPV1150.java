package datapro.eibs.salesplatform;

/**
 * Registros  de Cheques a Terceros 
 * Archivo de Cheques a terceros para plataforma de venta
 * @author evargas
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV115001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSEPV1150 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int R_CHECKS_THIRD_LIST = 100;
	protected static final int A_CHECKS_THIRD_LIST = 101;
	protected static final int R_CHECKS_THIRD_NEW = 200;
	protected static final int R_CHECKS_THIRD_MAINT = 201;
	protected static final int R_CHECKS_THIRD_DELETE = 202;	
	protected static final int R_CHECKS_THIRD_INQUIRY = 203;
	protected static final int A_CHECKS_THIRD_MAINT = 600;
	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_CHECKS_THIRD_LIST:
			procReqTablaChequesTercerosList(user, req, res, session);
			break;
		case A_CHECKS_THIRD_LIST:
			procActionTablaChequesTercerosList(user, req, res, session, null);
			break;
		case R_CHECKS_THIRD_NEW:
			procReqTablaChequesTerceros(user, req, res, session, "NEW");
			break;
		case R_CHECKS_THIRD_MAINT:
			procReqTablaChequesTerceros(user, req, res, session, "MAINTENANCE");
			break;
		case R_CHECKS_THIRD_INQUIRY:
			procReqTablaChequesTerceros(user, req, res, session, "INQUIRY");
			break;
		case A_CHECKS_THIRD_MAINT:
			procActionMaintenance(user, req, res, session);
			break;
		case R_CHECKS_THIRD_DELETE:
			procReqDelete(user, req, res, session);
			break;
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}


	/**
	 * procReqTablaSEGUROSList
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTablaChequesTercerosList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: EPV1150_checks_third_enter_search.jsp");
			forward("EPV1150_checks_third_enter_search.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

	/**
	 * procActionTablaChequesTercerosList: Muestra los diferentes Cheques a terceros que posee Un cliente	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionTablaChequesTercerosList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String option)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		boolean evaluation = false;
		
		try {
			mp = getMessageProcessor("EPV1150", req);

			EPV115001Message msg = (EPV115001Message) mp.getMessageRecord("EPV115001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());
			
			
			System.out.println("E01PVCCUN=/"+req.getParameter("E01PVCCUN")+"/");
			System.out.println("E01PVCNUM=/"+req.getParameter("E01PVCNUM")+"/");
			
			msg.setE01PVCCUN(req.getParameter("E01PVCCUN").trim());
			msg.setE01PVCNUM(req.getParameter("E01PVCNUM").trim());

			userPO.setCusNum(req.getParameter("E01PVCCUN").trim());
			userPO.setHeader23(req.getParameter("E01PVCNUM").trim());
			
			//Sends message
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
 
			session.setAttribute("userpPO", userPO);
			session.setAttribute("EPV115001List", list);
			forwardOnSuccess("EPV1150_checks_third_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqTablaseguros: This Method show a single  Tabla Seguros either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTablaChequesTerceros(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("EPV1150", req);
			userPO.setPurpose(option);
			
			//Creates the message with operation code depending on the option
			EPV115001Message msg = (EPV115001Message) mp.getMessageRecord("EPV115001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01TIMSYS(getTimeStamp());
			if (option.equals("NEW")) {
				msg.setH01OPECOD("0001");
			} else if (option.equals("MAINTENANCE")) {
				msg.setH01OPECOD("0002");
			} else {
				msg.setH01OPECOD("0004");
			}
			try {
				msg.setE01PVCSEQ(req.getParameter("E01PVCSEQ").trim());
			} catch (Exception e) {
				msg.setE01PVCSEQ("0");
			}
			
			//Sets the number for maintenance and inquiry options
			msg.setE01PVCCUN(userPO.getCusNum());
			msg.setE01PVCNUM(userPO.getHeader23());

			//Send message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV115001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go to maintenance page
				flexLog("About to call Page: EPV1150_checks_third_maintenance.jsp");
				if (option.equals("INQUIRY")) {
					// if the request is an inquiry sets the readOlnly attribute 'true'
					forward("EPV1150_checks_third_maintenance.jsp?readOnly=true", req, res);
				} else {
					forward("EPV1150_checks_third_maintenance.jsp", req, res);
				}
			} else {
				//if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1150_checks_third_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procActionMaintenance
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1150", req);

			EPV115001Message msg = (EPV115001Message) mp.getMessageRecord("EPV115001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0005");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV115001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go back to list
				//redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1150?SCREEN=101&E01PVCCUN="+ userPO.getCusNum()+"&E01PVCNUM="+ userPO.getHeader23(), res);
				req.setAttribute("ACT","S");
				forward("EPV1150_checks_third_maintenance.jsp", req, res);
			} else {
				//if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				forward("EPV1150_checks_third_maintenance.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqDelete
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDelete(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("MAINTENANCE");

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1150", req);
			
			//Creates message with the 'Delete'operation code
			EPV115001Message msg = (EPV115001Message) mp.getMessageRecord("EPV115001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0009");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets required values
			msg.setH01SCRCOD("01");
			//Sets the Code for delete options
			msg.setE01PVCCUN(userPO.getCusNum());
			msg.setE01PVCNUM(userPO.getHeader23());
			try {
				msg.setE01PVCSEQ(req.getParameter("E01PVCSEQ").trim());
			} catch (Exception e) {
				msg.setE01PVCSEQ("0");
			}
			
			//Send message
			mp.sendMessage(msg);

			//Receive Error and Data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV115001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				//If there are no errors request the list again
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1150?SCREEN=101&E01PVCCUN="+ userPO.getCusNum()+"&E01PVCNUM="+ userPO.getHeader23(), res);
			} else {
				//if there are errors show the list without updating
				session.setAttribute("error", msgError);
				forward("EPV1150_checks_third_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

}


