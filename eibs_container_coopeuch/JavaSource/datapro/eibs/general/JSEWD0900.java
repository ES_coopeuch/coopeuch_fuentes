package datapro.eibs.general;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.eibs.exception.FacadeException;
import com.datapro.eibs.exception.ItemNotFoundException;
import com.datapro.eibs.facade.FAReports;
import com.datapro.eibs.reports.vo.IFSFILES;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.EWD0900DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

public class JSEWD0900 extends JSEIBSServlet {

	protected static final int R_LIST   = 1;
	protected static final int A_LIST   = 2;
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			// Request
			case R_LIST :
				procReqList(user, req, res, session);
				break;
				// Actions
			case A_LIST :
				procActionList(user, req, res, session);
				break;
			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}


	private void copyMsgToMsg(MessageRecord source, MessageRecord target) {
		java.util.Enumeration enu = target.fieldEnumeration();
		MessageField field = null;
		String value = null;
		while (enu.hasMoreElements()) {
			field = (MessageField) enu.nextElement();
			try {
				value = source.getFieldString(field.getTag());
				if (value != null) {
					field.setString(value);
				}
			} catch (Exception e) {
			}
		}
	}

	private boolean isReportReady(EWD0900DSMessage msg, HttpServletResponse res) throws ServletException, IOException {
		
		boolean result = false;
		System.out.println("Connecting to IBS via jdbc.cnx.driver.eibs-server...");
		BufferedOutputStream output = null;
		Connection cnx = null;
		FAReports facade = null;
		try {
			facade = new FAReports();
			IFSFILES vo = facade.getIFSFiles(msg.getSWDFIL(), msg.getSWDSRD(), msg.getSWDNUM(), msg.getBigDecimalSWDPLN());
				
			byte buf[] = vo.getBINFILE();
			String type = vo.getFEXT().toLowerCase();
		
			res.reset();
			res.setContentLength(buf.length);
			if (type.trim().equals("xls") || type.trim().equals("doc") || type.trim().equals("txt")) {
				String imgFileName = msg.getSWDFIL() + "." + type;
				res.setHeader("Content-disposition", "attachment; filename=\"" + imgFileName + "\"");
				if (type.trim().equals("xls")) {
					res.setContentType("application/vnd.ms-excel");
				} else if (type.trim().equals("doc")) {
					res.setContentType("application/msword");
				} else if (type.trim().equals("txt")) {
					res.setContentType("application/x-text");
				}
			} else {
				res.setContentType("application/" + type);
			}
			output = new BufferedOutputStream(res.getOutputStream());
			//output.write(outbuf);
			output.write(buf);
			output.flush();
				
			result = true;
			System.out.println("Report sent to screen.");
				
		} catch (ItemNotFoundException e) {
			result = false;
		} catch (FacadeException e) {
			e.printStackTrace();
			flexLog("FacadeException ocurred. Exception = " + e.getMessage());
			throw new ServletException(e);
		} finally {
			if (output != null) output.close();
		}
		return result;
	}
	
	private void procActionList(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		JBObjList jbList = (JBObjList) session.getAttribute("EWD0900Help");
		jbList.initRow();
		int row = 0;
		int opt = 0;
		try {
			opt = Integer.parseInt(req.getParameter("opt"));
		} catch (Exception e) {
			opt = 2;
		}
		if (opt < 3) {
			try {
				row = Integer.parseInt(req.getParameter("ROW"));
			}catch (Exception e){
				row = jbList.getFirstRec();
			}
		}
		jbList.setCurrentRow(row);
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EWD0900", req);
			EWD0900DSMessage msg = (EWD0900DSMessage) jbList.getRecord();
			
			if ("*OPEN".equals(msg.getSWDSTS().trim())) {
				res.setContentType("text/html");
				PrintWriter out = res.getWriter();
				out.println("<HTML>");
				out.println("<HEAD>");
				out.println("<TITLE>Error - Warning</TITLE>");
				out.println("</HEAD>");
				out.println("<BODY>");
				out.println("<table id=tbhelp width=100% align=center>");
				out.println("	<tr>");
				out.println("		<td align=center colspan=3>");
				out.println("			<table width=100% border=0 cellspacing=2 cellpadding=2 class=tbenter>");
				out.println("				<tr>");
				out.println("					<td width=75> ");
				out.println("						<div align=right><font color=red><img src=\"" + req.getContextPath() + "/images/warning.gif\"></font></div>");
				out.println("					</td>");
				out.println("					<td>");
				out.println("						<div align=center nowrap><font color=#FF3333 face=\"Arial, Helvetica, sans-serif\" size=3><b>Errores - Advertencias</b></font></div>");
				out.println("					</td>");
				out.println("					<td width=75> ");
				out.println("						<div align=right><font color=red><img src=\"" + req.getContextPath() + "/images/warning.gif\"></font></div>");
				out.println("					</td>");
				out.println("				</tr>");
				out.println("			</table>");
				out.println("		</td>");
				out.println("	</tr>");
				out.println("	<tr>");
				out.println("		<td align=center colspan=3>");
				out.println("			&nbsp;");
				out.println("		</td>");
				out.println("	</tr>");
				out.println("	<tr>");
				out.println("		<td align=center>");
				out.println("			<IMG src=\"" + req.getContextPath() + "/images/warning01.gif\" alt=\"Warning!!!\">");
				out.println("		</td>");
				out.println("		<td align=center>");
				out.println("			<b>Su reporte aun no esta listo para ser consultado.<br>Por favor tenga paciencia y espere.<br>!!NO INTENTE GENERARLO DE NUEVO!!.</b>");
				out.println("		</td>");
				out.println("		<td align=center>");
				out.println("			<IMG src=\"" + req.getContextPath() + "/images/warning01.gif\" alt=\"Warning!!!\">");
				out.println("		</td>");
				out.println("	</tr>");
				out.println("</table>");
				out.println("</SCRIPT>");
				out.println("</BODY>");
				out.println("</HTML>");
			} else {
				EWD0900DSMessage msgToSend = new EWD0900DSMessage();
				copyMsgToMsg(msg, msgToSend);
				super.flexLog("SWDFIL :" + msgToSend.getSWDFIL());
				flexLog("JOB : "+ msgToSend.getSWDNUM()+"/"+msgToSend.getSWDSRD()+"/"+msgToSend.getSWDNAM());
				flexLog("SPLNBR :"+ msgToSend.getSWDPLN());
				
				if (opt > 1) { 
					msgToSend.setRWDTYP("D");  
				} else {
					msgToSend.setRWDTYP("I");
				}	
			
				if (opt < 2) {
					if (!isReportReady(msgToSend, res)) {
						mp.sendMessage(msgToSend);
						
						ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
						msgToSend = (EWD0900DSMessage) mp.receiveMessageRecord("EWD0900DS");
						boolean isNotError = !mp.hasError(msgError);
						
						if (isNotError) {
							session.setAttribute("REPORT", msg);
							forward("MISC_report_wait.jsp?URL='" + req.getContextPath() 
								+ "/servlet/datapro.eibs.tools.JSPDFReport?SCREEN=2'", req, res);
						} else {
							session.setAttribute("error", msgError);
							res.setContentType("text/html");
							PrintWriter out = res.getWriter();
							out.println("<HTML>");
							out.println("<HEAD>");
							out.println("<TITLE>Error - Warning</TITLE>");
							out.println("</HEAD>");
							out.println("<BODY>");
							out.println("<table id=tbhelp width=100% align=center>");
							out.println("	<tr>");
							out.println("		<td align=center colspan=3>");
							out.println("			<table width=100% border=0 cellspacing=2 cellpadding=2 class=tbenter>");
							out.println("				<tr>");
							out.println("					<td width=75> ");
							out.println("						<div align=right><font color=red><img src=\"" + req.getContextPath() + "/images/warning.gif\"></font></div>");
							out.println("					</td>");
							out.println("					<td>");
							out.println("						<div align=center nowrap><font color=#FF3333 face=\"Arial, Helvetica, sans-serif\" size=3><b>Errores - Advertencias</b></font></div>");
							out.println("					</td>");
							out.println("					<td width=75> ");
							out.println("						<div align=right><font color=red><img src=\"" + req.getContextPath() + "/images/warning.gif\"></font></div>");
							out.println("					</td>");
							out.println("				</tr>");
							out.println("			</table>");
							out.println("		</td>");
							out.println("	</tr>");
							out.println("	<tr>");
							out.println("		<td align=center colspan=3>");
							out.println("			&nbsp;");
							out.println("		</td>");
							out.println("	</tr>");
							out.println("	<tr>");
							out.println("		<td align=center>");
							out.println("			<IMG src=\"" + req.getContextPath() + "/images/error01.gif\" alt=\"Warning!!!\">");
							out.println("		</td>");
							out.println("		<td align=center>");
							out.println("			<b>" + msgError.getERNU01().trim() + " : " + msgError.getERDS01().trim() + "</b>");
							out.println("		</td>");
							out.println("		<td align=center>");
							out.println("			<IMG src=\"" + req.getContextPath() + "/images/error01.gif\" alt=\"Warning!!!\">");
							out.println("		</td>");
							out.println("	</tr>");
							out.println("</table>");
							out.println("</SCRIPT>");
							out.println("</BODY>");
							out.println("</HTML>");
						}
					}
				} else {
					ELEERRMessage msgError = null;
					boolean isNotError = true;
					
					switch (opt) {
						case 2:
							mp.sendMessage(msgToSend);
							msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
							msgToSend = (EWD0900DSMessage) mp.receiveMessageRecord("EWD0900DS");
							isNotError = !mp.hasError(msgError);
							break;
						case 3:
							mp.sendMessage(msgToSend);
							msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
							msgToSend = (EWD0900DSMessage) mp.receiveMessageRecord("EWD0900DS");
							while (jbList.getNextRow()) {
								msgToSend = (EWD0900DSMessage) jbList.getRecord();
								msgToSend.setRWDTYP("D"); 
								mp.sendMessage(msgToSend);
								msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
								msgToSend = (EWD0900DSMessage) mp.receiveMessageRecord("EWD0900DS");
							}
							break;
					}
					
					if (isNotError) {
						procReqList(user, req, res, session);
					} else {
						session.setAttribute("error", msgError);
						forward("EWD0900_sel_spool.jsp", req, res);
					}
				}
			}
			
		} finally {
			if (mp != null)	mp.close();
		}
	}

	private void procReqList(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		String PageToCall = "";
		ELEERRMessage msgError = null;
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EWD0900", req);
			EWD0900DSMessage msg = (EWD0900DSMessage) mp.getMessageRecord("EWD0900DS", user.getH01USR(), "");
			try { //From Pos
				msg.setRWDFRC(req.getParameter("Pos"));
			} catch (Exception e) {
				//msg.setRWDFRC("");
			}
			msg.setRWDTYP("L");
			msg.setRWDUSR(user.getH01USR()); // User
			
			mp.sendMessage(msg);
			
			JBObjList list = mp.receiveMessageRecordList("SWDOPE", "SWDREC");
			
			if (mp.hasError(list)) {
				list.initRow();
				list.getNextRow();
				msgError = (ELEERRMessage) list.getRecord();
				PageToCall = "error_viewer.jsp";
			} else {
				PageToCall = "EWD0900_sel_spool.jsp";
			}
		
			session.setAttribute("EWD0900Help", list);			
			session.setAttribute("error", msgError);
			forward(PageToCall, req, res);
		} finally {
			if (mp != null)	mp.close();
		}
	}

}
