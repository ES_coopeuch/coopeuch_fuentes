<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Aprobación de Negocios</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cdMant" class="datapro.eibs.beans.EDL013001Message"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSTreasury.jsp"> </SCRIPT>
<script src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">  

 <% 
   if (userPO.getOption().equals("TREASURY") && userPO.getHeader16().equals("N")) {  
  %>
		builtNewMenu(cdt_a_act_opt);		// Treasury without SWIFT Payment
  <%      
   } else if (userPO.getOption().equals("TREASURY") && (!userPO.getHeader16().equals("N"))) {
  %>
		builtNewMenu(cdt_a_opt);			// Treasury with SWIFT Payment
  <%
   } else {
  %> 
   		builtNewMenu(cd_a_opt);			//  Normal
  <% 
   }
  %>
  
  function change(value){
  	return value =='Y' ? 'txtchanged':'';
  }

</SCRIPT>

</head>

<body>


<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors();");
     out.println("</SCRIPT>");
 }
    out.println("<SCRIPT> initMenu(); </SCRIPT>");
%>

<h3 align="center">Aprobaci&oacute;n de <%=cdMant.getE01DEANR1() %><img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cd_ap_maint.jsp,EDL0140"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0130A" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="4">
  <INPUT TYPE=HIDDEN NAME="E01DEABNK"  value="<%= cdMant.getE01DEABNK().trim()%>">
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left">
                <input type="text" readonly <% if (cdMant.getF01DEACUN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEACUN" size="10" maxlength="9" value="<%= cdMant.getE01DEACUN().trim()%>">
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2">
                <input type="text" readonly name="E01CUSNA1" size="45" maxlength="45" value="<%= cdMant.getE01CUSNA1().trim()%>" >
                </font></font></font></div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Certificado :</b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left">
                <input type="text" readonly name="E01DEAACC" size="13" maxlength="12" value="<%= cdMant.getE01DEAACC().trim()%>" >
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b>
                <input type="text" readonly name="E01DEACCY2" size="3" maxlength="3" value="<%= cdMant.getE01DEACCY().trim()%>" >
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b>
                <input type="text" readonly name="E01DEAPRO" size="4" maxlength="4" value="<%= cdMant.getE01DEAPRO().trim()%>" >
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Informaci&oacute;n General</h4>
      <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Nombre del Certificado :</div>
            </td>
            <td nowrap >               
              <eibsinput:text property="E01DEANME" name="cdMant" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="true"  modified="F01DEANME"/>   
            </td>
            <td nowrap > 
            </td>
            <td nowrap > 
            </td>
          </tr>        
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Fecha de Apertura :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEAOD1().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAOD1" size="3" maxlength="2" value="<%= cdMant.getE01DEAOD1().trim()%>" >
              <input type="text" readonly <% if (cdMant.getF01DEAOD2().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAOD2" size="3" maxlength="2" value="<%= cdMant.getE01DEAOD2().trim()%>" >
              <input type="text" readonly <% if (cdMant.getF01DEAOD3().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAOD3" size="5" maxlength="4" value="<%= cdMant.getE01DEAOD3().trim()%>" >
            </td>
            <td nowrap > 
              <div align="right">Monto Original :</div>
            </td>
            <td nowrap >             
              <eibsinput:text  name="cdMant" property="E01DEAOAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" modified="F01DEAOAM"/>              
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Fecha de Vencimiento :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEAMD1().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAMD1" size="3" maxlength="2" value="<%= cdMant.getE01DEAMD1().trim()%>" >
              <input type="text" readonly <% if (cdMant.getF01DEAMD2().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAMD2" size="3" maxlength="2" value="<%= cdMant.getE01DEAMD2().trim()%>" >
              <input type="text" readonly <% if (cdMant.getF01DEAMD3().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAMD3" size="5" maxlength="4" value="<%= cdMant.getE01DEAMD3().trim()%>" >
            </td>
            <td nowrap > 
              <div align="right">Saldo Principal :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="cdMant" property="E01DEAMEP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />                 
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Tasa Actual :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="cdMant" property="E01RATE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"  modified="F01DEARTE"/>
            </td>
            <td nowrap > 
              <div align="right">Saldo de Inter&eacute;s :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="cdMant" property="E01DEAMEI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />                 
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Per&iacute;odo Base :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEABAS().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEABAS" size="3" maxlength="3" value="<%= cdMant.getE01DEABAS().trim()%>" >
            </td>
            <td nowrap > 
              <div align="right"> Moneda :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEACCY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEACCY" size="3" maxlength="3" value="<%= cdMant.getE01DEACCY().trim()%>" >
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="25%" > 
              <div align="right">Monto Rescate :</div>
            </td>
            <td nowrap width="23%" > 
            <eibsinput:text name="cdMant" property="E01DEAXRP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>"  readonly="true"/></td>            
            <td nowrap width="25%" > 
              <div align="right">Tasa Periodo :</div>
            </td>
            <td nowrap width="27%" > 
             <eibsinput:text name="cdMant" property="E01DEAIRT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>
            </td>
          </tr>             
        </table>
      </td>
    </tr>
  </table>
  <h4>Datos B&aacute;sicos de la Operaci&oacute;n</h4>
  <table class="tableinfo">
    <tr> 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Nueva Fecha Vencimiento :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E01NEWMD1" size="3" maxlength="2" value="<%= cdMant.getE01NEWMD1().trim()%>">
              <input type="text" readonly name="E01NEWMD2" size="3" maxlength="2" value="<%= cdMant.getE01NEWMD2().trim()%>">
              <input type="text" readonly name="E01NEWMD3" size="5" maxlength="4" value="<%= cdMant.getE01NEWMD3().trim()%>">
            </td>
            <td nowrap > 
              <div align="right">T&eacute;rmino :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEATRM().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEATRM" size="6" maxlength="5" value="<%= cdMant.getE01DEATRM().trim()%>">
              <input type="text" readonly <% if (cdMant.getF01DEATRC().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEATRC" size="15" maxlength="15" 
				  value="<% if (cdMant.getE01DEATRC().equals("D")) out.print("D&iacute;a(s)");
							else if (cdMant.getE01DEATRC().equals("M")) out.print("Mes(es)");
							else if (cdMant.getE01DEATRC().equals("Y")) out.print("A&ntilde;o(s)");
							else out.print("");%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Tipo de Tasa Variable :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEAFTB().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAFTB" size="2" maxlength="2" value="<%= cdMant.getE01DEAFTB().trim()%>">
              <input type="text" readonly <% if (cdMant.getF01DEAFTY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAFTY" size="3" maxlength="3" 
				  value="<% if (cdMant.getE01DEAFTY().equals("FP")) out.print("FP");
							else if (cdMant.getE01DEAFTY().equals("FS")) out.print("FS");
							else out.print("");%>" 
				>
            </td>
            <td nowrap > 
              <div align="right">Tasa Variable :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="cdMant" property="E01FLTRTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Ciclo/Fecha Revis. Tasa :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEARRP().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEARRP" size="3" maxlength="3" value="<%= cdMant.getE01DEARRP().trim()%>">
              / 
              <input type="text" readonly <% if (cdMant.getF01DEARD1().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEARD1" size="3" maxlength="2" value="<%= cdMant.getE01DEARD1().trim()%>">
              <input type="text" readonly <% if (cdMant.getF01DEARD2().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEARD2" size="3" maxlength="2" value="<%= cdMant.getE01DEARD2().trim()%>">
              <input type="text" readonly <% if (cdMant.getF01DEARD3().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEARD3" size="5" maxlength="4" value="<%= cdMant.getE01DEARD3().trim()%>">
            </td>
            <td nowrap > 
              <div align="right">Tasa Inter&eacute;s/Spread :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="cdMant" property="E01DEARTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true" modified="F01DEARTE"/>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Retenci&oacute;n/Impuesto :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEAWHF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAWHF" size="2" maxlength="1" value="<%= cdMant.getE01DEAWHF().trim()%>">
            </td>
            <td nowrap > 
              <div align="right">N&uacute;mero Referencia :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEAREF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAREF" size="12" maxlength="12" value="<%= cdMant.getE01DEAREF().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Condici&oacute;n de Contrato :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEADLC().equals("Y")) out.print("id=\"txtchanged\""); %>  name="E01DEADLC" size="15" maxlength="15" 
				  value="<% if (cdMant.getE01DEADLC().equals("1")) out.print("Vigente");
							else if (cdMant.getE01DEADLC().equals("2")) out.print("Vencido");
							else if (cdMant.getE01DEADLC().equals("3")) out.print("Embargado");
							else out.print("");%>" 
				>
            </td>
            <td nowrap > 
              <div align="right">Tasa de Cambio :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="cdMant" property="E01DEAEXR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_EXCHANGE_RATE %>" readonly="true" modified="F01DEAEXR"/>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Tipo de Inter&eacute;s :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEAICT().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAICT" size="2" maxlength="1" value="<%= cdMant.getE01DEAICT().trim()%>">
            </td>
            <td nowrap > 
              <div align="right">Calcular Inter&eacute;s :</div>
            </td>
            <td nowrap >
              <input type="text" readonly <% if (cdMant.getF01DEAIFL().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAIFL" size="2" maxlength="1" value="<%= cdMant.getE01DEAIFL().trim()%>">
            </td>
          </tr>
          <tr id="trdark">
            <td nowrap > 
              <div align="right">Direcci&oacute;n de Correo:</div>
            </td>
            <td nowrap > 
              <input type="text" readonly <% if (cdMant.getF01DEAMLA().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEAMLA" size="2" maxlength="2" value="<%= cdMant.getE01DEAMLA().trim()%>">
            </td>
            <td nowrap > 
              <div align="right">&#191;Bloqueo Correspondencia&#63; :</div>
            </td>
	    		<td>
	       			<input type="radio" disabled name="E01DEASUT" value="Y" onClick="document.forms[0].E01DEASUT.value='Y'"
		  			<%if(cdMant.getE01DEASUT().equals("Y")) out.print("checked");%>>
	          			S&iacute; 
	       			<input type="radio" disabled name="E01DEASUT" value="N" onClick="document.forms[0].E01DEASUT.value='N'"
		  			<%if(cdMant.getE01DEASUT().equals("N")) out.print("checked");%>>
	           			No 
				</td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Centro de Costos :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="cdMant" property="E01DEACCN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_COST_CENTER %>" readonly="true" modified="F01DEACCN"/>
            </td>
            <td nowrap > 
              <div align="right">Porcentaje Garant&iacute;a :</div>
            </td>
            <td nowrap >
              <eibsinput:text name="cdMant" property="E01DEACPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="true" modified="F01DEACPE"/>
            </td>
          </tr>
          <tr id="trdark">
            <td nowrap>
              <div align="right">Documento en Custodia :</div>
            </td>
            <td nowrap>
             <SELECT name="E01DEAECU" disabled>
                <OPTION value="N" <% if (!(cdMant.getE01DEAECU().equals("1") || cdMant.getE01DEAECU().equals("2"))) out.print("selected"); %>>No Custodia</OPTION>
                <OPTION value="1" <% if (cdMant.getE01DEAECU().equals("1")) out.print("selected"); %>>Electronica</OPTION>
                <OPTION value="2" <% if (cdMant.getE01DEAECU().equals("2")) out.print("selected"); %>>Custodia Fisica</OPTION>
              </SELECT>
            </td>
            <td nowrap width="25%" > 
              <div align="right">Banco/Sucursal :</div>
            </td>
            <td nowrap width="23%">           
              <input type="text" name="E01DEABNK" size="2" maxlength="2" value="<%= cdMant.getE01DEABNK().trim()%>" readonly>
              <input type="text" name="E01DEABRN" size="4" maxlength="4" value="<%= cdMant.getE01DEABRN().trim()%>" readonly>
            </td>      
          </tr>
    		<tr id="trclear">            
          		<td nowrap width="25%" >
           			<div align="right">Documento Con Mandato :</div>
          		</td> 
    	 		<TD>
           			<input type="radio" name="E01DEA2TC" disabled value="Y" onClick="document.forms[0].E01DEA2TC.value='Y'"
		  			<%if(cdMant.getE01DEA2TC().equals("Y")) out.print("checked");%>>
              			S&iacute; 
           			<input type="radio" name="E01DEA2TC" disabled value="N" onClick="document.forms[0].E01DEA2TC.value='N'"
		  			<%if(cdMant.getE01DEA2TC().equals("N")) out.print("checked");%>>
              			No 
    			</TD>
	        	<td nowrap width="25%" >
	       			<div align="right">Cuenta Mandato :</div>
	       		</td> 
	    		<td>
	              <input type="text" name="E01DEAREX" size="13" maxlength="12" value="<%= cdMant.getE01DEAREX().trim()%>" readonly>
				</td>
	        	</tr>
    		<tr id="trdark">            
	        	<td nowrap width="25%" >
	       			<div align="right">Documento Impreso :</div>
	       		</td> 
	    		<td>
	       			<input type="radio" disabled name="E01DEAF01" value="Y" onClick="document.forms[0].E01DEAF01.value='Y'"
		  			<%if(cdMant.getE01DEAF01().equals("Y")) out.print("checked");%>>
	          			S&iacute; 
	       			<input type="radio" disabled name="E01DEAF01" value="N" onClick="document.forms[0].E01DEAF01.value='N'"
		  			<%if(cdMant.getE01DEAF01().equals("N")) out.print("checked");%>>
	           			No 
				</td>
            	<td nowrap > 
              <div align="right">Clase de Certificado :</div>
            </td>
            <td nowrap >
              <input type="text" readonly <% if (cdMant.getF01DEACLF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01DEACLF" size="2" maxlength="1" value="<%= cdMant.getE01DEACLF().trim()%>">
            </td>
        	</tr>
        </table>
      </td>
    </tr>
  </table>
  
  <% if (cdMant.getH01FLGMAS().equals("N")) {%>
  
  <% if ((cdMant.getE01FLGFRA().trim().equals("Y"))) {%> 
  <h4>Depositos Desmaterializados</h4> 
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
	    <%if( currUser.getE01INT().equals("18")){%> 	
		    <tr id="trdark">            
	        	<td nowrap width="40%" >
    	   			<div align="right">Monto pago Final :</div>
    	   		</td> 
 		   		<td nowrap width="60%" >
       	         <eibsinput:text name="cdMant" property="E01PAYOFF" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
 				</td>
  	 		</tr>
        <% } else { %>
		    <tr id="trdark">            
    	    	<td nowrap width="25%" >
       				<div align="right">Numero Documentos :</div>
      	 		</td> 
    			<td nowrap width="25%" >
       	         <eibsinput:text name="cdMant" property="E01DEANFR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" maxlength="3" size="4" readonly="true"/>
				</td>
	            <td nowrap width="25%" > 
    	          <div align="right">En Monto de :</div>
     	       </td>      
 		       <td nowrap width="25%" >
       	         <eibsinput:text name="cdMant" property="E01DEAAFR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
       		   </td>      
	   		</tr>
		    <tr id="trclear">            
	        	<td nowrap width="25%" >
	       			<div align="right">Monto pago Final :</div>
  	     		</td> 
	    		<td nowrap width="25%" >
	                <eibsinput:text name="cdMant" property="E01PAYOFF" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
	 			</td>
	            <td nowrap width="25%" > 
	            </td>      
	            <td nowrap width="25%" >
 	           </td>      
	   		</tr>
   		<% } %> 

       </table>
      </td>
    </tr>
  </table>
<% } %> 
  
  <h4>Origen de Fondos</h4>
  
<TABLE id="mainTable" class="tableinfo">
  <TR><TD>
  
   <table id="headTable" >
    <tr id="trdark"> 
      <td nowrap align="center" >Concepto</td>
      <td nowrap align="center" >Banco </td>
      <td nowrap align="center" >Sucursal</td>
      <td nowrap align="center" >Moneda</td>
      <td nowrap align="center" >Referencia</td>
      <td nowrap align="center" >Monto</td>
    </tr>
    </table> 
      
    <div id="dataDiv" style="height:60; overflow-y :scroll; z-index:0" >
     <table id="dataTable" >
          <%
  				   int amount = 9;
 				   String name;
  					for ( int i=1; i<=amount; i++ ) {
   					  name = i + "";
   			%> 
          <tr id="trclear"> 
            <td nowrap > 
              <div align="center"> 
                <input type="hidden" name="E01OFFOP<%= name %>" value="<%= cdMant.getField("E01OFFOP"+name).getString().trim()%>">
                <input type="hidden" name="E01OFFGL<%= name %>" value="<%= cdMant.getField("E01OFFGL"+name).getString().trim()%>">
                <input type="text" name="E01OFFCO<%= name %>" size="35" maxlength="35" readonly value="<%= cdMant.getField("E01OFFCO"+name).getString().trim()%>" 
                  >
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFBK<%= name %>" size="2" maxlength="2" value="<%= cdMant.getField("E01OFFBK"+name).getString().trim()%>" readonly >
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFBR<%= name %>" size="4" maxlength="4" value="<%= cdMant.getField("E01OFFBR"+name).getString().trim()%>"
               readonly >
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFCY<%= name %>" size="3" maxlength="3" value="<%= cdMant.getField("E01OFFCY"+name).getString().trim()%>"
                readonly >
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFAC<%= name %>" size="13" maxlength="12"  value="<%= cdMant.getField("E01OFFAC"+name).getString().trim()%>"
               readonly >
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01OFFAM<%= name %>" size="15" maxlength="15"  value="<%= cdMant.getField("E01OFFAM"+name).getString().trim()%>" readonly >
              </div>
            </td>
          </tr>
          <%
    		}
    		%> 
    	  </table>
        </div>
        
		<table id="footTable" > 
          <tr id="trdark"> 
            <td nowrap align="right"><b>Equivalente Moneda del Certificado :</b>
            </td>
            <td nowrap align="center"><b><i><strong> 
                <input type="text" name="E01OFFEQV" size="15" maxlength="15" readonly value="<%= cdMant.getE01OFFEQV().trim()%>">
                </strong></i></b>
            </td>
          </tr>
        </table>
   </TD>  
</TR>	
</TABLE>    
 <SCRIPT language="javascript">
    function tableresize() {
     adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  tableresize();
  window.onresize=tableresize;
  </SCRIPT>
     <% } %>  
     
<jsp:include page="ESD0840_reevaluation_inquiry.jsp">
	<jsp:param name="flag" value="<%=cdMant.getH01FLGWK3()%>" />
</jsp:include>
     
  </form>
</body>
</html>
