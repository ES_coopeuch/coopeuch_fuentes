package com.datapro.eibs.constants;

/**
 * @author ogarcia
 *
 */
public interface StyleID {

	public static final String STYLEID_SCBARCOLOR = "SCBARCOLOR";
	public static final String STYLEID_TBENTER = "TBENTER";
	public static final String STYLEID_TBHEAD = "TBHEAD";
	public static final String STYLEID_TBHELP = "TBHELP";
	public static final String STYLEID_TBINFO = "TBINFO";
	public static final String STYLEID_TRDARK = "TRDARK";
	public static final String STYLEID_TRCLEAR = "TRCLEAR";
	public static final String STYLEID_TRNORMAL = "TRNORMAL";
	public static final String STYLEID_TRHIGHLIGHT = "TRHIGHLIGHT";
	public static final String STYLEID_TDADDINFO = "TDADDINFO";
	public static final String STYLEID_TDBKG = "TDBKG";
	public static final String STYLEID_IMGFILTER = "IMGFILTER";
	public static final String STYLEID_IMGFILTERPRESS = "IMGFILTERPRESS";
	public static final String STYLEID_TABNORMAL = "TABNORMAL";
	public static final String STYLEID_TABNORMALV = "TABNORMALV";
	public static final String STYLEID_TABHIGHLIGHT = "TABHIGHLIGHT";
	public static final String STYLEID_TABHIGHLIGHTL = "TABHIGHLIGHTL";
	public static final String STYLEID_TABHIGHLIGHTR = "TABHIGHLIGHTR";
	public static final String STYLEID_TABDATA = "TABDATA";
	public static final String STYLEID_TABDATALEFT = "TABDATALEFT";
	public static final String STYLEID_TABDATARIGHT = "TABDATARIGHT";
	public static final String STYLEID_EIBSBTN = "EIBSBTN";
	public static final String STYLEID_OPTMENUTB = "OPTMENUTB";
	public static final String STYLEID_TXTCHANGED = "TXTCHANGED";
	public static final String STYLEID_TXTRIGHT = "TXTRIGHT";
	public static final String STYLEID_TXTRCHANGED = "TXTRCHANGED";
	public static final String STYLEID_TXTLABEL = "TXTLABEL";
	public static final String STYLEID_TXTBASE = "TXTBASE";
	public static final String STYLEID_TABS = "TABS";
	
}
