package datapro.eibs.client;

/**
 * Servlet de Plan socio.
 * Creation date: (15/02/18)  
 * @author: Jose Vivas
 */

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDD100002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD008001Message;
import datapro.eibs.beans.ESD404001Message;
import datapro.eibs.beans.ESD408001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

public class JSESD4040 extends JSEIBSServlet {
	
	// entering options
	protected static final int R_ENTER_NEW 	 = 100;
	protected static final int A_ENTER_SOCIO_NEW  = 200;
	protected static final int A_ENTER_SOCIO_LIST = 300;
	protected static final int A_ENVIO_SOCIO = 400;
	protected static final int A_UPDATE_SOCIO = 500;
	protected static final int A_UPDATE_SOCIO_LIST = 600;
	protected static final int A_CKECK_LIST = 700;
	
	protected static final int R_MAINTANCE 	= 1000;
	protected static final int A_MAINTANCE_SOCIO = 1200;
	
	protected static final int R_CONSULT 	= 3000;
	protected static final int A_CONSULT_SOCIO = 3200;
	
	protected static final int R_APROBACION = 2000;
	
	protected static final int A_FORMULARIO = 4000;
	
	protected static final int R_FORMS_LIST = 5000;	

	protected String LangPath = "S";
	protected String bandUsuario = "0";
	protected String bandPerson = "3";
	protected String bandPR = "0";
	protected ArrayList<String> numCuentaLAlist = new ArrayList<String>();
	
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		MessageContext mc = null;
		
			
			
			if (screen == 1)
			{
				screen = 400;
			}
			
			switch (screen) {						
			// BEGIN Entering
			// Request
				case R_ENTER_NEW : 
					procReqEnterNew(user, req, res, session);
					break;
				case A_ENTER_SOCIO_NEW : 	
					procActionEnterSocioNew(mc, user, req, res, session, screen);
					break;	
				case A_ENTER_SOCIO_LIST : 
					procActionEnterSocioList(mc, user, req, res, session, screen);
					//procActionApproval(mc, msgUser, req, res, session);
					break;
				case A_UPDATE_SOCIO_LIST :
					procActionUpdateSocioList(mc, user, req, res, session, screen);
					break;
				case R_MAINTANCE :
					procReqMaintance(user, req, res, session);
					break;
				case A_MAINTANCE_SOCIO : 
					procActionMaintanceSocio(mc, user, req, res, session, screen);
					break;
				case R_CONSULT : 
					procReqEnterConsult(user, req, res, session);
					break;
				case A_CONSULT_SOCIO : 	
					procActionEnterSocioConsult(mc, user, req, res, session, screen);
					break;	
				case A_ENVIO_SOCIO :
					procActionEnterSocio(mc, user, req, res, session, screen);	
					break;
				case A_FORMULARIO : 	
					res.sendRedirect(super.srctx + "/servlet/datapro.eibs.client.JSESD4090?SCREEN=4000&E01PVMNUM=215539");
					break;	
				case A_UPDATE_SOCIO :
					procActionUpdateSocio(mc, user, req, res, session, screen);	
					break;	
				case R_FORMS_LIST :
					res.sendRedirect(super.srctx + "/servlet/datapro.eibs.client.client?SCREEN=4000&E01PVMNUM=215539");
					break;	
				case R_APROBACION :
					//request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0711?TYPE=RT&tipoProducto=CV&pagOperation=1
					res.sendRedirect(super.srctx + "/servlet/datapro.eibs.client.JSESD4080?SCREEN=5");
					
					break;
				default :
					res.sendRedirect(super.srctx + LangPath + super.devPage);
					break;
		}
	}
	
	private void Sleep(int i) {
		// TODO Apéndice de método generado automáticamente
		
	}
	
	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqEnterNew(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
		throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		if(numCuentaLAlist.size() > 0){
			numCuentaLAlist.clear();
		}
	
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos(); 
			userPO.setOption("PSOCIO");
			userPO.setPurpose("NEW");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);
		} catch (Exception ex) {
			flexLog("Error: " + ex); 
		}

		try {
			flexLog("About to call Page: " + LangPath + "ESD4040_socio_both_enter.jsp");
			forward("ESD4040_socio_both_enter.jsp", req, res);	
		}catch (Exception e) {
			flexLog("Exception calling page " + e);
		}
	}
	
	
	
	
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	/**
	 * 
	 */
	protected void procActionUpdateSocioList(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) 
		throws ServletException, IOException {
		
		String num = null;
	
		if(req.getParameter("tipoProducto").equals("LA")){
			num = req.getParameter("numeroCuenta").toString();
			numCuentaLAlist.add(num);
		}else if (req.getParameter("tipoProducto").equals("CV"))
			session.setAttribute("numCuentaCV", req.getParameter("numeroCuenta"));
		else if (req.getParameter("tipoProducto").equals("TC"))
			session.setAttribute("numCuentaTC", req.getParameter("numeroCuenta"));
	
		if (req.getParameter("NUEVO").equals("1"))
			procActionEnterSocioList(mc, user, req, res, session, screen);
		else
			procActionMaintanceSocioList(mc, user, req, res, session, screen);
	}// fin procActionEnterSocioList
	//---------------------------------------------------------------------------------------------------------------------------------------
	protected void procActionEnterSocioNew(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) 
		throws ServletException, IOException {
	
		MessageRecord newmessage = null;
		MessageRecord newmessage2 = null;
		MessageContext mcID = null;
		ESD008001Message msgClientPersonal = null;
		ESD404001Message msgPlanSocio = null;
		ELEERRMessage msgError = null;
		String decisionaval ="Respuesta NO Esperada";
	
		UserPos	userPO = null;
		UserPos	userPOAux = null;	
		boolean IsNotError = false;
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
		}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
	
		// Send Initial data
		try{
			mcID = new MessageContext(super.getMessageHandler("ESD0080", req));
			msgClientPersonal = (ESD008001Message)mcID.getMessageRecord("ESD008001");
			msgClientPersonal.setH01USR(user.getH01USR());
			msgClientPersonal.setH01PGM("ESD0080");
			msgClientPersonal.setH01TIM(getTimeStamp());
			msgClientPersonal.setH01SCR("01");
			msgClientPersonal.setH01OPE("0002");
			
			try {
				if (req.getParameter("E01CUN") != null){
					msgClientPersonal.setE01CUN(req.getParameter("E01CUN"));
				}
			}catch (Exception e){
				msgClientPersonal.setE01CUN("0");
				flexLog("Input data error " + e);
			}
			
			try {
				if (req.getParameter("E01IDN") != null && req.getParameter("E01IDN").indexOf("-") != -1	){
					msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));			 		
				}else {
					msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));
				}
			}catch (Exception e){
				msgClientPersonal.setE01IDN("");
				flexLog("Input data error " + e);
			}
				
			try {
				if (req.getParameter("statusMotor") != null ){
					decisionaval = req.getParameter("statusMotor") ;			 		
				}else {
					decisionaval = req.getParameter("statusMotor");
				}
			}catch (Exception e){
				msgClientPersonal.setE01IDN("");
				flexLog("Input data error " + e);
			}
			
			msgClientPersonal.send();	
			msgClientPersonal.destroy();
			flexLog("ESD008001 Message Sent");
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	
		// Receive Error Message
		try{
			newmessage = mcID.receiveMessage();
	  	  	if (newmessage.getFormatName().equals("ELEERR")) {
	  	  		msgError = (ELEERRMessage)newmessage;
	  	  		IsNotError = msgError.getERRNUM().equals("0");
	  	  		flexLog("IsNotError = " + IsNotError);
	  	  		showERROR(msgError);
	  	  	}
	  	  	else
	  	  		flexLog("Message " + newmessage.getFormatName() + " received.");	
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}	
	
		// Receive Data
		try{
			newmessage = mcID.receiveMessage();		
			if (newmessage.getFormatName().equals("ESD008001")) {
				try {
					msgClientPersonal = new datapro.eibs.beans.ESD008001Message();
					flexLog("ESD008001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex); 
				}

				msgClientPersonal = (ESD008001Message)newmessage;
				userPO.setCusNum(msgClientPersonal.getE01CUN());
				userPO.setOption("CLIENT_P");
				userPO.setCusType(msgClientPersonal.getE01LGT());
				userPO.setHeader1(msgClientPersonal.getE01CUN());
				userPO.setHeader2(msgClientPersonal.getE01IDN());
				userPO.setHeader3(msgClientPersonal.getE01NA1());
				
				session.setAttribute("IDuser", msgClientPersonal.getE01CUN() );
				session.setAttribute("RUTuser",msgClientPersonal.getE01IDN() );
				session.setAttribute("NOBuser",msgClientPersonal.getE01NA1());
				
				session.setAttribute("paginaPS", "4040");
				session.setAttribute("operation", "1");
				
				if (!msgClientPersonal.getE01IDN().equals("") && msgClientPersonal.getE01IDN().indexOf("-") != -1 ) {
					userPO.setHeader2(msgClientPersonal.getE01IDN());
				}
				flexLog("Putting java beans into the session");

				if (!IsNotError) {  // There are no errors
					try {
						flexLog("About to call Page: /pages/" + LangPath + "ESD4040_socio_both_enter.jsp");
						forward("ESD4040_socio_both_enter.jsp", req, res);
						mcID.close();
					}catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}		
			}		
			else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		}catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	
		// Send Initial data
		try{
			mc = new MessageContext(super.getMessageHandler("ESD4040", req));
			msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
			msgPlanSocio.setH01USR(user.getH01USR());
			msgPlanSocio.setH01PGM("ESD4040");
			msgPlanSocio.setH01TIM(getTimeStamp());
			msgPlanSocio.setH01SCR("01");
			msgPlanSocio.setH01OPE("0015");
			msgPlanSocio.setL01FILLE4("N");
	 	try {
		 	if (req.getParameter("E01CUN") != null){
		 		msgPlanSocio.setL01FILLE1(msgClientPersonal.getE01CUN()!=null ? msgClientPersonal.getE01CUN() : userPO.getCusNum());
		 		userPO.setCusNum(msgPlanSocio.getL01FILLE1().toString());
		 	}
		}catch (Exception e){
			msgPlanSocio.setL01FILLE1("0");
		    flexLog("Input data error " + e);
		}
		
		try {
		 	if (req.getParameter("E01IDN") != null && 
		 		req.getParameter("E01IDN").indexOf("-") != -1	){
//		 	 For PANAMA LONG IDENTIFICATION
		 		msgPlanSocio.setL01FILLE5(msgClientPersonal.getE01IDN());
		 		userPO.setHeader2(msgPlanSocio.getL01FILLE5().toString());
		 	}else {
		 		msgPlanSocio.setL01FILLE5(msgClientPersonal.getE01IDN());
		 		userPO.setHeader2(msgPlanSocio.getL01FILLE5().toString());
		 	}
		 	msgPlanSocio.setE01PSID(msgPlanSocio.getL01FILLE5().toString());
		}catch (Exception e){
			msgPlanSocio.setL01FILLE5("");
			flexLog("Input data error " + e);
		    flexLog("Input data error " + e);
		}
		msgPlanSocio.send();	
		msgPlanSocio.destroy();
	}		
	catch (Exception e){
		e.printStackTrace();
		flexLog("Error: " + e);
	  	throw new RuntimeException("Socket Communication Error");
	}	
	
	// Receive Error Message
	try{
		newmessage2 = mc.receiveMessage();
  	  	if (newmessage2.getFormatName().equals("ELEERR")) {
  	  		msgError = (ELEERRMessage)newmessage2;
  	  		IsNotError = msgError.getERRNUM().equals("0");
  	  		flexLog("IsNotError = " + IsNotError);
  	  		showERROR(msgError);
  	  	}
  	  	else
  	  		flexLog("Message " + newmessage.getFormatName() + " received.");	
	}catch (Exception e){
		e.printStackTrace();
		flexLog("Error: " + e);
		throw new RuntimeException("Socket Communication Error");
	}	
	
	// Receive Data
	try{
		newmessage2 = mc.receiveMessage();
		if (newmessage2.getFormatName().equals("ESD404001")) {
			try {
				msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
				flexLog("ESD404001 Message Received");
		  	} catch (Exception ex) {
				flexLog("Error: " + ex); 
		  	}
		  	
			msgPlanSocio = (ESD404001Message)newmessage2;
			userPO.setCusNum(msgPlanSocio.getL01FILLE1());
			userPO.setOption("CLIENT_P");
			req.setAttribute("IDN", msgClientPersonal.getE01IDN());
					
			if (IsNotError) {  // There are no errors				 
				if (msgPlanSocio.getL01FILLE4().equals("N")){				
					try {
						session.setAttribute("banderaPag", "1");
						userPOAux = userPO;
						flexLog("Putting java beans into the session");
						session.setAttribute("planSocio", msgPlanSocio);
						session.setAttribute("userPO", userPO);
						session.setAttribute("userPOAux", userPOAux);
						session.setAttribute("servlet", "4040");
						flexLog("ESD404001 Message Sent");	
						forward("ESD4040_plan_socio_new.jsp", req, res);
					}catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}else{
					procActionMaintanceSocio(mc, user, req, res, session, screen);
				}
			}
			else {  // There are errors
				try {
					flexLog("About to call Page: " + LangPath + "ESD4040_socio_both_enter.jsp");
					forward("ESD4040_socio_both_enter.jsp", req, res);	
				}
				catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			}
		}else{
			try {
				flexLog("About to call Page: " + LangPath + "ESD4040_socio_both_enter.jsp");
				forward("ESD4040_socio_both_enter.jsp", req, res);	
			}
			catch (Exception e) {
				flexLog("Exception calling page " + e);
			}
		}//fin if newmessage2
	}catch(Exception e){
		e.printStackTrace();
		flexLog("Error: " + e);
	  	throw new RuntimeException("Socket Communication Error");
	}
}//fin metodo procActionEnterSocioNew
	
	protected void procActionMaintanceSocio(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		MessageRecord newmessage = null;
		MessageRecord newmessage2 = null;
		MessageContext mcID = null;
		ESD008001Message msgClientPersonal = null;
		ESD404001Message msgPlanSocio = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		UserPos	userPOAux = null;
		boolean IsNotError = false;
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		userPO.setCusType(userPO.getHeader2());
		userPOAux = userPO;
		
		// Send Initial data
		try{
			mcID = new MessageContext(super.getMessageHandler("ESD0080", req));
			msgClientPersonal = (ESD008001Message)mcID.getMessageRecord("ESD008001");
		 	msgClientPersonal.setH01USR(user.getH01USR());
		 	msgClientPersonal.setH01PGM("ESD0080");
		 	msgClientPersonal.setH01TIM(getTimeStamp());
		 	msgClientPersonal.setH01SCR("01");
		 	msgClientPersonal.setH01OPE("0002");
		 	try {
			 	if (req.getParameter("E01CUN") != null){
				 	msgClientPersonal.setE01CUN(req.getParameter("E01CUN"));
			 	}else{
			 		msgClientPersonal.setE01CUN(userPO.getCusNum());
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01CUN("0");
			    flexLog("Input data error " + e);
			}
			try {
			 	if (req.getParameter("E01IDN") != null && 
			 		req.getParameter("E01IDN").indexOf("-") != -1	){
//			 	 For PANAMA LONG IDENTIFICATION
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));			 		
			 	}else {
			 		msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));
			 	}
			}
			catch (Exception e)
			{
				msgClientPersonal.setE01IDN("");
			    flexLog("Input data error " + e);
			}
		 	msgClientPersonal.send();	
		 	msgClientPersonal.destroy();
		 	flexLog("ESD008001 Message Sent");
		 	
		}		
		catch (Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		// Receive Error Message
		try{
		  newmessage = mcID.receiveMessage();
		  
		  if (newmessage.getFormatName().equals("ELEERR")) {
			msgError = (ELEERRMessage)newmessage;
			IsNotError = msgError.getERRNUM().equals("0");
			flexLog("IsNotError = " + IsNotError);
			showERROR(msgError);
		  }
		  else
			flexLog("Message " + newmessage.getFormatName() + " received.");
			
		}
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		// Receive Data
		try{
			newmessage = mcID.receiveMessage();
			
			if (newmessage.getFormatName().equals("ESD008001")) {
				try {
					msgClientPersonal = new datapro.eibs.beans.ESD008001Message();
					flexLog("ESD008001 Message Received");
			  	} catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}

				msgClientPersonal = (ESD008001Message)newmessage;

				userPO.setCusNum(msgClientPersonal.getE01CUN());
				userPO.setOption("CLIENT_P");
				userPO.setCusType(msgClientPersonal.getE01LGT());
				userPO.setHeader1(msgClientPersonal.getE01CUN());
				userPO.setHeader2(msgClientPersonal.getE01IDN());
				userPO.setHeader3(msgClientPersonal.getE01NA1());
				
				if (!msgClientPersonal.getE01IDN().equals("") &&
					msgClientPersonal.getE01IDN().indexOf("-") != -1 ) {
					
					userPO.setHeader2(msgClientPersonal.getE01IDN());
				}
				
				session.setAttribute("paginaPS", "4040");
				session.setAttribute("servlet", "4040");
				session.setAttribute("operation", "2");
				
				session.setAttribute("IDuser", msgClientPersonal.getE01CUN() );
				session.setAttribute("RUTuser",msgClientPersonal.getE01IDN() );
				session.setAttribute("NOBuser",msgClientPersonal.getE01NA1());
				
				flexLog("Putting java beans into the session");

				if (!IsNotError) {  // There are no errors
					try {
						
						flexLog("About to call Page: /pages/" + LangPath + "ESD4040_socio_maintance_both_enter.jsp");
						forward("ESD4040_socio_maintance_both_enter.jsp", req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
			
			else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		}
		catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		// Send Initial data
		try{
			mc = new MessageContext(super.getMessageHandler("ESD4040", req));
			msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
			msgPlanSocio.setH01USR(user.getH01USR());
			msgPlanSocio.setH01PGM("ESD4040");
			msgPlanSocio.setH01TIM(getTimeStamp());
			msgPlanSocio.setH01SCR("01");
			msgPlanSocio.setH01OPE("0002");
			
			try {
			 	if (req.getParameter("E01CUN") != null){
			 		msgPlanSocio.setL01FILLE1(msgClientPersonal.getE01CUN()!=null ? msgClientPersonal.getE01CUN() : userPO.getCusNum());
			 		userPO.setCusNum(msgPlanSocio.getL01FILLE1().toString());
			 	}
			}catch (Exception e){
				msgPlanSocio.setL01FILLE1("0");
			    flexLog("Input data error " + e);
			}
			
			try {
			 	if (req.getParameter("E01IDN") != null && 
			 		req.getParameter("E01IDN").indexOf("-") != -1	){
//			 	 For PANAMA LONG IDENTIFICATION
			 		msgPlanSocio.setL01FILLE5(msgClientPersonal.getE01IDN());
			 		userPO.setHeader2(msgPlanSocio.getL01FILLE5().toString());
			 	}else {
			 		msgPlanSocio.setL01FILLE5(msgClientPersonal.getE01IDN());
			 		userPO.setHeader2(msgPlanSocio.getL01FILLE5().toString());
			 	}
			 	msgPlanSocio.setE01PSID(msgPlanSocio.getL01FILLE5());
			}catch (Exception e){
				msgPlanSocio.setL01FILLE5("");
			    flexLog("Input data error " + e);
			}
			msgPlanSocio.send();	
			msgPlanSocio.destroy();
		 	flexLog("ESD404001 Message Sent");	
		 		
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		//recibe data
		try{
			newmessage2 = mc.receiveMessage();
			if(newmessage2.getFormatName().equals("ESD404001")){
				try {
					msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
					flexLog("ESD404001 Message Received");
			  	} catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}
			  	
			  	
			  	msgPlanSocio = (ESD404001Message)newmessage2;
			  	userPO.setCusNum(msgPlanSocio.getL01FILLE1());
				userPO.setOption("CLIENT_P");
				req.setAttribute("IDN", msgClientPersonal.getE01IDN());
				
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("planSocio", msgPlanSocio);
				session.setAttribute("userPO", userPO);
				session.setAttribute("userPOAux", userPOAux);
				if (IsNotError) { 
					try{
						if (msgPlanSocio.getL01FILLE4().equals("S")){
							flexLog("ESD404001 Message Sent");
							forward("ESD4040_plan_socio_maintance.jsp", req, res);
						}else{
							flexLog("ESD404001 Message Sent");	
							forward("ESD4040_plan_socio_validation.jsp", req, res);
						}
					}catch (Exception e) {
						flexLog("Exception calling page " + e);
						flexLog("About to call Page: " + LangPath + "ESD4040_socio_maintance_both_enter.jsp");
						forward("ESD4040_socio_maintance_both_enter.jsp", req, res);
					}
				}
				else {  // There are errors
					try {
						flexLog("About to call Page: " + LangPath + "ESD4040_socio_maintance_both_enter.jsp");
						forward("ESD4040_socio_maintance_both_enter.jsp", req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
	}//fin metodo procActionMaintanceSocio
	
	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqEnterConsult(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
		throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		if(numCuentaLAlist.size() > 0){
			numCuentaLAlist.clear();
		}
	
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos(); 
			userPO.setOption("PSOCIO");
			userPO.setPurpose("CONSULT");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);
		} catch (Exception ex) {
			flexLog("Error: " + ex); 
		}

		try {
			flexLog("About to call Page: " + LangPath + "ESD4040_socio_consult_both_enter.jsp");
			forward("ESD4040_socio_consult_both_enter.jsp", req, res);	
		}catch (Exception e) {
			flexLog("Exception calling page " + e);
		}
	}
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	protected void procActionEnterSocioConsult(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) 
		throws ServletException, IOException {
	
		MessageRecord newmessage = null;
		MessageRecord newmessage2 = null;
		MessageContext mcID = null;
		ESD008001Message msgClientPersonal = null;
		ESD404001Message msgPlanSocio = null;
		ELEERRMessage msgError = null;
		String decisionaval ="Respuesta NO Esperada";
	
		UserPos	userPO = null;
		UserPos	userPOAux = null;	
		boolean IsNotError = false;
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
		}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		userPO.setCusType(userPO.getHeader2());
	
		// Send Initial data
		try{
			mcID = new MessageContext(super.getMessageHandler("ESD0080", req));
			msgClientPersonal = (ESD008001Message)mcID.getMessageRecord("ESD008001");
			msgClientPersonal.setH01USR(user.getH01USR());
			msgClientPersonal.setH01PGM("ESD0080");
			msgClientPersonal.setH01TIM(getTimeStamp());
			msgClientPersonal.setH01SCR("01");
			msgClientPersonal.setH01OPE("0002");
			
			try {
				if (req.getParameter("E01CUN") != null){
					msgClientPersonal.setE01CUN(req.getParameter("E01CUN"));
				}
			}catch (Exception e){
				msgClientPersonal.setE01CUN("0");
				flexLog("Input data error " + e);
			}
			
			try {
				if (req.getParameter("E01IDN") != null && req.getParameter("E01IDN").indexOf("-") != -1	){
					msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));			 		
				}else {
					msgClientPersonal.setE01IDN(req.getParameter("E01IDN"));
				}
			}catch (Exception e){
				msgClientPersonal.setE01IDN("");
				flexLog("Input data error " + e);
			}
				
			try {
				if (req.getParameter("statusMotor") != null ){
					decisionaval = req.getParameter("statusMotor") ;			 		
				}else {
					decisionaval = req.getParameter("statusMotor");
				}
			}catch (Exception e){
				msgClientPersonal.setE01IDN("");
				flexLog("Input data error " + e);
			}
			
			msgClientPersonal.send();	
			msgClientPersonal.destroy();
			flexLog("ESD008001 Message Sent");
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	
		// Receive Error Message
		try{
			newmessage = mcID.receiveMessage();
	  	  	if (newmessage.getFormatName().equals("ELEERR")) {
	  	  		msgError = (ELEERRMessage)newmessage;
	  	  		IsNotError = msgError.getERRNUM().equals("0");
	  	  		flexLog("IsNotError = " + IsNotError);
	  	  		showERROR(msgError);
	  	  	}
	  	  	else
	  	  		flexLog("Message " + newmessage.getFormatName() + " received.");	
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}	
	
		// Receive Data
		try{
			newmessage = mcID.receiveMessage();		
			if (newmessage.getFormatName().equals("ESD008001")) {
				try {
					msgClientPersonal = new datapro.eibs.beans.ESD008001Message();
					flexLog("ESD008001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex); 
				}

				msgClientPersonal = (ESD008001Message)newmessage;
				userPO.setCusNum(msgClientPersonal.getE01CUN());
				userPO.setOption("CLIENT_P");
				userPO.setCusType(msgClientPersonal.getE01IDN());
				userPO.setHeader1(msgClientPersonal.getE01CUN());
				userPO.setCusName(msgClientPersonal.getE01CUN());
				userPO.setHeader2(msgClientPersonal.getE01IDN());
				userPO.setHeader3(msgClientPersonal.getE01NA1());
				
				session.setAttribute("IDuser", msgClientPersonal.getE01CUN() );
				session.setAttribute("RUTuser",msgClientPersonal.getE01IDN() );
				session.setAttribute("NOBuser",msgClientPersonal.getE01NA1());
				
				session.setAttribute("paginaPS", "4040");
				session.setAttribute("operation", "1");
				
				if (!msgClientPersonal.getE01IDN().equals("") && msgClientPersonal.getE01IDN().indexOf("-") != -1 ) {
					userPO.setHeader2(msgClientPersonal.getE01IDN());
				}
				flexLog("Putting java beans into the session");

				if (!IsNotError) {  // There are no errors
					try {
						flexLog("About to call Page: /pages/" + LangPath + "ESD4040_socio_consult_both_enter.jsp");
						forward("ESD4040_socio_consult_both_enter.jsp", req, res);
						mcID.close();
					}catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}		
			}		
			else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		}catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	
		// Send Initial data
		try{
			mc = new MessageContext(super.getMessageHandler("ESD4040", req));
			msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
			msgPlanSocio.setH01USR(user.getH01USR());
			msgPlanSocio.setH01PGM("ESD4040");
			msgPlanSocio.setH01TIM(getTimeStamp());
			msgPlanSocio.setH01SCR("01");
			msgPlanSocio.setH01OPE("0015");
			msgPlanSocio.setL01FILLE4("N");
	 	try {
		 	if (req.getParameter("E01CUN") != null){
		 		msgPlanSocio.setL01FILLE1(msgClientPersonal.getE01CUN()!=null ? msgClientPersonal.getE01CUN() : userPO.getCusNum());
		 		userPO.setCusNum(msgPlanSocio.getL01FILLE1().toString());
		 	}
		}catch (Exception e){
			msgPlanSocio.setL01FILLE1("0");
		    flexLog("Input data error " + e);
		}
		
		try {
		 	if (req.getParameter("E01IDN") != null && 
		 		req.getParameter("E01IDN").indexOf("-") != -1	){
//		 	 For PANAMA LONG IDENTIFICATION
		 		msgPlanSocio.setL01FILLE5(msgClientPersonal.getE01IDN());
		 		userPO.setHeader2(msgPlanSocio.getL01FILLE5().toString());
		 	}else {
		 		msgPlanSocio.setL01FILLE5(msgClientPersonal.getE01IDN());
		 		userPO.setHeader2(msgPlanSocio.getL01FILLE5().toString());
		 	}
		 	msgPlanSocio.setE01PSID(msgPlanSocio.getL01FILLE5().toString());
		}catch (Exception e){
			msgPlanSocio.setL01FILLE5("");
		    flexLog("Input data error " + e);
		}
		msgPlanSocio.send();	
		msgPlanSocio.destroy();
	}		
	catch (Exception e){
		e.printStackTrace();
		flexLog("Error: " + e);
	  	throw new RuntimeException("Socket Communication Error");
	}	
	
	// Receive Error Message
	try{
		newmessage2 = mc.receiveMessage();
  	  	if (newmessage2.getFormatName().equals("ELEERR")) {
  	  		msgError = (ELEERRMessage)newmessage2;
  	  		IsNotError = msgError.getERRNUM().equals("0");
  	  		flexLog("IsNotError = " + IsNotError);
  	  		showERROR(msgError);
  	  	}
  	  	else
  	  		flexLog("Message " + newmessage.getFormatName() + " received.");	
	}catch (Exception e){
		e.printStackTrace();
		flexLog("Error: " + e);
		throw new RuntimeException("Socket Communication Error");
	}	
	
	// Receive Data
	try{
		newmessage2 = mc.receiveMessage();
		if (newmessage2.getFormatName().equals("ESD404001")) {
			try {
				msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
				flexLog("ESD404001 Message Received");
		  	} catch (Exception ex) {
				flexLog("Error: " + ex); 
		  	}
		  	
			msgPlanSocio = (ESD404001Message)newmessage2;
			userPO.setCusNum(msgPlanSocio.getL01FILLE1());
			userPO.setOption("CLIENT_P");
			req.setAttribute("IDN", msgClientPersonal.getE01IDN());
					
			if (IsNotError) {  // There are no errors				 				
				try {
					if (msgPlanSocio.getL01FILLE4().equals("S")){
						session.setAttribute("banderaPag", "1");
						userPOAux = userPO;
						flexLog("Putting java beans into the session");
						session.setAttribute("planSocio", msgPlanSocio);
						session.setAttribute("userPO", userPO);
						session.setAttribute("userPOAux", userPOAux);
						session.setAttribute("servlet", "4040");
						flexLog("ESD404001 Message Sent");	
						forward("ESD4040_plan_socio_consult.jsp", req, res);
					}else{
						flexLog("ESD404001 Message Sent");	
						forward("ESD4040_plan_socio_validation.jsp", req, res);
					}
				}catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			}
			else {  // There are errors
				try {
					flexLog("About to call Page: " + LangPath + "ESD4040_socio_consult_both_enter.jsp");
					forward("ESD4040_socio_consult_both_enter.jsp", req, res);	
				}
				catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			}
		}else{
			try {
				flexLog("About to call Page: " + LangPath + "ESD4040_socio_consult_both_enter.jsp");
				forward("ESD4040_socio_consult_both_enter.jsp", req, res);	
			}
			catch (Exception e) {
				flexLog("Exception calling page " + e);
			}
		}//fin if newmessage2
	}catch(Exception e){
		e.printStackTrace();
		flexLog("Error: " + e);
	  	throw new RuntimeException("Socket Communication Error");
	}
}//fin metodo procActionEnterSocioConsult
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	/**
	 * 
	 */
	protected void procActionEnterSocioList(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) 
		throws ServletException, IOException {
		
		MessageRecord newmessage = null;
		ESD404001Message msgPlanSocio = null;
		ELEERRMessage msgError = null;
		UserPos	userPOAux = null;	
		boolean IsNotError = false;
	
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
		}

		userPOAux = (datapro.eibs.beans.UserPos)session.getAttribute("userPOAux");
		msgPlanSocio = (datapro.eibs.beans.ESD404001Message)session.getAttribute("planSocio");
		
		// Send Initial data
		try{
			mc = new MessageContext(super.getMessageHandler("ESD4040", req));
			msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
			msgPlanSocio.setH01USR(user.getH01USR());
			msgPlanSocio.setH01PGM("ESD4040");
			msgPlanSocio.setH01TIM(getTimeStamp());
			msgPlanSocio.setH01SCR("01");
			msgPlanSocio.setH01OPE("0015");
			msgPlanSocio.setL01FILLE1(userPOAux.getCusNum());
			msgPlanSocio.setL01FILLE5(userPOAux.getHeader2());
			msgPlanSocio.setE01PSID(userPOAux.getHeader2());
		
			msgPlanSocio.send();	
			msgPlanSocio.destroy();	
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
		
		// Receive Data
		try{
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ESD404001")) {
				try {
					msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
					flexLog("ESD404001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex); 
				}
		  	
				msgPlanSocio = (ESD404001Message)newmessage;
				
			
				if (!IsNotError) {  // There are no errors
					try {
						session.setAttribute("planSocio", msgPlanSocio);
						session.setAttribute("userPOAux", userPOAux);
						String banRes = session.getAttribute("operation").toString();
						if(banRes == "1"){
							flexLog("ESD404001 Message Sent");	
							forward("ESD4040_plan_socio_new.jsp", req, res);
						}
						if(banRes == "2"){
							flexLog("ESD404001 Message Sent");	
							forward("ESD4040_plan_socio_maintance.jsp", req, res);
						}
					}catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
				else {  // There are errors
					try {
						flexLog("About to call Page: " + LangPath + "ESD4040_socio_both_enter.jsp");
						forward("ESD4040_socio_both_enter.jsp", req, res);	
					}catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}		
	}// fin procActionEnterSocioList
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqMaintance(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses) 
		throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		if(numCuentaLAlist.size() > 0){
			numCuentaLAlist.clear();
		}

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos(); 
			userPO.setOption("PSOCIO");
			userPO.setPurpose("MAINTANCE");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);
		} catch (Exception ex) {
			flexLog("Error: " + ex); 
		}

		try {
			flexLog("About to call Page: " + LangPath + "ESD4040_socio_maintance_both_enter.jsp");
			forward("ESD4040_socio_maintance_both_enter.jsp", req, res);	
		}catch (Exception e) {
			flexLog("Exception calling page " + e);
		}
	}
	
	protected void procActionEnterSocio(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {

		MessageRecord newmessage = null;
		MessageContext mc1 = null;
		MessageContext mc2 = null;
		MessageContext mc3 = null;
		ESD404001Message msgPlanSocio = null;
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		boolean IsNotError = false;
		String numeroCV = "";
		String numeroTC = "";
		boolean CV = false;
		boolean TC = false;
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		numeroCV = req.getParameter("numCuentaCV");
		numeroTC = req.getParameter("numCuentaTC");
		
		if (numeroCV.equals(""))
			numeroCV = "0";
		
		if (numeroTC.equals(""))
			numeroTC = "0";
		
		if(numCuentaLAlist.size() > 0){
			for(int i=0; i < numCuentaLAlist.size();  i++){
				// Send Initial data
				try{
					flexLog("Entering to Plan Socio");
		
					mc = new MessageContext(super.getMessageHandler("ESD4040", req));
					msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
					msgPlanSocio.setH01USR(user.getH01USR());
					msgPlanSocio.setH01PGM("ESD4040");
					msgPlanSocio.setH01TIM(getTimeStamp());
					msgPlanSocio.setH01SCR(String.valueOf(i + 1));
					msgPlanSocio.setH01OPE("0001");
					
					msgPlanSocio.setL01FILLE1(session.getAttribute("IDuser").toString());
					msgPlanSocio.setL01FILLE2("1");
					msgPlanSocio.setL01FILLE5(session.getAttribute("RUTuser").toString());					
					msgPlanSocio.setE01PSTCP(req.getParameter("E01PSTCP")); 					
					msgPlanSocio.setE01PSNUCV(numeroCV);//numero de cuenta Vista					
					msgPlanSocio.setE01PSNULA(numCuentaLAlist.get(i));//nuemro de libreta de ahorro
					msgPlanSocio.setE01PSNUTC(numeroTC);//nuemro de tarjeta credito
							
					msgPlanSocio.send();	
					msgPlanSocio.destroy();	
					flexLog("ESD404001 Message Sent");
				}catch (Exception e){
					e.printStackTrace();
					flexLog("Error: " + e);
				  	throw new RuntimeException("Socket Communication Error");
				}
				
				// Receive Data
				try{
					newmessage = mc.receiveMessage();
					
					if (newmessage.getFormatName().equals("ELEERR")) {
						msgError = (ELEERRMessage)newmessage;
						IsNotError = msgError.getERRNUM().equals("0");
						flexLog("IsNotError = " + IsNotError);
						showERROR(msgError);
						break;
					  }
					  else
						flexLog("Message " + newmessage.getFormatName() + " received.");	
					
					
				}catch (Exception e)	{
					e.printStackTrace();
					flexLog("Error: " + e);
				  	throw new RuntimeException("Socket Communication Error");
				}
			}
			
			if (newmessage.getFormatName().equals("ESD404001")) {
				try {
					msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
					flexLog("ESD404001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex); 
				}
				msgPlanSocio = (ESD404001Message)newmessage;
				
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("client", msgPlanSocio);
				session.setAttribute("userPO", userPO);
				session.setAttribute("IPplanSocio", "PS"+session.getAttribute("IDuser").toString());
					
				if (!IsNotError) {  // There are no errors
					try {
						
						mp = getMessageProcessor("ESD4080", req);
						
						ESD408001Message msgPlan = (ESD408001Message) mp.getMessageRecord("ESD408001");
						
						msgPlan.setH01USR(user.getH01USR());
						msgPlan.setH01PGM("ESD4080");
						msgPlan.setH01TIM(getTimeStamp());
						msgPlan.setH01SCR("01");
						msgPlan.setH01OPE("0008");
						msgPlan.setE01CUN(session.getAttribute("IDuser").toString());
						
						mp.sendMessage(msgPlan);
						
						JBObjList list = mp.receiveMessageRecordList("H01MAS");
						int i = 0;
						ESD408001Message msgList = null;
						list.initRow();
						while (list.getNextRow()) {
			            	msgList = (ESD408001Message) list.getRecord();
			            	//Aprobar CV
			            	if (!msgList.getE01ACCV().equals("0") && !CV){
			            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCV());
			            		CV = true;
			            	}
			            	//Aprobar Libretas
			            	if (!msgList.getE01ACCH().equals("0"))
			            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCH());
			            	//Aprobar TC
			            	if (!msgList.getE01ACCT().equals("0") && !TC){
			            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCT());
			            		TC = true;
			            	}
						}
						
						//Aprobar Plan.
						try{
							mc2 = new MessageContext(super.getMessageHandler("ESD4080", req));
							ESD408001Message msgAprobacion = (ESD408001Message) mc2.getMessageRecord("ESD408001");
							
							msgAprobacion.setH01USR(user.getH01USR());
							msgAprobacion.setH01PGM("ESD4080");
							msgAprobacion.setH01TIM(getTimeStamp());
							msgAprobacion.setH01SCR("01");
							msgAprobacion.setH01OPE("0001");
							msgAprobacion.setE01CUN(session.getAttribute("IDuser").toString());
							msgAprobacion.setE01RUT(session.getAttribute("RUTuser").toString());
							
							msgAprobacion.send();	
							msgAprobacion.destroy();	
							flexLog("ESD408001 Message Sent");
						}catch (Exception e){
							e.printStackTrace();
							flexLog("Error: " + e);
							throw new RuntimeException("Socket Communication Error");
						}
						
						// Receive Data
						try{
							newmessage = mc2.receiveMessage();
							
							if (newmessage.getFormatName().equals("ELEERR")) {
								msgError = (ELEERRMessage)newmessage;
								IsNotError = msgError.getERRNUM().equals("0");
								flexLog("IsNotError = " + IsNotError);
							  }
							  else{
								  flexLog("Message " + newmessage.getFormatName() + " received.");
							  }
						}catch (Exception e)	{
							e.printStackTrace();
							flexLog("Error: " + e);
						  	throw new RuntimeException("Socket Communication Error");
						}
						
						flexLog("About to call Page: " + LangPath + "ESD4040_plan_socio_confirm.jsp");
						forward("ESD4040_plan_socio_confirm.jsp", req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
				else {  // There are errors
					try {
						flexLog("About to call Page: " + LangPath + "ESD4040_plan_socio_new.jsp");
						forward("ESD4040_plan_socio_new.jsp", req, res);
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
		}else{
			// Send Initial data
			try{
				flexLog("Entering to Plan Socio");
								
				mc = new MessageContext(super.getMessageHandler("ESD4040", req));
				msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
				msgPlanSocio.setH01USR(user.getH01USR());
				msgPlanSocio.setH01PGM("ESD4040");
				msgPlanSocio.setH01TIM(getTimeStamp());
				msgPlanSocio.setH01SCR("01");
				msgPlanSocio.setH01OPE("0001");
				
				msgPlanSocio.setL01FILLE1(session.getAttribute("IDuser").toString());
				msgPlanSocio.setL01FILLE2("1");
				msgPlanSocio.setL01FILLE5(session.getAttribute("RUTuser").toString());					
				msgPlanSocio.setE01PSTCP(req.getParameter("E01PSTCP")); 					
				msgPlanSocio.setE01PSNUCV(numeroCV);//numero de cuenta Vista					
				msgPlanSocio.setE01PSNULA("0");//nuemro de libreta de ahorro
				msgPlanSocio.setE01PSNUTC(numeroTC);//nuemro de tarjeta credito
						
				msgPlanSocio.send();	
				msgPlanSocio.destroy();	
				flexLog("ESD404001 Message Sent");
			}catch (Exception e){
				e.printStackTrace();
				flexLog("Error: " + e);
			  	throw new RuntimeException("Socket Communication Error");
			}
			
			// Receive Data
			try{
				newmessage = mc.receiveMessage();
				
				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage)newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				  }
				  else
					flexLog("Message " + newmessage.getFormatName() + " received.");	
				
				if (newmessage.getFormatName().equals("ESD404001")) {
					try {
						msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
						flexLog("ESD404001 Message Received");
					} catch (Exception ex) {
						flexLog("Error: " + ex); 
					}
					msgPlanSocio = (ESD404001Message)newmessage;
					
					flexLog("Putting java beans into the session");
					session.setAttribute("error", msgError);
					session.setAttribute("client", msgPlanSocio);
					session.setAttribute("userPO", userPO);
					session.setAttribute("IPplanSocio", "PS"+session.getAttribute("IDuser").toString());
						
					if (!IsNotError) {  // There are no errors
						try {
							
							mp = getMessageProcessor("ESD4080", req);
							
							ESD408001Message msgPlan = (ESD408001Message) mp.getMessageRecord("ESD408001");
							
							msgPlan.setH01USR(user.getH01USR());
							msgPlan.setH01PGM("ESD4080");
							msgPlan.setH01TIM(getTimeStamp());
							msgPlan.setH01SCR("01");
							msgPlan.setH01OPE("0008");
							msgPlan.setE01CUN(session.getAttribute("IDuser").toString());
							
							mp.sendMessage(msgPlan);
							
							JBObjList list = mp.receiveMessageRecordList("H01MAS");
							int i = 0;
							ESD408001Message msgList = null;
							list.initRow();
							while (list.getNextRow()) {
				            	msgList = (ESD408001Message) list.getRecord();
				            	//Aprobar CV
				            	if (!msgList.getE01ACCV().equals("0") && !CV){
				            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCV());
				            		CV = true;
				            	}
				            	//Aprobar Libretas
				            	if (!msgList.getE01ACCH().equals("0"))
				            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCH());
				            	//Aprobar TC
				            	if (!msgList.getE01ACCT().equals("0") && !TC){
				            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCT());
				            		TC = true;
				            	}
							}
							
							//Aprobar Plan
							try{
								mc2 = new MessageContext(super.getMessageHandler("ESD4080", req));
								ESD408001Message msgAprobacion = (ESD408001Message) mc2.getMessageRecord("ESD408001");
								
								msgAprobacion.setH01USR(user.getH01USR());
								msgAprobacion.setH01PGM("ESD4080");
								msgAprobacion.setH01TIM(getTimeStamp());
								msgAprobacion.setH01SCR("01");
								msgAprobacion.setH01OPE("0001");
								msgAprobacion.setE01CUN(session.getAttribute("IDuser").toString());
								msgAprobacion.setE01RUT(session.getAttribute("RUTuser").toString());
								
								msgAprobacion.send();	
								msgAprobacion.destroy();	
								flexLog("ESD408001 Message Sent");
							}catch (Exception e){
								e.printStackTrace();
								flexLog("Error: " + e);
								throw new RuntimeException("Socket Communication Error");
							}
							
							// Receive Data
							try{
								newmessage = mc2.receiveMessage();
								
								if (newmessage.getFormatName().equals("ELEERR")) {
									msgError = (ELEERRMessage)newmessage;
									IsNotError = msgError.getERRNUM().equals("0");
									flexLog("IsNotError = " + IsNotError);
								  }
								  else{
									  flexLog("Message " + newmessage.getFormatName() + " received.");
								  }
							}catch (Exception e)	{
								e.printStackTrace();
								flexLog("Error: " + e);
							  	throw new RuntimeException("Socket Communication Error");
							}
							
							flexLog("About to call Page: " + LangPath + "ESD4040_plan_socio_confirm.jsp");
							forward("ESD4040_plan_socio_confirm.jsp", req, res);	
						}
						catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
					else {  // There are errors
						try {
							flexLog("About to call Page: " + LangPath + "ESD4040_plan_socio_new.jsp");
							forward("ESD4040_plan_socio_new.jsp", req, res);
						}
						catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			}catch (Exception e)	{
				e.printStackTrace();
				flexLog("Error: " + e);
			  	throw new RuntimeException("Socket Communication Error");
			}
		}
	}
	
	protected void procActionUpdateSocio(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {

		MessageRecord newmessage = null;
		MessageContext mc1 = null;
		MessageProcessor mp = null;
		ESD404001Message msgPlanSocio = null;
		ELEERRMessage msgError = null;
		UserPos	userPO = null;	
		boolean IsNotError = false;
		String numeroCV = "";
		String numeroTC = "";
		String cancelarPlan = null;
		boolean CV = false;
		boolean TC = false;
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
		numeroCV = req.getParameter("numCuentaCV");
		cancelarPlan = req.getParameter("cancelarPlan");
		
		if (numeroCV.equals("") || numeroCV == null)
			numeroCV = "0";
		
		if (numeroTC.equals("") || numeroTC == null)
			numeroTC = "0";
		
		if (!cancelarPlan.equals("S")){
			if(numCuentaLAlist.size() > 0){
				for(int i=0; i < numCuentaLAlist.size();  i++){
					// Send Initial data
					try{
						flexLog("Entering to Plan Socio");
							
						mc = new MessageContext(super.getMessageHandler("ESD4040", req));
						msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
						msgPlanSocio.setH01USR(user.getH01USR());
						msgPlanSocio.setH01PGM("ESD4040");
						msgPlanSocio.setH01TIM(getTimeStamp());
						msgPlanSocio.setH01SCR(String.valueOf(i + 1));
						msgPlanSocio.setH01OPE("0001");
						
						msgPlanSocio.setL01FILLE1(session.getAttribute("IDuser").toString());
						msgPlanSocio.setL01FILLE2("0");
						msgPlanSocio.setL01FILLE5(session.getAttribute("RUTuser").toString());					
						msgPlanSocio.setE01PSTCP(req.getParameter("E01PSTCP")); 					
						msgPlanSocio.setE01PSNUCV(numeroCV);//numero de cuenta Vista					
						msgPlanSocio.setE01PSNULA(numCuentaLAlist.get(i));//nuemro de libreta de ahorro
						msgPlanSocio.setE01PSNUTC(numeroTC);//nuemro de tarjeta credito
						msgPlanSocio.setE01PSDCCP(cancelarPlan);
								
						msgPlanSocio.send();	
						msgPlanSocio.destroy();	
						flexLog("ESD404001 Message Sent");
					}catch (Exception e){
						e.printStackTrace();
						flexLog("Error: " + e);
					  	throw new RuntimeException("Socket Communication Error");
					}
					
					// Receive Data
					try{
						newmessage = mc.receiveMessage();
						
						if (newmessage.getFormatName().equals("ELEERR")) {
							msgError = (ELEERRMessage)newmessage;
							IsNotError = msgError.getERRNUM().equals("0");
							flexLog("IsNotError = " + IsNotError);
							showERROR(msgError);
							break;
						  }
						  else
							flexLog("Message " + newmessage.getFormatName() + " received.");	
						
						
					}catch (Exception e)	{
						e.printStackTrace();
						flexLog("Error: " + e);
						flexLog("Error: " + e);
					  	throw new RuntimeException("Socket Communication Error");
					}
				}
				
				if (newmessage.getFormatName().equals("ESD404001")) {
					try {
						msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
						flexLog("ESD404001 Message Received");
					} catch (Exception ex) {
						flexLog("Error: " + ex); 
					}
					msgPlanSocio = (ESD404001Message)newmessage;
					
					flexLog("Putting java beans into the session");
					session.setAttribute("error", msgError);
					session.setAttribute("client", msgPlanSocio);
					session.setAttribute("userPO", userPO);
					session.setAttribute("IPplanSocio", "PS"+session.getAttribute("IDuser").toString());
						
					if (!IsNotError) {  // There are no errors
						try {
							mp = getMessageProcessor("ESD4080", req);
							
							ESD408001Message msgPlan = (ESD408001Message) mp.getMessageRecord("ESD408001");
							
							msgPlan.setH01USR(user.getH01USR());
							msgPlan.setH01PGM("ESD4080");
							msgPlan.setH01TIM(getTimeStamp());
							msgPlan.setH01SCR("01");
							msgPlan.setH01OPE("0008");
							msgPlan.setE01CUN(session.getAttribute("IDuser").toString());
							
							mp.sendMessage(msgPlan);
							
							JBObjList list = mp.receiveMessageRecordList("H01MAS");
							int i = 0;
							ESD408001Message msgList = null;
							list.initRow();
							while (list.getNextRow()) {
				            	msgList = (ESD408001Message) list.getRecord();
				            	//Aprobar CV
				            	if (!msgList.getE01ACCV().equals("0") && !CV){
				            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCV());
				            		CV = true;
				            	}
				            	//Aprobar Libretas
				            	if (!msgList.getE01ACCH().equals("0"))
				            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCH());
				            	//Aprobar TC
				            	if (!msgList.getE01ACCT().equals("0") && !TC){
				            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCT());
				            		TC = true;
				            	}
							}
							
							flexLog("About to call Page: " + LangPath + "ESD4040_plan_socio_confirm.jsp");
							forward("ESD4040_plan_socio_confirm.jsp", req, res);	
						}
						catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
					else {  // There are errors
						try {
							flexLog("About to call Page: " + LangPath + "ESD4040_plan_socio_new.jsp");
							forward("ESD4040_plan_socio_new.jsp", req, res);
						}
						catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			}else{
				// Send Initial data
				try{
					flexLog("Entering to Plan Socio");
									
					mc = new MessageContext(super.getMessageHandler("ESD4040", req));
					msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
					msgPlanSocio.setH01USR(user.getH01USR());
					msgPlanSocio.setH01PGM("ESD4040");
					msgPlanSocio.setH01TIM(getTimeStamp());
					msgPlanSocio.setH01SCR("01");
					msgPlanSocio.setH01OPE("0001");
					
					msgPlanSocio.setL01FILLE1(session.getAttribute("IDuser").toString());
					msgPlanSocio.setL01FILLE2("0");
					msgPlanSocio.setL01FILLE5(session.getAttribute("RUTuser").toString());					
					msgPlanSocio.setE01PSTCP(req.getParameter("E01PSTCP")); 	
					msgPlanSocio.setE01PSNUCV(numeroCV);//numero de cuenta Vista					
					msgPlanSocio.setE01PSNULA("0");//nuemro de libreta de ahorro
					msgPlanSocio.setE01PSNUTC(numeroTC);//nuemro de tarjeta credito
					msgPlanSocio.setE01PSDCCP(cancelarPlan);
							
					msgPlanSocio.send();	
					msgPlanSocio.destroy();	
					flexLog("ESD404001 Message Sent");
				}catch (Exception e){
					e.printStackTrace();
					flexLog("Error: " + e);
				  	throw new RuntimeException("Socket Communication Error");
				}
				
				// Receive Data
				try{
					newmessage = mc.receiveMessage();
					
					if (newmessage.getFormatName().equals("ELEERR")) {
						msgError = (ELEERRMessage)newmessage;
						IsNotError = msgError.getERRNUM().equals("0");
						flexLog("IsNotError = " + IsNotError);
						showERROR(msgError);
					  }
					  else
						flexLog("Message " + newmessage.getFormatName() + " received.");	
					
					if (newmessage.getFormatName().equals("ESD404001")) {
						try {
							msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
							flexLog("ESD404001 Message Received");
						} catch (Exception ex) {
							flexLog("Error: " + ex); 
						}
						msgPlanSocio = (ESD404001Message)newmessage;
						
						flexLog("Putting java beans into the session");
						session.setAttribute("error", msgError);
						session.setAttribute("client", msgPlanSocio);
						session.setAttribute("userPO", userPO);
						session.setAttribute("IPplanSocio", "PS"+session.getAttribute("IDuser").toString());
							
						if (!IsNotError) {  // There are no errors
							try {
								mp = getMessageProcessor("ESD4080", req);
								
								ESD408001Message msgPlan = (ESD408001Message) mp.getMessageRecord("ESD408001");
								
								msgPlan.setH01USR(user.getH01USR());
								msgPlan.setH01PGM("ESD4080");
								msgPlan.setH01TIM(getTimeStamp());
								msgPlan.setH01SCR("01");
								msgPlan.setH01OPE("0008");
								msgPlan.setE01CUN(session.getAttribute("IDuser").toString());
								
								mp.sendMessage(msgPlan);
								
								JBObjList list = mp.receiveMessageRecordList("H01MAS");
								int i = 0;
								ESD408001Message msgList = null;
								list.initRow();
								while (list.getNextRow()) {
					            	msgList = (ESD408001Message) list.getRecord();
					            	//Aprobar CV
					            	if (!msgList.getE01ACCV().equals("0") && !CV){
					            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCV());
					            		CV = true;
					            	}
					            	//Aprobar Libretas
					            	if (!msgList.getE01ACCH().equals("0"))
					            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCH());
					            	//Aprobar TC
					            	if (!msgList.getE01ACCT().equals("0") && !TC){
					            		procActionApprovalProductos(mc, user, req, res, session, msgList.getE01ACCT());
					            		TC = true;
					            	}
								}
								
								flexLog("About to call Page: " + LangPath + "ESD4040_plan_socio_confirm.jsp");
								forward("ESD4040_plan_socio_confirm.jsp", req, res);	
							}
							catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						}
						else {  // There are errors
							try {
								flexLog("About to call Page: " + LangPath + "ESD4040_plan_socio_new.jsp");
								forward("ESD4040_plan_socio_new.jsp", req, res);
							}
							catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						}
					}
				}catch (Exception e)	{
					e.printStackTrace();
					flexLog("Error: " + e);
				  	throw new RuntimeException("Socket Communication Error");
				}
			}
		}else{
			// Send Initial data
			try{
				flexLog("Entering to Plan Socio");
							
				mc = new MessageContext(super.getMessageHandler("ESD4040", req));
				msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
				msgPlanSocio.setH01USR(user.getH01USR());
				msgPlanSocio.setH01PGM("ESD4040");
				msgPlanSocio.setH01TIM(getTimeStamp());
				msgPlanSocio.setH01SCR("01");
				msgPlanSocio.setH01OPE("0003");
				
				msgPlanSocio.setL01FILLE1(session.getAttribute("IDuser").toString());
				msgPlanSocio.setL01FILLE5(session.getAttribute("RUTuser").toString());
				msgPlanSocio.setE01PSDCCP(cancelarPlan);
						
				msgPlanSocio.send();	
				msgPlanSocio.destroy();	
				flexLog("ESD404001 Message Sent");
			}catch (Exception e){
				e.printStackTrace();
				flexLog("Error: " + e);
			  	throw new RuntimeException("Socket Communication Error");
			}
			
			// Receive Data
			try{
				newmessage = mc.receiveMessage();
				
				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage)newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				  }
				  else
					flexLog("Message " + newmessage.getFormatName() + " received.");	
				
				if (newmessage.getFormatName().equals("ESD404001")) {
					try {
						msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
						flexLog("ESD404001 Message Received");
					} catch (Exception ex) {
						flexLog("Error: " + ex); 
					}
					msgPlanSocio = (ESD404001Message)newmessage;
					
					flexLog("Putting java beans into the session");
					session.setAttribute("error", msgError);
					session.setAttribute("client", msgPlanSocio);
					session.setAttribute("userPO", userPO);
					session.setAttribute("IPplanSocio", req.getParameter("E01PSTCP")+session.getAttribute("IDuser").toString());
						
					if (!IsNotError) {  // There are no errors
						try {
							flexLog("About to call Page: " + LangPath + "ESD4040_plan_socio_confirm.jsp");
							forward("ESD4040_plan_socio_confirm.jsp", req, res);	
						}
						catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
					else {  // There are errors
						try {
							flexLog("About to call Page: " + LangPath + "ESD4040_plan_socio_new.jsp");
							forward("ESD4040_plan_socio_new.jsp", req, res);
						}
						catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				}
			}catch (Exception e)	{
				e.printStackTrace();
				flexLog("Error: " + e);
			  	throw new RuntimeException("Socket Communication Error");
			}
		}
	}
	
	protected void procActionMaintanceSocioList(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		MessageRecord newmessage = null;
		ESD404001Message msgPlanSocio = null;
		ELEERRMessage msgError = null;
		UserPos	userPOAux = null;
		boolean IsNotError = false;
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		userPOAux = (datapro.eibs.beans.UserPos)session.getAttribute("userPOAux");
		msgPlanSocio = (datapro.eibs.beans.ESD404001Message)session.getAttribute("planSocio");

		
		// Send Initial data
		try{
			mc = new MessageContext(super.getMessageHandler("ESD4040", req));
			msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
			msgPlanSocio.setH01USR(user.getH01USR());
			msgPlanSocio.setH01PGM("ESD4040");
			msgPlanSocio.setH01TIM(getTimeStamp());
			msgPlanSocio.setH01SCR("01");
			msgPlanSocio.setH01OPE("0002");
			msgPlanSocio.setL01FILLE1(userPOAux.getCusNum());
			msgPlanSocio.setL01FILLE5(userPOAux.getHeader2());	
			msgPlanSocio.setE01PSID(userPOAux.getHeader2());

			msgPlanSocio.send();	
			msgPlanSocio.destroy();
		 	flexLog("ESD404001 Message Sent");	
		 		
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
		//recibe data
		try{
			newmessage = mc.receiveMessage();
			
			if(newmessage.getFormatName().equals("ESD404001")){
				try {
					msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
					flexLog("ESD404001 Message Received");
			  	} catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}
			  	
			  	
			  	msgPlanSocio = (ESD404001Message)newmessage;

				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("planSocio", msgPlanSocio);
				session.setAttribute("userPOAux", userPOAux);
				if (!IsNotError) { 
					try{	
						flexLog("ESD404001 Message Sent");
						forward("ESD4040_plan_socio_maintance.jsp", req, res);

					}catch (Exception e) {
						flexLog("Exception calling page " + e);
						flexLog("About to call Page: " + LangPath + "ESD4040_socio_maintance_both_enter.jsp");
						forward("ESD4040_socio_maintance_both_enter.jsp", req, res);
					}
				}
				else {  // There are errors
					try {
						flexLog("About to call Page: " + LangPath + "ESD4040_socio_maintance_both_enter.jsp");
						forward("ESD4040_socio_maintance_both_enter.jsp", req, res);	
					}
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
		
	}//fin metodo procActionMaintanceSocio
	
	//------------aprobacion de los productos cuenta vista o una libreta de ahorro-----------------------------------------------------------
	protected void procActionApprovalProductos(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, String numCuenta) 
		throws ServletException, IOException {

		EDD100002Message msgList = null;
		MessageRecord newmessage = null;
		ELEERRMessage msgError = null;
		boolean IsNotError = false;

		// Send Initial data
		try{
		
			flexLog("Send Initial Data");
			msgList = (EDD100002Message)mc.getMessageRecord("EDD100002");
			msgList.setH02USERID(user.getH01USR());
		 	msgList.setH02PROGRM("EDD1000");
		 	msgList.setH02TIMSYS(getTimeStamp());
		 	//session.setAttribute("numCuenta", datapro.eibs.master.Util.justifyRight(rtFinish.getE15ACCNUM().trim(),12));
		 	msgList.setE02ACMACC(numCuenta);
		 	msgList.setE02ACTION("A");
		 	msgList.setE02MSGTXT("");
		 	msgList.send();	
		 	msgList.destroy();
		}catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
		
		try
		{
		  	newmessage = mc.receiveMessage();
		  
		  	if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError = new datapro.eibs.beans.ELEERRMessage();
				} 
				catch (Exception ex) {
					flexLog("Error: " + ex); 
			  	}
				msgError = (ELEERRMessage)newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);

				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);		
		  	}
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}
	}	
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	protected void procActionEnterSocioFormulario(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) 
		throws ServletException, IOException {
	
		MessageRecord newmessage = null;
		MessageRecord newmessage2 = null;
		MessageContext mcID = null;
		ESD008001Message msgClientPersonal = null;
		ESD404001Message msgPlanSocio = null;
		ELEERRMessage msgError = null;
		
		UserPos	userPO = null;
		UserPos	userPOAux = null;	
		boolean IsNotError = false;
		
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception ex) {
			flexLog("Error: " + ex); 
		}

		userPO = (datapro.eibs.beans.UserPos)session.getAttribute("userPO");
	
		
		
		// Send Initial data
		try{
			mc = new MessageContext(super.getMessageHandler("ESD4040", req));
			msgPlanSocio = (ESD404001Message)mc.getMessageRecord("ESD404001");
			msgPlanSocio.setH01USR(user.getH01USR());
			msgPlanSocio.setH01PGM("ESD4040");
			msgPlanSocio.setH01TIM(getTimeStamp());
			msgPlanSocio.setH01SCR("01");
			msgPlanSocio.setH01OPE("0015");
			msgPlanSocio.setL01FILLE4("N");
			msgPlanSocio.setL01FILLE1(userPO.getCusNum());
			msgPlanSocio.setL01FILLE5(userPO.getHeader2()); 	
		
			msgPlanSocio.send();	
			msgPlanSocio.destroy();
		}		
		catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}	
	
		// Receive Error Message
		try{
			newmessage2 = mc.receiveMessage();
	  	  	if (newmessage2.getFormatName().equals("ELEERR")) {
	  	  		msgError = (ELEERRMessage)newmessage2;
	  	  		IsNotError = msgError.getERRNUM().equals("0");
	  	  		flexLog("IsNotError = " + IsNotError);
	  	  		showERROR(msgError);
	  	  	}
	  	  	else
	  	  		flexLog("Message " + newmessage.getFormatName() + " received.");	
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}	
	
	// Receive Data
	try{
		newmessage2 = mc.receiveMessage();
		if (newmessage2.getFormatName().equals("ESD404001")) {
			try {
				msgPlanSocio = new datapro.eibs.beans.ESD404001Message();
				flexLog("ESD404001 Message Received");
		  	} catch (Exception ex) {
				flexLog("Error: " + ex); 
		  	}
		  	
			msgPlanSocio = (ESD404001Message)newmessage2;
			userPO.setCusNum(msgPlanSocio.getL01FILLE1());
			userPO.setOption("CLIENT_P");
					
			if (!IsNotError) {  // There are no errors				 				
				try {
					
					msgPlanSocio.setE01PSNPL("PS" + msgPlanSocio.getL01FILLE1());
					session.setAttribute("banderaPag", "1");
					userPOAux = userPO;
					flexLog("Putting java beans into the session");
					session.setAttribute("planSocio", msgPlanSocio);
					session.setAttribute("userPO", userPO);
					session.setAttribute("userPOAux", userPOAux);
					session.setAttribute("servlet", "4040");
					flexLog("ESD404001 Message Sent");	
					forward("ESD4040_plan_socio_formulario.jsp", req, res);
				}catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			}
			else {  // There are errors
				try {
					flexLog("About to call Page: " + LangPath + "ESD4040_socio_consult_both_enter.jsp");
					forward("ESD4040_socio_consult_both_enter.jsp", req, res);	
				}
				catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			}
		}else{
			try {
				flexLog("About to call Page: " + LangPath + "ESD4040_socio_consult_both_enter.jsp");
				forward("ESD4040_socio_consult_both_enter.jsp", req, res);	
			}
			catch (Exception e) {
				flexLog("Exception calling page " + e);
			}
		}//fin if newmessage2
	}catch(Exception e){
		e.printStackTrace();
		flexLog("Error: " + e);
	  	throw new RuntimeException("Socket Communication Error");
	}
}//fin metodo procActionEnterSocioFormulario


	protected void showERROR(ELEERRMessage m)
	{
		if (logType != NONE) {
			
			flexLog("ERROR received.");
			
			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01() + " code : " + m.getERDF01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02() + " code : " + m.getERDF02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03() + " code : " + m.getERDF03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04() + " code : " + m.getERDF04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05() + " code : " + m.getERDF05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06() + " code : " + m.getERDF06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07() + " code : " + m.getERDF07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08() + " code : " + m.getERDF08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09() + " code : " + m.getERDF09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10() + " code : " + m.getERDF10());
			
		}
	}
}
