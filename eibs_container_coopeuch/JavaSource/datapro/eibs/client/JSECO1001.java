package datapro.eibs.client;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */ 
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Random;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;
import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.ECC150101Message;
import datapro.eibs.beans.ECC150102Message;
import datapro.eibs.beans.ECC150103Message;
import datapro.eibs.beans.ECO060001Message;
import datapro.eibs.beans.ECO060002Message;
import datapro.eibs.beans.ECO100101Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ETG000000Message;
import datapro.eibs.beans.ExcelColStyle;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.ServiceLocator;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;
import datapro.eibs.sockets.MessageField;

public class JSECO1001 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_ROLPAGO_ENTER = 100;	
	protected static final int A_ROLPAGO_ENTER = 200;
	
	protected static final int R_EXC_GES_ENTER = 1100;
	protected static final int A_EXC_GES_LIST  = 1200;
	
	protected static final int A_EXC_GES_LIST_DETAIL  = 1300;
	protected static final int A_EXC_PROCESS  = 1400;
	protected static final int A_EXC_DELETE  = 1500;

	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		screen =  R_ROLPAGO_ENTER;
			
		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}		
		
		switch (screen) {
		case R_ROLPAGO_ENTER:
			procReqEnter(user, req, res, session);
			break;
		case A_ROLPAGO_ENTER:
			procActionEnter(user, req, res, session);
			break; 
		}		
	}
	
	protected void procReqEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
		UserPos userPO = getUserPos(ses);
		MessageProcessor mp = null;
		try {
 			 mp = getMessageProcessor("ECO1001", req);
 			 ECO100101Message msg = (ECO100101Message) mp.getMessageRecord("ECO100101", user.getH01USR(), "0001");
		
 			 mp.sendMessage(msg);
		
 			 ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
 			 msg = (ECO100101Message) mp.receiveMessageRecord("ECO100101");

 			 ses.setAttribute("error", msgError);
 			 ses.setAttribute("msgControl", msg);
 			 ses.setAttribute("userPO", userPO);

			forward("ECO1001_rol_pago_enter.jsp", req, res);
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	

	protected void procActionEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		ELEERRMessage msgError = null;

		boolean ok = false;
		String fileName = "";
		MessageProcessor mp = null;
		String Ind = "";
		
		try 
		{
			try 
			{Ind = req.getParameter("E01FLGFIL");}
			catch (Exception e) 
			{}
			
			if(Ind.equals("S"))
			{	
				SmartUpload mySmartUpload = new SmartUpload();
				com.jspsmart.upload.File myFile = null;
				try 
				{
					mySmartUpload.initialize(config, req, res);
					mySmartUpload.upload();
					myFile = mySmartUpload.getFiles().getFile(0);
					if (myFile.getSize() > 0) 
					{
						//solo acepta archivos menores a 5 Mb.
						if (myFile.getFileExt().equals("txt") || myFile.getFileExt().equals("TXT") )
						{
							if (myFile.getSize() < 5242880) 
							{ 
								fileName = "COEXCINP" ; //nombre del archivo
						
								byte[] bd = new byte[myFile.getSize()];
								for (int i = 0; i < myFile.getSize(); i++) {
									bd[i] = myFile.getBinaryData(i);
								}

								InputStream  input = new ByteArrayInputStream(bd);													
								String userid = ServiceLocator.getSlInfo().getString("ftp.cnx.userid.eibs-server");
								String password = ServiceLocator.getSlInfo().getString("ftp.cnx.password.eibs-server");												
						
								FTPWrapper ftp = new FTPStdWrapper(JSEIBSProp.getHostIP(), userid, password, "");
								
								if (ftp.open()) 
								{
									ftp.setFileType(FTPWrapper.ASCII);
									ftp.upload(input,fileName); 
									ok=true;
								}
								else
								{	
									msgError = new ELEERRMessage();
									msgError.setERRNUM("1");
									msgError.setERNU01("01");		                
									msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
								}	
							} 
							else 
							{
								msgError = new ELEERRMessage();
								msgError.setERRNUM("1");
								msgError.setERNU01("01");		                
								msgError.setERDS01("Archivo excede el tama�o permitido de 5MB.");	
							}	
						}	
						else
						{
							msgError = new ELEERRMessage();
							msgError.setERRNUM("1");
							msgError.setERNU01("01");		                
							msgError.setERDS01("Extensi�n de archivo debe ser TXT.");	
						}	
					}	
					else
					{
						//mandamos error en session 
						msgError = new ELEERRMessage();
						msgError.setERRNUM(new BigDecimal(1));
						msgError.setERDS01("El Archivo no contiene datos.");							
					}
				}
				catch (Exception e) 
				{
					String className = e.getClass().getName();
					String description = e.getMessage() == null ? "Exception General" : e.getMessage();	
					msgError = new ELEERRMessage();			
					msgError.setERRNUM("1");
					msgError.setERNU01("01");
					msgError.setERDS01(className);
					msgError.setERNU02("02");
					msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
					msgError.setERNU03("03");
					msgError.setERDS03("Para mas informacion revizar los archivos de log.");
					e.printStackTrace();			

					session.setAttribute("error", msgError);				
					forward("ECO0600_exc_apro_enter.jsp", req, res);
				} 	
			}
			
			if (ok || !Ind.equals("S"))
			{	
				mp = getMessageProcessor("ECO1001", req);
				ECO100101Message msg = (ECO100101Message) mp.getMessageRecord("ECO100101", user.getH01USR(), "0002");

				setMessageRecord(req, msg);
				//Sets message with page fields

				mp.sendMessage(msg);

					//Receive error and data
				msgError = (ELEERRMessage) mp.receiveMessageRecord();
				msg = (ECO100101Message) mp.receiveMessageRecord();
					//Sets session with required data
				session.setAttribute("userPO", userPO);
				session.setAttribute("msgControl", msg);
				session.setAttribute("error", msgError);

				forward("ECO1001_rol_pago_enter.jsp", req, res);
			}
			else
			{
				session.setAttribute("error", msgError);				
				forward("ECO1001_rol_pago_enter.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}


	//END
}
	



