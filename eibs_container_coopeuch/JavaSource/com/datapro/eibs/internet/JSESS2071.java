package com.datapro.eibs.internet;

/**
 * Insert the type's description here.
 * Creation date: (8/28/2000 4:02:17 PM)
 * @author: Orestes Garcia
**/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ESS204001Message;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

import com.datapro.eibs.internet.beans.JBCards;
import com.datapro.eibs.internet.beans.JBUser;
import com.datapro.eibs.internet.databeans.DC_CARDS;
import com.datapro.eibs.internet.generics.Util;
import com.datapro.exception.ServiceLocatorException;
import com.datapro.generics.BeanList;
//import com.datapro.generics.Util;
import com.datapro.security.SHA1;
import com.datapro.services.ServiceLocator;

public class JSESS2071 extends datapro.eibs.master.SuperServlet {

	protected static final int R_ASG_CARDS = 100;
	protected static final int A_ASG_CARDS = 200;
	protected String LangPath = "S";

	Random generator = new Random();
	
	public JSESS2071() {
		super();
	}

	public JSESS2071(int logType) {
		super(logType);

	}

	public void service(HttpServletRequest req, HttpServletResponse res) throws javax.servlet.ServletException, java.io.IOException {
		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			}
			catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		}
		else {

			int screen = R_ASG_CARDS;

			try {
				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
					switch (screen) {
						case R_ASG_CARDS :
							Read_Entities(msgUser,req,res,session);
							break;
						case A_ASG_CARDS :
							procAssign_Cards(msgUser,req,res,session);
							break;							
						default :
							res.sendRedirect(super.srctx + LangPath + super.devPage);
							break;
					}

				}
				catch (Exception e) {
					e.printStackTrace();
					int sck = super.iniSocket + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
					//return;
				}
			}
			catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}

		}

	}
	//OK
	protected void Read_Entities(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
		throws ServletException, IOException {

		com.datapro.generic.beanutil.BeanList lsCards = new com.datapro.generic.beanutil.BeanList();
		JBUser lstUser = new JBUser();

		lsCards = lstUser.getListEntities();
		ses.setAttribute("lsCards", lsCards);
		res.sendRedirect(super.srctx + LangPath + "ESS2071_cards_assign.jsp");
		return;
	}

	protected void procAssign_Cards(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
	throws ServletException, IOException {
		DC_CARDS Card = new DC_CARDS();
		JBCards cardbean = new JBCards();
		ELEERRMessage msgError = null;
		try {
			msgError = new ELEERRMessage();
		}catch(Exception ex){
			flexLog("Error: " + ex);
		}
		String CardI = req.getParameter("CARDI");
		String CardF = req.getParameter("CARDF");
		String Entity = req.getParameter("ENTITYID");
		int CardIn =0;
		int CardFn = 0;
		try{
			CardIn = Integer.valueOf(CardI).intValue();
			CardFn = Integer.valueOf(CardF).intValue();
		}catch(Exception e){
			msgError.setERRNUM("1");
			msgError.setERDS01("Tarjeta Inicial o Final invalida");
			msgError.setERNU01("9001");
			msgError.setERDF01("CARDI");
			ses.setAttribute("error", msgError);
			res.sendRedirect(super.srctx + LangPath + "ESS2071_cards_assign.jsp");
			return;
		}
		for(int xi=CardIn;xi<=CardFn;xi++){
			Card = cardbean.getCardSerial(String.valueOf(xi));
			if(Card.getSTS()==null || !Card.getSTS().trim().equals("P")){
				msgError.setERRNUM("1");
				msgError.setERNU01("9001");
				msgError.setERDF01("CARDI");
				msgError.setERDS01("Status de Tarjeta no puede Asignarse");
				ses.setAttribute("error", msgError);
				res.sendRedirect(super.srctx + LangPath + "ESS2071_cards_assign.jsp");
				return;
			}
		}

		for(int xi=CardIn;xi<=CardFn;xi++){
			Card = cardbean.getCardSerial(String.valueOf(xi));
			Card.setSTS("A");
			Card.setENTITYID(Entity);
			Card.setUTYPE("1");
			cardbean.setUpdateCard(Card);
		}

		res.sendRedirect(super.srctx + LangPath + "ESS2071_cards_assign_conf.jsp");
		return;
}
	
}