
<%@ page import = "datapro.eibs.master.Util" %>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Historico de Eventos Aprobaciones</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

</head>
<body >

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">
<%
if (userPO.getPurpose().equals("INQUIRY")){
%>
<%
if ( userPO.getHeader23().equals("G") ||  userPO.getHeader23().equals("V")){
%>
	builtNewMenu(ln_i_1_opt);
<%   
}
else if (userPO.getHeader23().equals("DFT")) {
%>
	builtNewMenu(dft_i_opt);
<%   
}
else  {
%>
	builtNewMenu(ln_i_2_opt);
<%   
 }
 %>
<% 
}
%>

</SCRIPT>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
   if (userPO.getPurpose().equals("INQUIRY")){ 
   out.println("<SCRIPT> initMenu(); </SCRIPT>");}
%> 


<h3 align="center"> Historico de Eventos <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="his_selection.jsp,EAPR010"> 
</h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEAPR010" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="3">
  <table class="tableinfo">
    <tr > 
      <td nowrap height="94"> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="50%"> 
              <div align="right"> <b>Seleccionar Fechas Desde :</b></div>
            </td>
            <td nowrap width="50%"> 
              <div align="left"> 
                <input type="text" name="E01FRDTE1" size="3" maxlength="2" value=<%= userPO.getHeader9().trim()%>>
                <input type="text" name="E01FRDTE2" size="3" maxlength="2" value=<%= userPO.getHeader10().trim()%>>
                <input type="text" name="E01FRDTE3" size="5" maxlength="4" value=<%= userPO.getHeader11().trim()%>>
                <a href="javascript:DatePicker(document.forms[0].E01FRDTE1,document.forms[0].E01FRDTE2,document.forms[0].E01FRDTE3)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 
              </div>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="50%"> 
              <div align="right"><b>Hasta :</b></div>
            </td>
            <td nowrap width="50%"> 
              <div align="left"> 
                <input type="text" name="E01TODTE1" size="3" maxlength="2" value=<%= userPO.getHeader12().trim()%>>
                <input type="text" name="E01TODTE2" size="3" maxlength="2" value=<%= userPO.getHeader13().trim()%>>
                <input type="text" name="E01TODTE3" size="5" maxlength="4" value=<%= userPO.getHeader14().trim()%>>
                <a href="javascript:DatePicker(document.forms[0].E01TODTE1,document.forms[0].E01TODTE2,document.forms[0].E01TODTE3)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="50%"> 
              <div align="right"><b>Modulo IBS :</b></div>
            </td>
            <td nowrap width="50%"> 
              <div align="left"> 
                <input type="text" name="E01HISACD" size="05" maxlength="02" value=<%= userPO.getHeader19().trim()%>>
              <a href="javascript:GetCode('E01HISACD','STATIC_application_codes.jsp')">
              <img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0"></a> 
              </div>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="50%"> 
              <div align="right"><b>Sucursal :</b></div>
            </td>
            <td nowrap width="50%"> 
              <div align="left"> 
                <input type="text" name="E01HISBRN" size="05" maxlength="04" value=<%= userPO.getHeader18().trim()%>>
                <a href="javascript:GetBranch('E01HISBRN','<%= userPO.getBank() %>',document.forms[0].E01HISBRN.value)"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0"></a> 
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="50%" > 
              <div align="right"><b>Evento :</b></div>
            </td>
            <td nowrap width="50%"> 
              <div align="left"> 
                <input type="text" name="E01HISEVN" size="05" maxlength="04" value=<%= userPO.getHeader17().trim()%> >
              <a href="javascript:GetEventos('E01HISEVN','',document.forms[0].E01HISACD.value)"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a> 
              </div>
            </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>


	<div align="center"> 
    	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  	</div>


  </form>
</body>
</html>
