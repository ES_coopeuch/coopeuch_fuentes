<html>
<head>
<title>Reconciliacion Bancos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

</head>

<body>

<H3 align="center">Conciliación Manual
	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="rcbank_enter_selection, ERC0040"></H3>
<hr size="4">

<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0040">
	<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">
  	<h4>&nbsp;</h4>

<input type="hidden" name="E01BRMEID" value="<%=session.getAttribute("codigo_banco")%>">
<input type="hidden" name="E01BRMCTA" value="<%=session.getAttribute("cuenta_banco")%>">

<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">   Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<% out.print(session.getAttribute("codigo_banco")+"-"+session.getAttribute("nombre_banco"));%>" 
	  			size="60"  readonly>
             </td>          
         </tr>
         
           <tr>
                
            <td nowrap width="10%" align="right"> Cuenta Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<%=session.getAttribute("cuenta_banco")%>" 
	  			size="30"  readonly>
             </td>
                    <td nowrap width="10%" align="right"> Cuenta IBS : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<%=session.getAttribute("cuenta_ibs")%>" 
	  			size="30"  readonly>
             </td>  
                
                </tr>
        </table>
      </td>
    </tr>
  </table>
  
  
   
	<div align="center"> 
    	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  	</div>
  

<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
  