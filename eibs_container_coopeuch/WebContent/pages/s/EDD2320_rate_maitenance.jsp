<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<html>
<head>
<title>Manteci&oacute;n de Tarifas</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="RTRates" class="datapro.eibs.beans.EDD232003Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

function cancel() {
	document.forms[0].SCREEN.value = 2100;
	document.forms[0].submit();
}

</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "NEW".equals(userPO.getPurpose()) ? "" : "readonly";
     
%>


<H3 align="center">
<% if(userPO.getPurpose().equals("NEW")){ %>
Nueva Tarifa
<% } else {%>
Mantenci&oacute;n Tarifa
<% } %>
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="rates_maitenance, EDD2320"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2320" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2600">
  
  <h4>Datos Tarifa</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Tarifa :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E03CMPTCTF" size="5" maxlength="4" value="<%= RTRates.getE03CMPTCTF().trim()%>"  <%=readonly%>>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Descripci&oacute;n  :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E03CMPTGTF" size="31" maxlength="30" value="<%= RTRates.getE03CMPTGTF().trim()%>" >
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Monto del Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E03CMPTMCB" size="16" maxlength="15" value="<%= RTRates.getE03CMPTMCB().trim()%>" class="TXTRIGHT" onkeypress="enterDecimal()">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Moneda :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E03CMPTMRT" size="4" maxlength="3" value="<%= RTRates.getE03CMPTMRT().trim()%>" readonly>
                <input type="text" name="E03CMPTMRG" size="31" maxlength="30" value="<%= RTRates.getE03CMPTMRG().trim()%>"  readonly>
				<a href="javascript:GetCurrencyDesc('E03CMPTMRT','E03CMPTMRG','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="help" align="absbottom" border="0"></a> 
                
              </div>
            </td>
          </tr>
  </table>

  <div align="center">
    <input id="EIBSBTN" type="submit" name="Enviar" value="Enviar" >
    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel()">
  </div>
  </form>

</body>
</html>
