<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Orden de Pago Masivo</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "opMasivo" class= "datapro.eibs.beans.EOF100001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">
 function enterCode(){

	if (trim(document.forms[0].E01CTCEID.value).length > 0) &&
	   (trim(document.forms[0].file.value).length > 0) 
	 {
	    return true;
	}else{
		alert("Es requerido que se entre un valor");
		document.forms[0].E01CTCEID.focus();
		return false;
	}
 } 
</script>

</head>

<body>
 
<H3 align="center">Carga de Archivo desde PC a Host<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="orden_pago_masivo_enter.jsp, EOF1000"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEOF1000" 
onsubmit="return(enterCode());"  enctype="multipart/form-data">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">
  <h4>&nbsp;</h4>
  <h4>&nbsp;</h4>
  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0"> 
    <tr id="trdark">
      <td width="50%"> 
        <div align="right">Codigo de Archivo a Cargar : </div>
      </td>
      <td width="50%"> 
        <div align="left"> 
          <input type="text" name="E01CTCEID" size="5" maxlength="4" value="<%= opMasivo.getE01CTCEID().trim()%>">
            <a href="javascript:GetChargeFiles('E01CTCEID','E01CTCNME')">
            <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0" ></a> 
          <input type="hidden" name="E01CTCNME">
        </div>
      </td>
    </tr>
  	<tr id="trdark"> 
        <td align=CENTER width="30%"> 
          <div align="right">Seleccione Archivo a Cargar :</div>
        </td>
        <td align=CENTER width="70%"> 
          <div align="left"> 
 	         <input type="file" name="file" size="50" >
          </div>
        </td>
      </tr>       
  </table>
  <h4>&nbsp;</h4>
  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
<script language="JavaScript">
		document.forms[0].E01CTCEID.focus();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
