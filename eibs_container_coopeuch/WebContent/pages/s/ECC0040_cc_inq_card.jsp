<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Credit Card Inquiry</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id="ccInq" class="datapro.eibs.beans.ECC004001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<SCRIPT LANGUAGE="javascript">
 
	//builtNewMenu(cc_i_opt);
  function goAction(op)
   {
      if (op == 1) 
      {
		pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150?SCREEN=110";
	  }
	  else if (op == 2)
      {
		pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150?SCREEN=210";
	  }
	  else if (op == 3)
	  {
		pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150?SCREEN=1100";
	  } 
	  else if (op == 4)
	  {
		pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150?SCREEN=2000";
	  } 	   	      
	   CenterWindow(pg,950,600,2);
   }  
</SCRIPT>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0"); 
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
  
   out.println("<SCRIPT> initMenu();  </SCRIPT>");

%> 

</head>
<body>
<h3 align="center">Consulta Tarjeta de Cr�dito
 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cc_inq_card.jsp,ECC0040"> 
</h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0040" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="">
	<TABLE class="tbenter" width="100%">
	<TR>
		<TD ALIGN=CENTER class=TDBKG><a href="javascript:goAction(1)"><b>Facturas<br> Nacionales</b></a>
		</TD>
		<TD ALIGN=CENTER class=TDBKG><a href="javascript:goAction(2)"><b>Facturas <br>Internacionales</b></a>
		</TD>
		<TD ALIGN=CENTER class=TDBKG><a href="javascript:goAction(3)"><b>Movimientos <br> No Facturados</b></a>
		</TD>
		<TD ALIGN=CENTER class=TDBKG><a href="javascript:goAction(4)"><b>Tarjetas <br> Adicionales</b></a>
		</TD>
		<TD ALIGN=CENTER class=TDBKG><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</TD>
	</TR>
</TABLE>  
 <% int row = 0;%>
<h4>Cliente</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <eibsinput:text name="ccInq" property="E01CCRCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
               </div>               
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre : </b></div>
            </td>
            <td nowrap width="20%"  > 
              <div align="left"> 
                <input type="text" name="E01CCMNME" size="35" maxlength="35" readonly value="<%= ccInq.getE01CCMNME().trim()%>">                                 
              </div>
            </td>            
            <td nowrap width="16%" > 
              <div align="right"><b>Identif. Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCID" size="15" maxlength="15" readonly value="<%= ccInq.getE01CCRCID().trim()%>">                
               </div>               
            </td>            
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta IBS : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <eibsinput:text name="ccInq" property="E01CCMACC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>                  
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <eibsinput:text name="ccInq" property="E01CCMCCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" readonly="true"/>              
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Oficial : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <eibsinput:text name="ccInq" property="E01CCMOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>" readonly="true"/>                
                </b> </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
          <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <eibsinput:text name="ccInq" property="E01CCMPRO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT %>" readonly="true"/>                  
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Desc. Producto : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="D01CCMPRO" size="35" maxlength="35" readonly value="<%= ccInq.getD01CCMPRO().trim()%>">                              
              </div>
            </td>
            <td nowrap width="16%"> 
            </td>
            <td nowrap width="16%"> 
            </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>
  <h4>Informaci�n de la Cuenta</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">N�mero Cuenta : </div>
            </td>
            <td nowrap width="23%"> 
               <eibsinput:text name="ccInq" property="E01CCMNXN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CARD_NUMBER %>" readonly="true"/>              
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Apertura : </div>
            </td>
            <td nowrap width="23%"> 
              <eibsinput:date name="ccInq" fn_year="E01CCMOPY" fn_month="E01CCMOPM" fn_day="E01CCMOPD" readonly="true"/>
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Estado de la Cuenta : </div>
            </td>
            <td nowrap width="23%">             
              <select name="E01CCMAST" disabled>
                <option value=" " <% if (!(
		                	ccInq.getE01CCMAST().equals("0") || 
                			ccInq.getE01CCMAST().equals("1"))) 
                			out.print("selected"); %> selected></option>
                <option value="0" <% if(ccInq.getE01CCMAST().equals("0")) out.print("selected");%>>Inactiva</option>
                <option value="1" <% if(ccInq.getE01CCMAST().equals("1")) out.print("selected");%>>Activa</option>                			
              </select>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Activaci�n : </div>
            </td>
            <td nowrap width="23%"> 
              <eibsinput:date name="ccInq" fn_year="E01CCMACY" fn_month="E01CCMACM" fn_day="E01CCMACI" readonly="true"/>
            </td>
          </tr>           
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Bloqueo 1 Cuenta :</div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="D01CCMCBK" size="35" maxlength="35" readonly value="<%= ccInq.getD01CCMCBK().trim()%>">                             
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha 1 Bloqueo : </div>
            </td>
            <td nowrap width="27%"> 
              <eibsinput:date name="ccInq" fn_year="E01CCMB1Y" fn_month="E01CCMB1M" fn_day="E01CCMB1D" readonly="true"/>   			
 			</td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Bloqueo 2 Cuenta :</div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="D01CCMCB2" size="35" maxlength="35" readonly value="<%= ccInq.getD01CCMCB2().trim()%>">                  
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha 2 Bloqueo : </div>
            </td>
 	      <td nowrap width="27%"> 
             <eibsinput:date name="ccInq" fn_year="E01CCMB2Y" fn_month="E01CCMB2M" fn_day="E01CCMB2D" readonly="true"/>  	
 			</td>    
	      </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Forma de Pago : </div>
            </td>
             <td nowrap width="27%"> 
               <input type="text" name="D01CCMFPA" size="35" maxlength="35" readonly value="<%= ccInq.getD01CCMFPA().trim()%>">                  
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cuenta Corriente : </div>
            </td>
            <td nowrap width="27%"> 
		     <eibsinput:text name="ccInq" property="E01CCMRPA" size="13" maxlength="13"  eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" readonly="true" />		        				
 			</td>
          </tr>           
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Cantidad Adicionales : </div>
            </td>
            <td nowrap width="27%"> 
          	  <eibsinput:text property="E01CCMNCC" name="ccInq" size="3" maxlength="3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" readonly="true" />		                      
 			</td>
            <td nowrap width="25%"> 
              <div align="right">Canal de Venta : </div>
            </td>
            <td nowrap width="27%"> 
               <input type="text" name="D01CCMSCH" size="35" maxlength="35" readonly value="<%= ccInq.getD01CCMSCH().trim()%>">  		                     				 
 			</td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Codigo FV :</div>
            </td>
            <td nowrap width="23%"> 
		        <eibsinput:text name="ccInq" property="E01CCMCFA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />            
            </td>
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="27%">             
 			</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Direcci�n Envio de Estado de Cuenta</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Direcci�n : </div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="D01CCMDIR" size="35" maxlength="35" readonly value="<%= ccInq.getD01CCMDIR().trim()%>">  		                     
            </td>
            <td nowrap width="25%"> 
              <div align="right">Comuna : </div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="D01CCMCOM" size="35" maxlength="35" readonly value="<%= ccInq.getD01CCMCOM().trim()%>">  		                    
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Ciudad : </div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="D01CCMCIU" size="35" maxlength="35" readonly value="<%= ccInq.getD01CCMCIU().trim()%>">  		                   
            </td>
            <td nowrap width="25%"> 
              <div align="right">Regi�n : </div>
            </td>
            <td nowrap width="23%">
               <input type="text" name="D01CCMREG" size="35" maxlength="35" readonly value="<%= ccInq.getD01CCMREG().trim()%>">  		                     
            </td>
          </tr>           
        </table>
      </td>
    </tr>
  </table>
  <h4>Informaci�n de la Tarjeta</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">N�mero Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
		        <eibsinput:text name="ccInq" property="E01CCRNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CARD_NUMBER %>" readonly="true" />            
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Activaci�n : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="ccInq" fn_year="E01CCRATY" fn_month="E01CCRATM" fn_day="E01CCRATD" readonly="true"/> 
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Estado Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCRSTS" disabled>
                <option value=" " <% if (!(
		                	ccInq.getE01CCRSTS().equals("00") || 
                			ccInq.getE01CCRSTS().equals("01"))) 
                			out.print("selected"); %> selected></option>
                <option value="00" <% if(ccInq.getE01CCRSTS().equals("00")) out.print("selected");%>>Inactiva</option>
                <option value="01" <% if(ccInq.getE01CCRSTS().equals("01")) out.print("selected");%>>Activa</option>  
              </select>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Expiraci�n : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="ccInq" fn_year="E01CCREXY" fn_month="E01CCREXM" fn_day="E01CCREXD" readonly="true"/>             
            </td>
          </tr>   
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Bloqueo Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="D01CCRLKC" size="35" maxlength="35" readonly value="<%= ccInq.getD01CCRLKC().trim()%>">  		              
            </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha Renovaci�n : </div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="ccInq" fn_year="E01CCREXY" fn_month="E01CCREXM" fn_day="E01CCREXD" readonly="true"/>               
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Tipo de Tarjeta : </div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCRTPI" disabled>
                <option value=" " <% if (!(
		                	ccInq.getE01CCRTPI().equals("T") || 
                			ccInq.getE01CCRTPI().equals("A"))) 
                			out.print("selected"); %> selected></option>
                <option value="T" <% if(ccInq.getE01CCRTPI().equals("T")) out.print("selected");%>>Titular</option>
                <option value="A" <% if(ccInq.getE01CCRTPI().equals("A")) out.print("selected");%>>Adicional</option>                			
              </select>
            </td>
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%"> 
            </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Rut TarjetaHabiente : </div>
            </td>
            <td nowrap width="23%"> 
                <input type="text" name="E01CCRCI2" size="15" maxlength="15" readonly value="<%= ccInq.getE01CCRCI2().trim()%>">               
            </td>
            <td nowrap width="25%"> 
              <div align="right">Nombre TarjetaHabiente : </div>
            </td>
            <td nowrap width="23%"> 
               <input type="text" name="E01CCRNAM" size="35" maxlength="35" readonly value="<%= ccInq.getE01CCRNAM().trim()%>">               
            </td>
          </tr>   
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="center"></div>
            </td>
            <td nowrap width="23%">         
            </td>
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%"> 
            </td>
          </tr>                                             
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="center"><h4>Cupos Diferenciados </h4> </div>
            </td>
            <td nowrap width="23%">         
            </td>
            <td nowrap width="25%"> 
            </td>
            <td nowrap width="23%"> 
            </td>
          </tr>                                                      
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Cupo Pesos : </div>
            </td>
            <td nowrap width="23%"> 
		        <eibsinput:text name="ccInq" property="E01CCRCDN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />            
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Avance Pesos : </div>
            </td>
            <td nowrap width="23%">
		        <eibsinput:text name="ccInq" property="E01CCRADN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />  
            </td>
          </tr>   
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Cupo Dolares : </div>
            </td>
            <td nowrap width="23%"> 
		        <eibsinput:text name="ccInq" property="E01CCRCDR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />  		              
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Avance Dolares : </div>
            </td>
            <td nowrap width="23%">
		        <eibsinput:text name="ccInq" property="E01CCRADI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />                
 			</td>
          </tr> 
        </table>
      </td>
    </tr>
  </table>          

   <h4>Saldos de la Cuenta</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">                        
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Deuda Total $ : </div>
            </td>
            <td nowrap width="23%"> 
          	     <eibsinput:text property="E01CCMDBS" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Pesos :</div>
            </td>
            <td nowrap width="27%"> 
          	     <eibsinput:text property="E01CCMLBC" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
 			</td>
          </tr> 
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Deuda Colocaciones $ :</div>
            </td>
            <td nowrap width="23%"> 
          	     <eibsinput:text property="E01CCMDCO" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Dolares :</div>
            </td>
            <td nowrap width="27%"> 
          	     <eibsinput:text property="E01CCMLFC" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
 			</td>
          </tr>          
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Deuda Total US$ :</div>
            </td>
            <td nowrap width="23%"> 
          	     <eibsinput:text property="E01CCMDFC" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Disponible Total $ :</div>
            </td>
            <td nowrap width="27%"> 
          	     <eibsinput:text property="E01CCMABS" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
 			</td>
          </tr>            
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Deuda Actual Cuotas :</div>
            </td>
            <td nowrap width="23%"> 
          	     <eibsinput:text property="E01CCMDQT" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Disponible Total US$ :</div>
            </td>
            <td nowrap width="27%"> 
          	     <eibsinput:text property="E01CCMAFC" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
 			</td>
          </tr>           
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="25%"> 
              <div align="right">Deuda Cuotas 3 CPC :</div>
            </td>
            <td nowrap width="23%"> 
          	     <eibsinput:text property="E01CCMDSI" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Disponible Avances :</div>
            </td>
            <td nowrap width="27%"> 
          	     <eibsinput:text property="E01CCMAAD" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
 			</td>
          </tr>                
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
            <td nowrap width="25%"> 
              <div align="right">Fecha Ultima Facturaci�n :</div>
            </td>
            <td nowrap width="23%">
              <eibsinput:date name="ccInq" fn_year="E01CCMLBY" fn_month="E01CCMLBM" fn_day="E01CCMLBD" readonly="true"/>               
            </td>            
            <td nowrap width="25%"> 
              <div align="right">Deuda Ultima Facturaci�n $ :</div>
            </td>
            <td nowrap width="23%"> 
          	 <eibsinput:text property="E01CCMLSB" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
          	</td>
          </tr>           
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>"> 
            <td nowrap width="25%"> 
              <div align="right">Deuda Ultima Facturaci�n US$ :</div>
            </td>
            <td nowrap width="23%"> 
          	     <eibsinput:text property="E01CCMLSI" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Mora 0 - 30 :</div>
            </td>
            <td nowrap width="27%"> 
          	     <eibsinput:text property="E01CCMP30" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
 			</td>
          </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>">  
            <td nowrap width="25%"> 
              <div align="right">Pago Minimo :</div>
            </td>
            <td nowrap width="23%"> 
          	     <eibsinput:text property="E01CCMMPA" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
          	</td>
            <td nowrap width="25%"> 
              <div align="right">Mora 31 - 60 :</div>
            </td>
            <td nowrap width="27%"> 
          	     <eibsinput:text property="E01CCMP60" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
 			</td>
          </tr>          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>"> 
            <td nowrap width="25%"> 
              <div align="right">Fecha Vencimiento Facturaci�n :</div>
            </td>
            <td nowrap width="23%"> 
              <eibsinput:date name="ccInq" fn_year="E01CCMDUY" fn_month="E01CCMDUM" fn_day="E01CCMDUD" readonly="true"/>             
            </td>
            <td nowrap width="25%"> 
              <div align="right">Mora 61 - 90 :</div>
            </td>
            <td nowrap width="27%"> 
          	     <eibsinput:text property="E01CCMP90" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
 			</td>
          </tr>      
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>">  
            <td nowrap width="25%"> 
              <div align="right">Cuenta Traspaso Saldo :</div>
            </td>
            <td nowrap width="23%"> 
          	  <eibsinput:text property="E01CCMNCA" name="ccInq" size="19" maxlength="16" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" readonly="true" />		        
		    </td>
            <td nowrap width="25%"> 
              <div align="right">Mora +90 :</div>
            </td>
            <td nowrap width="27%"> 
          	     <eibsinput:text property="E01CCMP91" name="ccInq" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
 			</td>
          </tr> 
        </table>
      </td>
    </tr>
  </table>
   <h4>Informacion Estadistica</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">                        
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>"> 
            <td nowrap width="25%"> 
              <div align="right">Dias Mora :</div>
            </td>
            <td nowrap width="23%"> 
          	  <eibsinput:text property="E01CCMPCT" name="ccInq" size="6" maxlength="6" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" readonly="true" />                 		        
            </td>
            <td nowrap width="25%"> 
              <div align="right">N�mero Compras Mes :</div>
            </td>
            <td nowrap width="27%"> 
          	  <eibsinput:text property="E01CCMNCM" name="ccInq" size="6" maxlength="6" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" readonly="true" />                		        
 			</td>            
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>">
            <td nowrap width="25%">
            	<div align="right">Dias Mora :</div>
            </td>
            <td nowrap width="27%">
          	  <eibsinput:text property="E01CCMPDY" name="ccInq" size="6" maxlength="6" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" readonly="true" />            		        
            </td>
            <td nowrap width="25%"> 
              <div align="right">N�mero pagos en el Mes :</div>
            </td>
            <td nowrap width="27%"> 
          	  <eibsinput:text property="E01CCMNPM" name="ccInq" size="6" maxlength="6" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" readonly="true" />                 		        
 			</td>            
          </tr>           
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++; %>">  
            <td nowrap width="25%">
            	<div align="right">Fecha Ultima Mora :</div>
            </td>
            <td nowrap width="27%">
              <eibsinput:date name="ccInq" fn_year="E01CCMLMY" fn_month="E01CCMLMM" fn_day="E01CCMLMD" readonly="true"/> 
            </td>
            <td nowrap width="25%"> 
              <div align="right">N�mero avances en el mes :</div>
            </td>
            <td nowrap width="27%"> 
          	  <eibsinput:text property="E01CCMNAM" name="ccInq" size="6" maxlength="6" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" readonly="true" />              			        
 			</td>
          </tr>           
        </table>
      </td>
    </tr>
  </table>
  <br>
  </form>
</body>
</html>
