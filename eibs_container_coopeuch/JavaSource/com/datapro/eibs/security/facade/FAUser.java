package com.datapro.eibs.security.facade;

import java.sql.SQLException;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.FacadeException;
import com.datapro.eibs.exception.LogicException;
import com.datapro.eibs.exception.MenuOptionNotAssignedException;
import com.datapro.eibs.exception.UserNotFoundException;
import com.datapro.eibs.security.access.jdbc.bean.SECGRPITM;
import com.datapro.eibs.security.access.jdbc.bean.SECMNU;
import com.datapro.eibs.security.access.jdbc.bean.SECMNUCHK;
import com.datapro.eibs.security.access.jdbc.bean.USRGRP;
import com.datapro.eibs.security.logic.LOUserAccess;
import com.datapro.eibs.security.logic.LOUserAdmin;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.DynamicDTO;

/**
 * @author ogarcia
 *
 */
public class FAUser {

	private static Category cat =
		Category.getInstance(FAUser.class.getName());

	/**
	 * Constructor for FAUser.
	 */
	public FAUser() {
		super();
	}

	/**
	 * Method getMenu.
	 * @param uid
	 * @return BeanList
	 * @throws UserNotFoundException
	 * @throws MenuOptionNotAssignedException
	 * @throws FacadeException
	 */
	public BeanList getMenu(String uid)
		throws UserNotFoundException, MenuOptionNotAssignedException, FacadeException {
			
		BeanList dtoMenuList = null;
		
		try {
			LOUserAccess ua = new LOUserAccess();
			BeanList beanMenuList = ua.getMenu(uid);
			dtoMenuList = new BeanList();
			beanMenuList.initRow();
			while (beanMenuList.getNextRow()) {
				DynamicDTO dtoItem = new DynamicDTO();
				SECMNU beanItem = (SECMNU) beanMenuList.getRecord();
				dtoItem.load(beanItem);
				dtoMenuList.addRow(dtoItem);
			}
		}
		catch (LogicException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
		
		return dtoMenuList;
			
	}
	
	/**
	 * Method getGroupsAndUsers.
	 * @return BeanList
	 * @throws FacadeException
	 */
	public BeanList getGroupsAndUsers()
		throws FacadeException {
			
		BeanList dtoGauList = null;
		
		try {
			boolean hasUnassigned = false;
			boolean needsUnassigned = true;
			LOUserAdmin ua = new LOUserAdmin();
			BeanList beanGauList = ua.getGroupsAndUsers();
			dtoGauList = new BeanList();
			beanGauList.initRow();
			while (beanGauList.getNextRow()) {
				DynamicDTO dtoItem = new DynamicDTO();
				USRGRP beanItem = (USRGRP) beanGauList.getRecord();
				if (beanItem.getGRPID().equals("")) {
					beanItem.setGRPID(Security.USER_GROUP_UNASSIGNED);
				}
				if (!hasUnassigned
					&& beanItem.getGRPID().equals(Security.USER_GROUP_UNASSIGNED)) {
					hasUnassigned = true;
				}
				if (needsUnassigned
					&& beanItem.getUSRID().equals(Security.USER_GROUP_UNASSIGNED)) {
					needsUnassigned = false;
				}
				dtoItem.load(beanItem);
				dtoGauList.addRow(dtoItem);
			}
			if (hasUnassigned && needsUnassigned) {
				USRGRP beanItem = new USRGRP();
				beanItem.setUSRID(Security.USER_GROUP_UNASSIGNED);
				beanItem.setGRPID("");
				beanItem.setSTATUS("");
				dtoGauList.addRow(beanItem);
			}
		}
		catch (LogicException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
		
		return dtoGauList;
			
	}
	
	/**
	 * Method newGroup.
	 * @param gid
	 * @throws FacadeException
	 */
	public void newGroup(String gid)
		throws FacadeException {
			
		try {
			LOUserAdmin ua = new LOUserAdmin();
			ua.newGroup(gid);
		}
		catch (LogicException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
			
	}
	
	/**
	 * Method dltGroup.
	 * @param gid
	 * @throws FacadeException
	 */
	public void dltGroup(String gid)
		throws FacadeException {
			
		try {
			LOUserAdmin ua = new LOUserAdmin();
			ua.dltGroup(gid);
		}
		catch (SQLException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
		catch (LogicException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
			
	}
	
	/**
	 * Method newUser.
	 * @param uid
	 * @param gid
	 * @throws FacadeException
	 */
	public void newUser(String uid, String gid)
		throws FacadeException {
			
		try {
			LOUserAdmin ua = new LOUserAdmin();
			ua.newUser(uid, gid);
		}
		catch (SQLException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
		catch (LogicException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
			
	}
	
	/**
	 * Method dltUser.
	 * @param uid
	 * @throws FacadeException
	 */
	public void dltUser(String uid)
		throws FacadeException {
			
		try {
			LOUserAdmin ua = new LOUserAdmin();
			ua.dltUser(uid);
		}
		catch (SQLException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
		catch (LogicException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
			
	}
	
	/**
	 * Method moveUser.
	 * @param uid
	 * @param gid
	 * @throws FacadeException
	 */
	public void moveUser(String uid, String gid)
		throws FacadeException {
			
		try {
			LOUserAdmin ua = new LOUserAdmin();
			ua.moveUser(uid, gid);
		}
		catch (LogicException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
			
	}
	
	/**
	 * Method chgStatus.
	 * @param uid
	 * @param sts
	 * @throws FacadeException
	 */
	public void chgStatus(String uid, String sts)
		throws FacadeException {
			
		try {
			LOUserAdmin ua = new LOUserAdmin();
			ua.chgStatus(uid, sts);
		}
		catch (LogicException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
			
	}
	
	/**
	 * Method getMenuAccess.
	 * @param uid
	 * @return BeanList
	 * @throws FacadeException
	 */
	public BeanList getMenuAccess(String uid)
		throws FacadeException {
			
		BeanList dtoMAList = null;
		
		try {
			LOUserAdmin ua = new LOUserAdmin();
			BeanList beanMAList = ua.getMenuAccess(uid);
			dtoMAList = new BeanList();
			beanMAList.initRow();
			while (beanMAList.getNextRow()) {
				DynamicDTO dtoItem = new DynamicDTO();
				SECMNUCHK beanItem = (SECMNUCHK) beanMAList.getRecord();
				dtoItem.load(beanItem);
				dtoMAList.addRow(dtoItem);
			}
		}
		catch (LogicException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
		
		return dtoMAList;
			
	}
	
	public void updMenuAccess(String uid, BeanList bl)
		throws FacadeException {
			
		try {
			BeanList beanMAList = new BeanList();
			bl.initRow();
			while (bl.getNextRow()) {
				SECGRPITM beanItem = new SECGRPITM();
				DynamicDTO dtoItem = (DynamicDTO) bl.getRecord();
				dtoItem.unload(beanItem);
				beanMAList.addRow(beanItem);
			}
			LOUserAdmin ua = new LOUserAdmin();
			ua.updMenuAccess(uid, beanMAList);
		}
		catch (LogicException e) {
			cat.error(e);
			throw new FacadeException(e.toString());
		}
		
	}
	
}
