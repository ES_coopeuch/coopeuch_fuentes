package datapro.eibs.params;

/*******************************************************************************************************************************/
/**  Creado por              :  Patricia Cataldo L.                 DATAPRO                                                    **/
/**  Identificacion          :  PCL01                                                                                          **/
/**  Fecha                   :  14/01/2013                                                                                     **/
/**  Objetivo                :  Mantenedor de Tabla de tramos de Postulacion Subsidio, para Ahorro Vivienda                    **/
/**                                                                                                                            **/
/*********************************************************************************************************************************/

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ECC014001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EDD200001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.EWD0008DSMessage;
import datapro.eibs.beans.EWD0014DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

public class JSEDD2000 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int A_TABLA_LIST = 100;
	protected static final int R_TABLA_NEW = 200;
	protected static final int R_TABLA_MAINT = 201;
	protected static final int R_TABLA_DELETE = 202;	
	protected static final int R_TABLA_INQUIRY = 203;
	protected static final int A_TABLA_MAINT = 600;
	protected static final int A_TABLA_LIST_HELP = 1000;	
	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
	    	flexLog("Screen ..." + screen);
		switch (screen) {
		case A_TABLA_LIST:
			procActionTablaList(user, req, res, session, null);
			break;
		case R_TABLA_NEW:
			procReqTabla(user, req, res, session, "NEW");
			break;
		case R_TABLA_MAINT:
			procReqTabla(user, req, res, session, "MAINTENANCE");
			break;
		case R_TABLA_INQUIRY:
			procReqTabla(user, req, res, session, "INQUIRY");
		//	procReqView(user, req, res, session);
			break;
		case A_TABLA_MAINT:
			procActionMaintenance(user, req, res, session);
			break;
		case R_TABLA_DELETE:
			procReqDelete(user, req, res, session);
			break;
		case A_TABLA_LIST_HELP:
			procActionTablaListHelp(user, req, res, session, null);
			break;			
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	/**
	 * procActionTablaPVPRDList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionTablaList(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session, 
			String option)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EDD2000", req);

			EDD200001Message msg = (EDD200001Message) mp.getMessageRecord("EDD200001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());
			//Sends message
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
 
			session.setAttribute("userPO", userPO);
			session.setAttribute("EDD200001List", list);
			forwardOnSuccess("EDD2000_Tabla_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqTabla: This Method show a single  Tabla either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTabla(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res,
			HttpSession session, 
			String option) throws ServletException, 
			IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("EDD2000", req);
			userPO.setPurpose(option);
			
			//Creates the message with operation code depending on the option
			EDD200001Message msg = (EDD200001Message) mp.getMessageRecord("EDD200001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01TIMSYS(getTimeStamp());
			if (option.equals("NEW")) {
				msg.setH01OPECOD("0001");
			} else if (option.equals("MAINTENANCE")) {
				msg.setH01OPECOD("0002");
			} else {
				msg.setH01OPECOD("0004");
			}
			
			//Sets the number for maintenance and inquiry options
			if (req.getParameter("E01TPSTTR") != null) {
				msg.setE01TPSTTR(req.getParameter("E01TPSTTR"));
			}

			//Send message
			flexLog("mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EDD200001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go to maintenance page
				flexLog("About to call Page: EDD2000_Tabla_maintenance.jsp");
				if (option.equals("INQUIRY")) {
					// if the request is an inquiry sets the readOlnly attribute 'true'
					forward("EDD2000_Tabla_maintenance.jsp?readOnly=true", req, res);
				} else {
					forward("EDD2000_Tabla_maintenance.jsp", req, res);
				}
			} else {
				//if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EDD2000_Tabla_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procActionMaintenance
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EDD2000", req);

			EDD200001Message msg = (EDD200001Message) mp.getMessageRecord("EDD200001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0005");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EDD200001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go back to list
				redirectToPage("/servlet/datapro.eibs.params.JSEDD2000?SCREEN=100", res);
			} else {
				//if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				forward("EDD2000_Tabla_maintenance.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqDelete
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDelete(
			ESS0030DSMessage user, 
			HttpServletRequest req,
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("MAINTENANCE");

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EDD2000", req);
			
			//Creates message with the 'Delete'operation code
			EDD200001Message msg = (EDD200001Message) mp.getMessageRecord("EDD200001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0009");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets required values
			msg.setH01SCRCOD("01");
			//Sets the Code for delete options
			if (req.getParameter("E01TPSTTR") != null) {
				msg.setE01TPSTTR(req.getParameter("E01TPSTTR"));
			}
			
			//Send message
			mp.sendMessage(msg);

			//Receive Error and Data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EDD200001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//If there are no errors request the list again
				redirectToPage("/servlet/datapro.eibs.params.JSEDD2000?SCREEN=100", res);
			} else {
				//if there are errors show the list without updating
				session.setAttribute("error", msgError);
				forward("EDD2000_Tabla_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	protected void procReqView(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			EDD200001Message msgDoc = null;
			UserPos userPO = null;

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			// Receive Data
			try {
				JBObjList bl = (JBObjList) ses.getAttribute("EDD200001List");
				int idx = Integer.parseInt(req.getParameter("COL"));
				bl.setCurrentRow(idx);

				msgDoc = (EDD200001Message) bl.getRecord();
			
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("cnvObj", msgDoc);

				try {
					forward("EDD2000_Tabla_maintenance.jsp?readOnly=true", req, res);

				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}
	protected void procActionTablaListHelp(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session, 
			String option)
			throws ServletException, IOException {

		MessageContext mc = null;
		MessageRecord newmessage = null;
		EDD200001Message msgHelp = null;
		ESS0030DSMessage msgUser = null;
		JBObjList beanList = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			msgUser =
				(datapro.eibs.beans.ESS0030DSMessage) session.getAttribute(
					"currUser");
			String Language = msgUser.getE01LAN();
			String LangPath = super.rootPath + Language + "/";

			try {
				flexLog("Opennig Socket Connection");
				mc = new MessageContext(super.getMessageHandler("EDD2000", req));

				try {
					msgHelp = (EDD200001Message) mc.getMessageRecord("EDD200001");
					msgHelp.setH01USERID(msgUser.getH01USR());
					msgHelp.setH01OPECOD("0015");	
					msgHelp.send();
					msgHelp.destroy();
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
					return;
				}

				// Receiving
				try {
					newmessage = mc.receiveMessage();

					if (newmessage.getFormatName().equals("EDD200001")) {

						try {
							beanList = new JBObjList();
						} catch (Exception ex) {
							flexLog("Error: " + ex);
						}

						boolean firstTime = true;
						String marker = "";
						String myFlag = "";
						StringBuffer myRow = null;
						String chk = "";

							while (true) {
								msgHelp = (EDD200001Message) newmessage;
								flexLog("EDD200001 Message Received");					
								marker = msgHelp.getH01FLGMAS();
				
								if (marker.equals("*")) {
									beanList.setShowNext(false);
									break;
								} else {
									beanList.addRow(msgHelp);
									if (marker.equals("+")) {
										beanList.setShowNext(true);
										break;
									}
								}
								newmessage = mc.receiveMessage();
							}

						flexLog("Putting java beans into the session");
						session.setAttribute("EDD2000Help", beanList);

						try {
							req.setAttribute("Type",req.getParameter("Type"));
							flexLog(
								"About to call Page: "
									+ LangPath
									+ "EDD2000_tipo_help_helpmessage.jsp");
							callPage(
								LangPath
									+ "EDD2000_tipo_help_helpmessage.jsp",
								req,
								res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
				}
			} catch (Exception e) {
				e.printStackTrace();
				int sck = getInitSocket(req) + 1;
				flexLog("Socket not Open(Port " + sck + "). Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
				//return;
			} finally {
				mc.close();
			}

		}

	}}
