<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ECO060001Message,datapro.eibs.beans.ECO060002Message"%>
<%@ page import="com.datapro.constants.EibsFields"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html> 
<head>
<title>Gesti&oacute;n de Aprobaci&oacute;n Masiva de Ex-Convenios</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="ExConDetalle" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "ExcSeleccion" class= "datapro.eibs.beans.ECO060001Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript"> 
  function goAction(op) 
   {
	  if (op =='1200')	
 	 {
 	 	document.forms[0].SCREEN.value = op;
		document.forms[0].action = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0600?SCREEN=1200&E01CMHFDD=<%=request.getAttribute("E01CMHFDD")%>&E01CMHFDM=<%=request.getAttribute("E01CMHFDM")%>&E01CMHFDY=<%=request.getAttribute("E01CMHFDY")%>&E01CMHFHD=<%=request.getAttribute("E01CMHFHD")%>&E01CMHFHM=<%=request.getAttribute("E01CMHFHM")%>&E01CMHFHY=<%=request.getAttribute("E01CMHFHY")%>&E01CMHIDC=<%=request.getAttribute("E01CMHIDC")%>&E01CMHREC=<%=request.getAttribute("E01CMHREC")%>";
	 	document.forms[0].submit();
	 }
   }
</SCRIPT>  

</head>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>
<h3 align="center">Gesti�n de Aprobaci�n Masiva de Ex-Convenios
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="exc_ges_list_detail.jsp, ECO0600">
</h3>

<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0600">
<input type="hidden" name="SCREEN" value="">

<input type="hidden" name="codigo_lista" value="<%=request.getAttribute("codigo_lista") %>" id="codigo_lista"> 
<input type="hidden" name="E01CMHSTS" value="<%=request.getAttribute("E01CMHSTS")%>">

<input type="hidden" name="E01CMHFDD" value="<%=request.getAttribute("E01CMHFDD") %>"> 
<input type="hidden" name="E01CMHFDM" value="<%=request.getAttribute("E01CMHFDM") %>"> 
<input type="hidden" name="E01CMHFDY" value="<%=request.getAttribute("E01CMHFDY") %>"> 

<input type="hidden" name="E01CMHFHD" value="<%=request.getAttribute("E01CMHFHD") %>"> 
<input type="hidden" name="E01CMHFHM" value="<%=request.getAttribute("E01CMHFHM") %>"> 
<input type="hidden" name="E01CMHFHY" value="<%=request.getAttribute("E01CMHFHY") %>"> 

<input type="hidden" name="E01CMHIDC" value="<%=request.getAttribute("E01CMHIDC") %>"> 
<input type="hidden" name="E01CMHREC" value="<%=request.getAttribute("E01CMHREC") %>"> 

 
   <table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">Id Archivo : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="E01CMHIDC" value="<%=ExcSeleccion.getE01CMHIDC() %>" 
	  			size="23"  readonly>
             </td>  
             
               <td nowrap width="10%" align="right">Fecha Carga : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="fechaCarga" size="23" maxlength="20" value="<% out.print(ExcSeleccion.getE01CMHCAD()+"/"+ExcSeleccion.getE01CMHCAM()+"/"+ExcSeleccion.getE01CMHCAY());%>" readonly>
             </td>  
         </tr>
         
          <tr>
             <td nowrap width="10%" align="right">Registros : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01CMHCAR" size="23" maxlength="20" value="<%=ExcSeleccion.getE01CMHCAR() %>" readonly>
             </td>
        <td nowrap width="10%" align="right">Reg Error : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01CMHVAR" size="23" maxlength="20" value="<%=ExcSeleccion.getE01CMHVAR() %>" readonly>
             </td>
             </tr>
      
           <tr>
             <td nowrap width="10%" align="right">Estado : 
               </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01CMHGST" size="23" maxlength="20" value="<%=ExcSeleccion.getE01CMHGST()%>" readonly>
             </td>
        <td nowrap width="10%" align="right">Fecha Estado : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="FechaEstado" size="23" maxlength="20" value="<%out.print(ExcSeleccion.getE01CMHCAD()+"/"+ExcSeleccion.getE01CMHCAM()+"/"+ExcSeleccion.getE01CMHCAY());%>" readonly>
             </td>
             </tr>
        
        <tr>
             <td nowrap width="10%" align="right">Usuario : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01CMHUPU" size="23" maxlength="20" value="<%=ExcSeleccion.getE01CMHUPU()%>" readonly >
             </td>
        <td nowrap width="10%" align="right" colspan="2"> 
               </td>
             </tr>
        </table>
      </td>
    </tr>
  </table>
 
 

<%
	if (ExConDetalle.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>


<table class="tbenter" width=100% >
	<tr>
		<td height="20"></td>
	</tr>
	<tr>
		<td>
		<table id="headTable" width="100%" align="left">
			<tr id="trdark">
				<th align="center" nowrap width="5%">SEC</th>
				<th align="center" nowrap width="10%">RUT Socio</th>
				<th align="center" nowrap width="10%">Estado</th>
				<th align="center" nowrap width="30%">Causal Rechazo</th>
				<th align="center" nowrap width="10%">Fecha Estado</th>
				<th align="center"></th>
			</tr>
			<%
			ExConDetalle.initRow();
				int k = 0,inicial;
				boolean firstTime = true;
				String chk = "";
				while (ExConDetalle.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ECO060002Message PDetalle = (ECO060002Message) ExConDetalle.getRecord();
		%>
			<tr>
				<td nowrap align="center" height="28"><%=PDetalle.getE02CCDSEQ()%></td>
				<td nowrap align="right" height="28"><%=PDetalle.getE02CCDIDN()%></td>
				<td nowrap align="center" height="28"><%=PDetalle.getE02CCDGST()%></td>
				<td nowrap align="left" height="28"><%=PDetalle.getE02CCDGER()%></td>
				<td nowrap align="center" height="28">
				<% out.print(PDetalle.getE02CCDUPD()+"/"+PDetalle.getE02CCDUPM()+"/"+PDetalle.getE02CCDUPY());%>
				</td>
				<td nowrap align="center" height="28">
				</td>
			</tr>
			<%}%>
		</table>
		</td>
	</tr>

	<tr>
	<td>
		<table class="tbenter" width="98%" align="center">
		<tr>
			<td width="40%" align="left">
			<%
			if (ExConDetalle.getShowPrev()) {
					int pos = ExConDetalle.getFirstRec() - 10;
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSECO0600?SCREEN=1300&posicion="
									+pos+"&codigo_lista=" + request.getAttribute("codigo_lista")
									+"&mark=atr�s"
									+"&E01CMHSTS="+request.getAttribute("E01CMHSTS")
									+"&E01CMHFDD="+request.getAttribute("E01CMHFDD")
									+"&E01CMHFDM="+request.getAttribute("E01CMHFDM")
									+"&E01CMHFDY="+request.getParameter("E01CMHFDY") 
									+"&E01CMHFHD="+request.getAttribute("E01CMHFHD")
									+"&E01CMHFHM="+request.getAttribute("E01CMHFHM")
									+"&E01CMHFHY="+request.getAttribute("E01CMHFHY")
									+"&E01CMHIDC="+request.getAttribute("E01CMHIDC")
									+"&E01CMHREC="+request.getAttribute("E01CMHREC")

									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
				%>
				</td>
				<td width="20%" align="center">
		    		<input id="EIBSBTN" type=button name="Submit" value="Atr�s" onClick="javascript:goAction('1200')">
 				</td>		
				<td width="40%" align="right">
				<%
				if (ExConDetalle.getShowNext()) {
					int pos = ExConDetalle.getLastRec();
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSECO0600?SCREEN=1300&posicion="
									+pos+"&codigo_lista=" + request.getAttribute("codigo_lista")
									+"&mark=atr�s"
									+"&E01CMHSTS="+request.getAttribute("E01CMHSTS")
									+"&E01CMHFDD="+request.getAttribute("E01CMHFDD")
									+"&E01CMHFDM="+request.getAttribute("E01CMHFDM")
									+"&E01CMHFDY="+request.getParameter("E01CMHFDY") 
									+"&E01CMHFHD="+request.getAttribute("E01CMHFHD")
									+"&E01CMHFHM="+request.getAttribute("E01CMHFHM")
									+"&E01CMHFHY="+request.getAttribute("E01CMHFHY")
									+"&E01CMHIDC="+request.getAttribute("E01CMHIDC")
									+"&E01CMHREC="+request.getAttribute("E01CMHREC")

									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/next_records.gif\" ></A>");
				}
				%>
				</td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<%
	}
%>

</form>
</body>
</html>
