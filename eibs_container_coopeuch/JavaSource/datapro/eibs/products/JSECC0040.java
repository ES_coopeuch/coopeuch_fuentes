package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Modificado por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  25/10/2012                                                                                      **/
/**  Objetivo                :  Servicio para consulta de tarjetas de Creditos y Debitos                                        **/
/**                                                                                                                             **/  
/**                                                                                                                             **/
/*********************************************************************************************************************************/

import java.beans.Beans;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ECC004001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

public class JSECC0040 extends datapro.eibs.master.SuperServlet  {

	// credit card 

	protected static final int R_SPECIAL_INST = 9;
	protected static final int R_INQUIRY_CARD = 3;
	protected static final int R_INQUIRY_CARD_POSITION = 35;	
	protected static final int R_INQUIRY_IDN = 23;
	protected static final int R_ENTER_INQUIRY = 100;
	protected static final int A_ENTER_INQUIRY = 200;
	protected static final int R_DET_CARD = 300;
	protected static final int A_ENTER_INQUIRY_DEBIT = 400;
			
	protected String LangPath = "S";
	/**
	 * JSECLI001 constructor comment.
	 */
	public JSECC0040() {
		super();
	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

		flexLog("free resources used by JSECC0040");

	}
	/**
	 * This method was created by David Mavilla.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	protected void procReqEnterInquiry(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
				throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos	userPO = null;	

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();

			userPO.setOption("CC");
			userPO.setHeader2("C");
		
			userPO.setPurpose("INQUIRY");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);
			
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}
		
		try {
			flexLog("About to call Page: " + LangPath + "ECC0040_cc_inq_enter_cards.jsp");
			callPage(LangPath + "ECC0040_cc_inq_enter_cards.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}
/**
 * This method was created in VisualAge.
*/
protected void procActionEnterInquiry(
		MessageContext mc, 
		ESS0030DSMessage user, 
		HttpServletRequest req, 
		HttpServletResponse res, 
		HttpSession ses)
			throws ServletException, IOException {

	ELEERRMessage msgError = null;
	UserPos	userPO = null;	

	try {
		msgError = new datapro.eibs.beans.ELEERRMessage();
	} catch (Exception ex) {
		flexLog("Error: " + ex); 
	}

	userPO = (datapro.eibs.beans.UserPos)ses.getAttribute("userPO");
    /*  search I = Identificacion del cliente
     *  search C = Numero de Tarjeta
     */
	String search = req.getParameter("search");
	if (search.equals("I")) {
		try {
			userPO.setIdentifier(req.getParameter("E01CCRCID"));
			userPO.setAccNum(req.getParameter(""));			
		} catch (Exception e) {
			userPO.setIdentifier("");
		}
		ses.setAttribute("userPO", userPO);
		res.sendRedirect(super.srctx + "/servlet/datapro.eibs.products.JSECC0040?SCREEN=23");
	} else {
		try {
			userPO.setAccNum(req.getParameter("E01CCRNUM"));
			userPO.setIdentifier(req.getParameter(""));
		} catch (Exception e) {
			userPO.setAccNum("");
		}
		ses.setAttribute("userPO", userPO);
		res.sendRedirect(super.srctx + "/servlet/datapro.eibs.products.JSECC0040?SCREEN=3");
	}

}

protected void procReqInquiryCard(
	MessageContext mc,
	ESS0030DSMessage user,
	HttpServletRequest req,
	HttpServletResponse res,
	HttpSession ses)
	throws ServletException, IOException {

	MessageRecord newmessage = null;
	ECC004001Message msgCC = null;
	ELEERRMessage msgError = null;
	UserPos userPO = null;
	boolean IsNotError = false;

	try {
		msgError = new ELEERRMessage();
	} catch (Exception ex) {
		flexLog("Error: " + ex);
	}

	userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
	userPO.setPurpose("INQUIRY");

	// Send Initial data
	try {
		msgCC = (ECC004001Message) mc.getMessageRecord("ECC004001");
		msgCC.setH01USERID(user.getH01USR());
		msgCC.setH01PROGRM("ECC0040");
		msgCC.setH01TIMSYS(getTimeStamp());
		msgCC.setH01SCRCOD("01");
		msgCC.setH01OPECOD("0004");
		msgCC.setE01CCRNUM(userPO.getAccNum());
		msgCC.setE01CCRCID("");
	    flexLog("mensaje enviado " + msgCC);	
		msgCC.send();
		msgCC.destroy();
	} catch (Exception e) {
		e.printStackTrace();
		flexLog("Error: " + e);
		throw new RuntimeException("Socket Communication Error");
	}

	// Receive Error Message
	// Receive Data
	try {
		newmessage = mc.receiveMessage();

		if (newmessage.getFormatName().equals("ELEERR")) {

			try {
				msgError =
					(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
						getClass().getClassLoader(),
						"datapro.eibs.beans.ELEERRMessage");
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			msgError = (ELEERRMessage) newmessage;
			IsNotError = msgError.getERRNUM().equals("0");
			flexLog("IsNotError = " + IsNotError);
			showERROR(msgError);
		} else {
			flexLog("Message " + newmessage.getFormatName() + " received.");				
		}
	} catch (Exception e) {
		e.printStackTrace();
		flexLog("Error: " + e + newmessage);
		throw new RuntimeException("Socket Communication Error Receiving");
	}	
	try {
		newmessage = mc.receiveMessage();

		if (newmessage.getFormatName().equals("ECC004001")) {

			try {
				msgCC = new ECC004001Message();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			msgCC = (ECC004001Message) newmessage;
			userPO.setCusNum(msgCC.getE01CCMCUN());
			userPO.setCusName(msgCC.getE01CCMNME());
			userPO.setHeader1(msgCC.getE01CCRCID());
			userPO.setCurrency(msgCC.getE01CCMCCY());
			userPO.setOfficer(msgCC.getE01CCMOFC());
			userPO.setProdCode(msgCC.getE01CCMPRO());
			userPO.setIdentifier(msgCC.getE01CCRCID());
			userPO.setAccNum(msgCC.getE01CCMACC());
			userPO.setHeader20(msgCC.getD01CCMPRO());
			userPO.setHeader21(msgCC.getE01CCRNXN());
			flexLog("Putting java beans into the session");
			ses.setAttribute("userPO", userPO);
			ses.setAttribute("error", msgError);

			if (IsNotError) { // There are no errors
				try {
					 if (msgCC.getE01CCRTDC().equals("C")) 
					 {
						ses.setAttribute("ccInq", msgCC);
						flexLog("About to call Page: " + LangPath + "ECC0040_cc_inq_card.jsp");
						callPage(LangPath + "ECC0040_cc_inq_card.jsp", req, res);						 
					 }
					 else
					 {
						ses.setAttribute("dcInq", msgCC); 
						flexLog("About to call Page: " + LangPath + "ECC0040_dc_inq_card.jsp");
						callPage(LangPath + "ECC0040_dc_inq_card.jsp", req, res);						 
					 }

				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}			
			} else {
				try {
					flexLog("About to call Page: " + LangPath + "ECC0040_cc_inq_enter_cards.jsp");
					callPage(LangPath + "ECC0040_cc_inq_enter_cards.jsp", req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			}				

		} else
			flexLog("Message " + newmessage.getFormatName() + " received.");

	} catch (Exception e) {
		e.printStackTrace();
		flexLog("Error: " + e);
		throw new RuntimeException("Socket Communication Data Receiving");
	}

}


protected void procReqInquiryCardPosition(
	MessageContext mc,
	ESS0030DSMessage user,
	HttpServletRequest req,
	HttpServletResponse res,
	HttpSession ses)
	throws ServletException, IOException {

	MessageRecord newmessage = null;
	ECC004001Message msgCC = null;
	ELEERRMessage msgError = null;
	UserPos userPO = null;
	boolean IsNotError = false;

	try {
		msgError = new ELEERRMessage();
	} catch (Exception ex) {
		flexLog("Error: " + ex);
	}

	userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
	userPO.setPurpose("INQUIRY");

	// Send Initial data
	try {
		msgCC = (ECC004001Message) mc.getMessageRecord("ECC004001");
		msgCC.setH01USERID(user.getH01USR());
		msgCC.setH01PROGRM("ECC0040");
		msgCC.setH01TIMSYS(getTimeStamp());
		msgCC.setH01SCRCOD("01");
		msgCC.setH01OPECOD("0006");		
		msgCC.setE01CCRCRA(req.getParameter("E01CCRCRA")==null?"":req.getParameter("E01CCRCRA"));
	    flexLog("mensaje enviado " + msgCC);	
		msgCC.send();
		msgCC.destroy();
	} catch (Exception e) {
		e.printStackTrace();
		flexLog("Error: " + e);
		throw new RuntimeException("Socket Communication Error");
	}

	// Receive Error Message
	// Receive Data
	try {
		newmessage = mc.receiveMessage();

		if (newmessage.getFormatName().equals("ELEERR")) {

			try {
				msgError =(ELEERRMessage) Beans.instantiate(getClass().getClassLoader(),"datapro.eibs.beans.ELEERRMessage");
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			msgError = (ELEERRMessage) newmessage;
			IsNotError = msgError.getERRNUM().equals("0");
			flexLog("IsNotError = " + IsNotError);
			showERROR(msgError);
		} else {
			flexLog("Message " + newmessage.getFormatName() + " received.");				
		}
	} catch (Exception e) {
		e.printStackTrace();
		flexLog("Error: " + e + newmessage);
		throw new RuntimeException("Socket Communication Error Receiving");
	}	
	try {
		newmessage = mc.receiveMessage();

		if (newmessage.getFormatName().equals("ECC004001")) {

			try {
				msgCC = new ECC004001Message();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			msgCC = (ECC004001Message) newmessage;
			userPO.setCusNum(msgCC.getE01CCMCUN());
			userPO.setCusName(msgCC.getE01CCMNME());
			userPO.setHeader1(msgCC.getE01CCRCID());
			userPO.setCurrency(msgCC.getE01CCMCCY());
			userPO.setOfficer(msgCC.getE01CCMOFC());
			userPO.setProdCode(msgCC.getE01CCMPRO());
			userPO.setIdentifier(msgCC.getE01CCRCID());
			userPO.setAccNum(msgCC.getE01CCMACC());
			userPO.setHeader20(msgCC.getD01CCMPRO());
			userPO.setHeader21(msgCC.getE01CCRNXN());
			flexLog("Putting java beans into the session");
			ses.setAttribute("userPO", userPO);
			ses.setAttribute("error", msgError);

			if (IsNotError) { // There are no errors
				try {
					 if (msgCC.getE01CCRTDC().equals("D")) 
					 {
							ses.setAttribute("dcInq", msgCC); 
							flexLog("About to call Page: " + LangPath + "ECC0040_dc_inq_card.jsp");
							callPage(LangPath + "ECC0040_dc_inq_card.jsp", req, res);						 
						 
					 }
					 else
					 {
							ses.setAttribute("ccInq", msgCC);
							flexLog("About to call Page: " + LangPath + "ECC0040_cc_inq_card.jsp");
							callPage(LangPath + "ECC0040_cc_inq_card.jsp", req, res);						 
						 
					 }

				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}			
			} else {
				try {
					flexLog("About to call Page: " + LangPath + "ECC0040_cc_inq_enter_cards.jsp");
					callPage(LangPath + "ECC0040_cc_inq_enter_cards.jsp", req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			}				

		} else
			flexLog("Message " + newmessage.getFormatName() + " received.");

	} catch (Exception e) {
		e.printStackTrace();
		flexLog("Error: " + e);
		throw new RuntimeException("Socket Communication Data Receiving");
	}

}
protected void procReqInquiryIdnList(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECC004001Message msgList = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		userPO.setPurpose("INQUIRY");

		// Send Initial data
		try {
			msgList = (ECC004001Message) mc.getMessageRecord("ECC004001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ECC0040");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			msgList.setH01OPECOD("0005");
			msgList.setE01CCRNXN("");
			msgList.setE01CCRCID(userPO.getIdentifier());
		    flexLog("mensaje enviado " + msgList);	
		    msgList.send();
		    msgList.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError =
						(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
							getClass().getClassLoader(),
							"datapro.eibs.beans.ELEERRMessage");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else {
				flexLog("Message " + newmessage.getFormatName() + " received.");				
			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e + newmessage);
			throw new RuntimeException("Socket Communication Error Receiving");
		}

		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECC004001")) {

				JBObjList beanList = new JBObjList();

				boolean firstTime = true;
				String marker = "";
				String chk = "";
					while (true) {

					msgList = (ECC004001Message) newmessage;

					marker = msgList.getH01FLGMAS();

					if (firstTime) {
						userPO.setCusNum(msgList.getE01CCMCUN());
						userPO.setCusName(msgList.getE01CCMNME());
						userPO.setHeader1(msgList.getE01CCRCID());
						userPO.setCurrency(msgList.getE01CCMCCY());
						userPO.setOfficer(msgList.getE01CCMOFC());
						userPO.setProdCode(msgList.getE01CCMPRO());
						userPO.setIdentifier(msgList.getE01CCRCID());
						userPO.setAccNum(msgList.getE01CCMACC());
						userPO.setHeader20(msgList.getD01CCMPRO());	
						userPO.setHeader21(msgList.getE01CCRNXN());	
						firstTime = false;
						chk = "checked";

					} else {
						chk = "";
					}

					if (marker.equals("*")) {
						beanList.setShowNext(false);
						break;
					} else {
						beanList.addRow(msgList);

						if (marker.equals("+")) {
							beanList.setShowNext(true);

							break;
						}
					}
					newmessage = mc.receiveMessage();
				}

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("ECC0040Help", beanList);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog(
							"About to call Page: " + LangPath + "ECC0040_cc_inq_card_list.jsp");
						callPage(LangPath + "ECC0040_cc_inq_card_list.jsp", req, res);					
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				
				} else {
					try {
						flexLog("About to call Page: " + LangPath + "ECC0040_cc_inq_enter_cards.jsp");
						callPage(LangPath + "ECC0040_cc_inq_enter_cards.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}				}				

			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Data Receiving");
		}

	}
	
protected void procReqDetCard(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {
			ECC004001Message msgCC = null;
			UserPos userPO = null;
			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			// Receive Data
			try {
				JBObjList bl = (JBObjList) ses.getAttribute("ECC0040Help");
				int idx = req.getParameter("CURRCODE")==null?0:req.getParameter("CURRCODE").equals("")?0:Integer.parseInt(req.getParameter("CURRCODE"));								
				flexLog("...Indice  " +  idx);
				bl.setCurrentRow(idx);
				msgCC = (ECC004001Message) bl.getRecord();			
				userPO.setCusNum(msgCC.getE01CCMCUN());
				userPO.setCusName(msgCC.getE01CCMNME());
				userPO.setHeader1(msgCC.getE01CCRCID());
				userPO.setCurrency(msgCC.getE01CCMCCY());
				userPO.setOfficer(msgCC.getE01CCMOFC());
				userPO.setProdCode(msgCC.getE01CCMPRO());
				userPO.setIdentifier(msgCC.getE01CCRCID());
				userPO.setAccNum(msgCC.getE01CCMACC());
				userPO.setHeader20(msgCC.getD01CCMPRO());	
				userPO.setHeader21(msgCC.getE01CCRNXN());				
				flexLog("Putting java beans into the session");
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("ccInq", msgCC);

				try {
					 if (msgCC.getE01CCRTDC().equals("C")) 
					 {
						ses.setAttribute("ccInq", msgCC);
						flexLog("About to call Page: " + LangPath + "ECC0040_cc_inq_card.jsp");
						callPage(LangPath + "ECC0040_cc_inq_card.jsp", req, res);						 
					 }
					 else
					 {
						ses.setAttribute("dcInq", msgCC); 
						flexLog("About to call Page: " + LangPath + "ECC0040_dc_inq_card.jsp");
						callPage(LangPath + "ECC0040_dc_inq_card.jsp", req, res);						 
					 }
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}
protected void procReqAdditionalCardsList(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECC004001Message msgList = null;
		JBObjList appList = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		String Type = req.getParameter("Type");
		if (Type == null) {
			Type = userPO.getHeader2();	
		} else {
			userPO.setHeader2(Type);
		}		 

		// Send Initial data
		try {
			msgList = (ECC004001Message) mc.getMessageRecord("ECC004001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ECC0040");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			msgList.setH01OPECOD("0015");
			
			msgList.setE01CCRTDC(Type);
		
			try {
				msgList.setE01CCRCRA(req.getParameter("E01CCRCRA"));
			} catch (Exception e) {
				msgList.setE01CCRCRA(userPO.getAccNum());
			}			
			msgList.setE01CCRTCL("S");
			flexLog("mensaje enviado..." + msgList);
			msgList.send();
			msgList.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		flexLog("Initializing java beans into the session");
		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError =
						(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
							getClass().getClassLoader(),
							"datapro.eibs.beans.ELEERRMessage");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else {
				flexLog("Message " + newmessage.getFormatName() + " received.");				
			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e + newmessage);
			throw new RuntimeException("Socket Communication Error Receiving");
		}
		// Receive Data		
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ECC004001")) {
				appList = new JBObjList();
				boolean firstTime = true;
				String marker = "";

					while (true) {
						msgList = (ECC004001Message) newmessage;
						flexLog("mensaje recibido..." + msgList);
						marker = msgList.getH01FLGMAS();

						if (marker.equals("*")) {
								userPO.setAccNum(msgList.getE01CCRCRA());
								userPO.setIdentifier(msgList.getE01CCRCRA());
								userPO.setCusNum(msgList.getE01CCRCUN());
								userPO.setCusName(msgList.getE01CCRNAM());
								userPO.setHeader1(msgList.getE01CCRNUM());
								//userPO.setHeader2(msgList.getE01TARTYP());				
								appList.setShowNext(false);
								break;
						} else {
							appList.addRow(msgList);
							if (firstTime) {
								firstTime = false;
								//appList.setFirstRec(Integer.parseInt(msgList.getE01CCRNUM()));
								userPO.setAccNum(msgList.getE01CCRCRA());
								userPO.setIdentifier(msgList.getE01CCRCRA());
								userPO.setCusNum(msgList.getE01CCRCUN());
								userPO.setCusName(msgList.getE01CCRNAM());
								userPO.setHeader1(msgList.getE01CCRNUM());
								//userPO.setHeader2(msgList.getE01TARTYP());				
								userPO.setHeader20("");
								userPO.setHeader21("");
							}
							if (marker.equals("+")) {
								appList.setShowNext(true);
								break;
							}
						}
						newmessage = mc.receiveMessage();
					}

					flexLog("Putting java beans into the session");
					ses.setAttribute("appList", appList);
					ses.setAttribute("error", msgError);
					ses.setAttribute("userPO", userPO);

					try {
						flexLog("About to call Page: " + LangPath + "ECC0040_cc_inq_additional_cards_list.jsp");
						callPage(LangPath + "ECC0040_cc_inq_additional_cards_list.jsp", req, res);

					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		Socket s = null;
		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			int screen = R_ENTER_INQUIRY;

			try {

				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					s = new Socket(super.hostIP, getInitSocket(req) + 1);
					s.setSoTimeout(super.sckTimeOut);
					mc =
						new MessageContext(
							new DataInputStream(new BufferedInputStream(s.getInputStream())),
							new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
							"datapro.eibs.beans");
				

				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}

				switch (screen) {
					// Request

					case R_ENTER_INQUIRY :
						procReqEnterInquiry(msgUser, req, res, session);
						break;	
					case R_INQUIRY_CARD :
						procReqInquiryCard(mc, msgUser, req, res, session);
						break;
					case R_INQUIRY_CARD_POSITION :
						procReqInquiryCardPosition(mc, msgUser, req, res, session);
						break;						
					case R_INQUIRY_IDN :
						procReqInquiryIdnList(mc, msgUser, req, res, session);
						break;																	
					// Action
					case A_ENTER_INQUIRY :
						procActionEnterInquiry(mc, msgUser, req, res, session);
						break;	
					case R_DET_CARD :
						procReqDetCard(mc, msgUser, req, res, session);
						break;		
					case A_ENTER_INQUIRY_DEBIT :
						procReqAdditionalCardsList(mc, msgUser, req, res, session);
						break;							
					
					default :
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
				}

				} catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 3;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
					//return;
				} finally {
					s.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}

		}

	}
	
	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

		}
	}


}


