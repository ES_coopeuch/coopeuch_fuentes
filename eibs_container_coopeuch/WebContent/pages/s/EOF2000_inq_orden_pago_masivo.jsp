<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>ordenes de pago masivo</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="opMasivo" class="datapro.eibs.beans.EOF200001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

</head>

<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
}
%>

<h3 align="center">CONSULTA ORDEN DE PAGO MASIVO
 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="inq_orden_pago_masivo.jsp, EOF2000"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEOF2000" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  
 <% int row = 0;%>
 
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Sucursal :</div>
            </td>
            <td width="70%" > 
	             <eibsinput:text property="E01OPMBRN" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH%>" readonly="true"/>
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Centro de Costo :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMCCO" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>"  readonly="false"/>
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Glosa :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMGLO" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" readonly="false"/>
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Fecha de Ingreso :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMFCU" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="false"/>
	        </td>
          </tr>   
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Codigo de Formato :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMCDF" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE%>" readonly="false"/>
	        </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Rut del Tomador :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMRTO" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>" readonly="false"/>
	        </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Rut del Beneficiario :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMRBE" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>" readonly="false"/>
	        </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Nombre del Beneficiario :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMNBE" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" readonly="false"/>
	        </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Monto :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMAMT" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="false"/>
	        </td>
          </tr>   
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Motivo de OP :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMMOT" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE%>" readonly="false"/>
	        </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Usuario :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMCUS" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" readonly="false"/>
	        </td>
          </tr>     
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Estado :</div>
            </td>
            <td nowrap width="70%"> 
			  <div align="left">
			  		<% if(opMasivo.getE01OPMSTS().equals("")) out.print("Cargada");
              				else if(opMasivo.getE01OPMSTS().equals("A")) out.print("Validada, Sin errores");
              				else if(opMasivo.getE01OPMSTS().equals("R")) out.print("Rechazada");
              				else if(opMasivo.getE01OPMSTS().equals("P")) out.print("Procesada");              				
							else out.print(" ");%>
              </div>
		   </td>
          </tr>   
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="30%" > 
              <div align="right">Error :</div>
            </td>
            <td width="70%" > 
                 <eibsinput:text property="E01OPMRDS" name="opMasivo" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" readonly="false"/>
	        </td>
          </tr>                                                                                           
         </table>
      </td>
    </tr>
  </table>

  </form>
</body>
</HTML>
