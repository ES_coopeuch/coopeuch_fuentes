//
// Generated By:JAX-WS RI IBM 2.1.1 in JDK 6 (JAXB RI IBM JAXB 2.1.3 in JDK 1.6)
//


package cl.coopeuch.core.tiendavirtual;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsultaProductoCampanaEntrada complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsultaProductoCampanaEntrada">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCampana" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codProducto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaProductoCampanaEntrada", namespace = "http://tiendavirtual.core.coopeuch.cl", propOrder = {
    "codCampana",
    "codProducto"
})
public class ConsultaProductoCampanaEntrada {

    protected int codCampana;
    protected int codProducto;

    /**
     * Gets the value of the codCampana property.
     * 
     */
    public int getCodCampana() {
        return codCampana;
    }

    /**
     * Sets the value of the codCampana property.
     * 
     */
    public void setCodCampana(int value) {
        this.codCampana = value;
    }

    /**
     * Gets the value of the codProducto property.
     * 
     */
    public int getCodProducto() {
        return codProducto;
    }

    /**
     * Sets the value of the codProducto property.
     * 
     */
    public void setCodProducto(int value) {
        this.codProducto = value;
    }

}
