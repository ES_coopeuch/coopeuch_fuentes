package datapro.eibs.products;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */ 
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;
import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.ECC150101Message;
import datapro.eibs.beans.ECC150102Message;
import datapro.eibs.beans.ECC150103Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ExcelColStyle;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.ServiceLocator;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;
import datapro.eibs.sockets.MessageField;

public class JSECC1501 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_ECC_ENTER = 100;	
	protected static final int A_ECC_ENTER = 200;
	
	protected static final int R_ECC_PREEM_ENTER_GES = 1100;
	protected static final int R_ECC_PREEM_ENTER_AUT = 1600;
	protected static final int R_ECC_PREEM_ENTER_CON = 1700;
	protected static final int A_ECC_PREEM_LIST  = 1200;
	
	protected static final int A_ECC_PREEM_LIST_DETAIL  = 1300;
	protected static final int A_ECC_PREEM_VALIDATE_DETAIL  = 1400;
	protected static final int A_ECC_PREEM_REFUSE  = 1500;
	protected static final int R_ECC_PREEM_ACTION_AUT = 1800;
	protected static final int R_ECC_PREEM_ACTION_DETAIL_TO_EXCEL = 1900;

	protected static final int R_ECC_PREEM_RUT_ENTER = 2000;
	protected static final int A_ECC_PREEM_RUT_ENTER = 2100;
	protected static final int A_ECC_PREEM_RUT_ENTER_TO_EXCEL = 2200;
	
	protected static final int A_ECC_ENTER_TC = 1;
	protected static final int A_ECC_ENTER_TD = 2;
	
	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		screen =  A_ECC_ENTER;
		String opt= "";
		
				
		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}		
		
		switch (screen) {
		case R_ECC_PREEM_ENTER_GES:
			opt= "01";
			break;
		case R_ECC_PREEM_ENTER_AUT: 
			opt= "02";
			break;
		case R_ECC_PREEM_ENTER_CON:
			opt= "03";
			break;
		}

		switch (screen) {
		case R_ECC_ENTER:
			procReqECCEnter(user, req, res, session);
			break;
		case A_ECC_ENTER:
			procActionECCEnter(user, req, res, session);
			break; 

		case R_ECC_PREEM_ENTER_GES:
		case R_ECC_PREEM_ENTER_AUT: 
		case R_ECC_PREEM_ENTER_CON: 
			procReqECCPreemEnter(user, req, res, session, opt);
			break;

		case A_ECC_PREEM_LIST:
			procActionECCPreemList(user, req, res, session);
			break; 

		case A_ECC_PREEM_LIST_DETAIL:
			procActionECCPreemListDetail(user, req, res, session);
			break; 

		case A_ECC_PREEM_VALIDATE_DETAIL:
			procActionECCPreemValidateDetail(user, req, res, session);
			break; 

		case A_ECC_PREEM_REFUSE:
			procActionECCPreemRefuse(user, req, res, session);
			break; 
			
		case R_ECC_PREEM_ACTION_AUT:
			procActionECCPreemAuthorize(user, req, res, session);
			break; 
			
		case R_ECC_PREEM_ACTION_DETAIL_TO_EXCEL:
			procActionECCPreemDetailtoExcel(user, req, res, session);
			break; 

		case R_ECC_PREEM_RUT_ENTER:
			procReqECCRutEnter(user, req, res, session);
			break;

		case A_ECC_PREEM_RUT_ENTER:
			procActionECCPreemListRutDetail(user, req, res, session);
			break; 

		case A_ECC_PREEM_RUT_ENTER_TO_EXCEL :
			procActionECCPreemListRutDetailtoExcel(user, req, res, session);
			break; 

			
		}		
	}
	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqECCEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: ECC1501_ecc_enter.jsp");
			forward("ECC1501_ecc_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	
	//
    protected void procActionECCEnter(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session)
    throws ServletException, IOException
    {
    	SmartUpload mySmartUpload = new SmartUpload();
    	com.jspsmart.upload.File myFile = null;
    	
		try
        {
			mySmartUpload.initialize(config, req, res);
			mySmartUpload.upload();
			myFile = mySmartUpload.getFiles().getFile(0);
			
			String StrTipInt = mySmartUpload.getRequest().getParameter("E01REOIDE").toString();

			switch (Integer.parseInt(StrTipInt)) 
			{
			case A_ECC_ENTER_TC:
				procActionECCEnter_TC(user, req, res, session, myFile);
				break;
			case A_ECC_ENTER_TD:
				procActionECCEnter_TD(user, req, res, session, myFile);
				break; 
			}
        }
		catch(Exception e)
		{
				e.printStackTrace();
		}
    
    }

	
	//
	protected void procActionECCEnter_TC(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, com.jspsmart.upload.File myFile )
			throws ServletException, IOException {
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		boolean ok = false;
		String idCartola = "";
		
		try {
			try {
				if (myFile.getSize() > 0) {
					//El archivo tiene datos buscamos el n�mero de Interfaz asignada.					
					mp = getMessageProcessor("ECC1501", req);					
					ECC150101Message msg = (ECC150101Message) mp.getMessageRecord("ECC150101");
					//seteamos las propiedades
					msg.setH01USERID(user.getH01USR());
					msg.setH01OPECOD("0001");
					msg.setH01TIMSYS(getTimeStamp());					
					msg.setH01SCRCOD("01");	
					
					//Sending message
					mp.sendMessage(msg);
		
					//Receive error and data
					msgError = (ELEERRMessage) mp.receiveMessageRecord();
					msg = (ECC150101Message) mp.receiveMessageRecord();		
					
					
					//havent errors i get the field
					if (!mp.hasError(msgError)) {
					try {
							String fileName =  "CCPINT";//nombre del archivo	
							idCartola = msg.getE01CCHIDC();
						
							byte[] bd = new byte[myFile.getSize()];
							for (int i = 0; i < myFile.getSize(); i++) {
								bd[i] = myFile.getBinaryData(i);
							}
						
							InputStream  input = new ByteArrayInputStream(bd);													
							String userid = ServiceLocator.getSlInfo().getString("ftp.cnx.userid.eibs-server");
							String password = ServiceLocator.getSlInfo().getString("ftp.cnx.password.eibs-server");												
						
							FTPWrapper ftp = new FTPStdWrapper(JSEIBSProp.getHostIP(), userid, password, "");
						
							if (ftp.open()) 
							{
								ftp.setFileType(FTPWrapper.ASCII);
								ftp.upload(input,fileName); 
								ok=true;
							}
							else
							{	
								msgError = new ELEERRMessage();
								msgError.setERRNUM("1");
								msgError.setERNU01("01");		                
								msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
							}	

						} catch (Exception e) {
							msgError = new ELEERRMessage();
							msgError.setERRNUM("1");
							msgError.setERNU01("01");		                
							msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
						}
					}
					
				}else{
					//mandamos error en session 
					msgError = new ELEERRMessage();
					msgError.setERRNUM(new BigDecimal(1));
					msgError.setERDS01("Archivo Vacio...");							
				}
				
			}catch (Exception e) {
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();	
				msgError = new ELEERRMessage();			
				msgError.setERRNUM("1");
	            msgError.setERNU01("01");
	            msgError.setERDS01(className);
	            msgError.setERNU02("02");
	            msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
	            msgError.setERNU03("03");
	            msgError.setERDS03("Para mas informacion revizar los archivos de log.");
				e.printStackTrace();			
			} 	
			
			if (ok){									
				ECC150101Message msg = (ECC150101Message) mp.getMessageRecord("ECC150101");
				msg.setH01USERID(user.getH01USR());
				msg.setH01OPECOD("0002");
				msg.setH01TIMSYS(getTimeStamp());
				//Sets message with page fields
				
				
				msg.setE01CCHIDC(idCartola);

				//Sending message
				mp.sendMessage(msg);

				//Receive error and data
				ELEERRMessage msgError2 = (ELEERRMessage) mp.receiveMessageRecord();
				msg = (ECC150101Message) mp.receiveMessageRecord();

				msg.setH01FLGWK1("1"); //TCREDITO

				//Sets session with required data
				session.setAttribute("userPO", userPO);
				session.setAttribute("bank", msg);

				if (!mp.hasError(msgError2)) {
					forward("ECC1501_ecc_procces.jsp", req, res);
				} else {
					//if there are errors go back to maintenance page and show errors
					session.setAttribute("error", msgError2);
					forward("ECC1501_ecc_enter.jsp", req, res);
				}

			
			}else{
				session.setAttribute("error", msgError);				
				forward("ECC1501_ecc_enter.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	//
	protected void procActionECCEnter_TD(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, com.jspsmart.upload.File myFile )
			throws ServletException, IOException {
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		boolean ok = false;
		
		try {
			try {
				if (myFile.getSize() > 0) {
					//El archivo tiene datos buscamos el n�mero de Interfaz asignada.					
					String fileName =  "DDCVIS";//nombre del archivo	
						
					byte[] bd = new byte[myFile.getSize()];
					for (int i = 0; i < myFile.getSize(); i++) 
						bd[i] = myFile.getBinaryData(i);
						
					InputStream  input = new ByteArrayInputStream(bd);													
					String userid = ServiceLocator.getSlInfo().getString("ftp.cnx.userid.eibs-server");
					String password = ServiceLocator.getSlInfo().getString("ftp.cnx.password.eibs-server");												
						
					FTPWrapper ftp = new FTPStdWrapper(JSEIBSProp.getHostIP(), userid, password, "");
						
					if (ftp.open()) 
					{
						ftp.setFileType(FTPWrapper.ASCII);
						ftp.upload(input,fileName); 
						ok=true;
					}
					else
					{	
						msgError = new ELEERRMessage();
						msgError.setERRNUM("1");
						msgError.setERNU01("01");		                
						msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
					}	
				}
				else
				{
					//mandamos error en session 
					msgError = new ELEERRMessage();
					msgError.setERRNUM(new BigDecimal(1));
					msgError.setERDS01("Archivo Vacio...");							
				}
			}catch (Exception e) {
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();	
				msgError = new ELEERRMessage();			
				msgError.setERRNUM("1");
	            msgError.setERNU01("01");
	            msgError.setERDS01(className);
	            msgError.setERNU02("02");
	            msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
	            msgError.setERNU03("03");
	            msgError.setERDS03("Para mas informacion revizar los archivos de log.");
				e.printStackTrace();			
			} 	

			session.setAttribute("error", msgError);
			if (ok)
			{ 	
				ECC150101Message msg = (ECC150101Message) new ECC150101Message();
				msg.setH01FLGWK1("2"); //TCREDITO

				forward("ECC1501_ecc_procces.jsp", req, res);
					
			}
			else 
					forward("ECC1501_ecc_enter.jsp", req, res);
		} 
		finally 
		{
			if (mp != null)
				mp.close();
		}
	}

	
//	
	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqECCPreemEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses, String opt)
			throws ServletException, IOException {

		try {
			//Se utiliza 01-VALIDACION/CARGA 02-AUTORIZACION 03-CONSULTA
			
			req.setAttribute("opt", opt);
			
			flexLog("About to call Page: ECC1501_ecc_preem_enter.jsp");
			forward("ECC1501_ecc_preem_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}


	protected void procActionECCPreemList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			
			mp = getMessageProcessor("ECC1501", req);
	
			ECC150101Message msgList = (ECC150101Message) mp.getMessageRecord("ECC150101", user.getH01USR(), "0003");
			
			
			if(req.getParameter("opt")!=null)
				msgList.setH01SCRCOD(req.getParameter("opt"));
			else
				msgList.setH01SCRCOD("01");
			
			if(req.getParameter("posicion")!=null)
				msgList.setE01CCHREC(req.getParameter("posicion"));
			else
				msgList.setE01CCHREC("0");

			setMessageRecord(req, msgList);
			
			req.setAttribute("opt", req.getParameter("opt"));
	        session.setAttribute("E01CCHIDC", req.getParameter("E01CCHIDC"));
	        session.setAttribute("E01CCHSTS", req.getParameter("E01CCHSTS"));
	        
			String fecha1=req.getParameter("E01CCHFDD")+"/"+req.getParameter("E01CCHFDM")+"/"+req.getParameter("E01CCHFDY");
			String fecha2=req.getParameter("E01CCHFHD")+"/"+req.getParameter("E01CCHFHM")+"/"+req.getParameter("E01CCHFHY");
			
			session.setAttribute("fecha_inicial", fecha1);
			session.setAttribute("fecha_final", fecha2);

			session.setAttribute("PreemHeader", msgList);
			mp.sendMessage(msgList);
						
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { // if there are errors go back to first page
				session.setAttribute("error", error);
				
				flexLog("About to call Page: ECC1501_ecc_preem_enter.jsp");
				forward("ECC1501_ecc_preem_enter.jsp", req, res);
			} else {
			
				
     		JBObjList list = mp.receiveMessageRecordList("H01FLGMAS","E01CCHREC");
				
			req.setAttribute("E01CCHSTS", req.getParameter("E01CCHSTS"));
			req.setAttribute("E01CCHFDD", req.getParameter("E01CCHFDD"));
			req.setAttribute("E01CCHFDM", req.getParameter("E01CCHFDM"));
			req.setAttribute("E01CCHFDY", req.getParameter("E01CCHFDY"));
			req.setAttribute("E01CCHFHD", req.getParameter("E01CCHFHD"));
			req.setAttribute("E01CCHFHM", req.getParameter("E01CCHFHM"));
			req.setAttribute("E01CCHFHY", req.getParameter("E01CCHFHY"));
			req.setAttribute("E01CCHIDC", req.getParameter("E01CCHIDC"));
			req.setAttribute("E01CCHREC", msgList.getE01CCHREC());
			
			session.setAttribute("PreemHeader", list);
			forward("ECC1501_ecc_preem_list.jsp", req, res);
			
			}
		} finally { 
			if (mp != null)
				mp.close();
		}
	}

	
	protected void procActionECCPreemListDetail(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			mp = getMessageProcessor("ECC1501", req);
			
			ECC150102Message msgList = (ECC150102Message) mp.getMessageRecord("ECC150102", user.getH01USR(), "0001");
			
			if (req.getParameter("codigo_lista")!= null){
				
				JBObjList list = (JBObjList)session.getAttribute("PreemHeader");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ECC150101Message listMessage = (ECC150101Message)list.get(index);
				
				msgList.setE02CCDIDC(listMessage.getE01CCHIDC());
				req.setAttribute("codigo_lista", index);
		
				session.setAttribute("ECCSeleccion", listMessage);
			}
			
			if(req.getParameter("posicion")!=null)
				msgList.setE02CCDREC(req.getParameter("posicion"));
			else
				msgList.setE02CCDREC("0");
			
			if(req.getParameter("opt")!=null)
				req.setAttribute("opt", req.getParameter("opt"));
			else
				req.setAttribute("opt","01");

			
			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { 
				session.setAttribute("error", error);
				flexLog("About to call Page: ECC1501_ecc_preem_list.jsp");
				JBObjList PreemHeader = (JBObjList)session.getAttribute("PreemHeader");
				req.setAttribute("E01CCHSTS", req.getParameter("E01CCHSTS"));
				req.setAttribute("E01CCHFDD", req.getParameter("E01CCHFDD"));
				req.setAttribute("E01CCHFDM", req.getParameter("E01CCHFDM"));
				req.setAttribute("E01CCHFDY", req.getParameter("E01CCHFDY"));
				req.setAttribute("E01CCHFHD", req.getParameter("E01CCHFHD"));
				req.setAttribute("E01CCHFHM", req.getParameter("E01CCHFHM"));
				req.setAttribute("E01CCHFHY", req.getParameter("E01CCHFHY"));
				req.setAttribute("E01CCHIDC", req.getParameter("E01CCHIDC"));
				req.setAttribute("E01CCHREC", req.getParameter("E01CCHREC"));

				forward("ECC1501_ecc_preem_list.jsp", req, res);
			}
			else
			{
				JBObjList list = mp.receiveMessageRecordList("H02FLGMAS","E02CCDREC");

				req.setAttribute("E01CCHSTS", req.getParameter("E01CCHSTS"));
				req.setAttribute("E01CCHFDD", req.getParameter("E01CCHFDD"));
				req.setAttribute("E01CCHFDM", req.getParameter("E01CCHFDM"));
				req.setAttribute("E01CCHFDY", req.getParameter("E01CCHFDY"));
				req.setAttribute("E01CCHFHD", req.getParameter("E01CCHFHD"));
				req.setAttribute("E01CCHFHM", req.getParameter("E01CCHFHM"));
				req.setAttribute("E01CCHFHY", req.getParameter("E01CCHFHY"));
				req.setAttribute("E01CCHIDC", req.getParameter("E01CCHIDC"));
				req.setAttribute("E01CCHREC", req.getParameter("E01CCHREC"));

        	    session.setAttribute("PreemDetalle", list);
     			forwardOnSuccess("ECC1501_ecc_list_detail.jsp", req, res);
			}  

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	
	
	protected void procActionECCPreemValidateDetail(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
	throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try { 
			mp = getMessageProcessor("ECC1501", req);
			ECC150101Message msgList = (ECC150101Message) mp.getMessageRecord("ECC150101", user.getH01USR(), "0004");
			
			if (req.getParameter("codigo_lista")!= null){
				JBObjList list = (JBObjList)session.getAttribute("PreemHeader");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ECC150101Message listMessage = (ECC150101Message)list.get(index);
				
				msgList.setE01CCHIDC(listMessage.getE01CCHIDC());
				
				req.setAttribute("codigo_lista", index);
		
				session.setAttribute("ECCSeleccion", listMessage);
			}
			
			if(req.getParameter("opt")!=null)
				req.setAttribute("opt", req.getParameter("opt"));
			else
				req.setAttribute("opt","01");

			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) 
			{ // if there are errors go back to first page
				session.setAttribute("error", error);
				JBObjList PreemHeader = (JBObjList)session.getAttribute("PreemHeader");
				req.setAttribute("E01CCHSTS", req.getParameter("E01CCHSTS"));
				req.setAttribute("E01CCHFDD", req.getParameter("E01CCHFDD"));
				req.setAttribute("E01CCHFDM", req.getParameter("E01CCHFDM"));
				req.setAttribute("E01CCHFDY", req.getParameter("E01CCHFDY"));
				req.setAttribute("E01CCHFHD", req.getParameter("E01CCHFHD"));
				req.setAttribute("E01CCHFHM", req.getParameter("E01CCHFHM"));
				req.setAttribute("E01CCHFHY", req.getParameter("E01CCHFHY"));
				req.setAttribute("E01CCHIDC", req.getParameter("E01CCHIDC"));
				req.setAttribute("E01CCHREC", req.getParameter("E01CCHREC"));

				
				forward("ECC1501_ecc_preem_list.jsp", req, res);
			} 
			else 
			{
		    
				procActionECCPreemList(user, req, res, session);
			} 
		} finally {
			if (mp != null)
				mp.close();
		}
	}	

	protected void procActionECCPreemRefuse(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
	throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try { 
			mp = getMessageProcessor("ECC1501", req);
			ECC150101Message msgList = (ECC150101Message) mp.getMessageRecord("ECC150101", user.getH01USR(), "0005");
			
			if (req.getParameter("codigo_lista")!= null){
				JBObjList list = (JBObjList)session.getAttribute("PreemHeader");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ECC150101Message listMessage = (ECC150101Message)list.get(index);
				
				msgList.setE01CCHIDC(listMessage.getE01CCHIDC());
				
				req.setAttribute("codigo_lista", index);
		
				session.setAttribute("ECCSeleccion", listMessage);
			}
			
			if(req.getParameter("opt")!=null)
				req.setAttribute("opt", req.getParameter("opt"));
			else
				req.setAttribute("opt","01");
			
			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) 
			{ // if there are errors go back to first page
				session.setAttribute("error", error);
				JBObjList PreemHeader = (JBObjList)session.getAttribute("PreemHeader");
				req.setAttribute("E01CCHSTS", req.getParameter("E01CCHSTS"));
				req.setAttribute("E01CCHFDD", req.getParameter("E01CCHFDD"));
				req.setAttribute("E01CCHFDM", req.getParameter("E01CCHFDM"));
				req.setAttribute("E01CCHFDY", req.getParameter("E01CCHFDY"));
				req.setAttribute("E01CCHFHD", req.getParameter("E01CCHFHD"));
				req.setAttribute("E01CCHFHM", req.getParameter("E01CCHFHM"));
				req.setAttribute("E01CCHFHY", req.getParameter("E01CCHFHY"));
				req.setAttribute("E01CCHIDC", req.getParameter("E01CCHIDC"));
				req.setAttribute("E01CCHREC", req.getParameter("E01CCHREC"));
				
				forward("ECC1501_ecc_preem_list.jsp", req, res);
			} 
			else 
			{
				
				procActionECCPreemList(user, req, res, session);
			} 
		} finally {
			if (mp != null)
				mp.close();
		}
	}	


	protected void procActionECCPreemAuthorize(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
	throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try { 
			mp = getMessageProcessor("ECC1501", req);
			ECC150101Message msgList = (ECC150101Message) mp.getMessageRecord("ECC150101", user.getH01USR(), "0006");
			
			if (req.getParameter("codigo_lista")!= null){
				JBObjList list = (JBObjList)session.getAttribute("PreemHeader");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ECC150101Message listMessage = (ECC150101Message)list.get(index);
				
				msgList.setE01CCHIDC(listMessage.getE01CCHIDC());
				
				req.setAttribute("codigo_lista", index);
		
				session.setAttribute("ECCSeleccion", listMessage);
			}
			
			if(req.getParameter("opt")!=null)
				req.setAttribute("opt", req.getParameter("opt"));
			else
				req.setAttribute("opt","01");
			
			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) 
			{ // if there are errors go back to first page
				session.setAttribute("error", error);
				JBObjList PreemHeader = (JBObjList)session.getAttribute("PreemHeader");
				req.setAttribute("E01CCHSTS", req.getParameter("E01CCHSTS"));
				req.setAttribute("E01CCHFDD", req.getParameter("E01CCHFDD"));
				req.setAttribute("E01CCHFDM", req.getParameter("E01CCHFDM"));
				req.setAttribute("E01CCHFDY", req.getParameter("E01CCHFDY"));
				req.setAttribute("E01CCHFHD", req.getParameter("E01CCHFHD"));
				req.setAttribute("E01CCHFHM", req.getParameter("E01CCHFHM"));
				req.setAttribute("E01CCHFHY", req.getParameter("E01CCHFHY"));
				req.setAttribute("E01CCHIDC", req.getParameter("E01CCHIDC"));
				req.setAttribute("E01CCHREC", req.getParameter("E01CCHREC"));
				
				forward("ECC1501_ecc_preem_list.jsp", req, res);
			} 
			else 
			{
		    
				procActionECCPreemList(user, req, res, session);
			} 
		} finally {
			if (mp != null)
				mp.close();
		}
	}	

	
	protected void procActionECCPreemDetailtoExcel(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
	throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try { 
			mp = getMessageProcessor("ECC1501", req);
			ECC150103Message msgList = (ECC150103Message) mp.getMessageRecord("ECC150103", user.getH01USR(), "0001");
			
			if (req.getParameter("codigo_lista")!= null){
				JBObjList list = (JBObjList)session.getAttribute("PreemHeader");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ECC150101Message listMessage = (ECC150101Message)list.get(index);
				
				msgList.setIDCARGA(listMessage.getE01CCHIDC());
				
				req.setAttribute("codigo_lista", index);
		
				session.setAttribute("ECCSeleccion", listMessage);
			}
			
			if(req.getParameter("opt")!=null)
				req.setAttribute("opt", req.getParameter("opt"));
			else
				req.setAttribute("opt","01");
			
			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) 
			{ // if there are errors go back to first page
				session.setAttribute("error", error);
				JBObjList PreemHeader = (JBObjList)session.getAttribute("PreemHeader");
				req.setAttribute("E01CCHSTS", req.getParameter("E01CCHSTS"));
				req.setAttribute("E01CCHFDD", req.getParameter("E01CCHFDD"));
				req.setAttribute("E01CCHFDM", req.getParameter("E01CCHFDM"));
				req.setAttribute("E01CCHFDY", req.getParameter("E01CCHFDY"));
				req.setAttribute("E01CCHFHD", req.getParameter("E01CCHFHD"));
				req.setAttribute("E01CCHFHM", req.getParameter("E01CCHFHM"));
				req.setAttribute("E01CCHFHY", req.getParameter("E01CCHFHY"));
				req.setAttribute("E01CCHIDC", req.getParameter("E01CCHIDC"));
				req.setAttribute("E01CCHREC", req.getParameter("E01CCHREC"));
				
				forward("ECC1501_ecc_preem_list.jsp", req, res);
			} 
			else 
			{
		    
				JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");
		        if (!list.isEmpty())
		        {
		   
		        	ECC150103Message msg = (ECC150103Message) list.get(0);
					String fileName =msg.getIDCARGA()+"_"+getTimeStamp() + ".xls";
					String excelName = "attachment; filename=\"" + fileName + "\"";
					res.setContentType("application/vnd.ms-excel");			
					res.setHeader("Content-disposition", excelName);
					
					Vector fields = new Vector();
					Enumeration enu = msg.fieldEnumeration();
					while (enu.hasMoreElements()) {
						MessageField field = (MessageField) enu.nextElement();
						String name = field.getTag();
						if ("H03TIMSYS".equals(name) ||  "H03SCRCOD".equals(name) ||
							"H03OPECOD".equals(name) ||  "H03FLGMAS".equals(name) ||	
							"H03FLGWK1".equals(name) ||  "H03FLGWK2".equals(name) ||
							"H03FLGWK3".equals(name) ||  "H03USERID".equals(name) ||
							"H03PROGRM".equals(name)
						   ) 
							continue;
						ExcelColStyle style = new ExcelColStyle();
						style.setName(name);
						style.setHidden(false);
						style.setLocked(false);
						fields.add(style);
					}	
 
					
					OutputStream out = ExcelUtils.getWorkBook(res.getOutputStream(), fields, list, false);
					out.flush();
		 
		        }

			} 
		} finally {
			if (mp != null)
				mp.close();
		}
	}	
	
	
	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqECCRutEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: ECC1501_ecc_preem_rut_enter.jsp");
			forward("ECC1501_ecc_preem_rut_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}	

	//**
	protected void procActionECCPreemListRutDetail(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			mp = getMessageProcessor("ECC1501", req);
			
			ECC150102Message msgList = (ECC150102Message) mp.getMessageRecord("ECC150102", user.getH01USR(), "0002");
			
			if (req.getParameter("E02CCDIDN")!= null)
			{
				req.setAttribute("E02CCDIDN", req.getParameter("E02CCDIDN"));
				msgList.setE02CCDIDN(req.getParameter("E02CCDIDN"));
			}
			else
			{
				req.setAttribute("E02CCDIDN", "0");
			    msgList.setE02CCDIDN("0");
			}	
			
			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { 
				session.setAttribute("error", error);
				flexLog("About to call Page: ECC1501_ecc_preem_rut_enter.jsp");
				forward("ECC1501_ecc_preem_rut_enter.jsp", req, res);
			}
			else
			{
				JBObjList list = mp.receiveMessageRecordList("H02FLGMAS");

        	    session.setAttribute("PreemDetalle", list);
     			forwardOnSuccess("ECC1501_ecc_list_rut_detail.jsp", req, res);
			}  

		} finally {
			if (mp != null)
				mp.close();
		}
	}

		
	protected void procActionECCPreemListRutDetailtoExcel(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
	throws ServletException, IOException {

		MessageProcessor mp1 = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			mp1 = getMessageProcessor("ECC1501", req);
			
			ECC150102Message msgList = (ECC150102Message) mp1.getMessageRecord("ECC150102", user.getH01USR(), "0002");
			
			if (req.getParameter("E02CCDIDN")!= null)
			{
				req.setAttribute("E02CCDIDN", req.getParameter("E02CCDIDN"));
				msgList.setE02CCDIDN(req.getParameter("E02CCDIDN"));
			}
			else
			{
				req.setAttribute("E02CCDIDN", "0");
			    msgList.setE02CCDIDN("0");
			}	
			
			//Sends message
			mp1.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp1.receiveMessageRecord();	
	
			if (mp1.hasError(error)) 
			{ // if there are errors go back to first page
				session.setAttribute("error", error);
				JBObjList PreemHeader = (JBObjList)session.getAttribute("PreemHeader");
				forward("ECC1501_ecc_list_rut_detail.jsp", req, res);
			} 
			else 
			{
				JBObjList list = mp1.receiveMessageRecordList("H02FLGMAS");
		        if (!list.isEmpty())
		        {
		   
		        	ECC150102Message msg = (ECC150102Message) list.get(0);
					String fileName =msg.getE02CCDIDN()+"_"+getTimeStamp() + ".xls";
					String excelName = "attachment; filename=\"" + fileName + "\"";
					res.setContentType("application/vnd.ms-excel");			
					res.setHeader("Content-disposition", excelName);
					
					Vector fields = new Vector();
					Enumeration enu = msg.fieldEnumeration();
					while (enu.hasMoreElements()) {
						MessageField field = (MessageField) enu.nextElement();
						String name = field.getTag();
						if ("H02TIMSYS".equals(name) ||  "H02SCRCOD".equals(name) ||
							"H02OPECOD".equals(name) ||  "H02FLGMAS".equals(name) ||	
							"H02FLGWK1".equals(name) ||  "H02FLGWK2".equals(name) ||
							"H02FLGWK3".equals(name) ||  "H02USERID".equals(name) ||
							"H02PROGRM".equals(name)
						   ) 
							continue;
						ExcelColStyle style = new ExcelColStyle();
						style.setName(name);
						style.setHidden(false);
						style.setLocked(false);
						fields.add(style);
					}	
 
					
					OutputStream out = ExcelUtils.getWorkBook(res.getOutputStream(), fields, list, false);
					out.flush();
		 
		        }

			} 
		} finally {
			if (mp1 != null)
				mp1.close();
		}
	}	
	//END
}
	



