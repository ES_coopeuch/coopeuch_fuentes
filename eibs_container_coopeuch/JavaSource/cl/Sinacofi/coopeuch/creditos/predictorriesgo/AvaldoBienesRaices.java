/**
 * AvaldoBienesRaices.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class AvaldoBienesRaices  implements java.io.Serializable {
    private java.math.BigInteger id;  // attribute
    private java.math.BigInteger idConsulta;
    private java.math.BigInteger manzana;
    private java.math.BigInteger predio;
    private java.lang.String comuna;
    private java.lang.String direccion;
    private java.lang.String tipoPropiedad;
    private java.lang.String destino;
    private java.math.BigDecimal avaluo;

    public AvaldoBienesRaices() {
    }

    public java.math.BigInteger getId() {
        return id;
    }

    public void setId(java.math.BigInteger id) {
        this.id = id;
    }

    public java.math.BigInteger getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(java.math.BigInteger idConsulta) {
        this.idConsulta = idConsulta;
    }

    public java.math.BigInteger getManzana() {
        return manzana;
    }

    public void setManzana(java.math.BigInteger manzana) {
        this.manzana = manzana;
    }

    public java.math.BigInteger getPredio() {
        return predio;
    }

    public void setPredio(java.math.BigInteger predio) {
        this.predio = predio;
    }

    public java.lang.String getComuna() {
        return comuna;
    }

    public void setComuna(java.lang.String comuna) {
        this.comuna = comuna;
    }

    public java.lang.String getDireccion() {
        return direccion;
    }

    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }

    public java.lang.String getTipoPropiedad() {
        return tipoPropiedad;
    }

    public void setTipoPropiedad(java.lang.String tipoPropiedad) {
        this.tipoPropiedad = tipoPropiedad;
    }

    public java.lang.String getDestino() {
        return destino;
    }

    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }

    public java.math.BigDecimal getAvaluo() {
        return avaluo;
    }

    public void setAvaluo(java.math.BigDecimal avaluo) {
        this.avaluo = avaluo;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        AvaldoBienesRaices other = (AvaldoBienesRaices) obj;
        boolean _equals;
        _equals = true
            && ((this.comuna==null && other.getComuna()==null) || 
             (this.comuna!=null &&
              this.comuna.equals(other.getComuna())))
            && ((this.direccion==null && other.getDireccion()==null) || 
             (this.direccion!=null &&
              this.direccion.equals(other.getDireccion())))
            && ((this.tipoPropiedad==null && other.getTipoPropiedad()==null) || 
             (this.tipoPropiedad!=null &&
              this.tipoPropiedad.equals(other.getTipoPropiedad())))
            && ((this.destino==null && other.getDestino()==null) || 
             (this.destino!=null &&
              this.destino.equals(other.getDestino())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        AvaldoBienesRaices history = (AvaldoBienesRaices) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId())))
            && ((this.idConsulta==null && other.getIdConsulta()==null) || 
             (this.idConsulta!=null &&
              this.idConsulta.equals(other.getIdConsulta())))
            && ((this.manzana==null && other.getManzana()==null) || 
             (this.manzana!=null &&
              this.manzana.equals(other.getManzana())))
            && ((this.predio==null && other.getPredio()==null) || 
             (this.predio!=null &&
              this.predio.equals(other.getPredio())))
            && ((this.avaluo==null && other.getAvaluo()==null) || 
             (this.avaluo!=null &&
              this.avaluo.equals(other.getAvaluo())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        AvaldoBienesRaices history = (AvaldoBienesRaices) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getIdConsulta() != null) {
            _hashCode += getIdConsulta().hashCode();
        }
        if (getManzana() != null) {
            _hashCode += getManzana().hashCode();
        }
        if (getPredio() != null) {
            _hashCode += getPredio().hashCode();
        }
        if (getComuna() != null) {
            _hashCode += getComuna().hashCode();
        }
        if (getDireccion() != null) {
            _hashCode += getDireccion().hashCode();
        }
        if (getTipoPropiedad() != null) {
            _hashCode += getTipoPropiedad().hashCode();
        }
        if (getDestino() != null) {
            _hashCode += getDestino().hashCode();
        }
        if (getAvaluo() != null) {
            _hashCode += getAvaluo().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
