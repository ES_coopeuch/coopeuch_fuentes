<%//P�gina hecha por Alonso Arana ---Datapro ---- 24/10/2013 %>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ESG010501Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Listado de renuncia de Seguros</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ESG0010501List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>	
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>
<script type="text/javascript">

	      $(function(){
					
					$("#radio_key").attr("checked", false);
                
				});


function goAction(op) {
var ok = false;	
    for(n=0; n<document.forms[0].elements.length; n++)
     {
     	var elementName= document.forms[0].elements[n].name;
     	if(elementName == "key") 
     	{
			if (document.forms[0].elements[n].checked == true) {
      			ok = true;
      			break;
			}
     	}
     }  

			if(ok){
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();
			}
	
			 else 
				alert("Debe seleccionar una opci�n a retractar");	 
}
</script>

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Retracto de Seguros<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="ESG0105_insurance_ret_list.jsp"></h3>
<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESG0010">
<input type="hidden" name="SCREEN" value=""> 

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Nombre : 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="userPO" property="cusName" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">Identificaci�n :  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="ID" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>

<%
	if (ESG0010501List.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>
<table class="tbenter" width="100%">
	<tr>
		<td align="center"  width="15%">&nbsp;</td>
		<td align="center" class="tdbkg" width="15%">
			<a href="javascript:goAction('402')"> <b>Retractar</b> </a></td>
		<td align="center"  width="15%">&nbsp;</td>
		<td align="center" width="15%">&nbsp;</td>
		
		<td align="center" width="15%">&nbsp;</td>			
	</tr>
</table>



	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="10px"></th>
			<th align="left" nowrap >Cuenta</th>
			<th align="left" nowrap >Vigencia desde</th>
			<th align="left" nowrap >Estado Pr�stamo</th>
			<th align="left" nowrap >Compa�ia</th>
		    <th align="left" nowrap >Seguro</th>
		</tr>
		<%
			ESG0010501List.initRow();
				boolean firstTime = true;
				String cta = "";
			    String nbr = "";
			    int flg = 0; 
			
				String chk = "";
				while (ESG0010501List.getNextRow()) {
	
				ESG010501Message convObj = (ESG010501Message) ESG0010501List
							.getRecord();
		      
		      	if (firstTime) {
						firstTime = false;
						
						chk = "checked";
						cta = convObj.getE01SREPAC();
						nbr = convObj.getE01SRECIA();
		
					} else {
						chk = "";
					}
		 
		 if (!nbr.equals(convObj.getE01SRECIA()) || !cta.equals(convObj.getE01SREPAC())) {
               flg = 0; 
               cta = convObj.getE01SREPAC();
			   nbr = convObj.getE01SRECIA();
		 }
		        
		%>
		<tr>
			<td nowrap width="10px">
			<%if ( flg == 0) { %>
			   <input type="radio" name="key" id="radio_key" 
				   Value="<%=ESG0010501List.getCurrentRow()%>" 
				   onclick="" <%=chk%>>
			<% } %>
			</td>
			<td nowrap align="left">
			 <%if ( flg == 0) { %>
			   <input type="hidden" name="cuenta_sel" 
			          value="<%=convObj.getE01SREPAC()%>">
			  
			                 <%=Util.formatCell(convObj.getE01SREPAC())%>
				<input type="hidden" name="MontoCr�dito" value="<%=convObj.getE01SREOAM()%>">
				<input type="hidden" name="descripcion_cuenta" value="<%=convObj.getE01SREDPA()%>">
			<% } %>                 
			</td>
			<td nowrap align="center">
			<%if ( flg == 0) { %>
			<%=Util.formatCell(convObj.getE01SRESDD() + "/"	+ convObj.getE01SRESDM() + "/" + convObj.getE01SRESDY()) %>
			    <input type="hidden" name="fecha_vigencia" value="<%=Util.formatCell(convObj.getE01SRESDD() + "/"	+ convObj.getE01SRESDM() + "/" + convObj.getE01SRESDY()) %>">
			<% } %> 
			</td>
			
			<td nowrap align="left">
			<%if ( flg == 0) { %>
			   <%=Util.formatCell(convObj.getE01SRESTD())%>
			<% } %>     
			</td>
			
			<td nowrap align="left">
			<%if ( flg == 0) { %>
			<%=Util.formatCell(convObj.getE01SRENM1())%>
			<input type="hidden" name="nombre_compania" value="<%=convObj.getE01SRENM1()%>">
	  	    <input type="hidden" name="codigo_compania" value="<%=convObj.getE01SREBID()%>">
	  	    <input type="hidden" name="rut_compania" value="<%=convObj.getE01SRECIA()%>">	
	  	    
	  	    	
			<% } %> 
			    
			</td>
			
			
	<!--Seguros -->		
			
			<td nowrap align="left">
			   <%=Util.formatCell(convObj.getE01SREDSC())%>
			   <input type="hidden" name="monto_devolucion" value="<%=convObj.getE01SRESOC()%>">	
			   <input type="hidden" name="monto_prima" value="<%=convObj.getE01SREMTP()%>">
			   <input type="hidden" name="codigo_seguro" value="<%=convObj.getE01SRECOD()%>">
			   <input type="hidden" name="descripcion_seguro" value="<%=convObj.getE01SREDSC()%>">
		    </td>
		</tr>
		<% 
			if ( flg == 0) {
			flg = 1;
			}
		}
		%>
	</table>

<script type="text/javascript">
    
     
</script> <br>

<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (ESG0010501List.getShowPrev()) {
					int pos = ESG0010501List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSESG105?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (ESG0010501List.getShowNext()) {
					int pos = ESG0010501List.getLastRec();
					out
								.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSESG105?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%

}	
%>
</form>

</body>
</html>

