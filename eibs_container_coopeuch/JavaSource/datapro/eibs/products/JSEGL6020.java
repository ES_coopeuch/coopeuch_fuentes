package datapro.eibs.products;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.EGL601001Message;
import datapro.eibs.beans.EGL602001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV103801Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.Util;
import datapro.eibs.services.ExcelResultSet;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.services.ExcelXLSXResultSet;

public class JSEGL6020 extends JSEIBSServlet {

	private ServletConfig config = null;
	
	protected static final int A_ENTER_FILE = 1;	
	protected static final int R_ENTER_FILE_PD = 2;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
        flexLog("Entre con Screen.." + screen);   
        
        switch (screen) {
			// Request
			case R_ENTER_FILE_PD :
				procReqImport(user, req, res, session);
				break;
			case A_ENTER_FILE :
				procReadExcelFile(user, req, res, session);
				break;
			default :
				forward(devPage, req, res); 
				break;
		}
	}

	private void procReadExcelFile(
			ESS0030DSMessage user, 
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session) 
		throws ServletException, IOException {

		UserPos userPO = getUserPos(session);		
		ELEERRMessage msgError = new ELEERRMessage();
		MessageProcessor mp = null;
		String PageToCall = "";
		String StrBanco = "";
		String StrSucursal = "";
		String StrLote = "";
		
		double  Ddebito = 0;
		double Dcredito = 0;
		
		int rows = 0;
		//read file
		try {
			SmartUpload mySmartUpload = new SmartUpload();
			mySmartUpload.initialize(config, req, res);
			mySmartUpload.upload();
			
			mp = getMessageProcessor("EGL6020", req);
			EGL602001Message msg = (EGL602001Message) mp.getMessageRecord("EGL602001", user.getH01USR(), "0001");

			try {
				StrBanco = mySmartUpload.getRequest().getParameter("E01TRABNK").trim();
			} catch (Exception e) {
			}

			try {
				StrSucursal = mySmartUpload.getRequest().getParameter("E01TRABRN").trim();
			} catch (Exception e) {
			}

			try {
				StrLote = mySmartUpload.getRequest().getParameter("E01TRABTH").trim();
			} catch (Exception e) {
			}

			msg.setE01ORGBNK(StrBanco);		 	  
			msg.setE01ORGBRN(StrSucursal);	
			msg.setE01ORGBTH(StrLote);

			mp.sendMessage(msg);

			// Receive error and data
			msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EGL602001Message) mp.receiveMessageRecord();

			if (mp.hasError(msgError)) {
				PageToCall = "EGL6020_transfer_file.jsp";
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				forward(PageToCall, req, res);
			} 
			else 
			{

				com.jspsmart.upload.File file =  mySmartUpload.getFiles().getFile(0);
				InputStream xls = Util.getStreamFromObject(file);													
				ResultSet rs = null;
				
				if (ExcelUtils.isXLSXVersion(file.getFilePathName())) {
					rs = new ExcelXLSXResultSet(xls, 0);
					((ExcelXLSXResultSet)rs).init();
				} else {
					rs = new ExcelResultSet(xls, 0);
					((ExcelResultSet)rs).init();
				}
				
				msg = (EGL602001Message) mp.getMessageRecord("EGL602001", user.getH01USR(), "0002");
				while (rs.next()) {

					try {
						ExcelUtils.populate(rs, msg);
					} 
					catch (Exception e) {
						msgError.setERRNUM("2");
						msgError.setERNU01("0001");		//4		                
						msgError.setERDS01("Error en Record: " + (rows + 1));//70
						msgError.setERNU02("0002");		//4		                
						msgError.setERDS02(e.getMessage().trim().length() > 70 ? e.getMessage().substring(0, 69) : e.getMessage().trim());//70
						session.setAttribute("error", msgError);						

						PageToCall = "EGL6020_transfer_file.jsp";

						session.setAttribute("error", msgError);
						session.setAttribute("userPO", userPO);
						break;
					}
					
					msg.setE01ORGBNK(StrBanco);		    	  
					msg.setE01ORGBRN(StrSucursal);	 
					msg.setE01ORGBTH(StrLote);

					Ddebito = Ddebito + msg.getBigDecimalE01DEBITO().doubleValue();
					Dcredito = Dcredito + msg.getBigDecimalE01CREDITO().doubleValue();

					if(rows==0)
						msg.setH01FLGWK1("C"); 
					
					mp.sendMessage(msg);
					rows++;

					msgError = (ELEERRMessage) mp.receiveMessageRecord();
					msg = (EGL602001Message) mp.receiveMessageRecord();

					if (mp.hasError(msgError)) {
						PageToCall = "EGL6020_transfer_file.jsp";
						
						msgError.setERDS01("Reg : " + (rows + 1) + " - " + msgError.getERDS01());
						
						session.setAttribute("error", msgError);
						session.setAttribute("userPO", userPO);
						break;
					} 
				}

				if (!mp.hasError(msgError))
				{	
					
					msg.setE01ORGBNK(StrBanco);		 	  
					msg.setE01ORGBRN(StrSucursal);	
					msg.setE01ORGBTH(StrLote);

					msg.setH01FLGMAS("*");    
					
					BigDecimal BDebito = new BigDecimal(Ddebito);
					msg.setE01DEBITO(BDebito);
					
					BigDecimal BCredito = new BigDecimal(Dcredito);
					msg.setE01CREDITO(BCredito);

					if(BDebito.compareTo(BCredito)!=0)
						msg.setH01FLGWK2("E"); //Se env�a error para eliminar la carga.

					mp.sendMessage(msg);

					msgError = (ELEERRMessage) mp.receiveMessageRecord();
					msg = (EGL602001Message) mp.receiveMessageRecord();

					if (mp.hasError(msgError)) {
						PageToCall = "EGL6020_transfer_file.jsp";
						session.setAttribute("error", msgError);
						session.setAttribute("userPO", userPO);
						forward(PageToCall, req, res);
					} 
					else 
					{	
						req.setAttribute("rows", String.valueOf(rows));
						PageToCall = "EGL6020_transfer_file_confirm.jsp";
						session.setAttribute("userPO", userPO);
						forward(PageToCall, req, res);
					}		
				}
				else 
				{	
					msg.setE01ORGBNK(StrBanco);		 	  
					msg.setE01ORGBRN(StrSucursal);	
					msg.setE01ORGBTH(StrLote);

					msg.setH01FLGMAS("*");    
					msg.setH01FLGWK2("E"); //Se env�a error para eliminar la carga.
					mp.sendMessage(msg);

					msgError = (ELEERRMessage) mp.receiveMessageRecord();
					msg = (EGL602001Message) mp.receiveMessageRecord();
					
					forward(PageToCall, req, res);
				}
			}

		} catch (Exception e) {
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
	private void procReqImport(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session  
			)
		throws ServletException, IOException {
		UserPos userPO = getUserPos(session);

		ELEERRMessage msgError = new ELEERRMessage(); 
	
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		
		forward("EGL6020_transfer_file.jsp", req, res);
	}

}
