<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ECC150101Message,datapro.eibs.beans.ECC150102Message"%>
<%@ page import="com.datapro.constants.EibsFields"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html> 
<head>
<title>Carga Preemisiones Tarjeta de Cr�dito</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="PreemDetalle" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "ECCSeleccion" class= "datapro.eibs.beans.ECC150101Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript"> 
  function goAction(op) 
   {
	  if (op =='1200')	
 	 {
 	 	document.forms[0].SCREEN.value = op;
		document.forms[0].action = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC1501?SCREEN=1200&E01CCHFDD=<%=request.getAttribute("E01CCHFDD")%>&E01CCHFDM=<%=request.getAttribute("E01CCHFDM")%>&E01CCHFDY=<%=request.getAttribute("E01CCHFDY")%>&E01CCHFHD=<%=request.getAttribute("E01CCHFHD")%>&E01CCHFHM=<%=request.getAttribute("E01CCHFHM")%>&E01CCHFHY=<%=request.getAttribute("E01CCHFHY")%>&E01CCHIDC=<%=request.getAttribute("E01CCHIDC")%>&E01CCHREC=<%=request.getAttribute("E01CCHREC")%>";
	 	document.forms[0].submit();
	 }
   }
</SCRIPT>  

</head>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>
<h3 align="center">Consulta Detalle Preemisiones Tarjeta de Cr�dito<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ECC_List_detail.jsp, ECC1501"></h3>
<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC1501">
<input type="hidden" name="SCREEN" value="">

<INPUT TYPE=HIDDEN NAME="opt" VALUE="<%=request.getAttribute("opt")%>">
<input type="hidden" name="codigo_lista" value="<%=request.getAttribute("codigo_lista") %>" id="codigo_lista"> 
<input type="hidden" name="E01CCHSTS" value="<%=request.getAttribute("E01CCHSTS")%>">

<input type="hidden" name="E01CCHFDD" value="<%=request.getAttribute("E01CCHFDD") %>"> 
<input type="hidden" name="E01CCHFDM" value="<%=request.getAttribute("E01CCHFDM") %>"> 
<input type="hidden" name="E01CCHFDY" value="<%=request.getAttribute("E01CCHFDY") %>"> 

<input type="hidden" name="E01CCHFHD" value="<%=request.getAttribute("E01CCHFHD") %>"> 
<input type="hidden" name="E01CCHFHM" value="<%=request.getAttribute("E01CCHFHM") %>"> 
<input type="hidden" name="E01CCHFHY" value="<%=request.getAttribute("E01CCHFHY") %>"> 

<input type="hidden" name="E01CCHIDC" value="<%=request.getAttribute("E01CCHIDC") %>"> 
<input type="hidden" name="E01CCHREC" value="<%=request.getAttribute("E01CCHREC") %>"> 

 
   <table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">Id Interfaz : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="cartola" value="<%=ECCSeleccion.getE01CCHIDC() %>" 
	  			size="23"  readonly>
             </td>  
             
               <td nowrap width="10%" align="right">Fecha Carga : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCAA" size="23" maxlength="20" value="<% out.print(ECCSeleccion.getE01CCHCAD()+"/"+ECCSeleccion.getE01CCHCAM()+"/"+ECCSeleccion.getE01CCHCAY());%>" readonly>
             </td>  
         </tr>
         
          <tr>
             <td nowrap width="10%" align="right">Registros : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCTA" size="23" maxlength="20" value="<%=ECCSeleccion.getE01CCHCAR() %>" readonly>
             </td>
        <td nowrap width="10%" align="right">Reg Error : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCAA" size="23" maxlength="20" value="<%=ECCSeleccion.getE01CCHVAR() %>" readonly>
             </td>
             </tr>
      
           <tr>
             <td nowrap width="10%" align="right">Estado : 
               </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01fecha" size="23" maxlength="20" value="<%=ECCSeleccion.getE01CCHGST()%>" readonly>
             </td>
        <td nowrap width="10%" align="right">Fecha Estado : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha2" size="23" maxlength="20" value="<%out.print(ECCSeleccion.getE01CCHCAD()+"/"+ECCSeleccion.getE01CCHCAM()+"/"+ECCSeleccion.getE01CCHCAY());%>" readonly>
             </td>
             </tr>
        
        <tr>
             <td nowrap width="10%" align="right">Usuario : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha" size="23" maxlength="20" value="<%=ECCSeleccion.getE01CCHUPU()%>" readonly >
             </td>
        <td nowrap width="10%" align="right" colspan="2"> 
               </td>
             </tr>
        </table>
      </td>
    </tr>
  </table>
 
 

<%
	if (PreemDetalle.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>


<table class="tbenter" width=100% >
	<tr>
		<td height="20"></td>
	</tr>
	<tr>
		<td>
		<table id="headTable" width="100%" align="left">
			<tr id="trdark">
				<th align="center" nowrap width="5%">SEC</th>
				<th align="center" nowrap width="10%">RUT Socio</th>
				<th align="center" nowrap width="15%">Canal de Venta</th>
				<th align="center" nowrap width="10%">Cod Prod</th>
				<th align="center" nowrap width="15%">Cupo Nacional</th>
				<th align="center" nowrap width="15%">Oficina</th>
				<th align="center" nowrap width="10%">Vendedor</th>
				<th align="center" nowrap width="10%">Estado</th>
				<th align="center" nowrap width="20%">Causal Rechazo</th>
				<th align="center" nowrap width="111">Fecha Estado</th>
			</tr>
			<%
			PreemDetalle.initRow();
				int k = 0,inicial;
				boolean firstTime = true;
				String chk = "";
				while (PreemDetalle.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ECC150102Message PDetalle = (ECC150102Message) PreemDetalle.getRecord();
		%>
			<tr>
				<td nowrap align="center" height="28"><%=PDetalle.getE02CCDSEQ()%></td>
				<td nowrap align="right" height="28"><%=PDetalle.getE02CCDIDN()%></td>
				<td nowrap align="left" height="28"><%=PDetalle.getE02CCDGCA()%></td>
				<td nowrap align="center" height="28"><%=PDetalle.getE02CCDPRD()%></td>
				<td nowrap align="right" height="28"><%=Util.formatCCY(PDetalle.getE02CCDNAC())%></td>
				<td nowrap align="left" height="28"><%=PDetalle.getE02CCDGBR()%></td>
				<td nowrap align="right" height="28"><%=PDetalle.getE02CCDIDV()%></td>
				<td nowrap align="center" height="28"><%=PDetalle.getE02CCDGST()%></td>
				<td nowrap align="left" height="28"><%=PDetalle.getE02CCDGER()%></td>
				<td nowrap align="center" height="28">
				<% out.print(PDetalle.getE02CCDUPD()+"/"+PDetalle.getE02CCDUPM()+"/"+PDetalle.getE02CCDUPY());%>
				</td>
			</tr>
			<%}%>
		</table>
		</td>
	</tr>

	<tr>
	<td>
		<table class="tbenter" width="98%" align="center">
		<tr>
			<td width="40%" align="left">
			<%
			if (PreemDetalle.getShowPrev()) {
					int pos = PreemDetalle.getFirstRec() - 10;
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.products.JSECC1501?SCREEN=1300&posicion="
									+pos+"&codigo_lista=" + request.getAttribute("codigo_lista")
									+"&mark=atr�s"
									+"&E01CCHSTS="+request.getAttribute("E01CCHSTS")
									+"&E01CCHFDD="+request.getAttribute("E01CCHFDD")
									+"&E01CCHFDM="+request.getAttribute("E01CCHFDM")
									+"&E01CCHFDY="+request.getParameter("E01CCHFDY") 
									+"&E01CCHFHD="+request.getAttribute("E01CCHFHD")
									+"&E01CCHFHM="+request.getAttribute("E01CCHFHM")
									+"&E01CCHFHY="+request.getAttribute("E01CCHFHY")
									+"&E01CCHIDC="+request.getAttribute("E01CCHIDC")
									+"&E01CCHREC="+request.getAttribute("E01CCHREC")
									+"&opt="+request.getAttribute("opt")

									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
				%>
				</td>
				<td width="20%" align="center">
		    		<input id="EIBSBTN" type=button name="Submit" value="Atr�s" onClick="javascript:goAction('1200')">
 				</td>		
				<td width="40%" align="right">
				<%
				if (PreemDetalle.getShowNext()) {
					int pos = PreemDetalle.getLastRec();
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.products.JSECC1501?SCREEN=1300&posicion="
									+pos+"&codigo_lista=" + request.getAttribute("codigo_lista")
									+"&mark=atr�s"
									+"&E01CCHSTS="+request.getAttribute("E01CCHSTS")
									+"&E01CCHFDD="+request.getAttribute("E01CCHFDD")
									+"&E01CCHFDM="+request.getAttribute("E01CCHFDM")
									+"&E01CCHFDY="+request.getParameter("E01CCHFDY") 
									+"&E01CCHFHD="+request.getAttribute("E01CCHFHD")
									+"&E01CCHFHM="+request.getAttribute("E01CCHFHM")
									+"&E01CCHFHY="+request.getAttribute("E01CCHFHY")
									+"&E01CCHIDC="+request.getAttribute("E01CCHIDC")
									+"&E01CCHREC="+request.getAttribute("E01CCHREC")
									+"&opt="+request.getAttribute("opt")

									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/next_records.gif\" ></A>");
				}
				%>
				</td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<%
	}
%>
	<SCRIPT language="JavaScript">
		showChecked("CURRCODE");
		
			function resizeDoc() {
      		 	divResize();
     		    adjustEquTables(headTable, dataTable, dataDiv1,1,false);
      		}
	 		resizeDoc();   			
     		window.onresize=resizeDoc;        
     </SCRIPT>


</form>
</body>
</html>
