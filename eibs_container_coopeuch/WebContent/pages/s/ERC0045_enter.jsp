<!-- Created by Nicolás Valeria ------Datapro----- 19/02/2015 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC200001Message"%>

<html>
<head>
<title>Sistema Bancario: Conciliación Bancaria</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="date" class="java.util.Date" scope="session" />      
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">
 function enterCode(){
	var diaDesde = document.forms[0].E01DESDDD.value;
	var mesDesde = document.forms[0].E01DESDDM.value;
	var anoDesde = document.forms[0].E01DESDDY.value;
	var diaHasta = document.forms[0].E01HASDDD.value;
	var mesHasta = document.forms[0].E01HASDDM.value;
	var anoHasta = document.forms[0].E01HASDDY.value;
	var fechaDesde = anoDesde + mesDesde + diaDesde;
	var fechaHasta = anoHasta + mesHasta + diaHasta;
	if (fechaDesde > fechaHasta){
		alert("La fecha desde no puede ser mayor a la fecha hasta");
		return false;
	}
	if (trim(document.forms[0].E01RCHRBK.value).length > 0) {
	    return true;
	}else{
		alert("Debe ingresar un Banco");
		document.forms[0].E01RCHRBK.focus();
		return false;
	}
 }
 
 
 

function addDate(){
	date = new Date();
	var month = date.getMonth()+1;
	if (month < 10){
		month = '0' + month;
	}
	var day = date.getDate();
	var year = date.getFullYear();
	document.getElementById('fecha1').value = day;
	document.getElementById('fecha2').value = month;
	document.getElementById('fecha3').value = year;
	document.getElementById('fecha4').value = day;
	document.getElementById('fecha5').value = month;
	document.getElementById('fecha6').value = year;
}
</script>
 
 

</head>

<body  onload="addDate();">
 
<H3 align="center">Conciliaci&oacute;n Cartola - Cartola<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="enter.jsp, ERC0045"></H3>

<hr size="4">
<br>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0045" onsubmit="return(enterCode());"  >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">
    <table width="100%" cellpadding="2" cellspacing="0" border="0" class="TABLEINFO" align="center">
    	<tbody style="width: 100%">
    		<tr style="width: 100%">
    			<td nowrap>
    				<table width="100%" cellpadding="2" cellspacing="0" border="0" class="TBHEAD" align="center">
    					<tbody>
    						<tr class="TRDARK">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Banco :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01RCHRBK" size="5" maxlength="4" value="">
			            			<a href="javascript:GetBankReconciliation('E01RCHRBK','E01DSCRBK','E01BRMCTA','E01BRMACC')">
			            				<img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0" >
			            			</a> 
	       							<input type="text" name="E01DSCRBK" readonly="readonly" size="43" maxlength="43" >
    							</td>
    							<td>
    								<div align="right">
    									Fecha Desde :
    								</div>
    							</td>
    							<td>
    								<input style="vertical-align: middle" type="text" name="E01DESDDD" id="fecha1" size="3" maxlength="2" value="" readonly>
					                <input style="vertical-align: middle" type="text" name="E01DESDDM" id="fecha2" size="3" maxlength="2" value="" readonly>
					                <input style="vertical-align: middle" type="text" name="E01DESDDY" id="fecha3" size="5" maxlength="4" value="" readonly>
					                <a href="javascript:DatePicker(document.forms[0].E01DESDDD,document.forms[0].E01DESDDM,document.forms[0].E01DESDDY)"><img style="vertical-align: middle" src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a>
    							</td>
    						</tr>
    						<tr class="TRCLEAR">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Cuenta Banco :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01BRMCTA" size="23" maxlength="20" value="">
    							</td>
    							<td>
    								<div align="right">
    									Fecha Hasta :
    								</div>
    							</td>
    							<td>
    								<input style="vertical-align: middle" type="text" name="E01HASDDD" size="3" id="fecha4" maxlength="2" value="" readonly>
					                <input style="vertical-align: middle" type="text" name="E01HASDDM" size="3" id="fecha5" maxlength="2" value="" readonly>
					                <input style="vertical-align: middle" type="text" name="E01HASDDY" size="5" id="fecha6" maxlength="4" value="" readonly>
					                <a href="javascript:DatePicker(document.forms[0].E01HASDDD,document.forms[0].E01HASDDM,document.forms[0].E01HASDDY)"><img style="vertical-align: middle" src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 
    							</td>
    						</tr>
    						<tr class="TRDARK">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Cuenta IBS :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01BRMACC" size="7" maxlength="7" value="">
    							</td>
    							<td>
    								<div align="right">
    									Opci&oacute;n :
    								</div>
    							</td>
    							<td>
    								<input type="radio" name="H01FLGWK3" id="DES_CON" checked="checked" value="C"> Conciliaci&oacute;n
    								<input type="radio" name="H01FLGWK3" id="DES_CON" value="D"> Desconciliaci&oacute;n
    							</td>
    						</tr>
    					</tbody>
    				</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    <br>
<p align="center">
	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
</p>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
