/**
 * NexusService.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * q0834.18 v82708152146
 */

package cl.coopeuch.core.nexus.nexus;

public interface NexusService extends javax.xml.rpc.Service {
    public cl.coopeuch.core.nexus.nexus.NexusPortType getNexusSOAP() throws javax.xml.rpc.ServiceException;

    public java.lang.String getNexusSOAPAddress();

    public cl.coopeuch.core.nexus.nexus.NexusPortType getNexusSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
