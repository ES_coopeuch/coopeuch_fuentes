package datapro.eibs.products;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;

import datapro.eibs.beans.EDD240001Message;
import datapro.eibs.beans.EDL005501Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERC004001Message;
import datapro.eibs.beans.ERC004003Message;
import datapro.eibs.beans.ERC004005Message;
import datapro.eibs.beans.ERC200001Message;
import datapro.eibs.beans.ERC200002Message;
import datapro.eibs.beans.ERC200003Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ETG000000Message;
import datapro.eibs.beans.ExcelColStyle;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.sockets.MessageField;

public class JSEDL0055 extends JSEIBSServlet {
	
	private static final long serialVersionUID = 1L;

	protected static final int R_INQ_FOG_INT = 100;
	protected static final int A_INQ_FOG_INT = 200;
	
	public JSEDL0055() {
	}

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
	
		case R_INQ_FOG_INT:
			procReqInqFogInt(user, req, res, session);
			break;
		
		case A_INQ_FOG_INT:
			procActInqFogInt(user, req, res, session);
			break;

		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	/*Objetivo : Presenta pantalla de Generacion Interfaz FOGAPE
	 *Autor : JLSP 
	 *Fecha : Marzo 2016 
	 */
	protected void procReqInqFogInt(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		EDL005501Message msgIntFog = null;
		ELEERRMessage msgError = null;
		
		UserPos userPO = (UserPos) ses.getAttribute("userPO"); 
		try {

			ses.setAttribute("error", msgError);
			ses.setAttribute("IntFog", msgIntFog);
			ses.setAttribute("userPO", userPO);
			forward("EDL0055_inq_fogape_interface.jsp", req, res);

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
		}
	}

	/*Objetivo : Presenta pantalla de Ejecucion Generacion Interfaz FOGAPE
	 *Autor : JLSP 
	 *Fecha : Marzo 2016 
	 */
	protected void procActInqFogInt(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
		UserPos userPO = (UserPos) ses.getAttribute("userPO"); 

		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDL0055", req);
			EDL005501Message msgIntFog = (EDL005501Message) mp.getMessageRecord("EDL005501", user.getH01USR(), "0001");
			msgIntFog.setH01TIMSYS(getTimeStamp());
			
			setMessageRecord(req, msgIntFog);
			
			mp.sendMessage(msgIntFog);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgIntFog = (EDL005501Message) mp.receiveMessageRecord("EDL005501");

			ses.setAttribute("IntFog", msgIntFog);
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

			if (mp.hasError(msgError)) {
				forward("EDL0055_inq_fogape_interface.jsp", req, res);
			} 
			else 
			{
				forward("EDL0055_proc_fogape_interface.jsp", req, res);
			}

		} finally {
			if (mp != null)	mp.close();
		}
	}
}
