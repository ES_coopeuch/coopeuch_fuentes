/**
 * DatosRespuestaSRut_Helper.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.consultaequifax;

public class DatosRespuestaSRut_Helper {
    // Type metadata
    private static com.ibm.ws.webservices.engine.description.TypeDesc typeDesc =
        new com.ibm.ws.webservices.engine.description.TypeDesc(DatosRespuestaSRut.class);

    static {
        com.ibm.ws.webservices.engine.description.FieldDesc field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("score");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "score"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("glosa");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "glosa"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("usu_co_sexo");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usu_co_sexo"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("usu_co_renta");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usu_co_renta"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("usu_co_niveleduc");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usu_co_niveleduc"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("usu_co_edad");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usu_co_edad"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("usu_co_diffsocio");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usu_co_diffsocio"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("usu_co_antig_lab");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usu_co_antig_lab"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("rellcnup12");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "rellcnup12"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("relconsp_3");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "relconsp_3"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("mesddamo12");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "mesddamo12"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("efx_ratio_leverage");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "efx_ratio_leverage"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("efx_antes_lag_apariciones");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "efx_antes_lag_apariciones"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("dic_totdocs_02");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "dic_totdocs_02"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("dic_totcons3_38");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "dic_totcons3_38"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("dic_totavaluo_32");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "dic_totavaluo_32"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("dic_totacreed_12");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "dic_totacreed_12"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("dic_tot_clqveh_10");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "dic_tot_clqveh_10"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("dic_r04_creds_m01_16");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "dic_r04_creds_m01_16"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("dic_nrodirecciones_18");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "dic_nrodirecciones_18"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("baja_dda_cons_3M");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "baja_dda_cons_3m"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("estadoServicio");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "estadoServicio"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("timeOut");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "timeOut"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("baseProactiva");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "baseProactiva"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("error");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "error"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://consultaequifax.cl", "error"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static com.ibm.ws.webservices.engine.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new DatosRespuestaSRut_Ser(
            javaType, xmlType, typeDesc);
    };

    /**
     * Get Custom Deserializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new DatosRespuestaSRut_Deser(
            javaType, xmlType, typeDesc);
    };

}
