package datapro.eibs.client;

/**
 * Insert the type's description here.
 * Creation date: (1/19/00 6:08:55 PM)
 * @author: Orestes Garcia
 */
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfTable;
import com.lowagie.text.pdf.PdfWriter;

import datapro.eibs.beans.ECIF21501Message;
import datapro.eibs.beans.ECIF30001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

public class JSECIF215 extends datapro.eibs.master.SuperServlet  {

	// CIF options
	protected static final int R_SEARCH = 1;
	protected static final int A_LIST = 4;
	// entering options
	protected static final int R_ENTER_CUS			= 100;
	protected static final int A_ENTER_CUS			= 200;

	protected String LangPath = "S";

	/**
	 * JSECLI001 constructor comment.
	 */
	public JSECIF215() {
		super();
	}

	/**
	 * This method was created by .
	 */
	public void destroy() {

		flexLog("free resources used by JSECIF215");

	}

	/**
	 * This method was created by Orestes Garcia.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/**
	 * This method was created .
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 */
	protected void procActionCustomerList(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses) {

		UserPos userPO = null;

		try {
			int option = Integer.parseInt(req.getParameter("opt"));

			switch (option) {
			case 10: // Consulta Saldos Deuda consolidada PDF.
				procReqPdfCustomerSaldDeudCons(mc, user, req, res, ses);
				break;
			case 20: // Consulta Saldos Deuda consolidada.
				procReqCustomerSaldDeudCons(mc, user, req, res, ses);
				break;
			default:
				res.sendRedirect(super.srctx + LangPath
						+ "Under_construction.jsp");
			}

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	protected void procReqCustomerSaldDeudCons(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ECIF21501Message msgData = null;
		ECIF30001Message msgDataSocio = null;		
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		String clientNum = "";

		try {

			userPO = (UserPos) ses.getAttribute("userPO");

			if (req.getParameter("E01CUN")!=null) {
				clientNum = req.getParameter("E01CUN");
			}
			else if (userPO.getCusNum().length() > 0)
				clientNum = userPO.getCusNum();
			else
				throw new Exception("Client number missing");
			
			userPO.setCusNum(clientNum);
			ses.setAttribute("userPO", userPO);

			msgError = new datapro.eibs.beans.ELEERRMessage();
			
			if ("S".equals(req.getParameter("VS"))){//VIENE de VISADO SENIOR
				req.setAttribute("VS", "S");
			} 
			
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		// Send Initial data
		try {
			msgData = (ECIF21501Message) mc.getMessageRecord("ECIF21501");
			msgData.setH01USR(user.getH01USR());
			msgData.setH01PGM("ECIF215");
			msgData.setH01TIM(getTimeStamp());
			msgData.setH01WK1(userPO.getHeader8());
			msgData.setH01SCR("01");
			msgData.setH01OPE("0001");

			msgData.setE01CUN(clientNum);

			msgData.send();
			msgData.destroy();

			flexLog("ECIF21501 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}


		// Receive Data
		try {

			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError = new datapro.eibs.beans.ELEERRMessage();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;

			}
			
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ECIF21501")) {

				try {
					msgData = new datapro.eibs.beans.ECIF21501Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgData = (ECIF21501Message) newmessage;
			}
			
		

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				if (!msgError.getERRNUM().equals("0")) {
					try {
						flexLog("About to call Page: " + LangPath + "ECIF010_cif_client_search.jsp");
						callPage(LangPath + "ECIF010_cif_client_search.jsp",req, res);

					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
				
				else {
					
					//consultamos  Indicadores del socio
					try {
						msgDataSocio = (ECIF30001Message) mc.getMessageRecord("ECIF30001");
						msgDataSocio.setH01USR(user.getH01USR());
						msgDataSocio.setH01PGM("ECIF300");
						msgDataSocio.setH01TIM(getTimeStamp());	
						msgDataSocio.setH01OPE("0001");						
						msgDataSocio.setE01CUN(msgData.getE01CUN());
						msgDataSocio.send();						
						msgDataSocio.destroy();

						flexLog("ECIF30001 Message Sent");
					} catch (Exception e) {
						e.printStackTrace();
						flexLog("Error: " + e);
						throw new RuntimeException("Socket Communication Error");
					}
					
					newmessage = mc.receiveMessage();
					if (newmessage.getFormatName().equals("ELEERR")) {

						try {
							msgError = new datapro.eibs.beans.ELEERRMessage();
						} catch (Exception ex) {
							flexLog("Error: " + ex);
						}

						msgError = (ELEERRMessage) newmessage;

					}
					
					newmessage = mc.receiveMessage();
					if (newmessage.getFormatName().equals("ECIF30001")) {

						try {
							msgDataSocio = new datapro.eibs.beans.ECIF30001Message();
						} catch (Exception ex) {
							flexLog("Error: " + ex);
						}

						msgDataSocio = (ECIF30001Message) newmessage;
					}					
				
					if (!msgError.getERRNUM().equals("0")) {
						try {
							ses.setAttribute("error", msgError);							
							flexLog("About to call Page: " + LangPath + "ECIF215_cif_client_search_identification.jsp");
							callPage(LangPath + "ECIF215_cif_client_search_identification.jsp", req, res);

						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}					
					else {
						flexLog("Putting java beans into the session");
						ses.setAttribute("error", msgError);
						ses.setAttribute("cifData", msgData);
						ses.setAttribute("cifDataSocio", msgDataSocio);
						try {
							flexLog("About to call Page: " + LangPath
									+ "ECIF215_cif_saldos_deuda_consolidada.jsp");
							callPage(LangPath
									+ "ECIF215_cif_saldos_deuda_consolidada.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}
	
	
	protected void procReqPdfCustomerSaldDeudCons(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ECIF21501Message msgData = null;
		ECIF30001Message msgDataSocio = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		String clientNum = "";
		String clientRut = "";
		String clientType = "";

		try {

			userPO = (UserPos) ses.getAttribute("userPO");
			
			userPO.setCusNum(clientNum);
			ses.setAttribute("userPO", userPO);

			req.getParameter("CUN");
			if (req.getParameter("CUN")!=null) {
				clientRut = req.getParameter("CUN");
			}
			else
				throw new Exception("Client Identify missing");
			if (req.getParameter("ID") != null){
				clientNum = req.getParameter("ID"); 
			}else
				throw new Exception("Client Identify missing"); 

			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		// Send Initial data
		try {
			msgData = (ECIF21501Message) mc.getMessageRecord("ECIF21501");
			msgData.setH01USR(user.getH01USR());
			msgData.setH01PGM("ECIF215");
			msgData.setH01TIM(getTimeStamp());
			msgData.setH01WK1(userPO.getHeader8());
			msgData.setH01SCR("01");
			if (clientNum.equals("0")){
				msgData.setH01OPE("0002");
				msgData.setE01RUT(clientRut);
				msgData.setE01TID("RUT");
				msgData.setE01PID("CL");
			}else {
				msgData.setH01OPE("0001");
				msgData.setE01CUN(clientNum);
			}
			

			msgData.send();
			msgData.destroy();

			flexLog("ECIF21501 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
		// Receive Data
		try {

			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError = new datapro.eibs.beans.ELEERRMessage();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;

			}
			
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ECIF21501")) {

				try {
					msgData = new datapro.eibs.beans.ECIF21501Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgData = (ECIF21501Message) newmessage;
			}
			
				if (!msgError.getERRNUM().equals("0")) {
					try {
						ses.setAttribute("error", msgError);						
						flexLog("About to call Page: " + LangPath + "ECIF215_cif_client_search_identification.jsp");
						callPage(LangPath + "ECIF215_cif_client_search_identification.jsp", req, res);

					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
				
				else {
					if (!msgData.getE01CUN().equals("0")){
						try {
							msgDataSocio = (ECIF30001Message) mc.getMessageRecord("ECIF30001");
							msgDataSocio.setH01USR(user.getH01USR());
							msgDataSocio.setH01PGM("ECIF300");
							msgDataSocio.setH01TIM(getTimeStamp());
							msgDataSocio.setH01OPE("0001");
							msgDataSocio.setE01CUN(msgData.getE01CUN());
							msgDataSocio.send();
							msgDataSocio.destroy();

							flexLog("ECIF30001 Message Sent");
						} catch (Exception e) {
							e.printStackTrace();
							flexLog("Error: " + e);
							throw new RuntimeException("Socket Communication Error");
						}

						newmessage = mc.receiveMessage();
						if (newmessage.getFormatName().equals("ELEERR")) {

							try {
								msgError = new datapro.eibs.beans.ELEERRMessage();
							} catch (Exception ex) {
								flexLog("Error: " + ex);
							}

							msgError = (ELEERRMessage) newmessage;

						}

						newmessage = mc.receiveMessage();
						if (newmessage.getFormatName().equals("ECIF30001")) {

							try {
								msgDataSocio = new datapro.eibs.beans.ECIF30001Message();
							} catch (Exception ex) {
								flexLog("Error: " + ex);
							}

							msgDataSocio = (ECIF30001Message) newmessage;
						}

						if (!msgError.getERRNUM().equals("0")) {
							try {
								ses.setAttribute("error", msgError);
								flexLog("About to call Page: "
										+ LangPath
										+ "ECIF215_cif_client_search_identification.jsp");
								callPage(
										LangPath
												+ "ECIF215_cif_client_search_identification.jsp",
										req, res);

							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						}
					}else {
						msgDataSocio = new ECIF30001Message();
						msgDataSocio.setE01CSG("");
						msgDataSocio.setE01CS3("");
						msgDataSocio.setE01CS2("");
						msgDataSocio.setE01CS4("");
					}
					
						
						Document doc = new Document(PageSize.LETTER, 36, 36, 36, 36);
						
						ByteArrayOutputStream baosPDF = new ByteArrayOutputStream();
						PdfWriter docWriter = null;
						try {
							docWriter = PdfWriter.getInstance(doc, baosPDF);
							Font normalFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL);
							Font headerBoldFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD);
							Font negrita = FontFactory.getFont(FontFactory.HELVETICA,10,Font.BOLD);
							headerBoldFont.setColor(Color.red);
							Paragraph TITLE = new Paragraph("Saldos y Deuda Consolidada (M$) al " + msgData.getE01FRF(), headerBoldFont);
							Paragraph BLANK = new Paragraph("", headerBoldFont);
							HeaderFooter header = new HeaderFooter(TITLE, false);
							header.setBorder(Rectangle.NO_BORDER);
							header.setAlignment(Element.ALIGN_CENTER);
							doc.setHeader(header);
							HeaderFooter footer = new HeaderFooter(new Phrase("Numero de P�gina: "), true);
							footer.setBorder(Rectangle.TOP);
							footer.setAlignment(Element.ALIGN_RIGHT);
							doc.setFooter(footer);

							doc.open();
							
							Table table = new Table(9, 3);
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							String titulo = "Informaci�n del Cliente ";
							if (msgData.getE01FL5().equals("3")) titulo+="   LISTA MAYOR RIESGO";
					 		else if (msgData.getE01FL5().equals("6")) titulo+="   PEP";
					 		else titulo+="     ";
							
							Cell cell = new Cell(new Paragraph(titulo, headerBoldFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_TOP);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(9);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("N�mero Cliente", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01CUN(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Nombre Cliente", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01NOM(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(6);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							String estado = "";
							 if (msgData.getE01EST().equals("1")) { estado+="Inactivo";}
				 	  	       else if (msgData.getE01EST().equals("2")) { estado+="Activo";  }
						       else if (msgData.getE01EST().equals("4")) { estado+="Fallecido"; }
						       else { estado+="";}
							
							cell = new Cell(new Paragraph(estado, normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Rut", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01RUT(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Usuario", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getH01USR(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(4);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Num Convenio", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01CVN(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Convenio", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01DSC(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(6);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("F. Socio", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FSC(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Oficina", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01OFC(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Fecha", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getH01TIM().substring(4,6) + "/" + msgData.getH01TIM().substring(2,4) + "/" + msgData.getH01TIM().substring(0,2), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(4);
							table.addCell(cell);
											
							doc.add(table);							
							
							table = new Table(1, 1);
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							cell = new Cell(new Paragraph("Indicadores Socio", headerBoldFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(0);
							table.addCell(cell);
							
							doc.add(table);
							
							table = new Table(4, 2);
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							cell = new Cell(new Paragraph("Calidad del Socio", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Comportamiento Pago Directo", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Comportamiento Pago Planilla", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Culpa", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							//
							cell = new Cell(new Paragraph(msgDataSocio.getE01CSG(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgDataSocio.getE01CS3(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgDataSocio.getE01CS2(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgDataSocio.getE01CS4(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							doc.add(table);
							doc.add(new Paragraph(" "));
							
							table = new Table(1, 1);
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							cell = new Cell(new Paragraph("Deuda Coopeuch", headerBoldFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(0);
							table.addCell(cell);
							
							doc.add(table);
							
							table = new Table(8, 50); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							cell = new Cell(new Paragraph("Deuda Directa x Mora", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							

							//Al D�a DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Al D�a", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getECDADIA07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							

							
							//Mora 30-90 ds DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 30-90 ds", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR101(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR102(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR103(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR104(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR105(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR106(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR107(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//Mora 90 a 180 ds DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 90 a 180 ds", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR201(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR202(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR203(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR204(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR205(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR206(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR207(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//Mora 180 ds a 3 a�os DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 180 ds a 3 a�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR301(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR302(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR303(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR304(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR305(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR306(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getECDMOR307(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							

							cell = new Cell(new Paragraph("Mora > 3 a�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR401(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR402(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR403(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR404(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR405(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR406(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDMOR407(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//TOTAL DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							

							////
							cell = new Cell(new Paragraph(".", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							

							//Al D�a DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Al D�a", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getECDADIA10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);	
							
							cell = new Cell(new Paragraph(msgData.getECDADIA13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDADIA14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							
							//Mora 30-90 ds DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 30-90 ds", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDMOR108(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR109(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR110(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR111(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR112(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getECDMOR113(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR114(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//Mora 90 a 180 ds DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 90 a 180 ds", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDMOR208(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR209(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR210(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR211(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR212(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDMOR213(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR214(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//Mora 180 ds a 3 a�os DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 180 ds a 3 a�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDMOR308(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR309(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR310(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR311(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR312(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDMOR313(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR314(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							

							cell = new Cell(new Paragraph("Mora > 3 a�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR408(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR409(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR410(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR411(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR412(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDMOR413(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDMOR414(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							

							//TOTAL DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDTOTL14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							////
							doc.add(table);
							doc.newPage();
							
							
							table = new Table(8, 50); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);

							
							//DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Deuda Indirecta x Mora", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//Al D�a DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Al D�a", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//Mora 30 ds a 3 A�os DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 30 ds a 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR101(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR102(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR103(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR104(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR105(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR106(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR107(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//Mora > 3 A�os DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Mora > 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR201(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR202(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR203(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR204(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR205(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR206(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getECIMOR207(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//TOTAL DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getECITOTL07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							

							////
							//DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph(".", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//Al D�a DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Al D�a", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIADIA14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//Mora 30 ds a 3 A�os DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 30 ds a 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECIMOR108(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR109(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR110(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR111(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR112(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR113(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR114(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//Mora > 3 A�os DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Mora > 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR208(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR209(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR210(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR211(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR212(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getECIMOR213(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECIMOR214(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECITOTL08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECITOTL14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							////

							//DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Deuda Directa x Tipo", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//CONSUMO DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Consumo", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//COMERCIAL DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Comercial", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							

							//HIPOTECARIO DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Hipotecario Vivienda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//TOTAL DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							

							////
							//DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph(".", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//CONSUMO DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Consumo", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDPCON08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCON14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//COMERCIAL DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Comercial", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPCOM14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//HIPOTECARIO DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Hipotecario Vivienda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPHIP14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getECDPTOT14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							////

							doc.add(table);
							doc.newPage();
							
							
							table = new Table(1, 1); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							//SALTO
							cell = new Cell(new Paragraph("Sistema Financiero", headerBoldFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(0);
							
							table.addCell(cell);
							
							doc.add(table);
							
							//TABLA SISTEMA FINANCIERO
							table = new Table(8, 27); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							cell = new Cell(new Paragraph("Deuda Directa x Mora", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell); 
							
							
							//AL DIA DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Al d�a", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//MORA 30-90 ds DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 30-90 ds", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR101(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR102(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR103(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR104(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR105(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR106(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getEDDMOR107(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//MORA 90 A 180 ds DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 90 a 180 ds", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR201(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR202(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR203(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR204(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR205(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR206(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR207(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//MORA 180 ds a 3 A�OS DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 180 ds a 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR301(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR302(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR303(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR304(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR305(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR306(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR307(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//MORA > 3 A�OS DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora > 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR401(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR402(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR403(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR404(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR405(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR406(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR407(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							////
							cell = new Cell(new Paragraph(" . ", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//AL DIA DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Al d�a", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDADIA14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//MORA 30-90 ds DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 30-90 ds", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR108(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR109(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR110(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR111(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR112(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR113(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getEDDMOR114(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//MORA 90 A 180 ds DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 90 a 180 ds", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR208(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR209(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR210(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR211(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR212(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR213(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR214(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//MORA 180 ds a 3 A�OS DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 180 ds a 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR308(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR309(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR310(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR311(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR312(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR313(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR314(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//MORA > 3 A�OS DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Mora > 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR408(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR409(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR410(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR411(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR412(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR413(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDMOR414(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//TOTAL DEUDA DIRECTA X MORA
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDTOTL14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							////
							
							//DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Deuda Indirecta x Mora", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//Al D�A DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Al D�a", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getEDIADIA07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//MORA 30 DS A 3 A�OS DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 30 ds a 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR101(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR102(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR103(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR104(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR105(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR106(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR107(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//MORA > 3 A�OS DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Mora > 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR201(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR202(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR203(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR204(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR205(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR206(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR207(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							////
					
							//DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph(" . ", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//Al D�A DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Al D�a", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIADIA13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getEDIADIA14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							//MORA 30 DS A 3 A�OS DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Mora 30 ds a 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR108(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR109(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR110(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR111(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR112(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR113(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR114(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//MORA > 3 A�OS DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Mora > 3 A�os", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR208(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR209(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR210(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR211(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR212(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR213(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDIMOR214(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL DEUDA INDIRECTA X MORA
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDITOTL14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							////
							
							doc.add(table);
							doc.newPage();
							
							//TABLA SISTEMA FINANCIERO
							table = new Table(8, 27); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							//DEUDA DIRECTA POR TIPO
							cell = new Cell(new Paragraph("Deuda Directa x Tipo", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//Nro. Int. c/Consumo DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Nro. Int. c/Consumo", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
																					
							//Consumo DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Consumo", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//Comercial DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Comercial", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//HIPOTECARIO VIVIENDA DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Hipotecario Vivienda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							////
							//DEUDA DIRECTA POR TIPO
							cell = new Cell(new Paragraph(".", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell); 
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//Nro. Int. c/Consumo DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Nro. Int. c/Consumo", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPNIN14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
																					
							//Consumo DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Consumo", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCON14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//Comercial DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Comercial", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPCOM14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//HIPOTECARIO VIVIENDA DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Hipotecario Vivienda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPHIP14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL DEUDA DIRECTA X TIPO
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEDDPTOT14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							////

							//OTRAS DEUDAS DIRECTAS
							cell = new Cell(new Paragraph("Otras Deudas Directas", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//DEUDA ADQUIRIDA OTRAS DEUDAS DIRECTA
							cell = new Cell(new Paragraph("Deuda Adquirida", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//OP. FINANCIERAS OTRAS DEUDAS DIRECTA
							cell = new Cell(new Paragraph("Op. Financieras", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//PREST. CONTINGENTES OTRAS DEUDAS DIRECTA
							cell = new Cell(new Paragraph("Prest. Contingentes", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//LEASING OTRAS DEUDAS DIRECTA
							cell = new Cell(new Paragraph("Leasing", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getEODDLSG07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//TOTAL OTRAS DEUDAS DIRECTAS
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							cell = new Cell(new Paragraph(msgData.getEODDTOT07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell); 

							////
							//OTRAS DEUDAS DIRECTAS
							cell = new Cell(new Paragraph(".", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//DEUDA ADQUIRIDA OTRAS DEUDAS DIRECTA
							cell = new Cell(new Paragraph("Deuda Adquirida", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDADQ14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//OP. FINANCIERAS OTRAS DEUDAS DIRECTA
							cell = new Cell(new Paragraph("Op. Financieras", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDOFI14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//PREST. CONTINGENTES OTRAS DEUDAS DIRECTA
							cell = new Cell(new Paragraph("Prest. Contingentes", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDCTG14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//LEASING OTRAS DEUDAS DIRECTA
							cell = new Cell(new Paragraph("Leasing", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDLSG14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL OTRAS DEUDAS DIRECTAS
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEODDTOT14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							doc.add(table);
							  
							////
							doc.newPage();
							
							table = new Table(8, 40); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							//LINEA CRED DISP.
							cell = new Cell(new Paragraph("Linea Cred Disp.", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL LINEA CRED DISP.
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getELCPTOT07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							////
							//LINEA CRED DISP.
							cell = new Cell(new Paragraph(". ", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Estado Deuda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL LINEA CRED DISP.
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getELCPTOT14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							////
							
							doc.add(table);
							doc.add(new Paragraph(" "));
							
							table = new Table(1, 1); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							//SALTO
							cell = new Cell(new Paragraph("Total General", headerBoldFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(0);
							
							table.addCell(cell);
							
							doc.add(table);
							
							//TABLA TOTAL GENERAL
							table = new Table(8, 20); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							//TOTALES
							
							cell = new Cell(new Paragraph("Totales", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Concepto", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//CONSUMO TOTALES
							cell = new Cell(new Paragraph("Consumo", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//COMERCIAL TOTALES
							cell = new Cell(new Paragraph("Comercial", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//HIPOTECARIO VIVIENDA TOTALES
							cell = new Cell(new Paragraph("Hipotecario Vivienda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL TOTALES
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL101(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL102(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL103(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL104(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL105(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL106(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL107(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//DEUDA DIRECTA TOTALES
							cell = new Cell(new Paragraph("Deuda Directa", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD001(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD002(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD003(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD004(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD005(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD006(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD007(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//DEUDA INDIRECTA TOTALES
							cell = new Cell(new Paragraph("Deuda Indirecta", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL TOTALES
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL201(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL202(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL203(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL204(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL205(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL206(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL207(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//TERMINO TABLA TOTALES

							////
							
							//TOTALES
							
							cell = new Cell(new Paragraph(".", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Concepto", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//CONSUMO TOTALES
							cell = new Cell(new Paragraph("Consumo", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCON14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//COMERCIAL TOTALES
							cell = new Cell(new Paragraph("Comercial", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTCOM14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//HIPOTECARIO VIVIENDA TOTALES
							cell = new Cell(new Paragraph("Hipotecario Vivienda", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTHIP14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL TOTALES
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL108(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL109(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL110(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL111(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL112(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL113(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL114(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//DEUDA DIRECTA TOTALES
							cell = new Cell(new Paragraph("Deuda Directa", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD008(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD009(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD010(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD011(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD012(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD013(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDD014(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//DEUDA INDIRECTA TOTALES
							cell = new Cell(new Paragraph("Deuda Indirecta", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTDIM14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL TOTALES
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL208(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL209(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL210(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL211(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL212(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL213(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETOTAL214(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//TERMINO TABLA TOTALES

							////
							
							doc.add(table);
							
							
							doc.newPage();
							
							table = new Table(8, 40); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							doc.add(new Paragraph(" "));
							
							table = new Table(1, 1); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							//SALTO
							cell = new Cell(new Paragraph("T. Cr�dito", headerBoldFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(0);
							
							table.addCell(cell);
							
							doc.add(table);
							
							//TABLA T. Credito
							table = new Table(8, 4); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							cell = new Cell(new Paragraph("Cupos", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Concepto", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//OTORGADO CUPOS
							cell = new Cell(new Paragraph("Otorgado", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//UTILIZADO CUPOS
							cell = new Cell(new Paragraph("Utilizado", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX); 
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//DISPONIBLE CUPOS
							cell = new Cell(new Paragraph("Disponible", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							////
							
							cell = new Cell(new Paragraph(" . ", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Concepto", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//OTORGADO CUPOS
							cell = new Cell(new Paragraph("Otorgado", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCROTO14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//UTILIZADO CUPOS
							cell = new Cell(new Paragraph("Utilizado", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRUTI14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//DISPONIBLE CUPOS
							cell = new Cell(new Paragraph("Disponible", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getETCRDIS14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							////
							
							//FIN TABLA T. CREDITOS
							
							doc.add(table);
							doc.add(new Paragraph(" "));
							
							table = new Table(1, 1); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							//SALTO
							cell = new Cell(new Paragraph("Garant�as", headerBoldFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(0);
							
							table.addCell(cell);
							
							doc.add(table);
							
							//TABLA T. Credito
							table = new Table(8, 4); //COLUMNA,FILA
							table.setBorderWidth(0);
							table.setCellsFitPage(true);
							table.setPadding(1);
							table.setSpacing(1);
							table.setWidth(100);
							
							cell = new Cell(new Paragraph("Valores", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Concepto", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//GENERALES VALORES
							cell = new Cell(new Paragraph("Generales", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							//ESPECIFICAS VALORES
							cell = new Cell(new Paragraph("Espec�ficas", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL VALORES
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT01(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT02(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT03(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT04(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT05(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT06(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT07(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							////

							cell = new Cell(new Paragraph(".", negrita));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							cell.setColspan(8);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph("Concepto", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							cell = new Cell(new Paragraph(msgData.getE01FRF08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getE01FRF14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setVerticalAlignment(Element.ALIGN_CENTER);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//GENERALES VALORES
							cell = new Cell(new Paragraph("Generales", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARGNR14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//ESPECIFICAS VALORES
							cell = new Cell(new Paragraph("Espec�ficas", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARESP14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							//TOTAL VALORES
							cell = new Cell(new Paragraph("Total", normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT08(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT09(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT10(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT11(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT12(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT13(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);
							
							cell = new Cell(new Paragraph(msgData.getEGARTOT14(), normalFont));
							cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							cell.setVerticalAlignment(Element.ALIGN_RIGHT);
							cell.setBorder(Rectangle.BOX);
							table.addCell(cell);

							////
							doc.add(table);						
							
							if (doc != null) doc.close();
							if (docWriter != null) docWriter.close();
							StringBuffer sbFilename = new StringBuffer();
							String fn = com.datapro.generic.tool.Util.getTimestamp().toString();
							fn = Util.replace(fn,":", "-");
							fn = Util.replace(fn,".", "-");
							sbFilename.append(fn);
							sbFilename.append(".pdf");
							
							res.setHeader("Cache-Control", "max-age=30");
							res.setContentType("application/pdf");

							StringBuffer sbContentDispValue = new StringBuffer();
							sbContentDispValue.append("inline");
							sbContentDispValue.append("; filename=");
							sbContentDispValue.append(sbFilename);
							
							res.setHeader("Content-disposition", sbContentDispValue.toString());
							res.setContentLength(baosPDF.size());

							ServletOutputStream sos = res.getOutputStream();
							baosPDF.writeTo(sos);
							sos.flush();
							
						

						} catch (DocumentException dex) {
							if (docWriter != null) docWriter.close();
							if (doc != null) doc.close();
							res.setContentType("text/html");
							PrintWriter writer = res.getWriter();
							writer.println(
								this.getClass().getName()
									+ " caught an exception: "
									+ dex.getClass().getName()
									+ "<br>");
							writer.println("<pre>");
							dex.printStackTrace(writer);
							writer.println("</pre>");
						} finally {
							if (docWriter != null) docWriter.close();
							if (doc != null && doc.isOpen()) doc.close();
							if (baosPDF != null) baosPDF.close(); 
						}
			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}


	
	/**
	 * This method was created .
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 */
	protected void procActionIdentifyList(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses) {

		UserPos userPO = null;

		try {
			procReqIdentifySaldDeudCons(mc, user, req, res, ses);

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	protected void procReqIdentifySaldDeudCons(MessageContext mc,
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {

		MessageRecord newmessage = null;
		ECIF21501Message msgData = null;
		ECIF30001Message msgDataSocio = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		String clientNum = "";
		String clientRut = "";
		String clientType = "";

		try {

			userPO = (UserPos) ses.getAttribute("userPO");
			
			req.getParameter("E01CUN");
			if (req.getParameter("E01CUN")!=null) {
				clientNum = req.getParameter("E01CUN");
			}
			else if (userPO.getCusNum().length() > 0)
				clientNum = userPO.getCusNum();
			
			userPO.setCusNum(clientNum);
			ses.setAttribute("userPO", userPO);

			req.getParameter("E01RUT");
			if (req.getParameter("E01RUT")!=null) {
				clientRut = req.getParameter("E01RUT");
			}
			else
				throw new Exception("Client Identify missing");

			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		// Send Initial data
		try {
			msgData = (ECIF21501Message) mc.getMessageRecord("ECIF21501");
			msgData.setH01USR(user.getH01USR());
			msgData.setH01PGM("ECIF215");
			msgData.setH01TIM(getTimeStamp());
			msgData.setH01WK1(userPO.getHeader8());
			msgData.setH01SCR("01");
			msgData.setH01OPE("0002");
			
			msgData.setE01RUT(req.getParameter("E01RUT"));
			msgData.setE01TID(req.getParameter("E01TID"));
			msgData.setE01PID(user.getE01CTR());

			msgData.send();
			msgData.destroy();

			flexLog("ECIF21501 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {

			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {

				try {
					msgError = new datapro.eibs.beans.ELEERRMessage();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;

			}
			
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ECIF21501")) {

				try {
					msgData = new datapro.eibs.beans.ECIF21501Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgData = (ECIF21501Message) newmessage;
			}
			
				if (!msgError.getERRNUM().equals("0")) {
					try {
						ses.setAttribute("error", msgError);						
						flexLog("About to call Page: " + LangPath + "ECIF215_cif_client_search_identification.jsp");
						callPage(LangPath + "ECIF215_cif_client_search_identification.jsp", req, res);

					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
				
				else {

						flexLog("Putting java beans into the session");
						ses.setAttribute("error", msgError);
						ses.setAttribute("cifData", msgData);
						ses.setAttribute("cifDataSocio", msgDataSocio);
		
						try {
							flexLog("About to call Page: " + LangPath
									+ "ECIF215_cif_saldos_deuda_consolidada.jsp");
							callPage(LangPath
									+ "ECIF215_cif_saldos_deuda_consolidada.jsp", req,
									res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}					
					
			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}
	
	
	/**
	 * This method was created in VisualAge.
	 * @param mc 
	 */
	protected void procReqSearch(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
				throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos	userPO = null;	

		try {

			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("CIF");
			userPO.setPurpose("INQUIRY");
			String opt = null;
			try {
				opt = req.getParameter("OPTION");
				if (opt == null) {
					opt = "";
				}
			}
			catch (Exception e) {
				opt = "";
			}
			userPO.setHeader8(opt);

			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

	  	} catch (Exception ex) {
			flexLog("Error: " + ex); 
	  	}

		try {
			flexLog("About to call Page: " + LangPath + "ECIF215_cif_client_search_identification.jsp");
			callPage(LangPath + "ECIF215_cif_client_search_identification.jsp", req, res);
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}

	}
	
	public void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			int screen = R_SEARCH;

			try {

				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session
						.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					mc = new MessageContext(super.getMessageHandler("ECIF215",
							req));

					try {
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					} catch (Exception e) {
						flexLog("Screen set to default value");
					}

					switch (screen) {
					// Requests
					case A_LIST:
						procActionCustomerList(mc, msgUser, req, res, session);
						break;
					case A_ENTER_CUS:
						procActionIdentifyList(mc, msgUser, req, res, session);
						break;
					case R_ENTER_CUS:
						procReqSearch(mc, msgUser, req, res, session);
						break;
					default:
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
					res.sendRedirect(super.srctx + LangPath
							+ super.sckNotOpenPage);
				} finally {
					if (mc != null)
						mc.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath
						+ super.sckNotRespondPage);
			}

		}

	}

	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

		}
	}
}