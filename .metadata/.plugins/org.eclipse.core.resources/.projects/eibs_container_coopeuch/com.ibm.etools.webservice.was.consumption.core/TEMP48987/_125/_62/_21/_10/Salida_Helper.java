/**
 * Salida_Helper.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public class Salida_Helper {
    // Type metadata
    private static com.ibm.ws.webservices.engine.description.TypeDesc typeDesc =
        new com.ibm.ws.webservices.engine.description.TypeDesc(Salida.class);

    static {
        com.ibm.ws.webservices.engine.description.FieldDesc field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("codigoRetorno");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codigoRetorno"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("glosaRetorno");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "glosaRetorno"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("resultado");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "resultado"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://10.21.62.125:8186/pricing/precio", "elementoSalida"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("rolDescuento");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "rolDescuento"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://10.21.62.125:8186/pricing/precio", "rolDescuento"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static com.ibm.ws.webservices.engine.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new Salida_Ser(
            javaType, xmlType, typeDesc);
    };

    /**
     * Get Custom Deserializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new Salida_Deser(
            javaType, xmlType, typeDesc);
    };

}
