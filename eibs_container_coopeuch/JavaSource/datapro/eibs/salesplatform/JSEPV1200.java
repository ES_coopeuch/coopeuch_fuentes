package datapro.eibs.salesplatform;

/*********************************************************************************************************************************/
/**  Creado por              :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  10/03/2012                                                                                      **/
/**  Objetivo                :  Mantenedor de Tabla de Leverage para Plataforma de Ventas                                       **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV120001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;

public class JSEPV1200 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int R_PVVRT_ENTER = 100;
	protected static final int A_PVVRT_ENTER = 150;	
	protected static final int A_PVVRT_LIST = 180;
	protected static final int R_PVVRT_PD_NEW = 200;
	protected static final int R_PVVRT_PD_MAINT = 201;
	protected static final int R_PVVRT_PD_DELETE = 202;	
	protected static final int R_PVVRT_PD_INQUIRY = 203;
	protected static final int R_PVVRT_TC_NEW = 300;
	protected static final int R_PVVRT_TC_MAINT = 301;
	protected static final int R_PVVRT_TC_DELETE = 302;	
	protected static final int R_PVVRT_TC_INQUIRY = 303;
	protected static final int R_PVVRT_RC_NEW = 400;
	protected static final int R_PVVRT_RC_MAINT = 401;
	protected static final int R_PVVRT_RC_DELETE = 402;	
	protected static final int R_PVVRT_RC_INQUIRY = 403;
	protected static final int A_PVVRT_MAINT = 600;

	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_PVVRT_ENTER :
			procReqEnterInquiry(user, req, res, session, null);
			break;		
		case A_PVVRT_ENTER :
			procActionEnterInquiry(user, req, res, session, null);
			break;			
		case A_PVVRT_LIST:
			procActionTablaPVVRTList(user, req, res, session, null);
			break;
		case R_PVVRT_PD_NEW:
			procReqTablaPVVRT(user, req, res, session, "NEW", "D");
			break;
		case R_PVVRT_PD_MAINT:
			procReqTablaPVVRT(user, req, res, session, "MAINTENANCE", "D");
			break;
		case R_PVVRT_PD_INQUIRY:
			procReqTablaPVVRT(user, req, res, session, "INQUIRY", "D");
			break;
		case R_PVVRT_PD_DELETE:
			procReqDelete(user, req, res, session, "D");
			break;
		case R_PVVRT_TC_NEW:
			procReqTablaPVVRT(user, req, res, session, "NEW", "T");
			break;
		case R_PVVRT_TC_MAINT:
			procReqTablaPVVRT(user, req, res, session, "MAINTENANCE", "T");
			break;
		case R_PVVRT_TC_INQUIRY:
			procReqTablaPVVRT(user, req, res, session, "INQUIRY", "T");
			break;
		case R_PVVRT_TC_DELETE:
			procReqDelete(user, req, res, session, "T");
			break;			
		case R_PVVRT_RC_NEW:
			procReqTablaPVVRT(user, req, res, session, "NEW", "R");
			break;
		case R_PVVRT_RC_MAINT:
			procReqTablaPVVRT(user, req, res, session, "MAINTENANCE", "R");
			break;
		case R_PVVRT_RC_INQUIRY:
			procReqTablaPVVRT(user, req, res, session, "INQUIRY", "R");
			break;
		case R_PVVRT_RC_DELETE:
			procReqDelete(user, req, res, session, "R");
			break;
		case A_PVVRT_MAINT:
			procActionMaintenance(user, req, res, session);
			break;	
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}
	protected void procReqEnterInquiry(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String option)
	throws ServletException, IOException {

		UserPos	userPO = null;	
		
		MessageProcessor mp = null;
		
		try {
 
			session.setAttribute("userpPO", userPO);
			forwardOnSuccess("EPV1200_PVVRT_enter_selection.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	protected void procActionEnterInquiry(
			ESS0030DSMessage user, 
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession ses,
			String option)
				throws ServletException, IOException {
		UserPos	userPO = null;	

		try {
			userPO = (datapro.eibs.beans.UserPos)ses.getAttribute("userPO");
			/*  search D = Pago Directo
			 *  search T = Tarjeta de Credito
			 *  search R = Retención de Cartera
			 */
			String search = req.getParameter("search");
			ses.setAttribute("userPO", userPO);
			res.sendRedirect(super.srctx + "/servlet/datapro.eibs.salesplatform.JSEPV1200?SCREEN=180&opt=" + search);
		
		} catch (Exception ex) {
			flexLog("Error: " + ex); 
		}
	}	
	/**
	 * procActionTablaPVVRTList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionTablaPVVRTList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String option)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EPV1200", req);
			EPV120001Message msg = (EPV120001Message) mp.getMessageRecord("EPV120001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0015");
			msg.setE01PVVDOP(req.getParameter("opt"));
			msg.setH01TIMSYS(getTimeStamp());
			//Sends message
			flexLog("mensaje enviado .. " + msg);
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
 
			session.setAttribute("userpPO", userPO);
			session.setAttribute("EPV120001List", list);			
			if (req.getParameter("opt").equals("D")) 
			{
				forwardOnSuccess("EPV1200_PVVRT_PD_list.jsp", req, res);
			} else if (req.getParameter("opt").equals("T")) 
			{
				forwardOnSuccess("EPV1200_PVVRT_TC_list.jsp", req, res);				
			} else 
			{
				forwardOnSuccess("EPV1200_PVVRT_RC_list.jsp", req, res);				
			}  

			
			
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqTablaPVVRT: This Method show a single  TablaPVVRT either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTablaPVVRT(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, 
			String option, 
			String tipo) 
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		userPO.setPurpose(option);	
		userPO.setOption(tipo);
		try {
			mp = getMessageProcessor("EPV1200", req);
			
			//Creates the message with operation code depending on the option
			EPV120001Message msg = (EPV120001Message) mp.getMessageRecord("EPV120001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01TIMSYS(getTimeStamp());
			msg.setE01PVVDOP(tipo);
			if (option.equals("NEW")) {
				msg.setH01OPECOD("0001");
			} else if (option.equals("MAINTENANCE")) {
				msg.setH01OPECOD("0002");
			} else {
				msg.setH01OPECOD("0004");
			}
			//Sets the number for maintenance and inquiry options
			if (req.getParameter("E01PVVICU") != null) {
				msg.setE01PVVICU(req.getParameter("E01PVVICU"));
			}

			//Send message
			flexLog("mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV120001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go to maintenance page
				flexLog("About to call Page: EPV1200_PVVRT_maintenance.jsp");
				if (option.equals("INQUIRY")) {
					// if the request is an inquiry sets the readOlnly attribute 'true'
					forward("EPV1200_PVVRT_maintenance.jsp?readOnly=true", req, res);
				} else {
					forward("EPV1200_PVVRT_maintenance.jsp", req, res);
				}
			} else {
				//if there are errors go back to list page
				session.setAttribute("error", msgError);
				if (tipo.equals("D")) 
				{
					forwardOnSuccess("EPV1200_PVVRT_PD_list.jsp", req, res);
				} else if (tipo.equals("T")) 
				{
					forwardOnSuccess("EPV1200_PVVRT_TC_list.jsp", req, res);				
				} else 
				{
					forwardOnSuccess("EPV1200_PVVRT_RC_list.jsp", req, res);				
				}  
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procActionMaintenance
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1200", req);
			EPV120001Message msg = (EPV120001Message) mp.getMessageRecord("EPV120001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0005");		
			msg.setH01TIMSYS(getTimeStamp());
			String tipo =  req.getParameter("TIPO");
			//Sets message with page fields
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);
			msg.setE01PVVDOP(tipo);
			//Sending message
			flexLog("mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV120001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go back to list
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1200?SCREEN=150&search=" + tipo, res);
			} else {
				//if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				forward("EPV1200_PVVRT_maintenance.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqDelete
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDelete(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session, String tipo)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("MAINTENANCE");
		userPO.setOption(tipo);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1200", req);
			
			//Creates message with the 'Delete'operation code
			EPV120001Message msg = (EPV120001Message) mp.getMessageRecord("EPV120001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0009");
			msg.setE01PVVDOP(tipo);			
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets required values
			msg.setH01SCRCOD("01");
			//Sets the Code for delete options
			if (req.getParameter("E01PVVICU") != null) {
				msg.setE01PVVICU(req.getParameter("E01PVVICU"));
			}
			
			//Send message
			mp.sendMessage(msg);

			//Receive Error and Data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV120001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//If there are no errors request the list again
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1200?SCREEN=150&search=" + tipo, res);
			} else {
				//if there are errors show the list without updating
				session.setAttribute("error", msgError);
				if (tipo.equals("D")) 
				{
					forwardOnSuccess("EPV1200_PVVRT_PD_list.jsp", req, res);
				} else if (tipo.equals("T")) 
				{
					forwardOnSuccess("EPV1200_PVVRT_TC_list.jsp", req, res);				
				} else 
				{
					forwardOnSuccess("EPV1200_PVVRT_RC_list.jsp", req, res);				
				}  
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

}
