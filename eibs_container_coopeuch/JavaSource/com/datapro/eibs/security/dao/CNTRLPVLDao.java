package com.datapro.eibs.security.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.PrivilegeNotFoundException;
import com.datapro.generic.access.DAOSuper;
import com.datapro.eibs.security.bean.CNTRLPVL;
import com.datapro.eibs.security.bean.CNTRLPVLNME;
import com.datapro.generic.beanutil.*;
import datapro.eibs.beans.JBObjList;


/**
 * @author ogarcia
 *
 */
public class CNTRLPVLDao extends DAOSuper {

	private static Category cat =
		Category.getInstance(CNTRLPVLDao.class.getName());

	/**
	 * Constructor for CNTRLPVLDao.
	 */
	public CNTRLPVLDao() {
		super();
	}
	
	/**
	 * CNTRLPVLDao constructor comment.
	 */

	public CNTRLPVLDao(String DBID) {
		super(DBID);
	}
	
	public CNTRLPVLDao(Connection connection) {
		super(connection);
	}
	
	public JBObjList select(String uid) 
		throws PrivilegeNotFoundException, DAOException {
		
		JBObjList list = null;
		try {
			
			String sql = "SELECT a.*, b.CNODSC, b.CNOF04 FROM CNTRLPVL a, CNOFC b " +
			             "WHERE a.PVLUSR=? AND b.CNOCFL=? AND a.PVLPVL=b.CNORCD";
			
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, "PV");
			ResultSet rs = ps.executeQuery();
			
			ResultSetMetaData md = rs.getMetaData();
			list = new JBObjList();
			while (rs.next()) {
				CNTRLPVLNME bean = new CNTRLPVLNME();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new PrivilegeNotFoundException(
					"User privilege not found exception(uid:" + uid + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		} 

		return list;
		
	}
	
	public CNTRLPVL select(String uid,String cde) 
		throws PrivilegeNotFoundException, DAOException {
		
		CNTRLPVL bean = null;
		
		try {
			
			String sql = "SELECT * FROM CNTRLPVL WHERE PVLUSR=? AND PVLPVL=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, cde);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNTRLPVL();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} else {
				throw new PrivilegeNotFoundException(
					"User privilege not found exception(uid:" + uid + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}
		
		return bean;
			
		
	}
	
	public int delete (String uid) throws DAOException {
		int rt;

		try {
			String sql = "DELETE FROM CNTRLPVL WHERE PVLUSR=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
		
	}
	
	public int delete (String uid,String cde) throws DAOException {
		int rt;

		try {
			String sql = "DELETE FROM CNTRLPVL WHERE PVLUSR=? AND PVLPVL=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, cde);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
		
	}
	
	public void insert (JBObjList list) throws DAOException {
		
		try {
			list.initRow();
			int result = 0;
			while (list.getNextRow()) {
				CNTRLPVL bean = (CNTRLPVL) list.getRecord();
			
				BeanSql bs = new BeanSql(bean);
				String sql = bs.insertPrepareStatement();
				PreparedStatement ps = cnx.prepareStatement(sql);
				bs.parsePrepareStatement(ps);
				ps.executeUpdate();
				ps.close();
			}

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}
		
	}
	
	public int insert (CNTRLPVL bean) throws DAOException {
		
		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.insertPrepareStatement();
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}
	
	public int update (CNTRLPVL bean) throws DAOException {
				
		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = "UPDATE cntrlpvl SET pvlval=? WHERE PVLUSR=? AND PVLPVL=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			//bs.parsePrepareStatement(ps);
			ps.setString(1, bean.getPVLVAL());
			ps.setString(2, bean.getPVLUSR());
			ps.setString(3, bean.getPVLPVL());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
		
		/*
		
		PreparedStatement st = connection.prepareStatement ("UPDATE "+ dbProp.get("dbSCHEMA" + dbPropIndex).trim() +".cntrlpvl SET pvlval=? WHERE pvlusr=? AND pvlpvl=?");
		*/
	}
	

}