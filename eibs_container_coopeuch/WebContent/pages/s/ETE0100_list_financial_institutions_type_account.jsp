<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ETE010002Message"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ETE010004Message"%>
<%@page import="com.datapro.constants.EibsFields"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Transferencias - Seguridad y Parámetros</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="msgtransa" class= "datapro.eibs.beans.ETE010002Message"  scope="session" />
<jsp:useBean id="IfiObj" class= "datapro.eibs.beans.ETE010002Message"  scope="session" />
<jsp:useBean id="listatipocuenta" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">
   $(function()
    {
		$("#radio_key1").attr("checked", false);
		$("#radio_key2").attr("checked", false);
    }
    );


function goAction(op) 
{
	if (op =='1300')
	{
		document.forms[0].SCREEN.value = op;
		document.forms[0].submit();	
		return;	
	}		

	var ok = false;

 	for(n=0; n<document.forms[0].elements.length; n++)
    {
      	var element = document.forms[0].elements[n];
 
      	if(element.name == "E04MIFSBI") 
	    {	
	      	if (element.checked == true) 
	      	{
	      		document.getElementById("codinsfin").value = element.value; 
        		ok = true;
        		break;
			}
	    }
     } 
      
     if(ok)
     {
      	var confirm1 = true;
      	
      	if (op =='1500')
			confirm1 = confirm("Desea Eliminar la Tipo de Cuenta Seleccionada?");
      	
	  	if (confirm1)
	  	{
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		
     } 
     else 
		alert("Debe Seleccionar un Tipo de Cuenta para Continuar.");	   
}

function goAction3(op) {

 	document.forms[0].SCREEN.value = op;
	document.forms[0].submit();		
}


</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Transferencias - Seguridad y Par&aacute;metros
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="list_financial_institutions_type_account, ETE0100">
</h3>

<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.tefmulti.JSETE0100">
<input type="hidden" name="SCREEN" value="100">
<input type="hidden" name="codinsfin" value="" id="codinsfin">

<table  class="tableinfo">
	<tr bordercolor="#FFFFFF"> 
		<td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Tipo de Transacci&oacute;n : 
              </td>
             <td nowrap width="10%" align="left">
               <eibsinput:text name="msgtransa" property="E01LTRCOD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>"  readonly="true" />	
               <eibsinput:text name="msgtransa" property="E01LTRDES" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>"  readonly="true" />	
             </td>
         </tr>
        </table>
      </td>
    </tr>
</table>

<table  class="tableinfo">
	<tr bordercolor="#FFFFFF"> 
		<td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Instituci&oacute;n : 
              </td>
             <td nowrap width="90%" align="left" colspan="3">
       			 <div align="left"> 
 		              <eibsinput:text name="IfiObj" property="E02LIFSBI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>"  readonly="true" />	
		              <eibsinput:text name="IfiObj" property="E02LIFDSB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>"  readonly="true" />	
        		</div>
             </td>
         </tr>
          <tr>
             <td nowrap width="10%" align="right"> Monto Estado : 
              </td>
             <td nowrap width="20%" align="left">
               <eibsinput:text name="IfiObj" property="E02LIFMAX" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />	
             </td>
             <td nowrap width="10%" align="right"> Estado :   
              </td>
             <td nowrap width="60%" align="left">
             	   <% String StrEstado = "NODEF";
             	      if (IfiObj.getE02LIFSTS().equals("A"))
             	      		StrEstado = "ACTIVA";
             	      else 
             	      	if(IfiObj.getE02LIFSTS().equals("C"))			
             	      		StrEstado = "CERRADA";
 				
             	      		
             	   %>
    			   <input type="text" name="E02LIFDSB" value="<%= StrEstado%>" size="15" maxlength="12"  readonly >
             </td>
         </tr>
        </table>
      </td>
    </tr>
</table>
<p align="center">
	<input id="EIBSBTN" type="button" name="Volver" value="Volver" onclick="goAction3('200')">
</p>
<br>
<br>
<table width="100%">
<tr id="trclear" >
<td>
<hr style="  border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));  "/>
<h3 align="center" >INSTITUCIONES FINANCIERAS - TIPOS DE CUENTA</h3>
</td>
</tr>
</table>

<table class="tbenter" width="100%">
<tr>
<td>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg"> 
			<a href="javascript:goAction('1300')"><b>NUEVA</b></a>
		</td>
		<td align="center" class="tdbkg">
			<a href="javascript:goAction('1400')"> <b>MANTENCI&Oacute;N</b> </a>
		</td>
		<td align="center" class="tdbkg">
			<a href="javascript:goAction('1500')"> <b>ELIMINAR</b> </a>
		</td>
	</tr>
</table>

</td>
</tr>

<tr>
<td>

<table id="headTable"  width="100%" align="left">
	<tr id="trdark">
			<th align="center" nowrap width="5%"></th>
			<th align="center" nowrap width="5%">Tipo Cuenta</th>
			<th align="center" nowrap width="25%">Descripci&oacute;n</th>
			<th align="center" nowrap width="15%">Monto Min</th>
			<th align="center" nowrap width="15%">Monto M&aacute;x</th>
			<th align="center" nowrap width="15%">Monto 1er TEF</th>
			<th align="center" nowrap width="15%">Monto TEF</th>
			<th align="center" nowrap width="5%">Nro Factor</th>
	</tr>
		<%
			listatipocuenta.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (listatipocuenta.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ETE010004Message regtrans = (ETE010004Message) listatipocuenta.getRecord();
		%>
	<tr>
			
			<td nowrap align="center">
				<input type="radio"  name="E04MIFSBI" id="radio_key1" value="<%=listatipocuenta.getCurrentRow() %>" <%=chk%>/>
			</td>
			
			<td nowrap align="center">
				<%=regtrans.getE04MIFTCA()%>
			</td>

			<td nowrap align="left">
				<%=regtrans.getE04MIFDTC()%>
			</td>
			
			<td nowrap align="right">
				<%=regtrans.getE04MIFMMI()%>
			</td>

			<td nowrap align="right">
				<%=regtrans.getE04MIFMMA()%>
			</td>

			<td nowrap align="right">
				<%=regtrans.getE04MIFMTR()%>
			</td>

			<td nowrap align="right">
				<%=regtrans.getE04MIFNU1()%>
			</td>
			
			<td nowrap align="center">
				<%=regtrans.getE04MIFFSE()%>
			</td>

	</tr>
		<%
			}
		%>
	</table>
</td>
</tr>

</table>

</form>
</body>
</html>
