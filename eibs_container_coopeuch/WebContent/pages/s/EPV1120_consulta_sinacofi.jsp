<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields,java.util.List,java.util.Iterator,cl.coopeuch.creditos.predictorriesgo.sinacofi.Salida"%>

<%@page import="java.util.ArrayList,java.math.BigDecimal"%>
<%@page import="com.ibm.ObjectQuery.crud.util.Array"%><html>
<head>
<title>Plataforma de Venta</title>
<%
  String ni = (String)session.getAttribute("NIVEL"); 
  ni = (ni==null?"":ni);
 %>	
 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%	//Sacamos de session el Objeto Sinacofi
	Salida  objSinac = (Salida)session.getAttribute("objSinac");
%>
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script language="Javascript1.1">


function showE()
{
 	var page= prefix +language + "error_viewer_esp.jsp?ERNU01=&ERDS01=" + " <%= request.getAttribute("ERDS01")%> " ;
	errorwin = CenterNamedWindow(page,'error',420,300,2);   
}

</SCRIPT>
 
</head>

<%if (!request.getAttribute("ERNU01").equals("")){%>
<body onload="showE()">
<%}else{%>
<body>
<%}%>


<%  java.text.DecimalFormat formateador = new java.text.DecimalFormat("###,###,###,###.##"); %>

<h3 align="center"> Consulta BUREAU - INFORME COMERCIAL<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="consulta_sinacofi.jsp,JSEPV1120"></h3>
<hr size="4">
<h4>Datos Solicitante </h4>
  <% int row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> RUT :</td>
             <td nowrap align="left" width="30%">
				<eibsinput:text name="userPO" property="identifier" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>	  			
             </td>
             <td nowrap align="right" width="20%"> Serie :</td>
             <td nowrap align="left" width="30%">
				<eibsinput:text name="userPO" property="header1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>
             </td>
         </tr>
       </table>
      </td>
    </tr>
  </table>

<%if (objSinac!=null){//no hay errores..%>

<%if (!request.getAttribute("flat01").equals("")){%>
<h4>Estado Cedula</h4> 
 <% row = 0;%>
 <%if (objSinac.getResultsEstadoCedula()!=null && objSinac.getResultsEstadoCedula().getEstadoCedula()!=null){ %>

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%">  VIGENTE:</td>
             <td nowrap align="left" width="20%">
                  <%=objSinac.getResultsEstadoCedula().getEstadoCedula().getCedulaVigente() %>        	    	       
             </td>
             <td nowrap align="right" width="20%"> Motivo :</td>
             <td nowrap align="left" width="40%">
 				<%=objSinac.getResultsEstadoCedula().getEstadoCedula().getRazon() %>             
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
             <td nowrap align="right"> Fecha Vencimiento :</td>
             <td nowrap align="left">
             	<%=(!"".equals(objSinac.getResultsEstadoCedula().getEstadoCedula().getFecha())?objSinac.getResultsEstadoCedula().getEstadoCedula().getFecha():"N/D" ) %>   
             </td>                      
             <td nowrap align="right"> Fuente :</td>
             <td nowrap align="left">
             	<%=objSinac.getResultsEstadoCedula().getEstadoCedula().getFuente() %>   
             </td> 
         </tr>              
        </table>
      </td>
    </tr>
  </table>
  <%}else{%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="center" >Información No Disponible</td>
         </tr>            
        </table>
      </td>
    </tr>
  </table>
  <%}%>
<%}%>

<%if (!request.getAttribute("flat26").equals("")){%>
  <h4>Detalle Directorio de Persona</h4> 
   <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
      	<% 	        
		    List<Salida.ResultsDirectiorioPersonas.DirectiorioPersonas> listaDAP =null;
		    if (objSinac.getResultsDirectiorioPersonas() !=null){
				listaDAP =  objSinac.getResultsDirectiorioPersonas().getDirectiorioPersonas();
		    }	      	
			if (listaDAP!=null && listaDAP.size()>0){
				//escribimos la tabla y el encabezado
	  	%>
	  		<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">	
		  	<%
			for (Iterator<Salida.ResultsDirectiorioPersonas.DirectiorioPersonas> iterator = listaDAP.iterator(); iterator.hasNext();) {
						Salida.ResultsDirectiorioPersonas.DirectiorioPersonas object = iterator.next();
		 	%>		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="right" >Direccion : </td>
    				<td nowrap align="left"  colspan="3"><%=object.getDireccion()%></td>		             		             
		         </tr>	         	  
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
					 <td nowrap align="right" width="14%">Comuna :</td>		      
		             <td nowrap align="left" width="20%"><%=object.getComuna()%></td>					        
		             <td nowrap align="right" width="13%">Ciudad :</td>
					 <td nowrap align="left" width="20%"><%=object.getCiudad()%></td>		             
		         </tr>	         	  
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 					 					 
					 <td nowrap align="right" >Actividad :</td>
					 <td nowrap align="left" ><%=object.getActividad()%></td>
					 <td nowrap align="right" >Fecha validez :</td>					 
					 <td nowrap align="left" ><%=object.getIFechaInformacion()%></td>					 					 
		          </tr>
		<%}
		%>
        		</table>
		<%			
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        		</table>
		<%	
			}			
      %>  
      </td>
    </tr>
  </table> 
<%}%>  
   
  <%       	      	
     List<Salida.ResultsDireccionesAsociadas.DireccionesAsociadas> listaDA =null;
     if (objSinac.getResultsDireccionesAsociadas() !=null){
		listaDA =  objSinac.getResultsDireccionesAsociadas().getDireccionesAsociadas();
    }	
%>

 <%if (!request.getAttribute("flat18").equals("")){%>
  <h4>Direcciones Asociadas al Rut (Cantidad Encontrada = <%=(listaDA!=null && listaDA.size()>0)?listaDA.size():"0"%>)</h4> 
   <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
      	<% 	       		     		      			
			if (listaDA!=null && listaDA.size()>0){
				//escribimos la tabla y el encabezado
	  	%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" width="25%">Dirección</td>
					 <td nowrap align="center" width="15%">Fecha</td>		             
		             <td nowrap align="center" width="15%">Comuna</td>
					 <td nowrap align="center" width="15%">Ciudad</td>
					 <td nowrap align="center" width="15%">Tipo</td>
					 <td nowrap align="center" width="15%">Fuente</td>					 
		         </tr>
	<%
		for (Iterator<Salida.ResultsDireccionesAsociadas.DireccionesAsociadas> iterator = listaDA.iterator(); iterator.hasNext();) {
					Salida.ResultsDireccionesAsociadas.DireccionesAsociadas object = iterator.next();
	 %>		         	  
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center"><%=object.getDireccion()%></td> 
		             <td nowrap align="center"><%=object.getIFecha()%></td>
					 <td nowrap align="center"><%=object.getComuna()%></td>					 
					 <td nowrap align="center"><%=object.getCiudad()%></td>
					 <td nowrap align="center"><%=object.getTipo()%></td>
					 <td nowrap align="center"><%=object.getFuente()%></td>					 
		          </tr> 		
		<%		
		}			
		%>
        		</table>
		<%			
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        		</table>
		<%	
			}			
      %>  
      </td>
    </tr>
  </table> 
<%}%>  

<%if (!request.getAttribute("flat82").equals("")){%>
 <h4>Detalle Protestos</h4>
  <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
      	<% 
      		List<String> lmoneda =  new ArrayList<String>();
      		List<BigDecimal> lmonto =  new ArrayList<BigDecimal>();
      		List<Integer> lcant =  new ArrayList<Integer>();
			//arreglo que contiene en la posicion un arreglo de los acreedores      		
      	    List<List<String>> lacreedor =  new ArrayList<List<String>>();
      	          		      		
      		List<Salida.ResultsProtestosyDocumentosVigentes.ProtestosyDocumentosVigentes> lista =null;
      		if (objSinac.getResultsProtestosyDocumentosVigentes()!=null){
				lista =  objSinac.getResultsProtestosyDocumentosVigentes().getProtestosyDocumentosVigentes();      		
      		}			
						
			if (lista!=null && lista.size()>0){				
				//escribimos la tabla y el encabezado
	  	%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" width="12%">Fecha Venc.</td>
		             <td nowrap align="center" width="14%">Tipo Dcto.</td>
					 <td nowrap align="center" width="8%">Moneda</td>
					 <td nowrap align="center" width="15%">Monto</td>
					 <td nowrap align="center" width="15%">Emisor/Librador.</td>
					 <td nowrap align="center" width="12%">Localidad</td>
					 <td nowrap align="center" width="8%">Nro OP.</td>
					 <td nowrap align="center" width="12%">Fecha Pub.</td>
		         </tr>	  
	  	<%			
				for (Iterator<Salida.ResultsProtestosyDocumentosVigentes.ProtestosyDocumentosVigentes> iterator = lista.iterator(); iterator.hasNext();) {
					Salida.ResultsProtestosyDocumentosVigentes.ProtestosyDocumentosVigentes object = iterator.next();
					//acumulamos el monto, moneda y documento
					 if (object.getIdMoneda()!=null){
						 int indice = lmoneda.indexOf(object.getIdMoneda()); 
						 if (indice == -1){//no existe lo agregamos
						 	lmoneda.add(object.getIdMoneda());
						 	lmonto.add((object.getMonto()==null)?(new BigDecimal(0)):object.getMonto());
						 	lcant.add(1);
						 	List<String> li_acreedor = new ArrayList<String>();
						 	li_acreedor.add(object.getBancoLibrador());
						 	lacreedor.add(li_acreedor);
						 }else{
						 	lmonto.set(indice,lmonto.get(indice).add(object.getMonto())) ;
						 	lcant.set(indice,new Integer((lcant.get(indice).intValue()+1))) ;
						 	List<String> li_acreedor = lacreedor.get(indice);
						 	if (!li_acreedor.contains(object.getBancoLibrador())){//lo agregamos
								li_acreedor.add(object.getBancoLibrador());
						 		lacreedor.set(indice,li_acreedor);					 		
						 	}
						 	
						 }
					 }//EoF
					 
		%>
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center"><%=object.getIFechaVencimiento()%></td>
		             <td nowrap align="center"><%=object.getDoc()%></td>
					 <td nowrap align="center"><%=object.getIdMoneda()%></td>
					 <td nowrap align="center"><%=formateador.format((object.getMonto()==null)?(new BigDecimal(0)):object.getMonto())%></td>
					 <td nowrap align="center"><%=object.getBancoLibrador()%></td>
					 <td nowrap align="center"><%=object.getLocalidad()%></td>
		             <td nowrap align="center"><%=object.getNumeroOperacion()%></td>
		             <td nowrap align="center"><%=object.getIFechaBoletin()%></td>		             
					 
		          </tr> 		
		<%				
				}
		%>
        		</table>
		<%								
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        		</table>
		<%	
			}			
      %>        
      
      </td>
    </tr>
  </table>  
 
 <%
 if (lista!=null && lista.size()>0){
 %>
 
 	 <h4>Resumen Protestos</h4>
		<% row = 0;%>
		  <table  class="tableinfo">
		    <tr bordercolor="#FFFFFF"> 
		      <td nowrap>       
		        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
		
		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center"  width="30%">  Moneda</td>
		             <td nowrap align="center"  width="25%">  Monto Total</td>
		             <td nowrap align="center"  width="25%">  Nro. Documentos</td>
		             <td nowrap align="center"  width="45%">  Nro. Acreedores</td>
		         </tr>
				<% for (int c=0;c<lmoneda.size();c++){
				 %>
			          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			             <td nowrap align="center"  ><%=lmoneda.get(c) %></td>
			             <td nowrap align="center"  ><%=formateador.format(lmonto.get(c)==null?new BigDecimal(0):lmonto.get(c))%></td>
			             <td nowrap align="center"  ><%=lcant.get(c) %></td>
			             <td nowrap align="center"  ><%=(lacreedor.get(c)==null)?"0":lacreedor.get(c).size()%></td>
			         </tr>				 
				 <%
				   } 
				 %>		         		                   
		        </table>
		      </td>
		    </tr>
		  </table> 	 
	<%}%>
<%}%>

<%if (!request.getAttribute("flat02").equals("")){%>
  <h4>Detalle Morosidad</h4> 
   <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
      	<% 
			List<String> lmonedaM =  new ArrayList<String>();
      		List<BigDecimal> lmontoM =  new ArrayList<BigDecimal>();
      		List<Integer> lcantM =  new ArrayList<Integer>();
			//arreglo que contiene en la posicion un arreglo de los acreedores      		
      	    List<List<String>> lacreedorM =  new ArrayList<List<String>>();
      	          	
			List<Salida.ResultsMorosidad.Morosidad> listaM =null;
      		if (objSinac.getResultsMorosidad()!=null){
				listaM =  objSinac.getResultsMorosidad().getMorosidad();
      		}	      		      				
			if (listaM!=null && listaM.size()>0){
				//escribimos la tabla y el encabezado
	  	%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" width="15%">Fecha Venc.</td>
		             <td nowrap align="center" width="15%">Tipo Dcto.</td>
					 <td nowrap align="center" width="15%">Moneda</td>
					 <td nowrap align="center" width="15%">Monto</td>
					 <td nowrap align="center" width="20%">Emisor/Librador.</td>
					 <td nowrap align="center" width="20%">Localidad</td>
		         </tr>	  
	  	<%			
				for (Iterator<Salida.ResultsMorosidad.Morosidad> iterator = listaM.iterator(); iterator.hasNext();) {
					Salida.ResultsMorosidad.Morosidad object = iterator.next();
					 //acumulamos el monto, moneda y documento
					 String moneda_s = object.getIdDocumentoOrIdMonedaOrAportanteComercial().get(3).getValue().toString();
					 String emisor_s = object.getIdDocumentoOrIdMonedaOrAportanteComercial().get(2).getValue().toString();

					 int indice = lmonedaM.indexOf(moneda_s); 
					 				 
					 if (indice == -1){//no existe lo agregamos
					 	lmonedaM.add(moneda_s);
					 	lmontoM.add(object.getMonto());
					 	lcantM.add(1);
					 	List<String> li_acreedorM = new ArrayList<String>();
					 	li_acreedorM.add(emisor_s);
					 	lacreedorM.add(li_acreedorM);
					 }else{
					 	lmontoM.set(indice,lmontoM.get(indice).add(object.getMonto())) ;
					 	lcantM.set(indice,new Integer((lcantM.get(indice).intValue()+1))) ;
					 	List<String> li_acreedorM = lacreedorM.get(indice);
					 	if (!li_acreedorM.contains(emisor_s)){//lo agregamos
							li_acreedorM.add(emisor_s);
					 		lacreedorM.set(indice,li_acreedorM);					 		
					 	}
					 	
					 }
				 
					 
		%>
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center">
		             	<%=object.getIdDocumentoOrIdMonedaOrAportanteComercial().get(1).getValue()%><br>	
		             </td> 
		             <td nowrap align="center"><%=object.getDocumento()%></td>
					 <td nowrap align="center"><%=moneda_s%></td>
					 <td nowrap align="center"><%=formateador.format(object.getMonto())%></td>
					 <td nowrap align="center"><%=emisor_s%></td>
					 <td nowrap align="center"><%=object.getComuna()%></td>
		          </tr> 		
		<%				
				}
		%>
        		</table>
		<%						
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        		</table>
		<%	
			}			
      %>  
      </td>
    </tr>
  </table>  
  
<%
 if (listaM!=null && listaM.size()>0){
 %>
 	
 	 <h4>Resumen Morosidad</h4>
		<% row = 0;%>
		  <table  class="tableinfo">
		    <tr bordercolor="#FFFFFF"> 
		      <td nowrap>       
		        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
		
		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center"  width="30%">  Moneda</td>
		             <td nowrap align="center"  width="25%">  Monto Total</td>
		             <td nowrap align="center"  width="25%">  Nro. Documentos</td>
		             <td nowrap align="center"  width="45%">  Nro. Acreedores</td>
		         </tr>
				<% for (int c=0;c<lmonedaM.size();c++){
				 %>
			          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			             <td nowrap align="center"  ><%=lmonedaM.get(c) %></td>
			             <td nowrap align="center"  ><%=formateador.format(lmontoM.get(c))%></td>
			             <td nowrap align="center"  ><%=lcantM.get(c) %></td>
			             <td nowrap align="center"  ><%=lacreedorM.get(c).size()%></td>
			         </tr>				 
				 <%
				   } 
				 %>		         		                   
		        </table>
		      </td>
		    </tr>
		  </table> 	 
<%}%>
<%}%>
 
 <%if (!request.getAttribute("flat14").equals("")){%>
 <h4>Multados del Banco Central</h4> 
   <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
      	<% 
      	
      		List<Salida.ResultsMultadosBcoCentral.MultadosBcoCentral> listaMBC =null;
      		if (objSinac.getResultsMultadosBcoCentral()!=null){
				listaMBC =  objSinac.getResultsMultadosBcoCentral().getMultadosBcoCentral();      		
      		}			
						
			if (listaMBC!=null && listaMBC.size()>0){				      	
				//escribimos la tabla y el encabezado
	  	%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">	
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" width="15%">Fecha Circular</td>
					 <td nowrap align="center" width="15%">Moneda</td>		             
		             <td nowrap align="center" width="15%">Motivo</td>
					 <td nowrap align="center" width="15%">Monto</td>
					 <td nowrap align="center" width="20%">Nro Circular</td>
					 <td nowrap align="center" width="20%">Nro Infraccion</td>
		         </tr>	 				
	  	<%			
				for (Iterator<Salida.ResultsMultadosBcoCentral.MultadosBcoCentral> iterator = listaMBC.iterator(); iterator.hasNext();) {
					Salida.ResultsMultadosBcoCentral.MultadosBcoCentral object = iterator.next();

		%>								
 
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center"><%=object.getIFeCircular()%></td> 
		             <td nowrap align="center"><%=object.getIdMoneda()%></td>
					 <td nowrap align="center"><%=object.getMotivo()%></td>
					 <td nowrap align="center"><%=formateador.format(object.getMonto())%></td>
					 <td nowrap align="center"><%=object.getNumeroCircular()%></td>
					 <td nowrap align="center"><%=object.getNumeroInfraccion()%></td>
		          </tr> 		
		<%
			}
		%>
        		</table>
		<%							
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        		</table>
		<%	
			}			
      %>  
      </td>
    </tr>
  </table>   
<%}%>  
 
 <%
       		List<Salida.ResultsAvaldoBienesRaices.AvaldoBienesRaices> listaAval =null;
      		if (objSinac.getResultsAvaldoBienesRaices()!=null){
				listaAval =  objSinac.getResultsAvaldoBienesRaices().getAvaldoBienesRaices();      		
      		}	
  %>
  
  <%if (!request.getAttribute("flat32").equals("")){%>
   <h4>Detalle Avaluo de Bienes Raices (Cantidad Encontrada = <%=(listaAval!=null && listaAval.size()>0)?listaAval.size():"0"%>)</h4> 
   <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
      	<% 
			if (listaAval!=null && listaAval.size()>0){										      						
				//escribimos la tabla y el encabezado
	  	%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">	
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" width="25%">Manzana-predio</td>
					 <td nowrap align="center" width="20%">Comuna</td>		             
		             <td nowrap align="center" width="20%">Dirección</td>
					 <td nowrap align="center" width="10%">Tipo</td>
					 <td nowrap align="center" width=15%">Destino</td>
					 <td nowrap align="center" width="10%">Avaluo</td>					 
		         </tr>					
				
	  	<%			
				for (Iterator<Salida.ResultsAvaldoBienesRaices.AvaldoBienesRaices> iterator = listaAval.iterator(); iterator.hasNext();) {
					Salida.ResultsAvaldoBienesRaices.AvaldoBienesRaices obj = iterator.next();

		%>										  
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center"><%=obj.getManzana()%><%=(obj.getPredio()==null)?"":"-"+obj.getPredio()%></td> 
		             <td nowrap align="center"><%=obj.getComuna()%></td>
					 <td nowrap align="center"><%=obj.getDireccion()%></td>					 
					 <td nowrap align="center"><%=obj.getTipoPropiedad()%></td>
					 <td nowrap align="center"><%=obj.getDestino()%></td>
					 <td nowrap align="center"><%=formateador.format(obj.getAvaluo())%></td>					 
		          </tr> 		
		<%
			}
		%>
        		</table>
		<%		
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        		</table>
		<%	
			}			
      %>  
      </td>
    </tr>
  </table> 
<%}%>  
  
 
 <%
       	    List<Salida.ResultsPrendasSinDesplazamiento.PrendasSinDesplazamiento> listaPSD =null;
      		if (objSinac.getResultsPrendasSinDesplazamiento()!=null){
				listaPSD =  objSinac.getResultsPrendasSinDesplazamiento().getPrendasSinDesplazamiento();      		
      		}	
  %>
 <%if (!request.getAttribute("flat08").equals("")){%>
  <h4>Prendas Sin Desplazamiento (Cantidad Encontrada = <%=(listaPSD!=null && listaPSD.size()>0)?listaPSD.size():"0"%>)</h4> 
   <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
      	<%       	

			if (listaPSD!=null && listaPSD.size()>0){		
				//escribimos la tabla y el encabezado
	  	%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" width="15%">Fecha</td>
					 <td nowrap align="center" width="15%">Cuerpo</td>		             
		             <td nowrap align="center" width="15%">Pagina</td>
					 <td nowrap align="center" width="15%">Extracto</td>
					 <td nowrap align="center" width="40%">Acreedor</td>
		         </tr>	  
		         
		<%			
		for (Iterator<Salida.ResultsPrendasSinDesplazamiento.PrendasSinDesplazamiento> iterator = listaPSD.iterator(); iterator.hasNext();) {
			Salida.ResultsPrendasSinDesplazamiento.PrendasSinDesplazamiento obj = iterator.next();

		%>		         
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center"><%=obj.getIFeDiarioOficial()%></td> 
		             <td nowrap align="center"><%=obj.getCuerpo()%></td>
					 <td nowrap align="center"><%=obj.getPagina()%></td>					 
					 <td nowrap align="center"><%=obj.getExtracto()%></td>
					 <td nowrap align="center"><%=obj.getNombreAcreedor()%></td>
		          </tr> 		
		<%
			}
		%>
        		</table>
		<%				
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        </table>
		<%	
			}			
      %>  
      </td>
    </tr>
  </table>   
<%}%>  

<%if (!request.getAttribute("flat07").equals("")){%>
   <h4>Detalle De Infractores Laborales y Previsionales</h4> 
   <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
      	<%       	
			List<Salida.ResultsInfraccionesLaborales.ResultsDetalleInfraccionesLab.DetalleInfraccionesLab> listaIL =null;
      		if (objSinac.getResultsInfraccionesLaborales() !=null && objSinac.getResultsInfraccionesLaborales().getResultsDetalleInfraccionesLab() !=null){
				listaIL =  objSinac.getResultsInfraccionesLaborales().getResultsDetalleInfraccionesLab().getDetalleInfraccionesLab();
      		}	      		      				
			if (listaIL!=null && listaIL.size()>0){
				//escribimos la tabla y el encabezado
	  	%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" width="15%">Boletin</td>
		             <td nowrap align="center" width="15%">Tipo de<br>Infracción.</td>
					 <td nowrap align="center" width="15%">Nro Meses<br>Adeudados</td>
					 <td nowrap align="center" width="15%">Monto</td>
					 <td nowrap align="center" width="20%">Inspección</td>
					 <td nowrap align="center" width="20%">Resolución</td>
					 <td nowrap align="center" width="20%">Motivo</td>
					 <td nowrap align="center" width="20%">Institución</td>					 					 
		         </tr>	  
	  	<%			
				for (Iterator<Salida.ResultsInfraccionesLaborales.ResultsDetalleInfraccionesLab.DetalleInfraccionesLab> iterator = listaIL.iterator(); iterator.hasNext();) {
					Salida.ResultsInfraccionesLaborales.ResultsDetalleInfraccionesLab.DetalleInfraccionesLab object = iterator.next();

		%>
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center">
		             	Nro:<%=object.getNumBoletin()%><br> Pag:<%=object.getPaginaBoletin()%><br><%=object.getIFeBoletin()%>
		             </td> 
		             <td nowrap align="center"><%=object.getTipoInfraccion()%></td>
					 <td nowrap align="center"><%=object.getNumMesesAdeu()%></td>
					 <td nowrap align="center"><%=formateador.format(object.getMontoDeuImp())%></td>
					 <td nowrap align="center"> <%=object.getRegInspec()%><br> Nro:<%=object.getNumInspec()%></td>
					 <td nowrap align="center"> Año:<%=object.getAnoResol()%><br>Nro:<%=object.getNumResol()%></td>
					 <td nowrap align="center"><%=object.getMotivoInfrac()%></td>
					 <td nowrap align="center"><%=object.getNombreAFP()%></td>
		          </tr>
		<%
				}
		%>
        		</table>
		<%							
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        </table>
		<%	
			}			
      %>  
      </td>
    </tr>
  </table>  
<%}%>  
  

      	<% 			
			if (objSinac.getResultsInfraccionesLaborales() != null && 
					objSinac.getResultsInfraccionesLaborales().getInfraccionesLaborales()!=null){
				//escribimos la tabla y el encabezado
	  	%>
		 <h4>Resumen De Infractores Laborales y Previsionales</h4> 
		   <% row = 0;%>
		  <table  class="tableinfo">
		    <tr bordercolor="#FFFFFF"> 
		      <td nowrap> 	  	
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
					 <td nowrap align="center" width="25%">&nbsp;</td>		           
		             <td nowrap align="center" width="25%">Numero de Infracciones Previsionales</td>
		             <td nowrap align="center" width="25%">Numero de Infracciones Laborales</td>
					 <td nowrap align="center" width="25%">&nbsp;</td>
		         </tr>	  
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center">&nbsp;</td> 
		             <td nowrap align="center"><%=objSinac.getResultsInfraccionesLaborales().getInfraccionesLaborales().getNumDeudasPrev()%></td>
					 <td nowrap align="center"><%=objSinac.getResultsInfraccionesLaborales().getInfraccionesLaborales().getNumMultas()%></td>
					 <td nowrap align="center">&nbsp;</td>					 
		          </tr>
		        </table>
		      </td>
		    </tr>
		  </table>  
		<%				

			}
		%>
  
 

 <%if (!request.getAttribute("flat16").equals("")){%>
 <h4>Detalle Deuda del Sistema Financiero</h4>
  <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
      	<% 
			List list =null;
      		if (objSinac.getResultsDeudaSistemaFinanciero()!=null){
				list =  objSinac.getResultsDeudaSistemaFinanciero().getDeudaSistemaFinanciero();
      		}      	
			
			if (list!=null && list.size()>0){
				//escribimos la tabla y el encabezado
	  	%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             
		             <td nowrap align="center" width="7%">Fecha<br>Deuda.</td>
		             		             
					 <td nowrap align="center" width="7%">Directa<br>Vigente </td>
					 <td nowrap align="center" width="7%">Directa<br>Vencida</td>						 	
		             <td nowrap align="center" width="7%">Directa<br>Castigada</td>
		             					 
					 <td nowrap align="center" width="7%">Indirecta<br>Vigente </td>
					 <td nowrap align="center" width="7%">Indirecta<br>Vencida</td>					 	             
					 <td nowrap align="center" width="7%">Indirecta<br>Castigada</td>
					 
					 <td nowrap align="center" width="6%">Morosa</td>
					 	
					 <td nowrap align="center" width="7%">Comercial</td>
					 <td nowrap align="center" width="7%">Comercial<br>Vigente</td>
					 <td nowrap align="center" width="7%">Comercial<br>Vencida</td>

					 <td nowrap align="center" width="6%">Cred.<br>Consumo</td>
					 <td nowrap align="center" width="6%">Hipotec.</td>
					 <td nowrap align="center" width="6%">Inv.<br>Finan.</td>
					 <td nowrap align="center" width="6%">Pacto</td>	
					 					 
		         </tr>	  
	  	<%			
				for (Iterator iterator = list.iterator(); iterator.hasNext();) {
					Salida.ResultsDeudaSistemaFinanciero.DeudaSistemaFinanciero object = (Salida.ResultsDeudaSistemaFinanciero.DeudaSistemaFinanciero) iterator.next();
		%>
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             
		             <td nowrap align="center"><%=object.getIFechaDeuda()%></td>
		             
					 <td nowrap align="right"><%=formateador.format(object.getDeudaDirectaVigente())%></td>
					 <td nowrap align="right"><%=formateador.format(object.getDeudaVencidaDirecta())%></td>					 
		             <td nowrap align="right"><%=formateador.format(object.getDeudaCastigadaDirecta())%></td>
		             					 
					 <td nowrap align="right"><%=formateador.format(object.getDeudaIndirectaVigente())%></td>
					 <td nowrap align="right"><%=formateador.format(object.getDeudaIndirectaVencida())%></td>					 		             
					 <td nowrap align="right"><%=formateador.format(object.getDeudaCastigadaIndirecta())%></td>			
					 
					 <td nowrap align="right"><%=formateador.format(object.getDeudaMorosa())%></td>
					 	
				 	 <td nowrap align="right"><%=formateador.format(object.getDeudaComercial())%></td>
					 <td nowrap align="right"><%=formateador.format(object.getDeudaComercialVigenteMEx())%></td>				 	 
				 	 <td nowrap align="right"><%=formateador.format(object.getDeudaComercialVencidaMEx())%></td>
				 	 
				 	 <td nowrap align="right"><%=formateador.format(object.getDeudaCreditosConsumo())%></td>				 	 
				 	 <td nowrap align="right"><%=formateador.format(object.getDeudaHipotecarios())%></td>				 	 
				 	 <td nowrap align="right"><%=formateador.format(object.getDeudaInversionesFinancieras())%></td>					 	 			 	 
					 <td nowrap align="right"><%=formateador.format(object.getDeudaOperacionesConPacto())%></td>	
					 
		          </tr> 		
		<%				
				}		
		%>
        		</table>
		<%						
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        </table>
		<%	
			}			
      %>  
      </td>
    </tr>
  </table>  
 <%} %>
  
 <%}else{%>
  	<h3 align="center">
  		Consulta a Servicio no Devuelve Datos.<BR>
	  	VERIFIQUE CON EL ADMINISTRADOR QUE SERVICIO WEB ESTE FUNCIONAL.
  	</h3>
 <%} %>
    
</body>
</html>
