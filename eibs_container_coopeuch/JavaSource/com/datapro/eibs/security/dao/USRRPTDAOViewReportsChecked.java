/*
 * Created on june 5, 2011 by fhernandez
 */
package com.datapro.eibs.security.dao;

import com.datapro.access.Key;
import com.datapro.eibs.access.DAOViewSuper;
import com.datapro.eibs.constants.Reports;
import com.datapro.eibs.security.key.USRRPTKEYViewReportsChecked;

/**
 * @author fhernandez
 *
 */
public class USRRPTDAOViewReportsChecked extends DAOViewSuper {

	protected String getFindSql() {
		String sql =
			"SELECT RP.*, SG.USRRPN AS CHECKED " +
			"FROM IBSRPT RP " +
			"LEFT JOIN USRRPT SG ON RP.IBSRPN=SG.USRRPN AND SG.USRUID = ? " +
			"WHERE RP.IBSLIF LIKE ? AND RP.IBSMOD LIKE ? AND " +
			"	(RP.IBSPER LIKE ? OR RP.IBSPER='" + Reports.REPORT_PERIOD_ANY + "') " +
			"ORDER BY RP.IBSRPN";
		return sql;
	}

	protected Object[] getFindArguments(Key keyObj) {
		USRRPTKEYViewReportsChecked key = (USRRPTKEYViewReportsChecked) keyObj;
		Object[] args = { 
				key.getUSRUID(),
				key.getIBSLIF() == null ? "%" : key.getIBSLIF(),
				key.getIBSMOD() == null ? "%" : key.getIBSMOD(),
				key.getIBSPER() == null ? "%" : key.getIBSPER()};
		return args;
	}

	protected String getFindByPrimaryKeySql() {
		return null;
	}

	protected Object[] getFindByPrimaryKeyArguments(Key keyObj) {
		return null;
	}

}