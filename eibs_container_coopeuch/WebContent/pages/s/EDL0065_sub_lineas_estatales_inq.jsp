<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<html>
<head>
<title>Mantenedor de L&iacute;neas Estatales MYPE</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="msgSubLinEst" class="datapro.eibs.beans.EDL006501Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">



</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
  
     
%>


<H3 align="center">Mantenedor de L&iacute;neas Estatales MYPE<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="sub_lineas_estatales_inq, EDL0065"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0060" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="500">
  <input type=HIDDEN name="opt"> 
  <input type="hidden" name="E01MLNMDA" size="7"  value="<%= msgSubLinEst.getE01MLNMDA().trim()%>" readonly >
  
    
  <h4>Datos L&iacute;nea</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">L&iacute;nea :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01MLNSEQ" size="7"  value="<%= msgSubLinEst.getE01MLNSEQ().trim()%>" readonly >
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">SubL&iacute;nea
              </div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
              	<input type="text" name="E01MLNIND" size="7"  value="<%= msgSubLinEst.getE01MLNIND().trim()%>" readonly >
              </div>
            </td>
          </tr>

           <tr id="trclear"> 
            <td nowrap width="20%"> 
              <div align="right">Garantizador :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01MLNGTZ" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNGTZ().trim()%>" readonly  style="text-align:right;">
                <input type="text" name="E01MLNDGR" size="41"  value="<%= msgSubLinEst.getE01MLNDGR().trim()%>" readonly  >
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Programa :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E01MLNPGM" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNPGM().trim()%>" readonly >
                <input type="text" name="E01MLNDPG" size="41"  value="<%= msgSubLinEst.getE01MLNDPG().trim()%>" readonly  >
              </div>
            </td>
          </tr>
          
                 <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Estado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSTS1" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNSTS().trim()%>" readonly  >
                <input type="text" name="E01MLNDST1" size="41"  value="<%= msgSubLinEst.getE01MLNDST().trim()%>" readonly  >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Tasa :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTAS1" size="11" maxlength="10" value="<%= msgSubLinEst.getE01MLNTAS().trim()%>" readonly style="text-align:right;" >
              </div>
            </td>
          </tr> 

          <tr id="trclear"> 
            <td height="23"> 
              <div align="right">% Garant&iacute;a :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNGRT" size="11" maxlength="10" value="<%= msgSubLinEst.getE01MLNGRT().trim()%>" onkeypress="enterDecimal()" style="text-align:right;"  >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Segmento 1 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG1" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNSG1().trim()%>"  readonly>
                <input type="text" name="E01MLNDG1" size="41"  value="<%= msgSubLinEst.getE01MLNDG1().trim()%>" readonly  >
             </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Cupo L&iacute;nea :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCLI" size="20" maxlength="19" value="<%= Util.formatCCY(msgSubLinEst.getE01MLNCLI().trim())%>"  readonly style="text-align:right;">
             </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 2 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG2" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNSG2().trim()%>"  readonly>
                <input type="text" name="E01MLNDG2" size="41"  value="<%= msgSubLinEst.getE01MLNDG2().trim()%>" readonly  >
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Cupo L&iacute;nea Pesos :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCMN" size="20" maxlength="19" value="<%= Util.formatCCY(msgSubLinEst.getE01MLNCMN().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 3 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG3" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNSG3().trim()%>"  readonly>
                <input type="text" name="E01MLNDG3" size="41"  value="<%= msgSubLinEst.getE01MLNDG3().trim()%>" readonly  >
             </div>
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Saldo Disponible :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSDO" size="20" maxlength="19" value="<%= Util.formatCCY(msgSubLinEst.getE01MLNSDO().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 4 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG4" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNSG4().trim()%>"  readonly>
                <input type="text" name="E01MLNDG4" size="41"  value="<%= msgSubLinEst.getE01MLNDG4().trim()%>" readonly  >
            </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Saldo Retenido :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
              	<input type="text" name="E01MLNSDR" size="20" maxlength="19" value="<%= Util.formatCCY(msgSubLinEst.getE01MLNSDR().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 5 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG5" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNSG5().trim()%>"  readonly>
                <input type="text" name="E01MLNDG5" size="41"  value="<%= msgSubLinEst.getE01MLNDG5().trim()%>" readonly  >
              </div>
            </td>
          </tr>
          
		</table>
	  </td>	
	</tr>
  </table>	  
  <h4>Datos Sub L&iacute;nea</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          
          <tr id="trclear"> 
            <td height="23"> 
              <div align="right">Motivo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCDM" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNCDM().trim()%>" readonly  >
                <input type="text" name="E01MLNMOT" size="41"  value="<%= msgSubLinEst.getE01MLNMOT().trim()%>" readonly  >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left"> 
             </div>
            </td>
          </tr> 


          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Estado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSTS" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNSTS().trim()%>" readonly  >
                <input type="text" name="E01MLNDST" size="41"  value="<%= msgSubLinEst.getE01MLNDST().trim()%>" readonly  >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Tasa :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTAS" size="11" maxlength="10" value="<%= msgSubLinEst.getE01MLNTAS().trim()%>"  style="text-align:right;" readonly>
             </div>
            </td>
          </tr> 

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Fecha de Inicio :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNFVD" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNFVD().trim()%>" readonly>
                <input type="text" name="E01MLNFVM" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNFVM().trim()%>" readonly>
                <input type="text" name="E01MLNFVY" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNFVY().trim()%>" readonly>
             </div>
            </td>
            <td nowrap rowspan="3"> 
              <div align="right">Comentarios :</div>
            </td>
            <td nowrap rowspan="3"> 
              <div align="left"> 
                <textarea name="E01MLNGLS"  cols="80" rows="7" onKeyDown="textCounter(this,200)" onKeyUp="textCounter(this,200)" onkeypress="textCounter(this,200)" readonly><%=msgSubLinEst.getE01MLNGLS().trim()%></textarea>
              </div>
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Fecha de T&eacute;rmino :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNFTD" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNFTD().trim()%>" readonly>
                <input type="text" name="E01MLNFTM" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNFTM().trim()%>" readonly>
                <input type="text" name="E01MLNFTY" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNFTY().trim()%>" readonly>
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Fecha de T&eacute;rmino Final :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTRD" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNTRD().trim()%>" readonly>
                <input type="text" name="E01MLNTRM" size="3" maxlength="2" value="<%= msgSubLinEst.getE01MLNTRM().trim()%>" readonly>
                <input type="text" name="E01MLNTRY" size="5" maxlength="4" value="<%= msgSubLinEst.getE01MLNTRY().trim()%>" readonly>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>


  <div align="center">
 </div>
  </form>
  
</body>
</html>
