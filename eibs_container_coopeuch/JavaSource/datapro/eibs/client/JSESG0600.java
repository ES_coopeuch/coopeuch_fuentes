package datapro.eibs.client;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */ 
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Random;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;
import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.ECC150101Message;
import datapro.eibs.beans.ECC150102Message;
import datapro.eibs.beans.ECC150103Message;
import datapro.eibs.beans.ECO024002Message;
import datapro.eibs.beans.ECO060001Message;
import datapro.eibs.beans.ECO060002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESG060001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ExcelColStyle;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.ServiceLocator;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSESG0600 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_REP_VEN_SEG_ENTER = 100;
	protected static final int A_REP_VEN_SEG_LIST  = 200;
	
	protected static final int A_REP_VEN_SEG_LIST_PROCESS  = 300;
	protected static final int A_REP_VEN_SEG_REPORT  = 400;

	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		screen =  R_REP_VEN_SEG_ENTER;
			
		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}		
		
		switch (screen) {

		case R_REP_VEN_SEG_ENTER:
			procReqRepVenSegEnter(user, req, res, session);
			break;

		case A_REP_VEN_SEG_LIST:
			procActionRepVenSegList(user, req, res, session);
			break; 

		case A_REP_VEN_SEG_LIST_PROCESS:
			procActionRepVenSegListpProcess(user, req, res, session);
			break; 

		case A_REP_VEN_SEG_REPORT:
			procActionRepVenSegReport(user, req, res, session);
			break; 
			

		}		
	}
	
	
	protected void procReqRepVenSegEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		ESG060001Message msg = new ESG060001Message();
		ELEERRMessage msgError = new ELEERRMessage();
	
		try {
			ses.setAttribute("msgenter", msg);
			ses.setAttribute("error", msgError);
		
			flexLog("About to call Page: ESG0600_reporte_ventas_seguros_enter.jsp");
			forward("ESG0600_reporte_ventas_seguros_enter.jsp", req, res);
		
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}


	protected void procActionRepVenSegList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		ELEERRMessage msgError = new ELEERRMessage();
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			
			mp = getMessageProcessor("ESG0600", req);
			ESG060001Message msg = (ESG060001Message) mp.getMessageRecord("ESG060001", user.getH01USR(), "0001");
			
			setMessageRecord(req, msg);

			req.setAttribute("DIAI", msg.getE01PACFID());
			req.setAttribute("MESI", msg.getE01PACFIM());
			req.setAttribute("ANOI", msg.getE01PACFIY());
			
			req.setAttribute("DIAF", msg.getE01PACFHD());
			req.setAttribute("MESF", msg.getE01PACFHM());
			req.setAttribute("ANOF", msg.getE01PACFHY());
			
			mp.sendMessage(msg);
						
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { // if there are errors go back to first page
				session.setAttribute("error", error);
				
				flexLog("About to call Page: ESG0600_reporte_ventas_seguros_enter.jsp");
				forward("ESG0600_reporte_ventas_seguros_enter.jsp", req, res);
			} else {
			
				
     		JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
				
			session.setAttribute("msglist", list);
			forward("ESG0600_reporte_ventas_seguros_list.jsp", req, res);
			
			}
		} finally { 
			if (mp != null)
				mp.close();
		}
	}

	
	protected void procActionRepVenSegListpProcess(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {
		
	  	int intReg = 0;
	  	
		JBObjList list = (JBObjList) session.getAttribute("msglist");
		JBObjList listFilter = new JBObjList(); 
		//read list form selected.
		String[] valPosSelected=req.getParameterValues("E02PLHSEL");
		//read other fields
			
		MessageProcessor mp = null;
		try {
		  	mp = getMessageProcessor("ESG0600", req);
				
			String FDesD =  req.getParameter("E01PACFID");
			String FDesM =  req.getParameter("E01PACFIM");
			String FDesY =  req.getParameter("E01PACFIY");
			
			String FHasD =  req.getParameter("E01PACFHD");
			String FHasM =  req.getParameter("E01PACFHM");
			String FHasY =  req.getParameter("E01PACFHY");
			
		  	
		  	list.initRow();
			while (list.getNextRow()) {
				ESG060001Message forma = (ESG060001Message) list.getRecord();
				ESG060001Message msg = (ESG060001Message) mp.getMessageRecord("ESG060001", user.getH01USR(), "0002");
				msg.setE01PACPAC(forma.getE01PACPAC());
				
				
				msg.setE01PACFID(FDesD);
				msg.setE01PACFIM(FDesM);
				msg.setE01PACFIY(FDesY);
				msg.setE01PACFHD(FHasD);
				msg.setE01PACFHM(FHasM);
				msg.setE01PACFHY(FHasY);
				
				if (valPosSelected!=null)
					if(BuscaSel(valPosSelected, String.valueOf(intReg++))) 
						msg.setE01MARCA("*");

					mp.sendMessage(msg);					

					MessageRecord msg1 = mp.receiveMessageRecord();
			}//EoF					
		  }finally {
			if (mp != null)
				mp.close();
		  }

		  procActionRepVenSegList(user, req, res, session);
	}	
	
	//
	private boolean BuscaSel(String[] Str1, String Str2){
		
		boolean vretorno = false;
		
		for (int i = 0; i < Str1.length; i++) {
			if(Str2.equals(Str1[i]))
			{	
				vretorno = true;
				break;
			}
		}
		
		return vretorno;
		
	}
	//
	protected void procActionRepVenSegReport(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {
		
		try {
			
			String FecDes =  req.getParameter("E01PACFID") + "-" + req.getParameter("E01PACFIM") + "-" + req.getParameter("E01PACFIY");
			String FecHas =  req.getParameter("E01PACFHD") + "-" + req.getParameter("E01PACFHM") + "-" + req.getParameter("E01PACFHY"); 
			String URLReport = JSEIBSProp.getReportePromo() + "/ReportServer/Pages/ReportViewer.aspx?/InformesEIBS/Reporte_VtaSucursal&idUsuario=" + user.getH01USR() + "&centroCosto=" + user.getE01CCN() + "&Rut=" + user.getE01IDN() +"&codOficina=" + user.getE01UBR() + "&FechaDesde=" + FecDes + "&FechaHasta=" + FecHas +"&Oficina="+ user.getE01UBR();
			    flexLog("About to call Page: " + URLReport );
				res.sendRedirect(URLReport);

		} finally {
		}
	}

	
	//END
}
	



