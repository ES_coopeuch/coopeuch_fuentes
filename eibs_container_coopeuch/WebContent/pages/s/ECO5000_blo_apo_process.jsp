<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ECO001001Message"%>


<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Bloqueo Aporte Convenios</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">


<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "msg" class= "datapro.eibs.beans.ECO500001Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>	
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>



<script type="text/javascript">
</script>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Bloqueo Aporte Convenios
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"  name="EIBS_GIF" ALT="blo_apo_process.jsp, ECO5000"></h3>

<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO5000">
<input type="hidden" name="SCREEN" value="300"> 

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Empleador: 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="msg" property="E01BLOCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">Identificación:  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="msg" property="E01BLOCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Nombre: 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="msg" property="E01DESCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
  

	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="15%">Solicitud</th>
			<th align="center" nowrap width="30%">Cliente Convenio</th>
			<th align="center" nowrap width="15%">Numero de Cliente</th>
			<th align="center" nowrap width="15%">Estado</th>
			<th align="center" nowrap width="25%">Comentario</th>
		</tr>
		<tr>
			<td nowrap>
				<div align="right"><%=msg.getE01BLONUM()%><div>	
			</td>
			<td nowrap>
				<div align="right"><%=msg.getE01DESCUN()%><div>	
			</td>
			<td nowrap>
				<div align="right"><%=msg.getE01BLOCUN()%><div>	
			</td>
			<td nowrap>
				<div align="right"><%=msg.getE01BLOSTS()%><div>	
			</td>
			<td nowrap>
                <input type="text" name="E01BLOBLK" size="5" maxlength="4" value="<%=msg.getE01BLOBLK() %>" readonly >
                <input type="text" name="E01DESBLK" size="50" maxlength="45" value="<%=msg.getE01DESBLK() %>" readonly >
                <a href="javascript:GetCodeDescCNOFC('E01BLOBLK','E01DESBLK','BF')">
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
			</td>
		</tr>
	</table>

 <div align="left">
  <p align="center"><input id="EIBSBTN" type=submit name="Submit" value="<% if(msg.getE01BLOBLK().equals("0")) {out.print("Bloquear");} else  {out.print("Desbloquear");} %>"></p>
 </div> 


</table>
</form>
</body>
</html>
