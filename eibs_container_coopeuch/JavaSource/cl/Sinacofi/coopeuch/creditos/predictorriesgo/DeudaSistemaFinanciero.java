/**
 * DeudaSistemaFinanciero.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class DeudaSistemaFinanciero  implements java.io.Serializable {
    private java.math.BigInteger id;  // attribute
    private java.math.BigInteger IFechaDeuda;
    private java.math.BigDecimal deudaDirectaVigente;
    private java.math.BigDecimal deudaInversionesFinancieras;
    private java.math.BigDecimal deudaVencidaDirecta;
    private java.math.BigDecimal deudaOperacionesConPacto;
    private java.math.BigDecimal deudaIndirectaVigente;
    private java.math.BigDecimal deudaIndirectaVencida;
    private java.math.BigDecimal deudaComercial;
    private java.math.BigDecimal deudaCreditosConsumo;
    private java.math.BigInteger numAcreedoresCreditosConsumo;
    private java.math.BigDecimal deudaHipotecarios;
    private java.math.BigDecimal deudaMorosa;
    private java.math.BigDecimal deudaCastigadaDirecta;
    private java.math.BigDecimal deudaCastigadaIndirecta;
    private java.math.BigDecimal lineaCreditoDisponible;
    private java.math.BigDecimal deudaComercialVigenteMEx;
    private java.math.BigDecimal deudaComercialVencidaMEx;

    public DeudaSistemaFinanciero() {
    }

    public java.math.BigInteger getId() {
        return id;
    }

    public void setId(java.math.BigInteger id) {
        this.id = id;
    }

    public java.math.BigInteger getIFechaDeuda() {
        return IFechaDeuda;
    }

    public void setIFechaDeuda(java.math.BigInteger IFechaDeuda) {
        this.IFechaDeuda = IFechaDeuda;
    }

    public java.math.BigDecimal getDeudaDirectaVigente() {
        return deudaDirectaVigente;
    }

    public void setDeudaDirectaVigente(java.math.BigDecimal deudaDirectaVigente) {
        this.deudaDirectaVigente = deudaDirectaVigente;
    }

    public java.math.BigDecimal getDeudaInversionesFinancieras() {
        return deudaInversionesFinancieras;
    }

    public void setDeudaInversionesFinancieras(java.math.BigDecimal deudaInversionesFinancieras) {
        this.deudaInversionesFinancieras = deudaInversionesFinancieras;
    }

    public java.math.BigDecimal getDeudaVencidaDirecta() {
        return deudaVencidaDirecta;
    }

    public void setDeudaVencidaDirecta(java.math.BigDecimal deudaVencidaDirecta) {
        this.deudaVencidaDirecta = deudaVencidaDirecta;
    }

    public java.math.BigDecimal getDeudaOperacionesConPacto() {
        return deudaOperacionesConPacto;
    }

    public void setDeudaOperacionesConPacto(java.math.BigDecimal deudaOperacionesConPacto) {
        this.deudaOperacionesConPacto = deudaOperacionesConPacto;
    }

    public java.math.BigDecimal getDeudaIndirectaVigente() {
        return deudaIndirectaVigente;
    }

    public void setDeudaIndirectaVigente(java.math.BigDecimal deudaIndirectaVigente) {
        this.deudaIndirectaVigente = deudaIndirectaVigente;
    }

    public java.math.BigDecimal getDeudaIndirectaVencida() {
        return deudaIndirectaVencida;
    }

    public void setDeudaIndirectaVencida(java.math.BigDecimal deudaIndirectaVencida) {
        this.deudaIndirectaVencida = deudaIndirectaVencida;
    }

    public java.math.BigDecimal getDeudaComercial() {
        return deudaComercial;
    }

    public void setDeudaComercial(java.math.BigDecimal deudaComercial) {
        this.deudaComercial = deudaComercial;
    }

    public java.math.BigDecimal getDeudaCreditosConsumo() {
        return deudaCreditosConsumo;
    }

    public void setDeudaCreditosConsumo(java.math.BigDecimal deudaCreditosConsumo) {
        this.deudaCreditosConsumo = deudaCreditosConsumo;
    }

    public java.math.BigInteger getNumAcreedoresCreditosConsumo() {
        return numAcreedoresCreditosConsumo;
    }

    public void setNumAcreedoresCreditosConsumo(java.math.BigInteger numAcreedoresCreditosConsumo) {
        this.numAcreedoresCreditosConsumo = numAcreedoresCreditosConsumo;
    }

    public java.math.BigDecimal getDeudaHipotecarios() {
        return deudaHipotecarios;
    }

    public void setDeudaHipotecarios(java.math.BigDecimal deudaHipotecarios) {
        this.deudaHipotecarios = deudaHipotecarios;
    }

    public java.math.BigDecimal getDeudaMorosa() {
        return deudaMorosa;
    }

    public void setDeudaMorosa(java.math.BigDecimal deudaMorosa) {
        this.deudaMorosa = deudaMorosa;
    }

    public java.math.BigDecimal getDeudaCastigadaDirecta() {
        return deudaCastigadaDirecta;
    }

    public void setDeudaCastigadaDirecta(java.math.BigDecimal deudaCastigadaDirecta) {
        this.deudaCastigadaDirecta = deudaCastigadaDirecta;
    }

    public java.math.BigDecimal getDeudaCastigadaIndirecta() {
        return deudaCastigadaIndirecta;
    }

    public void setDeudaCastigadaIndirecta(java.math.BigDecimal deudaCastigadaIndirecta) {
        this.deudaCastigadaIndirecta = deudaCastigadaIndirecta;
    }

    public java.math.BigDecimal getLineaCreditoDisponible() {
        return lineaCreditoDisponible;
    }

    public void setLineaCreditoDisponible(java.math.BigDecimal lineaCreditoDisponible) {
        this.lineaCreditoDisponible = lineaCreditoDisponible;
    }

    public java.math.BigDecimal getDeudaComercialVigenteMEx() {
        return deudaComercialVigenteMEx;
    }

    public void setDeudaComercialVigenteMEx(java.math.BigDecimal deudaComercialVigenteMEx) {
        this.deudaComercialVigenteMEx = deudaComercialVigenteMEx;
    }

    public java.math.BigDecimal getDeudaComercialVencidaMEx() {
        return deudaComercialVencidaMEx;
    }

    public void setDeudaComercialVencidaMEx(java.math.BigDecimal deudaComercialVencidaMEx) {
        this.deudaComercialVencidaMEx = deudaComercialVencidaMEx;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        DeudaSistemaFinanciero history = (DeudaSistemaFinanciero) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        DeudaSistemaFinanciero other = (DeudaSistemaFinanciero) obj;
        boolean _equals;
        _equals = true
            && ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId())))
            && ((this.IFechaDeuda==null && other.getIFechaDeuda()==null) || 
             (this.IFechaDeuda!=null &&
              this.IFechaDeuda.equals(other.getIFechaDeuda())))
            && ((this.deudaDirectaVigente==null && other.getDeudaDirectaVigente()==null) || 
             (this.deudaDirectaVigente!=null &&
              this.deudaDirectaVigente.equals(other.getDeudaDirectaVigente())))
            && ((this.deudaInversionesFinancieras==null && other.getDeudaInversionesFinancieras()==null) || 
             (this.deudaInversionesFinancieras!=null &&
              this.deudaInversionesFinancieras.equals(other.getDeudaInversionesFinancieras())))
            && ((this.deudaVencidaDirecta==null && other.getDeudaVencidaDirecta()==null) || 
             (this.deudaVencidaDirecta!=null &&
              this.deudaVencidaDirecta.equals(other.getDeudaVencidaDirecta())))
            && ((this.deudaOperacionesConPacto==null && other.getDeudaOperacionesConPacto()==null) || 
             (this.deudaOperacionesConPacto!=null &&
              this.deudaOperacionesConPacto.equals(other.getDeudaOperacionesConPacto())))
            && ((this.deudaIndirectaVigente==null && other.getDeudaIndirectaVigente()==null) || 
             (this.deudaIndirectaVigente!=null &&
              this.deudaIndirectaVigente.equals(other.getDeudaIndirectaVigente())))
            && ((this.deudaIndirectaVencida==null && other.getDeudaIndirectaVencida()==null) || 
             (this.deudaIndirectaVencida!=null &&
              this.deudaIndirectaVencida.equals(other.getDeudaIndirectaVencida())))
            && ((this.deudaComercial==null && other.getDeudaComercial()==null) || 
             (this.deudaComercial!=null &&
              this.deudaComercial.equals(other.getDeudaComercial())))
            && ((this.deudaCreditosConsumo==null && other.getDeudaCreditosConsumo()==null) || 
             (this.deudaCreditosConsumo!=null &&
              this.deudaCreditosConsumo.equals(other.getDeudaCreditosConsumo())))
            && ((this.numAcreedoresCreditosConsumo==null && other.getNumAcreedoresCreditosConsumo()==null) || 
             (this.numAcreedoresCreditosConsumo!=null &&
              this.numAcreedoresCreditosConsumo.equals(other.getNumAcreedoresCreditosConsumo())))
            && ((this.deudaHipotecarios==null && other.getDeudaHipotecarios()==null) || 
             (this.deudaHipotecarios!=null &&
              this.deudaHipotecarios.equals(other.getDeudaHipotecarios())))
            && ((this.deudaMorosa==null && other.getDeudaMorosa()==null) || 
             (this.deudaMorosa!=null &&
              this.deudaMorosa.equals(other.getDeudaMorosa())))
            && ((this.deudaCastigadaDirecta==null && other.getDeudaCastigadaDirecta()==null) || 
             (this.deudaCastigadaDirecta!=null &&
              this.deudaCastigadaDirecta.equals(other.getDeudaCastigadaDirecta())))
            && ((this.deudaCastigadaIndirecta==null && other.getDeudaCastigadaIndirecta()==null) || 
             (this.deudaCastigadaIndirecta!=null &&
              this.deudaCastigadaIndirecta.equals(other.getDeudaCastigadaIndirecta())))
            && ((this.lineaCreditoDisponible==null && other.getLineaCreditoDisponible()==null) || 
             (this.lineaCreditoDisponible!=null &&
              this.lineaCreditoDisponible.equals(other.getLineaCreditoDisponible())))
            && ((this.deudaComercialVigenteMEx==null && other.getDeudaComercialVigenteMEx()==null) || 
             (this.deudaComercialVigenteMEx!=null &&
              this.deudaComercialVigenteMEx.equals(other.getDeudaComercialVigenteMEx())))
            && ((this.deudaComercialVencidaMEx==null && other.getDeudaComercialVencidaMEx()==null) || 
             (this.deudaComercialVencidaMEx!=null &&
              this.deudaComercialVencidaMEx.equals(other.getDeudaComercialVencidaMEx())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        DeudaSistemaFinanciero history = (DeudaSistemaFinanciero) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getIFechaDeuda() != null) {
            _hashCode += getIFechaDeuda().hashCode();
        }
        if (getDeudaDirectaVigente() != null) {
            _hashCode += getDeudaDirectaVigente().hashCode();
        }
        if (getDeudaInversionesFinancieras() != null) {
            _hashCode += getDeudaInversionesFinancieras().hashCode();
        }
        if (getDeudaVencidaDirecta() != null) {
            _hashCode += getDeudaVencidaDirecta().hashCode();
        }
        if (getDeudaOperacionesConPacto() != null) {
            _hashCode += getDeudaOperacionesConPacto().hashCode();
        }
        if (getDeudaIndirectaVigente() != null) {
            _hashCode += getDeudaIndirectaVigente().hashCode();
        }
        if (getDeudaIndirectaVencida() != null) {
            _hashCode += getDeudaIndirectaVencida().hashCode();
        }
        if (getDeudaComercial() != null) {
            _hashCode += getDeudaComercial().hashCode();
        }
        if (getDeudaCreditosConsumo() != null) {
            _hashCode += getDeudaCreditosConsumo().hashCode();
        }
        if (getNumAcreedoresCreditosConsumo() != null) {
            _hashCode += getNumAcreedoresCreditosConsumo().hashCode();
        }
        if (getDeudaHipotecarios() != null) {
            _hashCode += getDeudaHipotecarios().hashCode();
        }
        if (getDeudaMorosa() != null) {
            _hashCode += getDeudaMorosa().hashCode();
        }
        if (getDeudaCastigadaDirecta() != null) {
            _hashCode += getDeudaCastigadaDirecta().hashCode();
        }
        if (getDeudaCastigadaIndirecta() != null) {
            _hashCode += getDeudaCastigadaIndirecta().hashCode();
        }
        if (getLineaCreditoDisponible() != null) {
            _hashCode += getLineaCreditoDisponible().hashCode();
        }
        if (getDeudaComercialVigenteMEx() != null) {
            _hashCode += getDeudaComercialVigenteMEx().hashCode();
        }
        if (getDeudaComercialVencidaMEx() != null) {
            _hashCode += getDeudaComercialVencidaMEx().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
