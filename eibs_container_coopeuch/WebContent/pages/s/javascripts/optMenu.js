function getHelp() {

 switch (optHelp) {
   case 1 :
      GetAccount(fieldName,fieldBank,fieldOPCode,'');
      break;
   case 2 :
      GetLedger(fieldName,fieldBank,fieldCCY,'',fieldHlp1);
      break;
   case 3 :
      GetBranch(fieldName,fieldBank);
      break;
   case 4 :
      GetCustomerDescId(fieldName,fieldHlp1,fieldHlp2);
      break;
   case 5 :
      GetCurrency(fieldName,fieldBank);
      break;
   case 6 :
      GetConcept(fieldName,fieldBank,fieldOPCode,fieldHlp1,fieldHlp2);
      break;
   case 7 :
      GetCodeCNOFC(fieldName,fieldOPCode);
      break;
   case 8 :
      GetCostCenter(fieldName,fieldBank);
      break;
   case 9 :
      GetAccountHolders(fieldName);
      break;
   case 10 :
      GetCodeDescCNOFC(fieldName, fieldHlp1, fieldOPCode);
      break;
   case 11 :
      GetCasualTable(fieldName,fieldHlp1);
      break;
   case 12 :
      GetConceptTr(fieldName,fieldBank,fieldOPCode,fieldHlp1,fieldHlp2);
      break;
   case 13 :
      GetCode(fieldName,'STATIC_inv_factor.jsp');
      break;
   case 14 :
      GetCode(fieldName,'STATIC_inv_frecuency.jsp');
      break;
   case 15 :
      GetCode(fieldName,'STATIC_doc_frecuency.jsp');
      break;
   case 16 :
      GetCode(fieldName,'STATIC_doc_type.jsp');
      break;
   case 17 :
      GetCode(fieldName,'STATIC_ln_tables_fee_charges.jsp');
      break;
   case 18 :
      GetCode(fieldName,'STATIC_ln_tables_frec_charges.jsp');
      break;
   case 19 :
      GetCode(fieldName,'STATIC_concepts_type.jsp');
      break;
   case 20 :
   	  GetLNReference(fieldName,fieldBank,fieldOPCode,'',fieldHlp1);
   	  break;
    case 21 :
      if (fieldHlp2 == "03") {
      		GetWireOpening(fieldName,fieldHlp1);
      } else {
      		GetAccount(fieldName,fieldBank,fieldOPCode,'');
      }
      break;
   case 25 : 
      GetAccByClient(fieldName, fieldBank, fieldOPCode, '', fieldHlp1);
      break;   
   case 26 : 
      GetAccountCustomer(fieldName, fieldBank, fieldOPCode, '', fieldHlp1);
      break;   
 }
}

function setTime(maxlen) {
  clearTimeout(outtime2);
  ontime=setInterval("AnimationMenu("+maxlen+")",1);
}

function startMove(){
  if (curlen == 0) {
    var maxlen = menuRef.clientWidth;
    if (menuactive) {
    	setVisibility(menuRef, "hidden");
     	outtime2 = setTimeout("setTime("+maxlen+")",1000);
   }else {
      setTime(maxlen);
   }
 }
}

function AnimationMenu(maxlen){
  var x = menuImgRef.offsetLeft;
  var x2 = x + menuImgRef.clientWidth + 2;
  var w = menuImgRef.clientWidth + 2 + menuRef.clientWidth;

 animactive = true;
 curlen = curlen + 10;
 clearTimeout(outtime);
 if (curlen < maxlen) {
  x = (!menuactive) ? x - 10 : x + 10;
  x2 = (!menuactive) ? x2 - 10 : x2 + 10;
  menuImgRef.style.left = x + "px";
  checkCombo(x,menuImgRef.offsetTop,w,menuRef.clientHeight);
  }
 else {
   clearInterval(ontime);
   curlen = 0;
   var rest= (maxlen % 10) + 2;
   if (menuactive == true) {
    x += rest;
    menuImgRef.style.left= x + "px";
    menuactive = false;
    animactive = false;
    checkCombo(x,menuImgRef.offsetTop,menuImgRef.clientWidth,menuImgRef.clientHeight);
    }
   else{
    x -= rest;
    x2 -= rest;
    menuImgRef.style.left= x + "px";
    menuRef.style.left= x2 + "px";

    checkCombo(x,menuImgRef.offsetTop,w,menuRef.clientHeight);
	setVisibility(menuRef, "visible");

    menuactive = true;
    outtime = setTimeout("startMove()",20000);}
 }

}


function MoveMenu(){
 var obj = document.body;
 var y = obj.scrollTop + 2;
 var x = obj.clientWidth - menuImgRef.clientWidth - 2 + obj.scrollLeft;
 var x2 = 0;

if (!animactive){
   menuImgRef.style.top=y + "px";
   menuRef.style.top= y + "px";
   menuImgRef.style.left= x + "px";
   checkCombo(x,y,menuImgRef.clientWidth,menuImgRef.clientHeight);
   menuactive = false;
  }
 else if (curlen ==0) {
   x -= menuRef.clientWidth + 2;
   x2 += x + menuImgRef.clientWidth + 2;
   menuImgRef.style.top= y  + "px";
   menuRef.style.top= y  + "px";
   menuImgRef.style.left=x + "px";
   menuRef.style.left=x2 + "px";
   checkCombo(x,y,menuImgRef.clientWidth + menuRef.clientWidth + 2,menuRef.clientHeight);
   }

}

function setCapture(element) {
	if (!element.setCapture) {
	    var capture = ["click", "mousedown", "mouseup", "mousemove", "mouseover", "mouseout" ];

	    element.setCapture = function(){
	        var self = this;
	        var flag = false;
	        this._capture = function(e){
	            if (flag) {return}
	            flag = true;
	            var event = document.createEvent("MouseEvents");
	            event.initMouseEvent(e.type,
	                e.bubbles, e.cancelable, e.view, e.detail,
	                e.screenX, e.screenY, e.clientX, e.clientY,
	                e.ctrlKey, e.altKey, e.shiftKey, e.metaKey,
	                e.button, e.relatedTarget);
	            self.dispatchEvent(event);
	            flag = false;
	        };
	        for (var i=0; i<capture.length; i++) {
	            window.addEventListener(capture[i], this._capture, true);
	        }
	    };
	}
	element.setCapture();
}

function releaseCapture(element) {
	if (!element.releaseCapture) {
	    var capture = ["click", "mousedown", "mouseup", "mousemove", "mouseover", "mouseout" ];

	    element.releaseCapture = function(){
	        for (var i=0; i<capture.length; i++) {
	            window.removeEventListener(capture[i], this._capture, true);
	        }
	        this._capture = null;
	    };
	}
	element.releaseCapture();
}

function switchMenu(evt) {
	var elem = getEventElement(evt);

  if (popupStatus=="open"){
    popupStatus="close";
    setCapture(hPopupRef);
    //hPopupRef.setCapture();
   }
   if (elem.className=="menuItem") {
      elem.className="highlightItem";
   } else if (elem.className=="highlightItem") {
      elem.className="menuItem";
   }
}

function doCut() {
 var el = document.forms[0][fieldName];
 if (doCopy()) { el.value = "";}
}

function doCopy() {
 var el = document.forms[0][fieldName];
 var bResult = window.clipboardData.setData("Text",el.value);
 return bResult;
}

function doPaste() {
 var el = document.forms[0][fieldName];
 el.value = window.clipboardData.getData("Text");
}

function doDelete() {
 var el = document.forms[0][fieldName];
 el.value = "";
}

function hideMenu() {
 hPopupRef.style.display="none";
}

function clickMenu(evt) {

   hPopupRef.style.display="none";
   //hPopupRef.releaseCapture();
   releaseCapture(hPopupRef);

   el=getEventElement(evt);
   document.forms[0][fieldName].style.cursor="auto";
   if (el.id=="mnuHelp") {
       getHelp();
   } else if (el.id=="mnuCut") {
       doCut();
   } else if (el.id=="mnuCopy") {
       doCopy();
   } else if (el.id=="mnuPaste") {
       doPaste();
   } else if (el.id=="mnuDelete") {
       doDelete();
   }
}

function builtHPopUp() {
	var styleHtml = "<STYLE>" +
		".menuItem {" +
		"	font-family:sans-serif;" +
		"	font-size:8pt;" +
		"	width:80;" +
		"	padding:2;" +
		"	padding-left:20;" +
		"	background-Color:menu;" +
		"	color:black;" +
		"	cursor:default" +
		"} " +
	   	".highlightItem {" +
	   	"	font-family:sans-serif;" +
	   	"	font-size:8pt;" +
	   	"	width:80;" +
	   	"	padding:2;" +
	   	"	padding-left:20;" +
	   	"	background-Color:highlight;" +
	   	"	color:white;" +
	   	"	cursor:default" +
	   	"} " +
	   	" </STYLE>";
	document.write(styleHtml);

	var divHtml = "<div id=\"hPopup\" " +
			"			style=\"position:absolute; " +
			"			display:none; " +
			"			left:25px; " +
			"			top:-50px; " +
			"			z-index:3; " +
			"			padding:1; " +
			"			background-Color:menu; " +
			"			border-width:thin; " +
			"			border-style:outset; " +
			"			cursor:default\">" +
	        "			<div class=\"menuItem\" id=mnuHelp style=\"border-bottom-width:thin; border-bottom-style:groove;\">Help</div>"+
		    "			<div class=\"menuItem\" id=mnuCut>Cut</div>"+
		    "			<div class=\"menuItem\" id=mnuCopy>Copy</div>"+
		    "			<div class=\"menuItem\" id=mnuPaste>Paste</div>"+
		    "			<div class=\"menuItem\" id=mnuDelete>Delete</div>"+
	  	    "		</div>";
	document.write(divHtml);

	var switchHtml ="<SCRIPT language=\"JavaScript\">"+
		    "			document.getElementById(\"hPopup\").onmousedown = clickMenu;" +
		    "			document.getElementById(\"hPopup\").onmouseover = switchMenu;" +
		    "			document.getElementById(\"hPopup\").onmouseout = switchMenu;" +
		    "		</SCRIPT>";
	document.write(switchHtml);

	hPopupRef = document.getElementById("hPopup");
	mnuHelpRef = document.getElementById("mnuHelpRef");
}


function menuCallback(){
	var xmlResponse = XMLResponse(CallbackObject.ajax);
	if(isValidObject(xmlResponse)){
		var xmlSerial;
		if(isValidObject(window.XMLSerializer)){
			xmlSerial = new XMLSerializer();
		}
		var options = (xmlResponse.xml ? xmlResponse.xml : xmlSerial.serializeToString(xmlResponse)); 
		options = options.substring(options.indexOf("<root>"));
		options = options.replace(new RegExp("<A","g"), "<BR><A");
		options = options.substring(options.indexOf("<A"));
		options = options.replace("</root>", "");
		options = options.replace(new RegExp("<hr /><BR />","g"), "<hr align=\"center\" style=\"height:1pt;color:navy\">");
		writeNewMenu(options);
	} 
}

function builtNewMenu(options) {
	var action = webapp + "/servlet/datapro.eibs.menu.JSOptMenu?optMenu="+options;
	GetXMLResponse(action, menuCallback, false);
}

function writeNewMenu(options) {
var divHtml = "<div id=\"Menu\" class=\"hiddenDiv\" onclick=\"cancelBub()\">"+
              "<table class=\"optMenuTB\">"+
    	      "<td align=\"left\" nowrap style=\"padding-right:9.5pt;\">"+ options +
    	      "</td>"+
              "</table>"+
  	      "</div><BR>";
var divImg = "<div id=\"MenuImg\" class=\"hiddenDivImg\" >"+
             "<IMG SRC = '" + webapp + "/images/s/options.gif' onmouseover=\"startMove()\">"+
  	     "</div>";

document.write(divImg);
document.write(divHtml);

var hideHtml = "<SCRIPT language=\"JavaScript\">"+
	       "document.onclick= closeMenu;"+
	       "</SCRIPT>";
document.write(hideHtml);

 menuImgRef = document.getElementById("MenuImg");
 menuRef = document.getElementById("Menu");
}

function showPopupHelp(evt) {
	if (hPopupRef != null) {
		evt = (evt) ? evt : ((window.event) ? window.event : "");
	    var y = evt.clientY + document.body.scrollTop - 1;
	    var x = evt.clientX + document.body.scrollLeft - 1;
	    var bodyW = document.body.clientWidth;
	    var bodyH = document.body.clientHeight;
	    var dif = 0;
	    hPopupRef.style.display = "";
	    if (bodyH < (evt.clientY + hPopupRef.offsetHeight)) {
	        dif = (evt.clientY + hPopupRef.offsetHeight) - bodyH;
	        y= y - dif - 5;
	    }	 
	    if (bodyW < (evt.clientX + hPopupRef.offsetWidth)) {
	         dif = (evt.clientX + hPopupRef.offsetWidth) - bodyW;
	         x= x - dif - 5;
	    }
	    if(evt.srcElement) {
		     evt.returnValue = false;
	    } else {
		     evt.preventDefault();
	    }
	    popupStatus = "open";
	    x = x - 3;
	    y = y - 3;
	    hPopupRef.style.top = y + "px";
	    hPopupRef.style.left = x + "px";
	}
}

function closeMenu(){
 if (menuactive == true) startMove();
}

function initMenu(){
	if(menuImgRef){
		var gentime = "";

		menuImgRef.style.visibility="visible";
		gentime = setInterval('MoveMenu()',10);

		applyTrans(menuRef);
	}
}


function init(opt,name,bank,ccy,fieldname1,fieldname2,opcode) {
 fieldName = name;
 fieldBank = bank;
 fieldCCY = ccy;
 optHelp = opt;
 fieldHlp1 = fieldname1;
 fieldHlp2 = fieldname2;
 fieldOPCode = opcode;
}

function getPdfForms(url) {
	CenterNamedWindow(
		url,
		'pdf',
		800,
		600,
		2);
}

function confirmDelete(url) {
	if (confirm("Esta usted seguro que desea eliminar este registro?")) {
		window.location.href = url;
	} 
}	

//Menu Options

// Corporative Customer option

client_corp_opt='client_corp_opt';

// Inquiry Corporative Customer option

client_inq_corp_opt='client_inq_corp_opt';

// Personal Customer option

client_personal_opt='client_personal_opt';

// Inquiry Personal Customer option

client_inq_personal_opt='client_inq_personal_opt';

// CD maintenance

cd_m_opt='cd_m_opt';

// CD Transactions

cd_t_m_opt='cd_t_m_opt';


// CD inquiry

cd_i_opt='cd_i_opt';


// Retail Accounts maintenance

rt_m_opt='rt_m_opt';

// Retail Accounts condition's document

rt_m_opt_f='rt_m_opt_f';

rt_i_opt_f='rt_i_opt_f';

sv_m_opt_f='sv_m_opt_f';

sv_i_opt_f='sv_i_opt_f';

cpar_m_opt_f='cpar_m_opt_f';

cpar_i_opt_f='cpar_i_opt_f';

// Retail Accounts condition's document

rt_m_opt_c='rt_m_opt_c';

sv_m_opt_c='sv_m_opt_c';

cpar_m_opt_c='cpar_m_opt_c';

// Retail Account inquiry

rt_i_opt='rt_i_opt';

// Savings Accounts maintenance

sv_m_opt='sv_m_opt';

cpar_m_opt='cpar_m_opt';

// Savings Documents Signers

sv_m_opt_d='sv_m_opt_d';

cpar_m_opt_d='cpar_m_opt_d';

// Savings Account inquiry

sv_i_opt='sv_i_opt';

cpar_i_opt='cpar_i_opt';

// Loans maintenance

ln_m_opt='ln_m_opt';


// Loans new

ln_n_opt='ln_n_opt';

  // Loans transactions

ln_t_m_opt='ln_t_m_opt';

// Loans inquiry

ln_i_1_opt='ln_i_1_opt';


ln_i_2_opt='ln_i_2_opt';

 // ejecutivos

ej_option='ej_option';
 // Credit Proposal

pc_option='pc_option';


 // Credit Proposal Header

pc_optionHeader='pc_optionHeader';

 // Credit Proposal Garantias/Avales

pc_optionGaran='pc_optionGaran';

pc_limits='pc_limits';

pc_docs='pc_docs';

// Credit cards maintenance

cc_m_opt='cc_m_opt';

// Credit cards inquiry

cc_i_opt='cc_i_opt';

// Debit cards inquiry

dc_i_opt='dc_i_opt';

// Collections inquiry

coll_i_opt='coll_i_opt';

// Letter of Credit inquiry

lc_i_opt='lc_i_opt';
  
// Letter of Credit Transfers 
lc_transfer='lc_transfer';

lc_approval_transfer='lc_approval_transfer';

// Letter of Credit Opening (Apertura)

lc_opening='lc_opening';

// Stand By Opening (Apertura)

sb_opening='sb_opening';

// Letter of Credit Approbal

lc_approbal_detail='lc_approbal_detail';

lc_maint_detail='lc_maint_detail';


// LETTER OF CREDIT APPROVAL

		// COMERCIAL NEW
lc_apr_cc_new='lc_apr_cc_new';

		// COMERCIAL MAINTENANCE
lc_apr_cc_maint='lc_apr_cc_maint';

		// COMERCIAL TRANSFER
lc_apr_tranfer_cc_maint='lc_apr_tranfer_cc_maint';

		// STANDBY
lc_apr_standby='lc_apr_standby';


lc_transfer_detail='lc_transfer_detail';

// Letter of Credit Maintenance

lc_maint='lc_maint';

// Letter of Credit Nego Approval

lc_nego_apr='lc_nego_apr';


// Documentary Debt Collections Maintenance

dc_d_maint='dc_d_maint';

// Single Debt Collections Maintenance

dc_s_maint='dc_s_maint';

// Documentary Debt Collections Approval Inquiry

dc_d_inquiry='dc_d_inquiry';

// Single Debt Collections Approval Inquiry

dc_s_inquiry='dc_s_inquiry';


// Line of Credit approval

cl_a_opt='cl_a_opt';

// Line of Credit inquiry
cl_i_opt='cl_i_opt';

ecif10_i_opt='ecif10_i_opt';

//ECIF170


ecif170_i_opt='ecif170_i_opt';

//Approvals

//CD approvals

cd_a_opt='cd_a_opt';

//Loans Approvals

ln_a_opt='ln_a_opt';

//Loans Transactions Approval
ln_t_a_opt='ln_t_a_opt';

//CD Transactions Approval
cd_t_a_opt='cd_t_a_opt';

//Credit Cards Approvals

cc_a_opt='cc_a_opt';

// Retail Accounts Approvals

rt_a_opt='rt_a_opt';

//Savings Accounts Approvals

sv_a_opt='sv_a_opt';

cpar_a_opt='cpar_a_opt';

  // Approval Corporative Customer

client_ap_corp_opt='client_ap_corp_opt';

  // Approval Personal Customer option

client_ap_personal_opt='client_ap_personal_opt';

//Collaterals Maintenance

colla_M_opt='colla_M_opt';

//Collaterals Approval

colla_A_opt='colla_A_opt';


//collaterals Inquiry

colla_i_opt='colla_i_opt';

//Paying and Receiving Swift

tranS_i_opt='tranS_i_opt';

//Paying and Receiving Fed

tranF_i_opt='tranF_i_opt';

//Paying and Receiving Telex

tranT_i_opt='tranT_i_opt';

//Official Checks

//Print from Maintenance

of_p_opt='of_p_opt';

//No Print from Maintenance

of_np_opt='of_np_opt';

//Approval

of_a_opt='of_a_opt';

//Print
//Official Check Printing Using forms
of_p1_opt='of_p1_opt';

//Official Check Printing using PDF 
of_p2_opt='of_p2_opt';

//Descuento Documentos
dft_m_opt='dft_m_opt';

dft_i_opt='dft_i_opt';

dft_a_opt='dft_a_opt';

// Boleta Garantia maintenance 25/09/2002

bg_m_opt='bg_m_opt';

bg_i_opt='bg_i_opt';

//Aprobacion de Tesoreria
cv_a_opt='cv_a_opt';

//Financial Transaction
pr_a_opt='pr_a_opt';

pr_r_opt='pr_r_opt';

pr_m_opt='pr_m_opt';

pr_inq_opt='pr_inq_opt';

//Foreign Exchange

fx_i_opt='fx_i_opt';



//Foreign Exchange

//CDS
cd_bo_opt='cd_bo_opt';

cd_bo_act_opt='cd_bo_act_opt';

//TDS
td_bo_opt='td_bo_opt';

td_bo_act_opt='td_bo_act_opt';

//FFS
ff_bo_opt='ff_bo_opt';

ff_bo_act_opt='ff_bo_act_opt';

//TPS
tp_bo_opt='tp_bo_opt';

tp_bo_act_opt='tp_bo_act_opt';


//ACC
ac_bo_opt='ac_bo_opt';

ac_bo_act_opt='ac_bo_act_opt';


//Spot
sf_bo_opt='sf_bo_opt';

//Forward
fw_bo_opt='fw_bo_opt';

//NDF
nd_bo_opt='nd_bo_opt';

//Option
op_bo_opt='op_bo_opt';

//Swap
sw_bo_opt='sw_bo_opt';

//PLP
cp_bo_opt='cp_bo_opt';

//FRA
fra_bo_opt='fra_bo_opt';


//Maintenance

//Swap
swm_bo_opt='swm_bo_opt';

//Option
opm_bo_opt='opm_bo_opt';

//NDF
ndm_bo_opt='ndm_bo_opt';

//Forward
fwm_bo_opt='fwm_bo_opt';

//Spot
sfm_bo_opt='sfm_bo_opt';

//Approval
//Swap
swa_bo_opt='swa_bo_opt';

//Option
opa_bo_opt='opa_bo_opt';

//NDF
nda_bo_opt='nda_bo_opt';

//Forward
fwa_bo_opt='fwa_bo_opt';

//Spot
sfa_bo_opt='sfa_bo_opt';

//Deals

cdt_a_opt='cdt_a_opt';

cdt_a_act_opt='cdt_a_act_opt';

//FRA

//FRA
fra_app_opt='fra_app_opt';


// Deals back office maintenance

cdm_m_opt='cdm_m_opt';

cdm_m_act_opt='cdm_m_act_opt';

//Foreign Exchange Inquiry

//Option
opi_bo_opt='opi_bo_opt';

//NDF
ndi_bo_opt='ndi_bo_opt';

//Forward
fwi_bo_opt='fwi_bo_opt';

//Spot
sfi_bo_opt='sfi_bo_opt';

//Payment Approval
swf_a_opt='swf_a_opt';

//Treasury Deals Inquiry
cdt_i_opt='cdt_i_opt';


//Investments Instruments
inst_basic_opt='inst_basic_opt';

inst_inq_opt='inst_inq_opt';

inst_app_opt='inst_app_opt';

trade_a_ticket_opt='trade_a_ticket_opt';

inv_port_opt='inv_port_opt';

inv_i_port_opt='inv_i_port_opt';

inv_a_port_opt='inv_a_port_opt';

coupon_ap_opt='coupon_ap_opt';

free_a_opt='free_a_opt';

cr_i_opt='cr_i_opt';

prd_loan_opt='prd_loan_opt';


// CP maintenance

cp_m_opt='cp_m_opt';

// CP Inquiry

cp_i_opt='cp_i_opt';

//Approval

// CP approvals

cp_a_opt='cp_a_opt';


//Sistema de Bastanteo
bastanteo_menu_1='bastanteo_menu_1';

bastanteo_menu_1_1='bastanteo_menu_1_1';

bastanteo_menu_2='bastanteo_menu_2';

bastanteo_menu_3='bastanteo_menu_3';

bastanteo_menu_inq_1='bastanteo_menu_inq_1';

bastanteo_menu_inq_1_1='bastanteo_menu_inq_1_1';

bastanteo_menu_inq_2='bastanteo_menu_inq_2';

bastanteo_menu_inq_3='bastanteo_menu_inq_3';
   	
   	// Line of Credit maintenance

cl_m_opt='cl_m_opt';

// Line of Credit inquiry

cl_i_opt='cl_i_opt';

// Line of Credit approval

cl_a_opt='cl_a_opt';

// Loan constructor

pc_m_opt='pc_m_opt';

// Loan constructor inquiry

pc_i_opt='pc_i_opt';

// Loan constructor approval

pc_a_opt='pc_a_opt';


// Menu INQUIRY covenant

covenat_qry_opt='covenat_qry_opt';

//Menu PlanSocio

Plan_Socio_Con='Plan_Socio_Con';
   	

function callBastanteo() {

    	page = webapp + "/servlet/datapro.eibs.bastanteo.JSECU0000?SCREEN=700";
    	CenterNamedWindow(page,'Information',650,500,2);
}



//end help menu def

// Functions adde from optMenu_Extended

