<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Parametros Boletin Comercial</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="bolDetails" class="datapro.eibs.beans.EUU002001Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
%>


<H3 align="center">Mantenimiento Parametros Envio a Boletin Comercial <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="param_boletin_comercial_maintenance.jsp, EUU0020"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEUU0020" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  
 <h4>Parametros Dias</h4>  
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Dias Planilla Socio :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <eibsinput:text name="bolDetails" property="E01BOLDPL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_REFERENCE %>" size="4" maxlength="3" onkeypress="enterInteger()" readonly="false"/>
             </div>
            </td>
            </tr>
           <tr id="trclear">
            <td nowrap width="16%" > 
              <div align="right"><b>Dias Pago Directo :</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"> 
                <eibsinput:text name="bolDetails" property="E01BOLDPN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_REFERENCE %>" size="4" maxlength="3" onkeypress="enterInteger()" readonly="false"/>
                </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Dias Planilla Empleador :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <eibsinput:text name="bolDetails" property="E01BOLDCE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_REFERENCE %>" size="4" maxlength="3" onkeypress="enterInteger()" readonly="false"/>
             </div>
            </td>
            </tr>          
        </table>
      </td>
    </tr>
  </table>
  <h4>Parametros Monto</h4>  
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right">Monto Minimo :</div>
            </td>
            <td nowrap width="23%" > 
                <eibsinput:text name="bolDetails" property="E01BOLMMI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" maxlength="6" onkeypress="enterRate()" readonly="false"/>
           </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>
          <div align="center"> 
            <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
          </div>
  </form>
</body>
</HTML>
