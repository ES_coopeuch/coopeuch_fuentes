<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Confirmación de Capitalización </title>
<meta http-equiv="Refresh" CONTENT="5;url='<%=request.getContextPath()%>/pages/background.jsp'">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">

<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<jsp:useBean id="svCapitaliza" class="datapro.eibs.beans.EDD260001Message"  scope="session" />


</head>

<body>

 
<h3 align="center">Confirmación<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="capitaliza_confirm_sv, EDD2600" ></h3>
<hr size="4">

  <table width="100%" height="100%" border="1" >
    <tr > 
      <td> 
        <table width="100%" height="100%">
          <tr> 
            
          <td align=center> La cuenta número <%= svCapitaliza.getE01ACMACC().trim()%> 
            , a nombre de  <%= svCapitaliza.getE01CUSNA1().trim()%> ha sido satisfactoriamente
            capitalizada. 
          </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</body>
</html>
