<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 
<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="datarec" class="datapro.eibs.beans.EPV117005Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/jquery/jquery-ui-1.8.13.custom.min.js"> </script>


<script type="text/javascript">

  function cerrarVentana(){
		window.open('','_parent','');
		window.close(); 
  	}
  	
//  Process according with user selection
 function goAction(op) {
	
   	switch (op){
	//Cancel
	case 1:  { 		
		cerrarVentana();	
       	break;
		}
	}
 }
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
	boolean newOnly=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<%
	// Determina si es nuevo o mantencion
	if (userPO.getPurpose().equals("NEW")){
			newOnly=false;
		} else if (userPO.getPurpose().equals("MAINTENANCE")) {
    		newOnly=false;
		} else {
    		newOnly=true;		
		}
%>

<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

 <% String emp = (String)session.getAttribute("EMPTV");
 	emp = (emp==null)?"":emp;//si es blanco viene llamado por menu, sino viene llamdo desde la pantalla EPV1010
 %>
 

<h3 align="center">
<%if (readOnly){ %>
	CONSULTA DE ARTICULOS
<%} else if (maintenance){ %>
	MANTENIMIENTO DE ARTICULOS
<%} else { %>
	NUEVO ARTICULOS
<%} %>
</h3>
 <%if ("".equals(emp)){ %>
	<h3 align="center"><img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="tienda_virtual_maintenance.jsp, EPV1170"></h3>
	<hr size="4">
<%}%>
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1170">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="H05FLGMAS"  value="<%= datarec.getH05FLGMAS().trim()%>">

 <% int row = 0;%>

<%if ("".equals(emp)){ %>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="datarec" property="E05PTVCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Propuesta : 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="datarec" property="E05PTVNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
             </td>
         </tr>
       </table>
      </td>
    </tr>
  </table>
  <%}else{%>
  	<input  type="hidden" name="E05PTVCUN" value="<%=datarec.getE05PTVCUN()%>">
  	<input  type="hidden" name="E05PTVNUM" value="<%=datarec.getE05PTVNUM()%>">
  <%} %>
  
    
 
   <h4>Direcci�n de Entrega</h4>
    <table class="tableinfo">
      <tr > 
        <td nowrap> 
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="left">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="39%"> 
                <div align="right">Direcci�n :</div>
              </td>
              <td nowrap width="61%">
					<eibsinput:text name="datarec" property="E05PTVMA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"/>                 
			  </td>
            </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="39%"> 
                <div align="right">N�mero :</div>
              </td>
              <td nowrap width="61%">
					<eibsinput:text name="datarec" property="E05PTVMA2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"/> 
			  </td>
            </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="39%"> 
                <div align="right">Referencia :</div>
              </td>
              <td nowrap width="61%">
					<eibsinput:text name="datarec" property="E05PTVMA3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"/>                  
			  </td>
            </tr>

			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap > 
                <div align="right">Region :</div>
              </td>
              <td nowrap>
                
                 <eibsinput:cnofc name="datarec" property="E05PTVCRG" required="true" flag="07" fn_code="E05PTVCRG" fn_description="E05DSCCRG"/>
				 <eibsinput:text property="E05DSCCRG" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="true" readonly="true"/>                 				
                
			  </td>
            </tr>

            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
              <td nowrap> 
                <div align="right">Comuna :</div>
              </td>
              <td nowrap>  				
                 <eibsinput:cnofc name="datarec" property="E05PTVCCO" required="true" flag="80" fn_code="E05PTVCCO" fn_description="E05DSCCCO"/>
				 <eibsinput:text property="E05DSCCCO" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="true" readonly="true"/>                 				
			  </td>                
            </tr>
            
			<tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">            
              <td nowrap > 
                <div align="right">C&oacute;digo Postal :</div>
              </td>
              <td nowrap>  
					<eibsinput:text name="datarec" property="E05PTVZPC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"/>               
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">            
              <td nowrap > 
                <div align="right">Telefono :</div>
              </td>
              <td nowrap>                 
					<eibsinput:text name="datarec" property="E05PTVHPN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"/>               
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">            
              <td nowrap > 
                <div align="right">Celular :</div>
              </td>
              <td nowrap>                 
				<eibsinput:text name="datarec" property="E05PTVPHN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>"/>               
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">            
              <td nowrap > 
                <div align="right">Email Contacto :</div>
              </td>
              <td nowrap>                 
					 <eibsinput:text name="datarec" property="E05PTVIAD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_EMAIL %>" />               
              </td>
            </tr>
            <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">            
              <td nowrap > 
                <div align="right">Observaci�n :</div>
              </td>
              <td nowrap>
     		    <textarea name="E05PTVRMK" onkeydown="limit(this,this.value)" onkeyup="limit(this,this.value)"  onkeypress="limit(this,this.value)" cols="100" rows="6" ><%= datarec.getE05PTVRMK()%> </textarea>
              </td>
            </tr>                      
        </table>
      </td>
    </tr>
  </table>

    
   <%if  (!readOnly) { %>
      <div align="center"> 
          <input id="EIBSBTN" type=submit name="Submit" value="Enviar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>
    <% } else { %>
      <div align="center"> 
          <input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
      </div>     
    <% } %>  
  </form>
 <%if ("S".equals(request.getAttribute("ACT"))){%>
 <script type="text/javascript">
 	  //recargamos la pagina que nos llamo..	  
	  window.opener.location.href =  '<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1170?SCREEN=101&E01PTVCUN=<%=userPO.getCusNum()%>&E01PTVNUM=<%=userPO.getHeader23()%>';  	
     <%//NOTA: solo para activar el check de la pagina integral%>
     <%  String re =(String) session.getAttribute("EMPTV");%>
	 <%  if ("S".equals(re)){%>
			window.opener.parent.setRecalculate3();	       
	 <% } %>   	  
  	  //cerramos la ventana
  	  cerrarVentana();	  
 </script>
 <% } %>   

 <script>
function limit(obj,valor)
{
	var limit = 500;
	var actual = valor.length;
	
	if (limit <= actual)
	{
		obj.value=valor.substring(0,limit);
	}
}
 </script>

</body>
</HTML>
