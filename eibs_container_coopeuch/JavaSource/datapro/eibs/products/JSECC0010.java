package datapro.eibs.products;

/**
 * Insert the type's description here.
 * Creation date: (1/19/00 6:08:55 PM)
 * @author: David Mavilla
 */
import java.beans.Beans;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Calendar;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.*;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSECC0010 extends datapro.eibs.master.SuperServlet {

	// credit card 
	protected static final int R_NEW = 1;
	protected static final int A_NEW = 2;
	protected static final int R_MAINTENANCE = 3;
	protected static final int A_MAINTENANCE = 4;

	protected static final int R_SPECIAL_CODES = 7;
	protected static final int A_SPECIAL_CODES = 8;
	protected static final int R_SPECIAL_INST = 9;
	protected static final int A_SPECIAL_INST = 10;
	protected static final int R_CARDS_ASSIGN = 11;
	protected static final int A_CARDS_ASSIGN = 12;
		
	protected static final int R_ENTER_MAINT = 200;
	protected static final int A_ENTER_MAINT = 400;
			
	protected String LangPath = "S";
	
	// credit card
	static final int A_ADD_NEW  			= 1200;
	static final int A_ADD_MAINT  			= 1400;
	static final int A_ADD_DELETE  			= 1800;

	static final int R_ADD_NEW 	 			= 1100;
	static final int R_ADD_MAINT  			= 1300;
	static final int R_ADD_DELETE 			= 1700;

	static final int R_ADD_LIST 			= 11;
	static final int A_ADD_LIST 			= 22;
	static final int A_ADD_NEW_MAINT		= 33;
	static final int A_ADD_CHANGE_STATUS	= 44;
	static final int R_ADD_PRIMARY_CARD 	= 55;
	static final int A_ADD_PRIMARY_CARD 	= 66;

	static final int R_ENTER_ADDITIONAL_CARDS = 1500;
	static final int A_ENTER_ADDITIONAL_CARDS = 1600;

	/**
	 * JSECLI001 constructor comment.
	 */
	public JSECC0010() {
		super();
	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

		flexLog("free resources used by JSECC0010");

	}
	/**
	 * This method was created by David Mavilla.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqNew(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECC001001Message msgCC = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		userPO.setPurpose("NEW");
		
		String opCode = null;
		opCode = "0001";

		// Send Initial data
		try {
			msgCC = (ECC001001Message) mc.getMessageRecord("ECC001001");
			msgCC.setH01USERID(user.getH01USR());
			msgCC.setH01PROGRM("ECC0010");
			msgCC.setH01TIMSYS(getTimeStamp());
			msgCC.setH01SCRCOD("01");
			msgCC.setH01OPECOD(opCode);

			try {
				msgCC.setE01CCMPRO(req.getParameter("E01CCMPRO"));
			} catch (Exception e) {
			}
			

			try {
				msgCC.setE01CCMACC(req.getParameter("E01CCMACC"));
			} catch (Exception e) {
			}
			
			msgCC.send();
			msgCC.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECC001001")) {
				try {
					msgCC = new ECC001001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgCC = (ECC001001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("ccNew", msgCC);
				ses.setAttribute("userPO", userPO);
				 
				if (IsNotError) { // There are no errors 
					try {
						flexLog("About to call Page3: " + LangPath + "ECC0010_cc_opening.jsp");
						callPage(LangPath + "ECC0010_cc_opening.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page4: " + LangPath + "ESD0711_products_detail.jsp");
						String firstLink =
							super.webAppPath
								+ LangPath
								+ "ESD0711_products_detail.jsp?appcode="
								+ req.getParameter("appcode").trim()
								+ "&typecode="
								+ req.getParameter("typecode").trim()
								+ "&generic="
								+ req.getParameter("generic").trim()
								+ "&title="
								+ req.getParameter("title").trim()
								+ "&bank="
								+ req.getParameter("bank").trim();
						res.setContentType("text/html");
						PrintWriter out = res.getWriter();
						printProdFrame(out, firstLink, LangPath);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionNew(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECC001001Message msgCC = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgCC = (ECC001001Message) ses.getAttribute("ccNew");
			msgCC.setH01USERID(user.getH01USR());
			msgCC.setH01PROGRM("ECC0010");
			msgCC.setH01TIMSYS(getTimeStamp());
			msgCC.setH01SCRCOD("01");
			msgCC.setH01OPECOD("0005");

			// all the fields here
			java.util.Enumeration enu = msgCC.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			// msgCD.send();
			mc.sendMessage(msgCC);
			msgCC.destroy();
			flexLog("ECC001001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECC001001")) {
				try {
					msgCC = new ECC001001Message();
					flexLog("ECC001001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgCC = (ECC001001Message) newmessage;

				userPO.setAccNum(msgCC.getE01CCMACC());
				userPO.setBank(msgCC.getE01CCMBNK());
				userPO.setHeader1(msgCC.getE01CCMPRO());
				userPO.setHeader2(msgCC.getE01CCMCUN());
				userPO.setHeader3(msgCC.getE01CUSNA1());
				userPO.setHeader4(msgCC.getE01CCMPCN());					
				userPO.setCurrency(msgCC.getE01CCMCCY());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("ccNew", msgCC);
				ses.setAttribute("userPO", userPO);
				
				if (IsNotError) { // There are no errors 
					flexLog("About to call Page: " + LangPath + "ECC0010_cc_confirm.jsp");
					callPage(LangPath + "ECC0010_cc_confirm.jsp", req, res);						
				}
				else { // There are errors 
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_opening.jsp");
						callPage(LangPath + "ECC0010_cc_opening.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqMaintenance(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECC001001Message msgCC = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		userPO.setPurpose("MAINTENANCE");

		String opCode = null;
		opCode = "0002";

		// Send Initial data
		try {
			msgCC = (ECC001001Message) mc.getMessageRecord("ECC001001");
			msgCC.setH01USERID(user.getH01USR());
			msgCC.setH01PROGRM("ECC0010");
			msgCC.setH01TIMSYS(getTimeStamp());
			msgCC.setH01SCRCOD("01");
			msgCC.setH01OPECOD(opCode);
			msgCC.setE01CCMACC(userPO.getAccNum());
			msgCC.send();
			msgCC.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECC001001")) {
				try {
					msgCC = new ECC001001Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgCC = (ECC001001Message) newmessage;

				userPO.setBank(msgCC.getE01CCMBNK());	
				userPO.setAccNum(msgCC.getE01CCMACC());
				userPO.setCusNum(msgCC.getE01CCMCUN());
				userPO.setCusName(msgCC.getE01CUSNA1());
				userPO.setIdentifier(msgCC.getE01CCMPCN());

				userPO.setHeader1(msgCC.getE01CCMPCN());
				userPO.setHeader2(msgCC.getE01CCMCUN());
				userPO.setHeader3(msgCC.getE01CUSNA1());
				userPO.setHeader4(msgCC.getE01CCMPCN());
				userPO.setHeader5(msgCC.getE01CCMPRO());					
				userPO.setCurrency(msgCC.getE01CCMCCY());
				
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("ccNew", msgCC);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_opening.jsp");
						callPage(LangPath + "ECC0010_cc_opening.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_enter_maint.jsp");
						callPage(LangPath + "ECC0010_cc_enter_maint.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	protected void procReqEnterMaint(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)	
		throws ServletException, IOException {

		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setPurpose("MAINTENANCE");
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath + "ECC0010_cc_enter_maint.jsp");
			callPage(LangPath + "ECC0010_cc_enter_maint.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	protected void procActionEnterMaint(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECC001001Message ccNew = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new datapro.eibs.beans.ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			ccNew = (ECC001001Message) mc.getMessageRecord("ECC001001");
			ccNew.setH01USERID(user.getH01USR());
			ccNew.setH01PROGRM("ECC0010");
			ccNew.setH01TIMSYS(getTimeStamp());
			ccNew.setH01SCRCOD("01");
			ccNew.setH01OPECOD("0002");
			try {
				ccNew.setE01CCMACC(req.getParameter("E01CCMACC"));
			} catch (Exception e) {
				ccNew.setE01CCMACC("0");
			}

			ccNew.send();
			ccNew.destroy();
			flexLog("ECC001001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECC001001")) {
				try {
					ccNew = new datapro.eibs.beans.ECC001001Message();
					flexLog("ECC001001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				ccNew = (ECC001001Message) newmessage;

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				if (IsNotError) { // There are no errors
					userPO.setOption("CC");
					userPO.setPurpose("MAINTENANCE");
					userPO.setBank(ccNew.getE01CCMBNK());
					userPO.setAccNum(ccNew.getE01CCMACC());
					userPO.setIdentifier(ccNew.getE01CCMPCN());
					userPO.setCusNum(ccNew.getE01CCMCUN());
					userPO.setCusName(ccNew.getE01CUSNA1());
					userPO.setHeader1(ccNew.getE01CCMPCN());
					userPO.setHeader2(ccNew.getE01CCMCUN());
					userPO.setHeader3(ccNew.getE01CUSNA1());
					userPO.setHeader4(ccNew.getE01CCMPCN());
					userPO.setHeader5(ccNew.getE01CCMPRO());					
					userPO.setCurrency(ccNew.getE01CCMCCY());


					ses.setAttribute("userPO", userPO);
					ses.setAttribute("ccNew", ccNew);

					try {
						flexLog("About to call Page: "	+ LangPath	+ "ECC0010_cc_opening.jsp");
						callPage(LangPath + "ECC0010_cc_opening.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_enter_maint.jsp");
						callPage(LangPath + "ECC0010_cc_enter_maint.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	protected void procActionMaintenance(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECC001001Message ccNew = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			ccNew = (ECC001001Message) ses.getAttribute("ccNew");
			ccNew.setH01USERID(user.getH01USR());
			ccNew.setH01PROGRM("ECC0010");
			ccNew.setH01TIMSYS(getTimeStamp());
			ccNew.setH01SCRCOD("01");
			ccNew.setH01OPECOD("0005");

			// all the fields here
			java.util.Enumeration enu = ccNew.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase().trim();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			//msgCD.send();
			mc.sendMessage(ccNew);
			ccNew.destroy();
			flexLog("ECC001001 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECC001001")) {
				try {
					ccNew = new ECC001001Message();
					flexLog("ECC001001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				ccNew = (ECC001001Message) newmessage;

				userPO.setPurpose("MAINTENANCE");
				userPO.setAccNum(ccNew.getE01CCMACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("ccNew", ccNew);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page1: " + LangPath + "ECC0010_cc_enter_maint.jsp");
						callPage(LangPath + "ECC0010_cc_enter_maint.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page2: " + LangPath + "ECC0010_cc_opening.jsp");
						callPage(LangPath + "ECC0010_cc_opening.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqSpecialCodes(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000002Message msgLN = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = "";
		if (userPO.getPurpose().equals("MAINTENANCE"))
			opCode = "0002";
		else
			opCode = "0004";

		// Send Initial data
		try {
			msgLN = (ESD000002Message) mc.getMessageRecord("ESD000002");
			msgLN.setH02USR(user.getH01USR());
			msgLN.setH02PGM("ECC0010");
			msgLN.setH02TIM(""); //getTimeStamp()
			msgLN.setH02SCR("01");
			msgLN.setH02WK1("M");
			msgLN.setH02OPE(opCode);
			msgLN.setE02ACC(userPO.getAccNum());
			msgLN.send();
			msgLN.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				//MOD : EMAT 9/5/2001
				// to display the page of special codes ...
				IsNotError = true;
				flexLog("IsNotError = " + IsNotError);
				//showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000002")) {
				try {
					msgLN = new ESD000002Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgLN = (ESD000002Message) newmessage;
				// showESD008004(msgLN);

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("lnCodes", msgLN);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_codes.jsp");
						callPage(LangPath + "ECC0010_cc_codes.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_opening.jsp");
						callPage(LangPath + "ECC0010_cc_opening.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionSpecialCodes(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000002Message msgLN = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			flexLog("Send Initial Data");
			msgLN = (ESD000002Message) ses.getAttribute("lnCodes");
			//msgLN = (ESD000002Message)mc.getMessageRecord("ESD000002");
			msgLN.setH02USR(user.getH01USR());
			msgLN.setH02PGM("ECC0010");
			msgLN.setH02TIM(getTimeStamp());
			msgLN.setH02SCR("01");
			msgLN.setH02OPE("0005");

			// all the fields here
			java.util.Enumeration enu = msgLN.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			//msgLN.send();
			mc.sendMessage(msgLN);
			msgLN.destroy();
			flexLog("ESD000002 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				//showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000002")) {
				try {
					msgLN = new ESD000002Message();
					flexLog("ESD000002 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgLN = (ESD000002Message) newmessage;
				// showESD008004(msgLN);

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("lnCodes", msgLN);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_opening.jsp");
						callPage(LangPath + "ECC0010_cc_opening.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_codes.jsp");
						callPage(LangPath + "ECC0010_cc_codes.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}
	
	protected void procReqEspInst(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000005Message msgCD = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		String opCode = "0002";

		// Send Initial data
		try {
			msgCD = (ESD000005Message) mc.getMessageRecord("ESD000005");
			msgCD.setH05USR(user.getH01USR());
			msgCD.setH05PGM("ECC0010");
			msgCD.setH05TIM(getTimeStamp());
			msgCD.setH05SCR("01");
			msgCD.setH05WK1("M");
			msgCD.setH05OPE(opCode);
			msgCD.setE05ACC(userPO.getAccNum());
			msgCD.setE05ACD("94");
			msgCD.send();
			msgCD.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000005")) {
				try {
					msgCD = new ESD000005Message();
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgCD = (ESD000005Message) newmessage;
				// showESD008004(msgCD);

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("cdInst", msgCD);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors 
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_special_inst.jsp");
						callPage(LangPath + "ECC0010_cc_special_inst.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_opening.jsp");
						callPage(LangPath + "ECC0010_cc_opening.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}
	
	protected void procActionEspInst(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ESD000005Message msgCD = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgCD = (ESD000005Message) ses.getAttribute("cdInst");
			//msgCD = (ESD000005Message)mc.getMessageRecord("ESD000005");
			msgCD.setH05USR(user.getH01USR());
			msgCD.setH05PGM("ECC0010");
			msgCD.setH05TIM(getTimeStamp());
			msgCD.setH05SCR("01");
			msgCD.setH05OPE("0005");
			msgCD.setE05ACC(userPO.getAccNum());
			msgCD.setE05ACD("94");

			// all the fields here
			java.util.Enumeration enu = msgCD.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			//msgCD.send();
			mc.sendMessage(msgCD);
			msgCD.destroy();
			flexLog("ESD000005 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ESD000005")) {
				try {
					msgCD = new ESD000005Message();
					flexLog("ESD000005 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgCD = (ESD000005Message) newmessage;
				// showESD008004(msgCD);

				//userPO.setAccNum(msgCD.getE05ACC())

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("cdInst", msgCD);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						{
							flexLog("About to call Page: " + LangPath + "ECC0010_cc_opening.jsp");
							callPage(LangPath + "ECC0010_cc_opening.jsp", req, res);
						}
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_special_inst.jsp");
						callPage(LangPath + "ECC0010_cc_special_inst.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}	
//---------------------------------------------------------------------------------------------------	
// Tarjetas Adicionales 
	protected void procReqEnterAditionalCard(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			ELEERRMessage msgError = null;
			UserPos userPO = null;

			try {
				msgError = new datapro.eibs.beans.ELEERRMessage();
				userPO = new datapro.eibs.beans.UserPos();

				String type = req.getParameter("Type");
				if (type == null) {
					type = "";
					userPO.setOption("");
				} else if (type.equals("D")) {
					userPO.setOption("RT");
					userPO.setHeader2("D");
				} else if (type.equals("C")) {
					userPO.setOption("CC");
					userPO.setHeader2("C");
				}

				userPO.setPurpose("MAINTENANCE");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			String opt = req.getParameter("OPT");
			if (opt == null) opt = "";


			if (opt.equals("4")) {
				try {
					flexLog("About to call Page: " + LangPath + "ECC0010_cc_enter_card.jsp");
					callPage(LangPath + "ECC0010_cc_enter_card.jsp?opt=" + opt,  req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			} else {
				try {
					flexLog("About to call Page: " + LangPath + "ECC0010_cc_enter_aditional_cards.jsp");
					callPage(LangPath + "ECC0010_cc_enter_aditional_cards.jsp?opt=" + opt,  req, res);
				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			}

		}

		protected void procReqAdditionalCardsList(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			MessageRecord newmessage = null;
			ECC001003Message msgList = null;
			JBObjList appList = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			boolean IsNotError = false;
			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			String Type = req.getParameter("Type");
			if (Type == null) Type = userPO.getHeader2();

			String opt = req.getParameter("opt");
			if (opt == null) opt = "";

			// Send Initial data
			try {
				msgList = (ECC001003Message) mc.getMessageRecord("ECC001003");
				msgList.setH03USERID(user.getH01USR());
				msgList.setH03PROGRM("ECC0010");
				msgList.setH03TIMSYS(getTimeStamp());
				msgList.setH03SCRCOD("01");
				msgList.setH03OPECOD("0015");

				msgList.setE03TARTYP(Type);

				try {
					msgList.setE03CCRCRA(req.getParameter("E01CCRCRA"));
				}
				catch (Exception e){
					msgList.setE03CCRCRA(userPO.getAccNum());
				}
				msgList.setE03CCRTCL("S");
				
				userPO.setAccNum(msgList.getE03CCRCRA());

				msgList.send();
				msgList.destroy();
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			flexLog("Initializing java beans into the session");
			// Receive Error Message
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Error Message
			try {
				    newmessage = mc.receiveMessage();
				    
					if (newmessage.getFormatName().equals("ECC001003")) {
						appList = new JBObjList();
						boolean firstTime = true;
						String marker = "";

						while (true) {
							msgList = (ECC001003Message) newmessage;
							marker = msgList.getH03FLGMAS();

							if (marker.equals("*")) {
									//userPO.setAccNum(msgList.getE01CCRCRA());
									userPO.setIdentifier(msgList.getE03CCRCRA());
									userPO.setCusNum(msgList.getE03PRICUN());
									userPO.setCusName(msgList.getE03PRINA1());
									userPO.setHeader1(msgList.getE03CCRTTI());
									userPO.setHeader2(msgList.getE03TARTYP());
									userPO.setID( msgList.getE03CCRCID() ) ;
									appList.setShowNext(false);
									break;
							} else {
								appList.addRow(msgList);
								if (firstTime) {
									firstTime = false;
									//appList.setFirstRec(Integer.parseInt(msgList.getE01CCRNUM()));
									//userPO.setAccNum(msgList.getE01CCRCRA());
									userPO.setIdentifier(msgList.getE03CCRCRA());
									userPO.setCusNum(msgList.getE03PRICUN());
									userPO.setCusName(msgList.getE03PRINA1());
									userPO.setHeader1(msgList.getE03CCRTTI());
									userPO.setHeader2(msgList.getE03TARTYP());
									userPO.setID( msgList.getE03CCRCID() ) ;
									userPO.setHeader20("");
									userPO.setHeader21("");
								}
								if (marker.equals("+")) {
									appList.setShowNext(true);
									break;
								}
							}
							newmessage = mc.receiveMessage();
						}

						flexLog("Putting java beans into the session");
						ses.setAttribute("appList", appList);
						ses.setAttribute("error", msgError);
						ses.setAttribute("userPO", userPO);

						try {
							flexLog("About to call Page: " + LangPath + "ECC0010_cc_additional_cards_list.jsp");
							callPage(LangPath + "ECC0010_cc_additional_cards_list.jsp"
														 + "?opt=" + opt,
														 req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}

					} else
						flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}
			}

		protected void procActionPrimaryCard(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			MessageRecord newmessage = null;
			ECC001001Message ccPrim = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			boolean IsNotError = false;

			try {
				msgError = new ELEERRMessage();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			// Send Initial data
			try {
				flexLog("Send Initial Data");
				ccPrim = (ECC001001Message) ses.getAttribute("msgCard");
				ccPrim.setH01USERID(user.getH01USR());
				ccPrim.setH01PROGRM("ECC0010");
				ccPrim.setH01TIMSYS(getTimeStamp());
				ccPrim.setH01SCRCOD("01");
				ccPrim.setH01OPECOD("0005");

				// all the fields here
				java.util.Enumeration enu = ccPrim.fieldEnumeration();
				MessageField field = null;
				String value = null;
				while (enu.hasMoreElements()) {
					field = (MessageField) enu.nextElement();
					try {
						value = req.getParameter(field.getTag()).toUpperCase().trim();
						if (value != null) {
							field.setString(value);
						}
					} catch (Exception e) {
					}
				}

				//msgCD.send();
				mc.sendMessage(ccPrim);
				ccPrim.destroy();
				flexLog("ECC001001 Message Sent");
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Error Message
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ECC001001")) {
					try {
						ccPrim = new ECC001001Message();
						flexLog("ECC001001 Message Received");
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					ccPrim = (ECC001001Message) newmessage;

					userPO.setPurpose("MAINTENANCE");
//					userPO.setAccNum(ccPrim.getE01CCRCRA());

					flexLog("Putting java beans into the session");
					ses.setAttribute("error", msgError);
					ses.setAttribute("msgCard", ccPrim);
					ses.setAttribute("userPO", userPO);

					if (IsNotError) { // There are no errors
						try {
							res.sendRedirect(super.srctx + "/servlet/datapro.eibs.products.JSECC0010?SCREEN=3");
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else { // There are errors
						try {
							flexLog("About to call Page2: " + LangPath + "ECC0010_cc_primary_card.jsp");
							callPage(LangPath + "ECC0010_cc_primary_card.jsp", req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}
		}

		/**
		 * This method was created in VisualAge.
		 */

		protected void procActionChangeStatus(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			if (req.getParameter("search").equals("A") ) {
				procChangeStatusByAccNumber( mc , user, req, res, ses  ) ;

			} else {
				procChangeStatusByCardNumber( mc , user, req, res, ses  ) ;

			}

		}

		/**
		 * This method was created in VisualAge.
		 */

		protected void procActionAditionalCardsList(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			MessageRecord newmessage = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			ESS0030DSMessage msgUser = null;
			ECC001003Message msgCC = null;
			JBObjList appList = null;

			appList = (JBObjList) ses.getAttribute("appList");
			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			try {
				int option = Integer.parseInt(req.getParameter("opt"));
				int row = -1;
				try {
					row = Integer.parseInt(req.getParameter("ROW"));
				} catch (Exception e) {
				}
				appList.setCurrentRow(row); 

				switch (option) {
					case 1 : // New
						long num = 0;
						if (appList.getNoResult()) {
							num = 1;
						} else {
							appList.getLastRec();
							msgCC = (ECC001003Message) appList.getRecord();
							num = Long.parseLong(msgCC.getE03CCRNUM()) + 1;
						}
						msgCC = null;
						msgCC = (ECC001003Message) mc.getMessageRecord("ECC001003");
						userPO.setHeader19(num + "");
						userPO.setHeader20("DO_NEW");
						userPO.setHeader21(super.webAppPath + LangPath + "ECC0010_cc_aditional_card_new.jsp?num=" + num);
						ses.setAttribute("userPO", userPO);
						ses.setAttribute("msgCard", msgCC);
						res.sendRedirect(super.srctx + LangPath + "ECC0010_cc_additional_cards_list.jsp?SEL=" + row + "&opt=" + option);
						break;
					case 2 : // Maintenance
						msgCC = (ECC001003Message) appList.getRecord();
						userPO.setHeader20("DO_MAINT");
						userPO.setHeader21(super.webAppPath + LangPath + "ECC0010_cc_aditional_card.jsp?ROW=" + row);
						ses.setAttribute("userPO", userPO);
						ses.setAttribute("msgCard", msgCC);
						res.sendRedirect(super.srctx + LangPath + "ECC0010_cc_additional_cards_list.jsp?SEL=" + row + "&opt=" + option);
						break;
					case 3 : // Delete
						msgCC = (ECC001003Message) appList.getRecord();
						procActionDelete(row, msgCC, mc, user, req, res, ses);
						break;
					case 4 : // Change Status
						msgCC = (ECC001003Message) appList.getRecord();
						userPO.setHeader20("DO_MAINT");
						userPO.setHeader21(super.webAppPath + LangPath + "ECC0010_cc_aditional_card_status.jsp?ROW=" + row);
						ses.setAttribute("userPO", userPO);
						ses.setAttribute("msgCard", msgCC);
						res.sendRedirect(super.srctx + LangPath + "ECC0010_cc_additional_cards_list.jsp?SEL=" + row + "&opt=" + option);
						break;
					case 5 : // ASSIGN PIN
						msgCC = (ECC001003Message) appList.getRecord();
						userPO.setHeader20("DO_ASSIGN_PIN");
						userPO.setHeader21(super.webAppPath + LangPath + "ECC0010_cc_aditional_card_assign_pin.jsp?ROW=" + row);
						ses.setAttribute("userPO", userPO);
						ses.setAttribute("msgCard", msgCC);
						res.sendRedirect(super.srctx + LangPath + "ECC0010_cc_aditional_card_assign_pin.jsp?SEL=" + row + "&opt=" + option);
						break;
					case 6 : // ADD ACCOUNT
						msgCC = (ECC001003Message) appList.getRecord();
						userPO.setHeader20("DO_ADD_ACCOUNT");
						userPO.setHeader21(super.webAppPath + LangPath + "ECC0010_cc_aditional_card_asign.jsp?ROW=" + row);
						ses.setAttribute("userPO", userPO);
						ses.setAttribute("msgCard", msgCC);
						res.sendRedirect(super.srctx + LangPath + "ECC0010_cc_aditional_card_asign.jsp?SEL=" + row + "&opt=" + option);
						break;

				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}

		/**
		 * This method was created in VisualAge.

		 *	 */
		protected void procReqPrimaryCard(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			MessageRecord newmessage = null;
			ECC001003Message msgPrim = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			boolean IsNotError = false;

			try {
				msgError = new ELEERRMessage();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			userPO.setPurpose("MAINTENANCE");

			String opCode = null;
			opCode = "0002";

			// Send Initial data
			try {
				msgPrim = (ECC001003Message) mc.getMessageRecord("ECC001003");
				msgPrim.setH03USERID(user.getH01USR());
				msgPrim.setH03PROGRM("ECC0010");
				msgPrim.setH03TIMSYS(getTimeStamp());
				msgPrim.setH03SCRCOD("01");
				msgPrim.setH03OPECOD(opCode);

				try {
					msgPrim.setE03CCRCRA(userPO.getAccNum());
				} catch (Exception e) {
				}
				try {
					msgPrim.setE03CCRNUM(userPO.getIdentifier());
				} catch (Exception e) {
				}
				try {
					msgPrim.setE03CCRTCL("P");
				} catch (Exception e) {
				}

				msgPrim.send();
				msgPrim.destroy();
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Error Message
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ECC001001")) {
					try {
						msgPrim = new ECC001003Message();
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					msgPrim = (ECC001003Message) newmessage;

					flexLog("Putting java beans into the session");
					ses.setAttribute("error", msgError);
					ses.setAttribute("msgCard", msgPrim);
					ses.setAttribute("userPO", userPO);

					if (IsNotError) { // There are no errors
						try {
							flexLog("About to call Page: " + LangPath + "ECC0010_cc_primary_card.jsp");
							callPage(LangPath + "ECC0010_cc_primary_card.jsp", req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					} else { // There are errors
						try {
							res.sendRedirect(super.srctx + "/servlet/datapro.eibs.products.JSECC0010?SCREEN=3");
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}

		protected void procActionDelete(
			int row,
			ECC001003Message msgOrig,
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			MessageRecord newmessage = null;
			ECC001003Message msgCC = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			boolean IsNotError = false;
			int option;

			try {
				msgError = new datapro.eibs.beans.ELEERRMessage();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			// Send Initial data
			try {
				option = Integer.parseInt(req.getParameter("opt"));

				flexLog("Send Initial Data");
				msgCC = (ECC001003Message) mc.getMessageRecord("ECC001003");
				msgCC.setH03USERID(user.getH01USR());
				msgCC.setH03PROGRM("ECC0010");
				msgCC.setH03TIMSYS(getTimeStamp());
				msgCC.setH03SCRCOD("01");
	  			msgCC.setH03OPECOD("0009");

				try {
					msgCC.setE03CCRCRA( msgOrig.getE03CCRCRA() ) ; // userPO.getAccNum()
				}
				catch (Exception e){
					msgCC.setE03CCRCRA("");
				}
				try {
					msgCC.setE03CCRNUM(msgOrig.getE03CCRNUM());
				}
				catch (Exception e){
					msgCC.setE03CCRNUM("");
				}

				//msgRT.send();
				mc.sendMessage(msgCC);
				msgCC.destroy();
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Error Message
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);

					if (IsNotError) { // There are no errors
						ses.setAttribute("userPO", userPO);
						res.sendRedirect(super.srctx + "/servlet/datapro.eibs.products.JSECC0010?SCREEN=600&E01CCRCRA="
															+ userPO.getAccNum()
															+ "&E01TARTYP=" + userPO.getHeader2()
															+ "&opt=" + option
															+ "&E01CCRTCL=S");
					} else {
						try {
							flexLog("Putting java beans into the session");
							ses.setAttribute("error", msgError);
							res.sendRedirect(super.srctx + LangPath + "ECC0010_cc_additional_cards_list.jsp?ROW=" + row + "&opt=" + option);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");



			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}

		protected void procActionNewMaint(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			MessageRecord newmessage = null;
			ECC001003Message msgCC = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			boolean IsNotError = false;
			int option;
			String numCta = null ;

			try {
				msgError = new datapro.eibs.beans.ELEERRMessage();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			// Send Initial data
			try {
				option = Integer.parseInt(req.getParameter("opt"));

				flexLog("Send Initial Data");
				msgCC = (ECC001003Message) mc.getMessageRecord("ECC001003");
				msgCC.setH03USERID(user.getH01USR());
				msgCC.setH03PROGRM("ECC0010");
				msgCC.setH03TIMSYS(getTimeStamp());
				msgCC.setH03SCRCOD("01");

				if (option == 1){
					msgCC.setH03OPECOD("0001");
				}else if ( option == 6 ) {
					msgCC.setH03OPECOD("0101");
				}else{
					msgCC.setH03OPECOD("0005");  
				}

				// all the fields here
				java.util.Enumeration enu = msgCC.fieldEnumeration();
				MessageField field = null;
				String value = null;
				while (enu.hasMoreElements()) {
					field = (MessageField) enu.nextElement();
					try {
						value = req.getParameter(field.getTag()).toUpperCase();
						if (value != null) {
							field.setString(value);
						}
					} catch (Exception e) {
					}
				}

				numCta = msgCC.getE03CCRCRA();
				userPO.setHeader23( msgCC.getE03CCRNUM() ) ;
				
				//msgRT.send();
				flexLog("mensaje enviado..." + msgCC);
				mc.sendMessage(msgCC);
				msgCC.destroy();
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Error Message
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ECC001003")) {
					try {
						msgCC = new datapro.eibs.beans.ECC001003Message();
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					msgCC = (ECC001003Message) newmessage;
					// showESD008004(msgRT);

					if (IsNotError) { // There are no errors

						if( option == 40 ) { //CAMBIO DE STATUS

							flexLog("About to call:" 
									+ "/servlet/datapro.eibs.products.JSECC0010?SCREEN=1500&Type=D&OPT=4"
									 );
							res.sendRedirect(super.srctx
									+ "/servlet/datapro.eibs.products.JSECC0010?SCREEN=1500&Type=D&OPT=4"
									);						
							
						}else if( option == 1 ){// ASIGNACION DE TARJETAS
							try {
									flexLog("About to call:" 
									+ "/servlet/datapro.eibs.products.JSECC0010?SCREEN=1600&"
									+ "&Type=C&E03CCRCRA=" + numCta );
							res.sendRedirect(super.srctx
									+ "/servlet/datapro.eibs.products.JSECC0010?SCREEN=1600&"
									+ "&Type=C&E03CCRCRA=" + numCta ); 
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}	
							/*
							try {
								flexLog("Card Asigned. Returning to Page for Printing" );
								flexLog("About to call Page: " + LangPath + "ECC0010_cc_aditional_card_new.jsp" );
								callPage(LangPath + "ECC0010_cc_aditional_card_new.jsp", req, res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
							*/
							
						} else if (option == 6) {
							
							flexLog("About to call:" 
									+ "/servlet/datapro.eibs.products.JSECC0010?SCREEN=1500&"
									+ "&Type=D" );
							res.sendRedirect(super.srctx
									+ "/servlet/datapro.eibs.products.JSECC0010?SCREEN=1500&"
									+ "&Type=D" );						
						}
						
						else {
							res.sendRedirect(super.srctx
							+ "/servlet/datapro.eibs.products.JSECC0010?SCREEN=1500&E01CCRCRA="
							+ msgCC.getE03CCRCRA()
							+ "&E01TARTYP="	+ msgCC.getE03TARTYP()
							+ "&opt=" + option
							+ "&E01CCRTCL=S");						
							
						}
						
						
						
						
						
	/*					res.sendRedirect(super.srctx
								+ "/servlet/datapro.eibs.products.JSECC0010?SCREEN=500&E01CCRCRA="
								+ msgCC.getE01CCRCRA()
								+ "&E01TARTYP="	+ msgCC.getE01TARTYP()
								+ "&opt=" + option
								+ "&E01CCRTCL=S");
	*/					/*
						res.setContentType("text/html");
						PrintWriter out = res.getWriter();
						out.println("<HTML>");
						out.println("<HEAD>");
						out.println("<TITLE>Close</TITLE>");
						out.println("</HEAD>");
						out.println("<BODY>");
						out.println("<SCRIPT LANGUAGE=\"JavaScript\">");
						out.println(
							"		top.opener.window.location.href='"
								+ super.webAppPath
								+ "/servlet/datapro.eibs.products.JSECC0010?SCREEN=600&E01CCRCRA="
								+ msgCC.getE01CCRCRA()
								+ "&E01TARTYP="	+ msgCC.getE01TARTYP()
								+ "&opt=" + option
								+ "&E01CCRTCL=S"
								+ "'");

						out.println("		top.close();");
						out.println("</SCRIPT>");
						out.println("<P>Close it!!!</P>");
						out.println("</BODY>");
						out.println("</HTML>");
						out.close();
						*/
					} else { // There are errors
						flexLog("Putting java beans into the session");
						ses.setAttribute("error", msgError);
						ses.setAttribute("msgCard", msgCC);
						ses.setAttribute("userPO", userPO);
						if (option == 1) {
							try {
								flexLog("About to call Page: " + LangPath + "ECC0010_cc_aditional_card_new.jsp");
								callPage(LangPath + "ECC0010_cc_aditional_card_new.jsp", req, res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						} else if (option == 2) {
							try {
								flexLog("About to call Page: " + LangPath + "ECC0010_cc_aditional_card.jsp");
								callPage(LangPath + "ECC0010_cc_aditional_card.jsp", req, res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						} else if (option == 40) { //CAMBIO DE STATUS
							try {
								flexLog("About to call Page: " + LangPath + "ECC0010_cc_aditional_card_status.jsp");
								callPage(LangPath + "ECC0010_cc_aditional_card_status.jsp", req, res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						} else if ( option == 6 ) { //ASIGNACION BANVALOR
							try {
								flexLog("About to call Page: " + LangPath + "ECC0010_cc_aditional_card_asign.jsp");
								callPage(LangPath + "ECC0010_cc_aditional_card_asign.jsp", req, res);
							} catch (Exception e) {
								flexLog("Exception calling page " + e);
							}
						}
					}
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}

		protected void procChangeStatusByCardNumber(
				MessageContext mc,
				ESS0030DSMessage user,
				HttpServletRequest req,
				HttpServletResponse res,
				HttpSession ses)
				throws ServletException, IOException {


			MessageRecord newmessage = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			ESS0030DSMessage msgUser = null;
			ECC001003Message msgCC = null;
			JBObjList appList = null;
			boolean IsNotError = false;

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			//int option = Integer.parseInt(req.getParameter("opt"));
			String Type = userPO.getHeader2();

			/*
			String card =  req.getParameter("E01CCRNUM");
			if (card != null && card.equals("")) {
				res.sendRedirect(super.srctx
					+ "/servlet/datapro.eibs.products.JSECC0010?SCREEN=600"
					+ "&E01TARTYP="	+ Type
					+ "&opt=4");
			} else if (card != null){
			*/
			try {
				flexLog("Send Initial Data");
				msgCC = (ECC001003Message) mc.getMessageRecord("ECC001003");
				msgCC.setH03USERID(user.getH01USR());
				msgCC.setH03PROGRM("EDD0010");
				msgCC.setH03TIMSYS(getTimeStamp());
				msgCC.setH03SCRCOD("01");
				msgCC.setH03OPECOD("0002");

				msgCC.setE03CCRNUM(req.getParameter("E01CCRNUM"));

				msgCC.send();
				//mc.sendMessage(msgCC);
				msgCC.destroy();
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Error Message
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ECC001003")) {
					try {
						msgCC = new datapro.eibs.beans.ECC001003Message();
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					msgCC = (ECC001003Message) newmessage;

					userPO.setAccNum(msgCC.getE03CCRCRA());
					userPO.setIdentifier(msgCC.getE03CCRNUM());
					userPO.setCusNum(msgCC.getE03PRICUN());
					userPO.setCusName(msgCC.getE03PRINA1());
					userPO.setHeader1(msgCC.getE03CCRNUM());
					//userPO.setHeader2(msgCC.getE01TARTYP());

					flexLog("Putting java beans into the session");
					ses.setAttribute("userPO", userPO);
					if (IsNotError) { // There are no errors
						ses.setAttribute("msgCard", msgCC);
						
	/*					try {
							flexLog("About to call Page: " + LangPath + "ECC0010_cc_additional_cards_list.jsp");
							callPage(LangPath + "ECC0010_cc_additional_cards_list.jsp", req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
	*/					
						
						try {
							flexLog("About to call Page: " + LangPath + "ECC0010_cc_aditional_card_status.jsp");
							callPage(LangPath + "ECC0010_cc_aditional_card_status.jsp", req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}

					} else { // There are errors
						ses.setAttribute("error", msgError);
						try {
							flexLog("About to call Page: " + LangPath + "ECC0010_cc_enter_card.jsp");
							callPage(LangPath + "ECC0010_cc_enter_card.jsp?Type=" + Type + "&OPT=4", req, res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}

		protected void procChangeStatusByAccNumber(
				MessageContext mc,
				ESS0030DSMessage user,
				HttpServletRequest req,
				HttpServletResponse res,
				HttpSession ses)
				throws ServletException, IOException {

			MessageRecord newmessage = null;
			ECC001003Message msgList = null;
			JBObjList appList = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			String Type = req.getParameter("Type");
			if (Type == null) {
				Type = userPO.getHeader2();
			} else {
				userPO.setHeader2(Type);
			}

			// Send Initial data
			try {
				msgList = (ECC001003Message) mc.getMessageRecord("ECC001003");
				msgList.setH03USERID(user.getH01USR());
				msgList.setH03PROGRM("ECC0010");
				msgList.setH03TIMSYS(getTimeStamp());
				msgList.setH03SCRCOD("01");
				msgList.setH03OPECOD("0015");

				msgList.setE03TARTYP(Type);

				try {
					msgList.setE03CCRCRA(req.getParameter("E01CCRCRA"));
				} catch (Exception e) {
					msgList.setE03CCRCRA(userPO.getAccNum());
				}
				msgList.setE03CCRTCL("S");

				msgList.send();
				msgList.destroy();
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			flexLog("Initializing java beans into the session");
			try {
				msgError = new datapro.eibs.beans.ELEERRMessage();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			// Receive Error Message
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {
					msgError = (ELEERRMessage) newmessage;

					flexLog("Putting java beans into the session");
					ses.setAttribute("error", msgError);


					try {
						flexLog("About to call Page: " + LangPath + "ECC0010_cc_inq_enter_aditional_cards.jsp");
						callPage(LangPath + "ECC0010_cc_inq_enter_aditional_cards.jsp", req, res);

					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				} else
					if (newmessage.getFormatName().equals("ECC001003")) {
						appList = new JBObjList();
						boolean firstTime = true;
						String marker = "";

						while (true) {
							msgList = (ECC001003Message) newmessage;
							marker = msgList.getH03FLGMAS();

							if (marker.equals("*")) {
									userPO.setAccNum(msgList.getE03CCRCRA());
									userPO.setIdentifier(msgList.getE03CCRCRA());
									userPO.setCusNum(msgList.getE03PRICUN());
									userPO.setCusName(msgList.getE03PRINA1());
									userPO.setHeader1(msgList.getE03CCRNUM());
									//userPO.setHeader2(msgList.getE01TARTYP());
									appList.setShowNext(false);
									break;
							} else {
								appList.addRow(msgList);
								if (firstTime) {
									firstTime = false;
									//appList.setFirstRec(Integer.parseInt(msgList.getE01CCRNUM()));
									userPO.setAccNum(msgList.getE03CCRCRA());
									userPO.setIdentifier(msgList.getE03CCRCRA());
									userPO.setCusNum(msgList.getE03PRICUN());
									userPO.setCusName(msgList.getE03PRINA1());
									userPO.setHeader1(msgList.getE03CCRNUM());
									//userPO.setHeader2(msgList.getE01TARTYP());
									userPO.setHeader20("");
									userPO.setHeader21("");
								}
								if (marker.equals("+")) {
									appList.setShowNext(true);
									break;
								}
							}
							newmessage = mc.receiveMessage();
						}

						flexLog("Putting java beans into the session");
						ses.setAttribute("appList", appList);
						ses.setAttribute("error", msgError);
						ses.setAttribute("userPO", userPO);

						try {
							flexLog("About to call Page: " + LangPath + "ECC0010_cc_inq_additional_cards_list.jsp");
							callPage(LangPath + "ECC0010_sc_inq_additional_cards_list.jsp", req, res);

						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}

					} else
						flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}
		
//---------------------------------------------------------------------------------------------------
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		Socket s = null;
		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			int screen = R_ENTER_MAINT;

			try {

				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					s = new Socket(super.hostIP, getInitSocket(req) + 1);
					s.setSoTimeout(super.sckTimeOut);
					mc =
						new MessageContext(
							new DataInputStream(new BufferedInputStream(s.getInputStream())),
							new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
							"datapro.eibs.beans");
				

				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}

				switch (screen) {
					// Request
					case R_NEW :
						procReqNew(mc, msgUser, req, res, session);
						break;
					case R_ENTER_MAINT :
						procReqEnterMaint(mc, msgUser, req, res, session);
						break;	
					case R_MAINTENANCE :
						procReqMaintenance(mc, msgUser, req, res, session);
						break;
					case R_SPECIAL_CODES :
						procReqSpecialCodes(mc, msgUser, req, res, session);
						break;						
					case R_SPECIAL_INST :
						procReqEspInst(mc, msgUser, req, res, session);
						break;						
						
					// Action
					case A_NEW :
						procActionNew(mc, msgUser, req, res, session);
						break;
					case A_ENTER_MAINT :
						procActionEnterMaint(mc, msgUser, req, res, session);
						break;
					case A_MAINTENANCE :
						procActionMaintenance(mc, msgUser, req, res, session);
						break;
					case A_SPECIAL_CODES :
						procActionSpecialCodes(mc, msgUser, req, res, session);
						break;						
					case A_SPECIAL_INST :
						procActionEspInst(mc, msgUser, req, res, session);
						break;	
						
					case R_ENTER_ADDITIONAL_CARDS :
						procReqEnterAditionalCard(mc, msgUser, req, res, session);
						break;
					case A_ENTER_ADDITIONAL_CARDS :
					case R_ADD_LIST :
						procReqAdditionalCardsList(mc, msgUser, req, res, session);
						break;
					case R_ADD_PRIMARY_CARD :
						procReqPrimaryCard(mc, msgUser, req, res, session);
						break;

					case A_ADD_LIST :
						procActionAditionalCardsList(mc, msgUser, req, res, session);
						break;
					case A_ADD_NEW_MAINT:
						procActionNewMaint(mc, msgUser, req, res, session);
						break;
					case A_ADD_CHANGE_STATUS :
						procActionChangeStatus(mc, msgUser, req, res, session);
						break;
					case A_ADD_PRIMARY_CARD :
						procActionPrimaryCard(mc, msgUser, req, res, session);
						break;
				
					default :
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
				}

				} catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 3;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
					//return;
				} finally {
					s.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}

		}

	}
	
	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

		}
	}


}