<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@page import="com.datapro.constants.EibsFields"%>

<%
	boolean readOnly=false;
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>

<jsp:useBean id="entity" class="datapro.eibs.beans.ESD400001Message"  scope="session" />

           <tr id="trdark"> 
             <td nowrap width="40%"> 
                <div align="right">Tipo Direcci�n :</div>
              </td>
              <td nowrap width="60%">  
                <% if ( !readOnly && entity.getE01RTP().equals("1")) { %>
	               <eibsinput:cnofc name="entity" flag="DT" property="E01ADT" fn_description="D01ADT" readonly="<%=readOnly %>"/>
	               <eibsinput:text  name="entity" property="D01ADT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" required="true" readonly="true"/>
    	        <%} else {%>
	               <eibsinput:cnofc name="entity" flag="DT" property="E01ADT" fn_description="D01ADT" readonly="<%=readOnly %>"/>
	               <eibsinput:text  name="entity" property="D01ADT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
     	        <% } %>
			  </td>
            </tr>

            <tr id="trclear"> 
              <td nowrap width="40%"> 
                <div align="right">Comuna :</div>
              </td>
              <td nowrap width="60%">  
                <% if ( !readOnly && entity.getE01RTP().equals("1")) { %>
	               <eibsinput:cnofc name="entity" flag="80" property="E01COM" fn_description="D01COM"  readonly="<%=readOnly %>"/>
	               <eibsinput:text  name="entity" property="D01COM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" required="true" readonly="true"/>
    	        <%} else {%>
	               <eibsinput:cnofc name="entity" flag="80" property="E01COM" fn_description="D01COM"  readonly="<%=readOnly %>"/>
	               <eibsinput:text  name="entity" property="D01COM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
     	        <% } %>
			  </td>
            </tr>

            <tr id="trdark"> 
              <td nowrap width="40%"> 
                <div align="right">Calle :</div>
              </td>
              <td nowrap width="60%">  
                <% if ( !readOnly && entity.getE01RTP().equals("1")) { %>
		           <eibsinput:text name="entity" property="E01MA2" required="false" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" required="true" readonly="<%=readOnly %>"/>
    	        <%} else {%>
		           <eibsinput:text name="entity" property="E01MA2" required="false" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="<%=readOnly %>"/>
     	        <% } %>
			  </td>
            </tr>

            <tr id="trclear"> 
              <td nowrap width="40%"> 
                <div align="right"> </div>
              </td>
              <td nowrap width="60%">  
	               <eibsinput:text  name="entity" property="E01MA3" required="false" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="<%=readOnly %>"/>
			  </td>
            </tr>
   
            <tr id="trdark"> 
              <td nowrap width="40%"> 
                <div align="right"> N&uacute;mero :</div>
              </td>
              <td nowrap width="60%">  
 	              <eibsinput:text  name="entity" property="E01NST" required="false" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="6" maxlength="5" readonly="<%=readOnly %>"/>
			  </td>
            </tr>
   
            <tr id="trclear"> 
              <td nowrap width="40%"> 
                <div align="right"> Casa/Depto :</div>
              </td>
              <td nowrap width="60%">  
	               <eibsinput:text  name="entity" property="E01MA4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="<%=readOnly %>"/>
			  </td>
            </tr>
   
            <tr id="trdark"> 
              <td nowrap width="40%"> 
                <div align="right"> Referencia :</div>
              </td>
              <td nowrap width="60%">  
	               <eibsinput:text  name="entity" property="E01CTR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>"  size="40" maxlength="35" readonly="<%=readOnly %>"/>
			  </td>
            </tr>

            <tr id="trclear"> 
              <td nowrap width="40%"> 
                <div align="right">C&oacute;digo Postal :</div>
              </td>
              <td nowrap width="60%">  
	               <eibsinput:text  name="entity" property="E01ZPC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PO_BOX %>" readonly="<%=readOnly %>"/>
                </td>
            </tr>

            <tr id="trdark"> 
              <td nowrap width="40%"> 
                <div align="right">Fecha Actualizaci&oacute;n :</div>
              </td>
              <td nowrap width="60%">  
                <% if ( !readOnly && entity.getE01RTP().equals("1")) { %>
	               <eibsinput:date name="entity" required="false" fn_year="E01DTY" fn_month="E01DTM" fn_day="E01DTD" required="true" readonly="<%=readOnly %>"/>
    	        <%} else {%>
	               <eibsinput:date name="entity" required="false" fn_year="E01DTY" fn_month="E01DTM" fn_day="E01DTD" readonly="<%=readOnly %>"/>
     	        <% } %>
                </td>
            </tr>

            <tr id="trclear"> 
              <td nowrap width="40%"> 
                <div align="right">Verificado por :</div>
              </td>
              <td nowrap width="60%">  
                <% if ( !readOnly && entity.getE01RTP().equals("1")) { %>
	               <eibsinput:cnofc name="entity" flag="43" property="E01ADF" fn_description="D01ADF"  readonly="<%=readOnly %>"/>
	               <eibsinput:text  name="entity" property="D01ADF" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" required="true" readonly="<%=readOnly %>"/>
    	        <%} else {%>
	               <eibsinput:cnofc name="entity" flag="43" property="E01ADF" fn_description="D01ADF"  readonly="<%=readOnly %>"/>
	               <eibsinput:text  name="entity" property="D01ADF" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>"  readonly="<%=readOnly %>"/>
     	        <% } %>
			  </td>
            </tr>

     <%         if ( !entity.getE01RTP().equals("5") ) {%>
     

            <tr id="trdark"> 
              <td nowrap width="40%"> 
                <div align="right">Estado Vigente :</div>
              </td>
              <td nowrap width="60%">  
	              <input type="radio"  name="E01FL1" <%=readOnly?"disabled":""%> value="Y" <%if (!entity.getE01FL1().equals("N")) out.print("checked"); %>  >S�
	              <input type="radio"  name="E01FL1" <%=readOnly?"disabled":""%> value="N" <%if (entity.getE01FL1().equals("N")) out.print("checked"); %>  >No
                </td>
            </tr>
 
<%         	 }%>
 