package datapro.eibs.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ECIF33001Message;
import datapro.eibs.beans.ECIF33003Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageRecord;

/**
 * 
 * @author evargas
 *
 */
public class JSECIF330 extends JSEIBSServlet {

	protected static final int STATEMENT_SEARCH = 100;
	protected static final int STATEMENT_SHOW = 200;
	protected static final int STATEMENT_SHOW_TRANSACC = 250;	
	protected static final int STATEMENT_PROCESS = 300;
	protected static final int A_CRYSTAL_REPORT = 500;
		

	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		switch (screen) {
			case STATEMENT_SEARCH:
				procSearchStatement(user, req, res, session);
			break;		

			case STATEMENT_SHOW:
				procStatementshow(user, req, res, session);
				break;	
			case STATEMENT_SHOW_TRANSACC:
				procStatementshowTransactionSelected(user, req, res, session);
				break;					
			case STATEMENT_PROCESS:
				procStatementProcess(user, req, res, session);
				break;					
			case A_CRYSTAL_REPORT :
				procActCrystalReport(user, req, res, session);
				break;
			default :
				//forward("MISC_not_available.jsp", req, res);
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}
	
	
	private int openConnection(URL url, OutputStream os)
		throws Exception {
		InputStream is = null;
		try {
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
		
			is = Util.getStreamFromObject(conn);
			return (int) copy(is, os);
		
		} catch (Exception e) {
			throw e;
		} finally {
			if (is != null)	is.close();
		}
	}
	private void procActCrystalReport(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException {
		
		System.out.println("Connecting to report generator...");
		OutputStream output = null;
		
		try {
			String tname = "cartola.rpt";
			String p = "&RPTPARAM_@ACCOUNT=" + req.getParameter("account");
			p += "&RPTPARAM_@REFERENCE=" + req.getParameter("reference");
			String http = JSEIBSProp.getCRVEngine() + "&TNAME=" + tname + p;
			URL url = new URL(http);
			
			res.reset();
			output = res.getOutputStream();
			int size = openConnection(url, output);
			res.setContentType("application/pdf");
			res.setContentLength(size);
			output.flush();
			System.out.println("Report sent to screen.");
		} catch (Exception e) {
			System.out.println("Exception e : " + e.getClass().getName() + " " + e.getMessage());
		} finally {
			if (output != null)	output.close();
		}
	}
	protected void procSearchStatement(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		forward("ECIF330_statement_account_enter.jsp", req, res);			
	}
	

	protected void procStatementshow(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession session)
			throws ServletException, IOException {
			
			MessageProcessor mp = null;
			try {
				mp = getMessageProcessor("ECIF330", req);

				ECIF33001Message msgList = (ECIF33001Message) mp.getMessageRecord("ECIF33001");
				msgList.setH01USERID(user.getH01USR());
				if (req.getParameter("E01SELACC")!=null && !"".equals(req.getParameter("E01SELACC").trim())){
					msgList.setE01SELACC(req.getParameter("E01SELACC"));
				}
				if (req.getParameter("E01SELRDY")!=null && !"".equals(req.getParameter("E01SELRDY").trim())){
					msgList.setE01SELRDY(req.getParameter("E01SELRDY"));
				}
				if (req.getParameter("E01SELRDM")!=null && !"".equals(req.getParameter("E01SELRDM").trim())){
					msgList.setE01SELRDM(req.getParameter("E01SELRDM"));
				}
				
			 	mp.sendMessage(msgList);
			
			 	MessageRecord resp = mp.receiveMessageRecord(); //receive first messaje, errors or header.
			 	 
		        if(resp.getFormatName().equals("ELEERR")){
		        	 session.setAttribute("error", ((ELEERRMessage)resp));
		        	 forward("ECIF330_statement_account_enter.jsp", req, res);
		        }else{
		        	JBObjList list = mp.receiveMessageRecordList("H02FLGMAS");
		        	session.setAttribute("header", resp);	//header dds01
		        	session.setAttribute("stmlist", list);
					forwardOnSuccess("ECIF330_statement_account_list.jsp", req, res);
		        }

			} finally {
				if (mp != null)	mp.close();
			}
		}

	protected void procStatementshowTransactionSelected(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession session)
			throws ServletException, IOException {
			
			MessageProcessor mp = null;
			try {
				mp = getMessageProcessor("ECIF330", req);

				ECIF33003Message msg = (ECIF33003Message) mp.getMessageRecord("ECIF33003");
				
				setMessageRecord(req, msg);//caption  search value				
				msg.setH03USERID(user.getH01USR());				
				if (req.getParameter("E01SELACC")!=null && !"".equals(req.getParameter("E01SELACC").trim())){
					msg.setE03SELACC(req.getParameter("E01SELACC"));
				}
				if (req.getParameter("E01SELNUM")!=null && !"".equals(req.getParameter("E01SELNUM").trim())){
					msg.setE03SELNUM(req.getParameter("E01SELNUM"));
				}
				
				try {
					if (req.getParameter("Pos")!=null & !"".equals(req.getParameter("Pos"))){
						//get to header of session.
						ECIF33003Message header=(ECIF33003Message)session.getAttribute("header");
						populate(header, msg);
						msg.setE03NUMREC(req.getParameter("Pos"));						
					}
				} catch (Exception e) {//nothing
				}
				
			 	mp.sendMessage(msg);
			
			 	MessageRecord resp = mp.receiveMessageRecord(); //receive first messaje, errors or header.
			 	 
		        if(resp.getFormatName().equals("ELEERR")){
		        	 ECIF33003Message hmsg = new ECIF33003Message();
		        	 setMessageRecord(req, hmsg);//caption  search value	
		        	 session.setAttribute("headerTrans", hmsg);
		        	 req.setAttribute("TYPE", "S");
		        	 session.setAttribute("error", ((ELEERRMessage)resp));
		        	 forward("ECIF330_statement_account_enter.jsp", req, res);
		        }else{
		        	session.setAttribute("header", ((ECIF33003Message)resp));	//header dds01		        	
				 	JBObjList list = mp.receiveMessageRecordList("H04FLGMAS","E04NUMREC");
		        	session.setAttribute("stmlist", list);
					forwardOnSuccess("ECIF330_statement_transaction_selected_list.jsp", req, res);
		        }

			} finally {
				if (mp != null)	mp.close();
			}
		}
	

	protected void procStatementProcess(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {
			
			MessageProcessor mp = null;
			try {
				
				//proceso presentacion del estado de cuenta
				
			} finally {
				if (mp != null)	mp.close();
			}
		}		

 }