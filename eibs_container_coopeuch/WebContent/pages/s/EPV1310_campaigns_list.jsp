<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV131001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html> 
<head>
<title>Conciliaci�n Bancos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ERClist" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

  $(function(){
     $("#radio_key").attr("checked", false);
  });


  function goAction(op) 
  {
    var ok = false;

    for(n=0; n<document.forms[0].elements.length; n++)
	  {
	     var element = document.forms[0].elements[n];
	     if(element.name == "radio_key") 
	     {	
	        if (element.checked == true) {
	      	   document.getElementById("codigo_lista").value = element.value; 
        	   ok = true;
        	   break;
			}
	      }
	   }

     if ( ok || op=="100") 
     {
	   document.forms[0].SCREEN.value = op;
	   document.forms[0].submit();		
     } 
     else 
		alert("Debe seleccionar un registro para continuar.");	   
  }
</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Consulta de Campa&ntilde;as<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="campaigns_list.jsp, EPV1310"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1310">
<input type="hidden" name="SCREEN" value="300">
<input type="hidden" name="codigo_lista" value="" id="codigo_lista">  

<input type="hidden" name="E01CMPIDC" value="<%=request.getParameter("E01CMPIDC") %>"> 
<input type="hidden" name="E01CCHREC" value="<%=request.getAttribute("E01CCHREC") %>"> 
<input type="hidden" name="E01FVIGDD" value="<%=request.getParameter("E01FVIGDD") %>"> 
<input type="hidden" name="E01FVIGDM" value="<%=request.getParameter("E01FVIGDM") %>"> 
<input type="hidden" name="E01FVIGDA" value="<%=request.getParameter("E01FVIGDA") %>"> 
<input type="hidden" name="E01FVIGHD" value="<%=request.getParameter("E01FVIGHD") %>"> 
<input type="hidden" name="E01FVIGHM" value="<%=request.getParameter("E01FVIGHM") %>"> 
<input type="hidden" name="E01FVIGHA" value="<%=request.getParameter("E01FVIGHA") %>"> 

   <table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td id="trdark"> 
        <table cellspacing="0" align="center" cellpadding="2" border="0" width="100%">
         <tr>
		<td nowrap width="10%" align="center" colspan="4"><h4>CRITERIOS DE CONSULTA</h4></td>
		</tr>
		
          <tr>
             <td nowrap width="10%" align="right">ID Campa&ntilde;as:</td>
             <td nowrap width="20%" align="left" colspan="3">
             	<input type="text" name="IdCampana" value="<% out.print(session.getAttribute("IdCampana"));%>" size="23" readonly>
             </td>    
             <td nowrap width="15%" align="right">Fecha Carga Desde : </td>
             <td nowrap width="17%"align="left">
 				<input type="text" name="E01fecha" size="15"  value="<% out.print(request.getAttribute("fecha_inicial")); %>" readonly>
             </td>
        	<td nowrap width="15%" align="right">Fecha Carga Hasta : 
            </td>
             <td nowrap width="17%"align="left">
 				<input type="text" name="E01fecha2" size="15"  value="<% out.print(request.getAttribute("fecha_final")); %>" readonly>
             </td>
             </tr>
        
        </table>
      </td>
    </tr>
  </table>
 
 


<%
	if (ERClist.getNoResult()) {
%>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('100')"><b>Atras</b></a>
		</td>
	</tr>
</table>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>


<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('300')"><b>Consultar</b></a>
		</td>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('100')"><b>Atras</b></a>
		</td>
	</tr>
</table>

<table class="tbenter" width=100% >
<tr><td height="20"></td></tr>

<tr>
<td>

<table id="headTable"  width="100%" align="left">
		<tr id="trdark">
			<th align="center" nowrap width="30"></th>
			<th align="center" nowrap width="100">Id Carga</th>
			<th align="center" nowrap width="100">Fecha Carga</th>
			<th align="center" nowrap width="100">Estado</th>
			<th align="center" nowrap width="50">Registros Correctos</th>
			<th align="center" nowrap width="50">Registros con Error</th>
			<th align="center" nowrap width="100">Usuario</th>
		</tr>
		<%
			ERClist.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (ERClist.getNextRow()) {
					//if (firstTime) {
						//firstTime = false;
						//chk = "checked";
					//} else {
					//	chk = "";
				//	}
				EPV131001Message pvprd = (EPV131001Message) ERClist.getRecord();
		%>
		<tr>
		
		<td nowrap><input type="radio" name="radio_key" id="radio_key" value="<%= ERClist.getCurrentRow()%>" <%=chk%>/></td>
		<td nowrap align="center"><%=pvprd.getE01CMPIDC() %></td>
		<td nowrap align="center"><% out.print(pvprd.getE01CMPVAD()+"/"+pvprd.getE01CMPVAM()+"/"+pvprd.getE01CMPVAY());  %></td>
		<td nowrap align="center"><%=pvprd.getE01CMPGST() %></td>
 	  	<td nowrap align="center"><%=pvprd.getE01CMPCAR() %></td>
 	  	<td nowrap align="center"><%=pvprd.getE01CMPERR() %></td>
	    <td nowrap align="center"><%=pvprd.getE01CMPVAU() %></td>	
 		</tr>
		<%
			}
		%>
	</table>
</td>
</tr>	

	<tr>
	<td>
	<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (ERClist.getShowPrev()) {
					int pos = ERClist.getFirstRec() - 11;					
					
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1310?SCREEN=200&posicion="
									+ pos
									+"&mark=atr�s"

									+"&E01CMPIDC="+request.getParameter("E01CMPIDC")

									+"&E01CCHREC="+request.getParameter("E01CCHREC")

									+"&E01FVIGDD="+request.getParameter("E01FVIGDD")
									+"&E01FVIGDM="+request.getParameter("E01FVIGDM")
									+"&E01FVIGDA="+request.getParameter("E01FVIGDA")

									+"&E01FVIGHD="+request.getParameter("E01FVIGHD")
									+"&E01FVIGHM="+request.getParameter("E01FVIGHM")
									+"&E01FVIGHA="+request.getParameter("E01FVIGHA")									
								

									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (ERClist.getShowNext()) {
					int pos = ERClist.getLastRec();
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1310?SCREEN=200&posicion="
									+pos
									+"&mark=adelante"

									+"&E01CMPIDC="+request.getParameter("E01CMPIDC")

									+"&E01CCHREC="+request.getParameter("E01CCHREC")

									+"&E01FVIGDD="+request.getParameter("E01FVIGDD")
									+"&E01FVIGDM="+request.getParameter("E01FVIGDM")
									+"&E01FVIGDA="+request.getParameter("E01FVIGDA")

									+"&E01FVIGHD="+request.getParameter("E01FVIGHD")
									+"&E01FVIGHM="+request.getParameter("E01FVIGHM")
									+"&E01FVIGHA="+request.getParameter("E01FVIGHA")									
									
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/next_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>


</table>
</td>	
</tr>	
</table>
<%
	}
%>


	 
 

</form>
</body>
</html>
