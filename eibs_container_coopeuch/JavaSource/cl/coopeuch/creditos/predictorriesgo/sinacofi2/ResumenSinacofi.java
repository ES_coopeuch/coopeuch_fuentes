//
// Generated By:JAX-WS RI IBM 2.1.1 in JDK 6 (JAXB RI IBM JAXB 2.1.3 in JDK 1.6)
//


package cl.coopeuch.creditos.predictorriesgo.sinacofi2;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;


@WebServiceClient(name = "ResumenSinacofi", targetNamespace = "http://resumensinacofi.core.coopeuch.cl/resumensinacofi", wsdlLocation = "WEB-INF/wsdl/ResumenSinacofi.wsdl")
public class ResumenSinacofi
    extends Service
{

    private final static URL RESUMENSINACOFI_WSDL_LOCATION;

    static {
        URL url = null;
        try {
            url = new URL("file:./WEB-INF/wsdl/ResumenSinacofi.wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        RESUMENSINACOFI_WSDL_LOCATION = url;
    }

    public ResumenSinacofi(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ResumenSinacofi() {
        super(RESUMENSINACOFI_WSDL_LOCATION, new QName("http://resumensinacofi.core.coopeuch.cl/resumensinacofi", "ResumenSinacofi"));
    }

    /**
     * 
     * @return
     *     returns ResumenSinacofiPortType
     */
    @WebEndpoint(name = "ResumenSinacofiSOAP")
    public ResumenSinacofiPortType getResumenSinacofiSOAP() {
        return (ResumenSinacofiPortType)super.getPort(new QName("http://resumensinacofi.core.coopeuch.cl/resumensinacofi", "ResumenSinacofiSOAP"), ResumenSinacofiPortType.class);
    }

}
