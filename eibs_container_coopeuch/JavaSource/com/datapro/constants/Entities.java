package com.datapro.constants;

/**
 * @author csepulveda
 *
 */
public interface Entities {

	public static final String ENTITY_TYPE_CONTACT 					 		= "C";
	public static final String ENTITY_TYPE_PERSONAL_REFERENCE		 		= "P";
	public static final String ENTITY_TYPE_SIGNER 					 		= "L";
	public static final String ENTITY_TYPE_LEGAL_REPRESENTATIVES	 		= "H";
	public static final String ENTITY_TYPE_BENEFICIARIES        	 		= "J";
	public static final String ENTITY_TYPE_DOCUMENTS				 		= "K";
	public static final String ENTITY_TYPE_SECONDARY_NAME_FOR_CREDIT 		= "T";
	public static final String ENTITY_TYPE_MAILING_ADDRESS 	  		 		= "1"; 
	public static final String ENTITY_TYPE_STOCK_HOLDER 	  		 		= "2";
	public static final String ENTITY_TYPE_BOARD_OF_DIRECTORS 	     		= "3";
	public static final String ENTITY_TYPE_CUSTOMER_BENEFICIARIES 	    	= "4";
	public static final String ENTITY_TYPE_CUSTOMER_LEGAL_REPRESENTATIVES 	= "5";
	public static final String ENTITY_TYPE_BANK_REFERENCES 	  				= "6";
	public static final String ENTITY_TYPE_COMMERCIAL_REFERENCES 	  		= "7";
	public static final String ENTITY_TYPE_ACCEPTANCES 	  					= "8";
	public static final String ENTITY_TYPE_CUSTOMER_ASSETS	  				= "9";
	public static final String ENTITY_TYPE_CUSTOMER_LIABILITIES	  			= "A";
	public static final String ENTITY_TYPE_VALUE_OF_ASSETS 	  				= "B";
	public static final String ENTITY_TYPE_MANAGERS_WITH_CREDIT 	  		= "C";
	public static final String ENTITY_TYPE_DISC_ECONOMIC_ACTIVITY 	  		= "D";
	public static final String ENTITY_TYPE_COLLATERAL_ADDRESS 	  			= "E";
	public static final String ENTITY_TYPE_CREDIT_LINE_AVALS 	  			= "R";
	public static final String ENTITY_TYPE_INSURANCE_BENEFICIARIES 	  		= "X";
	 

}
