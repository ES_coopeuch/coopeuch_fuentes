<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import = "datapro.eibs.master.*,datapro.eibs.beans.*" %>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Tarjetas Creditos/Debitos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id= "ECC0040Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script type="text/javascript">

</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Consulta de Tarjetas de Creditos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cc_inq_card_adic_list.jsp,ECC0040"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0040">
<input type="hidden" name="SCREEN" value="000"> 
 
<h3><b>Adicionales Tarjeta de Credito</b></h3> 
 <% int row = 0;%>
<h4>Cliente</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCUN" size="13" maxlength="12" readonly value="<%= userPO.getCusNum().trim()%>">                  
               </div>               
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre : </b></div>
            </td>
            <td nowrap width="20%"  > 
              <div align="left"> 
                <input type="text" name="E01CCMNME" size="35" maxlength="35" readonly value="<%= userPO.getCusName().trim()%>">                                 
              </div>
            </td>            
            <td nowrap width="16%" > 
              <div align="right"><b>Identif. Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCID" size="15" maxlength="15" readonly value="<%= userPO.getIdentifier().trim()%>">                
               </div>               
            </td>            
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta IBS : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCMACC" size="13" maxlength="12" readonly value="<%= userPO.getAccNum().trim() %>">                                 
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMCCY" size="5" maxlength="4" readonly value="<%= userPO.getCurrency().trim() %>">                              
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Oficial : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMOFC" size="5" maxlength="4" readonly value="<%= userPO.getOfficer().trim() %>">                               
                </b> </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
          <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMPRO" size="5" maxlength="4" readonly value="<%= userPO.getProdCode().trim() %>">                  
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Desc. Producto : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="D01CCMPRO" size="35" maxlength="35" readonly value="<%= userPO.getHeader20().trim()%>">                              
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Nro. Cuenta : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCRNXN" size="20" maxlength="20" readonly value="<%= userPO.getHeader21().trim() %>">                              
              </div>
            </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>
  <p>&nbsp;</p>
<%
	if (ECC0040Help.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>
<%
	} else { %>	
 <TABLE class="tbenter" width="100%">
	<TR>
		<TD ALIGN=CENTER class=TDBKG><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</TD>
	</TR>
</TABLE>
	<table class="tableinfo" id="dataTable" width="100%">
		<tr id="trclear">
			<th align="center" nowrap width="15%">Tarjeta</th>
			<th align="center" nowrap width="10%">Rut <br> Adicional</th>
			<th align="center" nowrap width="15%">Nombre <br> Adicional</th>
    		<th align="center" nowrap width="10%">Fecha<br> Apertura</th>
    		<th align="center" nowrap width="10%">Fecha <br> Activacion</th>
       		<th align="center" nowrap width="10%">  <br> Estado</th>
    		<th align="center" nowrap width="10%">  <br> Bloqueo</th>
       		<th align="center" nowrap width="10%">Cupo Diferenciado <br>Nacional</th>
       		<th align="center" nowrap width="10%"> Cupo Diferenciado <br>Internacional</th>
		</tr>
            <%
               ECC0040Help.initRow();
        		while (ECC0040Help.getNextRow()) 
        		{
                  datapro.eibs.beans.ECC004001Message msgList = (datapro.eibs.beans.ECC004001Message) ECC0040Help.getRecord();
		   %>
			<tr id="trdark">										
			<td nowrap align="left"><%=msgList.getE01CCRNUM().trim()%></td>	
			<td nowrap align="left"><%=msgList.getE01CCRCI2().trim()%></td>	
			<td nowrap align="left"><%=msgList.getE01CCRNAM().trim()%></td>				
			<td nowrap align="center"><%=Util.formatDate(msgList.getE01CCRISD(), msgList.getE01CCRISM(), msgList.getE01CCRISY())%></td>
			<td nowrap align="center"><%=Util.formatDate(msgList.getE01CCRATD(), msgList.getE01CCRATM(), msgList.getE01CCRATY())%></td>				
			<td nowrap align="center"><% if (msgList.getE01CCRSTS().equals("01")) {out.print("Activa");} else {out.print("Inactiva");}%></td> 
			<td nowrap align="center"><%=msgList.getD01CCRLKC().trim()%></td>	
			<td nowrap align="right" ><%=Util.formatCCY(msgList.getE01CCRCDN())%></td>	
			<td nowrap align="right" ><%=Util.formatCCY(msgList.getE01CCRCDR())%> </td>	
		</tr>
		<%}		%>
	</table>

<%
	}
%>
<SCRIPT language="JavaScript">
  
 function tableresize() {
    adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  tableresize();
  window.onresize=tableresize;   
</SCRIPT>
</form>
<SCRIPT language="JavaScript">
 
</SCRIPT>

</body>
</html>

