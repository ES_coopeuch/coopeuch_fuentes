package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class SinacofiWSPortTypeProxy implements cl.Sinacofi.coopeuch.creditos.predictorriesgo.SinacofiWSPortType {
  private boolean _useJNDI = true;
  private String _endpoint = null;
  private cl.Sinacofi.coopeuch.creditos.predictorriesgo.SinacofiWSPortType __sinacofiWSPortType = null;
  
  public SinacofiWSPortTypeProxy() {
    _initSinacofiWSPortTypeProxy();
  }
  
  private void _initSinacofiWSPortTypeProxy() {
  
    if (_useJNDI) {
      try {
        javax.naming.InitialContext ctx = new javax.naming.InitialContext();
        __sinacofiWSPortType = ((cl.Sinacofi.coopeuch.creditos.predictorriesgo.SinacofiWSService)ctx.lookup("java:comp/env/service/SinacofiWSService")).getSinacofiWSPort();
      }
      catch (javax.naming.NamingException namingException) {}
      catch (javax.xml.rpc.ServiceException serviceException) {}
    }
    if (__sinacofiWSPortType == null) {
      try {
        __sinacofiWSPortType = (new cl.Sinacofi.coopeuch.creditos.predictorriesgo.SinacofiWSServiceLocator()).getSinacofiWSPort();
        
      }
      catch (javax.xml.rpc.ServiceException serviceException) {}
    }
    if (__sinacofiWSPortType != null) {
      if (_endpoint != null)
        ((javax.xml.rpc.Stub)__sinacofiWSPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
      else
        _endpoint = (String)((javax.xml.rpc.Stub)__sinacofiWSPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
    }
    
  }
  
  
  public void useJNDI(boolean useJNDI) {
    _useJNDI = useJNDI;
    __sinacofiWSPortType = null;
    
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (__sinacofiWSPortType != null)
      ((javax.xml.rpc.Stub)__sinacofiWSPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.Sinacofi.coopeuch.creditos.predictorriesgo.SinacofiWSPortType getSinacofiWSPortType() {
    if (__sinacofiWSPortType == null)
      _initSinacofiWSPortTypeProxy();
    return __sinacofiWSPortType;
  }
  
  public cl.Sinacofi.coopeuch.creditos.predictorriesgo.Salida sinacofiWS(cl.Sinacofi.coopeuch.creditos.predictorriesgo.DatosConsulta defaultInput) throws java.rmi.RemoteException{
    if (__sinacofiWSPortType == null)
      _initSinacofiWSPortTypeProxy();
    return __sinacofiWSPortType.sinacofiWS(defaultInput);
  }
  
  public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResumenAntecedentesComercialesResponse resumenAntecedentesComerciales(cl.Sinacofi.coopeuch.creditos.predictorriesgo.DatosConsultaSum defaultInput) throws java.rmi.RemoteException{
    if (__sinacofiWSPortType == null)
      _initSinacofiWSPortTypeProxy();
    return __sinacofiWSPortType.resumenAntecedentesComerciales(defaultInput);
  }
  
  
}