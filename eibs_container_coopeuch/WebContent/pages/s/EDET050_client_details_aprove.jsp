<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.List"%>
<%@page import="datapro.eibs.beans.EDET05002Message"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datapro.eibs.beans.EDET05001Message"%>
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"   scope="session" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mantenedor Deterioro</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR"
	content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<TITLE>Conexi�n</TITLE>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>
<style type="text/css">
.cabecera{
	background-color: #ffffff;
	border: 1px;
	text-align: center;
}
.numero{
	text-align: right;
}
td{
	font-size: 8pt;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
body {
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
</style>
</head>
<body>
<%
	List<EDET05002Message> detalles = new ArrayList<EDET05002Message>();
	EDET05001Message msgClient = (EDET05001Message) session.getAttribute("EDET05001Message");
	detalles = (List<EDET05002Message>) session.getAttribute("EDET05002Message");
%>
<table style="background-color: #ffffff" cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead" align="center">
	<tr>
		<td nowrap>
			<div>
				<h3 align="center">Mantenedor Deterioro - Aprobaci&oacute;n<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="client_details_aprove, EDET050"></h3>
				<hr size="4" color="red">
			</div>
		</td>
	</tr>
</table>
<br>
<form action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSEDET050">
<input type="hidden" name="SCREEN" value="700" >
<input type="hidden" name="E01FLGDET" value="<%=msgClient.getE01FLGDET()%>" >
<table width="100%" style="background-color: #ffffff" border="0">
<tr>
<td nowrap style="width: 100%"><h4>Informaci&oacute;n del Cliente</h4></td></tr>
<tr><td nowrap style="width: 100%"><h4 style="text-align:right; margin-bottom: 0px">ESTADO RUT: <font style="color: ff6600" color="ff6600"><%
			if (msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")) {
		%>
		DETERIORADO
		<%
			} else {
		%>
		NO DETERIORADO
		<%
			}
		%>
		</font></h4></td>
</tr>
</table>
<table class="TABLEINFO">
	<tr>
		<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
			<tr style="width: 100%; background-color: #ffffff; font-size: 8pt; color: #990000; height:20pt;">
				<td nowrap style="width: 5%"></td>
				<td nowrap style="width: 3%; text-align: right;">
					<div align="right"><b>Cliente :</b></div>
				</td>
				<td nowrap style="width: 5%;">
					<input type="text" style="width: 90%" readonly="readonly" value="<%=msgClient.getE01NUMCLI()%>" name="E01NUMCLI">
				</td>
				<td nowrap style="width: 22%;">
					<input style="width: 90%" type="text" readonly="readonly" value="<%=msgClient.getE01CNA1()%>" name="E01CNA1">
				</td>
				<td nowrap style="width: 20%;">
					<b>
						RUT :
					</b> 
					<input type="text" value="<%=msgClient.getE01NRORUT()%>" name="E01NRORUT" readonly="readonly">
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<br>
<table width="100%" style="background-color: #ffffff" border="0">
<tr>
<td nowrap style="width: 100%"><h4>Solicitud</h4></td></tr>
<tr>
	<td nowrap style="width: 100%">
		<h4 style="text-align:right; margin-bottom: 0px">
			<% if((!msgClient.getE01ESTSOL().trim().equals(""))){ %>
			<font color="red">PENDIENTE DE APROBACI&Oacute;N</font>
			<% }else { %>
			<% } %>
		</h4>
	</td>
</tr>
</table>
<table style="width: 100%" class="TABLEINFO">
	<tr>
		<td nowrap>
			<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
				<tbody>
					<tr style="background-color: #ffffff; border-color: #ffffff">
						<td nowrap style="width: 17%">
							<div align="right">Fecha Ingreso :</div>
						</td>
						<td nowrap style="width: 20%">
							<input size="2" type="text" readonly="readonly" value="<%=msgClient.getE01FCHIDD()%>" name="E01FCHIDD">
							<input size="2" type="text" readonly="readonly" value="<%=msgClient.getE01FCHIMM()%>" name="E01FCHIMM">
							<input size="4" type="text" readonly="readonly" value="<%=msgClient.getE01FCHIAA()%>" name="E01FCHIAA">
						</td>
						<td nowrap style="width: 5%">
							<div align="right">Usuario :</div>
						</td>
						<td nowrap style="width: 25%"> 
							<input type="text" readonly="readonly" value="<%=msgClient.getE01USER()%>" name="E01USER">
						</td>
					</tr>
					<tr id="trdark">
						<td nowrap style="width: 17%">
							<div align="right">Fecha Vencimiento :</div>
						</td> 
						<td nowrap style="width: 20%">
							<input size="2" type="text" readonly="readonly" value="<%=msgClient.getE01FCHVDD()%>" name="E01FCHVDD">
							<input size="2" type="text" readonly="readonly" value="<%=msgClient.getE01FCHVMM()%>" name="E01FCHVMM">
							<input size="4" type="text" readonly="readonly" value="<%=msgClient.getE01FCHVAA()%>" name="E01FCHVAA">
						</td>
						<td nowrap style="width: 5%">
							<div align="right">Expira en :</div> 
						</td>
						<td nowrap style="width: 25%">
							<input type="text" size="5" readonly="readonly" value="<%=msgClient.getE01PLAZO()%>" name="E01PLAZO"> D&iacute;as</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</table>
<br>
<table width="100%" style="background-color: #ffffff" border="0">
<tr>
	<td nowrap style="width: 100%">
		<h4>Historial</h4>
	</td>
</tr>
</table>
<table style="width: 100%;" class="TABLEINFO" cellpadding="2" cellspacing="0">
	<tr style="width: 100%; background-color: #ffffff; font-size: 8pt; color: #990000; height:20pt;">
		<td nowrap style="width: 25%"><div align="right">Tipo �lt. <%
			if (msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")) {
		%>
		Marca
		<%
			} else {
		%>
		Desmarca
		<%
			}
		%> :</div></td>
		<td nowrap colspan="3">
			<input type="text" size="20" readonly="readonly" name="E01ULTMAR" value="<%if (msgClient.getE01ULTMAR().trim().equals("A")) {%><%="Automatico"%><%} else if (msgClient.getE01ULTMAR().trim().equals("M")) {%><%="Manual"%><%} else {%><%=""%><%}%>">
		</td>
	</tr>
	<tr>
		<td nowrap style="width: 25%"><div align="right">Fecha �lt. <%
			if (msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")) {
		%>
		Marca
		<%
			} else {
		%>
		Desmarca
		<%
			}
		%> :</div></td>
		<td nowrap style="width: 17%;">
			<input type="text" size="2" readonly="readonly" value="<% if(msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")){out.print(msgClient.getE01FCHMDD());}else{out.print(msgClient.getE01FCHDDD());}%>" name="<% if(msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")){out.print("E01FCHMDD");}else{out.print("E01FCHDDD");}%>">
			<input size="2" type="text" readonly="readonly" value="<% if(msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")){out.print(msgClient.getE01FCHMMM());}else{out.print(msgClient.getE01FCHDMM());}%>" name="<% if(msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")){out.print("E01FCHMMM");}else{out.print("E01FCHDMM");}%>">
			<input type="text" size="4" readonly="readonly" value="<% if(msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")){out.print(msgClient.getE01FCHMAA());}else{out.print(msgClient.getE01FCHDAA());}%>" name="<% if(msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")){out.print("E01FCHMAA");}else{out.print("E01FCHDAA");}%>">
		</td>
		<td nowrap style="width: 20%">
			<div align="right">Usuario �lt. <%
			if (msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")) {
		%>
		Marca
		<%
			} else {
		%>
		Desmarca
		<%
			}
		%> :</div> 
		</td>
		<td nowrap>
			<input type="text" readonly="readonly" value="<% if(msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")){out.print(msgClient.getE01USERM());}else{out.print(msgClient.getE01USERD());}%>" name="E01USERM">
		</td>
	</tr>
	</table>
<br>
<table width="100%" style="background-color: #ffffff" border="0">
<tr>
	<td nowrap style="width: 100%">
		<h4>Clasificaci&oacute;n</h4>
	</td>
</tr>
</table>
<table style="width: 100%;" class="TABLEINFO" cellpadding="2" cellspacing="0">
	<tr style="width: 100%; background-color: #ffffff; font-size: 8pt; color: #990000; height:20pt;">
		<td nowrap style="width: 25%;"><div align="right">Tipo Evaluaci&oacute;n :</div></td>
		<td nowrap style="width: 30%;">
			<input type="text" size="20" readonly="readonly" name="E01OPERAC" value="<% if(msgClient.getE01OPERAC().trim().equals("I")){%><%="Individual"%><%}else{%><%="Grupal"%><%}%>">
		</td>
		<td nowrap style="width: 7%">
			<div align="right">Letra Riesgo :</div> 
		</td>
		<td>
			<input type="text" size="5" readonly="readonly" value="<%=msgClient.getE01CODRSK()%>" name="E01CODRSK">
		</td>
	</tr>
</table>
<br>
<table width="100%" style="background-color: #ffffff" border="0">
<tr>
	<td nowrap style="width: 100%">
		<h4>Pr&eacute;stamos Vigentes</h4>
	</td>
</tr>
</table>
<table style="width: 100%;" class="TABLEINFO">
	<tr style="width: 100%; background-color: #ffffff; font-size: 8pt; color: #990000; height:20pt; text-align: center;">
		<td style="width: 60%" colspan="7" class="cabecera"><b>OPERACI&Oacute;N</b></td>
	</tr>
	<tr>
		<% if (msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")) {%> <td nowrap class="cabecera"><b>TIPO</b></td> <% } %>
		<td nowrap class="cabecera"><b>GRUPO</b></td>
		<td nowrap class="cabecera"><b>N&Uacute;MERO</b></td>
		<td nowrap class="cabecera"><b>ESTADO</b></td>
		<td nowrap class="cabecera"><b>SALDO</b></td>
		<td nowrap class="cabecera"><b>D&Iacute;AS MORA</b></td>
		<td nowrap class="cabecera"><b>FECHA VENC. CUOTA</b></td>
	</tr>
	<% if (detalles.size() > 0){ %>
		<% for (EDET05002Message message : detalles) { %>
		<tr>
			<% if (msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")) {%> <td><%if (message.getE02TIPOPE().trim().equalsIgnoreCase("P")) {out.print("PRINCIPAL");}else if (!message.getE02TIPOPE().trim().equalsIgnoreCase("")){out.print("ARRASTRADA");}%></td> <% } %>
			<td><%if (message.getE02GRPOPE().trim().equalsIgnoreCase("H")) {out.print("HIPOTECARIO");}else if (message.getE02GRPOPE().trim().equalsIgnoreCase("P")){out.print("PLANILLA");}else{out.print("OTROS");}%></td>
			<td class="numero"><%=message.getE02NUMOPE()%></td>
			<td><% if(message.getE02ESTOPE().trim().equalsIgnoreCase("C")){out.print("CERRADA");}else if(message.getE02ESTOPE().trim().equalsIgnoreCase("1")){out.print("VIGENTE");}else if(message.getE02ESTOPE().trim().equalsIgnoreCase("2")){out.print("VENCIDO");} else if(message.getE02ESTOPE().trim().equalsIgnoreCase("3")){out.print("CASTIGADO");} else if(message.getE02ESTOPE().trim().equalsIgnoreCase("4")){out.print("CASTIGADO N/INF");}%></td>
			<td class="numero"><%=message.getE02SALOPE()%></td>
			<td class="numero"><%=message.getE02DIASMO()%></td>
			<td style="text-align: center;"><%=message.getE02FCHVDD() + "/" + message.getE02FCHVMM() + "/" + message.getE02FCHVYY()%></td>
		</tr>
	<% }} else { %>
		<tr>
			<td colspan="<% if (msgClient.getE01FLGDET().trim().equalsIgnoreCase("Y")) {out.print("7");} else {out.print("6");}%>" style="text-align: center;">NO POSEE OPERACIONES VIGENTES</td>
		</tr>
	<% }%>
</table>
<br>
<table width="100%" style="background-color: #ffffff" border="0">
<tr>
	<td nowrap style="width: 100%">
		<h4>Deterioro</h4>
	</td>
</tr>
</table>
<table style="width: 100%;" class="TABLEINFO" cellpadding="2" cellspacing="0">
	<tr style="width: 100%; background-color: #ffffff; font-size: 8pt; color: #990000; height:20pt;">
		<td nowrap style="width:5%; border: 0px;">Acci&oacute;n :</td>
		<td nowrap style="text-align: left; border: 0px">
		<% if(msgClient.getE01FLGMAR().trim().equals("D")){ %>
		<input type="radio" readonly="readonly" name="marca" <% if(!error.getERRNUM().equals("0") || (!msgClient.getE01ESTSOL().trim().equals(""))){out.print("checked=\"checked\"");} %>> <label>Desmarca</label>
		<% }else if (msgClient.getE01FLGMAR().trim().equals("M")){ %>
		<input type="radio" readonly="readonly"  name="marca" <% if(!error.getERRNUM().equals("0") || (!msgClient.getE01ESTSOL().trim().equals(""))){out.print("checked=\"checked\"");} %>> <label>Marca</label>
		<% } %>
		<img src="<%=request.getContextPath()%>/images/Check.gif" alt="Campo Obligatorio" align="bottom" border="0"/>
		</td>
	</tr>
</table>
<input type="hidden" value="<%=msgClient.getE01FLGMAR()%>" name="E01FLGMAR">
<br>
<table width="100%" style="background-color: #ffffff" border="0" cellpadding="2" cellspacing="0">
<tr>
	<td nowrap style="width: 100%">
		<h4>Informaci&oacute;n Adicional</h4>
	</td>
</tr>
</table>
<table style="width: 100%; background-color: #ffffff;" class="TABLEINFO">
	<tr style="width: 100%">
		<td nowrap style="width: 25%"></td>
		<td nowrap style="width: 50%">Motivo :<input type="text" id="E01MOTIVO" name="E01MOTIVO" value="<%=msgClient.getE01MOTIVO()%>" size="4" readonly="readonly"> 
				<img src="<%=request.getContextPath()%>/images/Check.gif" alt="Campo Obligatorio" align="bottom" border="0"/>
			<input type="text" name="E01TXTMOT" readonly="readonly" value="<%=msgClient.getE01TXTMOT()%>" size="75"> </td>
		<td nowrap style="width: 25%"></td>
	</tr>
</table>
	<% if (!error.getERRNUM().equals("0")){%>
		<% if (error.getERNU01().trim().equalsIgnoreCase("8263")){ %>
		<br>
		<table width="100%" align="center" style="background-color: #ffffff">
			<tr style="width: 100%;text-align: center;">
				<td nowrap style="width: 100%;text-align: center;">
					<h4 style="width: 100%; text-align: center;"><input type="checkbox" id="chekea"><font color="red"> <b>Aceptar con Advertencias</b></font>
					<input type="hidden" name="H01FLGWK3" value="A"></h4>
				</td>
			</tr>
		</table>					
	<% }} %>
<table style="width: 100%; background-color: #ffffff;">
	<tr style="width: 100%">
		<td nowrap style="text-align: center;">
		<input id="EIBSBTN" type="button" value="Volver" onclick="back()">&nbsp;
		<% if(msgClient.getE01FLGMAR().trim().equals("D") || msgClient.getE01FLGMAR().trim().equals("M")){ %> 
			<input id="EIBSBTN" type="button" value="Aprobar" onclick="validate()"> 
		<% }  %> 
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
function back() {
    window.location.assign("<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSEDET050?SCREEN=400");
}
function validate(){
	marca = document.getElementsByName("marca");
	var seleccionado = false;
	for(var i=0; i<marca.length; i++) {    
		if(marca[i].checked) {
	    	seleccionado = true;
	    	break;
  		}
	}
	if (!seleccionado){
		alert('Debe seleccionar la acci�n de <%if(msgClient.getE01FLGMAR().trim().equals("D")){out.print("Desmarca");}else{out.print("Marca");}%>');
		return false;
	}
	
	valor = document.getElementById("E01MOTIVO").value;
		if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
			alert('Debe seleccionar un motivo');
		  	return false;
	}
	
	<% if (!error.getERRNUM().equals("0")){%>
		<% if (error.getERNU01().trim().equalsIgnoreCase("8263")){ %>
	elemento = document.getElementById("chekea");
	if( !elemento.checked ) {
		alert('Debe aceptar con advertencias');
	  	return false;
	}
	<% }} %>
	
	 var form = document.forms[0];
	 form.submit();
}
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">
            showErrors();
     </SCRIPT>
 <%
 }
%>
</body>
</html>