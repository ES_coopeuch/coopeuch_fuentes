<html>
<head>
<title>Sistema Bancario: Conciliación Bancaria</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">
 function enterCode(){
	
	if (trim(document.forms[0].E01BRMEID.value).length > 0) {
	    return true;
	}else{
		alert("Es requerido que se entre un valor");
		document.forms[0].E01BRMEID.focus();
		return false;
	}
 }
</script>

</head>


<jsp:useBean id= "rcbank" class= "datapro.eibs.beans.ERC004001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<body>

<H3 align="center">Gestión de Cartolas - Conciliación Manual
	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="rcbank_enter_selection, ERC0040"></H3>
<hr size="4">

<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0040" onsubmit="return(enterCode());">
	<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">
  	<h4>&nbsp;</h4>
  
<table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0"> 
    <tr id="trdark">
      <td width="50%"> 
        <div align="right">Banco : </div>
      </td>
      <td width="50%"> 
        <div align="left"> 
          <input type="text" name="E01BRMEID" size="5" maxlength="4" value="<%= rcbank.getE01BRMBNK().trim()%>">
            <a href="javascript:GetBankReconciliation('E01BRMEID','E01DSCRBK','E01BRMCTA','E01BRMACC')">
            <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0" ></a> 
          <input type="text" name="E01DSCRBK" readonly="readonly" size="43" maxlength="43" >
        </div>
      </td>
    </tr>

  	<tr id="trdark"> 
        <td align=CENTER width="50%"> 
          <div align="right">Cuenta Banco :</div>
        </td>
        <td align=CENTER width="50%"> 
          <div align="left"> 
 	         <input type="text" name="E01BRMCTA" size="23" maxlength="20" value="<%= rcbank.getE01BRMCTA().trim()%>">
          </div>
        </td>
      </tr>
      
      
  	<tr id="trdark"> 
        <td align=CENTER width="50%"> 
          <div align="right">Cuenta IBS :</div>
        </td>
        <td align=CENTER width="50%"> 
          <div align="left"> 
 	         <input type="text" name="E01BRMACC" size="7" maxlength="7" value="<%
 	         if(rcbank.getE01BRMACC().equals("0")){
 	         out.print("");
 	         }
 	         else{
 	         
 	         out.print(rcbank.getE01BRMACC());
 	         }
 	          %>">
          </div>
        </td>
      </tr>
     
        
  </table>
  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
<script language="JavaScript">
  document.forms[0].E01BRMEID.focus();
  document.forms[0].E01BRMEID.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>

</form>
</body>
</html>
