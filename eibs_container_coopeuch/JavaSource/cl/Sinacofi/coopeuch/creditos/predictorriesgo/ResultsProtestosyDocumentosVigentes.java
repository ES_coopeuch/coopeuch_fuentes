/**
 * ResultsProtestosyDocumentosVigentes.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResultsProtestosyDocumentosVigentes  implements java.io.Serializable {
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ProtestosyDocumentosVigentes[] protestosyDocumentosVigentes;

    public ResultsProtestosyDocumentosVigentes() {
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ProtestosyDocumentosVigentes[] getProtestosyDocumentosVigentes() {
        return protestosyDocumentosVigentes;
    }

    public void setProtestosyDocumentosVigentes(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ProtestosyDocumentosVigentes[] protestosyDocumentosVigentes) {
        this.protestosyDocumentosVigentes = protestosyDocumentosVigentes;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ProtestosyDocumentosVigentes getProtestosyDocumentosVigentes(int i) {
        return protestosyDocumentosVigentes[i];
    }

    public void setProtestosyDocumentosVigentes(int i, cl.Sinacofi.coopeuch.creditos.predictorriesgo.ProtestosyDocumentosVigentes value) {
        this.protestosyDocumentosVigentes[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsProtestosyDocumentosVigentes history = (ResultsProtestosyDocumentosVigentes) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ResultsProtestosyDocumentosVigentes other = (ResultsProtestosyDocumentosVigentes) obj;
        boolean _equals;
        _equals = true
            && ((this.protestosyDocumentosVigentes==null && other.getProtestosyDocumentosVigentes()==null) || 
             (this.protestosyDocumentosVigentes!=null &&
              java.util.Arrays.equals(this.protestosyDocumentosVigentes, other.getProtestosyDocumentosVigentes())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResultsProtestosyDocumentosVigentes history = (ResultsProtestosyDocumentosVigentes) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getProtestosyDocumentosVigentes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProtestosyDocumentosVigentes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProtestosyDocumentosVigentes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
