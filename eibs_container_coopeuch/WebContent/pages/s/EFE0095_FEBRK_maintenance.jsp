<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Mantenimiento Tablas de Compa�ias</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.EFE009501Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

 builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
	init(opth,field,bank,ccy,field1,field2,opcod);
	showPopupHelp();
}

function EraseId() {
	document.forms[0].E01FEBBID.value = '';
  }


 </script>
 
 
 
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	CONSULTA TABLAS DE COMPA�IAS
<%} else if (maintenance){ %>
	MANTENIMIENTO TABLAS DE COMPA�IAS
<%} else { %>
	NUEVO TABLA DE COMPA�IAS
<%} %>

 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="FEBRK_maintenance.jsp, EFE0095"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEFE0095" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01FEBATY" value="<%= cnvObj.getE01FEBATY().trim()%>">
  
 <% int row = 0;%>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap width="10%" align="right"> Tipo Registro : 
              </td>
             <td nowrap width="10%" align="left">
                <input type="text" readonly name="rectyp" size="2" maxlength="1" value="<%= userPO.getOption().trim()%>" readonly>
             </td>
             <td nowrap width="10%" align="right"> Descripcion : 
               </td>
             <td nowrap width="50%"align="left">
				<% 
			        if (userPO.getOption().equals("A")) out.print("SEGURO AUTOS");
			        else if (userPO.getOption().equals("C")) out.print("CREDIAHORROS");
			        else if (userPO.getOption().equals("T")) out.print("RECOLECTOR DE IMPUESTOS");
			        else if (userPO.getOption().equals("V")) out.print("AGENTES DE BOLSA");
			        else if (userPO.getOption().equals("G")) out.print("ADMINISTRACION DE FONDOS");
			        else if (userPO.getOption().equals("I")) out.print("COMPA�IAS DE SEGUROS");
			        else if (userPO.getOption().equals("E")) out.print("ENTIDADES DE GOBIERNO");
			        else if (userPO.getOption().equals("S")) out.print("VENDEDORES - COMISIONISTAS");
			        else if (userPO.getOption().equals("D")) out.print("AGENTES - REPRESENTANTES COMEX");
			        else if (userPO.getOption().equals("N")) out.print("NOTARIAS");
			        else if (userPO.getOption().equals("P")) out.print("PARTICIPANTES POOL CONTRATOS");
			        else if (userPO.getOption().equals("O")) out.print("INSPECTORES DE OBRA"); 
			        else if (userPO.getOption().equals("X")) out.print("TIENDA VIRTUAL"); 
			        else if (userPO.getOption().equals("Y")) out.print("TARJETAS ALIANZA");
				%>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Informacion Basica </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="20%" > 
              <div align="right">Codigo :</div>
            </td>
            <td width="30%" > 
				<% if ( !userPO.getPurpose().equals("NEW") ) { %>
                 <eibsinput:text name="cnvObj" property="E01FEBNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" readonly="true"/>
				<% } else { %>
                 <eibsinput:text name="cnvObj" property="E01FEBNUM"  eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER%>" readonly="false"/>
				<% } %>
	        </td>
            <td width="20%" > 
            <%if (!userPO.getOption().equals("I")){ %>
              <div align="right">Cliente :</div>
            <%} %>
          
            </td>
            <td width="30%" > 
			  <%if (!userPO.getOption().equals("I")){ %>
          
				<eibsinput:help name="cnvObj" property="E01FEBCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>"  
					fn_param_one="E01FEBCUN" fn_param_two="E01FEBNM1" fn_param_three="E01FEBBID"   readonly="<%=readOnly %>"/>
	          <%} %>
          
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
       
       <%if (userPO.getOption().equals("I")){ %>
            <td colspan="1" >
             <div align="right">Compania :</div>
       		</td>
            
            <td >
              <input type="text" name="E01FEBCIA" size="5" maxlength="4" value="<%= cnvObj.getE01FEBCIA().trim()%>" readonly>
              <input type="text" name="E01FEBNM1" size="40" maxlength="35" value="<%= cnvObj.getE01FEBNM1().trim()%>" readonly>
            <a href="javascript:GetCodeDescCNOFC('E01FEBCIA','E01FEBNM1','OS');EraseId()"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0"></a> 
            <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="absbottom" border="0" >  
       		</td>
            <td width="20%" > 
              <div align="right">Identificaci�n :</div>
            </td>
            <td width="30%" > 
	             <eibsinput:text property="E01FEBBID" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>" readonly="TRUE"/>
	        </td>    
       
       <%} else { %>
       
            <td width="20%" > 
              <div align="right">Nombre :</div>
            </td>
            <td width="30%" > 
                 <eibsinput:text property="E01FEBNM1" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly %>"/>
	        </td>
            <td width="20%" > 
              <div align="right">Identificaci�n :</div>
            </td>
            <td width="30%" > 
	             <eibsinput:text property="E01FEBBID" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>" readonly="<%=readOnly %>"/>
	        </td>
       <%} %>
       
       
          </tr>
  
  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
  
      <%if (userPO.getOption().equals("I")){ %>
  
            <td width="20%" > 
              <div align="right">Direcci�n :</div>
            </td>
            <td width="30%" colspan="3"> 
                 <eibsinput:text property="E01FEBNM2" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly %>"/>
	        </td>
  
       <%} else { %>
       
            <td width="20%" > 
              <div align="right">Direcci�n :</div>
            </td>
            <td width="30%" > 
                 <eibsinput:text property="E01FEBNM2" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly %>"/>
	        </td>
            <td width="20%" > 
              <div align="right"> Nombre Corto :</div>
            </td>
            <td width="30%" > 
	             <eibsinput:text property="E01FEBSNM" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_SHORT%>" readonly="<%=readOnly %>"/>
	        </td>
  
     <%} %>
    
  
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
 
            <td width="20%" > 
              <div align="right"> </div>
            </td>
            <td width="30%" > 
                 <eibsinput:text property="E01FEBNM3" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly %>"/>
	        </td>
            <td width="20%" > 
              <div align="right">Estado :</div>
            </td>
            <td width="30%" > 
                <eibsinput:cnofc name="cnvObj" property="E01FEBSTE" required="false" flag="27" fn_code="E01FEBSTE" fn_description="" readonly="<%=readOnly %>"/>
	        </td>
          </tr>
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="20%" > 
              <div align="right">Ciudad :</div>
            </td>
            <td width="30%" > 
                 <eibsinput:text property="E01FEBCTY" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly %>"/>
	        </td>
            <td width="20%" > 
              <div align="right">Area :</div>
            </td>
            <td width="30%" > 
                 <eibsinput:text property="E01FEBZIP" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ZIPCODE%>" readonly="<%=readOnly %>"/>
 	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="20%" > 
              <div align="right">Nombre Contacto :</div>
            </td>
            <td width="30%" > 
                 <eibsinput:text property="E01FEBNMC" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly %>"/>
	        </td>
           <td width="20%" > 
              <div align="right">Telefono :</div>
            </td>
            <td width="30%" > 
	             <eibsinput:text property="E01FEBPHC" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE%>" readonly="<%=readOnly %>"/>
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="20%" > 
              <div align="right">Email :</div>
            </td>
            <td width="30%" > 
                 <eibsinput:text property="E01FEBIA1" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_EMAIL%>" readonly="<%=readOnly %>"/>
	        </td>
            <td width="20%" > 
              <div align="right">Fax :</div>
            </td>
            <td width="30%" > 
	             <eibsinput:text property="E01FEBFAC" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE%>" readonly="<%=readOnly %>"/>
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="20%" > 
              <div align="right">Sitio Web :</div>
            </td>
            <td width="30%" > 
                 <eibsinput:text property="E01FEBIA2" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_EMAIL%>" readonly="<%=readOnly %>"/>
	        </td>
            <td width="20%" > 
              <div align="right">Formulario :</div>
            </td>
            <td width="30%" > 
              	<% if (!readOnly) { %>          
              	<input type="text" name="E01FEBZPC" size="20" maxlength="15" value="<%= cnvObj.getE01FEBZPC().trim()%>" >  
               		<a href="javascript:GetFormsCode('E01FEBZPC')">
               			<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0" ></a>
				<% } else {%>
              	<input type="text" name="E01FEBZPC" size="20" maxlength="15" value="<%= cnvObj.getE01FEBZPC().trim()%>" readonly >  
               	<% } %>		
	        </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>

  
  <h4>Producto  </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="20%" > 
              <div align="right"> Descripcion : </div>
            </td>
            <td width="30%" > 
                 <eibsinput:text property="E01FEBNM4" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly %>"/>
	        </td>
            <td width="20%" > 
              <div align="right"> Poliza :</div>
            </td>
            <td width="30%" > 
                 <eibsinput:text property="E01FEBPLZ" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CARD_NUMBER%>" readonly="<%=readOnly %>"/>
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap >
             <div align="right">Valor Deduccion :</div>
            </td>
            <td nowrap >
                 <eibsinput:text property="E01FEBVAL" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT_FEE%>" readonly="<%=readOnly %>"/>
            </td>
            <td nowrap > 
              <div align="right">Tipo Deduccion :</div>
            </td>
            <td nowrap >
              <select name="E01FEBFCT" <%=readOnly?"disabled":""%>>
                <option value="F" <% if (cnvObj.getE01FEBFCT().equals("F")) out.print("selected"); %>>Valor Fijo</option>
                <option value="1" <% if (cnvObj.getE01FEBFCT().equals("1")) out.print("selected"); %>>% Sobre monto Original</option>
                <option value="2" <% if (cnvObj.getE01FEBFCT().equals("2")) out.print("selected"); %>>% Sobre saldo Principal</option>
                <option value="3" <% if (cnvObj.getE01FEBFCT().equals("3")) out.print("selected"); %>>% Sobre valor Cuota</option>
              </select>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap > 
              <div align="right">Cobro de IVA :</div>
            </td>
            <td nowrap > 
              <select name="E01FEBWTH" <%=readOnly?"disabled":""%>>
                <option value="W" <% if (!(cnvObj.getE01FEBWTH().equals("F") || cnvObj.getE01FEBWTH().equals("B") || cnvObj.getE01FEBWTH().equals("N"))) out.print("selected"); %>>Retencion</option>
                <option value="F" <% if (cnvObj.getE01FEBWTH().equals("F")) out.print("selected"); %>>Forma 1099</option>
                <option value="B" <% if (cnvObj.getE01FEBWTH().equals("B")) out.print("selected"); %>>Ambos</option>
                <option value="N" <% if (cnvObj.getE01FEBWTH().equals("N")) out.print("selected"); %>>Ninguno</option>
              </select>
            </td>
            <td nowrap > 
              <div align="right">Tasa Activa:</div>
            </td>
            <td nowrap > 
               <eibsinput:text name="cnvObj" property="E01FEBRTA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="<%=readOnly %>"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap > 
              <div align="right">Via de Pago :</div>
            </td>
            <td nowrap > 
              <select name="E01FEBPVI" <%=readOnly?"disabled":""%>>
                <option value="A" <% if (cnvObj.getE01FEBPVI().equals("A")) out.print("selected"); %>>Cuenta Detalle</option>
                <option value="G" <% if (cnvObj.getE01FEBPVI().equals("G")) out.print("selected"); %>>Cuenta Contable</option>
                <option value="F" <% if (cnvObj.getE01FEBPVI().equals("F")) out.print("selected"); %>>Transfer FED</option>
                <option value="T" <% if (cnvObj.getE01FEBPVI().equals("T")) out.print("selected"); %>>Transfer Telex</option>
                <option value="1" <% if (cnvObj.getE01FEBPVI().equals("1")) out.print("selected"); %>>Swift MT-100</option>
                <option value="2" <% if (cnvObj.getE01FEBPVI().equals("2")) out.print("selected"); %>>Swift MT-200</option>
                <option value="" <% if (cnvObj.getE01FEBPVI().equals("")) out.print("selected"); %>>Ninguno</option>
              </select>
            </td>
            <td nowrap > 
              <div align="right">Tasa Pasiva :</div>
            </td>
            <td nowrap > 
               <eibsinput:text name="cnvObj" property="E01FEBRTL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="<%=readOnly %>"/>
	        </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
           <td nowrap >
             <div align="right">Canal Venta :</div>
            </td>
            <td nowrap >
                <eibsinput:cnofc name="cnvObj" property="E01FEBSCH" required="false" flag="62" fn_code="E01FEBSCH" fn_description="" readonly="<%=readOnly %>"/>
           </td>
          <% if (userPO.getOption().equals("I") ) { %>
 			<td nowrap > 
              <div align="right">Tabla Tasas de Seguro:</div>
            </td>
			<td nowrap> 
              <input type="text" name="E01FEBUC4" size="6" maxlength="4" value="<%= cnvObj.getE01FEBUC4().trim()%>">            
               <a href="javascript:GetRateTable('E01FEBUC4')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0" ></a>
            </td>
			<%} else {%>
            <td nowrap >
             <div align="right"></div>
            </td>
            <td nowrap >
            </td>
            <% } %>
           </tr>

          <% if (userPO.getOption().equals("I") ) { %>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap > 
              <div align="right">Tipo de Seguro :</div>
            </td>
            <td nowrap > 
              <select name="E01FEBITY" <%=readOnly?"disabled":""%>>
                <option value="1" <% if (cnvObj.getE01FEBITY().equals("1")) out.print("selected"); %>>Desgravamen</option>
                <option value="2" <% if (cnvObj.getE01FEBITY().equals("2")) out.print("selected"); %>>Cesantias</option>
                <option value="3" <% if (cnvObj.getE01FEBITY().equals("3")) out.print("selected"); %>>Autos</option>
                <option value="4" <% if (cnvObj.getE01FEBITY().equals("4")) out.print("selected"); %>>Educativo</option>
                <option value="5" <% if (cnvObj.getE01FEBITY().equals("5")) out.print("selected"); %>>Invalidez Total</option>
                <option value="6" <% if (cnvObj.getE01FEBITY().equals("6")) out.print("selected"); %>>Enfermedades Graves</option>
                <option value="7" <% if (cnvObj.getE01FEBITY().equals("7")) out.print("selected"); %>>Hospitalizacion</option>
                <option value="8" <% if (cnvObj.getE01FEBITY().equals("8")) out.print("selected"); %>>Vida/Invalidez 2/3</option>
                <option value="9" <% if (cnvObj.getE01FEBITY().equals("9")) out.print("selected"); %>>Proteccion Documentos</option>
                <option value="A" <% if (cnvObj.getE01FEBITY().equals("A")) out.print("selected"); %>>Fraude</option>
                <option value="O" <% if (cnvObj.getE01FEBITY().equals("O")) out.print("selected"); %>>Otros</option>
              </select>
            </td>
            <td nowrap > 
              <div align="right">Seguro Obligatorio :</div>
            </td>
            <td nowrap > 
              <input type="radio" name="E01FEBOTI" value="Y" <%if(!cnvObj.getE01FEBOTI().equals("N")) out.print("checked");%>>S&iacute; 
              <input type="radio" name="E01FEBOTI" value="N" <%if(cnvObj.getE01FEBOTI().equals("N")) out.print("checked");%>>No 
            </td>
          </tr>

	    <%if( currUser.getE01INT().equals("18")){%>     
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap >
             <div align="right">% Banco :</div>
            </td>
            <td nowrap >
               <eibsinput:text name="cnvObj" property="E01FEBMAR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" size="8" maxlength="6" readonly="false"/>
            </td>
            <td nowrap >
             <div align="right">% Corredora :</div>
            </td>
            <td nowrap >
               <eibsinput:text name="cnvObj" property="E01FEBCGM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" size="8" maxlength="6" readonly="false"/>
            </td>
            </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap >
             <div align="right">% Compania :</div>
            </td>
            <td nowrap >
               <eibsinput:text name="cnvObj" property="E01FEBCPY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" size="8" maxlength="6" readonly="false"/>
            </td>
            <td nowrap >
             <div align="right">% Socio :</div>
            </td>
            <td nowrap >
               <eibsinput:text name="cnvObj" property="E01FEBCPM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" size="8" maxlength="6" readonly="false"/>
            </td>           
          </tr>         
          <% } %>

         <% } %>
 
        </table>
      </td>
    </tr>
  </table>
 
  <h4>Contabilidad </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
   	<table cellspacing=2 cellpadding=2 width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap></td>
            <td nowrap align="CENTER"><b>Banco</b></td>
            <td nowrap align="CENTER"><b>Suc.</b></td>
            <td nowrap align="CENTER"><b>Mda.</b></td>
            <td nowrap align="CENTER"><b>C.Contable</b></td>
            <td nowrap align="CENTER"><b>Referencia</b></td>
            <td nowrap align="CENTER"><b>C.Costo</b></td>
         </tr>     	        	    

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap> 
              <div align="right">Cuenta Acreditar :</div>
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBCBK" size="2" maxlength="2" value="<%= cnvObj.getE01FEBCBK().trim()%>">             
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBCBR" size="4" maxlength="4" value="<%= cnvObj.getE01FEBCBR().trim()%>" oncontextmenu="showPopUp(branchHelp,this.name,document.forms[0].E01FEBCBK.value,'','','','')">             
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBCCY" size="3" maxlength="3" value="<%= cnvObj.getE01FEBCCY().trim()%>" oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E01FEBCBK.value,'','','','')">              
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBCGL" size="17" maxlength="16"  value="<%= cnvObj.getE01FEBCGL().trim()%>" oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01FEBCBK.value,document.forms[0].E01FEBCCY.value,'','','')">        
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBCAC" size="13" maxlength="12"  value="<%= cnvObj.getE01FEBCAC().trim()%>" oncontextmenu="showPopUp(accountHelp,this.name,document.forms[0].E01FEBCBK.value,'','','','RT')">        
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBCCN" size="10" maxlength="8"  value="<%= cnvObj.getE01FEBCCN().trim()%>" oncontextmenu="showPopUp(costcenterHelp,this.name,document.forms[0].E01FEBCBK.value,'','','','')">        
            </td>
         </tr>  

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap > 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBRBK" size="2" maxlength="2" value="<%= cnvObj.getE01FEBRBK().trim()%>">             
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBRBR" size="4" maxlength="4" value="<%= cnvObj.getE01FEBRBR().trim()%>" oncontextmenu="showPopUp(branchHelp,this.name,document.forms[0].E01FEBRBK.value,'','','','')">             
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBRCY" size="3" maxlength="3" value="<%= cnvObj.getE01FEBRCY().trim()%>" oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E01FEBRBK.value,'','','','')">              
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBRGL" size="17" maxlength="16"  value="<%= cnvObj.getE01FEBRGL().trim()%>" oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01FEBRBK.value,document.forms[0].E01FEBRCY.value,'','','')">        
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBRAC" size="13" maxlength="12"  value="<%= cnvObj.getE01FEBRAC().trim()%>" oncontextmenu="showPopUp(accountHelp,this.name,document.forms[0].E01FEBRBK.value,'','','','RT')">        
            </td>
            <td nowrap></td>
         </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap > 
              <div align="right">Cuenta por Cobrar :</div>
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBXBK" size="2" maxlength="2" value="<%= cnvObj.getE01FEBXBK().trim()%>">             
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBXBR" size="4" maxlength="4" value="<%= cnvObj.getE01FEBXBR().trim()%>" oncontextmenu="showPopUp(branchHelp,this.name,document.forms[0].E01FEBXBK.value,'','','','')">             
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBXCY" size="3" maxlength="3" value="<%= cnvObj.getE01FEBXCY().trim()%>" oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E01FEBXBK.value,'','','','')">              
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBXGL" size="17" maxlength="16"  value="<%= cnvObj.getE01FEBXGL().trim()%>" oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01FEBXBK.value,document.forms[0].E01FEBXCY.value,'','','')">        
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBXAC" size="13" maxlength="12"  value="<%= cnvObj.getE01FEBXAC().trim()%>" oncontextmenu="showPopUp(accountHelp,this.name,document.forms[0].E01FEBXBK.value,'','','','RT')">        
            </td>
            <td nowrap></td>
         </tr>

		<% if( currUser.getE01INT().equals("18")){%> 	
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap > 
              <div align="right">Comision Fogape :</div>
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBGBK" size="2" maxlength="2" value="<%= cnvObj.getE01FEBGBK().trim()%>">             
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBGBR" size="4" maxlength="4" value="<%= cnvObj.getE01FEBGBR().trim()%>" oncontextmenu="showPopUp(branchHelp,this.name,document.forms[0].E01FEBGBK.value,'','','','')">             
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBGCY" size="3" maxlength="3" value="<%= cnvObj.getE01FEBGCY().trim()%>" oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E01FEBGBK.value,'','','','')">              
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBGGL" size="17" maxlength="16"  value="<%= cnvObj.getE01FEBGGL().trim()%>" oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01FEBGBK.value,document.forms[0].E01FEBGCY.value,'','','')">        
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBGAC" size="13" maxlength="12"  value="<%= cnvObj.getE01FEBGAC().trim()%>" oncontextmenu="showPopUp(accountHelp,this.name,document.forms[0].E01FEBGBK.value,'','','','RT')">        
            </td>
            <td nowrap></td>
         </tr>   
		<%} %>         
		
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap > 
              <div align="right">Impuesto Sobre Deduccion :</div>
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBBN1" size="2" maxlength="2" value="<%= cnvObj.getE01FEBBN1().trim()%>">             
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBBR1" size="4" maxlength="4" value="<%= cnvObj.getE01FEBBR1().trim()%>" oncontextmenu="showPopUp(branchHelp,this.name,document.forms[0].E01FEBBN1.value,'','','','')">             
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBCC1" size="3" maxlength="3" value="<%= cnvObj.getE01FEBCC1().trim()%>" oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E01FEBBN1.value,'','','','')">              
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBGL1" size="17" maxlength="16"  value="<%= cnvObj.getE01FEBGL1().trim()%>" oncontextmenu="showPopUp(ledgerHelp,this.name,document.forms[0].E01FEBBN1.value,document.forms[0].E01FEBCC1.value,'','','')">        
            </td>
            <td nowrap>                
                <input type="text" name="E01FEBAC1" size="13" maxlength="12"  value="<%= cnvObj.getE01FEBAC1().trim()%>" oncontextmenu="showPopUp(accountHelp,this.name,document.forms[0].E01FEBBN1.value,'','','','RT')">        
            </td>
            <td nowrap><INPUT type="text" name="E01FEBUC1" size="8"
					maxlength="6" value="<%= cnvObj.getE01FEBUC1().trim()%>"
					oncontextmenu="showPopUp(accountHelp,this.name,document.forms[0].E01FEBBN1.value,'','','','RT')">%</td>
         </tr>                 
 
        </table>
      </td>
    </tr>
  </table>

<%if (readOnly){ %>
 
  <h4>Saldos </h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
   	<table cellspacing=2 cellpadding=2 width="100%" border="0">

         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap > 
              <div align="right">Monto Pago Anual :</div>
            </td>
            <td nowrap > 
               <eibsinput:text name="cnvObj" property="E01FEBPYT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
            </td>
            <td nowrap > 
              <div align="right">Retenido Anual :</div>
            </td>
            <td nowrap colspan=2> 
               <eibsinput:text name="cnvObj" property="E01FEBWYT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="false"/>
            </td>
         </tr>                 
 
        </table>
      </td>
    </tr>
  </table>
 
<% } %>  

<%if  (!readOnly) { %>
    <div align="center"> 
        <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    </div>
<% } %>  

  </form>
</body>
</HTML>
