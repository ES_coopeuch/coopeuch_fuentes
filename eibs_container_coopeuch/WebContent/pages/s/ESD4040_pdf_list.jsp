<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<HTML>
<HEAD>
<TITLE>Lista de Formularios</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR"
	content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<%@ page import="datapro.eibs.master.JSEIBSProp" %>

<link href="<%=request.getContextPath()%>/pages/style.css"	rel="stylesheet">

<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"
	scope="session" />

<jsp:useBean id="pdfList" class="datapro.eibs.beans.JBObjList"
	scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="javascript">

	function goPrint() {
		var cbs = document.forms[0].elements['ROWS'];
		for (var i=0, cbLen=cbs.length; i<cbLen; i++) {
			if (cbs[i].checked) {
				goAction(i, document.getElementById('N' + cbs[i].value).value,	document.getElementById('C' + cbs[i].value).value, document.getElementById('P' + cbs[i].value).value);
  			} 
		}
		if (<%=!"EVALUATION".equals(userPO.getPurpose())%>) {
			document.getElementById('EIBSBTN').disabled = true;
			document.forms[0].SCREEN.value = '100';
			document.forms[0].submit();
		}	
	}
  
  	function goAction(seq, pdf, copies, printer) {
    	var page = '<%=request.getContextPath()%>/servlet/datapro.eibs.reports.JSEFRM000FDF?SCREEN=3&action=2&copies=' + copies + '&pdfName=' + pdf + '&printerName=' + printer;
    	var name = 'pdf_1' + seq;
    	
		CenterNamedWindow(page,name,600,500,7);
		
  	}
 

  	function goView(pdf) {
    	var page = '<%=request.getContextPath()%>/servlet/datapro.eibs.reports.JSEFRM000FDF?SCREEN=3&action=1&copies=1&pdfName=' + pdf;
		CenterNamedWindow(page,'pdf',600,500,7);
  	}
  	
	function showPDF_NEW(url) {
		CenterNamedWindow(url,'pdf',600,500,7);
  	}
  	
  
</script>

</HEAD>

<BODY>

<%
	String disabled = "EVALUATION".equals(userPO.getPurpose()) ? "" : "disabled"; 
%>

<h3 align="center">Formularios<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="pdf_list.jsp, EPV1038"></h3>
<hr size="4">
<FORM Method="post"
	Action="<%=userPO.getRedirect()%>">

<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="">
<INPUT TYPE=HIDDEN NAME="pdfName" VALUE=""> 
<INPUT TYPE=HIDDEN NAME="copies" VALUE=""> 

<INPUT TYPE=HIDDEN NAME="E01CUN" VALUE="<%=userPO.getCusNum()%>">
<INPUT TYPE=HIDDEN NAME="E01IDN" VALUE="<%=userPO.getCusType()%>">
<INPUT TYPE=HIDDEN NAME="E01NME" VALUE="<%=userPO.getCusName()%>">

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Numero: 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">RUT:  
             </td>
             <td nowrap width="10%" align="left">            
	  			<eibsinput:text name="userPO" property="cusType" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>  			
             </td>
             <td nowrap width="10%" align="right"> Nombre: 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="userPO" property="cusName" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table> 
  <br>
  <br>
  
<% if (pdfList.getNoResult()) { %>
<TABLE class="tbenter" width=100% height=50%>
	<TR>
		<TD>
			<div align="center"><font size="3"><b> No hay resultado que corresponda a su criterio de b�squeda. </b></font></div>
		</TD>
	</TR>
</TABLE>
 <div align="center"> 
     <input id="EIBSBTN" type="submit" name="Cancel" value="Cancelar">    
 </div>
<% } else {	%>
<TABLE class="tableinfo" ALIGN=CENTER>
	<COL WIDTH=10%><COL WIDTH=50%><COL WIDTH=30%>
	<tr id="trclear"> 
  		<th align=CENTER nowrap >&nbsp;</th>
  		<th align=CENTER nowrap ><div align="left">Nombre del Formulario</div></th>
  		<th align=CENTER nowrap ><div align="center">N�mero de Copias</div></th>
 	</tr>
	<%
          		datapro.eibs.beans.EFRM00001Message msg = null;
				boolean firstTime = true;
				String chk = "";
				int rut = Integer.parseInt(userPO.getCusType().substring(0,userPO.getCusType().length()-1));
                pdfList.initRow();
                while (pdfList.getNextRow()) {
                 	msg = (datapro.eibs.beans.EFRM00001Message) pdfList.getRecord();
                 	if(rut>50000000){
                 		if(msg.getE01APFPTH().equals("PAGARE_FOGAPE_PN.PDF") && (msg.getE01APFUC2().equals("FGP"))){
                 			continue;
                 		}
                 	}else{
                 		if(msg.getE01APFPTH().equals("PAGARE_FOGAPE_JU.PDF") && (msg.getE01APFUC2().equals("FGP"))){
                 			continue;
                 		}
                 	}                 	
	%>
	<tr id="trdark">
		<td NOWRAP align=CENTER width="10%"><input type="checkbox"
			name="ROWS" value="<%= pdfList.getCurrentRow() %>" checked="checked" <%=disabled%>>
			<input type=HIDDEN id="N<%= pdfList.getCurrentRow() %>" name="NAME<%= pdfList.getCurrentRow() %>" value="<%= msg.getE01APFPTH() %>">
			<input type=HIDDEN id="C<%= pdfList.getCurrentRow() %>" name="COPIES<%= pdfList.getCurrentRow() %>" value="<%= msg.getE01APFCPI() %>">
			<input type=HIDDEN id="P<%= pdfList.getCurrentRow() %>" name="PRINTER<%= pdfList.getCurrentRow() %>" value="<%= msg.getE01APFDDS() %>">
		</td>

           <%if( msg.getE01APFPTH().equals("PAGARE_MIPE_PJ.pdf")){ 
 	          		String urlPDF = JSEIBSProp.getFORMPDFURLNEW() + "PdfServlet?numCredito=" + session.getAttribute("StrOpe") + "&tipoDoc=PAGARESMIPEPJ&idConsulta=" + session.getAttribute("StrId");
             %>
	            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:showPDF_NEW('<%= urlPDF %>')"><%= msg.getE01APFDSC() %></A></td>
            <%} else {
            	if( msg.getE01APFPTH().equals("PAGARE_MIPE_PN.pdf")){  
  	          		String urlPDF = JSEIBSProp.getFORMPDFURLNEW() + "PdfServlet?numCredito=" + session.getAttribute("StrOpe") + "&tipoDoc=PAGARESMIPEPN&idConsulta=" + session.getAttribute("StrId");
  			%>          
    	            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:showPDF_NEW('<%= urlPDF %>')"><%= msg.getE01APFDSC() %></A></td>
            
            <%	} else if( msg.getE01APFPTH().equals("PAGARE_FOGAPE_PN.PDF")){            	 
               		String urlPDF = JSEIBSProp.getFORMPDFURLNEW() + "PdfReporte?numSolicitud=" + session.getAttribute("StrNum") + "&tipoDoc=PAGAREFOGPN";               		
  			%>          
    	            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:showPDF_NEW('<%= urlPDF %>')"><%= msg.getE01APFDSC() %></A></td>
    	            
    	    <%	} else if( msg.getE01APFPTH().equals("PAGARE_FOGAPE_JU.PDF")){
                 		String urlPDF = JSEIBSProp.getFORMPDFURLNEW() + "PdfReporte?numSolicitud=" + session.getAttribute("StrNum") + "&tipoDoc=PAGAREFOGJU";
  			%>          
    	            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:showPDF_NEW('<%= urlPDF %>')"><%= msg.getE01APFDSC() %></A></td>

    	    <%	} else if( msg.getE01APFPTH().equals("RESUSCRIPCION_BKOF.PDF")){
                 		String urlPDF = JSEIBSProp.getFORMPDFURLNEW() + "PdfReporte?numCredito=" + session.getAttribute("StrNum") + "&tipoDoc=RESPAGCREDCONS";
  			%>          
    	            <td NOWRAP align=LEFT width="80%"><A HREF="javascript:showPDF_NEW('<%= urlPDF %>')"><%= msg.getE01APFDSC() %></A></td>
   
			<%	} else if(msg.getE01APFPTH().toUpperCase().equals("PAGARE_CRED_CONSUMO.PDF")){ 
  			          		String urlPDF = JSEIBSProp.getFORMPDFURLNEW() + "PdfReporte?numSolicitud=" + session.getAttribute("StrOpe") + "&tipoDoc=PAGCREDCONS";
  			  	%>	
            		 <td NOWRAP align=LEFT width="80%"><A HREF="javascript:showPDF_NEW('<%= urlPDF %>')"><%= msg.getE01APFDSC() %></A></td>    	            
       	                    
    	                    
    	    <%} else { %>
				<td NOWRAP align=LEFT width="50%"><A HREF="javascript:goView('<%= msg.getE01APFPTH() %>')"><%= msg.getE01APFDSC() %></A></td>

			<% }
 			}%>

		<td NOWRAP align=CENTER width="30%"><%= msg.getE01APFCPI() %></td>
	</tr>
	<% } %>
</TABLE>

 <div align="center"> 
     &nbsp;&nbsp;&nbsp;&nbsp;    
     <input id="EIBSBTN" type="submit" name="Cancel" value="Cancelar">    
 </div>


<% } %>
</FORM>

</BODY>
</HTML>
