/**
 * DirectiorioPersonas.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class DirectiorioPersonas  implements java.io.Serializable {
    private java.math.BigInteger id;  // attribute
    private java.lang.String nombre;
    private java.lang.String direccion;
    private java.lang.String comuna;
    private java.lang.String ciudad;
    private java.lang.String region;
    private java.math.BigInteger IFechaNacimiento;
    private java.lang.String actividad;
    private java.math.BigInteger IFechaInformacion;
    private java.lang.String situacionMilitar;
    private java.math.BigInteger IFechaDefuncion;

    public DirectiorioPersonas() {
    }

    public java.math.BigInteger getId() {
        return id;
    }

    public void setId(java.math.BigInteger id) {
        this.id = id;
    }

    public java.lang.String getNombre() {
        return nombre;
    }

    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }

    public java.lang.String getDireccion() {
        return direccion;
    }

    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }

    public java.lang.String getComuna() {
        return comuna;
    }

    public void setComuna(java.lang.String comuna) {
        this.comuna = comuna;
    }

    public java.lang.String getCiudad() {
        return ciudad;
    }

    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }

    public java.lang.String getRegion() {
        return region;
    }

    public void setRegion(java.lang.String region) {
        this.region = region;
    }

    public java.math.BigInteger getIFechaNacimiento() {
        return IFechaNacimiento;
    }

    public void setIFechaNacimiento(java.math.BigInteger IFechaNacimiento) {
        this.IFechaNacimiento = IFechaNacimiento;
    }

    public java.lang.String getActividad() {
        return actividad;
    }

    public void setActividad(java.lang.String actividad) {
        this.actividad = actividad;
    }

    public java.math.BigInteger getIFechaInformacion() {
        return IFechaInformacion;
    }

    public void setIFechaInformacion(java.math.BigInteger IFechaInformacion) {
        this.IFechaInformacion = IFechaInformacion;
    }

    public java.lang.String getSituacionMilitar() {
        return situacionMilitar;
    }

    public void setSituacionMilitar(java.lang.String situacionMilitar) {
        this.situacionMilitar = situacionMilitar;
    }

    public java.math.BigInteger getIFechaDefuncion() {
        return IFechaDefuncion;
    }

    public void setIFechaDefuncion(java.math.BigInteger IFechaDefuncion) {
        this.IFechaDefuncion = IFechaDefuncion;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        DirectiorioPersonas other = (DirectiorioPersonas) obj;
        boolean _equals;
        _equals = true
            && ((this.nombre==null && other.getNombre()==null) || 
             (this.nombre!=null &&
              this.nombre.equals(other.getNombre())))
            && ((this.direccion==null && other.getDireccion()==null) || 
             (this.direccion!=null &&
              this.direccion.equals(other.getDireccion())))
            && ((this.comuna==null && other.getComuna()==null) || 
             (this.comuna!=null &&
              this.comuna.equals(other.getComuna())))
            && ((this.ciudad==null && other.getCiudad()==null) || 
             (this.ciudad!=null &&
              this.ciudad.equals(other.getCiudad())))
            && ((this.region==null && other.getRegion()==null) || 
             (this.region!=null &&
              this.region.equals(other.getRegion())))
            && ((this.actividad==null && other.getActividad()==null) || 
             (this.actividad!=null &&
              this.actividad.equals(other.getActividad())))
            && ((this.situacionMilitar==null && other.getSituacionMilitar()==null) || 
             (this.situacionMilitar!=null &&
              this.situacionMilitar.equals(other.getSituacionMilitar())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        DirectiorioPersonas history = (DirectiorioPersonas) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId())))
            && ((this.IFechaNacimiento==null && other.getIFechaNacimiento()==null) || 
             (this.IFechaNacimiento!=null &&
              this.IFechaNacimiento.equals(other.getIFechaNacimiento())))
            && ((this.IFechaInformacion==null && other.getIFechaInformacion()==null) || 
             (this.IFechaInformacion!=null &&
              this.IFechaInformacion.equals(other.getIFechaInformacion())))
            && ((this.IFechaDefuncion==null && other.getIFechaDefuncion()==null) || 
             (this.IFechaDefuncion!=null &&
              this.IFechaDefuncion.equals(other.getIFechaDefuncion())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        DirectiorioPersonas history = (DirectiorioPersonas) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getNombre() != null) {
            _hashCode += getNombre().hashCode();
        }
        if (getDireccion() != null) {
            _hashCode += getDireccion().hashCode();
        }
        if (getComuna() != null) {
            _hashCode += getComuna().hashCode();
        }
        if (getCiudad() != null) {
            _hashCode += getCiudad().hashCode();
        }
        if (getRegion() != null) {
            _hashCode += getRegion().hashCode();
        }
        if (getIFechaNacimiento() != null) {
            _hashCode += getIFechaNacimiento().hashCode();
        }
        if (getActividad() != null) {
            _hashCode += getActividad().hashCode();
        }
        if (getIFechaInformacion() != null) {
            _hashCode += getIFechaInformacion().hashCode();
        }
        if (getSituacionMilitar() != null) {
            _hashCode += getSituacionMilitar().hashCode();
        }
        if (getIFechaDefuncion() != null) {
            _hashCode += getIFechaDefuncion().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
