package datapro.eibs.products;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERC001001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;


import datapro.eibs.beans.ERM031001Message;
/**
 * Servlet implementation class JSERM0310
 * made by Alonso Arana ---------------------------Datapro-------------------------------15/05/2014-------------------------------
 * 
 */
public class JSERM0310 extends JSEIBSServlet {
	private static final long serialVersionUID = 1L;

	//Parámetros de control de acreencias
	
	
	protected static final int A_enter_acreencia=100;
	protected static final int reps=200;
	protected static final int AcrePRocess=300;
	protected static final int  update_Acre=400;
	protected static final int ret_acreencia=2000;
	protected static final int new_acreencia=800;
	protected static final int update_acreencia=700;
	protected static final int delete_acreencia=500;
	
	
	//Parámetros de autómata de acreencias
	
	protected static final int A_enter_automata=1000;
	protected static final int list_automata=15000;
	protected static final int enter_product=20000;
	protected static final int maint_product=30000;
	
	
	protected static final int new_product=650;
	protected static final int update_product=750;
	protected static final int delete_product=40000;
	protected static final int  transaction_list=666;
	
	
	
	
			protected void processRequest(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res,
					HttpSession session, int screen) throws ServletException,
					IOException {
			
				switch (screen) {
				case A_enter_acreencia:
					procReqAcreencia(user, req, res, session);
					break;
					
				case reps:
					
					AcreenciaProcess(user, req, res, session);
					break;
				case ret_acreencia:
					
					AcreenciaProcess(user, req, res, session);
					break;
			
					
				case AcrePRocess:
					
					proqNewAcre(user, req, res, session, "NEW");
					break;
	
				case new_acreencia:
					
					NewAcreencia(user, req, res, session);
					break;
					
	
					
				case update_Acre:
					
					proqNewAcre(user, req, res, session, "MAINTENANCE");
					break;
					
				case update_acreencia:
					
					UpdateAcreencia(user, req, res, session);
					
					break;
					
				case delete_acreencia:
					
					DeleteAcreencia(user, req, res, session);
					break;
	
				case A_enter_automata:
					procReqAutomata(user, req, res, session);
					break;
		
				case list_automata:
					ProductList(user, req, res, session);
					break;
				case enter_product:
					proqNewProductAcre(user, req, res, session, "NEW");
					break;
		
				case new_product:
					NewProductAcreencia(user, req, res, session);
					break;
					
				case transaction_list:
					
				ProductList(user, req, res, session);
				break;
				
				case maint_product:
					
					proqNewProductAcre(user, req, res, session, "MAINTENANCE");
					break;
			
				case update_product:
					
					UpdateProductAcreencia(user, req, res, session);
					break;
					
					
				case delete_product:
					
					DeletePrdAcreencia(user, req, res, session);
					break;
					
					
					
				default:
					forward(SuperServlet.devPage, req, res);
					break;
				}
				
				
			}

			

			protected void procReqAcreencia(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession ses)
					throws ServletException, IOException {
				UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
				ses.setAttribute("userPO", userPO);
				try {
					flexLog("About to call Page: ERM0310_enter.jsp");
					forward("ERM0310_enter.jsp", req, res);
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Exception calling page " + e);
				}
			}
			
	

			protected void procReqAutomata(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession ses)
					throws ServletException, IOException {
				UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
				ses.setAttribute("userPO", userPO);
				try {
					flexLog("About to call Page: ERM0310_enter_product_acreencia.jsp");
					forward("ERM0310_enter_product_acreencia.jsp", req, res);
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Exception calling page " + e);
				}
			}
			
	
			
			
			protected void AcreenciaProcess(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					mp = getMessageProcessor("ERM0310", req);

					ERM031001Message msgList = (ERM031001Message) mp.getMessageRecord("ERM031001", user.getH01USR(), "0002");
					//Sets the employee  number from either the first page or the maintenance page
			
					if (req.getParameter("E01REMBNK")!= null){
										
				
						msgList.setE01ACRBNK(req.getParameter("E01REMBNK"));
						
						
					
					} 
					
					
					if(req.getParameter("E01ACRBNK")!=null){
						
						
						msgList.setE01ACRBNK(req.getParameter("E01ACRBNK"));
						
						
					}

					//Sends message
					
					flexLog("ERM031001: "+msgList);
					
					mp.sendMessage(msgList);
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
					msgList = (ERM031001Message) mp.receiveMessageRecord();
				
				
					session.setAttribute("AcreList", msgList);
					
					session.setAttribute("codigo_banco", msgList.getE01ACRBNK());
					session.setAttribute("desc_banco", msgList.getE01DESBNK());					
					
					
					if (mp.hasError(error)) { // if there are errors go back to first page
						
						if(req.getParameter("E01REMBNK").equals("01")){
							
							proqNewAcre(user, req, res, session, "NEW");	
							
						}
				
						else{
							
							session.setAttribute("error", error);
							
							flexLog("About to call Page: ERM0310_enter.jsp");
							forward("ERM0310_enter.jsp", req, res);							
							
						}
						
				
					} else {
					
						forward("ERM0310_list.jsp", req, res);

				
					}


				} finally {
					if (mp != null)
						mp.close();
				}				
					
			}
	
			
			protected void proqNewAcre(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res,
					HttpSession session, String option) throws ServletException,
					IOException {

				MessageProcessor mp = null;
				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					
					mp = getMessageProcessor("ERM0310", req);
					userPO.setPurpose(option);			
					//Creates the message with operation code depending on the option

					if(option.equals("NEW")) {
						
							session.removeAttribute("AcreList");
							
							forward("ERM0310_enter_information.jsp", req, res);
					
							return;
					}
					
				
					else if (option.equals("MAINTENANCE")){
							
							session.setAttribute("userPO", userPO);
							
							ERM031001Message msgList =(ERM031001Message) session.getAttribute("AcreList");
							
							flexLog("ERM031001 vuelta"+msgList);
							
							session.setAttribute("AcreList", msgList);
							
							forward("ERM0310_enter_information.jsp", req, res);
						}
						
					
					
				
						

				} finally {
					if (mp != null)
						mp.close();
				}
				
				
			}
			
			
			
			protected void NewAcreencia(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					mp = getMessageProcessor("ERM0310", req);

					ERM031001Message msgList = (ERM031001Message) mp.getMessageRecord("ERM031001", user.getH01USR(), "0001");
					//Sets the employee  number from either the first page or the maintenance page
					//Sends message

					
					setMessageRecord(req, msgList);
					
					
					session.setAttribute("AcreList", msgList);
					
					flexLog("ERM031001NewAcreencia: "+msgList);
					mp.sendMessage(msgList);

					
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
					msgList = (ERM031001Message) mp.receiveMessageRecord();
				
					if (mp.hasError(error)) { // if there are errors go back to first page
						
				
						session.setAttribute("error", error);
						
						flexLog("About to call Page: ERM0310_enter_information.jsp");
						forward("ERM0310_enter_information.jsp", req, res);
					} else {
					
					
						AcreenciaProcess(user, req, res, session);
				
					}


				} finally {
					if (mp != null)
						mp.close();
				}				
					
			}
			
			
			
			
			protected void UpdateAcreencia(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					mp = getMessageProcessor("ERM0310", req);

					ERM031001Message msgList = (ERM031001Message) mp.getMessageRecord("ERM031001", user.getH01USR(), "0003");
					//Sets the employee  number from either the first page or the maintenance page
					//Sends message
					
					
					
					setMessageRecord(req, msgList);
					
					flexLog("ERM031001UPDTEAcreencia: "+msgList);
					mp.sendMessage(msgList);
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
					msgList = (ERM031001Message) mp.receiveMessageRecord();
				
					if (mp.hasError(error)) { // if there are errors go back to first page
						
				
						session.setAttribute("error", error);
						
						flexLog("About to call Page: ERM0310_enter_information.jsp");
						forward("ERM0310_enter_information.jsp", req, res);
					} else {
					
					
						AcreenciaProcess(user, req, res, session);
				
					}


				} finally {
					if (mp != null)
						mp.close();
				}				
					
			}
			
			
		
			
			

			protected void DeleteAcreencia(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					mp = getMessageProcessor("ERM0310", req);

					ERM031001Message msgList = (ERM031001Message) mp.getMessageRecord("ERM031001", user.getH01USR(), "0009");
					//Sets the employee  number from either the first page or the maintenance page
					//Sends message
					
					
					
					setMessageRecord(req, msgList);
					
					flexLog("ERM03100DeleteAcreencia: "+msgList);
					mp.sendMessage(msgList);
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
				
					
					
					msgList = (ERM031001Message) mp.receiveMessageRecord();
				
					
					
					if (mp.hasError(error)) { // if there are errors go back to first page
						
				
						session.setAttribute("error", error);
						
						flexLog("About to call Page: ERM0310_list.jsp");
						forward("ERM0310_list.jsp", req, res);
					} else {
					
					
						procReqAcreencia(user, req, res, session);
				
					}


				} finally {
					if (mp != null)
						mp.close();
				}				
					
			}
			
			
			
			
			
			protected void ProductList(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					mp = getMessageProcessor("ERM0310", req);

					ERM031001Message msgList = (ERM031001Message) mp.getMessageRecord("ERM031001", user.getH01USR(), "0015");
			
			
					if (req.getParameter("E01REMBNK")!= null){
								
						msgList.setE01ACRBNK(req.getParameter("E01REMBNK"));
					
					} 
					
					
					if(req.getAttribute("E01ACRBNK")!=null){
						
						msgList.setE01ACRBNK((String)req.getAttribute("E01ACRBNK"));
					}
					
					
					if(req.getParameter("E01ACRBNK")!=null){	
						
						msgList.setE01ACRBNK(req.getParameter("E01ACRBNK"));
						
					}

					//Sends message
					
					msgList.setH01USERID(user.getH01USR());
					
					flexLog("ERM031001 Product: "+msgList);
					
					mp.sendMessage(msgList);
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
				
					JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
				
					session.setAttribute("AcreListPrd", list);
					
					session.setAttribute("codigo_banco", msgList.getE01ACRBNK());
					session.setAttribute("desc_banco", msgList.getE01DESBNK());					
					
					
					if (mp.hasError(error)) { // if there are errors go back to first page
								
							session.setAttribute("PrdList", msgList);
							
							session.setAttribute("error", error);
							
							flexLog("About to call Page: ERM0310_enter_product_acreencia.jsp");
							forward("ERM0310_enter_product_acreencia.jsp", req, res);							

				
					} else {
					
						forward("ERM0310_list_automata.jsp", req, res);

				
					}


				} finally {
					if (mp != null)
						mp.close();
				}				
					
			}
			
			
			
			protected void proqNewProductAcre(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res,
					HttpSession session, String option) throws ServletException,
					IOException {

				MessageProcessor mp = null;
				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					
					mp = getMessageProcessor("ERM0310", req);
					userPO.setPurpose(option);			
					//Creates the message with operation code depending on the option

					if(option.equals("NEW")) {
						
							session.removeAttribute("cnvObj");
							
							forward("ERM0310_product_maintance.jsp", req, res);
							return;
					}
					
					else if (req.getParameter("codigo_lista")!= null){
						
						JBObjList list = (JBObjList)session.getAttribute("AcreListPrd");
						int index = Util.parseInt(req.getParameter("codigo_lista"));
						ERM031001Message listMessage = (ERM031001Message)list.get(index);
					
					if (option.equals("MAINTENANCE")){
							
						session.setAttribute("userPO", userPO);
						session.setAttribute("cnvObj", listMessage);
							
							forward("ERM0310_product_maintance.jsp", req, res);
						}
						
					
					}
				
						

				} finally {
					if (mp != null)
						mp.close();
				}
				
				
			}
			
			
			
			protected void NewProductAcreencia(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					mp = getMessageProcessor("ERM0310", req);

					ERM031001Message msgList = (ERM031001Message) mp.getMessageRecord("ERM031001", user.getH01USR(), "0011");
					//Sets the employee  number from either the first page or the maintenance page
					//Sends message

					setMessageRecord(req, msgList);
			
								
					flexLog("ERM031001NewAcreenciaPrd: "+msgList);
					mp.sendMessage(msgList);
					
					session.setAttribute("cnvObj", msgList);
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
					msgList = (ERM031001Message) mp.receiveMessageRecord();
				
					flexLog("ERM031001NewAcreenciaVuelta: "+msgList);
					
					if (mp.hasError(error)) { // if there are errors go back to first page
						
						
						session.setAttribute("error", error);
						
						flexLog("About to call Page: ERM0310_product_maintance.jsp");
						forward("ERM0310_product_maintance.jsp", req, res);
					} else {
					
					
						ProductList(user, req, res, session);
				
					}


				} finally {
					if (mp != null)
						mp.close();
				}				
					
			}
			
			protected void UpdateProductAcreencia(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {

				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					mp = getMessageProcessor("ERM0310", req);

					ERM031001Message msgList = (ERM031001Message) mp.getMessageRecord("ERM031001", user.getH01USR(), "0012");
					//Sets the employee  number from either the first page or the maintenance page
					//Sends message

					setMessageRecord(req, msgList);
			
					
					
					flexLog("ERM031001UpdateAcreenciaPrd: "+msgList);
					mp.sendMessage(msgList);
					
					session.setAttribute("cnvObj", msgList);
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
					msgList = (ERM031001Message) mp.receiveMessageRecord();
				
					flexLog("ERM03100UpdateAcreenciaVuelta: "+msgList);
					
					if (mp.hasError(error)) { // if there are errors go back to first page
						
						
						session.setAttribute("error", error);
						
						flexLog("About to call Page: ERM0310_product_maintance.jsp");
						forward("ERM0310_product_maintance.jsp", req, res);
					} else {
					
					
						ProductList(user, req, res, session);
				
					}


				} finally {
					if (mp != null)
						mp.close();
				}				
					
			}
			
			protected void DeletePrdAcreencia(ESS0030DSMessage user,
					HttpServletRequest req, HttpServletResponse res, HttpSession session)
					throws ServletException, IOException {
				MessageProcessor mp = null;

				UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

				try {
					mp = getMessageProcessor("ERM0310", req);

					ERM031001Message msgList = (ERM031001Message) mp.getMessageRecord("ERM031001", user.getH01USR(), "0013");
					
					if (req.getParameter("codigo_lista")!= null){
						
						JBObjList list = (JBObjList)session.getAttribute("AcreListPrd");
						int index = Util.parseInt(req.getParameter("codigo_lista"));
						ERM031001Message listMessage = (ERM031001Message)list.get(index);
						
			
						msgList.setE01ACRBNK(listMessage.getE01ACRBNK());
						msgList.setE01ACRCDE(listMessage.getE01ACRCDE());
					}
					
					
					flexLog("ERM031001DeleteAcreenciaPrd: "+msgList);
					mp.sendMessage(msgList);
					
					session.setAttribute("cnvObj", msgList);
					
					ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
					msgList = (ERM031001Message) mp.receiveMessageRecord();
				
					
					req.setAttribute("E01ACRBNK", msgList.getE01ACRBNK());
					
					flexLog("ERM03100DeleteAcreenciaVuelta: "+msgList);
					
					if (mp.hasError(error)) { // if there are errors go back to first page
						
						
						session.setAttribute("error", error);
						
						flexLog("About to call Page: ERM0310_product_maintance.jsp");
						forward("ERM0310_product_maintance.jsp", req, res);
					} else {
					
					
						ProductList(user, req, res, session);
				
					}


				} finally {
					if (mp != null)
						mp.close();
				}
					
			}	
			
			
	
			
}
