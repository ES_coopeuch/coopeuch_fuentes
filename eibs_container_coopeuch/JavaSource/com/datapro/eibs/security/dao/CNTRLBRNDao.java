package com.datapro.eibs.security.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.BranchNotFoundException;
import com.datapro.generic.access.DAOSuper;
import com.datapro.eibs.security.bean.CNTRLBRN;
import com.datapro.generic.beanutil.*;
import datapro.eibs.beans.JBObjList;


public class CNTRLBRNDao extends DAOSuper {


    private static Category cat =
		Category.getInstance(CNTRLBRNDao.class.getName());
			
	/**
	 * CNTRLBTHDao constructor comment.
	 */

	public CNTRLBRNDao(String DBID) {
		super(DBID);
	}
	
	/**
	 * Constructor for DAOUserEibs.
	 */
	public CNTRLBRNDao() {
		super();
	}

	/**
	 * Constructor for DAOUserEibs.
	 * @param connection
	 */
	public CNTRLBRNDao(Connection connection) {
		super(connection);
	}
	
	public JBObjList select() throws DAOException {
		
		
		JBObjList beanList = new JBObjList();
    	
    	try {
			
			String sql = "SELECT * FROM cntrlbrn ORDER BY brnbnk, brnnum";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
						
			while (true) {
				if (!rs.next ())
				 break;
				 
				ResultSetMetaData md = rs.getMetaData();				
				CNTRLBRN bean = new CNTRLBRN ();
				BeanParser bn = new BeanParser(bean);
				if (bn.parseResultSet(md,rs)) beanList.addRow(bean);
			}
			rs.close();
			ps.close();
			if (beanList.getNoResult()) beanList=null;
			

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}
		return beanList;
    	
    	
	}
	
	public JBObjList selectBank() throws DAOException {
				
		JBObjList beanList = new JBObjList();
    	    	
    	try {
			
			String sql = "SELECT DISTINCT brnbnk FROM cntrlbrn ORDER BY brnbnk";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
						
			while (true) {
				if (!rs.next ())
				 break;
				 
				ResultSetMetaData md = rs.getMetaData();
				String bank	= rs.getString("BRNBNK");			
				beanList.addRow(bank);
			}
			rs.close();
			ps.close();
			if (beanList.getNoResult()) beanList=null;
			

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}
		return beanList;
		
	}
	
	public JBObjList select(String BANK) 
	throws BranchNotFoundException, DAOException {

		JBObjList list = null;

		try {
			String sql = "SELECT * FROM CNTRLBRN WHERE BRNBNK=? ORDER BY BRNBNK, BRNNUM";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, BANK);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new JBObjList();
			while (rs.next()) {
				CNTRLBRN bean = new CNTRLBRN();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new BranchNotFoundException(
					"Branch not found exception(bank: " + BANK + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;
		
	}
	
	public CNTRLBRN select(String bnk,String brn) 
	throws BranchNotFoundException, DAOException {

		CNTRLBRN bean = null;

		try {
			String sql = "SELECT * FROM CNTRLBRN WHERE BRNBNK=? AND BRNNUM=? ORDER BY BRNBNK, BRNNUM";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, bnk);
			ps.setString(2, brn);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNTRLBRN();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} else {
				throw new BranchNotFoundException(
					"Branch not found exception(bank: " + bnk + " - branch: " + brn + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return bean;
	}

}