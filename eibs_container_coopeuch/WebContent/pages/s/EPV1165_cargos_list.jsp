<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV116501Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields;import java.math.*;"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV116501List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">

function getRadioButtonSelectedValue(ctrl)
{
    for(i=0;i<ctrl.length;i++){
        if(ctrl[i].checked) return ctrl[i].value;
    }
    return ctrl.value
}

function showChecking(idx) {
	for ( var i=0; i<dataTable.rows.length; i++ ) {
       dataTable.rows[i].className="trnormal";
	}
    dataTable.rows[idx].className="trhighlight";

}
  function goAction(op) {
	var ok = false;
	var cun = "";
	var pg = "";

	if (op != '200'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01PVHSEQ") 
	      	{	
	      		if (element.checked == true) {
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = true;
      	
      	if (op =='202'){
      		confirm1 = confirm("Desea Eliminar el Cargos Adicional seleccionado?");
      	}
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			if (op =='202'){//solo para eliminar..
				document.forms[0].submit();		
			     <%//NOTA: solo para activar el check de la pagina integral%>
			     <%  String re =(String) session.getAttribute("EMPCA");%>
				 <%  if ("S".equals(re)){%>
						parent.setRecalculate3();	       
				 <% } %>						
			}else{
				dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1165?SCREEN="+document.forms[0].SCREEN.value+
					  "&cusNum="+document.forms[0].cusNum.value+"&Header23="+document.forms[0].Header23.value;					  		
//				if (op =='201' || op =='203'){
				if (op =='201'){				
					  dir = dir + "&E01PVHSEQ="+getRadioButtonSelectedValue(document.forms[0].E01PVHSEQ);
				}			
				CenterWindow(dir,780,500,2);
			}				
		}		

     } else {
		alert("Debe seleccionar un Cargos Adicionales para continuar.");	   
	 }
      
	}
	
function GetInqDet(col)
{
	dir = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1165?SCREEN=203&cusNum="+document.forms[0].cusNum.value+"&Header23="+document.forms[0].Header23.value;
	dir = dir + "&E01PVHSEQ="+col;
	CenterWindow(dir,780,500,2);
} 	
</script>

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

 <% String emp = (String)session.getAttribute("EMPCA");
 	emp = (emp==null)?"":emp;//si es blanco viene llamado por menu, sino viene llamdo desde la pantalla EPV1010
 %>
<%if ("".equals(emp)){ %>
<h3 align="center">Listado de Cargos Adicionales<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cargos_list.jsp,EPV1165"></h3>
<hr size="4">
<%}%>
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1165">
	<input type="hidden" name="SCREEN" value="201">
	<input type=HIDDEN name="totalRow" value="0">
     
<%if ("".equals(emp)){ %>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Cliente : 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Propuesta : 
               </td>
             <td nowrap width="50%"align="left">
   		        <input type="text" name="Header23" size="13" maxlength="12" value="<%= userPO.getHeader23()%>" readonly>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
<%}else{%>
    <input type="hidden" name="cusNum" size="13" maxlength="12" value="<%= userPO.getCusNum()%>" readonly>		
    <input type="hidden" name="Header23" size="13" maxlength="12" value="<%= userPO.getHeader23()%>" readonly>	
<%}%>  

<%if (!"I".equals(emp)){ %>	    		 
<table class="tbenter" width="100%">
	<tr>
 		<td align="center" class="tdbkg" width="20%"><a href="#"
			onclick="goAction('200')"> <b>Crear</b> </a></td> 
			<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('201')"> <b>Modificar</b> </a></td>
		<td align="center" class="tdbkg" width="20%">
			<a href="javascript:goAction('202')"> <b>Borrar</b> </a>
		</td> 
<%if ("".equals(emp)){ %>		
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
<%}%>	
	</tr>
</table>
<%}%>  

<%	
	int row = 0;
	if (EPV116501List.getNoResult()) {
%>
<table id="headTable" width="100%">
	<tr id="trclear">
		<th align="center" nowrap width="2%">&nbsp;</th>
		<th align="center" nowrap width="5%">Sec</th>						
		<th align="center" nowrap width="5%">Codigo</th>
		<th align="center" nowrap width="40%">Descripcion</th>
		<th align="center" nowrap width="20%">Monto</th>
		<th align="center" nowrap width="20%"></th>
	</tr>
</table>
<br>
<table class="tbenter" width=100%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No existen Cargos Adicionales a la solicitud. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>

	<table  id="mainTable" ALIGN=CENTER style="width:'95%'" height="68%" border="0">
		<tr height="5%">
		<TD NOWRAP width="100%" >
  				<TABLE id="headTable" width="97%" >
  					<TR id="trclear">  
						<th align="center" nowrap width="2%">&nbsp;</th>
						<th align="center" nowrap width="5%">Sec</th>						
						<th align="center" nowrap width="5%">Codigo</th>
						<th align="center" nowrap width="40%">Descripcion</th>
						<th align="center" nowrap width="20%">Monto</th>
						<th align="center" nowrap width="20%"></th>
          			</TR>
       			</TABLE>
      		</td>
		</tr>
		<tr height="95%">    
			<td NOWRAP width="100%">    		
   			    <div id="dataDiv1" class="scbarcolor" style="width:100%; height:100%; overflow:auto;">
    				<table id="dataTable" width="97%" > 
    				    							
						<% double suma = 0;
							EPV116501List.initRow();
								int k = 0;
								boolean firstTime = true;
								int errS = 0;
								
								String chk = "";
								while (EPV116501List.getNextRow()) {
									if (firstTime) {
										firstTime = false;
										chk = "checked";
									} else {
										chk = "";
									}
									EPV116501Message convObj = (EPV116501Message) EPV116501List.getRecord();
									
									suma += convObj.getBigDecimalE01PVHAMT().doubleValue();
									
									//detecta que se presenta mas de un seguro de desgravamen
									if(convObj.getE01PVHCDE().equals("105") || convObj.getE01PVHCDE().equals("121"))
										errS++; 
						%>
						<tr id="dataTable<%= EPV116501List.getCurrentRow() %>">
							<td nowrap width="2%">
								<%if(!convObj.getE01PVHAPC().equals("4")) { %>
									<input type="radio" name="E01PVHSEQ"	value="<%=convObj.getE01PVHSEQ()%>" <%=chk%> 
								 	onClick="javascript:showChecking(<%= EPV116501List.getCurrentRow() %>);" />
								<%} %>
							</td>
							<td nowrap width="5%" align="center"><a href="javascript:GetInqDet('<%=convObj.getE01PVHSEQ()%>');"><%=Util.formatCell(convObj.getE01PVHSEQ())%></a></td>							
							<td nowrap align="center"><a href="javascript:GetInqDet('<%=convObj.getE01PVHSEQ()%>');"><%=convObj.getE01PVHCDE()%></a></td>
							<td nowrap align="left"><a href="javascript:GetInqDet('<%=convObj.getE01PVHSEQ()%>');"><%=convObj.getE01PVHNAR()%></a></td>
							<td nowrap align="right"><a href="javascript:GetInqDet('<%=convObj.getE01PVHSEQ()%>');"><%=convObj.getE01PVHAMT()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
							</td>
						</tr>
						<%
							}			
						%>
						<tr>
							<td nowrap>&nbsp;</td>
							<td nowrap align="center">&nbsp;</td>
							<td nowrap align="left">&nbsp;</td>
							<td nowrap align="right"><b>TOTAL : </b> </td>
							<td nowrap align="right"><b><%=datapro.eibs.master.Util.formatCCY(suma)%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
						<%if(errS>1) {%>
							<input type=HIDDEN id="errSeg" value="ERR">
						<%}else { %>
							<input type=HIDDEN id="errSeg" value="">
						<%}%>
						</tr>
								
					</table>
   				</div>
   			</TD>
		</TR>	
	</TABLE>



<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV116501List.getShowPrev()) {
					int pos = EPV116501List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1165?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV116501List.getShowNext()) {
					int pos = EPV116501List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1165?SCREEN=100&customer_number="
									+ request.getAttribute("customer_number")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>
</form>
<%	
	if (!EPV116501List.getNoResult()) {
%>
  <SCRIPT language="JavaScript">
			showChecked("E01PVHSEQ");  
			function resizeDoc() {
      		 	divResize();
     		    adjustEquTables(headTable, dataTable, dataDiv1,1,false);
      		}
	 		resizeDoc();   			
     		window.onresize=resizeDoc;        
     </SCRIPT>
<%
	}
%>     
</body>
</html>
