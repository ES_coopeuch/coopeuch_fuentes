<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Definici&oacute;n de Comisiones - Relaci&oacute;n</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "RTComisEle" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="refComi" class="datapro.eibs.beans.EDD232001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<script language="JavaScript">



function goAction(op) {

	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}


function cancel() {
	document.forms[0].SCREEN.value = 100;
	document.forms[0].submit();
}


function goDelete() {

	if(confirm("Esta seguro que desea borrar este codigo?")){
		document.forms[0].opt.value = 3;
		document.forms[0].submit();
	}
}

</SCRIPT>  

</head>

<BODY>
<h3 align="center">Listado de Relaciones  Comisi&oacute;n<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="commission_relation_list, EDD2320"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2320" >
    <input type=HIDDEN name="SCREEN" value="1800">
    <input type=HIDDEN name="opt"> 
  
  <p> 
  <h4>Datos Comisi&oacute;n</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Comisi&oacute;n :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01CMTMCCM" size="5" maxlength="4" value="<%= refComi.getE01CMTMCCM().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Descripci&oacute;n  :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E01CMTMDCM" size="31" maxlength="30" value="<%= refComi.getE01CMTMDCM().trim()%>" readonly>
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Tipo de Comisi&oacute;n :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMTCM" size="2" maxlength="1" value="<%= refComi.getE01CMTMTCM().trim()%>" readonly>
                <input type="text" name="E01CMTMTCG" size="31" maxlength="30" value="<%= refComi.getE01CMTMTCG().trim()%>" readonly>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Tipo Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMTCB" size="2" maxlength="1" value="<%= refComi.getE01CMTMTCB().trim()%>" >
                <input type="text" name="E01CMTMTBG" size="31" maxlength="30" value="<%= refComi.getE01CMTMTBG().trim()%>" >
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Periocidad de Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMPCB" size="3" maxlength="2" value="<%= refComi.getE01CMTMPCB().trim()%>"  readonly>
                <input type="text" name="E01CMTMPCG" size="31" maxlength="30" value="<%= refComi.getE01CMTMPCG().trim()%>"  readonly>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Modalidad de Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMMCB" size="2" maxlength="1" value="<%= refComi.getE01CMTMMCB().trim()%>"   readonly>
                <input type="text" name="E01CMTMMCG" size="31" maxlength="30" value="<%= refComi.getE01CMTMMCG().trim()%>"   readonly>
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">D&iacute;a de C&aacute;lculo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMDCC" size="3" maxlength="2" value="<%= refComi.getE01CMTMDCC().trim()%>"  readonly>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">D&iacute;a de Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMDAP" size="2" maxlength="1" value="<%= refComi.getE01CMTMDAP().trim()%>"   readonly>
              </div>
            </td>
          </tr> 
          
          <tr id="trclear"> 
            <td nowrap  height="23"> 
              <div align="right">Impuesto :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMIPT" size="2" maxlength="1" value="<%= refComi.getE01CMTMIPT().trim()%>"   readonly>
              </div>
            </td>
            <td nowrap height="23"> 
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

    <%
	if ( RTComisEle.getNoResult() ) {
 %>
  </p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
          <table class="tbenter" width=100% align=center>
            <tr> 
              <td class=TDBKG width="50%"> 
                <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
              </td>
              <td class=TDBKG width="50%"> 
                <div align="center"><a href="javascript:cancel()"><b>Volver</b></a></div>
              </td>
            </tr>
          </table>
          <p>&nbsp;</p>
          
        </div>

	  </TD>
	</TR>
    </TABLE>
	
  <%  
		}
	else {
%> <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }

%> 
 
          
  <table class="tbenter" width=100% align=center height="8%">
    <tr> 
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
      </td>
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(2)"><b>Modificar</b></a></div>
      </td>
	<td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goDelete(3)"><b>Borrar</b></a></div>
      </td>      
	<td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:cancel()"><b>Volver</b></a></div>
      </td>      
    </tr>
  </table>
  <br>
 
 

  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
            <th align=CENTER nowrap width="20%">COD. PRODUCTO</th>
            <th align=LEFT nowrap >COD. CARGO</th>
            <th align=LEFT nowrap >COD TRX.</th>
            <th align=LEFT nowrap >CON PL&Aacute;STICO</th>
            <th align=LEFT nowrap >FILTRO 1</th>
            <th align=LEFT nowrap >FILTRO 2</th>
            <th align=LEFT nowrap >FILTRO 3</th>
            <th align=LEFT nowrap >FILTRO 4</th>
            <th align=LEFT nowrap >FILTRO 5</th>
            <th align=LEFT nowrap >COD COMISI&Oacute;N</th>
            <th align=LEFT nowrap >COD TARIFA</th>
            <th align=LEFT nowrap >COD DESCUENTO</th>
          </tr>
 
           <%
                RTComisEle.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (RTComisEle.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	datapro.eibs.beans.EDD232002Message msgList = (datapro.eibs.beans.EDD232002Message) RTComisEle.getRecord();
		 %>
          <tr id="dataTable<%= RTComisEle.getCurrentRow() %>"> 
            <td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE2" value="<%= RTComisEle.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLPRO() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLCCA() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLTRX() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLPLA() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLFI1() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLFI2() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLFI3() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLFI4() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLFI5() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLCCM() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLCTF() %></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgList.getE02CMRLCDC() %></td>
          </tr>
          <%
                }
              %>
          </table>
      </td>
    </tr>
  </table>

  
   

<SCRIPT language="JavaScript">
	showChecked("CURRCODE2");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

</body>
</html>
