<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Reporte de Ventas de Seguros</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

  
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "msgenter" class= "datapro.eibs.beans.ESG060001Message"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

</head>
<body>
 
<H3 align="center">Reporte de Ventas de Seguros
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="reporte_ventas_seguros_enter.jsp, ESG0600">
</H3>

<hr size="4"> 
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESG0600" >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">
    
<b><p align="left">SUCURSAL : <%=currUser.getE01UBR()%></p></b>
<table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
 			<td nowrap width="15%" align="right">Fecha Desde : 
            </td>
            <td nowrap width="35%"align="left">
  				<div align="left" > 
                <input type="text" name="E01PACFID" id="fecha1" size="3" maxlength="2" value="<%=msgenter.getE01PACFID().trim() %>" >
                <input type="text" name="E01PACFIM" id="fecha2" size="3" maxlength="2" value="<%=msgenter.getE01PACFIM().trim() %>" >
                <input type="text" name="E01PACFIY" id="fecha3" size="5" maxlength="4" value="<%=msgenter.getE01PACFIY().trim() %>" >
                <a id="f1" style="visibility:visible" href="javascript:DatePicker(document.forms[0].E01PACFID,document.forms[0].E01PACFIM,document.forms[0].E01PACFIY)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
              </div>
             </td>         

			<td nowrap width="15%" align="right">Fecha Hasta : 
            </td>

            <td nowrap width="35%" align="left" >
  				<div align="left" > 
  			
                <input type="text" name="E01PACFHD" id="fecha4" size="3" maxlength="2" value="<%=msgenter.getE01PACFHD().trim() %>" >
                <input type="text" name="E01PACFHM" id="fecha5" size="3" maxlength="2" value="<%=msgenter.getE01PACFHM().trim() %>" >
                <input type="text" name="E01PACFHY" id="fecha6" size="5" maxlength="4" value="<%=msgenter.getE01PACFHY().trim() %>" >
                <a id="f2" style="visibility:visible" href="javascript:DatePicker(document.forms[0].E01PACFHD,document.forms[0].E01PACFHM,document.forms[0].E01PACFHY)">
                <img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0" >
                </a> 	
              </div>
             </td>         

         </tr>
         </table>
      </td>
    </tr>
  </table>   

  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
<script language="JavaScript">
  document.forms[0].E01PACFID.focus();
  document.forms[0].E01PACFID.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
