<html>
<head>
<title>Integrantes Grupo Económico</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="mbrGroup" class="datapro.eibs.beans.ESD009201Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }
   
</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
%>


<H3 align="center">Integrantes Grupo Económico - Nuevo<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="group_member_new.jsp, ESD0092"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0092" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="500">
  <input type=HIDDEN name="OPECODE" value="0001">
  <h4>Informaci&oacute;n B&aacute;sica</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right">Grupo Económico :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" readonly name="E01CNORCD" size="6" maxlength="4" value="<%= mbrGroup.getE01CNORCD().trim()%>">
                <input type="text" readonly name="E01CNODSC" size="36" maxlength="35" value="<%= mbrGroup.getE01CNODSC().trim()%>" >
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width="16%" height="23"> 
              <div align="right">Cliente :</div>
            </td>
            <td nowrap height="23"> 
              <div align="left"> 
                <input type="text" readonly name="E01GEMCUN" size="11" maxlength="9" value="<%= mbrGroup.getE01GEMCUN().trim()%>">
                <a href="javascript:GetCustomerDescId('E01GEMCUN','E01GEMNA1','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0" ></a>
                <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="bottom" border="0">
              </div> 
              
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Identificación :</div>
            </td>
            <td nowrap height="19"> 
              <div align="left"> 
                <input type="text" readonly name="E01GEMIDN" size="27" maxlength="25" value="<%= mbrGroup.getE01GEMIDN().trim()%>">
                <input type="text" readonly name="E01GEMTID" size="5"  maxlength="4"  value="<%= mbrGroup.getE01GEMTID().trim()%>">
                <input type="text" readonly name="E01GEMPID" size="5"  maxlength="4"  value="<%= mbrGroup.getE01GEMPID().trim()%>">
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width="16%" height="23"> 
              <div align="right">Nombre :</div>
            </td>
            <td nowrap height="23"> 
              <div align="left"> 
                <input type="text" name="E01GEMNA1" size="61" maxlength="60" value="<%= mbrGroup.getE01GEMNA1().trim()%>">
              </div>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>

  <div align="center">
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>

</form>
</body>
</html>
