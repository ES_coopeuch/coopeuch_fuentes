<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Definici&oacute;n de Descuentos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "RTTabList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<script language="JavaScript">



function goAction(op) {
	if(op==4)
		document.forms[0].SCREEN.value = "6100";
		
	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}

function goDelete() {

	if(confirm("Esta seguro que desea borrar este codigo?")){
		document.forms[0].opt.value = 3;
		document.forms[0].submit();
	}
}

</SCRIPT>  

</head>

<BODY>
<h3 align="center">Listado de Tablas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="tabla_list, EDD2320"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2320" >
    <input type=HIDDEN name="SCREEN" value="4800">
    <input type=HIDDEN name="opt"> 
  
<%
	if ( RTTabList.getNoResult() ) {
 %>
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
          <table class="tbenter" width=100% align=center>
            <tr> 
              <td class=TDBKG > 
                <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
              </td>
            </tr>
          </table>
          <p>&nbsp;</p>
        </div>
	  </TD>
	</TR>
  </TABLE>

  <%  
	}
	else 
	{
 
  if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }

%> 
 
          
  <table class="tbenter" width=100% align=center height="8%">
    <tr> 
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
      </td>
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(2)"><b>Modificar</b></a></div>
      </td>
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(4)"><b>Ingreso</b></a></div>
      </td>
	  <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goDelete(3)"><b>Borrar</b></a></div>
      </td>      
    </tr>
  </table>
  <br>

  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
            <th align=LEFT nowrap width="20%">COD. TABLA</th>
            <th align=LEFT nowrap >DESCRIPCI&Oacute;N</th>
            <th align=LEFT nowrap >OBJ. TABLA</th>
            <th align=LEFT nowrap >TIPO TABLA</th>
            <th align=LEFT nowrap >TIPO RESULTADO</th>
            <th align=LEFT nowrap >MONEDA TABLA</th>
            <th align=LEFT nowrap >MONEDA RESULTADO</th>
          </tr>
 
           <%
                RTTabList.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (RTTabList.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	datapro.eibs.beans.EDD232005Message msgList = (datapro.eibs.beans.EDD232005Message) RTTabList.getRecord();
		 %>
          <tr id="dataTable<%= RTTabList.getCurrentRow() %>"> 
            <td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE" value="<%= RTTabList.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>
            <td NOWRAP  align=CENTER width="20%"><%= msgList.getE05CMTCTDC() %></td>
            <td NOWRAP  align=LEFT><%= msgList.getE05CMTCGDC() %></td>
            <td NOWRAP  align=LEFT><%= msgList.getE05CMTCTCG() %></td>
            <td NOWRAP  align=LEFT><%= msgList.getE05CMTCITG() %></td>
            <td NOWRAP  align=LEFT><%= msgList.getE05CMTCTMG() %></td>
            <td NOWRAP  align=LEFT><%= msgList.getE05CMTCMNG() %></td>
            <td NOWRAP  align=LEFT><%= msgList.getE05CMTCMRG() %></td>
          </tr>
          <%}%>
          </table>
      </td>
    </tr>
  </table>
  
     
<SCRIPT language="JavaScript">
	showChecked("CURRCODE");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

</body>
</html>
