<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import="datapro.eibs.beans.ECC015002Message"%>

<html>

<head>

<jsp:useBean id="cchData" class="datapro.eibs.beans.ECC015001Message" scope="session" />
<jsp:useBean id="ECC015002List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
	
<title>Resumen de Movimientos Internacionales de Tarjeta de Credito</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>


</head>

<BODY><form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150">
<INPUT TYPE=HIDDEN NAME="SCREEN" value="10">
<INPUT TYPE=HIDDEN NAME="opt" VALUE="1">

<TABLE  class="tableinfo" width="100%" CELLPADDING=0 CELLSPACING=0>	
        <tr id="trdark"> 
            <td nowrap align="left" width="80">
				<IMG src="<%=request.getContextPath()%>/images/logo1_mastercard.JPG" align="left" WIDTH=136 HEIGHT=46> 
		    </td>
		    <td nowrap align="right"  width="20"><b> </b>                 
		    	<IMG src="<%=request.getContextPath()%>/images/logo1_coopeuch.JPG" align="right" WIDTH=136 HEIGHT=46>
		    </td>
		</tr>
</TABLE>
		
<TABLE  class="tableinfo" width="100%" CELLPADDING=0 CELLSPACING=0>	
			<TR id="trclear">
				<!-- <td align="center" rowspan=0 ><h3>ESTADO DE CUENTA INTERNACIONAL DE TARJETA DE CREDITO</h3></Td>-->			
				<td align="center" rowspan=0 ><h3>RESUMEN DE MOVIMIENTOS INTERNACIONALES DE TARJETA DE CREDITO</h3></Td>
			</TR>
		</TABLE>

<TABLE class="tableinfo" width="100%">
	<TR>
		<TD>
		 <table border=0 CELLPADDING=0 CELLSPACING=0 width="100%">		
			<TR id="trdark">
				<td width="30%" align="left" rowspan="3"><b>NOMBRE DEL TITULAR <br>No DE TARJETA DE CREDITO<br> FECHA RESUMEN DE MOVIMIENTOS</b></Td>
				<td width="70%" align="left" rowspan="3"><b><%=cchData.getE01CCHNOM() %></b><br>
														 <b><%=cchData.getE01CCHNXN() %></b><br>
														 <b><%=cchData.getE01CCHFFD()%>/<%=cchData.getE01CCHFFM()%>/<%=cchData.getE01CCHFFA()%></b>
				 </Td>
			</TR>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<br>
<TABLE  class="tableinfo" width="100%">
	<TR>
		<TD>
		 <table cellpadding=0 cellspacing=0 width="100%" border=0>		
			<TR id="trclear">
				<td align="left" rowspan=0 ><h3>I. INFORMACION GENERAL</h3></Td>			
			</TR>
		</TABLE>
		</TD>
	</TR>
	<TR>
		<TD>
		 <table border=1 frame="box" CELLPADDING=0 CELLSPACING=0 width="100%">
				<TR id="trdark">
				<td width="30%" ALIGN=CENTER nowrap> </td>
				<td width="20%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>CUPO TOTAL</b></Td>
				<td width="20%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>CUPO UTILIZADO</b></Td>
				<td width="20%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>CUPO DISPONIBLE</b></Td>
				<td width="10%" ALIGN=CENTER rowspan="3"></Td>			
			</TR>
			<TR id="trdark">
				<Td width="30%" ALIGN="left" nowrap><b>CUPO TOTAL</b></Td>
				<Td width="20%" ALIGN=CENTER nowrap>US$<%= Util.formatCCY(cchData.getE01CCHCDO()) %></Td>
				<Td width="20%" ALIGN=CENTER nowrap>US$<%= Util.formatCCY(cchData.getE01CCHMUD()) %></Td>
				<Td width="20%" ALIGN=CENTER nowrap>US$<%= Util.formatCCY(cchData.getE01CCHCDD()) %></Td>			
			</TR>
		</TABLE>
    </TD>
</TR>
</TABLE>
<br>
<TABLE  class="tableinfo" width="100%">
	<TR>
		<TD width="100%">
		 <table cellpadding=0 cellspacing=0 width="100%" border=0>		
			<TR id="trclear">
				<td align="left" rowspan=0 ><h3>II. DETALLE</h3></Td>				
			</TR>
		</TABLE>
		 <table border=1 CELLPADDING=0 CELLSPACING=0 width="100%">
			<TR id="trclear">
				<td width="100%" align="left" nowrap><b>1. INFORMACION DE PAGO</b></Td>			
			</TR>
		</table>
		 <table border=1 frame="box" CELLPADDING=0 CELLSPACING=0 width="100%">
			<TR id="trdark">
				<td width="30%" align="left" nowrap><b><small>SALDO ANTERIOR FACTURADO</small></b></Td>
				<Td width="15%" ALIGN="right" nowrap>US$<%=cchData.getE01CCHSAD()%></Td>
				<Td width="10%" ALIGN=CENTER nowrap></Td>
				<td width="30%" align="left" nowrap><b><small>PERIODO FACTURADO DESDE</small></b></Td>
				<Td width="15%" ALIGN=right nowrap><%=Util.formatDate(cchData.getE01CCHVND(), cchData.getE01CCHVNM(), cchData.getE01CCHVNA())%></Td>												
			</TR>
			<TR id="trdark">
				<td width="30%" align="left" nowrap><b><small>ABONO REALIZADO</small></b></Td>
				<Td width="15%" ALIGN="right" nowrap>US$<%=cchData.getE01CCHSAD()%></Td>
				<Td width="10%" ALIGN=CENTER nowrap></Td>
				<td width="30%" align="left" nowrap><b><small>PERIODO FACTURADO HASTA</small></b></Td>
				<Td width="15%" ALIGN=right nowrap><%=Util.formatDate(cchData.getE01CCHFFD(),cchData.getE01CCHFFM(),cchData.getE01CCHFFA())%></Td>												
		    </TR>
			<TR id="trdark">
				<td width="30%" align="left" nowrap><b><small>TRASPASO DEUDA NACIONAL</small></b></Td>
				<Td width="15%" ALIGN="right" nowrap>US$<%=cchData.getE01CCHTDN()%></Td>
				<Td width="10%" ALIGN=CENTER nowrap></Td>
				<td width="30%" align="left" nowrap><b><small>PAGAR HASTA</small></b></Td>
				<Td width="15%" ALIGN=right nowrap><%=Util.formatDate(cchData.getE01CCHVTD(),cchData.getE01CCHVTM(),cchData.getE01CCHVTA())%></Td>												
		    </TR>	
			<TR id="trdark">
				<td width="30%" align="left" nowrap><b><small>DEUDA TOTAL</small></b></Td>
				<Td width="15%" ALIGN="right" nowrap>US$<%=cchData.getE01CCHDTD()%></Td>
				<Td width="10%" ALIGN=CENTER colspan="3"></Td>
		    </TR>	 
		</TABLE>
		</TD>
	</TR>
</TABLE>
<br>
<TABLE  class="tableinfo" width="100%">
	<TR>
		<TD width="100%">
		 <table border=1  CELLPADDING=0 CELLSPACING=0 width="100%">
			<TR id="trclear">
				<td width="100%" align="left" nowrap><b>2. INFORMACION DE TRANSACCIONES</b></Td>			
			</TR>
		</table>
		 <table border=1 frame="rhs" CELLPADDING=0 CELLSPACING=0 width="100%">	
			<TR id="trdark">
				<td width="25%" align=CENTER nowrap><small>NUMERO REFERENCIA <br>INTERNACIONAL</small></Td>
				<Td width="10%" ALIGN=CENTER nowrap><small>FECHA<br>OPERACION</small> </Td>
				<Td width="25%" ALIGN=CENTER nowrap><small>DESCRIPCION OPERACION O COBRO</small> </Td>	
	    		<Td width="15%" ALIGN=CENTER nowrap><small>CIUDAD</small></Td>
	    		<Td width="5%" ALIGN=CENTER nowrap><small>PAIS</small></Td>
	    		<Td width="10%" ALIGN=CENTER nowrap><small>MONTO<br>MONEDA<br>ORIGEN</small></Td>	
	       		<Td width="10%" ALIGN=CENTER nowrap><small>MONTO<br>US$</small></Td>	    			    												
			</TR>
			<%
				ECC015002List.initRow();
					boolean firstTime = true;
					while (ECC015002List.getNextRow()) {
						ECC015002Message convObj = (ECC015002Message) ECC015002List
								.getRecord();
								


	
								
								
			if (Integer.parseInt(convObj.getE02CCDCOR()) == 1 && Integer.parseInt(convObj.getE02CCDTTR()) == 1) {
			%>
			<TR id="trdark">
				<Td ALIGN=CENTER colspan="6" nowrap> </Td>
	       		<Td ALIGN=CENTER nowrap> </Td>	    			    												
			</TR>
			<TR id="trclear">
				<Td ALIGN=CENTER colspan="6" nowrap><b><%= convObj.getE02CCDNCO()%></b></Td>
	       		<Td ALIGN="right" nowrap><b><%= Util.formatCCY(convObj.getE02CCDMCU())%></b></Td>	    			    												
			</TR>
			<%
				} else {
			%>			
			<tr id="trdark">
				<td nowrap align="center"><%= convObj.getE02CCDREF()%></td>
				<td nowrap align="center"><%=Util.formatDate(convObj.getE02CCDFTD(),convObj.getE02CCDFTM(),convObj.getE02CCDFTA())%></td>								
				<td nowrap align="left"><%= convObj.getE02CCDNCO()%></td>
				<td nowrap align="left"><%= convObj.getE02CCDCIC()%></td>
				<td nowrap align="left"><%= convObj.getE02CCDCPA()%></td>
				<%
    
    double MonUS = 0; 
    try {
     String StrMonUS = convObj.getE02CCDMTR().replaceAll(",","");
     StrMonUS = StrMonUS.replace(".","");

     MonUS = Double.parseDouble(StrMonUS);
     }
    catch(Exception e )
     {
      MonUS = 0;
     }
     
    if (MonUS!=0 )
     MonUS = MonUS / 100;

 	double MonOrg = 0; 
    try {
     String StrMonOrg = convObj.getE02CCDMOR().replaceAll(",","");
     StrMonOrg = StrMonOrg.replace(".","");
     MonOrg = Double.parseDouble(StrMonOrg);
     }
    catch(Exception e )
    {
     MonOrg = 0;
    }
     
    if (MonOrg!=0)
      MonOrg = MonOrg / 10000;   
     %>
	
				<td nowrap align="right"><%= Util.formatCCY(String.valueOf(MonOrg))%></td>
				<td nowrap align="right"><%= Util.formatCCY(String.valueOf(MonUS))%></td>				
			</tr>
			<%
			    }
				}
			%>			
		</TABLE>
		</TD>
	</TR>
</TABLE>
<br>
<br>
<TABLE  class="tableinfo" width="100%"cellpadding=0 cellspacing=0>
	<TR>
		<TD>
		 <table cellpadding=0 cellspacing=0 width="100%" border=1>
		 		<TR id="trdark">
				<Td width="50%" ALIGN="left" nowrap><b> </b></Td>
				<Td width="50%" ALIGN="left"><IMG src="<%=request.getContextPath()%>/images/logo1_coopeuch.JPG" align="left" WIDTH=136 HEIGHT=46></Td>
				</TR>
		 		<TR id="trdark">
				<Td width="50%" ALIGN="left" nowrap><b>EMISOR</b></Td>
				<Td width="50%" ALIGN="left"><b>CLIENTE</b></Td>
				</TR>				
				<TR id="trdark">
				<Td width="50%" ALIGN=CENTER nowrap><b>COMPROBANTE DE PAGO</b></Td>
				<Td width="50%" ALIGN=CENTER nowrap><b>COMPROBANTE DE PAGO</b></Td>
				</TR>
          </table>  
           <table cellpadding=0 cellspacing=0 width="100%" border=1 frame="box">
				<TR id="trdark">
				<Td width="25%" ALIGN="left" nowrap><b>NOMBRE </b><br><%= cchData.getE01CCHNOM()%></Td>
				<Td width="25%" ALIGN="left" nowrap><b>NUMERO DE CUENTA</b><br><%= cchData.getE01CCHNXN()%> </Td>
				<Td width="25%" ALIGN="left" nowrap><b>NOMBRE</b><br><%= cchData.getE01CCHNOM()%></Td>
				<Td width="25%" ALIGN="left" nowrap><b>NUMERO DE CUENTA</b><br><%= cchData.getE01CCHNXN()%> </Td>
				</TR>
				<TR id="trdark">
				<Td width="25%" ALIGN="left" nowrap><b>PAGAR HASTA</b> <br><%=cchData.getE01CCHVTD()%>/<%=cchData.getE01CCHVTM()%>/<%=cchData.getE01CCHVTA()%></Td>
				<Td width="25%" ALIGN="left" nowrap><b>MONTO FACTURADO</b><br>US$<%= Util.formatCCY(cchData.getE01CCHDTD())%> </Td>
				<Td width="25%" ALIGN="left" nowrap><b>PAGAR HASTA</b><br> <%=cchData.getE01CCHVTD()%>/<%=cchData.getE01CCHVTM()%>/<%=cchData.getE01CCHVTA()%></Td>
				<Td width="25%" ALIGN="left" nowrap><b>MONTO FACTURADO</b><br>US$<%= Util.formatCCY(cchData.getE01CCHDTD())%> </Td>
				</TR>	
				<TR id="trdark">
				<Td width="25%" ALIGN="left" nowrap><b>MONTO CANCELADO</b><br>US$</Td>
				<Td width="25%" ALIGN="left" nowrap><b> </b><br>  </Td>
				<Td width="25%" ALIGN="left" nowrap><b>MONTO CANCELADO</b><br>US$</Td>
				<Td width="25%" ALIGN="left" nowrap><b> </b><br> </Td>
				</TR>			         
		</TABLE>
           <table cellpadding=0 cellspacing=0 width="100%" border=0>
				<TR id="trdark">
				<Td width="25%" ALIGN="left" nowrap><b>Cheque _____ </b></Td>
				<Td width="25%" ALIGN="left" nowrap><b>Efectivo _____ </b></Td>
				<Td width="25%" ALIGN="left" nowrap><b>Cheque _____ </b></Td>
				<Td width="25%" ALIGN="left" nowrap><b>Efectivo _____ </b></Td>
				</TR>		         
		</TABLE>		
    </TD>
</TR>
</TABLE>

</form>
</body>
</html>
