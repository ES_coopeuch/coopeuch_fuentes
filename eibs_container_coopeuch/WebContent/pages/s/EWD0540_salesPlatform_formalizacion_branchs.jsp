<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@page import="datapro.eibs.beans.EWD0540DSMessage"%><HTML>
<HEAD>
<META HTTP-EQUIV="Pragma" CONTENT="No-cache">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<TITLE>Covenat Form Help</TITLE>

<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "helpList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "helpBean" class= "datapro.eibs.beans.EWD0540DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT language="JavaScript">
	setTimeout("top.close()", <%= datapro.eibs.master.JSEIBSProp.getPopUpTimeOut() %>)
</SCRIPT>

<script language="javascript">

//<!-- Hide from old browsers
function selectCodes(code1) {
	var form = top.opener.document.forms[0];
	form[top.opener.fieldName].value = code1;  	   
	top.close();
 }
 
 function selectCodes1(code1,cbc1,dbc1,cbc2,dbc2) {
	var form = top.opener.document.forms[0];
	form[top.opener.fieldName].value = code1;
	form[top.opener.fieldAux2].value = cbc1;
	form[top.opener.fieldAux3].value = dbc1;
	form[top.opener.fieldAux4].value = cbc2;
	form[top.opener.fieldAux5].value = dbc2;
	//textos
	top.opener.document.getElementById(top.opener.fieldDesc).innerText = cbc1 +"-"+ dbc1;	   
	top.opener.document.getElementById(top.opener.fieldAux1).innerText = cbc2 +"-"+ dbc2;	
	top.close();
 }
 
//-->
</script>
</HEAD>
<BODY>
 <h4>Consulta de Bancos Por Sucursal</h4>

<% if (helpList.isEmpty()){%>
<TABLE class="tbenter" width=100% height=50%>
	<TR>
		<TD>
		<div align="center">
		<font size="3"><b> 
			No Existen Bancos asociados a sucursales. 
		</b></font></div>
		</TD>
	</TR>
</TABLE>

<%} else { %>
			 
  <TABLE  id="mainTable" class="tableinfo" ALIGN=CENTER width="100%">
		   <TR id="trdark">
  					<TH ALIGN=CENTER width="15%">Sucursal</TH> 
  					<TH ALIGN=CENTER width="25%">Nombre Sucursal</TH> 
  					<TH ALIGN=CENTER width="30%">Banco 1</TH>
  					<TH ALIGN=CENTER width="30%">Banco 2</TH>
      		   </TR>
			<%	                  	
	
				while (helpList.getNextRow()) {
				
					helpBean = (EWD0540DSMessage)helpList.getRecord();
					out.println("<tr>");
					out.println("<td ALIGN='center'><A HREF=\"javascript:selectCodes1('" + helpBean.getEWDBRN() +"','"+ helpBean.getEWDBC1() +"','"+ helpBean.getEWDBD1() +"','"+ helpBean.getEWDBC2() +"','"+ helpBean.getEWDBD2() +"')\">"  + helpBean.getEWDBRN() + " </a></td>");
					out.println("<td ALIGN='left'>  <A HREF=\"javascript:selectCodes1('" + helpBean.getEWDBRN() +"','"+ helpBean.getEWDBC1() +"','"+ helpBean.getEWDBD1() +"','"+ helpBean.getEWDBC2() +"','"+ helpBean.getEWDBD2() +"')\">"  + helpBean.getEWDNME() + " </a></td>");
					out.println("<td ALIGN='left'>  <A HREF=\"javascript:selectCodes1('" + helpBean.getEWDBRN() +"','"+ helpBean.getEWDBC1() +"','"+ helpBean.getEWDBD1() +"','"+ helpBean.getEWDBC2() +"','"+ helpBean.getEWDBD2() +"')\">"  + helpBean.getEWDBC1() + "-" + helpBean.getEWDBD1() + " </a></td>");
					out.println("<td ALIGN='left'>  <A HREF=\"javascript:selectCodes1('" + helpBean.getEWDBRN() +"','"+ helpBean.getEWDBC1() +"','"+ helpBean.getEWDBD1() +"','"+ helpBean.getEWDBC2() +"','"+ helpBean.getEWDBD2() +"')\">"  + helpBean.getEWDBC2() + "-" + helpBean.getEWDBD2() + " </a></td>");
					out.println("</tr>");					
				}
				
			}

			%>
</TABLE>

</BODY>
</HTML>
