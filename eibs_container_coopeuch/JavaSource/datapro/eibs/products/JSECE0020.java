package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Modificado por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  08/06/2012                                                                                      **/
/**  Objetivo                :  Servicio para Generacion y Emision de certificados de                                           **/
/**                             - Certificado de Deuda                                                                          **/
/*********************************************************************************************************************************/

import java.io.*;
import java.net.*;
import java.beans.Beans;
import javax.servlet.*;
import javax.servlet.http.*;

import datapro.eibs.beans.*;

import datapro.eibs.master.Util;
 
import datapro.eibs.sockets.*;
import java.util.Hashtable;
import java.util.ArrayList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.PropertyResourceBundle;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generic.beanutil.BeanList;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;

import datapro.eibs.beans.ECIF03001Message;
import datapro.eibs.beans.ECIF03002Message;
import datapro.eibs.beans.EDD009001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.sockets.DecimalField;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

import datapro.eibs.generic.SimpleAthenticator;
public class JSECE0020 extends datapro.eibs.master.SuperServlet {


	protected static final int R_ENTER_ID	 	 = 100;
	protected static final int R_LIST            = 1;
	protected static final int A_LIST            = 2;
	private String LangPath = "S";

/**
 * JSECE0020 constructor comment.
 */
public JSECE0020() {
	super();
}
/**
 * 
 */
public void destroy() {

	flexLog("free resources used by JSECE0020");
	
}
/**
 * 
 */
public void init(ServletConfig config) throws ServletException {
	super.init(config);
}

/**
 * This method was created in VisualAge.
 */
protected void procReqEnterID(
		ESS0030DSMessage user, 
		HttpServletRequest req, 
		HttpServletResponse res, 
		HttpSession ses)
		throws ServletException, IOException {

	ESD008001Message msgClient = null;
	ELEERRMessage msgError = null;
	UserPos	userPO = null;	

	try {

		msgClient = new datapro.eibs.beans.ESD008001Message(); 
		msgError = new datapro.eibs.beans.ELEERRMessage();
		userPO = new datapro.eibs.beans.UserPos(); 
		userPO.setOption("CLIENT");
		userPO.setPurpose("SOL");
		userPO.setRedirect("/servlet/datapro.eibs.products.JSECE0020?SCREEN=1");
		ses.setAttribute("client", msgClient);
		ses.setAttribute("error", msgError);
		ses.setAttribute("userPO", userPO);

  	} catch (Exception ex) {
		flexLog("Error: " + ex); 
  	}

	try {
		flexLog("About to call Page: " + LangPath + "ESD0080_client_both_enter.jsp");
		callPage(LangPath + "ESD0080_client_both_enter.jsp", req, res);
	}
	catch (Exception e) {
		e.printStackTrace();
		flexLog("Exception calling page " + e);
	}

}

protected void procReqCerConMPRD(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ECE002001Message msgList = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;
		try {
			msgError =
				(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
					getClass().getClassLoader(),
					"datapro.eibs.beans.ELEERRMessage");
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		// Send Initial data
		try {
			msgList = (ECE002001Message) mc.getMessageRecord("ECE002001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ECE0020");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			msgList.setH01OPECOD("0001");
			msgList.setE01SELCUN(req.getParameter("E01CUN"));
			try {
			if (msgList.getE01SELCUN().equals("0") || msgList.getE01SELCUN().equals(""))
			{
				if (userPO.getHeader1().equals("") || userPO.getHeader1().equals("0"))
				{
					msgList.setE01SELCUN(req.getParameter("E01CUN"));
				} 
				else 
				{
					msgList.setE01SELCUN(userPO.getHeader1());
			    }
			}
			} catch (Exception e) {

			}
			flexLog("cliente ..." + msgList.getE01SELCUN());
			flexLog("Cliente para crear = " + req.getParameter("E01CUN"));
			flexLog("userPO = " + userPO.getHeader1());
			msgList.send();
			msgList.destroy();
			flexLog("ECE0020010 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				try {
					msgError =
						(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
							getClass().getClassLoader(),
							"datapro.eibs.beans.ELEERRMessage");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else {
				flexLog("Message " + newmessage.getFormatName() + " received.");				
			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e + newmessage);
			throw new RuntimeException("Socket Communication Error Receiving");
		}

		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ECE002001")) {

				JBObjList beanList = new JBObjList();

				boolean firstTime = true;
				String marker = "";
				String chk = "";
					while (true) {

					msgList = (ECE002001Message) newmessage;
					marker = msgList.getH01FLGMAS();
                    flexLog("Mensaje recibido...." + msgList);
					if (firstTime) {
						userPO.setHeader1(msgList.getE01SELCUN());
						userPO.setHeader2(msgList.getE01CERNAM());
						userPO.setHeader3(msgList.getE01CERIDN());
						userPO.setHeader4(msgList.getE01CERISD());
						userPO.setHeader5(msgList.getE01CERISM());
						userPO.setHeader6(msgList.getE01CERISA());
						firstTime = false;
						chk = "checked";

					} else {
						chk = "";
					}

					if (marker.equals("*")) {
						beanList.setShowNext(false);
						break;
					} else {
						beanList.addRow(msgList);

						if (marker.equals("+")) {
							beanList.setShowNext(true);

							break;
						}
					}
					newmessage = mc.receiveMessage();
				}

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("ECE0020Help", beanList);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						flexLog(
							"About to call Page: "
								+ LangPath
								+ "ECE0020_cer_deudas_socio.jsp");
						callPage(
							LangPath + "ECE0020_cer_deudas_socio.jsp",
							req,
							res);					
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				
				} else {
					try {
						flexLog("About to call Page: " + LangPath + "ESD0080_client_both_enter.jsp");
						callPage(LangPath + "ESD0080_client_both_enter.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}				

			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Data Receiving");
		}

	}
protected void procActionPos(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		UserPos userPO = null;

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		flexLog(".....  = " + userPO.getOption()); 
		int inptOPT = 0;

		inptOPT = Integer.parseInt(req.getParameter("opt"));
        flexLog("Opcion = " + inptOPT);
		switch (inptOPT) {
			case 1 : //Imprimir Certificado
				procReqPDF(mc, user, req, res, ses);
				break;				
			default :
				res.sendRedirect(
					super.srctx
						+ "/servlet/datapro.eibs.products.JSECE0020?SCREEN=1");
				break;
		}
	}


protected void procReqPDF(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
		throws ServletException, IOException {
		DocumentException ex = null;
		ByteArrayOutputStream baosPDF = null;

		boolean ACT = false;
		try {
			baosPDF =
				generatePDFDocumentBytes(
					req,
					this.getServletContext(),
					ses,
					ACT);

				StringBuffer sbFilename = new StringBuffer();
				String fn = com.datapro.generic.tool.Util.getTimestamp().toString();
				fn = Util.replace(fn,":","-");
				fn = Util.replace(fn,".","-");
				sbFilename.append(fn);
				sbFilename.append(".pdf");

				res.setHeader("Cache-Control", "max-age=30");

				res.setContentType("application/pdf");

				StringBuffer sbContentDispValue = new StringBuffer();
				sbContentDispValue.append("inline");
				sbContentDispValue.append("; filename=");
				sbContentDispValue.append(sbFilename);

				res.setHeader(
					"Content-disposition",
					sbContentDispValue.toString());

				res.setContentLength(baosPDF.size());

				ServletOutputStream sos;

				sos = res.getOutputStream();

				baosPDF.writeTo(sos);

				sos.flush();

		} catch (DocumentException dex) {
			res.setContentType("text/html");
			PrintWriter writer = res.getWriter();
			writer.println(
				this.getClass().getName()
					+ " caught an exception: "
					+ dex.getClass().getName()
					+ "<br>");
			writer.println("<pre>");
			dex.printStackTrace(writer);
			writer.println("</pre>");
		} finally {
			if (baosPDF != null) {
				baosPDF.reset();
			}
		}

		return;

	}
protected ByteArrayOutputStream generatePDFDocumentBytes(
		final HttpServletRequest req,
		final ServletContext ctx,
		HttpSession session,
		boolean FLG)
		throws DocumentException 		{
		String TIT1     = "                                           CERTIFICADO DE DEUDAS";
		String detail01 = "      Coopeuch Ltda., certifica que el Sr(a) ";
		String detail02 = "Rut ";
		String detail03 = ", es socio(a) de ";
		String detail04 = "nuestra cooperativa, a contar del ";
		String detail05 = " y posee a la fecha las siguientes deudas:";
		String detail08 = "      Esta informacion es solo referencial a la fecha de emision del presente certificado.";
		String detail11 = " ______________________________________                ";
		String detail12 = "    ";
		String detail13 = "            Coopeuch Ltda.                             ";
	    ECE002001Message msgDoc = null;
		JBObjList bl = (JBObjList) session.getAttribute("ECE0020Help");
		int idx = 0;
		bl.setCurrentRow(idx);
		msgDoc = (ECE002001Message) bl.getRecord();
	    flexLog("Mensaje rescatado de la lista " + "index  " + idx + " " + msgDoc);
	    //Datos generales 
		String selcun = Util.unformatHTML(msgDoc.getE01SELCUN());       //Numero del cliente
		String selidn = Util.unformatHTML(msgDoc.getE01CERIDN()); 
		String nombre = Util.unformatHTML(msgDoc.getE01CERNAM());    //Nombre del Cliente	  
		String nomeje = Util.unformatHTML(msgDoc.getE01CERNUS());    //Nombre del Usuario
		String rut = Util.unformatHTML(msgDoc.getE01CERIDN());       //Rut del Cliente
        String mantisa= "";
        String digito="";
        String formattedRUT="";
        String formattedMONTO="";
        
		if (rut.length() > 0)
			{ 
	        mantisa=rut.substring(0,rut.length()-1);
	        digito=rut.substring(rut.length()-1);
	        DecimalFormat nf = new DecimalFormat("##,###,###"); 
	        formattedRUT=nf.format(Integer.valueOf(mantisa))+"-"+digito;
			}
		
		Document doc = new Document(PageSize.A4, 36, 36, 36, 36);           //setaer tamano de hoja

		ByteArrayOutputStream baosPDF = new ByteArrayOutputStream();
		PdfWriter docWriter = null;

		try {
			docWriter = PdfWriter.getInstance(doc, baosPDF);

			if (FLG) {
				docWriter.setEncryption(
					PdfWriter.STRENGTH128BITS,
					selcun,
					selidn,
					PdfWriter.AllowCopy | PdfWriter.AllowPrinting);
			}

			doc.addAuthor("eIBS");
			doc.addCreationDate();
			doc.addProducer();
			doc.addCreator(msgDoc.getE01SELCUN());
			doc.addKeywords("pdf, itext, Java, open source, http");

			Font normalFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL);
			Font normalBoldFont = FontFactory.getFont(FontFactory.COURIER, 8, Font.BOLD);
			Font normalBoldFont5 = FontFactory.getFont(FontFactory.COURIER, 5, Font.BOLD);
			Font normalBoldFont6 = FontFactory.getFont(FontFactory.COURIER, 6, Font.BOLD);
			Font normalBoldUnderFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD | Font.UNDERLINE);
			Font headerFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL);
			Font headerBoldFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD);
			Font headerBoldFont7 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);
			Font headerBoldUnderFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD | Font.UNDERLINE);

			Paragraph RAYA =
				    new Paragraph("____________________________________________________________________________________________________________",
					normalBoldFont);
			Paragraph BLANK = new Paragraph(" ", headerBoldFont);
			//Header principal
			Paragraph HEADER00  = new Paragraph("                                          " + TIT1, headerBoldFont);
			//Header secundarios de Productos CREDITOS
			Paragraph HEADER10  = new Paragraph("CREDITOS DE CONSUMO", headerBoldFont);
			Paragraph HEADER10A = new Paragraph("Numero Prestamo", headerBoldFont7);
			Paragraph HEADER10B = new Paragraph("Fecha Otorgamiento", headerBoldFont7);
			Paragraph HEADER10C = new Paragraph("Tipo Deuda", headerBoldFont7);	
			Paragraph HEADER10D = new Paragraph(" Estado", headerBoldFont7);
			Paragraph HEADER10E = new Paragraph("Cuotas Pactadas", headerBoldFont7);
			Paragraph HEADER10F = new Paragraph("Cuotas Pagadas", headerBoldFont7);
			Paragraph HEADER10G = new Paragraph("Monto Otorgado", headerBoldFont7);
			Paragraph HEADER10H = new Paragraph("Saldo Deuda", headerBoldFont7);	
			Paragraph HEADER10I = new Paragraph("Monto Cuota", headerBoldFont7);	
			//Header secundarios de Productos CREDITOS ESTUDIOS SUPERIORES
			Paragraph HEADER11  = new Paragraph("CREDITOS ESTUDIOS SUPERIORES", headerBoldFont);
			Paragraph HEADER11A = new Paragraph("Numero Prest.", headerBoldFont7);
			Paragraph HEADER11B = new Paragraph("Fecha Otorgmto", headerBoldFont7);
			Paragraph HEADER11C = new Paragraph("Estado", headerBoldFont7);	
			Paragraph HEADER11D = new Paragraph("Cuotas Pactadas", headerBoldFont7);
			Paragraph HEADER11E = new Paragraph("Cuotas Pagadas", headerBoldFont7);
			Paragraph HEADER11F = new Paragraph("Monto Otorgado UF", headerBoldFont7);
			Paragraph HEADER11G = new Paragraph("Saldo Deuda UF", headerBoldFont7);
			Paragraph HEADER11H = new Paragraph("Monto Cuota UF Tramo1", headerBoldFont7);	
			Paragraph HEADER11I = new Paragraph("Monto Cuota UF Tramo2", headerBoldFont7);		
			Paragraph HEADER11J = new Paragraph("Monto Otorgado  $", headerBoldFont7);
			Paragraph HEADER11K = new Paragraph("Saldo Deuda     $", headerBoldFont7);
			Paragraph HEADER11L = new Paragraph("Monto Cuota $ Tramo1", headerBoldFont7);
			Paragraph HEADER11M = new Paragraph("Monto Cuota $ Tramo2", headerBoldFont7);
			//Header secundarios de Productos CREDITOS COMERCIALES
			Paragraph HEADER12  = new Paragraph("CREDITOS COMERCIALES", headerBoldFont);
			Paragraph HEADER12A = new Paragraph("Numero Prestamo", headerBoldFont7);
			Paragraph HEADER12B = new Paragraph("Fecha Otorgamiento", headerBoldFont7);
			Paragraph HEADER12C = new Paragraph("Estado", headerBoldFont7);
			Paragraph HEADER12D = new Paragraph("Cuotas Pactadas", headerBoldFont7);
			Paragraph HEADER12E = new Paragraph("Cuotas Pagadas", headerBoldFont7);
			Paragraph HEADER12F = new Paragraph("Monto Otorgado", headerBoldFont7);
			Paragraph HEADER12G = new Paragraph("Saldo Deuda", headerBoldFont7);	
			Paragraph HEADER12H = new Paragraph("Monto Cuota", headerBoldFont7);	
			//Header secundarios de Productos CREDITOS HIPOTECARIOS 
			Paragraph HEADER13  = new Paragraph("CREDITOS HIPOTECARIOS", headerBoldFont);
			Paragraph HEADER13A = new Paragraph("Numero Prestamo", headerBoldFont7);
			Paragraph HEADER13B = new Paragraph("Fecha Otorgamiento", headerBoldFont7);
			Paragraph HEADER13C = new Paragraph("Estado", headerBoldFont7);
			Paragraph HEADER13D = new Paragraph("Cuotas Pactadas", headerBoldFont7);
			Paragraph HEADER13E = new Paragraph("Cuotas Pagadas", headerBoldFont7);
			Paragraph HEADER13F = new Paragraph("Monto Otorgado UF", headerBoldFont7);
			Paragraph HEADER13G = new Paragraph("Saldo Deuda UF", headerBoldFont7);	
			Paragraph HEADER13H = new Paragraph("Monto Cuota UF", headerBoldFont7);			
			//Header secundarios de Productos TARJETAS DE CREDITOS 
			Paragraph HEADER94  = new Paragraph("TARJETAS DE CREDITOS", headerBoldFont);
			Paragraph HEADER94A = new Paragraph("Tipo de Tarjeta", headerBoldFont7);
			Paragraph HEADER94B = new Paragraph("Cupo Nacional $", headerBoldFont7);
			Paragraph HEADER94C = new Paragraph("Monto Utilizado $", headerBoldFont7);
			Paragraph HEADER94D = new Paragraph("Cupo Internacional US$", headerBoldFont7);
			Paragraph HEADER94E = new Paragraph("Monto Utilizado US$", headerBoldFont7);
			
			Paragraph DETAIL01 = new Paragraph(detail01 + nombre + " " + detail02 + formattedRUT + detail03, normalBoldFont);			
			Paragraph DETAIL03 = new Paragraph(detail04 + msgDoc.getE01CERISD() + " de " + msgDoc.getD01CERISM() + " de " + msgDoc.getE01CERISA() + detail05, normalBoldFont);
			
			Paragraph DETAIL08 = new Paragraph(detail08, normalBoldFont);
			Paragraph DETAIL11 = new Paragraph(detail11, normalBoldFont);
			Paragraph DETAIL12 = new Paragraph(msgDoc.getE01CERNUS() + "                              ", normalBoldFont);
			Paragraph DETAIL13 = new Paragraph(msgDoc.getE01CERUCI() + "                              ", normalBoldFont);
			Paragraph DETAIL14 = new Paragraph(detail13, normalBoldFont);
			Paragraph DETAIL15 = new Paragraph(msgDoc.getE01CERUCI()+", " + msgDoc.getE01CERCDD() + " de " + msgDoc.getD01CERCMM() + " de " + msgDoc.getE01CERCAA(), normalBoldFont);
			Paragraph TOTAL = new Paragraph("TOTALES", headerBoldFont7);
			doc.open();

			Table table = new Table(1, 20);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);
			
			Cell cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
		
			cell = new Cell(HEADER00);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			table.addCell(cell);
			table.addCell(cell);
			
			cell = new Cell(DETAIL01);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
						
			cell = new Cell(DETAIL03);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);		
						
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);	
			doc.add(table);                                     //Agrego Lineas a la tabla
			
			//Creditos Consumo--------------------------------------------------------------------------------
			table = new Table(2, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);	
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell = new Cell(HEADER10);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);                                     //Agrego Lineas a la tabla

			table = new Table(9, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);		
			cell = new Cell(HEADER10A);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER10B);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER10C);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER10D);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER10E);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER10F);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER10G);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER10H);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER10I);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
 
	        DecimalFormat nf = new DecimalFormat("##,###,###"); 
	        DecimalFormat nuf = new DecimalFormat("##,###.###"); 
            String tipodeu = " ";
			int prod = 0;
			long suma1 = 0;
			long suma2 = 0;
			long suma3 = 0;
			String estado = " ";
            bl.initRow();
	        while (bl.getNextRow()) {
	        	    msgDoc = (datapro.eibs.beans.ECE002001Message) bl.getRecord();
	                if (msgDoc.getE01CERACD().equals("10") && (msgDoc.getE01CERTCR().equals("1"))){
                        prod = prod + 1;
                        if (msgDoc.getE01CERSTS().equals("1"))
                          	estado = "Vigente";
                        else  if (msgDoc.getE01CERSTS().equals("2"))
                           	estado = "Vencida";
                         else  if (msgDoc.getE01CERSTS().equals("3"))
                           	estado = "Castigada";
                         else  if (msgDoc.getE01CERSTS().equals("4"))
                            	estado = "Castigada no Inf.";
                         else  if (msgDoc.getE01CERSTS().equals("5"))
                            	estado = "Avenimiento";
                        if (msgDoc.getE01CERTDE().equals("R"))
                          	tipodeu = "Directa";
                         else  
                           	tipodeu = "Indirecta";
	        			Paragraph DETAIL10A = new Paragraph(msgDoc.getE01CERCTA(), normalBoldFont6);
	        			Paragraph DETAIL10B = new Paragraph(msgDoc.getE01CERSDD()+ "/"+ msgDoc.getE01CERSMM()+ "/"+ msgDoc.getE01CERSAA(), normalBoldFont6);
	        			Paragraph DETAIL10C = new Paragraph(tipodeu, normalBoldFont6);
	        			Paragraph DETAIL10D = new Paragraph(estado, normalBoldFont6);
	        			Paragraph DETAIL10E = new Paragraph(msgDoc.getE01CERCIN(), normalBoldFont6);
	        			Paragraph DETAIL10F = new Paragraph(msgDoc.getE01CERCPA(), normalBoldFont6);
	        			Paragraph DETAIL10G = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERMTO())), normalBoldFont6);
	        			Paragraph DETAIL10H = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERSDO())), normalBoldFont6);
	        			Paragraph DETAIL10I = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERT1$())), normalBoldFont6);
	        			suma1 = suma1 + Long.parseLong(msgDoc.getE01CERMTO());
	        			suma2 = suma2 + Long.parseLong(msgDoc.getE01CERSDO());
	        			suma3 = suma3 + Long.parseLong(msgDoc.getE01CERT1$());
	        			cell = new Cell(DETAIL10A);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL10B);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL10C);
	        			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL10D);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL10E);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL10F);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL10G);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL10H);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL10I);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);	        			
	                }
	        }
			if (prod == 0)
				{
				doc.add(table); 
				table = new Table(1, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
    			Paragraph DETAIL10A = new Paragraph("No Tiene Producto Vigente", normalBoldFont);
    			cell = new Cell(DETAIL10A);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
				doc.add(table);
    			}
			else
			{
				doc.add(table);
				table = new Table(1, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
				cell = new Cell(RAYA);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				doc.add(table);
				table = new Table(9, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
				cell = new Cell(TOTAL);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				cell = new Cell(BLANK);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				table.addCell(cell);
				table.addCell(cell);
				table.addCell(cell);
				table.addCell(cell);
    			Paragraph DETAIL10G = new Paragraph(nf.format(suma1), normalBoldFont6);
    			Paragraph DETAIL10H = new Paragraph(nf.format(suma2), normalBoldFont6);
    			Paragraph DETAIL10I = new Paragraph(nf.format(suma3), normalBoldFont6);
    			cell = new Cell(DETAIL10G);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL10H);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL10I);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
				doc.add(table);
			}

			table = new Table(1, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);
			//Creditos Estudios Superiores--------------------------------------------------------------------------------
			table = new Table(2, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);	
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell = new Cell(HEADER11);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);                                     //Agrego Lineas a la tabla

			table = new Table(13, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);		
			cell = new Cell(HEADER11A);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11B);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11C);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11D);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11E);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11F);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11G);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11H);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11I);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11J);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11K);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11L);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER11M);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
            tipodeu = " ";
			prod = 0;
			estado = " ";
			float suma11 = 0;
			float suma22 = 0;
			float suma33 = 0;
			float suma44 = 0;
			suma1 = 0;
			suma2 = 0;
			suma3 = 0;
			long suma4 = 0;
            bl.initRow();
	        while (bl.getNextRow()) {
	        	    msgDoc = (datapro.eibs.beans.ECE002001Message) bl.getRecord();
	                if (msgDoc.getE01CERACD().equals("10") && (msgDoc.getE01CERTCR().equals("2"))){
                        prod = prod + 1;
                        if (msgDoc.getE01CERSTS().equals("1"))
                          	estado = "Vigente";
                        else  if (msgDoc.getE01CERSTS().equals("2"))
                           	estado = "Vencida";
                         else  if (msgDoc.getE01CERSTS().equals("3"))
                           	estado = "Castigada";
                         else  if (msgDoc.getE01CERSTS().equals("4"))
                            	estado = "Castigada no Inf.";
                         else  if (msgDoc.getE01CERSTS().equals("5"))
                            	estado = "Avenimiento";
	        			Paragraph DETAIL11A = new Paragraph(msgDoc.getE01CERCTA(), normalBoldFont6);
	        			Paragraph DETAIL11B = new Paragraph(msgDoc.getE01CERSDD()+ "/"+ msgDoc.getE01CERSMM()+ "/"+ msgDoc.getE01CERSAA(), normalBoldFont6);
	        			Paragraph DETAIL11C = new Paragraph(estado, normalBoldFont6);
	        			Paragraph DETAIL11D = new Paragraph(msgDoc.getE01CERCIN(), normalBoldFont6);
	        			Paragraph DETAIL11E = new Paragraph(msgDoc.getE01CERCPA(), normalBoldFont6);
	        			Paragraph DETAIL11F = new Paragraph(msgDoc.getE01CERMTU(), normalBoldFont6);
	        			Paragraph DETAIL11G = new Paragraph(msgDoc.getE01CERSDU(), normalBoldFont6);
	        			Paragraph DETAIL11H = new Paragraph(msgDoc.getE01CERT1U(), normalBoldFont6);
	        			Paragraph DETAIL11I = new Paragraph(msgDoc.getE01CERT2U(), normalBoldFont6);	
	        			Paragraph DETAIL11J = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERMTO())), normalBoldFont6);
	        			Paragraph DETAIL11K = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERSDO())), normalBoldFont6);
	        			Paragraph DETAIL11L = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERT1$())), normalBoldFont6);
	        			Paragraph DETAIL11M = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERT2$())), normalBoldFont6);
	        			suma11 = suma11 + Float.parseFloat(msgDoc.getE01CERMTU().replaceAll(",","."));
	        			suma22 = suma22 + Float.parseFloat(msgDoc.getE01CERSDU().replaceAll(",","."));
	        			suma33 = suma33 + Float.parseFloat(msgDoc.getE01CERT1U().replaceAll(",","."));
	        			suma44 = suma44 + Float.parseFloat(msgDoc.getE01CERT2U().replaceAll(",","."));
	        			suma1 = suma1 + Long.parseLong(msgDoc.getE01CERMTO());
	        			suma2 = suma2 + Long.parseLong(msgDoc.getE01CERSDO());
	        			suma3 = suma3 + Long.parseLong(msgDoc.getE01CERT1$());
	        			suma4 = suma4 + Long.parseLong(msgDoc.getE01CERT2$());
	        			cell = new Cell(DETAIL11A);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11B);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11C);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11D);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11E);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11F);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11G);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11H);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11I);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11J);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11K);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11L);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL11M);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	                }
	        }
			if (prod == 0)
				{
				doc.add(table); 
				table = new Table(1, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
    			Paragraph DETAIL10A = new Paragraph("No Tiene Producto Vigente", normalBoldFont);
    			cell = new Cell(DETAIL10A);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
				doc.add(table);
    			}
			else
			{
				doc.add(table);
				table = new Table(1, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
				cell = new Cell(RAYA);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				doc.add(table);
				table = new Table(13, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
				cell = new Cell(TOTAL);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				cell = new Cell(BLANK);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				table.addCell(cell);
				table.addCell(cell);
				table.addCell(cell);
    			Paragraph DETAIL11F = new Paragraph(nuf.format(suma11), normalBoldFont6);
    			Paragraph DETAIL11G = new Paragraph(nuf.format(suma22), normalBoldFont6);
    			Paragraph DETAIL11H = new Paragraph(nuf.format(suma33), normalBoldFont6);
    			Paragraph DETAIL11I = new Paragraph(nuf.format(suma44), normalBoldFont6);
    			Paragraph DETAIL11J = new Paragraph(nf.format(suma1), normalBoldFont6);
    			Paragraph DETAIL11K = new Paragraph(nf.format(suma2), normalBoldFont6);
    			Paragraph DETAIL11L = new Paragraph(nf.format(suma3), normalBoldFont6);
    			Paragraph DETAIL11M = new Paragraph(nf.format(suma4), normalBoldFont6);
    			cell = new Cell(DETAIL11F);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL11G);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL11H);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL11I);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL11J);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL11K);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL11L);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL11M);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
				doc.add(table);
			}
			table = new Table(1, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);

			//Creditos Comerciales--------------------------------------------------------------------------------
			table = new Table(2, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);	
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell = new Cell(HEADER12);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);                                     //Agrego Lineas a la tabla

			table = new Table(8, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);		
			cell = new Cell(HEADER12A);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER12B);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER12C);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER12D);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER12E);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER12F);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER12G);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER12H);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
            tipodeu = " ";
			prod = 0;
			estado = " ";
			suma1 = 0;
			suma2 = 0;
			suma3 = 0;
            bl.initRow();
	        while (bl.getNextRow()) {
	        	    msgDoc = (datapro.eibs.beans.ECE002001Message) bl.getRecord();
	                if (msgDoc.getE01CERACD().equals("10") && (msgDoc.getE01CERTCR().equals("3"))){
                        prod = prod + 1;
                        if (msgDoc.getE01CERSTS().equals("1"))
                          	estado = "Vigente";
                        else  if (msgDoc.getE01CERSTS().equals("2"))
                           	estado = "Vencida";
                         else  if (msgDoc.getE01CERSTS().equals("3"))
                           	estado = "Castigada";
                         else  if (msgDoc.getE01CERSTS().equals("4"))
                            	estado = "Castigada no Inf.";
                         else  if (msgDoc.getE01CERSTS().equals("5"))
                            	estado = "Avenimiento";
	        			Paragraph DETAIL12A = new Paragraph(msgDoc.getE01CERCTA(), normalBoldFont6);
	        			Paragraph DETAIL12B = new Paragraph(msgDoc.getE01CERSDD()+ "/"+ msgDoc.getE01CERSMM()+ "/"+ msgDoc.getE01CERSAA(), normalBoldFont6);
	        			Paragraph DETAIL12C = new Paragraph(estado, normalBoldFont6);
	        			Paragraph DETAIL12D = new Paragraph(msgDoc.getE01CERCIN(), normalBoldFont6);
	        			Paragraph DETAIL12E = new Paragraph(msgDoc.getE01CERCPA(), normalBoldFont6);
	        			Paragraph DETAIL12F = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERMTO())), normalBoldFont6);
	        			Paragraph DETAIL12G = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERSDO())), normalBoldFont6);
	        			Paragraph DETAIL12H = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERT1$())), normalBoldFont6);
	        			suma1 = suma1 + Long.parseLong(msgDoc.getE01CERMTO());
	        			suma2 = suma2 + Long.parseLong(msgDoc.getE01CERSDO());
	        			suma3 = suma3 + Long.parseLong(msgDoc.getE01CERT1$());
	        			cell = new Cell(DETAIL12A);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL12B);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL12C);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL12D);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL12E);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL12F);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL12G);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL12H);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);	        			
	                }
	        }
			if (prod == 0)
				{
				doc.add(table); 
				table = new Table(1, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
    			Paragraph DETAIL10A = new Paragraph("No Tiene Producto Vigente", normalBoldFont);
    			cell = new Cell(DETAIL10A);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
				doc.add(table);
    			}
			else
			{
				doc.add(table);
				table = new Table(1, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
				cell = new Cell(RAYA);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				doc.add(table);
				table = new Table(8, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
				cell = new Cell(TOTAL);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				cell = new Cell(BLANK);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				table.addCell(cell);
				table.addCell(cell);
				table.addCell(cell);
    			Paragraph DETAIL12F = new Paragraph(nf.format(suma1), normalBoldFont6);
    			Paragraph DETAIL12G = new Paragraph(nf.format(suma2), normalBoldFont6);
    			Paragraph DETAIL12H = new Paragraph(nf.format(suma3), normalBoldFont6);
    			cell = new Cell(DETAIL12F);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL12G);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL12H);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
				doc.add(table);
			}
			table = new Table(1, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);

			//Cuenta Creditos Hipotecarios --------------------------------------------------------------------------------
			table = new Table(2, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);	
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell = new Cell(HEADER13);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);                                     //Agrego Lineas a la tabla
			
			table = new Table(8, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);		
			cell = new Cell(HEADER13A);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER13B);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER13C);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER13D);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER13E);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER13F);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER13G);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER13H);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
            prod = 0;
            suma11 = 0;
            suma22 = 0;
            suma33 = 0;
            bl.initRow();
	        while (bl.getNextRow()) {
	        	    msgDoc = (datapro.eibs.beans.ECE002001Message) bl.getRecord();
	                if (msgDoc.getE01CERACD().equals("13")){
                        prod = prod + 1;
                        if (msgDoc.getE01CERSTS().equals("A"))
                          	estado = "Activo";
                        else  
                           	estado = "Inactivo";
	        			Paragraph DETAIL13A = new Paragraph(msgDoc.getE01CERCTA(), normalBoldFont6);
	        			Paragraph DETAIL13B = new Paragraph(msgDoc.getE01CERSDD()+ "/"+ msgDoc.getE01CERSMM()+ "/"+ msgDoc.getE01CERSAA(), normalBoldFont6);
	        			Paragraph DETAIL13C = new Paragraph(estado, normalBoldFont6);
	        			Paragraph DETAIL13D = new Paragraph(msgDoc.getE01CERCIN(), normalBoldFont6);
	        			Paragraph DETAIL13E = new Paragraph(msgDoc.getE01CERCPA(), normalBoldFont6);
	        			Paragraph DETAIL13F = new Paragraph(msgDoc.getE01CERMTU(), normalBoldFont6);
	        			Paragraph DETAIL13G = new Paragraph(msgDoc.getE01CERSDU(), normalBoldFont6);
	        			Paragraph DETAIL13H = new Paragraph(msgDoc.getE01CERT1U(), normalBoldFont6);
	        			suma11 = suma11 + Float.parseFloat(msgDoc.getE01CERMTU().replaceAll(",","."));
	        			suma22 = suma22 + Float.parseFloat(msgDoc.getE01CERSDU().replaceAll(",","."));
	        			suma33 = suma33 + Float.parseFloat(msgDoc.getE01CERT1U().replaceAll(",","."));
	        			cell = new Cell(DETAIL13A);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL13B);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL13C);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL13D);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL13E);
	        			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL13F);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL13G);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL13H);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	                }
	        }
			if (prod == 0)
				{
				doc.add(table);
				table = new Table(1, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
    			Paragraph DETAIL13A = new Paragraph("No Tiene Producto Vigente", normalBoldFont);
    			cell = new Cell(DETAIL13A);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
				doc.add(table);
    			}
			else
				{
				doc.add(table);
				table = new Table(1, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
				cell = new Cell(RAYA);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				doc.add(table);
				table = new Table(8, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
				cell = new Cell(TOTAL);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				cell = new Cell(BLANK);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				table.addCell(cell);
				table.addCell(cell);
				table.addCell(cell);
    			Paragraph DETAIL13F = new Paragraph(nuf.format(suma11), normalBoldFont6);
    			Paragraph DETAIL13G = new Paragraph(nuf.format(suma22), normalBoldFont6);
    			Paragraph DETAIL13H = new Paragraph(nuf.format(suma33), normalBoldFont6);
    			cell = new Cell(DETAIL13F);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL13G);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL13H);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
				doc.add(table);
			}
			table = new Table(1, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);

			//Tarjetas de Credito --------------------------------------------------------------------------------
			table = new Table(2, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);	
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell = new Cell(HEADER94);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);                                     //Agrego Lineas a la tabla
			
			table = new Table(5, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);		
			cell = new Cell(HEADER94A);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER94B);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER94C);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER94D);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			cell = new Cell(HEADER94E);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell); 
			
            prod = 0;
            suma1 = 0;
            suma2 = 0;
            suma3 = 0;
            suma4 = 0;
            bl.initRow();
	        while (bl.getNextRow()) {
	        	    msgDoc = (datapro.eibs.beans.ECE002001Message) bl.getRecord();
	                if (msgDoc.getE01CERACD().equals("94")){
                        prod = prod + 1;
	        			Paragraph DETAIL94A = new Paragraph(msgDoc.getD01CERTIP(), normalBoldFont6);                        
	        			Paragraph DETAIL94B = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERMTO()))+ "   ", normalBoldFont6);
	        			Paragraph DETAIL94C = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERSDO()))+ "   ", normalBoldFont6);
	        			Paragraph DETAIL94D = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERMTI()))+ "   ", normalBoldFont6);
	        			Paragraph DETAIL94E = new Paragraph(nf.format(Integer.valueOf(msgDoc.getE01CERSDI()))+ "   ", normalBoldFont6);
	        			suma1 = suma1 + Long.parseLong(msgDoc.getE01CERMTO());
	        			suma2 = suma2 + Long.parseLong(msgDoc.getE01CERSDO());
	        			suma3 = suma3 + Long.parseLong(msgDoc.getE01CERMTI());
	        			suma4 = suma4 + Long.parseLong(msgDoc.getE01CERSDI());	        			
	        			cell = new Cell(DETAIL94A);
	        			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL94B);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL94C);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL94D);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	        			cell = new Cell(DETAIL94E);
	        			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        			cell.setBorder(Rectangle.NO_BORDER);
	        			table.addCell(cell);
	                }
	        }
			if (prod == 0)
				{
				doc.add(table);
				table = new Table(1, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
    			Paragraph DETAIL94A = new Paragraph("No Tiene Producto Vigente", normalBoldFont);
    			cell = new Cell(DETAIL94A);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
				doc.add(table);
    			}
			else
				{
				doc.add(table);
				table = new Table(1, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
				cell = new Cell(RAYA);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
				doc.add(table);
				table = new Table(5, 4);
				table.setBorderWidth(0);
				table.setCellsFitPage(true);
				table.setPadding(1);
				table.setSpacing(1);
				table.setWidth(100);
				cell = new Cell(TOTAL);
				cell.setBorder(Rectangle.NO_BORDER);
				table.addCell(cell);
    			Paragraph DETAIL94B = new Paragraph(nf.format(suma1), normalBoldFont6);
    			Paragraph DETAIL94C = new Paragraph(nf.format(suma2), normalBoldFont6);
    			Paragraph DETAIL94D = new Paragraph(nf.format(suma3), normalBoldFont6);
    			Paragraph DETAIL94E = new Paragraph(nf.format(suma4), normalBoldFont6);
    			cell = new Cell(DETAIL94B);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL94C);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL94D);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
    			cell = new Cell(DETAIL94E);
    			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    			cell.setBorder(Rectangle.NO_BORDER);
    			table.addCell(cell);
				doc.add(table);
			}

			table = new Table(1, 4);
			table.setBorderWidth(0);
			table.setCellsFitPage(true);
			table.setPadding(1);
			table.setSpacing(1);
			table.setWidth(100);
			cell = new Cell(BLANK);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);
			
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL08);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
		
			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			table.addCell(cell);
			
			cell = new Cell(DETAIL11);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL12);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new Cell(DETAIL13);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
	
			cell = new Cell(DETAIL14);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);

			cell = new Cell(BLANK);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
				
			cell = new Cell(DETAIL15);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			doc.add(table);
			
			
		} catch (DocumentException dex) {
			baosPDF.reset();
			throw dex;
		} finally {
			if (doc != null) {
				doc.close();
			}
			if (docWriter != null) {
				docWriter.close();
			}
		}

		if (baosPDF.size() < 1) {
			throw new DocumentException(
				"document has " + baosPDF.size() + " bytes");
		}

		return baosPDF;
	}



public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
   
	Socket s = null;
	MessageContext mc = null;

	ESS0030DSMessage msgUser = null;
  	HttpSession session = null;

	session = (HttpSession)req.getSession(false); 
	
	if (session == null) {
		try {
			res.setContentType("text/html");
			printLogInAgain(res.getWriter());
		}
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception ocurred. Exception = " + e);
		}
	}
	else {

		int screen = R_ENTER_ID;
		
		try {
		
			msgUser = (datapro.eibs.beans.ESS0030DSMessage)session.getAttribute("currUser");

			// Here we should get the path from the user profile
			LangPath = super.rootPath + msgUser.getE01LAN() + "/";

			try
			{
				flexLog("Opennig Socket Connection");
				s = new Socket(super.hostIP, getInitSocket(req) + 1);
				s.setSoTimeout(super.sckTimeOut);
			  	mc = new MessageContext(new DataInputStream(new BufferedInputStream(s.getInputStream())),
							      	    new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
									    "datapro.eibs.beans");
			
			try {
				screen = Integer.parseInt(req.getParameter("SCREEN"));
			}
			catch (Exception e) {
				flexLog("Screen set to default value");
			}
		    flexLog("Entre con Screen = " + screen);
			switch (screen) {
	
			    case R_ENTER_ID : 
			    	procReqEnterID(msgUser, req, res, session);
				break;
				case R_LIST :
					procReqCerConMPRD(mc, msgUser, req, res, session);
					break;
				case A_LIST :
					procActionPos(mc, msgUser, req, res, session);
					break;	
				default :
					res.sendRedirect(super.srctx +LangPath + super.devPage);
					break;
			}
			}
			catch (Exception e) {
				e.printStackTrace();
				int sck = getInitSocket(req) + 1;
				flexLog("Socket not Open(Port " + sck + "). Error: " + e);
				res.sendRedirect(super.srctx +LangPath + super.sckNotOpenPage);
			//	return;
			}
			finally {
				s.close();
			}
			

		}
		catch (Exception e) {
			flexLog("Error: " + e);
			res.sendRedirect(super.srctx +LangPath + super.sckNotRespondPage);
		}
		
	}
	
}
protected void showERROR(ELEERRMessage m) {
	if (logType != NONE) {

		flexLog("ERROR received.");

		flexLog("ERROR number:" + m.getERRNUM());
		flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
		flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
		flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
		flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
		flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
		flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
		flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
		flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
		flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
		flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

	}
}

}