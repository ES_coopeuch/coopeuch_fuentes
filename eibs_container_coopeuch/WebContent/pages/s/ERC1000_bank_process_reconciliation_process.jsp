<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Sistema Bancario: Conciliación Bancaria</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "bank" class= "datapro.eibs.beans.ERC100001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

</head>

<body>
 <% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>

<H3 align="center">Cargar  Cartola<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="bank_process_reconciliation_process.jsp, ERC1000"></H3>

<hr size="4">
<p>&nbsp;</p>

<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="banco" value="<%= bank.getE01BRMEID().trim()%> - <%= bank.getE01BRMNME().trim()%> " 
	  			size="70"  readonly>
             </td>    
             
         </tr>
         <tr>
             <td nowrap width="10%" align="right">Cuenta Banco : 
               </td>
             <td nowrap width="50%"align="left">
	  	<input type="text" name="cuenta_banco" value="<%= bank.getE01BRMCTA().trim()%>" size="20" readonly>
             </td>
             
               <td nowrap width="10%" align="right">Cuenta IBS : 
               </td>
             <td nowrap width="50%"align="left">
	  	<input type="text" name="cuenta_ibs" align="left" value="<%= bank.getE01BRMACC().trim()%>" size="20" readonly>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>


  
<br>

<table class="tableinfo" width="100%">
    <tr > 
      <td nowrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">     
		    <tr>
				<div align="center">
  					<h3>
  						<br>La cartola ha sido sometida a proceso de carga. Consulte su estado en Gestión de Cartola<br>
  					</h3>
  				 </div>		    
		      <td > 
		      </td>
		    </tr>
		 </table>
	  </td>
	</tr>
</table>

<br>
</body>
</html>
