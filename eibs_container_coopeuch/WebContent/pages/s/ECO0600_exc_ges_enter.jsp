<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Gesti&oacute;n de Aprobaci&oacute;n Masiva de Ex-Convenios</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="date" class="java.util.Date" scope="session" />      
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">

function BloqDes()
{
 date = new Date();
 var month_ant = date.getMonth();
 var month = date.getMonth()+1;
 var day = date.getDate();
 var year = date.getFullYear();

 if (document.getElementById('E01CMHIDC').value != '')
	{
	document.getElementById('fecha1').value = '0';
	document.getElementById('fecha1').disabled=true;
	document.getElementById('fecha2').value = '0';
	document.getElementById('fecha2').disabled=true;
	document.getElementById('fecha3').value = '0';
	document.getElementById('fecha3').disabled=true;
	document.getElementById('fecha4').value = '0';
	document.getElementById('fecha4').disabled=true;
	document.getElementById('fecha5').value = '0';
	document.getElementById('fecha5').disabled=true;
	document.getElementById('fecha6').value = '0';
	document.getElementById('fecha6').disabled=true;
	document.getElementById('f1').style.visibility = 'hidden';
	document.getElementById('f2').style.visibility = 'hidden';
	document.getElementById('E01CMHSTS').disabled=true; 
	}
	else 
    {
	document.getElementById('fecha1').disabled=false;
	document.getElementById('fecha2').disabled=false;
	document.getElementById('fecha3').disabled=false;
	document.getElementById('fecha4').disabled=false;
	document.getElementById('fecha5').disabled=false;
	document.getElementById('fecha6').disabled=false;
	document.getElementById('fecha1').value = day;
	document.getElementById('fecha2').value = month_ant;
	document.getElementById('fecha3').value = year;
	document.getElementById('fecha4').value = day;
	document.getElementById('fecha5').value = month;
	document.getElementById('fecha6').value = year;

	document.getElementById('E01CMHSTS').disabled=false; 

	document.getElementById('f1').style.visibility = 'visible';
	document.getElementById('f2').style.visibility = 'visible';
	}
	
}

function addDate(){

 date = new Date();
 var month_ant = date.getMonth();
 var month = date.getMonth()+1;
 var day = date.getDate();
 var year = date.getFullYear();

 if (document.getElementById('fecha1').value == '')
	document.getElementById('fecha1').value = day;

 if (document.getElementById('fecha2').value == '')
	document.getElementById('fecha2').value = month_ant;

 if (document.getElementById('fecha3').value == '')
	document.getElementById('fecha3').value = year;

 if (document.getElementById('fecha4').value == '')
	document.getElementById('fecha4').value = day;

 if (document.getElementById('fecha5').value == '')
	document.getElementById('fecha5').value = month;

 if (document.getElementById('fecha6').value == '')
	document.getElementById('fecha6').value = year;

}
</script>
 
 

</head>

<body  onload="addDate();">
 
<H3 align="center">Gesti&oacute;n de Aprobaci&oacute;n Masiva de Ex-Convenios
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="exc_ges_enter.jsp, ECO0600">
</H3>

<hr size="4"> 
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0600" >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="1200">
 
<table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">ID Archivo : 
             </td>
             <td nowrap width="20%" align="left">
	 	         <input type="text" name="E01CMHIDC" size="15" maxlength="14" value="" onKeypress="enterInteger()"  onchange="BloqDes()">
             </td>                 

			<td nowrap width="15%" align="right">Fecha Desde : 
            </td>
            <td nowrap width="20%"align="left">
  				<div align="left" > 
                <input type="text" name="E01CMHFDD" id="fecha1" size="3" maxlength="2" value="" readonly="readonly">
                <input type="text" name="E01CMHFDM" id="fecha2" size="3" maxlength="2" value="" readonly="readonly">
                <input type="text" name="E01CMHFDY" id="fecha3" size="5" maxlength="4" value="" readonly="readonly">
                <a id="f1" style="visibility:visible" href="javascript:DatePicker(document.forms[0].E01CMHFDD,document.forms[0].E01CMHFDM,document.forms[0].E01CMHFDY)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
              </div>
             </td>         

			<td nowrap width="15%" align="right">Fecha Hasta : 
            </td>

            <td nowrap width="20%" align="left" >
  				<div align="left" > 
  			
                <input type="text" name="E01CMHFHD" id="fecha4" size="3" maxlength="2" value="" readonly="readonly">
                <input type="text" name="E01CMHFHM" id="fecha5" size="3" maxlength="2" value="" readonly="readonly">
                <input type="text" name="E01CMHFHY" id="fecha6" size="5" maxlength="4" value="" readonly="readonly">
                <a id="f2" style="visibility:visible" href="javascript:DatePicker(document.forms[0].E01CMHFHD,document.forms[0].E01CMHFHM,document.forms[0].E01CMHFHY)">
                <img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0" >
                </a> 	
              </div>
             </td>         

         </tr>
    
         <tr>
             <td nowrap width="10%" align="right">Estado :
             </td>
             <td nowrap width="20%"align="left">
						<select name="E01CMHSTS">
 		                    <option value="T">TODOS</option>
             				<option value="A" >PENDIENTE</option>
             				<option value="V" >RECHAZADA</option>
             				<option value="P" >PROCESADA</option>
            			</select>
			</td>

            <td nowrap  align="right" colspan="4"></td>
         </tr>
        </table>
      </td>
    </tr>
  </table>

  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
<script language="JavaScript">
  document.forms[0].E01CMHIDC.focus();
  document.forms[0].E01CMHIDC.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
