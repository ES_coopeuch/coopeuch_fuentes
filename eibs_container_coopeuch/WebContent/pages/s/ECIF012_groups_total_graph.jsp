<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import = "datapro.eibs.beans.*" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Grupos Econ�micos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ECIF01201Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

</head>

<body>

 <% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     error.setERRNUM("0");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }

	int row;
	try {
		row = Integer.parseInt(request.getParameter("ROW"));
	} 
	catch (Exception e) {
		row = 0;
	}

	ECIF01201Help.setCurrentRow(row);
	ECIF01201Message grpTotal = (ECIF01201Message) ECIF01201Help.getRecord();
           
%>


<h3 align="center">Resumen de Operaciones por Grupo<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="groups_total_graph.jsp,ECIF012"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECIF010" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" value="6">
  <h4> </h4>
  <table class="tableinfo" width="90%" height="65%">
    <tr> 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0" height="100%">
          <tr id="tbenter">
            <td nowrap colspan=2> 
              <div align="center"> <%
              		if ( !grpTotal.getE01TOTASS().equals("0.00") ) {
					   
              	%> <APPLET archive="eibs_applets.zip" code="datapro.eibs.applets.graph.PieChart.class" width="100%" height="100%" align="absmiddle" codebase="<%=request.getContextPath()%>/pages/s/">
                  <param name=title value="Activos">
                  <param name=showlabel   value="YES">
                  <param name=showpercent value="YES">
                  <param name=bgcolor value="#D1D1D1">
                  <param name=columns value="11">
                  <param name=Plabel1 value="Sobregiros">
                  <param name=Pvalue1 value="<%= Util.parseCCYtoDouble(grpTotal.getP01OVDRFT().trim())%>">
                  <param name=Pcolor1 value="#0066FF">
                  <param name=Plabel2 value="Hipotecarios">
                  <param name=Pvalue2 value="<%= Util.parseCCYtoDouble(grpTotal.getP01LNSMOR().trim())%>">
                  <param name=Pcolor2 value="#CC99FF">
                  <param name=Plabel3 value="Arrendamientos">
                  <param name=Pvalue3 value="<%= Util.parseCCYtoDouble(grpTotal.getP01LNSLSG().trim())%>">
                  <param name=Pcolor3 value="#00CCFF">
                  <param name=Plabel4 value="Consumo">
                  <param name=Pvalue4 value="<%= Util.parseCCYtoDouble(grpTotal.getP01LNSCON().trim())%>">
                  <param name=Pcolor4 value="#00C000">
                  <param name=Plabel5 value="Pr�stamos">
                  <param name=Pvalue5 value="<%= Util.parseCCYtoDouble(grpTotal.getP01LNSGRL().trim())%>">
                  <param name=Pcolor5 value="#00FFFF">
                  <param name=Plabel6 value="Descontadas">
                  <param name=Pvalue6 value="<%= Util.parseCCYtoDouble(grpTotal.getP01DESDOC().trim())%>">
                  <param name=Pcolor6 value="#FFFF00">
                  <param name=Plabel7 value="Inversiones">
                  <param name=Pvalue7 value="<%= Util.parseCCYtoDouble(grpTotal.getP01INVERA().trim())%>">
                  <param name=Pcolor7 value="#00FF33">
                  <param name=Plabel8 value="Aceptaciones">
                  <param name=Pvalue8 value="<%= Util.parseCCYtoDouble(grpTotal.getP01ACEPTA().trim())%>">
                  <param name=Pcolor8 value="#FFCC33">
                  <param name=Plabel9 value="C. de C.">
                  <param name=Pvalue9 value="<%= Util.parseCCYtoDouble(grpTotal.getP01LCCONF().trim())%>">
                  <param name=Pcolor9 value="#FF3300">
                  <param name=Plabel10 value="Spot">
                  <param name=Pvalue10 value="<%= Util.parseCCYtoDouble(grpTotal.getP01SPTPUR().trim())%>">
                  <param name=Pcolor10 value="#99FF33">
                  <param name=Plabel11 value="Forward">
                  <param name=Pvalue11 value="<%= Util.parseCCYtoDouble(grpTotal.getP01FRWPUR().trim())%>">
                  <param name=Pcolor11 value="#FFFF99">
                </APPLET><%
              		}
              	%> </div>
            </td>
            <td nowrap colspan=2> 
              <div align="center">
              	<%
              		if ( !grpTotal.getE01TOTLIA().equals("0.00") ) {
					   
              	%> <APPLET archive="eibs_applets.zip" code="datapro.eibs.applets.graph.PieChart.class" width="100%" height="100%" codebase="<%=request.getContextPath()%>/pages/s/">
                  <param name=title value="Pasivos">
                  <param name=showlabel   value="YES">
                  <param name=showpercent value="YES">
                  <param name=bgcolor value="#D1D1D1">
                  <param name=columns value="10">
                  <param name=Plabel1 value="Cta sin Int">
                  <param name=Pvalue1 value="<%= Util.parseCCYtoDouble(grpTotal.getP01CTACTE().trim())%>">
                  <param name=Pcolor1 value="#00CCFF">
                  <param name=Plabel2 value="Ahorro">
                  <param name=Pvalue2 value="<%= Util.parseCCYtoDouble(grpTotal.getP01CTAAHO().trim())%>">
                  <param name=Pcolor2 value="#FFFF00">
                  <param name=Plabel3 value="Certificados">
                  <param name=Pvalue3 value="<%= Util.parseCCYtoDouble(grpTotal.getP01CDTDPO().trim())%>">
                  <param name=Pcolor3 value="#00FF33">
                  <param name=Plabel4 value="Inversiones">
                  <param name=Pvalue4 value="<%= Util.parseCCYtoDouble(grpTotal.getP01INVERP().trim())%>">
                  <param name=Pcolor4 value="#99FF33">
                  <param name=Plabel5 value="Participaciones">
                  <param name=Pvalue5 value="<%= Util.parseCCYtoDouble(grpTotal.getP01PARTIC().trim())%>">
                  <param name=Pcolor5 value="#FF3300">
                  <param name=Plabel6 value="Aceptaciones">
                  <param name=Pvalue6 value="<%= Util.parseCCYtoDouble(grpTotal.getP01ACEPTP().trim())%>">
                  <param name=Pcolor6 value="#CC99FF">
                  <param name=Plabel7 value="C. de C.">
                  <param name=Pvalue7 value="<%= Util.parseCCYtoDouble(grpTotal.getP01LCPROC().trim())%>">
                  <param name=Pcolor7 value="#0066FF">
                  <param name=Plabel8 value="Spot">
                  <param name=Pvalue8 value="<%= Util.parseCCYtoDouble(grpTotal.getP01SPTSAL().trim())%>">
                  <param name=Pcolor8 value="#FFCC33">
                  <param name=Plabel9 value="Forward">
                  <param name=Pvalue9 value="<%= Util.parseCCYtoDouble(grpTotal.getP01FRWSAL().trim())%>">
                  <param name=Pcolor9 value="#FF0000">
                  <param name=Plabel10 value="Cta con Int">
                  <param name=Pvalue10 value="<%= Util.parseCCYtoDouble(grpTotal.getP01CTAMMK().trim())%>">
                  <param name=Pcolor10 value="#FF6600">
                </APPLET>
              	<%
              		}
              	%>
				  </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <div align="center"> 
          <br>
          <div align="center"> 
            <input id="EIBSBTN" type=button name="Submit" value="Cerrar" onClick="top.close()">
  </div>
</form>
</body>
</html>
