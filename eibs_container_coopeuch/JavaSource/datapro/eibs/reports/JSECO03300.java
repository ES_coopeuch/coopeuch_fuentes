package datapro.eibs.reports;

/**
 * Registros  de BRMST
 * @author: Jose M. Buitrago
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ECO033001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERC034001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSECO03300 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int A_CARTOLAAPORTE	= 100;	
	protected static final int A_INFORMESALDOS	= 200;	
	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {

		case A_CARTOLAAPORTE:
			procCartolaAporte(user, req, res, session);
			break;
		case A_INFORMESALDOS:
			procInformeSaldos(user, req, res, session);
			break;
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	/**
	 * procReqTablaBrmst: This Method show a single  TablaBrmst either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procCartolaAporte(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ECO0330", req);
			
			//Creates the message with operation code depending on the option
			ECO033001Message msg = (ECO033001Message) mp.getMessageRecord("ECO033001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01OPECOD("0001");
			msg.setE01NROCTA("0");
			msg.setE01INDICA(" ");
			
			//Send message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ECO033001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);

			if (!mp.hasError(msgError)) {
			//if there are no errors go to maintenance page
				flexLog("About to call Page: " + JSEIBSProp.getReporteInformeSaldo() + "/Reportes/ReporteConvenios.do?nombre=Reporte_CartolaAporte");
				res.sendRedirect(JSEIBSProp.getReporteInformeSaldo() + "/Reportes/ReporteConvenios.do?nombre=Reporte_CartolaAporte ");
			  } 
			else 
			  {
				//if there are errors go back to list page
					session.setAttribute("error", msgError);
					forward("ECO0330_cartola_aporte.jsp", req, res);
			  }
			
			
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqTablaBrmst: This Method show a single  TablaBrmst either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException 
	 * @throws IOException
	 */
	protected void procInformeSaldos(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ECO0330", req);
			
			//Creates the message with operation code depending on the option
			ECO033001Message msg = (ECO033001Message) mp.getMessageRecord("ECO033001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01OPECOD("0001");
			msg.setE01NROCTA("0");
			msg.setE01INDICA(" ");
			
			//Send message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (ECO033001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);

			if (!mp.hasError(msgError)) {
			//if there are no errors go to maintenance page
				flexLog("About to call Page: " + JSEIBSProp.getReporteInformeSaldo() + "/Reportes/ReporteConvenios.do?nombre=Reporte_InformeSaldo");
				res.sendRedirect(JSEIBSProp.getReporteInformeSaldo() + "/Reportes/ReporteConvenios.do?nombre=Reporte_InformeSaldo");
			  } 
			else 
			  {
				//if there are errors go back to list page
					session.setAttribute("error", msgError);
					forward("ECO0330_informe_saldos.jsp", req, res);
			  }

						
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	
}
