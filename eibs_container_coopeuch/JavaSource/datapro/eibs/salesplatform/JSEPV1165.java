package datapro.eibs.salesplatform;

/**
 * Registros  de Tarjetas Alianza 
 * Archivo de Tarjetas Alianza para plataforma de venta
 * @author evargas  
 */
import java.io.IOException;

import javax.ccpp.SetAttribute;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV116501Message;
import datapro.eibs.beans.EPV116502Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import java.text.DecimalFormat;

public class JSEPV1165 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int R_CARGOS_LIST = 100;
	protected static final int A_CARGOS_LIST = 101;
	protected static final int R_CARGOS_NEW = 200;
	protected static final int R_CARGOS_MAINT = 201;
	protected static final int R_CARGOS_DELETE = 202;	
	protected static final int R_CARGOS_INQUIRY = 203;
	protected static final int A_CARGOS_MAINT = 600;
	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_CARGOS_LIST:
			procReqTablaCargosList(user, req, res, session);
			break;
		case A_CARGOS_LIST:
			procActionTablaCargosList(user, req, res, session, null);
			break;
		case R_CARGOS_NEW:
			procReqTablaCargos(user, req, res, session, "NEW");
			break;
		case R_CARGOS_MAINT:
			procReqTablaCargos(user, req, res, session, "MAINTENANCE");
			break;
		case R_CARGOS_INQUIRY:
			procReqTablaCargos(user, req, res, session, "INQUIRY");
			break;
		case A_CARGOS_MAINT:
			procActionMaintenance(user, req, res, session);
			break;
		case R_CARGOS_DELETE:
			procReqDelete(user, req, res, session);
			break;
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}


	/**
	 * procReqTablaSEGUROSList
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTablaCargosList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: EPV1165_cargos_enter_search.jsp");
			forward("EPV1165_cargos_enter_search.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

	/**
	 * procActionTablaCargosList: Muestra los diferentes Tarjetas Alianza que posee Un cliente	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionTablaCargosList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, String option)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		boolean evaluation = false;
		
		try {
			mp = getMessageProcessor("EPV1165", req);

			EPV116501Message msg = (EPV116501Message) mp.getMessageRecord("EPV116501");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());
			
			System.out.println("E01PVHCUN=/"+req.getParameter("E01PVHCUN")+"/");
			System.out.println("E01PVHNUM=/"+req.getParameter("E01PVHNUM")+"/");
			
			msg.setE01PVHCUN(req.getParameter("E01PVHCUN").trim());
			msg.setE01PVHNUM(req.getParameter("E01PVHNUM").trim());
			
			try
			{
				
			session.setAttribute("E01LNNAMT", req.getParameter("E01LNNAMT").trim());
			session.setAttribute("E01LNTERM", req.getParameter("E01LNTERM").trim());
			session.setAttribute("E01LNPROD", req.getParameter("E01LNPROD").trim());
			session.setAttribute("E01LNTYPE", req.getParameter("E01LNTYPE").trim());
			session.setAttribute("E01SIRATE", req.getParameter("E01SIRATE").trim());
			session.setAttribute("E01PVMBNK", req.getParameter("E01PVMBNK").trim());
			session.setAttribute("E01PVMBRN", req.getParameter("E01PVMBRN").trim());
			session.setAttribute("E01LNTERC", req.getParameter("E01LNTERC").trim());
			}
			catch (Exception e) {
				
			}
			
			userPO.setCusNum(req.getParameter("E01PVHCUN").trim());
			userPO.setHeader23(req.getParameter("E01PVHNUM").trim());
			
			//Sends message
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
 
			session.setAttribute("userpPO", userPO);
			session.setAttribute("EPV116501List", list);
			forwardOnSuccess("EPV1165_cargos_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqTablaseguros: This Method show a single  Tabla Tarjetas Alianza either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqTablaCargos(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, String option) throws ServletException,
			IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("EPV1165", req);
			userPO.setPurpose(option);
			
			//Creates the message with operation code depending on the option
			EPV116501Message msg = (EPV116501Message) mp.getMessageRecord("EPV116501");
			msg.setH01USERID(user.getH01USR());
			msg.setH01TIMSYS(getTimeStamp());
			if (option.equals("NEW")) {
				msg.setH01OPECOD("0001");
			} else if (option.equals("MAINTENANCE")) {
				msg.setH01OPECOD("0002");
				msg.setE01PVHSEQ(req.getParameter("E01PVHSEQ").trim());
			} else {
				msg.setH01OPECOD("0004");
				msg.setE01PVHSEQ(req.getParameter("E01PVHSEQ").trim());
			}
			
	
			//Sets the number for maintenance and inquiry options
			msg.setE01PVHCUN(userPO.getCusNum());
			msg.setE01PVHNUM(userPO.getHeader23());

			//Send message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV116501Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go to maintenance page

				//INICIO lectura de listado de avales
				if(msg.getH01FLGWK3().equals("A"))
				{	
					
					EPV116502Message msgAvales = (EPV116502Message) mp.getMessageRecord("EPV116502");
					msgAvales.setH02USERID(user.getH01USR());
					msgAvales.setH02TIMSYS(getTimeStamp());
					msgAvales.setE02PVTCUN(userPO.getCusNum());
					msgAvales.setE02PVTNUM(userPO.getHeader23());
					msgAvales.setE02PVTCDE(msg.getE01PVHCDE());
					msgAvales.setE02PVTSEQ(msg.getE01PVHSEQ());
					msgAvales.setH02OPECOD("0001");

					mp.sendMessage(msgAvales);

					//Receive error and data
					msgError = (ELEERRMessage) mp.receiveMessageRecord();
					JBObjList listAvales = mp.receiveMessageRecordList("H02FLGMAS");

					session.setAttribute("listAvales", listAvales);
				 }	
				//FIN Lectura de avales 
				
				flexLog("About to call Page: EPV1165_cargos_maintenance.jsp");
				if (option.equals("INQUIRY")) {
					// if the request is an inquiry sets the readOlnly attribute 'true'
					forward("EPV1165_cargos_maintenance.jsp?readOnly=true", req, res);
				} else {
					forward("EPV1165_cargos_maintenance.jsp", req, res);
				}
			} else {
				//if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1165_cargos_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procActionMaintenance
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1165", req);

			EPV116501Message msg = (EPV116501Message)  mp.getMessageRecord("EPV116501");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0005");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets message with page fields
			msg.setH01SCRCOD("01");

			setMessageRecord(req, msg);
			//Sending message
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV116501Message) mp.receiveMessageRecord();
			
			session.setAttribute("datarec", msg);
			//Sets session with required data

			session.setAttribute("userPO", userPO);
			
			if (!mp.hasError(msgError)) {
				//if there are no errors go back to list

				//INICIO Evvio de avales
				if(req.getParameter("H01FLGWK3").equals("A"))
				{	
					String Secuen = "";
					String CodSeg = "";
					String RutAva = "";
					String NomAva = "";
					String TipRel = "";
					String PorSeg = "";
					String IndPro = "";
					int subInd=0;
					boolean ft=true;
					
					int indAval =  Integer.parseInt(req.getParameter("E02PVTIND").toString());
					
					//VALIDA AVALES
					for(subInd=1; subInd<=indAval; subInd++)
					{
						IndPro = req.getParameter("E02INDICA"+subInd);
						PorSeg = req.getParameter("E02PVTPOR"+subInd);

						if(IndPro.equals("1")&&!PorSeg.equals("0"))
						{
							Secuen = req.getParameter("E02PVTSEQ"+subInd);
							CodSeg = req.getParameter("E02PVTCDE"+subInd);
							RutAva = req.getParameter("E02PVTIDE"+subInd);
							NomAva = req.getParameter("E02PVTMNE"+subInd);
							TipRel = req.getParameter("E02PVTREL"+subInd);

							EPV116502Message msgAval = (EPV116502Message) mp.getMessageRecord("EPV116502");
							msgAval.setH02USERID(user.getH01USR());
							msgAval.setH02TIMSYS(getTimeStamp());
							msgAval.setH02OPECOD("0003");

							msgAval.setE02PVTCUN(msg.getE01PVHCUN());
							msgAval.setE02PVTNUM(msg.getE01PVHNUM());
							msgAval.setE02PVTSEQ(Secuen);
							msgAval.setE02PVTCDE(CodSeg);
							msgAval.setE02PVTIDE(RutAva);
							msgAval.setE02PVTMNE(NomAva);
							msgAval.setE02PVTREL(TipRel);
							msgAval.setE02PVTPOR(PorSeg);
							
							msgAval.setE02LNOAMT((String)session.getAttribute("E01LNNAMT"));
							msgAval.setE02LNTERM((String)session.getAttribute("E01LNTERM"));
							msgAval.setE02LNPROD((String)session.getAttribute("E01LNPROD"));
							msgAval.setE02LNTYPE((String)session.getAttribute("E01LNTYPE"));
							msgAval.setE02LNRATE((String)session.getAttribute("E01SIRATE"));
							msgAval.setE02PVMBNK((String)session.getAttribute("E01PVMBNK"));
							msgAval.setE02PVMBRN((String)session.getAttribute("E01PVMBRN"));
							msgAval.setE02LNTERC((String)session.getAttribute("E01LNTERC"));
							
				            String strTodLiq = session.getAttribute("E01LNNAMT").toString().replace(".","");
				            strTodLiq = strTodLiq.replace(",",""); 
				            long inttotLiq =  Long.parseLong(strTodLiq)/100;
							
				            String strTodReq = session.getAttribute("E01TOTREQ").toString().replace(".","");
				            strTodReq = strTodReq.replace(",","");
				            long inttotReq =  Long.parseLong(strTodReq)/100;

				            String strTodChk = session.getAttribute("E01TOTCHK").toString().replace(".","");
				            strTodChk = strTodChk.replace(",","");
				            long inttotChk =  Long.parseLong(strTodChk)/100;
				            
				            long Total = inttotLiq+inttotReq+inttotChk;
				            
							msgAval.setE02FILLE1(Long.toString(Total));		 					

							mp.sendMessage(msgAval);
							
							msgError = (ELEERRMessage) mp.receiveMessageRecord();
							if (mp.hasError(msgError)) 
								subInd=indAval+1;	

							msgAval = (EPV116502Message) mp.receiveMessageRecord();
						}
					}	
					
					
					//Envio de avales.. 
					
					if (!mp.hasError(msgError))
					{	
						for(subInd=1; subInd<=indAval; subInd++)
						{
							IndPro = req.getParameter("E02INDICA"+subInd);
							PorSeg = req.getParameter("E02PVTPOR"+subInd);

							if(IndPro.equals("1")&&!PorSeg.equals("0"))
							{
								Secuen = req.getParameter("E02PVTSEQ"+subInd);
								CodSeg = req.getParameter("E02PVTCDE"+subInd);
								RutAva = req.getParameter("E02PVTIDE"+subInd);
								NomAva = req.getParameter("E02PVTMNE"+subInd);
								TipRel = req.getParameter("E02PVTREL"+subInd);
							
								EPV116502Message msgAval = (EPV116502Message) mp.getMessageRecord("EPV116502");
								msgAval.setH02USERID(user.getH01USR());
								msgAval.setH02TIMSYS(getTimeStamp());
								msgAval.setH02OPECOD("0002");
								if(ft)
								{	
									msgAval.setH02FLGWK3("E");
									ft=false;
								
								}
								msgAval.setE02PVTCUN(msg.getE01PVHCUN());
								msgAval.setE02PVTNUM(msg.getE01PVHNUM());
								msgAval.setE02PVTSEQ(Secuen);
								msgAval.setE02PVTCDE(CodSeg);
								msgAval.setE02PVTIDE(RutAva);
								msgAval.setE02PVTMNE(NomAva);
								msgAval.setE02PVTREL(TipRel);
								msgAval.setE02PVTPOR(PorSeg);
								
								mp.sendMessage(msgAval);

								msgError = (ELEERRMessage) mp.receiveMessageRecord();
								if (mp.hasError(msgError)) 
									subInd=indAval+1;	
	
								msgAval = (EPV116502Message) mp.receiveMessageRecord();
							}
						}	
					}
				}	
				
				//Fin Evvio de avales
				if (!mp.hasError(msgError)) 
					req.setAttribute("ACT","S");
				else
					session.setAttribute("error", msgError);
				
				forward("EPV1165_cargos_maintenance.jsp", req, res);
			
			} else {
				//if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				forward("EPV1165_cargos_maintenance.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqDelete
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDelete(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("MAINTENANCE");

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1165", req);
			
			//Creates message with the 'Delete'operation code
			EPV116501Message msg = (EPV116501Message) mp.getMessageRecord("EPV116501");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0009");
			msg.setH01TIMSYS(getTimeStamp());
			
			//Sets required values
			msg.setH01SCRCOD("01");
			//Sets the Code for delete options
			msg.setE01PVHCUN(userPO.getCusNum());
			msg.setE01PVHNUM(userPO.getHeader23());
			msg.setE01PVHSEQ(req.getParameter("E01PVHSEQ").trim());
			
			//Send message
			mp.sendMessage(msg);

			//Receive Error and Data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV116501Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("datarec", msg);

			if (!mp.hasError(msgError)) {
				//If there are no errors request the list again
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1165?SCREEN=101&E01PVHCUN=" + userPO.getCusNum()+"&E01PVHNUM="+userPO.getHeader23(), res);				
			} else {
				//if there are errors show the list without updating
				session.setAttribute("error", msgError);
				forward("EPV1165_cargos_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

}


