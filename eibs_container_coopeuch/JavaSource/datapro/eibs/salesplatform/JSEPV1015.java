package datapro.eibs.salesplatform;


/*********************************************************************************************************************************/
/**                       Plataforma de Ventas - Probacion Tasa de Descuento                                                    **/
/**  Creado     por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  06/02/2013                                                                                      **/
/**  Objetivo                :  Servicio para Aprobacion de holgura de Tasas de Descuento                                       **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV100502Message;
import datapro.eibs.beans.EPV100503Message;
import datapro.eibs.beans.EPV101001Message;
import datapro.eibs.beans.EPV101501Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV103001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

public class JSEPV1015 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_APPROVAL				= 5;
	protected static final int A_APPROVAL_APROB			= 2;
	protected static final int A_APPROVAL_RECHAZ		= 4;	
	protected static final int R_APPROVAL_INQ			= 3;
	protected static final int R_PASSWORD				= 1;
	

	protected void processRequest(
			ESS0030DSMessage user, 
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session, 
			int screen) 
			throws ServletException, IOException {
		
		switch (screen) {
		case R_PASSWORD :
			procReqPassword(req, res, session);
			break;
		case R_APPROVAL :
			procReqApproval(user, req, res, session);
			break;
		case A_APPROVAL_APROB :
			procActionApproval(user, req, res, session, "A");
			break;
		case A_APPROVAL_RECHAZ :
			procActionApproval(user, req, res, session, "R");
			break;						
		case R_APPROVAL_INQ :
			procReqApprovalInq(user, req, res, session);
			break;
			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}

	protected void procReqApproval(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		try {

			mp = getMessageProcessor("EPV1015", req);
			EPV101501Message msgList = (EPV101501Message) mp.getMessageRecord("EPV101501", user.getH01USR(), "0001");

			// Sends message
			mp.sendMessage(msgList);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			// if there are NO errors display list
			session.setAttribute("error", error);
			session.setAttribute("EPV101501List", list);
			forwardOnSuccess("EPV1015_salesplatform_holgura_list.jsp", req, res);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mp != null)
				mp.close();
		}
	}
	

	protected void procActionApproval(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res,
			HttpSession session,
			String action) 
			throws ServletException,
			IOException {
	
		JBObjList bl = (JBObjList) session.getAttribute("EPV101501List");		
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("PVMNUM"));
		} catch (Exception e) {
			idx = 0;
		}
		MessageProcessor mp = null;		
		try {
			mp = getMessageProcessor("EPV1015", req);									
			EPV101501Message msgList = (EPV101501Message) bl.get(idx);
			EPV101501Message msg = (EPV101501Message) mp.getMessageRecord("EPV101501", user.getH01USR(), "0002");			
			msg.setE01PVMNUM(msgList.getE01PVMNUM());
			msg.setE01ACTION(action);			

			// Send message
			flexLog("mensaje enviado..." + msg + "  " + req.getParameter("PVMNUM"));
			mp.sendMessage(msg);

		// Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			if (!mp.hasError(msgError)) {	
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1015?SCREEN=5", res);
			} else {
				session.setAttribute("error", msgError);
				forward("EPV1015_salesplatform_holgura_list.jsp", req, res);					
			}

		} finally {
			if (mp != null)
				mp.close();
		}

	}	
	protected void procReqApprovalInq(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res,
			HttpSession session) 
			throws ServletException,
			IOException {

			JBObjList bl = (JBObjList) session.getAttribute("EPV101501List");		
			int idx = 0;
			try {
				idx = Integer.parseInt(req.getParameter("E01PVMNUM"));
			} catch (Exception e) {
				idx = 0;
			}	
			flexLog("E01PVMNUM.." + req.getParameter("E01PVMNUM"));
			
			MessageProcessor mp = null;
			try {
				mp = getMessageProcessor("EPV1030", req);

				EPV101501Message msgList = (EPV101501Message) bl.get(idx);	
				
				EPV103001Message msg = (EPV103001Message) mp.getMessageRecord("EPV103001", user.getH01USR(), "0004");

				// Sets the client number
				msg.setE01PVMCUN(msgList.getE01PVMCUN());
				msg.setE01PVMNUM(msgList.getE01PVMNUM());

				// Send message
				mp.sendMessage(msg);

				// Receive error and data
				ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
				msg = (EPV103001Message) mp.receiveMessageRecord();
				session.setAttribute("document", msg);// set header page

				if (!mp.hasError(msgError)) {
					// if there are no errors go to maintenance page
					forward("EPV1030_salesplatform_documentos_inquiry.jsp", req, res);
				} else {
					// if there are errors go back to list page
					session.setAttribute("error", msgError);
					forward("EPV1015_salesplatform_holgura_list.jsp", req, res);
				}

			} finally {
				if (mp != null)
					mp.close();
			}			

	}
	
	protected void procReqPassword(
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession ses)
				throws ServletException, IOException {
		
	    flexLog ("Entre en procReqPassword ");

		try {
			redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1015?SCREEN=5", res);			
			redirectToPage("/servlet/datapro.eibs.menu.JSESS0030?SCREEN=7", res);			
			
		}
		catch (Exception e)	{
			e.printStackTrace();
			flexLog("Error: " + e);
		  	throw new RuntimeException("Socket Communication Error");
		}	
		
	}
 }	



