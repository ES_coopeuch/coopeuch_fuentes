package com.datapro.eibs.security.access.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.MenuOptionNotAssignedException;
import com.datapro.eibs.security.access.jdbc.bean.SECMNU;
import com.datapro.generic.access.DAOSuper;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.BeanParser;
import com.datapro.generic.beanutil.BeanSql; 


/**
 * @author ogarcia
 *
 */
public class DAOMenu extends DAOSuper {

	private static Category cat =
		Category.getInstance(DAOMenu.class.getName());

	/**
	 * Constructor for DAOUserEibs.
	 * @param dbid
	 */
	public DAOMenu(String dbid) {
		super(dbid);
	}

	/**
	 * Constructor for DAOUserEibs.
	 */
	public DAOMenu() {
		super();
	}

	/**
	 * Constructor for DAOUserEibs.
	 * @param connection
	 */
	public DAOMenu(Connection connection) {
		super(connection);
	}

	/**
	 * Method getMenu.
	 * @return SECMNU
	 * @throws DAOException
	 * Get user record.
	 */
	public SECMNU getMenuItem(int mid)
		throws DAOException {

		SECMNU bean = null;

		try {
			String sql = "SELECT * FROM SECMNU WHERE MNUIDN=?";
			
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setInt(1, mid);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new SECMNU();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} else {
				throw new DAOException(
					"Menu indefined.");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return bean;

	}

    /**
	 * Get menu.
	 */
	public BeanList getMenu()
		throws DAOException {

		BeanList list = null;

		try {
			String sql = "SELECT * FROM SECMNU";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				SECMNU bean = new SECMNU();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new MenuOptionNotAssignedException(
					".");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}
	
	/**
	 * Get menu.
	 */
	public BeanList getChilds(int master)
		throws DAOException {

		BeanList list = null;

		try {
			String sql = "SELECT * FROM SECMNU WHERE MSTMNUIDN=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setInt(1, master);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				SECMNU bean = new SECMNU();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new MenuOptionNotAssignedException(
					".");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}
	
	/**
	 * Method delete.
	 * @param mid
	 * @return int
	 * @throws DAOException
	 * Delete user record.
	 */
	public int delete(int mid) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM CNTRLEUP WHERE MNUIDN=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setInt(1, mid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

	/**
	 * Method insert.
	 * @param bean
	 * @return int
	 * @throws DAOException
	 * Insert user record.
	 */
	public int insert(SECMNU bean) throws DAOException {

		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.insertPrepareStatement();
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

	/**
	 * Method update.
	 * @param bean
	 * @return int
	 * @throws DAOException
	 * Update user record.
	 */
	public int update(SECMNU bean) throws DAOException {

		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.updatePrepareStatement() + " WHERE MNUIDN=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			ps.setInt(bs.getNextParamIndex(), bean.getMNUIDN());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

	
	/**
	 * Method deleteByGroup.
	 * @param gid
	 * @return int
	 * @throws DAOException
	 * Delete all eibs user for a group.
	 */
	public int deleteByGroup(int gid) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM SECMNU WHERE MNUIDN = ? OR MSTMNUIDN = ? ";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setInt(1, gid);
			ps.setInt(2, gid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}


}
