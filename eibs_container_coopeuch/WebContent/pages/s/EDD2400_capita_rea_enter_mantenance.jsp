<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import="datapro.eibs.beans.EDD240001Message"%>
<%@ page import="datapro.eibs.beans.EDD240001Message"%>
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Mantenimiento Cambio Valor Cuota y Reajuste</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="date" class="java.util.Date" scope="session" />      
<jsp:useBean id= "CapRea" class= "datapro.eibs.beans.EDD240001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">

</script>

</head>
<body >
<H3 align="center">Mantenimiento Cambio Valor Cuota y Reajuste<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="EDD2400_capita_rea_enter_mantenance.jsp, EDD2400"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSEDD2400">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200"> 

<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trclear">
             <td nowrap width="30%" align="right"> 
             </td>
             <td nowrap width="1"> 
             	<p>|</p>
			 </td>
             <td nowrap width="25%" align="left">
	  		    <div align="center">&Uacute;LTIMO PROCESO</div>
             </td>  
             <td nowrap width="1"> 
             	<p>|</p>
			 </td>
             <td nowrap width="43%" align="right">
	  		    <div align="center">PR&Oacute;XIMO PROCESO</div>
             </td>
         </tr>

         <tr id="trdark">
             <td nowrap align="right"> 
             	Valor Cuota Participaci&oacute;n : 
             </td>
             <td nowrap> 
             	<p>|</p>
			 </td>
             <td nowrap align="left">
             	<input type="text" name="E01VCMVCUP" id="fecha1" size="6" maxlength="5" value="<%=CapRea.getE01VCMVCUP() %>" class="TXTRIGHT" readonly>
 			 </td>                 
             <td nowrap> 
             	<p>|</p>
			 </td>
             <td nowrap  align="left"> 
             	<input type="text" name="E01VCMVCNP" id="fecha1" size="6" maxlength="5" value="<%=CapRea.getE01VCMVCNP() %>" class="TXTRIGHT" onkeypress=" enterInteger()">
             </td>
         </tr>
    
         <tr id="trclear">
             <td nowrap align="right"> 
             	Fecha Cambio Cuota Participaci&oacute;n : 
             </td>
             <td nowrap> 
             	<p>|</p>
			 </td>
             <td nowrap align="left">
                <input type="text" name="E01VCMFUCD"  size="3" maxlength="2" value="<%=CapRea.getE01VCMFUCD() %>" readonly>
                <input type="text" name="E01VCMFUCM"  size="3" maxlength="2" value="<%=CapRea.getE01VCMFUCM() %>" readonly>
                <input type="text" name="E01VCMFUCY"  size="5" maxlength="4" value="<%=CapRea.getE01VCMFUCY() %>" readonly>
             </td>                 
             <td nowrap> 
             	<p>|</p>
			 </td>
             <td nowrap  align="left"> 
                <input type="text" name="E01VCMFNCD" id="fecha1" size="3" maxlength="2" value="<%=CapRea.getE01VCMFNCD() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <input type="text" name="E01VCMFNCM" id="fecha2" size="3" maxlength="2" value="<%=CapRea.getE01VCMFNCM() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <input type="text" name="E01VCMFNCY" id="fecha3" size="5" maxlength="4" value="<%=CapRea.getE01VCMFNCY() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <a href="javascript:DatePicker(document.forms[0].E01VCMFNCD,document.forms[0].E01VCMFNCM,document.forms[0].E01VCMFNCY)">
                <img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
             </td>
         </tr>

         <tr id="trclear">
             <td nowrap align="right"> 
             	Motivo : 
             </td>
             <td nowrap> 
             	<p>|</p>
			 </td>
             <td nowrap align="left" valign="middle">
                <input type="text" name="E01VCMCMUC" size="6" maxlength="4" value="<%= CapRea.getE01VCMCMUC().trim()%>" readonly>
                <input type="text" name="E01VCMGMUC" size="50" maxlength="45" value="<%= CapRea.getE01VCMGMUC().trim()%>" readonly>
             </td>                 
             <td nowrap> 
             	<p>|</p>
			 </td>
             <td nowrap  align="left"> 
                <input type="text" name="E01VCMCMNC" size="6" maxlength="4" value="<%= CapRea.getE01VCMCMNC().trim()%>" readonly>
                <input type="text" name="E01VCMGMNC" size="50" maxlength="45" value="<%= CapRea.getE01VCMGMNC().trim()%>" readonly>
                <a href="javascript:GetCodeDescCNOFC('E01VCMCMNC','E01VCMGMNC','V9')">
					<img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0">
		        </a>
				<img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
             </td>
         </tr>

         <tr id="trdark">
             <td nowrap align="right"> 
             	Fecha Reajustes : 
             </td>
             <td nowrap> 
             	<p>|</p>
			 </td>
             <td nowrap align="left">
                <input type="text" name="E01VCMFURD"  size="3" maxlength="2" value="<%=CapRea.getE01VCMFURD() %>" readonly>
                <input type="text" name="E01VCMFURM"  size="3" maxlength="2" value="<%=CapRea.getE01VCMFURM() %>" readonly>
                <input type="text" name="E01VCMFURY"  size="5" maxlength="4" value="<%=CapRea.getE01VCMFURY() %>" readonly>
             </td>                 
             <td nowrap> 
             	<p>|</p>
			 </td>
             <td nowrap  align="left"> 
                <input type="text" name="E01VCMFNRD" id="fecha3" size="3" maxlength="2" value="<%=CapRea.getE01VCMFNRD() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <input type="text" name="E01VCMFNRM" id="fecha5" size="3" maxlength="2" value="<%=CapRea.getE01VCMFNRM() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <input type="text" name="E01VCMFNRY" id="fecha6" size="5" maxlength="4" value="<%=CapRea.getE01VCMFNRY() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <a href="javascript:DatePicker(document.forms[0].E01VCMFNRD,document.forms[0].E01VCMFNRM,document.forms[0].E01VCMFNRY)">
                <img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
             </td>
         </tr>

        </table>
      </td>
    </tr>
  </table>

  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
<script language="JavaScript">
  document.forms[0].E01VCMFCAD.focus();
  document.forms[0].E01VCMFCAD.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
