package datapro.eibs.products;

/**
 * Insert the type's description here.
 * Creation date: (1/19/00 6:08:55 PM)
 * @author: Orestes Garcia
 */
import java.io.*;
import java.net.*;
import java.beans.Beans;
import javax.servlet.*;
import javax.servlet.http.*;
import datapro.eibs.beans.*;

import datapro.eibs.master.Util;

import datapro.eibs.sockets.*;

public class JSEAPR010 extends datapro.eibs.master.SuperServlet {

	// CIF options
	protected static final int R_LIST = 1;
	protected static final int R_SEARCH = 2;
	protected static final int A_SEARCH = 3;
	protected static final int R_PRINT = 4;

	// entering options
	protected static final int R_ENTER = 100;
	protected static final int A_ENTER = 200;

	protected String LangPath = "S";

	/**
	 * JSECLI001 constructor comment.
	 */
	public JSEAPR010() {
		super();
	}
	/**
	 * This method was created by .
	 */
	public void destroy() {

		flexLog("free resources used by JSEAPR010");

	}
	/**
	 * This method was created by .
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionSearch(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		UserPos userPO = null;

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		try {
			userPO.setHeader9(req.getParameter("E01FRDTE1"));
			userPO.setHeader10(req.getParameter("E01FRDTE2"));
			userPO.setHeader11(req.getParameter("E01FRDTE3"));
		} catch (Exception e) {
			flexLog("DATE 1");
		}
		try {
			userPO.setHeader12(req.getParameter("E01TODTE1"));
			userPO.setHeader13(req.getParameter("E01TODTE2"));
			userPO.setHeader14(req.getParameter("E01TODTE3"));
		} catch (Exception e) {
			flexLog("DATE 2");
		}

		try {
			userPO.setHeader17(req.getParameter("E01HISEVN"));
		} catch (Exception e) {
			flexLog("E01HISEVN");
		}
		try {
			userPO.setHeader18(req.getParameter("E01HISBRN"));
		} catch (Exception e) {
			flexLog("E01HISBRN");
		}
		try {
			userPO.setHeader19(req.getParameter("E01HISACD"));
		} catch (Exception e) {
			flexLog("E01HISACD");
		}

		ses.setAttribute("userPO", userPO);

		procReqList(mc, user, req, res, ses);
	}
	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqList(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req,
		HttpServletResponse res, HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ELEERRMessage msgError = null;
		EAPR01001Message msgSearch = null;
		EAPR01001Message msgList = null;
		JBList beanList = null;
		UserPos userPO = null;

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		int posi = 0;
		// Send Initial data
		try {flexLog("Send Initial Data");
			 msgSearch = (EAPR01001Message) mc.getMessageRecord("EAPR01001");
			 msgSearch.setH01USERID(user.getH01USR());
			 msgSearch.setH01PROGRM("EAPR010");
			 msgSearch.setH01TIMSYS(getTimeStamp());
			 msgSearch.setH01SCRCOD("01");
			 msgSearch.setH01OPECOD("0004");

			 try { try {posi = Integer.parseInt(req.getParameter("Pos"));
						} catch (Exception e) {posi = 0;
											   flexLog("E01NUMPOS");
											  }
				   msgSearch.setE01NUMREC(""+posi);
				   try {msgSearch.setE01FRDTE1(userPO.getHeader9());
					    msgSearch.setE01FRDTE2(userPO.getHeader10());
					    msgSearch.setE01FRDTE3(userPO.getHeader11());
				        } catch (Exception e) {flexLog("DATE 1");
				        					  }
				   try {msgSearch.setE01TODTE1(userPO.getHeader12());
					    msgSearch.setE01TODTE2(userPO.getHeader13());
					    msgSearch.setE01TODTE3(userPO.getHeader14());
				        } catch (Exception e) {flexLog("DATE 2");
				        					  }
				   try {msgSearch.setE01HISEVN(userPO.getHeader17());
				   		} catch (Exception e) {flexLog("E01HISEVN");
				   							  }
				   try {msgSearch.setE01HISBRN(userPO.getHeader18());
				        } catch (Exception e) {flexLog("E01HISBRN");
				        					  }
				   try {msgSearch.setE01HISACD(userPO.getHeader19());
				   		} catch (Exception e) {flexLog("E01HISACD");
					       					  }
			 	} catch (Exception e) {e.printStackTrace();
			 						   flexLog("Input data error " + e);
			 						   }
			msgSearch.send();
			msgSearch.destroy();
		} catch (Exception e) {e.printStackTrace();
							   flexLog("Error: " + e);
							   throw new RuntimeException("Socket Communication Error");
							   }

		// Receive Message
		try {newmessage = mc.receiveMessage();
			 if (newmessage.getFormatName().equals("ELEERR")) {
				try {msgError = new datapro.eibs.beans.ELEERRMessage();
					} catch (Exception ex) {flexLog("Error: " + ex);
											}

				msgError = (ELEERRMessage) newmessage;
				// showERROR(msgError);
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				try {flexLog("About to call Page: "
							 + LangPath
							 + "EAPR010_his_selection.jsp");
					 callPage(LangPath + "EAPR010_his_selection.jsp", req, res);

					} catch (Exception e) {flexLog("Exception calling page " + e);
										  }
			} else if (newmessage.getFormatName().equals("EAPR01001")) {
					try {beanList = new datapro.eibs.beans.JBList();
						 } catch (Exception ex) {flexLog("Error: " + ex);
						 						}

					boolean firstTime = true;
					String marker = "";
					String myFlag = "";
					StringBuffer myRow = null;
					String chk = "";

					String DT1 = "";
					String DT2 = "";
					String DT3 = "";

					while (true) {msgList = (EAPR01001Message) newmessage;
							      marker = msgList.getE01INDOPE();

							      if (marker.equals("*")) {
							    	  beanList.setShowNext(false);
							    	  break;
							      } else {if (firstTime) {firstTime = false;
							      						  beanList.setFirstRec(
							      						  Integer.parseInt(msgList.getE01NUMREC()));
							      						  chk = "checked";
							      		 } else {chk = "";
							      		 }

							myRow = new StringBuffer("<TR>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISACC())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISCUN())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISRUT())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISNA1())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"LEFT\">"
										+ Util.formatCell(msgList.getE01HISPRO()+' '+msgList.getE01HISDES())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=RIGHT>"
										+ Util.formatCell(msgList.getE01HISRTE())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=RIGHT>"
										+ Util.formatCell(msgList.getE01HISPRI())
										+ "</TD>");
								myRow.append(
									"<TD NOWRAP ALIGN=RIGHT>"
										+ Util.formatCell(msgList.getE01HISINT())
										+ "</TD>");
								myRow.append(
										"<TD NOWRAP>"
											+ Util.formatCell(msgList.getE01HISCCY())
											+ "</TD>");
								myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISTRM()+' '+msgList.getD01HISTRC())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISOFI()+' '+msgList.getE01HISNOF())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISCDE()+' '+msgList.getE01HISNCD())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatDate(
											msgList.getE01HISFI1(),
											msgList.getE01HISFI2(),
											msgList.getE01HISFI3())
										+ "</TD>");
							myRow.append(
								"<TD NOWRAP ALIGN=\"CENTER\">"
									+ Util.formatDate(
										msgList.getE01HISFV1(),
										msgList.getE01HISFV2(),
										msgList.getE01HISFV3())
									+ "</TD>");
							myRow.append(
								"<TD NOWRAP ALIGN=RIGHT>"
									+ Util.formatCell(msgList.getE01HISSTS())
									+ "</TD>");
								myRow.append(
									"<TD NOWRAP ALIGN=CENTER>"
										+ Util.formatDate(
											msgList.getE01HISFE1(),
											msgList.getE01HISFE2(),
											msgList.getE01HISFE3())
										+ "</TD>");
							myRow.append("</TR>");
							beanList.addRow(myFlag, myRow.toString());

							if (marker.equals("+")) {
								beanList.setShowNext(true);
								break;
							}
						}

						newmessage = mc.receiveMessage();
					}

					flexLog("Putting java beans into the session");
					ses.setAttribute("cifList", beanList);
					ses.setAttribute("userPO", userPO);

					try {
						flexLog(
							"About to call Page: "
								+ LangPath
								+ "EAPR010_his_list_fp.jsp");
						callPage(LangPath + "EAPR010_his_list_fp.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}
	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqPrintList(MessageContext mc, ESS0030DSMessage user,
		HttpServletRequest req,	HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		ELEERRMessage msgError = null;
		EAPR01001Message msgSearch = null;
		EAPR01001Message msgList = null;
		JBList beanList = null;
		UserPos userPO = null;

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		int posi = 0;
		// Send Initial data
		try {flexLog("Send Initial Data");
			 msgSearch = (EAPR01001Message) mc.getMessageRecord("EAPR01001");
			 msgSearch.setH01USERID(user.getH01USR());
			 msgSearch.setH01PROGRM("EAPR010");
			 msgSearch.setH01TIMSYS(getTimeStamp());
			 msgSearch.setH01SCRCOD("01");
			 msgSearch.setH01OPECOD("0004");

			 try { try {posi = Integer.parseInt(req.getParameter("Pos"));
						} catch (Exception e) {posi = 0;
											   flexLog("E01NUMPOS");
											  }
				   try {msgSearch.setE01NUMREC(req.getParameter("Pos"));
				        } catch (Exception e) {msgSearch.setE01NUMREC("0");
				        					   flexLog("E01NUMPOS");
				        					  }
				   try {msgSearch.setE01FRDTE1(userPO.getHeader9());
					    msgSearch.setE01FRDTE2(userPO.getHeader10());
					    msgSearch.setE01FRDTE3(userPO.getHeader11());
				        } catch (Exception e) {flexLog("DATE 1");
				        					  }
				   try {msgSearch.setE01TODTE1(userPO.getHeader12());
					    msgSearch.setE01TODTE2(userPO.getHeader13());
					    msgSearch.setE01TODTE3(userPO.getHeader14());
				        } catch (Exception e) {flexLog("DATE 2");
				        					  }
				   try {msgSearch.setE01HISEVN(userPO.getHeader17());
				   		} catch (Exception e) {flexLog("E01HISEVN");
				   							  }
				   try {msgSearch.setE01HISBRN(userPO.getHeader18());
				        } catch (Exception e) {flexLog("E01HISBRN");
				        					  }
			 	} catch (Exception e) {e.printStackTrace();
			 						   flexLog("Input data error " + e);
			 						   }
			msgSearch.send();
			msgSearch.destroy();
		} catch (Exception e) {e.printStackTrace();
							   flexLog("Error: " + e);
							   throw new RuntimeException("Socket Communication Error");
							   }

		// Receive Message
		try {newmessage = mc.receiveMessage();
			 if (newmessage.getFormatName().equals("ELEERR")) {
				try {msgError = new datapro.eibs.beans.ELEERRMessage();
					} catch (Exception ex) {flexLog("Error: " + ex);
											}

				msgError = (ELEERRMessage) newmessage;
				// showERROR(msgError);
				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);

				try {flexLog("About to call Page: "
							 + LangPath
							 + "EAPR010_his_selection.jsp");
					 callPage(LangPath + "EAPR010_his_selection.jsp", req, res);

					} catch (Exception e) {flexLog("Exception calling page " + e);
										  }
			} else if (newmessage.getFormatName().equals("EAPR01001")) {
					try {beanList = new datapro.eibs.beans.JBList();
						 } catch (Exception ex) {flexLog("Error: " + ex);
						 						}

					boolean firstTime = true;
					String marker = "";
					String myFlag = "";
					StringBuffer myRow = null;
					String chk = "";

					String DT1 = "";
					String DT2 = "";
					String DT3 = "";

					while (true) {msgList = (EAPR01001Message) newmessage;
							      marker = msgList.getE01INDOPE();

							      if (marker.equals("*")) {
							    	  beanList.setShowNext(false);
							    	  break;
							      } else {if (firstTime) {firstTime = false;
							      						  beanList.setFirstRec(
							      						  Integer.parseInt(msgList.getE01NUMREC()));
							      						  chk = "checked";
							      		 } else {chk = "";
							      		 }

							myRow = new StringBuffer("<TR>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getD01HISACD())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISACC())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISBRN()+' '+msgList.getD01HISBRN())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
									+ Util.formatCell(msgList.getE01HISOFI()+' '+msgList.getE01HISNOF())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISPRO()+' '+msgList.getE01HISDES())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISCUN())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISRUT())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISNA1())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatCell(msgList.getE01HISCDE()+' '+msgList.getE01HISNCD())
										+ "</TD>");
							myRow.append(
									"<TD NOWRAP ALIGN=\"CENTER\">"
										+ Util.formatDate(
											msgList.getE01HISFI1(),
											msgList.getE01HISFI2(),
											msgList.getE01HISFI3())
										+ "</TD>");
							myRow.append(
								"<TD NOWRAP ALIGN=\"CENTER\">"
									+ Util.formatDate(
										msgList.getE01HISFV1(),
										msgList.getE01HISFV2(),
										msgList.getE01HISFV3())
									+ "</TD>");
							myRow.append(
								"<TD NOWRAP ALIGN=\"CENTER\">"
									+ Util.formatCell(msgList.getE01HISTRM()+' '+msgList.getD01HISTRC())
									+ "</TD>");
							myRow.append(
									"<TD NOWRAP>"
										+ Util.formatCell(msgList.getE01HISCCY())
										+ "</TD>");
							myRow.append(
								"<TD NOWRAP ALIGN=RIGHT>"
									+ Util.formatCell(msgList.getE01HISRTE())
									+ "</TD>");
							myRow.append(
								"<TD NOWRAP ALIGN=RIGHT>"
									+ Util.formatCell(msgList.getE01HISPRI())
									+ "</TD>");
							myRow.append(
								"<TD NOWRAP ALIGN=RIGHT>"
									+ Util.formatCell(msgList.getE01HISINT())
									+ "</TD>");
							myRow.append(
								"<TD NOWRAP ALIGN=RIGHT>"
									+ Util.formatCell(msgList.getE01HISSTS())
									+ "</TD>");
								myRow.append(
									"<TD NOWRAP ALIGN=CENTER>"
										+ Util.formatDate(
											msgList.getE01HISFE1(),
											msgList.getE01HISFE2(),
											msgList.getE01HISFE3())
										+ "</TD>");
							myRow.append("</TR>");
							beanList.addRow(myFlag, myRow.toString());

							if (marker.equals("+")) {
								beanList.setShowNext(true);
								break;
							}
						}

						newmessage = mc.receiveMessage();
					}

					flexLog("Putting java beans into the session");
					ses.setAttribute("cifList", beanList);
					ses.setAttribute("userPO", userPO);

					try {
						flexLog(
							"About to call Page: "
								+ LangPath
								+ "EAPR010_his_list_print_fp.jsp");
						callPage(LangPath + "EAPR010_his_list_print_fp.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}

				}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}
	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqSearch(
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		try {
			flexLog(
				"About to call Page: " + LangPath + "EAPR010_his_selection.jsp");
			callPage(LangPath + "EAPR010_his_selection.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}

	}
	/**
	 * This method was created in VisualAge.
	 * by .
	 * on 4/4/2013.
	 */
	protected void procReqSTEnterSearch(ESS0030DSMessage user, HttpServletRequest req,
		HttpServletResponse res, HttpSession ses)
		throws ServletException, IOException {

			try {flexLog("About to call Page: " + LangPath + "EAPR010_his_selection.jsp");
				 callPage(LangPath + "EAPR010_his_selection.jsp", req, res);
				} catch (Exception e) {flexLog("Exception calling page " + e);
										}
	}
	
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		MessageContext mc = null;
		ESS0030DSMessage msgUser = null;
		HttpSession session = null;
		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {res.setContentType("text/html");
				 printLogInAgain(res.getWriter());
				} catch (Exception e) {e.printStackTrace();
									   flexLog("Exception ocurred. Exception = " + e);
									  }
		} else {int screen = R_SEARCH;
				try {msgUser = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {flexLog("Opennig Socket Connection");
					 mc = new MessageContext(super.getMessageHandler("EAPR010", req));
					 try {screen = Integer.parseInt(req.getParameter("SCREEN"));
					 	  } catch (Exception e) {flexLog("Screen set to default value");
					 	  						 }
					 switch (screen) {
					 	case R_SEARCH :
							procReqSearch(msgUser, req, res, session);
							break;
						case A_SEARCH :
							procActionSearch(mc, msgUser, req, res, session);
							break;
						case R_LIST :
							procReqList(mc, msgUser, req, res, session);
							break;
						case R_PRINT :
							procReqPrintList(mc, msgUser, req, res, session);
							break;
							//entering options
						case R_ENTER :
							procReqSTEnterSearch(msgUser, req, res, session);
							break;
						default :
							res.sendRedirect(super.srctx + LangPath + super.devPage);
							break;
					}
				} catch (Exception e) {e.printStackTrace();
									   int sck = getInitSocket(req) + 1;
									   flexLog("Socket not Open(Port " + sck + "). Error: " + e);
									   res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
									   //return;
									} finally {mc.close();
											  }
			} catch (Exception e) {flexLog("Error: " + e);
								   res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
								  }
		}
	}
	
	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());
		}
	}
}