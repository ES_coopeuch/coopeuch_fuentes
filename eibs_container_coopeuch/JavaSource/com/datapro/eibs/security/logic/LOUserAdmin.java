package com.datapro.eibs.security.logic;

import java.sql.Connection;
import java.sql.SQLException;

import com.datapro.eibs.constants.General;
import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.LogicException;
import com.datapro.eibs.security.access.jdbc.bean.SECGRPITM;
import com.datapro.eibs.security.access.jdbc.dao.DAOGroupItem;
import com.datapro.eibs.security.access.jdbc.dao.DAOUserEibs;
import com.datapro.eibs.security.access.jdbc.dao.DAOUserIbs;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.DynamicDTO;
import com.datapro.sql.manager.DBConnector;

/**
 * @author ogarcia
 *
 */
public class LOUserAdmin {

	/**
	 * Constructor for LOUserAdmin.
	 */
	public LOUserAdmin() {
		super();
	}

	/**
	 * Method getGroupsAndUsers.
	 * @return BeanList
	 * @throws DAOException
	 */
	public BeanList getGroupsAndUsers() throws LogicException {

		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;
		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			DAOUserEibs ueibs = new DAOUserEibs(cnx);
			BeanList gauList = ueibs.getGroupsAndUsers();
			return gauList;
		}
		catch (DAOException e) {
			throw new LogicException(e.toString());
		}
		finally {
			dbCnx.close(null);
		}

	}

	/**
	 * Method getMenuAccess.
	 * @param uid
	 * @return BeanList
	 * @throws LogicException
	 */
	public BeanList getMenuAccess(String uid) throws LogicException {

		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;
		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			DAOGroupItem gi = new DAOGroupItem(cnx);
			BeanList maList = gi.getMenuAccesItems(uid);
			return maList;
		}
		catch (DAOException e) {
			throw new LogicException(e.toString());
		}
		finally {
			dbCnx.close(null);
		}

	}

	public void updMenuAccess(String uid, BeanList bl) throws LogicException {

		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;
		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			DAOGroupItem gi = new DAOGroupItem(cnx);
			gi.delete(uid);
			bl.initRow();
			while (bl.getNextRow()) {
				SECGRPITM beanItem = (SECGRPITM) bl.getRecord();
				gi.insert(beanItem);
			}
			
		}
		catch (DAOException e) {
			throw new LogicException(e.toString());
		}
		finally {
			dbCnx.close(null);
		}

	}

	/**
	 * Method newGroup.
	 * @param gid
	 * @return int
	 * @throws DAOException
	 */
	public void newGroup(String gid) throws LogicException {

		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;
		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			DAOUserIbs user = new DAOUserIbs(cnx);
			user.insert(gid, Security.USER_GROUP_TAG);
		}
		catch (DAOException e) {
			throw new LogicException(e.toString());
		}
		finally {
			dbCnx.close(null);
		}

	}

	/**
	 * Method dltGroup.
	 * @param gid
	 * @throws DAOException
	 */
	public void dltGroup(String gid) throws LogicException, SQLException {

		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;
		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			// cnx.setAutoCommit(false);
			DAOUserIbs uibs = new DAOUserIbs(cnx);
			uibs.delete(gid);
			uibs.deleteByGroup(gid);
			DAOUserEibs ueibs = new DAOUserEibs(cnx);
			ueibs.deleteByGroup(gid);
			DAOGroupItem gi = new DAOGroupItem(cnx);
			gi.delete(gid);
			// cnx.commit();
		}
		catch (DAOException e) {
			cnx.rollback();
			throw new LogicException(e.toString());
		}
		finally {
			dbCnx.close(null);
		}

	}

	/**
	 * Method newUser.
	 * @param uid
	 * @param gid
	 * @throws DAOException
	 */
	public void newUser(String uid, String gid) throws LogicException, SQLException {

		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;
		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			// cnx.setAutoCommit(false);
			DAOUserIbs uibs = new DAOUserIbs(cnx);
			uibs.insert(uid, gid);
			DAOUserEibs ueibs = new DAOUserEibs(cnx);
			ueibs.insert(uid, Security.USER_STATUS_PENDING_ACTIVATION);
			// cnx.commit();
		}
		catch (DAOException e) {
			cnx.rollback();
			throw new LogicException(e.toString());
		}
		finally {
			dbCnx.close(null);
		}

	}

	/**
	 * Method dltUser.
	 * @param uid
	 * @throws DAOException
	 */
	public void dltUser(String uid) throws LogicException, SQLException {

		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;
		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			// cnx.setAutoCommit(false);
			DAOUserIbs uibs = new DAOUserIbs(cnx);
			uibs.delete(uid);
			DAOUserEibs ueibs = new DAOUserEibs(cnx);
			ueibs.delete(uid);
			// cnx.commit();
		}
		catch (DAOException e) {
			cnx.rollback();
			throw new LogicException(e.toString());
		}
		finally {
			dbCnx.close(null);
		}

	}

	/**
	 * Method moveUser.
	 * @param uid
	 * @param gid
	 * @throws DAOException
	 */
	public void moveUser(String uid, String gid) throws LogicException {

		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;
		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			DAOUserIbs uibs = new DAOUserIbs(cnx);
			uibs.update(uid, gid);
		}
		catch (DAOException e) {
			throw new LogicException(e.toString());
		}
		finally {
			dbCnx.close(null);
		}

	}

	/**
	 * Method chgStatus.
	 * @param uid
	 * @param sts
	 * @throws DAOException
	 */
	public void chgStatus(String uid, String sts) throws LogicException {

		DBConnector dbCnx = DBConnector.instance(General.DBID_CURRENT_YEAR);
		Connection cnx = null;
		try {
			cnx = (Connection) dbCnx.getDBConnection(null);
			DAOUserEibs ueibs = new DAOUserEibs(cnx);
			ueibs.update(uid, sts);
		}
		catch (DAOException e) {
			throw new LogicException(e.toString());
		}
		finally {
			dbCnx.close(null);
		}

	}

}
