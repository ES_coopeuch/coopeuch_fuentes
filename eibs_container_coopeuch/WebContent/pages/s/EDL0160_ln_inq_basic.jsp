<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>

<html>
<head>
<title>Informaci�n B�sica</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="inqBasic" class="datapro.eibs.beans.EDL016002Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
 
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 

<SCRIPT Language="Javascript">

<%
if ( userPO.getHeader23().equals("G") ||  userPO.getHeader23().equals("V")){
%>
	builtNewMenu(ln_i_1_opt);
<%   
}
else  {
%>
	builtNewMenu(ln_i_2_opt);
<%   
}
%>

</SCRIPT>

</head>

<body>


<% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
  out.println("<SCRIPT> initMenu(); </SCRIPT>");
%>

<h3 align="center">Informaci&oacute;n B&aacute;sica<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ln_inq_basic.jsp,EDL0160"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXEDL0160" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
  <% if (!inqBasic.getE02PENDAP().trim().equals("")) { %> 
  <table border="0" cellspacing="0" cellpadding="0" width="100%">
  	<tr>
  		<td align="right" valign="top" width="85%" style="color:red;font-size:12;"><b><%=inqBasic.getE02PENDAP()%></b></td>
  		<td width="5%"><h6>&nbsp;</h6></td>
  	</tr>
  </table>
  <% } %>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="14%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="9%" > 
              <div align="left"> 
                <input type="text" name="E02CUN2" size="10" maxlength="9" readonly value="<%= userPO.getHeader2().trim()%>">
              </div>
            </td>
            <td nowrap width="12%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E02NA12" size="48" maxlength="45" readonly value="<%= userPO.getHeader3().trim()%>">
              </div>
            </td>
            <td nowrap > 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap ><b> 
              <input type="text" name="E02PRO2" size="4" maxlength="4" readonly value="<%= userPO.getHeader1().trim()%>">
              </b></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="14%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="9%"> 
              <div align="left"> 
                <input type="text" name="E02ACC" size="13" maxlength="12" value="<%= userPO.getIdentifier().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="12%"> 
              <div align="right">Oficial :</div>
            </td>
            <td nowrap width="33%"> 
              <div align="left"><b> 
                <input type="text" name="E02NA122" size="48" maxlength="45" readonly value="<%= userPO.getOfficer().trim()%>">
                </b> </div>
            </td>
            <td nowrap width="11%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="21%"> 
              <div align="left"><b> 
         	  <eibsinput:text name="inqBasic" property="E02DEACCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" readonly="true"/>   
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
<table class=tbenter>
   <tr > 
      <td nowrap width="30%"> 
   		<h4>Informaci&oacute;n B&aacute;sica</h4>
      </td>
      <td nowrap align=left width="35%"> 
   		<b>CREDITO :<font color="#ff6600"> <% 	if (inqBasic.getE02COLATR().trim().equals("") )
   													out.print("SIN GARANTIA");
   												else
   													out.print(inqBasic.getE02COLATR());
   											%></font></b>
      </td>
      <td nowrap align=left width="35%"> 
   		<b>ESTADO :<font color="#ff6600"><%= inqBasic.getE02STATUS().trim()%></font></b>
      </td>
    </tr>
  </table>
  
  <table class="tableinfo">
    <tr> 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
		<tr id="trclear"> 
            <td nowrap > 
              <div align="right">Numero Solicitud :</div>
            </td>
            <td nowrap > 
        	   <eibsinput:text name="inqBasic" property="E02DEAOFN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/> 
             </td>
            <td nowrap > 
              <div align="right">&nbsp;</div>
            </td>
            <td nowrap> 
					&nbsp;
            </td>
          </tr>        
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Monto Original :</div>
            </td>
            <td nowrap > 
        	   <eibsinput:text name="inqBasic" property="E02DEAOAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/> 
             </td>
            <td nowrap > 
              <div align="right">Fecha de Apertura :</div>
            </td>
            <td nowrap> 
              <input type="text" readonly name="E02DEAOD1" size="3" maxlength="2" value="<%= inqBasic.getE02DEAOD1().trim()%>" >
              <input type="text" readonly name="E02DEAOD2" size="3" maxlength="2" value="<%= inqBasic.getE02DEAOD2().trim()%>" >
              <input type="text" readonly name="E02DEAOD3" size="5" maxlength="4" value="<%= inqBasic.getE02DEAOD3().trim()%>" >
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Tipo de Inter&eacute;s :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEAICT" size="40" maxlength="35" 
				value="<% if (inqBasic.getE02DEAICT().equals("S")) out.print("Al Vencimiento Calendario");
							else if (inqBasic.getE02DEAICT().equals("M")) out.print("Al Vencimiento Comercial");
							else if (inqBasic.getE02DEAICT().equals("P")) out.print("Prepagados Calendario");
							else if (inqBasic.getE02DEAICT().equals("A")) out.print("Prepagados Comerciales");
							else if (inqBasic.getE02DEAICT().equals("D")) out.print("Descontados Calendario");
							else if (inqBasic.getE02DEAICT().equals("T")) out.print("Descontados Comerciales");
							else if (inqBasic.getE02DEAICT().equals("R")) out.print("Capitalizados(CD's)");
							else if (inqBasic.getE02DEAICT().equals("1")) out.print("Al Vencimiento Calendario");
							else if (inqBasic.getE02DEAICT().equals("2")) out.print("Al Vencimiento Comercial");
							else if (inqBasic.getE02DEAICT().equals("3")) out.print("Prepagados Calendario");
							else if (inqBasic.getE02DEAICT().equals("4")) out.print("Prepagados Comerciales");
							else if (inqBasic.getE02DEAICT().equals("5")) out.print("Descontados Calendario");
							else if (inqBasic.getE02DEAICT().equals("6")) out.print("Descontados Comerciales");
							else if (inqBasic.getE02DEAICT().equals("7")) out.print("Capitalizados (CD's)");
							else if (inqBasic.getE02DEAICT().equals("8")) out.print("Regla 78");
							else out.print("");%>" >
            </td>
            <td nowrap > 
              <div align="right">Fecha de Vencimiento :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEAMD1" size="3" maxlength="2" value="<%= inqBasic.getE02DEAMD1().trim()%>" >
              <input type="text" readonly name="E02DEAMD2" size="3" maxlength="2" value="<%= inqBasic.getE02DEAMD2().trim()%>" >
              <input type="text" readonly name="E02DEAMD3" size="5" maxlength="4" value="<%= inqBasic.getE02DEAMD3().trim()%>" >
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Per&iacute;odo Base :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEABAS" size="3" maxlength="3" value="<%= inqBasic.getE02DEABAS().trim()%>" >
            </td>
            <td nowrap > 
              <div align="right">T&eacute;rmino :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEATRM" size="5" maxlength="5" value="<%= inqBasic.getE02DEATRM().trim()%>">
              <input type="text" readonly name="E02DEATRC" size="10" 
				  value="<% if (inqBasic.getE02DEATRC().equals("D")) out.print("D&iacute;a(s)");
							else if (inqBasic.getE02DEATRC().equals("M")) out.print("Mes(es)");
							else if (inqBasic.getE02DEATRC().equals("Y")) out.print("A&ntilde;o(s)");
							else out.print("");%>" >
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Tabla de Tasa Flotante :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEAFTB" size="2" maxlength="2" value="<%= inqBasic.getE02DEAFTB().trim()%>">
              <input type="text" readonly name="E02DEAFTY" size="10" 
				  value="<% if (inqBasic.getE02DEAFTY().equals("FP")) out.print("Primaria");
							else if (inqBasic.getE02DEAFTY().equals("FS")) out.print("Secundaria");
							else out.print("");%>" >
	    	   <eibsinput:text name="inqBasic" property="E02FTLRTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>
            </td>
            <td nowrap > 
              <div align="right">Tasa de Inter&eacute;s/Spread :</div>
            </td>
            <td nowrap > 
        	   <eibsinput:text name="inqBasic" property="E02DEARTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">C&aacute;lculo Inter&eacute;s Normal :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEAIFL" size="40" maxlength="40" 
				  value="<% if (inqBasic.getE02DEAIFL().equals("1")) out.print("Capital Vigente");
							else if (inqBasic.getE02DEAIFL().equals("2")) out.print("Capital Original");
							else if (inqBasic.getE02DEAIFL().equals("3")) out.print("Capital Vigente - Capital Vencido");
							else if (inqBasic.getE02DEAIFL().equals("4")) out.print("No Calcula Intereses");
							else out.print("");%>" >
            </td>
            <td nowrap > 
              <div align="right">Tasa M&iacute;nima Permitida :</div>
            </td>
            <td nowrap > 
        	   <eibsinput:text name="inqBasic" property="E02DEAMIR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>
           </td>
          </tr>
         
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">C&aacute;lculo Inter&eacute;s Mora :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEAPCL" size="25" maxlength="25" 
				  value="<% if (inqBasic.getE02DEAPCL().equals("1")) out.print("Capital Vencido");
							else if (inqBasic.getE02DEAPCL().equals("2")) out.print("Capital Original");
							else if (inqBasic.getE02DEAPCL().equals("3")) out.print("Capital Vigente");
							else if (inqBasic.getE02DEAPCL().equals("4")) out.print("Capital Vencido + Intereses Vencido");
							else if (inqBasic.getE02DEAPCL().equals("5")) out.print("Capital Vencido + Intereses + Seguro Vencidos");
							else out.print("No Calcula Intereses de Mora");%>" >
              Gracia : 
              <input type="text" readonly name="E02DEAGPD" size="2" maxlength="2" value="<%= inqBasic.getE02DEAGPD().trim()%>" readonly="true"/> 
            </td>
            <td nowrap > 
              <div align="right">Tasa M&aacute;xima Permitida :</div>
            </td>
            <td nowrap > 
        	   <eibsinput:text name="inqBasic" property="E02DEAMXR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>
           </td>                          
          </tr>
        
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Tabla de Cargos :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="inqBasic" property="E02DEATLN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TABLE %>" readonly="true"/>
            </td>
            <td nowrap > 
              <div align="right">Inter&eacute;s de Mora :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEAPEI" size="7" maxlength="7" value="<%= inqBasic.getE02DEAPEI().trim()%>">
              <input type="text" readonly name="E02DEAPIF" size="2" maxlength="1" value="<%= inqBasic.getE02DEAPIF().trim()%>">
              <input type="text" readonly name="E02DLCPR3" size="7" maxlength="7" value="<%= inqBasic.getE02DLCPR3().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Banco / Sucursal :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="inqBasic" property="E02DEABNK" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BANK %>" readonly="true"/>
              / 
              <eibsinput:text name="inqBasic" property="E02DEABRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
            </td>
            <td nowrap >
              <div align="right">Cuenta Contable :</div>
            </td>
            <td nowrap >
               <eibsinput:text name="inqBasic" property="E02DEAGLN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER %>" readonly="true"/>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Tasa de Cambio :</div>
            </td>
            <td nowrap > 
             <eibsinput:text name="inqBasic" property="E02DEAEXR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_EXCHANGE_RATE %>" readonly="true"/> 
           </td>
            <td nowrap > 
              <div align="right">Centro de Costos :</div>
            </td>
            <td nowrap > 
             <eibsinput:text name="inqBasic" property="E02DEACCN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_COST_CENTER %>" readonly="true"/> 
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Contrapartida :</div>
            </td>
            <td nowrap > 
              <eibsinput:text name="inqBasic" property="E02DEAOFB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BANK %>" readonly="true"/>
              <eibsinput:text name="inqBasic" property="E02DEAOCR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
              <eibsinput:text name="inqBasic" property="E02DEAOCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" readonly="true"/>
              <eibsinput:text name="inqBasic" property="E02DEAOGL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>" readonly="true"/>
              <eibsinput:text name="inqBasic" property="E02DEAOAC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
            </td>
            <td nowrap > 
              <div align="right">L&iacute;nea de Cr&eacute;dito :</div>
            </td>
            <td nowrap > 
             <eibsinput:text name="inqBasic" property="E02DEACMC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" /> 
             <eibsinput:text name="inqBasic" property="E02DEACMN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CREDIT_LINE %>" readonly="true" /> 
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Clase de Cr&eacute;dito :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEACLF" size="35" maxlength="30" 
				value="<% if (inqBasic.getE02DEACLF().equals("A")) out.print("Arrendamiento Financiero");
							else if (inqBasic.getE02DEACLF().equals("C")) out.print("Pr�stamos de Consumo");
							else if (inqBasic.getE02DEACLF().equals("D")) out.print("Pr�stamos sobre Saldo Disoluto");
							else if (inqBasic.getE02DEACLF().equals("L")) out.print("Pr�stamo Regular");
							else if (inqBasic.getE02DEACLF().equals("H")) out.print("Hipotecarios");
							else if (inqBasic.getE02DEACLF().equals("P")) out.print("Politica Habitacional");
							else if (inqBasic.getE02DEACLF().equals("G")) out.print("Descuento Documentos(Factoring)");
							else if (inqBasic.getE02DEACLF().equals("V")) out.print("Valores al Cobro");
							else if (inqBasic.getE02DEACLF().equals("O")) out.print("Para Control de Sobregiros");
							else if (inqBasic.getE02DEACLF().equals("R")) out.print("Pr�stamo para Refinanciar Otro");
							else if (inqBasic.getE02DEACLF().equals("I")) out.print("Pr�stamo Credilinea");
							else out.print("Proyectos de Constructor");%>" >
            </td>
            <td nowrap > 
              <div align="right">Pr&eacute;stamo a Demanda :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEALNC" size="4" 
				  value="<% if (inqBasic.getE02DEALNC().equals("Y")) out.print("Si");
							else if (inqBasic.getE02DEALNC().equals("N")) out.print("No");
							else out.print("");%>" 
				 maxlength="2">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Forma C&aacute;lculo IVA :</div>
            </td>
            <td nowrap > 
              <input type="text" readonly name="E02DEAWHF" size="35" maxlength="35" 
				value="<% if (inqBasic.getE02DEAWHF().equals("1")) out.print("Retenci�n sobre Intereses ISR");
							else if (inqBasic.getE02DEAWHF().equals("2")) out.print("Cobre del IVA");
							else if (inqBasic.getE02DEAWHF().equals("3")) out.print("IVA mas ISR");
							else if (inqBasic.getE02DEAWHF().equals("4")) out.print("IVA solo en Comisiones");
							else if (inqBasic.getE02DEAWHF().equals("5")) out.print("IVA solo en Intereses");
							else if (inqBasic.getE02DEAWHF().equals("6")) out.print("Debito Bancario IDB");
							else if (inqBasic.getE02DEAWHF().equals("7")) out.print("IDB mas ISR");
							else if (inqBasic.getE02DEAWHF().equals("8")) out.print("IDB mas IVA");
							else if (inqBasic.getE02DEAWHF().equals("9")) out.print("Todo Tipo de Impuesto");
							else if (inqBasic.getE02DEAWHF().equals("N")) out.print("No Calcula Impuestos");
							else if (inqBasic.getE02DEAWHF().equals("F")) out.print("Cobro del FECI");
							else if (inqBasic.getE02DEAWHF().equals("G")) out.print("FECI e ITBMS(Panam�)");
							else out.print("");%>" >
            </td>
           
            
             <%  if (currUser.getE01INT().trim().equals("07")) {%>
                 <td nowrap > 
                   <div align="right">Condici&oacute;n de Cr&eacute;dito :</div>
                 </td>
                 <td width="27%" > 
                   <input type="text" readonly name="E02DEADLC" size="2" maxlength="1" value="<%= inqBasic.getE02DEADLC().trim()%>">
                 </td>
             <% } else {%> 
                <%  if (currUser.getE01INT().trim().equals("18")) {%>
                  <td nowrap width="25%" > 
                    <div align="right">Condici&oacute;n  Cr&eacute;dito :</div>
                  </td>
                  <td width="27%" > 
                    <select name="E02DEADLC" disabled>
                      <option value=" " <% if (!(inqBasic.getE02DEADLC().equals("1") ||inqBasic.getE02DEADLC().equals("2")||inqBasic.getE02DEADLC().equals("3")||inqBasic.getE02DEADLC().equals("4"))) out.print("selected"); %>></option>
                      <option value="1" <% if (inqBasic.getE02DEADLC().equals("1")) out.print("selected"); %>>Vigente</option>
                      <option value="2" <% if (inqBasic.getE02DEADLC().equals("2")) out.print("selected"); %>>Vencido</option>
                      <option value="3" <% if (inqBasic.getE02DEADLC().equals("3")) out.print("selected"); %>>Castigado</option>
                      <option value="4" <% if (inqBasic.getE02DEADLC().equals("4")) out.print("selected"); %>>Castigado N/Inf</option>
                    </select>
                   
                  </td> 
                
                <% } else {%> 
                  <td nowrap width="25%" > 
                    <div align="right">Condici&oacute;n  Cr&eacute;dito :</div>
                  </td>
                  <td width="27%" > 
                    <select name="E02DEADLC" disabled>
                      <option value=" " <% if (!(inqBasic.getE02DEADLC().equals("1")||inqBasic.getE02DEADLC().equals("2")||inqBasic.getE02DEADLC().equals("3")||inqBasic.getE02DEADLC().equals("4")||inqBasic.getE02DEADLC().equals("5")||inqBasic.getE02DEADLC().equals("6")||inqBasic.getE02DEADLC().equals("7"))) out.print("selected"); %>></option>
                      <option value="1" <% if (inqBasic.getE02DEADLC().equals("1")) out.print("selected"); %>>Vigente</option>
                      <option value="2" <% if (inqBasic.getE02DEADLC().equals("2")) out.print("selected"); %>>Vencido</option>
                      <option value="3" <% if (inqBasic.getE02DEADLC().equals("3")) out.print("selected"); %>>Castigado</option>
                      <option value="4" <% if (inqBasic.getE02DEADLC().equals("4")) out.print("selected"); %>>Castigado N/Inf</option>
                      <option value="5" <% if (inqBasic.getE02DEADLC().equals("5")) out.print("selected"); %>>Acelerado</option>
                      <option value="6" <% if (inqBasic.getE02DEADLC().equals("6")) out.print("selected"); %>>Vencido Anticipado</option>
                      <option value="7" <% if (inqBasic.getE02DEADLC().equals("7")) out.print("selected"); %>>Ejecuci�n Prejudicial</option>
                    </select>
                  
                  </td>
                <% }%>   
          
             <% }%>   
            
           
          </tr>
          <tr id="trdark">
            <td nowrap >
              <div align="right">Tipo de Relaci&oacute;n 1 :</div>
            </td>
            <td nowrap >
              <INPUT type="text" readonly name="E02DEAPAR" size="25" maxlength="25" value='<% if (inqBasic.getE02DEAPAR().equals("A")) out.print("Arrendamiento con Capitalizaci�n");
							else if (inqBasic.getE02DEAPAR().equals("B")) out.print("Arrendamiento con Capitalizaci�n");
							else if (inqBasic.getE02DEAPAR().equals("F")) out.print("Fondeo");
							else if (inqBasic.getE02DEAPAR().equals("G")) out.print("Administraci�n de Fondos");
							else if (inqBasic.getE02DEAPAR().equals("P")) out.print("Participaci�n");
							else if (inqBasic.getE02DEAPAR().equals("O")) out.print("Proyectos de Constructor");
							else if (inqBasic.getE02DEAPAR().equals("S")) out.print("Sindicaci�n");
							else if (inqBasic.getE02DEAPAR().equals("T")) out.print("Indexado a Certificado");
							else if (inqBasic.getE02DEAPAR().equals("1")) out.print("Tasa m�s alta de Certificado");
							else if (inqBasic.getE02DEAPAR().equals("2")) out.print("Tasa m�s alta de Ahorros");
							else if (inqBasic.getE02DEAPAR().equals("3")) out.print("Tasa m�s alta de CDS / Ahorros");
							else out.print("N/A");%>'>
              <eibsinput:text name="inqBasic" property="E02DEAPAC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
		    </td>
            <td nowrap >
              <div align="right">N&uacute;mero de Referencia :</div>
            </td>
            <td nowrap >
              <input type="text" readonly name="E02DEAREF" size="12" maxlength="12" value="<%= inqBasic.getE02DEAREF().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap >
              <div align="right">Tipo de Relaci&oacute;n 2 :</div>
            </td>
            <td nowrap >
               <INPUT type="text" readonly name="E02DEARET" size="25" maxlength="25" value='<% if (inqBasic.getE02DEARET().equals("P")) out.print("Pago sobre Pr�stamos");
							else if (inqBasic.getE02DEARET().equals("R")) out.print("Cr�dito Renovado");
							else if (inqBasic.getE02DEARET().equals("E")) out.print("Cr�dito Reestructurado");
							else if (inqBasic.getE02DEARET().equals("F")) out.print("Refinanciar sobre otro Cr�dito");
							else out.print("N/A");%>'>
              <eibsinput:text name="inqBasic" property="E02DEAREA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
			</td>
            <td nowrap >
              <div align="right">Direcciones de Correo :</div>
            </td>
            <td nowrap width="23%" height="19"><INPUT type="text" readonly name="E02DEAMLA" size="2" maxlength="2" value="<%= inqBasic.getE02DEAMLA().trim()%>"></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap >
              <div align="right">Vendedor :</div>
            </td>
            <td nowrap >
	             <eibsinput:text name="inqBasic" property="E02DEABRK" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER %>" readonly="true"/> 
	             <eibsinput:text name="inqBasic" property="E02DSCBRK" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="true"/> 
	            </td>
            <td nowrap >
              <div align="right">Comisi&oacute;n Vendedor :</div>
            </td>
            <td nowrap >
	             <eibsinput:text name="inqBasic" property="E02DEABCP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/> 
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap >
              <div align="right">Clave de Descuento :</div>
            </td>
            <td nowrap >
	           <input type="text" readonly name="E02DEACUI" size="6" value="<%= inqBasic.getE02DEACUI().trim()%>">
	           <eibsinput:text name="inqBasic" property="D02DEACUI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="true"/> 
            </td>
            <td nowrap >
            <div align="right">Fecha Reestruc/Refinanc :</div>
            </td>
            <td nowrap >
              <input type="text" name="E02DEAEX1" size="3" maxlength="2" value="<%= inqBasic.getE02DEAEX1().trim()%>" onchange="setRecalculate()"   >
              <input type="text" name="E02DEAEX2" size="3" maxlength="2" value="<%= inqBasic.getE02DEAEX2().trim()%>" onchange="setRecalculate()"   >
              <input type="text" name="E02DEAEX3" size="5" maxlength="4" value="<%= inqBasic.getE02DEAEX3().trim()%>" onchange="setRecalculate()"   >
            </td>
          </tr>
			
    	  <%if(!(inqBasic.getE02DEASCA().trim().equals("0.00"))){ %>	
          <tr id="trdark"> 
            <td nowrap >
              <div align="right">Tasa de Comisi&oacute;n :</div>
            </td>
            <td nowrap >
                 <input type="text" readonly name="E02DEASCA" size="5" maxlength="5" value="<%= inqBasic.getE02DEASCA().trim()%>" readonly>
           </td>
            <td nowrap >
              <div align="right">Destino del Cr&eacute;dito :</div>
            </td>
            <td nowrap >
	             <eibsinput:text name="inqBasic" property="E02DESTSF" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="true"/> 
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap >
              <div align="right">% de Garant&iacute;a :</div>
            </td>
            <td nowrap >
	           <eibsinput:text name="inqBasic" property="E02DEACFA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TABLE %>" readonly="true"/> 
            </td>
            <td nowrap >
              <div align="right">Venta Total Empresa :</div>
            </td>
            <td nowrap >
	           <eibsinput:text name="inqBasic" property="E02PVDCPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/> 
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap >
              <div align="right"></div>
            </td>
            <td nowrap >
            </td>
            <td nowrap >
              <div align="right">Tipo Beneficiario :</div>
            </td>
            <td nowrap >
	           <eibsinput:text name="inqBasic" property="E02PVDSTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/> 
            </td>
          </tr>
    	  <%} %>
          <tr id="trdark"> 
            <td nowrap >
              <div align="right">Fecha Mora Original :</div>
            </td>
            <td nowrap > 
           		<input type="text" name="E02DLCD1D" size="3" maxlength="2" value="<%= inqBasic.getE02DLCD1D().trim()%>" readonly>
                <input type="text" name="E02DLCD1M" size="3" maxlength="2" value="<%= inqBasic.getE02DLCD1M().trim()%>" readonly>
                <input type="text" name="E02DLCV1Y" size="5" maxlength="4" value="<%= inqBasic.getE02DLCV1Y().trim()%>" readonly>
            </td>
            <td nowrap >
            <div align="right"></div>
            </td>
            <td nowrap >
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Condiciones de Pagos</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="27%" > 
              <div align="right">Cuenta Contable a Debitar :</div>
            </td>
            <td nowrap width="26%"> 
              <eibsinput:text name="inqBasic" property="E02DEAREB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BANK %>" readonly="true"/>
              <eibsinput:text name="inqBasic" property="E02DEARPR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
              <eibsinput:text name="inqBasic" property="E02DEARPC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" readonly="true"/>
              <eibsinput:text name="inqBasic" property="E02DEARGL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>" readonly="true"/>
            </td>
            <td nowrap width="21%" > 
 	            <div align="right">Cuenta Detalle a Debitar :</div>
             </td>
            <td nowrap colspan="7"> 
			   <eibsinput:text name="inqBasic" property="E02DEARAC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="27%" > 
              <div align="right">Ciclo / Fecha Refinancia Int. :</div>
            </td>
            <td nowrap width="26%"> 
              <input type="text" readonly name="E02DEAXRC" size="3" maxlength="3" value="<%= inqBasic.getE02DEAXRC().trim()%>" >
              / 
              <input type="text" readonly name="E02DEAXR1" size="3" maxlength="2" value="<%= inqBasic.getE02DEAXR1().trim()%>" >
              <input type="text" readonly name="E02DEAXR2" size="3" maxlength="2" value="<%= inqBasic.getE02DEAXR2().trim()%>" >
              <input type="text" readonly name="E02DEAXR3" size="5" maxlength="4" value="<%= inqBasic.getE02DEAXR3().trim()%>" >
            </td>
            <td nowrap width="21%" > 
              <div align="right">Refinanciar Hasta :</div>
            </td>
            <td nowrap colspan="7"> 
              <input type="text" readonly name="E02DEAPC1" size="3" maxlength="2" value="<%= inqBasic.getE02DEAPC1().trim()%>" >
              <input type="text" readonly name="E02DEAPC2" size="3" maxlength="2" value="<%= inqBasic.getE02DEAPC2().trim()%>" >
              <input type="text" readonly name="E02DEAPC3" size="5" maxlength="4" value="<%= inqBasic.getE02DEAPC3().trim()%>" >
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="27%" > 
              <div align="right">Monto Pago Final :</div>
            </td>
            <td nowrap width="26%" > 
             <eibsinput:text name="inqBasic" property="E02DEABAP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/> 
            </td>
            <td nowrap width="21%" > 
              <div align="right">Fecha Pago Final :</div>
            </td>
            <td nowrap  colspan="7"> 
              <input type="text" readonly name="E02DEABA1" size="3" maxlength="2" value="<%= inqBasic.getE02DEABA1().trim()%>" >
              <input type="text" readonly name="E02DEABA2" size="3" maxlength="2" value="<%= inqBasic.getE02DEABA2().trim()%>" >
              <input type="text" readonly name="E02DEABA3" size="5" maxlength="4" value="<%= inqBasic.getE02DEABA3().trim()%>" >
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="27%" > 
              <div align="right">Mes Excluido en Pago :</div>
            </td>
            <td nowrap width="26%" > 
              <input type="text" readonly name="E02DEARPT" size="2" maxlength="2" value="<%= inqBasic.getE02DEARPT().trim()%>" >
            </td>
            <td nowrap width="21%" > 
              <div align="right">Cobro de FECI :</div>
            </td>
            <td nowrap colspan="7"> 
              <input type="text" readonly name="E02DEATX2" size="2" maxlength="2" value="<%= inqBasic.getE02DEATX2().trim()%>" >
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="27%" height="19"> 
               <div align="right">Autoriza Sobregiro :</div>
            </td>
            <td nowrap width="26%" height="19">
              <input type="text" readonly name="E02DEAODA" size="2"  value="<%= inqBasic.getE02DEAODA().trim()%>"  maxlength="2">
			</td>
            <td nowrap width="21%" > 
              <div align="right">Cuota Incluye Interes : </div>
            </td>
            <td nowrap colspan="7"> 
			  <input type="text" readonly name="E02DEAIIP" size="2" value="<%= inqBasic.getE02DEAIIP().trim()%>"  maxlength="2"> 
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="27%" > 
              <div align="right">% Programa / Incremento Pago :</div>
            </td>
            <td nowrap width="26%" > 
			  <eibsinput:text name="inqBasic" property="E02DEAPAP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="true"/>
              / 
              <input type="text" readonly name="E02DEA2TC" size="2" maxlength="1" value="<%= inqBasic.getE02DEA2TC().trim()%>" >
            </td>
            <td nowrap width="21%" > 
              <div align="right">Ciclo/ Pr&oacute;x. Incr. Pago :</div>
            </td>
            <td nowrap  colspan="7">
              <input type="text" readonly name="E02DEAPCU" size="3" maxlength="3" value="<%= inqBasic.getE02DEAPCU().trim()%>" >
              / 
              <input type="text" readonly name="E02DEALS1" size="3" maxlength="2" value="<%= inqBasic.getE02DEALS1().trim()%>" >
              <input type="text" readonly name="E02DEALS2" size="3" maxlength="2" value="<%= inqBasic.getE02DEALS2().trim()%>" >
              <input type="text" readonly name="E02DEALS3" size="5" maxlength="4" value="<%= inqBasic.getE02DEALS3().trim()%>" >
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="27%" > 
              <div align="right">Abonos a Capital :</div>
            </td>
            <td nowrap width="26%" > 
              <input type="text" readonly name="E02DLCABC" size="3" maxlength="3" value="<%= inqBasic.getE02DLCABC().trim()%>" >
              <input type="text" readonly name="E02DLCABF" size="2" maxlength="1" value="<%= inqBasic.getE02DLCABF().trim()%>" >
            </td>
            <td nowrap width="21%" > 
              <div align="right">Ciclo/ Pr&oacute;x. Abono Capital :</div>
            </td>
            <td nowrap  colspan="7">
              <input type="text" readonly name="E02DLCABC" size="3" maxlength="3" value="<%= inqBasic.getE02DLCABC().trim()%>" >
              / 
              <input type="text" readonly name="E02DLCAB1" size="3" maxlength="2" value="<%= inqBasic.getE02DLCAB1().trim()%>" >
              <input type="text" readonly name="E02DLCAB2" size="3" maxlength="2" value="<%= inqBasic.getE02DLCAB2().trim()%>" >
              <input type="text" readonly name="E02DLCAB3" size="5" maxlength="4" value="<%= inqBasic.getE02DLCAB3().trim()%>" >
            </td>
          </tr>
        <tr id="trclear">
		 <td nowrap width="24%">
			<div align="right">Tipo de Pago :</div>
				</td>
					<td nowrap width="26%">
						<input type="hidden"  name="E02DLCPVI" value="<%= inqBasic.getE02DEAPVI().trim()%>" >
						<input type="text" readonly size="25"  value=' <% if (inqBasic.getE02DEAPVI().equals("")) 
											out.print("Caja");
										else if (inqBasic.getE02DEAPVI().equals("1")) 
											out.print("PAC/Automatico");
										else if (inqBasic.getE02DEAPVI().equals("2")) 
											out.print("Convenio");
										else if (inqBasic.getE02DEAPVI().equals("4")) 
											out.print("PAC Multibanco");
										else 	
											out.print("");%>'>
								</td>
				<td nowrap width="24%">
				<div align="right">Codigo de Convenio :</div>
				</td>
					<td nowrap width="23%">
						<input type="text" size="5" maxlength="4" name="E02DLCCNV" readonly value="<%=inqBasic.getE02DEACNV().trim()%>">
					</td>
			</tr>
	         <tr id=trdark>
 			<td nowrap align="right" width="24%">                 
				Centro Responsabilidad :		  			   
 			</td>          
 			<td nowrap width="26%" >                 
 				<input type="text" name="E02DLCRES" size="5" maxlength="4" value="<%= inqBasic.getE02DLCRES().trim() %>" readonly>
                <input type="text" name="D02DLCRES" size="36" maxlength="35" value="<%= inqBasic.getD02DLCRES().trim() %>"readonly> 
            </td>	         
            <td nowrap align="right" width="24%">Fecha Gesti&oacute;n Cobranza :
            </td>
 			<td nowrap width="23%">                
			  <input type="text" name="E02DLCFCM" size="3" maxlength="2" value="<%= inqBasic.getE02DLCFCM().trim()%>" readonly>
              <input type="text" name="E02DLCFCD" size="3" maxlength="2" value="<%= inqBasic.getE02DLCFCD().trim()%>" readonly>
              <input type="text" name="E02DLCFCY" size="5" maxlength="4" value="<%= inqBasic.getE02DLCFCY().trim()%>" readonly>		  			  
 			</td>

 		  </tr>  
        </table>
      </td>
    </tr>
  </table>

  <h4>Prioridad de Pagos </h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="14%"  > 
              <div align="center"> 
                <input type="text" name="E02DEAPP1" size="2" maxlength="1" value="<%= inqBasic.getE02DEAPP1().trim()%>">
              </div>
            </td>
            <td nowrap width="14%"  > 
              <div align="center"> 
                <input type="text" name="E02DEAPP2" size="2" maxlength="1" value="<%= inqBasic.getE02DEAPP2().trim()%>">
              </div>
            </td>
            <td nowrap width="14%" > 
              <div align="center"> 
                <input type="text" name="E02DEAPP3" size="2" maxlength="1" value="<%= inqBasic.getE02DEAPP3().trim()%>">
              </div>
            </td>
			 <%if(currUser.getE01INT().equals("07")){%> 
	            <td nowrap width="14%"  > 
	              <div align="center"> 
	                <input type="text" name="E02DEAPP7" size="2" maxlength="1" value="<%= inqBasic.getE02DEAPP7().trim()%>">
	              </div>
	            </td>
			  <% } %>
            <td nowrap width="14%"  > 
              <div align="center"> 
                <input type="text" name="E02DEAPP4" size="2" maxlength="1" value="<%= inqBasic.getE02DEAPP4().trim()%>">
              </div>
            </td>
            <td nowrap width="14%" > 
              <div align="center"> 
                <input type="text" name="E02DEAPP5" size="2" maxlength="1" value="<%= inqBasic.getE02DEAPP5().trim()%>">
              </div>
            </td>
            <td nowrap width="14%"  > 
              <div align="center"> 
                <input type="text" name="E02DEAPP6" size="2" maxlength="1" value="<%= inqBasic.getE02DEAPP6().trim()%>">
              </div>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="14%" > 
              <div align="center">Principal</div>
            </td>
            <td nowrap width="14%" > 
              <div align="center">Intereses</div>
            </td>
            <td nowrap width="14%" > 
              <div align="center">Mora</div>
            </td>
			 <%if(currUser.getE01INT().equals("07")){%> 
	            <td nowrap width="14%" > 
	              <div align="center">FECI</div>
	            </td>
			  <% } %>
            <td nowrap width="14%" > 
              <div align="center">Comisiones</div>
            </td>
            <td nowrap width="14%" > 
              <div align="center">Impuestos</div>
            </td>
            <td nowrap width="14%" > 
              <div align="center">Deducciones</div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>  
  <p align="center">&nbsp; </p>
  </form>
</body>
</html>
             