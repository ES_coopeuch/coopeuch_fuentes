<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<html>
<head>
<title>Manteción de Comisiones</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="refComi" class="datapro.eibs.beans.EDD232001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

function cancel() {
	document.forms[0].SCREEN.value = 100;
	document.forms[0].submit();
}

function ChgTipCobro(TipComi) {
	
	if(TipComi=='1')
	{
			CodTipCobro = document.getElementById('E01CMTMTCB');
			CodTipCobro.value="";  
			
			DesTipCobro = document.getElementById('E01CMTMTBG');
			DesTipCobro.value="";  
			
			ImgTipCob = document.getElementById('imgtipcob');
			ImgTipCob.style.visibility = 'hidden'; 

			LnkTipCob = document.getElementById('lnktipcob');
			LnkTipCob.onclick=function(){ return false}; 


	}		
	else
	{
			
			ImgTipCob = document.getElementById('imgtipcob');
			ImgTipCob.style.visibility = 'visible'; 

			LnkTipCob = document.getElementById('lnktipcob');
			LnkTipCob.onclick=function(){ return true}; 


	}		

}





</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "NEW".equals(userPO.getPurpose()) ? "" : "readonly";
     
%>


<H3 align="center">

<% if(userPO.getPurpose().equals("NEW")){ %>
Nueva Comisi&oacute;n
<% } else {%>
Mantenci&oacute;n Comisi&oacute;n
<% } %>
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="commission_maitenance, EDD2320"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2320" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  
  <h4>Datos Comisi&oacute;n</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Comisi&oacute;n :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01CMTMCCM" size="5" maxlength="4" value="<%= refComi.getE01CMTMCCM().trim()%>"  <%=readonly%>>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Descripci&oacute;n  :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E01CMTMDCM" size="31" maxlength="30" value="<%= refComi.getE01CMTMDCM().trim()%>" >
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Tipo de Comisi&oacute;n :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMTCM" size="2" maxlength="1" value="<%= refComi.getE01CMTMTCM().trim()%>" readonly onfocus="ChgTipCobro(this.value);" >
                <input type="text" name="E01CMTMTCG" size="31" maxlength="30" value="<%= refComi.getE01CMTMTCG().trim()%>" readonly>
				<a href="javascript:GetCodeDescCNTIN('E01CMTMTCM','E01CMTMTCG','115')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
            <td nowrap> 
              <div align="right">Tipo Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" id="E01CMTMTCB" name="E01CMTMTCB" size="2" maxlength="1" value="<%= refComi.getE01CMTMTCB().trim()%>" readonly>
                <input type="text" id="E01CMTMTBG" name="E01CMTMTBG" size="31" maxlength="30" value="<%= refComi.getE01CMTMTBG().trim()%>" readonly>
				<a id="lnktipcob" href="javascript:GetCodeDescCNTIN('E01CMTMTCB','E01CMTMTBG','117')"><img id="imgtipcob" src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Periocidad de Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMPCB" size="2" maxlength="1" value="<%= refComi.getE01CMTMPCB().trim()%>" readonly>
                <input type="text" name="E01CMTMPCG" size="31" maxlength="30" value="<%= refComi.getE01CMTMPCG().trim()%>" readonly>
				<a href="javascript:GetCodeDescCNTIN('E01CMTMPCB','E01CMTMPCG','116')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Modalidad de Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01CMTMMCB" size="2" maxlength="1" value="<%= refComi.getE01CMTMMCB().trim()%>"  readonly>
                <input type="text" name="E01CMTMMCG" size="31" maxlength="30" value="<%= refComi.getE01CMTMMCG().trim()%>" readonly>
				<a href="javascript:GetCodeDescCNTIN('E01CMTMMCB','E01CMTMMCG','118')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
          </tr> 

          <tr id="trdark"> 
            <td height="23"> 
              <div align="right">D&iacute;a de C&aacute;lculo :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
	  			<select name="E01CMTMDCC">  
				  <%for(int contador = 0; contador < 32; contador++) {%>
                  <option value="<%=contador%>" <% if (refComi.getE01CMTMDCC().equals(Integer.toString(contador))) out.print("selected"); %>><%=contador%></option>                   
				  <%}%>
                </select>                
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">D&iacute;a de Cobro :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
	  			<select name="E01CMTMDAP">  
				  <%for(int contador = 0; contador < 32; contador++) {%>
                  <option value="<%=contador%>" <% if (refComi.getE01CMTMDAP().equals(Integer.toString(contador))) out.print("selected"); %>><%=contador%></option>                   
				  <%}%>
                </select>                
              </div>
            </td>
          </tr> 
          
          <tr id="trclear"> 
            <td nowrap  height="23"> 
              <div align="right">Impuesto :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
	  			<select name="E01CMTMIPT">  
                  <option value="Y" <% if (refComi.getE01CMTMIPT().equals("Y")) out.print("selected"); %>>S</option>                   
                  <option value="N" <% if (!refComi.getE01CMTMIPT().equals("Y")) out.print("selected"); %>>N</option>
                </select>                
                
              </div>
            </td>
            <td nowrap height="23"> 
            </td>
            <td nowrap> 
              <div align="left"> 
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <div align="center">
    <input id="EIBSBTN" type="submit" name="Enviar" value="Enviar" >
    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel()">
  </div>
  </form>

</body>
<SCRIPT LANGUAGE="JavaScript">

ChgTipCobro('<%= refComi.getE01CMTMTCM().trim()%>'); 

</SCRIPT>
</html>
