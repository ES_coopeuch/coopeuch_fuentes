/*
 * Created on Apr 7, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package datapro.eibs.products;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELC020001Message;
import datapro.eibs.beans.ELC022001Message;
import datapro.eibs.beans.ELC022002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageContextHandler;

/**
 * @author gmartinez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JSELC0220 extends SuperServlet {
		
	String LangPath = "s/";
	
	public JSELC0220() {
		super();
	}
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		HttpSession session = (HttpSession) req.getSession(false);
		Socket s = null;
		MessageContext mc = null;
		
		if (session == null) {
			try {
				res.setContentType("text/html");
				super.printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {
			int screen = -1;

			ESS0030DSMessage user = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");
			// Here we should get the path from the user profile
			LangPath = rootPath + user.getE01LAN() + "/";
			
			try {
				s = new Socket(hostIP, getInitSocket(req) + 1);
				s.setSoTimeout(sckTimeOut);
				mc =
					new MessageContext(
						new DataInputStream(new BufferedInputStream(s.getInputStream())),
						new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
						"datapro.eibs.beans");
						
				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
					flexLog("Screen  Number: " + screen);
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}
				
				String PageToCall = "";

				switch (screen) {
					case 3 : // ENTER / CALL LIST (from side menu)
					//case 6 : // ENTER / CALL LIST (from maintenance help)
						requestList(mc, user, req, res, session, screen);
						break;
					/*case 5 : // Call basic page from List
						requestNegotiation(mc, user, req, res, session, screen);
						break;*/
					case 8 : // Call basic page from optMenu
						requestInq(mc, user, req, res, session, screen);
						break;					
					case 2 :
						requestAction(mc, user, req, res, session, screen);
						break;
					default :
						PageToCall = "MISC_not_available.jsp";
						callPage(PageToCall, req, res);
						break;
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				res.sendRedirect(srctx + LangPath + sckNotRespondPage);
			} finally {
				if (s != null) s.close();
				flexLog("Socket used by JSELC0220 closed.");
			}
		}	
	}			

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void requestAction(MessageContext mc,ESS0030DSMessage user,HttpServletRequest req,HttpServletResponse res,HttpSession ses, int screen) throws IOException, ServletException {
		
		ELC022002Message msg02 = null;
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			msg02 = (ELC022002Message) msgHandle.initMessage("ELC022002", user.getH01USR(), "0002");
			//initTransaction(String.valueOf(screen), "");
			msgHandle.setFieldsFromPage(req, msg02);
			msgHandle.sendMessage(msg02);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			requestList(mc, user, req, res, ses, 3); //Request Approval List
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void requestInq(MessageContext mc,ESS0030DSMessage user,HttpServletRequest req,HttpServletResponse res,HttpSession ses,int screen) throws IOException, ServletException {				
		
		String PageToCall = "ELC0220_lc_doc_info_readonly.jsp";
		ELC020001Message msg = null;
		UserPos userPO = null;
		
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			msg = (ELC020001Message) msgHandle.initMessage("ELC020001", user.getH01USR(), "0002");
			initTransaction(String.valueOf(screen), "", ses);
			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			try {				
				msg.setE01LCMACC(req.getParameter("E02LCRNUM"));
				msg.setE01LCIDNO(req.getParameter("E02DRWNUM"));
			} catch (Exception e) {
				flexLog("E01LCIDNO not available from page");
				e.printStackTrace();
			}
			msgHandle.sendMessage(msg);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			msg = (ELC020001Message) msgHandle.receiveMessage();
			
			flexLog("Putting java beans into the session");				
			ses.setAttribute("userPO", userPO);
			ses.setAttribute("msg01", msg);				
			
			callPage(PageToCall, req, res);
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void requestList(MessageContext mc,ESS0030DSMessage user,HttpServletRequest req,HttpServletResponse res,HttpSession ses,int screen) throws IOException, ServletException {		
		
		ELC022001Message msg01 = null;
		JBObjList jbList = null;
		
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			msg01 = (ELC022001Message) msgHandle.initMessage("ELC022001", user.getH01USR(), "0015");
			
			initTransaction(String.valueOf(screen), "", ses);
			msgHandle.sendMessage(msg01);
			jbList = msgHandle.receiveMessageList("H01FLGMAS");
			
			flexLog("Putting java beans into the session");				
			if (jbList != null) ses.setAttribute("jbList", jbList);
			
			callPage("ELC0220_apr_list.jsp", req, res);
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	private void initTransaction(String optMenu, String purpose, HttpSession ses) {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		if (!optMenu.equals("")) userPO.setOption(optMenu);
		if (!purpose.equals("")) userPO.setPurpose(purpose);
	}
	
	public void callPage(String page, HttpServletRequest req, HttpServletResponse res) {
		try {
			super.callPage(LangPath + page, req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e.toString() + e.getMessage());
		}
		return; 
	}
}
