package datapro.eibs.menu;

/**
 * This type was created by Orestes Garcia.
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.security.HistUserAccessEntry;
import com.datapro.security.UserRegistryEibsImpl;

import datapro.eibs.master.ServiceLocator;

public class JSLogOff extends datapro.eibs.master.SuperServlet {

private static final String REGISTRY_KEY = ServiceLocator.getInstance()
	.getDataSourceJndiName(UserRegistryEibsImpl.DB_REGISTRY);

/**
 * Insert the method's description here.
 * Creation date: (1/14/00 12:29:44 PM)
 */
public JSLogOff() {
	super();	
}
/**
 * This method was created by Orestes Garcia.
 */
public void destroy() {

	flexLog("free resources used by JSESS0040");
	
}
/**
 * This method was created by Orestes Garcia.
 */
public void init(ServletConfig config) throws ServletException {
	super.init(config);

}

public void revokeSSO(HttpServletRequest request, HttpServletResponse response) {
	if (response != null) {
		if (false) {
			//com.ibm.websphere.security.WSSecurityHelper.revokeSSOCookies(request, response);			
		} else {
			Cookie[] cookies = request.getCookies();
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().startsWith("LtpaToken")) {
					cookies[i].setMaxAge(0);
					cookies[i].setPath("/");
					response.addCookie(cookies[i]);
				}
			}
		}
	}
	HttpSession session = (HttpSession)request.getSession(false);
	try {
		UserRegistryEibsImpl userRegistryFacade = new UserRegistryEibsImpl(
				session.getServletContext().getInitParameter("realm"));
		userRegistryFacade.initialize(REGISTRY_KEY);
		HistUserAccessEntry accessEntry = new HistUserAccessEntry();
		accessEntry.setUid(request.getRemoteUser());
		accessEntry.setPwdHistory(HistUserAccessEntry.TWO);
		userRegistryFacade.updateAccessHistory(accessEntry);
	} catch (Exception e) {
		System.out.println("The User Session couldn't be updated on DB : Cause By :" + e.getMessage());
	}
}
/**
 * This method was created by Orestes Garcia.
 * @param request HttpServletRequest
 * @param response HttpServletResponse
 */
public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

	HttpSession session = (HttpSession)req.getSession(false);
	
	if (session != null) {
		//session.invalidate();
	}
	
	if (req.getParameter("SSO") != null) {
		revokeSSO(req, res);
	}

	// set MINE type for HTTP Header
	res.setContentType("text/html");
	// get a handle to the output stream
	PrintWriter out = res.getWriter();


	out.println("<HTML>");
	out.println("<HEAD><TITLE>IBS Menu</TITLE></HEAD>");
	out.println("<BODY>");
	out.println("<SCRIPT>");
	out.println("	top.close();");
	out.println("</SCRIPT>");
	out.println("</BODY>");
	out.println("</HTML>");

	out.close();
	    
}
}