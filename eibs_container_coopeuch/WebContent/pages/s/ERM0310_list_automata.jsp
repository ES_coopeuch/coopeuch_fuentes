<!-- Hecho por Alonso Arana ------Datapro-----16/05/2014 -->
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERM031001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Par�metros de Productos de Acreencias</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="AcreListPrd" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

   $(function(){
				
					$("#radio_key").attr("checked", false);
                
				});


  function goAction(op,index) {
	var ok = false;
	var cun = "";
	var pg = "";
if(op=='20000'){
	document.forms[0].SCREEN.value = op;
			document.forms[0].submit();	
}

	if (op != '20000'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01RCCTCD") 
	      	{	
	      		if (element.checked == true) {
	      			document.getElementById("codigo_lista").value = element.value; 
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = true;
      	
      	if (op =='40000'){
      		confirm1 = confirm("Desea eliminar el registro seleccionado?");
      	}
      	
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		

     } else {
		alert("Debe seleccionar un registro para continuar.");	   
	 }    
	}



</SCRIPT>  

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Par�metros de Productos de Acreencias<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ERM310_list_automata.jsp,ERM310"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERM0310">
<input type="hidden" name="SCREEN" value="201">
<input type="hidden" name="codigo_lista" value="" id="codigo_lista">  


 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="left" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">   Banco : 
              </td>
             <td nowrap width="5%" align="left">
	  			<input type="text" name="E01ACRBNK" value="<%=session.getAttribute("codigo_banco")%>" 
	  			size="2"  readonly>
             </td>          
          <td nowrap width="10%" align="left">
	  			<input type="text" name="E01DESBNK" value="<%=session.getAttribute("desc_banco")%>" 
	  			size="50"  readonly>
             </td> 
         
         </tr>
         
      
        </table>
      </td>
    </tr>
  </table>
  

 <table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('20000')"><b>Crear</b></a>
		</td>
	
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('30000')"><b>Modificar</b></a>
		</td>
		
		
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('40000')"><b>Eliminar</b></a>
		</td>
		
		
	</tr>
</table>


<%
	if (AcreListPrd.getNoResult()) {
%>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>



<%
	
	
	} else {
%>

  <p>&nbsp;</p>
<table id="headTable"  width="" align="center">
		<tr id="trdark">
			<th align="center" nowrap width="30"></th>
			<th align="center" nowrap width="200">Producto</th>
			<th align="center" nowrap width="300">Descripci�n</th>
			<th align="center" nowrap width="100">Cargo Autom�tico</th>
				<th align="center" nowrap width="200">Fecha</th>
					<th align="center" nowrap width="200">Usuario</th>
		
			
		</tr>
		
		<%
			AcreListPrd.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (AcreListPrd.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
				ERM031001Message pvprd = (ERM031001Message) AcreListPrd.getRecord();
		%>
		<tr>
		
		<td nowrap><input type="radio" name="E01RCCTCD" id="radio_key" value="<%= AcreListPrd.getCurrentRow()%>" <%=chk%>/></td>
		<td nowrap align="center"><%=pvprd.getE01ACRCDE() %></td>
		<td nowrap align="left"><%=pvprd.getE01DESCDE() %></td>
 	  	<td nowrap align="center"><%
 	 if( pvprd.getE01ACRAUT().equals("S")){
 	 
 	 	out.print("SI");
 	 }
 	 
 	 else if( pvprd.getE01ACRAUT().equals("N")){
 	 out.print("NO");
 	 
 	 }
 	  	
 	  	 %></td>
 	  	<td nowrap align="center"><%out.print(pvprd.getE01ACRLMD()+"/"+pvprd.getE01ACRLMM()+"/"+pvprd.getE01ACRLMY()); %></td>		
 	  	<td nowrap align="left"><%=pvprd.getE01ACRLMU() %></td>		
		</tr>
		<%
			}
		%>
	</table>
<%
	}
%>
</form>
</body>
</html>
