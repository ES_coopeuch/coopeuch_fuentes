package datapro.eibs.client;

/**
 * Servlet de Plan socio.
 * Creation date: (15/02/18)  
 * @author: Jose Vivas
 */

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adobe.fdf.FDFDoc;

import datapro.eibs.beans.EDD100002Message;
import datapro.eibs.beans.EFRM00001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV100500Message;
import datapro.eibs.beans.EPV100501Message;
import datapro.eibs.beans.EPV109001Message;
import datapro.eibs.beans.ESD008001Message;
import datapro.eibs.beans.ESD403001Message;
import datapro.eibs.beans.ESD404001Message;
import datapro.eibs.beans.ESD404002Message;
import datapro.eibs.beans.ESD408001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ETE020002Message;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.reports.JSEFRM000PDF;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;

public class JSESD4040F extends JSEFRM000PDF {
	
	protected static final int R_EVALUATION_LIST = 1000;	
	protected static final int R_FORMALIZATION_LIST = 2000;
	protected static final int A_LIST = 3000;
	protected static final int R_FORMS_LIST = 4000;	
	
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_EVALUATION_LIST:
			case R_FORMALIZATION_LIST:	  
				procReqList(user, req, res, session, screen);
				break;
			case A_LIST:
				procActionList(user, req, res, session);
				break;
			case R_FORMS_LIST:
				procReqFormList(user, req, res, session);
				break;
			default :
				forward(SuperServlet.devPage, req, res);
				break;
		}		
	}

	protected void procReqFormList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		UserPos userPO = getUserPos(session);
		userPO.setCusName(userPO.getHeader3());
		
		MessageProcessor mp = null;
		try {
			
			mp = getMessageProcessor("ESD4090", req);
			
			ESD404002Message msgPersonalRelation = (ESD404002Message) mp.getMessageRecord("ESD404002");
			
			msgPersonalRelation.setH02USR(user.getH01USR());
			msgPersonalRelation.setH02PGM("ESD4090");
			msgPersonalRelation.setH02TIM(getTimeStamp());
			msgPersonalRelation.setH02SCR("01");
			msgPersonalRelation.setH02OPE("0015");
			
			mp.sendMessage(msgPersonalRelation);
			
			JBObjList list = mp.receiveMessageRecordList("H01MAS");
			
			if (!mp.hasError(list)) {
				session.setAttribute("error", mp.getError(list));
				forward("EPV1090_salesplatform_list.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");						
				session.setAttribute("pdfList", list);
				forward("ESD4040F_pdf_list.jsp", req, res);
			}
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}	
			
		finally {
			if (mp != null) mp.close();
		}
	}

	private void procActionList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EPV1005", req);
			EPV100501Message msgList = (EPV100501Message) mp.getMessageRecord("EPV100501", user.getH01USR(), "0015");
			try {
				msgList.setE01SELCUN(req.getParameter("customer_number").trim());
			} catch (Exception e) {
				msgList.setE01SELCUN(userPO.getCusNum());
			}
			msgList.setE01SELSTS("EVALUATION".equals(userPO.getPurpose()) ? "5" : "6"); //B: Ofertadas, G: Formalizadas.

			// Sends message
			mp.sendMessage(msgList);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			// Receive salesPlatform list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			//
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				forward("EPV1090_salesplatform_client_search.jsp", req, res);
			} else {
				EPV100500Message header = (EPV100500Message) list.get(0);	
				userPO.setCusNum(header.getE00CUSCUN());
				userPO.setCusType(header.getE00CUSIDN());
				userPO.setCusName(header.getE00CUSNA1());
				// save customer number
				userPO.setRedirect(srctx +  "/servlet/datapro.eibs.salesplatform.JSEPV1090?SCREEN=" + A_LIST);
				// if there are NO errors display list
				session.setAttribute("userPO", userPO);
				list.remove(0);
				session.setAttribute("EPV100501List", list);
				if (list.size() == 0) {//then no occurrences
					if (!"S".equals(req.getParameter("fin"))){//Only show when not fin
						error.setERRNUM("1");
						error.setERNU01("0001");		//4		                
						error.setERDS01("No existen solicitudes evaluadas para este Cliente");//70					
						session.setAttribute("error", error);						
					}
					forward("EPV1090_salesplatform_client_search.jsp", req, res);	
				} else {
					forward("EPV1090_salesplatform_list.jsp", req, res);
				}				
			}

		} finally {
			if (mp != null) mp.close();
		}
	}

	private void procReqList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		userPO.setPurpose(screen == R_EVALUATION_LIST ? "EVALUATION" : "FORMALIZATION");
		session.setAttribute("userPO", userPO);
		forward("EPV1090_salesplatform_client_search.jsp", req, res);
	}
}
