<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import="datapro.eibs.beans.EDD240001Message"%>
<%@ page import="datapro.eibs.beans.EDD240001Message"%>
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Generaci&oacute;n Interfaz FOGAPE</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="date" class="java.util.Date" scope="session" />      
<jsp:useBean id= "IntFog" class= "datapro.eibs.beans.EDL005501Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">

</script>

</head>
<body >
<H3 align="center">Generaci&oacute;n Interfaz FOGAPE<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="inq_fogape_Interface.jsp, EDL0055"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0055">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200"> 

<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
         <tr id="trclear">
        	     <td nowrap align="left" width="100%"> 
            	 	Se ha ejecutado el proceso de generaci&oacute;n de Interfaz FOGAPE tipo <%if(IntFog.getE01TIPREP().trim().equals("1")) out.print("Curses"); else if(IntFog.getE01TIPREP().trim().equals("1")) out.print("Pagos Cuotas"); %>
    	         </td>
			</tr>
        </table>
      </td>
    </tr>
  </table>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
