<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<title>Mantenedor de Clasificación por Rut</title>


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<SCRIPT LANGUAGE="javascript">

function validar() {
	var rut = validateRut(document.forms[0].rut.value, <%=currUser.getE01INT()%>);
	if (rut == 0) {
		alert('Debe introducir un Rut valido para continuar');
		document.forms[0].rut.focus();	
		return false;		
	} 
	//buscar que al menos un checkBox esta selected.
	document.forms[0].E01INDRUT.value = rut;
	
	return true;   
}
        
</SCRIPT>

</head>
<body>

<%
	if (!error.getERRNUM().equals("0")) {
		out.println("<script type=\"text/javascript\">");
		error.setERRNUM("0");
		out.println("showErrors()");
		out.println("</script>");
	}
%>

<h3 align="center">
Mantenedor de Clasificaci&oacute;n por Rut
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="clasrut_client_enter_search, EDET051"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSEDET051" onsubmit="return validar();">
<input type="HIDDEN" name="SCREEN" value="200">
<br>

<table id="TBHELPN" width="100%" border="0" cellspacing="0"
	cellpadding="0" style="margin-left: center; margin-right: center;">
	<tr>
		<td align="right" width="50%" nowrap>Rut :&nbsp;</td>
		<td  width="50%">
           	<input type="text" id="rut" name="E01INDRUT" maxlength="25" size="27" value="" >
           	           	           	
           	<a id="linkHelp" href="javascript:GetCustomerDescId('','','E01INDRUT')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="null" align="bottom" border="0"/></a>
           	<img src="<%=request.getContextPath()%>/images/Check.gif" align="bottom" border="0"/>
		</td>
	</tr>

</table>

  <p align="center">
  	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>

</form>
</body>
</html>
 