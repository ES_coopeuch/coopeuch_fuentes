package datapro.eibs.tools.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.datapro.eibs.tool.xml.DataBeanWrapper;
import com.datapro.eibs.tool.xml.elements.XMLElement;
import com.datapro.eibs.tool.xml.fields.FieldAmount;
import com.datapro.eibs.tool.xml.fields.FieldDate;
import com.datapro.eibs.tool.xml.fields.FieldText;
import com.datapro.exception.DocumentException;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.xml.DOMUtil;

import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;
import datapro.eibs.tools.EIBSFormsFieldGenerator;

/**
 * Class to hold an XML Document to map MessageRecords fields to XMLElement
 * fields.
 */
public class EibsFormsPdfInput extends DataBeanWrapper {

	private static final Logger log = Logger
			.getLogger(EibsFormsPdfInput.class);
	EIBSFormsFieldGenerator formats = new EIBSFormsFieldGenerator();

	/**
	 * @param stream
	 * @throws DocumentException
	 */
	public EibsFormsPdfInput(InputStream stream) throws DocumentException {
		super(stream);
	}

	/**
	 * @param source
	 * @throws DocumentException
	 */
	public EibsFormsPdfInput(InputSource source) throws DocumentException {
		super(source);		
	}

	/**
	 * @param fFile
	 * @throws FileNotFoundException
	 * @throws DocumentException
	 */
	public EibsFormsPdfInput(File fFile) throws FileNotFoundException,
			DocumentException {
		super(fFile);
	}

	protected Object getObjectFieldValue(Object obj, String fieldName)
			throws Exception {
		
		MessageRecord msg = (MessageRecord) obj;
		MessageField field = msg.getField(fieldName);
		return Util.cleanInvalidXmlCharacters(field.getString());
	}

	protected XMLElement getElement(Node textNode, Object obj) {

		Element childElement = null;
		XMLElement xmlElement = null;
		String elementField = "";

		try {
			if (textNode.getNodeType() == Node.ELEMENT_NODE) {
				Element textElement = (Element) textNode;
				NodeList textElementChildList = textElement.getChildNodes();

				String type = textElement.getAttribute("type");

				xmlElement = new XMLElement();
				xmlElement.setName(textElement.getTagName());
				xmlElement.setLanguage(getLanguage());
				
				if (type.equalsIgnoreCase("dateString")) {
					FieldDate fieldDate = new FieldDate();
					for (int i = 0; i < textElementChildList.getLength(); i++) {
						Node childNode = textElementChildList.item(i);
						String temp = "";
						if (childNode.getNodeType() == Node.ELEMENT_NODE) {
							childElement = (Element) childNode;
							if (childElement.getTagName().equals("alias")) {
								String alias = getTextNode(DOMUtil
										.getNodeChildElement(childElement,
												getLanguage(), 0));
								if (!alias.equals("")) xmlElement.setName(alias);
							} else if (childElement.getTagName().equals("if")) {
								if (opElementProcess(childNode, obj).equals("false")) {
									do {
										i++;
										try {
											childNode = textElementChildList.item(i);
											if (childNode.getNodeType() == Node.ELEMENT_NODE)
												childElement = (Element) childNode;
										} catch (Exception e) {
										}
									} while (!childElement.getTagName().equals("endif"));
								}
							} else if (childElement.getTagName().equals("format")) {
								fieldDate.setMask(getTextNode(childElement));
							} else if (childElement.getTagName().equals("field")) {
								StringTokenizer indexesString = 
									new StringTokenizer(textElement.getAttribute("indexes"), ",");
								int[] indexes = new int[3];
								for (int j = 0; j < indexes.length; j++) {
									String element = (String) indexesString.nextElement();
									indexes[j] = Integer.parseInt(element);
								}
								try {
									elementField = getTextNode(childElement);
									temp = getObjectFieldValue(obj, elementField).toString();
									fieldDate.setDate(
											temp.substring(0,indexes[0]), 
											temp.substring(indexes[0],indexes[1]), 
											temp.substring(indexes[1],indexes[2]),
											textElement.getAttribute("dateFormat"));
								} catch (Exception e) {
									log.error("Error getting field : "
											+ elementField);
								}
							}
						}
					}
					xmlElement.addValue(fieldDate);
					
				} else if (type.equalsIgnoreCase("date")) {
					FieldDate fieldDate = new FieldDate();
					String d1 = "";
					String d2 = "";
					String d3 = "";
					for (int i = 0; i < textElementChildList.getLength(); i++) {
						Node childNode = textElementChildList.item(i);
						String temp = "";
						if (childNode.getNodeType() == Node.ELEMENT_NODE) {
							childElement = (Element) childNode;
							if (childElement.getTagName().equals("alias")) {
								String alias = getTextNode(DOMUtil
										.getNodeChildElement(childElement,
												getLanguage(), 0));
								if (!alias.equals(""))
									xmlElement.setName(alias);
							} else if (childElement.getTagName().equals("if")) {
								if (opElementProcess(childNode, obj).equals("false")) {
									do {
										i++;
										try {
											childNode = textElementChildList.item(i);
											if (childNode.getNodeType() == Node.ELEMENT_NODE)
												childElement = (Element) childNode;
										} catch (Exception e) {
										}
									} while (!childElement.getTagName().equals(
											"endif"));
								}
							} else if (childElement.getTagName().equals("format")) {
								fieldDate.setMask(getTextNode(childElement));
							} else if (childElement.getTagName().equals("field")) {
								try {
									elementField = getTextNode(childElement);
									temp = getObjectFieldValue(obj,
											elementField).toString();
								} catch (Exception e) {
									log.error("Error getting field : "
											+ elementField);
								}
							}
							if (!d1.equals("") && !d2.equals("")
									&& d3.equals(""))
								d3 = temp;
							if (!d1.equals("") && d2.equals(""))
								d2 = temp;
							if (d1.equals(""))
								d1 = temp;
						}
					}
					fieldDate.setDate(d1, d2, d3);
					xmlElement.addValue(fieldDate);
				} else if (type.equalsIgnoreCase("amount")) {
					FieldAmount fieldAmount = new FieldAmount();
					for (int i = 0; i < textElementChildList.getLength(); i++) {
						Node childNode = textElementChildList.item(i);
						if (childNode.getNodeType() == Node.ELEMENT_NODE) {
							childElement = (Element) childNode;
							if (childElement.getTagName().equals("alias")) {
								String alias = getTextNode(DOMUtil
										.getNodeChildElement(childElement,
												getLanguage(), 0));
								if (!alias.equals(""))
									xmlElement.setName(alias);
							} else if (childElement.getTagName().equals("if")) {
								if (opElementProcess(childNode, obj).equals(
										"false")) {
									do {
										i++;
										try {
											childNode = textElementChildList.item(i);
											if (childNode.getNodeType() == Node.ELEMENT_NODE)
												childElement = (Element) childNode;
										} catch (Exception e) {
										}
									} while (!childElement.getTagName().equals(
											"endif"));
								}
							} else if (childElement.getTagName().equals("symbol")) {
								fieldAmount.setSymbol(getTextNode(childElement));
							} else if (childElement.getTagName().equals("length")) {
								xmlElement.setLength(Integer.parseInt(getTextNode(childElement)));
							} else if (childElement.getTagName().equals("field")) {
								try {
									elementField = getTextNode(childElement);
									fieldAmount.setAmount(getObjectFieldValue(
											obj, elementField).toString());
								} catch (Exception e) {
									log.error("Error getting field : "
											+ elementField);
								}
							}
						}
					}
					xmlElement.addValue(fieldAmount);
				} else if (type.equals("xsd:string") && textElement.getTagName().equals("xsd:element")){
					FieldText fieldText = new FieldText();
					try {
						String elementName = textElement.getAttribute("name");
						elementField = getTextNode(textElement);
						fieldText.setValue(fieldText.getValue()
								+ getObjectFieldValue(obj, elementName));
					} catch (Exception e) {
						log.error("Error getting field : " + elementField);
					}	
					xmlElement.addValue(fieldText);
				} else {
					FieldText fieldText = new FieldText();
					for (int i = 0; i < textElementChildList.getLength(); i++) {
						Node childNode = textElementChildList.item(i);
						if (childNode.getNodeType() == Node.ELEMENT_NODE) {
							childElement = (Element) childNode;
							if (childElement.getTagName().equals("alias")) {
								String alias = 
									getTextNode(
											DOMUtil.getNodeChildElement(
												childElement,
												getLanguage(), 0));
								if (!alias.equals("")) xmlElement.setName(alias);
							} else if (childElement.getTagName().equals("if")) {
								if (opElementProcess(childNode, obj).equals("false")) {
									do {
										i++;
										try {
											childNode = textElementChildList.item(i);
											if (childNode.getNodeType() == Node.ELEMENT_NODE)
												childElement = (Element) childNode;
										} catch (Exception e) {
										}
									} while (!childElement.getTagName().equals("endif"));
								}
							} else if (childElement.getTagName().equals("length")) {
								xmlElement.setLength(Integer.parseInt(getTextNode(childElement)));
							} else if (childElement.getTagName().equals("offset")) {
								xmlElement.setOffset(Integer.parseInt(getTextNode(childElement)));
							} else if (childElement.getTagName().equals("field")) {
								try {
									elementField = getTextNode(childElement);
									fieldText.setValue(fieldText.getValue()
											+ getObjectFieldValue(obj, elementField));
								} catch (Exception e) {
									log.error("Error getting field : " + elementField);
								}
							} 
						}
					}
					xmlElement.addValue(fieldText);
				}

				NamedNodeMap nodeAttributes = textElement.getAttributes();
				for (int i = 0; i < nodeAttributes.getLength(); i++) {
					Node attributeNode = nodeAttributes.item(i);
					if (attributeNode.getNodeType() == Node.ATTRIBUTE_NODE) {
						textElement.getAttribute(attributeNode.getNodeName());
						if (!attributeNode.getNodeName().equals("nillable")){
							setElement(xmlElement, attributeNode.getNodeName(),
									attributeNode.getNodeValue());
						}
					}
				}

			}
		} catch (Exception e) {
			log.error(e);
			throw new RuntimeException("Error Mapping XML file");
		}
		return xmlElement;
	}

	public BeanList getElementList(MessageRecord msg) throws IOException {

		BeanList dataList = new BeanList();
		Element element = null;
		if (getElementNode(msg.getFormatName()) == null){
			element = getElementNodeByName(formats.getFormatName(msg.getFormatName()));
		} else {
			element = getElementNode(msg.getFormatName());
		}
		NodeList elementCellsList = element.getChildNodes();
		if (elementCellsList != null) {
			for (int i = 0; i < elementCellsList.getLength(); i++) {
				Node cellNode = elementCellsList.item(i);
				XMLElement detail = getElement(cellNode, msg);
				if (detail != null)
					dataList.addRow(detail);
			}
		}
		return dataList;
	}
	
	public BeanList getXSDElementList(MessageRecord msg) throws IOException {

		BeanList dataList = new BeanList();
		Element element = null;
		if (getElementNode(msg.getFormatName()) == null){
			element = getElementNodeByName(formats.getFormatName(msg.getFormatName()));
		} else {
			element = getElementNode(msg.getFormatName());
		}
		if (element != null){
			NodeList elementCellsList = element.getChildNodes();
			if (elementCellsList != null) {
				for (int i = 0; i < elementCellsList.getLength(); i++) {
					Node cellNode = elementCellsList.item(i);
					if (cellNode.getNodeType() == Node.ELEMENT_NODE) {
					Element textElement = (Element) cellNode;
					if (textElement.getNodeName().equals("xsd:complexType")){
						NodeList list = textElement.getElementsByTagName("xsd:sequence");
						Element cellNode2 = (Element)list.item(0);
						NodeList fieldList = cellNode2.getChildNodes();
						for (int j = 0; j < fieldList.getLength(); j++) {
							Node fieldCellNode = fieldList.item(j);
							XMLElement detail = getElement(fieldCellNode, msg);
							if (detail != null)
								dataList.addRow(detail);
						}
					} else if (!textElement.getNodeName().equals("xsd:attribute")){
						XMLElement detail = getElement(cellNode, msg);
						if (detail != null)
							dataList.addRow(detail);
					}
				}
					
				}
			}
		}
		return dataList;
	}

	public BeanList getElementList(MessageRecord msg, String tagName)
			throws IOException {

		BeanList dataList = new BeanList();

		NodeList elementCellsList = getElementNode(tagName, 0).getChildNodes();
		if (elementCellsList != null) {
			for (int i = 0; i < elementCellsList.getLength(); i++) {
				Node cellNode = elementCellsList.item(i);
				XMLElement detail = getElement(cellNode, msg);
				if (detail != null)
					dataList.addRow(detail);
			}
		}
		return dataList;
	}

	public Element getElementNode(String elementId) {
		Element targetElement = null;

		NodeList elementCellsList = getDocument().getDocumentElement()
				.getChildNodes();
		for (int i = 0; i < elementCellsList.getLength(); i++) {
			Node childNode = elementCellsList.item(i);
			if (childNode.getNodeType() == Node.ELEMENT_NODE) {
				Element childElement = (Element) childNode;
				if (childElement.getAttribute("id").equals(elementId)) {
					targetElement = childElement;
					break;
				}
			}
		}
		return targetElement;
	}
	
	public Element getElementNodeByName(String elementId) {
		Element targetElement = null;

		NodeList elementCellsList = getDocument().getDocumentElement().getChildNodes();
		for (int i = 0; i < elementCellsList.getLength(); i++) {
			Node childNode = elementCellsList.item(i);
			if (childNode.getNodeType() == Node.ELEMENT_NODE) {
				Element childElement = (Element) childNode;
				if (childElement.getAttribute("name").equals(elementId)) {
					targetElement = childElement;
					break;
				}
			}
		}
		return targetElement;
	}
	
	public XMLElement initGroupElement(String formatName){
		XMLElement targetElement = new XMLElement();
		targetElement.setName(formats.getFormatName(formatName));
		return targetElement;
	}


}