<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Cajero</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="brnDetails" class="datapro.eibs.beans.EDD031001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<jsp:useBean id= "EDD031001Help" class= "datapro.eibs.beans.JBObjList"  scope="session" />


<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">"); 
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }

    int row;
    String sTitle = "";
    int sValue = 600;
    int sOption = 0;
    String sLabel = "";
     
   if (request.getParameter("SCREEN") != null){
      sValue = Integer.parseInt(request.getParameter("SCREEN"));
      if (sValue == 600)
        sTitle = "Mantenimiento Cajero";                
      else
       	sTitle = "Nuevo Cajero";                
   } 
%>

<H3 align="center"><%=sTitle%> <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="teller_maintenance.jsp, EDD0310"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.teller.JSEDD0310" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="<%=sValue%>">
  
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Identificaci�n :</b></div>
            </td>
            <td nowrap width="14%"> 
              <div align="left"> 
                <input type="text" name="E01TLMTID" size="11" maxlength="10" value="<%= brnDetails.getE01TLMTID().trim()%>"
					   readonly>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right"><b>Moneda :</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2"> 
                <input type="text" name="E01TLMCCY" size="4" maxlength="3" value="<%= brnDetails.getE01TLMCCY().trim()%>" 
					   readonly> 
                </font></font></font></div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Tipo Cajero:</b></div>
            </td>
            <td nowrap width="40%"> 
              <div align="left"> 
				<%String sType = "";
				  if (brnDetails.getE01TLMTYP().equals("T")) {sType = "Regular"; sOption = 1;
				  } else if (brnDetails.getE01TLMTYP().equals("H")) {sType = "Principal"; sOption = 2; 
				  } else if (brnDetails.getE01TLMTYP().equals("O")) {sType = "Oficial S/G"; sOption = 3;
				  } else if (brnDetails.getE01TLMTYP().equals("S")) {sType = "Supervisor Operaciones"; sOption = 4;
				  } else if (brnDetails.getE01TLMTYP().equals("I")) {sType = "Cajero Interface"; sOption = 5;
				  }%>  
                <input type="text" name="TMPTLMTYP" size="25" maxlength="22" value = "<%=sType%>"  readonly>
                <input type=HIDDEN name="E01TLMTYP" size="1" maxlength="3" value="<%= brnDetails.getE01TLMTYP().trim()%>" readonly>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <div id="maintenace" style="visibility:visible">
  <h4>Informaci�n Cajero</h4>  
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%" height="23"> 
              <div align="right">Banco del Cajero :</div>
            </td>
            <td nowrap height="23" colspan="3"> 
              <eibsinput:text name="brnDetails" property="E01TLMBNK" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BANK%>"/>
            </td>
          </tr>
          <tr id="trcelar"> 
            <td nowrap width="20%" height="23"> 
              <div align="right">Sucursal del Cajero :</div>
            </td>
            <td nowrap height="23" colspan="3"> 
              <eibsinput:help name="brnDetails" property="E01TLMBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH%>" 
          	    	fn_param_one="E01TLMBRN" fn_param_two="document.forms[0].E01TLMBNK.value" />
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="20%" height="23">
              <% switch (sOption){
                   case 5:
                   case 1: sLabel = "Efectivo M�ximo Permitido :";
                           break;
                   default: sLabel = "Monto Sobregiro :";
                            break;  
              } 
              %> 
              <div align="right"><%=sLabel%></div>
            </td>
            <td nowrap height="23" colspan="3"> 
                <eibsinput:text name="brnDetails" property="E01TLMMXC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="20%" height="23"> 
              <% switch (sOption){
                   case 5:
                   case 1: sLabel = "L�mite de Retiros en Efectivo :";
                           break;
                   default: sLabel = "Monto Retiros :";
                            break;  
              } 
              %> 
              <div align="right"><%=sLabel%></div>
            </td>
            <td nowrap height="23" colspan="3"> 
                <eibsinput:text name="brnDetails" property="E01TLMFLW" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="20%" height="23"> 
              <% switch (sOption){
                   case 5:
                   case 1: sLabel = "L�mite de Dep�sitos Efectivo :";
                           break;
                   default: sLabel = "Monto Dep�sitos :";
                            break;  
              } 
              %> 
              <div align="right"><%=sLabel%></div>
            </td>
            <td nowrap height="23" colspan="3"> 
                <eibsinput:text name="brnDetails" property="E01TLMFLD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />
            </td>
          </tr>
          <% if (sOption == 1 || sOption == 5){%>
          <tr id="trclear"> 
            <td nowrap width="20%" height="23"> 
              <div align="right">Cuenta Contable de Efectivo:</div>
            </td>
            <td nowrap height="23" colspan="3"> 
              <eibsinput:help name="brnDetails" property="E01TLMGLN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>" 
              fn_param_one="E01TLMGLN" fn_param_two="document.forms[0].E01TLMBNK.value" fn_param_three="document.forms[0].E01TLMCCY.value"/>
            </td>
          </tr>
          <tr id="trdark">
            <td nowrap width="20%" height="23"> 
              <div align="right">Cheque ON-US G/L No. (No Retenidos) :</div>
            </td>
            <td nowrap height="23" colspan="3"> 
              <eibsinput:help name="brnDetails" property="E01TLMONG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>" 
          	    	fn_param_one="E01TLMONG" fn_param_two="document.forms[0].E01TLMBNK.value" fn_param_three="document.forms[0].E01TLMCCY.value"/>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="20%" height="23"> 
              <div align="right">Cheque ON-US G/L No. (Con Retenciones) :</div>
            </td>
            <td nowrap height="23" colspan="3"> 
              <eibsinput:help name="brnDetails" property="E01TLMOHG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>" 
          	    	fn_param_one="E01TLMOHG" fn_param_two="document.forms[0].E01TLMBNK.value" fn_param_three="document.forms[0].E01TLMCCY.value"/>
            </td>
          </tr>
          <tr id="trdark">
            <td nowrap width="20%" height="23"> 
              <div align="right">Cuenta Contable Cheque :</div>
            </td>
            <td nowrap height="23" colspan="3"> 
              <eibsinput:help name="brnDetails" property="E01TLMLOG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>" 
          	    	fn_param_one="E01TLMLOG" fn_param_two="document.forms[0].E01TLMBNK.value" fn_param_three="document.forms[0].E01TLMCCY.value"/>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="20%" height="23"> 
              <div align="right">Cuenta Contable de Cheque No Local :</div>
            </td>
            <td nowrap height="23" colspan="3"> 
              <eibsinput:help name="brnDetails" property="E01TLMNLG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_GLEDGER%>" 
          	    	fn_param_one="E01TLMNLG" fn_param_two="document.forms[0].E01TLMBNK.value" fn_param_three="document.forms[0].E01TLMCCY.value"/>
            </td>
          </tr>
          <tr id="trdark">
            <td nowrap width="20%" height="23"> 
              <div align="right">Numero de Lote Para el Cajero :</div>
            </td>
            <td nowrap height="23" colspan="3"> 
              <input type="text" name="E01TLMBTH" size="6"	
                     maxlength="5" value="<%= brnDetails.getE01TLMBTH().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="20%" height="23"> 
              <div align="right">Numero Centro de Costo Cajero :</div>
            </td>
            <td nowrap height="23" colspan="3"> 
              <eibsinput:help name="brnDetails" property="E01TLMCST" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_COST_CENTER%>" 
          	    	fn_param_one="E01TLMCST" fn_param_two="document.forms[0].E01TLMBNK.value" />
            </td>
          </tr>
          <%}%>
        </table>
      </td>
    </tr>
  </table>
  </div>  
  
  <br>
  <div align="center"> 
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
  
  </form>
</body>
</html>
