package datapro.eibs.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import sun.net.ftp.FtpClient;

import com.datapro.generic.beanutil.BeanList;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to 
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FTPSunWrapper extends FtpClient implements FTPWrapper {

	private String server; 
	private String username; 
	private String password; 
	private String local;
	private int filetype;
	
	public FTPSunWrapper() {
		super();
	}

	public FTPSunWrapper(String server, String username, String password, String local) {
		this();
		this.server = server;
		this.username = username;
		this.password = password;
		this.local = local;
		this.filetype = BINARY;
	}

	public boolean cdRemotePath(String directory) throws IOException {
		cd(directory);
		String dir = pwd();
		return dir.substring(1).equals(directory);
	}

	public void close() throws IOException {
		if (serverIsOpen()) {
			closeServer();
			System.out.println("Connection closed.");
		}
	}

	public boolean command(String cmd, String params) throws IOException {
		issueCommand(cmd + " " + params);
		return isValidResponse();
	}

	public boolean delete(String filename) throws IOException {
		return command("delete", filename);
	}

	public void download(String filename) throws IOException {
        int i = 0;
        byte bytesIn[] = new byte[1024];
        FileOutputStream output = null;
		try {
			if (filetype == BINARY) binary();
	        BufferedInputStream in = new BufferedInputStream(get(filename));
			File file = new File(getLocalPath(), filename);
			try {
				output = new FileOutputStream(file);
			} catch (FileNotFoundException e) {
				if (file.createNewFile()) {
					output = new FileOutputStream(file);
				} else {
					throw new IOException("Can't create file. Please contact your network administrator.");
				}
			}
	        while((i = in.read(bytesIn)) >= 0) 
	        	output.write(bytesIn, 0, i);
		} finally {	
			if (output != null) output.close();
		}	
   }

	public String getLocalPath() {
		return local;
	}

	public BeanList getWorkDir() throws IOException {
		BeanList result = new BeanList();
		String file = "";
		//TelnetInputStream files = list();
		BufferedReader reader = new BufferedReader(new InputStreamReader(list()));
		while ((file = reader.readLine()) != null) {
			String fileName = file.substring(39); 
			result.addRow(fileName);
	    }
		return result;
	}

	public int getResponseCode() throws NumberFormatException {
		return Integer.parseInt(getResponseString().substring(0, 3));
	}
	
	public boolean isValidResponse() {
		try {
			int respCode = getResponseCode();
			return (respCode  >= 200 && respCode < 300);
		} catch (Exception e) {
			return false;
		}
	}
	public BeanList getWorkDir(String ext) throws IOException {
		BeanList result = new BeanList();
		BeanList files = getWorkDir();
		files.initRow();
		while (files.getNextRow()) {
			String name = (String) files.getRecord();
			String extension = name.lastIndexOf(".") < 0 ? "" : name.trim().substring(name.trim().lastIndexOf("."));
			if (!extension.equals("")) {
				for (StringTokenizer st = new StringTokenizer(ext, "|"); st.hasMoreElements();) {
					if (extension.toLowerCase().equals(st.nextToken().toLowerCase())) {
						result.addRow(name);
					}
				}
			}	
		}	
		return result;
	}

	public boolean makeDir(String directory) throws IOException {
		return command("mkdir", directory);
	}

	public boolean open() throws IOException {
		openServer(server);
		System.out.println("Connected to " + server + ".");
		// After connection attempt, you should check the reply code to verify success.
        if (!serverIsOpen()) {
            System.out.println("FTP server refused connection.");
            return false;
        } else {
        	login(username, password);
       		System.out.println(welcomeMsg);
       		return true;
        }	
	}

	public boolean removeDir(String directory) throws IOException {
		return command("rmdir", directory);
	}

	public void setLocalPath(String local) {
		this.local = local;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setServerName(String server) {
		this.server = server;
	}

	public void setUserName(String username) {
		this.username = username;
	}

	public void upload(String filename) throws IOException {
        FileInputStream in = new FileInputStream(filename);
        upload(in, filename);
   }

	public void upload(InputStream in, String fileName)
			throws IOException {
		int i = 0;
        byte bytesIn[] = new byte[1024];
        BufferedOutputStream out = new BufferedOutputStream(put(fileName));
        while((i = in.read(bytesIn)) >= 0) 
            out.write(bytesIn, 0, i);

        if (in != null) in.close();
        if (out != null) out.close();		
	}
	
	public String getRemotePath() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	public void setFileType(int type) {
		this.filetype = type;
	}


}
