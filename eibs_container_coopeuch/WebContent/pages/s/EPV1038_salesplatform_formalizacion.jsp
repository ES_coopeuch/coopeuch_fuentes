<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV101003Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="formalizacion" class="datapro.eibs.beans.EPV103801Message"  scope="session" />
<jsp:useBean id="EPV101003List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

</head>

<body onload="tableresize();">
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<SCRIPT LANGUAGE="javascript">
builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   if (field.substring(0,8) == "E01OFFAC"){
     var index = field.substr(8,1);
     var concepto=document.getElementById("E01OFFOP"+index).value;
     if (concepto == "01")
       Client=document.getElementById("E01PVMCUN").value;
     else
       Client="";                 
   }
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }
        
 function enviar(opt) {
	  document.forms[0].H01FLGWK1.value = opt;
	  if (opt=='R'){	  		
	  		if (!confirm('Se Dispone a Rechazar La solicitud. Desea Continuar...?')){
	  			return;
	  		}
	  }
	  document.forms[0].submit();
 }        
</SCRIPT>

<%
	boolean readOnly=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		}
	}
%>

<h3 align="center"> Plataforma de Ventas - Formalizacion<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_formalizacion.jsp,JSEPV1038"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1038">
  <input type="hidden" name="SCREEN" value="600"> 
  <input type=HIDDEN name="E01PVMCCY"  value="<%= formalizacion.getE01PVMCCY().trim()%>">    
  <input type=HIDDEN name="E01PVMBNK"  value="<%= formalizacion.getE01PVMBNK().trim()%>">
  <input type=HIDDEN name="E01SELBC1"  value="<%= formalizacion.getE01SELBC1().trim()%>">
  <input type=HIDDEN name="E01SELBC2"  value="<%= formalizacion.getE01SELBC2().trim()%>">
  <input type=HIDDEN name="E01SELBD1"  value="<%= formalizacion.getE01SELBD1().trim()%>">
  <input type=HIDDEN name="E01SELBD2"  value="<%= formalizacion.getE01SELBD2().trim()%>">
  <input type=HIDDEN name="H01FLGWK1"  value="<%= formalizacion.getH01FLGWK1().trim()%>"> <%-- R: Rechazo, A: Aprobar--%> 
  
  <input type=HIDDEN name="E01CUN"  value="<%= formalizacion.getE01PVMCUN().trim()%>">

 <% int row = 0;%>
 <% String name="";%>  
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="formalizacion" property="E01PVMCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Nombre :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="formalizacion" property="E01CUSNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="formalizacion" property="E01PVMNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="30%">
    	        <eibsinput:date name="formalizacion" fn_year="E01PVMODY" fn_month="E01PVMODM" fn_day="E01PVMODD" readonly="true" readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Sucursal :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="formalizacion" property="E01PVMBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
             </td>
             <td nowrap align="right" width="20%"> Ejecutivo :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="formalizacion" property="E01PVMOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>" readonly="true"/>
             </td>
         </tr>         
        </table>
      </td>
    </tr>
  </table>


<%if(formalizacion.getE01LNTYPG().equals("T")){%> 

  <h4>Informacion Tarjeta de Credito </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="formalizacion" property="E01LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="formalizacion" property="E01DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Direccion :</div>
            </td>
            <td nowrap width="30%">
              <input type="text" name="E01TCCMLA" size="3" maxlength="2" value="<%= formalizacion.getE01TCCMLA().trim()%>" readonly >
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">  
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Local :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="formalizacion" property="E01TCCUMB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Extranjera :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="formalizacion" property="E01TCCUME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="true"/>
            </td>
          </tr>                  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Dia de Pago :</div>
            </td>
            <td nowrap width="30%">             
				<select name="E01TCDYPG" disabled>
					<option></option>
					<option value="3" <% if (formalizacion.getE01TCDYPG().equals("3")) out.print("selected");%>>3</option>
					<option value="23" <% if (formalizacion.getE01TCDYPG().equals("23")) out.print("selected");%>>23</option>
				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Porcentaje de Pago :</div>
            </td>
            <td nowrap width="30%">            
				<select name="E01TCPRPG" disabled>
					<option></option>
					<option value="5" <% if (formalizacion.getE01TCPRPG().equals("5")) out.print("selected");%>>El 5%</option>
					<option value="100" <% if (formalizacion.getE01TCPRPG().equals("100")) out.print("selected");%>>El 100%</option>
				</select>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">
				<select name="E01TCMEPG" disabled>
					<option></option>
					<option value="1" <% if (formalizacion.getE01TCMEPG().equals("1")) out.print("selected");%>>Con Cuenta</option>
					<option value="2" <% if (formalizacion.getE01TCMEPG().equals("2")) out.print("selected");%>>Sin Cuenta</option>
				</select>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:text name="formalizacion" property="E01TCACPG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
            </td>
          </tr>  

        </table>
      </td>
    </tr>
  </table>

<%} else {%> 

  <h4>Informacion del Credito </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Tipo de Producto :</div>
            </td>
            <td nowrap width="30%" >
            	<eibsinput:text name="formalizacion" property="E01LNTYPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>	
            	<eibsinput:text name="formalizacion" property="E01DSTYPE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Termino del Contrato :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="formalizacion" property="E01LNTERM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM%>" readonly="true" /> 
	            <input type="text" readonly name="E01LNTERC" size="10" 
				  value="<% if (formalizacion.getE01LNTERC().equals("D")) out.print("D&iacute;a(s)");
							else if (formalizacion.getE01LNTERC().equals("M")) out.print("Mes(es)");
							else if (formalizacion.getE01LNTERC().equals("Y")) out.print("A&ntilde;o(s)");
							else out.print("");%>" >
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="formalizacion" property="E01LNPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="formalizacion" property="E01DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Monto del prestamo</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="formalizacion" property="E01LNOAMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/> 
            </td>
          </tr>                 
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Primer Pago :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:date name="formalizacion" fn_year="E01LNPXPY" fn_month="E01LNPXPM" fn_day="E01LNPXPD" readonly="true" />
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tasa de Interes :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="formalizacion" property="E01LNRATE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/> 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Convenio :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="formalizacion" property="E01LNCONV" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/> 
            </td>
            <td nowrap width="20%"> 
              <div align="right">Valor de la Cuota :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="formalizacion" property="E01LNCUAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/> 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">          
				<select name="E01LNTYPG" <%="disabled"%>>
					<option value=""
						<% if (formalizacion.getE01LNTYPG().equals("")) out.print("selected");%>>Caja</option>
					<option value="1"
						<% if (formalizacion.getE01LNTYPG().equals("1")) out.print("selected");%>>PAC</option>							            
					<option value="2"
						<% if (formalizacion.getE01LNTYPG().equals("2")) out.print("selected");%>>Convenio</option>
					<option value="4"
						<% if (formalizacion.getE01LNTYPG().equals("4")) out.print("selected");%>>PAC/Multibanco</option>	
				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:text name="formalizacion" property="E01LNPACC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>

  <h4>Distribucion del Prestamo </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <%
           for (int i=1;i<30;i++) {
            name = Util.addLeftChar('0', 2, String.valueOf(i));
		 %>
		 	<input type=HIDDEN name="E01PTYP<%=name%>"  value="<%=formalizacion.getField("E01PTYP"+name).getString().trim()%>">
		 	<input type=HIDDEN name="E01PDSC<%=name%>"  value="<%=formalizacion.getField("E01PDSC"+name).getString().trim()%>">
		 	<input type=HIDDEN name="E01PAMT<%=name%>"  value="<%=formalizacion.getField("E01PAMT"+name).getString().trim()%>">
		 <%            
		    if(!formalizacion.getField("E01PDSC"+name).getString().trim().equals("")){
		 %> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			<td nowrap width="25%" align="Left"></td>
			<td nowrap width="30%" align="Left"><%=formalizacion.getField("E01PDSC"+name).getString().trim()%></td>
			<td nowrap width="20%"align="right"><%=formalizacion.getField("E01PAMT"+name).getString().trim()%></td>
			<td nowrap width="25%" align="Left"></td>
          </tr>
          <%
		   	}
		   }
		  %> 
        </table>
      </td>
    </tr>
  </table>

  <h4>Desembolso del Monto Liquido</h4>
  <table id="mainTable" class="tableinfo">
  <tr>
   <td>
	<table id="headTable" >
    <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
      <td nowrap align="center" > Concepto</td>
      <td nowrap align="center" >Banco </td>
      <td nowrap align="center" >Sucursal</td>
      <td nowrap align="center" >Moneda</td>
      <td nowrap align="center" >Referencia</td>
      <td nowrap align="center" >Monto</td>
    </tr>
    </table> 
      
    <div id="dataDiv" style="height:60; overflow-y :scroll; z-index:0; ">
     <table id="dataTable" >
          <%
		   int amount = 9;
			for ( int i=1; i<=amount; i++ ) {
			  name = i + "";
   			%> 
	    <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap > 
              <div align="center" nowrap> 
                <input type="text" readonly name="E01OFFOP<%= name %>" id="E01OFFOP<%= name %>" value="<%= formalizacion.getField("E01OFFOP"+name).getString().trim()%>" size="3" maxlength="3">
                <input type="hidden" name="E01OFFGL<%= name %>" value="<%= formalizacion.getField("E01OFFGL"+name).getString().trim()%>">
                <input type="text" readonly name="E01OFFCO<%= name %>" size="35" maxlength="35"  value="<%= formalizacion.getField("E01OFFCO"+name).getString().trim()%>"
                <%if (!readOnly){%> 
                  oncontextmenu="showPopUp(conceptHelp,this.name,document.forms[0].E01PVMBNK.value,'','E01OFFGL<%= name %>','E01OFFOP<%= name %>','10'); return false;"
				<%}%>                  
                  >
              </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E01OFFBK<%= name %>" size="2" maxlength="2" value="<%= formalizacion.getField("E01OFFBK"+name).getString().trim()%>">
              </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E01OFFBR<%= name %>" size="4" maxlength="4" value="<%= formalizacion.getField("E01OFFBR"+name).getString().trim()%>"
                <%if (!readOnly){%>
                oncontextmenu="showPopUp(branchHelp,this.name,document.forms[0].E01PVMBNK.value,'','','',''); return false;"
				<%}%>                
               >
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFCY<%= name %>" size="3" maxlength="3" value="<%= formalizacion.getField("E01OFFCY"+name).getString().trim()%>"
                <%if (!readOnly){%>
                oncontextmenu="showPopUp(currencyHelp,this.name,document.forms[0].E01PVMBNK.value,'','','',''); return false;"
				<%}%>                
                >
              </div>
            </td>
            <td nowrap > 
              <div align="center"> 
                <input type="text" name="E01OFFAC<%= name %>" size="12" maxlength="12"  value="<%= formalizacion.getField("E01OFFAC"+name).getString().trim()%>"
                <%if (!readOnly){%>
                oncontextmenu="showPopUp(accountCustomerHelp,this.name,document.forms[0].E01PVMBNK.value,'',document.forms[0].E01PVMCUN.value,'','RT'); return false;"
				<%}%>                
                >
              </div>
            </td>
            <td nowrap> 
              <div align="center"> 
                <input type="text" name="E01OFFAM<%= name %>" size="23" maxlength="15"  value="<%= formalizacion.getField("E01OFFAM"+name).getString().trim()%>" onKeypress="enterDecimal()">
              </div>
            </td>
          </tr>
          <%
    		}
    		%> 
    	  </table>
        </div>
        
     <table id="footTable" > 
	    <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap  align="right"><b>Equivalente Moneda Credito :</b> 
            </td>
            <td nowrap align="center"><b><i><strong> 
                <input type="text" name="E01OFFEQV" size="23" maxlength="17" readonly value="<%= formalizacion.getE01OFFEQV().trim()%>">
                </strong></i></b>
            </td>
          </tr>
        </table>
  
     </td>  
  </tr>	
</table>  


<%if(formalizacion.getE01SELFCK().equals("Y")){%> 
  <h4>Emision de Cheques </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Sucursal :</div>
            </td>
            <td nowrap  width="30%"> 
            	<eibsinput:text name="formalizacion" property="E01SELBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
            	<%if (!readOnly){%>
				<a id="linkHelp" href="javascript:GetBankSalesPlatformFormbyBranchs(document.forms[0].E01PVMBNK.value,document.forms[0].E01PVMCCY.value,'E01SELBRN','E01SELBC1','E01SELBD1','E01SELBC2','E01SELBD2','bco1','bco2')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="seleccione agencias" align="bottom" border="0"/></a>
				<%}%>				            	
           </td>
            <td nowrap width="20%" > 
              <div align="right">Cheque del Banco :</div>
            </td>
            <td nowrap  width="30%"> 
              <input type="radio" name="E01SELBSL" value="1" <%if (!formalizacion.getE01SELBSL().equals("2")) out.print("checked"); %> <%=(readOnly?"disabled":"") %>  >           
              <font id="bco1"><%= formalizacion.getE01SELBC1() + "-" + formalizacion.getE01SELBD1().toUpperCase()%></font><br>
              <input type="radio" name="E01SELBSL" value="2" <%if (formalizacion.getE01SELBSL().equals("2")) out.print("checked"); %> <%=(readOnly?"disabled":"") %> >             
			  <font id="bco2"><%= formalizacion.getE01SELBC2() + "-" + formalizacion.getE01SELBD2().toUpperCase()%></font>              
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>
<% } %>   

<%} %>   

<%
	if (!readOnly){%>
	   <div align="center">
		   <input id="EIBSBTN" type="button"" name="Rechazar" value="Rechazar" onclick="enviar('R');">
		   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		   <input id="EIBSBTN" type="button"" name="Aprobar" value="Aprobar" onclick="enviar('A');">	                    
	   </div>	
<%	
	}
%>

 <SCRIPT language="javascript">
    function tableresize() {
     adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  //tableresize();
  window.onresize=tableresize;
 </SCRIPT>
 
</form>

</body>
</html>
