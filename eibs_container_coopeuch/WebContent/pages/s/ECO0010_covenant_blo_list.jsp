<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util"%>
<%@page import="com.datapro.constants.EibsFields, datapro.eibs.beans.ECO001004Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<title>Bloqueo / Desbloqueo de Aporte de Convenio</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"	rel="stylesheet">

<jsp:useBean id="msgList" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>	
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>


<script type="text/javascript">

  function goAction(op) 
  {
	var ok = false;
	
	if (op == '1100')
		ok = true;
	else 
	{
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "CURRCODE") 
	      	{	
	      		if (element.checked == true) {
        			ok = true;
        			break;
				}
	      	}
	      }
    } 
      
    if ( ok ) 
      {
 		document.forms[0].SCREEN.value = op;
		document.forms[0].submit();		
  	  }
     else 
     {
		alert("Debe seleccionar un convenio para continuar.");	   
	 }
      
	}
	
</script>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Bloqueo / Desbloqueo de Aporte de Convenio
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="covenant_blo_list.jsp, ECO0010"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0010">
<input type="hidden" name="SCREEN" value="1300"> 

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Empleador: 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">Identificaci�n:  
             </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="ID" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right"> Nombre: 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="userPO" property="cusName" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
  
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="15%">
			<a href="javascript:goAction('1300')" ><b>Bloquear/Desbloquear</b></a>
		</td>
		<td align="center" class="tdbkg" width="15%">
			<a href="javascript:goAction('1100')"> <b>Volver</b></a>
		</td>
	</tr>
</table>

<%
	if (msgList.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=90%>
	<TR>
		<TD>
		<div align="center"><font size="3"><b> No hay resultados que correspondan a su criterio de b�squeda. </b></font></div>
		</TD>
	</TR>
</TABLE>
<%
	} else {
%>

	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="5%"></th>
			<th align="center" nowrap width="20%">Solicitud</th>
			<th align="center" nowrap width="20%">Cliente</th>
			<th align="center" nowrap width="25%">Bloqueo Aporte</th>
		</tr>
		<%
			msgList.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (msgList.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ECO001004Message convObj = (ECO001004Message) msgList.getRecord();
		%>
		<tr>
			<td nowrap><input type="radio" id="CURRCODE" name="CURRCODE"	value="<%=k%>" <%=chk%>/></td>
			<td nowrap align="center"><%=convObj.getE04COSNUM()%></td>
			<td nowrap align="left"><%=convObj.getE04CONNA1()%></td>

			<td nowrap align="center"><%=convObj.getE04DESSTS()%></td>
		</tr>
		<%
		k++;
			}
		%>
	</table>
<%
	}
%>
</form>

</body>
</html>
