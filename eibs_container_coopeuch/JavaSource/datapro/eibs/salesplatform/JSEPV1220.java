package datapro.eibs.salesplatform;

/*******************************************************************************************************************************/
/**  Creado por              :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  21/02/2013                                                                                      **/
/**  Objetivo                :  Mantenedor de Tabla de Holguras por Sucursal para Plataforma de Ventas                          **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV122001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;

public class JSEPV1220 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	protected static final int A_PVOBR_ENTER = 100;
	protected static final int A_PVOBR_LIST = 150;	
	protected static final int R_PVOBR_NEW = 200;
	protected static final int R_PVOBR_MAINT = 201;
	protected static final int R_PVOBR_DELETE = 202;	
	protected static final int R_PVOBR_INQUIRY = 203;
	protected static final int A_PVOBR_MAINT = 600;
	protected static final int A_PVOBR_INQ_LIST = 700;
	
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
	    	flexLog("Screen..." + screen);
		switch (screen) {
		case A_PVOBR_ENTER:
			procActionTablaPVOBREnter(user, req, res, session);
			break;
		case A_PVOBR_LIST:
			procActionTablaPVOBRList(user, req, res, session);
			break;			
		case R_PVOBR_NEW:
			procReqNew(user, req, res, session);
			break;
		case R_PVOBR_MAINT:
			procReqMaintenance(user, req, res, session);
			break;
		case R_PVOBR_INQUIRY:
		  	procReqView(user, req, res, session);
			break;
		case A_PVOBR_MAINT:
			procActionMaintenance(user, req, res, session);
			break;
		case R_PVOBR_DELETE:
			procReqDelete(user, req, res, session);
			break;
		case A_PVOBR_INQ_LIST:
			procActionTablaPVOBRInqList(user, req, res, session);
			break;	
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}
	
	protected void procActionTablaPVOBREnter(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession ses)
			throws ServletException, IOException {
		try {
			flexLog("About to call Page: EPV1220_PVOBR_bank_branch_enter.jsp");
			forward("EPV1220_PVOBR_bank_branch_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	/**
	 * procActionTablaPVOBRList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionTablaPVOBRList(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EPV1220", req);

			EPV122001Message msg = (EPV122001Message) mp.getMessageRecord("EPV122001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());

			try{
				  msg.setE01PVGBNK(req.getParameter("E01PVGBNK"));
				}catch(Exception e){msg.setE01PVGBRN("01");}
				
			try{
			  msg.setE01PVGBRN(req.getParameter("E01PVGBRN"));
			}catch(Exception e){msg.setE01PVGBRN("0");}
			
			try{
				  msg.setE01PVGALL(req.getParameter("E01DBAALL"));
				}catch(Exception e){msg.setE01PVGALL("S");}			
            userPO.setBank(msg.getE01PVGBNK());
            userPO.setBranch(msg.getE01PVGBRN());
            userPO.setOption(msg.getE01PVGALL());   
            
            if (!userPO.getOption().equals("S"))
            {
    			msg.setH01OPECOD("0016");
            }
			//Sends message
		//	flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
 
			session.setAttribute("userPO", userPO);
			session.setAttribute("EPV122001List", list);
			forwardOnSuccess("EPV1220_PVOBR_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqTablaPVOBR: This Method show a single  TablaPVOBR either for 
	 * 					a new register, a maintenance or an inquiry. 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqNew(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res,
			HttpSession session) 
		throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		userPO.setPurpose("NEW");
	
		try {
			mp = getMessageProcessor("EPV1220", req);
			
			//Creates the message with operation code depending on the option
			EPV122001Message msg = (EPV122001Message) mp.getMessageRecord("EPV122001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01OPECOD("0001");
						
			//Sets the number for maintenance and inquiry options
			if (userPO.getBank() != null) {
				msg.setE01PVGBNK(userPO.getBank());
			}
			else
			{
				msg.setE01PVGBNK("01");
			}
			flexLog("userPO.getBranch() " + userPO.getBranch());
			flexLog("req.getParameter " + req.getParameter("E01PVGBRN"));
			if (userPO.getBranch() != null && (!userPO.getBranch().equals("0"))) {
				msg.setE01PVGBRN(userPO.getBranch());
			}	
			else
			{
				if (req.getParameter("E01PVGBRN") != null) {
					msg.setE01PVGBRN(req.getParameter("E01PVGBRN"));
			}}

			if (userPO.getOption() != null) {
				msg.setE01PVGALL(userPO.getOption());
			}
			
			try{
				  msg.setE01PVGTPR(req.getParameter("E01PVGTPR"));
				}catch(Exception e){msg.setE01PVGTPR("  ");}
				
				
			//Send message
			//flexLog("mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV122001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
					forward("EPV1220_PVOBR_maintenance.jsp", req, res);
			} else {
				//if there are errors go back to list page
				session.setAttribute("error", msgError);
				forward("EPV1220_PVOBR_list.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	protected void procReqMaintenance(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			EPV122001Message msgDoc = null;
			UserPos userPO = null;

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			userPO.setPurpose("MAINTENANCE");			

			// Receive Data
			try {
				JBObjList bl = (JBObjList) ses.getAttribute("EPV122001List");
				int idx = Integer.parseInt(req.getParameter("COLUMNA"));
				bl.setCurrentRow(idx);

				msgDoc = (EPV122001Message) bl.getRecord();
			
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("cnvObj", msgDoc);

				try {
					forward("EPV1220_PVOBR_maintenance.jsp?readOnly=false", req, res);

				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}

	/**
	 * procActionMaintenance
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1220", req);

			EPV122001Message msg = (EPV122001Message) session.getAttribute("cnvObj");			
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0005");							
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01SCRCOD("01");
			setMessageRecord(req, msg);

			//Sending message
			//flexLog("mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive error and data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV122001Message) mp.receiveMessageRecord();

			//Sets session with required data
			session.setAttribute("userPO", userPO);
			session.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//if there are no errors go back to list
				redirectToPage("/servlet/datapro.eibs.salesplatform.JSEPV1220?SCREEN=150&E01PVGBNK=" + userPO.getBank() + "&E01PVGBRN=" + userPO.getBranch() + "&E01DBAALL=" + userPO.getOption(), res);
			} else {
				//if there are errors go back to maintenance page and show errors
				session.setAttribute("error", msgError);
				forward("EPV1220_PVOBR_maintenance.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	/**
	 * procReqDelete
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDelete(
			ESS0030DSMessage user, 
			HttpServletRequest req,
			HttpServletResponse res, 
			HttpSession ses)
			throws ServletException, IOException {
		EPV122001Message msg = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("EPV1220", req);
			JBObjList bl = (JBObjList) ses.getAttribute("EPV122001List");
			int idx = Integer.parseInt(req.getParameter("COLUMNA"));
			bl.setCurrentRow(idx);
			msg = (EPV122001Message) bl.getRecord();
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0009");
			msg.setH01TIMSYS(getTimeStamp());
						
			//Send message
			//flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive Error and Data
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			msg = (EPV122001Message) mp.receiveMessageRecord();

			//Sets session with required data
			ses.setAttribute("userPO", userPO);
			ses.setAttribute("cnvObj", msg);

			if (!mp.hasError(msgError)) {
				//If there are no errors request the list again
				redirectToPage(("/servlet/datapro.eibs.salesplatform.JSEPV1220?SCREEN=150&E01PVGBNK=" + userPO.getBank() + "&E01PVGBRN=" + userPO.getBranch() + "&E01DBAALL=" + userPO.getOption()), res);
			} else {
				//if there are errors show the list without updating
				ses.setAttribute("error", msgError);
				forward("EPV1220_PVOBR_maintenance.jsp?readOnly=true", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	protected void procReqView(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			EPV122001Message msgDoc = null;
			UserPos userPO = null;

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

			// Receive Data
			try {
				JBObjList bl = (JBObjList) ses.getAttribute("EPV122001List");
				int idx = Integer.parseInt(req.getParameter("COLUMNA"));
				bl.setCurrentRow(idx);

				msgDoc = (EPV122001Message) bl.getRecord();
			
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("cnvObj", msgDoc);

				try {
					forward("EPV1220_PVOBR_maintenance.jsp?readOnly=true", req, res);

				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}
		}
	/**
	 * procActionTablaPVOBRList	  
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionTablaPVOBRInqList(
			ESS0030DSMessage user,
			HttpServletRequest req, 
			HttpServletResponse res, 
			HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EPV1220", req);

			EPV122001Message msg = (EPV122001Message) mp.getMessageRecord("EPV122001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01OPECOD("0016");
			msg.setH01TIMSYS(getTimeStamp());

			try{
				  msg.setE01PVGBNK(user.getE01UBK());
				}catch(Exception e){msg.setE01PVGBRN("01");}
				
			try{
			      msg.setE01PVGBRN(user.getE01UBR());
			    }catch(Exception e){msg.setE01PVGBRN("0");}
			
			try{
				  msg.setE01PVGALL(req.getParameter("E01DBAALL"));
				}catch(Exception e){msg.setE01PVGALL("S");}			
            userPO.setBank(msg.getE01PVGBNK());
            userPO.setBranch(msg.getE01PVGBRN());
            userPO.setOption(msg.getE01PVGALL());   

			//Sends message
			flexLog("Mensaje enviado..." + msg);
			mp.sendMessage(msg);

			//Receive insurance  list
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
 
			session.setAttribute("userPO", userPO);
			session.setAttribute("EPV122001List", list);
			forwardOnSuccess("EPV1220_PVOBR_inq_list.jsp", req, res);

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	
}
