package datapro.eibs.accounting;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;

import datapro.eibs.beans.EGL042001Message;
import datapro.eibs.beans.EGL042002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;

public class JSEGL0420 extends JSEIBSServlet {

	// GL Statement options
	protected static final int R_LIST 		= 1;
	protected static final int R_PRINT 		= 3;
	// entering options
	protected static final int R_SELECTION 	= 100;
	protected static final int A_SELECTION 	= 200;
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_LIST :
				procReqList(user, req, res, session);
				break;
			case R_PRINT :
				procReqPrintList(user, req, res, session);
				break;
				// Entering Options
			case R_SELECTION :
				procReqSelection(user, req, res, session);
				break;
			case A_SELECTION :
				procActionSelection(user, req, res, session);
				break;
			default :
				forward(devPage, req, res);
				break;
		}
	}

	private void procActionSelection(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		UserPos userPO = getUserPos(session);
		try {
			userPO.setBank(req.getParameter("E01TRABNK").trim());
		} catch (Exception e) {
		}
		try {
			userPO.setBranch(req.getParameter("E01TRABRN").trim());
		} catch (Exception e) {
		}
		try {
			userPO.setCurrency(req.getParameter("E01TRACCY").toUpperCase());
		} catch (Exception e) {
		}
		try {
			userPO.setAccNum(req.getParameter("E01TRAGLN").trim());
		} catch (Exception e) {
		}
		try {
			userPO.setHeader7(req.getParameter("E01HISCYC").trim());
		} catch (Exception e) {
		}
		try {
			userPO.setHeader8(req.getParameter("E01VALBTH").trim());
		} catch (Exception e) {
		}
		try {
			userPO.setHeader9(req.getParameter("E01FRDTE1"));
			userPO.setHeader10(req.getParameter("E01FRDTE2"));
			userPO.setHeader11(req.getParameter("E01FRDTE3"));
		} catch (Exception e) {
		}
		try {
			userPO.setHeader12(req.getParameter("E01TODTE1"));
			userPO.setHeader13(req.getParameter("E01TODTE2"));
			userPO.setHeader14(req.getParameter("E01TODTE3"));
		} catch (Exception e) {
		}
		try {
			userPO.setHeader15(req.getParameter("E01FRREFN").trim());
		} catch (Exception e) {
		}
		try {
			userPO.setHeader16(req.getParameter("E01TOREFN").trim());
		} catch (Exception e) {
		}

		try {
			userPO.setHeader17(req.getParameter("E01FRAMNT").trim());
		} catch (Exception e) {
		}
		try {
			userPO.setHeader18(req.getParameter("E01TOAMNT").trim());
		} catch (Exception e) {
		}

		session.setAttribute("userPO", userPO);

		procReqList(user, req, res, session);
	}

	private void procReqSelection(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		userPO.setOption("GL");
		userPO.setPurpose("STATEMENT");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("userPO", userPO);
		
		forward("EGL0420_st_selection.jsp", req, res);
	}

	private void procReqPrintList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EGL0420", req);
			EGL042001Message msgSearch = (EGL042001Message) mp.getMessageRecord("EGL042001");
			msgSearch.setH01USERID(user.getH01USR());
			msgSearch.setH01PROGRM("EGL0420");
			msgSearch.setH01TIMSYS(getTimeStamp());
			msgSearch.setH01SCRCOD("01");
			msgSearch.setH01OPECOD("0004");
			msgSearch.setH01FLGWK1("P");
			msgSearch.setE01NUMREC(String.valueOf(Util.parseInt(req.getParameter("Pos"))));
			try {
				msgSearch.setE01TRABNK(userPO.getBank().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TRABRN(userPO.getBranch().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TRACCY(userPO.getCurrency().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TRAGLN(userPO.getAccNum().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01HISCYC(userPO.getHeader7().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01VALBTH(userPO.getHeader8().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01FRDTE1(userPO.getHeader9());
				msgSearch.setE01FRDTE2(userPO.getHeader10());
				msgSearch.setE01FRDTE3(userPO.getHeader11());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TODTE1(userPO.getHeader12());
				msgSearch.setE01TODTE2(userPO.getHeader13());
				msgSearch.setE01TODTE3(userPO.getHeader14());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01FRREFN(userPO.getHeader15().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TOREFN(userPO.getHeader16().trim());
			} catch (Exception e) {
			}

			try {
				msgSearch.setE01FRAMNT(userPO.getHeader17().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TOAMNT(userPO.getHeader18().trim());
			} catch (Exception e) {
			}
			
			mp.sendMessage(msgSearch);
			JBObjList list = mp.receiveMessageRecordList("E01INDOPE", "E01NUMREC");
			
			if (mp.hasError(list)) {
				session.setAttribute("error", mp.getError(list));
				forward("EGL0420_st_selection.jsp", req, res);
			} else if (!list.isEmpty()) {
				list.initRow();
				if (list.getNextRow()) {
					EGL042002Message msgHeader = (EGL042002Message) list.get(0);
					session.setAttribute("stGLBal", msgHeader);
				}
				
				JBList beanList = new JBList();
				boolean firstTime = true;
				StringBuffer myRow = null;
				String strDebit = "";
				String strCredit = "";
				BigDecimal debit = new BigDecimal("0.00");
				BigDecimal credit = new BigDecimal("0.00");
				int countDebit = 0;
				int countCredit = 0;
				EGL042001Message msgList = null;
				while (list.getNextRow()) {
					msgList = (EGL042001Message) list.getRecord();
					if (firstTime) {
						firstTime = false;
						if (!list.getShowPrev()) {
							userPO.setHeader5(msgList.getE01BEGBAL());
						}	
					}
					strDebit = "&nbsp;";
					strCredit = "&nbsp;";

					if ("0".equals(msgList.getE01TRADCC())) {
						debit =
							debit.add(msgList.getBigDecimalE01TRAAMT());
						strDebit =
							Util.fcolorCCY(msgList.getE01TRAAMT());
						countDebit++;
					} else if ("5".equals(msgList.getE01TRADCC())) {
						credit =
							credit.add(
								msgList.getBigDecimalE01TRAAMT());
						strCredit =
							Util.fcolorCCY(msgList.getE01TRAAMT());
						countCredit++;
					}

					myRow = new StringBuffer("<TR>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\">"
						+ Util.formatDate(
							msgList.getE01DATE11(),
							msgList.getE01DATE12(),
							msgList.getE01DATE13())
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\">"
						+ Util.formatCell(msgList.getE01TRAACC())
						+ "</TD>");
					if (msgList.getE01NUMNAR().equals("0")) {
						myRow.append("<TD NOWRAP>"
							+ Util.formatCell(msgList.getE01TRANAR())
							+ "</TD>");
					} else {
						if (msgList
							.getE01NUMNAR()
							.trim()
							.equals("1")) {
							myRow.append("<TD NOWRAP>"
								+ Util.formatCell(msgList.getE01TRANAR())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA1())
								+ "</TD>");
						} else if (
							msgList.getE01NUMNAR().trim().equals(
								"2")) {
							myRow.append("<TD NOWRAP>"
								+ Util.formatCell(msgList.getE01TRANAR())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA1())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA2())
								+ "</TD>");
						} else if (
							msgList.getE01NUMNAR().trim().equals(
								"3")) {
							myRow.append("<TD NOWRAP>"
								+ Util.formatCell(msgList.getE01TRANAR())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA1())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA2())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA3())
								+ "</TD>");
						} else if (
							msgList.getE01NUMNAR().trim().equals(
								"4")) {
							myRow.append("<TD NOWRAP>"
								+ Util.formatCell(msgList.getE01TRANAR())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA1())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA2())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA3())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA4())
								+ "</TD>");
						} else if (
							msgList.getE01NUMNAR().trim().equals(
								"5")) {
							myRow.append("<TD NOWRAP>"
								+ Util.formatCell(msgList.getE01TRANAR())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA1())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA2())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA3())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA4())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA5())
								+ "</TD>");
						} else if (
							msgList.getE01NUMNAR().trim().equals(
								"6")) {
							myRow.append("<TD NOWRAP>"
								+ Util.formatCell(msgList.getE01TRANAR())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA1())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA2())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA3())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA4())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA5())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA6())
								+ "</TD>");
						} else if (
							msgList.getE01NUMNAR().trim().equals(
								"7")) {
							myRow.append("<TD NOWRAP>"
								+ Util.formatCell(msgList.getE01TRANAR())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA1())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA2())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA3())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA4())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA5())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA6())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA7())
								+ "</TD>");
						} else if (
							msgList.getE01NUMNAR().trim().equals(
								"8")) {
							myRow.append("<TD NOWRAP>"
								+ Util.formatCell(msgList.getE01TRANAR())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA1())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA2())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA3())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA4())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA5())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA6())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA7())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA8())
								+ "</TD>");
						} else if (
							msgList.getE01NUMNAR().trim().equals(
								"9")) {
							myRow.append("<TD NOWRAP>"
								+ Util.formatCell(msgList.getE01TRANAR())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA1())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA2())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA3())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA4())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA5())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA6())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA7())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA8())
								+ "<BR>"
								+ Util.formatCell(msgList.getE01TRANA9())
								+ "</TD>");
						}
					}

					myRow.append("<TD NOWRAP ALIGN=RIGHT>"
						+ strDebit
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>"
						+ strCredit
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>"
						+ Util.fcolorCCY(msgList.getE01ENDBAL())
						+ "</TD>");
					myRow.append("</TR>");
					beanList.addRow("", myRow.toString());
				}
				beanList.setShowNext(list.getShowNext());
				beanList.setShowPrev(list.getShowPrev());
				beanList.setFirstRec(list.getFirstRec());
				beanList.setLastRec(list.getLastRec());
				
				userPO.setHeader19(Util.fcolorCCY(debit.toString()));
				userPO.setHeader20(Util.fcolorCCY(credit.toString()));
				userPO.setHeader21(msgList.getE01ENDBAL());
				userPO.setHeader22(countDebit + "");
				userPO.setHeader23(countCredit + "");

				flexLog("Putting java beans into the session");
				session.setAttribute("userPO", userPO);
				session.setAttribute("glList", beanList);
				
				if ("V".equals(msgList.getE01VALBTH())) {
					forward("EGL0420_st_list_print_fv.jsp", req, res);
				} else if ("B".equals(msgList.getE01VALBTH())) {
					forward("EGL0420_st_list_print_fp.jsp", req, res);
				} else {
					forward(devPage, req, res);
				}
			}
			
		} finally {
			if (mp != null) mp.close();
		}
	}

	private void procReqList(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EGL0420", req);
			EGL042001Message msgSearch = (EGL042001Message) mp.getMessageRecord("EGL042001");
			msgSearch.setH01USERID(user.getH01USR());
			msgSearch.setH01PROGRM("EGL0420");
			msgSearch.setH01TIMSYS(getTimeStamp());
			msgSearch.setH01SCRCOD("01");
			msgSearch.setH01OPECOD("0004");
			msgSearch.setE01NUMREC(String.valueOf(Util.parseInt(req.getParameter("Pos"))));
			try {
				msgSearch.setE01TRABNK(userPO.getBank().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TRABRN(userPO.getBranch().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TRACCY(userPO.getCurrency().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TRAGLN(userPO.getAccNum().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01HISCYC(userPO.getHeader7().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01VALBTH(userPO.getHeader8().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01FRDTE1(userPO.getHeader9());
				msgSearch.setE01FRDTE2(userPO.getHeader10());
				msgSearch.setE01FRDTE3(userPO.getHeader11());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TODTE1(userPO.getHeader12());
				msgSearch.setE01TODTE2(userPO.getHeader13());
				msgSearch.setE01TODTE3(userPO.getHeader14());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01FRREFN(userPO.getHeader15().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TOREFN(userPO.getHeader16().trim());
			} catch (Exception e) {
			}

			try {
				msgSearch.setE01FRAMNT(userPO.getHeader17().trim());
			} catch (Exception e) {
			}
			try {
				msgSearch.setE01TOAMNT(userPO.getHeader18().trim());
			} catch (Exception e) {
			}
			
			mp.sendMessage(msgSearch);
			JBObjList list = mp.receiveMessageRecordList("E01INDOPE", "E01NUMREC");
			
			if (mp.hasError(list)) {
				session.setAttribute("error", mp.getError(list));
				forward("EGL0420_st_selection.jsp", req, res);
			} else if (!list.isEmpty()) {
				list.initRow();
				if (list.getNextRow()) {
					EGL042002Message msgHeader = (EGL042002Message) list.get(0);
					session.setAttribute("stGLBal", msgHeader);
				}
				
				JBList beanList = new JBList();
				boolean firstTime = true;
				StringBuffer myRow = null;
				String strDebit = "";
				String strCredit = "";
				BigDecimal debit = new BigDecimal("0.00");
				BigDecimal credit = new BigDecimal("0.00");
				EGL042001Message msgList = null;
				while (list.getNextRow()) {
					msgList = (EGL042001Message) list.getRecord();
					if (firstTime) {
						firstTime = false;
						if (!list.getShowPrev()) {
							userPO.setHeader5(msgList.getE01BEGBAL());
						}	
					}
					strDebit = "&nbsp;";
					strCredit = "&nbsp;";

					if ("0".equals(msgList.getE01TRADCC())) {
						debit =
							debit.add(msgList.getBigDecimalE01TRAAMT());
						strDebit =
							Util.fcolorCCY(msgList.getE01TRAAMT());
					} else if ("5".equals(msgList.getE01TRADCC())) {
						credit =
							credit.add(
								msgList.getBigDecimalE01TRAAMT());
						strCredit =
							Util.fcolorCCY(msgList.getE01TRAAMT());
					}

					myRow = new StringBuffer("<TR>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\">"
						+ Util.formatDate(
							msgList.getE01DATE11(),
							msgList.getE01DATE12(),
							msgList.getE01DATE13())
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=\"RIGHT\">"
						+ Util.formatCell(msgList.getE01TRAACC())
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=\"CENTER\">"
						+ Util.formatCell(msgList.getE01TRACDE())
						+ "</TD>");
					if (msgList.getE01TRADRR().equals("0")) {
						myRow.append("<TD NOWRAP>"
							+ Util.formatCell(msgList.getE01TRANAR())
							+ "</TD>");
					} else {
						myRow.append("<TD NOWRAP><A HREF=\"javascript:GetStatDesc('"
							+ msgList.getE01TRADRR()
							+ "','"
							+ msgList.getE01TRANAR()
							+ "','"
							+ Util.formatDate(
								msgList.getE01DATE11(),
								msgList.getE01DATE12(),
								msgList.getE01DATE13())
							+ "','"
							+ Util.formatCell(msgList.getE01TRACDE())
							+ "')\">"
							+ Util.formatCell(msgList.getE01TRANAR())
							+ "</A></TD>");
					}
					myRow.append("<TD NOWRAP ALIGN=RIGHT>"
						+ strDebit
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>"
						+ strCredit
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>"
						+ Util.fcolorCCY(msgList.getE01ENDBAL())
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=RIGHT>"
						+ Util.formatCell(msgList.getE01TRABTH())
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=CENTER>"
						+ Util.formatDate(
							msgList.getE01DATE21(),
							msgList.getE01DATE22(),
							msgList.getE01DATE23())
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=CENTER>"
						+ msgList.getE01TRATMS()
						+ "</TD>");
					myRow.append("<TD NOWRAP>"
						+ Util.formatCell(msgList.getE01TRAUSA())
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=CENTER>"
						+ Util.formatCell(msgList.getE01TRAOBK())
						+ "</TD>");
					myRow.append("<TD NOWRAP ALIGN=CENTER>"
						+ Util.formatCell(msgList.getE01TRAOBR())
						+ "</TD>");
					myRow.append("</TR>");
					beanList.addRow("", myRow.toString());
				}
				beanList.setShowNext(list.getShowNext());
				beanList.setShowPrev(list.getShowPrev());
				beanList.setFirstRec(list.getFirstRec());
				beanList.setLastRec(list.getLastRec());
				
				userPO.setHeader19(Util.fcolorCCY(debit.toString()));
				userPO.setHeader20(Util.fcolorCCY(credit.toString()));

				flexLog("Putting java beans into the session");
				session.setAttribute("userPO", userPO);
				session.setAttribute("glList", beanList);
				
				if ("V".equals(msgList.getE01VALBTH())) {
					forward("EGL0420_st_list_fv.jsp", req, res);
				} else if ("B".equals(msgList.getE01VALBTH())) {
					forward("EGL0420_st_list_fp.jsp", req, res);
				} else {
					forward(devPage, req, res);
				}
			}
			
		} finally {
			if (mp != null) mp.close();
		}
	}

}
