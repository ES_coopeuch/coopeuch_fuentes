/**
 * NexusServiceInformation.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * q0834.18 v82708152146
 */

package cl.coopeuch.core.nexus.nexus;

public class NexusServiceInformation implements com.ibm.ws.webservices.multiprotocol.ServiceInformation {

    private static java.util.Map operationDescriptions;
    private static java.util.Map typeMappings;

    static {
         initOperationDescriptions();
         initTypeMappings();
    }

    private static void initOperationDescriptions() { 
        operationDescriptions = new java.util.HashMap();

        java.util.Map inner0 = new java.util.HashMap();

        java.util.List list0 = new java.util.ArrayList();
        inner0.put("consultaEstadosDeCuentaPDF", list0);

        com.ibm.ws.webservices.engine.description.OperationDesc consultaEstadosDeCuentaPDF0Op = _consultaEstadosDeCuentaPDF0Op();
        list0.add(consultaEstadosDeCuentaPDF0Op);

        java.util.List list1 = new java.util.ArrayList();
        inner0.put("consultaMovimientosNoFacturados", list1);

        com.ibm.ws.webservices.engine.description.OperationDesc consultaMovimientosNoFacturados1Op = _consultaMovimientosNoFacturados1Op();
        list1.add(consultaMovimientosNoFacturados1Op);

        operationDescriptions.put("NexusSOAP",inner0);
        operationDescriptions = java.util.Collections.unmodifiableMap(operationDescriptions);
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _consultaEstadosDeCuentaPDF0Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc consultaEstadosDeCuentaPDF0Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "entrada"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "DatosEstadosDeCuentaPDF"), cl.coopeuch.core.nexus.DatosEstadosDeCuentaPDF.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("inputPosition","0");
        _params0[0].setOption("partQNameString","{http://nexus.core.coopeuch.cl}DatosEstadosDeCuentaPDF");
        _params0[0].setOption("partName","DatosEstadosDeCuentaPDF");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "salida"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "RespuestaEstadosDeCuentaPDF"), cl.coopeuch.core.nexus.RespuestaEstadosDeCuentaPDF.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("outputPosition","0");
        _returnDesc0.setOption("partQNameString","{http://nexus.core.coopeuch.cl}RespuestaEstadosDeCuentaPDF");
        _returnDesc0.setOption("partName","RespuestaEstadosDeCuentaPDF");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        consultaEstadosDeCuentaPDF0Op = new com.ibm.ws.webservices.engine.description.OperationDesc("consultaEstadosDeCuentaPDF", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "consultaEstadosDeCuentaPDF"), _params0, _returnDesc0, _faults0, null);
        consultaEstadosDeCuentaPDF0Op.setOption("inoutOrderingReq","false");
        consultaEstadosDeCuentaPDF0Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "NexusPortType"));
        consultaEstadosDeCuentaPDF0Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "consultaEstadosDeCuentaPDFResponse"));
        consultaEstadosDeCuentaPDF0Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "NexusService"));
        consultaEstadosDeCuentaPDF0Op.setOption("buildNum","q0834.18");
        consultaEstadosDeCuentaPDF0Op.setOption("ResponseNamespace","http://nexus.core.coopeuch.cl");
        consultaEstadosDeCuentaPDF0Op.setOption("targetNamespace","http://nexus.core.coopeuch.cl/nexus");
        consultaEstadosDeCuentaPDF0Op.setOption("ResponseLocalPart","consultaEstadosDeCuentaPDFResponse");
        consultaEstadosDeCuentaPDF0Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "consultaEstadosDeCuentaPDFRequest"));
        consultaEstadosDeCuentaPDF0Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.WRAPPED);
        return consultaEstadosDeCuentaPDF0Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _consultaMovimientosNoFacturados1Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc consultaMovimientosNoFacturados1Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "entrada"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "DatosMovimientosNoFacturados"), cl.coopeuch.core.nexus.DatosMovimientosNoFacturados.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("inputPosition","0");
        _params0[0].setOption("partQNameString","{http://nexus.core.coopeuch.cl}DatosMovimientosNoFacturados");
        _params0[0].setOption("partName","DatosMovimientosNoFacturados");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "salida"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "RespuestaMovimientosNoFacturados"), cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("outputPosition","0");
        _returnDesc0.setOption("partQNameString","{http://nexus.core.coopeuch.cl}RespuestaMovimientosNoFacturados");
        _returnDesc0.setOption("partName","RespuestaMovimientosNoFacturados");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        consultaMovimientosNoFacturados1Op = new com.ibm.ws.webservices.engine.description.OperationDesc("consultaMovimientosNoFacturados", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "consultaMovimientosNoFacturados"), _params0, _returnDesc0, _faults0, null);
        consultaMovimientosNoFacturados1Op.setOption("inoutOrderingReq","false");
        consultaMovimientosNoFacturados1Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "NexusPortType"));
        consultaMovimientosNoFacturados1Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "consultaMovimientosNoFacturadosResponse"));
        consultaMovimientosNoFacturados1Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "NexusService"));
        consultaMovimientosNoFacturados1Op.setOption("buildNum","q0834.18");
        consultaMovimientosNoFacturados1Op.setOption("ResponseNamespace","http://nexus.core.coopeuch.cl");
        consultaMovimientosNoFacturados1Op.setOption("targetNamespace","http://nexus.core.coopeuch.cl/nexus");
        consultaMovimientosNoFacturados1Op.setOption("ResponseLocalPart","consultaMovimientosNoFacturadosResponse");
        consultaMovimientosNoFacturados1Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl/nexus", "consultaMovimientosNoFacturadosRequest"));
        consultaMovimientosNoFacturados1Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.WRAPPED);
        return consultaMovimientosNoFacturados1Op;

    }


    private static void initTypeMappings() {
        typeMappings = new java.util.HashMap();
        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "DatosEstadosDeCuentaPDF"),
                         cl.coopeuch.core.nexus.DatosEstadosDeCuentaPDF.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "RespuestaEstadosDeCuentaPDF"),
                         cl.coopeuch.core.nexus.RespuestaEstadosDeCuentaPDF.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "Error"),
                         cl.coopeuch.core.nexus.Error.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "DatosMovimientosNoFacturados"),
                         cl.coopeuch.core.nexus.DatosMovimientosNoFacturados.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "RespuestaMovimientosNoFacturados"),
                         cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "listaTransacciones"),
                         cl.coopeuch.core.nexus.ListaTransacciones.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "ArrayOfDetalleError"),
                         cl.coopeuch.core.nexus.ArrayOfDetalleError.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "DetalleError"),
                         cl.coopeuch.core.nexus.DetalleError.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "RECORD"),
                         cl.coopeuch.core.nexus.RECORD.class);

        typeMappings = java.util.Collections.unmodifiableMap(typeMappings);
    }

    public java.util.Map getTypeMappings() {
        return typeMappings;
    }

    public Class getJavaType(javax.xml.namespace.QName xmlName) {
        return (Class) typeMappings.get(xmlName);
    }

    public java.util.Map getOperationDescriptions(String portName) {
        return (java.util.Map) operationDescriptions.get(portName);
    }

    public java.util.List getOperationDescriptions(String portName, String operationName) {
        java.util.Map map = (java.util.Map) operationDescriptions.get(portName);
        if (map != null) {
            return (java.util.List) map.get(operationName);
        }
        return null;
    }

}
