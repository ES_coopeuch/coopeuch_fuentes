<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields,java.util.List,java.util.Iterator,cl.coopeuch.creditos.predictorriesgo.sinacofi.Salida"%>

<%@page import="java.util.ArrayList,java.math.BigDecimal"%>
<%@page import="com.ibm.ObjectQuery.crud.util.Array"%><html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%	//Sacamos de session el Objeto Sinacofi
	Salida  objSinac = (Salida)session.getAttribute("objSinac");
%>
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
</head>


<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
 
 java.text.DecimalFormat formateador = new java.text.DecimalFormat("###,###,###,###.##");
%>

<h3 align="center"> Plataforma de Ventas - Consulta BUREAU<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="consulta_sinacofi.jsp,JSEPV1035"></h3>
<hr size="4">
 <% int row = 0;%>
 <h4>Datos Solicitud </h4> 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="30%">	  			
	  			<eibsinput:text name="document" property="E01PVMNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="30%">
				<eibsinput:date name="document" fn_year="E01PVMODY" fn_month="E01PVMODM" fn_day="E01PVMODD" readonly="true"/>             	  			
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" > Agencia :</td>
             <td nowrap align="left">
				<eibsinput:text name="document" property="E01PVMBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true" />	  			
             </td>
             <td nowrap align="right"> Ejecutivo :  </td>
             <td nowrap align="left" >    	        
             	<eibsinput:text name="document" property="E01PVMOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>" readonly="true" />
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" >Estatus :</td>
             <td nowrap align="left">
				<eibsinput:text name="document" property="E01PVMSTS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />
             	<eibsinput:text name="document" property="E01DSCSTS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>" readonly="true"/>    	                     					  			
             </td>
             <td nowrap align="right"></td>
             <td nowrap align="left" >    	        
             </td>
         </tr>                
        </table>
      </td>
    </tr>
  </table>
  
<h4>Datos Solicitante </h4> 
 <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="30%">
				<eibsinput:text name="document" property="E01PVMCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" />	  			
             </td>
             <td nowrap align="right" width="20%"> RUT :</td>
             <td nowrap align="left" width="30%">
             	<eibsinput:text name="document" property="E01CUSIDE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>    	        
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right"> Nombre :</td>
             <td nowrap align="left" colspan="3">
	  			<eibsinput:text name="document" property="E01CUSNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>                   
        </table>
      </td>
    </tr>
  </table>
<%if (objSinac!=null){//no hay errores..%>
<h4>Estado Cedula</h4> 
 <% row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%">  VIGENTE:</td>
             <td nowrap align="left" width="20%">
                  <%=objSinac.getResultsEstadoCedula().getEstadoCedula().getCedulaVigente() %>        	    	       
             </td>
             <td nowrap align="right" width="20%"> Motivo :</td>
             <td nowrap align="left" width="40%">
 				<%=objSinac.getResultsEstadoCedula().getEstadoCedula().getRazon() %>             
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
             <td nowrap align="right"> Fecha Vencimiento :</td>
             <td nowrap align="left">
             	<%=(!"".equals(objSinac.getResultsEstadoCedula().getEstadoCedula().getFecha())?objSinac.getResultsEstadoCedula().getEstadoCedula().getFecha():"N/D" ) %>   
             </td>                      
             <td nowrap align="right"> Fuente :</td>
             <td nowrap align="left">
             	<%=objSinac.getResultsEstadoCedula().getEstadoCedula().getFuente() %>   
             </td> 
         </tr>              
        </table>
      </td>
    </tr>
  </table>
  
 <h4>Detalle Protestos</h4>
  <% row = 0;%>
  
     <%   
  		out.println("  <table  class=\"tableinfo\">");
  		out.println("  <tr bordercolor=\"#FFFFFF\"> ");
  		out.println("        <td nowrap> ");
  	%>
  	 
      	<% 
      		List<String> lmoneda =  new ArrayList<String>();
      		List<BigDecimal> lmonto =  new ArrayList<BigDecimal>();
      		List<Integer> lcant =  new ArrayList<Integer>();
			//arreglo que contiene en la posicion un arreglo de los acreedores      		
      	    List<List<String>> lacreedor =  new ArrayList<List<String>>();
      	          		      		
      		List<Salida.ResultsProtestosyDocumentosVigentes.ProtestosyDocumentosVigentes> lista =null;
      		if (objSinac.getResultsProtestosyDocumentosVigentes()!=null){
				lista =  objSinac.getResultsProtestosyDocumentosVigentes().getProtestosyDocumentosVigentes();      		
      		}			
						
			if (lista!=null && lista.size()>0){				
				//escribimos la tabla y el encabezado
				out.println("<table cellspacing=\"0\" cellpadding=\"2\" width=\"100%\" border=\"0\" class=\"tbhead\">");
	  	%>
						
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" width="12%">Fecha Venc.</td>
		             <td nowrap align="center" width="14%">Tipo Dcto.</td>
					 <td nowrap align="center" width="8%">Moneda</td>
					 <td nowrap align="center" width="15%">Monto</td>
					 <td nowrap align="center" width="15%">Emisor/Librador.</td>
					 <td nowrap align="center" width="12%">Localidad</td>
					 <td nowrap align="center" width="8%">Nro OP.</td>
					 <td nowrap align="center" width="12%">Fecha Pub.</td>
		         </tr>	  
	  	<%			
				for (Iterator<Salida.ResultsProtestosyDocumentosVigentes.ProtestosyDocumentosVigentes> iterator = lista.iterator(); iterator.hasNext();) {
					Salida.ResultsProtestosyDocumentosVigentes.ProtestosyDocumentosVigentes object = iterator.next();
					//acumulamos el monto, moneda y documento
					 int indice = lmoneda.indexOf(object.getIdMoneda()); 
					 if (indice == -1){//no existe lo agregamos
					 	lmoneda.add(object.getIdMoneda());
					 	lmonto.add(object.getMonto());
					 	lcant.add(1);
					 	List<String> li_acreedor = new ArrayList<String>();
					 	li_acreedor.add(object.getBancoLibrador());
					 	lacreedor.add(li_acreedor);
					 }else{
					 	lmonto.set(indice,lmonto.get(indice).add(object.getMonto())) ;
					 	lcant.set(indice,new Integer((lcant.get(indice).intValue()+1))) ;
					 	List<String> li_acreedor = lacreedor.get(indice);
					 	if (!li_acreedor.contains(object.getBancoLibrador())){//lo agregamos
							li_acreedor.add(object.getBancoLibrador());
					 		lacreedor.set(indice,li_acreedor);					 		
					 	}
					 	
					 }
					 
		%>
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center"><%=object.getIFechaVencimiento()%></td>
		             <td nowrap align="center"><%=object.getDoc()%></td>
					 <td nowrap align="center"><%=object.getIdMoneda()%></td>
					 <td nowrap align="center"><%=formateador.format(object.getMonto())%></td>
					 <td nowrap align="center"><%=object.getBancoLibrador()%></td>
					 <td nowrap align="center"><%=object.getLocalidad()%></td>
		             <td nowrap align="center"><%=object.getNumeroOperacion()%></td>
		             <td nowrap align="center"><%=object.getIFechaBoletin()%></td>		             
					 
		          </tr> 		        		
		<%				
				}
				out.println("</table>");		
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        		</table>
		<%	
			}			
      %>        
      
     <%
		out.println("  </td>");
		out.println("</tr>");
		out.println("</table>");
  	%>
 
 
 <%
 if (lista!=null && lista.size()>0){
 %>
 	
 	 <h4>Resumen Protestos</h4>
		<% row = 0;%>
		  <table  class="tableinfo">
		    <tr bordercolor="#FFFFFF"> 
		      <td nowrap>       
		        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
		
		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center"  width="30%">  Moneda</td>
		             <td nowrap align="center"  width="25%">  Monto Total</td>
		             <td nowrap align="center"  width="25%">  Nro. Documentos</td>
		             <td nowrap align="center"  width="45%">  Nro. Acreedores</td>
		         </tr>
				<% for (int c=0;c<lmoneda.size();c++){
				 %>
			          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			             <td nowrap align="center"  ><%=lmoneda.get(c) %></td>
			             <td nowrap align="center"  ><%=formateador.format(lmonto.get(c))%></td>
			             <td nowrap align="center"  ><%=lcant.get(c) %></td>
			             <td nowrap align="center"  ><%=lacreedor.get(c).size()%></td>
			         </tr>				 
				 <%
				   } 
				 %>		         		                   
		        </table>
		      </td>
		    </tr>
		  </table> 	 
<% 	 
 }
 %>
  <h4>Detalle Morosidad</h4> 
   <% row = 0;%>

     <%   
  		out.println("  <table  class=\"tableinfo\">");
  		out.println("  <tr bordercolor=\"#FFFFFF\"> ");
  		out.println("        <td nowrap> ");
  	%> 
  	
      	<% 
			List<String> lmonedaM =  new ArrayList<String>();
      		List<BigDecimal> lmontoM =  new ArrayList<BigDecimal>();
      		List<Integer> lcantM =  new ArrayList<Integer>();
			//arreglo que contiene en la posicion un arreglo de los acreedores      		
      	    List<List<String>> lacreedorM =  new ArrayList<List<String>>();
      	          	
			List<Salida.ResultsMorosidad.Morosidad> listaM =null;
      		if (objSinac.getResultsMorosidad()!=null){
				listaM =  objSinac.getResultsMorosidad().getMorosidad();
      		}	      		      				
			if (listaM!=null && listaM.size()>0){
				//escribimos la tabla y el encabezado
				out.println("<table cellspacing=\"0\" cellpadding=\"2\" width=\"100%\" border=\"0\" class=\"tbhead\">");
	  	%>
						
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" width="15%">Fecha Venc.</td>
		             <td nowrap align="center" width="15%">Tipo Dcto.</td>
					 <td nowrap align="center" width="15%">Moneda</td>
					 <td nowrap align="center" width="15%">Monto</td>
					 <td nowrap align="center" width="20%">Emisor/Librador.</td>
					 <td nowrap align="center" width="20%">Localidad</td>
		         </tr>	  
	  	<%			
				for (Iterator<Salida.ResultsMorosidad.Morosidad> iterator = listaM.iterator(); iterator.hasNext();) {
					Salida.ResultsMorosidad.Morosidad object = iterator.next();
					 //acumulamos el monto, moneda y documento
					 String moneda_s = object.getIdDocumentoOrIdMonedaOrAportanteComercial().get(3).getValue().toString();
					 String emisor_s = object.getIdDocumentoOrIdMonedaOrAportanteComercial().get(2).getValue().toString();

					 int indice = lmonedaM.indexOf(moneda_s); 
					 				 
					 if (indice == -1){//no existe lo agregamos
					 	lmonedaM.add(moneda_s);
					 	lmontoM.add(object.getMonto());
					 	lcantM.add(1);
					 	List<String> li_acreedorM = new ArrayList<String>();
					 	li_acreedorM.add(emisor_s);
					 	lacreedorM.add(li_acreedorM);
					 }else{
					 	lmontoM.set(indice,lmontoM.get(indice).add(object.getMonto())) ;
					 	lcantM.set(indice,new Integer((lcantM.get(indice).intValue()+1))) ;
					 	List<String> li_acreedorM = lacreedorM.get(indice);
					 	if (!li_acreedorM.contains(emisor_s)){//lo agregamos
							li_acreedorM.add(emisor_s);
					 		lacreedorM.set(indice,li_acreedorM);					 		
					 	}
					 	
					 }
				 
					 
		%>
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center">
		             	<%=object.getIdDocumentoOrIdMonedaOrAportanteComercial().get(1).getValue()%><br>	
		             </td> 
		             <td nowrap align="center"><%=object.getDocumento()%></td>
					 <td nowrap align="center"><%=moneda_s%></td>
					 <td nowrap align="center"><%=formateador.format(object.getMonto())%></td>
					 <td nowrap align="center"><%=emisor_s%></td>
					 <td nowrap align="center"><%=object.getComuna()%></td>
		          </tr> 		        	
		<%				
				}
				out.println("</table>");		
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
        	</table>
		<%	
			}			
      %>  
      
     <%
		out.println("  </td>");
		out.println("</tr>");
		out.println("</table>");
  	%>
  
<%
 if (listaM!=null && listaM.size()>0){
 %>
 	
 	 <h4>Resumen Morosidad</h4>
		<% row = 0;%>
		  <table  class="tableinfo">
		    <tr bordercolor="#FFFFFF"> 
		      <td nowrap>       
		        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
		
		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center"  width="30%">  Moneda</td>
		             <td nowrap align="center"  width="25%">  Monto Total</td>
		             <td nowrap align="center"  width="25%">  Nro. Documentos</td>
		             <td nowrap align="center"  width="45%">  Nro. Acreedores</td>
		         </tr>
				<% for (int c=0;c<lmonedaM.size();c++){
				 %>
			          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
			             <td nowrap align="center"  ><%=lmonedaM.get(c) %></td>
			             <td nowrap align="center"  ><%=formateador.format(lmontoM.get(c))%></td>
			             <td nowrap align="center"  ><%=lcantM.get(c) %></td>
			             <td nowrap align="center"  ><%=lacreedorM.get(c).size()%></td>
			         </tr>				 
				 <%
				   } 
				 %>		         		                   
		        </table>
		      </td>
		    </tr>
		  </table> 	 
<% 	 
 }
 %>
   
 <h4>Detalle Deuda del Sistema Financiero</h4>
  <% row = 0;%>

     <%   
  		out.println("  <table  class=\"tableinfo\">");
  		out.println("  <tr bordercolor=\"#FFFFFF\"> ");
  		out.println("        <td nowrap> ");
  	%> 
  	
      	<% 
			List list =null;
      		if (objSinac.getResultsDeudaSistemaFinanciero()!=null){
				list =  objSinac.getResultsDeudaSistemaFinanciero().getDeudaSistemaFinanciero();
      		}      	
			
			if (list!=null && list.size()>0){
				//escribimos la tabla y el encabezado
				out.println("<table cellspacing=\"0\" cellpadding=\"2\" width=\"100%\" border=\"0\" class=\"tbhead\">");				
	  	%>
						
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             
		             <td nowrap align="center" width="7%">Fecha<br>Deuda.</td>
		             		             
					 <td nowrap align="center" width="7%">Directa<br>Vigente </td>
					 <td nowrap align="center" width="7%">Directa<br>Vencida</td>						 	
		             <td nowrap align="center" width="7%">Directa<br>Castigada</td>
		             					 
					 <td nowrap align="center" width="7%">Indirecta<br>Vigente </td>
					 <td nowrap align="center" width="7%">Indirecta<br>Vencida</td>					 	             
					 <td nowrap align="center" width="7%">Indirecta<br>Castigada</td>
					 
					 <td nowrap align="center" width="6%">Morosa</td>
					 	
					 <td nowrap align="center" width="7%">Comercial</td>
					 <td nowrap align="center" width="7%">Comercial<br>Vigente</td>
					 <td nowrap align="center" width="7%">Comercial<br>Vencida</td>

					 <td nowrap align="center" width="6%">Cred.<br>Consumo</td>
					 <td nowrap align="center" width="6%">Hipotec.</td>
					 <td nowrap align="center" width="6%">Inv.<br>Finan.</td>
					 <td nowrap align="center" width="6%">Pacto</td>	
					 					 
		         </tr>	  
	  	<%			
				for (Iterator iterator = list.iterator(); iterator.hasNext();) {
					Salida.ResultsDeudaSistemaFinanciero.DeudaSistemaFinanciero object = (Salida.ResultsDeudaSistemaFinanciero.DeudaSistemaFinanciero) iterator.next();
		%>
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             
		             <td nowrap align="center"><%=object.getIFechaDeuda()%></td>
		             
					 <td nowrap align="right"><%=formateador.format(object.getDeudaDirectaVigente())%></td>
					 <td nowrap align="right"><%=formateador.format(object.getDeudaVencidaDirecta())%></td>					 
		             <td nowrap align="right"><%=formateador.format(object.getDeudaCastigadaDirecta())%></td>
		             					 
					 <td nowrap align="right"><%=formateador.format(object.getDeudaIndirectaVigente())%></td>
					 <td nowrap align="right"><%=formateador.format(object.getDeudaIndirectaVencida())%></td>					 		             
					 <td nowrap align="right"><%=formateador.format(object.getDeudaCastigadaIndirecta())%></td>			
					 
					 <td nowrap align="right"><%=formateador.format(object.getDeudaMorosa())%></td>
					 	
				 	 <td nowrap align="right"><%=formateador.format(object.getDeudaComercial())%></td>
					 <td nowrap align="right"><%=formateador.format(object.getDeudaComercialVigenteMEx())%></td>				 	 
				 	 <td nowrap align="right"><%=formateador.format(object.getDeudaComercialVencidaMEx())%></td>
				 	 
				 	 <td nowrap align="right"><%=formateador.format(object.getDeudaCreditosConsumo())%></td>				 	 
				 	 <td nowrap align="right"><%=formateador.format(object.getDeudaHipotecarios())%></td>				 	 
				 	 <td nowrap align="right"><%=formateador.format(object.getDeudaInversionesFinancieras())%></td>					 	 			 	 
					 <td nowrap align="right"><%=formateador.format(object.getDeudaOperacionesConPacto())%></td>	
					 
		          </tr> 
		<%				
				}
				out.println("</table>");
			}else{
		%>
				<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">		
		          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
		             <td nowrap align="center" >
		             	El RUT no presenta información
		             </td>
		         </tr>	  
	        </table>
		<%	
			}			
      %>  

     <%
		out.println("  </td>");
		out.println("</tr>");
		out.println("</table>");
  	%>
  
 <%}else{%>
  	<h3 align="center">
	  	Consulta A BUREAU No Devuelve Datos.<BR>
	  	VERIFIQUE CON EL ADMINISTRADOR QUE SERVICIO WEB ESTE FUNCIONAL.
  	</h3>
 <%}%>    
</body>
</html>
