
function GetCodeTChannel(name,desc,aux1)
{
	fieldName=name;
	fieldDesc=desc;
	fieldAux1=aux1;
	page= prefix +language + "EPR0300_chanel_codes_helpfile.jsp";
	CenterWindow(page,450,300,2);
}

function GetFeRef(name,typ,prd,desc){

    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0325";
	fieldName=name;
	fieldAux1=typ;
	fieldAux2=prd;
	fieldAux3=desc;
	CenterWindow(page,700,300,3);
}

function GetFeRefNew(name,typ,prd){

    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0325";
	fieldName=name;
	fieldAux1=typ;
	fieldAux2=prd;
	CenterWindow(page,700,300,3);
}

function GetUser(code,name,usr){

	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0430?USR=" + usr;
	fieldName=code;
	fieldAux1=name;
	CenterWindow(page,500,450,3);

}

function GetGroupId(code){

	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0435";
	fieldName=code;
	CenterWindow(page,500,450,3);

}


function GetCloneTransfer(name)
{
	page = webapp + "/servlet/datapro.eibs.products.JSEPR1060H?SCREEN=1";
	fieldName=name;
	CenterWindow(page,600,350,3);
}

function GetCodeTransaction(name,desc,aux1)
{
	fieldName=name;
	fieldDesc=desc;
	fieldAux1=aux1;
	page= prefix +language + "EPR0300_transaction_codes_helpfile.jsp";
	CenterWindow(page,450,300,2);
}


function GetComission(ref) {
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0530?Ref=" + ref;
	CenterWindow(page,400,280,2);
}

function GetMxBank(name)
{
	fieldName=name;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0810";
	CenterWindow(page,600,270,2);
}

function GetPlastic(num)
{
	fieldNum=num;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0410";
	CenterWindow(page,300,150,2);
}

function GetCodeService(srv,srn,cia)
{
	fieldSrv=srv;
	fieldSrn=srn;
    	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0520?Cia=" + cia;
	CenterWindow(page,500,350,2);

}

function GetSequence(seq,cia,srv)
{
	fieldSeq=seq;
    	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0525?Cia=" + cia + "&Srv=" + srv;
	CenterWindow(page,500,350,2);

}


// *************************************************************************
// ESTA AYUDA TRAE EL NRO DE LA TARJETA DEBITO Y ACTUALIZA EN LA PANTALLA QUE LO
// ESTA LLAMANDO
// LOS CAMPOS NRO DE TARJETA FECHA DE EXPEDICION Y FECHA DE EXPIRACION

var monthEx ; 		// Mes Expiracion
var yearEx ;		// Anio Expiracion
var dayEm ;			// Day Emision
var monthEm ;		// Mes Emision
var yearEm ;		// Anio Emision




function GetPlastic2(num, codeflag)
{
	GetPlastic2(num, '', codeflag)
}

function GetPlastic2(num, type, codeflag)
{
	fieldName=num;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0411?type=" + type + "&codeflag=" + codeflag;
	CenterWindow(page,600,300,2);
}

function GetPlastic3(num, type, branch, user, codeflag)
{
	fieldName=num;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0413?type=" + type + "&branch=" + branch + "&user=" + user + "&codeflag=" + codeflag;
	CenterWindow(page,600,300,2);
}


function GetPlastic4(num, mEx, yEx, dEm, mEm, yEm, type, branch, user, codeflag)
{
	fieldName=num;
	monthEx = mEx ;
	yearEx = yEx ;
	dayEm = dEm ;
	monthEm = mEm ;
	yearEm = yEm ;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0413A?type=" + type + "&branch=" + branch + "&user=" + user + "&codeflag=" + codeflag;
	CenterWindow(page,600,300,2);
}
// ***************************************************************************


function GetPlasticStatus(num, desc)
{
	fieldName=num;
	fieldDesc=desc;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0414";
	CenterWindow(page,600,300,2);
}

function GetTransactionType(num, desc)
{
	fieldName=num;
	fieldDesc=desc;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0415";
	CenterWindow(page,600,300,2);
}

function GetCommissionType(num, desc)
{
	fieldName=num;
	fieldDesc=desc;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0416";
	CenterWindow(page,600,300,2);
}

function GetPaymentPeriod(num, desc)
{
	fieldName=num;
	fieldDesc=desc;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0417";
	CenterWindow(page,600,300,2);
}

function GetMxSquare(name)
{
	fieldName=name;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0815";
	CenterWindow(page,600,270,2);
}

function GetDescATMCard(name,desc)
{
	page= webapp + "/servlet/datapro.eibs.helps.JSEWD0135";
	fieldName=name;
	fieldDesc=desc;
	CenterWindow(page,400,300,2);
}

function GetDocInv(name)
{
	page= webapp + "/servlet/datapro.eibs.helps.JSEWD0165";
	fieldName=name;
	CenterWindow(page,400,300,2);
}

function NewShortCust(cusnum,cusname,cusidn) {
	fieldName=cusnum;
	fieldDesc=cusname;
	fieldId = cusidn;
	page = prefix + language + "ESD0080_client_short_enter_identification.jsp";
	CenterWindow(page,600,150,2);
}

function NewUpdateShortCust(name,desc,cusnum,cusidn) {
	fieldName=name;
	fieldDesc=desc;
	page = webapp + "/servlet/datapro.eibs.client.JSESD0080?SCREEN=7&CUSNUM=" + cusnum + "&CUSIDN=" + cusidn;
	CenterWindow(page,600,500,2);
}

function DOBPicker(d1,d2,d3)
{
    fieldDate1 = d1;
    fieldDate2 = d2;
    fieldDate3 = d3;
    fieldAux1 = false;
    page= prefix +language + "STATIC_date_picker.jsp";
    CenterWindow(page,270,270,5);
}

function DatePicker(d1,d2,d3)
{
    fieldDate1 = d1;
    fieldDate2 = d2;
    fieldDate3 = d3;
    fieldAux1 = true;
    page= prefix +language + "STATIC_date_picker.jsp";
    CenterWindow(page,270,270,5);
}

function getCodeAcpt(name,cod)
{
	page = prefix + language + "EWD0170_dft_hlp_acpt_container.jsp";
	fieldName = name;
	fieldAux1 = cod;
	CenterWindow(page,600,400,2);
}

function showTRImg(cod)
{
	page= prefix +language + "ECIF030_rt_transaction_img.jsp?imgName=" + cod;
	CenterWindow(page,600,400,2);
}

function showLCInq(cusnum)
{
	page = webapp + "/servlet/datapro.eibs.credit.JSELN0110?SCREEN=400&CUSNUM=" + cusnum;
	CenterWindow(page,600,500,2);
}

function showTransfer(wtnum)
{
	page = webapp + "/servlet/datapro.eibs.transfer.JSEDD0610?SCREEN=2&E01SCHNUM=" + wtnum;
	CenterWindow(page,600,500,2);
}

function checksStatus(chk)
{
	page = webapp + "/servlet/datapro.eibs.products.JSECH0565?SCREEN=3&chkNum=" + chk;
	CenterWindow(page,600,500,2);
}

function enterReason(op)
{
	option = op;
	var page= prefix +language + 'ESS0090_message_enter_text.jsp';
	CenterWindow(page,500,430,3);
}

function showMsgViewer() {

	page = prefix + language + "ESS0090_message_container.jsp";
	CenterWindow(page,600,300,2);

}

function showDeductionDetail(code,type,account) {

	page = webapp + "/servlet/datapro.eibs.products.JSEDL0305?SCREEN=2&CODE=" + code + "&TYPE=" + type + "&ACCOUNT="+ account;
	CenterWindow(page,600,500,2);

}

function showInqApproval(app, account, type) {
	clickElement("ACCNUM", account);
	page = webapp + "/servlet/datapro.eibs.products.JSEDL0140?SCREEN=3&ACCNUM=" + account + "&appCode=" + app + "&typeCode=" + type;
	CenterWindow(page,600,500,2);

}

function showInqApprovalDDA(app, account,type) {
	clickElement("ACCNUM", account);
	page = webapp + "/servlet/datapro.eibs.products.JSEDD1000?SCREEN=3&ACCNUM=" + account + "&appCode=" + app + "&typeCode=" + type;
	CenterWindow(page,600,500,2);

}

function showInqApprovalCL(app, account) {
	clickElement("ACCNUM", account);
	page = webapp + "/servlet/datapro.eibs.credit.JSELN0040?SCREEN=3&ACCNUM=" + account + "&appCode=" + app;
	CenterWindow(page,600,500,2);

}

function showInqApprovalPC(account, type) {
	page = webapp + "/servlet/datapro.eibs.products.JSEPC0000?SCREEN=600&E01PCMACC=" + account;
	CenterWindow(page,600,500,2);
}

function showInqApprovalClient(account) {
	clickElement("ACCNUM", account);
	page = webapp + "/servlet/datapro.eibs.client.JSESD0100?SCREEN=3&ACCNUM=" + account;
	CenterWindow(page,600,500,2);

}

function showInqApprovalColl(app,account) {
	clickElement("REFNUM", account);
	page = webapp + "/servlet/datapro.eibs.client.JSERA0080?SCREEN=3&REFNUM=" + account + "&appCode=" + app;;
	CenterWindow(page,600,500,2);
}


function showInqApprovalForex(app, account, type) {
	clickElement("ACCNUM", account);
	page = webapp + "/servlet/datapro.eibs.products.JSEDL9140?SCREEN=3&ACCNUM=" + account + "&appCode=30" + "&typeCode=" + type;
	CenterWindow(page,600,500,2);

}

function showInqApprovalCards(app, account, type, card) {
	clickElement("ACCNUM", account);
	page = webapp + "/servlet/datapro.eibs.products.JSECC0140?SCREEN=3&ACCNUM=" + account + "&appCode=" + app + "&typeCode=" + type + "&CARDNUM=" + card;
	CenterWindow(page,600,500,2);

}

function showInqApprovalCC(app, account, type) {
	clickElement("ACCNUM", account);
	page = webapp + "/servlet/datapro.eibs.products.JSECC0140?SCREEN=3&ACCNUM=" + account + "&appCode=" + app + "&typeCode=" + type;
	CenterWindow(page,600,500,2);

}


function GetCardAccounts(){
	try{
		var card = document.forms[0].CARD.value;
		window.location.href= webapp + "/servlet/datapro.eibs.products.JSECC0080?SCREEN=800&E01CCANUM=" + card;
	} catch (e){
		window.location.href= webapp + "/servlet/datapro.eibs.products.JSECC0080?SCREEN=800";
	}
}

function GetCardAccountsInq(){
	try{
		var card = document.forms[0].CARD.value;
		window.location.href = webapp + "/servlet/datapro.eibs.products.JSECC0080I?SCREEN=800&E01CCANUM=" + card;
	} catch (e){
		window.location.href = webapp + "/servlet/datapro.eibs.products.JSECC0080I?SCREEN=800";
	}
}


function GetTableFeeCod(name,searcher)
{

	fieldName=name;
    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0412?Search=" + searcher;
	CenterWindow(page,600,100,3);
}

function GetCodeCreditCard(name, flag)
{
	page= webapp + "/servlet/datapro.eibs.helps.JSEWD0410?codeflag=" + flag;
	fieldName=name;
	CenterWindow(page,400,400,2);
}

function showInqAcc(account) {

	page = webapp + "/servlet/datapro.eibs.client.JSECIF010?SCREEN=400&opt=1&ACCNUM=" + account;
	CenterWindow(page,600,500,2);

}

function showInqCollateral(account,apcod) {

	page = webapp + "/servlet/datapro.eibs.client.JSERA0000?SCREEN=4&APPCODE="+ apcod +"&ACCNUM=" + account;
	CenterWindow(page,600,500,2);

}

function goGaBasic() {
 var ref="";
 var client="";
 ref = getElementChecked("REFNUM");

  if (ref !== ""){

	page = webapp + "/servlet/datapro.eibs.client.JSERA0010?SCREEN=8&REF=" + ref + "&CLIENT=" + client;
	CenterWindow(page,600,500,2);

 }
}


function showProdAcc(app,flg,typ,cde)
{
	flg = "";
	page = webapp + "/servlet/datapro.eibs.client.JSECIF010?SCREEN=9&appCode=" + app + "&flag=" + flg + "&prodType=" + typ + "&prodCode=" + cde;
	CenterWindow(page,600,500,2);
}

function showServices(customer)
{
    page = webapp + "/servlet/datapro.eibs.products.JSECP001?SCREEN=4&CUSTOMER=" + customer;
	CenterWindow(page,530,530,2);

}


function showOffChkApproval(account, currency, purpose) {

	page = webapp + "/servlet/datapro.eibs.products.JSEOF0115A?SCREEN=3&ACCNUM=" + account + "&Currency=" + currency + "&Purpose=" + purpose;
	CenterWindow(page,600,500,2);

}

function showOffChkApprovalReemplazo(account, currency, purpose) {

	page = webapp + "/servlet/datapro.eibs.products.JSEOF0130?SCREEN=600&ACCNUM=" + account;
	CenterWindow(page,1000,720,2);

}

function getOffClass(name,page)
{
	page= prefix +language + page;
	fieldName=name;
	CenterWindow(page,320,300,2);
}

function showAcc(app,flg)
{
	page = webapp + "/servlet/datapro.eibs.client.JSECIF010?SCREEN=9&appCode=" + app + "&flag=" + flg;
	CenterWindow(page,600,500,2);
}

function showAcc2(app,flg, ofc)
{
	page = webapp + "/servlet/datapro.eibs.client.JSECIF010?SCREEN=9&appCode=" + app + "&flag=" + flg+ "&ofc=" + ofc;
	CenterWindow(page,600,500,2);
}



function showCDRates(ccy,tb)
{
	page = webapp + "/servlet/datapro.eibs.tables.JSECN0030?CCY=" + ccy + "&TABLE=" + tb;
	fieldName=name;
	CenterWindow(page,560,500,2);
}

function showDDAServCharge(bnk,typ,tb)
{
	page = webapp + "/servlet/datapro.eibs.tables.JSESD0712?BANK=" + bnk + "&TYPE=" + typ + "&TABLE=" + tb;
	fieldName=name;
	CenterWindow(page,560,500,2);
}

function showTransfDet(type,num,bank) {

	page =  webapp + "/servlet/datapro.eibs.transfer.JSEDD0610?SCREEN=5&TPY=" + type + "&NMR=" + num + "&BNK=" + bank;
	CenterWindow(page,600,500,2);

}

function showLCServCharge(bnk,typ,tb)
{
	page = webapp + "/servlet/datapro.eibs.tables.JSESD0714?BANK=" + bnk + "&TYPE=" + typ + "&TABLE=" + tb;
	fieldName=name;
	CenterWindow(page,600,500,2);
}

function showLNServCharge(bnk,typ,tb)
{
	page = webapp + "/servlet/datapro.eibs.tables.JSESD0713?BANK=" + bnk + "&TYPE=" + typ + "&TABLE=" + tb;
	fieldName=name;
	CenterWindow(page,600,500,2);
}

function showCOLServCharge(bnk,typ,tb)
{
	page = webapp + "/servlet/datapro.eibs.tables.JSESD0715?BANK=" + bnk + "&TYPE=" + typ + "&TABLE=" + tb;
	fieldName=name;
	CenterWindow(page,600,500,2);
}

function showErrors()
{
	var page= prefix +language + "error_viewer.jsp";
	errorwin = CenterNamedWindow(page,'error',420,300,2);
}

function showWarnings(accnum,name)
{
	clickElement(name, accnum);
	page = webapp + "/servlet/datapro.eibs.alerts.JSESD0000?ACCNUM=" + accnum;
	CenterWindow(page,420,300,2);
}

function GetCode(name,page)
{
	page= prefix +language + page;
	fieldName=name;
	CenterWindow(page,320,400,3);
}

function lnGetOver(name,page)
{
	page= prefix +language + page;
	fieldName=name;
	CenterWindow(page,400,250,2);

}

function lnGetContr(name,page)
{
	page= prefix +language + page;
	fieldName=name;
	CenterWindow(page,400,300,2);
}

function lnGetFact(name,page)
{
	page= prefix +language + page;
	fieldName=name;
	CenterWindow(page,400,300,2);
}

function GetCodeDesc(name,desc,page)
{
	page= prefix +language + page;
	fieldName=name;
	fieldDesc=desc;
	CenterWindow(page,320,450,2);
}

function GetCodeCNOFC(name, flag)
{
	page= prefix +language + "EWD0002_helpfile_CNOFC.jsp?codeflag=" + flag;
	fieldName=name;
	CenterWindow(page,400,450,2);
}

function GetCodeAuxCNOFC(name, flag, codeaux)
{
	page= prefix +language + "EWD0002_helpfile_CNOFC.jsp?codeflag=" + flag + "&codeaux="+codeaux;
	fieldName=name;
	CenterWindow(page,400,330,2);
}

function GetCodeDescCNOFC(name, desc, flag)
{
	page= prefix +language + "EWD0002_helpfile_CNOFC_Desc.jsp?codeflag=" + flag;
	fieldName=name;
	fieldDesc=desc;
	CenterWindow(page,320,450,2);
}

function GetCodeDescIndiCNOFC(name, desc, indi, flag)
{
	page= prefix +language + "EWD0002_helpfile_CNOFC_Desc_indi.jsp?codeflag=" + flag;
	fieldName=name;
	fieldDesc=desc;
	fieldIndi=indi;
	CenterWindow(page,320,450,2);
}

function GetCodeDescCNTIN(name, desc, flag)
{
	page= prefix +language + "EWD0003_helpfile_CNTIN_Desc.jsp?codeflag=" + flag;
	fieldName=name;
	fieldDesc=desc;
	CenterWindow(page,320,450,2);
}

function GetCodeDescFILES(name, desc, flag)
{
	page= prefix +language + "EWD0004_helpfile_FILES_Desc.jsp?codeflag=" + flag;
	fieldName=name;
	fieldDesc=desc;
	CenterWindow(page,320,450,2);
}

function GetCNOFCFilteredCodes(name, desc, filter1, flag)
{
  	fieldName=name;
	fieldDesc=desc;
	var filter=filter1;
    page= prefix +language + "EWD0002_helpfile_CNOFC_DescF.jsp?filter=EWDMID&EWDMID="+filter+"&codeflag=" + flag;
	// alert (page);
	CenterWindow(page,450,300,2);

}

function GetCodeDescAuxCNOFC(name, desc, flag, codeaux)
{
page= prefix +language + "EWD0002_helpfile_CNOFC_Desc.jsp?codeflag=" + flag + "&codeaux="+codeaux;
fieldName=name;
fieldDesc=desc;
CenterWindow(page,320,400,2);
}

// GetParroquiaCodes
function GetParroquiaCodes(name, desc, filter1, filter2,flag)
{

	fieldName=name;
	fieldDesc=desc;
	var filter="";
	// alert(filter1+ " , "+ filter2);
	if (filter1 != "")
	  filter=filter1;
	else
	  filter=filter2;
	page= prefix +language + "EWD0002_helpfile_CNOFC_DescF.jsp?filter=EWDMID&EWDMID="+filter+"&codeflag=" + flag;
	// alert (page);
	CenterWindow(page,450,300,2);
}
// GetPanamaAddressCodes cnofc
function Get2FilterCodes(name, desc, flag, filter1, filter2)
{

	fieldName=name;
	fieldDesc=desc;
	var f1=filter1;
	var f2=filter2;
	// alert(filter1+ " , "+ filter2);
	page= prefix +language + "EWD0002_helpfile_CNOFC_DescF.jsp?filter=EWDMID&filter=EWDMIC&EWDMID="+f1+"&EWDMIC=" + f2+"&codeflag=" + flag;
	// alert (page);
	CenterWindow(page,450,300,2);
}

// Used only to CorpBanca
function GetCodeDescCNOFC1(name, desc,name1, desc1, flag)
{
	page= prefix +language + "EWD0152_helpfile_CNOFC.jsp?codeflag=" + flag;
	fieldName=name;
	fieldDesc=desc;
	fieldAux1=name1;
	fieldAux2=desc1;
	CenterWindow(page,450,450,2);
}

// Used only to CorpBanca
function GetCodeDescDeal(name, desc)
{
	page= prefix +language + "EWD0158_Deal_Table.jsp";
	fieldName=name;
	fieldDesc=desc;
	CenterWindow(page,450,400,2);
}

function GetCustomer(name)
{
	GetCustomerDescId(name, '', '');
}

function GetCustomerDetails(customer,name,idnumber,idtype,idcountry,address2,address3,address4,city,state,country,pob,zip,citizen,prof,gender,marital,phone){

    page= prefix + language + "EWD0001_client_desc_details_help_container.jsp";
	fieldAux1=customer;
	fieldAux2=name;
	fieldAux3=idnumber;
	fieldAux4=idtype;
	fieldAux5=idcountry;
	fieldAux6=address2;
	fieldAux7=address3;
	fieldAux8=address4;
	fieldAux9=city;
	fieldAux10=state;
	fieldAux11=country;
	fieldAux12=pob;
	fieldAux13=zip;
	fieldAux14=citizen;
	fieldAux15=prof;
	fieldAux16=gender;
	fieldAux17=marital;
	fieldAux18=phone;

	CenterWindow(page,750,550,2);
}

function GetCustomerDetails2(cus,nme,idn,idt,idc,ctr,ste,rgn,mun,cty,par,pob,urb,str,hom,flr,apt,ref,zip,ctz,prf,gnd,mst,phn){

    page= prefix + language + "EWD0700_customer_details_help_container.jsp";
	fieldAux1=cus;
	fieldAux2=nme;
	fieldAux3=idn;
	fieldAux4=idt;
	fieldAux5=idc;
	fieldAux6=ctr;
	fieldAux7=ste;
	fieldAux8=rgn;
	fieldAux9=mun;
	fieldAux10=cty;
	fieldAux11=par;
	fieldAux12=pob;
	fieldAux13=urb;
	fieldAux14=str;
	fieldAux15=hom;
	fieldAux16=flr;
	fieldAux17=apt;
	fieldAux18=ref;
	fieldAux19=zip;
	fieldAux20=ctz;
	fieldAux21=prf;
	fieldAux22=gnd;
	fieldAux23=mst;
	fieldAux24=phn;

	CenterWindow(page,750,550,2);
}

function GetCustomerDetailsLC(customer,name,address2,address3,address4,state,zip,pais,type){

    page= prefix + language + "EWD0001_client_lc_help_container.jsp";
	fieldAux1=customer;
	fieldAux2=name;
	fieldAux3=address2;
	fieldAux4=address3;
	fieldAux5=address4;
	fieldAux6=state;
	fieldAux7=zip;
	fieldAux8=pais;
	fieldAux9=type;

	CenterWindow(page,750,550,2);
}

function GetCustomerId(name)
{
	GetCustomerDescId('', '', name);
}

function GetVendor(name)
{
	page= prefix + language + "EWD0030_vendor_help_container.jsp";
	fieldName=name;
	CenterWindow(page,500,500,2);
}

function GetVendor(name, codeflag)
{
	page= prefix + language + "EWD0030_vendor_help_container.jsp?codeflag=" + codeflag;
	fieldName=name;
	CenterWindow(page,500,500,2);
}

function GetDepositType(name,desc)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWB0010";
	fieldName=name;
	fieldDesc=desc;
	CenterWindow(page,500,500,2);
}

function GetLocations(name,desc)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWB0030";
	fieldName=name;
	fieldDesc=desc;
	CenterWindow(page,500,500,2);
}
function GetDepositNumber(name,type,loc,client)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWB0110";
	fieldName=name;
	fieldAux1=type;
	fieldAux2=loc;
	fieldAux3=client;
	CenterWindow(page,500,500,2);
}


function GetAvailableSafeDeposits(name,desc,type, typeDsc, location, locDsc, serial, numKeys )
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0750";
	fieldName=name;
	fieldDesc=desc;
	fieldAux1=type;
	fieldAux2=typeDsc;
	fieldAux3=location;
	fieldAux4=locDsc;
	fieldAux5=serial;
	fieldAux6=numKeys;
	CenterWindow(page,500,500,2);
}

function GetInternetVendor(name, codeflag)
{
	page= prefix + language + "EWD0705_vendor_help_container.jsp?codeflag=" + codeflag;
	fieldName=name;
	CenterWindow(page,500,500,2);
}

function GetFeeTable(name)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWB0000";
	fieldName=name;
	CenterWindow(page,500,500,2);
}

function GetOfficer(name)
{
	page= prefix + language + "EWD0122_officer_help_container.jsp";
	fieldName=name;
	CenterWindow(page,500,500,2);
}

function getOficial(cod,branch)
{
	if (branch == "") branch = "999";
	fieldCod=cod;
    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0123?Branch=" + branch;
	CenterWindow(page,500,350,2);

}

function GetCustomerDescId(name, desc, id)
{
	page= prefix + language + "EWD0001_client_desc_id_help_container.jsp";
	fieldName=name;
	fieldDesc=desc;
	fieldId=id;
	fieldAux1="";
	CenterWindow(page,880,540,2);
}

function GetCustomerDescId2(name, desc, id)
{
	page= prefix + language + "EWD0001B_client_desc_id_help_container.jsp";
	fieldName=name;
	fieldDesc=desc;
	fieldId=id;
	fieldAux1="";
	CenterWindow(page,530,560,2);
}

function GetCorrespondentDescId(name, desc, id)
{
	page= prefix + language + "EWD0505_correspondent_desc_id_help_container.jsp";
	fieldName=name;
	fieldDesc=desc;
	fieldId=id;
	fieldAux1="";
	CenterWindow(page,530,530,2);
}
function GetCorrespondentDescIdSwift(name, desc, id)
{
	page= prefix + language + "EWD0545_correspondent_desc_id_help_container.jsp";
	fieldName=name;
	fieldDesc=desc;
	fieldId=id;
	fieldAux1="";
	CenterWindow(page,530,530,2);
}

function GetCustomerCorp(name, desc, id)
{
	page= prefix + language + "EWD0001_client_desc_id_help_container.jsp";
	fieldName=name;
	fieldDesc=desc;
	fieldId=id;
	fieldAux1="1";
	CenterWindow(page,530,530,2);
}

function GetCustomerDescIdW(name, desc, id)
{
	page= prefix + language + "EWD0001W_client_desc_id_help_container.jsp";
	fieldName=name;
	fieldDesc=desc;
	fieldId=id;
	CenterWindow(page,530,530,2);
}

function GetAccount(name,bnk,app,sel)
{
   page= prefix +language + "EWD0005_client_help_container.jsp";
   fieldName=name;
   fieldDesc="";
   fieldAux1="";
   fieldAux2="";
   fieldAux3="";
   fieldAux4="";
   AppCode = app;
   Bank = bnk;
   Selection=sel;
   CenterWindow(page,980,550,2);
}

function GetAccountCustomer(name,bnk,app,sel,cust)
{
   page= prefix +language + "EWD0005_client_help_container.jsp";
   fieldName=name;
   AppCode = app;
   Bank = bnk;				
   Selection=sel;
   fieldDesc=cust;
   CenterWindow(page,600,450,2);
}

function GetAccountInfo(name,bnk,app,sel,id,cust,ccy,type,prod)
{
   page= prefix +language + "EWD0005_client_help_container.jsp";
   fieldName=name;
   fieldDesc=cust;
   fieldAux1=id;
   fieldAux2=ccy;
   fieldAux3=type;
   fieldAux4=prod;
   AppCode = app;
   Bank = bnk;
   Selection=sel;
   CenterWindow(page,800,550,2);
}

function GetAhorroVivienda(tipo,desc,valvdu,valvhu,valsdu,valshu,valamp)
{
   page= prefix +language + "EDD2000_tipo_help_container.jsp?Type=tipo";
   fieldTipo=tipo;
   fieldDesc=desc;
   fieldVdu=valvdu;
   fieldVhu=valvhu;
   fieldSdu=valsdu;
   fieldShu=valshu;
   fieldAmp=valamp;
   CenterWindow(page,800,550,2);
}

function GetOldAccount(bnk,brn,ccy,aty,cun,oac,acc)
{
   Bank = bnk;
   Branch = brn;
   Currency = ccy;
   PrdType = aty;
   OldAcc = oac;
   NewAcc = acc,
   Custumer = cun;
   page= prefix +language + "EWD0406_old_acc_help_container.jsp";
   CenterWindow(page,750,450,2);
}


function GetAccByClient(name,bnk,app,sel,client)
{
   page= prefix +language + "EWD0005_client_help_container.jsp";
   fieldName=name;
   fieldDesc="";
   fieldAux1="";
   fieldAux2="";
   fieldAux3="";
   fieldAux4="";
   AppCode = app;
   Bank = bnk;
   Selection=sel;
   Client=client;
   CenterWindow(page,800,550,2);
}

function GetLNReference(name,bnk,app,sel,client)
{
   GetAccByClient(name,bnk,app,sel,client);
}

function GetAccountByType(name,bnk,app,sel)
{
   page= prefix +language + "EWD0005_account_help_container.jsp";
   fieldName=name;
   AppCode = app;
   Bank = bnk;
   Selection=sel;
   CenterWindow(page,800,550,2);
}

function GetAcceptantId(name)
{
	page= prefix +language + "EWD0140_acceptant_help_container.jsp" ;
	fieldName=name;
	fieldAux1= 'none';
	CenterWindow(page,530,450,2);
}

function GetAcceptantIdNDA(name,ndaname)
{
	page= prefix +language + "EWD0140_acceptant_help_container.jsp" ;
	fieldName=name;
	fieldAux1= ndaname;
	CenterWindow(page,530,450,2);
}

function GetBeneficiaryId(name)
{
    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0141";
	fieldName=name;
	fieldAux1= 'none';
	CenterWindow(page,530,450,2);
}

function GetProduct(name,app,bank,desc,type)
{
	page= prefix +language + "EWD0008_client_help_container.jsp"
	fieldName=name;
	fieldAux1=desc;
	fieldAux2=type;
	AppCode = app;
	ProductBank = bank;
	CenterWindow(page,800,400,2);
}

function GetProductFamily(name, app, desc)
{
	page = prefix +language + "EWD0150_product_help_container.jsp"
	fieldName = name;
	AppCode = app;
	fieldAux1 = desc;
	CenterWindow(page,800,400,2);
}

function GetProductModule(name)
{
	page = prefix +language + "EWD0150_process_help_container.jsp"
	fieldName = name;
	CenterWindow(page,600,300,2);
}

function GetLedgerOrAccount(name,bnk,ccy,apc,sel)
{
   Bank = trim(bnk);
   Currency = trim(ccy);
   fieldName =name;
   AppCode = apc;
   Selection = sel;


	if ((Bank.length > 0) && (Currency.length > 0)){
     AppCode = "";
     GetLedger(fieldName,Bank,Currency,AppCode);
	}
	else {
		var n = getElementIndex(fieldName);
		if(n > 0){
			document.forms[0].elements[n-1].value = "";
	  		document.forms[0].elements[n-3].value = "";
		}
     }
     Bank = "";
     GetAccount(fieldName,Bank,AppCode,Selection);

}

function GetLedger(name,bnk,ccy,apc,desc,prodType)
{
   var prmProd = prodType == null ? "" : "?prodType=" + prodType; 	
   page = prefix + language + "EWD0010_client_help_container.jsp" + prmProd;
   fieldName=name;
   fieldDesc=desc;
   AppCode = apc;
   Bank = bnk;
   Currency=ccy;
   CenterWindow(page,600,550,2);


}

function GetCurrency(name,bank)
{
	fieldName=name;
        page = webapp + "/servlet/datapro.eibs.helps.JSEWD0012?BankNumber=" + bank;
	CenterWindow(page,500,250,2);

}

function GetCurrencyDesc(name,desc,bank)
{
	fieldName=name;
	fieldDesc=desc;
    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0012?SCREEN=2&BankNumber=" + bank;
	CenterWindow(page,500,250,2);

}

function GetInstCurrency(name,bank,inst)
{
	fieldName=name;
        page = webapp + "/servlet/datapro.eibs.helps.JSEWD0500?BankNumber=" + bank + "&Inst=" + inst;
	CenterWindow(page,500,350,2);

}


function GetCheckForeignExc(sub,name,lot,ini,fin,bal,chk,bank,branch,type,user)
{
	fieldSub=sub;
	fieldName=name;
	fieldLot=lot;
	fieldIni=ini;
	fieldFin=fin;
	fieldBal=bal;
	fieldChk=chk;
    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0510?Bank=" + bank + "&Branch=" + branch + "&Type=" + type + "&User=" + user;
	CenterWindow(page,500,350,2);

}


function GetValuePaper(lot,bal,ini,fin,bank,branch,type,sub)
{
	fieldLot=lot;
	fieldBal=bal;
	fieldIni=ini;
	fieldFin=fin;
    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0535?Bank=" + bank + "&Branch=" + branch + "&Type=" + type + "&Sub=" + sub;
	CenterWindow(page,500,350,2);

}


function GetBranch(name,bank,desc)
{

	fieldName=name;
	fieldDesc=desc;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0013?BankNumber=" + bank;
	CenterWindow(page,600,270,2);
}

function GetBatchDet(date1,date2,date3,batch,acr)
{
	var param="&DT1="+date1+"&DT2="+date2+"&DT3="+date3+"&BTH="+batch+"&ACR="+acr+"&Pos=0";
	page = webapp + "/servlet/datapro.eibs.client.JSEGL0421?SCREEN=1" + param;
	CenterWindow(page,900,500,2);
}

function GetBudget(name,bank,currency)
{
	page= prefix +language + "EWD0011_client_help_container.jsp"
	fieldName=name;
	BankNumber = bank;
	Currency = currency;
	CenterWindow(page,800,300,2);

}

function GetSwiftId(name)
{
	page= prefix +language + "EWD0130_bankid_help_container.jsp"
	fieldName=name;
	PVia="S";
	CenterWindow(page,830,450,2);

}

function GetFedId(name)
{
	page= prefix +language + "EWD0130_bankid_help_container.jsp"
	fieldName=name;
	PVia="F";
	CenterWindow(page,830,450,2);

}

function GetIssuerCode(name,type)
{
	page= prefix +language + "EWD0301_issuer_help_container.jsp"
	fieldName=name;
	fieldId=type;
	CenterWindow(page,530,450,2);
}

function GetPortfolioNum(name,type)
{
	page= prefix +language + "EWD0302_portfolio_help_container.jsp"
	fieldName=name;
	fieldId=type;
	CenterWindow(page,530,450,2);
}

function GetCommTable(name,bank)
{
	fieldName=name;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0303?Bank=" + bank;
	CenterWindow(page,400,280,2);
}

function GetDocumentType(name,type)
{
	page= prefix +language + "EWD0304_doc_help_container.jsp"
	fieldName=name;
	fieldId=type;
	CenterWindow(page,530,450,2);
}

function GetDocument(name, account, id, status)
{
   	fieldName=name;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0145?Account=" + account + "&id=" + id + "&Status=" + status;
   	CenterWindow(page,600,450,2);
}

function GetCostCenter(name,bank)
{

	fieldName=name;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0014?BankNumber=" + bank;
	CenterWindow(page,400,280,3);
}

function GetCreditLine(name,custnum)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0015?CustNum=" + custnum;
	fieldName=name;
	CenterWindow(page,630,90,3);

}

function GetCreditLineT(name,custnum)
{
	page = webapp + "/servlet/datapro.eibs.forex.JSEWD0015T?CustNum=" + custnum;
	fieldName=name;
	CenterWindow(page,630,90,2);

}

function GetSubAcc(name,account)
{
        page = webapp + "/servlet/datapro.eibs.helps.JSEWD0016?AccountNum=" + account;
	fieldName=name;
	CenterWindow(page,500,100,2);
}

function GetAct(name)
{
	page= prefix +language + "EWD0020_client_help_helpmessage.jsp"
	fieldName=name;
	CenterWindow(page,500,500,2);
}

function GetLoc(name)
{
	page= prefix +language + "EWD0021_client_help_helpmessage.jsp" ;
	fieldName=name;
	CenterWindow(page,350,170,3);
}

function GetDocLC(name,level)
{
   var LevelNum = "?Level=" + level ;
	page= prefix +language + "EWD0022_client_help_helpmessage.jsp"+ LevelNum;
	fieldName=name;
	CenterWindow(page,600,170,3);
}

function GetMailing(name,custnum)
{
        page = webapp + "/servlet/datapro.eibs.helps.JSEWD0026?CustNum=" + custnum;
	fieldName=name;
	CenterWindow(page,450,350,3);
}

function GetEntityHelp(name, desc, id, cuscun, entityType)
{
    page = webapp + "/servlet/datapro.eibs.client.JSESD4000?SCREEN=105&RECTYP="+ entityType+"&CUSCUN=" + cuscun;
	fieldName=name;
	fieldDesc=desc;
	fieldId=id;	
	CenterWindow(page,550,450,3);
}

function GetAddressList(nbr, addrln1, addrln2, addrln3, addrln4, cuscun)
{
    page = webapp + "/servlet/datapro.eibs.client.JSESD4000?SCREEN=105&RECTYP=1&CUSCUN=" + cuscun;
	fieldNbr=nbr;
	fieldAddrln1=addrln1;
	fieldAddrln2=addrln2;
	fieldAddrln3=addrln3;
	fieldAddrln4=addrln4;	
	CenterWindow(page,450,350,3);
}

function GetTellTran(name)
{
	page= prefix +language + "EWD0029_teller_code_transac_helpmessage.jsp" ;
	fieldName=name;
	CenterWindow(page,600,400,3);
}


function GetRateTable(name)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0032";
	fieldName=name;
	CenterWindow(page,430,200,3);
}

function GetRateTablePizarra(name)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0033";
	fieldName=name;
	CenterWindow(page,430,200,3);
}

function GetBanksSuc(bank, suc)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0702";
	fieldName=bank;
	fieldDesc=suc;
	
	CenterWindow(page,430,200,3);
}



function GetCNTRLPRF(name,amt)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0108";
	fieldName=name;
	fieldDesc=amt;
	CenterWindow(page,430,200,3);
}

function GetOvernightTable(name, bnk)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0045";
	fieldName=name;
	CenterWindow(page,430,200,3);
}

function GetFloating(name)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0100";
	fieldName=name;
	CenterWindow(page,600,140,3);
}

function GetTariffLC(name,searcher,EWDCUN)
{

	fieldName=name;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0101?Search=" + searcher + "&EWDCUN=" + EWDCUN;
	CenterWindow(page,600,150,3);
}

function GetRetCod(name,searcher,desc,product)
{

	fieldName=name;
	fieldDes=desc;
	
	if(product==null)
		product="";
		
    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0102?Search=" + searcher + "&Product="+product;
	CenterWindow(page,600,100,3);
}


function GetTariffColl(name,searcher,EWDCUN)
{

	fieldName=name;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0104?Search=" + searcher + "&EWDCUN=" + EWDCUN;
	CenterWindow(page,600,150,3);
}

function GetLoanTable(name,searcher)
{

	fieldName=name;
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0105?Search=" + searcher;
	CenterWindow(page,600,100,3);
}


function GetPrevTable(name)
{

        page = webapp + "/servlet/datapro.eibs.helps.JSEWD0106";
	fieldName=name;
	CenterWindow(page,600,240,3);
}

function GetCasualTable(name, desc)
{

	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0110";
	fieldName=name;
	fieldDesc=desc;
	CenterWindow(page,600,400,3);
}


function GetForExcFee(name)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0111";
	fieldName=name;
	CenterWindow(page,600,100,3);
}

function GetForExcPar(name)
{
        page = webapp + "/servlet/datapro.eibs.helps.JSEWD0112";
	fieldName=name;
	CenterWindow(page,600,100,3);
}

function GetInsPar(name)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0113";
	fieldName=name;
	CenterWindow(page,600,200,3);
}

function GetOffChkPar(name)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0115";
	fieldName=name;
	CenterWindow(page,600,250,3);
}

function GetConcept(name,bank,app,desc,ledger)
{
	var AppCode = "?App=" + app + "&Bank=" + bank;
        page = webapp + "/servlet/datapro.eibs.helps.JSEWD0035" + AppCode;
	fieldName=name;
	fieldAux1=desc;
	fieldAux2=ledger;
	CenterWindow(page,500,230,3);
}

function GetProductRates(name,app)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0002P?App=" + app;
	fieldName=name;
	CenterWindow(page,500,230,3);
}


function GetTypCHK(name,desc,prodtyp,ccy, ccy2,namccy)
{
if (ccy2 == null){
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0118?SCREEN=3&PRODTYP="+prodtyp+"&CCY="+ccy;
}else {
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0118?SCREEN=3&PRODTYP="+prodtyp+"&CCY="+ccy+"&CCY2="+ccy2;
}
	fieldName=name;
	fieldDesc=desc;
	fieldAux1=namccy;
	CenterWindow(page,500,250,3);
}

function GetTypCHKBook(name,hideF1,hideF2,desc,prodtyp,ccy)
{
	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0118?SCREEN=3&PRODTYP="+prodtyp+"&CCY="+ccy;
	fieldName=name;
	fieldAux1=hideF1;
	fieldAux2=hideF2;
	fieldDesc=desc;
	CenterWindow(page,500,250,3);
}

function GetCallReport(name,desc)
{
	page= prefix +language + "EWD0091_client_help_container.jsp";
	fieldName=name;
	fieldDesc=desc;
	CenterWindow(page,530,450,3);
}

function GetCheck(name, type, status)
{
	page = webapp + "/servlet/datapro.eibs.products.JSETL0510?SCREEN=1000&E01SELDTY="+type+"&E01SELSCH="+status;
	fieldName=name;
	CenterWindow(page,640,500,3);
}

function GetCheckByAccount(name, account, type, status)
{
	page = webapp + "/servlet/datapro.eibs.products.JSETL0510?SCREEN=1000&E01SELACC="+account+"&E01SELDTY="+type+"&E01SELSCH="+status;
	fieldName=name;
	fieldAccount="";
	CenterWindow(page,640,500,3);
}

function GetCheckAccount(check, account, type, status)
{
	page = webapp + "/servlet/datapro.eibs.products.JSETL0510?SCREEN=1000&E01SELDTY="+type+"&E01SELSCH="+status;
	fieldName=check;
	fieldAccount=account;
	CenterWindow(page,640,500,3);
}

function GetBroker(name,desc,type,seq)
{
   page= prefix +language + "EWD0023_brkr_help_container.jsp?Type=type"; 
   fieldName=name;
   fieldDesc=desc;
   fieldAux1=type;
   CenterWindow(page,600,500,2);
}


function GetBrokerI(name,desc){

	page = webapp + "/servlet/datapro.eibs.helps.JSEWD0306B";
	fieldName=name;
	fieldAux1=desc;
	CenterWindow(page,500,230,3);

}

function GetTypeBroker(name,type)
{
   page= prefix +language + "EWD0023_brkr_help_container.jsp";
   fieldName=name;
   fieldAux1=type;
   CenterWindow(page,900,400,2);
}

function GetCredCond(name,page)
{
	page= prefix +language + page;
	fieldName=name;
	CenterWindow(page,420,300,2);
}

function GetRel1(name,page)
{
	page= prefix +language + page;
	fieldName=name;
	CenterWindow(page,420,300,2);
}

function GetRel2(name,page)
{
	page= prefix +language + page;
	fieldName=name;
	CenterWindow(page,350,150,2);
}

function GetClassCred(name,page)
{
	page= prefix +language + page;
	fieldName=name;
	CenterWindow(page,420,300,2);
}

function showDedMaint() {

	var page= prefix +language + 'EDL0150_ln_ded_det.jsp';
	CenterWindow(page,600,300,2);

}

function showCollMaint(client) {

	var page= prefix +language + 'EDL0150_ln_coll_det.jsp?client='+client;
	CenterWindow(page,600,500,2);
}


function showPayMaint() {

	var page= prefix +language + 'EDL0150_ln_pay_pln_det.jsp';
	CenterWindow(page,600,500,2);

}

function showIrregPayMaint() {

	var page= prefix +language + 'EDL0150_ln_pay_pln_irreg.jsp';
	CenterWindow(page,600,500,2);

}

function showCDPayMaint() {

	page = webapp + "/servlet/datapro.eibs.products.JSEXEDL0130?SCREEN=9";
	CenterWindow(page,600,500,2);

}

function GetDepTyp(name,page)
{
	page= prefix +language + page;
	fieldName=name;
	CenterWindow(page,320,300,2);
}

function GetAccountHolders(name)
{
	page= prefix +language + "EWD0002_helpfile_CNOFC.jsp?codeflag=T8"; 
	fieldName=name;
	CenterWindow(page,340,400,2);
}

function GetStatDesc(App, Desc, Dte, TC)
{
	var AppCode = "?AppCode=" + App + "&Desc=" + Desc + "&Dte=" + Dte + "&TC=" + TC ;
	page= prefix +language + "EWD0040_st_add_desc.jsp" + AppCode;
	fieldName=name;
	CenterWindow(page,500,200,3);
}

function showQuoteDetail(rowNum)
{
	page= prefix +language + "EDL0900_ln_crn_pay_det.jsp?Row=" + rowNum;
	CenterWindow(page,800,550,3);
}

function showQuoteDetailUF(rowNum)
{
	page= prefix +language + "EDL0900_ln_crn_pay_det_UF.jsp?Row=" + rowNum;
	CenterWindow(page,800,550,3);
}

function showDocDesc() {

	page = webapp + "/servlet/datapro.eibs.trade.JSELC0455?SCREEN=2";
	CenterWindow(page,600,500,2);

}

function showDocDet(bank, branch, currency, amount, check) {

	page =  webapp + "/servlet/datapro.eibs.products.JSETL0510?SCREEN=3&BNK=" + bank + "&BRN=" + branch + "&CCY=" + currency + "&AMT=" + amount + "&CHK=" + check;
	CenterWindow(page,600,500,2);

}

function showDocDet_Chq(bank, branch, currency, amount, check) {   // nuevo

    page =  webapp + "/servlet/datapro.eibs.products.JSETL0510?SCREEN=600&BNK=" + bank + "&BRN=" + branch + "&CCY=" + currency + "&AMT=" + amount + "&CHK=" + check;
    CenterWindow(page,1000,720,2);

}


function showChkDet(bank, branch, currency, amount, check) {

	page =  webapp + "/servlet/datapro.eibs.products.JSETL0510?SCREEN=7&BNK=" + bank + "&BRN=" + branch + "&CCY=" + currency + "&AMT=" + amount + "&CHK=" + check;
	CenterWindow(page,600,500,2);

}

function showChkCanDet(bank, branch, currency, amount, check) {

	page =  webapp + "/servlet/datapro.eibs.products.JSETL0510?SCREEN=13&E02OFMBNK" + bank + "&E02OFMBRN=" + branch + "&E02OFMCCY=" + currency + "&E02OFMMCH=" + amount + "&E02OFMNCH=" + check;
	CenterWindow(page,600,500,2);

}

function showDocDesC() {

	page = webapp + "/servlet/datapro.eibs.trade.JSELC0455C?SCREEN=2";
	CenterWindow(page,600,500,2);

}

function GetBuySalRef(name,typ){

    page = webapp + "/servlet/datapro.eibs.helps.JSETL0510H";
	fieldName=name;
	fieldAux1=typ;
	fieldAux2=desc;
	CenterWindow(page,500,230,3);
}


function showMantDiv(account) {

	page =  webapp + "/servlet/datapro.eibs.products.JSETL0510H?SCREEN=3&E01EFEACC=" + account;
	window.location.href = page;


}

function showInqDiv(account) {

	page =  webapp + "/servlet/datapro.eibs.products.JSETL0510H?SCREEN=7&E01EFEACC=" + account;
	CenterWindow(page,600,500,2);

}

function goOpenerAction(opt) {
	var myopener = top.opener.document;

	 top.close();
	 if ( opt=="A" ) {
	   myopener.anchors("linkApproval").click();
	 } else if (opt=="J") {
	   myopener.anchors("linkJ").click();
	 } else if (opt=="K") {
	   myopener.anchors("linkK").click();
	 }else if (opt=="D") {
	   myopener.anchors("linkDelete").click();
	 }else if (opt=="Z") {
	   myopener.forms[0].submit();
	 } else {
	   myopener.anchors("linkReject").click();
	 }
	}
	function GetPortfolioNum(name,type)
	{
		page= prefix +language + "EWD0302_portfolio_help_container.jsp"
		fieldName=name;
		fieldId=type;
		CenterWindow(page,530,450,2);
	}

	function GetPortfolioNumDesc(port,cus,desc,cust)
	{
		if(trim(cust).length == 0){
			alert("Please enter a valid Customer number.");
	    } else {
			page = webapp + "/servlet/datapro.eibs.helps.JSEWD0302?FromRecord=0&shrCust=" + cust;
			fieldName=port;
			fieldDesc=cus;
			fieldAux1=desc;
			CenterWindow(page,620,400,2);
		}
	}

	function showRetAccountInq(code)
	{
		page = webapp + "/servlet/datapro.eibs.products.JSEDD0000I?SCREEN=1400&E01ACMACC=" + code;
		CenterWindow(page,560,500,2);
	}

	function showRetAccountHolders(code)
	{
		page = webapp + "/servlet/datapro.eibs.invest.JSEIE0010I?SCREEN=7&HOLDERS=" + code;
		CenterWindow(page,560,500,2);
	}

	function showCustomerInq(code)
	{
		page = webapp + "/servlet/datapro.eibs.client.JSESD0080?SCREEN=600&E01CUN=" + code;
		CenterWindow(page,560,500,2);
	}

	function GetCommCustodyTable(name,type,cust,table)
	{
			page = webapp + "/servlet/datapro.eibs.helps.JSEWD0312?FromRecord=0&shrCust=" + cust + "&shrType=" + type + "&shrTable=" + table;
			fieldName=name;
			CenterWindow(page,530,450,2);

	}

	function showDetailsDebit(customer,debtyp,debsts)
	{
		page = webapp + "/servlet/datapro.eibs.client.JSECIF200?SCREEN=500&E01CUSCUN=" + customer + "&DEBTYP=" + debtyp + "&DEBSTS=" + debsts;
		CenterWindow(page,560,500,2);
	}

	function showInqApprovalRT(convm) {
		page = webapp + "/servlet/datapro.eibs.misc.JSEDD1170?SCREEN=3&CONVM=" + convm;
		CenterWindow(page,600,500,2);

	}


	function GetProfileTable(name,desc){
	    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0355";
		fieldName=name;
		fieldAux1=desc;
		CenterWindow(page,500,230,3);
	}


	function GetAmortID(name,bank,branch)
	{
		fieldName=name;
	        page = webapp + "/servlet/datapro.eibs.helps.JSEWD0400?BNK=" + bank + "&BRN=" + branch;
		CenterWindow(page,700,400,2);

	}

	function GetAmortization(bnk,brn,ccy,gln,acc,acd)
	{
	   page= prefix +language + "EWD0405_amortization_help_container.jsp";
	   bank = bnk;
	   branch = brn;
	   currency=ccy;
	   glNumber=gln;
	   account=acc;
	   appCode = acd;
	   CenterWindow(page,600,450,2);
	}

	function showInqApprovalSwiftFree(user,ref)
	{
		page = webapp + "/servlet/datapro.eibs.forex.JSESWF010?SCREEN=9&USERID=" + user +"&REFNUM=" + ref;
		CenterWindow(page,560,500,2);
	}

	function GetSwiftIdDesc(name,desc)
	{
		page= prefix +language + "EWD0130D_bankid_help_container.jsp"
		fieldName=name;
		fieldDesc=desc;
		PVia="S";
		CenterWindow(page,930,500,2);

	}

	function GetSwiftIdDesc(name,desc,city,ctry)
	{
		page= prefix +language + "EWD0130D_bankid_help_container.jsp?args=4"
		fieldName=name;
		fieldDesc=desc;
		fieldAux1=city;
		fieldAux2=ctry;
		PVia="S";
		CenterWindow(page,930,500,2);

	}

	function GetSwiftAll(id,name,desc,city,ctry)
	{
		page= prefix +language + "EWD0130T_bankid_help_container.jsp"
		fieldName=id;
		fieldDesc=name;
		fieldAux1=desc;
		fieldAux2=city;
		fieldAux3=ctry;
		PVia="S";
		CenterWindow(page,930,500,2);

	}
	function GetBanksAba(id,name,type)
	{
		fieldName=id;
		fieldDesc=name;
		fieldAux1=type;
	   	page = prefix +language + "EWD0360_help_container.jsp"
		CenterWindow(page,450,300,2);
	}

	function GetAbaAll(id,name,desc,city,ctry)
	{
		page= prefix +language + "EWD0130T_bankid_help_container.jsp"
		fieldName=id;
		fieldDesc=name;
		fieldAux1=desc;
		fieldAux2=city;
		fieldAux3=ctry;
		PVia="F";
		CenterWindow(page,530,500,2);

	}

	function GetFedAll(id,name,desc,city,ctry)
	{
		page= prefix +language + "EWD0130T_bankid_help_container.jsp"
		fieldName=id;
		fieldDesc=name;
		fieldAux1=desc;
		fieldAux2=city;
		fieldAux3=ctry;
		PVia="F";
		CenterWindow(page,530,500,2);

	}

	function GetBeneficiary(customer,acc,bad1,bad2,bad3,bkid,bka1,bka2,bka3,bka4,ibid,iba1,iba2,iba3,iba4)
	{
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0131?CUSTOMER=" + customer;
		fielName=customer;
		fieldAux1=acc;
		fieldAux2=bad1;
		fieldAux3=bad2; 
		fieldAux4=bad3;
		fieldAux5=bkid;
		fieldAux6=bka1;
		fieldAux7=bka2;
		fieldAux8=bka3;
		fieldAux9=bka4;
		fieldAux10=ibid;
		fieldAux11=iba1;
		fieldAux12=iba2;
		fieldAux13=iba3;
		fieldAux14=iba4;
		CenterWindow(page,600,500,2);
	}

	function DisabledRef(field1,field2,sel){

	 if(sel == "A"){
		GetSwiftIdDesc(field1,field2);
	 }
	 if(sel == "D"){
	  return;
	 }
	}

	 function showInqSwift(fmt, ref, usr) {

		page =  webapp + "/servlet/datapro.eibs.forex.JSESWF0200?SCREEN=3&FORMAT=" + fmt + "&REFERENCE=" + ref + "&USERID=" + usr;
		CenterWindow(page,600,500,2);

	}

	function showInqSwiftI(fmt, ref, usr) {

		page =  webapp + "/servlet/datapro.eibs.forex.JSESWF0200I?SCREEN=3&FORMAT=" + fmt + "&REFERENCE=" + ref + "&USERID=" + usr;
		CenterWindow(page,600,700,2);

	}


	function showAverageAndInquiryPages(op, account) {

		page =  webapp + "/servlet/datapro.eibs.client.JSECIF170?SCREEN=4&opt=" + op + "&ACCNUM=" + account;
		CenterWindow(page,600,500,2);

	}

	function showProdAcc2(offCode,typ,prod)
	{
		page = webapp + "/servlet/datapro.eibs.client.JSECIF170?SCREEN=6&offCode=" + offCode + "&type=" + typ + "&prodType=" + prod ;
		CenterWindow(page,600,500,2);

	}

	function GetCollDraft(name)
	{
		page= prefix +language + "EWD0146_colldraft_help_container.jsp" ;
		fieldName=name;
		CenterWindow(page,530,450,2);
	}

	function GetCodeDescCNOFC(name, desc, flag)
	{
		page= prefix +language + "EWD0002_helpfile_CNOFC_Desc.jsp?codeflag=" + flag;
		fieldName=name;
		fieldDesc=desc;
		CenterWindow(page,450,550,2);
	}

	function GetCNOFCFilteredCodes(name, desc, filter1, flag)
	{
	  	fieldName=name;
		fieldDesc=desc;
		var filter=filter1;
	    page= prefix +language + "EWD0002_helpfile_CNOFC_DescF.jsp?filter=EWDMID&EWDMID="+filter+"&codeflag=" + flag;
		// alert (page);
		CenterWindow(page,450,300,2);

	}


	function GetParroquiaCodes(name, desc, filter1, filter2,flag)
	{

		fieldName=name;
		fieldDesc=desc;
		var filter="";
		// alert(filter1+ " , "+ filter2);
		if (filter1 != "")
		  filter=filter1;
		else
		  filter=filter2;
		page= prefix +language + "EWD0002_helpfile_CNOFC_DescF.jsp?filter=EWDMID&EWDMID="+filter+"&codeflag=" + flag;
		// alert (page);
		CenterWindow(page,450,300,2);
	}


	function showCollMaint(client) {

		var page= prefix +language + 'EDL0150_ln_coll_det.jsp?client='+client;
		CenterWindow(page,600,500,2);
	}


	function GetAccountAndProduct(name,product,bnk,app,sel)
	{
	   page= prefix +language + "EWD0005_client_help_container.jsp";
	   fieldName=name;
	   fieldProduct=product;
	   AppCode = app;
	   Bank = bnk;
	   Selection=sel;
	   CenterWindow(page,800,550,2);
	}

	// Propuesta de Credito

	function GetProposalsProducts(name,typname,apcname,type)
	{
		fieldName=name;
		fieldAux1=typname;
		fieldAux2=apcname;
	   	page = webapp + "/servlet/datapro.eibs.helps.JSEDP0743?E01APCTYP=" + type;
		CenterWindow(page,450,300,2);
	}

	function GetFormsProposals(name,typname,type)
	{
		fieldName=name;
		fieldAux1=typname;
		fieldAux2=type;
	   	page = webapp + "/servlet/datapro.eibs.helps.JSEDP0752?E01APFPRO=9696";
		CenterWindow(page,450,300,2);
	}

	function GetWireOpening(name,amount)
	{
		var page = webapp + "/servlet/datapro.eibs.helps.JSEPR0110?SCREEN=1";
		fieldName=name;
		fieldAux1=amount;
		CenterWindow(page,500,230,3);
	}

	function CrtCheck(name,loannbr)
	{
		page = webapp + "/servlet/datapro.eibs.products.JSEOF0115?SCREEN=700&OPTION=Y" + "&VAL=" + name + "&LNSACC=" + loannbr;
		fieldName=name;
		CenterWindow(page,1000,900,3);
	}

	function MntCheck(name,loannbr)
	{
		page = webapp + "/servlet/datapro.eibs.products.JSEOF0115?SCREEN=1400&OPTION=Y" + "&CHK=" + name + "&LNSACC=" + loannbr;
		fieldName=name;
		CenterWindow(page,1000,900,3);
	}

	function DelCheck(name,loannbr)
	{
		page = webapp + "/servlet/datapro.eibs.products.JSEOF0115?SCREEN=1500&OPTION=Y" + "&CHK=" + name + "&LNSACC=" + loannbr;
		fieldName=name;
		CenterWindow(page,1000,900,3);
	}

	function InqCheck(name,loannbr)
	{
		page = webapp + "/servlet/datapro.eibs.products.JSEOF0115?SCREEN=1600&OPTION=Y" + "&CHK=" + name + "&LNSACC=" + loannbr;
		fieldName=name;
		CenterWindow(page,1000,900,3);
	}

	function showInqColl(row) {
		page = webapp + "/servlet/datapro.eibs.client.JSERA0011?SCREEN=4&opt=4&ROW=" + row;
		CenterWindow(page,600,500,2);
	}

	function showInqPoliza(row) {
		page = webapp + "/servlet/datapro.eibs.client.JSERA0011?SCREEN=6&opt=4&ROW=" + row;
		CenterWindow(page,600,500,2);
	}

	function getClausula(type,screen,servlet)
	{
		var page = webapp + "/servlet/datapro.eibs.products.JSEWP0010?TYPE=" + type + "&SCREEN=" + screen + "&SERVLET=" + servlet;
		CenterWindow(page,500,280,3);
	}

	function GetCountryCodeOfac(name){

	    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0351";
		fieldName=name;
		CenterWindow(page,500,430,3);
	}

	function GetDenialCodeOfac(name){

	    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0352";
		fieldName=name;
		CenterWindow(page,500,430,3);
	}

	function GetInsuranceFormula(name){

	    page = webapp + "/servlet/datapro.eibs.helps.JSEWD0370";
		fieldName=name;
		CenterWindow(page,400,300,3);
	}

	function CrtCheck(name,loannbr)
	{
		page = webapp + "/servlet/datapro.eibs.products.JSEOF0115?SCREEN=700&OPTION=Y" + "&VAL=" + name + "&LNSACC=" + loannbr;
		fieldName=name;
		CenterWindow(page,1000,900,3);
	}


	function GetAchOperator(code,type,name,type1){

		page = webapp + "/servlet/datapro.eibs.ach.JSEWD0360?Type=" + type1;
		fieldName=code;
		fieldAux1=type;
		fieldDesc=name;
		CenterWindow(page,600,450,2);
	}

	function GetExternalAccountACH(benename, beneaddress, benecsz, beneemail, routing, externalacc, type,customer)
	{
		page= prefix +language + "EWD0362_ach_helpfile.jsp?customer="+ customer;
		fieldAux1=benename;
		fieldAux2=beneaddress;
		fieldAux3=benecsz;
		fieldAux4=beneemail;
		fieldAux5=routing;
		fieldAux6=externalacc;
		fieldAux7=type;

		CenterWindow(page,700,400,2);
	}

	function GetAchClass(code,name){

		page = webapp + "/servlet/datapro.eibs.ach.JSEWD0361";
		fieldName=code;
		fieldDesc=name;
		CenterWindow(page,600,450,2);
	}

	function GetAchTransaction(code,name){

		page = webapp + "/servlet/datapro.eibs.ach.JSEWD0363";
		fieldName=code;
		fieldDesc=name;
		CenterWindow(page,600,450,2);
	}

	function InqCheck(name,loannbr)
	{
		page = webapp + "/servlet/datapro.eibs.products.JSEOF0115?SCREEN=1600&OPTION=Y" + "&CHK=" + name + "&LNSACC=" + loannbr;
		fieldName=name;
		CenterWindow(page,800,600,3);
	}

	function GetLimit(name,bank,code)
	{
		fieldName=name;
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0380?bnk=" + bank + "&tbl=" + code;
		CenterWindow(page,600,400,2);
	}

	function GetReferenceCode(code)
	{
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0710";
		fieldName=code;
		CenterWindow(page,500,450,3);
	}

	function GetProductB(name,desc,app,bank)
	{
		page= prefix +language + "EWD0008_client_help_container.jsp"
		fieldName = name;
		fieldAux1 = desc; 
		AppCode = app;
		ProductBank = bank;				
		CenterWindow(page,600,400,2);
	}

	function showInqApprovalT(app, account, type, cus) {
	   var formLength = document.forms[0].elements.length;
	   for (n=0; n<formLength; n++)
	     {
	      	var elementName= document.forms[0].elements[n].name;
	      	var elementValue= document.forms[0].elements[n].value;
	      	if(elementName == "ACCNUM" && elementValue== account)
	      	{
	        		document.forms[0].elements[n].click();
	        		break;
	      	}
	      }
	    if ( app == '13' && type == '2') {
	    	page = webapp + "/servlet/datapro.eibs.forex.JSEDL0108B?SCREEN=200&E02DEAACC=" + account + "&E02DEACUN=" + cus;
	    } else {
			page = webapp + "/servlet/datapro.eibs.forex.JSEDL0140T?SCREEN=3&ACCNUM=" + account + "&appCode=" + app + "&typeCode=" + type;
		}
		CenterWindow(page,600,500,2);
	}

	function GetInstrumentParams(name,desc,currency,symbol,isin,cusip,product){
	    page= prefix +language + "EWD0308F_inst_help_container.jsp?INSTTYPE=" + product + "&ISINUM=" + isin;
		fieldName=name;
		fieldDesc=desc;
		fieldId=currency;
		fieldAux1=symbol;
		fieldAux2=isin;
		fieldDate1=cusip;
		fieldDate2=product;
		CenterWindow(page,600,400,3);
	}

	function GetCodeCovenantForm(name,ccy)
	{
		page= prefix +language + "EWD0205_covenant_form.jsp?firstCall=true";
		fieldName=name;
		fieldDesc=ccy;		
		CenterWindow(page,600,400,2);
	}

	function GetAccountRolloverTimeDepositByClient(name,amt,custnum)
	{
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0770?CustNum=" + custnum;
		fieldName=name;
		fieldDesc=amt;		
		CenterWindow(page,600,400,2);
	}

	function GetCodeCovenantRequest(name,custnum)
	{
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0200?CustNum=" + custnum;
		fieldName=name;
		CenterWindow(page,500,450,3);
	}


	function GetBankSalesPlatformFormbyBranchs(bankself,ccyself,codebranchs1,cbank1,dbank1,cbank2,dbank2,abank1,abank2)
	{
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0540?BNK="+bankself+"&CCY="+ccyself;
		fieldName=codebranchs1;
		fieldDesc=abank1;
		fieldAux1=abank2;
		fieldAux2=cbank1;
		fieldAux3=dbank1;
		fieldAux4=cbank2;
		fieldAux5=dbank2;
		CenterWindow(page,800,400,2);
	}

	function GetTablaSeguros(name,code,desc)
	{
		fieldName=name;
		fieldDesc=desc;
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0210?Code=" + code;
		CenterWindow(page,600,270,2);
	}

	function GetBankReconciliation(name,desc,aux1,aux2)
	{
		fieldName=name;
		fieldDesc=desc;
		fieldAux1=aux1;
		fieldAux2=aux2;
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0450";
		CenterWindow(page,600,270,2);
	}
	function GetChargeFiles(name,desc)
	{
		fieldName=name;
		fieldDesc=desc;
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0455";
		CenterWindow(page,600,270,2);
	}

	function GetFormsCode(name)
	{
		fieldName = name;
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0742";
		CenterWindow(page,600,270,2);
	}

	function GetEventos(num, desc, codacd)
	{
		fieldName=num;
		fieldDesc=desc;
		page = webapp + "/servlet/datapro.eibs.helps.JSEWD0419?Codacd=" + codacd;
		CenterWindow(page,600,300,2);
	}
