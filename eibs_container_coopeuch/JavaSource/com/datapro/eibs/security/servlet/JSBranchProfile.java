package com.datapro.eibs.security.servlet;

/**
 * Insert the type's description here.
 * 
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import com.datapro.eibs.security.facade.*;
import com.datapro.eibs.security.bean.*;
import com.datapro.eibs.security.object.BranchProfile;
import com.datapro.generic.beanutil.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;


public class JSBranchProfile extends datapro.eibs.master.SuperServlet {

    //Requests
	protected static final int R_BANK = 1;  
	protected static final int R_BRANCH = 2;
	
	protected static final String PAGE_BRANCH_SELECT = "BranchProfile_search.jsp";
	protected static final String PAGE_BRANCH_LIST = "BranchProfile_list.jsp";
	
	protected String LangPath = "S";
	
	/**
	 * 
	 */
	public JSBranchProfile() {
		super();
	}
	/**
	 * 
	 */
	public void destroy() {

	}
	/**
	 * 
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		//dbProp = new DBConnectorProperty();
	}
	/**
	 * 
	 */
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {

			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}

		} else {
			try {

				msgUser = (ESS0030DSMessage) session.getAttribute("currUser");

				LangPath = super.rootPath + msgUser.getE01LAN() + "/";
				
				int screen = R_BANK;
				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}

				switch (screen) {
					case R_BANK :
						procReqBank(req, res, session);
						break;
					case R_BRANCH :
						procReqBranchByBank(req, res, session);
						break;
					default :
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}
		}

	}
	
	protected void procReqBank(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		BranchProfile branchMng = new BranchProfile();
		JBObjList bankList = null;
		
		try {
				String defBank = req.getParameter("DEFBANK");
				if (defBank.equals(null)) defBank="";			
				bankList = branchMng.getBank();
				if (bankList == null) {
					bankList = new JBObjList();
				} 
				ses.setAttribute("bankList",bankList);
				try {
						flexLog("About to call Page: " + LangPath + PAGE_BRANCH_SELECT);
						//callPage(LangPath + PAGE_BRANCH_SELECT, req, res);
						res.sendRedirect(super.srctx + LangPath + PAGE_BRANCH_SELECT +"?DEFBANK=" + defBank);	
				}
				catch (Exception e) {
						flexLog("Exception calling page " + e);
				}

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	protected void procReqBranchByBank(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		
		JBObjList branchList = null;
		BranchProfile branchMng = new BranchProfile();
		
		try {			
				String bnk= req.getParameter("BRNBNK");
				
				branchList = branchMng.getBranch(bnk);
				if (branchList == null) {
					branchList = new JBObjList();
				}
				ses.setAttribute("bankBranchList",branchList);
				try {
					flexLog("About to call Page: " + LangPath + PAGE_BRANCH_LIST);
					res.sendRedirect(super.srctx + LangPath + PAGE_BRANCH_LIST);	
					}
				catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
				 

		} catch (Exception e) {
			flexLog("Error: " + e);
			//change to page of sql error
			res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
		}
	}
	
	

}
