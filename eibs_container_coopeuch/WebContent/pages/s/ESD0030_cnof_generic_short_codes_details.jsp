<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<html>
<head>
<title>Codigos de Referencia</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="refCodes" class="datapro.eibs.beans.ESD003002Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }
   
</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
%>


<H3 align="center">C&oacute;digos de Referencia del Sistema <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cnof_generic_short_codes_details.jsp, ESD0030"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSESD0030" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
   <INPUT TYPE=HIDDEN NAME="E02CNOBNK" value="<%= currUser.getE01UBK()%>">
   <INPUT TYPE=HIDDEN NAME="E02CNOSCY" value="<%= currUser.getE01BCU()%>">

  <h4>Informaci&oacute;n B&aacute;sica</h4>
  
   <% int row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="right">Clasificaci&oacute;n :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E02CNOCFL" size="3" maxlength="2" value="<%= refCodes.getE02CNOCFL().trim()%>">
              </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="23"> 
              <div align="right">C&oacute;digo :</div>
            </td>
            <td nowrap height="23"> 
              <div align="left"> 
                <input type="text" name="E02CNORCD" size="6" maxlength="4" value="<%= refCodes.getE02CNORCD().trim()%>">
                <input type="text" name="E02CNODSC" size="50" maxlength="45" value="<%= refCodes.getE02CNODSC().trim()%>" >
              </div>
            </td>
          </tr>
          
       <% if (refCodes.getE02CNOCFL().trim().equals("AB")) {%>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap width="16%" height="19"> 
               <div align="right">Actualiza Fecha Ultima Renovaci&oacute;n :</div>
             </td>
             <td nowrap height="19">
               <input type="hidden" name="E02CNOPAF" value="<%= refCodes.getE02CNOPAF()%>">
               <input type="radio" name="CE02CNOPAF" value="1" onClick="document.forms[0].E02CNOPAF.value='1'"
			   <%if(refCodes.getE02CNOPAF().equals("1")) out.print("checked");%>>
                S&iacute; 
               <input type="radio" name="CE02CNOPAF" value="2" onClick="document.forms[0].E02CNOPAF.value='2'"
			   <%if(refCodes.getE02CNOPAF().equals("2")) out.print("checked");%>>
               No <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" border="0" >
              </td>
            </tr>            
       <% } else if (refCodes.getE02CNOCFL().trim().equals("YN")) {%>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Codigo de Moneda :</div>
            </td>
            <td nowrap height="19"> 
		        <eibsinput:help name="refCodes" property="E02CNOSCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" 
               	    	fn_param_one="E02CNOSCY" fn_param_two="<%=currUser.getE01UBK() %>"/>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">Valor Referencial :</div>
              </td>
              <td nowrap height="19"> 
                <div align="left"> 
	                <eibsinput:text name="refCodes" property="E02CNOCHG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" />
                </div>
              </td>
          </tr>
       <% } else if (refCodes.getE02CNOCFL().trim().equals("IT")) {%>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">Compania de Seguros :</div>
              </td>
              <td nowrap height="19"> 
                <div align="left"> 
				<eibsinput:help name="refCodes" property="E02CNOUS1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BROKER %>" 
					fn_param_one="E02CNOUS1" fn_param_two=" " fn_param_three="I" />
                </div>
              </td>
          </tr>

       <% } else if (refCodes.getE02CNOCFL().trim().equals("X3")) {%>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="39%"> 
               <div align="right">Identificación :</div>
             </td>
             <td nowrap width="61%">  
 				<input type="text" name="E02CNORUT" size="28" maxlength="25" value="<%= refCodes.getE02CNORUT().trim()%>" >
               </td>
           </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">N&uacute;mero de Cuenta :</div>
              </td>
              <td nowrap height="19"> 
                <div align="left"> 
	   				<eibsinput:help name="refCodes" property="E02CNOSCG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" required="false" fn_param_one="E02CNOSCG" fn_param_two="<%=currUser.getE01UBK() %>" fn_param_three="RT" />
                </div>
              </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">Tipo Papel Valor :</div>
              </td>
              <td nowrap height="19"> 
                <div align="left"> 
	               <eibsinput:cnofc name="refCodes" flag="YJ" property="E02CNOUS1" fn_description="E02DSCUS1" required="false"/>
	              <eibsinput:text name="refCodes" property="E02DSCUS1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
                </div>
              </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">Subtipo Papel Valor :</div>
              </td>
              <td nowrap height="19"> 
                <div align="left"> 
		      	    <input type="text" name="E02CNOUS2" size="5" maxlength="4" value="<%= refCodes.getE02CNOUS2()%>">
		      	    <a href="javascript:GetCodeAuxCNOFC('E02CNOUS2','YI',document.forms[0].E02CNOUS1.value)"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0" ></a>
   	                <eibsinput:text name="refCodes" property="E02DSCUS2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
                </div>
              </td>
          </tr>
          <!-- BEGIN 0035_TRANSFERENCIAS ELECTRONICA DE FONDOS -->
               <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap width="16%" height="19"> 
               <div align="right">Permite Transferencias :</div>
             </td>
             <td nowrap height="19">
               <input type="hidden" name="E02CNOF01" value="<%if(refCodes.getE02CNORCD().trim().equals("") ) out.print("N"); else  out.print(refCodes.getE02CNOF01());%>">
               <input type="radio" name="CE02CNOF01" value="S" onClick="document.forms[0].E02CNOF01.value='S'"
			   <%if(refCodes.getE02CNOF01().equals("S")) out.print("checked");%>>
                S&iacute; 
               <input type="radio" name="CE02CNOF01" value="N" onClick="document.forms[0].E02CNOF01.value='N'"
			   <%if(refCodes.getE02CNOF01().equals("N") || refCodes.getE02CNORCD().trim().equals("") ) out.print("checked");%>>
               No
              </td>
            </tr>            
          <!-- END 0035_TRANSFERENCIAS ELECTRONICA DE FONDOS -->
          

          
       <% } else if (refCodes.getE02CNOCFL().trim().equals("RE")) {%>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">N&uacute;mero de Cuenta :</div>
              </td>
              <td nowrap height="19"> 
                <div align="left"> 
                <input type="text" name="E02CNOSCG" size="20" maxlength="16" value="<%= refCodes.getE02CNOSCG().trim()%>">
                <a href="javascript:GetLedger('E02CNOSCG',document.forms[0].E02CNOBNK.value,document.forms[0].E02CNOSCY.value,'')"> 
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a>
                </div>
              </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">Producto Remanentes :</div>
              </td>
              <td nowrap height="19"> 
                <div align="left"> 
	               <eibsinput:text name="refCodes" property="E02CNOUS1"  eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>" required="false"/>
	              <eibsinput:text name="refCodes" property="E02DSCUS1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>
                 <a href="javascript:GetProduct('E02CNOUS1','97','','E02DSCUS1')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="absbottom" border="0" ></a> 
                </div>
              </td>
          </tr>

       <% } else if (refCodes.getE02CNOCFL().trim().equals("RR")) {%>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">N&uacute;mero de Cuenta :</div>
              </td>
              <td nowrap height="19"> 
                <div align="left"> 
                <input type="text" name="E02CNOSCG" size="20" maxlength="16" value="<%= refCodes.getE02CNOSCG().trim()%>">
                <a href="javascript:GetLedger('E02CNOSCG',document.forms[0].E02CNOBNK.value,document.forms[0].E02CNOSCY.value,'')"> 
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0" ></a>
                </div>
              </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">Plazo :</div>
              </td>
              <td nowrap height="19"> 
                <div align="left"> 
	               <eibsinput:text name="refCodes" property="E02CNOFRP"  eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DAYS %>" required="false"/>
                </div>
              </td>
          </tr>

          <% } else {%>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">N&uacute;mero de Referencia :</div>
              </td>
              <td nowrap height="19"> 
                <div align="left"> 
                  <input type="text" name="E02CNOSCG" size="17" maxlength="16" value="<%= refCodes.getE02CNOSCG().trim()%>">
                </div>
              </td>
          </tr>
          <% }%>  
          
       
       <%  if (currUser.getE01INT().trim().equals("11")) {%>
          
       <%  if (refCodes.getE02CNOCFL().trim().equals("Y9")) {%>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Moneda de Control de Morosidad:</div>
            </td>
            <td nowrap height="19"> 
		        <eibsinput:help name="refCodes" property="E02CNOSCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" 
               	    	fn_param_one="E02CNOSCY" fn_param_two="<%=currUser.getE01UBK() %>"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Ciclo de Control de Morosidad:</div>
            </td>
            <td nowrap height="19"> 
              <div align="left"> 
                
                <select name="E02CNOF04">
                <option value="N" <% if (!(refCodes.getE02CNOF04().equals("D") ||refCodes.getE02CNOF04().equals("W")||refCodes.getE02CNOF04().equals("M")||refCodes.getE02CNOF04().equals("Q")||refCodes.getE02CNOF04().equals("S")||refCodes.getE02CNOF04().equals("Y"))) out.print("selected"); %>> No Aplica</option>
                <option value="D" <% if(refCodes.getE02CNOF04().equals("D")) out.print("selected");%>>Diario</option>
                <option value="W" <% if(refCodes.getE02CNOF04().equals("W")) out.print("selected");%>>Semanal</option>
                <option value="M" <% if(refCodes.getE02CNOF04().equals("M")) out.print("selected");%>>Mensual</option>
                <option value="Q" <% if(refCodes.getE02CNOF04().equals("Q")) out.print("selected");%>>Trimestral</option>
                <option value="S" <% if(refCodes.getE02CNOF04().equals("S")) out.print("selected");%>>Semestral</option>
                <option value="Y" <% if(refCodes.getE02CNOF04().equals("Y")) out.print("selected");%>>Anual</option>
              </select>
          
          
          
             </div>
            </td>
            
          </tr>
          
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Monto Total de Morosidad:</div>
            </td>
            <td nowrap height="19"> 
              <div align="left"> 
                <input type="text" name="E02CNOCHG" size="16" maxlength="15" value="<%= refCodes.getE02CNOCHG() %>" onKeypress="enterDecimal()" >
             </div>
            </td>
            
          </tr>


          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Porcentaje Total de Morosidad:</div>
            </td>
            <td nowrap height="19"> 
              <div align="left"> 
                <input type="text" name="E02CNOCST" size="8" maxlength="7" value="<%= refCodes.getE02CNOCST() %>" onKeypress="enterDecimal()" >
             </div>
            </td>
            
          </tr>

          
          <% }%>
  
          <%  if (refCodes.getE02CNOCFL().trim().equals("9I"))  {%>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
               <td nowrap width="16%" height="19"> 
                 <div align="right">Riesgo del Cr&eacute;dito:</div>
               </td>
               <td nowrap height="19"> 
                 <input type="text" name="E02CNOCST" size="8" maxlength="7" value="<%= refCodes.getE02CNOCST() %>" onKeypress="enterDecimal()" >
               </td>
             </tr>  
          <% }%>
           
          <%  if (refCodes.getE02CNOCFL().trim().equals("8E")) {%>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
                <td nowrap width="16%" height="19"> 
                  <div align="right">Reserva:</div>
                </td>
                <td nowrap height="19"> 
                  <input type="text" name="E02CNOACS" size="8" maxlength="7" value="<%= refCodes.getE02CNOACS() %>" onKeypress="enterDecimal()" >
                </td>
               </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
              <td nowrap width="16%" height="19"> 
                <div align="right">D&iacute;as Mora Hasta:</div>
              </td>
              <td nowrap height="19"> 
                <input type="text" name="E02CNOFRP" size="6" maxlength="5" value="<%= refCodes.getE02CNOFRP() %>" onKeypress="enterInteger()" >
              </td>
          </tr> 
          <% }%>
          
          <%  if (refCodes.getE02CNOCFL().trim().equals("10")) {%>
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             
                <td nowrap width="16%" height="19"> 
                  <div align="right">C&oacute;digo de Agencia:</div>
                </td>
                <td nowrap height="19"> 
                  <input type="text" name="E02CNOBRN" size="5" maxlength="4" value="<%= refCodes.getE02CNOBRN() %>" onKeypress="enterInteger()" >
                  <a href="javascript:GetBranch('E02CNOBRN','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="absbottom" border="0"  ></a>
                </td>
              </tr> 
          <% }%>
      <% }%>    


             
        </table>
      </td>
    </tr>
  </table>
  <div align="center">
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
  </form>
</body>
</html>
