<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page	language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@ page import = "datapro.eibs.master.Util, datapro.eibs.beans.ERC004002Message, datapro.eibs.beans.ERC004003Message" %>	
	
<html>
<head>
<title>Conciliaci�n Manual de Cartolas</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/Ajax.js"> </SCRIPT>

<style type="text/css"> 
	input { 
         text-align:right; 
    } 
</style> 

</head>

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "msg" class= "datapro.eibs.beans.ERC004001Message"  scope="session" />
<jsp:useBean id= "ibsList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "carList" class= "datapro.eibs.beans.JBObjList"  scope="session" />

<body>

<script language="JavaScript">
	

	function validate() {
  	    var debitibs = document.getElementById("IBS_TOTAL_DT").value;
  	    var debitcar = document.getElementById("CAR_TOTAL_DT").value;
  	    var creditibs = document.getElementById("IBS_TOTAL_CT").value;
  	    var creditcar = document.getElementById("CAR_TOTAL_CT").value;
	
		return (debitibs == creditcar) && (creditibs == debitcar);
	}
	
  	function goAction() {
  	    var itemsibs = "";
  	    var itemscar = "";
  	    var countibs = 0;
  	    var countcar = 0;
  		var form = document.forms[0];
		var formLength = form.elements.length;
    	for (n=0; n<formLength; n++) {
    		var elemt = form.elements[n];
      		if (elemt.type == 'checkbox') {
				if (elemt.checked) {
					if (elemt.name == 'rowcar') {
						itemscar += "&rowcar=" + elemt.value;
						countcar++;
					} else if (elemt.name == 'rowibs') {
						itemsibs += "&rowibs=" + elemt.value;
						countibs++;
					}
            	}	
    		}
		}
		if (countcar == 0 && countibs == 0) {
			alert("Error. No hay transacciones seleccionadas.");
		} else if (confirm("Ha seleccionado " + countcar + " trans. de Cartola y " + countibs + " trans. del IBS. \n\r �Desea usted continuar con esta selecci�n?")) {
			if (validate()) {
				document.forms[0].action.value += itemscar + itemsibs;  
				document.forms[0].submit();
			} else {
				alert("Error. Los totales de las transacciones seleccionadas no son iguales.");
			}
		}
  	}
  	
  	function update(obj) {
  		var url = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0040";
  		var screen = "";
  		
  		if (obj.name == 'rowibs') {
  			screen = obj.checked ? "500" : "501"
			url += "?SCREEN=" + screen + "&row=" + obj.value + "&debit=" + document.getElementById("IBS_TOTAL_DT").value + "&credit=" + document.getElementById("IBS_TOTAL_CT").value;
			GetXMLResponse(url, divCallback, false);
  		} else if (obj.name == 'rowcar') {
  			screen = obj.checked ? "600" : "601"
			url += "?SCREEN=" + screen + "&row=" + obj.value + "&debit=" + document.getElementById("CAR_TOTAL_DT").value + "&credit=" + document.getElementById("CAR_TOTAL_CT").value;
			GetXMLResponse(url, divCallback, false);
  		}
  	}

  function goAction2(op) 
   {
 	 if(op =='1200')
 	 {
 	 	document.forms[0].SCREEN.value = op;
  	 	document.forms[0].action = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0040";
	 	document.forms[0].submit();
	 }
	 else if (op =='200')	
 	 {
 	 	document.forms[0].SCREEN.value = op;
  	 	document.forms[0].action = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC2000?SCREEN=200&E01RCHSTN=<%=request.getAttribute("E01Cartola")%>&E01RCHAPF=<%=request.getAttribute("estado")%>&E01BRMEID=<%=session.getAttribute("codigo_banco")%>&E01RCHTFC=<%=request.getParameter("E01RCHTFC") %>&E01BRMACC=<%=session.getAttribute("cuenta_ibs")%>&E01DSCRBK=<%=session.getAttribute("nombre_banco")%>&E01BRMCTA=<%=session.getAttribute("cuenta_banco")%>&E01RCHFSD=<%=request.getParameter("E01RCHFSD")%>&E01RCHFSM=<%=request.getParameter("E01RCHFSM")%>&E01RCHFSY=<%=request.getParameter("E01RCHFSY")%>&E01RCHFLD=<%=request.getParameter("E01RCHFLD")%>&E01RCHFLM=<%=request.getParameter("E01RCHFLM")%>&E01RCHFLY=<%=request.getParameter("E01RCHFLY")%>&posicion=<%=request.getParameter("E01RCHREC")%>";
	 	document.forms[0].submit();
	 }
   }
  	
</script>

<H3 align="center">Conciliaci�n Manual
	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="rcbank_list_selection, ERC0040"></H3>
<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0040">
	<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="400">
	<input type="hidden" name="codigo_lista" value="<%=request.getAttribute("codigo_lista") %>">

	<input type="hidden" name="E01RCHSTN" value="<%=request.getAttribute("E01Cartola") %>">
	<input type="hidden" name="E01RCHAPF" value="<%=request.getAttribute("estado") %>"> 
	<input type="hidden" name="E01BRMEID" value="<%=session.getAttribute("codigo_banco") %>"> 

	<input type="hidden" name="E01RCHTFC" value="<%=request.getParameter("E01RCHTFC") %>" >
	<input type="hidden" name="E01BRMACC" value="<%=session.getAttribute("cuenta_ibs")%>">
	<input type="hidden" name="E01DSCRBK" value="<%=session.getAttribute("nombre_banco") %>">
 	<input type="hidden" name="E01BRMCTA" value="<%=session.getAttribute("cuenta_banco")%>">
  
  	<input type="hidden" name="E01RCHFSD" value="<%=request.getParameter("E01RCHFSD") %>"> 
	<input type="hidden" name="E01RCHFSM" value="<%=request.getParameter("E01RCHFSM") %>"> 
	<input type="hidden" name="E01RCHFSY" value="<%=request.getParameter("E01RCHFSY") %>"> 

	<input type="hidden" name="E01RCHFLD" value="<%=request.getParameter("E01RCHFLD") %>"> 
	<input type="hidden" name="E01RCHFLM" value="<%=request.getParameter("E01RCHFLM") %>"> 
	<input type="hidden" name="E01RCHFLY" value="<%=request.getParameter("E01RCHFLY") %>"> 

	<input type="hidden" name="E01RCHREC" value="<%=request.getParameter("E01RCHREC")%>">


 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" >
          <tr>
             <td nowrap width="10%" align="right">   Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input align="left" type="text" name="codigo_banco" value="<% out.print(session.getAttribute("codigo_banco")+"-"+session.getAttribute("nombre_banco"));%>" 
	  			size="60"  readonly>
             </td>          
             <td nowrap width="10%" align="right">   Cartola : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input align="left" type="text" name="codigo_banco" value="<%=Util.formatCell(msg.getE01CARSTN())%>" 
	  			size="15"  readonly>
             </td>          

         </tr>
 
         <tr>
            <td nowrap width="10%" align="right"> Cuenta Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<%=session.getAttribute("cuenta_banco")%>" 
	  			size="30"  readonly>
             </td>
                    <td nowrap width="10%" align="right"> Cuenta IBS : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<%=session.getAttribute("cuenta_ibs")%>" 
	  			size="30"  readonly>
             </td>  
          </tr>
        </table>
      </td>
    </tr>
    <tr><td height="10" ></td></tr>
 
  </table>
	
  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0">

  		<tr height="40" valign="middle" class="tableinfo">
  			<th width="59%" align="center" bgcolor="#CCCCCC"  rowspan="2"> 
  			<b>Transaciones de Cartola</b></th>
 			<th width="2%" align="center"></th>
  			<th align="center"  bgcolor="#CCCCCC" colspan="3"><b>Transaciones del IBS</b></th>
  		</tr>
  		
  		<tr height="40" valign="middle" class="tableinfo" >
			<th width="2%" align="center" ></th>
  			<td align="right"  width="17%" bgcolor="#CCCCCC">
  			<b>Fecha Hasta :</b> 
  			</td>
  			<td align="center"  width="33%" bgcolor="#CCCCCC">
  				<div align="left"> 
                <input type="text" name="E01HASDDD" id="E01HASDDD" size="3" maxlength="2" value="<%=Util.formatCell(msg.getE01HASDDD())%>">
                <input type="text" name="E01HASDDM" id="E01HASDDM" size="3" maxlength="2" value="<%=Util.formatCell(msg.getE01HASDDM())%>">
                <input type="text" name="E01HASDDY" id="E01HASDDY" size="5" maxlength="4" value="<%=Util.formatCell(msg.getE01HASDDY())%>">
                <a href="javascript:DatePicker(document.forms[0].E01HASDDD,document.forms[0].E01HASDDM,document.forms[0].E01HASDDY)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
				</div>  			 
  			</td>
  			<td align="center" bgcolor="#CCCCCC">
  				<input id="EIBSBTN" type=button name="Submit" value="Buscar" onClick="javascript:goAction2('1200')">
  			</td>
 
  		</tr>
  		<tr>
  			<td width="50%">
				<table id="headTable1" class="tbhead" cellspacing=0 cellpadding=2 width="100%" border="0">
					<tr id="trdark">
						<th align="center" width="5%">&nbsp;</th>
						<th align="center" width="15%">Fecha</th>
						<th align="center" width="15%">Referencia</th>
						<th align="center" width="35%">Descripci�n</th>
						<th align="center" width="15%">D�bito</th>
						<th align="center" width="15%">Cr�dito</th>
					</tr>
				</table>
  			</td>
  			<td>
  			</td>
  			<td width="17%" colspan="3">
				<table id="headTable2" class="tbhead" cellspacing=0 cellpadding=2 width="100%" border="0">
					<tr id="trdark">
						<th align="center" width="5%">&nbsp;</th>
						<th align="center" width="15%">Fecha</th>
						<th align="center" width="15%">Referencia</th>
						<th align="center" width="35%">Descripci�n</th>
						<th align="center" width="15%">D�bito</th>
						<th align="center" width="15%">Cr�dito</th>
					</tr>
				</table>
  			</td>
  		</tr>
  		<tr>
  			<td width="50%" align="center" valign="top">
  				<table  id="dataTable1" class="tableinfo" ALIGN=CENTER style="width:'100%'">
					<tr>    
						<td NOWRAP width="100%">
							 <div id="dataDiv1" class="scbarcolor" style="width:100%; height:230px; overflow:auto;">  
							 	<table id="car" width="100%">      
<%
								carList.initRow();
								while (carList.getNextRow()) {
									ERC004003Message car = (ERC004003Message) carList.getRecord();
%>
										<tr>
											<td align="center" width="5%">
												<input type="checkbox" id="rowcar" name="rowcar" value="<%=carList.getCurrentRow()%>" onclick="update(this)">
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatDate(car.getE03RCSSDD(), car.getE03RCSSDM(), car.getE03RCSSDY())%></div>
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatCell(car.getE03RCSCKN())%></div>
											</td>
											<td align="center" width="35%">
												<div><%=car.getE03RCSGLO() %></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCCY(car.getE03RCSAMD())%></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCCY(car.getE03RCSAMC())%></div>
											</td>
										</tr> 
<%									
								} 
								if (carList.size() < ibsList.size()) {
									for (int i = carList.size(); i < ibsList.size(); i++) {
%>
										<tr>
											<td align="center" width="5%">
												<input type="checkbox" id="rowcar" name="rowcar" value="<%=i%>" disabled="disabled">
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatCell("")%></div>
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatCell("")%></div>
											</td>
											<td align="center" width="35%">
												<div><%=Util.formatCell("")%></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCell("")%></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCell("")%></div>
											</td>
										</tr>
			
<%									
									}
								}
%>
								</table>
							</div>  				
						</td>
					</tr>	
  				</table>
  			</td>
  			<td></td>
  			<td align="center" valign="top" width="17%" colspan="3">
  				<table  id="dataTable2" class="tableinfo" ALIGN=CENTER style="width:'100%'">
					<tr>    
						<td NOWRAP width="100%">
							 <div id="dataDiv2" class="scbarcolor" style="width:100%; height:230px; overflow:auto;">  
							 	<table id="ibs" width="100%">      
<%
								ibsList.initRow();
								while (ibsList.getNextRow()) {
									ERC004002Message ibs = (ERC004002Message) ibsList.getRecord();
%>
										<tr>
											<td align="center" width="5%">
												<input type="checkbox" id="rowibs" name="rowibs" value="<%=ibsList.getCurrentRow()%>" onclick="update(this)">
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatDate(ibs.getE02RCTBDD(), ibs.getE02RCTBDM(), ibs.getE02RCTBDY())%></div>
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatCell(ibs.getE02RCTCKN())%></div>
											</td>
											<td align="center" width="35%">
												<div><%=ibs.getE02RCTNAR() %></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCCY(ibs.getE02RCTAMD())%></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCCY(ibs.getE02RCTAMC())%></div>
											</td>
										</tr> 
<%									
								} 
								if (ibsList.size() < carList.size()) {
									for (int i = ibsList.size(); i < carList.size(); i++) {
%>
										<tr>
											<td align="center" width="5%">
												<input type="checkbox" id="rowcar" name="rowibs" value="<%=i%>" disabled="disabled">
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatCell("")%></div>
											</td>
											<td align="center" width="15%">
												<div><%=Util.formatCell("")%></div>
											</td>
											<td align="center" width="35%">
												<div><%=Util.formatCell("")%></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCell("")%></div>
											</td>
											<td align="right" width="15%">
												<div><%=Util.formatCell("")%></div>
											</td>
										</tr>
			
<%									
									}
								}
%>
								</table>
							</div>  				
						</td>
					</tr>	
  				</table>
  			</td>
  		</tr>
  		<tr>
			<td height="39">
				<div id="CAR_TOTAL">
				<table class="tableinfo" cellspacing=0 cellpadding=2 width="96%" border="0">
				<tr>
					<td align="center" width="61%"><div>&nbsp;</div></td>
					<td align="right" width="20%"><input id="CAR_TOTAL_DT" align="right" size="16" value="<%=Util.formatCCY("0.00")%>" readonly="readonly"></td>
					<td align="right" width="20%"><input id="CAR_TOTAL_CT" align="right" size="16" value="<%=Util.formatCCY("0.00")%>" readonly="readonly"></td>
				</tr>
				</table>
				</div>
			</td>
			<td height="39"></td>
		<td colspan="3" height="39">
		<div id="IBS_TOTAL">
		<table class="tableinfo" cellspacing=0 cellpadding=2 width="100%"
			border="0">
			<tr>
				<td align="center" width="62%">
				<div>&nbsp;</div>
				</td>
				<td align="right" width="19%"><input id="IBS_TOTAL_DT"
					align="right" size="16" value="<%=Util.formatCCY("0.00")%>"
					readonly="readonly"></td>
				<td align="right" width="19%"><input id="IBS_TOTAL_CT"
					align="right" size="16" value="<%=Util.formatCCY("0.00")%>"
					readonly="readonly"></td>
			</tr>
		</table>
		</div>
		</td>
	</tr>
  		<tr height="12">
  		</tr>
  	</table>
  	
  
  	<div align="center"> 
    	<input id="EIBSBTN" type=button name="Submit" value="Atr�s" onClick="javascript:goAction2('200')">
    	<input id="EIBSBTN" type=button name="Submit" value="Enviar" onClick="javascript:goAction()">	
  	</div>	  
  	<SCRIPT language="JavaScript">
  		/*
		function resizeDoc() {
     		adjustEquTables(headTable1, car, dataDiv1, 1, false);
     		adjustEquTables(headTable2, ibs, dataDiv2, 1, false);
      	}
	 	resizeDoc();   			
     	window.onresize=resizeDoc;
     	*/        
     </SCRIPT>
</form>
</body>
</html>