/**
 * RolDescuento.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public class RolDescuento  implements java.io.Serializable {
    private _125._62._21._10.NivelDescuento[] nivelDescuento;

    public RolDescuento() {
    }

    public _125._62._21._10.NivelDescuento[] getNivelDescuento() {
        return nivelDescuento;
    }

    public void setNivelDescuento(_125._62._21._10.NivelDescuento[] nivelDescuento) {
        this.nivelDescuento = nivelDescuento;
    }

    public _125._62._21._10.NivelDescuento getNivelDescuento(int i) {
        return nivelDescuento[i];
    }

    public void setNivelDescuento(int i, _125._62._21._10.NivelDescuento value) {
        this.nivelDescuento[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        RolDescuento history = (RolDescuento) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        RolDescuento other = (RolDescuento) obj;
        boolean _equals;
        _equals = true
            && ((this.nivelDescuento==null && other.getNivelDescuento()==null) || 
             (this.nivelDescuento!=null &&
              java.util.Arrays.equals(this.nivelDescuento, other.getNivelDescuento())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        RolDescuento history = (RolDescuento) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getNivelDescuento() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNivelDescuento());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNivelDescuento(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
