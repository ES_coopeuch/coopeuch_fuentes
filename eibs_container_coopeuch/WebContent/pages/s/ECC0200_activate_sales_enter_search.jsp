<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>
<HTML><HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css"> 
<TITLE>Search Activate Sales</TITLE>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
 
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "msgSerch" class= "datapro.eibs.beans.ECC020001Message"  scope="session" />
 
 <%
 String cent=request.getParameter("center");
 cent= (cent==null)?"":cent;
  %>
<script language="JavaScript">
	function validate(){
		var ok = false;
		if(isNaN(document.forms[0].E01SPVINB.value)||(document.forms[0].E01SPVINB.value.length < 1)){
			alert("Debe introducir un valor valido de Agencia");
			document.forms[0].E01SPVINB.value='';
			document.forms[0].E01SPVINB.focus();
		}else if (document.forms[0].E01SPVISY.value=='0' || document.forms[0].E01SPVISM.value=='0' || document.forms[0].E01SPVISD.value=='0'){
			alert('Debe colocar fecha una Fecha.');
			document.forms[0].E01SPVISY.focus();
		}else{
			ok=true;			  	
		}
		return ok;
	}
</script>

</HEAD>
<body > 
<h3 align="center">Solicitudes Post Ventas <br> Busqueda<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="activate_sales_enter_search.jsp,ECC0200"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0200" onsubmit="return validate();">

  <input type=HIDDEN name="SCREEN" value="200">  
    <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trclear"> 
   	        <td nowrap width="10%">&nbsp;</td>
            <td nowrap width="10%"> 
              <div align="right"><b>B&uacute;squeda por: </b></div>
            </td>
            <td nowrap width="5%">&nbsp;</td>
            <td nowrap width="10%">&nbsp;</td>
            <td nowrap width="40%">&nbsp;</td>
                      
            </tr>
		    <tr id="trclear">
	         <td nowrap>&nbsp;</td>            
            <td nowrap> 
              <div align="right"><b>Sucursal : </b></div>
            </td>
            <td nowrap width="5%">
                <input type="text" name="E01SPVINB" size="5" maxlength="4" value="<%=msgSerch.getE01SPVINB() %>" <%=("C".equals(cent))?"":"readonly='readonly'" %> >
                <%if ("C".equals(cent)){ %>
              	<a href="javascript:GetBranch('E01SPVINB','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" border="0"  ></a>
                <%} %>
            </td>
             </tr>
             <tr id="trclear">
             <td nowrap>&nbsp;</td> 
             <td nowrap> 
              <div align="right"><b>Fecha de Solicitud : </b></div>
            </td>
            <td nowrap width="40%">
					<eibsinput:date name="msgSerch" fn_year="E01SPVISY" fn_month="E01SPVISM" fn_day="E01SPVISD"/>
				</td>
			</tr> 
		</table>
      </td>
    </tr>
  </table>
  <br>
          <div align="center"> 
            <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
          </div>


</FORM>
</BODY>
</HTML>
 