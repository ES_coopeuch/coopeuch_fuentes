package com.datapro.eibs.security.access.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.exception.BankNotFoundException;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.security.access.jdbc.bean.CNTRLCNT;
import com.datapro.generic.access.DAOSuper;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.BeanParser;

/**
 * @author ogarcia
 *
 */
public class DAOBank extends DAOSuper {

	private static Category cat =
		Category.getInstance(DAOBank.class.getName());

	/**
	 * Constructor for DAOBank.
	 * @param dbid
	 */
	public DAOBank(String dbid) {
		super(dbid);
	}

	/**
	 * Constructor for DAOBank.
	 */
	public DAOBank() {
		super();
	}

	/**
	 * Constructor for DAOBank.
	 * @param connection
	 */
	public DAOBank(Connection connection) {
		super(connection);
	}

	/**
	 * Get banks.
	 */
	public BeanList getBanks()
		throws BankNotFoundException, DAOException {

		BeanList list = null;

		try {
			String sql = "SELECT * FROM CNTRLCNT ORDER BY CNTKEY";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				CNTRLCNT bean = new CNTRLCNT();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new BankNotFoundException(
					"Branch not found exception.");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}

}
