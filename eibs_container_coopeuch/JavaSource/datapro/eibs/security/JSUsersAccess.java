package datapro.eibs.security;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.eibs.constants.Reports;
import com.datapro.eibs.facade.FAReports;
import com.datapro.eibs.facade.FASecurity;
import com.datapro.eibs.reports.vo.IBSRPTViewModules;
import com.datapro.eibs.security.key.CNTRLBTHKEYViewUsers;
import com.datapro.eibs.security.key.USRRPTKEY;
import com.datapro.eibs.security.key.USRRPTKEYViewReportsChecked;
import com.datapro.eibs.security.vo.CNTRLBTHViewUsers;
import com.datapro.eibs.security.vo.USRRPT;
import com.datapro.eibs.security.vo.USRRPTViewReportsChecked;
import com.datapro.generic.beanutil.BeanList;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.Util;

/**
 * @author fhernandez
 */
public class JSUsersAccess extends JSEIBSServlet {

	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		switch (screen) {
			case 1 :
				procReqUsers(user, req, res, session);
				break;
			case 2 :
				procReqUsersTree(user, req, res, session);
				break;
			case 3 :
				procReqUserReports(user, req, res, session);
				break;
			case 4 :
				procActReports(user, req, res, session);
				break;
			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

	private void procReqUsers(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) 
		throws ServletException, IOException {
		forward("Users_reports_tree.jsp", req, res);
	}

	private void addNode(StringBuffer bf, CNTRLBTHViewUsers bUser, boolean isGroup){
		String description = "";
		String id = "";
		if (isGroup) {
			description = bUser.getBTHGRP();
			id = bUser.getBTHGRP(); 
		} else {
			description = bUser.getBTHUSR();
			id = bUser.getBTHUSR();
		}
		bf.append("\"data\" : \"" + description + "\"");
		bf.append(", \"attr\" : {");
		bf.append(		"\"id\" : \"" + id + "\"");
		bf.append(		", \"type\" : \"" + (isGroup ? "G" : "U") + "\" ");
		bf.append(		"}");
		bf.append(", \"state\" : \"" + (isGroup ? "open" : "closed") + "\" ");
	}
	
	private void addUser(StringBuffer bf, CNTRLBTHViewUsers bUser, Iterator iterator, int count){
		//Adding user
		if (count > 0) 
			bf.append(",");
		bf.append("{");
		addNode(bf, bUser, false);
		bf.append("}");
	}	
	
	private CNTRLBTHViewUsers addUsers(StringBuffer bf, CNTRLBTHViewUsers bUser, Iterator iterator){
		String group = bUser.getBTHGRP();
		int count = 0;
		do {
			if (!bUser.getBTHUSR().equals("")) {
				addUser(bf, bUser, iterator, count++);
			}
			if (iterator.hasNext()) {
				bUser = (CNTRLBTHViewUsers) iterator.next();
			} else {
				bUser = null;
				break;
			}
		} while (bUser.getBTHGRP().equals(group));
		return bUser;
	}
	
	private CNTRLBTHViewUsers addGroup(StringBuffer bf, CNTRLBTHViewUsers bUser, Iterator iterator, int count){
		//Adding group node
		if (count > 0) 
			bf.append(",");
		bf.append("{");
		if (bUser.getBTHGRP().equals("")) {
			//Adding empty label group
			CNTRLBTHViewUsers empty = new CNTRLBTHViewUsers();
			empty.setBTHGRP("*");
			addNode(bf, empty, true);
		} else {
			addNode(bf, bUser, true);
		}
		String group = bUser.getBTHGRP();
		while (bUser != null && group.equals(bUser.getBTHGRP())) {
			bf.append(", \"children\" : [");
			bUser = addUsers(bf, bUser, iterator);
			bf.append("]");
		}
		bf.append("}");
		return bUser;
	}
	
	protected void procReqUsersTree(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		StringBuffer bf = new StringBuffer();
		try {
			FASecurity faSecurity = new FASecurity();
			CNTRLBTHKEYViewUsers key = new CNTRLBTHKEYViewUsers();
			key.setBTHUSR(req.getParameter("user"));
			key.setBTHGRP(req.getParameter("group"));
			List list = (List) faSecurity.getUsers(key).getList();
			String group = null;
			CNTRLBTHViewUsers bUser = null;
			int row = 0;
			Iterator iterator = list.iterator();
			do {
				if (group == null && iterator.hasNext()) {
					bUser = (CNTRLBTHViewUsers) iterator.next();
				}
				if (bUser != null) {
					group = bUser.getBTHGRP();
					bUser = addGroup(bf, bUser, iterator, row);
					row++;
				}
			} while (bUser != null && !bUser.getBTHGRP().equals(group));
				
			res.setContentType("application/json");
			res.setCharacterEncoding("ISO-8859-1");
			res.getOutputStream().println("[" + bf.toString() + "]");
			res.getOutputStream().close();
			
		} catch (Exception e) {
			e.printStackTrace();
			res.getWriter().println(
					"Transaccion Fallida: " + "9999" + "-" + e.getMessage());
			res.getOutputStream().close();
		}		
	}
	
	private static final char[] toTake = new char[]{'"','\\','\b', '\f', '\n', '\r', '\t'};	
	private static final String[] toReplace =  new String[]{
		"�", "A", "�", "a", "�", "E", "�", "e", "�", "I", "�", "i", "�", "O", "�", "o", "�", "U", "�", "u", "�", "n"};
	
	private String escapeJson(String string){
		String newString = string;
		for (int i = 0; i < toTake.length; i++) {
			char mChar = toTake[i];
			while (newString.indexOf(mChar) > -1) {
				newString = Util.takeCharacter(newString, mChar);
			}
		}		
		for (int i = 0; i < toReplace.length; i = i + 2) {
			String mChar = toReplace[i];
			while (newString.indexOf(mChar) > -1) {
				newString = Util.replace(newString, mChar, toReplace[i + 1]);
			}
		}
		return newString;
	}
	
	private void procReqUserReports(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) 
		throws ServletException, IOException {
		String module = req.getParameter("type");
		
		StringBuffer bf = new StringBuffer();
		try {
			List list = null;
			if (module == null || module.equals("")) {
				//All Reports Or Header
				module = null;
				list = new FAReports().getReportsModules();
			} else if (module.equals("DM*")) {
				FASecurity faSecurity = new FASecurity();
				faSecurity.setSessionUser(user);
				USRRPTKEYViewReportsChecked key = new USRRPTKEYViewReportsChecked();
				key.setUSRUID(req.getParameter("user"));
				key.setIBSMOD(req.getParameter("id"));
				list = (List) faSecurity.getUserAccessToReports(key).getList();
			} 
			String type = (module == null ? "DM*" : module);
			
			String selected = null;
			int row = 0;
			Iterator iter = list.listIterator();
			while (iter.hasNext()) {
				String id = null;
				String description = null;
				if (module == null) {
					IBSRPTViewModules object = (IBSRPTViewModules) iter.next();
					id = object.getIBSMOD();
					description = (String) Reports.REPORTS_MODULES.get(object.getIBSMOD());
					if (description == null) {
						description = object.getIBSMOD();
					}
					object.setIBSDSC(description);
					selected = "null";
				} else {
					USRRPTViewReportsChecked object = (USRRPTViewReportsChecked) iter.next();
					module = object.getIBSMOD();
					id = object.getIBSRPN();
					description = object.getIBSRPN() + " - " + object.getIBSDSC();
					selected = object.getCHECKED() == null ? "false" : "true";
				}
				if (row > 0) {
					bf.append(",\n");
				}
				bf.append("{");
				bf.append("\"data\" : \"" + escapeJson(description) + "\"");
				bf.append(", \"attr\" : {");
				bf.append(		"\"id\" : \"" + id + "\"");
				bf.append(		", \"type\" : \"" + type + "\" ");
				bf.append(", \"selected\" : \"" + selected + "\"");
				if (selected.equals("true")) {
					bf.append(", \"class\" : \"jstree-checked\"");
				} else if (selected.equals("null")) {
					bf.append(", \"class\" : \"jstree-undetermined\"");
				}
				bf.append(		"}");
				bf.append(", \"state\" : \"closed\" ");
				bf.append("}");
				row++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String jsonString = null;
		if (module == null || module.equals("HM*")) {
			jsonString = "{" +	
					"\"data\":\"Reportes\",\"children\":[" + bf.toString() + "]" +
					", \"state\" : \"open\" " +
					"}";
		} else {
			jsonString = bf.toString();
		}
		//jsonString = JSONUtils.valueToString(jsonString);
		//jsonString = jsonString.substring(1, jsonString.length() - 1);
		res.setContentType("application/json");
		res.setCharacterEncoding("ISO-8859-1");
		res.getOutputStream().println("[" + jsonString + "]");
		res.getOutputStream().close();
	}

	
	private void procActReports(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) 
		throws ServletException, IOException {
		try {
			FASecurity faSecurity = new FASecurity();
			
			String userId = req.getParameter("user");
			String added = req.getParameter("added");
			if (added != null) {
				BeanList addedList = new BeanList(); 
				StringTokenizer token = new StringTokenizer(added, ",");
				while (token.hasMoreElements()) {
					USRRPT bean = new USRRPT();
					bean.setUSRUID(userId);
					bean.setUSRRPN(token.nextElement().toString());
					addedList.addRow(bean);
				}
				faSecurity.addAccessToReports(addedList);
			}
			String removed = req.getParameter("removed");
			if (removed != null) {
				BeanList removedList = new BeanList();
				StringTokenizer token = new StringTokenizer(removed, ",");
				while (token.hasMoreElements()) {
					USRRPTKEY key = new USRRPTKEY();
					key.setUSRUID(userId);
					key.setUSRRPN(token.nextElement().toString());
					removedList.addRow(key);
				}
				faSecurity.removeAccessToReports(removedList);
			}
			res.getWriter().println("Transaccion Exitosa");
			
		} catch (Exception e) {
			e.printStackTrace();
			res.getWriter().println(
					"Transaccion Fallida: " + "9999" + "-" + e.getMessage());
		}
	}

}
