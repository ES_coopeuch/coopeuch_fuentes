<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>
<HTML><HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css"> 
<TITLE>Search Activate Sales</TITLE>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
 
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "msgSerch" class= "datapro.eibs.beans.EDD020001Message"  scope="session" />
 
 <%
 String cent=request.getParameter("center");
 cent= (cent==null)?"":cent;
  %>
<script language="JavaScript">
	function validate(){
		var ok = false;
		if (document.forms[0].E01LOGFCY.value=='0' || document.forms[0].E01LOGFCM.value=='0' || document.forms[0].E01LOGFCD.value=='0'){
			alert('Debe colocar fecha una Fecha.');
			document.forms[0].E01LOGFCY.focus();
		}else{
			ok=true;			  	
		}
		return ok;
	}
</script>

</HEAD>
<body > 
<h3 align="center">Consulta Transacciones ATM Cuenta Vista<br> Busqueda<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="inq_transaction_enter_search.jsp,EDD0200"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD0200" onsubmit="return validate();">

  <input type=HIDDEN name="SCREEN" value="200">  
    <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trclear"> 
   	        <td nowrap width="10%">&nbsp;</td>
            <td nowrap width="20%"> 
              <div align="right"><b>B&uacute;squeda por: </b></div>
            </td>
            <td nowrap width="40%">&nbsp;</td>
            <td nowrap width="30%">&nbsp;</td>                   
            </tr>
             <tr id="trclear">
	             <td nowrap>&nbsp;</td> 
	             <td nowrap> 
	              <div align="right"><b>Fecha   Contable : </b></div>
	             </td>
	             <td nowrap>
						<eibsinput:date name="msgSerch" fn_year="E01LOGFCY" fn_month="E01LOGFCM" fn_day="E01LOGFCD"/>
				 </td>
				 <td nowrap>&nbsp;</td>  				 
			</tr> 
		    <tr id="trclear">
		        <td nowrap>&nbsp;</td>            
	            <td nowrap>
	            	<div align="right"><b>Canal : </b></div>
	            </td>
	            <td nowrap>
	            <select name="E01LOGCAJ">
					<option value="ATMBECH"> Banco Estado </option>
					<option value="ATMTRBK"> Transbank </option>
					<option value="ATMRDBC"> Redbanc </option>										            	
				</select> 
	            </td>
	 			<td nowrap>&nbsp;</td>            
            </tr>	
		    <tr id="trclear">
		        <td nowrap>&nbsp;</td>            
	            <td nowrap>
	            	<div align="right"><b>Tipo de Consulta : </b></div>
	            </td>
	            <td nowrap>
	             <input type="radio" name="E01FLGSUP" id="E01FLGSUP" value="D" checked="checked">Detalle
	             <input type="radio" name="E01FLGSUP" id="E01FLGSUP" value="R" >Resumen
	            </td>
	 			<td nowrap>&nbsp;</td>            
            </tr>	
              
		</table>
      </td>
    </tr>
  </table>
  <br>
          <div align="center"> 
            <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
          </div>


</FORM>
</BODY>
</HTML>
 