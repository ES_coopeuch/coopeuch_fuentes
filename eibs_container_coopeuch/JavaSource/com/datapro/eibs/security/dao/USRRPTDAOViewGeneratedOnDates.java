/*
 * Created on Jul 5, 2006 by fhernandez
 * DAO IBSRPTDAOViewUserItems ITM
 * In security
 */
package com.datapro.eibs.security.dao;

import com.datapro.access.Key;
import com.datapro.eibs.access.DAOViewSuper;
import com.datapro.eibs.security.key.USRRPTKEYViewGeneratedOnDates;
import com.datapro.generic.tool.Util;
;

/**
 * @author fhernandez
 *
 */
public class USRRPTDAOViewGeneratedOnDates extends DAOViewSuper {

	protected String getFindSql() {
		String sql = 
			"SELECT B.IBSDSC, E.EDPDTY, E.EDPDTM, E.EDPDTD, E.EDPPTH, E.EDPRPN " 
			+ "FROM USRRPT A "
			+ "LEFT JOIN IBSRPT B ON A.USRRPN = B.IBSRPN "
			+ "LEFT JOIN EODPDF E ON A.USRRPN = E.EDPRPN " 
			+ "INNER JOIN CNTRLBTH C ON A.USRUID = C.BTHUSR OR C.BTHGRP = A.USRUID "
			+ "WHERE C.BTHUSR = ? AND B.IBSLIF = ? AND B.IBSMOD LIKE ? AND E.EDPFLG LIKE ? " 
			+ "	AND ((10000*(E.EDPDTY))+(100*E.EDPDTM)+(E.EDPDTD) >= ? ) "
			+ "	AND ((10000*(E.EDPDTY))+(100*E.EDPDTM)+(E.EDPDTD) <= ? ) "
			+ "ORDER BY E.EDPRPN";
		return sql;
	}

	protected Object[] getFindArguments(Key keyObj) {
		USRRPTKEYViewGeneratedOnDates key = (USRRPTKEYViewGeneratedOnDates) keyObj;
		Object[] args = {
				key.getUSRUID(),
				key.getIBSLIF(),
				key.getIBSMOD() == null ? "%" : key.getIBSMOD(),
				key.getEDPFLG() == null ? "%" : key.getEDPFLG(),
				"" + Util.dateToInt(
						key.getFROMDTY(),
						key.getFROMDTM(),
						key.getFROMDTD()),
				"" + Util.dateToInt(
						key.getTODTY(),
						key.getTODTM(),
						key.getTODTD())};
		return args;
	}

	protected String getFindByPrimaryKeySql() {
		return null;
	}

	protected Object[] getFindByPrimaryKeyArguments(Key keyObj) {
		return null;
	}
}