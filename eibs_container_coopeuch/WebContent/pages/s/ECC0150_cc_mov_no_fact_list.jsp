<%@ page import="datapro.eibs.beans.ECC015002Message"%>
<%@ page import="datapro.eibs.master.Util"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<title>Movimientos No Facturados Tarjetas de Creditos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ECC015002List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">

</script>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Movimientos No Facturados<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="cc_mov_no_fact_list_nexus.jsp,ECC0150"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150">
<input type="hidden" name="SCREEN" value="201"> 
 <% int row = 0;%>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCUN" size="13" maxlength="12" readonly value="<%= userPO.getCusNum().trim()%>">                  
               </div>               
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre : </b></div>
            </td>
            <td nowrap width="20%"  > 
              <div align="left"> 
                <input type="text" name="E01CCMNME" size="35" maxlength="35" readonly value="<%= userPO.getCusName().trim()%>">                                 
              </div>
            </td>            
            <td nowrap width="16%" > 
              <div align="right"><b>Identif. Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCID" size="15" maxlength="15" readonly value="<%= userPO.getIdentifier().trim()%>">                
               </div>               
            </td>            
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta IBS : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCMACC" size="13" maxlength="12" readonly value="<%= userPO.getAccNum().trim() %>">                                 
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMCCY" size="5" maxlength="4" readonly value="<%= userPO.getCurrency().trim() %>">                              
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Oficial : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMOFC" size="5" maxlength="4" readonly value="<%= userPO.getOfficer().trim() %>">                               
                </b> </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
          <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMPRO" size="5" maxlength="4" readonly value="<%= userPO.getProdCode().trim() %>">                  
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Desc. Producto : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="D01CCMPRO" size="35" maxlength="35" readonly value="<%= userPO.getHeader20().trim()%>">                              
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Nro. Cuenta : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCRNXN" size="20" maxlength="20" readonly value="<%= userPO.getHeader21().trim() %>">                              
              </div>
            </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>

<%
	if (ECC015002List.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=90%>
	<TR>
		<TD>
		<div align="center"><font size="3"><b> No hay
		resultados que correspondan a su criterio de b�squeda. </b></font></div>
		</TD>
	</TR>	
</TABLE>
<%
	} else {
%>
 <TABLE class="tbenter" width="100%">
	<TR>
	<TD ALIGN=CENTER class=TDBKG><a href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
	</TD>
	</TR>
</TABLE>
<TABLE  class="tableinfo" width="100%">
	<TR>
		<TD>
		 <table cellpadding=0 cellspacing=0 width="100%" border=0>		
			<TR id="trclear">
				<td align="center" rowspan=0 ><h3>Movimientos Nacionales sin Facturar</h3></Td>			
			</TR>
		</TABLE>
		</TD>
	</TR>
	<TR>
		<TD>
		 <table border=0 CELLPADDING=0 CELLSPACING=0 width="100%">
				<TR id="trclear">
				<td width="25%" ALIGN=CENTER nowrap><b>No Documento</b></Td>
				<td width="20%" ALIGN=CENTER nowrap><b>Fecha</b></Td>
				<td width="35%" ALIGN=CENTER nowrap><b>Descripcion</b></Td>	
				<td width="20%" ALIGN=CENTER nowrap><b>Monto $</b></Td>			
			</TR>
			<%
				ECC015002List.initRow();
					while (ECC015002List.getNextRow()) 
					{
						ECC015002Message convObj = (ECC015002Message) ECC015002List
								.getRecord();
				if (convObj.getE02CCDTFA().equals("N")) 
				{
			%>
				<tr id="trdark">
				<td nowrap align="left"><%=convObj.getE02CCDMIC()%></td>
				<td nowrap align="center"><%=Util.formatDate(convObj.getE02CCDFTD(),convObj.getE02CCDFTM(),convObj.getE02CCDFTA())%></td>			
				<td nowrap align="left"><%=convObj.getE02CCDGLO()%></td>	 			
				<td nowrap align="right"><%=Util.formatCCY(convObj.getE02CCDMTR())%></td>			
			</tr>
			<%
			    }
				}
			%>
		</TABLE>
    </TD>
</TR>
</TABLE>
<br>
<TABLE  class="tableinfo" width="100%">
	<TR>
		<TD>
		 <table cellpadding=0 cellspacing=0 width="100%" border=0>		
			<TR id="trclear">
				<td align="center" rowspan=0 ><h3>Movimientos Internacionales sin Facturar</h3></Td>			
			</TR>
		</TABLE>
		</TD>
	</TR>
	<TR>
		<TD>
		 <table border=0 CELLPADDING=0 CELLSPACING=0 width="100%">
				<TR id="trclear">
				<td width="20%" ALIGN=CENTER nowrap><b>No Documento</b></Td>
				<td width="10%" ALIGN=CENTER nowrap><b>Fecha</b></Td>
				<td width="30%" ALIGN=CENTER nowrap><b>Descripcion</b></Td>	
				<td width="15%" ALIGN=CENTER nowrap><b>Ciudad</b></Td>		
				<td width="5%" ALIGN=CENTER nowrap><b>Pais</b></Td>
				<td width="10%" ALIGN=CENTER nowrap><b>Monto<br> Moneda Origen</b></Td>				
				<td width="10%" ALIGN=CENTER nowrap><b>Monto USD$</b></Td>			
			</TR>
			<%
				ECC015002List.initRow();
					while (ECC015002List.getNextRow()) 
					{
						ECC015002Message convObj = (ECC015002Message) ECC015002List
								.getRecord();
				    if (convObj.getE02CCDTFA().equals("I")) 
						{
			%>
				<tr id="trdark">
				<td nowrap align="left"><%=convObj.getE02CCDMIC()%></td>
				<td nowrap align="center"><%=Util.formatDate(convObj.getE02CCDFTD(),convObj.getE02CCDFTM(),convObj.getE02CCDFTA())%></td>			
				<td nowrap align="left"><%= convObj.getE02CCDGLO()%></td>				
				<td nowrap align="left"><%= convObj.getE02CCDCIU()%></td>
				<td nowrap align="center"><%= convObj.getE02CCDCPA()%></td>	
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE02CCDMOR())%></td>
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE02CCDMTR())%></td>																					
			</tr>
			<%
			    }
				}
			%>
		</TABLE>
    </TD>
</TR>
</TABLE>
<%
	}
%>
</form>
</body>
</html>
