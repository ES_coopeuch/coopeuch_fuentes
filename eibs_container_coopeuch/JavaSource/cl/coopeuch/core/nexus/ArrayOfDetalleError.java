/**
 * ArrayOfDetalleError.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.coopeuch.core.nexus;

public class ArrayOfDetalleError  implements java.io.Serializable {
    private cl.coopeuch.core.nexus.DetalleError[] detalleError;

    public ArrayOfDetalleError() {
    }

    public cl.coopeuch.core.nexus.DetalleError[] getDetalleError() {
        return detalleError;
    }

    public void setDetalleError(cl.coopeuch.core.nexus.DetalleError[] detalleError) {
        this.detalleError = detalleError;
    }

    public cl.coopeuch.core.nexus.DetalleError getDetalleError(int i) {
        return detalleError[i];
    }

    public void setDetalleError(int i, cl.coopeuch.core.nexus.DetalleError value) {
        this.detalleError[i] = value;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ArrayOfDetalleError history = (ArrayOfDetalleError) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        ArrayOfDetalleError other = (ArrayOfDetalleError) obj;
        boolean _equals;
        _equals = true
            && ((this.detalleError==null && other.getDetalleError()==null) || 
             (this.detalleError!=null &&
              java.util.Arrays.equals(this.detalleError, other.getDetalleError())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ArrayOfDetalleError history = (ArrayOfDetalleError) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getDetalleError() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetalleError());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetalleError(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
