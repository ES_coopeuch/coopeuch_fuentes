/**
 * ProtestosyDocumentosVigentes.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ProtestosyDocumentosVigentes  implements java.io.Serializable {
    private java.math.BigInteger id;  // attribute
    private java.lang.String doc;
    private java.lang.String mon;
    private java.lang.String motivo;
    private java.math.BigInteger idConsulta;
    private java.math.BigInteger numeroBoletinComercial;
    private java.math.BigInteger paginaBoletin;
    private java.math.BigInteger IFechaBoletin;
    private java.math.BigInteger IFechaVencimiento;
    private java.lang.String idDocumento;
    private java.lang.String idMoneda;
    private java.math.BigDecimal monto;
    private java.lang.String idMotivo;
    private java.lang.String localidad;
    private java.lang.String notaria;
    private java.math.BigInteger numeroOperacion;
    private java.lang.String bancoLibrador;

    public ProtestosyDocumentosVigentes() {
    }

    public java.math.BigInteger getId() {
        return id;
    }

    public void setId(java.math.BigInteger id) {
        this.id = id;
    }

    public java.lang.String getDoc() {
        return doc;
    }

    public void setDoc(java.lang.String doc) {
        this.doc = doc;
    }

    public java.lang.String getMon() {
        return mon;
    }

    public void setMon(java.lang.String mon) {
        this.mon = mon;
    }

    public java.lang.String getMotivo() {
        return motivo;
    }

    public void setMotivo(java.lang.String motivo) {
        this.motivo = motivo;
    }

    public java.math.BigInteger getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(java.math.BigInteger idConsulta) {
        this.idConsulta = idConsulta;
    }

    public java.math.BigInteger getNumeroBoletinComercial() {
        return numeroBoletinComercial;
    }

    public void setNumeroBoletinComercial(java.math.BigInteger numeroBoletinComercial) {
        this.numeroBoletinComercial = numeroBoletinComercial;
    }

    public java.math.BigInteger getPaginaBoletin() {
        return paginaBoletin;
    }

    public void setPaginaBoletin(java.math.BigInteger paginaBoletin) {
        this.paginaBoletin = paginaBoletin;
    }

    public java.math.BigInteger getIFechaBoletin() {
        return IFechaBoletin;
    }

    public void setIFechaBoletin(java.math.BigInteger IFechaBoletin) {
        this.IFechaBoletin = IFechaBoletin;
    }

    public java.math.BigInteger getIFechaVencimiento() {
        return IFechaVencimiento;
    }

    public void setIFechaVencimiento(java.math.BigInteger IFechaVencimiento) {
        this.IFechaVencimiento = IFechaVencimiento;
    }

    public java.lang.String getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(java.lang.String idDocumento) {
        this.idDocumento = idDocumento;
    }

    public java.lang.String getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(java.lang.String idMoneda) {
        this.idMoneda = idMoneda;
    }

    public java.math.BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(java.math.BigDecimal monto) {
        this.monto = monto;
    }

    public java.lang.String getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(java.lang.String idMotivo) {
        this.idMotivo = idMotivo;
    }

    public java.lang.String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(java.lang.String localidad) {
        this.localidad = localidad;
    }

    public java.lang.String getNotaria() {
        return notaria;
    }

    public void setNotaria(java.lang.String notaria) {
        this.notaria = notaria;
    }

    public java.math.BigInteger getNumeroOperacion() {
        return numeroOperacion;
    }

    public void setNumeroOperacion(java.math.BigInteger numeroOperacion) {
        this.numeroOperacion = numeroOperacion;
    }

    public java.lang.String getBancoLibrador() {
        return bancoLibrador;
    }

    public void setBancoLibrador(java.lang.String bancoLibrador) {
        this.bancoLibrador = bancoLibrador;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        ProtestosyDocumentosVigentes other = (ProtestosyDocumentosVigentes) obj;
        boolean _equals;
        _equals = true
            && ((this.doc==null && other.getDoc()==null) || 
             (this.doc!=null &&
              this.doc.equals(other.getDoc())))
            && ((this.mon==null && other.getMon()==null) || 
             (this.mon!=null &&
              this.mon.equals(other.getMon())))
            && ((this.motivo==null && other.getMotivo()==null) || 
             (this.motivo!=null &&
              this.motivo.equals(other.getMotivo())))
            && ((this.idDocumento==null && other.getIdDocumento()==null) || 
             (this.idDocumento!=null &&
              this.idDocumento.equals(other.getIdDocumento())))
            && ((this.idMoneda==null && other.getIdMoneda()==null) || 
             (this.idMoneda!=null &&
              this.idMoneda.equals(other.getIdMoneda())))
            && ((this.idMotivo==null && other.getIdMotivo()==null) || 
             (this.idMotivo!=null &&
              this.idMotivo.equals(other.getIdMotivo())))
            && ((this.localidad==null && other.getLocalidad()==null) || 
             (this.localidad!=null &&
              this.localidad.equals(other.getLocalidad())))
            && ((this.notaria==null && other.getNotaria()==null) || 
             (this.notaria!=null &&
              this.notaria.equals(other.getNotaria())))
            && ((this.bancoLibrador==null && other.getBancoLibrador()==null) || 
             (this.bancoLibrador!=null &&
              this.bancoLibrador.equals(other.getBancoLibrador())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ProtestosyDocumentosVigentes history = (ProtestosyDocumentosVigentes) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId())))
            && ((this.idConsulta==null && other.getIdConsulta()==null) || 
             (this.idConsulta!=null &&
              this.idConsulta.equals(other.getIdConsulta())))
            && ((this.numeroBoletinComercial==null && other.getNumeroBoletinComercial()==null) || 
             (this.numeroBoletinComercial!=null &&
              this.numeroBoletinComercial.equals(other.getNumeroBoletinComercial())))
            && ((this.paginaBoletin==null && other.getPaginaBoletin()==null) || 
             (this.paginaBoletin!=null &&
              this.paginaBoletin.equals(other.getPaginaBoletin())))
            && ((this.IFechaBoletin==null && other.getIFechaBoletin()==null) || 
             (this.IFechaBoletin!=null &&
              this.IFechaBoletin.equals(other.getIFechaBoletin())))
            && ((this.IFechaVencimiento==null && other.getIFechaVencimiento()==null) || 
             (this.IFechaVencimiento!=null &&
              this.IFechaVencimiento.equals(other.getIFechaVencimiento())))
            && ((this.monto==null && other.getMonto()==null) || 
             (this.monto!=null &&
              this.monto.equals(other.getMonto())))
            && ((this.numeroOperacion==null && other.getNumeroOperacion()==null) || 
             (this.numeroOperacion!=null &&
              this.numeroOperacion.equals(other.getNumeroOperacion())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ProtestosyDocumentosVigentes history = (ProtestosyDocumentosVigentes) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getDoc() != null) {
            _hashCode += getDoc().hashCode();
        }
        if (getMon() != null) {
            _hashCode += getMon().hashCode();
        }
        if (getMotivo() != null) {
            _hashCode += getMotivo().hashCode();
        }
        if (getIdConsulta() != null) {
            _hashCode += getIdConsulta().hashCode();
        }
        if (getNumeroBoletinComercial() != null) {
            _hashCode += getNumeroBoletinComercial().hashCode();
        }
        if (getPaginaBoletin() != null) {
            _hashCode += getPaginaBoletin().hashCode();
        }
        if (getIFechaBoletin() != null) {
            _hashCode += getIFechaBoletin().hashCode();
        }
        if (getIFechaVencimiento() != null) {
            _hashCode += getIFechaVencimiento().hashCode();
        }
        if (getIdDocumento() != null) {
            _hashCode += getIdDocumento().hashCode();
        }
        if (getIdMoneda() != null) {
            _hashCode += getIdMoneda().hashCode();
        }
        if (getMonto() != null) {
            _hashCode += getMonto().hashCode();
        }
        if (getIdMotivo() != null) {
            _hashCode += getIdMotivo().hashCode();
        }
        if (getLocalidad() != null) {
            _hashCode += getLocalidad().hashCode();
        }
        if (getNotaria() != null) {
            _hashCode += getNotaria().hashCode();
        }
        if (getNumeroOperacion() != null) {
            _hashCode += getNumeroOperacion().hashCode();
        }
        if (getBancoLibrador() != null) {
            _hashCode += getBancoLibrador().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
