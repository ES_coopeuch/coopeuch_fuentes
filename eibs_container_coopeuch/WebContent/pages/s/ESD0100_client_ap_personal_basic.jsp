<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Informacion Basica</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<SCRIPT Language="Javascript">
   builtNewMenu(client_ap_personal_opt);
</SCRIPT>

</head>

<body bgcolor="#FFFFFF">

 <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
 if ( !userPO.getPurpose().equals("NEW") ) {
    out.println("<SCRIPT> initMenu(); </SCRIPT>");
 }
%>

<h3 align="center">Informaci&oacute;n Cliente Personal<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="client_ap_personal_basic, ESD0100"  ></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2">
    <h4> Nombre y Apellidos</h4>
    <div align="left">
      
    <table class="tableinfo">
      <tr > 
          <td nowrap > 
            <div align="center"> 
              
            <table cellspacing="0" cellpadding="2" width="100%" border="0">
              <tr  id="trdark"> 
                <td nowrap width="32%"> 
                  <div align="right">No Cliente :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" readonly <% if (client.getF01CUN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01CUN" size="15" maxlength="10" value="<%= client.getE01CUN().trim()%>">
                </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="32%"> 
                  <div align="right">Primer Nombre 
                    :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" readonly <% if (client.getF01FNA().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01FNA" size="35" maxlength="30" value="<%= client.getE01FNA().trim()%>">
                </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="32%" > 
                  <div align="right">Segundo Nombre 
                    :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" readonly <% if (client.getF01FN2().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01FN2" size="35" maxlength="30" value="<%= client.getE01FN2().trim()%>">
                </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="32%"> 
                  <div align="right">Primer Apellido 
                    :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" readonly <% if (client.getF01LN1().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01LN1" size="35" maxlength="30" value="<%= client.getE01LN1().trim()%>">
                </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="32%"> 
                  <div align="right">Segundo Apellido 
                    :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" readonly <% if (client.getF01LN2().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01LN2" size="35" maxlength="30" value="<%= client.getE01LN2().trim()%>">
                </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="32%"> 
                  <div align="right">Nombre de Casada 
                    :</div>
                </td>
                <td nowrap colspan=4> 
                  <input type="text" readonly <% if (client.getF01ACA().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ACA" size="35" maxlength="30" value="<%= client.getE01ACA().trim()%>">
                </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="32%" > 
                  <div align="right">Nombre Legal 
                    :</div>
                </td>
                <td nowrap colspan=4 > 
                  <input type="text" readonly <% if (client.getF01NA1().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01NA1" size="65" maxlength="60" value="<%= client.getE01NA1().trim()%>">
                </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="32%" > 
                  <div align="right">Nombre Corto 
                    :</div>
                </td>
                <td nowrap colspan=4 > 
                  <input type="text" readonly <% if (client.getF01SHN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01SHN" size="28" maxlength="25" value="<%= client.getE01SHN().trim()%>">
                </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="32%" > 
                  <div align="right">Sexo 
                    :</div>
                </td>
                <td nowrap colspan=4> 
                  <p> 
                    <input type="text" readonly name="E01SEX" size="13" maxlength="13"
				  value="<% if (client.getE01SEX().equals("F")) { out.print("Femenino"); }
							else if (client.getE01SEX().equals("M")) { out.print("Masculino"); }
							else { out.print(""); } %>" >
                  </p>
                </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="32%" > 
                  <div align="right">Estado Civ&iacute;l 
                    :</div>
                </td>
                <td nowrap colspan=4  > 
					<%	if( client.getH01SCR().equals("18")){%> 	
                  <input type="text" readonly name="E01MST" size="45" maxlength="35"
                  value="<% if (client.getE01MST().equals("1")) { out.print("Soltero(a)"); }
							else if (client.getE01MST().equals("2")) { out.print("Casado(a)- Separacion de Bienes"); }
	                    	else if (client.getE01MST().equals("3")) { out.print("Casado(a) - Sociedad Conyugal"); }
							else if (client.getE01MST().equals("4")) { out.print("Casado(a) - Participacion"); }
	                    	else if (client.getE01MST().equals("7")) { out.print("CONVIVIENTE CIVIL - SEPARACION DE BIENES"); }
							else if (client.getE01MST().equals("8")) { out.print("CONVIVIENTE CIVIL - COMUNIDAD DE BIENES"); }
							else if (client.getE01MST().equals("5")) { out.print("Viudo(a)"); }
							else if (client.getE01MST().equals("6")) { out.print("Separado(a)"); }
							else if (client.getE01MST().equals("9")) { out.print("Otro"); }
							else { out.print(""); } %>" >
					<% } else {%> 	
                  <input type="text" readonly name="E01MST" size="15" maxlength="13"
                  value="<% if (client.getE01MST().equals("1")) { out.print("Soltero(a)"); }
							else if (client.getE01MST().equals("2")) { out.print("Casado(a)"); }
	                    	else if (client.getE01MST().equals("3")) { out.print("Divorciado(a)"); }
							else if (client.getE01MST().equals("4")) { out.print("Viudo(a)"); }
							else if (client.getE01MST().equals("5")) { out.print("Union Libre"); }
							else if (client.getE01MST().equals("6")) { out.print("Otro"); }
							else { out.print(""); } %>" >
					<%} %>   
           	    </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="32%" > 
                  <div align="right">Nacionalidad 
                    :</div>
                </td>
                <td nowrap  colspan="3"> 
                  <input type="text" readonly <% if (client.getF01CCS().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01CCS" size="35" maxlength="35" value="<%= client.getD01CCS().trim()%>">
                </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="32%" > 
                  <div align="right"> 
                    Dependientes :</div>
                </td>
                <td nowrap  colspan="3"> 
                  <input type="text" readonly <% if (client.getF01NSO().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01NSO" size="4" maxlength="2" value="<%= client.getE01NSO().trim()%>">
                </td>
              </tr>
            </table>
            
          </div>
          </td>
        </tr>
      </table>
    
    
  </div>
    
    <% System.out.println("Bloque Dirección H01SCR="+client.getH01SCR()); %> 

   		<%	if( client.getH01SCR().equals("07")){%> 	
			<jsp:include page="ESD0080_address_template_basic_banesco_panama.jsp" flush="true">
				<jsp:param name="suffix" value="E01" />
				<jsp:param name="title" value="Dirección" />
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
				<jsp:param name="basic" value="true" />
			</jsp:include>

		<% } else if( client.getH01SCR().equals("03")){%> 	
			<jsp:include page="ESD0080_address_template_venezuela.jsp" flush="true">				
				<jsp:param name="title" value="Dirección" />
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
			</jsp:include>
		
		<%} else if( client.getH01SCR().equals("18")){%> 	
			<jsp:include page="ESD0080_address_template_basic_chile.jsp" flush="true">
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
				<jsp:param name="title" value="Dirección Principal" />
				<jsp:param name="suffix" value="01" />
			</jsp:include>
	
		<% } else {%> 	
			<jsp:include page="ESD0080_address_template_basic_generic.jsp" flush="true">
				<jsp:param name="messageName" value="client" />
				<jsp:param name="readOnly" value="true" />
				<jsp:param name="title" value="Dirección" />
				<jsp:param name="suffix" value="01" />
			</jsp:include>
	
		<%} %>   
		
	<% System.out.println("Bloque Identificación H01SCR="+client.getH01SCR()); %>  
    <%if( client.getH01SCR().equals("07")){%> 	
		<jsp:include page="ESD0080_ident_template_Banesco.jsp" flush="true">
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="true" />
			<jsp:param name="title" value="Identificación" />
			<jsp:param name="suffix" value="01" />
		</jsp:include>
	
	<%} else {%> 
		<jsp:include page="ESD0080_ident_template_generic.jsp" flush="true">
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="true" />
			<jsp:param name="title" value="Identificación" />
			<jsp:param name="suffix" value="01" />

		</jsp:include>
	<%} %>  
   

  <h4>Fechas</h4>
    
  <table class="tableinfo">
    <tr > 
        <td nowrap > 
          
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trdark"> 
              <td nowrap> 
                
              <div align="right">Fecha de Nacimiento :</div>
              </td>
              <td nowrap> 
                <input type="text" readonly <% if (client.getF01BDD().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01BDD" size="3" maxlength="2" value="<%= client.getE01BDD().trim()%>">
                <input type="text" readonly <% if (client.getF01BDM().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01BDM" size="3" maxlength="2" value="<%= client.getE01BDM().trim()%>">
                <input type="text" readonly <% if (client.getF01BDY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01BDY" size="5" maxlength="4" value="<%= client.getE01BDY().trim()%>">
              </td>
              <td nowrap> 
                
              <div align="right">Fecha 1er Contacto 
                :</div>
              </td>
              <td nowrap>                
              <input type="text" readonly <% if (client.getF01IDD().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01IDD" size="3" maxlength="2" value="<%= client.getE01IDD().trim()%>">
              <input type="text" readonly <% if (client.getF01IDM().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01IDM" size="3" maxlength="2" value="<%= client.getE01IDM().trim()%>">                
              <input type="text" readonly <% if (client.getF01IDY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01IDY" size="5" maxlength="4" value="<%= client.getE01IDY().trim()%>">
              </td>
            </tr>
          </table>
          
        </td>
      </tr>
    </table>
    <h4>Tel&eacute;fonos</h4>
    
  <table class="tableinfo" >
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trdark"> 
            <td nowrap width="27%"> 
              <div align="right">Tel&eacute;fono Casa 
                :</div>
            </td>
            <td nowrap width="21%"> 
              <input type="text" readonly <% if (client.getF01HPN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01HPN" size="16" maxlength="15" value="<%= client.getE01HPN().trim()%>">
            </td>
            <td nowrap width="29%"> 
              <div align="right">Tel&eacute;fono Oficina 
                :</div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" readonly <% if (client.getF01PHN().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01PHN" size="16" maxlength="15" value="<%= client.getE01PHN().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="27%" > 
              <div align="right">Tel&eacute;fono Fax 
                :</div>
            </td>
            <td nowrap width="21%" > 
              <input type="text" readonly <% if (client.getF01FAX().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01FAX" size="16" maxlength="15" value="<%= client.getE01FAX().trim()%>">
            </td>
            <td nowrap width="29%" > 
              <div align="right">Tel&eacute;fono Celular 
                :</div>
            </td>
            <td nowrap width="23%" > 
              <input type="text" readonly <% if (client.getF01PH1().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01PH1" size="16" maxlength="15" value="<%= client.getE01PH1().trim()%>">
            </td>
          </tr>
        </table>
        </td>
    </tr>
  </table>
    <h4>C&oacute;digos de Clasificaci&oacute;n</h4>
    
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trdark"> 
            <td nowrap width="35%"> 
              <div align="right">Ejecutivo Principal :</div>
            </td>
            <td nowrap width="65%"> 
              <input type="text" readonly <% if (client.getF01OFC().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01OFC" size="45" maxlength="45" value="<%= client.getD01OFC().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="35%"> 
              <div align="right">Ejecutivo Secundario :</div>
            </td>
            <td nowrap width="65%"> 
              <input type="text" readonly <% if (client.getF01OF2().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01OF2" size="45" maxlength="45" value="<%= client.getD01OF2().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="35%"> 
              <div align="right">Sector Econ&oacute;mico :</div>
            </td>
            <td nowrap width="65%"> 
              <input type="text" readonly <% if (client.getF01INC().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01INC" size="45" maxlength="45" value="<%= client.getD01INC().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="35%"> 
              <div align="right">Actividad Econ&oacute;mica :</div>
            </td>
            <td nowrap width="65%"> 
              <input type="text" readonly <% if (client.getF01BUC().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01BUC" size="45" maxlength="45" value="<%= client.getD01BUC().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="35%"> 
              <div align="right">Pa&iacute;s de Residencia :</div>
            </td>
            <td nowrap width="65%"> 
              <input type="text" readonly <% if (client.getF01GEC().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01GEC" size="45" maxlength="45" value="<%= client.getD01GEC().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="35%"> 
              <div align="right">Tipo de Relaci&oacute;n :</div>
            </td>
            <td nowrap width="65%"> 
              <input type="text" readonly <% if (client.getF01UC1().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01UC1" size="45" maxlength="45" value="<%= client.getD01UC1().trim()%>">
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap width="35%"> 
              <div align="right">Clasificaci&oacute;n :</div>
            </td>
            <td nowrap width="65%"> 
              <input type="text" readonly <% if (client.getF01UC2().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01UC2" size="45" maxlength="45" value="<%= client.getD01UC2().trim()%>">
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width="35%"> 
              <div align="right">Sub Clasificaci&oacute;n :</div>
            </td>
            <td nowrap width="65%"> 
              <input type="text" readonly <% if (client.getF01SCL().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01SCL" size="45" maxlength="45" value="<%= client.getD01SCL().trim()%>">
            </td>
          </tr>

      
          <tr id="trdark"> 
            <td nowrap  width="35%"> 
              <div align="right">Nivel SocioEcon&oacute;mico :</div>
            </td>
            <td nowrap  width="65%"> 
              <input type="text" readonly <% if (client.getF01UC3().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01UC3" size="45" maxlength="45" value="<%= client.getD01UC3().trim()%>">
            </td>
          </tr>



          <tr id="trclear"> 
            <td nowrap  width="35%"> 
              <div align="right">Unidad de Negocio :</div>
            </td>
            <td nowrap  width="65%"> 
              <input type="text" readonly <% if (client.getF01UC4().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01UC4" size="45" maxlength="45" value="<%= client.getD01UC4().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap  width="35%"> 
              <div align="right">Segmento :</div>
            </td>
            <td nowrap  width="65%"> 
              <input type="text" readonly <% if (client.getF01UC5().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01UC5" size="45" maxlength="45" value="<%= client.getD01UC5().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap  width="35%"> 
              <div align="right">SubSegmento :</div>
            </td>
            <td nowrap  width="65%"> 
              <input type="text" readonly <% if (client.getF01UC6().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01UC6" size="45" maxlength="45" value="<%= client.getD01UC6().trim()%>">
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>
  <h4>Datos Adicionales</h4>
    
  <table class="tableinfo" >
    <tr > 
        <td nowrap >
          
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="41%"> 
              <div align="right">Nivel de Educaci&oacute;n 
                :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01EDL().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01EDL" size="45" maxlength="45" value="<%= client.getD01EDL().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="41%"> 
              <div align="right">Profesi&oacute;n/Ocupaci&oacute;n 
                :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01UC9().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01UC9" size="45" maxlength="45" value="<%= client.getD01UC9().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="41%"> 
              <div align="right">Nivel de Ingreso 
                :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01INL().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01INL" size="3" maxlength="1" value="<%= client.getE01INL().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="41%"> 
              <div align="right">Nivel de Riesgo :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01RSL().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01RSL" size="45" maxlength="45" value="<%= client.getD01RSL().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="41%"> 
              <div align="right">Convenio :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01CCO().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01CCO" size="45" maxlength="45" value="<%= client.getD01CCO().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="41%"> 
              <div align="right">Bloqueo Correspondencia :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01C18().equals("Y")) out.print("id=\"txtchanged\""); %> name="D01C18" size="45" maxlength="45" value="<%= client.getD01C18().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="41%"> 
              <div align="right">Total Activos :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01TACT().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01TACT" size="45" maxlength="45" value="<%= client.getE01TACT().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="41%"> 
              <div align="right">Total Pasivos :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01TPAS().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01TPAS" size="45" maxlength="45" value="<%= client.getE01TPAS().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="41%"> 
              <div align="right">Patrimonio :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01TPAT().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01TPAT" size="45" maxlength="45" value="<%= client.getE01TPAT().trim()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="41%"> 
              <div align="right">Resultado del Ejercicio :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01RESU().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01RESU" size="45" maxlength="45" value="<%= client.getE01RESU().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="41%"> 
              <div align="right">Monto de Ingreso Renta :</div>
            </td>
            <td nowrap width="59%"> 
              <input type="text" readonly <% if (client.getF01MING().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01MING" size="45" maxlength="45" value="<%= client.getE01MING().trim()%>">
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    <h4>Datos Operativos</h4>
    
  <table class="tableinfo">
    <tr > 
        <td nowrap >
          
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trdark"> 
              
            <td nowrap width="23%"> 
              <div align="right">Estado del Cliente :</div>
              </td>
              
            <td nowrap width="36%"> 
            <input type="text" readonly name="E01STS" size="13" maxlength="12"
			  <% if (client.getF01STS().equals("Y")) out.print("id=\"txtchanged\""); %>
			  value="<% if (client.getE01STS().equals("1")) { out.print("Inactivo"); }
					  	else if (client.getE01STS().equals("2")) { out.print("Activo"); }
						else if (client.getE01STS().equals("3")) { out.print("Lista Mayor Riesgo"); }
						else if (client.getE01STS().equals("4")) { out.print("Fallecido"); }
						else { out.print(""); } %> " >
            </td>
              
            <td nowrap width="25%"> 
              <div align="right">Clase de Cliente :</div>
              </td>
              
            <td nowrap width="16%"> 
                <input type="text" readonly name="E01CCL" size="13" maxlength="12" <% if (client.getF01CCL().equals("Y")) out.print("id=\"txtchanged\""); %>
				  value="<% if (client.getE01CCL().equals("0")) { out.print("Socio"); }
						  	else if (client.getE01CCL().equals("1")) { out.print("Emp/Gobierno");  }	
							else if (client.getE01CCL().equals("2")) { out.print("Emp/Privada"); }
							else if (client.getE01CCL().equals(" ")) { out.print("No Aplica"); }
							else { out.print(""); } %> " >
            </td>
            </tr>
            <tr id="trclear"> 
              
            <td nowrap width="23%" > 
              <div align="right">Tipo de Cliente :</div>
              </td>
              
            <td nowrap width="36%" > 
              <input type="text" readonly name="E01TYP" size="10" maxlength="10"
			  <% if (client.getF01TYP().equals("Y")) out.print("id=\"txtchanged\""); %>
			  value="<% if (client.getE01TYP().equals("R")) { out.print("Regular"); }
						else if (client.getE01TYP().equals("M")) { out.print("Master"); }	
						else if (client.getE01TYP().equals("G")) { out.print("Grupo"); }
						else { out.print("Grupo"); } %>" >
			</td>
            <td nowrap width="25%" > 
              <div align="right">No. de Grupo :</div>
              </td>
              
            <td nowrap width="16%" > 
              <input type="text" readonly <% if (client.getF01GRP().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01GRP" size="10" maxlength="9" value="<%= client.getE01GRP().trim()%>">
              </td>
            </tr>
            <tr id="trdark"> 
              
            <td nowrap width="23%"> 
              <div align="right">Idioma :</div>
              </td>
              
            <td nowrap width="36%"> 
              <input type="text" readonly name="E01LIF" size="10" maxlength="10"
			  <% if (client.getF01LIF().equals("Y")) out.print("id=\"txtchanged\""); %>
			  value="<% if (client.getE01LIF().equals("S")) { out.print("Espa&ntilde;ol"); }
						else if (client.getE01LIF().equals("E")) { out.print("Ingl&eacute;s"); }
						else { out.print(""); } %>" >
			</td>
              
            <td nowrap width="25%"> 
              <div align="right">Impuestos/Retenciones 
                :</div>
              </td>
              
            <td width="16%"> 
              <input type="text" readonly <% if (client.getF01TAX().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01TAX" size="3" maxlength="1" value="<%= client.getE01TAX().trim()%>">
            </td>
            </tr>
            <tr id="trclear"> 
              
            <td width="23%"> 
              <div align="right">Forma Calificaci&oacute;n 
                :</div>
              </td>
              
            <td width="36%"> 
              <input type="text" readonly <% if (client.getF01FL2().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01FL2" size="3" maxlength="1" value="<%= client.getE01FL2().trim()%>">
            </td>
              
            <td width="25%"> 
              <div align="right">Tabla de Previsi&oacute;n :</div>
              </td>
              
            <td width="16%">
              <input type="text" readonly name="E01PRV" size="4" maxlength="2" value="<%= client.getE01PRV().trim()%>">
            </td>
            </tr>
         
                     <tr id="trdark"> 
              
            <td nowrap width="10%"> 
              <div align="right">Clasificaci&oacute;n Individual :</div>
            </td>
              
            <td nowrap width="15%" bordercolor="#FFFFFF"> 
              <input type="text" name="E01CIN" size="2" maxlength="1" value="<%= client.getE01CIN().trim()%>" readonly>
            </td>
            
            <td nowrap width="8%"> 
            </td>
              
            <td nowrap width="67%"> 
            </td>
            </tr>
         
         
         
          </table>
        </td>
      </tr>
    </table>
    <h4>Miscelaneos</h4>
    
  <table class="tableinfo">
    <tr > 
        <td nowrap >
          
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trdark"> 
              
            <td nowrap  width="18%"> 
              <div align="right">Referido por :</div>
              </td>
              
            <td nowrap  width="36%"> 
              <input type="text" readonly <% if (client.getF01RBY().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01RBY" size="5" maxlength="4" value="<%= client.getE01RBY().trim()%>">
                <input type="text" readonly <% if (client.getF01RBN().equals("Y")) out.print("id=\"txtchanged\""); %>  name="E01RBN" size="45" maxlength="45" value="<%= client.getE01RBN().trim()%>">
            </td>
              
            <td nowrap  width="21%"> 
              <div align="right">Tipo Rel. Familiar 
                :</div>
              </td>
              
            <td nowrap  width="25%"> 
              <input type="text" readonly name="E01FL3" size="15" maxlength="15"
			  <% if (client.getF01FL3().equals("Y")) out.print("id=\"txtchanged\""); %>
              value="<% if (client.getE01FL3().equals("1")) { out.print("Ninguno"); }
						else if (client.getE01FL3().equals("2")) { out.print("Padre"); }
						else if (client.getE01FL3().equals("3")) { out.print("Madre"); }
               			else if (client.getE01FL3().equals("4")) { out.print("Hermano(a)"); }
						else if (client.getE01FL3().equals("5")) { out.print("Abuelo(a)"); }
               			else if (client.getE01FL3().equals("6")) { out.print("Tio(a)"); }
               			else if (client.getE01FL3().equals("7")) { out.print("Hijo(a)"); }
               			else if (client.getE01FL3().equals("8")) { out.print("Conyuge"); }               			
               			else if (client.getE01FL3().equals("9")) { out.print("Primo(a)"); }
               			else if (client.getE01FL3().equals("A")) { out.print("Cunado(a)"); }
               			else if (client.getE01FL3().equals("B")) { out.print("Nieto(a)"); }
               			else if (client.getE01FL3().equals("C")) { out.print("Sobrino(a)"); }
               			else if (client.getE01FL3().equals("D")) { out.print("Suegro(a)"); }
               			else if (client.getE01FL3().equals("E")) { out.print("Nuera"); } 
               			else if (client.getE01FL3().equals("F")) { out.print("Yerno"); }                			               			               			               			               			               			               			               			
						else { out.print(""); } %>" >
            </td>
            </tr>

            <tr id="trclear"> 
            <td nowrap width="18%"> 
              <div align="right">Rel. con el Banco :</div>
              </td>
              
            <td nowrap width="36%"> 
              <input type="text" readonly <% if (client.getF01STF().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01STF" size="3" maxlength="1" value="<%= client.getE01STF().trim()%>">
            </td>
              
            <td nowrap width="21%"> 
              <div align="right">Residente :</div>
              </td>
              
            <td nowrap width="25%"> 
              <input type="text" readonly name="E01FL1" size="3" maxlenth="3"
			  <% if (client.getF01FL1().equals("Y")) out.print("id=\"txtchanged\""); %>
			  value="<% if (client.getE01FL1().equals("1")) { out.print("Si"); }
						else { out.print("No"); } %>" >
            </td>
            </tr>
            
            <tr id="trdark"> 
            <td nowrap width="18%"> 
              <div align="right">Tarjeta ATM :</div>
              </td>
              
            <td nowrap width="36%"> 
              <input type="text" readonly <% if (client.getF01ATM().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ATM" size="2" maxlength="1" value="<%= client.getE01ATM().trim()%>">
            </td>
              
            <td nowrap width="21%"> 
              <div align="right">Nivel de Consulta 
                :</div>
              </td>
              
            <td nowrap width="25%"> 
              <input type="text" readonly <% if (client.getF01ILV().equals("Y")) out.print("id=\"txtchanged\""); %> name="E01ILV" size="4" maxlength="1" value="<%= client.getE01ILV().trim()%>">
              </td>
            </tr>
            
            <tr id="trclear"> 
            <td nowrap width="20%"> 
              <div align="right">Fuente :</div>
              </td>
              
            <td nowrap  width="42%"> 
               <%
              	boolean bTesoreria = (client.getE01FL8().indexOf("T") > -1);
              	boolean bFideicomiso = (client.getE01FL8().indexOf("F") > -1);
              	boolean bFEM = (client.getE01FL8().indexOf("E") > -1);
              	boolean bTerceros = (client.getE01FL8().indexOf("R") > -1);
              %>
              <INPUT type="checkbox" name="E01FL8_TES" value="1" <% if (bTesoreria == true) out.print("checked"); %> readonly>Tesoreria
              <INPUT type="checkbox" name="E01FL8_FID" value="1" <% if (bFideicomiso == true) out.print("checked"); %> readonly>Fideicomiso
              <INPUT type="checkbox" name="E01FL8_FEM" value="1" <% if (bFEM == true) out.print("checked"); %> readonly>FEM
              <INPUT type="checkbox" name="E01FL8_TER" value="1" <% if (bTerceros == true) out.print("checked"); %> readonly>Terceros
             </td>
              
            <td nowrap width="18%"> 
               <div align="right">Cabeza Familia :</div>
             </td>
              
            <td nowrap  width="20%"> 
               <input type="text" readonly name="E01L04" size="3" maxlenth="3"
			  <% if (client.getF01L04().equals("Y")) out.print("id=\"txtchanged\""); %>
			  value="<% if (client.getE01L04().equals("Y")) { out.print("Si"); }
						else { out.print("No"); } %>" >
             </td>
            </tr>            
           <tr id="trdark"> 
            <td nowrap  width="20%"> 
              <div align="right">Poder Especial :</div>
              </td>             
            <td nowrap  width="20%"> 
              <input type="radio" disabled name="E01L01" value="Y" <%if (client.getE01L01().equals("Y")) out.print("checked"); %>>
                S&iacute; 
                <input type="radio" disabled name="E01L01" value="N" <%if (!client.getE01L01().equals("Y")) out.print("checked"); %>>
                No 
                </td>            
            <td nowrap  width="18%"> 
              <div align="right">Fecha Poder Especial :</div>
              </td>     
              <td nowrap> 
	               <eibsinput:date name="client" fn_year="E01A1Y" fn_month="E01A1M" fn_day="E01A1D" readonly="true"/>
              </td>
            </tr>             
            
            <tr id="trclear"> 
            <td nowrap width="18%"> 
                 <div align="right">Sucursal Administrativa :</div>
            </td>
            <td nowrap width="36%"> 
	             <eibsinput:text name="client" property="E01BRA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH%>" readonly="true" />
            </td>
              
            <td nowrap width="21%"> 
             </td>
            <td nowrap width="25%"> 

            </td>
            </tr>
            
          </table>
        </td>
      </tr>
    </table>
    <h4>Personas Relacionadas</h4>
    <table class="tableinfo">
    	<tr>
    	   <tr id="trclear">	
    	   <td nowrap width="42%">
    	       <div align="right">Tiene Personas Relacionadas :</div> 
    	   </td>
    	   <td nowrap width="42%">
    	   <%
    	   if ( client.getL01FILLE4().equalsIgnoreCase("S") || client.getL01FILLE4().equalsIgnoreCase("s") ) {
 		   %>
 		       S&iacute;
    	   <% }else { %>
    	      <% if ( client.getL01FILLE4().equalsIgnoreCase("N") || client.getL01FILLE4().equalsIgnoreCase("n") ) {%>
    		      
              	  No 
    		  <% } 
    	   } %>
    	   </td>
    	</tr> 
  </table>
    
       <%if( client.getH01SCR().equals("11")){%>    
  <h4>Datos Propios Honduras</h4>
  <table class="tableinfo">
    <tr > 
        <td nowrap  >
            
          <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trdark"> 
            <td nowrap   width="20%"> 
              <div align="right">Tipo de Vivienda :</div>
            </td>  
            <td nowrap  width="42%"> 
              <input type="text" readonly name="E01FL4" size="10" maxlength="10"
              value="<% if (client.getE01FL4().equals("1")) { out.print("Propio"); }
						else if (client.getE01FL4().equals("2")) { out.print("Familiar"); }
						else if (client.getE01FL4().equals("3")) { out.print("Alquilada"); }
						else { out.print(""); } %>" >
            </td>
            <td nowrap   width="18%"> 
              <div align="right">Sector Econ&oacute;mico Honduras BCH :</div>
            </td>            
            <td nowrap   width="20%"> 
              <input type="text" readonly name="D01FC4" size="46" maxlength="45" value="<%= client.getD01FC4().trim()%>">
            </td>
          </tr>
         <tr id="trclear">               
            <td nowrap  width="20%"> 
              <div align="right">Segmento de Banca :</div>
            </td>
            <td nowrap width="42%"> 
                <input type="text" readonly name="E01CD5" size="5" maxlength="4" value="<%= client.getE01CD5().trim()%>">
               
            </td>
            <td nowrap   width="18%"> 
              <div align="right">Clasificaci&oacute;n Destino de Fondos :</div>
            </td>            
            <td nowrap   width="20%">
                  <input type="text" readonly name="E01FG2" size="10" maxlength="10"
              value="<% if (client.getE01FG2().equals("M")) { out.print("Comercial"); }
						else if (client.getE01FG2().equals("C")) { out.print("Consumo"); }
						else if (client.getE01FG2().equals("V")) { out.print("Vivienda"); }
						else { out.print(""); } %>" >
            </td> 
          </tr>
          <tr id="trdark">   
            <td nowrap width="20%"> 
              <div align="right">Categor&iacute;a Creditic&iacute;a Subjectiva :</div>
            </td>
            <td nowrap  width="42%">
                <input type="text" readonly name="E01FL3" size="15" maxlength="15"
              value="<% if (client.getE01FL3().equals("1")) { out.print("Bueno"); }
						else if (client.getE01FL3().equals("2")) { out.print("Bajo"); }
						else if (client.getE01FL3().equals("3")) { out.print("Normal"); }
						else if (client.getE01FL3().equals("4")) { out.print("Menci&oacute;n"); }
               			else if (client.getE01FL3().equals("5")) { out.print("Especial"); }
						else if (client.getE01FL3().equals("6")) { out.print("Dudoso"); }
               			else if (client.getE01FL3().equals("7")) { out.print("Perdida"); }
						else { out.print(""); } %>" >
            
          
            </td>
             <td nowrap width="20%"> 
               <div align="right">Relaci&oacute;n Banco CNBS :</div>
             </td>
            <td nowrap  width="42%">
                  <input type="text" readonly name="E01OBC" size="25" maxlength="25"
              value="<% if (client.getE01OBC().equals("1")) { out.print("Propiedad"); }
						else if (client.getE01OBC().equals("2")) { out.print("Gesti&oacute;n"); }
						else if (client.getE01OBC().equals("3")) { out.print("Otros Funcionarios y Empleados"); }
						else if (client.getE01OBC().equals("4")) { out.print("Por Presunci&oacute;n"); }
						else { out.print(""); } %>" >
            
            </td>   
            </tr>
        
        </table>
          
     
	 
            
        </td>
      </tr>
    </table>
  <%}%>	
   
    
    </form>
</body>
</html>

