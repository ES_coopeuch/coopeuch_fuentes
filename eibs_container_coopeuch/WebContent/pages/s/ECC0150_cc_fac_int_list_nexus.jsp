<%@ page import="datapro.eibs.master.Util"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="java.util.Calendar"%>

<%HttpServletResponse httpResponse = (HttpServletResponse) response;
httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
httpResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0
httpResponse.setDateHeader("Expires", 0); // Proxies.%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
<title>Resumen de facturaciones Internacionales</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">


function goAction(op) 
{
	  	document.forms[0].SCREEN.value = op;
		document.forms[0].submit();
}
  
</script>

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<% 
Calendar calendar = new GregorianCalendar();

int year = calendar.get(Calendar.YEAR);
int month = calendar.get(Calendar.MONTH); 
%> 


<h3 align="center">Estado de Cuenta Internacional<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="cc_fac_int_list_nexus.jsp,ECC0150"></h3>
<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150">
<input type="hidden" name="SCREEN" value="800"> 
 <% int row = 0;%>
<h4>Cliente</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCUN" size="13" maxlength="12" readonly value="<%= userPO.getCusNum().trim()%>">                  
               </div>               
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre : </b></div>
            </td>
            <td nowrap width="20%"  > 
              <div align="left"> 
                <input type="text" name="E01CCMNME" size="35" maxlength="35" readonly value="<%= userPO.getCusName().trim()%>">                                 
              </div>
            </td>            
            <td nowrap width="16%" > 
              <div align="right"><b>Identif. Cliente : </b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left"> 
                <input type="text" name="E01CCRCID" size="15" maxlength="15" readonly value="<%= userPO.getIdentifier().trim()%>">                
               </div>               
            </td>            
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta IBS : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCMACC" size="13" maxlength="12" readonly value="<%= userPO.getAccNum().trim() %>">                                 
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMCCY" size="5" maxlength="4" readonly value="<%= userPO.getCurrency().trim() %>">                              
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Oficial : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMOFC" size="5" maxlength="4" readonly value="<%= userPO.getOfficer().trim() %>">                               
                </b> </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>">
          <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
               <input type="text" name="E01CCMPRO" size="5" maxlength="4" readonly value="<%= userPO.getProdCode().trim() %>">                  
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Desc. Producto : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="D01CCMPRO" size="35" maxlength="35" readonly value="<%= userPO.getHeader20().trim()%>">                              
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Nro. Cuenta : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
               <input type="text" name="E01CCRNXN" size="20" maxlength="20" readonly value="<%= userPO.getHeader21().trim() %>">                              
              </div>
            </td>
          </tr>          
        </table>
      </td>
    </tr>
  </table>
  <p>&nbsp;</p>
 <TABLE class="tbenter" width="100%">
	<TR>
		<TD nowrap width="20%">
		</TD>

		<TD nowrap width="10%">
			<DIV align="right">A�o :</DIV>
		</TD>
		<TD nowrap width="10%"> 
       		<DIV align="left">
        	<SELECT name="E01ANOECC">
					<OPTION value="<%=year %>"  selected><%=year %></OPTION>
					<OPTION value="<%=year-1 %>" ><%=year-1 %></OPTION>
			</SELECT> 
			</DIV>
		</TD>

		<TD nowrap width="10%">
			<DIV align="right">Mes :</DIV>
		</TD>
		<TD nowrap width="10%"> 
       		<DIV align="left">
        	<SELECT name="E01MESECC">
					<%for (int x=1; x<13; x=x+1) {   %>
						<OPTION value="<%if(x<10) out.println("0".concat(Integer.toString(x))); else out.println(Integer.toString(x)); %>" <%if(x==month) out.println("selected");  %>><%if(x<10) out.println("0".concat(Integer.toString(x))); else out.println(Integer.toString(x)); %></OPTION>
					<%}%>
			</SELECT> 
			</DIV>

		</TD>
		<TD nowrap width="20%">
			<p align="center">
				<input id="EIBSBTN" type=button name="Submit" value="Consultar" onClick="goAction(800)">
		    </p>  
		</TD>

		<TD nowrap width="20%">
		</TD>

		</TR>
</TABLE>
</form>

</body>
</html>
