<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>

<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Solicitu de Cambio de Estado de Conven�o</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "deal" class= "datapro.eibs.beans.ESD079101Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">
 function enterCode(){
	    

	    
		if ((trim(document.forms[0].E01COTCDE.value).length > 0) && (document.forms[0].E01COTNUM.value!="0" && document.forms[0].E01COTNUM.value!="" )) 
		{
			alert("Debe Ingresar N�mero Solicitud o N�mero de Convenio, NO ambos");
			document.forms[0].E01COTCDE.focus();
			return false;
		}
		else
		{
			if ((trim(document.forms[0].E01COTCDE.value).length > 0) || (document.forms[0].E01COTNUM.value!="0" && document.forms[0].E01COTNUM.value!="" )) 
	    		return true;
			else
			{
				alert("Debe Ingresar N�mero de Solicitud o N�mero de Convenio");
				document.forms[0].E01COTCDE.focus();
				return false;
			}
		}
}



function ChgSel(CnvSel)
{
 if (CnvSel != '')
	{
	document.getElementById('E01COTECU').value = '';
	document.getElementById('E01COTNUM').value= '';
	}
}

function ChgEmpCnv(EmpCnvSel)
{
 if (EmpCnvSel != '' && EmpCnvSel != '0')
	document.getElementById('E01COTCDE').value = '';
}

</script>

</head>

<body>
 
<H3 align="center">Solcicitud de Cambio de Estado de Convenios<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="req_change_deal_status_enter.jsp, ESD0791"></H3>
<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0791" onsubmit="return(enterCode());">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">
  
  <h4>&nbsp;</h4>
  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0"> 
    <tr id="trdark"> 
  		<td nowrap width="50%"> 
           <div align="right">N�mero Cliente Empleador : </div>
        </td>
        <td nowrap width="50%" colspan="3"> 
 			<input type="text" name="E01COTECU" maxlength="9" size="11" value="<%if(request.getAttribute("E01COTECU")==null) out.print("0"); else out.print(request.getAttribute("E01COTECU"));  %>" onchange="ChgEmpCnv(this.value)" onblur="ChgEmpCnv(this.value)"  onclick="ChgEmpCnv('cambio')" onkeypress=" enterInteger()" class="TXTRIGHT">
 			<a id="linkHelp" href="javascript:GetCustomerDescId('E01COTECU','','')" onclick="ChgEmpCnv('cambio')">
 			<img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0"/></a>
        </td>
    </tr>

    <tr id="trclear">
      <td width="50%"> 
        <div align="right">N�mero de Solicitud : </div>
      </td>
      <td width="50%"> 
        <div align="left"> 
 			<input type="text" name="E01COTNUM" maxlength="12" size="14" value="0" onclick="ChgEmpCnv('cambio')" onkeypress=" enterInteger()" onchange="ChgEmpCnv(this.value)" onblur="ChgEmpCnv(this.value)" class="TXTRIGHT">
 			<a id="linkHelp" href="javascript:GetCodeCovenantRequest('E01COTNUM',document.forms[0].E01COTECU.value)" onclick="ChgEmpCnv('cambio')">
 			<img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0"/>
			</a>
        </div>
      </td>
    </tr>
    
    <tr id="trdark">
      <td width="50%"> 
        <div align="right">N�mero de Convenio : </div>
      </td>
      <td width="50%"> 
        <div align="left"> 
          <input type="text" name="E01COTCDE" size="5" maxlength="4" value="<%= deal.getE01COTCDE().trim()%>" onchange="ChgSel(this.value)" onblur="ChgSel(this.value)"  onclick="ChgSel('cambio')">
            <a href="javascript:GetCodeDescDeal('E01COTCDE','E01COTDES')" onclick="ChgSel('cambio')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absbottom" border="0" ></a> 
          <input type="hidden" name="E01COTDES">
        </div>
      </td>
    </tr>
  </table>
  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
  
<script language="JavaScript">
  document.forms[0].E01COTCDE.focus();
  document.forms[0].E01COTCDE.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
