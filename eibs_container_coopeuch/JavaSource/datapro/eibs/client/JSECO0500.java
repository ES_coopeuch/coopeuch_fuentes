package datapro.eibs.client;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ECO050001Message;
import datapro.eibs.beans.ERC200002Message;
import datapro.eibs.beans.ERM031001Message;
import datapro.eibs.beans.ESD008001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;

/**
 * Servlet implementation class JSECO0500
 * 
 * Hecho por Alonso Arana-----------------------11/06/2014--------------DATAPRO--------------------------------
 *EX-Convenios
 *Solicitud-Aprobacion
 *
 */

public class JSECO0500 extends JSEIBSServlet {
	private static final long serialVersionUID = 1L;

	protected static final int A_list_product=101;
	protected static final int A_desvinculacion=102;
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
	
		switch (screen) {
		case A_list_product:
			ProductList(user, req, res, session);
			break;
		case A_desvinculacion:
			Desvinculacion(user, req, res, session);
			break;
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	protected void ProductList(ESS0030DSMessage user, 
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
	throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		try {
			mp = getMessageProcessor("ECO0500", req);

			ECO050001Message msgList = (ECO050001Message) mp.getMessageRecord("ECO050001", user.getH01USR(), "0001");
			//Sets the employee  number from either the first page or the maintenance page
	
			if (req.getParameter("E01CUN")!= null){
				msgList.setE01COHCUN(req.getParameter("E01CUN"));
			} 
			
			if (req.getParameter("E01IDN")!= null){
				msgList.setE01CUSIDN(req.getParameter("E01IDN"));
			} 
			
			
			flexLog("ECO050001: list"+msgList);
			
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	

			if (mp.hasError(error)) { // if there are errors go back to first page
						
					ses.setAttribute("error", error);
					
					flexLog("About to call Page: ECO0500_enter_client.jsp");
					forward("ECO0500_enter_client.jsp", req, res);							

		
			} else {
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
				
				ses.setAttribute("ConvenioList", list);
				
				 if (!list.isEmpty())
			        {
					 ECO050001Message listMessage = (ECO050001Message)list.get(0);
			          
					 
					 ses.setAttribute("cabezera_convenio", listMessage);
					 
			          
			        	}
				forward("ECO0500_product_list.jsp", req, res);
					}
		}
		finally {
			if (mp != null)
			mp.close();
			}

		}
	
	protected void Desvinculacion(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		try {
			mp = getMessageProcessor("ECO0500", req);
			ECO050001Message msgList = (ECO050001Message) mp.getMessageRecord("ECO050001", user.getH01USR(), "0002");
			//Sets the employee  number from either the first page or the maintenance page
			setMessageRecord(req, msgList);
			
			flexLog("ERM050001 Desvinculacion: "+msgList);
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();		
			if (mp.hasError(error)) { // if there are errors go back to first page
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");	
				ses.setAttribute("ConvenioList", list);	
				ses.setAttribute("error", error);
				if (!list.isEmpty())
		        {
				 ECO050001Message listMessage = (ECO050001Message)list.get(0);
				 ses.setAttribute("cabezera_convenio", listMessage);
		        }
					forward("ECO0500_product_list.jsp", req, res);
			} 
			else 
			{
				res.sendRedirect(super.srctx + "/servlet/datapro.eibs.products.JSEXEDD0000?SCREEN=2014");
			}
		} finally {
			if (mp != null)
				mp.close();
		}				
	}
//end
}
