<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import="datapro.eibs.beans.EDD240001Message"%>
<%@ page import="datapro.eibs.beans.EDD240001Message"%>
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Generaci&oacute;n Interfaz FOGAPE</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="date" class="java.util.Date" scope="session" />      
<jsp:useBean id= "IntFog" class= "datapro.eibs.beans.EDL005501Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">

</script>

</head>
<body >
<H3 align="center">Generaci&oacute;n Interfaz FOGAPE<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="inq_fogape_Interface.jsp, EDL0055"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0055">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200"> 

<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">

         <tr id="trclear">
             <td nowrap  align="right" colspan="2"> 
             	Tipo de Consulta : 
             </td>
 
             <td nowrap align="left" colspan="2"> 
             	<select name="E01TIPREP">
                	<option value=" " <% if (!(IntFog.getE01TIPREP().equals("1")|| IntFog.getE01TIPREP().equals("2"))) out.print("selected"); %>> 
                    </option>
                    <option value="1" <% if (IntFog.getE01TIPREP().equals("1")) out.print("selected"); %>>Curses</option>
                    <option value="2" <% if (IntFog.getE01TIPREP().equals("2")) out.print("selected"); %>>Pagos Cuotas</option>                   
                </select>
             </td>
		</tr>

        <tr id="trdark" >
             <td nowrap align="right" width="25%" height="50px"> 
             	Fecha Desde : 
             </td>

             <td nowrap align="left"  width="25%" height="50px">
                 <input type="text" name="E01FECDDD" size="3" maxlength="2" value="<%=IntFog.getE01FECDDD() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <input type="text" name="E01FECDMM"  size="3" maxlength="2" value="<%=IntFog.getE01FECDMM() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <input type="text" name="E01FECDAA"  size="5" maxlength="4" value="<%=IntFog.getE01FECDAA() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <a href="javascript:DatePicker(document.forms[0].E01FECDDD,document.forms[0].E01FECDMM,document.forms[0].E01FECDAA)">
                <img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
 			 </td>                 
             <td nowrap align="right"  width="25%"> 
             	Fecha Hasta : 
             </td>

             <td nowrap align="left"  width="25%">
                 <input type="text" name="E01FECHDD"  size="3" maxlength="2" value="<%=IntFog.getE01FECHDD() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <input type="text" name="E01FECHMM" size="3" maxlength="2" value="<%=IntFog.getE01FECHMM() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <input type="text" name="E01FECHAA"  size="5" maxlength="4" value="<%=IntFog.getE01FECHAA() %>" onkeypress=" enterInteger()" class="TXTRIGHT" >
                <a href="javascript:DatePicker(document.forms[0].E01FECHDD,document.forms[0].E01FECHMM,document.forms[0].E01FECHAA)">
                <img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
 			 </td>                 
         </tr>
    

        </table>
      </td>
    </tr>
  </table>

  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
<script language="JavaScript">
  document.forms[0].E01TIPREP.focus();
  document.forms[0].E01TIPREP.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
