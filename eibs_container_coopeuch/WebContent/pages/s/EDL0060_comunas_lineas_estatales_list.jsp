<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import = "datapro.eibs.beans.EDL006001Message" %>

<html>
<head>
<title>Mantenedor de L&iacute;neas Estatales MYPE</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "listComunas" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="msgLinEst" class="datapro.eibs.beans.EDL006001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
<script language="JavaScript">
function goAction(op) {

	document.forms[0].opt.value = op;

	if(op==3)
	{
		<%if(userPO.getHeader2().equals(""))
		{
			if(userPO.getHeader1().equals("INQ"))
				out.print("document.forms[0].SCREEN.value = 110;");
			else
				out.print("document.forms[0].SCREEN.value = 100;");
		}else{
			out.print("document.forms[0].SCREEN.value = 200;");
		}				
		%>
		document.forms[0].submit();
	}
	else 
		if(op==2)
			{
				if(confirm("Esta seguro que desea borrar esta comuna?"))
						document.forms[0].submit();
			}
		else 
			document.forms[0].submit();
		 
}


</SCRIPT>  

</head>

<BODY>
<h3 align="center"><% if(userPO.getHeader1().equals("INQ")) out.print("Consulta "); else out.print("Mantenedor de "); %> L&iacute;neas Estatales MYPE<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="comunas_lineas_estatales_list, EDL0060"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0060" >
    <input type=HIDDEN name="SCREEN" value="600">
    <input type=HIDDEN name="opt"> 
	<input type=HIDDEN name="CURRCODEL" value="<%=request.getParameter("CURRCODEL").trim()%>"> 
	<input type=HIDDEN name="CURRCODESL" value="<%=request.getParameter("CURRCODESL").trim()%>"> 
    

  <h4>
  		<%if(userPO.getHeader2().equals(""))
		{
			out.print("Datos L&iacute;nea");
		}else{
			out.print("Datos SubL&iacute;nea");
		}				
		%>
  </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">L&iacute;nea :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01MLNSEQ" size="7"  value="<%= msgLinEst.getE01MLNSEQ().trim()%>" readonly >
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">
              <div align="right"><%if(userPO.getHeader2().equals("SUB")){out.print("SubL&iacute;nea :");} %>
              </div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
               <%if(userPO.getHeader2().equals("SUB")){ %>
              	<input type="text" name="E01MLNIND" size="7"  value="<%= msgLinEst.getE01MLNIND().trim()%>" readonly >
 	           <%} %>	
              </div>
            </td>
          </tr>

           <tr id="trclear"> 
            <td nowrap width="20%"> 
              <div align="right">Garantizador :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E01MLNGTZ" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNGTZ().trim()%>" readonly  style="text-align:right;">
                <input type="text" name="E01MLNDGR" size="41"  value="<%= msgLinEst.getE01MLNDGR().trim()%>" readonly  >
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Programa :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E01MLNPGM" size="3" maxlength="2" value="<%= msgLinEst.getE01MLNPGM().trim()%>" readonly >
                <input type="text" name="E01MLNDPG" size="41"  value="<%= msgLinEst.getE01MLNDPG().trim()%>" readonly  >
              </div>
            </td>
          </tr>
          
                 <tr id="trdark"> 
            <td height="23"> 
              <div align="right">Estado :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSTS1" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSTS().trim()%>" readonly  >
                <input type="text" name="E01MLNDST1" size="41"  value="<%= msgLinEst.getE01MLNDST().trim()%>" readonly  >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Tasa :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNTAS1" size="11" maxlength="10" value="<%= msgLinEst.getE01MLNTAS().trim()%>" readonly style="text-align:right;" >
              </div>
            </td>
          </tr> 

		<%if(userPO.getHeader2().equals(""))
		{%>
	      <tr id="trclear"> 
            <td height="23"> 
              <div align="right">% Garant&iacute;a :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNGRT" size="11" maxlength="10" value="<%= msgLinEst.getE01MLNGRT().trim()%>" onkeypress="enterDecimal()" style="text-align:right;"  >
              </div>
            </td>
            <td nowrap  height="23"> 
              <div align="right">Segmento 1 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG1" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG1().trim()%>"  readonly>
                <input type="text" name="E01MLNDG1" size="41"  value="<%= msgLinEst.getE01MLNDG1().trim()%>" readonly  >
             </div>
            </td>
          </tr> 
 		
          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Cupo L&iacute;nea :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCLI" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNCLI().trim())%>"  readonly style="text-align:right;">
             </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 2 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG2" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG2().trim()%>"  readonly>
                <input type="text" name="E01MLNDG2" size="41"  value="<%= msgLinEst.getE01MLNDG2().trim()%>" readonly  >
              </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Cupo L&iacute;nea Pesos :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNCMN" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNCMN().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 3 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG3" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG3().trim()%>"  readonly>
                <input type="text" name="E01MLNDG3" size="41"  value="<%= msgLinEst.getE01MLNDG3().trim()%>" readonly  >
             </div>
            </td>
          </tr>

          <tr id="trdark"> 
            <td nowrap height="23"> 
              <div align="right">Saldo Disponible :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSDO" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNSDO().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 4 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG4" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG4().trim()%>"  readonly>
                <input type="text" name="E01MLNDG4" size="41"  value="<%= msgLinEst.getE01MLNDG4().trim()%>" readonly  >
            </div>
            </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap height="23"> 
              <div align="right">Saldo Retenido :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
              	<input type="text" name="E01MLNSDR" size="20" maxlength="19" value="<%= Util.formatCCY(msgLinEst.getE01MLNSDR().trim())%>"  readonly style="text-align:right;">
              </div>
            </td>
            <td nowrap> 
              <div align="right">Segmento 5 :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E01MLNSG5" size="5" maxlength="4" value="<%= msgLinEst.getE01MLNSG5().trim()%>"  readonly>
                <input type="text" name="E01MLNDG5" size="41"  value="<%= msgLinEst.getE01MLNDG5().trim()%>" readonly  >
              </div>
            </td>
          </tr>
		<%} %>
		
		          
		</table>
	  </td>	
	</tr>
  </table>	 
   

<%
	if (msgLinEst.getE01MLNSUB().equals("1") && userPO.getHeader2().equals("") ) {
%>
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <table class="tbenter" width=100% align=center>
            <tr> 
			  <td class=TDBKG width="100%"> 
        		<div align="center"><a href="javascript:goAction(3)"><b>Volver</b></a></div>
      		  </td>                  
            </tr>
          </table>
          <p>&nbsp;</p>
          <p><b>Para este Garantizador, comunas solo en subl&iacute;neas </b></p>
        </div>
	  </TD>
	</TR>
    </TABLE>


<%
	}
	else
	{
	if ( listComunas.getNoResult() ) {
%>

  
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p>&nbsp;</p>
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
          <table class="tbenter" width=100% align=center>
            <tr> 
			<% if(!userPO.getHeader1().equals("INQ")){ %>
              <td class=TDBKG width="50%"> 
                <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
              </td>
			<%}%>
			<td class=TDBKG width="50%"> 
    	    	<div align="center"><a href="javascript:goAction(3)"><b>Volver</b></a></div>
      		</td>     
            </tr>
          </table>
          <p>&nbsp;</p>
          
        </div>

	  </TD>
	</TR>
    </TABLE>
	
<%}else {
 
		 if ( !error.getERRNUM().equals("0")  ) {
     			error.setERRNUM("0");
    			out.println("<SCRIPT Language=\"Javascript\">");
     			out.println("       showErrors()");
     			out.println("</SCRIPT>");
    		 }
%> 
 
     
	<table class="tbenter" width=100% align=center height="8%">
	<tr> 
		<% if(!userPO.getHeader1().equals("INQ")){ %>
    	<td class=TDBKG width="33%"> 
        	<div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
      	</td>
		<td class=TDBKG width="33%"> 
        	<div align="center"><a href="javascript:goAction(2)"><b>Eliminar</b></a></div>
      	</td>      
		<%}%>
		<td class=TDBKG width="34%"> 
        	<div align="center"><a href="javascript:goAction(3)"><b>Volver</b></a></div>
      	</td>     
    </tr>
  	</table>
	<br>
       <h4>Datos Comuna</h4>   
  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
 			<th align=LEFT nowrap width="5%">Comunas</th>
 			<th align=LEFT nowrap width="30%"></th>
 			<th align=LEFT nowrap width="60%"></th>
          </tr>
 
           <%
                listComunas.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (listComunas.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	
               		datapro.eibs.beans.EDL006001Message msgList = (datapro.eibs.beans.EDL006001Message) listComunas.getRecord();
					 %>
					<tr id="dataTable"> 
           				<td NOWRAP align=CENTER>
           				<% if(!userPO.getHeader1().equals("INQ")){ %>
           					<input type="radio" name="CURRCODE" value="<%= listComunas.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"> 
           				<% }%>
           				</td>

						<td align="right" nowrap>
								<%=msgList.getE01MLNCCM() %>
						</td>

						<td align="left" nowrap>
							<%=msgList.getE01MLNDCM() %>
						</td>						
					
         			</tr>
          	<% 
          	
          }	} %>
              </table>
              </td>
              </tr>
  </table>
  
   

<SCRIPT language="JavaScript">
	showChecked("CURRCODE");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

</body>
</html>
