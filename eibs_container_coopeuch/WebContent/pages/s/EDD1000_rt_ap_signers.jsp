<html>
<head>
<title>Firmantes</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />
<jsp:useBean id= "signersList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "rtFirm" class= "datapro.eibs.beans.EDD550001Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">
<% 
 
    int row = 0;
  
    try {		  	
	  	row = Integer.parseInt(request.getParameter("ROW")); 
	  	signersList.setCurrentRow(row);
        rtFirm = (datapro.eibs.beans.EDD550001Message ) signersList.getRecord(); 
       
	 } 
	 catch (Exception e) {
			row = 0;	    
	 }
	
 
 
%>

</SCRIPT>

</head>

<body>
<h3 align="center">Firmantes<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="EDD1000_rt_ap_signers.jsp, EDD1000"></h3>
<hr size="4">
<FORM METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD0000F" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="20">  
  
  <INPUT TYPE=HIDDEN NAME="ROW" VALUE="<%= row %>">
  <INPUT TYPE=HIDDEN NAME="E01MAN" VALUE="<%= rtFirm.getE01MAN() %>">
  <INPUT TYPE=HIDDEN NAME="E01CUN" VALUE="<%= rtFirm.getE01CUN() %>">
  <INPUT TYPE=HIDDEN NAME="E01RTY" VALUE="<%= rtFirm.getE01RTY() %>">
  
  <table id="mainTable" class="tableinfo">
    	<tr id="trdark"> 
            <td width="13%" nowrap> 
              <div align="right">N�mero de Cliente : </div>
            </td>
            <td nowrap> 
               <input type="text" name="E01RCN" maxlength="12" size="12" value="<%= rtFirm.getE01RCN()%>" readonly>
            </td>
          </tr>

	     <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right">Nombre Legal : </div>
            </td>
            <td nowrap> 
              <input type="text" name="E01MA1" maxlength="45" size="46" value="<%= rtFirm.getE01MA1()%>" readonly>
            </td>
          </tr>
</table>

<div id="SecPanama" style="display:none;">
<table id="mainTable1" class="tableinfo">
          <tr id="trdark"> 
            <td width="13%" nowrap> 
              <div align="right">Calle : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01MA2" maxlength="45" size="46" value="<%= rtFirm.getE01MA2()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right">Residencia/Edificio : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01MA3" maxlength="45" size="45" value="<%= rtFirm.getE01MA3()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td width="13%" nowrap> 
              <div align="right">No. Casa/Apto. : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01MA4" maxlength="45" size="45" value="<%= rtFirm.getE01MA4()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right">Provincia : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="D01STE" maxlength="40" size="40" value="<%= rtFirm.getD01STE()%>" readonly>
				<%--
              <a href="javascript:GetCodeCNOFC('E01STE','27')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0"
              </a>	
				--%>
            </td>
          </tr>
          <tr id="trdark"> 
            <td width="13%" nowrap> 
              <div align="right">Distrito : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="D01MLC" maxlength="40" size="40" value="<%= rtFirm.getD01MLC()%>" readonly>
				<%--
              <a href="javascript:Get2FilterCodes('E01UC7','D01MLC','PI', document.getElementById('E01STE').value,' ')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0"	
			 </a>
 				--%>
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right">Corregimiento : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="D01INC" maxlength="40" size="40" value="<%= rtFirm.getD01INC()%>" reaconly>
				<%--
              <a href="javascript:Get2FilterCodes('','D01INC','PE',document.getElementById('E01STE').value,document.getElementById('E01UC7').value)"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0"></a>	
 				--%>
            </td>
          </tr>
          <tr id="trdark"> 
            <td width="13%" nowrap> 
              <div align="right">Pa&iacute;s : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01CTR" maxlength="20" size="21" value="<%= rtFirm.getE01CTR()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right">Apartado Postal : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01POB" maxlength="10" size="11" value="<%= rtFirm.getE01POB()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td width="13%" nowrap> 
              <div align="right">C&oacute;digo Postal : </div>
            </td>
            <td width="23%" nowrap>
              <input type="text" name="E01ZPC" maxlength="15" size="16" value="<%= rtFirm.getE01ZPC()%>" reaconly> 
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right">Tel&eacute;fono : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01HPN" maxlength="18" size="15" value="<%= rtFirm.getE01HPN()%>" readonly>
            </td>
          </tr>
</table>  
</div>  

<div id="SecNoPanama" style="display:none;">
<table id="mainTable2" class="tableinfo">

          <tr id="trdark"> 
            <td width="13%" nowrap> 
              <div align="right">Direcci&oacute;n : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01MA2" maxlength="45" size="45" value="<%= rtFirm.getE01MA2()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right"></div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01MA3" maxlength="45" size="45" value="<%= rtFirm.getE01MA3()%>" reaonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right"></div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01MA4" maxlength="45" size="45" value="<%= rtFirm.getE01MA4()%>" reaconly>
            </td>
          </tr>
          <%if(currUser.getE01INT().equals("18")){%> 
	          <tr id="trdark"> 
	            <td width="13%" nowrap> 
	              <div align="right">Ciudad : </div>
	            </td>
	            <td width="23%" nowrap> 
	              <input type="text" name="E01TID" maxlength="4" size="5" value="<%= rtFirm.getE01TID()%>" readonly>
	            </td>
	          </tr>
	          <tr id="trclear"> 
	            <td width="13%" nowrap> 
	              <div align="right">Comuna : </div>
	            </td>
	            <td width="23%" nowrap> 
	              <input type="text" name="E01PID" maxlength="4" size="5" value="<%= rtFirm.getE01PID()%>" readonly>
	            </td>
	          </tr>
          <%} else if(currUser.getE01INT().equals("06")){%>         
		         <tr id="trdark"> 
		            <td width="13%" nowrap> 
		              <div align="right">Departamento :</div>
		            </td>
		            <td width="23%" nowrap> 
		              <input type="text" name="E01TID" maxlength="4" size="5" value="<%= rtFirm.getE01TID()%>"  readonly>
		            </td>
		          </tr>
		         <tr id="trclear">  
		            <td width="13%" nowrap> 
		              <div align="right">Municipio :</div>
		            </td>
		           <td width="23%" nowrap> 
		              <input type="text" name="E01BNI" maxlength="6" size="7" value="<%= rtFirm.getE01BNI()%>" readonly>
		            </td>
		          </tr>
          <%} else {%>
			  <tr id="trdark"> 
	            <td width="13%" nowrap> 
	              <div align="right">Ciudad : </div>
	            </td>
	            <td width="23%" nowrap> 
	              <input type="text" name="E01CTY" maxlength="30" size="31" value="<%= rtFirm.getE01CTY()%>" readonly>
	            </td>
	          </tr>
	          <tr id="trclear"> 
	            <td width="13%" nowrap> 
	              <div align="right">Estado : </div>
	            </td>
	            <td width="23%" nowrap> 
	              <input type="text" name="E01STE" maxlength="4" size="5" value="<%= rtFirm.getE01STE()%>" readonly>
	          </tr>
		<%}%>
          <tr id="trdark"> 
            <td width="13%" nowrap> 
              <div align="right">Pa&iacute;s : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01CTR" maxlength="20" size="21" value="<%= rtFirm.getE01CTR()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right">Apartado Postal : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01POB" maxlength="10" size="11" value="<%= rtFirm.getE01POB()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td width="13%" nowrap> 
              <div align="right">C&oacute;digo Postal : </div>
            </td>
            <td width="23%" nowrap>
              <input type="text" name="E01ZPC" maxlength="15" size="16" value="<%= rtFirm.getE01ZPC()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right">Tel&eacute;fono : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01HPN" maxlength="18" size="15" value="<%= rtFirm.getE01HPN()%>" readonly>
            </td>
          </tr>
</table>  
</div>
<table id="mainTable3" class="tableinfo">

          <%
          String idrow1 = "trdark";
          String idrow2 = "trclear";
           
          if(!(currUser.getE01INT().equals("06"))){%> 
             
          <tr id="trclear"> 
            <td width="13%" nowrap> 
              <div align="right">Identificaci&oacute;n : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01BNI" maxlength="28" size="25" value="<%= rtFirm.getE01BNI()%>">
            </td>
          </tr>
          <%} else {
              idrow2 = "trdark";
              idrow1 = "trclear";
          }
          %>
<%--          
          <tr id="<%= idrow2 %>"> 
            <td width="13%" nowrap> 
              <div align="right">Tipo de Firma: </div>
            </td>
            <td width="23%" nowrap> 
              <select name="E01FL1">
                <option value=" " <% if (!(rtFirm.getE01FL1().equals("1") || rtFirm.getE01FL1().equals("2") || rtFirm.getE01FL1().equals("3"))) out.print("selected"); %>></option>
                <option value="1" <% if (rtFirm.getE01FL1().equals("1")) out.print("selected"); %>>Firma Individual</option>
                <option value="2" <% if (rtFirm.getE01FL1().equals("2")) out.print("selected"); %>>Firma Mancomunada</option>
				<option value="3" <% if (rtFirm.getE01FL1().equals("3")) out.print("selected"); %>>Firma Indistinta</option>
			  </select>
            </td>
          </tr>         
--%>          
          <tr id="<%= idrow1 %>"> 
            <td width="13%" nowrap> 
              <div align="right">Categor�a de Firma : </div>
            </td>
            <td width="23%" nowrap> 
              <input type="text" name="E01FL3" maxlength="1" size="2" value="<%= rtFirm.getE01FL3()%>">
              <a href="javascript:GetCodeCNOFC('E01FL3','FI')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0"></a>	
            </td>
          </tr>
</table>  
       
  <br>
          <div align="center"> 
            <input id="EIBSBTN" type="button" name="Submit" value="Cerrar" onclick="window.close();">
          </div>
</form>
<SCRIPT Language="Javascript">

procDirec("<%=rtFirm.getE01CTR() %>");		 	 

function procDirec(country){
if (country =="PANAMA" || country =="PA" ) {
	document.getElementById("SecPanama").style.display = "";
	document.getElementById("SecNoPanama").style.display = "none";
   }else{
	if (country =="") {
	document.getElementById("SecPanama").style.display = "none";
	document.getElementById("SecNoPanama").style.display = "none";
   }else{
	document.getElementById("SecPanama").style.display = "none";
	document.getElementById("SecNoPanama").style.display = "";
   }
	}
}

</SCRIPT>
</body>
</html>
