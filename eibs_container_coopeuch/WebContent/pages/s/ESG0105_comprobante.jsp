<%//P�gina hecha por Alonso Arana ---Datapro ---- 24/10/2013 %>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ESG010501Message"%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>FORMULARIO DE T�RMINO ANTICIPADO DE SEGUROS</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">
<jsp:useBean id="RetractosList" class="datapro.eibs.beans.ESG010503Message" scope="session" />
<jsp:useBean id="receipt" class="datapro.eibs.beans.EDL014030Message"  scope="session" />
<jsp:useBean id="receiptH" class="datapro.eibs.beans.JBListRec"  scope="session" />
<jsp:useBean id="receiptB" class="datapro.eibs.beans.JBListRec"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>	
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>
<script type="text/javascript" >


function goAction(op){
	var ok = false;	
    var fol = "";
 
      			fol = <%=RetractosList.getE03SREFOL()%>
      		
				 var pg = webapp+"/servlet/datapro.eibs.client.JSESG0010I?SCREEN=402&key="+fol;
 				CenterWindow(pg,700,600,2); 
				
 }
   

</script>
 
 
 </head>
<body>
 <h3 align="center">Comprobante de Retracto de Seguros<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PAC_client_search.jsp,ESG0010"></h3>
<hr size="4">

<table class="tbenter" width="100%" height="10%">
		<tr>
			<td colspan="4"  align="center" id="eibsNew" class="tdbkg" width="25%">
			<a href="javascript:goAction('402')"> <b>Imprimir Formulario</b> </a>
		    </td>
		</tr>
	</table>

 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
  	    <td><h4>Datos del cliente:</h4></td>
    </tr>
    <tr> 
  	 <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" > Cliente : 
              </td>
             <td nowrap width="10%" >
	  		<%=userPO.getCusNum()%>
             </td>
             <td nowrap width="10%" > Nombre : 
               </td>
             <td nowrap width="29%">
	  			<%=userPO.getCusName() %>
             </td>
             <td nowrap width="15%" >Identificaci�n :  
             </td>
             <td nowrap width="10%" >
	  		<%=userPO.getID() %>
             </td>
         </tr>
     
        </table>
      </td>
    </tr>
    

  </table>

<table>
<tr>
<td height="20">
</td>
</tr>
</table>
 
 
 
 
 
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
  	    <td><h4>Datos adicionales del cliente:</h4></td>
    </tr>
    <tr> 
  	 <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" > Tel�fono : 
              </td>
             <td nowrap width="10%" >
	  		<%=RetractosList.getE03SREHPN()%>
             </td>
             <td nowrap width="10%" >Celular : 
               </td>
             <td nowrap width="29%">
			<%=RetractosList.getE03SREPH1()%>
             </td>
             <td nowrap width="15%" >Correo Electr�nico :  
             </td>
             <td nowrap width="10%" >
	  			  	 <%=RetractosList.getE03SREIAD()%>
             </td>
         </tr>
     
        </table>
      </td>
   
   
    </tr>
 

  </table>
 
 <table>
<tr>
<td height="20">
</td>
</tr>
</table>
 
 
 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
    <td><h4>Datos de la poliza:</h4></td>
    </tr>
    <tr> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> N�  de Poliza : 
              </td>
             <td nowrap width="20%" align="left">
	 			<%=RetractosList.getE03SREPLZ() %>
	 		             </td>
        
          
             <td nowrap width="10%" align="right"> 	 
           
           N� de Operaci�n:
             </td>
     
             <td nowrap width="20%" align="left">
		<%=RetractosList.getE03SREPAC() %>
             </td>
            
             
           
             
         </tr>
        </table>
      </td>
    </tr>
  </table>
  
  
  
  
  
  
  
  
  
  <br></br>
  
  <table id="headTable" width="100%">
		<tr id="trdark">
			
			<th align="center" nowrap width="10%">Seguros</th>
			<th align="center" nowrap width="30%">Seguros Renunciados</th>
			<th align="center" nowrap width="20%">Monto Devoluci�n</th>
		
		</tr>
	
			
		<% if(!RetractosList.getE03SRECO1().equals("")){ %>
		<tr>
			<td nowrap align="left">
				<%=Util.formatCell(Util.trim(RetractosList.getE03SREDS1())) %>
			</td>
			<td nowrap align="center">
			X
			</td>
			<td nowrap align="center">
			  <%=Util.formatCell(RetractosList.getE03SRESO1()) %>
			</td>
			
		</tr>
	<%} %>
	
	<% if(!RetractosList.getE03SRECO2().equals("")){ %>
	
	
	<tr>
			<td nowrap align="left">
				<%=Util.formatCell(Util.trim(RetractosList.getE03SREDS2())) %>
			</td>
			<td nowrap align="center">
			X
			</td>
			<td nowrap align="center">
			  <%=Util.formatCell(RetractosList.getE03SRESO2()) %>
			</td>
			
		</tr>
	
	
	<%} %>
	
		<% if(!RetractosList.getE03SRECO3().equals("")){ %>
	
	
	<tr>
			<td nowrap align="left">
				<%=Util.formatCell(Util.trim(RetractosList.getE03SREDS3())) %>
			</td>
			<td nowrap align="center">
			X
			</td>
			<td nowrap align="center">
			  <%=Util.formatCell(RetractosList.getE03SRESO3()) %>
			</td>
			
		</tr>
	
	
	<%} %>
	
	
		<% if(!RetractosList.getE03SRECO4().equals("")){ %>
	
	
	<tr>
			<td nowrap align="left">
				<%=Util.formatCell(Util.trim(RetractosList.getE03SREDS4())) %>
			</td>
			<td nowrap align="center">
			X
			</td>
			<td nowrap align="center">
			  <%=Util.formatCell(RetractosList.getE03SRESO4()) %>
			</td>
			
		</tr>
	
	
	<%} %>
	
		<% if(!RetractosList.getE03SRECO5().equals("")){ %>
	
	
	<tr>
			<td nowrap align="left">
				<%=Util.formatCell(Util.trim(RetractosList.getE03SREDS5())) %>
			</td>
			<td nowrap align="center">
			X
			</td>
			<td nowrap align="center">
			  <%=Util.formatCell(RetractosList.getE03SRESO5()) %>
			</td>
			
		</tr>
	
	
	<%} %>
	
	
	</table>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		
		
		</td>
		<td width="50%" align="right">
		
		</td>
	</tr>
</table>
  
  
  
  </body>
  
  </html>