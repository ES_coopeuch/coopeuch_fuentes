// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 12-02-2015 17:11:06
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ExcelUtils.java

package datapro.eibs.services;

import datapro.eibs.beans.ExcelColStyle;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.DecimalField;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils
{

    public ExcelUtils()
    {
    }

    private static String getClassName(Class c)
    {
        if(c.isArray())
            return (new StringBuilder(String.valueOf(c.getComponentType().getName()))).append("[]").toString();
        else
            return c.getName();
    }

    public static void populate(ResultSet rs, Object bean)
        throws IllegalArgumentException, SQLException
    {
        try
        {
            ResultSetMetaData md = rs.getMetaData();
            PropertyDescriptor pds[] = Introspector.getBeanInfo(bean.getClass()).getPropertyDescriptors();
            String type = "";
            int index = -1;
            String name = "";
            for(int i = 0; i < pds.length; i++)
            {
                name = pds[i].getName();
                if(!name.equals("class"))
                {
                    index = rs.findColumn(name);
                    if(index >= 0)
                    {
                        type = getClassName(pds[i].getPropertyType());
                        Method setter = pds[i].getWriteMethod();
                        if(setter != null)
                            if(type.equals("java.math.BigInteger"))
                            {
                                if(rs.getBigDecimal(name) != null)
                                    setter.invoke(bean, new Object[] {
                                        new BigInteger(rs.getBigDecimal(name).toString())
                                    });
                            } else
                            if(type.equals("java.math.BigDecimal"))
                            {
                                if(rs.getBigDecimal(name) != null)
                                    setter.invoke(bean, new Object[] {
                                        rs.getBigDecimal(name)
                                    });
                            } else
                            if(type.equals("boolean"))
                            {
                                if(md.getColumnClassName(index).equals("boolean"))
                                    setter.invoke(bean, new Object[] {
                                        new Boolean(rs.getBoolean(name))
                                    });
                            } else
                            if(type.equals("short"))
                            {
                                if(rs.getBigDecimal(name) != null)
                                    setter.invoke(bean, new Object[] {
                                        new Short(rs.getBigDecimal(name).toString())
                                    });
                            } else
                            if(type.equals("int") || type.equals("java.lang.Integer"))
                            {
                                if(rs.getBigDecimal(name) != null)
                                    setter.invoke(bean, new Object[] {
                                        new Integer(rs.getBigDecimal(name).toString())
                                    });
                            } else
                            if(type.equals("long") || type.equals("java.lang.Long"))
                            {
                                if(rs.getBigDecimal(name) != null)
                                    setter.invoke(bean, new Object[] {
                                        new Long(rs.getBigDecimal(name).toString())
                                    });
                            } else
                            if(type.equals("float"))
                            {
                                if(rs.getBigDecimal(name) != null)
                                    setter.invoke(bean, new Object[] {
                                        new Float(rs.getBigDecimal(name).floatValue())
                                    });
                            } else
                            if(type.equals("double"))
                            {
                                if(rs.getBigDecimal(name) != null)
                                    setter.invoke(bean, new Object[] {
                                        new Double(rs.getBigDecimal(name).doubleValue())
                                    });
                            } else
                            if(type.equals("java.sql.Date"))
                            {
                                if(rs.getDate(name) != null)
                                    setter.invoke(bean, new Object[] {
                                        rs.getDate(name)
                                    });
                            } else
                            if(type.equals("java.sql.Time"))
                            {
                                if(rs.getDate(name) != null)
                                    setter.invoke(bean, new Object[] {
                                        new Time(rs.getDate(name).getTime())
                                    });
                            } else
                            if(type.equals("java.sql.Timestamp"))
                            {
                                if(rs.getDate(name) != null)
                                    setter.invoke(bean, new Object[] {
                                        new Timestamp(rs.getDate(name).getTime())
                                    });
                            } else
                            if(type.equals("java.lang.String") && rs.getString(name) != null)
                                setter.invoke(bean, new Object[] {
                                    rs.getString(name)
                                });
                    }
                }
            }

        }
        catch(Exception e)
        {
            System.out.println((new StringBuilder(String.valueOf(e.getClass().getName()))).append(": ").append(e.getMessage()).toString());
        }
    }

    public static void populate(ResultSet rs, MessageRecord message)
        throws Exception
    {
        String name = "";
        try
        {
            ResultSetMetaData md = rs.getMetaData();
            Enumeration enu = message.fieldEnumeration();
            MessageField field = null;
            int type = 0;
            int index = -1;
            while(enu.hasMoreElements()) 
            {
                field = (MessageField)enu.nextElement();
                name = field.getTag();
                index = rs.findColumn(name);
                if(index >= 0)
                {
                    type = field.getType();
                    switch(type)
                    {
                    default:
                        break;

                    case 1: // '\001'
                        if(md.getColumnClassName(index).equals("java.math.BigDecimal"))
                        {
                            if(rs.getBigDecimal(name) != null)
                                field.setString(Util.addLeftChar('0', field.getLength(), rs.getBigDecimal(name).toString()));
                            else
                                field.setString(Util.addLeftChar('0', field.getLength(), rs.getString(name)));
                            break;
                        }
                        if(md.getColumnClassName(index).equals("java.sql.Date"))
                        {
                            if(rs.getDate(name) != null)
                                field.setString(rs.getDate(name).toString());
                            break;
                        }
                        if(md.getColumnClassName(index).equals("boolean"))
                        {
                            field.setString(rs.getBoolean(name) ? "true" : "false");
                            break;
                        }
                        if(rs.getString(name) != null)
                            field.setString(rs.getString(name));
                        break;

                    case 2: // '\002'
                        if(rs.getBigDecimal(name) != null)
                            ((DecimalField)field).setBigDecimal(rs.getBigDecimal(name));
                        break;
                    }
                }
            }
        }
        catch(Exception e)
        {
            String error = (new StringBuilder("Error in column ")).append(name).append(": ").toString();
            throw new Exception((new StringBuilder(String.valueOf(error))).append(e.getMessage()).toString());
        }
    }

    private static String getFileExtension(String filename)
    {
        String extension = "";
        int i = filename.lastIndexOf('.');
        int p = Math.max(filename.lastIndexOf('/'), filename.lastIndexOf('\\'));
        if(i > p)
            extension = filename.substring(i + 1);
        return extension;
    }

    public static boolean isXLSXVersion(String filename)
    {
        return "xlsx".equalsIgnoreCase(getFileExtension(filename));
    }

    public static OutputStream ConvertExcelToCSV(String xlsFile, OutputStream fileOut)
        throws IOException
    {
        return ConvertExcelToCSV(xlsFile, fileOut, ",");
    }

    public static OutputStream ConvertExcelToCSV(String xlsFile, OutputStream fileOut, String separator)
        throws IOException
    {
        try
        {
            File file = new File(xlsFile);
            FileInputStream xls = new FileInputStream(file);
            String encoding = "UTF8";
            OutputStreamWriter osw = new OutputStreamWriter(fileOut, encoding);
            BufferedWriter bw = new BufferedWriter(osw);
            Workbook workBook = null;
            Sheet sheet = null;
            Cell cell = null;
            if(isXLSXVersion(xlsFile))
                workBook = new XSSFWorkbook(xls);
            else
                workBook = new HSSFWorkbook(xls);
            sheet = workBook.getSheetAt(0);
            for(Iterator rows = sheet.rowIterator(); rows.hasNext(); bw.newLine())
            {
                Iterator cells = ((Row)rows.next()).cellIterator();
                String line = "";
                boolean first = true;
                while(cells.hasNext()) 
                {
                    cell = (Cell)cells.next();
                    String value = "";
                    switch(cell.getCellType())
                    {
                    case 3: // '\003'
                        value = " ";
                        break;

                    case 4: // '\004'
                        value = cell.getBooleanCellValue() ? "true" : "false";
                        break;

                    case 5: // '\005'
                        value = (new Integer(cell.getErrorCellValue())).toString();
                        break;

                    case 2: // '\002'
                        value = cell.getCellFormula();
                        break;

                    case 0: // '\0'
                        if(DateUtil.isCellDateFormatted(cell))
                            value = cell.getDateCellValue().toString();
                        else
                            value = String.valueOf(cell.getNumericCellValue());
                        break;

                    case 1: // '\001'
                    default:
                        value = cell.getRichStringCellValue().toString();
                        break;
                    }
                    if(first)
                    {
                        line = value;
                        first = false;
                    } else
                    {
                        line = (new StringBuilder(String.valueOf(line))).append(separator).append(value).toString();
                    }
                }
                bw.write(line);
            }

        }
        catch(FileNotFoundException e)
        {
            System.out.println((new StringBuilder("File not found: ")).append(xlsFile).toString());
        }
        return fileOut;
    }

    public static void setData(String xlsFile, int sheet, int row, int cell, Object data)
        throws IOException
    {
        FileInputStream xls;
        Workbook workBook;
        File file = new File(xlsFile);
        xls = new FileInputStream(file);
        workBook = null;
        if(isXLSXVersion(xlsFile))
            workBook = new XSSFWorkbook(xls);
        else
            workBook = new HSSFWorkbook(xls);
        setData(workBook, sheet, row, cell, data);
        if(xls != null)
            xls.close();
        return;
    }

    public static void setData(OutputStream out, InputStream xls, int sheet, int row, int cell, Object data)
        throws IOException
    {
        setData(out, xls, sheet, row, cell, data, false);
    }

    public static void setData(OutputStream out, InputStream xls, int sheet, int row, int cell, Object data, boolean xlsx)
        throws IOException
    {
        Workbook workBook = null;
        if(xlsx)
            workBook = new XSSFWorkbook(xls);
        else
            workBook = new HSSFWorkbook(xls);
        setData(workBook, sheet, row, cell, data);
        workBook.write(out);
    }

    public static void setData(Workbook workBook, int sheet, int row, int cell, Object data)
        throws IOException
    {
        Sheet s = workBook.getSheetAt(sheet);
        Row r = s.getRow(row);
        Cell c = r.getCell(cell);
        switch(c.getCellType())
        {
        case 3: // '\003'
            break;

        case 4: // '\004'
            c.setCellValue(data.toString().equals("true"));
            break;

        case 5: // '\005'
            c.setCellErrorValue(((Integer)data).byteValue());
            break;

        case 2: // '\002'
            c.setCellFormula((String)data);
            break;

        case 0: // '\0'
            if(DateUtil.isCellDateFormatted(c))
                c.setCellValue((java.util.Date)data);
            else
                c.setCellValue(((BigDecimal)data).doubleValue());
            break;

        case 1: // '\001'
        default:
            if(workBook instanceof XSSFWorkbook)
                c.setCellValue(new XSSFRichTextString((String)data));
            else
                c.setCellValue(new HSSFRichTextString((String)data));
            break;
        }
    }

    public static Object getData(String xlsFile, int sheet, int row, int cell)
        throws IOException
    {
        FileInputStream xls;
        Object result;
        File file = new File(xlsFile);
        xls = new FileInputStream(file);
        result = null;
        result = getData(((InputStream) (xls)), sheet, row, cell);
        if(xls != null)
            xls.close();
        return result;
    }

    public static Object getData(InputStream xls, int sheet, int row, int cell)
        throws IOException
    {
        return getData(xls, sheet, row, cell, false);
    }

    public static Object getData(InputStream xls, int sheet, int row, int cell, boolean xlsx)
        throws IOException
    {
        Workbook workBook = null;
        if(xlsx)
            workBook = new XSSFWorkbook(xls);
        else
            workBook = new HSSFWorkbook(xls);
        Sheet s = workBook.getSheetAt(sheet);
        Row r = s.getRow(row);
        Cell c = r.getCell(cell);
        Object result = null;
        switch(c.getCellType())
        {
        case 3: // '\003'
            result = "";
            break;

        case 4: // '\004'
            result = new Boolean(c.getBooleanCellValue());
            break;

        case 5: // '\005'
            result = new Byte(c.getErrorCellValue());
            break;

        case 2: // '\002'
            result = c.getCellFormula();
            break;

        case 0: // '\0'
            if(DateUtil.isCellDateFormatted(c))
                result = c.getDateCellValue();
            else
                result = new BigDecimal(c.getNumericCellValue());
            break;

        case 1: // '\001'
        default:
            result = c.getRichStringCellValue().toString();
            break;
        }
        return result;
    }

    public static OutputStream getWorkBook(Workbook workBook, OutputStream fileWriter)
    {
        try
        {
            workBook.write(fileWriter);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return fileWriter;
    }

    public static Workbook getWorkBookFromFile(File file)
        throws IOException
    {
        FileInputStream xls;
        Workbook workBook;
        xls = null;
        workBook = null;
        try
        {
            xls = new FileInputStream(file);
            if(isXLSXVersion(file.getName()))
                workBook = new XSSFWorkbook(xls);
            else
                workBook = new HSSFWorkbook(xls);
        }
        catch(FileNotFoundException e)
        {
            System.out.println((new StringBuilder("File not found: ")).append(file.getAbsolutePath()).toString());
        }
        if(xls != null)
            try
            {
                xls.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        finally{
        if(xls != null)
            try
            {
                xls.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        if(xls != null)
            try
            {
                xls.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }}
        return workBook;
    }

    public static OutputStream getOutputStreamFromFile(String xlsFile, OutputStream fileWriter)
    {
        File file;
        FileInputStream xls;
        file = new File(xlsFile);
        xls = null;
        try
        {
            xls = new FileInputStream(file);
            Workbook workBook = null;
            if(isXLSXVersion(xlsFile))
                workBook = new XSSFWorkbook();
            else
                workBook = new HSSFWorkbook();
            fileWriter = getWorkBook(workBook, fileWriter);
        }
        catch(FileNotFoundException e)
        {
            System.out.println((new StringBuilder("File not found: ")).append(xlsFile).toString());
        }
        if(xls != null)
            try
            {
                xls.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
            finally{
        if(xls != null)
            try
            {
                xls.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        if(xls != null)
            try
            {
                xls.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }}
        return fileWriter;
    }

    public static Workbook createWorkBook(String fields[], boolean xlsx)
    {
        Workbook workBook = null;
        if(xlsx)
            workBook = new XSSFWorkbook();
        else
            workBook = new HSSFWorkbook();
        Sheet sheet = workBook.createSheet();
        Row row = sheet.createRow(0);
        sheet.createFreezePane(0, 1);
        Font setFont = workBook.createFont();
        setFont.setBoldweight((short)700);
        CellStyle headerCellStyle = workBook.createCellStyle();
        headerCellStyle.setFillForegroundColor((short)43);
        headerCellStyle.setFillPattern((short)1);
        headerCellStyle.setFont(setFont);
        headerCellStyle.setAlignment((short)2);
        headerCellStyle.setLocked(true);
        for(int i = 0; i < fields.length; i++)
        {
            int width = 256 * (fields[i].length() + 5);
            sheet.setColumnWidth(i, width);
        }

        for(int i = 0; i < fields.length; i++)
        {
            Cell cell = row.createCell(i, 1);
            if(xlsx)
                cell.setCellValue(new XSSFRichTextString(fields[i]));
            else
                cell.setCellValue(new HSSFRichTextString(fields[i]));
            cell.setCellStyle(headerCellStyle);
        }

        return workBook;
    }

    private static int isValidField(String fields[], String tag)
    {
        int result = -1;
        for(int i = 0; i < fields.length; i++)
        {
            if(!fields[i].equals(tag))
                continue;
            result = i;
            break;
        }

        return result;
    }

    public static void insertRow(Sheet sheet, Vector fields, Object bean)
    {
        try
        {
            String fieldNames[] = new String[fields.size()];
            for(int i = 0; i < fieldNames.length; i++)
                fieldNames[i] = ((ExcelColStyle)fields.get(i)).getTag() != null ? ((ExcelColStyle)fields.get(i)).getTag() : ((ExcelColStyle)fields.get(i)).getName();

            Row row = sheet.createRow(sheet.getLastRowNum() + 1);
            PropertyDescriptor pds[] = Introspector.getBeanInfo(bean.getClass()).getPropertyDescriptors();
            String type = "";
            int index = 0;
            String name = "";
            for(int i = 0; i < pds.length; i++)
            {
                name = pds[i].getName();
                if(!name.equals("class"))
                {
                    index = isValidField(fieldNames, name);
                    if(index >= 0)
                    {
                        type = getClassName(pds[i].getPropertyType());
                        Method getter = pds[i].getReadMethod();
                        if(getter != null)
                            if(type.equals("java.math.BigInteger"))
                            {
                                BigInteger value = (BigInteger)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 0);
                                cell.setCellValue(value.doubleValue());
                            } else
                            if(type.equals("java.math.BigDecimal"))
                            {
                                BigDecimal value = (BigDecimal)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 0);
                                cell.setCellValue(value.doubleValue());
                            } else
                            if(type.equals("boolean"))
                            {
                                Boolean value = (Boolean)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 4);
                                cell.setCellValue(value.booleanValue());
                            } else
                            if(type.equals("short"))
                            {
                                Short value = (Short)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 0);
                                cell.setCellValue(value.doubleValue());
                            } else
                            if(type.equals("int") || type.equals("java.lang.Integer"))
                            {
                                Integer value = (Integer)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 0);
                                cell.setCellValue(value.doubleValue());
                            } else
                            if(type.equals("long") || type.equals("java.lang.Long"))
                            {
                                Long value = (Long)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 0);
                                cell.setCellValue(value.doubleValue());
                            } else
                            if(type.equals("float"))
                            {
                                Float value = (Float)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 0);
                                cell.setCellValue(value.doubleValue());
                            } else
                            if(type.equals("double"))
                            {
                                Double value = (Double)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 0);
                                cell.setCellValue(value.doubleValue());
                            } else
                            if(type.equals("java.sql.Date"))
                            {
                                java.util.Date value = (java.util.Date)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 1);
                                cell.setCellValue(value);
                            } else
                            if(type.equals("java.sql.Time"))
                            {
                                Time value = (Time)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 1);
                                cell.setCellValue(value);
                            } else
                            if(type.equals("java.sql.Timestamp"))
                            {
                                Timestamp value = (Timestamp)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 1);
                                cell.setCellValue(value);
                            } else
                            if(type.equals("java.lang.String"))
                            {
                                String value = (String)getter.invoke(bean, new Object[0]);
                                Cell cell = row.createCell(index, 1);
                                if(sheet instanceof XSSFSheet)
                                    cell.setCellValue(new XSSFRichTextString(value));
                                else
                                    cell.setCellValue(new HSSFRichTextString(value));
                            }
                    }
                }
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void insertRow(Sheet sheet, Vector fields, MessageRecord message)
    {
        String fieldNames[] = new String[fields.size()];
        for(int i = 0; i < fieldNames.length; i++)
            fieldNames[i] = ((ExcelColStyle)fields.get(i)).getTag() != null ? ((ExcelColStyle)fields.get(i)).getTag() : ((ExcelColStyle)fields.get(i)).getName();

        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        Enumeration enu = message.fieldEnumeration();
        MessageField field = null;
        int index = 0;
        String name = "";
        while(enu.hasMoreElements()) 
        {
            field = (MessageField)enu.nextElement();
            name = field.getTag();
            index = isValidField(fieldNames, name);
            if(index >= 0)
            {
                Cell cell = row.createCell(index, field.getType() != 2 ? 1 : 0);
                if(field.getType() == 2)
                {
                    if(field.getDecimals() == 0 && field.getLength() > 11)
                    {
                        if(sheet instanceof XSSFSheet)
                            cell.setCellValue(new XSSFRichTextString(field.getString()));
                        else
                            cell.setCellValue(new HSSFRichTextString(field.getString()));
                    } else
                    {
                        cell.setCellValue(Util.parseCCYtoDouble(field.getString()));
                    }
                } else
                if(sheet instanceof XSSFSheet)
                    cell.setCellValue(new XSSFRichTextString(field.getString()));
                else
                    cell.setCellValue(new HSSFRichTextString(field.getString()));
            }
        }
    }

    private static void setColumnUnLocked(int column, Workbook workBook)
    {
        CellStyle lockedStyle = workBook.createCellStyle();
        lockedStyle.setLocked(false);
        Sheet sheet = workBook.getSheetAt(0);
        for(int rowIndex = sheet.getFirstRowNum() + 1; rowIndex <= sheet.getLastRowNum(); rowIndex++)
        {
            Row row = sheet.getRow(rowIndex);
            Cell cell = row.getCell(column);
            cell.setCellStyle(lockedStyle);
        }

    }

    public static OutputStream getWorkBook(OutputStream fileWriter, Vector fields, List list)
    {
        return getWorkBook(fileWriter, fields, list, true);
    }

    public static OutputStream getWorkBook(OutputStream fileWriter, Vector fields, List list, boolean readonly)
    {
        return getWorkBook(fileWriter, fields, list, readonly, false);
    }

    public static OutputStream getWorkBook(OutputStream fileWriter, Vector fields, List list, boolean readonly, boolean xlsx)
    {
        String fieldNames[] = new String[fields.size()];
        for(int i = 0; i < fieldNames.length; i++)
            fieldNames[i] = ((ExcelColStyle)fields.get(i)).getName();

        Workbook workBook = createWorkBook(fieldNames, xlsx);
        Sheet sheet = workBook.getSheetAt(0);
        if(!list.isEmpty())
        {
            for(Iterator iterator = list.iterator(); iterator.hasNext();)
            {
                Object object = iterator.next();
                if(object instanceof MessageRecord)
                    insertRow(sheet, fields, (MessageRecord)object);
                else
                    insertRow(sheet, fields, object);
            }

            for(int i = 0; i < fieldNames.length; i++)
                if(!((ExcelColStyle)fields.get(i)).isLocked())
                    setColumnUnLocked(i, workBook);

            for(int i = 0; i < fieldNames.length; i++)
                sheet.setColumnHidden(i, ((ExcelColStyle)fields.get(i)).isHidden());

            if(readonly)
                sheet.protectSheet("eloy");
        }
        try
        {
            workBook.write(fileWriter);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return fileWriter;
    }

    public static OutputStream createWorkBook(OutputStream fileWriter, Vector fields, boolean xlsx)
    {
        String fieldNames[] = new String[fields.size()];
        for(int i = 0; i < fieldNames.length; i++)
            fieldNames[i] = ((ExcelColStyle)fields.get(i)).getName();

        Workbook workBook = createWorkBook(fieldNames, xlsx);
        try
        {
            workBook.write(fileWriter);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return fileWriter;
    }
}
