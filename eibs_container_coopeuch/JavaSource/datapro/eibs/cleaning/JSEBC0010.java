package datapro.eibs.cleaning; 

/*********************************************************************************************************************************/
/**  Creado     por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  07/08/2012                                                                                      **/
/**  Objetivo                :  Marcar y desmarcar documentos a enviar al boletin Comercial                                     **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/

import java.beans.Beans;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EBC001001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSEBC0010 extends datapro.eibs.master.SuperServlet {


	protected static final int R_SEARCH = 1;
	protected static final int A_SEARCH = 3;

 	protected static final int A_RETURN_ITEMS = 5; 
	protected static final int R_LARGE_ITEMS = 8;
 

	protected String LangPath = "S";

	/**
	 * JSECLI001 constructor comment.
	 */
	public JSEBC0010() {
		super();
	}

	/**
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

		flexLog("free resources used by JSEBC0010");

	}

	/**
	 * This method was created by 
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	/**
	 * Solicita parametros de seleccion 
	 */
	protected void procReqSearch(
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {
		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {
			msgError =
				(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
					getClass().getClassLoader(),
					"datapro.eibs.beans.ELEERRMessage");
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);

		try {
			flexLog(
				"About to call Page: "
					+ LangPath
					+ "EBC0010_boletin_comercial_search.jsp");
			callPage(LangPath + "EBC0010_boletin_comercial_search.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}

	}
	/**
	 * Trae lista de datos de acuerdo a los criterios de seleccion
	 */
	protected void procReqList(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		EBC001001Message msgSearch = null;
		ELEERRMessage msgError = null;
		boolean IsNotError = false;
		UserPos userPO = null;
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		msgError = new datapro.eibs.beans.ELEERRMessage();

		try {

			msgSearch = (EBC001001Message) mc.getMessageRecord("EBC001001");
			msgSearch.setH01USERID(user.getH01USR());
			msgSearch.setH01PROGRM("EBC0010");
			msgSearch.setH01TIMSYS(getTimeStamp());
			msgSearch.setH01SCRCOD("01");
			msgSearch.setH01OPECOD("0001");

			// all the fields here
			java.util.Enumeration enu = msgSearch.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) {
				field = (MessageField) enu.nextElement();
				try {
					value = req.getParameter(field.getTag()).toUpperCase();
					if (value != null) {
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}
			if (msgSearch.getE01SELBRN().equals("0") && msgSearch.getE01SELCUN().equals("0"))
			{
				msgSearch.setE01SELBRN(userPO.getBranch());
				msgSearch.setE01SELCUN(userPO.getCusNum());
			}
			if (req.getParameter("FromRecord") != null) 
				{
				  msgSearch.setE01SELFRR(req.getParameter("FromRecord"));				
				}
			else if (userPO.getHeader9() != null && userPO.getHeader9() != "")
				  {
				    int numero = 0;
				    numero = Integer.parseInt(userPO.getHeader9());
					numero = numero - 1;
					msgSearch.setE01SELFRR(String.valueOf(numero));	
				  }
				  else
				  {
				  msgSearch.setE01SELFRR("0");
				  }
			
            flexLog("Mensaje enviado..." + msgSearch);
			msgSearch.send();
			msgSearch.destroy();
			flexLog("EBC001001 Message Sent");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
		//=============================================================================
		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();
			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
       //=============================================================================	
      // Receive Data
		try {
			newmessage = mc.receiveMessage();
	        flexLog("Mensaje data recibido..." + newmessage.getFormatName());
			if (newmessage.getFormatName().equals("EBC001001")) 
			{
				JBObjList beanList = new JBObjList();
				boolean firstTime = true;
				String marker = "";
				String chk = "";
			//=============================================================================					
				while (true) 
				{
					msgSearch = (EBC001001Message) newmessage;
			        flexLog("Mensaje detalle recibido..." + msgSearch);
					marker = msgSearch.getH01FLGMAS();
					if (firstTime) 
					{
						userPO.setHeader19(msgSearch.getE01BCMIND());
						userPO.setHeader20(msgSearch.getE01BCMINM());
						userPO.setHeader21(msgSearch.getE01BCMINY());
						userPO.setHeader22(msgSearch.getD01SELBRN());
						userPO.setHeader23(msgSearch.getD01SELCUN());
						userPO.setBranch(msgSearch.getE01SELBRN());
						userPO.setCusNum(msgSearch.getE01SELCUN());
						beanList.setFirstRec(
								Integer.parseInt(msgSearch.getE01SELFRR()));						
						firstTime = false;
						chk = "checked";
					}
					else 
					{
						chk = "";
					}

					if (marker.equals("*")) 
					{
						beanList.setShowNext(false);
						break;
					} 
					else 
					{
						beanList.addRow(msgSearch);
						if (marker.equals("+")) 
						{
						beanList.setShowNext(true);
						break;
						}
					}
					newmessage = mc.receiveMessage();
				}
		  //=============================================================================	

				flexLog("Putting java beans into the session");
				ses.setAttribute("EBC0010Help", beanList);
				ses.setAttribute("userPO", userPO);
			
				try 
				{
					flexLog("About to call Page:******** "
							+ LangPath
							+ "EBC0010_boletin_comercial_list.jsp");
					callPage(LangPath
							+ "EBC0010_boletin_comercial_list.jsp",
						req,
						res);
					}
				catch (Exception e) 
				{
					flexLog("Exception calling page " + e);
					e.printStackTrace();
				}
			//=============================================================================
			} 
			else
			{
				flexLog("Message " + newmessage.getFormatName() + " received.");
			}
	}
			catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}

	/**
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 */

	protected void procActionList(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses) {

			MessageRecord newmessage = null;
			EBC001001Message msgSearch = null;
			ELEERRMessage msgError = null;
			boolean IsNotError = false;
			UserPos userPO = null;
			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			try 
			{
				JBObjList beanList = (JBObjList) ses.getAttribute("EBC0010Help");
				beanList.initRow();
				int fil =0;
				boolean firstTime = true;
				while (beanList.getNextRow()) 
				{
					try 
					{					
						msgSearch = (EBC001001Message) beanList.getRecord();
						msgSearch.setH01USERID(user.getH01USR());
						msgSearch.setH01PROGRM("EBC0010");
						msgSearch.setH01TIMSYS(getTimeStamp());
						msgSearch.setH01SCRCOD("01");
						msgSearch.setH01OPECOD("0002");

						msgSearch.setE01BCMPFL(req.getParameter("E01BCMPFL"+fil));
//REQ - 0038 - INICIO
						msgSearch.setE01BCMGMD(req.getParameter("E01BCMGMD"+fil));
						msgSearch.setE01BCMCMD(req.getParameter("E01BCMCMD"+fil));
//REQ - 0038 - FIN
						if (firstTime) 
						{
							userPO.setHeader9(msgSearch.getE01SELFRR());
							firstTime = false;
						}						
                        if (!msgSearch.getE01BCMPFL().equals(msgSearch.getE01BCMPFA()))
                        {
                        	flexLog("mensaje enviado..." + msgSearch);
                        	mc.sendMessage(msgSearch);
                        	msgSearch.destroy();
                        	flexLog("EBC0010 Message Sent");

                        	// Receive Error Message
                        	newmessage = mc.receiveMessage();

                        	if (newmessage.getFormatName().equals("ELEERR")) 
                        	{
                        		msgError = (ELEERRMessage) newmessage;
                        		IsNotError = msgError.getERRNUM().equals("0");
                        		if (IsNotError) 
                        		{
                        			//	
                        		} 
                        		else 
                        		{
//REQ - 0038 - INICIO
                        			ses.setAttribute("error", msgError);
                    				break;
//REQ - 0038 - INICIO
                        		}
                        	}
                        }
                    }
                    catch (Exception e) 
                    {
                       	e.printStackTrace();
                       	flexLog("Error: " + e);
                       	throw new RuntimeException("Socket Communication Error");
                    }
					  fil++;
				}

					procReqList(mc, user, req, res, ses);


			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

		}
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		Socket s = null;
		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
	  	HttpSession session = null;

		session = (HttpSession)req.getSession(false); 

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			int screen = R_SEARCH;

			try {

				msgUser =
					(datapro.eibs.beans.ESS0030DSMessage) session.getAttribute(
						"currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					s = new Socket(super.hostIP, getInitSocket(req) + 1);
					s.setSoTimeout(super.sckTimeOut);
				  	mc = new MessageContext(new DataInputStream(new BufferedInputStream(s.getInputStream())),
								      	    new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
										    "datapro.eibs.beans");

					try {
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					} catch (Exception e) {
						flexLog("Screen set to default value");
					}
				    flexLog("Entre con Screen = " + screen);
					switch (screen) {
						// Requests
						case R_SEARCH :
							procReqSearch(msgUser, req, res, session);
							break;
						case A_SEARCH :
							procReqList(mc, msgUser, req, res, session);
							break;
						case R_LARGE_ITEMS :
							procReqList(mc, msgUser, req, res, session);
							break;
						case A_RETURN_ITEMS :
							procActionList(mc, msgUser, req, res, session);
							break;
						default :
							res.sendRedirect(
								super.srctx + LangPath + super.devPage);
							break;
					}
				} 			catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx +LangPath + super.sckNotOpenPage);
				//	return;
				} finally {
					s.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(
					super.srctx + LangPath + super.sckNotRespondPage);
			}

		}

	}
	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

		}
	}
}