/* Double Master for Banesco */
package com.datapro.eibs.internet;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfWriter;
import datapro.eibs.beans.*;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.*;

import java.awt.Color;
import java.io.*;
import java.net.Socket;
import java.util.Enumeration;
import javax.servlet.*;
import javax.servlet.http.*;

public class JSESS2000 extends SuperServlet
{
    protected static final int R_ENTER_NEW_COR = 100;
    protected static final int R_ENTER_NEW_PER = 200;
    protected static final int R_ENTER_NEW_COR_N = 102;
    protected static final int R_ENTER_NEW_PER_N = 202;
    protected static final int R_ENTER_MAINT_COR = 110;
    protected static final int R_ENTER_MAINT_PER = 210;
    protected static final int R_ENTER_MAINT_COR_N = 112;
    protected static final int R_ENTER_MAINT_PER_N = 212;
    protected static final int R_STATUS = 300;
    protected static final int R_PASSWORD = 400;
    protected static final int R_PASSWORD_N = 402;
    protected static final int R_PASSWORD_GEN = 425;
    protected static final int A_SUBMIT_COR = 101;
    protected static final int A_SUBMIT_PER = 201;
    protected static final int A_STATUS = 301;
    protected static final int A_PASSWORD = 401;
    protected static final int R_USER_INQ = 409;
    protected String LangPath;
    
    public JSESS2000() 
    {
        LangPath = "E";
    }

    public void destroy()
    {
        flexLog("free resources used by JSESS2000");
    }

    public void init(ServletConfig config)
        throws ServletException
    {
        super.init(config);
    }

    public void service(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        Socket s = null;
        MessageContext mc = null;
        ESS0030DSMessage msgUser = null;
        HttpSession session = null;
        session = req.getSession(false);
        if(session == null)
        {
            try
            {
                res.setContentType("text/html");
                printLogInAgain(res.getWriter());
            }
            catch(Exception e)
            {
                e.printStackTrace();
                flexLog("Exception ocurred. Exception = " + e);
            }
        } else
        {
            int screen = 0;
            try
            {
                msgUser = (ESS0030DSMessage)session.getAttribute("currUser");
                LangPath = SuperServlet.rootPath + msgUser.getE01LAN() + "/";
                try
                {
                    flexLog("Opennig Socket Connection");
    				mc = new MessageContext(super.getMessageHandler("ESD2000", req));
                    try
                    {
                        screen = Integer.parseInt(req.getParameter("SCREEN"));
                    }
                    catch(Exception e)
                    {
                        flexLog("Screen set to default value");
                    }
                    switch(screen)
                    {
                    case R_ENTER_NEW_COR: // 'd'
                        procReqCorNew(req, res);
                        break;

                    case R_ENTER_NEW_PER: 
                        procReqPerNew(req, res, session);
                        break;

                    case R_ENTER_NEW_COR_N: // new corp.
                        procReqCorNewData(mc, msgUser, req, res, session);
                        break;

                    case R_ENTER_NEW_PER_N: 
                        procReqPerNewData(mc, msgUser, req, res, session);
                        break;

                    case R_ENTER_MAINT_COR: // 'n'
                        procReqCorMaint(req, res);
                        break;

                    case R_ENTER_MAINT_PER: 
                        procReqPerMaint(req, res);
                        break;

                    case R_ENTER_MAINT_COR_N: // 'p'
                        procReqCorMaintData(mc, msgUser, req, res, session);
                        break;

                    case R_ENTER_MAINT_PER_N: 
                        procReqPerMaintData(mc, msgUser, req, res, session);
                        break;

                    case R_PASSWORD: 
                        procReqPass(req, res);
                        break;

                    case R_PASSWORD_N: 
                        procReqPassData(req, res, session);
                        break;

                    case R_STATUS: 
                        procReqStatus(req, res);
                        break;

                    case A_PASSWORD: 
                        procActionPass(mc, msgUser, req, res, session);
                        break;

                    case R_PASSWORD_GEN: 
                        procReqPasswordGenerator(mc, msgUser, req, res, session);
                        break;

                    case A_STATUS: 
        				mc = new MessageContext(super.getMessageHandler("ESD0080", req));
                        procActionPass(mc, msgUser, req, res, session);
                        break;

                    case A_SUBMIT_COR: // 'e'
                        procAction(mc, msgUser, req, res, session);
                        break;

                    case A_SUBMIT_PER: 
                        procAction(mc, msgUser, req, res, session);
                        break;

                    case R_USER_INQ: 
                        reqUserInquiry(mc, msgUser, req, res, session);
                        break;

                    default:
                        res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.devPage);
                        break;
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    int sck = SuperServlet.iniSocket + 2;
                    flexLog("Socket not Open(Port " + sck + "). Error: " + e);
                    res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.sckNotOpenPage);
                }
                finally
                {
                    s.close();
                }
            }
            catch(Exception e)
            {
                flexLog("Error: " + e);
                res.sendRedirect(SuperServlet.srctx + LangPath + SuperServlet.sckNotRespondPage);
            }
        }
    }

    protected void procReqPass(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        try
        {
            flexLog("About to call Page: " + LangPath + "ESS2000_client_change_password.jsp");
            callPage(LangPath + "ESS2000_client_change_password.jsp", req, res);
        }
        catch(Exception e)
        {
            flexLog("Exception calling page " + e);
        }
    }

    protected void procReqPassData(HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        ESS200001Message cusdata = null;
        cusdata = new ESS200001Message();
        String CUSID = null;
        CUSID = req.getParameter("CUSID");
        cusdata.setE01EUSUSR(CUSID);
        ses.setAttribute("cusdata", cusdata);
        try
        {
            flexLog("About to call Page: " + LangPath + "ESS2000_client_change_password_data.jsp");
            callPage(LangPath + "ESS2000_client_change_password_data.jsp", req, res);
        }
        catch(Exception e)
        {
            flexLog("Exception calling page " + e);
        }
    }

    protected void procReqStatus(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        try
        {
            flexLog("About to call Page: " + LangPath + "ESS2000_client_corporate_new.jsp");
            callPage(LangPath + "ESS2000_client_corporate_new.jsp", req, res);
        }
        catch(Exception e)
        {
            flexLog("Exception calling page " + e);
        }
    }

    protected void procReqCorNew(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        try
        {
            flexLog("About to call Page: " + LangPath + "ESS2000_client_corporate_new.jsp");
            callPage(LangPath + "ESS2000_client_corporate_new.jsp", req, res);
        }
        catch(Exception e)
        {
            flexLog("Exception calling page " + e);
        }
    }

    protected void procReqPerNew(HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        ESS200001Message msgClientPersonal = null;
        msgClientPersonal = new ESS200001Message();
        ses.setAttribute("cusdata", msgClientPersonal);
        try
        {
            flexLog("About to call Page: " + LangPath + "ESS2000_client_personal_new.jsp");
            callPage(LangPath + "ESS2000_client_personal_new.jsp", req, res);
        }
        catch(Exception e)
        {
            flexLog("Exception calling page " + e);
        }
    }

    protected void procReqCorNewData(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ESS200001Message msgClientCorporate = null;
        UserPos userPO = null;
        ELEERRMessage msgError = null;
        boolean IsNotError = false;
        msgClientCorporate = new ESS200001Message();
        String CUSID = null;
        String MASTERID = null;
        CUSID = req.getParameter("CUSID").toUpperCase();
        MASTERID = req.getParameter("MASTERID").toUpperCase();
        try
        {
            msgClientCorporate = (ESS200001Message)mc.getMessageRecord("ESS200001");
            msgClientCorporate.setH01USERID(user.getH01USR());
            msgClientCorporate.setH01PROGRM("ESS2000");
            msgClientCorporate.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgClientCorporate.setH01FLGWK1("C");
            msgClientCorporate.setH01OPECOD("0003");
            msgClientCorporate.setE01EUSUSR(CUSID);
            msgClientCorporate.setE01EUSENT(MASTERID); 
            msgClientCorporate.send();
            msgClientCorporate.destroy();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR"))
            {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
		try
		{
			newmessage = mc.receiveMessage();
			try
			{
				msgClientCorporate = new ESS200001Message();
			}
			catch(Exception ex)
			{
				flexLog("Error: " + ex);
			}
			msgClientCorporate = (ESS200001Message)newmessage;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

        msgClientCorporate.setE01EUSUSR(CUSID);
        ses.setAttribute("cusdata", msgClientCorporate);
        ses.setAttribute("error", msgError);
        userPO = (UserPos)ses.getAttribute("userPO");
        userPO.setPurpose("");
        ses.setAttribute("userPO", userPO);
        if(IsNotError)
            try
            {
                flexLog("About to call Page: " + LangPath + "ESS2000_client_corporate_new_data.jsp");
                callPage(LangPath + "ESS2000_client_corporate_new_data.jsp", req, res);
            }
            catch(Exception e)
            {
                flexLog("Exception calling page " + e);
            }
        else
            try
            {
                flexLog("About to call Page: " + LangPath + "ESS2000_client_corporate_new.jsp");
                callPage(LangPath + "ESS2000_client_corporate_new.jsp", req, res);
            }
            catch(Exception e)
            {
                flexLog("Exception calling page " + e);
            }
    }

    protected void procReqPerNewData(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ESS200001Message msgClientPersonal = null;
        UserPos userPO = null;
        ELEERRMessage msgError = null;
        boolean IsNotError = false;
        msgClientPersonal = new ESS200001Message();
        String CUSID = null;
        CUSID = req.getParameter("CUSID");
        try
        {
            msgClientPersonal = (ESS200001Message)mc.getMessageRecord("ESS200001");
            msgClientPersonal.setH01USERID(user.getH01USR());
            msgClientPersonal.setH01PROGRM("ESS2000");
            msgClientPersonal.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgClientPersonal.setH01FLGWK1("U");
            msgClientPersonal.setH01OPECOD("0003");
            msgClientPersonal.setE01EUSUSR(CUSID);
            msgClientPersonal.send();
            msgClientPersonal.destroy();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR"))
            {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            try
            {
                msgClientPersonal = new ESS200001Message();
            }
            catch(Exception ex)
            {
                flexLog("Error: " + ex);
            }
            msgClientPersonal = (ESS200001Message)newmessage;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        msgClientPersonal.setE01EUSUSR(CUSID);
        ses.setAttribute("cusdata", msgClientPersonal);
        ses.setAttribute("error", msgError);
        userPO = (UserPos)ses.getAttribute("userPO");
        userPO.setPurpose("");
        ses.setAttribute("userPO", userPO);
        if(IsNotError)
            try
            {
                flexLog("About to call Page: " + LangPath + "ESS2000_client_personal_new_data.jsp");
                callPage(LangPath + "ESS2000_client_personal_new_data.jsp", req, res);
            }
            catch(Exception e)
            {
                flexLog("Exception calling page " + e);
            }
        else
            try
            {
                flexLog("About to call Page: " + LangPath + "ESS2000_client_personal_new.jsp");
                callPage(LangPath + "ESS2000_client_personal_new.jsp", req, res);
            }
            catch(Exception e)
            {
                flexLog("Exception calling page " + e);
            }
    }

    protected void procReqCorMaintData(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ESS200001Message msgClientCorporate = null;
        UserPos userPO = null;
        ELEERRMessage msgError = null;
        boolean IsNotError = false;
        msgClientCorporate = new ESS200001Message();
        String CUSID = null;
        String MASTERID = null;
        CUSID = req.getParameter("CUSID");
		MASTERID = req.getParameter("MASTERID").toUpperCase();
        try
        {
            msgClientCorporate = (ESS200001Message)mc.getMessageRecord("ESS200001");
            msgClientCorporate.setH01USERID(user.getH01USR());
            msgClientCorporate.setH01PROGRM("ESS2000");
            msgClientCorporate.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgClientCorporate.setH01FLGWK1("C");
            msgClientCorporate.setH01OPECOD("0002");
            msgClientCorporate.setE01EUSUSR(CUSID);
			msgClientCorporate.setE01EUSENT(MASTERID); 
            msgClientCorporate.send();
            msgClientCorporate.destroy();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR"))
            {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            try
            {
                msgClientCorporate = new ESS200001Message();
            }
            catch(Exception ex)
            {
                flexLog("Error: " + ex);
            }
            msgClientCorporate = (ESS200001Message)newmessage;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        msgClientCorporate.setE01EUSUSR(CUSID);
        ses.setAttribute("cusdata", msgClientCorporate);
        ses.setAttribute("error", msgError);
        userPO = (UserPos)ses.getAttribute("userPO");
        userPO.setPurpose("MAINT");
        ses.setAttribute("userPO", userPO);
        if(IsNotError)
            try
            {
                flexLog("About to call Page: " + LangPath + "ESS2000_client_corporate_new_data.jsp");
                callPage(LangPath + "ESS2000_client_corporate_new_data.jsp", req, res);
            }
            catch(Exception e)
            {
                flexLog("Exception calling page " + e);
            }
        else
            try
            {
                flexLog("About to call Page: " + LangPath + "ESS2000_client_corporate_maint.jsp");
                callPage(LangPath + "ESS2000_client_corporate_maint.jsp", req, res);
            }
            catch(Exception e)
            {
                flexLog("Exception calling page " + e);
            }
    }

    protected void procReqPerMaintData(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ESS200001Message msgClientPersonal = null;
        UserPos userPO = null;
        ELEERRMessage msgError = null;
        boolean IsNotError = false;
        msgClientPersonal = new ESS200001Message();
        String CUSID = null;
        CUSID = req.getParameter("CUSID");
        try
        {
            msgClientPersonal = (ESS200001Message)mc.getMessageRecord("ESS200001");
            msgClientPersonal.setH01USERID(user.getH01USR());
            msgClientPersonal.setH01PROGRM("ESS2000");
            msgClientPersonal.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgClientPersonal.setH01FLGWK1("U");
            msgClientPersonal.setH01OPECOD("0002");
            msgClientPersonal.setE01EUSUSR(CUSID);
            msgClientPersonal.send();
            msgClientPersonal.destroy();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR"))
            {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            try
            {
                msgClientPersonal = new ESS200001Message();
            }
            catch(Exception ex)
            {
                flexLog("Error: " + ex);
            }
            msgClientPersonal = (ESS200001Message)newmessage;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        msgClientPersonal.setE01EUSUSR(CUSID);
        ses.setAttribute("cusdata", msgClientPersonal);
        ses.setAttribute("error", msgError);
        userPO = (UserPos)ses.getAttribute("userPO");
        userPO.setPurpose("MAINT");
        ses.setAttribute("userPO", userPO);
        if(IsNotError)
            try
            {
                flexLog("About to call Page: " + LangPath + "ESS2000_client_personal_new_data.jsp");
                callPage(LangPath + "ESS2000_client_personal_new_data.jsp", req, res);
            }
            catch(Exception e)
            {
                flexLog("Exception calling page " + e);
            }
        else
            try
            {
                flexLog("About to call Page: " + LangPath + "ESS2000_client_personal_maint.jsp");
                callPage(LangPath + "ESS2000_client_personal_maint.jsp", req, res);
            }
            catch(Exception e)
            {
                flexLog("Exception calling page " + e);
            }
    }

    protected void procReqCorMaint(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        try
        {
            flexLog("About to call Page: " + LangPath + "ESS2000_client_corporate_maint.jsp");
            callPage(LangPath + "ESS2000_client_corporate_maint.jsp", req, res);
        }
        catch(Exception e)
        {
            flexLog("Exception calling page " + e);
        }
    }

    protected void procReqPerMaint(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        try
        {
            flexLog("About to call Page: " + LangPath + "ESS2000_client_personal_maint.jsp");
            callPage(LangPath + "ESS2000_client_personal_maint.jsp", req, res);
        }
        catch(Exception e)
        {
            flexLog("Exception calling page " + e);
        }
    }

    protected void procAction(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ESS200001Message msgRT = null;
        ELEERRMessage msgError = null;
        UserPos userPO = null;
        boolean IsNotError = false;
        String USRFLG = null;
        String PERCOR = null;
        String SCREEN = null;
        try
        {
            msgError = new ELEERRMessage();
        }
        catch(Exception ex)
        {
            flexLog("Error: " + ex);
        }
        userPO = (UserPos)ses.getAttribute("userPO");
        PERCOR = userPO.getPurpose();
        USRFLG = req.getParameter("E01EUSCTY");
        if(USRFLG.equals("1"))
            USRFLG = "C";
        if(USRFLG.equals("2"))
            USRFLG = "U";
        try
        {
            flexLog("Send Initial Data");
            msgRT = (ESS200001Message)mc.getMessageRecord("ESS200001");
            msgRT.setH01USERID(user.getH01USR());
            msgRT.setH01PROGRM("ESD200001");
            msgRT.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgRT.setH01FLGWK1(USRFLG);
            msgRT.setH01OPECOD("0005");
            Enumeration enu = msgRT.fieldEnumeration();
            MessageField field = null;
            String value = null;
            while(enu.hasMoreElements()) 
            {
                field = (MessageField)enu.nextElement();
                try
                {
                    value = req.getParameter(field.getTag()).toUpperCase();
                    if(value != null)
                        field.setString(value);
                }
                catch(Exception exception) { }
            }
            mc.sendMessage(msgRT);
            msgRT.destroy();
            flexLog("ESD200001 Message Sent");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR"))
            {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ESS200001"))
            {
                try
                {
                    msgRT = new ESS200001Message();
                    flexLog("ESS200001 Message Received");
                }
                catch(Exception ex)
                {
                    flexLog("Error: " + ex);
                }
                msgRT = (ESS200001Message)newmessage;
                flexLog("Putting java beans into the session");
                ses.setAttribute("error", msgError);
                ses.setAttribute("cusdata", msgRT);
                ses.setAttribute("userPO", userPO);
                if(IsNotError)
                {
                    if(PERCOR.equals(""))
                    {
                        if(USRFLG.equals("C"))
                            SCREEN = "100";
                        if(USRFLG.equals("U"))
                            SCREEN = "200";
                    } else
                    {
                        if(USRFLG.equals("C"))
                            SCREEN = "110";
                        if(USRFLG.equals("U"))
                            SCREEN = "210";
                    }
                    if(msgRT.getH01FLGWK3().trim().equals("N"))
                        callPage(LangPath + "ESS2000_personal_password_new.jsp", req, res);
                    else
                        res.sendRedirect(SuperServlet.srctx + "/servlet/com.datapro.eibs.internet.JSESS2000?SCREEN=" + SCREEN);
                } else
                {
                    try
                    {
                        if(USRFLG.equals("C"))
                        {
                            flexLog("About to call Page: " + LangPath + "ESS2000_client_corporate_new_data.jsp");
                            callPage(LangPath + "ESS2000_client_corporate_new_data.jsp", req, res);
                        }
                        if(USRFLG.equals("U"))
                        {
                            flexLog("About to call Page: " + LangPath + "ESS2000_client_personal_new_data.jsp");
                            callPage(LangPath + "ESS2000_client_personal_new_data.jsp", req, res);
                        }
                    }
                    catch(Exception e)
                    {
                        flexLog("Exception calling page " + e);
                    }
                }
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
    }

    protected void procActionPass(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ESD008020Message msgClientPersonal = null;
        ELEERRMessage msgError = null;
        UserPos userPO = null;
        boolean IsNotError = false;
        try
        {
            msgError = new ELEERRMessage();
        }
        catch(Exception ex)
        {
            flexLog("Error: " + ex);
        }
        userPO = (UserPos)ses.getAttribute("userPO");
        try
        {
            flexLog("Send Initial Data");
            msgClientPersonal = (ESD008020Message)mc.getMessageRecord("ESD008020");
            msgClientPersonal.setH20USR(user.getH01USR());
            msgClientPersonal.setH20PGM("ESS2000");
            msgClientPersonal.setH20TIM(SuperServlet.getTimeStamp());
            msgClientPersonal.setH20SCR("01");
            msgClientPersonal.setH20OPE("0005");
            msgClientPersonal.setE20CUN(req.getParameter("E20CUN"));
            msgClientPersonal.setE20LGT("2");
            Enumeration enu = msgClientPersonal.fieldEnumeration();
            MessageField field = null;
            String value = null;
            while(enu.hasMoreElements()) 
            {
                field = (MessageField)enu.nextElement();
                try
                {
                    value = req.getParameter(field.getTag()).toUpperCase();
                    if(value != null)
                        field.setString(value);
                }
                catch(Exception exception) { }
            }
            msgClientPersonal.send();
            msgClientPersonal.destroy();
            flexLog("ESD008020 Message Sent");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR"))
            {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ESD008020"))
            {
                try
                {
                    msgClientPersonal = new ESD008020Message();
                    flexLog("ESD008020 Message Received");
                }
                catch(Exception ex)
                {
                    flexLog("Error: " + ex);
                }
                msgClientPersonal = (ESD008020Message)newmessage;
                userPO.setCusNum(msgClientPersonal.getE20CUN());
                userPO.setCusName(msgClientPersonal.getE20NA1());
                flexLog("Putting java beans into the session");
                ses.setAttribute("error", msgError);
                ses.setAttribute("cusdata", msgClientPersonal);
                ses.setAttribute("userPO", userPO);
                if(IsNotError)
                    try
                    {
                        flexLog("About to call Page: " + LangPath + "ESS2000_client_short_personal_confirm.jsp");
                        callPage(LangPath + "ESS2000_client_short_personal_confirm.jsp", req, res);
                    }
                    catch(Exception e)
                    {
                        flexLog("Exception calling page " + e);
                    }
                else
                    try
                    {
                        flexLog("About to call Page: " + LangPath + "ESS2000_client_short_personal_basic.jsp");
                        callPage(LangPath + "ESS2000_client_short_personal_basic.jsp", req, res);
                    }
                    catch(Exception e)
                    {
                        flexLog("Exception calling page " + e);
                    }
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
    }

    protected void showERROR(ELEERRMessage m)
    {
        if(SuperServlet.logType != 0)
        {
            flexLog("ERROR received.");
            flexLog("ERROR number:" + m.getERRNUM());
            flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01() + " code : " + m.getERDF01());
            flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02() + " code : " + m.getERDF02());
            flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03() + " code : " + m.getERDF03());
            flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04() + " code : " + m.getERDF04());
            flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05() + " code : " + m.getERDF05());
            flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06() + " code : " + m.getERDF06());
            flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07() + " code : " + m.getERDF07());
            flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08() + " code : " + m.getERDF08());
            flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09() + " code : " + m.getERDF09());
            flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10() + " code : " + m.getERDF10());
        }
    }

    protected void reqUserInquiry(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ESS200001Message msgClientPersonal = null;
        UserPos userPO = null;
        ELEERRMessage msgError = null;
        boolean IsNotError = false;
        msgClientPersonal = new ESS200001Message();
        String CUSID = null;
        String ENTITY = null;
        CUSID = req.getParameter("CUSID");
        ENTITY = req.getParameter("E01EUSENT");
        if(ENTITY==null) ENTITY="";
        try
        {
            msgClientPersonal = (ESS200001Message)mc.getMessageRecord("ESS200001");
            msgClientPersonal.setH01USERID(user.getH01USR());
            msgClientPersonal.setH01PROGRM("ESS2000");
            msgClientPersonal.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgClientPersonal.setH01FLGWK1("I");
            msgClientPersonal.setH01OPECOD("0002");
            msgClientPersonal.setE01EUSUSR(CUSID);
            msgClientPersonal.setE01EUSENT(ENTITY);
            msgClientPersonal.send();
            msgClientPersonal.destroy();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR"))
            {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            try
            {
                msgClientPersonal = new ESS200001Message();
            }
            catch(Exception ex)
            {
                flexLog("Error: " + ex);
            }
            msgClientPersonal = (ESS200001Message)newmessage;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        msgClientPersonal.setE01EUSUSR(CUSID);
        ses.setAttribute("cusdata", msgClientPersonal);
        ses.setAttribute("error", msgError);
        userPO = (UserPos)ses.getAttribute("userPO");
        userPO.setPurpose("INQ");
        ses.setAttribute("userPO", userPO);
        if(IsNotError)
            callPage(LangPath + "ESS2000_User_Inquiry.jsp", req, res);
    }

    protected void procReqPasswordGenerator(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        MessageRecord newmessage = null;
        ESS200001Message msgRT = null;
        UserPos userPO = null;
        boolean IsNotError = false;
        ELEERRMessage msgError = null;
        try
        {
            msgError = new ELEERRMessage();
        }
        catch(Exception ex)
        {
            flexLog("Error: " + ex);
        }
        try
        {
            flexLog("Send Initial Data");
            msgRT = (ESS200001Message)mc.getMessageRecord("ESS200001");
            msgRT.setH01USERID(user.getH01USR());
            msgRT.setH01PROGRM("ESS200001");
            msgRT.setH01TIMSYS(SuperServlet.getTimeStamp());
            msgRT.setH01OPECOD("0099");
            Enumeration enu = msgRT.fieldEnumeration();
            MessageField field = null;
            String value = null;
            while(enu.hasMoreElements()) 
            {
                field = (MessageField)enu.nextElement();
                try
                {
                    value = req.getParameter(field.getTag()).toUpperCase();
                    if(value != null)
                        field.setString(value);
                }
                catch(Exception exception) { }
            }
            mc.sendMessage(msgRT);
            msgRT.destroy();
            flexLog("ESD200001 Message Sent");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        try
        {
            newmessage = mc.receiveMessage();
            if(newmessage.getFormatName().equals("ELEERR"))
            {
                msgError = (ELEERRMessage)newmessage;
                IsNotError = msgError.getERRNUM().equals("0");
                flexLog("IsNotError = " + IsNotError);
                showERROR(msgError);
                ses.setAttribute("error", msgError);
            } else
            {
                flexLog("Message " + newmessage.getFormatName() + " received.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog("Error: " + e);
            throw new RuntimeException("Socket Communication Error");
        }
        if(!IsNotError)
        {
            res.sendRedirect(SuperServlet.srctx + LangPath + "ESS2000_personal_password.generated.jsp");
            return;
        }
        newmessage = mc.receiveMessage();
        msgRT = new ESS200001Message();
        msgRT = (ESS200001Message)newmessage;
        String RUser = msgRT.getE01EUSUSR();
        String RName = msgRT.getE01EUSPAS();
        String RPwd = msgRT.getE01EUSACP();
        javax.servlet.ServletOutputStream out = res.getOutputStream();
        ByteArrayOutputStream baosPDF = null;
        try
        {
            baosPDF = generatePDFDocumentBytes(ses, req, res, RUser, RName, RPwd);
            res.setContentType("application/pdf");
            res.setHeader("Cache-Control", "no-cache");
            javax.servlet.ServletOutputStream sos = res.getOutputStream();
            baosPDF.writeTo(sos);
            sos.flush();
        }
        catch(DocumentException dex)
        {
            res.setContentType("text/html");
            PrintWriter writer = res.getWriter();
            writer.println(getClass().getName() + " caught an exception: " + dex.getClass().getName() + "<br>");
            writer.println("<pre>");
            dex.printStackTrace(writer);
            writer.println("</pre>");
        }
        finally
        {
            if(baosPDF != null)
                baosPDF.reset();
        }
    }

    protected ByteArrayOutputStream generatePDFDocumentBytes(HttpSession session, HttpServletRequest req, HttpServletResponse res, String User, String Nombre, String Clave)
        throws DocumentException
    {
        Document doc = new Document(PageSize.A4, 36F, 36F, 36F, 36F);
        ByteArrayOutputStream baosPDF = new ByteArrayOutputStream();
        PdfWriter docWriter = null;
        try
        {
            docWriter = PdfWriter.getInstance(doc, baosPDF);
            com.lowagie.text.Font normalFont = FontFactory.getFont("Arial", 12F, 0);
            com.lowagie.text.Font normalFont11 = FontFactory.getFont("Arial", 14F, 0);
            com.lowagie.text.Font normalBoldFont = FontFactory.getFont("Arial", 8F, 1);
            com.lowagie.text.Font headerBoldFont = FontFactory.getFont("Arial", 10F, 1);
            com.lowagie.text.Font titleBoldFont = FontFactory.getFont("Arial", 14F, 1, new Color(0, 114, 59));
            com.lowagie.text.Font title1BoldFont = FontFactory.getFont("Arial", 18F, 1, new Color(0, 114, 59));
            com.lowagie.text.Font title0BoldFont = FontFactory.getFont("Arial", 8F, 1, new Color(0, 114, 59));
            com.lowagie.text.Font title2BoldFont = FontFactory.getFont("Arial", 8F, 1);
            com.lowagie.text.Font title3BoldFont = FontFactory.getFont("Arial", 10F, 4);
            Paragraph NOMBRE = new Paragraph("   " + Nombre, normalFont);
            Paragraph CLAVE = new Paragraph(Clave, normalFont11);
            doc.open();
            docWriter.addJavaScript("var pp=this.getPrintParams();", false);
            docWriter.addJavaScript("pp.interactive = pp.constants.interactionLevel.silent;", false);
            String PrinterName = "pp.printerName=\"Epson_clave\";";
            docWriter.addJavaScript(PrinterName, false);
            docWriter.addJavaScript("this.print(pp);", false);
            Table table = new Table(1, 1);
            Cell cell = new Cell("");
            Cell cellB = new Cell("");
            table.setBorderWidth(0.0F);
            table.setCellsFitPage(true);
            table.setPadding(0.0F);
            table.setSpacing(0.0F);
            table.setAlignment(0);
            table.setWidth(80F);
            cellB = new Cell("");
            cellB.setHorizontalAlignment(0);
            cellB.setVerticalAlignment(6);
            cellB.setBorder(0);
            table.addCell(cellB);
            table.addCell(cellB);
            table.addCell(cellB);
            table.addCell(cellB);
            table.addCell(cellB);
            table.addCell(cellB);
            cell = new Cell(NOMBRE);
            cell.setHorizontalAlignment(0);
            cell.setVerticalAlignment(6);
            cell.setBorder(0);
            table.addCell(cell);
            table.addCell(cellB);
            table.addCell(cellB);
            table.addCell(cellB);
            cell = new Cell(CLAVE);
            cell.setHorizontalAlignment(2);
            cell.setVerticalAlignment(4);
            cell.setBorder(0);
            table.addCell(cell);
            doc.add(table);
        }
        catch(DocumentException dex)
        {
            baosPDF.reset();
            throw dex;
        }
        finally
        {
            if(doc != null)
                doc.close();
            if(docWriter != null)
                docWriter.close();
        }
        if(baosPDF.size() < 1)
            throw new DocumentException("document has " + baosPDF.size() + " bytes");
        else
            return baosPDF;
    }


}
