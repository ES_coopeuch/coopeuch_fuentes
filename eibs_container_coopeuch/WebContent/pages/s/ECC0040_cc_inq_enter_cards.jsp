<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Consulta Tarjetas de Creditos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<SCRIPT Language="javascript">
function CheckIDN() {

	var Type = getElementChecked("search").value;
	
	if (Type == "I") {
		var rut = validateRut(document.forms[0].E01CCRCID.value, <%=currUser.getE01INT()%>);
		if (rut == 0) {
			alert("Debe ser Identificaci�n Valida");
			document.forms[0].E01CCRCID.focus();
		} else {
			document.forms[0].submit();
		}
	} else {
		if (document.forms[0].E01CCRNUM.value=="" || !isNumeric(document.forms[0].E01CCRNUM.value)) {
			alert("Debe ingresar un n�mero de tarjeta v�lido");
			document.forms[0].E01CCRNUM.value='';
			document.forms[0].E01CCRNUM.focus();
		} else {
			document.forms[0].submit();
		} 
	}

}

</SCRIPT>


</head>


<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<body bgcolor="#FFFFFF">

<H3 align="center">Consulta Tarjetas	
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cc_inq_enter_cards, ECC0040"></H3>

<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0040">
  <p> 
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">
  </p>

  <table class="tbenter" HEIGHT="25%" width="100%" border="0">
    <tr> 
      <td nowrap ALIGN="right">
      	<input type="radio" name="search" value="I" onclick="document.forms[0].E01CCRCID.focus()" checked>
      		Identificaci&oacute;n : </td>
      <td nowrap ALIGN="left"> 
        <INPUT type="text" name="E01CCRCID" size="25" maxlength="25" onclick="document.forms[0].search[0].click()" >
         <a href="javascript: GetCustomerDescId('', '', 'E01CCRCID')"> 
         <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda"	border="0"></a>      
       </td>
     </tr>
    <tr> 
      <td nowrap ALIGN="right">
		<input type="radio" name="search" value="C" onclick="document.forms[0].E01CCRNUM.focus()">N&uacute;mero de Tarjeta : </td>
      <td nowrap ALIGN="left"> 
        <INPUT type="text" name="E01CCRNUM" size="24" maxlength="19" onclick="document.forms[0].search[1].click()" onkeypress="enterInteger()">
      </td>
    </tr>       
  </table>  
 <p align="center"> 
    <input id="EIBSBTN" type=button name="Submit" value="Enviar" onClick="CheckIDN()">
  </p>  
<script language="JavaScript">
  document.forms[0].E01CCRCID.focus();
  document.forms[0].E01CCRCID.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
  <%
 }
%> 
</form>
</body>
</html>

