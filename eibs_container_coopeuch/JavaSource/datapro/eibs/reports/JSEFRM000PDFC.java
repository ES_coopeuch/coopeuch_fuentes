package datapro.eibs.reports;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.PropertyResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adobe.fdf.FDFDoc;

import datapro.eibs.beans.EFRM00001Message;
import datapro.eibs.beans.EFRM08501Message;
import datapro.eibs.beans.EFRM08601Message;
import datapro.eibs.beans.EFRM08701Message;
import datapro.eibs.beans.EFRM08801Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageRecord;

public class JSEFRM000PDFC extends JSEFRM000PDF {

	private static final long serialVersionUID = 595535202818698946L;
	
	protected void procReqFormList(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws IOException, ServletException {
		
		UserPos userPO = getUserPos(session);			
		float opecode = Util.parseFloat(req.getParameter("OPE_CODE"));
			
		MessageProcessor mp = null;
 
		MessageRecord newmessage = null;		
		try {
		
			switch ((int) opecode) {
				case 85 :
					mp = getMessageProcessor("EFRM085", req);
					EFRM08501Message msg = (EFRM08501Message) mp.getMessageRecord("EFRM08501");
					msg.setH85USERID(user.getH01USR());
					msg.setH85PROGRM("EFRM085");
					msg.setH85TIMSYS(getTimeStamp());
					try {
						msg.setE85COSECU(req.getParameter("CUST_NUMBER").trim());//CLIENTE EMPLEADOR
					} catch (Exception e) {
					}
					try {
						msg.setE85COSNUM(req.getParameter("REQ_NUMBER").trim());//NUMERO SOLICITUD									
					} catch (Exception e) {
					}
					mp.sendMessage(msg);		
					break;
				case 86 :
					mp = getMessageProcessor("EFRM086", req);
					EFRM08601Message msg1 = (EFRM08601Message) mp.getMessageRecord("EFRM08601");
					msg1.setH86USERID(user.getH01USR());
					msg1.setH86PROGRM("EFRM086");
					msg1.setH86TIMSYS(getTimeStamp());
					try {
						msg1.setE86ENTCUN(userPO.getHeader2().trim());//Numero Cliente.
						msg1.setE86FILLE1(userPO.getAccNum().trim());//Numero Cliente.
					} catch (Exception e) {
					}
					mp.sendMessage(msg1);		
					break;			
				case 87 :
					mp = getMessageProcessor("EFRM087", req);
					EFRM08701Message msg2 = (EFRM08701Message) mp.getMessageRecord("EFRM08701");
					msg2.setH87USERID(user.getH01USR());
					msg2.setH87PROGRM("EFRM087");
					msg2.setH87TIMSYS(getTimeStamp());
					try {
						msg2.setE87ENTCUN(req.getParameter("CUST_NUMBER").trim());//Numero Cliente userPO.getCusNum()
					} catch (Exception e) {
					}
					try {
						msg2.setE87PACNUM(req.getParameter("REQ_NUMBER").trim());//NUMERO SEGURO
					} catch (Exception e) {
					}
					try {
						msg2.setE87PACCOD(req.getParameter("REQ_CODSEG").trim());//Codigo de seguro
					} catch (Exception e) {
					}					
					mp.sendMessage(msg2);		
					break;
				case 88 :
					mp = getMessageProcessor("EFRM088", req);
					EFRM08801Message msg3 = (EFRM08801Message) mp.getMessageRecord("EFRM08801");
					msg3.setH88USERID(user.getH01USR());
					msg3.setH88PROGRM("EFRM088");
					msg3.setH88OPECOD("0001");
					msg3.setH88TIMSYS(getTimeStamp());
					try {
						msg3.setE88REQCUN(req.getParameter("CUST_NUMBER").trim());////Numero Cliente.
					} catch (Exception e) {
					}
					mp.sendMessage(msg3);		
					break;						
				default :
					session.setAttribute("error_msg", "Reporte no Implementado");			 			
					forward("EFRM000_pdf_forms_error.jsp", req, res);
					return;					
			}
		
			newmessage = mp.receiveMessageRecord();
			//redirecting
			if (mp.hasError(newmessage)) {
				String error = ((ELEERRMessage) newmessage).getERNU01() + " - " + ((ELEERRMessage) newmessage).getERDS01();
				session.setAttribute("error_msg", error);
				forward("EFRM000_pdf_forms_error.jsp", req, res);
				
			} else {			

				FDFDoc outputFDF = new FDFDoc();
				if (session.getAttribute("pdfData") != null) {
					session.removeAttribute("pdfData");
				}
				if (dstXML == null || dstXML.isEmpty()) {
					initDataStructure();			
					flexLog("XML file was reloaded.");
				}
				// Add General Information like User ID....
				getFormData(outputFDF, (String)formatNames.get("ESS0030DS") + ".", user);
				
				//Add Especific Information..
        		buildFormList(outputFDF, newmessage, ".", 0);

        		//find report to shows
        		PropertyResourceBundle reportsProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle("ReportsCoopeuch");        		
        		String pdfName = reportsProperties.getString("pdfName" + (int)opecode);
        		if (pdfName == null || "".equals(pdfName)) {
					session.setAttribute("error_msg", "Reporte no Implementado");			 			
					forward("EFRM000_pdf_forms_error.jsp", req, res);
    			} else {
        			String[] pdfNameA = pdfName.split(";");
        			
        			//fill the list
        			JBObjList list = new JBObjList();
        			for (int i = 0; i < pdfNameA.length; i++) {
        				pdfNameA[i] = pdfNameA[i].trim();//quitar space
        				EFRM00001Message msgHeader = new EFRM00001Message();
        				msgHeader.setE01APFPTH(pdfNameA[i]);//name PDF
        				msgHeader.setE01APFOPE("1");//Operacion, default 1
        				msgHeader.setE01APFCPI("1");//Number copies, default 1
        				msgHeader.setE01APFDDS("");//Printer Name, default space    				
        		     	msgHeader.setE01APFDSC(pdfNameA[i].length()>35?pdfNameA[i].substring(0,35):pdfNameA[i]);////Nombre PDF    		     	
        				
        		     	if(opecode==86 && i==2) //PREGUNTA ESPECIFICAMENTE POR FORMULARIO DE MODIFICACION DE COTIZACIONES
        		     	{
        		     		EFRM08601Message msg1 = (EFRM08601Message)  newmessage;
        		     		msgHeader.setH01FLGWK1(msg1.getH86FLGWK1());
        		     	}
        		     	
        		     	list.addRow(msgHeader);
    				}    			        		        		        		
        			

    				flexLog("Putting java beans into the session");
    				session.setAttribute("pdfData", outputFDF);							
    				session.setAttribute("pdfList", list);
    				
    				res.setContentType("text/html");
    				PrintWriter out = res.getWriter();
    				String href = req.getContextPath() + "/pages/s/MISC_search_wait.jsp?URL='" + req.getContextPath() + LangPath + "EFRM000C_pdf_list.jsp'";
    				out.println("<HTML>");
    				out.println("<HEAD>");
    				out.println("<TITLE>pdf</TITLE>");
    				out.println("</HEAD>");
    				out.println("<BODY>");
    				out.println("<SCRIPT LANGUAGE=\"JavaScript\">");
    				out.println("		top.location.href = \""+ href + "\";");
     				out.println("</SCRIPT>");
    				out.println("<P>PDF</P>");
    				out.println("</BODY>");
    				out.println("</HTML>");					
    			}
        		   
		 	}
			
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
		
	
}
