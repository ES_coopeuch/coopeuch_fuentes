<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Definici&oacute;n de Descuentos - Ingreso</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="RTDisTab" class="datapro.eibs.beans.EDD232004Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

</head>

<BODY>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="JavaScript">
function cancel() {
	document.forms[0].SCREEN.value = 5100;
	document.forms[0].submit();
}

</SCRIPT>  

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
    String readonly = "NEW".equals(userPO.getPurpose()) ? "" : "readonly";     
%>



<h3 align="center">Nueva Relaci&oacute;n Descuento - Tabla<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="discounts_tabla_maitenance, EDD2320"></h3>
<hr size="4">
<form name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2320" >
    <input type=HIDDEN name="SCREEN" value="5600">

  <h4>Datos Descuento</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Descuento :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E04CMRDCDC" size="5" maxlength="4" value="<%= RTDisTab.getE04CMRDCDC().trim()%>"  readonly>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Descripci&oacute;n  :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E04CMRDGDC" size="31" maxlength="30" value="<%= RTDisTab.getE04CMRDGDC().trim()%>" readonly >
              </div>
            </td>
          </tr>
 		</table>
  	</td>
   </tr>
  </table>

  <h4>Datos Tabla</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">Codigo de Tabla :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E04CMRDTDC" size="7" maxlength="6" value="<%= RTDisTab.getE04CMRDTDC().trim()%>" readonly>
                <input type="text" name="E04CMRDTDG" size="20" maxlength="21" value="<%= RTDisTab.getE04CMRDTDG().trim()%>" readonly>
				<a href="javascript:GetCodeDescFILES('E04CMRDTDC','E04CMRDTDG','CMTCA')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right"></div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
              </div>
            </td>
          </tr>
  </table>

  
    <div align="center">
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    <input id="EIBSBTN" type="button" name="Cancel" value="Cancelar" onclick="cancel()">
  </div>
  
</form>

</body>
</html>
