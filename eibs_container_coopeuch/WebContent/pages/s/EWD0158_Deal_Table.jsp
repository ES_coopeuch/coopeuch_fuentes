<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Pragma" CONTENT="No-cache">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<TITLE>DEAL TABLE Help</TITLE>
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<%@ page import = "java.io.*,java.net.*,datapro.eibs.sockets.*,datapro.eibs.beans.*,datapro.eibs.master.*,java.math.*" %>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT language="JavaScript">
	setTimeout("top.close()", <%= datapro.eibs.master.JSEIBSProp.getPopUpTimeOut() %>)
</SCRIPT>
<script language="javascript">

//<!-- Hide from old browsers
function a(code1, desc1) {
	var form = top.opener.document.forms[0];

	form[top.opener.fieldName].value = code1;
  	if(top.opener.fieldDesc != ""){
		try{
			form[top.opener.fieldDesc].value = desc1;
		} catch (e){
		}
	}
  	   
	top.close();
 }

function goSearch() {
  window.location.href="<%=request.getContextPath()%>/pages/s/EWD0158_Deal_Table.jsp?codeflag=" + document.forms[0].codFlag.value + "&FromRecord=0&SelNew=" + document.forms[0].SelNew.value + "&SelOld=" + document.forms[0].SelOld.value; 
}

//-->
</script>
</HEAD>
<BODY>
<form>
<%
    String codeflag = request.getParameter("codeflag");
    codeflag=(codeflag==null)?"":codeflag;
    String selNew = request.getParameter("SelNew");
    selNew=(selNew==null)?"":selNew;
    
	MessageProcessor mp = null;
	int rows = 0;

	try {
		mp = new MessageProcessor("EWD0158");

		MessageRecord newmessage = null;
		EWD0158DSMessage msgHelp = null;
               
		// Send Request
		msgHelp = (EWD0158DSMessage)mp.getMessageRecord("EWD0158DS");
        //msgHelp.setEWDTBL(codeflag);	
		msgHelp.setEWDSHN(selNew);        	
		msgHelp.send();	
		msgHelp.destroy();

		// Receive Help
                 	  // newmessage = mc.receiveMessage();
                 	  
                 	  // if (newmessage.getFormatName().equals("EWD0158DS")) {
                      //  msgHelp =  (EWD0158DSMessage)newmessage;
                      //  out.println("<h4>Tabla de Convenios</h4>");
			%>
			
<INPUT TYPE=HIDDEN NAME="totalRow" VALUE="0">
<INPUT TYPE=HIDDEN NAME="SelOld" VALUE="">
<INPUT TYPE=HIDDEN NAME="codFlag" VALUE="">		
	
			  <h4>Tabla de Convenios</h4>
			  <table id="TBHELP">
				<tr>
				<td nowrap><b>Busqueda Rapida : </b></td>
			  	<td nowrap>
					<input type="text" name="SelNew"  size=20 maxlength=20 value="<%=selNew%>">
        			&nbsp;&nbsp;<a href="javascript:goSearch();"><img src="<%=request.getContextPath()%>/images/search1.gif" align="absbottom" border="0" ></a> 
      			</td>
    		  </tr>
			</table>			  
			  <TABLE  id="mainTable" class="tableinfo" ALIGN=CENTER style="width:'95%'">
 			   <TR> 
    			     <TD NOWRAP width="100%" >
  				<TABLE id="headTable" >
  				   <TR id="trdark">  
      					<TH ALIGN=CENTER NOWRAP>C�digo</TH>
      					<TH ALIGN=CENTER NOWRAP>Descripci�n</TH>
      					<TH ALIGN=CENTER NOWRAP>Tipo</TH>
          			   </TR>
       			</TABLE>
  
   			    <div id="dataDiv1" class="scbarcolor">
    				<table id="dataTable" > 
			<% 
                 	//}
                  	
		int ct = 0;
		while (ct++ < datapro.eibs.master.JSEIBSProp.getMaxIterations()) {
			if (ct == datapro.eibs.master.JSEIBSProp.getMaxIterations()) {
				System.out.println("MAX_ITERATION_REACHED_ERROR class:" + this.getClass().getName());
			}
			newmessage = mp.receiveMessageRecord();
			if (newmessage.getFormatName().equals("EWD0158DS")) {
				msgHelp =  (EWD0158DSMessage)newmessage;
				if ( msgHelp.getEWDOPE().equals("*") ) {
					break;
				}
				String myCode = null;
				String myDesc = null;
				String myType = null;
				myCode = msgHelp.getEWDCDE().trim();
				myDesc = msgHelp.getEWDDES().trim();
				myType = msgHelp.getEWDCTY().trim();
				out.println("<tr>");
				out.println("<td nowrap >" + myCode + "</td>");
				out.println("<td nowrap><NOBR><A HREF=\"javascript:a('" + myCode +"', '" + myDesc + "')\">"  + myDesc + "</a></td>");
				out.println("<td nowrap><NOBR><A HREF=\"javascript:a('" + myCode +"', '" + myDesc + "')\">"  + myType + "</a></td>");
				out.println("</tr>");
				rows++;
			} else {
				out.println("Message " + newmessage.getFormatName() + " received.");
				break;
			}
		}
			%>
			 </table>
   			</div>
   			</TD>
   		      </TR>	
		    </TABLE>

		    <SCRIPT language="JavaScript">
			  document.forms[0].totalRow.value="<%= rows %>";
			  divResize();
     		  adjustEquTables(headTable, dataTable, dataDiv1,1,false);        
		    </SCRIPT>
                    <%
	} catch (Exception e) {
		out.print("Error : " + e);
	} finally {
		mp.close();
	}
%>
</form>
</BODY>
</HTML>
