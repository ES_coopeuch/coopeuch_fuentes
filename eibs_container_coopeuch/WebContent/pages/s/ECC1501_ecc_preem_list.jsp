<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page
	import="datapro.eibs.master.Util,datapro.eibs.beans.ECC150101Message"%>
<%@page import="com.datapro.constants.EibsFields"%>

<html>
<head>
<title>Gesti�n de Preemisi�n de Tarjeta de Cr�dito</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="PreemHeader" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"   scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"    	   scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

$(function(){
$("#radio_key").attr("checked", false);
});

function goAction(op) {
var ok = false;
	
		for(n=0; n<document.forms[0].elements.length; n++)
		{
			var element = document.forms[0].elements[n];
	   	 if(element.name == "ID_Interfaz") 
	   	 {	
	   	 	if (element.checked == true) 
	   	 	{
	   	   		document.getElementById("codigo_lista").value = element.value; 
       	 		ok = true;
       	 		break;
			}
	 	 }
		}
      
    	if ( ok ) 
    	{
			var confirm1 = true;
      	
     	 	if (op =='1500')
      		{
      			confirm1 = confirm("�Desea Rechazar la cartola seleccionada?");
      		
				if (confirm1){
					document.forms[0].SCREEN.value = op;
					document.forms[0].submit();		
				}
			}
 			else
			{
				document.forms[0].SCREEN.value = op;
				document.forms[0].submit();		
			} 
     	} 
     	else 
     	{
			alert("Debe seleccionar un registro para continuar.");	   
	 	}    
}

</SCRIPT>

</head>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>

<h3 align="center">
		<%if(request.getAttribute("opt").equals("01"))
			out.print("Gesti�n de Preemisi�n de Tarjeta de Cr�dito");
		  else if(request.getAttribute("opt").equals("02"))
			out.print("Autorizaci�n de Preemisi�n de Tarjeta de Cr�dito");
		  else 
			out.print("Consulta de Preemisi�n de Tarjeta de Cr�dito");
		%>
	<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ECC1501_ECC_Preem_list.jsp,ECC1501">
</h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC1501">
<input type="hidden" name="SCREEN" value=""> 
<input type="hidden" name="codigo_lista" value="" id="codigo_lista"> 
<INPUT TYPE=HIDDEN NAME="opt" VALUE="<%=request.getAttribute("opt")%>"> 
<input type="hidden" name="E01CCHSTS" value="<%=request.getAttribute("E01CCHSTS")%>">

<input type="hidden" name="E01CCHFDD" value="<%=request.getAttribute("E01CCHFDD")%>"> 
<input type="hidden" name="E01CCHFDM" value="<%=request.getAttribute("E01CCHFDM")%>"> 
<input type="hidden" name="E01CCHFDY" value="<%=request.getAttribute("E01CCHFDY")%>"> 
<input type="hidden" name="E01CCHFHD" value="<%=request.getAttribute("E01CCHFHD")%>"> 
<input type="hidden" name="E01CCHFHM" value="<%=request.getAttribute("E01CCHFHM")%>"> 
<input type="hidden" name="E01CCHFHY" value="<%=request.getAttribute("E01CCHFHY")%>"> 
<input type="hidden" name="E01CCHIDC" value="<%=request.getAttribute("E01CCHIDC")%>">
<input type="hidden" name="E01CCHREC" value="<%=request.getAttribute("E01CCHREC")%>">

<h3 align="center">FILTROS DE LA CONSULTA</h3>
<table class="tableinfo" width="100%">
	<tr bordercolor="#FFFFFF">
		<td id="trdark">
		<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
			<!--  tr>
				<td nowrap width="10%" colspan="6">
				<h3 align="center">FILTROS DE LA CONSULTA</h3>
				</td>
			</tr -->

			<tr>
				<td nowrap width="10%" align="right">ID Interfaz :</td>
				<td nowrap width="20%" align="left"><input type="text"
					name="E01CCHIDC" size="15" maxlength="14"
					value="<%out.print(session.getAttribute("E01CCHIDC"));%>" readonly>
				</td>

				<td nowrap width="15%" align="right">Fecha Carga Desde :</td>
				<td nowrap width="20%" align="left"><input type="text"
					name="E01fecha" size="12"
					value="<%out.print(session.getAttribute("fecha_inicial"));%>"
					readonly></td>

				<td nowrap width="15%" align="right">Fecha Carga Hasta :</td>
				<td nowrap width="20%" align="left"><input type="text"
					name="E01fecha2" size="12"
					value="<%out.print(session.getAttribute("fecha_final"));%>"
					readonly></td>
			</tr>

			<tr>
				<td nowrap width="10%" align="right">Estado :</td>
				<td nowrap width="20%" align="left"><input type="text"
					name="Estado" size="23" maxlength="20"
					value="<%if (request.getAttribute("E01CCHSTS").equals("T"))
				out.print("TODOS");
			else if (request.getAttribute("E01CCHSTS").equals("C"))
				out.print("CARGADO");
			else if (request.getAttribute("E01CCHSTS").equals("V"))
				out.print("VALIDADO");
			else if (request.getAttribute("E01CCHSTS").equals("A"))
				out.print("AUTORIZADO");
			else if (request.getAttribute("E01CCHSTS").equals("E"))
				out.print("ENVIADO");
			else if (request.getAttribute("E01CCHSTS").equals("R"))
				out.print("RECHAZADO");
			else
				out.print(" ");%>"
					readonly></td>
				<td nowrap colspan="4"></td>


			</tr>
		</table>
		</td>
	</tr>
</table>




<%
	if (PreemHeader.getNoResult()) {
%>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center"><font size="3"> <b> No hay
		resultados que correspondan a su criterio de b�squeda. </b> </font></div>
		</td>
	</tr>
</table>
<%
	} else {
%>

<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%">
			<a href="javascript:goAction('1300')"><b>Consultar</b></a></td>
		
		
		<%if (request.getAttribute("opt").equals("01")) {%>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('1400')"> <b>Validar</b></a> 
		<%} else if (request.getAttribute("opt").equals("02")) { %>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('1800')"> <b>Autorizar</b></a> 
		<%} else {%>
		<td align="center" width="10%"> 
		<%}%>
		</td>

		<% if (!request.getAttribute("opt").equals("03")) {%> 
		<td align="center" class="tdbkg" width="10%">
			<a href="javascript:goAction('1500')"><b>Rechazar</b></a> 
		<%} else if (!request.getAttribute("opt").equals("01"))  {%>
		<td align="center" class="tdbkg" width="10%">
			<a href="javascript:goAction('1900')"><b>Exportar Excel</b></a> 
		<%} else  {%>
		<td align="center" width="10%">
		<%} %>
		</td>
	</tr>
</table>


<table class="tbenter" width=100%>
	<tr>
		<td height="20"></td>
	</tr>

	<tr>
		<td>
		<table id="headTable" width="100%" align="left">
			<tr id="trdark">
				<th align="center" nowrap width="30"></th>
				<th align="center" nowrap width="100">ID</th>
				<th align="center" nowrap width="100">Fecha Carga</th>
				<th align="center" nowrap width="120">Reg</th>
				<th align="center" nowrap width="120">Reg Error</th>
				<th align="center" nowrap width="150">Estado</th>
				<th align="center" nowrap width="100">Fecha Estado</th>
				<th align="center" nowrap width="150">Usuario</th>
				<th align="center" nowrap width=""></th>
			</tr>

			<%
				PreemHeader.initRow();
					int k = 0;
					boolean firstTime = true;
					String chk = "";
					while (PreemHeader.getNextRow()) {

						ECC150101Message pvprd = (ECC150101Message) PreemHeader
								.getRecord();
			%>
			<tr>
				<td nowrap>
					<input type="radio" name="ID_Interfaz"	id="codigo_lista" value="<%=PreemHeader.getCurrentRow()%>" <%=chk%> />
				</td>
				<td nowrap align="center"><%=pvprd.getE01CCHIDC()%></td>
				<td nowrap align="center">
				<%
					out.print(pvprd.getE01CCHCAD() + "/" + pvprd.getE01CCHCAM()
									+ "/" + pvprd.getE01CCHCAY());
				%>
				</td>
				<td nowrap align="center"><%=pvprd.getE01CCHCAR()%></td>
				<td nowrap align="center"><%=pvprd.getE01CCHVAR()%></td>
				<td nowrap align="center"><%=pvprd.getE01CCHGST()%></td>
				<td nowrap align="center">
				<%
					out.print(pvprd.getE01CCHCAD() + "/" + pvprd.getE01CCHCAM()
									+ "/" + pvprd.getE01CCHCAY());
				%>
				</td>
				<td nowrap align="left"><%=pvprd.getE01CCHUPU()%></td>
				<td nowrap align="center"></td>
			</tr>
			<%
				}
			%>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table class="tbenter" width="98%" align="center">
			<tr>
				<td width="40%" align="left">
				<%
					if (PreemHeader.getShowPrev()) {
							int pos = PreemHeader.getFirstRec() - 10;

							out
									.println("<A HREF=\""
											+ request.getContextPath()
											+ "/servlet/datapro.eibs.products.JSECC1501?SCREEN=1200&posicion="
											+ pos + "&mark=atr�s" + "&E01CCHSTS="
											+ request.getAttribute("E01CCHSTS")
											+ "&E01CCHFDD="
											+ request.getAttribute("E01CCHFDD")
											+ "&E01CCHFDM="
											+ request.getAttribute("E01CCHFDM")
											+ "&E01CCHFDY="
											+ request.getParameter("E01CCHFDY")
											+ "&E01CCHFHD="
											+ request.getAttribute("E01CCHFHD")
											+ "&E01CCHFHM="
											+ request.getAttribute("E01CCHFHM")
											+ "&E01CCHFHY="
											+ request.getAttribute("E01CCHFHY")
											+ "&E01CCHIDC="
											+ request.getAttribute("E01CCHIDC")
											+ "&opt=" + request.getAttribute("opt")

											+ "\"><IMG border=\"0\" src=\""
											+ request.getContextPath()
											+ "/images/s/previous_records.gif\" ></A>");
						}
				%>
				</td>
				<td width="20%" align="center"></td>

				<td width="40%" align="right">
				<%
					if (PreemHeader.getShowNext()) {
							int pos = PreemHeader.getLastRec();
							out
									.println("<A HREF=\""
											+ request.getContextPath()
											+ "/servlet/datapro.eibs.products.JSECC1501?SCREEN=1200&posicion="
											+ pos + "&mark=adelante" + "&E01CCHSTS="
											+ request.getAttribute("E01CCHSTS")
											+ "&E01CCHFDD="
											+ request.getAttribute("E01CCHFDD")
											+ "&E01CCHFDM="
											+ request.getAttribute("E01CCHFDM")
											+ "&E01CCHFDY="
											+ request.getParameter("E01CCHFDY")
											+ "&E01CCHFHD="
											+ request.getAttribute("E01CCHFHD")
											+ "&E01CCHFHM="
											+ request.getAttribute("E01CCHFHM")
											+ "&E01CCHFHY="
											+ request.getAttribute("E01CCHFHY")
											+ "&E01CCHIDC="
											+ request.getAttribute("E01CCHIDC")
											+ "&opt=" + request.getAttribute("opt")

											+ "\"><IMG border=\"0\" src=\""
											+ request.getContextPath()
											+ "/images/s/next_records.gif\" ></A>");
						}
				%>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<%
	}
%> 

	<SCRIPT language="JavaScript">
		showChecked("CURRCODE");
		
			function resizeDoc() {
      		 	divResize();
     		    adjustEquTables(headTable, dataTable, dataDiv1,1,false);
      		}
	 		resizeDoc();   			
     		window.onresize=resizeDoc;        
     </SCRIPT>
     </form>
</body>
</html>
