/*
 * Created on Jun 12, 2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package datapro.eibs.approval;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDC014001Message;
import datapro.eibs.beans.EDC014002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageContextHandler;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 * Comercial Letter Of Credit Maintenance
 */
public class JSEDC0140 extends SuperServlet {

	String LangPath = "s/";
	
	public JSEDC0140() {
		super();
	}
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = (HttpSession) req.getSession(false);
		if (session == null) {
			try {
				res.setContentType("text/html");
				super.printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {
			int screen = -1;

			ESS0030DSMessage user = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");
			// Here we should get the path from the user profile
			LangPath = rootPath + user.getE01LAN() + "/";
			
			MessageContext mc = null;
			try {
				mc = new MessageContext(super.getMessageHandler("EDC0140", req));
						
				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
					flexLog("Screen  Number: " + screen);
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}
				
				String PageToCall = "";

				switch (screen) {
					case 5 :
						procReqApprovalList(mc, user, req, res, screen);
						break;
					case 7 :
						procInquiryPayment(mc, user, req, res, screen);
						break;
					case 9 :
						procPaymentRejectApproval(mc, user, req, res, screen);
						break;
					default :
						PageToCall = "MISC_not_available.jsp";
						callPage(PageToCall, req, res);
						break;
				}

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				res.sendRedirect(srctx + LangPath + sckNotRespondPage);
			} finally {
				if (mc != null) mc.close();
				flexLog("Socket used by JSEDC0140 closed.");
			}
		}	
	}
	
	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void procPaymentRejectApproval(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		String PageToCall = "EDC0140_payments_list.jsp";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			EDC014001Message msg01 = (EDC014001Message) msgHandle.initMessage("EDC014001", user.getH01USR(), "0009");
			int pos = req.getParameter("ITEM").indexOf('_');
			int len = req.getParameter("ITEM").length();
			String acc = req.getParameter("ITEM").substring(0, pos);
			String num = req.getParameter("ITEM").substring(pos + 1, len);
			msg01.setE01DCMACC(acc);
			msg01.setE01PMTNUM(num);
			msg01.setH01FLGWK1(req.getParameter("E02ACTION"));
			msgHandle.sendMessage(msg01);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			boolean isNotError = msgError.getERRNUM().equals("0");
			if (isNotError) {
				procReqApprovalList(mc, user, req, res, screen);	
			} else {
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				session.setAttribute("msg01", msg01);
				callPage(PageToCall, req, res);
			}
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void procInquiryPayment(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		String PageToCall = "EDC0140_coll_payment_inquiry.jsp";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			EDC014001Message msg01 = (EDC014001Message) msgHandle.initMessage("EDC014001", user.getH01USR(), "0002");
			msg01.setE01DCMACC(req.getParameter("E01DCMACC") == null ? "" : req.getParameter("E01DCMACC").trim());
			msg01.setE01PMTNUM(req.getParameter("E01PMTNUM") == null ? "" : req.getParameter("E01PMTNUM").trim());
			msg01.setE01DCMACD(req.getParameter("E01DCMACD") == null ? "" : req.getParameter("E01DCMACD").trim());
			userPO.setType(msg01.getE01DCMACD());
			msgHandle.sendMessage(msg01);
			ELEERRMessage msgError = msgHandle.receiveErrorMessage();
			boolean isNotError = msgError.getERRNUM().equals("0");
			if (isNotError) {
				//Receive General Data
				msg01 = (EDC014001Message) msgHandle.receiveMessage("EDC014001");
				session.setAttribute("msg01", msg01);
				//Receive Transaction Data
				EDC014002Message msg02 = (EDC014002Message) msgHandle.receiveMessage("EDC014002");
				session.setAttribute("msg02", msg02);
			}		
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			callPage(PageToCall, req, res);
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	/**
	 * @param req
	 * @param res
	 * @param screen
	 * @throws ServletException 
	 */
	private void procReqApprovalList(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, int screen) throws IOException, ServletException {
		HttpSession session = (HttpSession) req.getSession(false);
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		String PageToCall = "EDC0140_payments_list.jsp";
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			EDC014001Message msg01 = (EDC014001Message) msgHandle.initMessage("EDC014001", user.getH01USR(), "0001");
			msgHandle.sendMessage(msg01);
			JBObjList jbList = msgHandle.receiveMessageList("H01FLGMAS", JSEIBSProp.getMaxIterations());
			session.setAttribute("userPO", userPO);
			session.setAttribute("appList", jbList);
			callPage(PageToCall, req, res);
		
		} catch (Exception e) {
			throw new ServletException(e.getClass().getName() + " --> " + e.getMessage());
		}
	}

	public void callPage(String page, HttpServletRequest req, HttpServletResponse res) {
		try {
			super.callPage(LangPath + page, req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e.toString() + e.getMessage());
		}
		return; 
	}
	
}
