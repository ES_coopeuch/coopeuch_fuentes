<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV121601Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV121601List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script type="text/javascript">

  function goAction(op) {
	var ok = false;
	var cun = "";
	var pg = "";

	if (op != '200'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "TPRTPR") 
	      	{	
	      		if (element.checked == true) {
	      			document.getElementById("E01TPRTPR").value = element.value; 
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = true;
      	
      	if (op =='202'){
      		confirm1 = confirm("Desea Eliminar este Registro Seleccionado?");
      	}
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		

     } else {
		alert("Debe seleccionar un registro para continuar.");	   
	 }    
	}

</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Tipos de Evaluacion Plataforma de Venta<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PRDRO_list.jsp,EPV1216"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1216">
<input type="hidden" name="SCREEN" value="201">
<input type="hidden" name="E01TPRTPR" value="" id="E01TPRTPR">  

 
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="20%"> 
			<a href="javascript:goAction('200')"><b>Crear</b></a>
		</td>
		<td align="center" class="tdbkg" width="20%">
			<a href="javascript:goAction('201')"> <b>Modificar</b> </a>
		</td>
		<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('202')"> <b>Borrar</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>

<%
	if (EPV121601List.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>

	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="5%"></th>
			<th align="center" nowrap width="10%">Codigo</th>
			<th align="center" nowrap width="30%">Descripci�n</th>
			<th align="center" nowrap width="15%">Producto <br> Corto Plazo</th>
			<th align="center" nowrap width="15%">Producto <br> Largo Plazo</th>
    		<th align="center" nowrap width="10%">Flag Garantia</th>				
		</tr>
		<%
			EPV121601List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EPV121601List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EPV121601Message pvprd = (EPV121601Message) EPV121601List
							.getRecord();
		%>
		<tr>
			<td nowrap><input type="radio" name="TPRTPR"	value="<%=pvprd.getE01TPRTPR() %>" <%=chk%>/></td>
			<td nowrap align="center"><a href="javascript:goAction('203');"><%=Util.formatCell(pvprd.getE01TPRTPR()) %></a></td>
			<td nowrap align="center"><a href="javascript:goAction('203');"><%=pvprd.getE01TPRDES()%></a></td>																			   																		   
			<td nowrap align="center"><a href="javascript:goAction('203');"><%=pvprd.getE01TPRPRC()%></a></td>	
			<td nowrap align="center"><a href="javascript:goAction('203');"><%=pvprd.getE01TPRPRL()%></a></td>			
			<td nowrap align="center"><a href="javascript:goAction('203');"><% if (pvprd.getE01TPRFLG().equals("N")) out.print("Sin Garantia"); 
																			   if (pvprd.getE01TPRFLG().equals("Y")) out.print("Con Garantia");
																			   if (pvprd.getE01TPRFLG().equals("V")) out.print("Variable");%></a></td> 
		</tr>
		<%
			}
		%>
	</table>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV121601List.getShowPrev()) {
					int pos = EPV121601List.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1216?SCREEN=100&codNum="
									+ request.getAttribute("codNum")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV121601List.getShowNext()) {
					int pos = EPV121601List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1216?SCREEN=100&codNum="
									+ request.getAttribute("codNum")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>
</form>
</body>
</html>

