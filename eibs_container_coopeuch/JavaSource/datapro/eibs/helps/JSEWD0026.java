package datapro.eibs.helps;

/**
 * This type was created by Orestes Garcia.
 */
import java.io.*;
import java.net.*;
import java.beans.Beans;
import javax.servlet.*;
import javax.servlet.http.*;
import datapro.eibs.beans.*;

import datapro.eibs.master.Util;

import datapro.eibs.sockets.*;

public class JSEWD0026 extends datapro.eibs.master.SuperServlet {

	/**
	 * Insert the method's description here.
	 * Creation date: (1/14/00 12:29:44 PM)
	 */
	public JSEWD0026() {
		super();
	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

		flexLog("free resources used by JSESS0040");

	}
	/**
	 * This method was created by Orestes Garcia.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}
	/**
	 * This method was created by Eric Luis.
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 */
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		MessageContext mc = null;
		HttpSession session;
		MessageRecord newmessage = null;
		EWD0026DSMessage msgHelp = null;
		ESS0030DSMessage msgUser = null;
		JBListRec beanListRec = null;
		int colNum = 27;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			msgUser =
				(datapro.eibs.beans.ESS0030DSMessage) session.getAttribute(
					"currUser");
			String Language = msgUser.getE01LAN();
			String LangPath = super.rootPath + Language + "/";

			try {
				flexLog("Opennig Socket Connection");
				mc = new MessageContext(super.getMessageHandler("EWD0026", req));

				try {
					msgHelp =
						(EWD0026DSMessage) mc.getMessageRecord("EWD0026DS");
					msgHelp.setEWDACC(req.getParameter("CustNum"));
					msgHelp.send();
					msgHelp.destroy();
					flexLog("EWD0026DS Message Sent");
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
					return;
				}

				// Receiving
				try {
					newmessage = mc.receiveMessage();

					if (newmessage.getFormatName().equals("EWD0026DS")) {

						try {
							beanListRec = new JBListRec();
							beanListRec.init(colNum);
							flexLog("EWD0026DS Message Received");
						} catch (Exception ex) {
							flexLog("Error: " + ex);
						}

						String marker = "";
						String myFlag = "";
						String myRow[] = new String[colNum];
						for (int i = 0; i < colNum; i++) {
							myRow[i] = "";
						}
						while (true) {

							msgHelp = (EWD0026DSMessage) newmessage;

							marker = msgHelp.getEWDOPE();

							if (marker.equals("*")) {
								//beanListRec.setShowNext(false);
								break;
							} else {

								myRow[0] = msgHelp.getEWDMAN();
								myRow[1] = msgHelp.getEWDACC();
								myRow[2] = msgHelp.getEWDMA1();
								myRow[3] = msgHelp.getEWDMA2();
								myRow[4] = msgHelp.getEWDMA3();
								myRow[5] = msgHelp.getEWDMA4();
								myRow[6] = msgHelp.getEWDCTY();
								myRow[7] = msgHelp.getEWDSTE();
								myRow[8] = msgHelp.getEWDCTR();
								myRow[9] = msgHelp.getEWDZPC();
								myRow[10] = msgHelp.getEWDPOB();
								//Only to Corpbanca
								myRow[11] = msgHelp.getEWDPID(); 
								myRow[12] = msgHelp.getEWDDSC(); 
								myRow[13] = msgHelp.getEWDTID(); 
								myRow[14] = msgHelp.getEWDFL1(); //Type Address
								myRow[15] = msgHelp.getEWDFL2(); //Verificacion
								myRow[16] = msgHelp.getEWDFL3(); //Vigencia
								myRow[17] =
									Util.formatDate(
										msgHelp.getEWDDT1(),
										msgHelp.getEWDDT2(),
										msgHelp.getEWDDT3());
								//Verif. Date
								myRow[18] = msgHelp.getEWDBSX(); //Modo Verif.
								myRow[19] = msgHelp.getEWDHPN(); //Phone
									
								myRow[20] = msgHelp.getEWDNST(); 
								myRow[21] = msgHelp.getEWDADT(); //Tipo direccion
								myRow[22] = msgHelp.getDWDADT(); 
								myRow[23] = msgHelp.getEWDCOM(); 
								myRow[24] = msgHelp.getDWDCOM(); 
								myRow[25] = msgHelp.getEWDADF(); 
								myRow[26] = msgHelp.getDWDADF(); 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 								
								beanListRec.addRow(myFlag, myRow);

							}
							newmessage = mc.receiveMessage();
						}

						flexLog("Putting java beans into the session");
						session.setAttribute("ewd0026Help", beanListRec);

						try {
							flexLog(
								"About to call Page: "
									+ LangPath
									+ "EWD0026_client_mailing_address_helpmessage.jsp");
							callPage(
								LangPath
									+ "EWD0026_client_mailing_address_helpmessage.jsp",
								req,
								res);
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
				}

			} catch (Exception e) {
				e.printStackTrace();
				int sck = getInitSocket(req) + 1;
				flexLog("Socket not Open(Port " + sck + "). Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
				//return;
			} finally {
				mc.close();
			}
		}

	}
}