package com.datapro.eibs.security.object;

/**
 * Insert the type's description here.
 * Creation date: (7/19/00 6:55:55 PM)
 * @author: Ramses
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

import com.datapro.sql.manager.*;
import com.datapro.eibs.security.bean.*;
import com.datapro.eibs.security.dao.*;
import com.datapro.eibs.security.facade.*;
import com.datapro.generic.beanutil.*;

import java.sql.Connection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;


public class PrivilegeProfile {

	/**
	 * 
	 */
	public PrivilegeProfile() {
		super();
	}
	/**
	 * 
	 */
	public void destroy() {

	}

	public CNOFCPVNME getPrivilege(String PVLCOD) {

		CNOFCPVDao priviDao = new CNOFCPVDao("AS400");
		CNOFCPVNME priviBean = null;
		
		try {
			
			priviBean = priviDao.select(PVLCOD);
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return priviBean;
	}
	
	public JBObjList getPrivilege() {

		CNOFCPVDao priviDao = new CNOFCPVDao("AS400");
		JBObjList priviListBean = null;
		
		try {
			
			priviListBean = priviDao.select();
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return priviListBean;
	}
	
		
}
