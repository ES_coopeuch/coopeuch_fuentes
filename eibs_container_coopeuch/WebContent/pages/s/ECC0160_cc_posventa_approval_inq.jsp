<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%>
<html>
<head>
<title>Credit Cards Mantenance</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id="ccPvta" class="datapro.eibs.beans.ECC016001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<SCRIPT LANGUAGE="javascript">
           
</SCRIPT>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0"); 
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
  
%> 

</head>
<body>
<h3 align="center" >Plataforma PosVenta<BR> Aprobacion Solicitudes Tarjetas de Credito<br> <%if (userPO.getOption().equals("1")) out.print("A C T I V A C I O N");%>
                                                                        <%if (userPO.getOption().equals("2")) out.print("A D I C I O N A L E S");%>
																		<%if (userPO.getOption().equals("3")) out.print("Cambio Cupo");%>
																	    <%if (userPO.getOption().equals("4")) out.print("Bloqueo/Desbloqueo");%>
                                                                        <%if (userPO.getOption().equals("5")) out.print("Reemision de Tarjeta");%>
                                                                        <%if (userPO.getOption().equals("6")) out.print("Cambio de Codigo FV");%>
                                                                        <%if (userPO.getOption().equals("7")) out.print("Pago Minimo/Pac Multibanco");%>
                                                                        <%if (userPO.getOption().equals("8")) out.print("Reseteo Clave");%>
                                                                        <%if (userPO.getOption().equals("9")) out.print("Cambio Direcci�n envio Estado Cuenta");%>
                                                                        <%if (userPO.getOption().equals("10")) out.print("");%>  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    																	    

<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cc_posventa_approval_inq.jsp,ECC0160"> 
</h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0160" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="500">
   <TABLE class="tbenter" width=100%>
   	<TR>
      <TD> 
        <div align="center">
   	      <table class="tableinfo">
            <tr > 
              <td nowrap> 
                <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
                  <tr id="trclear"> 
                    <td nowrap width="16%"> 
                      <div align="right"><b>Identificaci&oacute;n :</b></div>
                    </td>
                    <td nowrap width="20%"> 
                      <div align="left"> 
                        <input type="text" name="E01CCRCID" size="15" maxlength="15" value="<%= ccPvta.getE01CCRCID() %>" readonly>
                      </div>
                    </td>
                    <td nowrap width="16%" > 
                      <div align="right"><b>Cliente :</b></div>
                    </td>
                    <td nowrap width="20%" > 
                      <div align="left"><b> 
                        <input type="text" name="E01CCMCUN" size="10" maxlength="9" value="<%= ccPvta.getE01CCMCUN()%>" readonly >
                        </b> </div>
                    </td>
                    <td nowrap width="16%" > 
                      <div align="right"><b>Nombre :</b></div>
                    </td>
                    <td nowrap > 
                      <div align="left"><font face="Arial"><font face="Arial"><font size="2"> 
                        <input type="text" name="E01CCMNME" size="35" maxlength="35" value="<%= ccPvta.getE01CCMNME()%>" readonly>
                        </font></font></font></div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>      
        </div>
      </td>
    </tr>
 </table>      
<h4>Informacion de la Tarjeta</h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trclear"> 
            <td nowrap width="19%"> 
              <div align="right">Cuenta : </div>
            </td>
            <td nowrap width="41%">
              <div align="left" >
                <input type="text" name="E01CCMNXN" size="20" maxlength="20" value="<%= ccPvta.getE01CCMNXN().trim() %>" readonly >
              </div>              
            </td>                
            <td nowrap width="18%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="22%">
              <div align="left" > </div>              
            </td>
          </tr>         
          <tr id="trdark"> 
            <td nowrap width="19%"> 
              <div align="right">Tarjeta : </div>
            </td>
            <td nowrap width="41%">
              <div align="left" >
                <input type="text" name="E01CCRNUM" size="15" maxlength="15" value="<%= ccPvta.getE01CCRNUM().trim() %>" readonly >
                <input type="text" name="D01CCMPRO" size="35" maxlength="35" value="<%= ccPvta.getD01CCMPRO().trim() %>" readonly >
              </div>              
            </td>                
            <td nowrap width="18%"> 
              <div align="right">Tipo : </div>
            </td>
            <td nowrap width="22%">
              <div align="left" >                
                <input type="text" name="E01CCRTPI" size="10" maxlength="10" value="<% if (ccPvta.getE01CCRTPI().trim().equals("T")) out.print("TITULAR"); else out.print("ADICIONAL"); %>" readonly > 
              </div>              
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="19%"> 
              <div align="right">Rut Titular : </div>
            </td>
            <td nowrap width="41%"> 
              <div align="left"> 
                <input type="text" readonly name="E01CCRCID" size="12" maxlength="12" value="<%= ccPvta.getE01CCRCID()%>" readonly>
                <input type="text" name="E01CCMNME" size="35" maxlength="35" value="<%= ccPvta.getE01CCMNME()%>" readonly>
               </div>
            </td>
            <td nowrap width="18%"> 
              <div align="right">Fecha Activaci&oacute;n : </div>
            </td>
            <td nowrap width="22%"> 
              <input type="text" name="E01CCRATD" size="3" maxlength="2" value="<%= ccPvta.getE01CCRATD().trim()%>" readonly>
              <input type="text" name="E01CCRATM" size="3" maxlength="2" value="<%= ccPvta.getE01CCRATM().trim()%>" readonly>
              <input type="text" name="E01CCRATY" size="5" maxlength="4" value="<%= ccPvta.getE01CCRATY().trim()%>" readonly>              
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="19%"> 
              <div align="right">Estado de la Tarjeta : </div>
            </td>
            <td nowrap width="41%"> 
              <input type="text" name="E01CCRSTS" size="25" maxlength="25" value="<% if(ccPvta.getE01CCRSTS().equals("1")) out.print("Activa"); else out.print("Inactiva");%>" readonly>
            </td>
            <td nowrap width="18%"> 
              <div align="right">Fecha de Apertura : </div>
            </td>
            <td nowrap width="22%"> 
              <input type="text" name="E01CCMOPD" size="3" maxlength="2" value="<%= ccPvta.getE01CCMOPD().trim()%>" readonly>
              <input type="text" name="E01CCMOPM" size="3" maxlength="2" value="<%= ccPvta.getE01CCMOPM().trim()%>" readonly>
              <input type="text" name="E01CCMOPY" size="5" maxlength="4" value="<%= ccPvta.getE01CCMOPY().trim()%>" readonly>
            </td>
            </tr>  
           <tr id="trclear"> 
            <td nowrap width="19%"> 
              <div align="right">Bloqueo : </div>
            </td>
            <td nowrap width="41%"> 
              <input type="text" name="D01CCRLKC" size="25" maxlength="25" value="<%= ccPvta.getD01CCRLKC()%>" readonly>
            </td>
            <td nowrap width="18%"> 
              <div align="right">Cupo compras Nacional : </div>
            </td>
            <td nowrap width="22%">            
              <eibsinput:text property="E01CCRCDN" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly ="true"/>
              </td>
            </tr> 
          <tr id="trdark"> 
            <td nowrap width="19%"> 
              <div align="right">Codigo FV : </div>
            </td>
            <td nowrap width="41%"> 
              <input type="text" name="E01CCMCFA" size="3" maxlength="2" value="<%= ccPvta.getE01CCMCFA()%>" readonly>
            </td>
            <td nowrap width="18%">  
              <div align="right">Cupo compras Internacional : </div>
            </td>
            <td nowrap width="22%"> 
              <eibsinput:text property="E01CCRCDR" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly ="true"/> 				 
            </td>
          </tr>                          
          <tr id="trclear"> 
            <td nowrap width="19%"> 
              <div align="right">Pago M&iacute;nimo : </div>
            </td>
            <td nowrap width="41%"> 
              <input type="text" name="D01CCMFPA" size="35" maxlength="35" value="<%= ccPvta.getD01CCMFPA().trim()%>" readonly> 
            </td>
            <td nowrap width="18%"> 
              <div align="right">Cupo avances Nacional : </div>
            </td>
            <td nowrap width="22%"> 
              <eibsinput:text property="E01CCRADN" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly ="true"/>               
 			</td>
          </tr> 
          <tr id="trdark"> 
            <td nowrap width="19%"> 
              <div align="right">Direcc. Estado Cuenta : </div>
            </td>
            <td nowrap width="41%"> 
              <input type="text" name="E01CCMMLA" size="2" maxlength="2" value="<%= ccPvta.getE01CCMMLA() %>" readonly> 
 			</td>
            <td nowrap width="18%"> 
              <div align="right">Cupo avances Internacional : </div>
            </td>
            <td nowrap width="22%"> 
              <eibsinput:text property="E01CCRADI" size="17" maxlength="16" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly ="true"/>              
 			</td>
          </tr>                      
        </table>
      </td>
    </tr>
  </table>
<br>
<%-----------------------------------------------------------------------------------------Activacion de la Tarjeta ---------%>
<% if (userPO.getOption().equals("1")) { %>
 <h4>Activaci&oacute;n de la Tarjeta</h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                 
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Estado de Tarjeta Anterior : </div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCRSTS" disabled>
                <option value="0" <% if(ccPvta.getE01CCRSTS().equals("0")) out.print("selected");%>>Inactiva</option>  
                <option value="1" <% if(ccPvta.getE01CCRSTS().equals("1")) out.print("selected");%>>Activa</option>                               			
              </select>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Estado de Tarjeta Nuevo : </div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCRSTS" disabled>
                <option value="0" <% if(ccPvta.getE01CCRSTS().equals("0")) out.print("selected");%>>Activa</option>                			
              </select>
            </td>
          </tr>                 
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                            
        </table>
      </td>
    </tr>
  </table> 
<%} %>
<%-----------------------------------------------------------------------------------------Ingreso Adicionales --------%>  
<% if (userPO.getOption().equals("2")) { %> 
 <h4>Ingreso de Adicionales</h4>      
   <table class="tableinfo">  
   <tr>
   <td >     
    <table cellspacing="0" cellpadding="2" width="100%" border="0" >
           <tr id="trclear"> 
            <td> 
              <div align="left"><h4>Adicional 1</h4> </div>
            </td>        
          </tr>                           
           <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Rut Adicional : </div>
            </td>
            <td nowrap width="32%"> 
             <input type="text" name="E01SPVRA1" size="25" maxlength="25" value="<%= ccPvta.getE01SPVRA1() %>" readonly >
            </td>
             <td nowrap width="25%"> </td>  
             <td nowrap width="20%"> </td>                        
          </tr>        
          <tr id="trclear">
            <td nowrap width="22%"> 
              <div align="right">Apellido Paterno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPA1" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPA1() %>" readonly>
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
          <tr id="trdark">
            <td nowrap width="22%"> 
              <div align="right">Apellido Materno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVSA1" size="13" maxlength="13" value="<%=ccPvta.getE01SPVSA1() %>" readonly>
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>Cupos Diferenciados</b> </div>
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Nombres : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPN1" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPN1() %>"readonly >
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Nacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVCN1" size="17" maxlength="16" name="ccPvta" readonly ="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                
            </td>            
          </tr> 
         <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Fecha de Nacimiento : </div>
            </td>
             <td nowrap width="32%">
                <eibsinput:date name="ccPvta" fn_year="E01SPVBY1" fn_month="E01SPVBM1" fn_day="E01SPVBD1" readonly ="true" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Avance Nacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVAN1" size="17" maxlength="16" name="ccPvta" readonly ="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                
            </td>            
          </tr> 
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Sexo : </div>
            </td>
             <td nowrap width="32%" >
              <input type="radio" name="E01SPVSX1" readonly value="F" <% if (ccPvta.getE01SPVSX1().equals("F")) out.print("checked"); %>>
              Femenino 
              <input type="radio" name="E01SPVSX1" readonly value="M" <% if (ccPvta.getE01SPVSX1().equals("M")) out.print("checked"); %>>
              Masculino </td> 
            <td nowrap width="25%"> 
              <div align="right">Cupo Internacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVIN1" size="17" maxlength="16" readonly ="true" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                
            </td>            
          </tr>  
         <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Parentesco : </div>
            </td>
             <td nowrap width="32%">
              <select name="E01SPVRE1" disabled>
                <option value=" " <% if (!(ccPvta.getE01SPVRE1().equals("1") ||
                                           ccPvta.getE01SPVRE1().equals("2") ||
                                           ccPvta.getE01SPVRE1().equals("3") ||
                                           ccPvta.getE01SPVRE1().equals("4") ||
                                           ccPvta.getE01SPVRE1().equals("5") ||
                                           ccPvta.getE01SPVRE1().equals("6") ||
                                           ccPvta.getE01SPVRE1().equals("7") ||
                                           ccPvta.getE01SPVRE1().equals("8") ||
                                           ccPvta.getE01SPVRE1().equals("9") ||
                                           ccPvta.getE01SPVRE1().equals("A") ||
                                           ccPvta.getE01SPVRE1().equals("B") ||   
                                           ccPvta.getE01SPVRE1().equals("C") ||
                                           ccPvta.getE01SPVRE1().equals("D") ||
                                           ccPvta.getE01SPVRE1().equals("E") ||  
                                           ccPvta.getE01SPVRE1().equals("F")))  out.print("selected"); %>></option>
   	     	    <option value="1" <% if (ccPvta.getE01SPVRE1().equals("1")) out.print("selected"); %>>Ninguna</option>
	   	   	    <option value="2" <% if (ccPvta.getE01SPVRE1().equals("2")) out.print("selected"); %>>Padre</option>
                <option value="3" <% if (ccPvta.getE01SPVRE1().equals("3")) out.print("selected"); %>>Madre</option>
                <option value="4" <% if (ccPvta.getE01SPVRE1().equals("4")) out.print("selected"); %>>Hermano(a)</option>
                <option value="5" <% if (ccPvta.getE01SPVRE1().equals("5")) out.print("selected"); %>>Abuelo(a)</option>
                <option value="6" <% if (ccPvta.getE01SPVRE1().equals("6")) out.print("selected"); %>>Tio(a)</option>
                <option value="7" <% if (ccPvta.getE01SPVRE1().equals("7")) out.print("selected"); %>>Hijo(a)</option>
   	     	    <option value="8" <% if (ccPvta.getE01SPVRE1().equals("8")) out.print("selected"); %>>Conyuge</option>
	   	   	    <option value="9" <% if (ccPvta.getE01SPVRE1().equals("9")) out.print("selected"); %>>Primo(a)</option>
                <option value="A" <% if (ccPvta.getE01SPVRE1().equals("A")) out.print("selected"); %>>Cunado(a)</option>
                <option value="B" <% if (ccPvta.getE01SPVRE1().equals("B")) out.print("selected"); %>>Nieto(a)</option>
                <option value="C" <% if (ccPvta.getE01SPVRE1().equals("C")) out.print("selected"); %>>Sobrino(a)</option>
                <option value="D" <% if (ccPvta.getE01SPVRE1().equals("D")) out.print("selected"); %>>Suegro(a)</option>
                <option value="E" <% if (ccPvta.getE01SPVRE1().equals("E")) out.print("selected"); %>>Nuera(a)</option>  
                <option value="F" <% if (ccPvta.getE01SPVRE1().equals("F")) out.print("selected"); %>>Yerno</option>                               
              </select>             
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Avance Internacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVAI1" size="17" maxlength="16" readonly ="true" name="ccPvta" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                
            </td>        
          </tr>                             
        </table>
   </td>
   </tr>     
</table>
   <table class="tableinfo">  
   <tr>
   <td >     
    <table cellspacing="0" cellpadding="2" width="100%" border="0" >
           <tr id="trclear"> 
            <td> 
              <div align="left"><h4>Adicional 2</h4> </div>
            </td>        
          </tr>                           
           <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Rut Adicional : </div>
            </td>
            <td nowrap width="32%"> 
             <input type="text" name="E01SPVRA2" size="25" maxlength="25"  value="<%= ccPvta.getE01SPVRA2() %>" readonly>
            </td>
             <td nowrap width="25%"> </td>  
             <td nowrap width="20%"> </td>                        
          </tr>        
          <tr id="trclear">
            <td nowrap width="22%"> 
              <div align="right">Apellido Paterno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPA2" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPA2()%>" readonly>
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
          <tr id="trdark">
            <td nowrap width="22%"> 
              <div align="right">Apellido Materno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVSA2" size="13" maxlength="13" value="<%=ccPvta.getE01SPVSA2()%>" readonly>
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>Cupos Diferenciados</b> </div>
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Nombres : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPN2" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPN2()%>" readonly>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Nacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVCN2" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                
            </td>            
          </tr> 
         <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Fecha de Nacimiento : </div>
            </td>
             <td nowrap width="32%">
                <eibsinput:date name="ccPvta" fn_year="E01SPVBY2" fn_month="E01SPVBM2" fn_day="E01SPVBD2" readonly="true"/>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Avance Nacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVAN2" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                
            </td>            
          </tr> 
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Sexo : </div>
            </td>
             <td nowrap width="32%">
              <input type="radio" name="E01SPVSX2" value="F" readonly  <% if (ccPvta.getE01SPVSX2().equals("F")) out.print("checked"); %>>
              Femenino 
              <input type="radio" name="E01SPVSX2" value="M" readonly  <% if (ccPvta.getE01SPVSX2().equals("M")) out.print("checked"); %>>
              Masculino </td> 
            <td nowrap width="25%"> 
              <div align="right">Cupo Internacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVIN2" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                 
            </td>            
          </tr>  
         <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Parentesco : </div>
            </td>
             <td nowrap width="32%">
              <select name="E01SPVRE2" disabled>
                <option value=" " <% if (!(ccPvta.getE01SPVRE2().equals("1") ||
                                           ccPvta.getE01SPVRE2().equals("2") ||
                                           ccPvta.getE01SPVRE2().equals("3") ||
                                           ccPvta.getE01SPVRE2().equals("4") ||
                                           ccPvta.getE01SPVRE2().equals("5") ||
                                           ccPvta.getE01SPVRE2().equals("6") ||
                                           ccPvta.getE01SPVRE2().equals("7") ||
                                           ccPvta.getE01SPVRE2().equals("8") ||
                                           ccPvta.getE01SPVRE2().equals("9") ||
                                           ccPvta.getE01SPVRE2().equals("A") ||
                                           ccPvta.getE01SPVRE2().equals("B") ||   
                                           ccPvta.getE01SPVRE2().equals("C") ||
                                           ccPvta.getE01SPVRE2().equals("D") ||
                                           ccPvta.getE01SPVRE2().equals("E") ||  
                                           ccPvta.getE01SPVRE2().equals("F")))  out.print("selected"); %>></option>
   	     	    <option value="1" <% if (ccPvta.getE01SPVRE2().equals("1")) out.print("selected"); %>>Ninguna</option>
	   	   	    <option value="2" <% if (ccPvta.getE01SPVRE2().equals("2")) out.print("selected"); %>>Padre</option>
                <option value="3" <% if (ccPvta.getE01SPVRE2().equals("3")) out.print("selected"); %>>Madre</option>
                <option value="4" <% if (ccPvta.getE01SPVRE2().equals("4")) out.print("selected"); %>>Hermano(a)</option>
                <option value="5" <% if (ccPvta.getE01SPVRE2().equals("5")) out.print("selected"); %>>Abuelo(a)</option>
                <option value="6" <% if (ccPvta.getE01SPVRE2().equals("6")) out.print("selected"); %>>Tio(a)</option>
                <option value="7" <% if (ccPvta.getE01SPVRE2().equals("7")) out.print("selected"); %>>Hijo(a)</option>
   	     	    <option value="8" <% if (ccPvta.getE01SPVRE2().equals("8")) out.print("selected"); %>>Conyuge</option>
	   	   	    <option value="9" <% if (ccPvta.getE01SPVRE2().equals("9")) out.print("selected"); %>>Primo(a)</option>
                <option value="A" <% if (ccPvta.getE01SPVRE2().equals("A")) out.print("selected"); %>>Cunado(a)</option>
                <option value="B" <% if (ccPvta.getE01SPVRE2().equals("B")) out.print("selected"); %>>Nieto(a)</option>
                <option value="C" <% if (ccPvta.getE01SPVRE2().equals("C")) out.print("selected"); %>>Sobrino(a)</option>
                <option value="D" <% if (ccPvta.getE01SPVRE2().equals("D")) out.print("selected"); %>>Suegro(a)</option>
                <option value="E" <% if (ccPvta.getE01SPVRE2().equals("E")) out.print("selected"); %>>Nuera(a)</option>  
                <option value="F" <% if (ccPvta.getE01SPVRE2().equals("F")) out.print("selected"); %>>Yerno</option>                               
              </select>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Avance Internacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVAI2" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                
            </td>        
          </tr>                             
        </table>
   </td>
   </tr>     
</table>        
   <table class="tableinfo">  
   <tr>
   <td >     
    <table cellspacing="0" cellpadding="2" width="100%" border="0" >
           <tr id="trclear"> 
            <td> 
              <div align="left"><h4>Adicional 3</h4> </div>
            </td>        
          </tr>                           
           <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Rut Adicional : </div>
            </td>
            <td nowrap width="32%"> 
             <input type="text" name="E01SPVRA3" size="25" maxlength="25" value="<%= ccPvta.getE01SPVRA3() %>" readonly>
            </td>
             <td nowrap width="25%"> </td>  
             <td nowrap width="20%"> </td>                        
          </tr>        
          <tr id="trclear">
            <td nowrap width="22%"> 
              <div align="right">Apellido Paterno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPA3" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPA3()%>" readonly>
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
          <tr id="trdark">
            <td nowrap width="22%"> 
              <div align="right">Apellido Materno : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVSA3" size="13" maxlength="13" value="<%=ccPvta.getE01SPVSA3()%>" readonly >
            </td>
            <td nowrap width="25%"> 
              <div align="center"><b>Cupos Diferenciados</b> </div>
            </td>
             <td nowrap width="25%"> 
            </td>            
          </tr>   
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Nombres : </div>
            </td>
             <td nowrap width="32%">
                <input type="text" name="E01SPVPN3" size="13" maxlength="13" value="<%=ccPvta.getE01SPVPN3()%>" readonly >
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Nacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVCN3" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                 
            </td>            
          </tr> 
         <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Fecha de Nacimiento : </div>
            </td>
             <td nowrap width="32%">
                <eibsinput:date name="ccPvta" fn_year="E01SPVBY3" fn_month="E01SPVBM3" fn_day="E01SPVBD3" readonly="true" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Avance Nacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVAN3" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                 
            </td>            
          </tr> 
         <tr id="trclear"> 
            <td nowrap width="22%"> 
              <div align="right">Sexo : </div>
            </td>
             <td nowrap width="32%">
              <input type="radio" name="E01SPVSX3" readonly value="F" <% if (ccPvta.getE01SPVSX3().equals("F")) out.print("checked"); %>>
              Femenino 
              <input type="radio" name="E01SPVSX3" readonly value="M" <% if (ccPvta.getE01SPVSX3().equals("M")) out.print("checked"); %>>
              Masculino </td>             
            <td nowrap width="25%"> 
              <div align="right">Cupo Internacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVIN3" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                 
            </td>            
          </tr>  
         <tr id="trdark"> 
            <td nowrap width="22%"> 
              <div align="right">Parentesco : </div>
            </td>
             <td nowrap width="32%">
              <select name="E01SPVRE3" disabled>
                <option value=" " <% if (!(ccPvta.getE01SPVRE3().equals("1") ||
                                           ccPvta.getE01SPVRE3().equals("2") ||
                                           ccPvta.getE01SPVRE3().equals("3") ||
                                           ccPvta.getE01SPVRE3().equals("4") ||
                                           ccPvta.getE01SPVRE3().equals("5") ||
                                           ccPvta.getE01SPVRE3().equals("6") ||
                                           ccPvta.getE01SPVRE3().equals("7") ||
                                           ccPvta.getE01SPVRE3().equals("8") ||
                                           ccPvta.getE01SPVRE3().equals("9") ||
                                           ccPvta.getE01SPVRE3().equals("A") ||
                                           ccPvta.getE01SPVRE3().equals("B") ||   
                                           ccPvta.getE01SPVRE3().equals("C") ||
                                           ccPvta.getE01SPVRE3().equals("D") ||
                                           ccPvta.getE01SPVRE3().equals("E") ||  
                                           ccPvta.getE01SPVRE3().equals("F")))  out.print("selected"); %>></option>
   	     	    <option value="1" <% if (ccPvta.getE01SPVRE3().equals("1")) out.print("selected"); %>>Ninguna</option>
	   	   	    <option value="2" <% if (ccPvta.getE01SPVRE3().equals("2")) out.print("selected"); %>>Padre</option>
                <option value="3" <% if (ccPvta.getE01SPVRE3().equals("3")) out.print("selected"); %>>Madre</option>
                <option value="4" <% if (ccPvta.getE01SPVRE3().equals("4")) out.print("selected"); %>>Hermano(a)</option>
                <option value="5" <% if (ccPvta.getE01SPVRE3().equals("5")) out.print("selected"); %>>Abuelo(a)</option>
                <option value="6" <% if (ccPvta.getE01SPVRE3().equals("6")) out.print("selected"); %>>Tio(a)</option>
                <option value="7" <% if (ccPvta.getE01SPVRE3().equals("7")) out.print("selected"); %>>Hijo(a)</option>
   	     	    <option value="8" <% if (ccPvta.getE01SPVRE3().equals("8")) out.print("selected"); %>>Conyuge</option>
	   	   	    <option value="9" <% if (ccPvta.getE01SPVRE3().equals("9")) out.print("selected"); %>>Primo(a)</option>
                <option value="A" <% if (ccPvta.getE01SPVRE3().equals("A")) out.print("selected"); %>>Cunado(a)</option>
                <option value="B" <% if (ccPvta.getE01SPVRE3().equals("B")) out.print("selected"); %>>Nieto(a)</option>
                <option value="C" <% if (ccPvta.getE01SPVRE3().equals("C")) out.print("selected"); %>>Sobrino(a)</option>
                <option value="D" <% if (ccPvta.getE01SPVRE3().equals("D")) out.print("selected"); %>>Suegro(a)</option>
                <option value="E" <% if (ccPvta.getE01SPVRE3().equals("E")) out.print("selected"); %>>Nuera(a)</option>  
                <option value="F" <% if (ccPvta.getE01SPVRE3().equals("F")) out.print("selected"); %>>Yerno</option>                               
              </select>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Avance Internacional : </div>
            </td>
             <td nowrap width="25%">
                <eibsinput:text property="E01SPVAI3" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>"/>                 
            </td>        
          </tr>                             
        </table>
   </td>
   </tr>     
</table>          
<%}%>   
  <%-----------------------------------------------------------------------------------------Cambio Cupo ---------%> 
<% if (userPO.getOption().equals("3")) { %>  
 <h4>Cambio Cupo </h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead"> 
          <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="Center"><b>Cupos Anteriores</b></div>
            </td>
             <td nowrap> </td>
            <td nowrap width="25%"> 
              <div align="right"><b>Cupos Nuevos</b></div>
            </td>
             <td nowrap>       				 
            </td>
          </tr>  
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Cupo Compras Nacional : </div>
            </td>
             <td nowrap>
              <eibsinput:text property="E01CCRCDN" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" />              				 
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Compras Nacional : </div>
            </td>
             <td nowrap>
              <eibsinput:text property="E01SPVCNC" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" />              				 
            </td>
          </tr> 
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right">Cupo Compras Internacional : </div>
            </td>
             <td nowrap>
              <eibsinput:text property="E01CCRCDR" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" />              				 
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Compras Internacional : </div>
            </td>
             <td nowrap>
              <eibsinput:text property="E01SPVCIC" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" />              				 
            </td>
          </tr> 
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Cupo Avances Nacional : </div>
            </td>
             <td nowrap>
              <eibsinput:text property="E01CCRADN" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Avances Nacional : </div>
            </td>
             <td nowrap>
              <eibsinput:text property="E01SPVCAN" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" />
            </td>
          </tr>   
          <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right">Cupo Avances Internacional : </div>
            </td>
             <td nowrap>
              <eibsinput:text property="E01CCRADI" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" />
            </td>
            <td nowrap width="25%"> 
              <div align="right">Cupo Avances Internacional : </div>
            </td>
             <td nowrap>
              <eibsinput:text property="E01SPVCAI" size="17" maxlength="16" name="ccPvta" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" />
            </td>
          </tr>                                                       
        </table>
      </td>
    </tr>
  </table>   
 <%}%> 
<%-----------------------------------------------------------------------------------------Bloqueos y Desbloqueos ---------%> 
<% if (userPO.getOption().equals("4")) { %>  
 <h4>Bloqueos - Desbloqueos </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                  
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Bloqueo/Desbloqueo Anterior : </div>
            </td>
             <td nowrap>
              <select name="E01SPVBLI" disabled>
                <option value=" "><% if (ccPvta.getE01SPVBLI().equals(" ")) out.print("selected"); %>>Sin Bloqueo</option> 
   	     	    <option value="O" <% if (ccPvta.getE01SPVBLI().equals("O")) out.print("selected"); %>>Bloqueo Perdida</option>
	   	   	    <option value="N" <% if (ccPvta.getE01SPVBLI().equals("N")) out.print("selected"); %>>Bloqueo Perdida</option>
                <option value="B" <% if (ccPvta.getE01SPVBLI().equals("B")) out.print("selected"); %>>Bloqueo Banco 2</option>
                <option value="S" <% if (ccPvta.getE01SPVBLI().equals("S")) out.print("selected"); %>>Bloqueo Banco</option>
                <option value="W" <% if (ccPvta.getE01SPVBLI().equals("W")) out.print("selected"); %>>Bloqueo Internacional</option>                              
              </select>             
            </td>
            <td nowrap width="25%"> 
              <div align="right">Bloqueo/Desbloqueo Nuevo : </div>
            </td>
             <td nowrap>
              <select name="E01SPVBLN" disabled>
                <option value=" "><% if (ccPvta.getE01SPVBLN().equals(" ")) out.print("selected"); %>>Sin Bloqueo</option>  
   	     	    <option value="O" <% if (ccPvta.getE01SPVBLN().equals("O")) out.print("selected"); %>>Bloqueo Perdida</option>
	   	   	    <option value="N" <% if (ccPvta.getE01SPVBLN().equals("N")) out.print("selected"); %>>Bloqueo Perdida</option>
                <option value="B" <% if (ccPvta.getE01SPVBLN().equals("B")) out.print("selected"); %>>Bloqueo Banco 2</option>
                <option value="S" <% if (ccPvta.getE01SPVBLN().equals("S")) out.print("selected"); %>>Bloqueo Banco</option>
                <option value="W" <% if (ccPvta.getE01SPVBLN().equals("W")) out.print("selected"); %>>Bloqueo Internacional</option>                              
              </select>             
            </td>
          </tr>     
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                                       
        </table>
      </td>
    </tr>
  </table>   
 <%} %>
 <%-----------------------------------------------------------------------------------------Reemision de la Tarjeta ---------%> 
<% if (userPO.getOption().equals("5")) { %>  
 <h4>Reemisi&oacute;n</h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>        
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Motivo Remisi&oacute;n :</div>
            </td>
            <td nowrap width="23%"> 
                <input type="text" name="E01SPVMOT" size="4" maxlength="4" value="<%= ccPvta.getE01SPVMOT() %>" readonly >
                <input type="text" name="E01SPVRMK"  size="30" maxlength="30" value="<%= ccPvta.getE01SPVRMK()%>" readonly>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Sucursal de Env�o de Tarjeta :</div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> 
                <input type="text" name="E01SPVSEC" size="4" maxlength="4" value="<%=ccPvta.getE01SPVSEC()%>" readonly >
                <input type="text" name="DESCBRANCH"  size="30" maxlength="30" value="<%=ccPvta.getE01SPVBEC()%>" readonly>
              </div> 			
             </td>
          </tr> 
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                                       
        </table>
      </td>
    </tr>
  </table> 
 <%} %>
 <%-----------------------------------------------------------------------------------------Cambio de cargo FV ---------%> 
<% if (userPO.getOption().equals("6")) { %>  
 <h4>Cambio de Codigo FV </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">  
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>  
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">FV Anterior: </div>
            </td>
             <td nowrap>
                <input type="text" name="E01CCMCFA" size="3" maxlength="3" value="<%= ccPvta.getE01CCMCFA() %>" readonly >                           				   	   	                              			 
            </td>
            <td nowrap width="25%"> 
              <div align="right">FV Nuevo : </div>
            </td>
             <td nowrap>
                <input type="text" name="E01SPVCFC" size="3" maxlength="3" value="<%= ccPvta.getE01SPVCFC() %>" readonly > 	   	   	                              			 
            </td>
          </tr>                       
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                          
        </table>
      </td>
    </tr>
  </table>   
 <%}%> 
 <%-----------------------------------------------------------------------------------------Cambio Pago Minimo / Pac Multibanco---------%> 
<% if (userPO.getOption().equals("7")) { %>  
 <h4>Pago M&iacute;nimo </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                  
           <tr id="trdark">
            <td nowrap width="27%"> 
              <div align="right">Pago M&iacute;nimo Anterior : </div> 			
             </td> 
             <td> 
              <select name="E01SPVCPM" disabled>
                <option value=0 <% if (!(ccPvta.getField("E01CCMCPM").getString().equals("0")) || ccPvta.getField("E01CCMCPM").getString().equals("1")) out.print("selected"); %> selected></option>
                <option value=0 <% if (ccPvta.getField("E01CCMCPM").getString().equals("0")) out.print("selected");%>>   5%</option>
                <option value=1 <% if (ccPvta.getField("E01CCMCPM").getString().equals("1")) out.print("selected");%>> 100%</option>                			
              </select>				  
             </td>               
            <td> 
              <div align="right">Cuenta Corriente Actual :</div> 
             </td>               
            <td nowrap width="27%"> 
				<input type="text" name="E01CCMRPA" size="14" maxlength="14" value="<%= ccPvta.getE01CCMRPA().trim()%>" readonly>			
             </td>
           </tr> 
           <tr id="trdark">
            <td nowrap width="27%"> 
              <div align="right">Pago M&iacute;nimo Nuevo : </div> 			
             </td> 
             <td> 
              <select name="E01SPVCPM" disabled>
                <option value=0 <% if (!(ccPvta.getField("E01SPVCPM").getString().equals("0")) || ccPvta.getField("E01SPVCPM").getString().equals("1")) out.print("selected"); %> selected></option>
                <option value=0 <% if (ccPvta.getField("E01SPVCPM").getString().equals("0")) out.print("selected");%>>   5%</option>
                <option value=1 <% if (ccPvta.getField("E01SPVCPM").getString().equals("1")) out.print("selected");%>> 100%</option>                			
              </select>				  
             </td>               
            <td> 
              <div align="right">Cuenta Corriente Actual :</div> 
             </td>               
            <td nowrap width="27%"> 
				<input type="text" name="E01CCMRPA" size="14" maxlength="14" value="<%= ccPvta.getE01CCMRPA().trim()%>" readonly>			
             </td>
           </tr>                                        
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>           
        </table>
      </td>
    </tr>
  </table> 
   <h4>Pac Multibanco </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                  
           <tr id="trdark">
            <td nowrap width="27%"> 
              <div align="right">Pac Multibanco : </div> 			
             </td> 
             <td> 
              <select name="E01SPVBLN" disabled>
                <option value='N' <% if (ccPvta.getField("E01SPVBLN").getString().trim().equals("") || ccPvta.getField("E01SPVBLN").getString().equals("N")) out.print("selected"); %> selected>NO</option>
                <option value='Y' <% if (ccPvta.getField("E01SPVBLN").getString().equals("Y")) out.print("selected");%>>SI</option>            			
              </select>				  
             </td>               
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
           </tr>                                       
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>           
        </table>
      </td>
    </tr>
  </table>   
    
 <%}%> 

<%-----------------------------------------------------------------------------------------Reseteo Clave ---------%> 
<% if (userPO.getOption().equals("8")) { %>   
 <h4>Reseteo Clave </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">      
           <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>   
           <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="23%"> 
              <div align="Center"><b> Reseteo de Clave </b></div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>             
            <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="25%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="27%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                                           
        </table>
      </td>
    </tr>
  </table>   
 <%}%>  
 <%-----------------------------------------------------------------------------------------Cambio Direccion envio estado cuenta ---------%> 
<% if (userPO.getOption().equals("9")) { %>  
 <h4>Direcci�n Envio Estado Cuenta </h4>         
  <table class="tableinfo">
    <tr > 
      <td nowrap width="1192"> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trclear"> 
            <td nowrap width="21%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="32%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="22%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="26%"> 
              <div align="right"> </div> 			
             </td>
          </tr>           
           <tr id="trdark">
            <td nowrap width="21%"> 
              <div align="right">Correlativo Direcci�n Anterior : </div> 			
             </td> 
              <td nowrap width="32%">  
               <input type="text" name="E01CCMMLA" size="3" maxlength="2" value="<%= ccPvta.getE01CCMMLA().trim()%>"> 
             </td> 
            <td nowrap width="22%"> 
              <div align="right">Correlativo Direcci�n Nuevo : </div> 			
             </td> 
              <td nowrap width="26%">  
               <input type="text" name="E01SPVMAN" size="3" maxlength="2" value="<%= ccPvta.getE01SPVMAN().trim()%>"> 
             </td>              
            </tr>           
           <tr id="trclear">
            <td nowrap width="21%"> 
              <div align="right">Direcci�n : </div> 			
             </td> 
              <td nowrap width="32%">  
               <input type="text" name="E01CCMDIR" size="35" maxlength="35" value="<%= ccPvta.getD01CCMDIR().trim()%>"> 
             </td> 
            <td nowrap width="22%"> 
              <div align="right">Direcci�n : </div> 			
             </td> 
              <td nowrap width="26%">  
               <input type="text" name="E01SPVDIR" size="35" maxlength="35" value="<%= ccPvta.getD01SPVDIR().trim()%>"> 
             </td>                
            </tr>
           <tr id="trdark">
            <td nowrap width="21%"> 
              <div align="right">Comuna : </div> 			
             </td> 
              <td nowrap width="32%">  
               <input type="text" name="E01CCMCOM" size="35" maxlength="35" value="<%= ccPvta.getD01CCMCOM().trim()%>"> 
             </td> 
            <td nowrap width="22%"> 
              <div align="right">Comuna : </div> 			
             </td> 
              <td nowrap width="26%">  
               <input type="text" name="E01SPVCOM" size="35" maxlength="35" value="<%= ccPvta.getD01SPVCOM().trim()%>"> 
             </td>                
            </tr>  
           <tr id="trclear">
            <td nowrap width="21%"> 
              <div align="right">Ciudad : </div> 			
             </td> 
              <td nowrap width="32%">  
               <input type="text" name="E01CCMCIU" size="35" maxlength="35" value="<%= ccPvta.getD01CCMCIU().trim()%>"> 
             </td> 
            <td nowrap width="22%"> 
              <div align="right">Ciudad : </div> 			
             </td> 
              <td nowrap width="26%">  
               <input type="text" name="E01SPVCIU" size="35" maxlength="35" value="<%= ccPvta.getD01SPVCIU().trim()%>"> 
             </td>                
            </tr>  
           <tr id="trdark">
            <td nowrap width="21%"> 
              <div align="right">Regi�n : </div> 			
             </td> 
              <td nowrap width="32%">  
               <input type="text" name="E01CCMREG" size="35" maxlength="35" value="<%= ccPvta.getD01CCMREG().trim()%>"> 
             </td> 
            <td nowrap width="22%"> 
              <div align="right">Regi�n : </div> 			
             </td> 
              <td nowrap width="26%">  
               <input type="text" name="E01SPVREG" size="35" maxlength="35" value="<%= ccPvta.getD01SPVREG().trim()%>"> 
             </td>                
            </tr>                                
            <tr id="trclear"> 
            <td nowrap width="21%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="32%"> 
              <div align="right"> </div>
            </td>
             <td nowrap width="22%"> 
              <div align="right"> </div>
            </td>
            <td nowrap width="26%"> 
              <div align="right"> </div> 			
             </td>
          </tr>                        
        </table>
      </td>
    </tr>
  </table>   
  <% } %>
  </form>
</body>
</html>
