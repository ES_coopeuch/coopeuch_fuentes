package com.datapro.eibs.security.struts.bean;

/**
 * @author ogarcia
 *
 */
public class JBMenuAccessItem {

   /**
    * The MNUIDN attribute.
   */
   protected int MNUIDN = 0;
   
   /**
    * Gets the MNUIDN value.
   */
   public int getMNUIDN() {
      return MNUIDN;
   }
   
   /**
    * Sets the MNUIDN value.
   */
   public void setMNUIDN(int MNUIDN) {
      this.MNUIDN = MNUIDN;
   }
   /**
    * The URL attribute.
   */
   protected java.lang.String URL = "";
   
   /**
    * Gets the URL value.
   */
   public java.lang.String getURL() {
      return URL.trim();
   }
   
   /**
    * Sets the URL value.
   */
   public void setURL(java.lang.String URL) {
      this.URL = URL;
   }
   /**
    * The IMG2 attribute.
   */
   protected java.lang.String IMG2 = "";
   
   /**
    * Gets the IMG2 value.
   */
   public java.lang.String getIMG2() {
      return IMG2.trim();
   }
   
   /**
    * Sets the IMG2 value.
   */
   public void setIMG2(java.lang.String IMG2) {
      this.IMG2 = IMG2;
   }
   /**
    * The IMG1 attribute.
   */
   protected java.lang.String IMG1 = "";
   
   /**
    * Gets the IMG1 value.
   */
   public java.lang.String getIMG1() {
      return IMG1.trim();
   }
   
   /**
    * Sets the IMG1 value.
   */
   public void setIMG1(java.lang.String IMG1) {
      this.IMG1 = IMG1;
   }
   /**
    * The NMEDEF attribute.
   */
   protected java.lang.String NMEDEF = "";
   
   /**
    * Gets the NMEDEF value.
   */
   public java.lang.String getNMEDEF() {
      return NMEDEF.trim();
   }
   
   /**
    * Sets the NMEDEF value.
   */
   public void setNMEDEF(java.lang.String NMEDEF) {
      this.NMEDEF = NMEDEF;
   }
   /**
    * The NMEKEY attribute.
   */
   protected java.lang.String NMEKEY = "";
   
   /**
    * Gets the NMEKEY value.
   */
   public java.lang.String getNMEKEY() {
      return NMEKEY.trim();
   }
   
   /**
    * Sets the NMEKEY value.
   */
   public void setNMEKEY(java.lang.String NMEKEY) {
      this.NMEKEY = NMEKEY;
   }
   /**
    * The MSTMNUIDN attribute.
   */
   protected int MSTMNUIDN = 0;
   
   /**
    * Gets the MSTMNUIDN value.
   */
   public int getMSTMNUIDN() {
      return MSTMNUIDN;
   }
   
   /**
    * Sets the MSTMNUIDN value.
   */
   public void setMSTMNUIDN(int MSTMNUIDN) {
      this.MSTMNUIDN = MSTMNUIDN;
   }
   /**
    * The STATUS attribute.
   */
   protected java.lang.String STATUS = "";
   
   /**
    * Gets the STATUS value.
   */
   public java.lang.String getSTATUS() {
      return STATUS.trim();
   }
   
   /**
    * Sets the STATUS value.
   */
   public void setSTATUS(java.lang.String STATUS) {
      this.STATUS = STATUS;
   }
   /**
    * The FLGLMTAMT attribute.
   */
   protected java.lang.String FLGLMTAMT = "";
   
   /**
    * Gets the FLGLMTAMT value.
   */
   public java.lang.String getFLGLMTAMT() {
      return FLGLMTAMT.trim();
   }
   
   /**
    * Sets the FLGLMTAMT value.
   */
   public void setFLGLMTAMT(java.lang.String FLGLMTAMT) {
      this.FLGLMTAMT = FLGLMTAMT;
   }
   /**
    * The FLGAPPLEV attribute.
   */
   protected java.lang.String FLGAPPLEV = "";
   
   /**
    * Gets the FLGAPPLEV value.
   */
   public java.lang.String getFLGAPPLEV() {
      return FLGAPPLEV.trim();
   }
   
   /**
    * Sets the FLGAPPLEV value.
   */
   public void setFLGAPPLEV(java.lang.String FLGAPPLEV) {
      this.FLGAPPLEV = FLGAPPLEV;
   }
   /**
    * The TARGET attribute.
   */
   protected java.lang.String TARGET = "";
   
   /**
    * Gets the TARGET value.
   */
   public java.lang.String getTARGET() {
      return TARGET.trim();
   }
   
   /**
    * Sets the TARGET value.
   */
   public void setTARGET(java.lang.String TARGET) {
      this.TARGET = TARGET;
   }

   /**
    * The CHECKED attribute.
   */
   protected int CHECKED = 0;
   
   /**
    * Gets the CHECKED value.
   */
   public int getCHECKED() {
      return CHECKED;
   }
   
   /**
    * Sets the CHECKED value.
   */
   public void setCHECKED(int CHECKED) {
      this.CHECKED = CHECKED;
   }

   /**
    * The CHECKED attribute.
   */
   protected boolean BOLCHK = false;
   
   /**
    * Gets the CHECKED value.
   */
   public boolean getBOLCHK() {
      return BOLCHK;
   }
   
   /**
    * Sets the CHECKED value.
   */
   public void setBOLCHK(boolean BOLCHK) {
      this.BOLCHK = BOLCHK;
   }

}
