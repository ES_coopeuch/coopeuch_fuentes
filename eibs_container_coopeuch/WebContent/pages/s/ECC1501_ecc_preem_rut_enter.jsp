<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Preemisiones Tarjeta de Cr&eacute;dito</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" 	scope="session" />


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT Language="javascript">
function CheckIDN(){
		if ( document.forms[0].E02CCDIDN.value.length < 1) 
		{
			alert("Debe Ingresar Identificación");
		}
		else
		{
			var rut = validateRut(document.getElementById("E02CCDIDN").value, <%=currUser.getE01INT()%>);
			
			if (rut == 0) 
			{
				alert("Identificación Erronea");
				document.forms[0].E02CCDIDN.value='';
				document.forms[0].E02CCDIDN.focus();
			} 
			else 
				document.forms[0].submit();
		}		
	} 

</SCRIPT>


</head>




<body bgcolor="#FFFFFF">

<H3 align="center">Consulta Detalle de Preemisiones por RUT	
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ECC_preem_rut_enter.jsp, ECC1501"></H3>

<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC1501">
  <p><INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="2100"></p>

  <table class="tbenter" HEIGHT="25%" width="100%" border="0">
    <tr> 
      <td nowrap width="45%"> 
         <div align="right"><B>Identificaci&oacute;n : </B> </div>
      </td>
      <td nowrap ALIGN="left"> 
        <INPUT type="text" name="E02CCDIDN" id="E02CCDIDN" size="25" maxlength="25" >
         <a href="javascript: GetCustomerDescId('', '', 'E02CCDIDN')"> 
         <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda"	border="0"></a>      
       </td>
     </tr>    
  </table>  
 <p align="center"> 
    <input id="EIBSBTN" type=button name="Submit" value="Enviar" onClick="CheckIDN()">
  </p>  
<script language="JavaScript">
  document.forms[0].E02CCDIDN.focus();
  document.forms[0].E02CCDIDN.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
  <%
 }
%> 
</form>
</body>
</html>
