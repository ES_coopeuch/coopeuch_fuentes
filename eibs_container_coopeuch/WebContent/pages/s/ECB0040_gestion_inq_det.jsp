<html>
<head> 
<title>Gestion de Cobranzas</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<%@ page import ="datapro.eibs.master.Util" %>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id= "gesCOB" class= "datapro.eibs.beans.ECB004001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" 	class= "datapro.eibs.beans.UserPos"  		scope="session"/>


<SCRIPT>


</SCRIPT>


<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">
            showErrors();
     </SCRIPT>
 <%
 }
%>
</head>
<body>

<H3 align="center">Gestion de Cobranzas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="gestion_inq_det,ECB0040"></H3>
<hr size="4">


<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECB0040">
 
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="400">
   <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="14%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="9%" > 
              <div align="left"> 
                <input type="text" name="E01GCMCUN" size="10" maxlength="9" readonly value="<%= gesCOB.getE01GCMCUN().trim()%>"readonly>
              </div>
            </td>
            <td nowrap width="12%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap > 
              <div align="left"> 
                <input type="text" name="E01CUSNA1" size="45" maxlength="45" readonly value="<%= gesCOB.getE01CUSNA1().trim()%>" readonly>
              </div>
            </td>
            <td nowrap > 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap ><b> 
              <input type="text" name="E01GCMPRO" size="4" maxlength="4" readonly value="<%= gesCOB.getE01GCMPRO().trim()%>" readonly>
              </b></td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="14%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="9%"> 
              <div align="left"> 
                <input type="text" name="E01GCMACC" size="13" maxlength="12" value="<%= gesCOB.getE01GCMACC().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="12%"> 
              <div align="right"><b>Oficial :</b></div>
            </td>
            <td nowrap width="33%"> 
              <div align="left"><b> 
                <input type="text" name="D01CUSOFC" size="45" maxlength="45" readonly value="<%= gesCOB.getD01CUSOFC().trim()%>" readonly>
                </b> </div>
            </td>
            <td nowrap width="11%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="21%"> 
              <div align="left"><b> 
                <input type="text" name="E01GCMCCY" size="3" maxlength="3" value="<%= gesCOB.getE01GCMCCY().trim()%>" readonly>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
   <h4>Datos del Cliente</h4>
  
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
			<tr id="trdark"> 
            <td nowrap > 
              <div align="right">Direcci&oacute;n :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01CUSNA2" size="45" maxlength="45" value="<%=  gesCOB.getE01CUSNA2().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Telefono 1 :</div>
            </td>
            <td nowrap >
           		<input type="text" name="E01CUSPHN" size="15" maxlength="15" value="<%=  gesCOB.getE01CUSPHN().trim()%>" readonly> 
           
            </td>
          </tr> 
            <tr id="trclear"> 
            <td nowrap > 
              <div align="right"></div>
            </td>
            <td nowrap > 
              <input type="text" name="E01CUSNA3" size="45" maxlength="45" value="<%= gesCOB.getE01CUSNA3().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Telefono 2 :</div>
            </td>
            <td nowrap >
           		<input type="text" name="E01CUSHPN" size="15" maxlength="15" value="<%= gesCOB.getE01CUSHPN().trim()%>" readonly> 
           
            </td>
          </tr>        
         <tr id="trdark"> 
            <td nowrap > 
              <div align="right"></div>
            </td>
            <td nowrap > 
              <input type="text" name="E01CUSNA4" size="45" maxlength="45" value="<%=  gesCOB.getE01CUSNA4().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Telefono 3 :</div>
            </td>
            <td nowrap >
           		<input type="text" name="E01CUSPH1" size="15" maxlength="15" value="<%=  gesCOB.getE01CUSPH1().trim()%>" readonly> 
           
            </td>
          </tr> 
        </table>
      </td>
    </tr>
  </table>
  <br/>
  <table class=tbenter>
   <tr > 
      <td nowrap> 
   		<h4>Sumario</h4>
      </td>
      <td nowrap align=right> 
   		<b>ESTADO :</b>
      </td>
      <td nowrap> 
   		<b><font color="#ff6600"><%=  gesCOB.getE01STATUS().trim()%></font></b>
      </td>
    </tr>
  </table>
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
			<tr id="trdark"> 
            <td nowrap > 
              <div align="right">Monto Liquidacion :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEAOAM" size="15" maxlength="15" value="<%=  gesCOB.getE01DEAOAM().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Saldo Prestamo :</div>
            </td>
            <td nowrap >
           		<input type="text" name="E01DEAMEP" size="15" maxlength="15" value="<%=  gesCOB.getE01DEAMEP().trim()%>" readonly> 
           
            </td>
          </tr> 
            <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Principal Vencido :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEAPDU" size="15" maxlength="15" value="<%=  gesCOB.getE01DEAPDU()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Interes Vencido :</div>
            </td>
            <td nowrap >
           		<input type="text" name="E01DEAIDU" size="15" maxlength="15" value="<%= gesCOB.getE01DEAIDU()%>" readonly> 
           
            </td>
          </tr>        
			<tr id="trdark"> 
             <td nowrap > 
              <div align="right">Fecha de Liquidacion :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEAOD1" size="3" maxlength="2" value="<%= gesCOB.getE01DEAOD1().trim()%>" readonly>
              <input type="text" name="E01DEAOD2" size="3" maxlength="2" value="<%= gesCOB.getE01DEAOD2().trim()%>" readonly>
              <input type="text" name="E01DEAOD3" size="5" maxlength="4" value="<%= gesCOB.getE01DEAOD3().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Fecha Vencimiento :</div>
            </td>
            <td nowrap >
           		 <input type="text" name="E01DEAMD1" size="3" maxlength="2" value="<%=  gesCOB.getE01DEAMD1().trim()%>" readonly>
             	 <input type="text" name="E01DEAMD2" size="3" maxlength="2" value="<%=  gesCOB.getE01DEAMD2().trim()%>" readonly>
            	  <input type="text" name="E01DEAMD3" size="5" maxlength="4" value="<%= gesCOB.getE01DEAMD3().trim()%>" readonly>
           		
           
            </td>
          </tr>
          
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Ciclo de Pago de Capital :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEAPPD" size="5" maxlength="3" value="<%= gesCOB.getE01DEAPPD().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Ciclo de Pago de Interes :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEAIPD" size="5" maxlength="3" value="<%= gesCOB.getE01DEAIPD().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Cuenta Cte/Ahorro :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEARAC" size="15" maxlength="15" value="<%=  gesCOB.getE01DEARAC().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Saldo Cuenta :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01ACMMGB" size="15" maxlength="15" value="<%=  gesCOB.getE01ACMMGB().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Monto Bloqueado :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01ACMHAM" size="15" maxlength="15" value="<%=  gesCOB.getE01ACMHAM().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Saldo Disponible :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01ACMMNB" size="15" maxlength="15" value="<%=  gesCOB.getE01ACMMNB().trim()%>" readonly>
              
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Tipo de Garantia :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01COLATR" size="15" maxlength="15" value="<%=  gesCOB.getE01COLATR().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Ultima Tasa Inter&eacute;s :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01NOWRTE" size="9" maxlength="9" value="<%=  gesCOB.getE01NOWRTE().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Linea de Credito :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01DEACMN" size="15" maxlength="15" value="<%=  gesCOB.getE01DEACMN().trim()%>" readonly>
            </td>
            <td nowrap > 
              <div align="right">Saldo Total :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E01TOTPYM" size="15" maxlength="15" value="<%=  gesCOB.getE01TOTPYM().trim()%>" readonly>
            </td>
          </tr>
          <tr id="trclear">
            <td nowrap >
              <div align="right">Fecha Vcto Documento :</div>
            </td>
            <td nowrap >
               <input type="text" name="E01DEAEX1" size="3" maxlength="2" value="<%=  gesCOB.getE01DEAEX1().trim()%>" readonly>
             	<input type="text" name="E01DEAEX2" size="3" maxlength="2" value="<%=  gesCOB.getE01DEAEX2().trim()%>" readonly>
            	<input type="text" name="E01DEAEX3" size="5" maxlength="4" value="<%=  gesCOB.getE01DEAEX3().trim()%>" readonly>
           	
            </td>
            
          </tr>
         
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Informaci&oacute;n Adicional</h4>
  
  <table class="tableinfo">
   <tr> 
   <td>
    <table cellspacing=0 cellpadding=2 width="100%" border="0">
    	<tr id=trdark> 
	      <td nowrap width="20%"> 
	        <div align="right">Usuario : </div>
	      </td>
	      <td nowrap> 
	        <input type="text" name="E01GCMUSR" size="12" maxlength="10" readonly value="<%= gesCOB.getE01GCMUSR().trim() %>">
	      </td>
     	</tr>    	
     	<tr id=trclear> 
	      <td nowrap width="20%"> 
	        <div align="right">C&oacute;digo : </div>
	      </td>
	      <td nowrap> 
	        <input type="text" name="E01GCMACC" size="12" maxlength="12" readonly value="<%= gesCOB.getE01GCMACC().trim() %>">
	      </td>
     	</tr> 
    	<tr id=trdark> 
	      <td nowrap width="20%"> 
	        <div align="right">Secuencia : </div>
	      </td>
	      <td nowrap> 
	        <input type="text" name="E01GCMSEQ" size="6" maxlength="5" readonly value="<%= gesCOB.getE01GCMSEQ().trim() %>">
	      </td>
     	</tr>      	
     	<tr id=trclear> 
	      <td nowrap width="20%"> 
	        <div align="right">Nombre : </div>
	      </td>
	      <td nowrap> 
	        <input type="text" name="E01CUSNA1" size="45" maxlength="45" readonly value="<%= gesCOB.getE01CUSNA1().trim() %>">
	      </td>
     	</tr>
     	<tr id=trdark> 
	      <td nowrap width="20%"> 
	        <div align="right">R.U.T : </div>
	      </td>
	      <td nowrap> 
	        <input type="text" name="E01CUSIDN" size="18" maxlength="15" readonly value="<%= gesCOB.getE01CUSIDN().trim() %>">
	      </td>  
	      </tr>
	    <tr id=trclear> 
	      <td nowrap width="20%"> 
	       	<div align="right">C&oacute;digo de Cobrador :</div>
	      </td>
     	  <td nowrap >                
		 	<input type="text" name="E01GCMCOD" size="4" maxlength="5" readonly value="<%= gesCOB.getE01GCMCOD().trim()%>">
		 	<input type="text" name="E01GCMCNAM" size="45" maxlength="45" readonly value="<%= gesCOB.getE01GCMCNAM().trim()%>">
          </td>
        </tr>    
     	    	
      	<tr id=trdark> 
	      <td nowrap width="20%"> 
	        <div align="right">Gesti&oacute;n de Cobranza : </div>
	      </td>
	      <td nowrap> 
	        <input type="text" name="E01GCMMA0" size="90" maxlength="80" readonly value="<%= gesCOB.getE01GCMMA0().trim() %>">
	      </td>
     	</tr>
     	     	
     	<tr id=trclear> 
	      <td nowrap width="20%"> 
	        <div align="right"> </div>
	      </td>
	      <td nowrap>
	       <input type="text" name="E01GCMMA1" size="90" maxlength="80" readonly value="<%= gesCOB.getE01GCMMA1().trim() %>"> 
	        
	      </td>
     	</tr>
     	<tr id=trdark> 
	      <td nowrap> 
	        <div align="right"></div>
	      </td>
	      <td nowrap>
	       <input type="text" name="E01GCMMA2" size="90" maxlength="80" readonly value="<%= gesCOB.getE01GCMMA2().trim() %>">
	      	
      	  </td>
     	</tr>
     	<tr id=trclear> 
	      <td nowrap> 
	        <div align="right"></div>
	      </td>
	      <td nowrap>
	       <input type="text" name="E01GCMMA3" size="90" maxlength="80" readonly value="<%= gesCOB.getE01GCMMA3().trim() %>">
	      	
      	  </td>
     	</tr>
     	<tr id=trdark> 
	      <td nowrap> 
	        <div align="right"></div>
	      </td>
	      <td nowrap>
	       <input type="text" name="E01GCMMA4" size="90" maxlength="80" readonly value="<%= gesCOB.getE01GCMMA4().trim() %>">
	      	
      	  </td>
     	</tr>
     	<tr id=trclear> 
	      <td nowrap> 
	        <div align="right"></div>
	      </td>
	      <td nowrap>
	       <input type="text" name="E01GCMMA5" size="90" maxlength="80" readonly value="<%= gesCOB.getE01GCMMA5().trim()%>">
	      	
      	  </td>
     	</tr>
     	<tr id=trdark> 
	      <td nowrap> 
	        <div align="right"></div>
	      </td>
	      <td nowrap>
	       <input type="text" name="E01GCMMA6" size="90" maxlength="80" readonly value="<%=gesCOB.getE01GCMMA6().trim() %>">
	      	
      	  </td>
     	</tr>
     	<tr id=trclear> 
	      <td nowrap> 
	        <div align="right"></div>
	      </td>
	      <td nowrap>
	       <input type="text" name="E01GCMMA7" size="90" maxlength="80" readonly value="<%= gesCOB.getE01GCMMA7().trim() %>">
	      	
      	  </td>
     	</tr>
     	<tr id=trdark> 
	      <td nowrap> 
	        <div align="right"></div>
	      </td>
	      <td nowrap>
	       <input type="text" name="E01GCMMA8" size="90" maxlength="80" readonly value="<%= gesCOB.getE01GCMMA8().trim() %>">
	      	
      	  </td>
     	</tr>
     	<tr id=trclear> 
	      <td nowrap> 
	        <div align="right"></div>
	      </td>
	      <td nowrap>
	       <input type="text" name="E01GCMMA9" size="90" maxlength="80" readonly value="<%= gesCOB.getE01GCMMA9().trim() %>">
	      	
      	  </td>
     	</tr>
     </table>
    </td>
   </tr>
  </table>
  
</form> 
</body>
</html>
