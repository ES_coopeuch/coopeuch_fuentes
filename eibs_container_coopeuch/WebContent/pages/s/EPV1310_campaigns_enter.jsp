
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV131001Message"%>

<html>
<head>
<title>Consulta de Campa&ntilde;as</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="date" class="java.util.Date" scope="session" />      
<jsp:useBean id= "bank" class= "datapro.eibs.beans.EPV131001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">
 
function addDate(){
date = new Date();
var month_i = date.getMonth();
var day_i = date.getDate();
var year_i = date.getFullYear();

date.setMonth(date.getMonth() + 1);

var month_f = date.getMonth();
var day_f = date.getDate();
var year_f = date.getFullYear();



if (document.getElementById('fecha1').value == ''){
document.getElementById('fecha1').value = day_i;
}

if (document.getElementById('fecha2').value == ''){
document.getElementById('fecha2').value = month_i;
}


if (document.getElementById('fecha3').value == ''){
document.getElementById('fecha3').value = year_i;
}

if (document.getElementById('fecha4').value == ''){
document.getElementById('fecha4').value = day_f;
}


if (document.getElementById('fecha5').value == ''){
document.getElementById('fecha5').value = month_f;
}

if (document.getElementById('fecha6').value == ''){
document.getElementById('fecha6').value = year_f;
}


}
</script>
 
 

</head>

<body  onload="addDate();">
 
<H3 align="center">Consulta de Campa&ntilde;as<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="campaigns_enter.jsp, EPV1310"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1310"   >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">

<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap>
		<table cellspacing="0" align="center" cellpadding="2" width="110%"
			border="0" class="tbhead">
			<tr>
				<td nowrap width="10%" align="right">Id de Carga :</td>
				<td nowrap align="left" width="10%">
				<div align="left"><input type="text" name="E01CMPIDC"
					size="13" maxlength="43"></div>
				</td>
				<td nowrap align="right">Fecha Desde :</td>
				<td nowrap align="left" width="15%">
				<div align="left">
					<input type="text" name="E01FVIGDD" id="fecha1" size="3" maxlength="2" value="" readonly="readonly"> 
					<input type="text" name="E01FVIGDM" id="fecha2" size="3" maxlength="2" value="" readonly="readonly"> 
					<input type="text" name="E01FVIGDA" id="fecha3" size="5" maxlength="4" value="" readonly="readonly"> 
					<a 	href="javascript:DatePicker(document.forms[0].E01FVIGDD,document.forms[0].E01FVIGDM,document.forms[0].E01FVIGDA)">
					<img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a>
				</div>
				</td>
				<td nowrap align="right" width="9%">Fecha Hasta :</td>
				<td nowrap align="left" width="49%">
				<div align="left">
					<input type="text" name="E01FVIGHD" id="fecha4" size="3" maxlength="2" value="" readonly="readonly">  
					<input type="text" name="E01FVIGHM" id="fecha5" size="3" maxlength="2" value="" readonly="readonly">  
					<input type="text" name="E01FVIGHA" id="fecha6" size="5" maxlength="4" value="" readonly="readonly">  
					<a 	href="javascript:DatePicker(document.forms[0].E01FVIGHD,document.forms[0].E01FVIGHM,document.forms[0].E01FVIGHA)">
					<img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a>
				</div>
				</td>
			</tr>
		</table>
</td>
</tr>
</table>
		<p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
<script language="JavaScript">
  document.forms[0].E01CMPIDC.focus();
  document.forms[0].E01CMPIDC.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
