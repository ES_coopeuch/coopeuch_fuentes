<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.datapro.constants.Entities"%> 
<%@ page import="datapro.eibs.master.Util"%>


<%@page import="datapro.eibs.beans.EXP006001Message"%>
<%@page import="datapro.eibs.beans.EXP006002Message"%><html>
<head>
<title>Productos Externos</title>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<jsp:useBean id="EXP0060List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id= "externos" class= "datapro.eibs.beans.EXP006001Message"  scope="session" /> 
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<SCRIPT SRC="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT SRC="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script Language="Javascript">

</script>

</head>

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }    
 %> 

<h3 align="center">Consulta de Productos Externos <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="remanenetes_basic_inquiry.jsp,EXP0060"></h3>
<hr size="4">

<form  method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXP0060">
<input type="hidden" name="SCREEN" value="400">

<input type="hidden" name="E01EXPACD" value="<%= externos.getField("E01EXPACD").getString().trim()%>">
<input type="hidden" name="E01EXPTYP" value="<%= externos.getField("E01EXPTYP").getString().trim()%>">
<input type="hidden" name="E01EXPGLN" value="<%= externos.getField("E01EXPGLN").getString().trim()%>">

 <% int row = 0;%>

 <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left">
				<eibsinput:text name="externos" property="E01EXPCUN"  eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" />
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left">
	  			<eibsinput:text name="externos" property="E01CUSNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
              </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left">
      			<eibsinput:text name="externos" property="E01EXPACC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true"/>
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
	  			<eibsinput:text name="externos" property="E01EXPCCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>" readonly="true"/>
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b>
	  			<eibsinput:text name="externos" property="E01EXPPRO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>" readonly="true"/>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
 <h4>Información de Saldos</h4>
 <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Saldo Principal :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPPRI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap width="20%" > 
              <div align="right">Ultima Calculo :</div>
            </td>
            <td nowrap width="30%" > 
    	        <eibsinput:date name="externos" fn_year="E01EXPLCY" fn_month="E01EXPLCM" fn_day="E01EXPLCD" readonly="true"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Saldo Reajuste :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPREA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap width="20%" > 
              <div align="right">Periodo Base :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPBAS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="3" readonly="true"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Saldo Intereses :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPINT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap width="20%" > 
              <div align="right">Tasa de Intereses :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPRTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Saldo Mora :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPMOR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap width="20%" > 
              <div align="right">Tabla Tasa Flotante :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPFTB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR %>" size="2" readonly="true"/>
 		        <eibsinput:text name="externos" property="E01EXPFTY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR %>" size="2" readonly="true"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Saldo Deuda :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01TOTBAL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap width="20%" > 
              <div align="right">Termino :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPTRM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TERM %>"  readonly="true"/>
 		        <eibsinput:text name="externos" property="E01EXPTRC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR %>" size="2" readonly="true"/>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>

  
 <h4>Información Basica</h4>
 <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing=0 cellpadding=2 width="100%" border="0">
       
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Tipo Producto :</div>
            </td>
            <td nowrap width="30%" > 
                 <eibsinput:text name="externos" property="E01EXPTYP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>
                 <eibsinput:text name="externos" property="E01DSCTYP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>
            </td>
            <td nowrap width="20%" > 
              <div align="right">Fecha Vencimiento :</div>
            </td>
            <td nowrap width="30%" > 
    	        <eibsinput:date name="externos" fn_year="E01EXPMAY" fn_month="E01EXPMAM" fn_day="E01EXPMAD" readonly="true"/>
            </td>
          </tr>
       
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Monto Inicial :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPOAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap width="20%" > 
              <div align="right">Fecha Apertura :</div>
            </td>
            <td nowrap width="30%" > 
    	        <eibsinput:date name="externos" fn_year="E01EXPODY" fn_month="E01EXPODM" fn_day="E01EXPODD" readonly="true"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Banco/Sucursal :</div>
            </td>
            <td nowrap width="30%" > 
		        <eibsinput:text name="externos" property="E01EXPBNK" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BANK %>" readonly="true" />
	            <eibsinput:text name="externos" property="E01EXPBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH%>" readonly="true" />
            </td>
            <td nowrap width="20%" > 
              <div align="right">Referencia :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPREF" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>"  readonly="true"/>
            </td>
          </tr>

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" > 
              <div align="right">Ejecutivo :</div>
            </td>
            <td nowrap width="30%" > 
                 <eibsinput:text name="externos" property="E01EXPOFI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>
                 <eibsinput:text name="externos" property="E01DSCOFI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>
            </td>
            <td nowrap width="20%" > 
              <div align="right">Centro de Costos :</div>
            </td>
            <td nowrap width="30%" > 
 		        <eibsinput:text name="externos" property="E01EXPCCN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_COST_CENTER %>"  readonly="true"/>
            </td>
          </tr>

        </table>
      </td>
    </tr>
  </table>


  
<h4>Plan de Pagos</h4>

<% 
 int k = 0;
	if (EXP0060List.getNoResult()) {
%>
<TABLE class="tbenter" width=100% >
	<TR>
		<TD>
		<div align="center"><font size="3"><b> No hay Plan de pagos Registrado. </b></font></div>
		</TD>
	</TR>
</TABLE>
<%
	} else {
%>
 <table id="mainTable" class="tableinfo" align="center"  width=100%>
	<tr>
		<td nowrap valign="top">

		<TABLE id="headTable" width=100%>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
				<th align="center" nowrap width="10%">Fecha</th>
				<th align="center" nowrap width="10%">Numero</th>
				<th align="center" nowrap width="10%">Principal</th>
				<th align="center" nowrap width="10%">Reajuste</th>
				<th align="center" nowrap width="10%">Intereses</th>
				<th align="center" nowrap width="10%">Mora</th>
				<th align="center" nowrap width="10%">Estatus</th>
				<th align="center" nowrap width="10%">Fecha Pago</th>

    		</TR>
			<%
				EXP0060List.initRow();
				k = 0;
				while (EXP0060List.getNextRow()) {
					EXP006002Message convObj = (EXP006002Message) EXP0060List.getRecord();
			%>
	          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
				<td nowrap align="right" width="10%">
					<%=Util.formatCell(convObj.getE02EXDPDD() + "/" + convObj.getE02EXDPDM() + "/"+ convObj.getE02EXDPDY())%>
				</td>
				<td nowrap align="right" width="10%"><%= convObj.getE02EXDPNU()%></td>
				<td nowrap align="right" width="10%"><%= convObj.getE02EXDPPM()%></td>
				<td nowrap align="right" width="10%"><%= convObj.getE02EXDRPM()%></td>
				<td nowrap align="right" width="10%"><%= convObj.getE02EXDIPM()%></td>
				<td nowrap align="right" width="10%"><%= convObj.getE02EXDPIA()%></td>
				<td nowrap align="center" width="10%"><%= convObj.getE02EXDPFL()%></td>
				<td nowrap align="right" width="10%">
					<%=Util.formatCell(convObj.getE02EXDDTD() + "/" + convObj.getE02EXDDTM() + "/"+ convObj.getE02EXDDTY())%>
				</td>
    		</TR>
			<% 
					k++;
				}
			%>
			 </table>
		</td>
	</tr>
</table>

<% } %>

	
</form>
</body>
</html>
