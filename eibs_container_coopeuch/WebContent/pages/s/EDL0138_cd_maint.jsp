<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Reinversion de Certificados de Deposito</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<%@ page import = "java.io.*,java.net.*,datapro.eibs.beans.*,datapro.eibs.master.*,java.math.*" %>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id="cdMant" class="datapro.eibs.beans.EDL013801Message"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<SCRIPT LANGUAGE="javascript">
 function CheckSubmit(act)
{
   document.forms[0].ACTION.value=act;
   document.forms[0].submit();
}


</SCRIPT>
<SCRIPT Language="Javascript">

  builtNewMenu(cd_i_opt);
  initMenu(); 
    
</SCRIPT>


</head>
<body >

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }
%>
<div align="center"></div>
<h3 align="center">Reinversion de Certificados de Depositos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cd_maint, EDL0138"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL0138" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="300">
  <INPUT TYPE=HIDDEN NAME="ACTION" VALUE="F">
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="0" width="100%" >
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left">
                <input type="text" name="E01DEACUN" size="10" maxlength="9" value="<%= cdMant.getE01DEACUN().trim()%>">
                </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left">
                <input type="text" name="E01CUSNA1" size="45" maxlength="45" value="<%= cdMant.getE01CUSNA1().trim()%>" readonly>
                </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>


<h4>Contrato Original</h4>
<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr id="trclear">
				<td nowrap>
				<div align="right">Contrato :</div>
				</td>
				<td nowrap>
				<div align="left"><input type="text" name="E01OLDACC" size="13" maxlength="12" value="<%=cdMant.getE01OLDACC().trim()%>" readonly>
				</div>
				</td>
				<td nowrap>
				<div align="right">Oficial : </div>
				</td>
				<td nowrap>
				<div align="left"><input type="text" name="D01OLDOFC" size="48" maxlength="45" readonly value="<%=cdMant.getD01OLDOFC()  .trim()%>"> </div>
				</td>
				</tr>
			<tr id="trdark">
				<td nowrap>
				<div align="right">Moneda :</div>
				</td>
				<td nowrap>
				<div align="left"><input type="text" name="E01OLDCCY" size="4" maxlength="3" value="<%=cdMant.getE01OLDCCY().trim()%>"
					readonly> </div>
				</td>
				<td nowrap>
				<div align="right">Producto : </div>
				</td>
				<td nowrap> 
				<input type="text" name="E01OLDPRO" size="5" maxlength="4" readonly value="<%=cdMant.getE01OLDPRO().trim()%>">
				</td>				
			</tr>
			<tr id="trclear">
				<td nowrap width="25%">
				<div align="right">Fecha de Apertura :</div>
				</td>
				<td nowrap width="23%">
				<input type="text" name="E01OLDOD1" size="3" maxlength="2" value="<%=cdMant.getE01OLDOD1() .trim()%>" readonly> 
				<input type="text" name="E01OLDOD2" size="3" maxlength="2" value="<%=cdMant.getE01OLDOD2().trim()%>" readonly> 
				<input type="text" name="E01OLDOD3" size="5" maxlength="4" value="<%=cdMant.getE01OLDOD3().trim()%>" readonly>
				</td>
				<td nowrap width="26%">
				<div align="right">Fecha de Vencimiento :</div>
				</td>
				<td nowrap width="26%">
				<input type="text" name="E01OLDMD1" size="3" maxlength="2" value="<%=cdMant.getE01OLDMD1().trim()%>" readonly> 
				<input type="text" name="E01OLDMD2" size="3" maxlength="2" value="<%=cdMant.getE01OLDMD2().trim()%>" readonly> 
				<input type="text" name="E01OLDMD3" size="5" maxlength="4" value="<%=cdMant.getE01OLDMD3().trim()%>" readonly>
				</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="25%">
				<div align="right">Monto Original :</div>
				</td>
				<td nowrap width="23%">
				<eibsinput:text name="cdMant" property="E01OLDOAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
				</td>
				<td nowrap width="26%">
				<div align="right">T&eacute;rmino :</div>
				</td>
				<td nowrap width="26%">
				<input type="text" name="E01OLDTRM" size="5" maxlength="5" value="<%=cdMant.getE01OLDTRM().trim()%>" readonly> 
				<input type="text" name="E01OLDTRC" size="10" value='<%if (cdMant.getE01OLDTRC().equals("D")) out.print("D&iacute;a(s)");
																		else if (cdMant.getE01OLDTRC().equals("M"))	out.print("Mes(es)");
																		else if (cdMant.getE01OLDTRC().equals("Y")) out.print("A&ntilde;o(s)");
																		else out.print("");%>' readonly>
				</td>
			</tr>			
			<tr id="trclear">
				<td nowrap width="25%">
				<div align="right">Monto Reajuste :</div>
				</td>
				<td nowrap width="23%">
				<eibsinput:text name="cdMant" property="E01OLDREA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
				</td>
				<td nowrap width="26%">
				<div align="right">Tasa Interes :</div>
				</td>
				<td nowrap width="26%">
				<eibsinput:text name="cdMant" property="E01OLDRTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true" />
				</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="25%">
				<div align="right">Monto Intereses :</div>
				</td>
				<td nowrap width="23%">
				<eibsinput:text name="cdMant" property="E01OLDMEI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
				</td>
				<td nowrap width="26%">
				<div align="right">Per&iacute;odo Base :</div>
				</td>
				<td nowrap width="26%">
				<input type="text" name="E01OLDBAS" size="4" maxlength="3" value="<%=cdMant.getE01OLDBAS().trim()%>" readonly></td>
			</tr>
			<tr id="trclear">
            	<td nowrap width="25%" >
					<div align="right">Monto Final :</div>
            	</td> 
     	    	<td>
					<eibsinput:text name="cdMant" property="E01OLDBAL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true" />
     			</td>
				<td nowrap width="26%">
				<div align="right">Tasa Periodo :</div>
				</td>
				<td nowrap width="26%">
				<eibsinput:text name="cdMant" property="E01OLDIRT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE %>" readonly="true" />
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<h4>Reinversion</h4>
  <table class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
			<tr id="trclear">
				<td nowrap>
				<div align="right"> Nuevo Contrato : </div>
				</td>
				<td nowrap>
				<div align="left"><input type="text" name="E01DEAACC" size="13" maxlength="12" value="<%=cdMant.getE01DEAACC().trim()%>" readonly>
				</div>
				</td>
            <td nowrap width="25%" > 
              <div align="right">N&uacute;mero Referencia :</div>
            </td>
            <td nowrap width="27%" > 
              <input type="text" name="E01DEAREF" size="13" maxlength="12" value="<%= cdMant.getE01DEAREF().trim()%>">
            </td> 				
				</tr>           
          <tr id="trdark"> 
            <td nowrap width="25%" > 
              <div align="right">Monto Original :</div>
            </td>
            <td nowrap width="23%" > 
                <eibsinput:text name="cdMant" property="E01DEAOAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>"  required="false" readonly="true"/>
            </td>
            <td nowrap width="25%"> 
              <div align="right">Tasa Inter&eacute;s/Spread :</div>
            </td>
            <td nowrap width="27%"> 
              <input type="text" name="E01DEARTE" id="txtright" size="10" maxlength="9" value="<%= cdMant.getE01DEARTE().trim()%>" >
            </td>         
          </tr>
          <tr id="trclear"> 
             <td nowrap width="25%"> 
              <div align="right">Fecha de Apertura :</div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" name="E01DEAOD1" size="3" maxlength="2" value="<%= cdMant.getE01DEAOD1().trim()%>" readonly>
              <input type="text" name="E01DEAOD2" size="3" maxlength="2" value="<%= cdMant.getE01DEAOD2().trim()%>" readonly>
              <input type="text" name="E01DEAOD3" size="5" maxlength="4" value="<%= cdMant.getE01DEAOD3().trim()%>" readonly>
            </td>            
            <td nowrap width="25%"> 
              <div align="right">Fecha de Vencimiento :</div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" name="E01DEAMD1" size="3" maxlength="2" value="<%= cdMant.getE01DEAMD1().trim()%>">
              <input type="text" name="E01DEAMD2" size="3" maxlength="2" value="<%= cdMant.getE01DEAMD2().trim()%>">
              <input type="text" name="E01DEAMD3" size="5" maxlength="4" value="<%= cdMant.getE01DEAMD3().trim()%>">
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="absbottom" border="0" > 
            </td>
          </tr>
          <tr id="trdark">     
            <td nowrap width="25%" > 
            </td>   
            <td nowrap width="25%" > 
            </td>                 
            <td nowrap width="25%"> 
              <div align="right">T&eacute;rmino :</div>
            </td>
            <td nowrap width="27%"> 
              <input type="text" name="E01DEATRM" size="6" maxlength="5" value="<%= cdMant.getE01DEATRM().trim()%>">
              <select name="E01DEATRC">
                <option value=" " <% if (!(cdMant.getE01DEATRC().equals("D") ||cdMant.getE01DEATRC().equals("M")||cdMant.getE01DEATRC().equals("Y"))) out.print("selected"); %>></option>
                <option value="D" <% if(cdMant.getE01DEATRC().equals("D")) out.print("selected");%>>D&iacute;a(s)</option>
                <option value="M" <% if(cdMant.getE01DEATRC().equals("M")) out.print("selected");%>>Mes(es)</option>
                <option value="Y" <% if(cdMant.getE01DEATRC().equals("Y")) out.print("selected");%>>A&ntilde;o(s)</option>
              </select>
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="campo obligatorio" align="absbottom" border="0" > 
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap width="25%" > 
            </td>
            <td nowrap width="23%" >
            </td>                      
            <td nowrap width="25%" > 
              <div align="right">Cuenta de Transito :</div>
            </td>
            <td nowrap width="23%" >
              <DIV align="left"> 
                <INPUT type="text" name="E01DEACTR" size="16" maxlength="16" value="<%= cdMant.getE01DEACTR() %>" onkeypress="enterInteger()">
        		<A href="javascript:GetLedger('E01DEACTR','<%= cdMant.getE01DEABNK() %>','<%= cdMant.getE01OLDCCY() %>','')">
		        <IMG src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="absbottom" border="0"></A>  
              </DIV>                  
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

 <SCRIPT language="javascript">
    function tableresize() {
     adjustEquTables(headTable,dataTable,dataDiv,0,true);
   }
  tableresize();
  window.onresize=tableresize;
  </SCRIPT>
  <% if(error.getERWRNG().equals("Y")){%>
   <h4 style="text-align:center"><input type="checkbox" name="H01FLGWK2" value="A" <% if(cdMant.getH01FLGWK2().equals("A")){ out.print("checked");} %>>
      Aceptar con Aviso</h4>
  <% } %>         
  <p align="center"> 
    <input id="EIBSBTN" type=button name="Submit"  onClick="CheckSubmit('F')" value="Enviar">
  </p>

  
  </form>
</body>
</html>
