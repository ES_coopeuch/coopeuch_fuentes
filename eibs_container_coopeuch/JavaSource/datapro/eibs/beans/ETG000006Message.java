package datapro.eibs.beans;

import datapro.eibs.sockets.*;
import java.io.*;
import java.math.*;

import java.util.*;

/**
* Class generated by AS/400 CRTCLASS command from ETG000006 physical file definition.
* 
* File level identifier is 1151215152500.
* Record format level identifier is 411F013A260FA.
*/

public class ETG000006Message extends MessageRecord
{
  final static String fldnames[] = {
                                     "H06USERID",
                                     "H06PROGRM",
                                     "H06TIMSYS",
                                     "H06SCRCOD",
                                     "H06OPECOD",
                                     "H06FLGMAS",
                                     "H06FLGWK1",
                                     "H06FLGWK2",
                                     "H06FLGWK3",
                                     "ETG06TAB",
                                     "ETG06ELE",
                                     "ETG06VAL01",
                                     "ETG06VAL02",
                                     "ETG06VAL03",
                                     "ETG06VAL04",
                                     "ETG06VAL05",
                                     "ETG06VAL06",
                                     "ETG06VAL07",
                                     "ETG06ALFA1",
                                     "ETG06NREC",
                                     "ETG06NPAG"
                                   };
  final static String tnames[] = {
                                   "H06USERID",
                                   "H06PROGRM",
                                   "H06TIMSYS",
                                   "H06SCRCOD",
                                   "H06OPECOD",
                                   "H06FLGMAS",
                                   "H06FLGWK1",
                                   "H06FLGWK2",
                                   "H06FLGWK3",
                                   "ETG06TAB",
                                   "ETG06ELE",
                                   "ETG06VAL01",
                                   "ETG06VAL02",
                                   "ETG06VAL03",
                                   "ETG06VAL04",
                                   "ETG06VAL05",
                                   "ETG06VAL06",
                                   "ETG06VAL07",
                                   "ETG06ALFA1",
                                   "ETG06NREC",
                                   "ETG06NPAG"
                                 };
  final static String fid = "1151215152500";
  final static String rid = "411F013A260FA";
  final static String fmtname = "ETG000006";
  final int FIELDCOUNT = 21;
  private static Hashtable tlookup = new Hashtable();
  private CharacterField fieldH06USERID = null;
  private CharacterField fieldH06PROGRM = null;
  private CharacterField fieldH06TIMSYS = null;
  private CharacterField fieldH06SCRCOD = null;
  private CharacterField fieldH06OPECOD = null;
  private CharacterField fieldH06FLGMAS = null;
  private CharacterField fieldH06FLGWK1 = null;
  private CharacterField fieldH06FLGWK2 = null;
  private CharacterField fieldH06FLGWK3 = null;
  private DecimalField fieldETG06TAB = null;
  private CharacterField fieldETG06ELE = null;
  private DecimalField fieldETG06VAL01 = null;
  private DecimalField fieldETG06VAL02 = null;
  private DecimalField fieldETG06VAL03 = null;
  private DecimalField fieldETG06VAL04 = null;
  private DecimalField fieldETG06VAL05 = null;
  private DecimalField fieldETG06VAL06 = null;
  private DecimalField fieldETG06VAL07 = null;
  private CharacterField fieldETG06ALFA1 = null;
  private DecimalField fieldETG06NREC = null;
  private DecimalField fieldETG06NPAG = null;

  /**
  * Constructor for ETG000006Message.
  */
  public ETG000006Message()
  {
    createFields();
    initialize();
  }

  /**
  * Create fields for this message.
  * This method implements the abstract method in the MessageRecord superclass.
  */
  protected void createFields()
  {
    recordsize = 163;
    fileid = fid;
    recordid = rid;
    message = new byte[getByteLength()];
    formatname = fmtname;
    fieldnames = fldnames;
    tagnames = tnames;
    fields = new MessageField[FIELDCOUNT];

    fields[0] = fieldH06USERID =
    new CharacterField(message, HEADERSIZE + 0, 10, "H06USERID");
    fields[1] = fieldH06PROGRM =
    new CharacterField(message, HEADERSIZE + 10, 10, "H06PROGRM");
    fields[2] = fieldH06TIMSYS =
    new CharacterField(message, HEADERSIZE + 20, 12, "H06TIMSYS");
    fields[3] = fieldH06SCRCOD =
    new CharacterField(message, HEADERSIZE + 32, 2, "H06SCRCOD");
    fields[4] = fieldH06OPECOD =
    new CharacterField(message, HEADERSIZE + 34, 4, "H06OPECOD");
    fields[5] = fieldH06FLGMAS =
    new CharacterField(message, HEADERSIZE + 38, 1, "H06FLGMAS");
    fields[6] = fieldH06FLGWK1 =
    new CharacterField(message, HEADERSIZE + 39, 1, "H06FLGWK1");
    fields[7] = fieldH06FLGWK2 =
    new CharacterField(message, HEADERSIZE + 40, 1, "H06FLGWK2");
    fields[8] = fieldH06FLGWK3 =
    new CharacterField(message, HEADERSIZE + 41, 1, "H06FLGWK3");
    fields[9] = fieldETG06TAB =
    new DecimalField(message, HEADERSIZE + 42, 4, 0, "ETG06TAB");
    fields[10] = fieldETG06ELE =
    new CharacterField(message, HEADERSIZE + 46, 8, "ETG06ELE");
    fields[11] = fieldETG06VAL01 =
    new DecimalField(message, HEADERSIZE + 54, 12, 2, "ETG06VAL01");
    fields[12] = fieldETG06VAL02 =
    new DecimalField(message, HEADERSIZE + 66, 12, 2, "ETG06VAL02");
    fields[13] = fieldETG06VAL03 =
    new DecimalField(message, HEADERSIZE + 78, 12, 2, "ETG06VAL03");
    fields[14] = fieldETG06VAL04 =
    new DecimalField(message, HEADERSIZE + 90, 11, 2, "ETG06VAL04");
    fields[15] = fieldETG06VAL05 =
    new DecimalField(message, HEADERSIZE + 101, 11, 2, "ETG06VAL05");
    fields[16] = fieldETG06VAL06 =
    new DecimalField(message, HEADERSIZE + 112, 11, 2, "ETG06VAL06");
    fields[17] = fieldETG06VAL07 =
    new DecimalField(message, HEADERSIZE + 123, 13, 0, "ETG06VAL07");
    fields[18] = fieldETG06ALFA1 =
    new CharacterField(message, HEADERSIZE + 136, 11, "ETG06ALFA1");
    fields[19] = fieldETG06NREC =
    new DecimalField(message, HEADERSIZE + 147, 8, 0, "ETG06NREC");
    fields[20] = fieldETG06NPAG =
    new DecimalField(message, HEADERSIZE + 155, 8, 0, "ETG06NPAG");

    synchronized (tlookup)
    {
      if (tlookup.isEmpty())
      {
        for (int i = 0; i < tnames.length; i++)
          tlookup.put(tnames[i], new Integer(i));
      }
    }

    taglookup = tlookup;
  }

  /**
  * Set field H06USERID using a String value.
  */
  public void setH06USERID(String newvalue)
  {
    fieldH06USERID.setString(newvalue);
  }

  /**
  * Get value of field H06USERID as a String.
  */
  public String getH06USERID()
  {
    return fieldH06USERID.getString();
  }

  /**
  * Set field H06PROGRM using a String value.
  */
  public void setH06PROGRM(String newvalue)
  {
    fieldH06PROGRM.setString(newvalue);
  }

  /**
  * Get value of field H06PROGRM as a String.
  */
  public String getH06PROGRM()
  {
    return fieldH06PROGRM.getString();
  }

  /**
  * Set field H06TIMSYS using a String value.
  */
  public void setH06TIMSYS(String newvalue)
  {
    fieldH06TIMSYS.setString(newvalue);
  }

  /**
  * Get value of field H06TIMSYS as a String.
  */
  public String getH06TIMSYS()
  {
    return fieldH06TIMSYS.getString();
  }

  /**
  * Set field H06SCRCOD using a String value.
  */
  public void setH06SCRCOD(String newvalue)
  {
    fieldH06SCRCOD.setString(newvalue);
  }

  /**
  * Get value of field H06SCRCOD as a String.
  */
  public String getH06SCRCOD()
  {
    return fieldH06SCRCOD.getString();
  }

  /**
  * Set field H06OPECOD using a String value.
  */
  public void setH06OPECOD(String newvalue)
  {
    fieldH06OPECOD.setString(newvalue);
  }

  /**
  * Get value of field H06OPECOD as a String.
  */
  public String getH06OPECOD()
  {
    return fieldH06OPECOD.getString();
  }

  /**
  * Set field H06FLGMAS using a String value.
  */
  public void setH06FLGMAS(String newvalue)
  {
    fieldH06FLGMAS.setString(newvalue);
  }

  /**
  * Get value of field H06FLGMAS as a String.
  */
  public String getH06FLGMAS()
  {
    return fieldH06FLGMAS.getString();
  }

  /**
  * Set field H06FLGWK1 using a String value.
  */
  public void setH06FLGWK1(String newvalue)
  {
    fieldH06FLGWK1.setString(newvalue);
  }

  /**
  * Get value of field H06FLGWK1 as a String.
  */
  public String getH06FLGWK1()
  {
    return fieldH06FLGWK1.getString();
  }

  /**
  * Set field H06FLGWK2 using a String value.
  */
  public void setH06FLGWK2(String newvalue)
  {
    fieldH06FLGWK2.setString(newvalue);
  }

  /**
  * Get value of field H06FLGWK2 as a String.
  */
  public String getH06FLGWK2()
  {
    return fieldH06FLGWK2.getString();
  }

  /**
  * Set field H06FLGWK3 using a String value.
  */
  public void setH06FLGWK3(String newvalue)
  {
    fieldH06FLGWK3.setString(newvalue);
  }

  /**
  * Get value of field H06FLGWK3 as a String.
  */
  public String getH06FLGWK3()
  {
    return fieldH06FLGWK3.getString();
  }

  /**
  * Set field ETG06TAB using a String value.
  */
  public void setETG06TAB(String newvalue)
  {
    fieldETG06TAB.setString(newvalue);
  }

  /**
  * Get value of field ETG06TAB as a String.
  */
  public String getETG06TAB()
  {
    return fieldETG06TAB.getString();
  }

  /**
  * Set numeric field ETG06TAB using a BigDecimal value.
  */
  public void setETG06TAB(BigDecimal newvalue)
  {
    fieldETG06TAB.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG06TAB as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG06TAB()
  {
    return fieldETG06TAB.getBigDecimal();
  }

  /**
  * Set field ETG06ELE using a String value.
  */
  public void setETG06ELE(String newvalue)
  {
    fieldETG06ELE.setString(newvalue);
  }

  /**
  * Get value of field ETG06ELE as a String.
  */
  public String getETG06ELE()
  {
    return fieldETG06ELE.getString();
  }

  /**
  * Set field ETG06VAL01 using a String value.
  */
  public void setETG06VAL01(String newvalue)
  {
    fieldETG06VAL01.setString(newvalue);
  }

  /**
  * Get value of field ETG06VAL01 as a String.
  */
  public String getETG06VAL01()
  {
    return fieldETG06VAL01.getString();
  }

  /**
  * Set numeric field ETG06VAL01 using a BigDecimal value.
  */
  public void setETG06VAL01(BigDecimal newvalue)
  {
    fieldETG06VAL01.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG06VAL01 as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG06VAL01()
  {
    return fieldETG06VAL01.getBigDecimal();
  }

  /**
  * Set field ETG06VAL02 using a String value.
  */
  public void setETG06VAL02(String newvalue)
  {
    fieldETG06VAL02.setString(newvalue);
  }

  /**
  * Get value of field ETG06VAL02 as a String.
  */
  public String getETG06VAL02()
  {
    return fieldETG06VAL02.getString();
  }

  /**
  * Set numeric field ETG06VAL02 using a BigDecimal value.
  */
  public void setETG06VAL02(BigDecimal newvalue)
  {
    fieldETG06VAL02.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG06VAL02 as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG06VAL02()
  {
    return fieldETG06VAL02.getBigDecimal();
  }

  /**
  * Set field ETG06VAL03 using a String value.
  */
  public void setETG06VAL03(String newvalue)
  {
    fieldETG06VAL03.setString(newvalue);
  }

  /**
  * Get value of field ETG06VAL03 as a String.
  */
  public String getETG06VAL03()
  {
    return fieldETG06VAL03.getString();
  }

  /**
  * Set numeric field ETG06VAL03 using a BigDecimal value.
  */
  public void setETG06VAL03(BigDecimal newvalue)
  {
    fieldETG06VAL03.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG06VAL03 as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG06VAL03()
  {
    return fieldETG06VAL03.getBigDecimal();
  }

  /**
  * Set field ETG06VAL04 using a String value.
  */
  public void setETG06VAL04(String newvalue)
  {
    fieldETG06VAL04.setString(newvalue);
  }

  /**
  * Get value of field ETG06VAL04 as a String.
  */
  public String getETG06VAL04()
  {
    return fieldETG06VAL04.getString();
  }

  /**
  * Set numeric field ETG06VAL04 using a BigDecimal value.
  */
  public void setETG06VAL04(BigDecimal newvalue)
  {
    fieldETG06VAL04.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG06VAL04 as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG06VAL04()
  {
    return fieldETG06VAL04.getBigDecimal();
  }

  /**
  * Set field ETG06VAL05 using a String value.
  */
  public void setETG06VAL05(String newvalue)
  {
    fieldETG06VAL05.setString(newvalue);
  }

  /**
  * Get value of field ETG06VAL05 as a String.
  */
  public String getETG06VAL05()
  {
    return fieldETG06VAL05.getString();
  }

  /**
  * Set numeric field ETG06VAL05 using a BigDecimal value.
  */
  public void setETG06VAL05(BigDecimal newvalue)
  {
    fieldETG06VAL05.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG06VAL05 as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG06VAL05()
  {
    return fieldETG06VAL05.getBigDecimal();
  }

  /**
  * Set field ETG06VAL06 using a String value.
  */
  public void setETG06VAL06(String newvalue)
  {
    fieldETG06VAL06.setString(newvalue);
  }

  /**
  * Get value of field ETG06VAL06 as a String.
  */
  public String getETG06VAL06()
  {
    return fieldETG06VAL06.getString();
  }

  /**
  * Set numeric field ETG06VAL06 using a BigDecimal value.
  */
  public void setETG06VAL06(BigDecimal newvalue)
  {
    fieldETG06VAL06.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG06VAL06 as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG06VAL06()
  {
    return fieldETG06VAL06.getBigDecimal();
  }

  /**
  * Set field ETG06VAL07 using a String value.
  */
  public void setETG06VAL07(String newvalue)
  {
    fieldETG06VAL07.setString(newvalue);
  }

  /**
  * Get value of field ETG06VAL07 as a String.
  */
  public String getETG06VAL07()
  {
    return fieldETG06VAL07.getString();
  }

  /**
  * Set numeric field ETG06VAL07 using a BigDecimal value.
  */
  public void setETG06VAL07(BigDecimal newvalue)
  {
    fieldETG06VAL07.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG06VAL07 as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG06VAL07()
  {
    return fieldETG06VAL07.getBigDecimal();
  }

  /**
  * Set field ETG06ALFA1 using a String value.
  */
  public void setETG06ALFA1(String newvalue)
  {
    fieldETG06ALFA1.setString(newvalue);
  }

  /**
  * Get value of field ETG06ALFA1 as a String.
  */
  public String getETG06ALFA1()
  {
    return fieldETG06ALFA1.getString();
  }

  /**
  * Set field ETG06NREC using a String value.
  */
  public void setETG06NREC(String newvalue)
  {
    fieldETG06NREC.setString(newvalue);
  }

  /**
  * Get value of field ETG06NREC as a String.
  */
  public String getETG06NREC()
  {
    return fieldETG06NREC.getString();
  }

  /**
  * Set numeric field ETG06NREC using a BigDecimal value.
  */
  public void setETG06NREC(BigDecimal newvalue)
  {
    fieldETG06NREC.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG06NREC as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG06NREC()
  {
    return fieldETG06NREC.getBigDecimal();
  }

  /**
  * Set field ETG06NPAG using a String value.
  */
  public void setETG06NPAG(String newvalue)
  {
    fieldETG06NPAG.setString(newvalue);
  }

  /**
  * Get value of field ETG06NPAG as a String.
  */
  public String getETG06NPAG()
  {
    return fieldETG06NPAG.getString();
  }

  /**
  * Set numeric field ETG06NPAG using a BigDecimal value.
  */
  public void setETG06NPAG(BigDecimal newvalue)
  {
    fieldETG06NPAG.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field ETG06NPAG as a BigDecimal.
  */
  public BigDecimal getBigDecimalETG06NPAG()
  {
    return fieldETG06NPAG.getBigDecimal();
  }

}
