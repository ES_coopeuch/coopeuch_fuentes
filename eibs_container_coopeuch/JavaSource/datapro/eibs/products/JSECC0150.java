package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Creado por              :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  05/11/2012                                                                                      **/
/**  Objetivo                :  Servicio para consulta de facturaciones de tarjetas de Creditos                                 **/
/**                                                                                                                             **/  
/**                                                                                                                             **/
/*********************************************************************************************************************************/

import java.beans.Beans;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.*;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;
import java.io.OutputStream;

import cl.coopeuch.core.nexus.*;
import cl.coopeuch.core.nexus.nexus.NexusPortTypeProxy;

import com.ibm.misc.BASE64Decoder;

import datapro.eibs.beans.ECC015001Message;
import datapro.eibs.beans.ECC015002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;

public class JSECC0150 extends datapro.eibs.master.SuperServlet  {

	// credit card 

	/**
	 * 
	 */
	private static final long serialVersionUID = 9118761411574738319L;
	protected static final int R_FACTURA_NAC_LIST = 100;
	protected static final int R_FACTURA_NAC_LIST_NEXUS = 110; //PRO 0039
	protected static final int R_FACTURA_INT_LIST = 200;
	protected static final int R_FACTURA_INT_LIST_NEXUS = 210; //PRO 0039
	protected static final int R_FACTURA_NAC_DET = 500;
	protected static final int R_FACTURA_INT_DET = 600;
	protected static final int R_MOVTOS_NO_FACTURADOS = 1000;	
	protected static final int R_MOVTOS_NO_FACTURADOS_NEXUS = 1100;	
	protected static final int R_TARJETAS_ADICIONALES = 2000;	

	protected static final int R_FACTURA_NAC_DET_NEXUS = 700; //PRO 0039 
	protected static final int R_FACTURA_INT_DET_NEXUS = 800; //PRO 0039 

	protected String LangPath = "S";
	/**
	 * JSECC0150 constructor comment.
	 */
	public JSECC0150() {
		super();
	}
	/**
	 *  
	 */
	public void destroy() {

		flexLog("free resources used by JSECC0150");

	}
	/**
	 *  
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	protected void procReqFacturasList(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses,
			String option)
			throws ServletException, IOException {

			MessageRecord newmessage = null;
			ECC015001Message msgList = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			boolean IsNotError = false;

			try {
				msgError = new ELEERRMessage();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			userPO.setPurpose("INQUIRY");

			// Send Initial data
			try {
				msgList = (ECC015001Message) mc.getMessageRecord("ECC015001");
				msgList.setH01USERID(user.getH01USR());
				msgList.setH01PROGRM("ECC0150");
				msgList.setH01TIMSYS(getTimeStamp());
				msgList.setH01SCRCOD("01");
				msgList.setH01OPECOD("0004");
				msgList.setE01SELNXN(userPO.getHeader21());
				msgList.setE01SELTIP(option);			
			    flexLog("mensaje enviado " + msgList);	
			    msgList.send();
			    msgList.destroy();
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {

					try {
						msgError =
							(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
								getClass().getClassLoader(),
								"datapro.eibs.beans.ELEERRMessage");
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else {
					flexLog("Message " + newmessage.getFormatName() + " received.");				
				}
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e + newmessage);
				throw new RuntimeException("Socket Communication Error Receiving");
			}

			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ECC015001")) {

					JBObjList beanList = new JBObjList();

					boolean firstTime = true;
					String marker = "";
					String chk = "";
						while (true) {

						msgList = (ECC015001Message) newmessage;

						marker = msgList.getH01FLGMAS();

						if (firstTime) {
							firstTime = false;
							chk = "checked";

						} else {
							chk = "";
						}

						if (marker.equals("*")) {
							beanList.setShowNext(false);
							break;
						} else {
							beanList.addRow(msgList);

							if (marker.equals("+")) {
								beanList.setShowNext(true);

								break;
							}
						}
						newmessage = mc.receiveMessage();
					}

					flexLog("Putting java beans into the session");
					ses.setAttribute("error", msgError);
					ses.setAttribute("ECC015001List", beanList);
					ses.setAttribute("userPO", userPO);
                    if (option.equals("N"))
                    {
						try {
							flexLog(
								"About to call Page: " + LangPath + "ECC0150_cc_fac_nac_list.jsp");
							callPage(LangPath + "ECC0150_cc_fac_nac_list.jsp", req, res);					
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
                     } else
 						try {
							flexLog(
								"About to call Page: " + LangPath + "ECC0150_cc_fac_int_list.jsp");
							callPage(LangPath + "ECC0150_cc_fac_int_list.jsp", req, res);					
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}                    	 
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Data Receiving");
			}

		}			

	//PRO 0039 INI

	protected void procReqFacturasListNexus(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses,
			String option)
			throws ServletException, IOException 
			{

			MessageRecord newmessage = null;
			ELEERRMessage msgError = null;
			UserPos userPO = null;
			boolean IsNotError = false;

			try {
				msgError = new ELEERRMessage();
			} 
			catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
			userPO.setPurpose("INQUIRY");

			ses.setAttribute("userPO", userPO);

			if (option.equals("N"))
            {
				try {
					flexLog("About to call Page: " + LangPath + "ECC0150_cc_fac_nac_list_nexus.jsp");
							callPage(LangPath + "ECC0150_cc_fac_nac_list_nexus.jsp", req, res);					
						} 
				catch (Exception e) {
							flexLog("Exception calling page " + e);
					}
            } else
 				try {
 					flexLog("About to call Page: " + LangPath + "ECC0150_cc_fac_int_list_nexus.jsp");
							callPage(LangPath + "ECC0150_cc_fac_int_list_nexus.jsp", req, res);					
						} 
				catch (Exception e) {
							flexLog("Exception calling page " + e);
					}                    	 
		} 
	//PRO 0039 END
	
	protected void procReqFactDetNac(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses,
			String option)
			throws ServletException, IOException {

		UserPos	userPO = null;	
		ECC015001Message msgCC = null;
		ECC015002Message msgList = null;
		MessageRecord newmessage = null;
		ELEERRMessage msgError = null;
		boolean IsNotError = false;
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Receive Data
		try {
			JBObjList bl = (JBObjList) ses.getAttribute("ECC015001List");
			int idx = (req.getParameter("CURR150")==null || req.getParameter("CURR150").equals(""))?0:Integer.parseInt(req.getParameter("CURR150"));				
			bl.setCurrentRow(idx);
			msgCC = (ECC015001Message) bl.getRecord();			
			try {
				msgError = new ELEERRMessage();
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}
			// Send Initial data
			try {
				msgList = (ECC015002Message) mc.getMessageRecord("ECC015002");
				msgList.setH02USERID(user.getH01USR());
				msgList.setH02PROGRM("ECC0150");
				msgList.setH02TIMSYS(getTimeStamp());
				msgList.setH02SCRCOD("01");
				msgList.setH02OPECOD("0004");
				msgList.setE02SELNXN(userPO.getHeader21());
				msgList.setE02SELTIP(option);		
				msgList.setE02SELFFA(msgCC.getBigDecimalE01CCHFFA());
				msgList.setE02SELFFM(msgCC.getBigDecimalE01CCHFFM());
				if (option.equals("I")) 
				{
					msgList.setH02OPECOD("0005");	
				}
				flexLog("mensaje enviado " + msgList);	
			    msgList.send();
			    msgList.destroy();
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Error");
			}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {

					try {
						msgError =
							(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
								getClass().getClassLoader(),
								"datapro.eibs.beans.ELEERRMessage");
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else {
					flexLog("Message " + newmessage.getFormatName() + " received.");				
				}
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e + newmessage);
				throw new RuntimeException("Socket Communication Error Receiving");
			}

			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ECC015002")) {

					JBObjList beanList = new JBObjList();

					boolean firstTime = true;
					String marker = "";
					String chk = "";
						while (true) {

						msgList = (ECC015002Message) newmessage;

						marker = msgList.getH02FLGMAS();

						if (firstTime) {
							firstTime = false;
							chk = "checked";

						} else {
							chk = "";
						}

						if (marker.equals("*")) {
							beanList.setShowNext(false);
							break;
						} else {
							beanList.addRow(msgList);

							if (marker.equals("+")) {
								beanList.setShowNext(true);

								break;
							}
						}
						newmessage = mc.receiveMessage();
					}
							
					ses.setAttribute("userPO", userPO);
					ses.setAttribute("cchData", msgCC);					
					ses.setAttribute("error", msgError);
					ses.setAttribute("ECC015002List", beanList);
                    if (option.equals("N"))
                    {
						try {
							flexLog(
								"About to call Page: " + LangPath + "ECC0150_cc_fac_nac_det.jsp");
							callPage(LangPath + "ECC0150_cc_fac_nac_det.jsp", req, res);					
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}
                     } else
 						try {
							flexLog(
								"About to call Page: " + LangPath + "ECC0150_cc_fac_int_det.jsp");
							callPage(LangPath + "ECC0150_cc_fac_int_det.jsp", req, res);					
						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}                    	 
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				throw new RuntimeException("Socket Communication Data Receiving");
			}

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	protected void procReqMovNoFacturados(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

		UserPos	userPO = null;	
		ECC015002Message msgList = null;
		MessageRecord newmessage = null;
		ELEERRMessage msgError = null;
		boolean IsNotError = false;
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Receive Data
		try {
			msgList = (ECC015002Message) mc.getMessageRecord("ECC015002");
			msgList.setH02USERID(user.getH01USR());
			msgList.setH02PROGRM("ECC0150");
			msgList.setH02TIMSYS(getTimeStamp());
			msgList.setH02SCRCOD("01");
			msgList.setH02OPECOD("0006");
			msgList.setE02SELNXN(userPO.getHeader21());
			flexLog("mensaje enviado " + msgList);	
		    msgList.send();
		    msgList.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {

					try {
						msgError =
							(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
								getClass().getClassLoader(),
								"datapro.eibs.beans.ELEERRMessage");
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else {
					flexLog("Message " + newmessage.getFormatName() + " received.");				
				}
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e + newmessage);
				throw new RuntimeException("Socket Communication Error Receiving");
			}

			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ECC015002")) {

					JBObjList beanList = new JBObjList();

					boolean firstTime = true;
					String marker = "";
					String chk = "";
						while (true) {

						msgList = (ECC015002Message) newmessage;

						marker = msgList.getH02FLGMAS();

						if (firstTime) {
							firstTime = false;
							chk = "checked";

						} else {
							chk = "";
						}

						if (marker.equals("*")) {
							beanList.setShowNext(false);
							break;
						} else {
							beanList.addRow(msgList);

							if (marker.equals("+")) {
								beanList.setShowNext(true);

								break;
							}
						}
						newmessage = mc.receiveMessage();
					}
							
					ses.setAttribute("userPO", userPO);				
					ses.setAttribute("error", msgError);
					ses.setAttribute("ECC015002List", beanList);
					try {
						flexLog(
							"About to call Page: " + LangPath + "ECC0150_cc_mov_no_fact_list.jsp");
						callPage(LangPath + "ECC0150_cc_mov_no_fact_list.jsp", req, res);					
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	//PRO 0039 INI
	protected void procReqMovNoFacturadosNexus(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession session)
			throws ServletException, IOException {

		UserPos	userPO = null;	
		ELEERRMessage msgError = new ELEERRMessage();
		String numtarjeta = "";
		DatosMovimientosNoFacturados entrada =  null;
		RespuestaMovimientosNoFacturados resp = null;

		userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		if(userPO.getHeader21().toString()!=null)
			numtarjeta  = userPO.getHeader21().toString().trim();

		try 
		{
			entrada = new DatosMovimientosNoFacturados();

			entrada.setNum_cta_tar(numtarjeta);	
	
			NexusPortTypeProxy nep = new NexusPortTypeProxy();
	  		String UrlServicio = JSEIBSProp.getEndPoinNexus();
  	  		nep.setEndpoint(UrlServicio);

			resp = nep.consultaMovimientosNoFacturados(entrada);
	
			if (resp.getError()!=null)
			{	
				msgError = new ELEERRMessage();	
				msgError.setERRNUM("1");
	            msgError.setERNU01("01");//4
	            
	            if(resp.getCoderror()>0)
	            	msgError.setERDS01(Integer.toString(resp.getCoderror()).concat(" ").concat(resp.getMsgerror()));
	            else 
	            	if(resp.getError().getErrores().getDetalleError(0).getCodigo()!="")
	            		msgError.setERDS01(resp.getError().getErrores().getDetalleError(0).getDescripcion());
	            	else
	            		msgError.setERDS01("Error de Comunicacion al acceder a servicio de Movtos no Facturados.");
	            session.setAttribute("error", msgError);				
			}
		}
		catch (Exception e) {
			String className = e.getClass().getName();
			String description = e.getMessage() == null ? "Exception General" : e.getMessage();					

			msgError = new ELEERRMessage();	
			msgError.setERRNUM("1");
            msgError.setERNU01("01");//4
            msgError.setERDS01("Error de Comunicacion al acceder a servicio de Movtos no Facturados.");
            msgError.setERNU02("02");//4
            msgError.setERDS02(className);
            msgError.setERNU03("03");//4
            msgError.setERDS03(description.length() > 70 ? description.substring(0, 70) : description);	
            session.setAttribute("error", msgError);				
		}
		
					
		try {
			session.setAttribute("userPO", userPO);				
			session.setAttribute("RespMov", resp);

			flexLog("About to call Page: " + LangPath + "ECC0150_cc_mov_no_fact_list_nexus.jsp");
			callPage(LangPath + "ECC0150_cc_mov_no_fact_list_nexus.jsp", req, res);					
			} 
		catch (Exception e) 
		{
			flexLog("Exception calling page " + e);
		}
	}
	//PRO 0039 FIN
	
	protected void procReqTarjetasAdic(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

		UserPos	userPO = null;	
		ECC004001Message msgList = null;
		MessageRecord newmessage = null;
		ELEERRMessage msgError = null;
		boolean IsNotError = false;
		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Receive Data
		try {
			msgList = (ECC004001Message) mc.getMessageRecord("ECC004001");
			msgList.setH01USERID(user.getH01USR());
			msgList.setH01PROGRM("ECC0040");
			msgList.setH01TIMSYS(getTimeStamp());
			msgList.setH01SCRCOD("01");
			msgList.setH01OPECOD("0010");
			msgList.setE01CCMCUN(userPO.getCusNum());
			msgList.setE01CCMNME(userPO.getCusName());
			msgList.setE01CCRCID(userPO.getHeader1());
			msgList.setE01CCMCCY(userPO.getCurrency());
			msgList.setE01CCMOFC(userPO.getOfficer());
			msgList.setE01CCMPRO(userPO.getProdCode());
			msgList.setE01CCRCID(userPO.getIdentifier());
			msgList.setE01CCMACC(userPO.getAccNum());
			msgList.setD01CCMPRO(userPO.getHeader20());		
			msgList.setE01CCRNXN(userPO.getHeader21());				
			flexLog("mensaje enviado " + msgList);	
		    msgList.send();
		    msgList.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

			// Receive Data
			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ELEERR")) {

					try {
						msgError =
							(datapro.eibs.beans.ELEERRMessage) Beans.instantiate(
								getClass().getClassLoader(),
								"datapro.eibs.beans.ELEERRMessage");
					} catch (Exception ex) {
						flexLog("Error: " + ex);
					}

					msgError = (ELEERRMessage) newmessage;
					IsNotError = msgError.getERRNUM().equals("0");
					flexLog("IsNotError = " + IsNotError);
					showERROR(msgError);
				} else {
					flexLog("Message " + newmessage.getFormatName() + " received.");				
				}
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Error: " + e + newmessage);
				throw new RuntimeException("Socket Communication Error Receiving");
			}

			try {
				newmessage = mc.receiveMessage();

				if (newmessage.getFormatName().equals("ECC004001")) {

					JBObjList beanList = new JBObjList();

					boolean firstTime = true;
					String marker = "";
					String chk = "";
						while (true) {

						msgList = (ECC004001Message) newmessage;

						marker = msgList.getH01FLGMAS();

						if (firstTime) {	
							firstTime = false;
							chk = "checked";
							userPO.setCusNum(msgList.getE01CCMCUN());
							userPO.setCusName(msgList.getE01CCMNME());
							userPO.setHeader1(msgList.getE01CCRCID());
							userPO.setCurrency(msgList.getE01CCMCCY());
							userPO.setOfficer(msgList.getE01CCMOFC());
							userPO.setProdCode(msgList.getE01CCMPRO());
							userPO.setIdentifier(msgList.getE01CCRCID());
							userPO.setAccNum(msgList.getE01CCMACC());
							userPO.setHeader20(msgList.getD01CCMPRO());
							userPO.setHeader21(msgList.getE01CCRNXN());							
						} else {
							chk = "";
						}

						if (marker.equals("*")) {
							beanList.setShowNext(false);
							break;
						} else {
							beanList.addRow(msgList);

							if (marker.equals("+")) {
								beanList.setShowNext(true);

								break;
							}
						}
						newmessage = mc.receiveMessage();
					}
					flexLog("Datos userpo " + userPO.getCusName().trim());
					ses.setAttribute("userPO", userPO);				
					ses.setAttribute("error", msgError);
					ses.setAttribute("ECC0040Help", beanList);
					try {
						flexLog(
							"About to call Page: " + LangPath + "ECC0040_cc_inq_card_adic_list.jsp");
						callPage(LangPath + "ECC0040_cc_inq_card_adic_list.jsp", req, res);					
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

	//PRO 0039 INI

	protected void procReqFactDetNacNexusWebService(
			MessageContext mc,
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession session,
			String option)
			throws ServletException, IOException {

        ELEERRMessage msgError = new ELEERRMessage();				

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		try {
				
			//set enter parameters
			String dpList="";
			String ano = "";
			String mes = "";
			String numtarjeta = "";
			String SPeriodoActual = ""; 
			
			Calendar fechaact = Calendar.getInstance();
		    int anorango = fechaact.get(Calendar.YEAR)- 1;
		    int mesrango = fechaact.get(Calendar.MONTH);
		    if (mesrango<10)
		    	SPeriodoActual =  String.valueOf(anorango).concat("0").concat(String.valueOf(mesrango)).concat("01");
		    else
		    	SPeriodoActual =  String.valueOf(anorango).concat(String.valueOf(mesrango)).concat("01");
		    
		    int IPeriodoActual = Integer.parseInt(SPeriodoActual);
			
			DatosEstadosDeCuentaPDF entrada = null;
  		    NexusPortTypeProxy nep = null;
   		    RespuestaEstadosDeCuentaPDF resp = null;
	  		    
			if(req.getParameter("E01ANOECC").toString()!=null)
				ano = req.getParameter("E01ANOECC").toString().trim();
			if(req.getParameter("E01MESECC").toString()!=null)
				mes = req.getParameter("E01MESECC").toString().trim();
			
			String fecha = ano.concat(mes).concat("01");
			
			int intfecha = Integer.parseInt(fecha);
				
			if(req.getParameter("E01CCRNXN").toString()!=null)
				numtarjeta  = req.getParameter("E01CCRNXN").toString().trim();

  			try {
  				entrada = new DatosEstadosDeCuentaPDF();
  				
  	  			entrada.setFecha_fact(intfecha);
  	  			entrada.setNum_cta_tar(numtarjeta);
  	  			entrada.setTipo_eecc_pdf(option);			
			
  	  		    nep = new NexusPortTypeProxy();
  	  		    String UrlServicio = JSEIBSProp.getEndPoinNexus();
  	  		    nep.setEndpoint(UrlServicio);

  	  		    flexLog("INICIO LLAMADA A ESTADO DE CUENTA PDF");
  	  		
  	  		    resp = nep.consultaEstadosDeCuentaPDF(entrada);

  	  		    flexLog("FIN LLAMADA A ESTADO DE CUENTA PDF");
  	  		    
				if (resp.getError()==null)
				{

	  	  		    flexLog("INICIO PRESENTACION PDF");

					dpList = resp.getVariable_pdf();
					BASE64Decoder decoder = new BASE64Decoder();
					byte[] decodedBytes = decoder.decodeBuffer(dpList);

	  	  		    res.reset(); 
				    res.setContentType("application/pdf"); 
				    res.setHeader("Content-disposition", "attachment; filename=\"ECCxxx"+option+"_"+numtarjeta.substring(15)+"_"+intfecha+".pdf\""); 

				    OutputStream output = res.getOutputStream();
				    output.write(decodedBytes);
				    output.flush();
				    output.close();

				    flexLog("FIN PRESENTACION PDF");

				}
				else
				{
					msgError = new ELEERRMessage();				
					msgError.setERRNUM("1");
		            msgError.setERNU01("01");//4
		            if(resp.getCoderror()>0)
		           		msgError.setERDS01(Integer.toString(resp.getCoderror()).concat(" ").concat(resp.getMsgerror()));
		            else 
		            	if(resp.getError().getErrores().getDetalleError(0).getCodigo()!="")
			            	if(resp.getError().getErrores().getDetalleError(0).getCodigo()!="35")
			            		if(intfecha>=IPeriodoActual)
			            			msgError.setERDS01("No existe facturación EECC para la fecha consultada");
			            		else
			            			msgError.setERDS01("No se encuentra disponible el EECC para la fecha consultada");
			            	else 
			            		msgError.setERDS01(resp.getError().getErrores().getDetalleError(0).getDescripcion());
		            	else
		            		msgError.setERDS01("Error de Comunicacion al acceder a servicio de estado de cuenta.");
		  			
		            session.setAttribute("error", msgError);				
				}
			}
			catch (Exception e) {
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();					
                
				msgError = new ELEERRMessage();				
				msgError.setERRNUM("1");
	            msgError.setERNU01("01");//4
	            msgError.setERDS01("Error de Comunicacion al acceder a servicio de estado de cuenta.");
	            msgError.setERNU02("02");//4
	            msgError.setERDS02(className);
	            msgError.setERNU03("03");//4
	            msgError.setERDS03(description.length() > 70 ? description.substring(0, 70) : description);	
	  			session.setAttribute("error", msgError);				
			}
			
			if (!msgError.getERRNUM().equals("0")) 
			{
				session.setAttribute("userPO", userPO);			

				if (option.equals("NAC"))
				{
					try {
						callPage(LangPath + "ECC0150_cc_fac_nac_list_nexus.jsp", req, res);					
						} 
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else
					try {
						callPage(LangPath + "ECC0150_cc_fac_int_list_nexus.jsp", req, res);					
						} 
					catch (Exception e) {
						flexLog("Exception calling page " + e);
					}  
			} 
		} 
		finally {
			
		}
	}	

	//PRO 0039 FIN

	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		Socket s = null;
		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			int screen = R_FACTURA_NAC_LIST;

			try {

				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					s = new Socket(super.hostIP, getInitSocket(req) + 1);
					s.setSoTimeout(super.sckTimeOut);
					mc =
						new MessageContext(
							new DataInputStream(new BufferedInputStream(s.getInputStream())),
							new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
							"datapro.eibs.beans");
				

				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}

				switch (screen) {
					// Request

				case R_FACTURA_NAC_LIST:
					procReqFacturasList(mc, msgUser, req, res, session, "N");
					break;	
				case R_FACTURA_NAC_LIST_NEXUS:
					procReqFacturasListNexus(mc, msgUser, req, res, session, "N");
					break;		//PRO 0039
				case R_FACTURA_INT_LIST:
					procReqFacturasList(mc, msgUser, req, res, session, "I");
					break;
				case R_FACTURA_INT_LIST_NEXUS:
					procReqFacturasListNexus(mc, msgUser, req, res, session, "I");
					break;	//PRO 0039
				case R_FACTURA_NAC_DET:
					procReqFactDetNac(mc, msgUser, req, res, session, "N");
					break;
				case R_FACTURA_NAC_DET_NEXUS:
					procReqFactDetNacNexusWebService(mc, msgUser, req, res, session, "NAC");
					break; 	//PRO 0039
				case R_FACTURA_INT_DET_NEXUS:
					procReqFactDetNacNexusWebService(mc, msgUser, req, res, session, "INT");
					break; 	//PRO 0039
				case R_FACTURA_INT_DET:
					procReqFactDetNac(mc, msgUser, req, res, session, "I");
					break;	
				case R_MOVTOS_NO_FACTURADOS:
					procReqMovNoFacturados(mc, msgUser, req, res, session);
					break;	
				case R_MOVTOS_NO_FACTURADOS_NEXUS:
					procReqMovNoFacturadosNexus(mc, msgUser, req, res, session);
					break;		//PRO 0039
				case R_TARJETAS_ADICIONALES:
					procReqTarjetasAdic(mc, msgUser, req, res, session);
					break;						
					default :
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
				}

				} catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 3;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
				} finally {
					s.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}

		}

	}
	
	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

		}
	}
	
//END
	
}


