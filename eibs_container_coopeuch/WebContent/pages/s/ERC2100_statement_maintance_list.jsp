<!-- Hecho por Alonso Arana ------Datapro-----06/02/2014 -->
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC210001Message,datapro.eibs.beans.ERC210002Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Conciliaci�n Bancos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ERC" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "ERCSeleccion" class= "datapro.eibs.beans.ERC210001Message"  scope="session" />


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

   $(function(){
				
					$("#radio_key").attr("checked", false);
                
				});


  function goAction(op,index) {
	var ok = false;
	var cun = "";
	var pg = "";

if(op=='400'){
	document.forms[0].SCREEN.value = op;
			document.forms[0].submit();	
}

	if (op != '400'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01RCCTCD") 
	      	{	
	      		if (element.checked == true) {
	      			document.getElementById("codigo_lista").value = element.value; 
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = true;
      	
      	if (op =='750'){
      		confirm1 = confirm("�Desea eliminar este movimiento seleccionado?");
      	}
      	
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		

     } else {
		alert("Debe seleccionar un movimiento para continuar.");	   
	 }    
	}







function goAction2(op,index) {
	var ok = false;
	var cun = "";
	var pg = "";

if(op=='1000'){
	document.forms[0].SCREEN.value = op;
			document.forms[0].submit();	
}

	}




</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Cartola Manual<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ERC2100_statement_maintance_list.jsp,ERC2100"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC2100">
<input type="hidden" name="SCREEN" value="201">
<input type="hidden" name="codigo_lista" value="" id="codigo_lista">  
<input type="hidden" name="E01BRMEID" value="<%=session.getAttribute("codigo_banco") %>">
 <input type="hidden" name="E01DSCRBK" value="<%=session.getAttribute("nombre_banco") %>">
 
   <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="banco" value="<% out.print(session.getAttribute("codigo_banco")+"-"+session.getAttribute("nombre_banco"));%>" 
	  			size="60"  readonly>
             </td>    
         </tr>
         
          <tr>
             <td nowrap width="10%" align="right">Cuenta Banco : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCTA" size="23" maxlength="20" value="<%=session.getAttribute("cuenta_banco")%>" readonly>
             </td>
        <td nowrap width="10%" align="right">Cuenta IBS : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMACC" size="23" maxlength="20" value="<%=session.getAttribute("cuenta_ibs")%>" readonly>
             </td>
             </tr>

          <tr>
             <td nowrap width="10%" align="right">N� Cartola :</td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E02WCSSTN" size="23" maxlength="20" value="<%=ERCSeleccion.getE01WCHSTN()%>" readonly>
             </td>
        	 <td nowrap width="10%" align="right">Fecha de Carga :</td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="fecha_carga" size="23" maxlength="20" value="<% out.print(ERCSeleccion.getE01WCHSDD()+"/"+ERCSeleccion.getE01WCHSDM()+"/"+ERCSeleccion.getE01WCHSDY());%>" readonly>
             </td>
             </tr>


      
           
        </table>
      </td>
    </tr>
  </table>
 
 







<table class="tbenter" width="100%">
	<tr></tr>
	<tr>
		
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('400')"><b>Crear</b></a>
		</td>
	
	
	
	
	
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('630')"><b>Modificar</b></a>
		</td>
		
		
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('750')"><b>Eliminar</b></a>
		</td>
		
			
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction2('1000')"><b>Atr�s</b></a>
		</td>
	</tr>
	<tr></tr>
</table>

<p>&nbsp;</p>
	<table id="headTable"  width="100%" align="left">
		<tr id="trdark">
		
			<th align="center" nowrap width="10" height="32"></th>

						<th align="center" nowrap width="100" height="32">Fecha</th>
						<th align="center" nowrap width="100" height="32">Referencia</th>
						<th align="center" nowrap width="100" height="32">Descripci�n</th>
			<th align="center" nowrap width="100" height="32">D�bito</th>
			<th align="center" nowrap width="100" height="32">Cr�dito</th>
			
	<th align="center" nowrap width="100" height="32">Sub Tipo</th>
	
		</tr>
		<%
		
			
		
		
			ERC.initRow();
				int k = 0,inicial;
				boolean firstTime = true;
				String chk = "";
				while (ERC.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
						

				
				
					} else {
						chk = "";
					}
				ERC210002Message pvprd = (ERC210002Message) ERC.getRecord();
		%>
		<tr>
			
			<td nowrap align="center" height="15">
			
			<input type="hidden" name="codigo_banco" value="<%out.print(session.getAttribute("codigo_banco"));%>">
			
			<input type="radio" name="E01RCCTCD" id="radio_key" value="<%=ERC.getCurrentRow()%>" <%=chk%>/>
			</td>

			<td nowrap align="center" height="15"><%=Util.formatDate(pvprd.getE02WCSSDD(),pvprd.getE02WCSSDM(),pvprd.getE02WCSSDY())%> </td>
			<td nowrap align="center" height="15"><%=pvprd.getE02WCSCKN()%></td>
			<td nowrap align="left">
				<%=pvprd.getE02DESCDE()%>
			</td>
				<td nowrap align="right" height="15">
				<% 
				if (pvprd.getE02WCSDCC().equals("D"))
					out.print(pvprd.getE02WCSAMT());
				 %>
			</td>	

			<td nowrap align="right" height="15">
				<% 
				if (pvprd.getE02WCSDCC().equals("C"))
				out.print(pvprd.getE02WCSAMT());
				 %>
			</td>	

		

			<td nowrap align="center">
				<%=pvprd.getE02DESAPC()%>
			</td>
		</tr>
		<%
			}
		%>
	</table>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (ERC.getShowPrev()) {
					int pos = ERC.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1217?SCREEN=100&codNum="
									+ request.getAttribute("codNum")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (ERC.getShowNext()) {
					int pos = ERC.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1217?SCREEN=100&codNum="
									+ request.getAttribute("codNum")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>


</form>
</body>
</html>
