/**
 * NexusPortType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.coopeuch.core.nexus.nexus;

public interface NexusPortType extends java.rmi.Remote {
    public cl.coopeuch.core.nexus.RespuestaEstadosDeCuentaPDF consultaEstadosDeCuentaPDF(cl.coopeuch.core.nexus.DatosEstadosDeCuentaPDF entrada) throws java.rmi.RemoteException;
    public cl.coopeuch.core.nexus.RespuestaMovimientosNoFacturados consultaMovimientosNoFacturados(cl.coopeuch.core.nexus.DatosMovimientosNoFacturados entrada) throws java.rmi.RemoteException;
}
