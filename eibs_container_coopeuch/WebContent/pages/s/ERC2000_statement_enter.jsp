<!-- Hecho por Alonso Arana ------Datapro-----06/02/2014 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC200001Message"%>

<html>
<head>
<title>Sistema Bancario: Conciliaci�n Bancaria</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="date" class="java.util.Date" scope="session" />      
<jsp:useBean id= "bank" class= "datapro.eibs.beans.ERC200001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">
 function enterCode(){
	
	if (trim(document.forms[0].E01BRMEID.value).length > 0) {
	    return true;
	}else{
		alert("Es requerido que se entre un valor");
		document.forms[0].E01BRMEID.focus();
		return false;
	}
 }
 
 
 

function addDate(){
date = new Date();
var month = date.getMonth()+1;
var day = date.getDate();
var year = date.getFullYear();

if (document.getElementById('fecha1').value == ''){
document.getElementById('fecha1').value = day;
}

if (document.getElementById('fecha2').value == ''){
document.getElementById('fecha2').value = month;
}


if (document.getElementById('fecha3').value == ''){
document.getElementById('fecha3').value = year;
}

if (document.getElementById('fecha4').value == ''){
document.getElementById('fecha4').value = day;
}


if (document.getElementById('fecha5').value == ''){
document.getElementById('fecha5').value = month;
}

if (document.getElementById('fecha6').value == ''){
document.getElementById('fecha6').value = year;
}


}
</script>
 
 

</head>

<body  onload="addDate();">
 
<H3 align="center">Gesti�n de Cartolas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="statement_enter.jsp, ERC2000"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC2000" 
onsubmit="return(enterCode());"  >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">

<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  		
	  		    <div align="left"> 
          <input type="text" name="E01BRMEID" size="5" readonly maxlength="4" value="<%=bank.getE01RCHRBK()%>">
            <a href="javascript:GetBankReconciliation('E01BRMEID','E01DSCRBK','E01BRMCTA','E01BRMACC')">
            <img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0" ></a> 
       <input type="text" name="E01DSCRBK" readonly="readonly" size="43" maxlength="43" >
   
      </div>
             </td>                 
             <td nowrap width="10%" align="right"> Tipo de Fecha : 
              </td>
               <td nowrap width="50%"align="left">
  				<p> 
  				
  				  <input type="radio" name="E01RCHTFC" id="radio_key"  value="2" checked>
                  Fecha de Carga	
                 <input type="radio" name="E01RCHTFC" id="radio_key"  value="1"  >
                  Fecha Cartola
           
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
             </td>
         </tr>
    
         <tr>
             <td nowrap width="10%" align="right">Cuenta Banco : 
               </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01BRMCTA" readonly size="23" maxlength="20" value="<%= bank.getE01RCHCTA().trim()%>">
             </td>
              
             <td nowrap width="10%" align="right">Fecha Desde : 
              </td>
               <td nowrap width="50%"align="left">
  				<div align="left"> 
  			
                <input type="text" name="E01RCHFSD" id="fecha1" size="3" maxlength="2" value="">
                <input type="text" name="E01RCHFSM" id="fecha2" size="3" maxlength="2" value="">
                <input type="text" name="E01RCHFSY" id="fecha3" size="5" maxlength="4" value="">
                <a href="javascript:DatePicker(document.forms[0].E01RCHFSD,document.forms[0].E01RCHFSM,document.forms[0].E01RCHFSY)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 	
             Fecha Hasta:
                   <input type="text" name="E01RCHFLD" id="fecha4" size="3" maxlength="2" value="">
                <input type="text" name="E01RCHFLM" size="3" id="fecha5" maxlength="2" value="">
                <input type="text" name="E01RCHFLY" size="5" id="fecha6" maxlength="4" value="">
                <a href="javascript:DatePicker(document.forms[0].E01RCHFLD,document.forms[0].E01RCHFLM,document.forms[0].E01RCHFLY)"><img src="<%=request.getContextPath()%>/images/calendar.gif" alt="ayuda" border="0"></a> 
              </div>
             
             </td>
        
        
         </tr>
         	
             <tr>
             <td nowrap width="10%" align="right">Cuenta IBS : 
               </td>
             <td nowrap width="50%"align="left">

			<div align="left"> 
 	         <input type="text" name="E01BRMACC" readonly size="7" maxlength="7" value="<%
 	         if(bank.getE01RCHACC().equals("0")){
 	         out.print("");
 	         }
 	         else{
 	         
 	         out.print(bank.getE01RCHACC());
 	         }
 	          %>">
          </div>

             </td>
             
             <td nowrap width="10%" align="right">Estado : 
               </td>
                    <td nowrap width="50%"align="left">
           
 		       
 		    <% boolean flag = false; %><select name="E01RCHAPF">
                       <option value="T">TODOS</option>
                <option value="1" >PENDIENTE</option>
             <option value="2" >CONCILIADA PARCIAL</option>
             <option value="3" >CONCILIADA TOTAL</option>
             <option value="4" >CON ERRORES</option>
            <option value="5" >ELIMINADA</option>
            
              </select>
         &nbsp;&nbsp;&nbsp;       
      N� Cartola :
 	<input type="text" name="E01RCHSTN" size="23" maxlength="10" value="<%= bank.getE01RCHSTN().trim()%>">
                    </td>
             
         </tr>
        </table>
      </td>
    </tr>
  </table>

  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
<script language="JavaScript">
  document.forms[0].E01BRMEID.focus();
  document.forms[0].E01BRMEID.select();
</script>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
