<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<%@ page import = "datapro.eibs.master.*,datapro.eibs.beans.*" %>
<title>Tasas de Pizarra</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "tbRate" class= "datapro.eibs.beans.ECN000003Message"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT Language="javascript">

 function goAction(op) {
   var ok = true;
   if (op == "C") {
   	  ok = confirm("Desea cambiar los valores de Plazo o Montos");
   }
   if (ok) {
   	document.forms[0].OPT.value = op;
   	document.forms[0].submit(); 
   }	 
 }

</SCRIPT>
</head>

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     out.println("<SCRIPT Language=\"Javascript\">");
	 error.setERRNUM("0");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Tasas para Seguros<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="rate_table_maint_ins.jsp,ECN0000"></h3>
<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSECN0000">

  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="12">
  <INPUT TYPE=HIDDEN NAME="OPT" VALUE="">
  <INPUT TYPE=HIDDEN NAME="E03CDRSFL" VALUE="<%= tbRate.getE03CDRSFL()%>">      

<TABLE class="tableinfo">
    <TR>
    <TD>
		<TABLE width="102%">
			<tr>
				<td>
				<div align="right">Codigo Tabla :</div>
				</td>
				<td>
					<input type="text" name="E03CDRRTB" size="3" maxlength="2"
					value="<%= tbRate.getE03CDRRTB()%>" <%if (userPO.getPurpose().equals("READONLY")) {%>readonly<%}%>></td>
				<td>
				<div align="right">Descripcion :</div>
				</td>
				<td width="239">
					<input type="text" name="E03CDRDSC" size="48"
					maxlength="45" value="<%= tbRate.getE03CDRDSC()%>" <%if (userPO.getPurpose().equals("READONLY")) {%>readonly<%}%>></td>
			</tr>
			<tr>
				<td>
				<div align="right">Fecha Aplicaci&oacute;n :</div>
				</td>
				<td><input type="text" name="E03CDRDT1" size="3" maxlength="2"
					onKeypress="enterInteger()" value="<%= tbRate.getE03CDRDT1()%>" <%if (userPO.getPurpose().equals("READONLY")) {%>readonly<%}%>> 
					<input type="text" name="E03CDRDT2" size="3" maxlength="2"
					onKeypress="enterInteger()" value="<%= tbRate.getE03CDRDT2()%>" <%if (userPO.getPurpose().equals("READONLY")) {%>readonly<%}%>> 
					<input type="text" name="E03CDRDT3" size="5" maxlength="4"
					onKeypress="enterInteger()" value="<%= tbRate.getE03CDRDT3()%>" <%if (userPO.getPurpose().equals("READONLY")) {%>readonly<%}%>>
				</td>
				<td>
				<div align="right">Moneda :</div>
				</td>
				<td width="239"><input type="text" name="E03CDRCCY" size="3"
					maxlength="3" value="<%= tbRate.getE03CDRCCY()%>" <%if (userPO.getPurpose().equals("READONLY")) {%>readonly<%}%>>
				</td>
			</tr>
		</TABLE>
		</TD>
  </TR>
  </TABLE>	  

 <br> 
  <TABLE class="tableinfo">
    <TR id="trdark">
      <TH ALIGN=CENTER  nowrap width=3% > Plazo</TH> 
      <TH ALIGN=CENTER  nowrap width=8%>Edad en A�os Hasta </TH>   
      <TH ALIGN=CENTER  nowrap colspan=6>Nota : La Primera Fila de plazo 1 mes se utilizara para marcar los a�os para el Termino de Cobertura</TH>   
    </TR>
    <TR id="trdark">
      <TH ALIGN=CENTER  nowrap >(Meses)</TH>
      <%
      String name1= "";
      String name2= ""; 
      for(int i=1; i<8;i++) {
        name1= ""+i;
      %> 
      <TH ALIGN=CENTER  nowrap>
        <% if (userPO.getHeader1().equals("")) {%>
            <%= tbRate.getField("E03CDUA"+name1).getString().trim()%>
            <input type="hidden" name="E03CDUA<%=i%>" value="<%= tbRate.getField("E03CDUA"+name1).getString().trim()%>" >
        <% } else {%>
      		<input type="input" name="E03CDUA<%=i%>" value="<%= tbRate.getField("E03CDUA"+name1).getString().trim()%>" size=12 maxlength=12 <%if (userPO.getPurpose().equals("READONLY")) {%>readonly<%}else{%>onkeypress="enterInteger()"<%}%>>
      	<% } %>
      </TH> 
      <% } %>  
    </TR>
      <%
       for(int i=1;i<16; i++) { 
         name2=(i<10)? "0"+i: ""+i;                               
       %>             
                    
        <TR>
          <td align="center" id=trdark>
           <% if (userPO.getHeader1().equals("")) {%>
            <%= tbRate.getField("E03CDD"+name2).getString().trim()%>
          	<input type="hidden" name="E03CDD<%=name2%>" size="5" maxlength="4" value="<%= tbRate.getField("E03CDD"+name2).getString().trim()%>">      	
		   <% } else {%>
		    <input type="input" name="E03CDD<%=name2%>" size="5" maxlength="4" value="<%= tbRate.getField("E03CDD"+name2).getString().trim()%>" <%if (userPO.getPurpose().equals("READONLY")) {%>readonly<%}else{%>onkeypress="enterInteger()"<%}%>>      			   
		   <% } %>
		  </td>
      	  <% 
      		for(int j=1; j<8;j++) {
      		  name1= ""+ j + name2;
     	  %>
          <td NOWRAP align=center>
          	<input type="input" name="E03CL<%=name1%>" size="10" maxlength="10" value="<%= tbRate.getField("E03CL"+name1).getString().trim()%>" <%if (userPO.getPurpose().equals("READONLY")) {%>readonly<%}else{%>onkeypress="enterRate()"<%}%>>      	
		  </td>		  
		  <%}%>
		</TR>
        <%}%> 
  </TABLE>
  <br>
  <TABLE class=tbenter>
  <%if (!userPO.getPurpose().equals("READONLY")) {%>
  <tr>
  	<td align="center">         
	      <input id="EIBSBTN" type=button name="Submit" value="Enviar" onclick="goAction('X')">
  	</td>
  	<td align="center">         
	      <input id="EIBSBTN" type=button name="Submit" value="Palzo/Edad" onclick="goAction('C')">
    </td>
  </tr>
  <%}%>
  </TABLE>
  <%if (!userPO.getHeader1().equals("")) {%>
    <SCRIPT Language="javascript">
   		document.forms[0].E03CDUA1.focus();
	    document.forms[0].E03CDUA1.select(); 	
    </SCRIPT>
  <%}%>
</form>
</body>
</html>
