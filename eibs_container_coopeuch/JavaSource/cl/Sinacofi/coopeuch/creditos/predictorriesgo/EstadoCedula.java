/**
 * EstadoCedula.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class EstadoCedula  implements java.io.Serializable {
    private java.math.BigInteger id;  // attribute
    private java.math.BigInteger idConsulta;
    private java.math.BigInteger rut;
    private java.math.BigInteger dv;
    private java.lang.String tipoDocumento;
    private java.math.BigInteger numeroSerie;
    private java.lang.String razon;
    private java.lang.String fecha;
    private java.lang.String fuente;
    private java.lang.String cedulaVigente;
    private java.math.BigInteger numeroRegistros;

    public EstadoCedula() {
    }

    public java.math.BigInteger getId() {
        return id;
    }

    public void setId(java.math.BigInteger id) {
        this.id = id;
    }

    public java.math.BigInteger getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(java.math.BigInteger idConsulta) {
        this.idConsulta = idConsulta;
    }

    public java.math.BigInteger getRut() {
        return rut;
    }

    public void setRut(java.math.BigInteger rut) {
        this.rut = rut;
    }

    public java.math.BigInteger getDv() {
        return dv;
    }

    public void setDv(java.math.BigInteger dv) {
        this.dv = dv;
    }

    public java.lang.String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(java.lang.String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public java.math.BigInteger getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(java.math.BigInteger numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public java.lang.String getRazon() {
        return razon;
    }

    public void setRazon(java.lang.String razon) {
        this.razon = razon;
    }

    public java.lang.String getFecha() {
        return fecha;
    }

    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }

    public java.lang.String getFuente() {
        return fuente;
    }

    public void setFuente(java.lang.String fuente) {
        this.fuente = fuente;
    }

    public java.lang.String getCedulaVigente() {
        return cedulaVigente;
    }

    public void setCedulaVigente(java.lang.String cedulaVigente) {
        this.cedulaVigente = cedulaVigente;
    }

    public java.math.BigInteger getNumeroRegistros() {
        return numeroRegistros;
    }

    public void setNumeroRegistros(java.math.BigInteger numeroRegistros) {
        this.numeroRegistros = numeroRegistros;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        EstadoCedula other = (EstadoCedula) obj;
        boolean _equals;
        _equals = true
            && ((this.tipoDocumento==null && other.getTipoDocumento()==null) || 
             (this.tipoDocumento!=null &&
              this.tipoDocumento.equals(other.getTipoDocumento())))
            && ((this.razon==null && other.getRazon()==null) || 
             (this.razon!=null &&
              this.razon.equals(other.getRazon())))
            && ((this.fecha==null && other.getFecha()==null) || 
             (this.fecha!=null &&
              this.fecha.equals(other.getFecha())))
            && ((this.fuente==null && other.getFuente()==null) || 
             (this.fuente!=null &&
              this.fuente.equals(other.getFuente())))
            && ((this.cedulaVigente==null && other.getCedulaVigente()==null) || 
             (this.cedulaVigente!=null &&
              this.cedulaVigente.equals(other.getCedulaVigente())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        EstadoCedula history = (EstadoCedula) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId())))
            && ((this.idConsulta==null && other.getIdConsulta()==null) || 
             (this.idConsulta!=null &&
              this.idConsulta.equals(other.getIdConsulta())))
            && ((this.rut==null && other.getRut()==null) || 
             (this.rut!=null &&
              this.rut.equals(other.getRut())))
            && ((this.dv==null && other.getDv()==null) || 
             (this.dv!=null &&
              this.dv.equals(other.getDv())))
            && ((this.numeroSerie==null && other.getNumeroSerie()==null) || 
             (this.numeroSerie!=null &&
              this.numeroSerie.equals(other.getNumeroSerie())))
            && ((this.numeroRegistros==null && other.getNumeroRegistros()==null) || 
             (this.numeroRegistros!=null &&
              this.numeroRegistros.equals(other.getNumeroRegistros())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        EstadoCedula history = (EstadoCedula) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getIdConsulta() != null) {
            _hashCode += getIdConsulta().hashCode();
        }
        if (getRut() != null) {
            _hashCode += getRut().hashCode();
        }
        if (getDv() != null) {
            _hashCode += getDv().hashCode();
        }
        if (getTipoDocumento() != null) {
            _hashCode += getTipoDocumento().hashCode();
        }
        if (getNumeroSerie() != null) {
            _hashCode += getNumeroSerie().hashCode();
        }
        if (getRazon() != null) {
            _hashCode += getRazon().hashCode();
        }
        if (getFecha() != null) {
            _hashCode += getFecha().hashCode();
        }
        if (getFuente() != null) {
            _hashCode += getFuente().hashCode();
        }
        if (getCedulaVigente() != null) {
            _hashCode += getCedulaVigente().hashCode();
        }
        if (getNumeroRegistros() != null) {
            _hashCode += getNumeroRegistros().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
