<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Consulta de Log de Validaci&oacute;n Interfaz Motor de Pago </title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "LogList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<script language="JavaScript">



function goAction(op) {
	document.forms[0].submit();
}

</SCRIPT>  

</head>

<BODY>
<h3 align="center">Consulta de Log de Validaci&oacute;n Interfaz Motor de Pago
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="validation_log_list, EREC101"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.motorpago.JSEREC101" >
    <input type=HIDDEN name="SCREEN" value="400">
  
    <%
	if ( LogList.getNoResult() ) {
 %>
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
        </div>
	  </TD>
	</TR>
  </TABLE>

  <%  
	}
	else 
	{
 
  if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }

%> 
 
          
  <table class="tbenter" width=100% align=center height="8%">
    <tr> 
      <td class=TDBKG width="25%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Ver Detalle</b></a></div>
      </td>
  </table>
  <br>

  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
            <th align=CENTER nowrap width="10%">ID. INTERFAZ</th>
            <th align=LEFT nowrap >NOMBRE INTERFAZ</th>
            <th align=LEFT nowrap >FECHA PROCESO</th>
            <th align=LEFT nowrap >ESTADO VALIDACI&Oacute;N</th>
            <th align=LEFT nowrap >NRO. REG. PROC.</th>
            <th align=LEFT nowrap >TOTAL REG. PROC.</th>
            <th align=LEFT nowrap >NRO. REG. ERR.</th>
            <th align=LEFT nowrap >TOTAL REG. ERR.</th>
          </tr>
 
           <%
                LogList.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (LogList.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	datapro.eibs.beans.EREC10101Message msgLogList = (datapro.eibs.beans.EREC10101Message) LogList.getRecord();
		 %>
          <tr id="dataTable<%= LogList.getCurrentRow() %>"> 
            <td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE" value="<%= LogList.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>
            <td NOWRAP  align=CENTER width="10%"><%= msgLogList.getS01REHIDE() %></td>
            <td NOWRAP  align=LEFT ><%= msgLogList.getS01REHINT() %></td>
            <td NOWRAP  align="center" ><% out.print(msgLogList.getS01REHFCD()+"/"+msgLogList.getS01REHFCM()+"/"+msgLogList.getS01REHFCY());  %></td>
            <td NOWRAP  align=LEFT ><%= msgLogList.getS01REHDES() %></td>
            <td NOWRAP  align="right" ><%= msgLogList.getS01REHTRP() %></td>
            <td NOWRAP  align="right" ><%= Util.formatCCY(msgLogList.getS01REHTMP()) %></td>
            <td NOWRAP  align="right" ><%= msgLogList.getS01REHTRE() %></td>
            <td NOWRAP  align="right" ><%= Util.formatCCY(msgLogList.getS01REHTME()) %></td>
          </tr>
          <%
                }
              %>
          </table>
      </td>
    </tr>
  </table>
  

     
<SCRIPT language="JavaScript">
	showChecked("CURRCODE");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

</body>
</html>
