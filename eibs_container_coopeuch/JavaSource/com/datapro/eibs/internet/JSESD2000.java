package com.datapro.eibs.internet;

import java.io.*;
import java.net.*;
import java.beans.Beans;
import javax.servlet.*;
import javax.servlet.http.*;

import datapro.eibs.beans.*;
import datapro.eibs.sockets.*;

import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.SuperServlet;

/**
 * @author erodriguez
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

public class JSESD2000 extends SuperServlet {

	Socket s = null;
	MessageContext mc = null;
	ESS0030DSMessage user = null;
	ELEERRMessage msgError = null;
	JBObjList jbList = null;
	UserPos userPO = null;

	// Action 
	protected static final int A_POSITION 	= 800;
	protected static final int R_PREV_LIST 	= 100;
	protected static final int R_ENTER 		= 1;
	protected static final int R_NEW 		= 300;
	protected static final int R_MAINTENANCE= 500;
	protected static final int A_MAINTENANCE= 600;
	
	String LangPath = "e/";
	
	public JSESD2000() {
		super();
	}
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	public void destroy() {
		try {
			if (s != null) s.close();
			flexLog("free resources used by JSESD2000");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
		}
	}

	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = (HttpSession) req.getSession(false);
		if (session == null) {
			try {
				res.setContentType("text/html");
				super.printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {
			int screen = -1;

			user = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");
			// Here we should get the path from the user profile
			LangPath = super.rootPath + user.getE01LAN() + "/";
			
			userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
			
			try {
				s = new Socket(super.hostIP, getInitSocket(req) + 1);
				s.setSoTimeout(super.sckTimeOut);
				mc =
					new MessageContext(
						new DataInputStream(new BufferedInputStream(s.getInputStream())),
						new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
						"datapro.eibs.beans");
						
				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
					flexLog("Screen  Number: " + screen);
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}
				
				String PageToCall = "";

				switch (screen) {
					//Request
					case R_PREV_LIST :
						procReqTablesList(req, res, session);
						break;
					case R_NEW :
						procReqNew(req, res, session);							
						break;	
					case R_MAINTENANCE :
						procReqMaintenance(req, res, session);							
						break;
					// Action
					case A_POSITION :
						procActionPos(req, res, session);
						break;
					case A_MAINTENANCE :
						procActionMaintenance(req, res, session);							
						break;
					default :
						PageToCall = "MISC_not_available.jsp";
						callPage(PageToCall, req, res);
						break;
				}

			} catch (IOException e) {
				e.printStackTrace();
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			} finally {
				s.close();
				flexLog("Socket used by JSESD2000 closed.");
			}
		}	
	}

	/**
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procActionMaintenance(HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException {
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ESD200001Message msgRT = (ESD200001Message) session.getAttribute("prevDetails");
			msgRT = (ESD200001Message) msgHandle.initMessage(msgRT, user.getH01USR(), "0005");
			msgRT.setH01SCRCOD("01");
			msgHandle.setFieldsFromPage(req, msgRT);
			msgHandle.sendMessage(msgRT);
			msgError = msgHandle.receiveErrorMessage();
			boolean isNotError = msgError.getERRNUM().equals("0");
			msgRT = (ESD200001Message) msgHandle.receiveMessage();
			if (isNotError) {
				procReqTablesList(req, res, session);
			} else {
				putDataInSession(session, "prevDetails", msgRT);
				String PageToCall = "ESD2000_internet_parameters_details.jsp";
				callPage(PageToCall, req, res);
				
			}
		
	} catch (ClassNotFoundException e) {
		throw new IOException(e.getMessage());
	} catch (IllegalAccessException e) {
		throw new IOException(e.getMessage());
	} catch (InstantiationException e) {
		throw new IOException(e.getMessage());
	} catch (Exception e) {
		throw new IOException(e.getMessage());
	}
	}

	/**
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procActionPos(HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException {
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt"));
		} catch (Exception e) {
			inptOPT = 0;
		}

		switch (inptOPT) {
			case 1 : //New
				procReqNew(req, res, session);
				break;
			case 2 : //Maintenance
				procReqMaintenance(req, res, session);
				break;
			default :
				procReqTablesList(req, res, session);				
				break;
		}
		
	}

	/**
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqMaintenance(HttpServletRequest req, HttpServletResponse res, HttpSession session) {
		userPO.setHeader21(user.getE01INT());
		initTransaction("", "MAINTENANCE");
		jbList = (JBObjList) session.getAttribute("ESD200001Help");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE"));
		} catch (Exception e) {
			idx = 0;
		}
		jbList.setCurrentRow(idx);
		ESD200001Message msgDoc = (ESD200001Message) jbList.getRecord();
		putDataInSession(session, "prevDetails", msgDoc);
		String PageToCall = "ESD2000_internet_parameters_details.jsp";
		callPage(PageToCall, req, res);
		
	}

	/**
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqNew(HttpServletRequest req, HttpServletResponse res, HttpSession session) {
		userPO.setHeader21(user.getE01INT());
		initTransaction("", "NEW");
		ESD200001Message msgRT = new ESD200001Message();
		putDataInSession(session, "prevDetails", msgRT);
		String PageToCall = "ESD2000_internet_parameters_details.jsp";
		callPage(PageToCall, req, res);
		
	}

	/**
	 * @param req
	 * @param res
	 * @param session
	 */
	private void procReqTablesList(HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException {
		try {
			MessageContextHandler msgHandle = new MessageContextHandler(mc);
			ESD200001Message msg = (ESD200001Message) msgHandle.initMessage("ESD200001", user.getH01USR(), "0015");
			msg.setH01SCRCOD("01");
			msgHandle.sendMessage(msg);
			msgError = msgHandle.receiveErrorMessage();
			boolean isNotError = msgError.getERRNUM().equals("0");
			jbList = msgHandle.receiveMessageList("E01FINREC", JSEIBSProp.getMaxIterations());
			putDataInSession(session, "ESD200001Help", jbList);
			String PageToCall = "ESD2000_internet_parameters_list.jsp";
			callPage(PageToCall, req, res);
		
		} catch (ClassNotFoundException e) {
			throw new IOException(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new IOException(e.getMessage());
		} catch (InstantiationException e) {
			throw new IOException(e.getMessage());
		} catch (Exception e) {
			throw new IOException(e.getMessage());
		}
	}

	private void initTransaction(String optMenu, String purpose) {
		try {
			if (!optMenu.equals("")) userPO.setOption(optMenu);
			if (!purpose.equals("")) userPO.setPurpose(purpose);
			
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}
		try {
		} catch (Exception ex) {
			flexLog("Error getting userPO from session: " + ex);
		}
	}
	
	public void callPage(String page, HttpServletRequest req, HttpServletResponse res) {
		try {
			super.callPage(LangPath + page, req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e.toString() + e.getMessage());
		}
		return; 
	}
	
	private void putDataInSession(HttpSession session, String name, Object obj) {
		try {
			flexLog("Putting java beans into the session");

			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			if (obj != null)	session.setAttribute(name, obj);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error at putBeansInSession(): " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}

}
