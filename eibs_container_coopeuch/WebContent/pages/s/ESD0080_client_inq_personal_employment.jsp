<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Datos de Empleo</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<jsp:useBean id= "employment" class= "datapro.eibs.beans.ESD008006Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<SCRIPT Language="Javascript">

  builtNewMenu(client_inq_personal_opt);
  initMenu();
  
  
  function FillZeroLeft_ini(idField){

		field = document.getElementById(idField);
	    while (field.value.length < field.maxLength )
	      field.value='0'+field.value;	  	 
}
</SCRIPT>

</head>




<body>


<h3 align="center">Datos del Empleo <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="client_inq_personal_employment, ESD0080"></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="6">
  <INPUT TYPE=HIDDEN NAME="H06WK3" VALUE="<%=employment.getH06WK3().trim() %>">
  <INPUT TYPE=HIDDEN NAME="H06WK2" VALUE="<%=employment.getH06WK2().trim() %>">
   
<table class="tableinfo">
  <tr > 
    <td nowrap> 
      <table cellspacing="0" cellpadding="2" width="100%" class="tbhead">
        <tr>
          <td nowrap align="right"> Cliente :</td>
          <td nowrap align="left"><%= userPO.getHeader1()%></td>
          <td nowrap align="right"> ID. :</td>
          <td nowrap align="left"><%= userPO.getHeader2()%></td>
          <td nowrap align="right"> Nombre :</td>
          <td nowrap align="left"><%= userPO.getHeader3()%></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table class="tableinfo" >
    <tr > 
      <td wrap >
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Nombre de la Empresa :</div>
            </td>
            <td nowrap colspan="4"> 
                <eibsinput:text name="employment" property="E06CP1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="true"/>
             </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Direcci&oacute;n :</div>
            </td>
            <td nowrap colspan="4"> 
                <eibsinput:text name="employment" property="E06CP2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="true"/>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Tipo Empresa :</div>
            </td>
            <td nowrap > 
                <eibsinput:text name="employment" property="E06EPT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>" readonly="true"/>
                <eibsinput:text name="employment" property="D06EPT" size="46" readonly="true"/>
              </td>
            <td nowrap > 
              <div align="right">Rut Empresa :</div>
            </td>
            <td nowrap >
              <input type="text" name="E06RUC" size="26" maxlength="25" value="<%= employment.getE06RUC().trim()%>">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Cargo :</div>
            </td>
            <td nowrap >
                <eibsinput:text name="employment" property="E06FC3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>" readonly="true"/>
                <eibsinput:text name="employment" property="D06FC3" size="46" readonly="true"/>
            </td>
            <td nowrap > 
              <div align="right"> Cargo Estrategico</div>
            </td>
            <td nowrap >              
               <input type="text" readonly name="E06L05" size="3" maxlenth="3"
			  value="<% if (employment.getE06L05().equals("Y")) { out.print("Si"); }
						else { out.print("No"); } %>" >
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Actividad Econ&oacute;mica : </div>
            </td>
            <td nowrap > 
                <eibsinput:text name="employment" property="E06UC5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>" readonly="true"/>
                <eibsinput:text name="employment" property="D06UC5" size="46" readonly="true"/>                
            </td>
            <td nowrap > 
              <div align="right">Fecha Ingreso Empleo Acual :</div>
            </td>
            <td nowrap >
             <input type="text" name="E06SWM" size="3" maxlength="2" value="<%= employment.getE06SWM().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             <input type="text" name="E06SWD" size="3" maxlength="2" value="<%= employment.getE06SWD().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             <input type="text" name="E06SWY" size="5" maxlength="4" value="<%= employment.getE06SWY().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">             
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap>  
              <div align="right">Tiempo de Empleo(A�os) :</div>
            </td>
            <td nowrap > 
              <input type="text" name="E06TIM" size="6" maxlength="2" value="<%= employment.getE06TIM().trim()%>">
            </td>
            <td nowrap > 
              <div align="right">Fecha Ingreso Empleo Anterior :</div>
            </td>
            <td nowrap> 
             <input type="text" name="E06IA1" size="3" maxlength="2" value="<%= employment.getE06IA1().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             <input type="text" name="E06IA2" size="3" maxlength="2" value="<%= employment.getE06IA2().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             <input type="text" name="E06IA3" size="5" maxlength="4" value="<%= employment.getE06IA3().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">                          
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Tipo de Contrato :</div>
            </td>
            <td nowrap >
             <SELECT name="E06NEM">
                   <OPTION value=" " <% if (!(employment.getE06NEM().equals("1") || 
                                              employment.getE06NEM().equals("2") || 
                                              employment.getE06NEM().equals("3") || 
                                              employment.getE06NEM().equals("4") || 
                                              employment.getE06NEM().equals("5") || 
                                              employment.getE06NEM().equals("6") || 
                                              employment.getE06NEM().equals("7") || 
                                              employment.getE06NEM().equals("8") ||                                                                                                                                                                                                                                                                                                                                                                                 
                                              employment.getE06NEM().equals("9")) ) out.print("selected");%>></OPTION>
                   <OPTION value="1" <% if (employment.getE06NEM().equals("1")) out.print("selected"); %>="">Contrata</OPTION>
                   <OPTION value="2" <% if (employment.getE06NEM().equals("2")) out.print("selected"); %>="">Honorario</OPTION>
                   <OPTION value="3" <% if (employment.getE06NEM().equals("3")) out.print("selected"); %>="">Indefinido</OPTION>
                   <OPTION value="4" <% if (employment.getE06NEM().equals("4")) out.print("selected"); %>="">Independiente</OPTION>
                   <OPTION value="5" <% if (employment.getE06NEM().equals("5")) out.print("selected"); %>="">Pensionado</OPTION>
                   <OPTION value="6" <% if (employment.getE06NEM().equals("6")) out.print("selected"); %>="">Planta</OPTION>
                   <OPTION value="7" <% if (employment.getE06NEM().equals("7")) out.print("selected"); %>="">Plazo Fijo</OPTION> 
                   <OPTION value="8" <% if (employment.getE06NEM().equals("8")) out.print("selected"); %>="">Suplencia</OPTION>
                   <OPTION value="9" <% if (employment.getE06NEM().equals("9")) out.print("selected"); %>="">Sin COntrato</OPTION>                                                                                                                 
             </SELECT>
            </td>
            <td nowrap > 
              <div align="right">Fecha Termino Empleo Anterior :</div>
            </td>
            <td nowrap > 
             <input type="text" name="E06FA1" size="3" maxlength="2" value="<%= employment.getE06FA1().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             <input type="text" name="E06FA2" size="3" maxlength="2" value="<%= employment.getE06FA2().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             <input type="text" name="E06FA3" size="5" maxlength="4" value="<%= employment.getE06FA3().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">                    
            </td>
          </tr>
          
        </table>
      </td>
    </tr>
  </table>
    
  <br>
  <table class="tableinfo" >
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trdark"> 
            <td nowrap> 
              <div align="right">Sueldo L&iacute;quido :</div>
            </td>
            <td nowrap> 
                <eibsinput:text name="employment" property="E06AMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap>
              <div align="right">Fecha Eval. Salario :</div>
            </td>
            <td nowrap>
             <INPUT type="text" name="E06LSM" size="3" maxlength="2" value="<%= employment.getE06LSM().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             <INPUT type="text" name="E06LSD" size="3" maxlength="2" value="<%= employment.getE06LSD().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             <INPUT type="text" name="E06LSY" size="5" maxlength="4" value="<%= employment.getE06LSY().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Tipo de Salario :</div>
            </td>
            <td nowrap >
               <SELECT name="E06SAT">
                       <OPTION value=" " <% if (!(employment.getE06SAT().equals("W") || employment.getE06SAT().equals("M") || employment.getE06SAT().equals("S") || employment.getE06SAT().equals("Y")|| employment.getE06SAT().equals("B"))) out.print("selected");%>></OPTION>
                       <OPTION value="W" <% if (employment.getE06SAT().equals("W")) out.print("selected"); %>>Semanal</OPTION>
					  <OPTION value="B" <% if (employment.getE06SAT().equals("B")) out.print("selected"); %>="">Quincenal</OPTION>
                       <OPTION value="M" <% if (employment.getE06SAT().equals("M")) out.print("selected"); %>>Mensual</OPTION>
                       <OPTION value="S" <% if (employment.getE06SAT().equals("S")) out.print("selected"); %>>Semestral</OPTION>
                       <OPTION value="Y" <% if (employment.getE06SAT().equals("Y")) out.print("selected"); %>>Anual</OPTION>
               </SELECT>
            </td>
            <td nowrap > 
              <div align="right">D�a de Pago en el Mes :</div>
            </td>
            <td nowrap ><INPUT type="text" name="E06MPA" size="2" maxlength="2" value="<%= employment.getE06MPA().trim()%>" onkeypress="enterInteger()"> </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Patrimonio Neto :</div>
            </td>
            <td nowrap >
                <eibsinput:text name="employment" property="E06CAP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
             </td>
            <td nowrap > 
              <div align="right">Fecha Patrimonio :</div>
            </td>
            <td nowrap >
             <INPUT type="text" name="E06DP1" size="3" maxlength="2" value="<%= employment.getE06DP1().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             <INPUT type="text" name="E06DP2" size="3" maxlength="2" value="<%= employment.getE06DP2().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
             <INPUT type="text" name="E06DP3" size="5" maxlength="4" value="<%= employment.getE06DP3().trim()%>" onkeypress="enterInteger()" class="TXTRIGHT">
            </td>
          </tr>          
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Sueldo Bruto :</div>
            </td>
            <td nowrap >
                <eibsinput:text name="employment" property="E06CAI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap > 
              <div align="right">Tipo de Renta :</div>
            </td>
            <td nowrap > 
                <SELECT name="E06TYR">
                    <OPTION value=" " <% if (!(employment.getE06TYR().equals("1") || employment.getE06TYR().equals("2")) ) out.print("selected");%>></OPTION>
                    <OPTION value="1" <% if (employment.getE06TYR().equals("1")) out.print("selected"); %>>Fija</OPTION>
                    <OPTION value="2" <% if (employment.getE06TYR().equals("2")) out.print("selected"); %>>Variable</OPTION>
                </SELECT> 
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Otros Ingresos :</div>
            </td>
            <td nowrap >
                <eibsinput:text name="employment" property="E06CAS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap > 
              <div align="right">Renta Depurada :</div>
            </td>
            <td nowrap >
            	<eibsinput:text name="employment" property="E06A11" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
          </tr>
          <tr id="trclear"> 
            <td nowrap > 
              <div align="right">Egreso en Educaci�n :</div>
            </td>
            <td nowrap >
                <eibsinput:text name="employment" property="E06AM3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
            <td nowrap > 
              <div align="right">Otros Egresos :</div>
            </td>
            <td nowrap >
                <eibsinput:text name="employment" property="E06AM4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Origen de Ingresos : </div>
            </td>
            <td nowrap >
                  <eibsinput:text name="employment" property="E06ORI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC %>" readonly="true"/>
                  <eibsinput:text name="employment" property="D06ORI" size="46" readonly="true"/>
            </td>
            <td nowrap > 
              <div align="right">Arriendo :</div>
            </td>
            <td nowrap >
                <eibsinput:text name="employment" property="E06AM2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="true"/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

    
  <br>
  <table class="tableinfo" >
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trclear"> 
            <td nowrap> 
              <div align="right">Rol de Pago :</div>
            </td>
            <td nowrap> 
              <input type="text" readonly name="E06XU3" size="16" maxlength="15" value="<%= employment.getE06XU3().trim()%>">
            </td>
            <td nowrap>
              <div align="right">N�mero de Seguro Social :</div>
            </td>
            <td nowrap>
              <input type="text" readonly name="E06NDO" size="13" maxlength="12" value="<%= employment.getE06NDO().trim()%>" onkeypress="enterInteger()">
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right">Identificaci�n Contralor�a :</div>
            </td>
            <td nowrap >
              <input type="text" readonly name="E06CD3" size="5" maxlength="4" value="<%= employment.getE06CD3().trim()%>">
              <input type="text" readonly name="E06CD4" size="5" maxlength="4" value="<%= employment.getE06CD4().trim()%>">
              <input type="text" readonly name="E06F01" size="6" maxlength="5" value="<%= employment.getE06F01().trim()%>">
            </td>
            <td nowrap > 
              <div align="right">Rol de Pago Pensionados : </div>
            </td>
            <td nowrap >
              <input type="text" name="E06RP1" size="3" maxlength="2" value="<%= employment.getE06RP1().trim()%>" readonly>
              <input type="text" name="E06RP2" size="15" maxlength="13" value="<%= employment.getE06RP2().trim()%>" readonly>
              <input type="text" name="E06RP3" size="2" maxlength="1" value="<%= employment.getE06RP3().trim()%>" readonly>
              <input type="text" name="E06RP4" size="2" maxlength="1" value="<%= employment.getE06RP4().trim()%>" readonly >
              <input type="text" name="E06RP5" size="3" maxlength="2" value="<%= employment.getE06RP5().trim()%>" readonly>
              <input type="text" name="E06RP6" size="2" maxlength="1" value="<%= employment.getE06RP6().trim()%>" readonly >

            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap > 
              <div align="right"></div>
            </td>
            <td nowrap >
            </td>
            <td nowrap > 
              <div align="right">Motivo : </div>
            </td>
            <td nowrap >
                <input type="text" name="E06XO2" size="5" maxlength="4" value="<%= employment.getE06XO2()%>" readonly >
                <input type="text" name="E06DMO" size="50" maxlength="45" value="<%=employment.getE06DMO()%>" readonly >
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  
  
  
 <SCRIPT Language="Javascript">
   var j=document.forms[0].elements.length;
    var felem=document.forms[0].elements;
    for(i=0;i< j;i++){
       if (felem[i].tagName!=="select"){
         if (felem[i].type=="text") felem[i].readOnly=true; else felem[i].disabled=true;
       } 
       else { felem[i].disabled=true;}
    }
</SCRIPT> 
</form>
<SCRIPT Language="Javascript">

FillZeroLeft_ini('E06NDO');
FillZeroLeft_ini('E06CD3');
FillZeroLeft_ini('E06CD4');
FillZeroLeft_ini('E06F01');
FillZeroLeft_ini('E06RP1');
FillZeroLeft_ini('E06RP2');
FillZeroLeft_ini('E06RP5');

</SCRIPT>
</body>
</html>
