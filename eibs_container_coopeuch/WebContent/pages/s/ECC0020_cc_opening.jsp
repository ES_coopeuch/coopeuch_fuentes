<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%-- 
<html>
<head>
<title>Credit Cards Mantenance</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id="ccNew" class="datapro.eibs.beans.ECC002001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<SCRIPT LANGUAGE="javascript">

   builtHPopUp();

  function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }
   
</SCRIPT>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0"); 
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
  
 if (!userPO.getPurpose().equals("NEW")){
   out.println("<SCRIPT> initMenu();  </SCRIPT>");}

%> 

</head>
<body>
<h3 align="center" >Plataforma PosVenta<BR> Tarjetas Ingreso de Solicitudes

<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cc_opening.jsp,ECC0020"> 
</h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0020" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="4">
  <INPUT TYPE=HIDDEN NAME="E01CCMBNK" VALUE="<%= ccNew.getE01CCMBNK().trim()%>">
  <INPUT TYPE=HIDDEN NAME="E01CCMATY" VALUE="<%= ccNew.getE01CCMATY().trim()%>">
  
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" >
              <div align="left" >
                <input type="text" name="E01CCMCUN" size="10" maxlength="9" value="<%= ccNew.getE01CCMCUN().trim()%>" readonly >
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b></div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"> 
                <input type="text" name="E01CUSNA1" size="45" maxlength="45" value="<%= ccNew.getE01CUSNA1().trim()%>" readonly >
              </div>
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta : </b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left"> 
                <input type="text" readonly name="E01CCMACC" size="13" maxlength="9" value="<% if (ccNew.getE01CCMACC().trim().equals("999999999999")) out.print("NUEVA CUENTA"); else out.print(ccNew.getE01CCMACC().trim()); %>">
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda :</b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
                <input type="text" name="E01CCMCCY" size="3" maxlength="3" value="<%= ccNew.getE01CCMCCY().trim()%>" readonly>
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b> 
                <input type="text" name="E01CCMPRO" size="4" maxlength="4" value="<%= ccNew.getE01CCMPRO().trim()%>" readonly>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Informacion de la Cuenta</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="25%"> 
              <div align="right">Estado de la Cuenta:</div>
            </td>
            <td nowrap width="23%"> 
              <select name="E01CCMAST">
                <option value=" " <% if (!(
		                	ccNew.getE01CCMAST().equals("A") || 
                			ccNew.getE01CCMAST().equals("C") || 
                			ccNew.getE01CCMAST().equals("V"))) 
                			out.print("selected"); %> selected></option>
                <option value="A" <% if(ccNew.getE01CCMAST().equals("A")) out.print("selected");%>>Activo</option>
                <option value="C" <% if(ccNew.getE01CCMAST().equals("C")) out.print("selected");%>>Cancelado</option>                			
                <option value="V" <% if(ccNew.getE01CCMAST().equals("V")) out.print("selected");%>>Vencido</option>
              </select>
             </td>
            <td nowrap width="25%"> 
              <div align="right">Fecha de Emision :</div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" name="E01CCMOPD" size="3" maxlength="2" value="<%= ccNew.getE01CCMOPD().trim()%>" readonly>
              <input type="text" name="E01CCMOPM" size="3" maxlength="2" value="<%= ccNew.getE01CCMOPM().trim()%>" readonly>
              <input type="text" name="E01CCMOPY" size="5" maxlength="4" value="<%= ccNew.getE01CCMOPY().trim()%>" readonly>
            </td>
            </tr>  
          <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right">Cupo Nacional :</div>
            </td>
            <td nowrap width="27%"> 
              <input type="text" name="E01CCMLBC" size="17" maxlength="16" value="<%= ccNew.getE01CCMLBC().trim()%>" readonly> 
 			</td>
            <td nowrap width="25%">  
              <div align="right">Cupo Internacional:</div>
            </td>
            <td nowrap width="23%"> 
				<input type="text" name="E01CCMLFC" size="17" maxlength="16" value="<%= ccNew.getE01CCMLFC().trim()%>" readonly> 
            </td>
          </tr>                          
          <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right">Codigo VIP  :</div>
            </td>
            <td nowrap width="23%"> 
				<input type="text" name="E01CCMVIP" size="2" maxlength="1" value="<%= ccNew.getE01CCMVIP().trim()%>" readonly> 
            </td>
            <td nowrap width="25%"> 
              <div align="right">No. Tarjetas Adicionales:</div>
            </td>
            <td nowrap width="27%"> 
              <input type="text" name="E01CCMNCC" size="8" maxlength="7" value="<%= ccNew.getE01CCMNCC().trim()%>" readonly> 
 			</td>
          </tr>  
          <tr id="trclear"> 
            <td nowrap width="25%"> 
              <div align="right">Situacion  :</div>
            </td>
            <td nowrap width="23%"> 
				<input type="text" name="E01CCMVIP" size="2" maxlength="1" value="<%= ccNew.getE01CCMVIP().trim()%>" readonly> 
            </td>
            <td nowrap width="25%"> 
              <div align="right">Tipo Emision:</div>
            </td>
            <td nowrap width="27%"> 
              <input type="text" name="E01CCMNCC" size="8" maxlength="7" value="<%= ccNew.getE01CCMNCC().trim()%>" readonly> 
 			</td>
          </tr>            
        </table>
      </td>
    </tr>
  </table>
  
  <br>
  <div align="center"> 
	   <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
  </form>
</body>
</html>

--%>