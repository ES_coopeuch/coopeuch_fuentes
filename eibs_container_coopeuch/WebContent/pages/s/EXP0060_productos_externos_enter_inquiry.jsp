<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Productos Externos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">


<SCRIPT SRC="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<jsp:useBean id= "externos" class= "datapro.eibs.beans.EXP006001Message"  scope="session" /> 
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

</head>
<body>

<H3 align="center">Consulta de Productos Externos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="externos_enter_inquiry,ELC5000"></H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXP0060">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="300">

  <h4>&nbsp;</h4>
  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>

    <tr>
      <td nowrap width="50%">
        <div align="right">N&uacute;mero de Cuenta : </div>
      </td>
      <td nowrap width="50%">
        <input type="text" name="E01EXPACC" size="14" maxlength="12" onKeypress="enterInteger()" value="<%= externos.getE01EXPACC() %>">
        <a href="javascript:GetAccount('E01EXPACC','','70','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" align="bottom" border="0" ></a>
      </td>
    </tr>
  </table>
  <br>

  <div align="center">
     <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
<script language="JavaScript">
  document.forms[0].E01EXPACC.focus();
  document.forms[0].E01EXPACC.select();
</script>
<%
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
