package datapro.eibs.beans;

import datapro.eibs.sockets.*;
import java.io.*;
import java.math.*;

import java.util.*;

/**
* Class generated by AS/400 CRTCLASS command from ERC200001 physical file definition.
* 
* File level identifier is 1140303110822.
* Record format level identifier is 39238BBEDFAC2.
*/

public class ERC200001Message extends MessageRecord
{
  final static String fldnames[] = {
                                     "H01USERID",
                                     "H01PROGRM",
                                     "H01TIMSYS",
                                     "H01SCRCOD",
                                     "H01OPECOD",
                                     "H01FLGMAS",
                                     "H01FLGWK1",
                                     "H01FLGWK2",
                                     "H01FLGWK3",
                                     "E01RCHRBK",
                                     "E01RCHCTA",
                                     "E01RCHBNK",
                                     "E01RCHCCY",
                                     "E01RCHGLN",
                                     "E01RCHACC",
                                     "E01RCHSDM",
                                     "E01RCHSDD",
                                     "E01RCHSDY",
                                     "E01RCHFSM",
                                     "E01RCHFSD",
                                     "E01RCHFSY",
                                     "E01RCHFLM",
                                     "E01RCHFLD",
                                     "E01RCHFLY",
                                     "E01RCHTFC",
                                     "E01RCHSTN",
                                     "E01RCHBBL",
                                     "E01RCHBBF",
                                     "E01RCHTRG",
                                     "E01RCHTRE",
                                     "E01RCHUSA",
                                     "E01RCHUSO",
                                     "E01RCHTCA",
                                     "E01RCHAPF",
                                     "E01DESAPF",
                                     "E01RCHLMM",
                                     "E01RCHLMD",
                                     "E01RCHLMY",
                                     "E01RCHLMT",
                                     "E01RCHLMU",
                                     "E01RCHRNU",
                                     "E01RCHREC"
                                   };
  final static String tnames[] = {
                                   "H01USERID",
                                   "H01PROGRM",
                                   "H01TIMSYS",
                                   "H01SCRCOD",
                                   "H01OPECOD",
                                   "H01FLGMAS",
                                   "H01FLGWK1",
                                   "H01FLGWK2",
                                   "H01FLGWK3",
                                   "E01RCHRBK",
                                   "E01RCHCTA",
                                   "E01RCHBNK",
                                   "E01RCHCCY",
                                   "E01RCHGLN",
                                   "E01RCHACC",
                                   "E01RCHSDM",
                                   "E01RCHSDD",
                                   "E01RCHSDY",
                                   "E01RCHFSM",
                                   "E01RCHFSD",
                                   "E01RCHFSY",
                                   "E01RCHFLM",
                                   "E01RCHFLD",
                                   "E01RCHFLY",
                                   "E01RCHTFC",
                                   "E01RCHSTN",
                                   "E01RCHBBL",
                                   "E01RCHBBF",
                                   "E01RCHTRG",
                                   "E01RCHTRE",
                                   "E01RCHUSA",
                                   "E01RCHUSO",
                                   "E01RCHTCA",
                                   "E01RCHAPF",
                                   "E01DESAPF",
                                   "E01RCHLMM",
                                   "E01RCHLMD",
                                   "E01RCHLMY",
                                   "E01RCHLMT",
                                   "E01RCHLMU",
                                   "E01RCHRNU",
                                   "E01RCHREC"
                                 };
  final static String fid = "1140303110822";
  final static String rid = "39238BBEDFAC2";
  final static String fmtname = "ERC200001";
  final int FIELDCOUNT = 42;
  private static Hashtable tlookup = new Hashtable();
  private CharacterField fieldH01USERID = null;
  private CharacterField fieldH01PROGRM = null;
  private CharacterField fieldH01TIMSYS = null;
  private CharacterField fieldH01SCRCOD = null;
  private CharacterField fieldH01OPECOD = null;
  private CharacterField fieldH01FLGMAS = null;
  private CharacterField fieldH01FLGWK1 = null;
  private CharacterField fieldH01FLGWK2 = null;
  private CharacterField fieldH01FLGWK3 = null;
  private CharacterField fieldE01RCHRBK = null;
  private CharacterField fieldE01RCHCTA = null;
  private CharacterField fieldE01RCHBNK = null;
  private CharacterField fieldE01RCHCCY = null;
  private DecimalField fieldE01RCHGLN = null;
  private DecimalField fieldE01RCHACC = null;
  private DecimalField fieldE01RCHSDM = null;
  private DecimalField fieldE01RCHSDD = null;
  private DecimalField fieldE01RCHSDY = null;
  private DecimalField fieldE01RCHFSM = null;
  private DecimalField fieldE01RCHFSD = null;
  private DecimalField fieldE01RCHFSY = null;
  private DecimalField fieldE01RCHFLM = null;
  private DecimalField fieldE01RCHFLD = null;
  private DecimalField fieldE01RCHFLY = null;
  private DecimalField fieldE01RCHTFC = null;
  private DecimalField fieldE01RCHSTN = null;
  private DecimalField fieldE01RCHBBL = null;
  private DecimalField fieldE01RCHBBF = null;
  private DecimalField fieldE01RCHTRG = null;
  private DecimalField fieldE01RCHTRE = null;
  private CharacterField fieldE01RCHUSA = null;
  private CharacterField fieldE01RCHUSO = null;
  private CharacterField fieldE01RCHTCA = null;
  private CharacterField fieldE01RCHAPF = null;
  private CharacterField fieldE01DESAPF = null;
  private DecimalField fieldE01RCHLMM = null;
  private DecimalField fieldE01RCHLMD = null;
  private DecimalField fieldE01RCHLMY = null;
  private CharacterField fieldE01RCHLMT = null;
  private CharacterField fieldE01RCHLMU = null;
  private CharacterField fieldE01RCHRNU = null;
  private DecimalField fieldE01RCHREC = null;

  /**
  * Constructor for ERC200001Message.
  */
  public ERC200001Message()
  {
    createFields();
    initialize();
  }

  /**
  * Create fields for this message.
  * This method implements the abstract method in the MessageRecord superclass.
  */
  protected void createFields()
  {
    recordsize = 306;
    fileid = fid;
    recordid = rid;
    message = new byte[getByteLength()];
    formatname = fmtname;
    fieldnames = fldnames;
    tagnames = tnames;
    fields = new MessageField[FIELDCOUNT];

    fields[0] = fieldH01USERID =
    new CharacterField(message, HEADERSIZE + 0, 10, "H01USERID");
    fields[1] = fieldH01PROGRM =
    new CharacterField(message, HEADERSIZE + 10, 10, "H01PROGRM");
    fields[2] = fieldH01TIMSYS =
    new CharacterField(message, HEADERSIZE + 20, 12, "H01TIMSYS");
    fields[3] = fieldH01SCRCOD =
    new CharacterField(message, HEADERSIZE + 32, 2, "H01SCRCOD");
    fields[4] = fieldH01OPECOD =
    new CharacterField(message, HEADERSIZE + 34, 4, "H01OPECOD");
    fields[5] = fieldH01FLGMAS =
    new CharacterField(message, HEADERSIZE + 38, 1, "H01FLGMAS");
    fields[6] = fieldH01FLGWK1 =
    new CharacterField(message, HEADERSIZE + 39, 1, "H01FLGWK1");
    fields[7] = fieldH01FLGWK2 =
    new CharacterField(message, HEADERSIZE + 40, 1, "H01FLGWK2");
    fields[8] = fieldH01FLGWK3 =
    new CharacterField(message, HEADERSIZE + 41, 1, "H01FLGWK3");
    fields[9] = fieldE01RCHRBK =
    new CharacterField(message, HEADERSIZE + 42, 4, "E01RCHRBK");
    fields[10] = fieldE01RCHCTA =
    new CharacterField(message, HEADERSIZE + 46, 20, "E01RCHCTA");
    fields[11] = fieldE01RCHBNK =
    new CharacterField(message, HEADERSIZE + 66, 2, "E01RCHBNK");
    fields[12] = fieldE01RCHCCY =
    new CharacterField(message, HEADERSIZE + 68, 3, "E01RCHCCY");
    fields[13] = fieldE01RCHGLN =
    new DecimalField(message, HEADERSIZE + 71, 17, 0, "E01RCHGLN");
    fields[14] = fieldE01RCHACC =
    new DecimalField(message, HEADERSIZE + 88, 13, 0, "E01RCHACC");
    fields[15] = fieldE01RCHSDM =
    new DecimalField(message, HEADERSIZE + 101, 3, 0, "E01RCHSDM");
    fields[16] = fieldE01RCHSDD =
    new DecimalField(message, HEADERSIZE + 104, 3, 0, "E01RCHSDD");
    fields[17] = fieldE01RCHSDY =
    new DecimalField(message, HEADERSIZE + 107, 5, 0, "E01RCHSDY");
    fields[18] = fieldE01RCHFSM =
    new DecimalField(message, HEADERSIZE + 112, 3, 0, "E01RCHFSM");
    fields[19] = fieldE01RCHFSD =
    new DecimalField(message, HEADERSIZE + 115, 3, 0, "E01RCHFSD");
    fields[20] = fieldE01RCHFSY =
    new DecimalField(message, HEADERSIZE + 118, 5, 0, "E01RCHFSY");
    fields[21] = fieldE01RCHFLM =
    new DecimalField(message, HEADERSIZE + 123, 3, 0, "E01RCHFLM");
    fields[22] = fieldE01RCHFLD =
    new DecimalField(message, HEADERSIZE + 126, 3, 0, "E01RCHFLD");
    fields[23] = fieldE01RCHFLY =
    new DecimalField(message, HEADERSIZE + 129, 5, 0, "E01RCHFLY");
    fields[24] = fieldE01RCHTFC =
    new DecimalField(message, HEADERSIZE + 134, 2, 0, "E01RCHTFC");
    fields[25] = fieldE01RCHSTN =
    new DecimalField(message, HEADERSIZE + 136, 13, 0, "E01RCHSTN");
    fields[26] = fieldE01RCHBBL =
    new DecimalField(message, HEADERSIZE + 149, 17, 2, "E01RCHBBL");
    fields[27] = fieldE01RCHBBF =
    new DecimalField(message, HEADERSIZE + 166, 17, 2, "E01RCHBBF");
    fields[28] = fieldE01RCHTRG =
    new DecimalField(message, HEADERSIZE + 183, 8, 0, "E01RCHTRG");
    fields[29] = fieldE01RCHTRE =
    new DecimalField(message, HEADERSIZE + 191, 8, 0, "E01RCHTRE");
    fields[30] = fieldE01RCHUSA =
    new CharacterField(message, HEADERSIZE + 199, 10, "E01RCHUSA");
    fields[31] = fieldE01RCHUSO =
    new CharacterField(message, HEADERSIZE + 209, 10, "E01RCHUSO");
    fields[32] = fieldE01RCHTCA =
    new CharacterField(message, HEADERSIZE + 219, 1, "E01RCHTCA");
    fields[33] = fieldE01RCHAPF =
    new CharacterField(message, HEADERSIZE + 220, 1, "E01RCHAPF");
    fields[34] = fieldE01DESAPF =
    new CharacterField(message, HEADERSIZE + 221, 20, "E01DESAPF");
    fields[35] = fieldE01RCHLMM =
    new DecimalField(message, HEADERSIZE + 241, 3, 0, "E01RCHLMM");
    fields[36] = fieldE01RCHLMD =
    new DecimalField(message, HEADERSIZE + 244, 3, 0, "E01RCHLMD");
    fields[37] = fieldE01RCHLMY =
    new DecimalField(message, HEADERSIZE + 247, 5, 0, "E01RCHLMY");
    fields[38] = fieldE01RCHLMT =
    new CharacterField(message, HEADERSIZE + 252, 26, "E01RCHLMT");
    fields[39] = fieldE01RCHLMU =
    new CharacterField(message, HEADERSIZE + 278, 10, "E01RCHLMU");
    fields[40] = fieldE01RCHRNU =
    new CharacterField(message, HEADERSIZE + 288, 10, "E01RCHRNU");
    fields[41] = fieldE01RCHREC =
    new DecimalField(message, HEADERSIZE + 298, 8, 0, "E01RCHREC");

    synchronized (tlookup)
    {
      if (tlookup.isEmpty())
      {
        for (int i = 0; i < tnames.length; i++)
          tlookup.put(tnames[i], new Integer(i));
      }
    }

    taglookup = tlookup;
  }

  /**
  * Set field H01USERID using a String value.
  */
  public void setH01USERID(String newvalue)
  {
    fieldH01USERID.setString(newvalue);
  }

  /**
  * Get value of field H01USERID as a String.
  */
  public String getH01USERID()
  {
    return fieldH01USERID.getString();
  }

  /**
  * Set field H01PROGRM using a String value.
  */
  public void setH01PROGRM(String newvalue)
  {
    fieldH01PROGRM.setString(newvalue);
  }

  /**
  * Get value of field H01PROGRM as a String.
  */
  public String getH01PROGRM()
  {
    return fieldH01PROGRM.getString();
  }

  /**
  * Set field H01TIMSYS using a String value.
  */
  public void setH01TIMSYS(String newvalue)
  {
    fieldH01TIMSYS.setString(newvalue);
  }

  /**
  * Get value of field H01TIMSYS as a String.
  */
  public String getH01TIMSYS()
  {
    return fieldH01TIMSYS.getString();
  }

  /**
  * Set field H01SCRCOD using a String value.
  */
  public void setH01SCRCOD(String newvalue)
  {
    fieldH01SCRCOD.setString(newvalue);
  }

  /**
  * Get value of field H01SCRCOD as a String.
  */
  public String getH01SCRCOD()
  {
    return fieldH01SCRCOD.getString();
  }

  /**
  * Set field H01OPECOD using a String value.
  */
  public void setH01OPECOD(String newvalue)
  {
    fieldH01OPECOD.setString(newvalue);
  }

  /**
  * Get value of field H01OPECOD as a String.
  */
  public String getH01OPECOD()
  {
    return fieldH01OPECOD.getString();
  }

  /**
  * Set field H01FLGMAS using a String value.
  */
  public void setH01FLGMAS(String newvalue)
  {
    fieldH01FLGMAS.setString(newvalue);
  }

  /**
  * Get value of field H01FLGMAS as a String.
  */
  public String getH01FLGMAS()
  {
    return fieldH01FLGMAS.getString();
  }

  /**
  * Set field H01FLGWK1 using a String value.
  */
  public void setH01FLGWK1(String newvalue)
  {
    fieldH01FLGWK1.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK1 as a String.
  */
  public String getH01FLGWK1()
  {
    return fieldH01FLGWK1.getString();
  }

  /**
  * Set field H01FLGWK2 using a String value.
  */
  public void setH01FLGWK2(String newvalue)
  {
    fieldH01FLGWK2.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK2 as a String.
  */
  public String getH01FLGWK2()
  {
    return fieldH01FLGWK2.getString();
  }

  /**
  * Set field H01FLGWK3 using a String value.
  */
  public void setH01FLGWK3(String newvalue)
  {
    fieldH01FLGWK3.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK3 as a String.
  */
  public String getH01FLGWK3()
  {
    return fieldH01FLGWK3.getString();
  }

  /**
  * Set field E01RCHRBK using a String value.
  */
  public void setE01RCHRBK(String newvalue)
  {
    fieldE01RCHRBK.setString(newvalue);
  }

  /**
  * Get value of field E01RCHRBK as a String.
  */
  public String getE01RCHRBK()
  {
    return fieldE01RCHRBK.getString();
  }

  /**
  * Set field E01RCHCTA using a String value.
  */
  public void setE01RCHCTA(String newvalue)
  {
    fieldE01RCHCTA.setString(newvalue);
  }

  /**
  * Get value of field E01RCHCTA as a String.
  */
  public String getE01RCHCTA()
  {
    return fieldE01RCHCTA.getString();
  }

  /**
  * Set field E01RCHBNK using a String value.
  */
  public void setE01RCHBNK(String newvalue)
  {
    fieldE01RCHBNK.setString(newvalue);
  }

  /**
  * Get value of field E01RCHBNK as a String.
  */
  public String getE01RCHBNK()
  {
    return fieldE01RCHBNK.getString();
  }

  /**
  * Set field E01RCHCCY using a String value.
  */
  public void setE01RCHCCY(String newvalue)
  {
    fieldE01RCHCCY.setString(newvalue);
  }

  /**
  * Get value of field E01RCHCCY as a String.
  */
  public String getE01RCHCCY()
  {
    return fieldE01RCHCCY.getString();
  }

  /**
  * Set field E01RCHGLN using a String value.
  */
  public void setE01RCHGLN(String newvalue)
  {
    fieldE01RCHGLN.setString(newvalue);
  }

  /**
  * Get value of field E01RCHGLN as a String.
  */
  public String getE01RCHGLN()
  {
    return fieldE01RCHGLN.getString();
  }

  /**
  * Set numeric field E01RCHGLN using a BigDecimal value.
  */
  public void setE01RCHGLN(BigDecimal newvalue)
  {
    fieldE01RCHGLN.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHGLN as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHGLN()
  {
    return fieldE01RCHGLN.getBigDecimal();
  }

  /**
  * Set field E01RCHACC using a String value.
  */
  public void setE01RCHACC(String newvalue)
  {
    fieldE01RCHACC.setString(newvalue);
  }

  /**
  * Get value of field E01RCHACC as a String.
  */
  public String getE01RCHACC()
  {
    return fieldE01RCHACC.getString();
  }

  /**
  * Set numeric field E01RCHACC using a BigDecimal value.
  */
  public void setE01RCHACC(BigDecimal newvalue)
  {
    fieldE01RCHACC.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHACC as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHACC()
  {
    return fieldE01RCHACC.getBigDecimal();
  }

  /**
  * Set field E01RCHSDM using a String value.
  */
  public void setE01RCHSDM(String newvalue)
  {
    fieldE01RCHSDM.setString(newvalue);
  }

  /**
  * Get value of field E01RCHSDM as a String.
  */
  public String getE01RCHSDM()
  {
    return fieldE01RCHSDM.getString();
  }

  /**
  * Set numeric field E01RCHSDM using a BigDecimal value.
  */
  public void setE01RCHSDM(BigDecimal newvalue)
  {
    fieldE01RCHSDM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHSDM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHSDM()
  {
    return fieldE01RCHSDM.getBigDecimal();
  }

  /**
  * Set field E01RCHSDD using a String value.
  */
  public void setE01RCHSDD(String newvalue)
  {
    fieldE01RCHSDD.setString(newvalue);
  }

  /**
  * Get value of field E01RCHSDD as a String.
  */
  public String getE01RCHSDD()
  {
    return fieldE01RCHSDD.getString();
  }

  /**
  * Set numeric field E01RCHSDD using a BigDecimal value.
  */
  public void setE01RCHSDD(BigDecimal newvalue)
  {
    fieldE01RCHSDD.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHSDD as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHSDD()
  {
    return fieldE01RCHSDD.getBigDecimal();
  }

  /**
  * Set field E01RCHSDY using a String value.
  */
  public void setE01RCHSDY(String newvalue)
  {
    fieldE01RCHSDY.setString(newvalue);
  }

  /**
  * Get value of field E01RCHSDY as a String.
  */
  public String getE01RCHSDY()
  {
    return fieldE01RCHSDY.getString();
  }

  /**
  * Set numeric field E01RCHSDY using a BigDecimal value.
  */
  public void setE01RCHSDY(BigDecimal newvalue)
  {
    fieldE01RCHSDY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHSDY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHSDY()
  {
    return fieldE01RCHSDY.getBigDecimal();
  }

  /**
  * Set field E01RCHFSM using a String value.
  */
  public void setE01RCHFSM(String newvalue)
  {
    fieldE01RCHFSM.setString(newvalue);
  }

  /**
  * Get value of field E01RCHFSM as a String.
  */
  public String getE01RCHFSM()
  {
    return fieldE01RCHFSM.getString();
  }

  /**
  * Set numeric field E01RCHFSM using a BigDecimal value.
  */
  public void setE01RCHFSM(BigDecimal newvalue)
  {
    fieldE01RCHFSM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHFSM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHFSM()
  {
    return fieldE01RCHFSM.getBigDecimal();
  }

  /**
  * Set field E01RCHFSD using a String value.
  */
  public void setE01RCHFSD(String newvalue)
  {
    fieldE01RCHFSD.setString(newvalue);
  }

  /**
  * Get value of field E01RCHFSD as a String.
  */
  public String getE01RCHFSD()
  {
    return fieldE01RCHFSD.getString();
  }

  /**
  * Set numeric field E01RCHFSD using a BigDecimal value.
  */
  public void setE01RCHFSD(BigDecimal newvalue)
  {
    fieldE01RCHFSD.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHFSD as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHFSD()
  {
    return fieldE01RCHFSD.getBigDecimal();
  }

  /**
  * Set field E01RCHFSY using a String value.
  */
  public void setE01RCHFSY(String newvalue)
  {
    fieldE01RCHFSY.setString(newvalue);
  }

  /**
  * Get value of field E01RCHFSY as a String.
  */
  public String getE01RCHFSY()
  {
    return fieldE01RCHFSY.getString();
  }

  /**
  * Set numeric field E01RCHFSY using a BigDecimal value.
  */
  public void setE01RCHFSY(BigDecimal newvalue)
  {
    fieldE01RCHFSY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHFSY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHFSY()
  {
    return fieldE01RCHFSY.getBigDecimal();
  }

  /**
  * Set field E01RCHFLM using a String value.
  */
  public void setE01RCHFLM(String newvalue)
  {
    fieldE01RCHFLM.setString(newvalue);
  }

  /**
  * Get value of field E01RCHFLM as a String.
  */
  public String getE01RCHFLM()
  {
    return fieldE01RCHFLM.getString();
  }

  /**
  * Set numeric field E01RCHFLM using a BigDecimal value.
  */
  public void setE01RCHFLM(BigDecimal newvalue)
  {
    fieldE01RCHFLM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHFLM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHFLM()
  {
    return fieldE01RCHFLM.getBigDecimal();
  }

  /**
  * Set field E01RCHFLD using a String value.
  */
  public void setE01RCHFLD(String newvalue)
  {
    fieldE01RCHFLD.setString(newvalue);
  }

  /**
  * Get value of field E01RCHFLD as a String.
  */
  public String getE01RCHFLD()
  {
    return fieldE01RCHFLD.getString();
  }

  /**
  * Set numeric field E01RCHFLD using a BigDecimal value.
  */
  public void setE01RCHFLD(BigDecimal newvalue)
  {
    fieldE01RCHFLD.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHFLD as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHFLD()
  {
    return fieldE01RCHFLD.getBigDecimal();
  }

  /**
  * Set field E01RCHFLY using a String value.
  */
  public void setE01RCHFLY(String newvalue)
  {
    fieldE01RCHFLY.setString(newvalue);
  }

  /**
  * Get value of field E01RCHFLY as a String.
  */
  public String getE01RCHFLY()
  {
    return fieldE01RCHFLY.getString();
  }

  /**
  * Set numeric field E01RCHFLY using a BigDecimal value.
  */
  public void setE01RCHFLY(BigDecimal newvalue)
  {
    fieldE01RCHFLY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHFLY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHFLY()
  {
    return fieldE01RCHFLY.getBigDecimal();
  }

  /**
  * Set field E01RCHTFC using a String value.
  */
  public void setE01RCHTFC(String newvalue)
  {
    fieldE01RCHTFC.setString(newvalue);
  }

  /**
  * Get value of field E01RCHTFC as a String.
  */
  public String getE01RCHTFC()
  {
    return fieldE01RCHTFC.getString();
  }

  /**
  * Set numeric field E01RCHTFC using a BigDecimal value.
  */
  public void setE01RCHTFC(BigDecimal newvalue)
  {
    fieldE01RCHTFC.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHTFC as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHTFC()
  {
    return fieldE01RCHTFC.getBigDecimal();
  }

  /**
  * Set field E01RCHSTN using a String value.
  */
  public void setE01RCHSTN(String newvalue)
  {
    fieldE01RCHSTN.setString(newvalue);
  }

  /**
  * Get value of field E01RCHSTN as a String.
  */
  public String getE01RCHSTN()
  {
    return fieldE01RCHSTN.getString();
  }

  /**
  * Set numeric field E01RCHSTN using a BigDecimal value.
  */
  public void setE01RCHSTN(BigDecimal newvalue)
  {
    fieldE01RCHSTN.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHSTN as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHSTN()
  {
    return fieldE01RCHSTN.getBigDecimal();
  }

  /**
  * Set field E01RCHBBL using a String value.
  */
  public void setE01RCHBBL(String newvalue)
  {
    fieldE01RCHBBL.setString(newvalue);
  }

  /**
  * Get value of field E01RCHBBL as a String.
  */
  public String getE01RCHBBL()
  {
    return fieldE01RCHBBL.getString();
  }

  /**
  * Set numeric field E01RCHBBL using a BigDecimal value.
  */
  public void setE01RCHBBL(BigDecimal newvalue)
  {
    fieldE01RCHBBL.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHBBL as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHBBL()
  {
    return fieldE01RCHBBL.getBigDecimal();
  }

  /**
  * Set field E01RCHBBF using a String value.
  */
  public void setE01RCHBBF(String newvalue)
  {
    fieldE01RCHBBF.setString(newvalue);
  }

  /**
  * Get value of field E01RCHBBF as a String.
  */
  public String getE01RCHBBF()
  {
    return fieldE01RCHBBF.getString();
  }

  /**
  * Set numeric field E01RCHBBF using a BigDecimal value.
  */
  public void setE01RCHBBF(BigDecimal newvalue)
  {
    fieldE01RCHBBF.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHBBF as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHBBF()
  {
    return fieldE01RCHBBF.getBigDecimal();
  }

  /**
  * Set field E01RCHTRG using a String value.
  */
  public void setE01RCHTRG(String newvalue)
  {
    fieldE01RCHTRG.setString(newvalue);
  }

  /**
  * Get value of field E01RCHTRG as a String.
  */
  public String getE01RCHTRG()
  {
    return fieldE01RCHTRG.getString();
  }

  /**
  * Set numeric field E01RCHTRG using a BigDecimal value.
  */
  public void setE01RCHTRG(BigDecimal newvalue)
  {
    fieldE01RCHTRG.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHTRG as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHTRG()
  {
    return fieldE01RCHTRG.getBigDecimal();
  }

  /**
  * Set field E01RCHTRE using a String value.
  */
  public void setE01RCHTRE(String newvalue)
  {
    fieldE01RCHTRE.setString(newvalue);
  }

  /**
  * Get value of field E01RCHTRE as a String.
  */
  public String getE01RCHTRE()
  {
    return fieldE01RCHTRE.getString();
  }

  /**
  * Set numeric field E01RCHTRE using a BigDecimal value.
  */
  public void setE01RCHTRE(BigDecimal newvalue)
  {
    fieldE01RCHTRE.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHTRE as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHTRE()
  {
    return fieldE01RCHTRE.getBigDecimal();
  }

  /**
  * Set field E01RCHUSA using a String value.
  */
  public void setE01RCHUSA(String newvalue)
  {
    fieldE01RCHUSA.setString(newvalue);
  }

  /**
  * Get value of field E01RCHUSA as a String.
  */
  public String getE01RCHUSA()
  {
    return fieldE01RCHUSA.getString();
  }

  /**
  * Set field E01RCHUSO using a String value.
  */
  public void setE01RCHUSO(String newvalue)
  {
    fieldE01RCHUSO.setString(newvalue);
  }

  /**
  * Get value of field E01RCHUSO as a String.
  */
  public String getE01RCHUSO()
  {
    return fieldE01RCHUSO.getString();
  }

  /**
  * Set field E01RCHTCA using a String value.
  */
  public void setE01RCHTCA(String newvalue)
  {
    fieldE01RCHTCA.setString(newvalue);
  }

  /**
  * Get value of field E01RCHTCA as a String.
  */
  public String getE01RCHTCA()
  {
    return fieldE01RCHTCA.getString();
  }

  /**
  * Set field E01RCHAPF using a String value.
  */
  public void setE01RCHAPF(String newvalue)
  {
    fieldE01RCHAPF.setString(newvalue);
  }

  /**
  * Get value of field E01RCHAPF as a String.
  */
  public String getE01RCHAPF()
  {
    return fieldE01RCHAPF.getString();
  }

  /**
  * Set field E01DESAPF using a String value.
  */
  public void setE01DESAPF(String newvalue)
  {
    fieldE01DESAPF.setString(newvalue);
  }

  /**
  * Get value of field E01DESAPF as a String.
  */
  public String getE01DESAPF()
  {
    return fieldE01DESAPF.getString();
  }

  /**
  * Set field E01RCHLMM using a String value.
  */
  public void setE01RCHLMM(String newvalue)
  {
    fieldE01RCHLMM.setString(newvalue);
  }

  /**
  * Get value of field E01RCHLMM as a String.
  */
  public String getE01RCHLMM()
  {
    return fieldE01RCHLMM.getString();
  }

  /**
  * Set numeric field E01RCHLMM using a BigDecimal value.
  */
  public void setE01RCHLMM(BigDecimal newvalue)
  {
    fieldE01RCHLMM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHLMM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHLMM()
  {
    return fieldE01RCHLMM.getBigDecimal();
  }

  /**
  * Set field E01RCHLMD using a String value.
  */
  public void setE01RCHLMD(String newvalue)
  {
    fieldE01RCHLMD.setString(newvalue);
  }

  /**
  * Get value of field E01RCHLMD as a String.
  */
  public String getE01RCHLMD()
  {
    return fieldE01RCHLMD.getString();
  }

  /**
  * Set numeric field E01RCHLMD using a BigDecimal value.
  */
  public void setE01RCHLMD(BigDecimal newvalue)
  {
    fieldE01RCHLMD.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHLMD as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHLMD()
  {
    return fieldE01RCHLMD.getBigDecimal();
  }

  /**
  * Set field E01RCHLMY using a String value.
  */
  public void setE01RCHLMY(String newvalue)
  {
    fieldE01RCHLMY.setString(newvalue);
  }

  /**
  * Get value of field E01RCHLMY as a String.
  */
  public String getE01RCHLMY()
  {
    return fieldE01RCHLMY.getString();
  }

  /**
  * Set numeric field E01RCHLMY using a BigDecimal value.
  */
  public void setE01RCHLMY(BigDecimal newvalue)
  {
    fieldE01RCHLMY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHLMY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHLMY()
  {
    return fieldE01RCHLMY.getBigDecimal();
  }

  /**
  * Set field E01RCHLMT using a String value.
  */
  public void setE01RCHLMT(String newvalue)
  {
    fieldE01RCHLMT.setString(newvalue);
  }

  /**
  * Get value of field E01RCHLMT as a String.
  */
  public String getE01RCHLMT()
  {
    return fieldE01RCHLMT.getString();
  }

  /**
  * Set field E01RCHLMU using a String value.
  */
  public void setE01RCHLMU(String newvalue)
  {
    fieldE01RCHLMU.setString(newvalue);
  }

  /**
  * Get value of field E01RCHLMU as a String.
  */
  public String getE01RCHLMU()
  {
    return fieldE01RCHLMU.getString();
  }

  /**
  * Set field E01RCHRNU using a String value.
  */
  public void setE01RCHRNU(String newvalue)
  {
    fieldE01RCHRNU.setString(newvalue);
  }

  /**
  * Get value of field E01RCHRNU as a String.
  */
  public String getE01RCHRNU()
  {
    return fieldE01RCHRNU.getString();
  }

  /**
  * Set field E01RCHREC using a String value.
  */
  public void setE01RCHREC(String newvalue)
  {
    fieldE01RCHREC.setString(newvalue);
  }

  /**
  * Get value of field E01RCHREC as a String.
  */
  public String getE01RCHREC()
  {
    return fieldE01RCHREC.getString();
  }

  /**
  * Set numeric field E01RCHREC using a BigDecimal value.
  */
  public void setE01RCHREC(BigDecimal newvalue)
  {
    fieldE01RCHREC.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01RCHREC as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01RCHREC()
  {
    return fieldE01RCHREC.getBigDecimal();
  }

}
