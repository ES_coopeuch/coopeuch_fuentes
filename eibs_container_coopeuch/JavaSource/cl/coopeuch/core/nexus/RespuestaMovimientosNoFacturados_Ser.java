/**
 * RespuestaMovimientosNoFacturados_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * q0834.18 v82708152146
 */

package cl.coopeuch.core.nexus;

public class RespuestaMovimientosNoFacturados_Ser extends com.ibm.ws.webservices.engine.encoding.ser.BeanSerializer {
    /**
     * Constructor
     */
    public RespuestaMovimientosNoFacturados_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        RespuestaMovimientosNoFacturados bean = (RespuestaMovimientosNoFacturados) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_26;
          propValue = new java.lang.Integer(bean.getCoderror());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_27;
          propValue = bean.getMsgerror();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_28;
          propValue = bean.getIdwsst247();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_39;
          propValue = new java.lang.Integer(bean.getOrganizacion_s());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_40;
          propValue = bean.getNum_cuenta_s();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_41;
          propValue = bean.getApaterno();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_42;
          propValue = bean.getAmaterno();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_43;
          propValue = bean.getNombre_clte();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_44;
          propValue = new java.lang.Integer(bean.getCiclo_fac());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_45;
          propValue = new java.lang.Integer(bean.getCodigo_fv());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_46;
          propValue = new java.lang.Integer(bean.getFec_ult_fac_cal());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_47;
          propValue = new java.lang.Integer(bean.getFec_prox_fac_cal());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_48;
          propValue = new java.lang.Integer(bean.getFec_ini_fac_act());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_49;
          propValue = new java.lang.Integer(bean.getFec_consulta());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_50;
          propValue = new java.lang.Integer(bean.getFec_ult_proc());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_51;
          propValue = new java.lang.Integer(bean.getFec_ult_trans());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_52;
          propValue = new java.lang.Integer(bean.getFec_prim_trans());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_53;
          propValue = bean.getInd_presencia_datos();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_54;
          propValue = bean.getInd_trans_restantes();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_55;
          propValue = new java.lang.Integer(bean.getTransacciones_c());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_21,
              true,null,context);
          propQName = QName_0_56;
          propValue = bean.getTransacciones();
          serializeChild(propQName, null, 
              propValue, 
              QName_3_86,
              true,null,context);
          propQName = QName_0_57;
          propValue = new java.lang.Double(bean.getMonto1());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_58;
          propValue = new java.lang.Double(bean.getMonto2());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_59;
          propValue = new java.lang.Double(bean.getMonto3());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_60;
          propValue = new java.lang.Double(bean.getMonto4());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_61;
          propValue = new java.lang.Double(bean.getMonto5());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_62;
          propValue = new java.lang.Double(bean.getMonto6());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_63;
          propValue = new java.lang.Double(bean.getMonto7());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_22,
              true,null,context);
          propQName = QName_0_64;
          propValue = new java.lang.Long(bean.getNumero1());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_65;
          propValue = new java.lang.Long(bean.getNumero2());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_66;
          propValue = new java.lang.Long(bean.getNumero3());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_67;
          propValue = new java.lang.Long(bean.getNumero4());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_68;
          propValue = new java.lang.Long(bean.getNumero5());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_69;
          propValue = new java.lang.Long(bean.getNumero6());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_70;
          propValue = new java.lang.Long(bean.getNumero7());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_87,
              true,null,context);
          propQName = QName_0_71;
          propValue = bean.getFlag1();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_72;
          propValue = bean.getFlag2();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_73;
          propValue = bean.getFlag3();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_74;
          propValue = bean.getFlag4();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_75;
          propValue = bean.getFlag5();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_76;
          propValue = bean.getFlag6();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_77;
          propValue = bean.getFlag7();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_78;
          propValue = bean.getString1();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_79;
          propValue = bean.getString2();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_80;
          propValue = bean.getString3();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_81;
          propValue = bean.getString4();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_82;
          propValue = bean.getString5();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_83;
          propValue = bean.getString6();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_84;
          propValue = bean.getString7();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_85;
          propValue = bean.getFiller_04();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_7,
              true,null,context);
          }
          propQName = QName_0_31;
          propValue = bean.getError();
          serializeChild(propQName, null, 
              propValue, 
              QName_3_32,
              true,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_76 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "flag6");
    private final static javax.xml.namespace.QName QName_0_75 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "flag5");
    private final static javax.xml.namespace.QName QName_0_28 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "idwsst247");
    private final static javax.xml.namespace.QName QName_0_74 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "flag4");
    private final static javax.xml.namespace.QName QName_0_73 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "flag3");
    private final static javax.xml.namespace.QName QName_0_72 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "flag2");
    private final static javax.xml.namespace.QName QName_0_71 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "flag1");
    private final static javax.xml.namespace.QName QName_0_49 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "fec_consulta");
    private final static javax.xml.namespace.QName QName_0_39 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "organizacion_s");
    private final static javax.xml.namespace.QName QName_0_52 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "fec_prim_trans");
    private final static javax.xml.namespace.QName QName_0_84 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "string7");
    private final static javax.xml.namespace.QName QName_0_83 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "string6");
    private final static javax.xml.namespace.QName QName_0_45 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "codigo_fv");
    private final static javax.xml.namespace.QName QName_0_82 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "string5");
    private final static javax.xml.namespace.QName QName_0_26 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "coderror");
    private final static javax.xml.namespace.QName QName_0_81 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "string4");
    private final static javax.xml.namespace.QName QName_0_80 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "string3");
    private final static javax.xml.namespace.QName QName_0_55 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "transacciones_c");
    private final static javax.xml.namespace.QName QName_0_79 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "string2");
    private final static javax.xml.namespace.QName QName_0_78 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "string1");
    private final static javax.xml.namespace.QName QName_0_51 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "fec_ult_trans");
    private final static javax.xml.namespace.QName QName_0_70 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero7");
    private final static javax.xml.namespace.QName QName_0_47 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "fec_prox_fac_cal");
    private final static javax.xml.namespace.QName QName_0_69 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero6");
    private final static javax.xml.namespace.QName QName_1_22 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "double");
    private final static javax.xml.namespace.QName QName_0_68 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero5");
    private final static javax.xml.namespace.QName QName_0_67 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero4");
    private final static javax.xml.namespace.QName QName_0_66 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero3");
    private final static javax.xml.namespace.QName QName_0_65 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero2");
    private final static javax.xml.namespace.QName QName_0_64 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "numero1");
    private final static javax.xml.namespace.QName QName_0_41 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "apaterno");
    private final static javax.xml.namespace.QName QName_0_42 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "amaterno");
    private final static javax.xml.namespace.QName QName_0_53 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "ind_presencia_datos");
    private final static javax.xml.namespace.QName QName_1_7 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_44 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "ciclo_fac");
    private final static javax.xml.namespace.QName QName_0_27 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "msgerror");
    private final static javax.xml.namespace.QName QName_0_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "error");
    private final static javax.xml.namespace.QName QName_0_85 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "filler_04");
    private final static javax.xml.namespace.QName QName_1_87 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "long");
    private final static javax.xml.namespace.QName QName_1_21 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_0_40 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "num_cuenta_s");
    private final static javax.xml.namespace.QName QName_3_86 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://nexus.core.coopeuch.cl",
                  "listaTransacciones");
    private final static javax.xml.namespace.QName QName_0_56 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "transacciones");
    private final static javax.xml.namespace.QName QName_0_63 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto7");
    private final static javax.xml.namespace.QName QName_0_62 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto6");
    private final static javax.xml.namespace.QName QName_0_61 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto5");
    private final static javax.xml.namespace.QName QName_0_60 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto4");
    private final static javax.xml.namespace.QName QName_3_32 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://nexus.core.coopeuch.cl",
                  "Error");
    private final static javax.xml.namespace.QName QName_0_54 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "ind_trans_restantes");
    private final static javax.xml.namespace.QName QName_0_43 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "nombre_clte");
    private final static javax.xml.namespace.QName QName_0_59 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto3");
    private final static javax.xml.namespace.QName QName_0_58 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto2");
    private final static javax.xml.namespace.QName QName_0_57 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "monto1");
    private final static javax.xml.namespace.QName QName_0_50 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "fec_ult_proc");
    private final static javax.xml.namespace.QName QName_0_48 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "fec_ini_fac_act");
    private final static javax.xml.namespace.QName QName_0_46 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "fec_ult_fac_cal");
    private final static javax.xml.namespace.QName QName_0_77 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "",
                  "flag7");
}
