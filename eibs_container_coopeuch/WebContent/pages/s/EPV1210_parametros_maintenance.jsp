<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.EPV121001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

 builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
	init(opth,field,bank,ccy,field1,field2,opcod);
	showPopupHelp();
}

 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	Consulta Parametros de Control en Plataforma de Venta
<%} else if (maintenance){ %>
	Mantenci�n Parametros de Control en Plataforma de Venta
<%} else { %>
	Nuevo  Parametros de Control en Plataforma de Venta
<%} %>

 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="parametros_maintenance.jsp, EPV1210"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1210" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  
 <% int row = 0;%>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">Banco :</div>
            </td>
            <td width="50%" > 
             <eibsinput:text name="cnvObj"  property="E01PVEBNK"  eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BANK %>" readonly="true" />
             <eibsinput:text name="cnvObj"  property="E01NMEBNK"  eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="true" />
       		 </td>    
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">Porcentaje Cupo Tarjeta calculo indice de liquidez :</div>
            </td>
            <td width="50%" > 
              <eibsinput:text  name="cnvObj" property="E01PVECIL" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="<%=readOnly %>" />
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">Factor para calculo cupo Tarjeta Cr�dito :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVECTC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>" readonly="<%=readOnly %>" />
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="40%" > 
              <div align="right">Limite M�ximo control visado Senior :</div>
            </td>
            <td width="60%" > 
     	    	<eibsinput:help name="cnvObj" property="E01PVECLS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY %>"fn_param_one="E01PVECLS" fn_param_two="document.forms[0].E01PVEBNK.value"  required="false" readonly="<%=readOnly%>" />
                <eibsinput:text name="cnvObj" property="E01PVEMLS" size="4" maxlength="3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" readonly="<%=readOnly %>" />
	        </td>
           </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">Porcentaje PMT Renegociado Directo :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVEVCC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="4" maxlength="3" readonly="<%=readOnly %>" />
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">M�ximo D�as Mantener Reserva de Articulos :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVEDLR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="4" maxlength="3" readonly="<%=readOnly %>" />
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">M�ximo D�as para Cursar una operacion :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVEDCU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER %>" size="4" maxlength="3" readonly="<%=readOnly %>" />
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">Cupo M�nimo para Tarjeta de Cr�dito :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVEAM1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>"  readonly="<%=readOnly %>" />
	        </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">PMT M�nimo Planilla :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVEAM2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>"  readonly="<%=readOnly %>" />
	        </td>
          </tr>   
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">PMT M�nimo Pago Directo :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVEAM3" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>"  readonly="<%=readOnly %>" />
	        </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">PV M�nimo Planilla :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVEAM4" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>"  readonly="<%=readOnly %>" />
	        </td>
          </tr>    
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">PV M�nimo Pago Directo :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVEAM5" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>"  readonly="<%=readOnly %>" />
	        </td>
          </tr> 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right">% M�nimo emisi�n de Cheque :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVEPR1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE %>"  readonly="<%=readOnly %>" />
	        </td>
          </tr>   
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td width="50%" > 
              <div align="right"> Veces Renta Aval Sin Convenio :</div>
            </td>
            <td width="50%" > 
                <eibsinput:text  name="cnvObj" property="E01PVEPR2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL %>" maxlength="6" readonly="<%=readOnly %>" />
	        </td>
          </tr> 
        </table>
      </td>
    </tr>
  </table>

<%if  (!readOnly) { %>
    <div align="center"> 
        <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    </div>
<% } %>

  </form>
</body>
</HTML>
