<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Bloqueo / Desbloqueo de Aporte de Convenio</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="msg" class="datapro.eibs.beans.ECO001004Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

//  Process according with user selection
 function goAction(op) {
	
   	switch (op){
	//Cancel
	case 1:  {
 		document.forms[0].SCREEN.value = "1200";
       	break;
		}
	}
	document.forms[0].submit();
 }
 
function callEntidadRelacionada(){
	var pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0010?SCREEN=203&E01COSNUM=" + document.forms[0].E02COSNUM.value;
	document.location.href = pg;
}

function callConsultaEvaluacionConv(){
	var pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0010?SCREEN=355&E01COSNUM=" + document.forms[0].E02COSNUM.value+"&E01EMPCUN="+document.forms[0].E02COSECU.value;
	document.location.href = pg;
}
 
 </script>
</head>

<body>

<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>

<h3 align="center">
Bloqueo / Desbloqueo de Aporte de Convenio
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="convenant_maintenance_blo, ECO0010">
</h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0010" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="1400">

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Empleador: 
              </td>
             <td nowrap width="20%" align="left">
	  			<input type="text" name="E04COSECU" maxlength="9" size="11" value="<%=userPO.getCusNum().trim() %>" readonly="readonly" class="TXTRIGHT">
             </td>
             <td nowrap width="10%" align="right">Identificación:  
             </td>
             <td nowrap width="20%" align="left">
	  			<input type="text" name="E04COSFID" maxlength="25" size="27" value="<%=userPO.getID().trim() %>" readonly="readonly">
             </td>
             <td nowrap width="10%" align="right"> Nombre: 
               </td>
             <td nowrap width="30%"align="left">
	  			<input type="text" name="E04EMPNA1" maxlength="60" size="64" value="<%=userPO.getCusName().trim() %>" readonly="readonly" class="TXTBASE">
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <h4>Antecedentes</h4>
    
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="trdark"> 
            <td width="10%"> 
              <div align="right"> Solicitud :</div>
            </td>
            <td width="45%">
	  			<eibsinput:text name="msg" property="E04BLONUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>"  readonly="true" />
            </td>
		<%if(!msg.getE04BLOSTS().trim().equals("")){ %>
            <td width="20%" > 
              <div align="right"> Estado  :</div>
            </td>
            <td width="25%" > 
                <input type="text" name="E04BLOSTS" size="2"  value="<%=msg.getE04BLOSTS() %>" readonly >
                <input type="text" name="E04DESSTS" size="30" value="<%=msg.getE04DESSTS() %>" readonly >
            </td>
		<%} else {%>            
            <td width="10%" > 
              <div align="right">  </div>
            </td>
            <td width="25%" > 
             </td>
		<%}%>            

          </tr>


          <tr id="trclear"> 
            <td > 
              <div align="right"> Cliente Convenio :</div>
            </td>
            <td >
	  			<eibsinput:text name="msg" property="E04BLOCCU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>"  readonly="true" />
	  			<eibsinput:text name="msg" property="E04CONNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="true" />
            </td>
		<%if(!msg.getE04BLOFIY().trim().equals("0")){ %>
            <td > 
              <div align="right">Fecha de Bloqueo : </div>
            </td>
            <td > 
    	        	<eibsinput:date name="msg" fn_year="E04BLOFIY" fn_month="E04BLOFIM" fn_day="E04BLOFID" required="true" />
            </td>
	 	<%} else {%>
            <td > 
              <div align="right"></div>
            </td>
            <td > 
            </td>
	 	<%}%>
          </tr>

          <tr id="trdark"> 
            <td > 
              <div align="right">Motivo Bloqueo :</div>
            </td>
            <td > 
                <input type="text" name="E04BLOBLK" size="5" maxlength="4" value="<%=msg.getE04BLOBLK() %>" readonly >
                <input type="text" name="E04DESBLK" size="50" maxlength="45" value="<%=msg.getE04DESBLK() %>" readonly >
     			<%if(!msg.getE04BLOSTS().equals("B")){ %>
                <a href="javascript:GetCodeDescCNOFC('E04BLOBLK','E04DESBLK','BF')">
                <img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
     			<%}%>
              </td>
            <td  > 
              <div align="right"></div>
            </td>
            <td > 
            </td>
          </tr>


        </table>
      </td>
    </tr>
  </table>
 
  <div align="center"> 
	<input id="EIBSBTN" type=submit name="Submit" value="<%if(!msg.getE04BLOSTS().equals("B")){out.print("Bloquear");} else {out.print("Desbloquear");} %>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="EIBSBTN" type=button name="Cancel" value="Cancelar" onclick="javascript:goAction(1);">
	</div>


  </form>
</body>
</HTML>
