/**
 * RespuestaEstadosDeCuentaPDF_Helper.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * q0834.18 v82708152146
 */

package cl.coopeuch.core.nexus;

public class RespuestaEstadosDeCuentaPDF_Helper {
    // Type metadata
    private static final com.ibm.ws.webservices.engine.description.TypeDesc typeDesc =
        new com.ibm.ws.webservices.engine.description.TypeDesc(RespuestaEstadosDeCuentaPDF.class);

    static {
        typeDesc.setOption("buildNum","q0834.18");
        com.ibm.ws.webservices.engine.description.FieldDesc field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("coderror");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "coderror"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("msgerror");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "msgerror"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("idwsst247");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "idwsst247"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("filler_02");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "filler_02"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("variable_pdf");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "variable_pdf"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("error");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "error"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://nexus.core.coopeuch.cl", "Error"));
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static com.ibm.ws.webservices.engine.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new RespuestaEstadosDeCuentaPDF_Ser(
            javaType, xmlType, typeDesc);
    };

    /**
     * Get Custom Deserializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new RespuestaEstadosDeCuentaPDF_Deser(
            javaType, xmlType, typeDesc);
    };

}
