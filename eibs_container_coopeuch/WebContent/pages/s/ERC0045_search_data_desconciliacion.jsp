<!-- Created by Nicol�s Valeria ------Datapro----- 19/02/2015 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC200001Message"%>


<%@page import="datapro.eibs.beans.ERC004502Message"%>
<%@page import="datapro.eibs.beans.ERC004503Message"%><html>
<head>
<title>Sistema Bancario: Conciliaci�n Bancaria</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "data" class= "datapro.eibs.beans.ERC004501Message"  scope="session" />
<jsp:useBean id= "descBanco" class= "java.lang.String"  scope="session" />
<jsp:useBean id="listDebito" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="listCredito" class="datapro.eibs.beans.JBObjList" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<style type="text/css">
.headerTable{
	border-top-width : 1px;
	border-right-width : 1px;
	border-bottom-width : 1px;
	border-left-width : 1px;
	border-color: #990000;
	border-style : solid solid solid solid;
	width:100%;
}
.tablaData{
	font-family: "Verdana, Arial, Helvetica, sans-serif";
	font-size:8pt;
	background-color: #F0F0F0;
	border-color: red;
	color: #990000;
}
</style>
<script type="text/javascript">
function goPage(){
	var ok = false;
	for(n=0; n<document.forms[0].elements.length; n++)
	  {
	     var element = document.forms[0].elements[n];
	     if(element.name == "rowDebe") 
	     {	
	        if (element.checked == true) {
        	   ok = true;
        	   break;
			}
	      }
	   }
	if (!ok) {
		alert("Error. No hay transacciones seleccionadas.");
	}else if (confirm("�Desea usted continuar con esta selecci�n?")){
		document.forms[0].submit();
	}
}

</script>
</head>

<body>
 
<H3 align="center">Desconciliaci&oacute;n Cartola - Cartola<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="search_data.jsp, ERC0045"></H3>

<hr size="4">
<br>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0045">
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="400">
    <table width="100%" cellpadding="2" cellspacing="0" border="0" class="TABLEINFO" align="center">
    	<tbody style="width: 100%">
    		<tr style="width: 100%">
    			<td nowrap>
    				<table width="100%" cellpadding="2" cellspacing="0" border="0" class="TBHEAD" align="center">
    					<tbody>
    						<tr class="TRDARK">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Banco :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01RCHRBK" size="5" maxlength="4" value="<%=data.getE01RCHRBK()%>">
	       							<input type="text" name="E01DSCRBK" readonly="readonly" size="43" maxlength="43" value="<%=descBanco%>" >
    							</td>
    							<td>
    								<div align="right">
    									Fecha Desde :
    								</div>
    							</td>
    							<td>
    								<input style="vertical-align: middle" type="text" name="E01DESDDD" id="fecha1" size="3" maxlength="2" value="<%=data.getE01DESDDD()%>" readonly>
					                <input style="vertical-align: middle" type="text" name="E01DESDDM" id="fecha2" size="3" maxlength="2" value="<%=data.getE01DESDDM()%>" readonly>
					                <input style="vertical-align: middle" type="text" name="E01DESDDY" id="fecha3" size="5" maxlength="4" value="<%=data.getE01DESDDY()%>" readonly>
    							</td>
    						</tr>
    						<tr class="TRCLEAR">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Cuenta Banco :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01BRMCTA" size="23" maxlength="20" value="<%=data.getE01BRMCTA()%>">
    							</td>
    							<td>
    								<div align="right">
    									Fecha Hasta :
    								</div>
    							</td>
    							<td>
    								<input style="vertical-align: middle" type="text" name="E01HASDDD" size="3" id="fecha4" maxlength="2" value="<%=data.getE01HASDDD()%>" readonly>
					                <input style="vertical-align: middle" type="text" name="E01HASDDM" size="3" id="fecha5" maxlength="2" value="<%=data.getE01HASDDM()%>" readonly>
					                <input style="vertical-align: middle" type="text" name="E01HASDDY" size="5" id="fecha6" maxlength="4" value="<%=data.getE01HASDDY()%>" readonly>
    							</td>
    						</tr>
    						<tr class="TRDARK">
    							<td nowrap style="width: 8%"></td>
    							<td nowrap>
    								<div align="right">
    									Cuenta IBS :
    								</div>
    							</td>
    							<td nowrap>
    								<input type="text" name="E01BRMACC" size="7" maxlength="7" value="<%=data.getE01BRMACC()%>">
    							</td>
    							<td>
    								<div align="right">
    									Opci&oacute;n :
    								</div>
    							</td>
    							<td>
    								<input type="radio" disabled="disabled" name="H01FLGWK3" id="DES_CON" <% if(data.getH01FLGWK3().equalsIgnoreCase("c")){%>checked="checked"<%}%> value="C"> Conciliaci&oacute;n
    								<input type="radio" disabled="disabled" name="H01FLGWK3" id="DES_CON" <% if(data.getH01FLGWK3().equalsIgnoreCase("d")){%>checked="checked"<%}%> value="D"> Desconciliaci&oacute;n
    								<input type="hidden"  name="H01FLGWKK"   value="<%=data.getH01FLGWK3()%>"> 
    							</td>
    						</tr>
    					</tbody>
    				</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    <br>
    <div style="display: block;width: 100%" >
    <table cellpadding="2" cellspacing="0" border="0" style="background-color: #ffffff; float: left;" width="49.5%" >
   		<tr style="width: 100%">
   			<td nowrap class="headerTable" style="width: 100%;text-align: center;"><b>D E B E</b></td>
   		</tr>
   		<tr class="headerTable">
   			<td style="width: 100%;text-align: center;">
   				<table id="headTable2" class="tbhead" cellspacing=0 cellpadding=2 width="100%" border="1">
					<tr id="trdark">
						<th nowrap align="center" width="10%">Id</th>
						<th nowrap align="center" width="15%">Fecha</th>
						<th nowrap align="center" width="10%">REF.</th>
						<th nowrap align="center" width="40%">Descripci�n</th>
						<th nowrap align="center" width="20%">Monto</th>
					</tr>
				</table>
   			</td>
   		</tr>
    </table>
    <table cellpadding="2" cellspacing="0" border="0" style="background-color: #ffffff; float: right;" width="49.5%">
   		<tr style="width: 100%">
   			<td nowrap class="headerTable" style="width: 100%;text-align: center;"><b>H A B E R</b></td>
   		</tr>
   		<tr class="headerTable">
   			<td style="width: 100%;text-align: center;">
   				<table id="headTable2" class="tbhead" cellspacing=0 cellpadding=2 width="100%" border="1">
					<tr id="trdark">
						<th nowrap align="center" width="10%">ID</th>
						<th nowrap align="center" width="15%">Fecha</th>
						<th nowrap align="center" width="10%">REF.</th>
						<th nowrap align="center" width="40%">Descripci�n</th>
						<th nowrap align="center" width="20%">Monto</th>
					</tr>
				</table>
   			</td>
   		</tr>
    </table>
    </div>
    <div style="width: 100%">
		<div class="tablaData" style="height: 300px;overflow: auto; margin-top: 0px; float: left;width: 49.5%">
			<table align="left" width="100%" border="0" cellpadding="2" cellspacing="0">
				<%
					int i = 1;
					ERC004503Message msgAnterior = new ERC004503Message();
					while(listCredito.getNextRow()){
						 ERC004503Message msg = (ERC004503Message) listCredito.getRecord();
				 %>
				<tr <%if ((i%2) == 0){%>class="TRCLEAR"<%}else{%>class="TRDARK"<%}%>>
					<td nowrap align="center" width="10%">
						<% if (!msgAnterior.getE03RCSCNU().equalsIgnoreCase(msg.getE03RCSCNU())) {%><input type="radio" id="<%=msg.getE03RCSCNU()%>" name="rowDebe" value="<%=msg.getE03RCSCNU()%>"> <% }else { %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<% }%> <%=msg.getE03RCSCNU()%>
					</td>
					<td nowrap align="center" width="15%">
						<div><%=Util.formatDate(msg.getE03RCSSDD(), msg.getE03RCSSDM(), msg.getE03RCSSDY())%></div>
					</td>
					<td nowrap align="center" width="10%">
						<div><%=Util.formatCell(msg.getE03RCSCKN())%></div>
					</td>
					<td nowrap align="left" width="40%">
						<div><%=msg.getE03RCSGLO()%></div>
					</td>
					<td nowrap align="right" width="20%">
						<div><%=Util.formatCCY(msg.getE03RCSAMC())%></div>
					</td>
				</tr>
				<%
					i++;
					msgAnterior = msg;
				 } %>
			</table>
		</div>
		<div class="tablaData" style="width: 49.5%;float: right;overflow: auto;height: 300px;">
			<table align="right" width="100%" border="0"  cellpadding="2" cellspacing="0">
				<%
					i = 1;
					while(listDebito.getNextRow()){
						 ERC004502Message msg = (ERC004502Message) listDebito.getRecord();
				 %>
				<tr <%if ((i%2) == 0){%>class="TRCLEAR"<%}else{%>class="TRDARK"<%}%>>
					<td nowrap align="center" width="10%">
						<div><%=msg.getE02RCSCNU()%></div>
					</td>
					<td nowrap align="center" width="15%">
						<div><%=Util.formatDate(msg.getE02RCSSDD(), msg.getE02RCSSDM(), msg.getE02RCSSDY())%></div>
					</td>
					<td nowrap align="center" width="10%">
						<div><%=Util.formatCell(msg.getE02RCSCKN())%></div>
					</td>
					<td nowrap align="left" width="40%">
						<div><%=msg.getE02RCSGLO()%></div>
					</td>
					<td nowrap align="right" width="20%">
						<div><%=Util.formatCCY(msg.getE02RCSAMD())%></div>
					</td>
				</tr>
				<%
					i++; 
				} %>
			</table>
			</table>
		</div>
	</div>
    <br>
    
<p align="center">
	<input id="EIBSBTN" type="button" name="Submit" value="Enviar" onclick="goPage()">
</p>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
</body>
</html>
