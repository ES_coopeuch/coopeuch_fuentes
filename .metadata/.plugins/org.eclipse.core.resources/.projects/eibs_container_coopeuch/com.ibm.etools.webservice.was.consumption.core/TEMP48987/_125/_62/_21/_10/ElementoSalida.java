/**
 * ElementoSalida.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package _125._62._21._10;

public class ElementoSalida  implements java.io.Serializable {
    private java.math.BigDecimal tasaPizarra;
    private java.math.BigDecimal tasaMaxima;
    private java.math.BigDecimal tasaSugerida;
    private java.math.BigDecimal tasaMinima;
    private java.math.BigDecimal tasaMejorPrecio;
    private java.lang.String nombreMejorPrecio;
    private int tipoResultado;
    private int plazo;
    private java.math.BigDecimal monto;

    public ElementoSalida() {
    }

    public java.math.BigDecimal getTasaPizarra() {
        return tasaPizarra;
    }

    public void setTasaPizarra(java.math.BigDecimal tasaPizarra) {
        this.tasaPizarra = tasaPizarra;
    }

    public java.math.BigDecimal getTasaMaxima() {
        return tasaMaxima;
    }

    public void setTasaMaxima(java.math.BigDecimal tasaMaxima) {
        this.tasaMaxima = tasaMaxima;
    }

    public java.math.BigDecimal getTasaSugerida() {
        return tasaSugerida;
    }

    public void setTasaSugerida(java.math.BigDecimal tasaSugerida) {
        this.tasaSugerida = tasaSugerida;
    }

    public java.math.BigDecimal getTasaMinima() {
        return tasaMinima;
    }

    public void setTasaMinima(java.math.BigDecimal tasaMinima) {
        this.tasaMinima = tasaMinima;
    }

    public java.math.BigDecimal getTasaMejorPrecio() {
        return tasaMejorPrecio;
    }

    public void setTasaMejorPrecio(java.math.BigDecimal tasaMejorPrecio) {
        this.tasaMejorPrecio = tasaMejorPrecio;
    }

    public java.lang.String getNombreMejorPrecio() {
        return nombreMejorPrecio;
    }

    public void setNombreMejorPrecio(java.lang.String nombreMejorPrecio) {
        this.nombreMejorPrecio = nombreMejorPrecio;
    }

    public int getTipoResultado() {
        return tipoResultado;
    }

    public void setTipoResultado(int tipoResultado) {
        this.tipoResultado = tipoResultado;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public java.math.BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(java.math.BigDecimal monto) {
        this.monto = monto;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        ElementoSalida other = (ElementoSalida) obj;
        boolean _equals;
        _equals = true
            && ((this.nombreMejorPrecio==null && other.getNombreMejorPrecio()==null) || 
             (this.nombreMejorPrecio!=null &&
              this.nombreMejorPrecio.equals(other.getNombreMejorPrecio())))
            && this.tipoResultado == other.getTipoResultado()
            && this.plazo == other.getPlazo();
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ElementoSalida history = (ElementoSalida) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.tasaPizarra==null && other.getTasaPizarra()==null) || 
             (this.tasaPizarra!=null &&
              this.tasaPizarra.equals(other.getTasaPizarra())))
            && ((this.tasaMaxima==null && other.getTasaMaxima()==null) || 
             (this.tasaMaxima!=null &&
              this.tasaMaxima.equals(other.getTasaMaxima())))
            && ((this.tasaSugerida==null && other.getTasaSugerida()==null) || 
             (this.tasaSugerida!=null &&
              this.tasaSugerida.equals(other.getTasaSugerida())))
            && ((this.tasaMinima==null && other.getTasaMinima()==null) || 
             (this.tasaMinima!=null &&
              this.tasaMinima.equals(other.getTasaMinima())))
            && ((this.tasaMejorPrecio==null && other.getTasaMejorPrecio()==null) || 
             (this.tasaMejorPrecio!=null &&
              this.tasaMejorPrecio.equals(other.getTasaMejorPrecio())))
            && ((this.monto==null && other.getMonto()==null) || 
             (this.monto!=null &&
              this.monto.equals(other.getMonto())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ElementoSalida history = (ElementoSalida) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getTasaPizarra() != null) {
            _hashCode += getTasaPizarra().hashCode();
        }
        if (getTasaMaxima() != null) {
            _hashCode += getTasaMaxima().hashCode();
        }
        if (getTasaSugerida() != null) {
            _hashCode += getTasaSugerida().hashCode();
        }
        if (getTasaMinima() != null) {
            _hashCode += getTasaMinima().hashCode();
        }
        if (getTasaMejorPrecio() != null) {
            _hashCode += getTasaMejorPrecio().hashCode();
        }
        if (getNombreMejorPrecio() != null) {
            _hashCode += getNombreMejorPrecio().hashCode();
        }
        _hashCode += getTipoResultado();
        _hashCode += getPlazo();
        if (getMonto() != null) {
            _hashCode += getMonto().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
