<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%// P�gina de Creaci�n de convenios creada Por Alonso Arana--- Datapro  17/10/2013 %>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page import="com.datapro.constants.Entities"%> 

<html>
<head>
<title>Solicitu de Cambio de Estado de Conven�o</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<jsp:useBean id="deal" class="datapro.eibs.beans.ESD079101Message" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
</head>

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

</SCRIPT>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%> 
<H3 align="center">Solcicitud de Cambio de Estado de Convenios<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="req_change_deal_status_new.jsp
, ESD0791"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0791" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" value="300">

  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr id="trclear"> 
            <td width="10%" height="37"> 
              <div align="right">Solicitud :</div>
            </td>
            <td width="10%" height="37"> 
	  			<eibsinput:text name="deal" property="E01COTNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>"  readonly="true"/>
            </td>
            <td width="10%" height="37"> 
              <div align="right">Convenio :</div>
            </td>
            <td width="10%" height="37"> 
                <eibsinput:text property="E01COTCDE" name="deal" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>
          </td>
            <td width="10%" height="37"> 
            </td>
            <td width="50%" height="37"> 
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
<H4>Datos Cliente Convenio</H4>
  <table  class="tableinfo">
    <tr> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="trdark"> 
            <td width="15%" > 
              <div align="right">Estado :</div>
            </td>
            <td width="15%" > 
               <select name="E01COTSTS" >
                    <option value="X" <% if (deal.getE01COTSTS().equals("X")) out.print("selected"); %>>Seleccione</option>
                    <option value=" " <% if (deal.getE01COTSTS().trim().equals("")) out.print("selected"); %>>Vigente</option>
                    <option value="A" <% if (deal.getE01COTSTS().equals("A")) out.print("selected"); %>>Bloqueado</option>                   
                    <option value="C" <% if (deal.getE01COTSTS().equals("C")) out.print("selected"); %>>Cerrado</option>                   
                  </select>
          </td>
            <td width="15%"> 
            Motivo :</td>
            <td width="55%"> 
                <input type="text" name="E01COTMTV" value="<%= deal.getE01COTMTV()%>" size="5" maxlength="4"  >
                <input type="text" name="E01COTDMT" value="<%= deal.getE01COTDMT()%>" size="50" maxlength="45"  readonly >
                
	                <a href="javascript:GetCodeDescCNOFC('E01COTMTV','E01COTDMT','MV')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
	                <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >  
            </td>
          </tr>

          <tr id="tclear"> 
            <td width="15%"> 
              <div align="right">Detalle :</div>
            </td>
            <td width="50%" align="left" colspan=3> 
            	<input type="text" name="E01COTDET"  value="<%= deal.getE01COTDET()%>" maxlength="150" size="150" value="">
             </td>
          </tr>
      
        </table>
      </td>
    </tr>
  </table>  
   
  <p align="center"> 
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>
  </form>
 </body>
</html>
