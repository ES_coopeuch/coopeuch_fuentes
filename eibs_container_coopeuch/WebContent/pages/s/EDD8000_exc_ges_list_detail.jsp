<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EDD800001Message,datapro.eibs.beans.EDD800002Message"%>
<%@ page import="com.datapro.constants.EibsFields"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html> 
<head>
<title>Gesti&oacute;n de Aprobaci&oacute;n Masiva Cargos y Abonos Cuentas Vistas</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="ExConDetalle" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "ExcSeleccion" class= "datapro.eibs.beans.EDD800001Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript"> 
  function goAction(op) 
   {
	  if (op =='1200')	
 	 {
 	 	document.forms[0].SCREEN.value = op;
		document.forms[0].action = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD8000?SCREEN=1200";
	 	document.forms[0].submit();
	 }
   }
</SCRIPT>  

</head>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>
<h3 align="center">Gesti&oacute;n de Aprobaci&oacute;n Masiva Cargos y Abonos Cuentas Vistas
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="exc_ges_list_detail.jsp, EDD8000">
</h3>

<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD8000">
<input type="hidden" name="SCREEN" value="">

<input type="hidden" name="codigo_lista" value="<%=request.getAttribute("codigo_lista") %>" id="codigo_lista">
 
   <table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">Id Archivo : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="E01CMHIDC" value="<%=ExcSeleccion.getE01IDCARGA()%>" 
	  			size="23"  readonly>
             </td>  
             
               <td nowrap width="10%" align="right">Fecha Carga : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="fechaCarga" size="23" maxlength="20" value="<% out.print(ExcSeleccion.getE01CMHCAD()+"/"+ExcSeleccion.getE01CMHCAM()+"/"+ExcSeleccion.getE01CMHCAY());%>" readonly>
             </td> 
             <td nowrap width="10%" align="left">RECHAZOS 
             </td>
             <td nowrap width="10%"align="left">ABONOS 
             </td>
             <td nowrap width="10%"align="left">CARGOS 
             </td> 
         </tr>
         
          <tr>
             <td nowrap width="10%" align="right">Registros : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01CMHCAR" size="23" maxlength="20" value="<%=ExcSeleccion.getE01CMHCAR() %>" readonly>
             </td>
        <td nowrap width="10%" align="right">Reg Error : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01CMHVAR" size="23" maxlength="20" value="<%=ExcSeleccion.getE01CMHVAR() %>" readonly>
             </td>
               <td nowrap width="10%" align="left">Cuenta Vista No Activa : 
               </td>
               <td nowrap width="50%"align="left">
 					<input type="text" name="E01TACTNA" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TACTNA() %>" readonly>
               </td>
               <td nowrap width="50%"align="left">
 					<input type="text" name="E01TCCTNA" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TCCTNA() %>" readonly>
               </td>
             </tr>
      
           <tr>
             <td nowrap width="10%" align="right">Estado : 
               </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01ESTCARG" size="23" maxlength="20" value="<%
 				if(ExcSeleccion.getE01ESTCARG().equalsIgnoreCase("P")){
						out.print("PROCESADO");
					}else if(ExcSeleccion.getE01ESTCARG().equalsIgnoreCase("R")){
						out.print("RECHAZADO");
					}else if(ExcSeleccion.getE01ESTCARG().equalsIgnoreCase("A")){
						out.print("APROBADO");
					}else if(ExcSeleccion.getE01ESTCARG().equalsIgnoreCase("G")){
						out.print("GENERADO");
					}
 				%>" readonly>
             </td>
        		<td nowrap width="10%" align="right">Fecha Estado : 
               </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="FechaEstado" size="23" maxlength="20" value="<%out.print(ExcSeleccion.getE01CMHCAD()+"/"+ExcSeleccion.getE01CMHCAM()+"/"+ExcSeleccion.getE01CMHCAY());%>" readonly>
             </td>
	             <td nowrap width="10%" align="left">Cuenta Vista No Corresp. a  RUT : 
	             </td>
	             <td nowrap width="50%"align="left">
 					<input type="text" name="E01TANRUT" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TANRUT()%>" readonly>
             	 </td>
             	 <td nowrap width="50%"align="left">
 					<input type="text" name="E01TCNRUT" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TCNRUT()%>" readonly>
             	 </td>
             </tr>
        
        	<tr>
             <td nowrap width="10%" align="right">Usuario : 
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01CMHCAU" size="23" maxlength="20" value="<%=ExcSeleccion.getE01CMHCAU()%>" readonly >
             </td>
        	 <td nowrap width="10%" align="right" colspan="2"> 
             </td>
             <td nowrap width="10%" align="left">Cuenta Vista   Sin Saldo :   
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01TASISA" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TASISA()%>" readonly >
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01TCSISA" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TCSISA()%>" readonly >
             </td>
            </tr>
            <tr>
             <td nowrap width="10%" align="right">Cantidad Abono : 
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01TABONO" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TABONO()%>" readonly >
             </td>
        	 <td nowrap width="10%" align="right">Cantidad Cargo : 
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01TCARGO" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TCARGO()%>" readonly >
             </td>
             <td nowrap width="10%" align="left">RUT No Socio : 
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01TARUTN" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TARUTN()%>" readonly >
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01TCRUTN" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TCRUTN()%>" readonly >
             </td>
            </tr>
            <tr>
             <td nowrap width="10%" align="right">Monto Abono : 
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01MABONO" size="23" maxlength="20" value="<%=ExcSeleccion.getE01MABONO()%>" readonly >
             </td>
        	 <td nowrap width="10%" align="right">Monto Cargo : 
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01MCARGO" size="23" maxlength="20" value="<%=ExcSeleccion.getE01MCARGO()%>" readonly >
             </td>
             <td nowrap width="10%" align="left">Monto Sin Valor : 
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01TAMONT" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TAMONT()%>" readonly >
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01TCMONT" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TCMONT()%>" readonly >
             </td>
            </tr>
            <tr>
             <td nowrap width="10%" align="right" colspan="2"> 
             </td>             
        	 <td nowrap width="10%" align="right" colspan="2"> 
             </td>             
             <td nowrap width="10%" align="left">Abono Duplicado : 
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01TAABOD" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TAABOD()%>" readonly >
             </td>
             <td nowrap width="50%"align="left">
 				<input type="text" name="E01TCCARD" size="23" maxlength="20" value="<%=ExcSeleccion.getE01TCCARD()%>" readonly >
             </td>
             <td nowrap width="50%"align="left"> 				
             </td>
            </tr>
        </table>
      </td>
    </tr>
  </table>
 
 

<%
	if (ExConDetalle.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>


<table class="tbenter" width=100% >
	<tr>
		<td height="20"></td>
	</tr>
	<tr>
		<td>
		<table id="headTable" width="100%" align="left">
			<tr id="trdark">
				<th align="center" nowrap width="5%">SEC</th>
				<th align="center" nowrap width="10%">NRO. PRODUCTO</th>
				<th align="center" nowrap width="10%">RUT SOCIO</th>
				<th align="center" nowrap width="10%">MONTO</th>
				<th align="center" nowrap width="10%">TIPO MVTO</th>
				<th align="center" nowrap width="10%">ESTADO MVTO</th>
				<th align="center" nowrap width="40%">MOTIVO RECHAZO</th>
				<th align="center" nowrap width="30%">DESCRIPCION REGISTRO</th>
				<th align="center"></th>
			</tr>
			<%
			ExConDetalle.initRow();
				int k = 0,inicial;
				boolean firstTime = true;
				String chk = "";
				while (ExConDetalle.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EDD800002Message PDetalle = (EDD800002Message) ExConDetalle.getRecord();
		%>
			<tr>
				<td nowrap align="center" height="28"><%=PDetalle.getE02SEQ()%></td>
				<td nowrap align="right" height="28"><%=PDetalle.getE02NUNACC()%></td>
				<td nowrap align="center" height="28"><%=PDetalle.getE02NUMRUT()%></td>
				<td nowrap align="right" height="28"><%=PDetalle.getE02MONTO()%></td>
				<td nowrap align="center" height="28">
				<%	if(PDetalle.getE02TIPMOV().equalsIgnoreCase("A")){
						out.print("ABONO");
					}else if(PDetalle.getE02TIPMOV().equalsIgnoreCase("C")){
						out.print("CARGO");
					}
				%>
				<td nowrap align="center" height="28">
				<%if(PDetalle.getE02ESTCAR().equalsIgnoreCase("P")){
						out.print("PROCESADO");
					}else if(PDetalle.getE02ESTCAR().equalsIgnoreCase("R")){
						out.print("RECHAZADO");
					}else if(PDetalle.getE02ESTCAR().equalsIgnoreCase("A")){
						out.print("APROBADO");
					}else if(PDetalle.getE02ESTCAR().equalsIgnoreCase("G")){
						out.print("GENERADO");
					}
				%>
				<td nowrap align="center" height="28"><%
					if(PDetalle.getE02MOTREC().equalsIgnoreCase("0")){
						out.print("");
					}else{
						out.print(PDetalle.getE02MOTREC()+" : " +PDetalle.getE02GLOREC());
					}
					%>
				<td nowrap align="center" height="28"><%=PDetalle.getE02DESCRI()%>
				</td>
				<td nowrap align="center" height="28">
				</td>
			</tr>
			<%}%>
		</table>
		</td>
	</tr>

	<tr>
	<td>
		<table class="tbenter" width="98%" align="center">
		<tr>
			<td width="40%" align="left">
			<%
			if (ExConDetalle.getShowPrev()) {
					int pos = ExConDetalle.getFirstRec() - 10;
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.products.JSEDD8000?SCREEN=1300&posicion="
									+pos+"&codigo_lista=" + request.getAttribute("codigo_lista")
									+"&mark=atr�s"

									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
				%>
				</td>
				<td width="20%" align="center">
		    		<input id="EIBSBTN" type=button name="Submit" value="Atr�s" onClick="javascript:goAction('1200')">
 				</td>		
				<td width="40%" align="right">
				<%
				if (ExConDetalle.getShowNext()) {
					int pos = ExConDetalle.getLastRec();
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.products.JSEDD8000?SCREEN=1300&posicion="
									+pos+"&codigo_lista=" + request.getAttribute("codigo_lista")
									+"&mark=adelante"

									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/next_records.gif\" ></A>");
				}
				%>
				</td>
			</tr>
		</table>
	</td>
	</tr>
</table>
<%
	}
%>

</form>
</body>
</html>
