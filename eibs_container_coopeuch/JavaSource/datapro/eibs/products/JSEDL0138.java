package datapro.eibs.products;

/*********************************************************************************************************************************/
/**  Creado     por          :  Patricia Cataldo L.                 DATAPRO                                                     **/
/**  Identificacion          :  PCL01                                                                                           **/
/**  Fecha                   :  29/03/2013                                                                                      **/
/**  Objetivo                :  Servicio para Reinversion de Depositos a Plazo Fijos                                            **/
/**                                                                                                                             **/
/*********************************************************************************************************************************/
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import datapro.eibs.beans.EDL013801Message;
import datapro.eibs.beans.EFT000010Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.UserPos;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSEDL0138 extends datapro.eibs.master.SuperServlet {

	// certificate of deposit 
	
	// entering options
	protected static final int R_ENTER_MAINT = 100;
	protected static final int A_ENTER_MAINT = 200;	
	protected static final int A_MAINTENANCE = 300;

    
	protected String LangPath = "S";

	/**
	 * JSEDL0130 constructor comment.
	 */
	public JSEDL0138() {
		super();
	}
	/**
	 * 
	 * This method was created by Orestes Garcia.
	 */
	public void destroy() {

		flexLog("free resources used by JSEDL0130");

	}
	/**
	 * This method was created by David Mavilla.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	
	protected void procReqEnterMaint(
			ESS0030DSMessage user,
			HttpServletRequest req,
			HttpServletResponse res,
			HttpSession ses)
			throws ServletException, IOException {

			ELEERRMessage msgError = null;
			UserPos userPO = null;

			try {

				msgError = new ELEERRMessage();
				userPO = new UserPos();
				userPO.setOption("CD");
				userPO.setPurpose("MAINTENANCE");
				userPO.setRedirect("/servlet/datapro.eibs.products.JSEDL0138?SCREEN=200");
				userPO.setProdCode("CD");
				//Others Parameters
				userPO.setHeader1("E01DEAACC");
				userPO.setHeader2("H01FLGWK2");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);

			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			try {
				flexLog("About to call Page: " + LangPath + "GENERIC_account_enter.jsp");
				callPage(LangPath + "GENERIC_account_enter.jsp", req, res);
			} catch (Exception e) {
				flexLog("Exception calling page " + e);
			}

		}
	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionEnterMaint(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDL013801Message msgCD = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try {
			msgError = new ELEERRMessage();
		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgCD = (EDL013801Message) mc.getMessageRecord("EDL013801");
			msgCD.setH01USERID(user.getH01USR());
			msgCD.setH01PROGRM("EDL0138");
			msgCD.setH01TIMSYS(getTimeStamp());
			msgCD.setH01SCRCOD("01");
			msgCD.setH01OPECOD("0002");
			try {
				msgCD.setE01OLDACC(req.getParameter("E01DEAACC"));
			} catch (Exception e) {
				msgCD.setE01DEAACC(userPO.getIdentifier());
			}
			try {
				msgCD.setH01FLGWK2(req.getParameter("H01FLGWK2"));
			} catch (Exception e) {
			}
            flexLog("mensaje enviado..." + msgCD);
			msgCD.send();
			msgCD.destroy();
			flexLog("EDL013801 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
	            flexLog("error recibido enviado..." + msgError);
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Data
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("EDL013801")) {
				try {
					msgCD = new EDL013801Message();
		            flexLog("normal recibido enviado..." + msgCD);
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgCD = (EDL013801Message) newmessage;
				

				userPO.setIdentifier(msgCD.getE01OLDACC());
				userPO.setHeader2(msgCD.getE01DEACUN());
				userPO.setHeader3(msgCD.getE01CUSNA1());
				userPO.setCurrency(msgCD.getE01DEACCY());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("userPO", userPO);
				ses.setAttribute("cdMant", msgCD);

				if (IsNotError) { // There are no errors
					try {
						flexLog("About to call Page: " + LangPath + "EDL0138_cd_maint.jsp");
						callPage(LangPath + "EDL0138_cd_maint.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						userPO = new UserPos();
						userPO.setOption("CD");
						userPO.setPurpose("MAINTENANCE");
						userPO.setRedirect("/servlet/datapro.eibs.products.JSEDL0138?SCREEN=200");
						userPO.setProdCode("CD");
						//Others Parameters
						userPO.setHeader1("E01DEAACC");
						userPO.setHeader2("H01FLGWK2");
						ses.setAttribute("error", msgError);
						ses.setAttribute("userPO", userPO);
						flexLog("About to call Page: " + LangPath + "GENERIC_account_enter.jsp");
						callPage(LangPath + "GENERIC_account_enter.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}
	/**
	 * This method was created in VisualAge.
	 */
	protected void procActionMaintenance(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		EDL013801Message msgCD = null;
		EFT000010Message msgCDNew = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		boolean IsNotError = false;

		try 
		{
			msgError = new ELEERRMessage();
		} catch (Exception ex) 
		{
			flexLog("Error: " + ex);
		}

		userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try 
		{
			flexLog("Send Initial Data");
			msgCD = (EDL013801Message) ses.getAttribute("cdMant");
			msgCD.setH01USERID(user.getH01USR());
			msgCD.setH01PROGRM("EDL0138");
			msgCD.setH01TIMSYS(getTimeStamp());
			msgCD.setH01SCRCOD("01");
			msgCD.setH01OPECOD("0005");

			// all the fields here
			java.util.Enumeration enu = msgCD.fieldEnumeration();
			MessageField field = null;
			String value = null;
			while (enu.hasMoreElements()) 
			{
				field = (MessageField) enu.nextElement();
				try 
				{
					value = req.getParameter(field.getTag()).toUpperCase().trim();
					if (value != null) 
					{
						field.setString(value);
					}
				} catch (Exception e) {
				}
			}

			//msgCD.send();
			flexLog("EDL013801 Message Sent " + msgCD);
			mc.sendMessage(msgCD);
			msgCD.destroy();
			flexLog("EDL013801 Message Sent");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {
			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;
				flexLog("Message Error recivido " + msgError);
				IsNotError = msgError.getERRNUM().equals("0");
				flexLog("IsNotError = " + IsNotError);
				showERROR(msgError);
			} else
				flexLog("Message " + newmessage.getFormatName() + " received.");

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
		// Receive Data
		try {
		newmessage = mc.receiveMessage();
		if (newmessage.getFormatName().equals("EFT000010")) {
			try {
				msgCDNew = new EFT000010Message();
				flexLog("EFT000010 Message Received");
			} catch (Exception ex) {
				flexLog("Error: " + ex);
			}

			msgCDNew = (EFT000010Message) newmessage;
			flexLog("Message EFT000 recivido " + msgCDNew);
			// showESD008004(msgCD);

			userPO.setIdentifier(msgCDNew.getE10DEAACC());

			flexLog("Putting java beans into the session");
			ses.setAttribute("error", msgError);
			ses.setAttribute("cdFinish", msgCDNew);
			ses.setAttribute("userPO", userPO);

			try {
				flexLog("About to call Page1: " + LangPath + "EDL0138_cd_confirm.jsp");
				callPage(LangPath + "EDL0138_cd_confirm.jsp", req, res);
			} catch (Exception e) {
				flexLog("Exception calling page " + e);
			}
		} else {
			if (newmessage.getFormatName().equals("EDL013801")) {
				try {
					msgCD = new EDL013801Message();
					flexLog("EDL013001 Message Received");
				} catch (Exception ex) {
					flexLog("Error: " + ex);
				}

				msgCD = (EDL013801Message) newmessage;
				flexLog("Message EDL0138 recivido " + msgCD);
				userPO.setIdentifier(msgCD.getE01OLDACC());

				flexLog("Putting java beans into the session");
				ses.setAttribute("error", msgError);
				ses.setAttribute("cdMant", msgCD);
				ses.setAttribute("userPO", userPO);

				if (IsNotError) { // There are no errors
					try {
						procReqEnterMaint(user, req, res, ses);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				} else { // There are errors
					try {
						flexLog("About to call Page2: " + LangPath + "EDL0138_cd_maint.jsp");
						callPage(LangPath + "EDL0138_cd_maint.jsp", req, res);
					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
				}
				} else
					flexLog("Message " + newmessage.getFormatName() + " received.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}
	
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		Socket s = null;
		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

	
		int screen = R_ENTER_MAINT;

			try {

				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					s = new Socket(super.hostIP, getInitSocket(req) + 1);
					s.setSoTimeout(super.sckTimeOut);
					mc =
						new MessageContext(
							new DataInputStream(new BufferedInputStream(s.getInputStream())),
							new DataOutputStream(new BufferedOutputStream(s.getOutputStream())),
							"datapro.eibs.beans");
				

				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}

				switch (screen) {
					case R_ENTER_MAINT :
						procReqEnterMaint(msgUser, req, res, session);
						break;
					case A_ENTER_MAINT :
						procActionEnterMaint(mc, msgUser, req, res, session);
						break;
					case A_MAINTENANCE :
						procActionMaintenance(mc, msgUser, req, res, session);
						break;			
					default :
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
				}

				} catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
					//return;
				} finally {
					s.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}

		}

	}
	
	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10());

		}
	}
}