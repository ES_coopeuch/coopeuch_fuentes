<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV101003Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ventas" class="datapro.eibs.beans.EPV107001Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Reporte de Ventas<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_inquiry.jsp,JSEPV1070"></h3>
<hr size="4">
<form method="POST" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1070">
  <input type="hidden" name="SCREEN" value=" "> 

 <% int row = 0;%>
 <% String name="";%>  
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="ventas" property="E01PVVCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Nombre :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="ventas" property="E01PVVNME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="ventas" property="E01PVVNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="30%">
    	        <eibsinput:date name="ventas" fn_year="E01PVVODY" fn_month="E01PVVODM" fn_day="E01PVVODD"  readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Sucursal :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="ventas" property="E01PVVBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
             </td>
             <td nowrap align="right" width="20%"> Ejecutivo :</td>
             <td nowrap align="left" width="30%">
	  			<eibsinput:text name="ventas" property="E01PVVOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>" readonly="true"/>
             </td>
         </tr>         
        </table>
      </td>
    </tr>
  </table>


  <h4>Informacion de la Operacion </h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:text name="ventas" property="E01PVVPRO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" readonly="true"/>
            	<eibsinput:text name="ventas" property="E01DSCPRO" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Codigo de descuento :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="ventas" property="E01PVVDSD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR%>" size = "4" readonly="true"/> 
            </td>
          </tr>                 
		  <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Ejecutivo :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>
            	<eibsinput:text name="ventas" property="E01NMEOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Sucursal de Venta :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="ventas" property="E01PVVBRS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH%>" readonly="true"/> 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Convenio :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVCNV" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>
            	<eibsinput:text name="ventas" property="E01DSCCNV" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Promocion :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="ventas" property="E01PVVPRD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR%>" size = "4" readonly="true"/> 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Vendedor :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVVCD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>
            	<eibsinput:text name="ventas" property="E01NMEVCD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Identificacion :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="ventas" property="E01PVVVID" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>" readonly="true"/> 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Supervisor :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVSCD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CNOFC%>" readonly="true"/>
            	<eibsinput:text name="ventas" property="E01NMESCD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Identificacion :</div>
            </td>
            <td nowrap width="30%">
            	<eibsinput:text name="ventas" property="E01PVVSID" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>" readonly="true"/> 
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Monto Aprobado :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVAPA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Monto Liquido :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVNET" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Reliquidaciones :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVPLR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Seguros Financiados :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVSOM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Primas No consumidas :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVPMM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Seguros No Financiados :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVSAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Tienda Virtual :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVTVI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tarjeta Alianza :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVTRA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cheque a Terceros :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVCQT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Gasto Notaria :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVGNT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Impuestos :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVIMP" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Otros Cargos :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVOTH" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Moneda :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVCCY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CURRENCY%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Tasa de Interes :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01LNSRTE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>"  readonly="true"/>
            </td>
          </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Fecha 1er Vencimiento :</div>
            </td>
            <td nowrap width="30%">
    	        <eibsinput:date name="ventas" fn_year="E01PVVFPY" fn_month="E01PVVFPM" fn_day="E01PVVFPD" readonly="true" />
            </td>
            <td nowrap width="20%"> 
              <div align="right">Periodo Base :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVBAS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="3" readonly="true"/>
            </td>
           </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Fecha de Vencimiento :</div>
            </td>
            <td nowrap width="30%">
    	        <eibsinput:date name="ventas" fn_year="E01PVVMAY" fn_month="E01PVVMAM" fn_day="E01PVVMAD" readonly="true" />
            </td>
            <td nowrap width="20%"> 
              <div align="right">Costo de Fondos :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVFCA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
            </td>
         </tr>  
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Monto de la Cuota :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVPAM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Numero de pagos :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="ventas" property="E01PVVPNU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="5" readonly="true"/>
            </td>
          </tr>  
 
        </table>
      </td>
    </tr>
  </table>
 
</form>

</body>
</html>
