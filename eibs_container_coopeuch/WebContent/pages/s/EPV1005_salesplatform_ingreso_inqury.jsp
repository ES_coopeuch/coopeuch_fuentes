<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page
	import="datapro.eibs.master.Util,datapro.eibs.beans.EPV121601Message"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="platfObj" class="datapro.eibs.beans.EPV100502Message"
	scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"
	scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"
	scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage"
	scope="session" />
<jsp:useBean id="EPV100503List" class="datapro.eibs.beans.JBObjList"
	scope="session" />
<jsp:useBean id="EPV121601List" class="datapro.eibs.beans.JBObjList" scope="session" />	
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

 function setRecalculate() {
	  document.forms[0].RECALC.checked = false;
	  UpdateFlag(false);  
 }
 
 function UpdateFlag(val) {
  document.forms[0].H02FLGWK2.value = (val==true)?"X":"";
 }


 function mostrarDeuda() {
  	if (document.getElementById('end').style.display ==''){
		document.getElementById('end').style.display ='none'; 
		document.getElementById('e1').innerText ='+'; 	
  	}else{  	
		document.getElementById('end').style.display ='';
		document.getElementById('e1').innerText ='-';		  	
	}
 }

 function consultarSinacofi() {	
 	//validamos que haya colocado la serie rut.
 	if (document.forms[0].E02PVMIDS.value==''){
 		alert("Para efectuar esta consulta debe colocar la serie Rut.!");
	 	document.forms[0].E02PVMIDS.focus();
	 	return;
 	} 	
 	if (document.forms[0].E02PVMCPR.value==''){
 		alert("Para efectuar esta consulta debe posser rut Empleador.!");
	 	document.forms[0].E02PVMCPR.focus();
	 	return;
 	}  	 	
	document.forms[0].SCREEN.value = '850';	
	document.forms[0].submit();
 }
 
  function consultarPautaEvaluacion() {
	var pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0010?SCREEN=350&E01COSNUM=" + document.forms[0].E02COSNUM.value+"&E01EMPCUN="+document.forms[0].E02PVMCUE.value;
	CenterWindow(pg,750,600,2);	
 }
 
  function consultarRentasHistoricas() {  
	var pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1005?SCREEN=700&E05PVMCUN=" + document.forms[0].E02PVMCUN.value;
	CenterWindow(pg,750,600,2);		
 }
 
 function consultarMedioEvaluacion() { 
	 if (document.forms[0].E02PVMTPR.value!=""){
		var pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1216?SCREEN=203&E01TPRTPR=" + document.forms[0].E02PVMTPR.value;
		CenterWindow(pg,750,600,2);		
	 }
 } 
 
//  Process according with user selection
 function goAction(op) {

 	if (op=='600' && !document.forms[0].RECALC.checked){//no ha presionado calc
 		alert("Debe Calcular para continuar..!!!");
	 	document.forms[0].calcular.focus();
	 	return;	
 	} 	
 	if (op=='601'){
 		//esta calculando guardamos el estado oculto/mostrar deuda.
 		document.forms[0].ESTADO.value = document.getElementById('end').style.display
 		
 	}
	document.forms[0].SCREEN.value = op;	
	document.forms[0].submit();
 }
 </script>
</head>

<%
	boolean readOnly = false;
	boolean maintenance = false;
	int row = 0;
%>

<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null) {
		if (request.getParameter("readOnly").toLowerCase().equals(
				"true")) {
			readOnly = true;
		} else {
			readOnly = false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">CONSULTA SOLICITUD DE CREDITO <img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left"
	name="EIBS_GIF" ALT="salesplatform_ingreso_inqury.jsp,JSEPV1005"></h3>
<hr size="4">
<form method="post"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1005">
<INPUT TYPE=HIDDEN NAME="SCREEN" VALUE=""> <INPUT TYPE=HIDDEN
	NAME="ESTADO" VALUE=""> <input type=HIDDEN
	name="customer_number" value="<%=userPO.getCusNum()%>"> <input
	type=HIDDEN name="E02PVMBNK" value="<%=currUser.getE01UBK().trim()%>">
<input type=HIDDEN name="H02FLGWK1" value="<%=platfObj.getH02FLGWK1()%>">
<!--M:Mantenimiento Blanco:Nuevo--> <INPUT TYPE=HIDDEN NAME="H02FLGWK2"
	VALUE="<%=platfObj.getH02FLGWK2()%>"> <INPUT TYPE=HIDDEN
	NAME="E02COSNUM" VALUE="<%=platfObj.getE02COSNUM()%>"> <INPUT
	TYPE=HIDDEN NAME="E02PVMCUE" VALUE="<%=platfObj.getE02PVMCUE()%>">
<INPUT TYPE=HIDDEN NAME="E02PVPPEM" VALUE="<%=platfObj.getE02PVPPEM()%>">
<INPUT TYPE=HIDDEN NAME="E02PVPPEY" VALUE="<%=platfObj.getE02PVPPEY()%>">


<table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			class="tbhead">
			<tr>
				<td nowrap width="20%" align="right">Cliente:</td>
				<td nowrap align="left" width="12%"><eibsinput:text
					name="platfObj" property="E02PVMCUN"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>"
					readonly="true" size="13" /></td>
				<td nowrap align="right" width="14%">Nombre:</td>
				<td nowrap align="left" colspan="3"><eibsinput:text
					name="platfObj" property="E02PVMSHN"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"
					readonly="true" /></td>
			</tr>
			<tr>
				<td nowrap align="right">Solicitud:</td>
				<td nowrap align="left" width="163"><eibsinput:text
					name="platfObj" property="E02PVMNUM"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>"
					readonly="true" size="13" /></td>
				<td nowrap align="right" width="177">Fecha Solicitud:</td>
				<td nowrap align="left" width="250"><eibsinput:date
					name="platfObj" fn_year="E02PVMOPY" fn_month="E02PVMOPM"
					fn_day="E02PVMOPD" readonly="true" /></td>
				<td width="420">
				<table class="tbhead">
					<tr bordercolor="#FFFFFF">
						<td nowrap align="right" width="147">Sucursal :</td>
						<td nowrap align="left" width="86"><eibsinput:text
							name="platfObj" property="E02PVMBRN"
							eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>"
							readonly="true" /></td>
					</tr>
					<tr bordercolor="#FFFFFF">
						<td nowrap align="right" width="147">Ejecutivo :</td>
						<td nowrap align="left" width="86"><eibsinput:text
							name="platfObj" property="E02PVMOFC"
							eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>"
							readonly="true" /></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<h4>Datos B�sicos Cliente:</h4>

<table class="tableinfo">
	<tr bordercolor="#FFFFFF">
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">

			<tr id="trdark">
				<td width="15%">
				<div align="right">Rut:</div>
				</td>
				<td width="35%"><eibsinput:text property="E02PVMIDN"
					name="platfObj"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>"
					readonly="true" /></td>
				<td width="15%">
				<div align="right">Socio desde:</div>
				</td>
				<td width="35%"><eibsinput:date name="platfObj"
					fn_year="E02PVMSCY" fn_month="E02PVMSCM" fn_day="E02PVMSCD"
					readonly="true" /></td>
			</tr>
			<tr id="trclear">
				<td width="15%">
				<div align="right">Serie Rut:</div>
				</td>
				<td width="35%"><eibsinput:text property="E02PVMIDS"
					name="platfObj"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>"
					size="12" maxlength="10" readonly="true" /></td>
				<td width="15%">&nbsp;</td>
				<td width="35%">&nbsp;</td>
			</tr>

			<tr id="trdark">
				<td width="15%">
				<div align="right">Primer Nombre:</div>
				</td>
				<td width="35%"><eibsinput:text name="platfObj"
					property="E02PVMFNA"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="true" />
				</td>
				<td width="15%">
				<div align="right">Segundo Nombre:</div>
				</td>
				<td width="35%"><eibsinput:text property="E02PVMFN2"
					name="platfObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"
					readonly="true" /></td>
			</tr>
			<tr id="trclear">
				<td width="15%">
				<div align="right">Primer Apellido:</div>
				</td>
				<td width="35%"><eibsinput:text property="E02PVMLN1"
					name="platfObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"
					readonly="true" /></td>
				<td width="15%">
				<div align="right">Segundo Apellido:</div>
				</td>
				<td width="35%"><eibsinput:text property="E02PVMLN2"
					name="platfObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"
					readonly="true" /></td>
			</tr>

			<tr id="trdark">
				<td width="15%">
				<div align="right">Nombre Corto:</div>
				</td>
				<td width="35%"><eibsinput:text name="platfObj"
					property="E02PVMSHN"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME %>" readonly="true" />
				</td>
				<td width="15%">
				<div align="right">Fecha Nacimiento:</div>
				</td>
				<td width="35%"><eibsinput:date name="platfObj"
					fn_year="E02PVMBDY" fn_month="E02PVMBDM" fn_day="E02PVMBDD"
					readonly="true" /></td>
			</tr>
			<tr id="trclear">
				<td width="15%">
				<div align="right">N�mero de Cargas:</div>
				</td>
				<td width="35%"><eibsinput:text property="E02PVMNSO"
					name="platfObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>"
					size="3" maxlength="2" readonly="true" /></td>
				<td width="15%">
				<div align="right">Sexo:</div>
				</td>
				<td width="35%"><input type="hidden" name="E02PVMSEX"
					value="<%=platfObj.getE02PVMSEX()%>"> <%
 	if (platfObj.getE02PVMSEX().equals("F"))
 		out.print("Femenino");
 %>
				<%
					if (platfObj.getE02PVMSEX().equals("M"))
						out.print("Masculino");
				%>
				</td>
			</tr>

			<tr id="trdark">
				<td width="15%">
				<div align="right">Nacionalidad:</div>
				</td>
				<td width="35%"><eibsinput:text name="platfObj"
					property="E02PVMCCS"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />
				<eibsinput:text name="platfObj" property="E02DSCCCS"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>"
					readonly="true" /></td>
				<td width="15%"></td>
				<td width="35%"></td>
			</tr>
			<tr id="trclear">
				<td width="15%">
				<div align="right">Nivel de estudio:</div>
				</td>
				<td width="35%"><eibsinput:text name="platfObj"
					property="E02PVMNED"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />
				<eibsinput:text name="platfObj" property="E02DSCNED"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>"
					readonly="true" /></td>
				<td width="15%">
				<div align="right">Profesi�n:</div>
				</td>
				<td width="35%" height="19"><eibsinput:text name="platfObj"
					property="E02PVMPRF"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />
				<eibsinput:text name="platfObj" property="E02DSCPRF"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>"
					readonly="true" /></td>
			</tr>

			<tr id="trdark">
				<td width="15%">
				<div align="right">Estado Civil:</div>
				</td>
				<td width="35%">
				<%
					String edoCivil = "";
					if ("1".equals(platfObj.getE02PVMMST())) {
						edoCivil = "Soltero(a)";
					} else if ("2".equals(platfObj.getE02PVMMST())) {
						edoCivil = "Casado(a) - Separaci�n de Bienes";
					} else if ("3".equals(platfObj.getE02PVMMST())) {
						edoCivil = "Casado(a) - Sociedad Conyugal";
					} else if ("4".equals(platfObj.getE02PVMMST())) {
						edoCivil = "Casado(a)";
					} else if ("5".equals(platfObj.getE02PVMMST())) {
						edoCivil = "Viudo(a)";
					} else if ("6".equals(platfObj.getE02PVMMST())) {
						edoCivil = "Separado(a)";
					} else if ("7".equals(platfObj.getE02PVMMST())) {
						edoCivil = "Otro";
					}
				%> <eibsinput:text name="platfObj" property="E02PVMMST"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />
				<b><%=edoCivil%></b></td>
				<td width="15%">
				<div align="right">Sector Econ�mico:</div>
				</td>
				<td width="35%"><eibsinput:text name="platfObj"
					property="E02PVMSEC"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />
				<eibsinput:text name="platfObj" property="E02DSCSEC"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>"
					readonly="true" /></td>
			</tr>
			<tr id="trclear">
				<td width="15%">
				<div align="right">Origen de Ingreso:</div>
				</td>
				<td width="35%"><eibsinput:text name="platfObj"
					property="E02PVMORI"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />
				<eibsinput:text name="platfObj" property="E02DSCORI"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>"
					readonly="true" /></td>
				<td width="15%">
				<div align="right">Actividad Econ�mica:</div>
				</td>
				<td width="35%"><eibsinput:text name="platfObj"
					property="E02PVMAEC"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CODE %>" readonly="true" />
				<eibsinput:text name="platfObj" property="E02DSCAEC"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR_40 %>"
					readonly="true" /></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<h4>Direcci�n Principal</h4>

<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="left">

			<tr id="trdark">
				<td nowrap width="39%">
				<div align="right">Tipo Direccion :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMADT"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMADT()%>"
					readonly="true"> <input type="text" name="E02DSCADT"
					size="50" maxlength="45" value="<%=platfObj.getE02DSCADT()%>"
					readonly="true"></td>
			</tr>

			<tr id="trclear">
				<td nowrap width="39%">
				<div align="right">Comuna :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMCOM"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMCOM()%>"
					readonly="true"> <input type="text" name="E02DSCCOM"
					size="50" maxlength="45" value="<%=platfObj.getE02DSCCOM()%>"
					readonly="true"></td>
			</tr>

			<tr id="trdark">
				<td nowrap width="39%">
				<div align="right">Calle :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMNA2"
					size="50" maxlength="45" value="<%=platfObj.getE02PVMNA2()%>"
					readonly="true"></td>
			</tr>

			<tr id="trclear">
				<td nowrap width="39%">
				<div align="right"></div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMNA3"
					size="50" maxlength="45" value="<%=platfObj.getE02PVMNA3()%>"
					readonly="true"></td>
			</tr>

			<tr id="trdark">
				<td nowrap width="39%">
				<div align="right">N&uacute;mero :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMNST"
					size="10" maxlength="10" value="<%=platfObj.getE02PVMNST()%>"
					readonly="true"></td>
			</tr>

			<tr id="trclear">
				<td nowrap width="39%">
				<div align="right">Casa/Depto :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMNA4"
					size="50" maxlength="45" value="<%=platfObj.getE02PVMNA4()%>"
					readonly="true"></td>
			</tr>

			<tr id="trdark">
				<td nowrap width="39%">
				<div align="right">Referencia :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMCTR"
					size="40" maxlength="35" value="<%=platfObj.getE02PVMCTR()%>"
					readonly="true"></td>
			</tr>

			<tr id="trclear">
				<td nowrap width="39%">
				<div align="right">C&oacute;digo Postal :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMZPC"
					size="16" maxlength="15" value="<%=platfObj.getE02PVMZPC()%>"
					readonly="true"></td>
			</tr>

			<tr id="trdark">
				<td nowrap width="39%">
				<div align="right">Fecha Inicio :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMDTD"
					size="3" maxlength="2" value="<%=platfObj.getE02PVMDTD()%>"
					readonly="true"> <input type="text" name="E02PVMDTM"
					size="3" maxlength="2" value="<%=platfObj.getE02PVMDTM()%>"
					readonly="true"> <input type="text" name="E02PVMDTY"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMDTY()%>"
					readonly="true"></td>
			</tr>

			<tr id="trclear">
				<td nowrap width="39%">
				<div align="right">Verificado por :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMADF"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMADF()%>"
					readonly="true"> <input type="text" name="E02DSCADF"
					size="50" maxlength="45" value="<%=platfObj.getE02DSCADF()%>"
					readonly="true"></td>
			</tr>

			<tr id="trdark">
				<td nowrap width="39%">
				<div align="right">Direcci&oacute;n E-Mail :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMIAD"
					size="65" maxlength="60" value="<%=platfObj.getE02PVMIAD()%>"
					readonly="true"></td>
			</tr>

			<tr id="trdark">
				<td nowrap width="39%">
				<div align="right">Direcci&oacute;n Pagina WEB :</div>
				</td>
				<td nowrap width="61%"><input type="text" name="E02PVMWEB"
					size="65" maxlength="60" value="<%=platfObj.getE02PVMWEB()%>"
					readonly="true"></td>
			</tr>

		</table>
		</td>
	</tr>
</table>

<h4>Tel&eacute;fonos</h4>

<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id="trdark">
				<td nowrap width="27%">
				<div align="right">Tel&eacute;fono Casa :</div>
				</td>
				<td nowrap width="21%"><input type="text" name="E02PVMHPN"
					size="16" maxlength="15"
					value="<%=platfObj.getE02PVMHPN().trim()%>" readonly="true">
				</td>
				<td nowrap width="29%">
				<div align="right">Tel&eacute;fono Oficina :</div>
				</td>
				<td nowrap width="23%"><input type="text" name="E02PVMPHN"
					size="16" maxlength="15"
					value="<%=platfObj.getE02PVMPHN().trim()%>" readonly="true">
				</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="27%">
				<div align="right">Tel&eacute;fono Fax :</div>
				</td>
				<td nowrap width="21%"><input type="text" name="E02PVMFAX"
					size="16" maxlength="15"
					value="<%=platfObj.getE02PVMFAX().trim()%>" readonly="true">
				</td>
				<td nowrap width="29%">
				<div align="right">Tel&eacute;fono Celular :</div>
				</td>
				<td nowrap width="23%"><input type="text" name="E02PVMPH1"
					size="16" maxlength="15"
					value="<%=platfObj.getE02PVMPH1().trim()%>" readonly="true">
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<h4>Datos Operativos</h4>

<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id="trdark">
				<td nowrap width="50%">
				<div align="right">Fecha Firma Contrato uso Canales
				Electr�nicos :</div>
				</td>
				<td nowrap width="50%"><eibsinput:date name="platfObj"
					fn_year="E02PVMFCY" fn_month="E02PVMFCM" fn_day="E02PVMFCD"
					readonly="true" /></td>
			</tr>
			<tr id="trclear">
				<td nowrap>
				<div align="right">Fecha Firma de mandato o carta poder :</div>
				</td>
				<td nowrap><eibsinput:date name="platfObj" fn_year="E02PVMFMY"
					fn_month="E02PVMFMM" fn_day="E02PVMFMD" readonly="true" /></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<h4>Antecedentes Laborales</h4>
<table class="tableinfo">
	<tr>
		<td nowrap><b>Empleo Actual</b>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id="trclear">
				<td nowrap width="27%">
				<div align="right">Nombre del Empleador :</div>
				</td>
				<td nowrap width="21%"><input type="text" name="E02PVMCP1"
					size="36" maxlength="35"
					value="<%=platfObj.getE02PVMCP1().trim()%>" readonly="true">
				</td>
				<td nowrap width="29%">
				<div align="right">Rut Empleador :</div>
				</td>
				<td nowrap width="23%"><input type="text" name="E02PVMCPR"
					size="16" maxlength="15"
					value="<%=platfObj.getE02PVMCPR().trim()%>" readonly="true">
				</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="27%">
				<div align="right">Cargo :</div>
				</td>
				<td nowrap width="21%"><input type="text" name="E02PVMUC3"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMUC3().trim()%>"
					readonly="true"> <input type="text" name="E02DSCUC3"
					size="26" maxlength="20"
					value="<%=platfObj.getE02DSCUC3().trim()%>" readonly="true">
				</td>
				<td nowrap width="29%">
				<div align="right">Tipo de Empresa :</div>
				</td>
				<td nowrap width="23%"><input type="text" name="E02PVMEPT"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMEPT().trim()%>"
					readonly="true"> <input type="text" name="E02DSCEPT"
					size="26" maxlength="20"
					value="<%=platfObj.getE02DSCEPT().trim()%>" readonly="true">
				</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="27%">
				<div align="right">Tipo Contrato :</div>
				</td>
				<td nowrap width="21%"><input type="text" name="E02PVMNEM"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMNEM().trim()%>"
					readonly="true"> <input type="text" name="E02DSCNEM"
					size="26" maxlength="20"
					value="<%=platfObj.getE02DSCNEM().trim()%>" readonly="true">
				</td>
				<td nowrap width="29%">
				<div align="right">Tipo de Renta :</div>
				</td>
				<td nowrap width="23%"><input type="text" name="E02PVMTYR"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMTYR().trim()%>"
					readonly="true"> <input type="text" name="E02DSCTYR"
					size="26" maxlength="20-"
					value="<%=platfObj.getE02DSCTYR().trim()%>" readonly="true">

				</td>

			</tr>
			<tr id="trdark">
				<td nowrap width="27%">
				<div align="right">Fecha Ingreso :</div>
				</td>
				<td nowrap width="21%"><eibsinput:date name="platfObj"
					fn_year="E02PVMSWY" fn_month="E02PVMSWM" fn_day="E02PVMSWD"
					readonly="true" /></td>
				<td nowrap width="29%">
				<div align="right">Tipo Salario :</div>
				</td>
				<td nowrap width="23%"><input type="text" name="E02PVMSAT"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMSAT().trim()%>"
					readonly="true"> <input type="text" name="E02DSCSAT"
					size="26" maxlength="25"
					value="<%=platfObj.getE02DSCSAT().trim()%>" readonly="true">
				</td>

			</tr>
			<tr id="trclear">
				<td nowrap width="27%">
				<div align="right">Fecha Vencimiento Contrata:</div>
				</td>
				<td nowrap width="21%"><eibsinput:date name="platfObj"
					fn_year="E02PVMCNY" fn_month="E02PVMCNM" fn_day="E02PVMCND"
					readonly="true" /></td>
				<td nowrap width="29%">
				<div align="right">D�a de pago :</div>
				</td>
				<td nowrap width="23%"><input type="text" name="E02PVMDPG"
					size="11" maxlength="10"
					value="<%=platfObj.getE02PVMDPG().trim()%>" readonly="true">
				</td>
			</tr>
			<tr id="trdark"> 
            <td nowrap width="27%" > <div align="right">Continuidad Laboral:</div></td>
            <td nowrap width="21%" >
    	        <select name="E02PVMFLC" disabled="disabled">
					<option value='Y' <%="Y".equals(platfObj.getE02PVMFLC())?"selected":""%> >SI</option>					
					<option value='N' <%="N".equals(platfObj.getE02PVMFLC())?"selected":""%> >NO</option>					
				</select> 
	        </td>
            <td nowrap width="29%" > 
              <div align="right">Lagunas:</div>
            </td>
            <td nowrap width="23%" > 
                <select name="E02PVMFLL" disabled="disabled">
					<option value='Y' <%="Y".equals(platfObj.getE02PVMFLL())?"selected":""%> >SI</option>					
					<option value='N' <%="N".equals(platfObj.getE02PVMFLL())?"selected":""%> >NO</option>					
				</select>                               
            </td>
          </tr> 			
		</table>
		<b>Empleo Anterior</b>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id="trdark">
				<td nowrap width="27%">
				<div align="right">Fecha Ingreso :</div>
				</td>
				<td nowrap width="21%"><eibsinput:date name="platfObj"
					fn_year="E02PVMIEY" fn_month="E02PVMIEM" fn_day="E02PVMIED"
					readonly="true" /></td>
				<td nowrap width="29%">
				<div align="right">Fecha T�rmino :</div>
				</td>
				<td nowrap width="23%"><eibsinput:date name="platfObj"
					fn_year="E02PVMTEY" fn_month="E02PVMTEM" fn_day="E02PVMTED"
					readonly="true" /></td>
			</tr>
		</table>
		</td>
	</tr>
</table>


<h4>Datos de la Venta</h4>

<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id="trclear">
				<td nowrap width="27%">
				<div align="right"></div>
				</td>
				<td nowrap width="21%">
				<div align="right">Vendedor :</div>
				</td>
				<td nowrap width="29%"><eibsinput:cnofc name="platfObj"
					property="E02PVMVCD" required="false" flag="CA" fn_code="E02PVMVCD"
					fn_description="E02DSCVCD" readonly="true" /> <eibsinput:text
					property="E02DSCVCD" name="platfObj"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="true" />
				</td>
				<td nowrap>&nbsp;</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="27%">
				<div align="right"></div>
				</td>
				<td nowrap width="21%">
				<div align="right">Supervisor Vendedor :</div>
				</td>
				<td nowrap width="29%"><eibsinput:cnofc name="platfObj"
					property="E02PVMSUP" required="false" readonly="true" flag="CA"
					fn_code="E02PVMSUP" fn_description="E02DSCSUP" /><eibsinput:text
					property="E02DSCSUP" name="platfObj"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="false"
					readonly="true" /></td>
				<td nowrap>&nbsp;</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="27%"></td>
				<td nowrap width="21%">
				<div align="right">Canal Venta :</div>
				</td>
				<td nowrap width="29%"><eibsinput:cnofc name="platfObj"
					property="E02PVMSLC" required="false" flag="62" fn_code="E02PVMSLC"
					fn_description="E02DSCSLC" readonly="true" /><eibsinput:text
					property="E02DSCSLC" name="platfObj"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="false"
					readonly="true" /></td>
				<td nowrap width="23%">&nbsp;</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="27%">&nbsp;</td>
				<td nowrap width="21%">
				<div align="right">Oficina Venta :</div>
				</td>
				<td nowrap width="29%"><eibsinput:help name="platfObj"
					property="E02PVMBRS"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>"
					required="false" readonly="true" fn_param_one="E02PVMBRS"
					fn_param_two="document.forms[0].E02PVMBNK.value"
					fn_param_three="E02DSCBRS" /><eibsinput:text
					property="E02DSCBRS" name="platfObj"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" required="false"
					readonly="true" /></td>
				<td nowrap width="23%"></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="27%">&nbsp;</td>
				<td nowrap width="21%">
				<div align="right">Fuente Informacion :</div>
				</td>
				<td nowrap width="29%"><eibsinput:cnofc name="platfObj"
					property="E02PVMCVE" required="false" flag="65" fn_code="E02PVMCVE"
					fn_description="E02DSCCVE" readonly="true" /> <eibsinput:text
					property="E02DSCCVE" name="platfObj"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="true"
					required="false" /></td>
				<td nowrap width="23%">&nbsp;</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="27%">&nbsp;</td>
				<td nowrap width="21%">
				<div align="right">Campa�a :</div>
				</td>
				<td nowrap width="29%"><input type="text" name="E02PVMCAM"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMCAM().trim()%>"
					readonly> <eibsinput:text property="E02DSCCAM"
					name="platfObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"
					readonly="true" /></td>
				<td nowrap width="23%">&nbsp;</td>
			</tr>
			<tr id="trdark">
				<td nowrap width="27%">&nbsp;</td>
				<td nowrap width="21%">
				<div align="right">Tipo Campa�a :</div>
				</td>
				<td nowrap width="29%"><input type="text" name="E02TYPCAM"
					size="5" maxlength="4" value="<%=platfObj.getE02TYPCAM().trim()%>"
					readonly> <eibsinput:text property="E02DSCTYP"
					name="platfObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"
					readonly="true" /></td>
				<td nowrap width="23%">&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<h4>Datos BUREAU</h4>

<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id="<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>">
				<td nowrap width="10%">&nbsp;</td>
				<td nowrap width="40%">&nbsp;</td>
				<td nowrap width="25%" align="center"><b>Informados</b></td>
				<td nowrap width="25%" align="center"><b>Ajustados</b></td>
			</tr>
			<tr id="<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>">
				<td nowrap width="10%">&nbsp;</td>
				<td nowrap width="40%">Cantidad Protestos ultimos 6 meses</td>
				<td nowrap width="25%" align="center"><eibsinput:text
					name="platfObj" property="E02PVPU6I"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" size="6"
					maxlength="5" readonly="true" /></td>
				<td nowrap width="25%" align="center"><eibsinput:text
					name="platfObj" property="E02PVPU6A"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" size="6"
					maxlength="5" readonly="true" /></td>
			</tr>
			<tr id="<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>">
				<td nowrap width="10%">&nbsp;</td>
				<td nowrap width="40%">Cantidad Cheques Protestados ultimos 12
				meses:</td>
				<td nowrap width="25%" align="center"><eibsinput:text
					name="platfObj" property="E02PVPU12"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" size="6"
					maxlength="5" readonly="true" /></td>
				<td nowrap width="25%" align="center"><eibsinput:text
					name="platfObj" property="E02PVP12A"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" size="6"
					maxlength="5" readonly="true" /></td>
			</tr>
			<tr id="<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>">
				<td nowrap width="10%">&nbsp;</td>
				<td nowrap width="40%">Monto Total Protestos</td>
				<td nowrap width="25%" align="center"><eibsinput:text
					name="platfObj" property="E02PVPPRO"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="25%" align="center"><eibsinput:text
					name="platfObj" property="E02PVPPRA"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
			</tr>
			<tr id="<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>">
				<td nowrap width="10%">&nbsp;</td>
				<td nowrap width="40%">Monto Mora Total</td>
				<td nowrap width="25%" align="center"><eibsinput:text
					name="platfObj" property="E02PVPMOT"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="25%" align="center"><eibsinput:text
					name="platfObj" property="E02PVPMOA"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
			</tr>
			<tr id="<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>">
				<td nowrap width="10%">&nbsp;</td>
				<td nowrap width="40%">Fecha Vigencia Cedula</td>
				<td nowrap width="25%" align="center"><eibsinput:date
					name="platfObj" fn_year="E02PVPVCY" fn_month="E02PVPVCM"
					fn_day="E02PVPVCD" readonly="true" /></td>
				<td nowrap width="25%" align="center">&nbsp;</td>
			</tr>
			<tr id="<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>">
				<td nowrap width="10%">&nbsp;</td>
				<td nowrap width="40%" colspan="3">Estado
				Cedula: <eibsinput:text
					name="platfObj" property="E02PVPCED"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR%>" size="2"
					maxlength="1" readonly="true" /> <eibsinput:text name="platfObj"
					property="E02DSCCED"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CHAR%>" size="40"
					maxlength="30" readonly="true" /></td>
			</tr>

		</table>
		</td>
	</tr>
</table>

<h4>Endeudamiento (Pesos) <a id="e1"
	href="Javascript:mostrarDeuda();">-</a></h4>
<div id="end">
<table class="tableinfo">
	<tr>
		<td nowrap>
		<%
			row = 0;
		%>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%" align="center"><b>SBIF</b></td>
				<td nowrap width="21%" align="center"><b>Deuda <%=platfObj.getE02PVPPEM()%>
				/ <%=platfObj.getE02PVPPEY()%></b></td>
				<td nowrap width="29%" align="center"><b>Compromiso
				Ponderado</b></td>
				<td nowrap width="23%" align="center"><b>Compromiso
				Ajustado</b></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Consumo</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMCON"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMCONP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMCONM"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					 readonly="true" /></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Comercial</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMCMR"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMCMRP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMCMRM"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					 readonly="true" /></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">&nbsp;&nbsp;&nbsp;<b>Hipotecario</b></td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMHIP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMHIPP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMHIPM"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					 readonly="true" /></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Disponible</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMDIS"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMDISP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMDISM"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					 readonly="true" /></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">&nbsp;&nbsp;&nbsp;<b>Contingente</b></td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMCNT"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMCNTP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMCNTM"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					 readonly="true" /></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>TOTAL</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMTOD"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMTOC"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMTOA"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"
					 readonly="true" /></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left" style="font: bold;"></div>
				</td>
				<td nowrap width="21%" align="center"></td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="center" style="font: bold;">COOPEUCH</div>
				</td>
				<td nowrap width="21%" align="center"><b>Saldo</b></td>
				<td nowrap width="29%" align="center"><b>Mensualidad</b></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Planilla</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMPLA"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMPLAP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Pago Directo</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMPDI"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMPDIP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Tarjeta de Credito</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMTCR"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMTCRP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Ahorro</b></div>
				</td>
				<td nowrap width="21%" align="center">&nbsp;</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMAHOP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Cuotas
				Participaci�n</b></div>
				</td>
				<td nowrap width="21%" align="center">&nbsp;</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMCUOP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Pac Seguros</b></div>
				</td>
				<td nowrap width="21%" align="center">&nbsp;</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMPACP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>TOTAL</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMTOT"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMTOTP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">&nbsp;</td>
				<td nowrap width="21%" align="center">&nbsp;</td>
				<td nowrap width="29%" align="center">&nbsp;</td>
				<td nowrap width="23%" align="center">&nbsp;</td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="center" style="font: bold;">MOROSIDAD</div>
				</td>
				<td nowrap width="21%" align="center"><b>Deuda</b></td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">&nbsp;&nbsp;&nbsp;<b>Directos al d�a
				e impagos &lt; 30 d�as</b></td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMD30"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Directos impagos
				entre 30 y &lt; 90 d�as</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMD90"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">&nbsp;&nbsp;&nbsp;<b>Directos
				impagos entre 90 y &lt; 180 d�as</b></td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVM180"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;&nbsp;<b>Directos impagos
				entre 180 d�as y &lt; a 3 a�os</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVM3AN"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="trclear">
				<td nowrap width="27%">&nbsp;&nbsp;&nbsp;<b>Directos
				impagos &gt;= 3 a�os</b></td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMM3A"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%"></td>
				<td nowrap width="21%" align="center"></td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;<b>Indirectos al d�a e
				impagos &lt; 30 d�as</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMI30"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">&nbsp;&nbsp;<b>Indirectos impagos
				&gt;= 30 d�as y &lt; 3 a�os</b></td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMI3A"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;&nbsp;<b>Indirectos impagos&gt;= 3
				a�os</b></div>
				</td>
				<td nowrap width="21%" align="center"><eibsinput:text
					name="platfObj" property="E02PVMIM3"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
			<tr id="<%=(row++ % 2 == 0 ? "trdark" : "trclear")%>">
				<td nowrap width="27%">
				<div align="left">&nbsp;</div>
				</td>
				<td nowrap width="21%" align="center"></td>
				<td nowrap width="29%" align="center"></td>
				<td nowrap width="23%" align="center"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</div>

<h4>Complemento de Renta 
    <input type="radio" name="E02PVMCRW" value="Y" <%if (!platfObj.getE02PVMCRW().equals("N")) out.print("checked");%>
	onclick="activarCamposComplento('Y');" disabled="disabled"> Si 
	<input type="radio" name="E02PVMCRW" value="N" <%if (platfObj.getE02PVMCRW().equals("N")) out.print("checked");%>
	onclick="activarCamposComplento('N');" disabled="disabled"> No</h4>

<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id="trclear">
				<td nowrap width="27%">
				<div align="right">Rut C�nyugue :</div>
				</td>
				<td nowrap width="21%"><eibsinput:text name="platfObj"
					property="E02PVMWID"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>"
					readonly="true" /></td>
				<td nowrap width="29%">
				<div align="right">Renta C�nyugue :</div>
				</td>
				<td nowrap width="23%"><eibsinput:text name="platfObj"
					property="E02PVMRCO"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					 readonly="true" /></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="27%">
				<div align="right">Nombre C�nyugue :</div>
				</td>
				<td nowrap width="21%"><eibsinput:text name="platfObj"
					property="E02PVMLNW"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL%>"
					readonly="true" /></td>
				<td nowrap width="29%">
				<div align="right">Egresos C�nyugue:</div>
				</td>
				<td nowrap width="23%"><eibsinput:text name="platfObj"
					property="E02PVMEWF"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					 readonly="true"/></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<h4>Ingresos / Egresos (Pesos)</h4>
<%
	row = 0;
%>
<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap colspan="2" align="center"><b>INGRESOS</b></td>
				<td nowrap colspan="2" align="center"><b>OTROS DESCUENTOS
				PLANILLAS</b></td>
			</tr>
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap width="27%">
				<div align="right">Renta Bruta :</div>
				</td>
				<td nowrap width="21%">
					<eibsinput:text name="platfObj" property="E02PVMRBR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/>
				</td>
				<td nowrap width="29%">
				<div align="right">Cuotas de Participaci�n :</div>
				</td>
				<td nowrap width="23%">
					<eibsinput:text name="platfObj" property="E02PVMCPA" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
			</tr>
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap>
				<div align="right">Renta Liquida :</div>
				</td>
				<td nowrap>
					<eibsinput:text name="platfObj" property="E02PVMICU" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/>
				</td>
				<td nowrap>
				<div align="right">Otros con Prioridad :</div>
				</td>
				<td nowrap>
					<eibsinput:text name="platfObj" property="E02PVMOPR" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/>
				</td>
			</tr>
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap>
				<div align="right">Renta Depurada :</div>
				</td>
				<td nowrap>
					<eibsinput:text name="platfObj" property="E02PVMRDE" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/>
				</td>
				<td nowrap>
				<div align="right">Total Descuentos Planilla:</div>
				</td>
				<td nowrap>
					<eibsinput:text name="platfObj" property="E02PVMTDS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
			</tr>
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap>
				<div align="right">Otros Ingresos :</div>
				</td>
				<td nowrap>
					<eibsinput:text name="platfObj" property="E02PVMOIN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/>
				</td>
				<td nowrap colspan="2" align="center">
					<b>OTROS DESCUENTOS	DIRECTO</b>
				</td>

			</tr>
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap>
				<div align="right">Renta Neta Conyugue :</div>
				</td>
				<td nowrap>
					<eibsinput:text name="platfObj" property="E02PVMRNY" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
				<td nowrap>
					<div align="right">Divid. Hipot. Coopeuch :</div>
				</td>
				<td nowrap>
					<eibsinput:text name="platfObj" property="E02PVMDIV" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"/>
				</td>

			</tr>
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap width="27%">
				<div align="right">Renta Total :</div>
				</td>
				<td nowrap width="21%"><eibsinput:text name="platfObj"
					property="E02PVMRTO"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					required="false" readonly="true" /></td>
				<td nowrap>
				<div align="right">Otros Descuentos :</div>
				</td>
				<td nowrap><eibsinput:text name="platfObj" property="E02PVMECU"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true"
					 /></td>

			</tr>

			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap>
				<div align="right">Tipo Otros Ingresos :</div>
				</td>
				<td nowrap width="21%"><eibsinput:cnofc name="platfObj"
					property="E02PVMTOI" required="false" flag="30" fn_code="E02PVMTOI" readonly="true" />
				</td>
				<td nowrap>
				<div align="right">Total Descuentos Directos:</div>
				</td>
				<td nowrap><eibsinput:text name="platfObj" property="E02PVMTD2"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					required="false" readonly="true" /></td>
			</tr>
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap>
				<div align="right">Actividad Otros Ingresos :</div>
				</td>
				<td nowrap width="21%"><eibsinput:cnofc name="platfObj"
					property="E02PVMAOI" required="false" flag="12" fn_code="E02PVMAOI" readonly="true"/>
				</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
			</tr>
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap></td>
				<td nowrap></td>
				<td nowrap>
				<div align="right">Pmt Max Planilla :</div>
				</td>
				<td nowrap><eibsinput:text name="platfObj" property="E02PVMMCP"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					required="false" readonly="true" /></td>
			</tr>
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap>
				<div align="right">Fecha Act. Renta :</div>
				</td>
				<td nowrap><eibsinput:date name="platfObj" fn_year="E02PVMARY"
					fn_month="E02PVMARM" fn_day="E02PVMARD"  readonly="true"/></td>
				<td nowrap>
				<div align="right">Pmt Max Pago Directo:</div>
				</td>
				<td nowrap><eibsinput:text name="platfObj" property="E02PVMMCD"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"
					required="false" readonly="true" /></td>
			</tr>
			<tr id='<%=(row % 2 == 1) ? "trdark" : "trclear"%><%row++;%>'>
				<td nowrap colspan="2" align="center"><input id="EIBSBTN"
					type=button name="Rentas" value="Rentas Hist."
					onclick="consultarRentasHistoricas();"></td>
				<td nowrap colspan="2" align="center"></td>
			</tr>

		</table>
		</td>
	</tr>
</table>

<h4>Convenio</h4>

<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id="trdark">
				<td nowrap width="27%">
				<div align="right">Convenio :</div>
				</td>
				<td nowrap width="21%"><input type="text" name="E02PVMCNV"
					size="5" maxlength="4" value="<%=platfObj.getE02PVMCNV().trim()%>"
					readonly="true"></td>
				<td nowrap width="23%">
				<div align="right">Descripci�n :</div>
				</td>
				<td nowrap width="29%"><input type="text" name="E02DSCCNV"
					size="45" maxlength="35"
					value="<%=platfObj.getE02DSCCNV().trim()%>" readonly="true">
				</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="27%">
				<div align="right">Estado :</div>
				</td>
				<td nowrap width="21%"><input type="hidden" name="E02PVMSCN"
					size="16" maxlength="15"
					value="<%=platfObj.getE02PVMSCN().trim()%>" readonly="true">
				<input type="text" name="E02DSCSCN" size="26" maxlength="25"
					value="<%=platfObj.getE02DSCSCN().trim()%>" readonly="true">
				</td>
				<td nowrap width="23%">
				<div align="right">Proximo Vencimiento :</div>
				</td>
				<td nowrap width="29%"><eibsinput:date name="platfObj"
					fn_year="E02PVMPVY" fn_month="E02PVMPVM" fn_day="E02PVMPVD"
					readonly="true" readonly="true" /></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="27%">
				<div align="right">Descuento Empleador :</div>
				</td>
				<td nowrap width="21%"><eibsinput:text name="platfObj"
					property="E02PVMDCT"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>"
					readonly="true" /></td>
				<td nowrap width="23%">
				<div align="right">Descuento por excepci�n :</div>
				</td>
				<td nowrap width="29%">
					<eibsinput:text name="platfObj" property="E02PVMDEX" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PERCENTAGE%>" readonly="true"/> &nbsp;&nbsp;&nbsp;<input
					id="EIBSBTN" type=button name="Pauta" value="Pauta Eval."
					onclick="consultarPautaEvaluacion();"></td>
			</tr>
			<tr id="trclear">
				<td nowrap>
				<div align="right">Base C�lculo :</div>
				</td>
				<td nowrap><input type="hidden" name="E02PVMBCA" size="16"
					maxlength="15" value="<%=platfObj.getE02PVMBCA().trim()%>"
					readonly="true"> <input type="text" name="E02DSCBCA"
					size="26" maxlength="25"
					value="<%=platfObj.getE02DSCBCA().trim()%>" readonly="true">
				</td>
				<td nowrap>
				<div align="right">Maximo Descuento Convenio :</div>
				</td>
				<td nowrap width="29%"><eibsinput:text name="platfObj"
					property="E02PVMMBC"
					eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" readonly="true" />
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
 <h4>Selecci�n Medio de Evaluaci�n</h4>
    
  <table class="tableinfo">
    <tr > 
      <td nowrap >
      <%row=0; %> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
		<tr id="<%=(row++%2==0?"trdark":"trclear")%>">
        	<td>
        		&nbsp;
			</td>
		 </tr>        
        <tr id="<%=(row++%2==0?"trdark":"trclear")%>">
        	<td valign="middle">
        		<input type="hidden" name="E02PVMDOP" value="<%=platfObj.getE02PVMDOP() %>">
      	  		<select name="E02PVMTPR" onchange="seleccionarDOP()" disabled="disabled">
					<option value=""> - - - - - - - - - - - - - - - - Ninguno - - - - - - - - - - - - - - - - </option>            
            	<%
	            	if (EPV121601List!=null && !EPV121601List.getNoResult()) {
	            		EPV121601List.initRow();
	            		while (EPV121601List.getNextRow()) {
							EPV121601Message convObj = (EPV121601Message) EPV121601List.getRecord();
							out.println("<option value='"+convObj.getE01TPRTPR()+"'>"+convObj.getE01TPRDES()+"</option>");
    		        	}
    		        }
            	 %>
				</select> 
				<script type="text/javascript">
					document.forms[0].E02PVMTPR.value='<%=platfObj.getE02PVMTPR()%>'
				</script>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input id="EIBSBTN" type=button name="detalleProducto" value="Detalle Medio."    onclick="consultarMedioEvaluacion();">				
			</td>
		 </tr>
		<tr id="<%=(row++%2==0?"trdark":"trclear")%>">
        	<td>
        		&nbsp;
			</td>
		 </tr>        		 
        </table>        
        
	</td>        
   </tr>  
  </table>

<h4>Informaci�n de Auditoria (Ultima Actualizaci�n)</h4>

<table class="tableinfo">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			align="center">
			<tr id="trdark">
				<td nowrap width="27%">
				<div align="right">Fecha :</div>
				</td>
				<td nowrap width="21%">
					<eibsinput:date name="platfObj" fn_year="E02PVMUDY" fn_month="E02PVMUDM"
					fn_day="E02PVMUDD" readonly="true" />
				</td>
				<td nowrap width="23%">
				<div align="right">Hora :</div>
				</td>
				<td nowrap width="29%">
					<input type="text" name="E02PVMUTM" size="20" maxlength="35"
					value="<%=platfObj.getE02PVMUTM().trim().substring(11,19)%>" readonly="true">
				</td>
			</tr>
			<tr id="trclear">
				<td nowrap width="27%">
				<div align="right">usuario :</div>
				</td>
				<td nowrap width="21%">
				<input type="text" name="E02PVMUUS" size="26" maxlength="25"
					value="<%=platfObj.getE02PVMUUS().trim()%>" readonly="true">
				</td>
				<td nowrap width="23%">
				<div align="right">&nbsp;</div>
				</td>
				<td nowrap width="29%">&nbsp;</td>
			</tr>
						
		</table>
		</td>
	</tr>
</table>


</form>
</body>
</HTML>
