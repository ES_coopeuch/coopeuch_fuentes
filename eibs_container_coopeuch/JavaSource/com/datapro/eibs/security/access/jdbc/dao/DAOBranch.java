package com.datapro.eibs.security.access.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.exception.BranchNotFoundException;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.security.bean.CNTRLBRN;
import com.datapro.generic.access.DAOSuper;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.BeanParser;

/**
 * @author ogarcia
 *
 */
public class DAOBranch extends DAOSuper {

	private static Category cat =
		Category.getInstance(DAOBranch.class.getName());

	/**
	 * Constructor for DAOBranch.
	 * @param dbid
	 */
	public DAOBranch(String dbid) {
		super(dbid);
	}

	/**
	 * Constructor for DAOBranch.
	 */
	public DAOBranch() {
		super();
	}

	/**
	 * Constructor for DAOBranch.
	 * @param connection
	 */
	public DAOBranch(Connection connection) {
		super(connection);
	}

	/**
	 * Get branch record.
	 */
	public CNTRLBRN getBranch(String bnk, String brn)
		throws BranchNotFoundException, DAOException {

		CNTRLBRN bean = null;

		try {
			String sql = "SELECT * FROM CNTRLBRN WHERE BRNBNK=? AND BRNNUM=? ORDER BY BRNBNK, BRNNUM";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, bnk);
			ps.setString(2, brn);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNTRLBRN();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} else {
				throw new BranchNotFoundException(
					"Branch not found exception(bank: " + bnk + " - branch: " + brn + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return bean;

	}

	/**
	 * Get branches per bank.
	 */
	public BeanList getBranches(String bnk)
		throws BranchNotFoundException, DAOException {

		BeanList list = null;

		try {
			String sql = "SELECT * FROM CNTRLBRN WHERE BRNBNK=? ORDER BY BRNBNK, BRNNUM";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, bnk);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				CNTRLBRN bean = new CNTRLBRN();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new BranchNotFoundException(
					"Branch not found exception(bank: " + bnk + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}

	/**
	 * Get branches.
	 */
	public BeanList getBranches()
		throws BranchNotFoundException, DAOException {

		BeanList list = null;

		try {
			String sql = "SELECT * FROM CNTRLBRN ORDER BY BRNBNK, BRNNUM";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				CNTRLBRN bean = new CNTRLBRN();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			if (list.getNoResult()) {
				throw new BranchNotFoundException(
					"Branch not found exception.");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}

}
