/**
 * PrendasSinDesplazamiento.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class PrendasSinDesplazamiento  implements java.io.Serializable {
    private java.math.BigInteger id;  // attribute
    private java.math.BigInteger idConsulta;
    private java.math.BigInteger IFeDiarioOficial;
    private java.math.BigInteger cuerpo;
    private java.math.BigInteger pagina;
    private java.math.BigInteger extracto;
    private java.math.BigInteger rutAcreedor;
    private java.lang.String nombreAcreedor;
    private java.math.BigInteger rutNotario;
    private java.lang.String nombreNotario;

    public PrendasSinDesplazamiento() {
    }

    public java.math.BigInteger getId() {
        return id;
    }

    public void setId(java.math.BigInteger id) {
        this.id = id;
    }

    public java.math.BigInteger getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(java.math.BigInteger idConsulta) {
        this.idConsulta = idConsulta;
    }

    public java.math.BigInteger getIFeDiarioOficial() {
        return IFeDiarioOficial;
    }

    public void setIFeDiarioOficial(java.math.BigInteger IFeDiarioOficial) {
        this.IFeDiarioOficial = IFeDiarioOficial;
    }

    public java.math.BigInteger getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(java.math.BigInteger cuerpo) {
        this.cuerpo = cuerpo;
    }

    public java.math.BigInteger getPagina() {
        return pagina;
    }

    public void setPagina(java.math.BigInteger pagina) {
        this.pagina = pagina;
    }

    public java.math.BigInteger getExtracto() {
        return extracto;
    }

    public void setExtracto(java.math.BigInteger extracto) {
        this.extracto = extracto;
    }

    public java.math.BigInteger getRutAcreedor() {
        return rutAcreedor;
    }

    public void setRutAcreedor(java.math.BigInteger rutAcreedor) {
        this.rutAcreedor = rutAcreedor;
    }

    public java.lang.String getNombreAcreedor() {
        return nombreAcreedor;
    }

    public void setNombreAcreedor(java.lang.String nombreAcreedor) {
        this.nombreAcreedor = nombreAcreedor;
    }

    public java.math.BigInteger getRutNotario() {
        return rutNotario;
    }

    public void setRutNotario(java.math.BigInteger rutNotario) {
        this.rutNotario = rutNotario;
    }

    public java.lang.String getNombreNotario() {
        return nombreNotario;
    }

    public void setNombreNotario(java.lang.String nombreNotario) {
        this.nombreNotario = nombreNotario;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        PrendasSinDesplazamiento other = (PrendasSinDesplazamiento) obj;
        boolean _equals;
        _equals = true
            && ((this.nombreAcreedor==null && other.getNombreAcreedor()==null) || 
             (this.nombreAcreedor!=null &&
              this.nombreAcreedor.equals(other.getNombreAcreedor())))
            && ((this.nombreNotario==null && other.getNombreNotario()==null) || 
             (this.nombreNotario!=null &&
              this.nombreNotario.equals(other.getNombreNotario())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        PrendasSinDesplazamiento history = (PrendasSinDesplazamiento) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId())))
            && ((this.idConsulta==null && other.getIdConsulta()==null) || 
             (this.idConsulta!=null &&
              this.idConsulta.equals(other.getIdConsulta())))
            && ((this.IFeDiarioOficial==null && other.getIFeDiarioOficial()==null) || 
             (this.IFeDiarioOficial!=null &&
              this.IFeDiarioOficial.equals(other.getIFeDiarioOficial())))
            && ((this.cuerpo==null && other.getCuerpo()==null) || 
             (this.cuerpo!=null &&
              this.cuerpo.equals(other.getCuerpo())))
            && ((this.pagina==null && other.getPagina()==null) || 
             (this.pagina!=null &&
              this.pagina.equals(other.getPagina())))
            && ((this.extracto==null && other.getExtracto()==null) || 
             (this.extracto!=null &&
              this.extracto.equals(other.getExtracto())))
            && ((this.rutAcreedor==null && other.getRutAcreedor()==null) || 
             (this.rutAcreedor!=null &&
              this.rutAcreedor.equals(other.getRutAcreedor())))
            && ((this.rutNotario==null && other.getRutNotario()==null) || 
             (this.rutNotario!=null &&
              this.rutNotario.equals(other.getRutNotario())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        PrendasSinDesplazamiento history = (PrendasSinDesplazamiento) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getIdConsulta() != null) {
            _hashCode += getIdConsulta().hashCode();
        }
        if (getIFeDiarioOficial() != null) {
            _hashCode += getIFeDiarioOficial().hashCode();
        }
        if (getCuerpo() != null) {
            _hashCode += getCuerpo().hashCode();
        }
        if (getPagina() != null) {
            _hashCode += getPagina().hashCode();
        }
        if (getExtracto() != null) {
            _hashCode += getExtracto().hashCode();
        }
        if (getRutAcreedor() != null) {
            _hashCode += getRutAcreedor().hashCode();
        }
        if (getNombreAcreedor() != null) {
            _hashCode += getNombreAcreedor().hashCode();
        }
        if (getRutNotario() != null) {
            _hashCode += getRutNotario().hashCode();
        }
        if (getNombreNotario() != null) {
            _hashCode += getNombreNotario().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
