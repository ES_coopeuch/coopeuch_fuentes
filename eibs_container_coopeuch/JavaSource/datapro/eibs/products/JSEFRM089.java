package datapro.eibs.products;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.adobe.fdf.FDFDoc;

import datapro.eibs.beans.ECO001004Message;
import datapro.eibs.beans.ECO0010DSMessage;
import datapro.eibs.beans.EFRM08901Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD008001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.reports.JSEFRM000PDF;

public class JSEFRM089 extends  JSEFRM000PDF {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5013250952357918505L;
	
	protected static final int R_ENTER_SEARCH  = 100;
	protected static final int A_ENTER_SEARCH = 200;
	/**
	 * 
	 */
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {

		case R_ENTER_SEARCH :
			procReqEnterID(user, req, res, session);
			break;
		case A_ENTER_SEARCH:
			procActionEnterID(user, req, res, session);
			break;
			
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	
	//
	protected void procReqEnterID(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		
		ESD008001Message msgClient = null;
		ELEERRMessage msgError = null;

		try {

			msgClient = new datapro.eibs.beans.ESD008001Message(); 
			msgError = new datapro.eibs.beans.ELEERRMessage();

			UserPos userPO = new datapro.eibs.beans.UserPos(); 
			userPO.setOption("CLIENT");
			userPO.setPurpose("SOL");
			userPO.setRedirect("/servlet/datapro.eibs.products.JSEFRM089?SCREEN=200");
			ses.setAttribute("client", msgClient);
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

			flexLog("About to call Page: ESD0080_client_both_enter.jsp");
			forward("ESD0080_client_both_enter.jsp", req, res);

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}

	//
	protected void procActionEnterID(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("ECO0010", req);
			EFRM08901Message msg = (EFRM08901Message) mp.getMessageRecord("EFRM08901", user.getH01USR(), "0001");


			msg.setE89ENTCUN (req.getParameter("E01CUN") == null ? "0" : req.getParameter("E01CUN"));
			msg.setE89NUMRUT(req.getParameter("E01IDN") == null ? "" : req.getParameter("E01IDN"));
	
			mp.sendMessage(msg);

			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msg = (EFRM08901Message)mp.receiveMessageRecord();
			
			session.setAttribute("error", msgError);
			session.setAttribute("userPO", userPO);
			session.setAttribute("msg", msg);
			
			if (mp.hasError(msgError)) { 
				flexLog("About to call Page: ESD0080_client_both_enter.jsp");
			    forward("ESD0080_client_both_enter.jsp", req, res);
			}				
			else 
			{
				flexLog("About to call Page: EFRM089_mandato_pensionado_consulta.jsp");
				forwardOnSuccess("EFRM089_mandato_pensionado_consulta.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

//END
	
}
