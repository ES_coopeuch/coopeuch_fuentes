package datapro.eibs.products;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDD232001Message;
import datapro.eibs.beans.EDD232002Message;
import datapro.eibs.beans.EDD232003Message;
import datapro.eibs.beans.EDD232004Message;
import datapro.eibs.beans.EDD232005Message;
import datapro.eibs.beans.EDD232006Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;

public class JSEDD2320 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID      = 1L;
	protected static final int R_COMI_LIST			= 100;
	protected static final int A_COMISION 			= 800;

	protected static final int A_MAINTENANCE 		= 600;
	
	protected static final int R_COMI_ELE_LIST		= 1100;
	protected static final int A_COMISION_ELE 		= 1800;
	protected static final int A_MAINTENANCE_ELE 	= 1600;

	protected static final int R_RAT_LIST			= 2100;
	protected static final int A_RATES 				= 2800;
	protected static final int A_MAINTENANCE_RAT 	= 2600;

	protected static final int R_DIS_LIST			= 3100;
	protected static final int A_DISCOUNTS			= 3800;
	protected static final int A_MAINTENANCE_DIS 	= 3600;

	protected static final int R_TAB_LIST			= 4100;
	protected static final int A_TABLAS 			= 4800;
	protected static final int A_MAINTENANCE_TAB 	= 4600;

	protected static final int R_DIS_TAB_LIST			= 5100;
	protected static final int A_DISCOUNTS_TAB 		    = 5800;
	protected static final int A_MAINTENANCE_DIS_TAB 	= 5600;
	
	protected static final int R_TAB_ELE_LIST			= 6100;
	protected static final int A_TABLAS_ELE 		    = 6800;
	protected static final int A_MAINTENANCE_TAB_ELE 	= 6600;
	
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_COMI_LIST :
				procReqComiList(user, req, res, session);
				break;
				// Action
			case A_COMISION :
				procActionCom(user, req, res, session);
				break;
			case A_MAINTENANCE :
				procActionMaintenance(user, req, res, session);
				break;
			case R_COMI_ELE_LIST :
				procReqComiEleList(user, req, res, session);
				break;
			case A_COMISION_ELE :
				procActionComEle(user, req, res, session);
				break;
			case A_MAINTENANCE_ELE :
				procActionMaintenanceEle(user, req, res, session);
				break;

			case R_RAT_LIST :
				procReqRatList(user, req, res, session);
				break;
			case A_RATES :
				procActionRat(user, req, res, session);
				break;
			case A_MAINTENANCE_RAT :
				procActionMaintenanceRat(user, req, res, session);
				break;

			case R_DIS_LIST :
				procReqDisList(user, req, res, session);
				break;
			case A_DISCOUNTS :
				procActionDis(user, req, res, session);
				break;
			case A_MAINTENANCE_DIS :
				procActionMaintenanceDis(user, req, res, session);
				break;
			case R_DIS_TAB_LIST :
				procReqDisTabList(user, req, res, session);
				break;
			case A_DISCOUNTS_TAB :
				procActionDisTab(user, req, res, session);
				break;
			case A_MAINTENANCE_DIS_TAB :
				procActionMaintenanceDisTab(user, req, res, session);
				break;
				
			case R_TAB_LIST :
				procReqTabList(user, req, res, session);
				break;
			case A_TABLAS :
				procActionTab(user, req, res, session);
				break;
			case A_MAINTENANCE_TAB :
				procActionMaintenanceTab(user, req, res, session);
				break;
			case R_TAB_ELE_LIST :
				procReqTabEleList(user, req, res, session);
				break;
			case A_TABLAS_ELE :
				procActionTabEle(user, req, res, session);
				break;
			case A_MAINTENANCE_TAB_ELE :
				procActionMaintenanceTabEle(user, req, res, session);
				break;

				
			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

	private void procActionMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDD2320", req);
			EDD232001Message msgRT = (EDD232001Message) new EDD232001Message() ;
			setMessageRecord(req, msgRT);
			msgRT.setH01USERID(user.getH01USR());
			msgRT.setH01PROGRM("EDD2320");
			msgRT.setH01TIMSYS(getTimeStamp());
			
			if(userPO.getPurpose().equals("NEW"))
					msgRT.setH01OPECOD("0005");
			else
				msgRT.setH01OPECOD("0001");
			
			mp.sendMessage(msgRT);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRT = (EDD232001Message) mp.receiveMessageRecord("EDD232001");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("refComi", msgRT);
				session.setAttribute("userPO", userPO);
				
				forward("EDD2320_commission_maitenance.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				
				procReqComiList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}

	private void procActionCom(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNew(user, req, res, session);
				break;
			case 3 : //Deletion
				userPO.setPurpose("DELETE");
				session.setAttribute("userPO", userPO);
				procActionDelete(user, req, res, session);
				break;
			default : //Maintenance
				userPO.setPurpose("MAINTENANCE");
				session.setAttribute("userPO", userPO);
				procReqMaintenance(user, req, res, session);
				break;
		}
	}

	private void procReqMaintenance(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		
		JBObjList bl = (JBObjList) session.getAttribute("RTComis");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		bl.setCurrentRow(idx);
		EDD232001Message msgDoc = (EDD232001Message) bl.getRecord();

		flexLog("Putting java beans into the session");
		session.setAttribute("refComi", msgDoc);
		session.setAttribute("userPO", userPO);
			
		forward("EDD2320_commission_maitenance.jsp", req, res);
	}

	private void procActionDelete(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
	
		JBObjList bl = (JBObjList) session.getAttribute("RTComis");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
		bl.setCurrentRow(idx);
		EDD232001Message msgDoc = (EDD232001Message) bl.getRecord();

		try {
			mp = getMessageProcessor("EDD2320", req);
			msgDoc.setH01USERID(user.getH01USR());
			msgDoc.setH01PROGRM("EDD2320");
			msgDoc.setH01TIMSYS(getTimeStamp());
			msgDoc.setH01OPECOD("0004");
		
			mp.sendMessage(msgDoc);
		
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgDoc = (EDD232001Message) mp.receiveMessageRecord("EDD232001");

			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqComiList(user, req, res, session);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqComiList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}

	private void procReqNew(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		flexLog("Putting java beans into the session");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("refComi", new EDD232001Message());
		
		forward("EDD2320_commission_maitenance.jsp", req, res);
	}

	private void procReqComiList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDD2320", req);
			EDD232001Message msgList = (EDD232001Message) mp.getMessageRecord("EDD232001", user.getH01USR(), "0002");

			mp.sendMessage(msgList);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);

				forward("error_viewer.jsp", req, res);
			} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("RTComis", list);
				session.setAttribute("userPO", userPO);
				
				forward("EDD2320_commission_list.jsp", req, res);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}


	//
	private void procReqComiEleList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		EDD232001Message msgRTComis = null;
		JBObjList list = null;
		JBObjList bl = null;
	
	try 
	{
			//Rescato tabla seleccionada
				try 
				{
					bl = (JBObjList) session.getAttribute("RTComis");
					int idx = 0;
					idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
					bl.setCurrentRow(idx);
					msgRTComis = (EDD232001Message) bl.getRecord();
				} catch (Exception e) {
					msgRTComis = (EDD232001Message) session.getAttribute("RTComis");
				}
				mp = getMessageProcessor("EDD2320", req);
				EDD232001Message msgComEle = (EDD232001Message) mp.getMessageRecord("EDD232001", user.getH01USR(), "0003");
				msgComEle.setE01CMTMCCM(msgRTComis.getE01CMTMCCM());
				
				mp.sendMessage(msgComEle);
			
				msgError = (ELEERRMessage) mp.receiveMessageRecord(); 
			
				list = mp.receiveMessageRecordList("H02FLGMAS");

				if (mp.hasError(msgError)) {
					flexLog("Putting java beans into the session");
					session.setAttribute("error", msgError);
					session.setAttribute("RTComis", bl);
					session.setAttribute("userPO", userPO);

					forward("EDD2320_commission_list.jsp", req, res);
				} else {
					flexLog("Putting java beans into the session");
					session.setAttribute("RTComisEle", list);
					session.setAttribute("refComi", msgRTComis);
					session.setAttribute("userPO", userPO);
				
					forward("EDD2320_commission_relation_list.jsp", req, res);
					}
		} 
		finally {
			if (mp != null)	mp.close(); 
		}
	}
//	

	
	private void procActionComEle(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNewEle(user, req, res, session);
				break;
			case 3 : //Deletion
				userPO.setPurpose("DELETE");
				session.setAttribute("userPO", userPO);
				procActionDeleteEle(user, req, res, session);
				break;
			default : //Maintenance
				userPO.setPurpose("MAINTENANCE");
				session.setAttribute("userPO", userPO);
				procReqMaintenanceEle(user, req, res, session);
				break;
		}
	}

//
	private void procReqMaintenanceEle(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		
		EDD232001Message msgRT =  (EDD232001Message) session.getAttribute("refComi");

		setMessageRecord(req, msgRT);
		
		JBObjList bl = (JBObjList) session.getAttribute("RTComisEle");
		int idx = 0;
		
		try {idx = Integer.parseInt(req.getParameter("CURRCODE2").trim());} 
		catch (Exception e) {throw new ServletException(e);}
		
		bl.setCurrentRow(idx);
		
		EDD232002Message msgRel = (EDD232002Message) bl.getRecord();
		
		session.setAttribute("RTComis", msgRT);
		session.setAttribute("RefRel", msgRel);
		session.setAttribute("userPO", userPO);
		
		forward("EDD2320_commission_relation_maitenance.jsp", req, res);

	}

//
	private void procActionMaintenanceEle(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
			
		try {
			EDD232001Message msgCom = (EDD232001Message) new EDD232001Message();
			EDD232002Message msgRel = (EDD232002Message) new EDD232002Message();
		
			setMessageRecord(req, msgCom); 

			mp = getMessageProcessor("EDD2320", req);
			
			msgRel.setH02USERID(user.getH01USR());
			msgRel.setH02PROGRM("EDD2320");
			msgRel.setH02TIMSYS(getTimeStamp());
			msgRel.setH02OPECOD("0001");

			if(userPO.getPurpose().equals("NEW"))
				msgRel.setH02OPECOD("0005");
			else
				msgRel.setH02OPECOD("0001");
			
			
			setMessageRecord(req, msgRel);
			
			msgRel.setE02CMRLCCM(msgCom.getE01CMTMCCM());
				
			mp.sendMessage(msgRel);

			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRel = (EDD232002Message) mp.receiveMessageRecord("EDD232002");
		
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("RTComis", msgCom);
				session.setAttribute("RefRel", msgRel);
				
				forward("EDD2320_commission_relation_maitenance.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");

				session.setAttribute("userPO", userPO);
				session.setAttribute("RTComis", msgCom);
				procReqComiEleList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}
//
	
	private void procReqNewEle(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
	
		EDD232001Message msgRT = (EDD232001Message) new EDD232001Message();
		EDD232002Message msgRel = (EDD232002Message) new EDD232002Message();
			
		setMessageRecord(req, msgRT);

		flexLog("Putting java beans into the session");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("RTComis", msgRT);
		session.setAttribute("RefRel", msgRel);
		
		forward("EDD2320_commission_relation_maitenance.jsp", req, res);
	}
//
	
	private void procActionDeleteEle(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		EDD232002Message msgRel =  (EDD232002Message) new EDD232002Message();
		
		try
		{
			EDD232001Message msgRT =  (EDD232001Message) session.getAttribute("refComi");
			setMessageRecord(req, msgRT);
		
			JBObjList bl = (JBObjList) session.getAttribute("RTComisEle");
			int idx = 0;
		
			try {idx = Integer.parseInt(req.getParameter("CURRCODE2").trim());} 
			catch (Exception e) {throw new ServletException(e);}
		
			bl.setCurrentRow(idx);
		
			msgRel = (EDD232002Message) bl.getRecord();
		
			mp = getMessageProcessor("EDD2320", req);
		
			msgRel.setH02USERID(user.getH01USR());
			msgRel.setH02PROGRM("EDD2320");
			msgRel.setH02TIMSYS(getTimeStamp());
			msgRel.setH02OPECOD("0004");
			msgRel.setE02CMRLCCM(msgRT.getE01CMTMCCM());
			
			mp.sendMessage(msgRel);

			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRel = (EDD232002Message) mp.receiveMessageRecord("EDD232002");

			session.setAttribute("refComi", msgRT);
			session.setAttribute("refRel", msgRel);
		
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqComiEleList(user, req, res, session);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqComiEleList(user, req, res, session);
			}

	} finally {
			if (mp != null)	mp.close();
		}
	}

/***/
	private void procReqRatList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDD2320", req);
			EDD232003Message msgRTRat = (EDD232003Message) mp.getMessageRecord("EDD232003", user.getH01USR(), "0003");

			mp.sendMessage(msgRTRat);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList msgRTRatList = mp.receiveMessageRecordList("H03FLGMAS");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);

				forward("error_viewer.jsp", req, res);
			} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("RTRatList", msgRTRatList);
				session.setAttribute("userPO", userPO);
				
				forward("EDD2320_rate_list.jsp", req, res);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
/***/
	private void procActionRat(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNewRat(user, req, res, session);
				break;
			case 3 : //Deletion
				userPO.setPurpose("DELETE");
				session.setAttribute("userPO", userPO);
				procActionDeleteRat(user, req, res, session);
				break;
			default : //Maintenance
				userPO.setPurpose("MAINTENANCE");
				session.setAttribute("userPO", userPO);
				procReqMaintenanceRat(user, req, res, session);
				break;
		}
	}

/***/
	private void procReqNewRat(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		flexLog("Putting java beans into the session");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("RTRates", new EDD232003Message());
		
		forward("EDD2320_rate_maitenance.jsp", req, res);
	}

/***/
	private void procActionDeleteRat(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
	
		JBObjList bl = (JBObjList) session.getAttribute("RTRatList");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
		bl.setCurrentRow(idx);
		EDD232003Message msgRTRat = (EDD232003Message) bl.getRecord();

		try {
			mp = getMessageProcessor("EDD2320", req);
			msgRTRat.setH03USERID(user.getH01USR());
			msgRTRat.setH03PROGRM("EDD2320");
			msgRTRat.setH03TIMSYS(getTimeStamp());
			msgRTRat.setH03OPECOD("0004");
		
			mp.sendMessage(msgRTRat);
		
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRTRat = (EDD232003Message) mp.receiveMessageRecord("EDD232003");

			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqRatList(user, req, res, session);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqRatList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}

/***/
	private void procReqMaintenanceRat(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		
		JBObjList bl = (JBObjList) session.getAttribute("RTRatList");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		bl.setCurrentRow(idx);
		EDD232003Message msgRTRat = (EDD232003Message) bl.getRecord();

		flexLog("Putting java beans into the session");
		session.setAttribute("RTRates", msgRTRat);
		session.setAttribute("userPO", userPO);
			
		forward("EDD2320_rate_maitenance.jsp", req, res);
	}
	
/***/
	private void procActionMaintenanceRat(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		
		try {
			mp = getMessageProcessor("EDD2320", req);
			EDD232003Message msgRTRat = (EDD232003Message) new EDD232003Message() ;
			setMessageRecord(req, msgRTRat);
			msgRTRat.setH03USERID(user.getH01USR());
			msgRTRat.setH03PROGRM("EDD2320");
			msgRTRat.setH03TIMSYS(getTimeStamp());

			if(userPO.getPurpose().equals("NEW"))
				msgRTRat.setH03OPECOD("0005");
			else
				msgRTRat.setH03OPECOD("0001");

			mp.sendMessage(msgRTRat);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRTRat = (EDD232003Message) mp.receiveMessageRecord("EDD232003");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("RTRates", msgRTRat);
				session.setAttribute("userPO", userPO);
				
				forward("EDD2320_rate_maitenance.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				
				procReqRatList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}

	/***/
	private void procReqDisList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDD2320", req);
			EDD232004Message msgRTDis = (EDD232004Message) mp.getMessageRecord("EDD232004", user.getH01USR(), "0003");

			mp.sendMessage(msgRTDis);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList msgRTDisList = mp.receiveMessageRecordList("H04FLGMAS");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);

				forward("error_viewer.jsp", req, res);
			} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("RTDisList", msgRTDisList);
				session.setAttribute("userPO", userPO);
				
				forward("EDD2320_discounts_list.jsp", req, res);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
	/***/
	private void procActionDis(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNewDis(user, req, res, session);
				break;
			case 3 : //Deletion
				userPO.setPurpose("DELETE");
				session.setAttribute("userPO", userPO);
				procActionDeleteDis(user, req, res, session);
				break;
			default : //Maintenance
				userPO.setPurpose("MAINTENANCE");
				session.setAttribute("userPO", userPO);
				procReqMaintenanceDis(user, req, res, session);
				break;
		}
	}

/***/
	private void procReqNewDis(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		flexLog("Putting java beans into the session");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("RTDis", new EDD232004Message());
		
		forward("EDD2320_discounts_maitenance.jsp", req, res);
	}
/***/
	private void procActionMaintenanceDis(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		
		try {
			mp = getMessageProcessor("EDD2320", req);
			EDD232004Message msgRTDis = (EDD232004Message) new EDD232004Message() ;
			setMessageRecord(req, msgRTDis);
			msgRTDis.setH04USERID(user.getH01USR());
			msgRTDis.setH04PROGRM("EDD2320");
			msgRTDis.setH04TIMSYS(getTimeStamp());
			
			if(userPO.getPurpose().equals("NEW"))
				msgRTDis.setH04OPECOD("0006");
			else
				msgRTDis.setH04OPECOD("0001");
			
			mp.sendMessage(msgRTDis);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRTDis = (EDD232004Message) mp.receiveMessageRecord("EDD232004");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("RTDis", msgRTDis);
				session.setAttribute("userPO", userPO);
				
				forward("EDD2320_discounts_maitenance.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				
				procReqDisList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}
	/***/
	private void procActionDeleteDis(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
	
		JBObjList bl = (JBObjList) session.getAttribute("RTDisList");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
		bl.setCurrentRow(idx);
		EDD232004Message msgRTDis = (EDD232004Message) bl.getRecord();

		try {
			mp = getMessageProcessor("EDD2320", req);
			msgRTDis.setH04USERID(user.getH01USR());
			msgRTDis.setH04PROGRM("EDD2320");
			msgRTDis.setH04TIMSYS(getTimeStamp());
			msgRTDis.setH04OPECOD("0004");
		
			mp.sendMessage(msgRTDis);
		
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRTDis = (EDD232004Message) mp.receiveMessageRecord("EDD232004");

			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqDisList(user, req, res, session);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqDisList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}

/***/
	private void procReqMaintenanceDis(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		
		JBObjList bl = (JBObjList) session.getAttribute("RTDisList");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		bl.setCurrentRow(idx);
		EDD232004Message msgRTDis = (EDD232004Message) bl.getRecord();

		flexLog("Putting java beans into the session");
		session.setAttribute("RTDis", msgRTDis);
		session.setAttribute("userPO", userPO);
			
		forward("EDD2320_Discounts_maitenance.jsp", req, res);
	}

	/***/
	private void procReqTabList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDD2320", req);
			EDD232005Message msgRTTab = (EDD232005Message) mp.getMessageRecord("EDD232005", user.getH01USR(), "0003");

			mp.sendMessage(msgRTTab);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList msgRTTabList = mp.receiveMessageRecordList("H05FLGMAS");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);

				forward("error_viewer.jsp", req, res);
			} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("RTTabList", msgRTTabList);
				session.setAttribute("userPO", userPO);
				
				forward("EDD2320_tabla_list.jsp", req, res);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}
	
	/***/
	private void procActionTab(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNewTab(user, req, res, session);
				break;
			case 3 : //Deletion
				userPO.setPurpose("DELETE");
				session.setAttribute("userPO", userPO);
				procActionDeleteTab(user, req, res, session);
				break;
			default : //Maintenance
				userPO.setPurpose("MAINTENANCE");
				session.setAttribute("userPO", userPO);
				procReqMaintenanceTab(user, req, res, session);
				break;
		}
	}

/***/
	private void procReqNewTab(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		flexLog("Putting java beans into the session");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("RTTab", new EDD232005Message());
		
		forward("EDD2320_tabla_maitenance.jsp", req, res);
	}
/***/

	private void procActionMaintenanceTab(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		
		try {
			mp = getMessageProcessor("EDD2320", req);
			EDD232005Message msgRTTab = (EDD232005Message) new EDD232005Message() ;
			setMessageRecord(req, msgRTTab);
			msgRTTab.setH05USERID(user.getH01USR());
			msgRTTab.setH05PROGRM("EDD2320");
			msgRTTab.setH05TIMSYS(getTimeStamp());

			if(userPO.getPurpose().equals("NEW"))
				msgRTTab.setH05OPECOD("0005");
			else
				msgRTTab.setH05OPECOD("0001");
			
			mp.sendMessage(msgRTTab);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRTTab = (EDD232005Message) mp.receiveMessageRecord("EDD232005");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("RTTab", msgRTTab);
				session.setAttribute("userPO", userPO);
				
				forward("EDD2320_tabla_maitenance.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				
				procReqTabList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}
	/***/
	private void procActionDeleteTab(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
	
		JBObjList bl = (JBObjList) session.getAttribute("RTTabList");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
		bl.setCurrentRow(idx);
		EDD232005Message msgRTTab = (EDD232005Message) bl.getRecord();

		try {
			mp = getMessageProcessor("EDD2320", req);
			msgRTTab.setH05USERID(user.getH01USR());
			msgRTTab.setH05PROGRM("EDD2320");
			msgRTTab.setH05TIMSYS(getTimeStamp());
			msgRTTab.setH05OPECOD("0004");
		
			mp.sendMessage(msgRTTab);
		
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRTTab = (EDD232005Message) mp.receiveMessageRecord("EDD232005");

			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqTabList(user, req, res, session);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqTabList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}

/***/
	private void procReqMaintenanceTab(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		
		JBObjList bl = (JBObjList) session.getAttribute("RTTabList");
		int idx = 0;
		try {
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
		} catch (Exception e) {
			throw new ServletException(e);
		}
		bl.setCurrentRow(idx);
		EDD232005Message msgRTTab = (EDD232005Message) bl.getRecord();

		flexLog("Putting java beans into the session");
		session.setAttribute("RTTab", msgRTTab);
		session.setAttribute("userPO", userPO);
			
		forward("EDD2320_tabla_maitenance.jsp", req, res);
	}
	
/***/
	private void procReqTabEleList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		EDD232005Message msgRTTab = null;
		JBObjList list = null;
		JBObjList bl = null;
	
		try 
		{
			bl = (JBObjList) session.getAttribute("RTTabList");
			int idx = 0;
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
			bl.setCurrentRow(idx);
			msgRTTab = (EDD232005Message) bl.getRecord();
		} catch (Exception e) {
			msgRTTab = (EDD232005Message) session.getAttribute("RTTab");
		}

		try 
		{
			mp = getMessageProcessor("EDD2320", req);
			EDD232006Message msgRTTabEle = (EDD232006Message) mp.getMessageRecord("EDD232006", user.getH01USR(), "0003");
			msgRTTabEle.setE06CMTLTDC(msgRTTab.getE05CMTCTDC());
				
			mp.sendMessage(msgRTTabEle);
			
			msgError = (ELEERRMessage) mp.receiveMessageRecord();
			
			list = mp.receiveMessageRecordList("H06FLGMAS");

			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("RTTabList", bl);
				session.setAttribute("userPO", userPO);

				forward("EDD2320_tabla_list.jsp", req, res);
			} else 
			{
				flexLog("Putting java beans into the session");
				session.setAttribute("RTTabEleList", list);
				session.setAttribute("RTTab", msgRTTab);
				session.setAttribute("userPO", userPO);
				
				forward("EDD2320_tabla_element_list.jsp", req, res);
			}
		} 
		finally {
			if (mp != null)	mp.close();
		}
	}

/***/
	private void procActionTabEle(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNewTabEle(user, req, res, session);
				break;
			case 3 : //Deletion
				userPO.setPurpose("DELETE");
				session.setAttribute("userPO", userPO);
				procActionDeleteTabEle(user, req, res, session);
				break;
			default : //Maintenance
				userPO.setPurpose("MAINTENANCE");
				session.setAttribute("userPO", userPO);
				procReqMaintenanceTabEle(user, req, res, session);
				break;
		}
	}

/***/
	private void procReqNewTabEle(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
	
		EDD232006Message msgRTTabEle = (EDD232006Message) new EDD232006Message();
		setMessageRecord(req, msgRTTabEle);

		flexLog("Putting java beans into the session");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("RTTabEle", msgRTTabEle);
		
		forward("EDD2320_tabla_element_maitenance.jsp", req, res);
	}

/***/
	private void procActionMaintenanceTabEle(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
			
		try {
			EDD232005Message msgRTTab = (EDD232005Message) new EDD232005Message();
			EDD232006Message msgRTTabEle = (EDD232006Message) new EDD232006Message();
		
			setMessageRecord(req, msgRTTab); 

			mp = getMessageProcessor("EDD2320", req);
			
			msgRTTabEle.setH06USERID(user.getH01USR());
			msgRTTabEle.setH06PROGRM("EDD2320");
			msgRTTabEle.setH06TIMSYS(getTimeStamp());
			
			if(userPO.getPurpose().equals("NEW"))
				msgRTTabEle.setH06OPECOD("0005");
			else
				msgRTTabEle.setH06OPECOD("0001");

			
			setMessageRecord(req, msgRTTabEle);
			
			msgRTTabEle.setE06CMTLTDC(msgRTTab.getE05CMTCTDC());
				
			mp.sendMessage(msgRTTabEle);

			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRTTabEle = (EDD232006Message) mp.receiveMessageRecord("EDD232006");
		
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("RTTab", msgRTTab);
				session.setAttribute("RTTabEle", msgRTTabEle);
				
				forward("EDD2320_tabla_element_maitenance.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");

				session.setAttribute("userPO", userPO);

				procReqTabEleList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}

/***/	
	private void procReqMaintenanceTabEle(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		
		EDD232005Message msgRTTab =  (EDD232005Message) session.getAttribute("RTTab");

		setMessageRecord(req, msgRTTab);
		
		JBObjList bl = (JBObjList) session.getAttribute("RTTabEleList");
		int idx = 0;
		
		try {idx = Integer.parseInt(req.getParameter("CURRCODE2").trim());} 
		catch (Exception e) {throw new ServletException(e);}
		
		bl.setCurrentRow(idx);
		
		EDD232006Message msgRTTabEle = (EDD232006Message) bl.getRecord();
		
		session.setAttribute("RTTab", msgRTTab);
		session.setAttribute("RTTabEle", msgRTTabEle);
		session.setAttribute("userPO", userPO);
		
		forward("EDD2320_tabla_element_maitenance.jsp", req, res);

	}

/***/	
	private void procActionDeleteTabEle(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		EDD232006Message msgRTTabEle =  (EDD232006Message) new EDD232006Message();
		
		try
		{
			EDD232005Message msgRTTab =  (EDD232005Message) session.getAttribute("RTTab");
			setMessageRecord(req, msgRTTab);
		
			JBObjList bl = (JBObjList) session.getAttribute("RTTabEleList");
			int idx = 0;
		
			try {idx = Integer.parseInt(req.getParameter("CURRCODE2").trim());} 
			catch (Exception e) {throw new ServletException(e);}
		
			bl.setCurrentRow(idx);
		
			msgRTTabEle = (EDD232006Message) bl.getRecord();
		
			mp = getMessageProcessor("EDD2320", req);
		
			msgRTTabEle.setH06USERID(user.getH01USR());
			msgRTTabEle.setH06PROGRM("EDD2320");
			msgRTTabEle.setH06TIMSYS(getTimeStamp());
			msgRTTabEle.setH06OPECOD("0004");
			msgRTTabEle.setE06CMTLTDC(msgRTTab.getE05CMTCTDC());
			
			mp.sendMessage(msgRTTabEle);

			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRTTabEle = (EDD232006Message) mp.receiveMessageRecord("EDD232006");

			session.setAttribute("RTTab", msgRTTab);
			session.setAttribute("RTTabEle", msgRTTabEle);
		
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqTabEleList(user, req, res, session);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqTabEleList(user, req, res, session);
			}

	} finally {
			if (mp != null)	mp.close();
		}
	}

/***/
	private void procReqDisTabList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		EDD232004Message msgRTDis = null;
		JBObjList list = null;
		JBObjList bl = null;
	
		try 
		{
			bl = (JBObjList) session.getAttribute("RTDisList");
			int idx = 0;
			idx = Integer.parseInt(req.getParameter("CURRCODE").trim());
			bl.setCurrentRow(idx);
			msgRTDis = (EDD232004Message) bl.getRecord();
		} catch (Exception e) {
			msgRTDis = (EDD232004Message) session.getAttribute("RTDis");
		}

		try 
		{
			mp = getMessageProcessor("EDD2320", req);
			EDD232004Message msgRTDisEle = (EDD232004Message) mp.getMessageRecord("EDD232004", user.getH01USR(), "0005");
			msgRTDisEle.setE04CMRDCDC(msgRTDis.getE04CMRDCDC());
				
			mp.sendMessage(msgRTDisEle);
			
			msgError = (ELEERRMessage) mp.receiveMessageRecord();
			
			list = mp.receiveMessageRecordList("H04FLGMAS");

			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("RTDisList", bl);
				session.setAttribute("userPO", userPO);

				forward("EDD2320_discounts_list.jsp", req, res);
			} else 
			{
				flexLog("Putting java beans into the session");
				session.setAttribute("RTDisTabList", list);
				session.setAttribute("RTDis", msgRTDis);
				session.setAttribute("userPO", userPO);
				
				forward("EDD2320_discounts_tabla_list.jsp", req, res);
			}
		} 
		finally {
			if (mp != null)	mp.close();
		}
	}

/***/
	private void procActionDisTab(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNewDisTab(user, req, res, session);
				break;
			case 3 : //Deletion
				userPO.setPurpose("DELETE");
				session.setAttribute("userPO", userPO);
				procActionDeleteDisTab(user, req, res, session);
				break;
			default : //Maintenance
				break;
		}
	}

/***/
	private void procReqNewDisTab(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
	
		EDD232004Message msgRTDisTab = (EDD232004Message) new EDD232004Message();
		setMessageRecord(req, msgRTDisTab);

		msgRTDisTab.setE04CMRDTDC("");
		msgRTDisTab.setE04CMRDTDG("");
		
		flexLog("Putting java beans into the session");
		session.setAttribute("error", new ELEERRMessage());
		session.setAttribute("RTDisTab", msgRTDisTab);
		
		forward("EDD2320_discounts_tabla_maitenance.jsp", req, res);
	}

/***/
	private void procActionMaintenanceDisTab(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
			
		try {
			EDD232004Message msgRTDisTab = (EDD232004Message) new EDD232004Message();
		
			mp = getMessageProcessor("EDD2320", req);
			
			msgRTDisTab.setH04USERID(user.getH01USR());
			msgRTDisTab.setH04PROGRM("EDD2320");
			msgRTDisTab.setH04TIMSYS(getTimeStamp());
			msgRTDisTab.setH04OPECOD("0007"); 
			
			setMessageRecord(req, msgRTDisTab);
			
			mp.sendMessage(msgRTDisTab);

			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRTDisTab = (EDD232004Message) mp.receiveMessageRecord("EDD232004");
		
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("RTDisTab", msgRTDisTab);
				
				forward("EDD2320_discounts_tabla_maitenance.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");

				session.setAttribute("userPO", userPO);

				procReqDisTabList(user, req, res, session);
			}
		} finally {
			if (mp != null)	mp.close();
		}
	}

/***/	
	private void procReqMaintenanceDisTab(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		
		EDD232005Message msgRTTab =  (EDD232005Message) session.getAttribute("RTTab");

		setMessageRecord(req, msgRTTab);
		
		JBObjList bl = (JBObjList) session.getAttribute("RTTabEleList");
		int idx = 0;
		
		try {idx = Integer.parseInt(req.getParameter("CURRCODE2").trim());} 
		catch (Exception e) {throw new ServletException(e);}
		
		bl.setCurrentRow(idx);
		
		EDD232006Message msgRTTabEle = (EDD232006Message) bl.getRecord();
		
		session.setAttribute("RTTab", msgRTTab);
		session.setAttribute("RTTabEle", msgRTTabEle);
		session.setAttribute("userPO", userPO);
		
		forward("EDD2320_tabla_element_maitenance.jsp", req, res);

	}

/***/	
	private void procActionDeleteDisTab(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		EDD232004Message msgRTDisTab =  (EDD232004Message) new EDD232004Message();
		
		try
		{
			EDD232004Message msgRTDis =  (EDD232004Message) session.getAttribute("RTDis");
			setMessageRecord(req, msgRTDis);
		
			JBObjList bl = (JBObjList) session.getAttribute("RTDisTabList");
			int idx = 0;
		
			try {idx = Integer.parseInt(req.getParameter("CURRCODE2").trim());} 
			catch (Exception e) {throw new ServletException(e);}
		
			bl.setCurrentRow(idx);
		
			msgRTDisTab = (EDD232004Message) bl.getRecord();
		
			mp = getMessageProcessor("EDD2320", req);
		
			msgRTDisTab.setH04USERID(user.getH01USR());
			msgRTDisTab.setH04PROGRM("EDD2320");
			msgRTDisTab.setH04TIMSYS(getTimeStamp());
			msgRTDisTab.setH04OPECOD("0008");
			msgRTDisTab.setE04CMRDCDC(msgRTDis.getE04CMRDCDC());
			
			mp.sendMessage(msgRTDisTab);

			msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgRTDisTab = (EDD232004Message) mp.receiveMessageRecord("EDD232004");

			session.setAttribute("RTDis", msgRTDis);
		
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqDisTabList(user, req, res, session);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
			
				procReqDisTabList(user, req, res, session);
			}

	} finally {
			if (mp != null)	mp.close();
		}
	}

/***/
	
//	
}
