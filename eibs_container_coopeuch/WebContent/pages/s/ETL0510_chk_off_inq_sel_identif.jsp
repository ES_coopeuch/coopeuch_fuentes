
<%@ page import = "datapro.eibs.master.Util" %>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>

<html>
<head>
<title>Consulta Documentos Varios</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "checkSel" class= "datapro.eibs.beans.ETL051001Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>


</head>
<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>
<h3 align="center">
  <% if (userPO.getOption().equals("3"))
  		 out.print("Consulta de Orden de Pago");
 	 else if (userPO.getOption().equals("4")) 	 
 	   		 out.print("Consulta de Cheques de Terceros");
 	 else if (userPO.getOption().equals("1")) 
		 	 out.print("Aprobación de Cheques Oficiales");	 
  	 else out.print("Consulta de Documentos Varios");
  %>
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="chk_off_inq_sel_identif.jsp,ETL0510"> 
</h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSETL0510" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="400">
  <INPUT TYPE=HIDDEN NAME="OPTION" VALUE="<%= userPO.getHeader10()%>">
  <h4>Informaci&oacute;n B&aacute;sica de Seleccion</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
         <tr id="trclear"> 
            <td nowrap width="20%">
              <div align="right">Identificación Beneficiario :</div>
            </td>
            <td nowrap width="35%">
               <input type="text" name="E01SELBNF" size="12" maxlength="11" value="<%= checkSel.getE01SELBNF() %>">
            </td>
            </tr>
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">N&uacute;mero Cliente Aplicante :</div>
            </td>
            <td nowrap width="35%"> 
              <input type="text" name="E01SELCUN" size="10" maxlength="9" value="<%= checkSel.getE01SELCUN() %>"  readonly onKeypress="enterInteger()">
                <a href="javascript:GetCustomer('E01SELCUN')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda" border="0" ></a>
            </td>
          </tr>            
		  <% if (userPO.getOption().equals("4")) {%>   
          <tr id="trclear"> 
            <td nowrap width="20%"> 
              <div align="right">Estado del Cheque :</div>
			
            </td>
            <td nowrap width="35%"> 
              <input type="text" name="E01SELSCH" size="2" maxlength="1" value="A" readonly>              
              <a href="javascript:GetCode('E01SELSCH','STATIC_dv_stat_Chile.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" border="0" ></a> 
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">Referencia Cheque :</div>
            </td>
            <td nowrap width="35%"> 
              <input type="text" name="E01SELNCH" size="12" maxlength="11" value="<%= checkSel.getE01SELNCH() %>"  onKeypress="enterInteger()">
            </td>
            </tr>
            <tr id="trclear"> 
            <td nowrap width="20%"> 
              <div align="right">N&uacute;mero de Cheque :</div>
            </td>
            <td nowrap width="35%"> 
              <input type="text" name="E01SELRE2" size="17" maxlength="16" value="<%= checkSel.getE01SELRE2()%>">
            </td>
            </tr>
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">Cuenta Banco :</div>
            </td>
            <td nowrap width="35%"> 
              <input type="text" name="E01SELNPR" size="13" maxlength="12" value="<%= checkSel.getE01SELNPR() %>"  onKeypress="enterInteger()">
            </td>
            </tr>
           <tr id="trclear"> 
            <td nowrap width="20%"> 
              <div align="right">Operación Origen :</div>
            </td>
            <td nowrap width="35%"> 
              <input type="text" name="E01SELDAC" size="13" maxlength="12" value="<%= checkSel.getE01SELDAC() %>"  onKeypress="enterInteger()">
            </td>
            </tr>
		  <% } else {%>   
          <tr id="trclear"> 
            <td nowrap width="20%"> 
              <div align="right">Estado del Documento :</div>
			
            </td>
            <td nowrap width="35%"> 
              <input type="text" name="E01SELSCH" size="2" maxlength="1" value="A" readonly>              
              <a href="javascript:GetCode('E01SELSCH','STATIC_dv_stat.jsp')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" border="0" ></a> 
            </td>
          </tr>
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">N&uacute;mero de Documento :</div>
            </td>
            <td nowrap width="35%"> 
              <input type="text" name="E01SELNCH" size="12" maxlength="11" value="<%= checkSel.getE01SELNCH() %>"  onKeypress="enterInteger()">
            </td>
            </tr>

		 <% }%>          

        </table>
      </td>
    </tr>
  </table>
  <div align="center"> 
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
  </form>
</body>
</html>
