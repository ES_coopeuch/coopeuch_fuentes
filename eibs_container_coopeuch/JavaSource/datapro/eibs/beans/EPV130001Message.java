package datapro.eibs.beans;

import datapro.eibs.sockets.*;
import java.io.*;
import java.math.*;

import java.util.*;

/**
* Class generated by AS/400 CRTCLASS command from EPV130001 physical file definition.
* 
* File level identifier is 1140529102659.
* Record format level identifier is 4D822397A16B8.
*/

public class EPV130001Message extends MessageRecord
{
  final static String fldnames[] = {
                                     "H01USERID",
                                     "H01PROGRM",
                                     "H01TIMSYS",
                                     "H01SCRCOD",
                                     "H01OPECOD",
                                     "H01FLGMAS",
                                     "H01FLGWK1",
                                     "H01FLGWK2",
                                     "H01FLGWK3",
                                     "E01CMPIDC",
                                     "E01CMPSTS",
                                     "E01CMPGST",
                                     "E01CMPCAY",
                                     "E01CMPCAM",
                                     "E01CMPCAD",
                                     "E01CMPCAU"
                                   };
  final static String tnames[] = {
                                   "H01USERID",
                                   "H01PROGRM",
                                   "H01TIMSYS",
                                   "H01SCRCOD",
                                   "H01OPECOD",
                                   "H01FLGMAS",
                                   "H01FLGWK1",
                                   "H01FLGWK2",
                                   "H01FLGWK3",
                                   "E01CMPIDC",
                                   "E01CMPSTS",
                                   "E01CMPGST",
                                   "E01CMPCAY",
                                   "E01CMPCAM",
                                   "E01CMPCAD",
                                   "E01CMPCAU"
                                 };
  final static String fid = "1140529102659";
  final static String rid = "4D822397A16B8";
  final static String fmtname = "EPV130001";
  final int FIELDCOUNT = 16;
  private static Hashtable tlookup = new Hashtable();
  private CharacterField fieldH01USERID = null;
  private CharacterField fieldH01PROGRM = null;
  private CharacterField fieldH01TIMSYS = null;
  private CharacterField fieldH01SCRCOD = null;
  private CharacterField fieldH01OPECOD = null;
  private CharacterField fieldH01FLGMAS = null;
  private CharacterField fieldH01FLGWK1 = null;
  private CharacterField fieldH01FLGWK2 = null;
  private CharacterField fieldH01FLGWK3 = null;
  private DecimalField fieldE01CMPIDC = null;
  private CharacterField fieldE01CMPSTS = null;
  private CharacterField fieldE01CMPGST = null;
  private DecimalField fieldE01CMPCAY = null;
  private DecimalField fieldE01CMPCAM = null;
  private DecimalField fieldE01CMPCAD = null;
  private CharacterField fieldE01CMPCAU = null;

  /**
  * Constructor for EPV130001Message.
  */
  public EPV130001Message()
  {
    createFields();
    initialize();
  }

  /**
  * Create fields for this message.
  * This method implements the abstract method in the MessageRecord superclass.
  */
  protected void createFields()
  {
    recordsize = 90;
    fileid = fid;
    recordid = rid;
    message = new byte[getByteLength()];
    formatname = fmtname;
    fieldnames = fldnames;
    tagnames = tnames;
    fields = new MessageField[FIELDCOUNT];

    fields[0] = fieldH01USERID =
    new CharacterField(message, HEADERSIZE + 0, 10, "H01USERID");
    fields[1] = fieldH01PROGRM =
    new CharacterField(message, HEADERSIZE + 10, 10, "H01PROGRM");
    fields[2] = fieldH01TIMSYS =
    new CharacterField(message, HEADERSIZE + 20, 12, "H01TIMSYS");
    fields[3] = fieldH01SCRCOD =
    new CharacterField(message, HEADERSIZE + 32, 2, "H01SCRCOD");
    fields[4] = fieldH01OPECOD =
    new CharacterField(message, HEADERSIZE + 34, 4, "H01OPECOD");
    fields[5] = fieldH01FLGMAS =
    new CharacterField(message, HEADERSIZE + 38, 1, "H01FLGMAS");
    fields[6] = fieldH01FLGWK1 =
    new CharacterField(message, HEADERSIZE + 39, 1, "H01FLGWK1");
    fields[7] = fieldH01FLGWK2 =
    new CharacterField(message, HEADERSIZE + 40, 1, "H01FLGWK2");
    fields[8] = fieldH01FLGWK3 =
    new CharacterField(message, HEADERSIZE + 41, 1, "H01FLGWK3");
    fields[9] = fieldE01CMPIDC =
    new DecimalField(message, HEADERSIZE + 42, 11, 0, "E01CMPIDC");
    fields[10] = fieldE01CMPSTS =
    new CharacterField(message, HEADERSIZE + 53, 1, "E01CMPSTS");
    fields[11] = fieldE01CMPGST =
    new CharacterField(message, HEADERSIZE + 54, 15, "E01CMPGST");
    fields[12] = fieldE01CMPCAY =
    new DecimalField(message, HEADERSIZE + 69, 5, 0, "E01CMPCAY");
    fields[13] = fieldE01CMPCAM =
    new DecimalField(message, HEADERSIZE + 74, 3, 0, "E01CMPCAM");
    fields[14] = fieldE01CMPCAD =
    new DecimalField(message, HEADERSIZE + 77, 3, 0, "E01CMPCAD");
    fields[15] = fieldE01CMPCAU =
    new CharacterField(message, HEADERSIZE + 80, 10, "E01CMPCAU");

    synchronized (tlookup)
    {
      if (tlookup.isEmpty())
      {
        for (int i = 0; i < tnames.length; i++)
          tlookup.put(tnames[i], new Integer(i));
      }
    }

    taglookup = tlookup;
  }

  /**
  * Set field H01USERID using a String value.
  */
  public void setH01USERID(String newvalue)
  {
    fieldH01USERID.setString(newvalue);
  }

  /**
  * Get value of field H01USERID as a String.
  */
  public String getH01USERID()
  {
    return fieldH01USERID.getString();
  }

  /**
  * Set field H01PROGRM using a String value.
  */
  public void setH01PROGRM(String newvalue)
  {
    fieldH01PROGRM.setString(newvalue);
  }

  /**
  * Get value of field H01PROGRM as a String.
  */
  public String getH01PROGRM()
  {
    return fieldH01PROGRM.getString();
  }

  /**
  * Set field H01TIMSYS using a String value.
  */
  public void setH01TIMSYS(String newvalue)
  {
    fieldH01TIMSYS.setString(newvalue);
  }

  /**
  * Get value of field H01TIMSYS as a String.
  */
  public String getH01TIMSYS()
  {
    return fieldH01TIMSYS.getString();
  }

  /**
  * Set field H01SCRCOD using a String value.
  */
  public void setH01SCRCOD(String newvalue)
  {
    fieldH01SCRCOD.setString(newvalue);
  }

  /**
  * Get value of field H01SCRCOD as a String.
  */
  public String getH01SCRCOD()
  {
    return fieldH01SCRCOD.getString();
  }

  /**
  * Set field H01OPECOD using a String value.
  */
  public void setH01OPECOD(String newvalue)
  {
    fieldH01OPECOD.setString(newvalue);
  }

  /**
  * Get value of field H01OPECOD as a String.
  */
  public String getH01OPECOD()
  {
    return fieldH01OPECOD.getString();
  }

  /**
  * Set field H01FLGMAS using a String value.
  */
  public void setH01FLGMAS(String newvalue)
  {
    fieldH01FLGMAS.setString(newvalue);
  }

  /**
  * Get value of field H01FLGMAS as a String.
  */
  public String getH01FLGMAS()
  {
    return fieldH01FLGMAS.getString();
  }

  /**
  * Set field H01FLGWK1 using a String value.
  */
  public void setH01FLGWK1(String newvalue)
  {
    fieldH01FLGWK1.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK1 as a String.
  */
  public String getH01FLGWK1()
  {
    return fieldH01FLGWK1.getString();
  }

  /**
  * Set field H01FLGWK2 using a String value.
  */
  public void setH01FLGWK2(String newvalue)
  {
    fieldH01FLGWK2.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK2 as a String.
  */
  public String getH01FLGWK2()
  {
    return fieldH01FLGWK2.getString();
  }

  /**
  * Set field H01FLGWK3 using a String value.
  */
  public void setH01FLGWK3(String newvalue)
  {
    fieldH01FLGWK3.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK3 as a String.
  */
  public String getH01FLGWK3()
  {
    return fieldH01FLGWK3.getString();
  }

  /**
  * Set field E01CMPIDC using a String value.
  */
  public void setE01CMPIDC(String newvalue)
  {
    fieldE01CMPIDC.setString(newvalue);
  }

  /**
  * Get value of field E01CMPIDC as a String.
  */
  public String getE01CMPIDC()
  {
    return fieldE01CMPIDC.getString();
  }

  /**
  * Set numeric field E01CMPIDC using a BigDecimal value.
  */
  public void setE01CMPIDC(BigDecimal newvalue)
  {
    fieldE01CMPIDC.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01CMPIDC as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01CMPIDC()
  {
    return fieldE01CMPIDC.getBigDecimal();
  }

  /**
  * Set field E01CMPSTS using a String value.
  */
  public void setE01CMPSTS(String newvalue)
  {
    fieldE01CMPSTS.setString(newvalue);
  }

  /**
  * Get value of field E01CMPSTS as a String.
  */
  public String getE01CMPSTS()
  {
    return fieldE01CMPSTS.getString();
  }

  /**
  * Set field E01CMPGST using a String value.
  */
  public void setE01CMPGST(String newvalue)
  {
    fieldE01CMPGST.setString(newvalue);
  }

  /**
  * Get value of field E01CMPGST as a String.
  */
  public String getE01CMPGST()
  {
    return fieldE01CMPGST.getString();
  }

  /**
  * Set field E01CMPCAY using a String value.
  */
  public void setE01CMPCAY(String newvalue)
  {
    fieldE01CMPCAY.setString(newvalue);
  }

  /**
  * Get value of field E01CMPCAY as a String.
  */
  public String getE01CMPCAY()
  {
    return fieldE01CMPCAY.getString();
  }

  /**
  * Set numeric field E01CMPCAY using a BigDecimal value.
  */
  public void setE01CMPCAY(BigDecimal newvalue)
  {
    fieldE01CMPCAY.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01CMPCAY as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01CMPCAY()
  {
    return fieldE01CMPCAY.getBigDecimal();
  }

  /**
  * Set field E01CMPCAM using a String value.
  */
  public void setE01CMPCAM(String newvalue)
  {
    fieldE01CMPCAM.setString(newvalue);
  }

  /**
  * Get value of field E01CMPCAM as a String.
  */
  public String getE01CMPCAM()
  {
    return fieldE01CMPCAM.getString();
  }

  /**
  * Set numeric field E01CMPCAM using a BigDecimal value.
  */
  public void setE01CMPCAM(BigDecimal newvalue)
  {
    fieldE01CMPCAM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01CMPCAM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01CMPCAM()
  {
    return fieldE01CMPCAM.getBigDecimal();
  }

  /**
  * Set field E01CMPCAD using a String value.
  */
  public void setE01CMPCAD(String newvalue)
  {
    fieldE01CMPCAD.setString(newvalue);
  }

  /**
  * Get value of field E01CMPCAD as a String.
  */
  public String getE01CMPCAD()
  {
    return fieldE01CMPCAD.getString();
  }

  /**
  * Set numeric field E01CMPCAD using a BigDecimal value.
  */
  public void setE01CMPCAD(BigDecimal newvalue)
  {
    fieldE01CMPCAD.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01CMPCAD as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01CMPCAD()
  {
    return fieldE01CMPCAD.getBigDecimal();
  }

  /**
  * Set field E01CMPCAU using a String value.
  */
  public void setE01CMPCAU(String newvalue)
  {
    fieldE01CMPCAU.setString(newvalue);
  }

  /**
  * Get value of field E01CMPCAU as a String.
  */
  public String getE01CMPCAU()
  {
    return fieldE01CMPCAU.getString();
  }

}
