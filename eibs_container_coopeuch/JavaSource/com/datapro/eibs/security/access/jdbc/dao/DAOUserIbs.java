package com.datapro.eibs.security.access.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.apache.log4j.Category;

import com.datapro.eibs.constants.Security;
import com.datapro.eibs.exception.DAOException;
import com.datapro.eibs.exception.UserNotFoundException;
import com.datapro.eibs.security.access.jdbc.bean.CNTRLBTH;
import com.datapro.generic.access.DAOSuper;
import com.datapro.generic.beanutil.BeanList;
import com.datapro.generic.beanutil.BeanParser;
import com.datapro.generic.beanutil.BeanSql;

/**
 * @author ogarcia
 *
 */
public class DAOUserIbs extends DAOSuper {

	private static Category cat =
		Category.getInstance(DAOUserIbs.class.getName());

	/**
	 * Constructor for DAOUserIbs.
	 * @param dbid
	 */
	public DAOUserIbs(String dbid) {
		super(dbid);
	}

	/**
	 * Constructor for DAOUserIbs.
	 */
	public DAOUserIbs() {
		super();
	}

	/**
	 * Constructor for DAOUserIbs.
	 * @param connection
	 */
	public DAOUserIbs(Connection connection) {
		super(connection);
	}

	/**
	 * Get user's group id.
	 */
	public String getGroupID(String uid)
		throws UserNotFoundException, DAOException {

		String gid = "";

		try {
			String sql = "SELECT BTHKEY,BTHF03 FROM CNTRLBTH WHERE BTHKEY=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				gid = rs.getString("BTHF03").trim();
			} else {
				throw new UserNotFoundException(
					"User not found exception(uid:" + uid + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		if (gid.equals("")) {
			gid = Security.USER_GROUP_UNASSIGNED;
		}

		return gid;

	}

	/**
	 * Get user record.
	 */
	public CNTRLBTH getUser(String uid)
		throws UserNotFoundException, DAOException {

		CNTRLBTH bean = null;

		try {
			String sql = "SELECT * FROM CNTRLBTH WHERE BTHKEY=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			if (rs.next()) {
				bean = new CNTRLBTH();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
			} else {
				throw new UserNotFoundException(
					"User not found exception(uid:" + uid + ").");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return bean;

	}

	/**
	 * Get users by group id.
	 */
	public BeanList getUsersByGroup(String gid)
		throws DAOException {

		BeanList list = null;

		try {
			String sql = "SELECT * FROM CNTRLBTH WHERE BTHF03=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, gid);
			ResultSet rs = ps.executeQuery();
			ResultSetMetaData md = rs.getMetaData();
			list = new BeanList();
			while (rs.next()) {
				CNTRLBTH bean = new CNTRLBTH();
				BeanParser bp = new BeanParser(bean);
				bp.parseResultSet(md, rs);
				list.addRow(bean);
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return list;

	}

	/**
	 * Delete user record.
	 */
	public int delete(String uid) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM CNTRLBTH WHERE BTHKEY=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

	/**
	 * Insert user record.
	 */
	public int insert(CNTRLBTH bean) throws DAOException {

		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.insertPrepareStatement();
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

	/**
	 * Update user record.
	 */
	public int update(CNTRLBTH bean) throws DAOException {

		int rt;

		try {
			BeanSql bs = new BeanSql(bean);
			String sql = bs.updatePrepareStatement() + " WHERE BTHKEY=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			bs.parsePrepareStatement(ps);
			ps.setString(bs.getNextParamIndex(), bean.getBTHKEY());
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

	/**
	 * Insert user record.
	 */
	public int insert(String uid, String gid) throws DAOException {

		int rt;

		try {
			String sql = "INSERT INTO CNTRLBTH (BTHKEY,BTHF03) VALUES(?,?)";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, uid);
			ps.setString(2, gid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

	/**
	 * Delete all ibs user for a group.
	 */
	public int deleteByGroup(String gid) throws DAOException {

		int rt;

		try {
			String sql = "DELETE FROM CNTRLBTH WHERE BTHF03=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, gid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;
	}

	/*
	 * Change group id for a given user record.
	 */
	public int update(String uid, String gid) throws DAOException {

		int rt;

		try {
			String sql = "UPDATE CNTRLBTH SET BTHF03=? WHERE BTHKEY=?";
			PreparedStatement ps = cnx.prepareStatement(sql);
			ps.setString(1, gid);
			ps.setString(2, uid);
			rt = ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			cat.error(e);
			throw new DAOException(e.toString());
		}

		return rt;

	}

}
