<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>


<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<html>
<head>
<title>Entity</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="entity" class="datapro.eibs.beans.ESD400001Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 
 <script language="JavaScript">
 	var jsIsCustomer = "";
 
 <% 
 	boolean isApprovalInquiry = userPO.getPurpose().equals("APPROVAL_INQ");
 	boolean isInquiry = userPO.getPurpose().equals("INQUIRY");
 	boolean isReadOnly = isApprovalInquiry || isInquiry;
 	boolean isCustomer = !entity.getField("E01RCN").getString().trim().equals("0");  
    String country = entity.getField("E01INT").toString().trim();    	
 
 	if (isApprovalInquiry) {%>
		<% if ( userPO.getOption().equals("CLIENT_P") ) { %>
		     builtNewMenu(client_ap_personal_opt);
         <% } else {  %>
		     builtNewMenu(client_ap_corp_opt);
         <% } %>
   <% } else if (isInquiry) { %>
	    <% if ( userPO.getOption().equals("CLIENT_P") ) { %>
		     builtNewMenu(client_inq_personal_opt);
         <% } else {  %>
		     builtNewMenu(client_inq_corp_opt);
         <% } %>	 
   <%} else {%>
         <% if ( userPO.getOption().equals("CLIENT_P") ) { %>
		     builtNewMenu(client_personal_opt);
         <% } else {  %>
		     builtNewMenu(client_corp_opt);
         <% } %>
   <% } %> 
   <%
   
	 String title = "Direcciones";
	 String indtitle = "Direcci&oacute;n Numero";
	 if ( entity.getE01RTP().equals("2") ) {
	 	title = "Accionistas";
	 	indtitle = "Accionista Numero";
	 }
	 if ( entity.getE01RTP().equals("3") ) {
	 	title = "Junta Directiva";
	 	indtitle = "Director Numero";
	 }
	 if ( entity.getE01RTP().equals("4") ) {
	 	title = "Beneficiarios";
	 	indtitle = "Beneficiario Numero";
	 }
	 if ( entity.getE01RTP().equals("5") ) {
	 	title = "Representante Legal";
	 	indtitle = "Representante Numero";
	 }
	 if ( entity.getE01RTP().equals("C") ) {
          if ( userPO.getOption().equals("CLIENT_P") ) { 	
		 	title = "Grupo Familiar";
		 	indtitle = "Familiar Numero";
		  }
	      else {	
		 	title = "Contactos";
		 	indtitle = "Contacto Numero";
		  }
	 }
	%>
 
  function showEntityFields(view){
		if(view){
			document.getElementById('entity').style.display='none';
			document.getElementById('address').style.display='none';
			document.getElementById('customer').style.display='block';
			document.forms[0].E01MA1.readOnly = true;			
			document.forms[0].E01BNI.readOnly = true;			
		} else {
			document.getElementById('entity').style.display='block';
			document.getElementById('address').style.display='block';
			document.getElementById('customer').style.display='none';
			document.forms[0].E01MA1.readOnly = false;
			document.forms[0].E01BNI.readOnly = false;
		}
  }
  
  
  
  

  
function goSend() {

	var formLength= document.forms[0].elements.length;

	for(n=0;n<formLength;n++) 
	{
     	var elementName= document.forms[0].elements[n].name;
      	if(elementName == "E01FL1")
      	{
			if (document.forms[0].elements[n].checked == true) 
			{
			    vigente=document.forms[0].elements[n].value;
				break;
			}
      	}
    }
    
    if(vigente=="Y")
	<%	 if ( entity.getE01RTP().equals("5") ) { %>
		msg='Representante Legal y su direcci�n quedaran registrados como Vigente';    
	<%	 } else { %>
		msg='La direcci�n quedara registrada como Vigente';    

	<%	 } %>
    else
	<%	 if ( entity.getE01RTP().equals("5") ) { %>
		msg='Representante Legal y su direcci�n quedaran registrados como NO Vigente';    
	<%	 } else { %>
		msg='La direcci�n quedara registrada como NO Vigente';
	<%	 } %>		
 	if(confirm(msg))
		document.forms[0].submit();
}
 </SCRIPT>  
 
<%  
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
        isReadOnly=false;
    }
    out.println("<SCRIPT> initMenu(); </SCRIPT>");  
%>


<H3 align="center"><%= title %> <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="entity_maintenance.jsp, ESD4000"></H3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4000" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01MAN"  value="<%= entity.getE01MAN().trim()%>">
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Cliente: 
              </td>
             <td nowrap width="12%" align="left">
      			<%= userPO.getHeader1()%>
             </td>
             <td nowrap width="6%" align="right">ID:  
             </td>
             <td nowrap width="14%" align="left">
      			<%= userPO.getHeader2()%>
             </td>
             <td nowrap width="8%" align="right"> Nombre: 
               </td>
             <td nowrap width="50%"align="left">
      			<%= userPO.getHeader3()%>
             </td>
        </tr>
        </table>
      </td>
    </tr>
  </table>
  
  <h4> <%= indtitle + " : " + entity.getE01MAN() %> </h4>
    
   <% if (!entity.getE01RTP().equals("1")) { //Others %>

	  <table  class="tableinfo">
	    <tr bordercolor="#FFFFFF"> 
	      <td nowrap> 
	        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id="trdark"> 
       		<td nowrap width="40%"> 
              <div align="right">N�mero de Cliente : </div>
           </td>
            <td nowrap width="60%" colspan="3"> 
				<eibsinput:help name="entity" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" property="E01RCN" modified="F01RCN"
					fn_param_one="E01RCN" fn_param_two="E01MA1" fn_param_three="E01BNI"   readonly="<%=isReadOnly %>"/>
           </td>
          </tr>
         <tr id="trclear"> 
           <td nowrap width="40%"> 
             <div align="right">Nombre :</div>
           </td>
           <td nowrap width="60%" colspan="3">
		    <%if  (!isReadOnly) { %>
			 <eibsinput:text name="entity" property="E01MA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" required="true"  readonly="<%=isReadOnly || isCustomer %>"/>                
			<% } else { %>
			 <eibsinput:text name="entity" property="E01MA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>"  readonly="<%=isReadOnly || isCustomer %>"/>                
			<% } %>
		   </td>
         </tr> 
         <tr id="trdark"> 
           <td nowrap width="40%"> 
              <div align="right">Identificaci�n :</div>
           </td>
           <td nowrap width="60%" colspan="3">
			  <eibsinput:text name="entity" property="E01BNI" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>"  readonly="<%=isReadOnly || isCustomer %>"/>                
				Tipo :
              <eibsinput:cnofc name="entity" property="E01TID"  flag="34" fn_code="E01TID" readonly="<%=isReadOnly %>"/>
				Pais :
              <eibsinput:cnofc name="entity" property="E01PID"  flag="03" fn_code="E01PID" readonly="<%=isReadOnly %>"/>
		   </td>
          </tr> 
          
           <tr id="trclear">
                <td nowrap width="40%"> 
                    <div align="right">Fecha Nacimiento :</div>
                </td>
                <td nowrap width="20%" > 
			    <%if  (!isReadOnly) { %>
	               <eibsinput:date name="entity" required="true" fn_year="E01D2Y" fn_month="E01D2M" fn_day="E01D2D" readonly="<%=isReadOnly || isCustomer %>"/>
				<% } else { %>
	               <eibsinput:date name="entity"  fn_year="E01D2Y" fn_month="E01D2M" fn_day="E01D2D" readonly="<%=isReadOnly || isCustomer %>"/>
				<% } %>
                </td>
                <td nowrap width="10%"> 
                    <div align="right">Sexo :</div>
                </td>
               <td nowrap width="30%" colspan="3"> 
 			       <p> 
                    <input type="radio" name="E01BSX" <%=isReadOnly?"disabled":""%> value="F" <%if (entity.getE01BSX().equals("F")) out.print("checked"); %>>
                    Femenino 
                    <input type="radio" name="E01BSX" <%=isReadOnly?"disabled":""%> value="M" <%if (entity.getE01BSX().equals("M")) out.print("checked"); %>>
                    Masculino 
				    <%if  (!isReadOnly) { %>
		                 <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" > 
					<% } %>
                    </p>                    
               </td>
           </tr>    

           <tr id="trdark">
                <td nowrap width="40%"> 
                    <div align="right">Estado Civil :</div>
                </td>
               <td nowrap width="60%" colspan="3"> 
                  <select name="E01BMS" <%=isReadOnly?"disabled":""%>>
                    <option value=" " <% if (!(entity.getE01BMS().equals("1")||entity.getE01BMS().equals("2") || entity.getE01BMS().equals("3")||entity.getE01BMS().equals("4")||entity.getE01BMS().equals("5"))) out.print("selected"); %>> 
                    </option>
                    <% if (currUser.getE01INT().equals("07")) { %>
                    <option value="1" <% if (entity.getE01BMS().equals("1")) out.print("selected"); %>>Soltero(a)</option>
                    <option value="2" <% if (entity.getE01BMS().equals("2")) out.print("selected"); %>>Casado(a)</option>                   
                    <%} else if (currUser.getE01INT().equals("18")) { %>
                    <option value="1" <% if (entity.getE01BMS().equals("1")) out.print("selected"); %>>Soltero(a)</option>
                    <option value="2" <% if (entity.getE01BMS().equals("2")) out.print("selected"); %>>Casado(a) - Separacion de Bienes</option>                   
                    <option value="3" <% if (entity.getE01BMS().equals("3")) out.print("selected"); %>>Casado(a) - Sociedad Conyugal</option>
                    <option value="4" <% if (entity.getE01BMS().equals("4")) out.print("selected"); %>>Casado(a) - Participacion</option>
                    <option value="5" <% if (entity.getE01BMS().equals("5")) out.print("selected"); %>>Viudo(a)</option>
                    <option value="6" <% if (entity.getE01BMS().equals("6")) out.print("selected"); %>>Separado(a)</option>
                    <option value="7" <% if (entity.getE01BMS().equals("7")) out.print("selected"); %>>Otro</option>
					<%} else { %>
                    <option value="1" <% if (entity.getE01BMS().equals("1")) out.print("selected"); %>>Soltero(a)</option>
                    <option value="2" <% if (entity.getE01BMS().equals("2")) out.print("selected"); %>>Casado(a)</option>                   
                    <option value="3" <% if (entity.getE01BMS().equals("3")) out.print("selected"); %>>Divorciado(a)</option>
                    <option value="4" <% if (entity.getE01BMS().equals("4")) out.print("selected"); %>>Viudo(a)</option>
                    <option value="5" <% if (entity.getE01BMS().equals("5")) out.print("selected"); %>>Otro</option>
                    <option value="6" <% if (entity.getE01BMS().equals("6")) out.print("selected"); %>>Uni�n Libre</option>
					<% } %>
                  </select>
			    <%if  (!isReadOnly) { %>
	                 <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" > 
				<% } %>
                </td>
            </tr>    

           <tr id="trclear">
                <td nowrap width="40%"> 
                    <div align="right">Trabaja :</div>
                </td>
                <td nowrap width="20%"> 
                    <input type="radio" name="E01FL2" <%=isReadOnly?"disabled":""%> value="Y" <%if (entity.getE01FL2().equals("Y")) out.print("checked"); %>>
                    Si 
                    <input type="radio" name="E01FL2" <%=isReadOnly?"disabled":""%> value="N" <%if (entity.getE01FL2().equals("N")) out.print("checked"); %>>
                    No 
				    <%if  (!isReadOnly) { %>
		                 <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" > 
					<% } %>
                </td>
                <td nowrap width="10%"> 
                    <div align="right">Dependiente :</div>
                </td>
                <td nowrap width="30%"> 
                    <input type="radio" name="E01FL3" <%=isReadOnly?"disabled":""%> value="Y" <%if (entity.getE01FL3().equals("Y")) out.print("checked"); %>>
                    Si 
                    <input type="radio" name="E01FL3" <%=isReadOnly?"disabled":""%> value="N" <%if (entity.getE01FL3().equals("N")) out.print("checked"); %>>
                    No 
				    <%if  (!isReadOnly) { %>
		                 <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" > 
					<% } %>
                </td>
           </tr>  
     
             <tr id="trdark"> 
                <td nowrap width="40%"> 
                   <div align="right">Telefono :</div>
               </td>
               <td nowrap width="60%" colspan="3">         
				  <eibsinput:text name="entity" property="E01HPN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PHONE %>" required="false"  readonly="<%=isReadOnly%>"/>                
               </td>
            </tr>   

             <tr id="trclear"> 
                <td nowrap width="40%"> 
                   <div align="right">Email :</div>
               </td>
               <td nowrap width="60%" colspan="3">         
			  <eibsinput:text name="entity" property="E01IAD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_EMAIL %>" required="false"  readonly="<%=isReadOnly%>"/>                
               </td>
            </tr>   

             <tr id="trdark"> 
                <td nowrap width="40%"> 
                   <div align="right">Profesi&oacute;n/Ocupaci&oacute;n :</div>
               </td>
               <td nowrap width="60%" colspan="3">         
                 <eibsinput:cnofc name="entity" property="E01EDL" required="false" flag="49" fn_code="E01EDL" fn_description="D01EDL" readonly="<%=isReadOnly %>"/>
                 <eibsinput:text property="D01EDL" name="entity" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" readonly="true"/>
               </td>
            </tr>   
             <tr id="trclear"> 
                <td nowrap width="40%"> 
                   <div align="right">Cargo :</div>
               </td>
               <td nowrap width="60%" colspan="3">         
                 <eibsinput:cnofc name="entity" property="E01UC1" required="false" flag="32" fn_code="E01UC1" fn_description="D01UC1" readonly="<%=isReadOnly %>"/>
                 <eibsinput:text property="D01UC1" name="entity" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" readonly="true"/>
               </td>
            </tr>  
   <% if (entity.getE01RTP().equals("2")) { %>
             <tr id="trdark"> 
                <td nowrap width="40%"> 
                   <div align="right">Tipo Beneficiario :</div>
               </td>
               <td nowrap width="60%" colspan="3">         
                 <eibsinput:cnofc name="entity" property="E01TBE" required="false" flag="BE" fn_code="E01TBE" fn_description="E01BEN" readonly="<%=isReadOnly %>"/>
                 <%if  (!isReadOnly) { %>
                 <eibsinput:text property="E01BEN" name="entity" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>"  required="true" readonly="true"/>
                 <%} else {%>
                 <eibsinput:text property="E01BEN" name="entity" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>"  required="false" readonly="true"/>
                 <%} %>
                 
               
               </td>
            </tr> 

          <tr id="trclear"> 
       		<td nowrap width="40%"> 
              <div align="right">% Participaci�n de la PJ Declarante : </div>
           </td>
            <td nowrap width="60%" colspan="3"> 
				<input type="text" name="E01PPA" maxlength="5" size="7" value="<%=entity.getE01PPA() %>" onkeypress=" enterDecimal()" class="TXTRIGHT" <%if  (isReadOnly) {out.print("readonly");} %> >
				<%if  (!isReadOnly) { %>
				<img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" > 		
				<%}%>

			</td>
          </tr>
          <% } %>
	        <% if (entity.getE01RTP().equals("C") && userPO.getOption().equals("CLIENT_P") ) { //Grupo Familiar %>
             <tr id="trdark"> 
                <td nowrap width="40%"> 
                   <div align="right">Tipo Relacion :</div>
               </td>
               <td nowrap width="60%" colspan="3">         
                  <select name="E01RTY" <%=isReadOnly?"disabled":""%>>
                    <option value=" " <% if (!(entity.getE01RTY().equals("1") ||
                                               entity.getE01RTY().equals("2") || 
                                               entity.getE01RTY().equals("3") ||
                                               entity.getE01RTY().equals("4") ||
                                               entity.getE01RTY().equals("5") ||
                                               entity.getE01RTY().equals("6") || 
                                               entity.getE01RTY().equals("7") ||
                                               entity.getE01RTY().equals("8") ||
                                               entity.getE01RTY().equals("9") ||
                                               entity.getE01RTY().equals("A") || 
                                               entity.getE01RTY().equals("B") ||
                                               entity.getE01RTY().equals("C") ||
                                               entity.getE01RTY().equals("D") ||                                                                                                                                             
                                               entity.getE01RTY().equals("E"))) out.print("selected"); %>></option>
                    <option value="1" <% if (entity.getE01RTY().equals("1")) out.print("selected"); %>>Padre</option>                   
                    <option value="2" <% if (entity.getE01RTY().equals("2")) out.print("selected"); %>>Madre</option>
                    <option value="3" <% if (entity.getE01RTY().equals("3")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="4" <% if (entity.getE01RTY().equals("4")) out.print("selected"); %>>Abuelo(a)</option>
                    <option value="5" <% if (entity.getE01RTY().equals("5")) out.print("selected"); %>>Tio(a)</option>
                    <option value="6" <% if (entity.getE01RTY().equals("6")) out.print("selected"); %>>Hijo(a)</option>
                    <option value="7" <% if (entity.getE01RTY().equals("7")) out.print("selected"); %>>Conyuge</option>                   
                    <option value="8" <% if (entity.getE01RTY().equals("8")) out.print("selected"); %>>Primo(a)</option>
                    <option value="9" <% if (entity.getE01RTY().equals("9")) out.print("selected"); %>>Cunado(a)</option>
                    <option value="A" <% if (entity.getE01RTY().equals("A")) out.print("selected"); %>>Nieto(a)</option>
                    <option value="B" <% if (entity.getE01RTY().equals("B")) out.print("selected"); %>>Sobrino(a)</option>
                    <option value="C" <% if (entity.getE01RTY().equals("C")) out.print("selected"); %>>Suegro(a)</option>  
                    <option value="D" <% if (entity.getE01RTY().equals("D")) out.print("selected"); %>>Nuera</option>
                    <option value="E" <% if (entity.getE01RTY().equals("E")) out.print("selected"); %>>Yerno</option>                                        
                  </select>
			    <%if  (!isReadOnly) { %>
	                 <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" > 
				<% } %>
               </td>
            </tr>   
		   <%} %>
    <%         if (  entity.getE01RTP().equals("5") ) {%>
     

            <tr id="trdark"> 
              <td nowrap width="40%"> 
                <div align="right">Estado Vigente :</div>
              </td>
              <td nowrap width="60%">  
	              <input type="radio"  name="E01FL1" <%=isReadOnly?"disabled":""%> value="Y" <%if (!entity.getE01FL1().equals("N")) out.print("checked"); %>  >S�
	              <input type="radio"  name="E01FL1" <%=isReadOnly?"disabled":""%> value="N" <%if (entity.getE01FL1().equals("N")) out.print("checked"); %>  >No
                </td>
            </tr>

<%         	 }%>
            </table>
	      </td>
	    </tr>
	  </table>
	        
   <%} %>

  <% if (!entity.getE01RTP().equals("1")) { //Others %>
  <h4>  Direcci�n  </h4>
   <%} %>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
       
        <% if (entity.getE01RTP().equals("1")) { //Direcciones %>
          <tr id="trclear"> 
            <td nowrap width="40%"> 
              <div align="right">Nombre :</div>
            </td>
            <td nowrap width="60%" colspan="3"> 
                <eibsinput:text name="entity" property="E01MA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" required="false" readonly="<%=isReadOnly %>"/>
            </td>
          </tr> 
        <% } %>  
                
	        <% String pageName = "ESD4000_address_template_country" + country + ".jsp"; %>
		        <jsp:include page="<%= pageName %>" flush="true">
	        	<jsp:param name="readOnly" value="<%=isReadOnly%>" />
		        </jsp:include>
         
        </table>
      </td>
    </tr>
  </table>
  
  <br>
  
     <%if  (!isReadOnly) { %>
       <div align="center"> 
			<%	 if ( entity.getE01RTP().equals("4") || entity.getE01RTP().equals("C")  ) { %>
           		<input id="EIBSBTN" type=Submit name="Submit" value="Enviar" >
			<%	 } else { %>
           		<input id="EIBSBTN" type=button name="Submit" value="Enviar" onClick="goSend()">
			<%	 } %>		
       </div>
       <% if (!userPO.getHeader10().equals("1")){ //Others%>
       <script language="JavaScript">
			showEntityFields(<%= isCustomer %>);	
		</script>
		<% } %> 
     <% } %>  
  </form>
</body>
</HTML>
