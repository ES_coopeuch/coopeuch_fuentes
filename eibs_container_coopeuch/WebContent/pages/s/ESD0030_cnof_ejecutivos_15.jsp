<html>
<head>
<title>Codigos de Referencia</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

</head>

<jsp:useBean id="refCodes" class="datapro.eibs.beans.ESD003002Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<body>

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
builtHPopUp();

function showPopUp(opth,field,bank,ccy,field1,field2,opcod) {
   init(opth,field,bank,ccy,field1,field2,opcod);
   showPopupHelp();
   }
   
</SCRIPT>

<% 
    if ( !error.getERRNUM().equals("0")  ) {
        out.println("<SCRIPT Language=\"Javascript\">");
        error.setERRNUM("0");
        out.println("       showErrors()");
        out.println("</SCRIPT>");
    }
    
%>

<% if (refCodes.getE02CNOCFL().trim().equals("15") ) { %> 
<H3 align="center">C&oacute;digos de Referencia del Sistema - Ejecutivos de Cuenta<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cnof_ejecutivos_15.jsp, ESD0030"></H3>
<% } else { %>
<H3 align="center">C&oacute;digos de Referencia del Sistema - Ejecutivos de Venta<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cnof_ejecutivos_15.jsp, ESD0030"></H3>
<% } %>

<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.params.JSESD0030" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
   <INPUT TYPE=HIDDEN NAME="E02CNOBNK" value="<%= currUser.getE01UBK()%>">
  <h4>Informaci&oacute;n B&aacute;sica</h4>

 <% int row = 0;%>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%"> 
              <div align="right">Clasificaci&oacute;n :</div>
            </td>
            <td nowrap> 
              <div align="left"> 
                <input type="text" name="E02CNOCFL" size="3" maxlength="2" value="<%= refCodes.getE02CNOCFL().trim()%>" readonly>
              </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="23"> 
              <div align="right">C&oacute;digo Ejecutivo :</div>
            </td>
            <td nowrap height="23"> 
              <div align="left"> 
				<% if ( !userPO.getPurpose().equals("NEW") ) { %>
                <input type="text" name="E02CNORCD" size="6" maxlength="4" value="<%= refCodes.getE02CNORCD().trim()%>" readonly>
				<% } else { %>
                <input type="text" name="E02CNORCD" size="6" maxlength="4" value="<%= refCodes.getE02CNORCD().trim()%>">
				<% } %>
 
                <input type="text" name="E02CNODSC" size="50" maxlength="45" value="<%= refCodes.getE02CNODSC().trim()%>" >
              </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Estatus:</div>
            </td>
            <td nowrap height="19"> 
               <select name="E02CNOF07">
                    <option value=" " <% if (!(refCodes.getE02CNOF07().equals("1")||refCodes.getE02CNOF07().equals("2"))) out.print("selected"); %>> 
                    </option>
                    <option value="1" <% if (refCodes.getE02CNOF07().equals("1")) out.print("selected"); %>>Vigente</option>
                    <option value="2" <% if (refCodes.getE02CNOF07().equals("2")) out.print("selected"); %>>No Vigente</option>                   
                  </select>
	        </td>	             
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Tipo de Banca:</div>
            </td>
            <td nowrap height="19"> 
              <div align="left"> 
                <input type="text" name="E02CNOCPC" size="5" maxlength="4" value="<%= refCodes.getE02CNOCPC().trim()%>">
                <a href="javascript:GetCodeCNOFC('E02CNOCPC','TB')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
			  </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="16%" height="19"> 
              <div align="right">Tipo de Ejecutivo:</div>
            </td>
            <td nowrap height="19"> 
              <div align="left"> 
                <input type="text" name="E02CNOTYP" size="5" maxlength="4" value="<%= refCodes.getE02CNOTYP().trim()%>">
                <a href="javascript:GetCodeCNOFC('E02CNOTYP','ET')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
			  </div>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="39%"> 
               <div align="right">Direcci&oacute;n E-Mail :</div>
             </td>
             <td nowrap width="61%">  
 				<input type="text" name="E02CNOEML" size="60" maxlength="60" value="<%= refCodes.getE02CNOEML().trim()%>" >
               </td>
           </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="39%"> 
               <div align="right">Identificación :</div>
             </td>
             <td nowrap width="61%">  
 				<input type="text" name="E02CNORUT" size="28" maxlength="25" value="<%= refCodes.getE02CNORUT().trim()%>" >
               </td>
           </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
				<td nowrap width="16%" height="19" align="right">Telefono:</td> 
				<td nowrap height="19">
					<input type="text" name="E02CNOPHN" size="16" maxlength="15" value="<%= refCodes.getE02CNOPHN().trim()%>" onKeyPress="enterInteger()">
				</td>
			</tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
				<td nowrap width="16%" height="19" align="right">Extension:</td> 
				<td nowrap height="19">
					<input type="text" name="E02CNOPXT" size="5" maxlength="4" value="<%= refCodes.getE02CNOPXT().trim()%>" onKeyPress="enterInteger()">
				</td>
			</tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
				<td nowrap width="16%" height="19" align="right">Centro de Costos:</td> 
				<td nowrap height="19">
	              <input type="text" name="E02CNOCCN" size="8" maxlength="8" value="<%= refCodes.getE02CNOCCN().trim()%>">
    	          <a href="javascript:GetCostCenter('E02CNOCCN','01')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Centros de Costo" align="absmiddle" border="0"  ></a> 
				</td>
			</tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
				<td nowrap width="16%" height="19" align="right">Supervisor:</td> 
				<td nowrap height="19">
					<input type="text" name="E02CNOSUP" size="5" maxlength="4" value="<%= refCodes.getE02CNOSUP().trim()%>" >
					<% if (refCodes.getE02CNOCFL().trim().equals("15") ) { %> 
	                <a href="javascript:GetCodeCNOFC('E02CNOSUP','15')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
					<% } else { %>
	                <a href="javascript:GetCodeCNOFC('E02CNOSUP','CA')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
					<% } %>
				</td>
			</tr>
			
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
				<td nowrap width="16%" height="19" align="right">Banco:</td> 
				<td nowrap height="19">
					<input type="text" name="E02CNOBNK" size="3" maxlength="2" value="<%= refCodes.getE02CNOBNK().trim()%>" >
				</td>
			</tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
				<td nowrap width="16%" height="19" align="right">Agencia:</td> 
				<td nowrap height="19">
					<input type="text" name="E02CNOBRN" size="5" maxlength="4" value="<%= refCodes.getE02CNOBRN().trim()%>" onKeyPress="enterInteger()">
					<A href="javascript:GetBranch('E02CNOBRN','')"><IMG	src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" border="0"></A>
				</td>
          </tr>
		  <!--  IFRS -->
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
				<td nowrap width="16%" height="19" align="right">Comisionable:</td> 
				<td nowrap height="19">
		  			<select name="E02CNOF04" > 
    	              <option value=" " <% if (refCodes.getE02CNOF04().equals("")) out.print("selected"); %>>Seleccione</option>
    	              <option value="S" <% if (refCodes.getE02CNOF04().equals("S")) out.print("selected"); %>>SI</option>
        	          <option value="N" <% if (refCodes.getE02CNOF04().equals("N")) out.print("selected"); %>>NO</option>                   
                	</select>
				</td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
				<td nowrap width="16%" height="19" align="right">Grupo de Comisiones:</td> 
				<td nowrap height="19">
					<input type="text" name="E02CNOACD" size="5" maxlength="4" value="<%= refCodes.getE02CNOACD().trim()%>" >
	                <a href="javascript:GetCodeCNOFC('E02CNOACD','GC')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
				</td>
			</tr>

		  
		  
 	





		</table>
      </td>
    </tr>
  </table>

  <p align="center"> 
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>

 </form>
</body>
</html>
