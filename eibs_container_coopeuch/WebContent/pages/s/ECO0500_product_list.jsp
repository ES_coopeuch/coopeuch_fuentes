<!-- Hecho por Alonso Arana ------Datapro-----16/05/2014 -->
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ECO050001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Solicitud de Ex-Convenios</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ConvenioList" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="cabezera_convenio" class="datapro.eibs.beans.ECO050001Message" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class="datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

   $(function(){
				
					$("#radio_key").attr("checked", false);
                
				});


var maxLength =300;
function testLength(ta) {
  if(ta.value.length > maxLength) {
    ta.value = ta.value.substring(0, maxLength);
  }
}



function goAction3(op) {

if(op=='2014'){
 	 	document.forms[0].SCREEN.value = op;
		document.forms[0].action = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEXEDD0000?SCREEN=2014";
	 	document.forms[0].submit();

}

	}





</SCRIPT>  

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Solicitud de Ex-Convenios<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="product_list.jsp,ECO0500"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECO0500">
<input type="hidden" name="SCREEN" value="102">





<table class="tableinfo" border="0">
	<tr>
		<td nowrap>
		<table cellspacing="0" cellpadding="2" width="100%" border="0"
			class="tbhead">
			<tr id="trdark">
				<td nowrap width="14%">
				<div align="right"><b>Cliente :</b></div>
				</td>
				<td nowrap width="9%">
				<div align="left"><input type="text" name="E01COHCUN" readonly size="10"
					maxlength="9" readonly value="<%=cabezera_convenio.getE01COHCUN() %>"></div>
				</td>
				<td nowrap width="12%">
				<div align="right"><b>Nombre :</b></div>
				</td>
				<td nowrap>
				<div align="left"><input type="text" name="E02NA12" size="48"
					maxlength="45" readonly value="<%=cabezera_convenio.getE01CUSSHN()%> "></div>
				</td>
				<td nowrap>
				<div align="right"><b>Rut  Cliente : </b></div>
				</td>
				<td nowrap><b> <input type="text" name="E02PRO2" size="10"
					maxlength="4" readonly value="<%=cabezera_convenio.getE01CUSIDN()%>"> </b></td>
			</tr>
			<tr id="trdark">
				<td nowrap width="14%">
				<div align="right"><b>Convenio :</b></div>
				</td>
				<td nowrap width="9%">
				<div align="left"><input type="text" name="E01COHCDE" size="13"
					maxlength="12" value="<%=cabezera_convenio.getE01COHCDE() %>" readonly>
				</div>
				</td>
				<td nowrap width="12%">
				<div align="right"><b>Descripci�n :</b></div>
				</td>
				<td nowrap width="33%">
				<div align="left"><b> <input type="text" name="E02NA122" size="60"
					maxlength="45" readonly value="<%=cabezera_convenio.getE01DESCDE() %>"> </b>
				</div>
				</td>
				<td nowrap width="11%">
				<div align="right"><b>Fecha Ingreso : </b></div>
				</td>
				<td nowrap width="21%">
			
			
				<b><input type="text" name="E02PRO20" size="10" maxlength="4"
					readonly
					value='<%out.print(cabezera_convenio.getE01CUFA4D()+"/"+cabezera_convenio.getE01CUFA4M()+"/"+cabezera_convenio.getE01CUFA4Y()); %>'></b></td>
			</tr>

			
			<tr id="trdark">
				<td nowrap width="14%">
				<div align="right"><b></b></div>
				</td>
				<td nowrap width="9%"><b> </b>
				<div align="left">
				</div>
				</td>
				<td nowrap width="12%">
				</td>
				<td nowrap width="11%">
				<div align="right"><b> </b></div>
				</td>
				<td nowrap width="21%">
				<div align="left"><b> </b>
				</div>
				</td>
			</tr>
		
			
		</table>
		</td>
	</tr>
</table>
  


<table class="tbenter" width="100%">
	<tr></tr>
	<tr>
				
			
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction3('2014')"><b>Atr�s</b></a >
		</td>
	</tr>
	<tr></tr>
</table>


<%
	if (ConvenioList.getNoResult()) {
%>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>



<%
	
	
	} else {
%>

	
	 <h4>Productos</h4>


<table id="headTable"  width="" align="center">
		<tr id="trdark">
		
			<th align="center" nowrap width="200">Operaci�n</th>
			<th align="center" nowrap width="300">Tipo</th>
			<th align="center" nowrap width="100">Producto</th>
					<th align="center" nowrap width="100">Descripci�n</th>
				<th align="center" nowrap width="200">Estado</th>
					<th align="center" nowrap width="100">Dias mora</th>
		
			
		</tr>
		
		<%
			ConvenioList.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				String CtaAux = "";
				while (ConvenioList.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
			ECO050001Message pvprd = (ECO050001Message)	ConvenioList.getRecord();
		
		
		if( !pvprd.getE01CODACC().equals(CtaAux))
		{
			CtaAux = pvprd.getE01CODACC();
		%>
		<tr>
		
	
		<td nowrap align="center"><%=pvprd.getE01CODACC() %></td>
		<td nowrap align="left"><%=pvprd.getE01CODTYP() %></td>
 	  	<td nowrap align="center"><%=pvprd.getE01CODPRD()%></td>
 	  	  	<td nowrap align="center"><%=pvprd.getE01DESPRD()%></td>
 	  	<td nowrap align="center"><%=pvprd.getE01ESTADO() %></td>		
 	  	<td nowrap align="left"><%=pvprd.getE01DLCMOR() %></td>		
		</tr>
		<%
				}

			}
		%>
	</table>
	
	
	
	
	<h4>Desvinculaci�n</h4>
<table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
              <td nowrap width="5%" align="right"></td>
 
             <td nowrap width="10%" align="right">Medio de Pago : 
              </td>
             <td nowrap width="10%" align="left">
				<select name="MEDIOPAGO"  >
	  		    	<option value="1" "selected">CAJA</option>
              </select>
             </td>  

             <td nowrap width="10%" align="right">Motivo : 
              </td>
             <td nowrap width="10%" align="left">
	  		
	  		
	  		  <% boolean flag = false; %><select name="E01COHMOT"  >
	  		             <option value=" " <%if (cabezera_convenio.getE01COHMOT().equals(" ")) { flag = true; out.print("selected"); }%>>SELECCIONAR</option>
                       <option value="1" <%if (cabezera_convenio.getE01COHMOT().equals("1")) { flag = true; out.print("selected"); }%>>DESVINCULACION</option>
                <option value="2" <%if (cabezera_convenio.getE01COHMOT().equals("2")) { flag = true; out.print("selected"); }%> >RENUNCIA</option>
             <option value="3" <%if (cabezera_convenio.getE01COHMOT().equals("3")) { flag = true; out.print("selected"); }%> >FALLECIMIENTO</option>
             <option value="4" <%if (cabezera_convenio.getE01COHMOT().equals("4")) { flag = true; out.print("selected"); }%> >JUBILACION</option>
              </select>
             </td>  
             
              <td nowrap width="5%" align="right"></td>
 
               <td nowrap width="10%" align="right">Observaciones  : 
               </td>
             <td nowrap width="50%"align="left">

 
 <textarea
  onchange="testLength(this)"
  onkeyup="testLength(this)"
  onpaste="testLength(this)"
  cols="60" rows="5" maxlength="300" name="E01OBSERV" ><%= cabezera_convenio.getE01OBSERV().trim()%></textarea>
 
             </td>  
         </tr>
         

      

             
            
        </table>
      </td>
    </tr>
  </table>
  
    <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    
  </p>
	
<%
	}
%>
</form>
</body>
</html>
