package datapro.eibs.beans;

import datapro.eibs.sockets.*;
import java.io.*;
import java.math.*;

import java.util.*;

/**
* Class generated by AS/400 CRTCLASS command from ECO200101 physical file definition.
* 
* File level identifier is 1170511123149.
* Record format level identifier is 4FB1934A16524.
*/

public class ECO200101Message extends MessageRecord
{
  final static String fldnames[] = {
                                     "H01USERID",
                                     "H01PROGRM",
                                     "H01TIMSYS",
                                     "H01SCRCOD",
                                     "H01OPECOD",
                                     "H01FLGMAS",
                                     "H01FLGWK1",
                                     "H01FLGWK2",
                                     "H01FLGWK3",
                                     "E01PERAA",
                                     "E01PERMM",
                                     "E01AMTCNV",
                                     "E01FILLE1",
                                     "E01FILLE2",
                                     "E01FILLE3",
                                     "E01FILLE4",
                                     "E01FILLE5",
                                     "E01FILLE6"
                                   };
  final static String tnames[] = {
                                   "H01USERID",
                                   "H01PROGRM",
                                   "H01TIMSYS",
                                   "H01SCRCOD",
                                   "H01OPECOD",
                                   "H01FLGMAS",
                                   "H01FLGWK1",
                                   "H01FLGWK2",
                                   "H01FLGWK3",
                                   "E01PERAA",
                                   "E01PERMM",
                                   "E01AMTCNV",
                                   "E01FILLE1",
                                   "E01FILLE2",
                                   "E01FILLE3",
                                   "E01FILLE4",
                                   "E01FILLE5",
                                   "E01FILLE6"
                                 };
  final static String fid = "1170511123149";
  final static String rid = "4FB1934A16524";
  final static String fmtname = "ECO200101";
  final int FIELDCOUNT = 18;
  private static Hashtable tlookup = new Hashtable();
  private CharacterField fieldH01USERID = null;
  private CharacterField fieldH01PROGRM = null;
  private CharacterField fieldH01TIMSYS = null;
  private CharacterField fieldH01SCRCOD = null;
  private CharacterField fieldH01OPECOD = null;
  private CharacterField fieldH01FLGMAS = null;
  private CharacterField fieldH01FLGWK1 = null;
  private CharacterField fieldH01FLGWK2 = null;
  private CharacterField fieldH01FLGWK3 = null;
  private DecimalField fieldE01PERAA = null;
  private DecimalField fieldE01PERMM = null;
  private DecimalField fieldE01AMTCNV = null;
  private DecimalField fieldE01FILLE1 = null;
  private DecimalField fieldE01FILLE2 = null;
  private DecimalField fieldE01FILLE3 = null;
  private CharacterField fieldE01FILLE4 = null;
  private CharacterField fieldE01FILLE5 = null;
  private CharacterField fieldE01FILLE6 = null;

  /**
  * Constructor for ECO200101Message.
  */
  public ECO200101Message()
  {
    createFields();
    initialize();
  }

  /**
  * Create fields for this message.
  * This method implements the abstract method in the MessageRecord superclass.
  */
  protected void createFields()
  {
    recordsize = 265;
    fileid = fid;
    recordid = rid;
    message = new byte[getByteLength()];
    formatname = fmtname;
    fieldnames = fldnames;
    tagnames = tnames;
    fields = new MessageField[FIELDCOUNT];

    fields[0] = fieldH01USERID =
    new CharacterField(message, HEADERSIZE + 0, 10, "H01USERID");
    fields[1] = fieldH01PROGRM =
    new CharacterField(message, HEADERSIZE + 10, 10, "H01PROGRM");
    fields[2] = fieldH01TIMSYS =
    new CharacterField(message, HEADERSIZE + 20, 12, "H01TIMSYS");
    fields[3] = fieldH01SCRCOD =
    new CharacterField(message, HEADERSIZE + 32, 2, "H01SCRCOD");
    fields[4] = fieldH01OPECOD =
    new CharacterField(message, HEADERSIZE + 34, 4, "H01OPECOD");
    fields[5] = fieldH01FLGMAS =
    new CharacterField(message, HEADERSIZE + 38, 1, "H01FLGMAS");
    fields[6] = fieldH01FLGWK1 =
    new CharacterField(message, HEADERSIZE + 39, 1, "H01FLGWK1");
    fields[7] = fieldH01FLGWK2 =
    new CharacterField(message, HEADERSIZE + 40, 1, "H01FLGWK2");
    fields[8] = fieldH01FLGWK3 =
    new CharacterField(message, HEADERSIZE + 41, 1, "H01FLGWK3");
    fields[9] = fieldE01PERAA =
    new DecimalField(message, HEADERSIZE + 42, 5, 0, "E01PERAA");
    fields[10] = fieldE01PERMM =
    new DecimalField(message, HEADERSIZE + 47, 3, 0, "E01PERMM");
    fields[11] = fieldE01AMTCNV =
    new DecimalField(message, HEADERSIZE + 50, 17, 2, "E01AMTCNV");
    fields[12] = fieldE01FILLE1 =
    new DecimalField(message, HEADERSIZE + 67, 16, 0, "E01FILLE1");
    fields[13] = fieldE01FILLE2 =
    new DecimalField(message, HEADERSIZE + 83, 16, 0, "E01FILLE2");
    fields[14] = fieldE01FILLE3 =
    new DecimalField(message, HEADERSIZE + 99, 16, 0, "E01FILLE3");
    fields[15] = fieldE01FILLE4 =
    new CharacterField(message, HEADERSIZE + 115, 50, "E01FILLE4");
    fields[16] = fieldE01FILLE5 =
    new CharacterField(message, HEADERSIZE + 165, 50, "E01FILLE5");
    fields[17] = fieldE01FILLE6 =
    new CharacterField(message, HEADERSIZE + 215, 50, "E01FILLE6");

    synchronized (tlookup)
    {
      if (tlookup.isEmpty())
      {
        for (int i = 0; i < tnames.length; i++)
          tlookup.put(tnames[i], new Integer(i));
      }
    }

    taglookup = tlookup;
  }

  /**
  * Set field H01USERID using a String value.
  */
  public void setH01USERID(String newvalue)
  {
    fieldH01USERID.setString(newvalue);
  }

  /**
  * Get value of field H01USERID as a String.
  */
  public String getH01USERID()
  {
    return fieldH01USERID.getString();
  }

  /**
  * Set field H01PROGRM using a String value.
  */
  public void setH01PROGRM(String newvalue)
  {
    fieldH01PROGRM.setString(newvalue);
  }

  /**
  * Get value of field H01PROGRM as a String.
  */
  public String getH01PROGRM()
  {
    return fieldH01PROGRM.getString();
  }

  /**
  * Set field H01TIMSYS using a String value.
  */
  public void setH01TIMSYS(String newvalue)
  {
    fieldH01TIMSYS.setString(newvalue);
  }

  /**
  * Get value of field H01TIMSYS as a String.
  */
  public String getH01TIMSYS()
  {
    return fieldH01TIMSYS.getString();
  }

  /**
  * Set field H01SCRCOD using a String value.
  */
  public void setH01SCRCOD(String newvalue)
  {
    fieldH01SCRCOD.setString(newvalue);
  }

  /**
  * Get value of field H01SCRCOD as a String.
  */
  public String getH01SCRCOD()
  {
    return fieldH01SCRCOD.getString();
  }

  /**
  * Set field H01OPECOD using a String value.
  */
  public void setH01OPECOD(String newvalue)
  {
    fieldH01OPECOD.setString(newvalue);
  }

  /**
  * Get value of field H01OPECOD as a String.
  */
  public String getH01OPECOD()
  {
    return fieldH01OPECOD.getString();
  }

  /**
  * Set field H01FLGMAS using a String value.
  */
  public void setH01FLGMAS(String newvalue)
  {
    fieldH01FLGMAS.setString(newvalue);
  }

  /**
  * Get value of field H01FLGMAS as a String.
  */
  public String getH01FLGMAS()
  {
    return fieldH01FLGMAS.getString();
  }

  /**
  * Set field H01FLGWK1 using a String value.
  */
  public void setH01FLGWK1(String newvalue)
  {
    fieldH01FLGWK1.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK1 as a String.
  */
  public String getH01FLGWK1()
  {
    return fieldH01FLGWK1.getString();
  }

  /**
  * Set field H01FLGWK2 using a String value.
  */
  public void setH01FLGWK2(String newvalue)
  {
    fieldH01FLGWK2.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK2 as a String.
  */
  public String getH01FLGWK2()
  {
    return fieldH01FLGWK2.getString();
  }

  /**
  * Set field H01FLGWK3 using a String value.
  */
  public void setH01FLGWK3(String newvalue)
  {
    fieldH01FLGWK3.setString(newvalue);
  }

  /**
  * Get value of field H01FLGWK3 as a String.
  */
  public String getH01FLGWK3()
  {
    return fieldH01FLGWK3.getString();
  }

  /**
  * Set field E01PERAA using a String value.
  */
  public void setE01PERAA(String newvalue)
  {
    fieldE01PERAA.setString(newvalue);
  }

  /**
  * Get value of field E01PERAA as a String.
  */
  public String getE01PERAA()
  {
    return fieldE01PERAA.getString();
  }

  /**
  * Set numeric field E01PERAA using a BigDecimal value.
  */
  public void setE01PERAA(BigDecimal newvalue)
  {
    fieldE01PERAA.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01PERAA as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01PERAA()
  {
    return fieldE01PERAA.getBigDecimal();
  }

  /**
  * Set field E01PERMM using a String value.
  */
  public void setE01PERMM(String newvalue)
  {
    fieldE01PERMM.setString(newvalue);
  }

  /**
  * Get value of field E01PERMM as a String.
  */
  public String getE01PERMM()
  {
    return fieldE01PERMM.getString();
  }

  /**
  * Set numeric field E01PERMM using a BigDecimal value.
  */
  public void setE01PERMM(BigDecimal newvalue)
  {
    fieldE01PERMM.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01PERMM as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01PERMM()
  {
    return fieldE01PERMM.getBigDecimal();
  }

  /**
  * Set field E01AMTCNV using a String value.
  */
  public void setE01AMTCNV(String newvalue)
  {
    fieldE01AMTCNV.setString(newvalue);
  }

  /**
  * Get value of field E01AMTCNV as a String.
  */
  public String getE01AMTCNV()
  {
    return fieldE01AMTCNV.getString();
  }

  /**
  * Set numeric field E01AMTCNV using a BigDecimal value.
  */
  public void setE01AMTCNV(BigDecimal newvalue)
  {
    fieldE01AMTCNV.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01AMTCNV as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01AMTCNV()
  {
    return fieldE01AMTCNV.getBigDecimal();
  }

  /**
  * Set field E01FILLE1 using a String value.
  */
  public void setE01FILLE1(String newvalue)
  {
    fieldE01FILLE1.setString(newvalue);
  }

  /**
  * Get value of field E01FILLE1 as a String.
  */
  public String getE01FILLE1()
  {
    return fieldE01FILLE1.getString();
  }

  /**
  * Set numeric field E01FILLE1 using a BigDecimal value.
  */
  public void setE01FILLE1(BigDecimal newvalue)
  {
    fieldE01FILLE1.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01FILLE1 as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01FILLE1()
  {
    return fieldE01FILLE1.getBigDecimal();
  }

  /**
  * Set field E01FILLE2 using a String value.
  */
  public void setE01FILLE2(String newvalue)
  {
    fieldE01FILLE2.setString(newvalue);
  }

  /**
  * Get value of field E01FILLE2 as a String.
  */
  public String getE01FILLE2()
  {
    return fieldE01FILLE2.getString();
  }

  /**
  * Set numeric field E01FILLE2 using a BigDecimal value.
  */
  public void setE01FILLE2(BigDecimal newvalue)
  {
    fieldE01FILLE2.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01FILLE2 as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01FILLE2()
  {
    return fieldE01FILLE2.getBigDecimal();
  }

  /**
  * Set field E01FILLE3 using a String value.
  */
  public void setE01FILLE3(String newvalue)
  {
    fieldE01FILLE3.setString(newvalue);
  }

  /**
  * Get value of field E01FILLE3 as a String.
  */
  public String getE01FILLE3()
  {
    return fieldE01FILLE3.getString();
  }

  /**
  * Set numeric field E01FILLE3 using a BigDecimal value.
  */
  public void setE01FILLE3(BigDecimal newvalue)
  {
    fieldE01FILLE3.setBigDecimal(newvalue);
  }

  /**
  * Return value of numeric field E01FILLE3 as a BigDecimal.
  */
  public BigDecimal getBigDecimalE01FILLE3()
  {
    return fieldE01FILLE3.getBigDecimal();
  }

  /**
  * Set field E01FILLE4 using a String value.
  */
  public void setE01FILLE4(String newvalue)
  {
    fieldE01FILLE4.setString(newvalue);
  }

  /**
  * Get value of field E01FILLE4 as a String.
  */
  public String getE01FILLE4()
  {
    return fieldE01FILLE4.getString();
  }

  /**
  * Set field E01FILLE5 using a String value.
  */
  public void setE01FILLE5(String newvalue)
  {
    fieldE01FILLE5.setString(newvalue);
  }

  /**
  * Get value of field E01FILLE5 as a String.
  */
  public String getE01FILLE5()
  {
    return fieldE01FILLE5.getString();
  }

  /**
  * Set field E01FILLE6 using a String value.
  */
  public void setE01FILLE6(String newvalue)
  {
    fieldE01FILLE6.setString(newvalue);
  }

  /**
  * Get value of field E01FILLE6 as a String.
  */
  public String getE01FILLE6()
  {
    return fieldE01FILLE6.getString();
  }

}
