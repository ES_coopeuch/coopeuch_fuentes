// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 12-02-2015 16:59:36
// Home Page:  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   JSERC1000.java

package datapro.eibs.products;

import com.jspsmart.upload.*;
import com.jspsmart.upload.File;

import datapro.eibs.beans.*;
import datapro.eibs.master.*;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;
import java.io.*;
import java.math.BigDecimal;
import java.util.PropertyResourceBundle;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class JSERC1000 extends JSEIBSServlet
{

    public JSERC1000()
    {
        config = null;
    }

    public void init(ServletConfig config)
        throws ServletException
    {
        super.init(config);
        this.config = config;
    }

    protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen)
        throws ServletException, IOException
    {
        screen = 200;
        try
        {
            screen = Integer.parseInt(req.getParameter("SCREEN"));
        }
        catch(Exception exception) { }
        switch(screen)
        {
        case 100: // 'd'
            procReqBankReconciliationEnter(user, req, res, session);
            break;

        case 200: 
            procActionBankReconciliationEnter(user, req, res, session);
            break;

        case 300: 
            procReqBankProcessReconciliationEnter(user, req, res, session);
            break;

        case 400: 
            procActionBankProcessReconciliationEnter(user, req, res, session);
            break;

        default:
            forward(SuperServlet.devPage, req, res);
            break;
        }
    }

    protected void procReqBankReconciliationEnter(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        try
        {
            flexLog("About to call Page: ERC1000_bank_reconciliation_enter.jsp");
            forward("ERC1000_bank_reconciliation_enter.jsp", req, res);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
        }
    }

    protected void procReqBankProcessReconciliationEnter(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses)
        throws ServletException, IOException
    {
        try
        {
            flexLog("About to call Page: ERC1000_bank_reconciliation_enter.jsp");
            forward("ERC1000_bank_process_reconciliation_enter.jsp", req, res);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
        }
    }

    protected void procActionBankReconciliationEnter(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session)
        throws ServletException, IOException
    {
        UserPos userPO;
        MessageProcessor mp;
        boolean ok;
        userPO = (UserPos)session.getAttribute("userPO");
        mp = null;
        ELEERRMessage msgError = null;
        ok = false;
        SmartUpload mySmartUpload = new SmartUpload();
        File myFile = null;
        ELEERRMessage msgError1;
        try
        {
            mySmartUpload.initialize(config, req, res);
            mySmartUpload.upload();
            myFile = mySmartUpload.getFiles().getFile(0);
            if(myFile.getSize() > 0)
            {
                mp = getMessageProcessor("ERC1000", req);
                ERC100001Message msg = (ERC100001Message)mp.getMessageRecord("ERC100001");
                msg.setH01USERID(user.getH01USR());
                msg.setH01OPECOD("0002");
                msg.setH01TIMSYS(getTimeStamp());
                msg.setH01SCRCOD("01");
                String E01BRMEID = mySmartUpload.getRequest().getParameter("E01BRMEID");
                String E01BRMCTA = mySmartUpload.getRequest().getParameter("E01BRMCTA");
                msg.setE01BRMEID(E01BRMEID);
                msg.setE01BRMCTA(E01BRMCTA);
                mp.sendMessage(msg);
                msgError1 = (ELEERRMessage)mp.receiveMessageRecord();
                msg = (ERC100001Message)mp.receiveMessageRecord();
                session.setAttribute("bank", msg);
                if(!mp.hasError(msgError1))
                {
                    String fileName = msg.getE01BRMFNM();
                    byte bd[] = new byte[myFile.getSize()];
                    for(int i = 0; i < myFile.getSize(); i++)
                        bd[i] = myFile.getBinaryData(i);

                    InputStream input = new ByteArrayInputStream(bd);
                    String userid = ServiceLocator.getSlInfo().getString("ftp.cnx.userid.eibs-server");
                    String password = ServiceLocator.getSlInfo().getString("ftp.cnx.password.eibs-server");
                    FTPWrapper ftp = new FTPStdWrapper(JSEIBSProp.getHostIP(), userid, password, "");
                    if(ftp.open())
                    {
                        if(ftp.cdRemotePath((new StringBuilder(String.valueOf(userid.substring(0, 3).toUpperCase()))).append("HIFILES").toString()))
                        {
                            ftp.setFileType(0);
                            ftp.upload(input, fileName);
                            ok = true;
                        }
                    } else
                    {
                        msgError1 = new ELEERRMessage();
                        msgError1.setERRNUM("1");
                        msgError1.setERNU01("01");
                        msgError1.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");
                    }
                }
            } else
            {
                msgError1 = new ELEERRMessage();
                msgError1.setERRNUM(new BigDecimal(1));
                msgError1.setERDS01("Archivo Vacio...");
            }
        }
        catch(Exception e)
        {
            String className = e.getClass().getName();
            String description = e.getMessage() != null ? e.getMessage() : "Exception General";
            msgError1 = new ELEERRMessage();
            msgError1.setERRNUM("3");
            msgError1.setERNU01("01");
            msgError1.setERDS01(className);
            msgError1.setERNU02("02");
            msgError1.setERDS02(description.length() <= 70 ? description : description.substring(0, 70));
            msgError1.setERNU03("03");
            msgError1.setERDS03("Para mas informacion revizar los archivos de log.");
            e.printStackTrace();
        }
       try{ if(ok)
        {
            ERC100001Message msg = (ERC100001Message)mp.getMessageRecord("ERC100001");
            msg.setH01USERID(user.getH01USR());
            msg.setH01OPECOD("0005");
            msg.setH01TIMSYS(getTimeStamp());
            msg.setH01SCRCOD("01");
            ERC100001Message msg2 = (ERC100001Message)session.getAttribute("bank");
            msg.setE01BRMCTA(msg2.getE01BRMCTA());
            msg.setE01BRMEID(msg2.getE01BRMEID());
            mp.sendMessage(msg);
            ELEERRMessage msgError2 = (ELEERRMessage)mp.receiveMessageRecord();
            msg = (ERC100001Message)mp.receiveMessageRecord();
            session.setAttribute("userPO", userPO);
            session.setAttribute("bank", msg);
            if(!mp.hasError(msgError2))
            {
                forward("ERC1000_bank_process_reconciliation_process.jsp", req, res);
            } else
            {
                session.setAttribute("error", msgError2);
                forward("ERC1000_bank_reconciliation_enter.jsp", req, res);
            }
        } else
        {
            session.setAttribute("error", msgError1);
            forward("ERC1000_bank_reconciliation_enter.jsp", req, res);
        }
    }finally{
        if(mp != null)
            mp.close();
    }}

    protected void procActionBankProcessReconciliationEnter(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session)
        throws ServletException, IOException
    {
        UserPos userPO;
        MessageProcessor mp;
        userPO = (UserPos)session.getAttribute("userPO");
        mp = null;
        try{mp = getMessageProcessor("ERC1000", req);
        ERC100001Message msg = (ERC100001Message)mp.getMessageRecord("ERC100001");
        msg.setH01USERID(user.getH01USR());
        msg.setH01OPECOD("0005");
        msg.setH01TIMSYS(getTimeStamp());
        msg.setH01SCRCOD("01");
        setMessageRecord(req, msg);
        mp.sendMessage(msg);
        ELEERRMessage msgError = (ELEERRMessage)mp.receiveMessageRecord();
        msg = (ERC100001Message)mp.receiveMessageRecord();
        session.setAttribute("userPO", userPO);
        session.setAttribute("bank", msg);
        if(!mp.hasError(msgError))
        {
            forward("ERC1000_bank_process_reconciliation_process.jsp", req, res);
        } else
        {
            session.setAttribute("error", msgError);
            forward("ERC1000_bank_process_reconciliation_enter.jsp", req, res);
        }
    }finally{
        if(mp != null)
            mp.close();
    }    }

    private static final long serialVersionUID = 0x4a9661ffeeaa46e2L;
    protected static final int R_BANK_ENTER = 100;
    protected static final int A_BANK_ENTER = 200;
    protected static final int R_BANK_PROCESS_ENTER = 300;
    protected static final int A_BANK_PROCESS_ENTER = 400;
    private ServletConfig config;
}
