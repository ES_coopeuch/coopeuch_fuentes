<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@page import="com.datapro.constants.EibsFields"%>

<%@page import="datapro.eibs.sockets.MessageRecord"%>
<%@page import="datapro.eibs.beans.ESD400001Message"%>
<html>
<head>  
  
<title>Direcciones</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "ESD400001List" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script language="JavaScript">


   <% if (userPO.getPurpose().equals("APPROVAL_INQ")) {%>
		<% if ( userPO.getOption().equals("CLIENT_P") ) { %>
		     builtNewMenu(client_ap_personal_opt);
         <% } else {  %>
		     builtNewMenu(client_ap_corp_opt);
         <% } %>
   <% } else { %>

   <% if (userPO.getPurpose().equals("INQUIRY")) { %>
	    <% if ( userPO.getOption().equals("CLIENT_P") ) { %>
		     builtNewMenu(client_inq_personal_opt);
         <% } else {  %>
		     builtNewMenu(client_inq_corp_opt);
         <% } %>	 
   <%} else {%>
         <% if ( userPO.getOption().equals("CLIENT_P") ) { %>
		     builtNewMenu(client_personal_opt);
         <% } else {  %>
		     builtNewMenu(client_corp_opt);
         <% } %>
   <% } %> 
   <% } %>  
     

function goNew() {
	document.forms[0].SCREEN.value="300";
	document.forms[0].submit();
}

function goMant() {
	isCheck();
	if (!ok) {
		alert("Por Favor Seleccione Registro!");
		return;	 
	}
	document.forms[0].SCREEN.value="500";
	document.forms[0].submit();
}

function goAsiPri() {
	isCheck();
	if (!ok) {
		alert("Por Favor Seleccione Registro!");
		return;	 
	}
	
	if(confirm("Esta seguro de realizar la Asignación ?"))
	{
		document.forms[0].ASIVIG.value="1";
		document.forms[0].SCREEN.value="1600";
		document.forms[0].submit();
	}	
}


function goInquiry() {
    isCheck();
	if (!ok) {
		alert("Por Favor Seleccione Registro!");
		return;	 
	}
	var row = document.forms[0].actRow.value;
	var pg = "";
	pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4000?SCREEN=900&ROW="+row;  
	CenterWindow(pg,600,500,2);
}

function goApproval() {
    isCheck();
	if (!ok) {
		alert("Por Favor Seleccione Registro!");
		return;	 
	}
	var row = document.forms[0].actRow.value;
    var pg = "";
	pg = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4000?SCREEN=500&ROW="+row;
	CenterWindow(pg,600,500,2);	
}

function goDelete() {

    isCheck();
	if ( !ok ) {
		alert("Por favor seleccione el registro!!!");
		return;	 
	}
	document.forms[0].SCREEN.value="700";
	document.forms[0].submit();
}

function isCheck() {
	var formLength= document.forms[0].elements.length;
   	ok = false;
	for(n=0;n<formLength;n++) {
     	var elementName= document.forms[0].elements[n].name;
      	if(elementName == "ROW") {
			if (document.forms[0].elements[n].checked == true) {
			    document.forms[0].actRow.value = document.forms[0].elements[n].value;
				ok = true;
				break;
			}
      	}
    }
}

<%  
	 String title = "Direcciones";
	 if ( userPO.getHeader10().equals("2") ) {
	 	title = " Accionistas";
	 }
	 if ( userPO.getHeader10().equals("3") ) {
	 	title = "Junta Directiva";
	 }
	 if ( userPO.getHeader10().equals("4") ) {
	 	title = "Beneficiarios";
	 }
	 if ( userPO.getHeader10().equals("5") ) {
	 	title = "Representante Legal";
	 }
	 if ( userPO.getHeader10().equals("C") ) {
          if ( userPO.getOption().equals("CLIENT_P") ) { 	
		 	title = "Grupo Familiar";
		  }
	      else {	
		 	title = "Contactos";
		  }
	 }
%>

</SCRIPT>  

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
    out.println("<SCRIPT> initMenu(); </SCRIPT>");
%>

</head>

<BODY>
<h3 align="center"> <%= title %><img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="entity_list.jsp, ESD4000"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4000" >
  <p> 
	<input type=HIDDEN name="SCREEN" value="800">
    <input type=HIDDEN name="TOTROWS" value="0">
    <input type=HIDDEN name="opt" value="1">
	<INPUT TYPE=HIDDEN name="actRow" value="0">
	<INPUT TYPE=HIDDEN name="NEXTROWS" value="0">
	<INPUT TYPE=HIDDEN name="CURRROWS" value="0">
	<INPUT TYPE=HIDDEN name="FromRecord" value="0"> 
	<INPUT TYPE=HIDDEN name="num" value="0"> 
	<INPUT TYPE=HIDDEN name="ASIVIG" value="0"> 
  </p>

 <table class="tableinfo">
  <tr > 
    <td nowrap> 
      <table cellspacing="0" cellpadding="2" width="100%" class="tbhead" bgcolor="#FFFFFF" bordercolor="#FFFFFF" bordercolorlight="#FFFFFF" bordercolordark="#FFFFFF"  align="center">
        <tr>
             <td nowrap width="10%" align="right"> Cliente: 
               </td>
          <td nowrap width="12%" align="left">
      			<%= userPO.getHeader1()%>
          </td>
            <td nowrap width="6%" align="right">ID:  
            </td>
          <td nowrap width="14%" align="left">
      			<%= userPO.getHeader2()%>
          </td>
            <td nowrap width="8%" align="right"> Nombre: 
               </td>
          <td nowrap width="50%"align="left">
      			<%= userPO.getHeader3()%>
          </td>
        </tr>
      </table>
    </td>
  </tr>
 </table>

 
  <p> 
 <%
	if ( ESD400001List.getNoResult() ) {
 %>
  </p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
          <table class="tbenter" width=100% align=center>
             <%if  (userPO.getPurpose().equals("MAINTENANCE")) { %>
            
               <tr>
                   <td class=TDBKG width="30%"> 
                     <div align="center"><a href="javascript:goNew()"><b>Crear</b></a></div>              
                   </td>
                   <td class=TDBKG width="30%"> 
                     <div align="center"><a href="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080?SCREEN=1"><b>Regresar</b></a></div>
                   </td>
               </tr>
            <%} else {%>
                  <%if  (userPO.getPurpose().equals("INQUIRY")) { %>
                    <tr>
                      <td class=TDBKG width="30%"> 
                        <div align="center"><a href="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080?SCREEN=1"><b>Regresar</b></a></div>
                      </td>
                    </tr>
                   <%} else {%> 
                       <tr>
                         <td class=TDBKG width="30%"> 
                           <div align="center"><a href="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080A?SCREEN=1"><b>Regresar</b></a></div>
                         </td>
                       </tr>
                   <%} %>  
            <%} %>	    
               
          </table>
          <p>&nbsp;</p>
          
        </div>

	  </TD>
	</TR>
    </TABLE>
	
  <%  
		}
	else {
%> <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }

%> 
  <p> 
          
  <table class="tbenter" width=100% align=center>
      <%if  (userPO.getPurpose().equals("MAINTENANCE")) { %>
   
        <tr> 
          <td class=TDBKG width="33%"> 
		     <div align="center"><a href="javascript:goNew()"><b>Crear</b></a></div>
          </td>
		  <td class=TDBKG width="33%"> 
             <div align="center"><a href="javascript:goMant()"><b>Modificar</b></a></div>
          </td>
	      <td class=TDBKG width="34%"> 
         <%  if ( userPO.getHeader10().equals("1") ) { %>
            <div align="center"><a href="javascript:goAsiPri()"><b>Asignar Vigencia</b></a></div>
         <%  } else { %>
             <div align="center"><a href="javascript:goDelete()"><b>Borrar</b></a></div>
         <%  }  %> 
          </td>
        </tr>
     <%} else {%>
          <%if  (userPO.getPurpose().equals("INQUIRY")) { %>
            <tr> 
              <td class=TDBKG width="30%"> 
		        <div align="center"><a href="javascript:goInquiry()"><b>Consultar</b></a></div>
              </td>
            </tr>       
          <%} else {%>
            <tr> 
              <td class=TDBKG width="30%"> 
		        <div align="center"><a href="javascript:goApproval()"><b>Visualizar</b></a></div>
              </td>
            </tr>
          <%} %>   
     <%} %> 
        
  </table>
   
  <br>
  <table  id=cfTable class="tableinfo">
    <tr > 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=center nowrap width="5%">&nbsp;</th>
            <th align=center nowrap width="5%"> Numero</th>
            <%  if ( userPO.getHeader10().equals("1") ) { %>
            <th align=center nowrap width="70%"> Direcci&oacute;n</th>
            <th align=center nowrap width="10%">Tipo </th>
            <th align=center nowrap width="10%">Vigencia</th>
         	<%  } else { %>            
         	<th align=center nowrap width="40%"> Nombre</th>
            <th align=center nowrap width="40%"> Direcci&oacute;n</th>
            <th align=center nowrap width="10%"> Estado</th>
         	<%  }  %>
          </tr>
          
          <%
          int row = 0;
    	  int i = 0;
          ESD400001List.initRow();
          while (ESD400001List.getNextRow()) {
            ESD400001Message msgList = (ESD400001Message) ESD400001List.getRecord();	 
         %>   
             
          
           <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            
            <td NOWRAP align="center" width="5%">
           	  <INPUT TYPE="radio" name="ROW" value="<%= msgList.getE01MAN()%>" 
           	  	<% if (ESD400001List.getCurrentRow() == 0) out.print(" checked"); %>>
		    </td>
           	<%  if ( userPO.getHeader10().equals("1") ) { %>
	            <td NOWRAP  align=center ><%= msgList.getE01MAN() %></td>
    	        <td NOWRAP  align=left >
        	    	<%= msgList.getE01AD1() %> &nbsp;
            		<%= msgList.getE01AD2() %> &nbsp;
            		<%= msgList.getE01AD3() %> &nbsp;
            		<%= msgList.getE01AD4() %> &nbsp;
            	</td>
           	<%  } else { %>
       			<td NOWRAP  align=center width=\"5%\"><%= msgList.getE01MAN() %></td>
            	<td NOWRAP  align=left width=\"40%\"><%= msgList.getE01MA1() %></td>
            	<td NOWRAP  align=left width=\"40%\">
           		
					<%if(!msgList.getE01AD1().trim().equals("")){ %>           		
	           		 	<%= msgList.getE01AD1() %> <br>
					<%}%>

					<%if(!msgList.getE01AD2().trim().equals("")){ %>           		
    	       		 	<%= msgList.getE01AD2() %> <br>
					<%}%>
					
					<%if(!msgList.getE01AD3().trim().equals("")){ %>           		
	         		   	<%= msgList.getE01AD3() %> <br>
					<%}%>
	
					<%if(!msgList.getE01AD4().trim().equals("")){ %>           		
	            		<%= msgList.getE01AD4() %> <br>
					<%}%>

            	</td>
			<%  }  %>
           	<%  if ( userPO.getHeader10().equals("1") ) { %>
            <td NOWRAP  align="center" >
    	        	<%= msgList.getE01ADT() %> 
            </td>
           <%  }  %>

           	<%  if ( userPO.getHeader10().equals("1") || userPO.getHeader10().equals("5")  ) { %>
	            <td NOWRAP  align="center" >
    	        <% if(msgList.getE01FL1().equals("Y")){%>
        		    <img  src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >  <%= msgList.getD01RTX() %>
            	<%}%>
 				</td>
 			<%  }  %>	
          </tr>
          
            <% i++; } %> 

        </table>
  </table>
     
<SCRIPT language="JavaScript">
    document.forms[0].TOTROWS.value = <%= i %>;
	document.forms[0].NEXTROWS.value = <%= ESD400001List.getLastRec()%>;
	document.forms[0].CURRROWS.value = <%= ESD400001List.getFirstRec()%>;
     
</SCRIPT>

<%}%>
	

  </form>

</body>
</html>
