<!-- Hecho por Alonso Arana ------Datapro-----06/02/2014 -->
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC210001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Conciliaci�n Bancos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ERClist" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

   $(function(){
				
					$("#radio_key").attr("checked", false);
                
				});


  function goAction(op,index) {
	var ok = false;
	var cun = "";
	var pg = "";


	if (op != '300'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01RCCTCD") 
	      	{	
	      		if (element.checked == true) {
	      			document.getElementById("codigo_lista").value = element.value; 
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = true;
      	
      	if (op =='202'){
      		confirm1 = confirm("Desea eliminar la cartola seleccionada?");
      	}
      	
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		

     } else {
		alert("Debe seleccionar una cartola para continuar.");	   
	 }    
	}







function goAction2(op) {


if(op=='300'){
     
      	
		var confirm1 = confirm("�Desea crear una nueva cartola?");
	if(confirm1){
	document.forms[0].SCREEN.value = op;
			document.forms[0].submit();	

		}


}


	}




</SCRIPT>  

</head>

<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Cartola Manual<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ERC2100_statement_list.jsp,ERC2100"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC2100">
<input type="hidden" name="SCREEN" value="201">
<input type="hidden" name="codigo_lista" value="" id="codigo_lista">  

 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">Banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<% out.print(session.getAttribute("codigo_banco")+"-"+session.getAttribute("nombre_banco"));%>" 
	  			size="60"  readonly>
             </td>  
              <td nowrap width="10%" align="right">Cuenta IBS : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCAA" size="23" maxlength="20" value="<%=session.getAttribute("cuenta_ibs")%>" readonly>
             </td>  
         </tr>
         
          <tr>
             <td nowrap width="10%" align="right">Cuenta Banco : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCTA" size="23" maxlength="20" value="<%=session.getAttribute("cuenta_banco")%>" readonly>
             </td>
       
             </tr>
           
        </table>
      </td>
    </tr>
  </table>
 
 
 <table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction2('300')"><b>Crear</b></a>
		</td>
	
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('202')"><b>Eliminar</b></a>
		</td>
		
		
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('500')"><b>Mantenci�n Movimientos</b></a>
		</td>
		<td align="center" class="tdbkg" width="10%"><a
			href="javascript:goAction('600')"> <b>Aprobar</b> </a></td>
		
	</tr>
</table>


<%
	if (ERClist.getNoResult()) {
%>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>



<%
	
	
	} else {
%>

  <p>&nbsp;</p>
<table id="headTable"  width="" align="left">
		<tr id="trdark">
			<th align="center" nowrap width="30"></th>
			<th align="center" nowrap width="100">Cartola</th>
			<th align="center" nowrap width="200">Fecha Creaci�n</th>
			<th align="center" nowrap width="100">Usuario</th>
			<th align="center" width="60%"></th>
			
			
			
			
		</tr>
		
		<%
			ERClist.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (ERClist.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
				ERC210001Message pvprd = (ERC210001Message) ERClist.getRecord();
		%>
		<tr>
		
		<td nowrap><input type="radio" name="E01RCCTCD" id="radio_key" value="<%= ERClist.getCurrentRow()%>" <%=chk%>/></td>
		<td nowrap align="right"><%=pvprd.getE01WCHSTN()%></td>
		
		
		<td nowrap align="center"><%
	
        String s = Util.getTimeFromTimestamp(pvprd.getE01WCHLMT());

        String[] temp;

        String delimiter;

        delimiter = ":";

        temp = s.split(delimiter);
	
		 
		out.print(pvprd.getE01WCHLMD()+"/"+pvprd.getE01WCHLMM()+"/"+pvprd.getE01WCHLMY()+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+temp[0]+":"+temp[1]+":"+temp[2]);%></td>

		
 	  	<td nowrap align="right"><%=pvprd.getE01WCHLMU()%></td>
 	  	<td nowrap align="right"></td>
			
 	 
	
		</tr>
		<%
			}
		%>
	</table>
<%
	}
%>
</form>
</body>
</html>
