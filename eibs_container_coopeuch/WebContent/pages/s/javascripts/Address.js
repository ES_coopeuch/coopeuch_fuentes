    function clearMunicipio(aux_Mun, mun_dsc, aux_parr, parr_dsc){
    	var munCode = document.getElementById(aux_Mun);
    	var munDsc = document.getElementById(mun_dsc);
    	var parrCode = document.getElementById(aux_parr);
    	var parrDsc = document.getElementById(parr_dsc)
    	if (munCode != null) munCode.value = "";
    	if (munDsc != null) munDsc.value = "";
    	if (parrCode != null) parrCode.value = "";
    	if (parrDsc != null) parrDsc.value = "";
    }
    
    function clearParroquia(aux_parr, parr_dsc){
    	var parrCode = document.getElementById(aux_parr);
    	var parrDsc = document.getElementById(parr_dsc);
    	if (parrCode != null) parrCode.value = "";
    	if (parrDsc != null) parrDsc.value = "";
    }    

	function concatCiudad() {
	    if (this.CTY != null) {
    		this.CTY.value = this.Aux_CTYCode.value + this.Aux_CTYDsc.value;
    	}	
    }
    
	function Address(pIndex){
		// source text fields
		this.index = pIndex;
		this.Aux_CTYCode = document.getElementById("Aux_CTYCode" + this.index);
		this.Aux_CTYDsc = document.getElementById("Aux_CTYDsc" + this.index);
  
		//hidden field CUSLN3   
		this.CTY = document.getElementById("E"+ this.index+ "4CTY");//incluye codigo de ciudad y descripcion
  
		//object functions  
		this.concatCTY = concatCiudad;
	}

	function calculateAddress(evt){
		evt = (evt) ? evt : ((window.event) ? window.event : "");
		for (i = 1; i <= 10; i++){
    		var index = "";
    		var cont = i;
    		if (cont == 10) index = "A";
    		else index = cont.toString();
    		var address = new Address(index);
    		address.concatCTY();
		}
	}