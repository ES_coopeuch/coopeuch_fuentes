package datapro.eibs.client;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDET05001Message;
import datapro.eibs.beans.EDET05002Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD000004Message;
import datapro.eibs.beans.ESD000005Message;
import datapro.eibs.beans.ESD000007Message;
import datapro.eibs.beans.ESD000011Message;
import datapro.eibs.beans.ESD000012Message;
import datapro.eibs.beans.ESD008001Message;
import datapro.eibs.beans.ESD008002Message;
import datapro.eibs.beans.ESD008003Message;
import datapro.eibs.beans.ESD008006Message;
import datapro.eibs.beans.ESD008008Message;
import datapro.eibs.beans.ESD008009Message;
import datapro.eibs.beans.ESD008010Message;
import datapro.eibs.beans.ESD008011Message;
import datapro.eibs.beans.ESD008015Message;
import datapro.eibs.beans.ESD008020Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBListRec;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSEDET050 extends datapro.eibs.master.SuperServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// personal client options
	protected static final int R_ENTER_INQUIRY = 100;
	protected static final int R_VIEW_DETAILS = 200;
	protected static final int R_SAVE_CLIENT = 300;
	protected static final int R_LIST_CLIENT = 400;
	protected static final int R_APROVAL_CLIENT = 500;
	protected static final int R_RECHAZAR_CLIENT = 600;
	protected static final int R_APROVAL_FINAL_CLIENT = 700;
	protected static final int R_RECHAZAR_FINAL_CLIENT = 800;
	protected static final int R_VISUALIZACION_CLIENT = 900;

	protected String LangPath = "S";

	/**
	 * JSEDET050 constructor comment.
	 */
	public JSEDET050() {
		super();
	}

	public void destroy() {

		flexLog("free resources used by JSEDET050");

	}

	/**
	 * This method was created by Orestes Garcia.
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	@SuppressWarnings("static-access")
	public void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			int screen = R_ENTER_INQUIRY;

			try {

				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session
						.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";

				try {
					flexLog("Opennig Socket Connection");
					mc = new MessageContext(super.getMessageHandler("EDET050",
							req));

					try {
						screen = Integer.parseInt(req.getParameter("SCREEN"));
					} catch (Exception e) {
						flexLog("Screen set to default value");
					}

					switch (screen) {
					case R_ENTER_INQUIRY:
						procReqEnterInq(msgUser, req, res, session);
						break;
					case R_VIEW_DETAILS:
						procDetailsClien(mc, msgUser, req, res, session);
						break;
					case R_SAVE_CLIENT:
						procSaveClient(mc, msgUser, req, res, session);
						break;
					case R_LIST_CLIENT:
						procListClien(mc, msgUser, req, res, session);
						break;
					case R_APROVAL_CLIENT:
						procPreAproveClient(mc, msgUser, req, res, session);
						break;
					case R_APROVAL_FINAL_CLIENT:
						procAprovalClient(mc,msgUser,req,res,session);
						break;
					case R_RECHAZAR_CLIENT:
						procPreRechazoClient(mc, msgUser, req, res, session);
						break;
					case R_RECHAZAR_FINAL_CLIENT:
						procRechazoClient(mc, msgUser, req, res, session);
						break;
					case R_VISUALIZACION_CLIENT:
						procVisualizacionClient(mc,msgUser,req,res,session);
						break;
					default:
						procReqEnterInq(msgUser, req, res, session);
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
					flexLog("Socket not Open(" + mc.toString() + "). Error: "
							+ e);
					res.sendRedirect(super.srctx + LangPath
							+ super.sckNotOpenPage);
					// return;
				} finally {
					mc.close();
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath
						+ super.sckNotRespondPage);
			}

		}

	}

	protected void showERROR(ELEERRMessage m) {
		if (logType != NONE) {

			flexLog("ERROR received.");

			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01()
					+ " code : " + m.getERDF01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02()
					+ " code : " + m.getERDF02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03()
					+ " code : " + m.getERDF03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04()
					+ " code : " + m.getERDF04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05()
					+ " code : " + m.getERDF05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06()
					+ " code : " + m.getERDF06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07()
					+ " code : " + m.getERDF07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08()
					+ " code : " + m.getERDF08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09()
					+ " code : " + m.getERDF09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10()
					+ " code : " + m.getERDF10());

		}
	}

	/**
	 * This method was created in VisualAge.
	 */
	protected void procReqEnterInq(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		EDET05001Message msgClient = null;
		ELEERRMessage msgError = null;
		UserPos userPO = null;

		try {

			msgClient = new EDET05001Message();
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = new datapro.eibs.beans.UserPos();
			userPO.setOption("CLIENT");
			userPO.setPurpose("INQUIRY");
			ses.setAttribute("client", msgClient);
			ses.setAttribute("error", msgError);
			ses.setAttribute("userPO", userPO);

		} catch (Exception ex) {
			flexLog("Error: " + ex);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDET050_client_search.jsp");
			callPage(LangPath + "EDET050_client_search.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
		}

	}

	protected void procDetailsClien(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		// Encargado de enviar y recibir la información principal del cliente
		EDET05001Message msgClient = null;
		// Encargado de recibir el primer mensaje que siempre es error
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		// Poseera el rut del cliente
		String rutClient = null;
		// Lista encargada de guardar las operaciones
		List<EDET05002Message> detalles = new ArrayList<EDET05002Message>();

		// Recibir los datos necesarios para luego enviarlos al AS
		try {
			msgClient = new EDET05001Message();
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = (UserPos) ses.getAttribute("userPO");

			if (req.getParameter("E01IDN") != null) {
				rutClient = req.getParameter("E01IDN");
			} else {
				flexLog("About to call Page: " + LangPath
						+ "EDET050_client_search.jsp");
				callPage(LangPath + super.sckNotOpenPage, req, res);
			}

		} catch (Exception ex) {
			flexLog("Error: " + ex);
			flexLog("About to call Page: " + LangPath
					+ "EDET050_client_search.jsp");
			callPage(LangPath + "EDET050_client_search.jsp", req, res);
		}
		// Envio de data para busqueda
		try {
			msgClient = (EDET05001Message) mc.getMessageRecord("EDET05001");
			msgClient.setH01USERID(user.getH01USR());
			msgClient.setH01PROGRM("EDET050");
			msgClient.setH01TIMSYS(getTimeStamp());
			msgClient.setH01OPECOD("0001");
			msgClient.setE01NRORUT(rutClient);

			msgClient.send();
			msgClient.destroy();

			flexLog("EDET050 Mensaje Enviado");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Recibiendo Datos
		try {
			msgError = (ELEERRMessage) mc.receiveMessage();
			// Se valida el primer mensaje que es de error
			int error = 0;
			try{
				error = Integer.parseInt(msgError.getERRNUM().trim());
			}catch(Exception e){
				error = 0;
			}
			if (error > 0) {
				// if there are errors go back to
				// first page
				ses.setAttribute("error", msgError);
				flexLog("About to call Page: " + LangPath
						+ "EDET050_client_search.jsp");
				callPage(LangPath + "EDET050_client_search.jsp", req, res);
			} else {
				// Recibe Información del cliente
				EDET05001Message header = (EDET05001Message) mc
						.receiveMessage();
				// Mientras el flag H02FLGMAS venga con el valor "+" se
				// agregaran a la lista en caso contrario dejara de recibir
				// mensajes
				while (true) {
					// Se recibe mensaje
					EDET05002Message message = (EDET05002Message) mc
							.receiveMessage();
					// Se valida el flag
					if (message.getH02FLGMAS().equals("*")) {
						// Si no es "+" se acaba el recibo de datos
						break;
					}
					// Se agrega a la lista
					detalles.add(message);
				}
				// Si no hay errores se enviara a la página para marca o
				// desmarca de cliente.
				ses.setAttribute("EDET05001Message", header);
				ses.setAttribute("EDET05002Message", detalles);
				ses.setAttribute("userPO", userPO);
				flexLog("About to call Page: " + LangPath
						+ "EDET050_client_details.jsp");
				callPage(LangPath + "EDET050_client_details.jsp", req, res);
			}

		} catch (Exception e) {
			flexLog("Exception calling page " + e.getMessage());
			flexLog("About to call Page: " + LangPath
					+ "EDET050_client_search.jsp");
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}

		try {
			flexLog("About to call Page: " + LangPath
					+ "EDET050_client_search.jsp");
			callPage(LangPath + "EDET050_client_search.jsp", req, res);
		} catch (Exception e) {
			flexLog("Exception calling page " + e);
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}

	}

	protected void procSaveClient(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		// Encargado de enviar y recibir la información principal del cliente
		EDET05001Message msgClient = null;
		// Encargado de recibir el primer mensaje que siempre es error
		ELEERRMessage msgError = null;
		UserPos userPO = null;

		// Recibir y enviar los datos necesarios para el AS
		EDET05001Message message = (EDET05001Message) ses.getAttribute("EDET05001Message");
		try {
			msgClient = (EDET05001Message) mc.getMessageRecord("EDET05001");
			msgClient.setH01USERID(user.getH01USR());
			msgClient.setH01PROGRM("EDET050");
			msgClient.setH01TIMSYS(getTimeStamp());
			msgClient.setH01OPECOD("0002");
			try {
				msgClient.setH01FLGWK3(req.getParameter("H01FLGWK3"));
			} catch (Exception e) {
				msgClient.setH01FLGWK3("");
			}
			msgClient.setE01NRORUT(req.getParameter("E01NRORUT"));
			msgClient.setE01CNA1(req.getParameter("E01CNA1"));
			msgClient.setE01CODRSK(req.getParameter("E01CODRSK"));
			msgClient.setE01FLGDET(req.getParameter("E01FLGDET").toString());
			if (req.getParameter("E01FLGDET").toString().equalsIgnoreCase("Y")){
				msgClient.setE01FCHMAA(req.getParameter("E01FCHMAA"));
				msgClient.setE01FCHMDD(req.getParameter("E01FCHMDD"));
				msgClient.setE01FCHMMM(req.getParameter("E01FCHMMM"));
			}else {
				msgClient.setE01FCHDAA(req.getParameter("E01FCHDAA"));
				msgClient.setE01FCHDDD(req.getParameter("E01FCHDDD"));
				msgClient.setE01FCHDMM(req.getParameter("E01FCHDMM"));
			}
			msgClient.setE01FCHIAA(req.getParameter("E01FCHIAA"));
			msgClient.setE01FCHIDD(req.getParameter("E01FCHIDD"));
			msgClient.setE01FCHIMM(req.getParameter("E01FCHIMM"));
			msgClient.setE01FCHVAA(req.getParameter("E01FCHVAA"));
			msgClient.setE01FCHVDD(req.getParameter("E01FCHVDD"));
			msgClient.setE01FCHVMM(req.getParameter("E01FCHVMM"));
			msgClient.setE01MOTIVO(req.getParameter("E01MOTIVO"));
			msgClient.setE01NRORUT(req.getParameter("E01NRORUT"));
			msgClient.setE01NUMCLI(req.getParameter("E01NUMCLI"));
			msgClient.setE01OPERAC(req.getParameter("E01OPERAC").equalsIgnoreCase("Individual") ? "I" : "G");
			msgClient.setE01PLAZO(req.getParameter("E01PLAZO"));
			msgClient.setE01TXTMOT(req.getParameter("E01TXTMOT"));
			message.setE01TXTMOT(req.getParameter("E01TXTMOT"));
			message.setE01MOTIVO(req.getParameter("E01MOTIVO"));
			msgClient.setE01USER(req.getParameter("E01USER"));
			msgClient.setE01FLGMAR(req.getParameter("E01FLGMAR"));
			msgClient.setE01ULTMAR("");

			msgClient.send();
			msgClient.destroy();

			flexLog("EDET050 Mensaje Enviado");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			callPage(LangPath + super.sckNotOpenPage, req, res);
			throw new RuntimeException("Socket Communication Error");
		}

		// Recibiendo Datos
		try {
			msgError = (ELEERRMessage) mc.receiveMessage();
			// Se valida el primer mensaje que es de error
			int error = 0;
			try{
				error = Integer.parseInt(msgError.getERRNUM().trim());
			}catch(Exception e){
				error = 0;
			}
			if (error > 0) { // if there are errors go
				// back to second page
				ses.setAttribute("error", msgError);
				ses.setAttribute("EDET05001Message", message);
				flexLog("About to call Page: " + LangPath
						+ "EDET050_client_details.jsp");
				callPage(LangPath + "EDET050_client_details.jsp", req, res);
			}

			else {
				// Recibe Información del cliente
				EDET05001Message header = (EDET05001Message) mc
						.receiveMessage();
				// Si no hay errores se enviara a la página para marca.
				ses.setAttribute("msgData", header);
				ses.setAttribute("userPO", userPO);
				flexLog("About to call Page: " + LangPath
						+ "EDET050_client_procces.jsp");
				callPage(LangPath + "EDET050_client_procces.jsp", req, res);
			}
		} catch (Exception e) {
			flexLog("Exception calling page " + e.getMessage());
			flexLog("About to call Page: " + LangPath
					+ "EDET050_client_search.jsp");
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}
	}

	protected void procListClien(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		// Encargado de enviar y recibir la información principal del cliente
		EDET05001Message msgClient = null;
		// Encargado de recibir el primer mensaje que siempre es error
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		// Lista encargada de guardar las operaciones
		List<EDET05001Message> detalles = new ArrayList<EDET05001Message>();
		boolean more = false;
		boolean first = true;
		int reinli = 0;
		int normal = 0;
		// Recibir los datos necesarios para luego enviarlos al AS
		try {
			msgClient = new EDET05001Message();
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = (UserPos) ses.getAttribute("userPO");

		} catch (Exception ex) {
			flexLog("Error: " + ex);
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}
		// Envio de data para busqueda
		try {
			msgClient = (EDET05001Message) mc.getMessageRecord("EDET05001");
			msgClient.setH01USERID(user.getH01USR());
			msgClient.setH01PROGRM("EDET050");
			msgClient.setH01TIMSYS(getTimeStamp());
			msgClient.setH01OPECOD("0003");
			try{
				reinli = Integer.parseInt(req.getParameter("E01REINLI"));
				msgClient.setE01REINLI(""+reinli);
				if (reinli > 1){
					first = false;
				}
			}catch(Exception e){
				msgClient.setE01REINLI("1");
				reinli++;
			}
			
			msgClient.send();
			msgClient.destroy();

			flexLog("EDET050 Mensaje Enviado");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			callPage(LangPath + sckNotOpenPage, req, res);
			throw new RuntimeException("Socket Communication Error");
		}

		// Recibiendo Datos
		try {
			msgError = (ELEERRMessage) mc.receiveMessage();
			// Se valida el primer mensaje que es de error
			int error = 0;
			try{
				error = Integer.parseInt(msgError.getERRNUM().trim());
			}catch(Exception e){
				error = 0;
			}
			if (error > 0) { // if there are errors go
				// back to
				// first page
				ses.setAttribute("error", msgError);
				flexLog("About to call Page: " + LangPath
						+ "EDET050_client_search.jsp");
				callPage(LangPath + super.sckNotOpenPage, req, res);
			}

			else {
				// Mientras el flag H02FLGMAS venga con el valor "+" se
				// agregaran a la lista en caso contrario dejara de recibir
				// mensajes
				while (true) {
					// Se recibe mensaje
					EDET05001Message message = (EDET05001Message) mc
							.receiveMessage();
					// Se valida el flag
					if (message.getH01FLGMAS().equals("*")) {
						// Si no es "+" se acaba el recibo de datos
						break;
					} else if (message.getH01FLGMAS().equals("+")){
						more = true;
						break;
					}
					// Se agrega a la lista
					detalles.add(message);
				}
				// Si no hay errores se enviara a la página para marca o
				// desmarca de cliente.
				if (first){
					normal = detalles.size();
				}else {
					try{
						normal = Integer.parseInt(req.getParameter("normal"));
					}catch(Exception e){}
				}
				ses.setAttribute("more", more);
				ses.setAttribute("first", first);
				ses.setAttribute("reinli", reinli);
				ses.setAttribute("EDET05001Message", detalles);
				ses.setAttribute("normal", normal);
				ses.setAttribute("userPO", userPO);
				flexLog("About to call Page: " + LangPath
						+ "EDET050_list_client_details.jsp");
				callPage(LangPath + "EDET050_list_client_details.jsp", req, res);
			}

		} catch (Exception e) {
			flexLog("Exception calling page " + e.getMessage());
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}
	}
	
	protected void procPreAproveClient(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		// Encargado de enviar y recibir la información principal del cliente
		EDET05001Message msgClient = null;
		// Encargado de recibir el primer mensaje que siempre es error
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		// Poseera el rut del cliente
		String rutClient = null;
		// Lista encargada de guardar las operaciones
		List<EDET05002Message> detalles = new ArrayList<EDET05002Message>();

		// Recibir los datos necesarios para luego enviarlos al AS
		try {
			msgClient = new EDET05001Message();
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = (UserPos) ses.getAttribute("userPO");

			if (req.getParameter("E01IDN") != null) {
				rutClient = req.getParameter("E01IDN");
			} else {
				flexLog("About to call Page: " + LangPath + super.sckNotOpenPage);
				callPage(LangPath + super.sckNotOpenPage, req, res);
			}

		} catch (Exception ex) {
			flexLog("Error: " + ex);
			flexLog("About to call Page: " + LangPath + "EDET050_list_client_details.jsp");
			callPage(LangPath + "EDET050_list_client_details.jsp", req, res);
		}
		// Envio de data para busqueda
		try {
			msgClient = (EDET05001Message) mc.getMessageRecord("EDET05001");
			msgClient.setH01USERID(user.getH01USR());
			msgClient.setH01PROGRM("EDET050");
			msgClient.setH01TIMSYS(getTimeStamp());
			msgClient.setH01OPECOD("0001");
			msgClient.setE01NRORUT(rutClient);

			msgClient.send();
			msgClient.destroy();

			flexLog("EDET050 Mensaje Enviado");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Recibiendo Datos
		try {
			msgError = (ELEERRMessage) mc.receiveMessage();
			// Se valida el primer mensaje que es de error
			int error = 0;
			try{
				error = Integer.parseInt(msgError.getERRNUM().trim());
			}catch(Exception e){
				error = 0;
			}
			if (error > 0) {
				// if there are errors go back to
				// first page
				ses.setAttribute("error", msgError);
				flexLog("About to call Page: " + LangPath + "EDET050_list_client_details.jsp");
				callPage(LangPath + "EDET050_list_client_details.jsp", req, res);
			} else {
				// Recibe Información del cliente
				EDET05001Message header = (EDET05001Message) mc
						.receiveMessage();
				// Mientras el flag H02FLGMAS venga con el valor "+" se
				// agregaran a la lista en caso contrario dejara de recibir
				// mensajes
				while (true) {
					// Se recibe mensaje
					EDET05002Message message = (EDET05002Message) mc
							.receiveMessage();
					// Se valida el flag
					if (message.getH02FLGMAS().equals("*")) {
						// Si no es "+" se acaba el recibo de datos
						break;
					}
					// Se agrega a la lista
					detalles.add(message);
				}
				// Si no hay errores se enviara a la página para marca o
				// desmarca de cliente.
				ses.setAttribute("EDET05001Message", header);
				ses.setAttribute("EDET05002Message", detalles);
				ses.setAttribute("userPO", userPO);
				flexLog("About to call Page: " + LangPath + "EDET050_client_details_aprove.jsp");
				callPage(LangPath + "EDET050_client_details_aprove.jsp", req, res);
			}

		} catch (Exception e) {
			flexLog("Exception calling page " + e.getMessage());
			flexLog("About to call Page: " + LangPath + "super.sckNotOpenPage");
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}
	}
	
	protected void procAprovalClient(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		// Encargado de enviar y recibir la información principal del cliente
		EDET05001Message msgClient = null;
		// Encargado de recibir el primer mensaje que siempre es error
		ELEERRMessage msgError = null;
		UserPos userPO = null;

		// Recibir y enviar los datos necesarios para el AS
		EDET05001Message message = (EDET05001Message) ses.getAttribute("EDET05001Message");
		try {
			msgClient = (EDET05001Message) mc.getMessageRecord("EDET05001");
			msgClient.setH01USERID(user.getH01USR());
			msgClient.setH01PROGRM("EDET050");
			msgClient.setH01TIMSYS(getTimeStamp());
			msgClient.setH01OPECOD("0004");
			msgClient.setE01NRORUT(req.getParameter("E01NRORUT"));
			msgClient.setE01FLGMAR(req.getParameter("E01FLGMAR"));

			msgClient.send();
			msgClient.destroy();

			flexLog("EDET050 Mensaje Enviado");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			callPage(LangPath + super.sckNotOpenPage, req, res);
			throw new RuntimeException("Socket Communication Error");
		}

		// Recibiendo Datos
		try {
			msgError = (ELEERRMessage) mc.receiveMessage();
			// Se valida el primer mensaje que es de error
			int error = 0;
			try{
				error = Integer.parseInt(msgError.getERRNUM().trim());
			}catch(Exception e){
				error = 0;
			}
			if (error > 0) { // if there are errors go
				// back to second page
				ses.setAttribute("error", msgError);
				ses.setAttribute("EDET05001Message", message);
				flexLog("About to call Page: " + LangPath + "EDET050_client_details.jsp");
				callPage("/servlet/datapro.eibs.client.JSEDET050?SCREEN=400", req, res);
			}

			else {
				// Recibe Información del cliente
				EDET05001Message header = (EDET05001Message) mc.receiveMessage();
				// Si no hay errores se enviara a la página para marca.
				ses.setAttribute("msgData", header);
				ses.setAttribute("userPO", userPO);
				flexLog("About to call Page: " + LangPath + "EDET050_client_procces.jsp");
				callPage("/servlet/datapro.eibs.client.JSEDET050?SCREEN=400", req, res);
			}
		} catch (Exception e) {
			flexLog("Exception calling page " + e.getMessage());
			flexLog("About to call Page: " + LangPath + "EDET050_client_search.jsp");
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}
	}
	
	
	protected void procPreRechazoClient(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		// Encargado de enviar y recibir la información principal del cliente
		EDET05001Message msgClient = null;
		// Encargado de recibir el primer mensaje que siempre es error
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		// Poseera el rut del cliente
		String rutClient = null;
		// Lista encargada de guardar las operaciones
		List<EDET05002Message> detalles = new ArrayList<EDET05002Message>();

		// Recibir los datos necesarios para luego enviarlos al AS
		try {
			msgClient = new EDET05001Message();
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = (UserPos) ses.getAttribute("userPO");

			if (req.getParameter("E01IDN") != null) {
				rutClient = req.getParameter("E01IDN");
			} else {
				flexLog("About to call Page: " + LangPath + super.sckNotOpenPage);
				callPage(LangPath + super.sckNotOpenPage, req, res);
			}

		} catch (Exception ex) {
			flexLog("Error: " + ex);
			flexLog("About to call Page: " + LangPath + "EDET050_list_client_details.jsp");
			callPage(LangPath + "EDET050_list_client_details.jsp", req, res);
		}
		// Envio de data para busqueda
		try {
			msgClient = (EDET05001Message) mc.getMessageRecord("EDET05001");
			msgClient.setH01USERID(user.getH01USR());
			msgClient.setH01PROGRM("EDET050");
			msgClient.setH01TIMSYS(getTimeStamp());
			msgClient.setH01OPECOD("0001");
			msgClient.setE01NRORUT(rutClient);

			msgClient.send();
			msgClient.destroy();

			flexLog("EDET050 Mensaje Enviado");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Recibiendo Datos
		try {
			msgError = (ELEERRMessage) mc.receiveMessage();
			// Se valida el primer mensaje que es de error
			int error = 0;
			try{
				error = Integer.parseInt(msgError.getERRNUM().trim());
			}catch(Exception e){
				error = 0;
			}
			if (error > 0) {
				// if there are errors go back to
				// first page
				ses.setAttribute("error", msgError);
				flexLog("About to call Page: " + LangPath + "EDET050_list_client_details.jsp");
				callPage(LangPath + "EDET050_list_client_details.jsp", req, res);
			} else {
				// Recibe Información del cliente
				EDET05001Message header = (EDET05001Message) mc
						.receiveMessage();
				// Mientras el flag H02FLGMAS venga con el valor "+" se
				// agregaran a la lista en caso contrario dejara de recibir
				// mensajes
				while (true) {
					// Se recibe mensaje
					EDET05002Message message = (EDET05002Message) mc
							.receiveMessage();
					// Se valida el flag
					if (message.getH02FLGMAS().equals("*")) {
						// Si no es "+" se acaba el recibo de datos
						break;
					}
					// Se agrega a la lista
					detalles.add(message);
				}
				// Si no hay errores se enviara a la página para marca o
				// desmarca de cliente.
				ses.setAttribute("EDET05001Message", header);
				ses.setAttribute("EDET05002Message", detalles);
				ses.setAttribute("userPO", userPO);
				flexLog("About to call Page: " + LangPath + "EDET050_client_details_rechazo.jsp");
				callPage(LangPath + "EDET050_client_details_rechazo.jsp", req, res);
			}

		} catch (Exception e) {
			flexLog("Exception calling page " + e.getMessage());
			flexLog("About to call Page: " + LangPath + super.sckNotOpenPage);
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}
	}
	
	protected void procRechazoClient(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		// Encargado de enviar y recibir la información principal del cliente
		EDET05001Message msgClient = null;
		// Encargado de recibir el primer mensaje que siempre es error
		ELEERRMessage msgError = null;
		UserPos userPO = null;

		// Recibir y enviar los datos necesarios para el AS
		EDET05001Message message = (EDET05001Message) ses.getAttribute("EDET05001Message");
		try {
			msgClient = (EDET05001Message) mc.getMessageRecord("EDET05001");
			msgClient.setH01USERID(user.getH01USR());
			msgClient.setH01PROGRM("EDET050");
			msgClient.setH01TIMSYS(getTimeStamp());
			msgClient.setH01OPECOD("0005");
			msgClient.setE01NRORUT(req.getParameter("E01NRORUT"));
			msgClient.setE01FLGMAR(req.getParameter("E01FLGMAR"));

			msgClient.send();
			msgClient.destroy();

			flexLog("EDET050 Mensaje Enviado");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			callPage(LangPath + super.sckNotOpenPage, req, res);
			throw new RuntimeException("Socket Communication Error");
		}

		// Recibiendo Datos
		try {
			msgError = (ELEERRMessage) mc.receiveMessage();
			// Se valida el primer mensaje que es de error
			int error = 0;
			try{
				error = Integer.parseInt(msgError.getERRNUM().trim());
			}catch(Exception e){
				error = 0;
			}
			if (error > 0) { // if there are errors go
				// back to second page
				ses.setAttribute("error", msgError);
				ses.setAttribute("EDET05001Message", message);
				flexLog("About to call Page: " + LangPath
						+ "EDET050_client_details_rechazo.jsp");
				callPage(LangPath + "EDET050_client_details_rechazo.jsp", req, res);
			}

			else {
				// Recibe Información del cliente
				EDET05001Message header = (EDET05001Message) mc.receiveMessage();
				// Si no hay errores se enviara a la página para marca.
				ses.setAttribute("msgData", header);
				ses.setAttribute("userPO", userPO);
				flexLog("About to call Page: " + LangPath + "EDET050_client_procces.jsp");
				callPage("/servlet/datapro.eibs.client.JSEDET050?SCREEN=400", req, res);
			}
		} catch (Exception e) {
			flexLog("Exception calling page " + e.getMessage());
			flexLog("About to call Page: " + LangPath + super.sckNotOpenPage);
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}
	}
	
	protected void procVisualizacionClient(MessageContext mc, ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		// Encargado de enviar y recibir la información principal del cliente
		EDET05001Message msgClient = null;
		// Encargado de recibir el primer mensaje que siempre es error
		ELEERRMessage msgError = null;
		UserPos userPO = null;
		// Poseera el rut del cliente
		String rutClient = null;
		// Lista encargada de guardar las operaciones
		List<EDET05002Message> detalles = new ArrayList<EDET05002Message>();

		// Recibir los datos necesarios para luego enviarlos al AS
		try {
			msgClient = new EDET05001Message();
			msgError = new datapro.eibs.beans.ELEERRMessage();
			userPO = (UserPos) ses.getAttribute("userPO");

			if (req.getParameter("E01IDN") != null) {
				rutClient = req.getParameter("E01IDN");
			} else {
				flexLog("About to call Page: " + LangPath + super.sckNotOpenPage);
				callPage(LangPath + super.sckNotOpenPage, req, res);
			}

		} catch (Exception ex) {
			flexLog("Error: " + ex);
			flexLog("About to call Page: " + LangPath + super.sckNotOpenPage);
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}
		// Envio de data para busqueda
		try {
			msgClient = (EDET05001Message) mc.getMessageRecord("EDET05001");
			msgClient.setH01USERID(user.getH01USR());
			msgClient.setH01PROGRM("EDET050");
			msgClient.setH01TIMSYS(getTimeStamp());
			msgClient.setH01OPECOD("0001");
			msgClient.setE01NRORUT(rutClient);

			msgClient.send();
			msgClient.destroy();

			flexLog("EDET050 Mensaje Enviado");
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Recibiendo Datos
		try {
			msgError = (ELEERRMessage) mc.receiveMessage();
			// Se valida el primer mensaje que es de error
			int error = 0;
			try{
				error = Integer.parseInt(msgError.getERRNUM().trim());
			}catch(Exception e){
				error = 0;
			}
			if (error > 0) {
				// if there are errors go back to
				// first page
				ses.setAttribute("error", msgError);
				flexLog("About to call Page: " + LangPath + "EDET050_list_client_details.jsp");
				callPage(LangPath + "EDET050_list_client_details.jsp", req, res);
			} else {
				// Recibe Información del cliente
				EDET05001Message header = (EDET05001Message) mc
						.receiveMessage();
				// Mientras el flag H02FLGMAS venga con el valor "+" se
				// agregaran a la lista en caso contrario dejara de recibir
				// mensajes
				while (true) {
					// Se recibe mensaje
					EDET05002Message message = (EDET05002Message) mc
							.receiveMessage();
					// Se valida el flag
					if (message.getH02FLGMAS().equals("*")) {
						// Si no es "+" se acaba el recibo de datos
						break;
					}
					// Se agrega a la lista
					detalles.add(message);
				}
				// Si no hay errores se enviara a la página para marca o
				// desmarca de cliente.
				ses.setAttribute("EDET05001Message", header);
				ses.setAttribute("EDET05002Message", detalles);
				ses.setAttribute("userPO", userPO);
				flexLog("About to call Page: " + LangPath + "EDET050_client_details_visualizacion.jsp");
				callPage(LangPath + "EDET050_client_details_visualizacion.jsp", req, res);
			}

		} catch (Exception e) {
			flexLog("Exception calling page " + e.getMessage());
			flexLog("About to call Page: " + LangPath + super.sckNotOpenPage);
			callPage(LangPath + super.sckNotOpenPage, req, res);
		}
	}
}
