package datapro.eibs.client;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.EDET05101Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.sockets.MessageRecord;

public class JSEDET051 extends JSEIBSServlet {

	protected static final int R_CLASIFICACION_RUT_LIST	= 100;
	protected static final int A_CLASIFICACION_RUT_LIST = 200;
	protected static final int R_SELECT_MAINT_CLASIFICACION_RUT = 300;
	protected static final int R_MAINTENANCE_CLASIFICACION_RUT = 400;
	
		protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		switch (screen) {
			case R_CLASIFICACION_RUT_LIST :
				procReqClasRutList(user, req, res, session);
				break;
			case A_CLASIFICACION_RUT_LIST :
				procActClasRutList(user, req, res, session);
				break;
			case R_SELECT_MAINT_CLASIFICACION_RUT :
				procSelMainClasRut(user, req, res, session);
				break;
			case R_MAINTENANCE_CLASIFICACION_RUT:
				procActMainClasRut(user, req, res, session);
				break;
			default :
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

	
	private void procReqClasRutList(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos	userPO = new UserPos();
		ELEERRMessage msgError = new ELEERRMessage(); 
		
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		
		forward("EDET051_clasrut_client_enter_search.jsp", req, res);
	}


	private void procActClasRutList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = getUserPos(session);
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDET051", req);
			EDET05101Message msgList = (EDET05101Message) mp.getMessageRecord("EDET05101", user.getH01USR(), "0003");
			setMessageRecord(req, msgList);
			userPO.setID(req.getParameter("E01INDRUT"));
						
			mp.sendMessage(msgList);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord();
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);

				forward("EDET051_clasrut_client_enter_search.jsp", req, res);
			} else {
				
				flexLog("Putting java beans into the session");
				session.setAttribute("listRut", list);
				session.setAttribute("userPO", userPO);
				
				forward("EDET051_clasrut_client_list.jsp", req, res);
			}
		} 
		
		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}

	private void procSelMainClasRut(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		int inptOPT = 0;
		try {
			inptOPT = Integer.parseInt(req.getParameter("opt").trim());
		} catch (Exception e) {
			inptOPT = 0;
		}
		switch (inptOPT) {
			case 1 : //New
				userPO.setPurpose("NEW");
				session.setAttribute("userPO", userPO);
				procReqNew(user, req, res, session);
				break;
			default : 
				forward("MISC_not_available.jsp", req, res);
				break;
		}
	}

	private void procReqNew(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session) throws ServletException, IOException {

		UserPos userPO = getUserPos(session);
		ELEERRMessage msgError = new ELEERRMessage(); 
		
		session.setAttribute("error", msgError);
		session.setAttribute("userPO", userPO);
		
		forward("EDET051_clasrut_client_maintenance.jsp", req, res);
	}
	

	private void procActMainClasRut(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		UserPos userPO = getUserPos(session);
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("EDET051", req);
			EDET05101Message msgcli = (EDET05101Message) session.getAttribute("msgcli");
			msgcli.setH01USERID(user.getH01USR());
			msgcli.setH01PROGRM("EDET051");
			msgcli.setH01TIMSYS(getTimeStamp());
			msgcli.setH01OPECOD("0001");
			
			setMessageRecord(req, msgcli);
			
			mp.sendMessage(msgcli);
			
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			msgcli = (EDET05101Message) mp.receiveMessageRecord("EDET05101");
			
			if (mp.hasError(msgError)) {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("msgcli", msgcli);
				session.setAttribute("userPO", userPO);
				
				forward("EDET051_clasrut_client_maintenance.jsp", req, res);
			} else {
				flexLog("Putting java beans into the session");
				session.setAttribute("error", msgError);
				session.setAttribute("msgcli", msgcli);
				session.setAttribute("userPO", userPO);
				
				procActClasRutList(user, req, res, session);
			}
		} 

		catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
		
		finally {
			if (mp != null)	mp.close();
		}
	}
//	
}
