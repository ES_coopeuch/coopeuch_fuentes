<%// Hecho por Alonso Arana-----------------------------------/12/2013 -------------Datapro--------------- %>
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="cnvObj" class="datapro.eibs.beans.EPV121701Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">
	      $(function(){
	$("#num").prop('enabled', true);
	$("#alfa").prop('disabled', true);
	 });
			
	function flag(){
	
		var value = getElementChecked("E01LISFLG").value;
		
		if (value=='N'){
		//Variable colocamo los titulos corrrespondientes		
		    document.forms[0].E01LISCAL.disabled = true;
				    document.forms[0].E01LISCAL.value='';
			document.forms[0].E01LISCNU.disabled = false;		    			
		}else if(value=='A'){			
		    document.forms[0].E01LISCAL.disabled = false;
					document.forms[0].E01LISCNU.value='';
			document.forms[0].E01LISCNU.disabled = true;		    											
		}						
	}	

 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<%if (readOnly){ %>
	Consulta de Par�metros  de Cr�dito Listo
<%} else if (maintenance){ %>
	Mantenci�n Par�metros  de Cr�dito Listo
<%} else { %>
	Nuevo  Par�metros  de Cr�dito Listo
<%} %>

 <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="EPV1017_credit_maintenance.jsp, EPV1217"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1217" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="E01UBK" value="<%= currUser.getE01UBK().trim()%>">
  
 <% int row = 0;%>
 
    
  <table  class="tableinfo" width="100%" border="0">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr > 
            <td width="40%" nowrap="nowrap"> 
              <div align="right">C�digo :</div>
            </td>
            <td width="60%" nowrap="nowrap"> 
            <%if (readOnly){%>
            	<eibsinput:text property="E01LISCOD" name="cnvObj" eibsType="<%=EibsFields.EIBS_FIELD_TYPE_TEXT%>" size="4" disabled="true"/>
            <%}else if (maintenance){
			
			out.print(cnvObj.getE01LISCOD());
			%>
			
            	<input type="hidden" name="E01LISCOD" value="<%=cnvObj.getE01LISCOD() %>">
            <%}
            else{
            %>
            
            	 <input type="text" name="E01LISCOD"  size="4" maxlength="4">
            
            <% 
            }
            
             %>            
	             
	        </td>
          </tr>

          <tr > 
            <td > 
              <div align="right">Descripci�n :</div>
            </td>
            <td  > 
            <%if (readOnly){%>
            	<eibsinput:text property="E01LISDES" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" disabled="true"/>
            <%}else if (maintenance){%>
            	<eibsinput:text property="E01LISDES" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>"/>
            <%}
            
            else{
            %>
                   	 <input type="text" name="E01LISDES"  size="60"   maxlength="60" >
     
            <% 
            }
             %>
	             
	        </td>
          </tr>
         
                    
                
          <tr id="f1"> 
            <td> 
              <div align="right">Tipo:</div>
            </td>
            <td> 
 		      
 		       <%if (maintenance){%>
 		       <p> 
 		      
                 <input type="radio" name="E01LISFLG" id="radio_key" onclick="flag();"  value="N" <%if (cnvObj.getE01LISFLG().equals("N")) out.print("checked"); %>  <%=readOnly?"disabled":""%>  >
                  N�merico
                 <input type="radio" name="E01LISFLG" id="radio_key"  value="A" onclick="flag();" <%if (cnvObj.getE01LISFLG().equals("A")) out.print("checked"); %>   <%=readOnly?"disabled":""%>  >
                  Alfan�merico	
                 
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
                 <%}
                 
                 else{
                 %>
                 
                       <p> 
 		      
                 <input type="radio" name="E01LISFLG" id="radio_key" onclick="flag();"  value="N" <%out.print("checked"); %>  <%=readOnly?"disabled":""%>  >
                  N�merico
                 <input type="radio" name="E01LISFLG" id="radio_key"  value="A" onclick="flag();" <%=readOnly?"disabled":""%>  >
                  Alfan�merico	
                 
                   &nbsp;&nbsp;&nbsp;&nbsp;
                </p>
                 <% 
                 }
                 
                   %>             
	        </td>
          </tr>
          
	          <tr id="f3"> 
            <td> 
              <div align="right" id="div_valor">Valor:</div>
            </td>
            <td> 
 		            <%if (readOnly){%>
 		            
 		         <%    if(cnvObj.getE01LISFLG().equals("N")){ %>
            	
            	<eibsinput:text property="E01LISCNU" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" disabled="true"/>
          		
          		
          		 <%}
          		 
          		 if(cnvObj.getE01LISFLG().equals("A")){
          		 %>
          		 
          		  	<eibsinput:text property="E01LISCAL" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION%>" disabled="true"/>
          		 
          		 <% }
          		 
          		  %>
           
            <%}else if(maintenance){%>
            
              <%    if(cnvObj.getE01LISFLG().equals("N")){ %>
            
            	 <input type="text" name="E01LISCNU" value="<%=cnvObj.getE01LISCNU() %>" size="15" maxlength="15" >
             	<eibsinput:text property="E01LISCAL" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TEXT%>" size="4" maxlength="4" disabled="true"/>
         			
         			<%} %>
               	 
          
              <%    if(cnvObj.getE01LISFLG().equals("A")){ %>
            
            
            
            	<eibsinput:text property="E01LISCNU" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>" size="7" maxlength="15" disabled="true"/>
           
             	<eibsinput:text property="E01LISCAL" name="cnvObj" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_TEXT%>" size="4" maxlength="4" />
         			
         			<%} %>
          		 
          		 <% }
          		 
          		 else
          		 {
          		 %>
          		 
          		 <input type="text" name="E01LISCNU" id="num" size="15" maxlength="15" >
          		
          		<input type="text" name="E01LISCAL" id="alfa" size="4" maxlength="4">
            
 				<% 
          		 }
          		 %>          		
                               
	        </td>
          </tr>
          
	

         
        </table>
      </td>
    </tr>
  </table>

<%if  (1==1) { %>
    <div align="center"> 
        <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
    </div>
<% } %>  


  </form>
</body>
</HTML>
