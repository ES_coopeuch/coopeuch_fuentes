package com.datapro.eibs.security.object;

/**
 * Insert the type's description here.
 * Creation date: (7/19/00 6:55:55 PM)
 * @author: Ramses
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.math.BigDecimal;
import com.datapro.sql.manager.*;
//import com.datapro.eibs.security.access.jdbc.bean.*;
import com.datapro.eibs.security.bean.*;
import com.datapro.eibs.security.dao.*;
import com.datapro.eibs.security.facade.*;
import com.datapro.generic.beanutil.*;

import java.sql.Connection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;


public class UserProfile {

	/**
	 * 
	 */
	public UserProfile() {
		super();
	}
	/**
	 * 
	 */
	public void destroy() {

	}

	

	public USERBean getUser(String USERID) {

		CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
		CNTRLBTH userBTHBean = null;
		CNTRLEUPDao userEUPDao = new CNTRLEUPDao("AS400");
		CNTRLEUP userEUPBean = null;
		USERBean userBean = null;
		DynamicDTO dto = new DynamicDTO();
		try {
			
			userBTHBean = userBTHDao.select(USERID);
			//userBTHDao.close();
			userEUPBean = userEUPDao.select(USERID);
			//userEUPDao.close();
			if (userBTHBean != null) {
				if (userEUPBean == null) userEUPBean = new CNTRLEUP();
				if (dto.load(userBTHBean) && dto.load(userEUPBean)) {
					userBean = new USERBean();
					if(! dto.unload(userBean)) userBean = null;
				} 
			}	
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
				 
		return userBean;
	}
	
	public int delete(String USERID) {
				
		int result = 0;

		try {
			CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
			result = userBTHDao.delete(USERID);
			//userBTHDao.close();
			if (result != 0) {
				    CNTRLEUPDao userEUPDao = new CNTRLEUPDao("AS400");
					int result1 = userEUPDao.delete(USERID);
					//userEUPDao.close();
					//if (result != 0) {
						CNTRLDBADao userDBADao = new CNTRLDBADao("AS400");
						result1 = userDBADao.delete(USERID);
					//	userDBADao.close();	
					//	if (result != 0) {
							CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
							result1 = userPVLDao.delete(USERID);
					//		userPVLDao.close();		
					//	}	
					//}
			}
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 

		return result;
	}

	public int update(USERBean formBean) {
		
		CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
		CNTRLEUPDao userEUPDao = new CNTRLEUPDao("AS400");
		CNTRLBTH userBTH = new CNTRLBTH();
		CNTRLEUP userEUP = new CNTRLEUP();
		
		DynamicDTO dto = new DynamicDTO();
		
		int result=0;
		try {
						
			if (dto.load(formBean)) {
				if(dto.unload(userBTH) && dto.unload(userEUP)){ 
					result = userBTHDao.update(userBTH);
					//userBTHDao.close();
					if (result != 0) result = userEUPDao.update(userEUP);
					//userEUPDao.close();
				}	//if (result != 0)
			}	    //result = userDBADao.insert(bnkList);
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 
		
		
		return result;
	}
	
	public int update(USERBean formBean,CNTRLBTH userBTH) {
		
		CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
				
		DynamicDTO dto = new DynamicDTO();
		
		int result=0;
		try {
						
			if (dto.load(formBean)) {
				if(dto.unload(userBTH)){ 
					result = userBTHDao.update(userBTH);
					//userBTHDao.close();
				}	
			}	    
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 
		
		return result;
	}
	
	public int update(USERBean formBean,CNTRLEUP userEUP) {
		
		
		CNTRLEUPDao userEUPDao = new CNTRLEUPDao("AS400");
		
		DynamicDTO dto = new DynamicDTO();
		
		int result=0;
		try {
						
			if (dto.load(formBean)) {
				if( dto.unload(userEUP)){ 
					result = userEUPDao.update(userEUP);
					//userEUPDao.close();
				}	
			}	   
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 
		
		return result;
	}
	
	public int update(CNTRLBTH userBTH) {
		
		CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
				
		
		int result=0;
		try {
						
			result = userBTHDao.update(userBTH);
			//userBTHDao.close();			    
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 
		
		return result;
	}
	
	public int update(CNTRLEUP userEUP) {
		
		CNTRLEUPDao userEUPDao = new CNTRLEUPDao("AS400");		
		
		int result=0;
		try {
						
			result = userEUPDao.update(userEUP);
			//userEUPDao.close();			    
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 
		
		return result;
	}
	
	public int addUser(CNTRLBTH userBTH,CNTRLEUP userEUP) {
		
		CNTRLEUPDao userEUPDao = new CNTRLEUPDao("AS400");
		CNTRLBTHDao userBTHDao = new CNTRLBTHDao("AS400");
		CNTRLDBADao userDBADao = new CNTRLDBADao("AS400");		
		CNTRLDBA dbaBean = null;
		int result=0;
		try {
						
			result = userBTHDao.insert(userBTH);
			//userBTHDao.close();
			if (result != 0) {
				result = userEUPDao.insert(userEUP);
				//userEUPDao.close();
			}
			if (result != 0) {
				dbaBean = new CNTRLDBA();
				dbaBean.setDBAUSR(userBTH.getBTHKEY());
				dbaBean.setDBABNK(userBTH.getBTHUBK());
				dbaBean.setDBABRN(userBTH.getBTHUBR());				
				result = userDBADao.insert(dbaBean);
				//userDBADao.close();
			}			    
		} catch (Exception e) {
			//flexLog("Error: " + e);
		} 
		
		
		return result;
	}
	
	public JBObjList getUserBranch(String USERID) {

		CNTRLDBADao userDBADao = new CNTRLDBADao("AS400");
		
		JBObjList userBean = new JBObjList();
		try {
			
			userBean = userDBADao.select(USERID);
			//userDBADao.close();
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return userBean;
	}
	
	public CNTRLDBA getUserBranch(String USERID,String BANK,String BRANCH,String FLAGALL) {

		CNTRLDBADao userDBADao = new CNTRLDBADao("AS400");
		CNTRLDBA userDBABean = null;
		
		try {
			
			userDBABean = userDBADao.select(USERID,BANK,BRANCH,FLAGALL);
			//userDBADao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return userDBABean;
	}
	
	public int deleteUserBranch(String USERID,String BANK) {

		CNTRLDBADao userDBADao = new CNTRLDBADao("AS400");
		int result = 0;
		
		try {
			
			result = userDBADao.delete(USERID,BANK);
			//userDBADao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
	
	public int deleteUserBranch(String USERID,String BANK,String BRANCH, String FLAGALL) {

		CNTRLDBADao userDBADao = new CNTRLDBADao("AS400");
		int result = 0;
		
		try {
			
			result = userDBADao.delete(USERID,BANK,BRANCH,FLAGALL);
			//userDBADao.close();
			
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
	
	public int addUserBranch(String USERID,String BANK,String BRANCH,String FLAGALL) {

		CNTRLDBADao userDBADao = new CNTRLDBADao("AS400");
		CNTRLDBA bean = new CNTRLDBA();
		int result = 0;
		
		try {
			bean.setDBAUSR(USERID);
			bean.setDBABNK(BANK);
			bean.setDBABRN(new BigDecimal(BRANCH));
			bean.setDBAALL(FLAGALL);
			result = userDBADao.insert(bean);
			//userDBADao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
	
	public JBObjList getUserPrivilege(String USERID) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		
		JBObjList userBean = new JBObjList();
		try {
			
			userBean = userPVLDao.select(USERID);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return userBean;
	}
	
	public CNTRLPVL getUserPrivilege(String USERID,String PVLCOD) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		CNTRLPVL userPVLBean = null;
		
		try {
			
			userPVLBean = userPVLDao.select(USERID,PVLCOD);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return userPVLBean;
	}
	
	public int deleteUserPrivilege(String USERID) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		int result = 0;
		
		try {
			
			result = userPVLDao.delete(USERID);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
	
	public int deleteUserPrivilege(String USERID,String PVLCOD) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		int result = 0;
		
		try {
			
			result = userPVLDao.delete(USERID,PVLCOD);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
	
	public int addUserPrivilege(String USERID,String PVLCOD,String PVLVAL) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		CNTRLPVL bean = new CNTRLPVL();
		int result = 0;
		
		try {
			bean.setPVLUSR(USERID);
			bean.setPVLPVL(PVLCOD);
			bean.setPVLVAL(PVLVAL);
			result = userPVLDao.insert(bean);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
	
	public int updateUserPrivilege(String USERID,String PVLCOD,String PVLVAL) {

		CNTRLPVLDao userPVLDao = new CNTRLPVLDao("AS400");
		CNTRLPVL bean = new CNTRLPVL();
		int result = 0;
		
		try {
			bean.setPVLUSR(USERID);
			bean.setPVLPVL(PVLCOD);
			bean.setPVLVAL(PVLVAL);
			result = userPVLDao.update(bean);
			//userPVLDao.close();
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
		 
		return result;
	}
}
