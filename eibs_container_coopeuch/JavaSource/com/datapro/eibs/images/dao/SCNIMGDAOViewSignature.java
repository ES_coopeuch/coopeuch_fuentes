package com.datapro.eibs.images.dao;

import com.datapro.access.Key;
import com.datapro.eibs.access.DAOViewSuper;
import com.datapro.eibs.images.key.SCNIMGKEYViewSignature;
import com.datapro.eibs.images.md.SCNIMGMDViewSignature;

public class SCNIMGDAOViewSignature extends DAOViewSuper {

	protected Object[] getFindByPrimaryKeyArguments(Key keyObj) {
		SCNIMGKEYViewSignature key = (SCNIMGKEYViewSignature) keyObj;
		SCNIMGMDViewSignature md = new SCNIMGMDViewSignature();
		Object[] args = { key.getTBLNUM(), formatKeyValue(key.getTBLTBN(), md.getTBLTBN()), key.getTBLSEQ() };
		return args;
	}

	protected String getFindByPrimaryKeySql() {
		String sql = "SELECT TOP 1 TBLDSC, IMGBIN, IMGLEN " +
					 "FROM SCNDOCTBL " +
					 "INNER JOIN SCNDOCIMG ON TBLUID=IMGUID " +
					 "WHERE TBLTYP='C' AND TBLNUM=? AND TBLTBN=? AND TBLSEQ=? AND TBLDTY='SC' " +
					 "ORDER BY TBLSSQ DESC";
		return sql;
	}

	protected String getFindSql() {
		// TODO Auto-generated method stub
		return null;
	}

	protected Object[] getFindArguments(Key keyObj) {
		// TODO Auto-generated method stub
		return null;
	}

}
