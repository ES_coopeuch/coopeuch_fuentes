<!-- Hecho por Alonso Arana ------Datapro-----21/01/2014 -->
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ERC001001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Conciliaci�n Bancos</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="ERClist" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

   $(function(){
				
					$("#radio_key").attr("checked", false);
                
				});


  function goAction(op,index) {
	var ok = false;
	var cun = "";
	var pg = "";

if(op=='200'){
	document.forms[0].SCREEN.value = op;
			document.forms[0].submit();	
}

	if (op != '200'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01RCCTCD") 
	      	{	
	      		if (element.checked == true) {
	      			document.getElementById("codigo_lista").value = element.value; 
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {
      	var confirm1 = true;
      	
      	if (op =='202'){
      		confirm1 = confirm("Desea Eliminar este Registro Seleccionado?");
      	}
      	
		if (confirm1){
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}		

     } else {
		alert("Debe seleccionar un registro para continuar.");	   
	 }    
	}







function goAction2(op,index) {
	var ok = false;
	var cun = "";
	var pg = "";

  // document.forms[0].elements[index].checked = true;

$("input[name=E01RCCTCD][value=" + index + "]").prop('checked', true); 
	if (op != '200'){	//Checks something is selected
	 	for(n=0; n<document.forms[0].elements.length; n++)
	     {
	      	var element = document.forms[0].elements[n];
	      	if(element.name == "E01RCCTCD") 
	      	{	
	      		if (element.checked == true) {
	      			document.getElementById("codigo_lista").value = element.value; 
        			ok = true;
        			break;
				}
	      	}
	      }
      } else {
      	ok = true;
      }
      
      if ( ok ) {	
      	
	
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
			

       
	}
	
	}




</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Mantenedor de Movimientos de Bancos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PRDRO_list.jsp,EPV1216"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSERC0010">
<input type="hidden" name="SCREEN" value="201">
<input type="hidden" name="codigo_lista" value="" id="codigo_lista">  

 
 
   <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Codigo del banco : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="codigo_banco" value="<%=session.getAttribute("codigo_banco")%>" 
	  			size="4"  readonly>
             </td>
             <td nowrap width="10%" align="right">Nombre del Banco : 
               </td>
             <td nowrap width="50%"align="left">
	  	<input type="text" name="nombre_banco" value="<%=session.getAttribute("nombre_banco") %>" size="40" readonly>
             </td>
             
             
         </tr>
        </table>
      </td>
    </tr>
  </table>
 
 
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="20%"> 
			<a href="javascript:goAction('200')"><b>Crear</b></a>
		</td>
		<td align="center" class="tdbkg" width="20%">
			<a href="javascript:goAction('201')"> <b>Modificar</b> </a>
		</td>
		<td align="center" class="tdbkg" width="20%"><a
			href="javascript:goAction('202')"> <b>Borrar</b> </a></td>
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>

<%
	if (ERClist.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>

	<table id="headTable"  width="100%" align="left">
		<tr id="trdark">
			<th align="center" nowrap width="30"></th>
			<th align="center" nowrap width="100">Codigo Movimiento</th>

						<th align="center" nowrap width="400">Glosa</th>
						<th align="center" nowrap width="100">Tipo</th>
						<th align="center" nowrap width="200">Sub-Tipo</th>
			<th align="center" nowrap width=""></th>
			
					
		</tr>
		<%
			ERClist.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (ERClist.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ERC001001Message pvprd = (ERC001001Message) ERClist.getRecord();
		%>
		<tr>
			
			<td nowrap><input type="radio" name="E01RCCTCD" id="radio_key" value="<%=ERClist.getCurrentRow() %>" <%=chk%>/></td>
			<td nowrap align="center"><a href="javascript:goAction2('203','<%=ERClist.getCurrentRow()%>');"><%=pvprd.getE01RCCTCD() %></a></td>

			<td nowrap align="left"><a href="javascript:goAction2('203','<%=ERClist.getCurrentRow()%>');"><%=pvprd.getE01RCCNME()  %></a></td>
			<td nowrap align="center"><a href="javascript:goAction2('203','<%=ERClist.getCurrentRow()%>');"><%
			if(pvprd.getE01RCCDCC().equals("D")){
			out.print("D�BITO");
			
			}
			if(pvprd.getE01RCCDCC().equals("C")){
		
			out.print("CR�DITO");
				}
			
			  %></a></td>
			<td nowrap align="center"><a href="javascript:goAction2('203','<%=pvprd.getE01RCCTCD()%>');"></a>
			<%
			
			if (pvprd.getE01RCCSCO().equals("AB")){
			
				out.print("ABONO");
			}
			
				if (pvprd.getE01RCCSCO().equals("DP")){
			
				out.print("DEP�SITO");
			}
			
			
				if (pvprd.getE01RCCSCO().equals("CH")){
			
				out.print("CHEQUE");
			}
		
			if (pvprd.getE01RCCSCO().equals("CA")){
			
				out.print("CARGO");
			}	
			
			 %>
			</td>	
	
			
		    <td></td>
		

		</tr>
		<%
			}
		%>
	</table>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (ERClist.getShowPrev()) {
					int pos = ERClist.getFirstRec() - 13;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1217?SCREEN=100&codNum="
									+ request.getAttribute("codNum")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (ERClist.getShowNext()) {
					int pos = ERClist.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1217?SCREEN=100&codNum="
									+ request.getAttribute("codNum")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>
</form>
</body>
</html>
