package datapro.eibs.products;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ERC004001Message;
import datapro.eibs.beans.ERC004003Message;
import datapro.eibs.beans.ERC004005Message;
import datapro.eibs.beans.ERC200001Message;
import datapro.eibs.beans.ERC200002Message;
import datapro.eibs.beans.ERC200003Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ExcelColStyle;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.sockets.MessageField;

public class JSERC2000 extends JSEIBSServlet {
	
	private static final long serialVersionUID = 1L;

	protected static final int A_enter_statement = 100;
	protected static final int A_list_statement = 200;
	protected static final int A_inquiry_statement = 300;
	protected static final int A_excel_generate = 600;
	protected static final int A_reconciliation_automatic = 801;
	protected static final int A_delete_statement = 900;
	protected static final int A_reconciliation_manual = 400;
	protected static final int A_desconciliacion = 500;
	protected static final int R_desconcilacion_response = 650;
	
	public JSERC2000() {
	}

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case A_enter_statement: // 'd'
			procReqStatement(user, req, res, session);
			break;

		case A_list_statement:
			procActionStatementList(user, req, res, session);
			break;

		case A_inquiry_statement:
			procActionStatementDetail(user, req, res, session);
			break;

		case A_excel_generate:
			generateExcelDocument(user, req, res, session);
			break;

		case A_delete_statement:
			procActionStatementEliminar(user, req, res, session);
			break;

		case A_reconciliation_automatic:
			procActionStatementAutomaticReconcilation(user, req, res, session);
			break;

		case A_reconciliation_manual:
			procReqManual(user, req, res, session);
			break;

		case A_desconciliacion:
			procCargaDesconcilacion(user, req, res, session);
			break;

		case R_desconcilacion_response:
			proqDesconcilacion(user, req, res, session);
			break;

		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	protected void procReqStatement(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		UserPos userPO = (UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		try {
			flexLog("About to call Page: ERC2000_statement_enter.jsp");
			forward("ERC2000_statement_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e)
					.toString());
		}
	}

	protected void procReqManual(ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession ses) throws ServletException,
			IOException {
		UserPos userPO = (UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		try {
			flexLog("About to call Page: ERC2000_statement_enter.jsp");
			forward("ERC2000_reconcilation_manual_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e)
					.toString());
		}
	}

	protected void procActionStatementList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2000", req);
			ERC200001Message msgList = (ERC200001Message) mp.getMessageRecord(
					"ERC200001", user.getH01USR(), "0015");
			if (req.getParameter("E01BRMEID") != null) {
				String codigo_banco = req.getParameter("E01BRMEID");
				msgList.setE01RCHRBK(codigo_banco);
			}
			if (req.getParameter("E01BRMCTA") != null) {
				String cuenta_banco = req.getParameter("E01BRMCTA");
				msgList.setE01RCHCTA(cuenta_banco);
			}
			if (req.getParameter("posicion") != null)
				msgList.setE01RCHREC(req.getParameter("posicion"));
			if (req.getParameter("mark") != null)
				flexLog((new StringBuilder("marker:")).append(
						req.getParameter("mark")).toString());
			if (req.getParameter("E01RCHSTN") != null) {
				int numero = Integer.parseInt(req.getParameter("E01RCHSTN"));
				if (numero > 0)
					msgList.setE01RCHAPF("T");
			}
			setMessageRecord(req, msgList);
			String fecha1 = (new StringBuilder(String.valueOf(req
					.getParameter("E01RCHFSD")))).append("/").append(
					req.getParameter("E01RCHFSM")).append("/").append(
					req.getParameter("E01RCHFSY")).toString();
			String fecha2 = (new StringBuilder(String.valueOf(req
					.getParameter("E01RCHFLD")))).append("/").append(
					req.getParameter("E01RCHFLM")).append("/").append(
					req.getParameter("E01RCHFLY")).toString();
			req.setAttribute("fecha_inicial", fecha1);
			req.setAttribute("fecha_final", fecha2);
			if (req.getParameter("E01RCHSTN") != null)
				req.setAttribute("numero_cartola", req
						.getParameter("E01RCHSTN"));
			if (req.getParameter("E01RCHAPF") != null)
				req.setAttribute("estado", req.getParameter("E01RCHAPF"));
			session.setAttribute("lista_entrada", msgList);


			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC2000_statement_enter.jsp");
				forward("ERC2000_statement_enter.jsp", req, res);
			} else {
				JBObjList list = mp.receiveMessageRecordList("H01FLGMAS",
						"E01RCHREC");
				session.setAttribute("ERClist", list);
				session.setAttribute("codigo_banco", req
						.getParameter("E01BRMEID"));
				session.setAttribute("cuenta_banco", req
						.getParameter("E01BRMCTA"));
				session.setAttribute("nombre_banco", req
						.getParameter("E01DSCRBK"));
				session.setAttribute("cuenta_ibs", req
						.getParameter("E01BRMACC"));
				forwardOnSuccess("ERC2000_statement_list.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionStatementDetail(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2000", req);
			ERC200001Message msgList = (ERC200001Message) mp.getMessageRecord(
					"ERC200001", user.getH01USR(), "0004");
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC200001Message listMessage = (ERC200001Message) list
						.get(index);
				msgList.setE01RCHRBK(listMessage.getE01RCHRBK());
				msgList.setE01RCHCTA(listMessage.getE01RCHCTA());
				msgList.setE01RCHSTN(listMessage.getE01RCHSTN());
				req.setAttribute("codigo_lista", Integer.valueOf(index));
				session.setAttribute("ERCSeleccion", listMessage);
			}
			if (req.getParameter("posicion") != null)
				msgList.setE01RCHREC(req.getParameter("posicion"));
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			String fecha1 = (new StringBuilder(String.valueOf(req
					.getParameter("E01RCHFSD")))).append("/").append(
					req.getParameter("E01RCHFSM")).append("/").append(
					req.getParameter("E01RCHFSY")).toString();
			String fecha2 = (new StringBuilder(String.valueOf(req
					.getParameter("E01RCHFLD")))).append("/").append(
					req.getParameter("E01RCHFLM")).append("/").append(
					req.getParameter("E01RCHFLY")).toString();
			if (fecha1 != null) {
				session
						.setAttribute("E01RCHFSD", req
								.getParameter("E01RCHFSD"));
				session
						.setAttribute("E01RCHFSM", req
								.getParameter("E01RCHFSM"));
				session
						.setAttribute("E01RCHFSY", req
								.getParameter("E01RCHFSY"));
			}
			if (fecha2 != null) {
				session
						.setAttribute("E01RCHFLD", req
								.getParameter("E01RCHFLD"));
				session
						.setAttribute("E01RCHFLM", req
								.getParameter("E01RCHFLM"));
				session
						.setAttribute("E01RCHFLY", req
								.getParameter("E01RCHFLY"));
			}
			req.setAttribute("fecha_inicial", fecha1);
			req.setAttribute("fecha_final", fecha2);
			if (req.getParameter("E01RCHSTN") != null)
				req.setAttribute("numero_cartola", req
						.getParameter("E01RCHSTN"));
			if (req.getParameter("E01RCHAPF") != null)
				req.setAttribute("estado", req.getParameter("E01RCHAPF"));
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC2000_statement_list.jsp");
				forward("ERC2000_statement_list.jsp", req, res);
			} else {
				JBObjList list = mp.receiveMessageRecordList("H02FLGMAS",
						"E02RCSREC");
				flexLog((new StringBuilder("listaDetalle")).append(list)
						.toString());
				if (!list.isEmpty()) {
					ERC200002Message listMessage = (ERC200002Message) list
							.get(0);
					if (listMessage.getH02FLGWK3().equals("E")) {
						session.setAttribute("ErrorList", list);
						forwardOnSuccess("ERC2000_statement_error.jsp", req,
								res);
					} else {
						session.setAttribute("ERC", list);
						forwardOnSuccess("ERC2000_statement_detail.jsp", req,
								res);
					}
				}
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	private void generateExcelDocument(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws IOException, ServletException {
		MessageProcessor mp;
		mp = null;
		ELEERRMessage msgError = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2000", req);
			ERC200001Message msgList = (ERC200001Message) mp.getMessageRecord(
					"ERC200001", user.getH01USR(), "1000");
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC200001Message listMessage = (ERC200001Message) list
						.get(index);
				msgList.setE01RCHRBK(listMessage.getE01RCHRBK());
				msgList.setE01RCHCTA(listMessage.getE01RCHCTA());
				msgList.setE01RCHSTN(listMessage.getE01RCHSTN());
			}
			flexLog((new StringBuilder("mensaje_excel:")).append(msgList)
					.toString());
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				ERC200001Message listMessage = (ERC200001Message) session
						.getAttribute("lista_entrada");
				String fecha1 = (new StringBuilder(String.valueOf(req
						.getParameter("E01RCHFSD")))).append("/").append(
						req.getParameter("E01RCHFSM")).append("/").append(
						req.getParameter("E01RCHFSY")).toString();
				String fecha2 = (new StringBuilder(String.valueOf(req
						.getParameter("E01RCHFLD")))).append("/").append(
						req.getParameter("E01RCHFLM")).append("/").append(
						req.getParameter("E01RCHFLY")).toString();
				req.setAttribute("fecha_inicial", fecha1);
				req.setAttribute("fecha_final", fecha2);
				if (req.getParameter("E01RCHSTN") != null)
					req.setAttribute("numero_cartola", req
							.getParameter("E01RCHSTN"));
				if (req.getParameter("E01RCHAPF") != null)
					req.setAttribute("estado", req.getParameter("E01RCHAPF"));
				forward("ERC2000_statement_list.jsp", req, res);
			} else {
				JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");
				flexLog((new StringBuilder("mensaje_excel venida:")).append(
						list).toString());
				if (!list.isEmpty()) {
					ERC200003Message msg = (ERC200003Message) list.get(0);
					String fileName = (new StringBuilder(String.valueOf(msg
							.getNROCARTO()))).append("_")
							.append(msg.getBANCO()).append("_").append(
									msg.getCTABANCO()).append("_").append(
									getTimeStamp()).append(".xls").toString();
					String excelName = (new StringBuilder(
							"attachment; filename=\"")).append(fileName)
							.append("\"").toString();
					res.setContentType("application/vnd.ms-excel");
					res.setHeader("Content-disposition", excelName);
					Vector fields = new Vector();
					for (Enumeration enu = msg.fieldEnumeration(); enu
							.hasMoreElements();) {
						MessageField field = (MessageField) enu.nextElement();
						String name = field.getTag();
						if (!"H03USERID".equals(name)
								&& !"H03PROGRM".equals(name)
								&& !"H03TIMSYS".equals(name)
								&& !"H03SCRCOD".equals(name)
								&& !"H03OPECOD".equals(name)
								&& !"H03FLGMAS".equals(name)
								&& !"H03FLGWK1".equals(name)
								&& !"H03FLGWK2".equals(name)
								&& !"H03FLGWK3".equals(name)) {
							ExcelColStyle style = new ExcelColStyle();
							style.setName(name);
							style.setHidden(false);
							style.setLocked(false);
							fields.add(style);
						}
					}

					OutputStream out = ExcelUtils.getWorkBook(res
							.getOutputStream(), fields, list, false);
					out.flush();
				}
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionStatementEliminar(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2000", req);
			ERC200001Message msgList = (ERC200001Message) mp.getMessageRecord(
					"ERC200001", user.getH01USR(), "0009");
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC200001Message listMessage = (ERC200001Message) list
						.get(index);
				msgList.setE01RCHRBK(listMessage.getE01RCHRBK());
				msgList.setE01RCHCTA(listMessage.getE01RCHCTA());
				msgList.setE01RCHSTN(listMessage.getE01RCHSTN());
			}
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC2000_statement_list.jsp");
				forward("ERC2000_statement_list.jsp", req, res);
			} else {
				procActionStatementList(user, req, res, session);
			}
		} finally {
			if (mp != null)
				mp.close();
			return;
		}
	}

	protected void procActionStatementListAux(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2000", req);
			ERC200001Message msgList = (ERC200001Message) mp.getMessageRecord(
					"ERC200001", user.getH01USR(), "0015");
			ERC200001Message list = (ERC200001Message) session
					.getAttribute("lista_seleccionada");
			if (!list.isEmpty()) {
				msgList.setE01RCHRBK(list.getE01RCHBNK());
				msgList.setE01RCHCTA(list.getE01RCHCTA());
				int numero = Integer.parseInt(list.getE01RCHSTN());
				if (numero > 0)
					msgList.setE01RCHAPF("T");
			}
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				flexLog("About to call Page: ERC2000_statement_list.jsp");
				forward("ERC2000_statement_list.jsp", req, res);
			} else {
				JBObjList list2 = mp.receiveMessageRecordList("H01FLGMAS");
				session.setAttribute("ERClist", list2);
				forwardOnSuccess("ERC2000_statement_list.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionStatementAutomaticReconcilation(
			ESS0030DSMessage user, HttpServletRequest req,
			HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		try {
			mp = getMessageProcessor("ERC2000", req);
			ERC200001Message msgList = (ERC200001Message) mp.getMessageRecord(
					"ERC200001", user.getH01USR(), "0022");
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				ERC200001Message listMessage = (ERC200001Message) list
						.get(index);
				msgList.setE01RCHRBK(listMessage.getE01RCHRBK());
				msgList.setE01RCHCTA(listMessage.getE01RCHCTA());
				msgList.setE01RCHSTN(listMessage.getE01RCHSTN());
				session.setAttribute("ERClist", list);
				session.setAttribute("ERCSeleccion", listMessage);
			}
			mp.sendMessage(msgList);
			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) {
				session.setAttribute("error", error);
				ERC200001Message listMessage = (ERC200001Message) session
						.getAttribute("lista_entrada");
				String fecha1 = (new StringBuilder(String.valueOf(req
						.getParameter("E01RCHFSD")))).append("/").append(
						req.getParameter("E01RCHFSM")).append("/").append(
						req.getParameter("E01RCHFSY")).toString();
				String fecha2 = (new StringBuilder(String.valueOf(req
						.getParameter("E01RCHFLD")))).append("/").append(
						req.getParameter("E01RCHFLM")).append("/").append(
						req.getParameter("E01RCHFLY")).toString();
				req.setAttribute("fecha_inicial", fecha1);
				req.setAttribute("fecha_final", fecha2);
				if (req.getParameter("E01RCHSTN") != null)
					req.setAttribute("numero_cartola", req
							.getParameter("E01RCHSTN"));
				if (req.getParameter("E01RCHAPF") != null)
					req.setAttribute("estado", req.getParameter("E01RCHAPF"));
				forward("ERC2000_statement_list.jsp", req, res);
			} else {
				procActionStatementList(user, req, res, session);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	private void procCargaDesconcilacion(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws IOException, ServletException {
		UserPos userPO;
		MessageProcessor mp;
		userPO = getUserPos(session);
		mp = null;
		
		try {
			mp = getMessageProcessor("ERC0040", req);
			ERC004001Message msg = (ERC004001Message) mp.getMessageRecord("ERC004001");
			msg.setH01USERID(user.getH01USR());
			msg.setH01PROGRM("ERC0040");
			msg.setH01OPECOD("0015");
			msg.setH01TIMSYS(getTimeStamp());
			msg.setH01FLGWK3("D");
			String codigo_lista = "";
			
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				int index = Integer.parseInt(req.getParameter("codigo_lista"));
				ERC200001Message listMessage = (ERC200001Message) list.get(index);
				
				msg.setE01RCHRBK(listMessage.getE01RCHRBK());
				msg.setE01BRMCTA(listMessage.getE01RCHCTA());
				msg.setE01CARSTN(listMessage.getE01RCHSTN());
				msg.setE01BRMACC(listMessage.getE01RCHACC());
				
				if (req.getParameter("E01HASDDD") != null) {
					msg.setE01HASDDD(req.getParameter("E01HASDDD"));
					msg.setE01HASDDM(req.getParameter("E01HASDDM"));
					msg.setE01HASDDY(req.getParameter("E01HASDDY"));
				} else {
					msg.setE01HASDDD("0");
					msg.setE01HASDDM("0");
					msg.setE01HASDDY("0");
				}
			} else {
				msg.setE01RCHRBK("");
				msg.setE01BRMCTA("");
				msg.setE01CARSTN("");
				msg.setE01BRMACC("");
			}
			mp.sendMessage(msg);
		
			ELEERRMessage msgError = (ELEERRMessage) mp.receiveMessageRecord("ELEERR");
			if (mp.hasError(msgError)) 
			{
				session.setAttribute("error", msgError);
				ERC200001Message listMessage = (ERC200001Message) session.getAttribute("lista_entrada");
				session.setAttribute("lista_entrada", listMessage);
				JBObjList list = (JBObjList) session.getAttribute("ERClist");
				session.setAttribute("ERClist", list);
				session.setAttribute("userPO", userPO);
				session.setAttribute("codigo_banco", req.getParameter("E01BRMEID"));
				session.setAttribute("cuenta_banco", req.getParameter("E01BRMCTA"));
				session.setAttribute("nombre_banco", req.getParameter("E01DSCRBK"));
				session.setAttribute("cuenta_ibs", req.getParameter("E01BRMACC"));
				String fecha1 = (new StringBuilder(String.valueOf(req.getParameter("E01RCHFSD")))).append("/").append(req.getParameter("E01RCHFSM")).append("/").append(req.getParameter("E01RCHFSY")).toString();
				String fecha2 = (new StringBuilder(String.valueOf(req.getParameter("E01RCHFLD")))).append("/").append(req.getParameter("E01RCHFLM")).append("/").append(req.getParameter("E01RCHFLY")).toString();
				req.setAttribute("fecha_inicial", fecha1);
				req.setAttribute("fecha_final", fecha2);
				
				if (req.getParameter("E01RCHAPF") != null)
					req.setAttribute("estado", req.getParameter("E01RCHAPF"));
				if (req.getParameter("E01RCHSTN") != null)
					req.setAttribute("numero_cartola", req.getParameter("E01RCHSTN"));
				
				forward("ERC2000_statement_list.jsp", req, res);
			} 
			else 
			{
				session.setAttribute("userPO", userPO);
			
				msg = (ERC004001Message) mp.receiveMessageRecord("ERC004001");
				JBObjList ibsList = mp.receiveMessageRecordList("H02FLGMAS");
				JBObjList carList = mp.receiveMessageRecordList("H03FLGMAS");
				
				req.setAttribute("codigo_lista", req.getParameter("codigo_lista"));
				session.setAttribute("msg", msg);
				session.setAttribute("carList", carList);
				session.setAttribute("ibsList", ibsList);
				if (req.getParameter("E01RCHAPF") != null)
					req.setAttribute("estado", req.getParameter("E01RCHAPF"));
					session.setAttribute("codigo_banco", req.getParameter("E01BRMEID"));
					session.setAttribute("cuenta_banco", req.getParameter("E01BRMCTA"));
					session.setAttribute("nombre_banco", req.getParameter("E01DSCRBK"));
					session.setAttribute("cuenta_ibs", req.getParameter("E01BRMACC"));
					String fecha1 = (new StringBuilder(String.valueOf(req.getParameter("E01RCHFSD")))).append("/").append(req.getParameter("E01RCHFSM")).append("/").append(req.getParameter("E01RCHFSY")).toString();
					String fecha2 = (new StringBuilder(String.valueOf(req.getParameter("E01RCHFLD")))).append("/").append(req.getParameter("E01RCHFLM")).append("/").append(req.getParameter("E01RCHFLY")).toString();
					req.setAttribute("fecha_inicial", fecha1);
					req.setAttribute("fecha_final", fecha2);
				
					if (req.getParameter("E01RCHSTN") != null) 
					{
						req.setAttribute("E01Cartola", req.getParameter("E01RCHSTN"));
						req.setAttribute("numero_cartola", req.getParameter("E01RCHSTN"));
					}
				
					if (req.getParameter("E01RCHREC") != null)
						req.setAttribute("E01RCHREC", req.getParameter("E01RCHREC"));
				
					forward("ERC2000_reconciliacion_list.jsp", req, res);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	private void proqDesconcilacion(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws IOException, ServletException {
		UserPos userPO;
		MessageProcessor mp;
		userPO = getUserPos(session);
		mp = null;
		try {
			mp = getMessageProcessor("ERC0040", req);
			ERC004005Message msg = (ERC004005Message) mp
					.getMessageRecord("ERC004005");
			msg.setH05USERID(user.getH01USR());
			msg.setH05PROGRM("ERC0040");
			msg.setH05OPECOD("0024");
			msg.setH05TIMSYS(getTimeStamp());
			if (req.getParameter("codigo_lista") != null) {
				JBObjList list = (JBObjList) session.getAttribute("carList");
				int index = Integer.parseInt(req.getParameter("codigo_lista"));
				ERC004003Message listMessage = (ERC004003Message) list
						.get(index);
				msg.setE05RCHRBK(listMessage.getE03RCSRBK());
				msg.setE05BRMCTA(listMessage.getE03RCSCTA());
				msg.setE05CARSTN(listMessage.getE03RCSSTN());
				msg.setE05NROCON(listMessage.getE03RCSCNU());
			}
			flexLog((new StringBuilder("mensaje clase 4005:")).append(msg)
					.toString());
			mp.sendMessage(msg);
			ELEERRMessage msgError = (ELEERRMessage) mp
					.receiveMessageRecord("ELEERR");
			if (mp.hasError(msgError)) {
				session.setAttribute("error", msgError);
				JBObjList list = (JBObjList) session.getAttribute("carList");
				session.setAttribute("carList", list);
				session.setAttribute("userPO", userPO);
				session.setAttribute("codigo_banco", req
						.getParameter("E01BRMEID"));
				session.setAttribute("cuenta_banco", req
						.getParameter("E01BRMCTA"));
				session.setAttribute("nombre_banco", req
						.getParameter("E01DSCRBK"));
				session.setAttribute("cuenta_ibs", req
						.getParameter("E01BRMACC"));
				String fecha1 = (new StringBuilder(String.valueOf(req
						.getParameter("E01RCHFSD")))).append("/").append(
						req.getParameter("E01RCHFSM")).append("/").append(
						req.getParameter("E01RCHFSY")).toString();
				String fecha2 = (new StringBuilder(String.valueOf(req
						.getParameter("E01RCHFLD")))).append("/").append(
						req.getParameter("E01RCHFLM")).append("/").append(
						req.getParameter("E01RCHFLY")).toString();
				req.setAttribute("fecha_inicial", fecha1);
				req.setAttribute("fecha_final", fecha2);
				if (req.getParameter("E01RCHAPF") != null)
					req.setAttribute("estado", req.getParameter("E01RCHAPF"));
				if (req.getParameter("E01RCHSTN") != null)
					req.setAttribute("numero_cartola", req
							.getParameter("E01RCHSTN"));
				forward("ERC2000_statement_list.jsp", req, res);
			} else {
				if (req.getParameter("E01RCHAPF") != null)
					req.setAttribute("estado", req.getParameter("E01RCHAPF"));
				session.setAttribute("codigo_banco", req
						.getParameter("E01BRMEID"));
				session.setAttribute("cuenta_banco", req
						.getParameter("E01BRMCTA"));
				session.setAttribute("nombre_banco", req
						.getParameter("E01DSCRBK"));
				session.setAttribute("cuenta_ibs", req
						.getParameter("E01BRMACC"));
				String fecha1 = (new StringBuilder(String.valueOf(req
						.getParameter("E01RCHFSD")))).append("/").append(
						req.getParameter("E01RCHFSM")).append("/").append(
						req.getParameter("E01RCHFSY")).toString();
				String fecha2 = (new StringBuilder(String.valueOf(req
						.getParameter("E01RCHFLD")))).append("/").append(
						req.getParameter("E01RCHFLM")).append("/").append(
						req.getParameter("E01RCHFLY")).toString();
				req.setAttribute("fecha_inicial", fecha1);
				req.setAttribute("fecha_final", fecha2);
				procActionStatementList(user, req, res, session);
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

}
