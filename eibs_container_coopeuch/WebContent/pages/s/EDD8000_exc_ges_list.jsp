<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page
	import="datapro.eibs.master.Util,datapro.eibs.beans.EDD800001Message"%>
<%@page import="com.datapro.constants.EibsFields"%>

<html>
<head>
<title>Gesti&oacute;n de Aprobaci&oacute;n Carga Masiva Cargos y Abonos Cuentas Vistas</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="ExCon" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"   scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos"    	   scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

$(function(){
$("#radio_key").attr("checked", false);
});

function goAction(op) {
var ok = false;

   		if (op =='1100' || op == '1200')
   		{
			document.forms[0].SCREEN.value = op;
			document.forms[0].submit();		
		}
 		else 
   		{
 			
			//VERIFICA UN REGISTRO SELECCIONADO
			for(n=0; n<document.forms[0].elements.length; n++)
			{
				var element = document.forms[0].elements[n];
		   	 if(element.name == "ID_Interfaz") 
		   	 {	
		   	 	if (element.checked == true) 
		   	 	{
		   	   		document.getElementById("codigo_lista").value = element.value; 
   	    	 		ok = true;
   	    	 		break;
				}
		 	 }
			}

			//SE SELECCIONO UN GEGISTRO      
    		if ( ok ) 
    		{
				var confirm1 = true;
      		
     		 	if (op =='1500')
      			{
      				confirm1 = confirm("�Desea Eliminar el Archivo Seleccionado?");
      			
					if (confirm1){
						document.forms[0].SCREEN.value = op;
						document.forms[0].submit();		
					}
				}
 				else
				{
					document.forms[0].SCREEN.value = op;
					document.forms[0].submit();		
				} 
     		} 
     		else 
     		{
				alert("Debe seleccionar un registro para continuar.");	   
	 		}    
		}
	}

</SCRIPT>

</head>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>



<H3 align="center">Gesti&oacute;n de Aprobaci&oacute;n Carga Masiva Cargos y Abonos Cuentas Vistas
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="exc_ges_list.jsp, EDD8000">
</H3>

<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD8000" target="main">
<input type="hidden" name="SCREEN" value="1200"> 
<input type="hidden" name="codigo_lista" value="" id="codigo_lista"> 


<%
	if (ExCon.getNoResult()) {
%>


<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center"><font size="3"> <b> No hay
		resultados que correspondan a su criterio de b�squeda. </b> </font></div>
		</td>
	</tr>
	<tr>		
	</tr>
</table>
<%
	} else {
%>

<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%">
			<a href="javascript:goAction('1300')"><b>Consultar</b></a>
		</td>
		<td align="center" class="tdbkg" width="10%"> 
			<a href="javascript:goAction('1400')"> <b>Aprobar</b></a> 
		</td>
		<td align="center" class="tdbkg" width="10%">
			<a href="javascript:goAction('1500')"><b>Rechazar</b></a> 
		</td>		
	</tr>
</table>


<table class="tbenter" width=100%>
	<tr>
		<td height="20"></td>
	</tr>

	<tr>
		<td>
		<table id="headTable" width="100%" align="left">
			<tr id="trdark">
				<th align="center" nowrap width="30"></th>
				<th align="center" nowrap width="100">ID</th>
				<th align="center" nowrap width="100">FECHA CARGA</th>
				<th align="center" nowrap width="120">CANTIDAD ABONO</th>
				<th align="center" nowrap width="120">MONTO ABONO</th>
				<th align="center" nowrap width="100">CANTIDAD CARGO</th>
				<th align="center" nowrap width="100">MONTO CARGO</th>
				<th align="center" nowrap width="100">ESTADO</th>
				<th align="center" nowrap width="150">GLOSA</th>
				<th align="center" nowrap width="100">FECHA ESTADO</th>
				<th align="center" nowrap width="100">USUARIO</th>
				<th align="center" nowrap width=""></th>
			</tr>

			<%
				ExCon.initRow();
					int k = 0;
					boolean firstTime = true;
					String chk = "";
					while (ExCon.getNextRow()) {

						EDD800001Message pvprd = (EDD800001Message) ExCon
								.getRecord();
			%>
			<tr>
				<td nowrap>
					<input type="radio" name="ID_Interfaz"	id="codigo_lista" value="<%=ExCon.getCurrentRow()%>" <%=chk%> />
				</td>
				<td nowrap align="center"><%=pvprd.getE01IDCARGA()%></td>
				<td nowrap align="center">
				<%
					out.print(pvprd.getE01CMHCAD() + "/" + pvprd.getE01CMHCAM()
									+ "/" + pvprd.getE01CMHCAY());
				%>
				</td>
				<td nowrap align="center"><%=pvprd.getE01TABONO()%></td>
				<td nowrap align="center"><%=pvprd.getE01MABONO()%></td>
				<td nowrap align="center"><%=pvprd.getE01TCARGO()%></td>
				<td nowrap align="center"><%=pvprd.getE01MCARGO()%></td>
				<td nowrap align="center">
				<% if(pvprd.getE01ESTCARG().equalsIgnoreCase("P")){
						out.print("PROCESADO");
					}else if(pvprd.getE01ESTCARG().equalsIgnoreCase("R")){
						out.print("RECHAZADO");
					}else if(pvprd.getE01ESTCARG().equalsIgnoreCase("A")){
						out.print("APROBADO");
					}else if(pvprd.getE01ESTCARG().equalsIgnoreCase("G")){
						out.print("GENERADO");
					}
				%>
				</td>
				<td nowrap align="center"><%=pvprd.getE01GLOCARG()%></td>				
				<td nowrap align="center">
				<%
					out.print(pvprd.getE01CMHCAD() + "/" + pvprd.getE01CMHCAM()
									+ "/" + pvprd.getE01CMHCAY());
				%>
				</td>
				<td nowrap align="left"><%=pvprd.getE01CMHCAU()%></td>
				<td nowrap align="center"></td>
			</tr>
			<%
				}
			%>
		</table>
		</td>
	</tr>
	<tr>
		<td>		
		</td>
	</tr>
</table>

<%
	}
%> 
     </form>
</body>
</html>
