//author: fhernandez

function EventElement(evt) {
    evt = (evt) ? evt : ((window.event) ? window.event : "");
	var elem;
    if (evt) {
        if (evt.target) {
			elem = evt.target;
			while(elem.nodeType != elem.ELEMENT_NODE){
				elem = elem.parentNode;
			}
        } else {
            elem = evt.srcElement;
        }
    }
    return elem;
}

function CallbackObject() {
	this.ajax;
	this.callback;
}

function CreateXMLHttpRequest() {
	try { return new XMLHttpRequest(); } catch(e) {}
	try { return new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) {}
	try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) {}
	return null;
}
//Parameter Asynchronus takes values: true or false
function GetXMLResponse(URL, callbackObject, asynchronus){
	ajax = CreateXMLHttpRequest();
	CallbackObject.ajax = ajax;
	ajax.onreadystatechange = callbackObject;
	ajax.open("GET", URL, asynchronus);
	ajax.send(null);
}

function PostXMLRequest(URL, data, callbackObject, asynchronus){
	ajax = CreateXMLHttpRequest();
	CallbackObject.ajax = ajax;
	ajax.onreadystatechange = ReadyStateHandler(callbackObject);
	ajax.open("POST", URL, asynchronus);
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(data);
}

function XMLResponse(ajax){
	// Comprobamos si la peticion se ha completado (estado 4) 
	if( ajax.readyState == 4 ) {
		// Comprobamos si la respuesta ha sido correcta (resultado HTTP 200)
		if( ajax.status == 200 ) {
	    	// Escribimos el resultado en la pagina HTML mediante DHTML	
			return ajax.responseXML;
		}
	}
}

function TextResponse(ajax){
	// Comprobamos si la peticion se ha completado (estado 4) 
	if( ajax.readyState == 4 ) {
		// Comprobamos si la respuesta ha sido correcta (resultado HTTP 200)
		if( ajax.status == 200 ) {
	    	// Escribimos el resultado en la pagina HTML mediante DHTML			
			return ajax.responseText;
		}
	}
}

function ReadyStateHandler(callbackHandler) {
  	// Return an anonymous function that listens to the 
  	// XMLHttpRequest instance
  	return function () {
	    // If the request's status is "complete"
	    if (CallbackObject.ajax.readyState == 4) {
	    	// Check that a successful server response was received
	      	if (CallbackObject.ajax.status == 200) {
	        	// Pass the payload of the response to the handler function
	        	callbackHandler(CallbackObject.ajax);
	      	} else {
	        	// An HTTP problem has occurred
	        	alert("HTTP error: " + CallbackObject.ajax.status);
	      	}
	    }
  	};
}

function selectCallback(){
	xmlResponse = XMLResponse(CallbackObject.ajax);	
	if(xmlResponse){
		var xmlSerial;
		if(isValidObject(window.XMLSerializer)){
			xmlSerial = new XMLSerializer();
		}
	    var items = xmlResponse.getElementsByTagName('select');
		for (var i = 0 ; i < items.length ; i++) {
			var item = items.item(i);
			var control = document.getElementsByName(item.getAttribute("name"));
			control.outerHTML=(item.xml ? item.xml : xmlSerial.serializeToString(item));
		}
	}
}

function inputCallback(){
	xmlResponse = XMLResponse(CallbackObject.ajax);	
	if(xmlResponse){
		var xmlSerial;
		if(isValidObject(window.XMLSerializer)){
			xmlSerial = new XMLSerializer();
		}
	    var items = xmlResponse.getElementsByTagName('input');
		for (var i = 0 ; i < items.length ; i++) {
			var item = items.item(i);
			var control = document.getElementsByName(item.getAttribute("name"));
			control.outerHTML=(item.xml ? item.xml : xmlSerial.serializeToString(item));
		}
	}
}


function tableCallback(){
	xmlResponse = XMLResponse(CallbackObject.ajax);
	if(xmlResponse){
		var xmlSerial;
		if(isValidObject(window.XMLSerializer)){
			xmlSerial = new XMLSerializer();
		}
	    var items = xmlResponse.getElementsByTagName('table');
		for (var i = 0 ; i < items.length ; i++) {
			var item = items.item(i);
			var control = document.getElementById(item.getAttribute("id"));
			control.outerHTML=(item.xml ? item.xml : xmlSerial.serializeToString(item));
		}
	}
}

function divCallback(){
	xmlResponse = XMLResponse(CallbackObject.ajax);
	if(xmlResponse){
		var xmlSerial;
		if(isValidObject(window.XMLSerializer)){
			xmlSerial = new XMLSerializer();
		}
	    var items = xmlResponse.getElementsByTagName('div');
		for (var i = 0 ; i < items.length ; i++) {
			var item = items.item(i);
			var control = document.getElementById(item.getAttribute("id"));
			control.outerHTML=(item.xml ? item.xml : xmlSerial.serializeToString(item));
		}
	}
}

function callWaiting(divName, waitImage, waitLabel){
  	var mWaitDiv = document.getElementById(divName);
   	mWaitDiv.innerHTML = "";
  	var iconWait = document.createElement('img');
   	iconWait.setAttribute("src", waitImage);
   	iconWait.setAttribute("align", "bottom" );
   	iconWait.setAttribute("border", "0");
    var textWait=document.createElement("span").appendChild(document.createTextNode(waitLabel));
    mWaitDiv.appendChild(iconWait);
    mWaitDiv.appendChild(textWait);
}

