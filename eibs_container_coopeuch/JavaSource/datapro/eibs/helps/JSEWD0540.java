package datapro.eibs.helps;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.EWD0540DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.sockets.MessageRecord;

public class JSEWD0540 extends JSEIBSServlet{


	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

			MessageProcessor mp = null;
		
			try {
				mp = getMessageProcessor("EWD0205", req);
		
				EWD0540DSMessage msgHelp = null;
		               
				// Send Request
				msgHelp = (EWD0540DSMessage)mp.getMessageRecord("EWD0540DS");
				msgHelp.setEWDSBK(req.getParameter("BNK"));
				msgHelp.setEWDSCY(req.getParameter("CCY"));				
	
				mp.sendMessage(msgHelp);
				
				JBObjList list = mp.receiveMessageRecordList("EWDOPE");
				
				session.setAttribute("helpList", list);
				forward("EWD0540_salesPlatform_formalizacion_branchs.jsp", req, res);
				
				
			}catch (Exception e) {
				// TODO: handle exception
			}

	}
}