<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import="datapro.eibs.beans.ECC015002Message"%>

<html>

<head>

<jsp:useBean id="cchData" class="datapro.eibs.beans.ECC015001Message" scope="session" />
<jsp:useBean id="ECC015002List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
	
<title>Resumen de Movimientos Nacionales de Tarjeta de Credito</title>

<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>


</head>

<BODY><form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC0150">
<INPUT TYPE=HIDDEN NAME="SCREEN" value="10">
<INPUT TYPE=HIDDEN NAME="opt" VALUE="1">

<TABLE  class="tableinfo" width="100%" CELLPADDING=0 CELLSPACING=0>	
        <tr id="trdark"> 
            <td nowrap align="left" width="80">
				<IMG src="<%=request.getContextPath()%>/images/logo1_mastercard.JPG" align="left" WIDTH=136 HEIGHT=46> 
		    </td>
		    <td nowrap align="right"  width="20">                 
		    	<IMG src="<%=request.getContextPath()%>/images/logo1_coopeuch.JPG" align="right" WIDTH=136 HEIGHT=46>
		    </td>
		</tr>
</TABLE>
		
<TABLE  class="tableinfo" width="100%" CELLPADDING=0 CELLSPACING=0>	
			<TR id="trclear">
				<td align="center" rowspan=0 ><h3>RESUMEN DE MOVIMIENTOS NACIONALES DE TARJETA DE CREDITO</h3></Td>			
			</TR>
		</TABLE>

<TABLE class="tableinfo" width="100%">
	<TR>
		<TD>
		 <table border=0 CELLPADDING=0 CELLSPACING=0 width="100%">		
			<TR id="trdark">
				<td width="30%" align="left" rowspan="3"><b>NOMBRE DEL TITULAR <br>No DE TARJETA DE CREDITO<br> FECHA RESUMEN DE MOVIMIENTOS</b></Td>
				<td width="70%" align="left" rowspan="3"><b><%=cchData.getE01CCHNOM() %></b><br>
														 <b>XXXX XXXX XXX <%=cchData.getE01CCHNXN() %></b><br>
														 <b><%=cchData.getE01CCHFFD()%>/<%=cchData.getE01CCHFFM()%>/<%=cchData.getE01CCHFFA()%></b>
				 </Td>
			</TR>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<br>
<TABLE  class="tableinfo" width="100%">
	<TR>
		<TD>
		 <table cellpadding=0 cellspacing=0 width="100%" border=0>		
			<TR id="trclear">
				<td align="left" rowspan=0 ><h3>I. INFORMACION GENERAL</h3></Td>			
			</TR>
		</TABLE>
		</TD>
	</TR>
	<TR>
		<TD>
		 <table border=1 frame="box" CELLPADDING=0 CELLSPACING=0 width="100%">
				<TR id="trdark">
				<td width="30%" ALIGN=CENTER nowrap> </td>
				<td width="20%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>CUPO TOTAL</b></Td>
				<td width="20%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>CUPO UTILIZADO</b></Td>
				<td width="20%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>CUPO DISPONIBLE</b></Td>
				<td width="10%" ALIGN=CENTER rowspan="2"></Td>			
			</TR>
			<TR id="trdark">
				<Td width="30%" ALIGN="left" nowrap><b>CUPO TOTAL</b></Td>
				<Td width="20%" ALIGN=CENTER nowrap>$<%= Util.formatCCY(cchData.getE01CCHCPE()) %></Td>
				<Td width="20%" ALIGN=CENTER nowrap>$<%= Util.formatCCY(cchData.getE01CCHMUP()) %></Td>
				<Td width="20%" ALIGN=CENTER nowrap>$<%= Util.formatCCY(cchData.getE01CCHCDP()) %></Td>			
			</TR>
			<TR id="trdark">
				<Td width="30%" ALIGN=CENTER nowrap> </Td>
				<Td width="20%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>ROTATIVO</b></Td>
				<Td width="20%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>CUOTAS</b></Td>
				<Td width="20%" ALIGN=CENTER rowspan="5" colspan="4" >			
			</TR>
			<TR id="trdark">
				<Th width="30%" ALIGN="left" nowrap>TASA INTERES VIGENTE</Th>
				<Td width="20%" ALIGN=CENTER nowrap><%= cchData.getE01CCHTPP() %></Td>
				<Td width="20%" ALIGN=CENTER nowrap><%= cchData.getE01CCH524() %></Td>		
			</TR>		
			<TR id="trdark">
				<Td width="30%" ALIGN=CENTER nowrap></Td>
				<Td width="20%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>DESDE</b></Td>
				<Td width="20%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>HASTA</b></Td>			
			</TR>
			<TR id="trdark">
				<Td width="30%" ALIGN="left" nowrap><b>PERIODO FACTURADO</b></Td>
				<Td width="20%" ALIGN=CENTER nowrap><%=cchData.getE01CCHVND()%>/<%=cchData.getE01CCHVNM()%>/<%=cchData.getE01CCHVNA()%></Td>
				<Td width="20%" ALIGN=CENTER nowrap><%=cchData.getE01CCHFFD()%>/<%=cchData.getE01CCHFFM()%>/<%=cchData.getE01CCHFFA()%></Td>		
			</TR>
			<TR id="trdark">
				<Td width="30%" ALIGN="left" nowrap><b> PAGAR HASTA</b></Td>
				<Td width="20%" ALIGN=CENTER colspan="2"><%=cchData.getE01CCHVTD()%>/<%=cchData.getE01CCHVTM()%>/<%=cchData.getE01CCHVTA()%> </Td>				
			</TR>
		</TABLE>
    </TD>
</TR>
</TABLE>
<TABLE  class="tableinfo">
	<TR>
		<TD width="100%">
		 <table cellpadding=0 cellspacing=0 width="100%" border=0>		
			<TR id="trclear">
				<td align="left" rowspan=0 ><h3>II. DETALLE</h3></Td>				
			</TR>
		</TABLE>
		 <table border=1 CELLPADDING=0 CELLSPACING=0 width="100%">
			<TR id="trclear">
				<td width="100%" align="left" nowrap><b>1. PERIODO ANTERIOR</b></Td>			
			</TR>
		</table>
		 <table border=1 frame="box" CELLPADDING=0 CELLSPACING=0 width="100%">
			<TR id="trdark">
				<td width="40%" align="left" nowrap><b><small>FECHA DE FACTURACION ANTERIOR</small></b></Td>
				<Td width="15%" ALIGN=CENTER nowrap><%=cchData.getE01CCHFAD()%>/<%=cchData.getE01CCHFAM()%>/<%=cchData.getE01CCHFAA()%></Td>
				<Td width="55%" ALIGN=CENTER rowspam="5"></Td>												
			</TR>
			<TR id="trdark">
				<td width="40%" align="left" rowspan="4" nowrap><b><small>MONTO FACTURADO A PAGAR (PERIODO ANTERIOR)<br>
																		  MONTO PAGADO PERIODO ANTERIOR<br>
																		  SALDO ADEUDADO FINAL PERIODO ANTERIOR</small></b></Td>
				<Td width="15%" ALIGN=CENTER nowrap>$<%=Util.formatCCY(cchData.getE01CCHSAN())%> <br>
				                                    $<%=Util.formatCCY(cchData.getE01CCHTPG())%> <br>				                                    
			                                        $<%=Util.formatCCY(cchData.getE01CCHSAD())%> </Td>
				<Td width="55%" ALIGN=CENTER rowspan="1"></Td>	
			</TR>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<TABLE  class="tableinfo">
	<TR>
		<TD width="100%">
		 <table border=1  CELLPADDING=0 CELLSPACING=0 width="100%">
			<TR id="trclear">
				<td width="100%" align="left" nowrap><b>2. PERIODO ACTUAL</b></Td>			
			</TR>
		</table>
		 <table border=1 frame="rhs" CELLPADDING=0 CELLSPACING=0 width="100%">	
			<TR id="trdark">
				<td width="15%" align=CENTER nowrap><small>LUGAR DE <br>OPERACION</small></Td>
				<Td width="10%" ALIGN=CENTER nowrap><small>FECHA<br>OPERACION</small> </Td>
				<Td width="15%" ALIGN=CENTER nowrap><small>CODIGO<br>REFERENCIA</small> </Td>
				<Td width="20%" ALIGN=CENTER nowrap><small>DESCRIPCION OPERACION O COBRO</small> </Td>	
	    		<Td width="10%" ALIGN=CENTER nowrap><small>MONTO<br>OPERACION<br>O COBRO</small></Td>
	    		<Td width="10%" ALIGN=CENTER nowrap><small>MONTO<br>TOTAL A<br>PAGAR</small></Td>
	    		<Td width="10%" ALIGN=CENTER nowrap><small>CARGOS MES <br>CUOTA</small></Td>
	       		<Td width="10%" ALIGN=CENTER nowrap><small>CARGOS MES <br>VALOR CUOTA</small></Td>	    			    												
			</TR>
			<%
				ECC015002List.initRow();
					boolean firstTime = true;
					while (ECC015002List.getNextRow()) {
						ECC015002Message convObj = (ECC015002Message) ECC015002List
								.getRecord();
			if (Integer.parseInt(convObj.getE02CCDCOR()) == 1 && Integer.parseInt(convObj.getE02CCDTTR()) == 1) {
			%>
			<TR id="trdark">
				<Td ALIGN=CENTER colspan="7" nowrap> </Td>
	       		<Td ALIGN=CENTER nowrap> </Td>	    			    												
			</TR>
			<TR id="trclear">
				<Td ALIGN=CENTER colspan="7" nowrap><b><%= convObj.getE02CCDNCO()%></b></Td>
	       		<Td ALIGN="right" nowrap><b><%= Util.formatCCY(convObj.getE02CCDMCU())%></b></Td>	    			    												
			</TR>
			<%
				} else {
			%>			
			<tr id="trdark">
				<td nowrap align="center"><%= convObj.getE02CCDCIU()%></td>
				<td nowrap align="center"><%=Util.formatDate(convObj.getE02CCDFTD(),convObj.getE02CCDFTM(),convObj.getE02CCDFTA())%></td>				
				<td nowrap align="right"><%= convObj.getE02CCDMIC()%></td>				
				<td nowrap align="left"><%= convObj.getE02CCDNCO()%> <%= convObj.getE02CCDGL1()%> <%= convObj.getE02CCDGL2()%></td>
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE02CCDMTR())%></td>
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE02CCDMTP())%></td>
				<td nowrap align="center"><%= convObj.getE02CCDCUO()%>/<%= convObj.getE02CCDTCU()%></td>
				<td nowrap align="right"><%= Util.formatCCY(convObj.getE02CCDMCU())%></td>				
			</tr>
			<%
			    }
				}
			%>			
		</TABLE>
		</TD>
	</TR>
</TABLE>
<br>
<TABLE  class="tableinfo" width="100%" >
	<TR>
		<TD>
		 <table cellpadding=2 cellspacing=0 width="100%" border=0>
			<TR id="trclear">
				<td align="left" rowspan=0 ><h3>III. INFORMACION DE PAGO</h3></Td>			
			</TR>
		</TABLE>
		 <table cellpadding=2 border=1 cellspacing=0 width="100%" border=0>
				<TR id="trdark">
				<Td width="30%" ALIGN="left" colspan="4" nowrap>MONTO TOTAL FACTURADO A PAGAR<br>
				                                     MONTO MINIMO A PAGAR<br>
				                                     CARGO AUTOMATICO </td>
				<Td width="15%" ALIGN="right" colspan="4" nowrap>$ <%=Util.formatCCY(cchData.getE01CCHDTP())%><br>
				                                     $ <%=Util.formatCCY(cchData.getE01CCHPMI())%><br>
				                                     $ <%=Util.formatCCY(cchData.getE01CCHMCC())%></Td>	
				<Td width="55%" ALIGN=CENTER colspan="3" nowrap> </Td>				                                     	
			</TR>
		</TABLE>
		<BR>
		 <table cellpadding=0 cellspacing=0 width="100%" border=0>
			<TR id="trdark">
				<Td width="100%" ALIGN=CENTER bgcolor="#F2F1F1" nowrap><b>VENCIMIENTO PROXIMOS 4 MESES</b></Td>			
			</TR>
		</table>
			<table cellpadding=0 cellspacing=0 width="100%" border="1">		
			<TR id="trdark">
				<Td width="20%" ALIGN=CENTER nowrap><b>ACTUAL</b></Td>
				<Td width="20%" ALIGN=CENTER nowrap><b><%= cchData.getE01CCHMM1()%></b></Td>
				<Td width="20%" ALIGN=CENTER nowrap><b><%= cchData.getE01CCHMM2()%></b></Td>
				<Td width="20%" ALIGN=CENTER nowrap><b><%= cchData.getE01CCHMM3()%></b></Td>
				<Td width="20%" ALIGN=CENTER nowrap><b><%= cchData.getE01CCHMM4()%></b></Td>			
			</TR>
			<TR id="trdark">
				<Td width="20%" ALIGN=CENTER nowrap>$<%=Util.formatCCY(cchData.getE01CCHSCC())%></Td>
				<Td width="20%" ALIGN=CENTER nowrap>$<%=Util.formatCCY(cchData.getE01CCHCM1())%></Td>
				<Td width="20%" ALIGN=CENTER nowrap>$<%=Util.formatCCY(cchData.getE01CCHCM2())%></Td>
				<Td width="20%" ALIGN=CENTER nowrap>$<%=Util.formatCCY(cchData.getE01CCHCM3())%></Td>
				<Td width="20%" ALIGN=CENTER nowrap>$<%=Util.formatCCY(cchData.getE01CCHCM4())%></Td>				
			</TR>
	    </table>
        <BR>
		 <table cellpadding=0 cellspacing=0 width="100%" border="1">
				<TR id="trdark">
				<Td width="30%" ALIGN=CENTER nowrap></Td>
				<Td width="10%" ALIGN=CENTER nowrap><b>DESDE</b></Td>
				<Td width="10%" ALIGN=CENTER nowrap><b>HASTA</b></Td>
				<Td width="50%" ALIGN=CENTER rowspan="2" nowrap></Td>			
			</TR>
			<TR id="trdark">
				<Td width="30%" ALIGN="left" nowrap>PROXIMO PERIODO DE FACTURACION</Td>
				<Td width="10%" ALIGN=CENTER nowrap><%=cchData.getE01CCHFND()%>/<%=cchData.getE01CCHFNM()%>/<%=cchData.getE01CCHFNA()%></Td>
				<Td width="10%" ALIGN=CENTER nowrap><%=cchData.getE01CCHPFD()%>/<%=cchData.getE01CCHPFM()%>/<%=cchData.getE01CCHPFA()%></Td>		
			</TR>
		</TABLE>
    </TD>
</TR>
</TABLE>
<br>
<br>
<br>
<TABLE  class="tableinfo" width="100%"cellpadding=0 cellspacing=0>
	<TR>
		<TD>
		 <table cellpadding=0 cellspacing=0 width="100%" border=1>
		 		<TR id="trdark">
				<Td width="50%" ALIGN="left" nowrap><b> </b></Td>
				<Td width="50%" ALIGN="left"><IMG src="<%=request.getContextPath()%>/images/logo1_coopeuch.JPG" align="left" WIDTH=136 HEIGHT=46></Td>
				</TR>
				<TR id="trdark">
				<Td width="50%" ALIGN="left" nowrap></Td>
				<Td width="50%" ALIGN="left"><b>       </b><small> Estimados Cliente,  para pagar su Tarjeta de Cr�dito por caja, favor completar el n�mero en los casilleros correspondientes.</small></Td>
				</TR>				
		 		<TR id="trdark">
				<Td width="50%" ALIGN="left" ><b>EMISOR</b></Td>
				<Td width="50%" ALIGN="left"><b>CLIENTE</b></Td>
				</TR>				
				<TR id="trdark">
				<Td width="50%" ALIGN=CENTER nowrap><b>COMPROBANTE DE PAGO</b></Td>
				<Td width="50%" ALIGN=CENTER nowrap><b>COMPROBANTE DE PAGO</b></Td>
				</TR>
          </table>  
           <table cellpadding=0 cellspacing=0 width="100%" border=1 frame="box">
				<TR id="trdark">
				<Td width="25%" ALIGN="left" nowrap><b>NOMBRE </b><br><%= cchData.getE01CCHNOM()%></Td>
				<Td width="25%" ALIGN="right" nowrap><b>NUMERO DE CUENTA</b><br><%= cchData.getE01CCHNXN()%> </Td>
				<Td width="25%" ALIGN="left" nowrap><b>NOMBRE</b><br><%= cchData.getE01CCHNOM()%></Td>
				<Td width="25%" ALIGN="right" nowrap><b>NUMERO DE CUENTA</b><br><%= cchData.getE01CCHNXN()%> </Td>
				</TR>
				<TR id="trdark">
				<Td width="25%" ALIGN="left" nowrap><b>PAGAR HASTA</b> <br><%=cchData.getE01CCHVTD()%>/<%=cchData.getE01CCHVTM()%>/<%=cchData.getE01CCHVTA()%></Td>
				<Td width="25%" ALIGN="left" nowrap><b>MONTO TOTAL FACTURADO A PAGAR</b><br>$<%= Util.formatCCY(cchData.getE01CCHDTP())%> </Td>
				<Td width="25%" ALIGN="left" nowrap><b>PAGAR HASTA</b><br> <%=cchData.getE01CCHVTD()%>/<%=cchData.getE01CCHVTM()%>/<%=cchData.getE01CCHVTA()%></Td>
				<Td width="25%" ALIGN="left" nowrap><b>MONTO TOTAL FACTURADO A PAGAR </b><br>$<%= Util.formatCCY(cchData.getE01CCHDTP())%> </Td>
				</TR>	
				<TR id="trdark">
				<Td width="25%" ALIGN="left" nowrap><b>MONTO MINIMO A PAGAR</b><br>$<%= Util.formatCCY(cchData.getE01CCHPMI())%></Td>
				<Td width="25%" ALIGN="left" nowrap><b>MONTO CANCELADO</b><br>$ </Td>
				<Td width="25%" ALIGN="left" nowrap><b>MONTO MINIMO A PAGAR</b><br>$<%= Util.formatCCY(cchData.getE01CCHPMI())%></Td>
				<Td width="25%" ALIGN="left" nowrap><b>MONTO CANCELADO</b><br>$ </Td>
				</TR>			         
		</TABLE>
           <table cellpadding=0 cellspacing=0 width="100%" border=0>
				<TR id="trdark">
				<Td width="25%" ALIGN="left" nowrap><b>Cheque _____ </b></Td>
				<Td width="25%" ALIGN="left" nowrap><b>Efectivo _____ </b></Td>
				<Td width="25%" ALIGN="left" nowrap><b>Cheque _____ </b></Td>
				<Td width="25%" ALIGN="left" nowrap><b>Efectivo _____ </b></Td>
				</TR>		         
		</TABLE>		
    </TD>
</TR>
</TABLE>

<p>Este documento no reemplaza el estado de cuenta de la tarjeta de cr�dito que es enviado al domicilio o correo electr�nico registrado en nuestros sistemas.</p>
</form>
</body>
</html>
