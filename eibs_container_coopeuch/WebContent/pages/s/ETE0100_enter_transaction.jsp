<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Trasferencias - Seguridad y Parámetros</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "trans" class= "datapro.eibs.beans.ETE010002Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

<script language="JavaScript">
 function enterCode(){
	
	if (trim(document.forms[0].E01LTRCOD.value).length > 0) {
	    return true;
	}else{
		alert("Es requerido que ingrese el Tipo de Transferencia");
		document.forms[0].E01BRMEID.focus();
		return false;
	}
 }
</script>

</head>

<body>
 
<H3 align="center">Transferencias - Seguridad y Par&aacute;metros
<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="enter_transaction.jsp, ETE0100">
</H3>

<hr size="4">
<p>&nbsp;</p>

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.tefmulti.JSETE0100" onsubmit="return(enterCode());" >
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">

  <table class="tbenter" cellspacing=0 cellpadding=2 width="100%" border="0"> 
    <tr id="trdark">
      <td width="50%"> 
        <div align="right">Tipo de Transferencia : </div>
      </td>
      <td width="50%"> 
        <div align="left"> 	
 	       <input type="text" name="E02LIFCOD" value="<%= trans.getE02LIFCOD()%>" size="5" maxlength="4">
           <input type="text" name="E02LIFDCO" value="<%= trans.getE02LIFDCO()%>" size="50" maxlength="45"  readonly >
                
	       <a href="javascript:GetCodeDescCNOFC('E02LIFCOD','E02LIFDCO','TQ')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a>
	       <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" >  
        </div>
      </td>
    </tr>
  </table>
  <p align="center">
      <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </p>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
      error.setERRNUM("0");
 %>
     <SCRIPT Language="Javascript">;
            showErrors();
     </SCRIPT>
 <%
 }
%>
</form>
<script language="JavaScript">
  document.forms[0].E01LTRCOD.focus();
  document.forms[0].E01LTRCOD.select();
</script>
</body>
</html>