package datapro.eibs.products;

import com.datapro.generics.Util;
import datapro.eibs.beans.*;
import datapro.eibs.master.*;
import datapro.eibs.sockets.MessageRecord;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;

public class JSEDL0903 extends JSEIBSServlet {

	private static final long serialVersionUID = 1L;
	protected static final int R_ENTER_INQUIRY = 100;
	protected static final int A_ENTER_INQUIRY = 200;
	protected static final int R_ENTER_INQUIRY_CONVENIO = 300;
	protected static final int A_ENTER_INQUIRY_CONVENIO = 400;
	protected static final int R_ENTER_INQUIRY_AVENIMIENTO = 500;
	protected static final int A_ENTER_INQUIRY_AVENIMIENTO = 600;
	
	public JSEDL0903() {
	}

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
		switch (screen) {
		case R_ENTER_INQUIRY: 
			procReqEnterInquiry(user, req, res, session, "CJ"); //Cobranza Judicial
			break;

		case A_ENTER_INQUIRY: 
			procActionEnterInquiry(user, req, res, session, "CJ");
			break;

		case R_ENTER_INQUIRY_CONVENIO: 
			procReqEnterInquiry(user, req, res, session, "CV"); //Convenio de Pago
			break;

		case A_ENTER_INQUIRY_CONVENIO: 
			procActionEnterInquiry(user, req, res, session, "CV");
			break;

		case R_ENTER_INQUIRY_AVENIMIENTO: 
			procReqEnterInquiry(user, req, res, session, "AV"); //Avenimiento
			break;

		case A_ENTER_INQUIRY_AVENIMIENTO: 
			procActionEnterInquiry(user, req, res, session, "AV");
			break;
			
			
			
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
	}

	
	protected void procReqEnterInquiry(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession ses, String proceso) throws ServletException,
			IOException 
			{
		
		UserPos userPO = (UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		
		try 
		{
			ses.setAttribute("TP", proceso.toString());

			flexLog("About to call Page: EDL0903_ln_enter_inquiry.jsp");
			forward("EDL0903_ln_enter_inquiry.jsp", req, res);
		} 
		catch (Exception e) {
			e.printStackTrace();
			flexLog((new StringBuilder("Exception calling page ")).append(e).toString());
		}
	}

	protected void procActionEnterInquiry(ESS0030DSMessage user,	HttpServletRequest req, HttpServletResponse res, HttpSession session, String proceso)
			throws ServletException, IOException 
			{
		
		MessageProcessor mp;
		mp = null;
		UserPos userPO = (UserPos) session.getAttribute("userPO");
		
		try {
			mp = getMessageProcessor("EDL0903", req);

			EDL090301Message msg = (EDL090301Message) mp.getMessageRecord("EDL090301", user.getH01USR(), "0001");
		
			try {
			 	if (req.getParameter("E01DEAACC") != null)
			 		msg.setE01DEAACC(req.getParameter("E01DEAACC"));
			}
			catch (Exception e)	{
				msg.setE01DEAACC("0");
			}
			
			if(proceso.equals("CV"))
		 		msg.setH01FLGWK1("C"); //Convenio de Pago
			else 
				if(proceso.equals("AV"))
			 		msg.setH01FLGWK1("A"); //Avenimiento
				
			mp.sendMessage(msg);

			ELEERRMessage error = (ELEERRMessage) mp.receiveMessageRecord();
			if (mp.hasError(error)) 
			{
				session.setAttribute("error", error);
				
				flexLog("About to call Page: EDL0903_ln_enter_inquiry.jsp");
				forward("EDL0903_ln_enter_inquiry.jsp", req, res);
			} 
			else 
			{
				msg = (EDL090301Message) mp.receiveMessageRecord();

				JBObjList PlanCuenta = mp.receiveMessageRecordList("H02FLGMAS");

				session.setAttribute("header", msg);
				session.setAttribute("list", PlanCuenta);

				flexLog("About to call Page: EDL0903_ln_crn_pay.jsp");
				forwardOnSuccess("EDL0903_ln_crn_pay.jsp", req, res);
				
			}
		} finally {
			if (mp != null)
				mp.close();
		}
	}

	
//
}
