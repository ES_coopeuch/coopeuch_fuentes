package datapro.eibs.reports;

/**
 * Insert the type's description here.
 * Creation date: (8/28/2000 4:02:17 PM)
 * @author: Orestes Garcia
**/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.w3c.dom.Element;

import com.datapro.output.PDFFormLoader;
import com.datapro.xml.DOMUtil;

import datapro.eibs.beans.EFRM00001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD000004Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.master.Util;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageRecord;
import datapro.eibs.tools.EIBSFormsFieldGenerator;
import datapro.eibs.tools.xml.EibsFormsPdfInput;
import datapro.eibs.tools.xml.EibsFormsPdfOutput;

public class JSEFRM000XDP extends SuperServlet {

	static final int R_FORM_LIST = 1;
	static final int R_FORM = 3;
	static final int R_APPLICATION = 5;
	
	private String LangPath = "S";
	private String userLanguage;

	/**
	 * JSReportManager constructor comment.
	 */
	public JSEFRM000XDP() {
		super();
	}
	
	/**
	 * service method comment.
	 */
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws javax.servlet.ServletException, java.io.IOException {

		MessageContext mc = null;

		ESS0030DSMessage msgUser = null;
		HttpSession session = null;

		session = (HttpSession) req.getSession(false);

		if (session == null) {
			try {
				res.setContentType("text/html");
				printLogInAgain(res.getWriter());
			} catch (Exception e) {
				e.printStackTrace();
				flexLog("Exception ocurred. Exception = " + e);
			}
		} else {

			try {

				int screen = R_FORM;

				msgUser = (datapro.eibs.beans.ESS0030DSMessage) session.getAttribute("currUser");

				// Here we should get the path from the user profile
				LangPath = super.rootPath + msgUser.getE01LAN() + "/";
				userLanguage = msgUser.getE01LAN().equals("s") ? "es" : "en";

				try {
					flexLog("Opennig Socket Connection");
					mc = new MessageContext(super.getMessageHandler("EFRM000", req));
				} catch (Exception e) {
					e.printStackTrace();
					int sck = getInitSocket(req) + 1;
					flexLog("Socket not Open(Port " + sck + "). Error: " + e);
					res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
					return;
				}

				try {
					screen = Integer.parseInt(req.getParameter("SCREEN"));
				} catch (Exception e) {
					flexLog("Screen set to default value");
				}

				switch (screen) {
					case R_FORM_LIST :
						procReqFormList(mc, msgUser, req, res, session);
						break;
					case R_FORM :
						procReqForm(req, res, session);
						break;
					default :
						res.sendRedirect(super.srctx + LangPath + super.devPage);
						break;
				}

				try {
					mc.close();
				} catch (Exception e) {
					flexLog("Error closing socket connection " + e);
				}

			} catch (Exception e) {
				flexLog("Error: " + e);
				res.sendRedirect(super.srctx + LangPath + super.sckNotRespondPage);
			}
		}

	}
	
	/**
	 * Insert the method's description here.
	 * Creation date: (9/25/2000 11:17:32 AM)
	 */
	private void procReqFormList(
		MessageContext mc,
		ESS0030DSMessage user,
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses)
		throws ServletException, IOException {

		MessageRecord newmessage = null;
		EFRM00001Message msgHeader = null;
		ELEERRMessage msgError = null;
		String appCode = null;
		
		UserPos userPO = (UserPos) ses.getAttribute("userPO");

		// Send Initial data
		try {
			msgHeader = (EFRM00001Message) mc.getMessageRecord("EFRM00001");
			msgHeader.setH01USERID(user.getH01USR());
			msgHeader.setH01PROGRM("EFRM000");
			msgHeader.setH01TIMSYS(getTimeStamp());

			try {
				// nothing - Datapro Forms & B - Bankers Forms & P - Datapro PDF Forms
				msgHeader.setH01FLGWK1(req.getParameter("INTERFACE"));
			} catch (Exception ex) {
				//msgHeader.setH01FLGWK1("");
				msgHeader.setH01FLGWK1("P");
			}
			try {
				// 01 - Opening
				msgHeader.setE01SELFTY(req.getParameter("OPE_CODE"));
			} catch (Exception ex) {
			}

			try {
				appCode = req.getParameter("APP_CODE");
				if (appCode == null)
					appCode = "";
			} catch (Exception e) {
				appCode = "";
			}

			if (appCode.equalsIgnoreCase("XX")) {
				try {
					msgHeader.setH01OPECOD(req.getParameter("ACCOUNT").toUpperCase());
				} catch (Exception e) {
				}
			} else {
				msgHeader.setE01SELACD(appCode);
				try {
					msgHeader.setE01SELACC(req.getParameter("ACCOUNT"));
				} catch (Exception e) {
					if (appCode.equals("00")) {
						msgHeader.setE01SELACC(userPO.getCusNum());
					} else {
						msgHeader.setE01SELACC(userPO.getIdentifier());
					}
				}
				try {
					msgHeader.setH01OPECOD(Util.justifyRight(req.getParameter("SEQ"), 4));
				} catch (Exception e) {
				}
			}

			msgHeader.send();
			msgHeader.destroy();
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		// Receive Error Message
		try {

			newmessage = mc.receiveMessage();

			if (newmessage.getFormatName().equals("ELEERR")) {
				msgError = (ELEERRMessage) newmessage;

				flexLog("Putting java beans into the session");
				String err = msgError.getERNU01() + " - " + msgError.getERDS01();
				ses.setAttribute("error_msg", err);

				try {
					flexLog("About to call Page: " + LangPath + "EFRM000_forms_req_error.jsp");
					callPage(LangPath + "EFRM000_forms_req_error.jsp", req, res);

				} catch (Exception e) {
					flexLog("Exception calling page " + e);
				}
			} else if (newmessage.getFormatName().equals("EFRM00001")) {
				boolean nothing = true;
				JBObjList pdfList = new JBObjList();

				while (true) {
					msgHeader = (EFRM00001Message) newmessage;
					if (msgHeader.getE01MORFRM().equals("*"))
						break;
					else {
						if (nothing) nothing = false;
						pdfList.addRow(msgHeader);
					}
					newmessage = mc.receiveMessage();
				}

				if (!nothing) {
					EibsFormsPdfOutput eformsOutput =
						new EibsFormsPdfOutput("eIBSPDFDataStructure");
					EIBSFormsFieldGenerator formats = new EIBSFormsFieldGenerator();

					if (!appCode.equalsIgnoreCase("XX")) {
						EibsFormsPdfInput eformsInput =
							new EibsFormsPdfInput(
									new URL(JSEIBSProp.getFORMPDFURL()+"eIBSFormsFields.xsd")
										.openStream());
						eformsInput.setDateFormat(user.getE01DTF());
						eformsInput.setLanguage(userLanguage);

						// Receive Data
						newmessage = mc.receiveMessage();
						Element tableParent = null;
						while (true) {
							String ddsName = newmessage.getFormatName();
							String formatsDesc =  formats.getFormatName(newmessage.getFormatName());
							if (ddsName.equals("EFRM00001")) { // End
								break;
							} else if(ddsName.equals("ESD000004")){
								ESD000004Message msg = (ESD000004Message) newmessage;
								String elementName = "";
								switch (Integer.parseInt(msg.getE04RTP())) {
									case 2 : 
										elementName = "StockHolder";
										break;
									case 3 : 
										elementName = "Board";
										break;
									case 4 : 
										elementName = "Beneficiary";
										break;
									case 5 : 
										elementName = "LegalRep";
										break;
									case 6 : 
										elementName = "BankRef";
										break;
									case 7 :
										elementName = "CommercialRef";
										break;
									case 8 : 
										elementName = "PersonalRef";
										break;
									default: 
										elementName = "";
								}
								eformsOutput.addXMLGroupAndChildElements(formats.getFormatName(newmessage.getFormatName()),
										eformsInput.getElementList(newmessage, elementName));
							} else {
								if (formats.isTableGroup(newmessage.getFormatName())){
									if (tableParent == null || !tableParent.getTagName().equals(formatsDesc+"Table")){
										tableParent = eformsOutput.addXMLGroupAndChildElements(formatsDesc+"Table", null);
									}
									eformsOutput.addXMLGroupAndChildElements(tableParent, formatsDesc, eformsInput.getXSDElementList(newmessage));
								} else {
									eformsOutput.addXMLGroupAndChildElements(formatsDesc, eformsInput.getXSDElementList(newmessage));
								}
							}
							newmessage = mc.receiveMessage();

						}
					}
					
					// Output the document, use standard formatter
					try {
						if (super.formActive) 
							ses.setAttribute("pdfData", eformsOutput);
						flexLog("Putting java beans into the session");
						ses.setAttribute("pdfList", pdfList);

						try {
							flexLog("About to call Page: " + LangPath + "EFRM000XDP_forms_pdf_list.jsp");
							callPage(LangPath + "EFRM000XDP_forms_pdf_list.jsp", req, res);

						} catch (Exception e) {
							flexLog("Exception calling page " + e);
						}

					} catch (Exception e) {
						flexLog("Error: " + e);
					}
				} else {
					flexLog("Putting java beans into the session");
					String err = "";
					ses.setAttribute("error_msg", err);
					try {
						flexLog("About to call Page: " + LangPath + "EFRM000_forms_req_error.jsp");
						callPage(LangPath + "EFRM000XDP_forms_req_error.jsp", req, res);

					} catch (Exception e) {
						flexLog("Exception calling page " + e);
					}
					// }
				}

			} else {
				flexLog("Message " + newmessage.getFormatName() + " received.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

	}


	private void procReqForm(
		HttpServletRequest req,
		HttpServletResponse res,
		HttpSession ses) 
		throws ServletException, IOException {

		ServletOutputStream out =
			(ServletOutputStream) res.getOutputStream();
		try {
			int row = Integer.parseInt(req.getParameter("ROW"));
			
			JBObjList pdfList = (JBObjList) ses.getAttribute("pdfList");
			pdfList.initRow();
			pdfList.setCurrentRow(row);
			EFRM00001Message msgHeader = (EFRM00001Message) pdfList.getRecord();
			
			String docType = null;
			if (msgHeader.getE01APFIFS().equals("B"))
				docType = "BFO";
			else
				docType = "eIBSForm";

			String path = msgHeader.getE01APFPTH().trim();
			String copies = msgHeader.getE01APFCPI().trim();

			String operation = null;
			switch (Integer.parseInt(msgHeader.getE01APFOPE())) {
				case 3 :
					operation = "Prepare";
					break;
				case 2 :
					operation = "Print";
					break;								
				case 1 :
				default :
					operation = "Preview";
			}
			
			// Output the document
			try {
				res.setContentType("application/pdf");
				//res.setHeader("Content-Disposition", "attachment; filename=\"" + path + "\";");
				
				EibsFormsPdfOutput eformsOutput = (EibsFormsPdfOutput)ses.getAttribute("pdfData");
				new PDFFormLoader().load(
					new ByteArrayInputStream(
						eformsOutput.toString().getBytes()),
					new URL(JSEIBSProp.getFORMPDFURL()+path).openStream(),
					out,
					PDFFormLoader.OUTPUT_FORMAT_XML);

			
			} catch (Exception e) {
				flexLog("Error: " + e);
				
				res.setContentType("text/html");
				out.println("<HTML>");
				out.println("<HEAD>");
				out.println("<TITLE>Close</TITLE>");
				out.println("</HEAD>");
				out.println("<BODY>");
				out.println("<SCRIPT LANGUAGE=\"JavaScript\">");
				out.println("		top.close();");
				out.println("</SCRIPT>");
				out.println("<P>Close it!!!</P>");
				out.println("</BODY>");
				out.println("</HTML>");
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		} finally {
			if (out != null)	
				out.close();
		}
	}

}