<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Datos Adicionales CES</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
</head>

<jsp:useBean id= "lnCes" class= "datapro.eibs.beans.ESD402001Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<SCRIPT Language="Javascript">

<%
if ( userPO.getHeader23().equals("G") ||  userPO.getHeader23().equals("V")){
%>
	builtNewMenu(ln_i_1_opt);
<%   
}
else  {
%>
	builtNewMenu(ln_i_2_opt);
<%   
}
%>

          
</SCRIPT>

<body nowrap bgcolor="#FFFFFF">
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
    out.println("<SCRIPT> initMenu(); </SCRIPT>");

%> 
<h3 align="center">Consulta Datos Adicionales <%= userPO.getHeader4().trim()%><img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ln_inq_datos_ces.jsp,ESD4020" ></h3> 
<hr size="4">
 <FORM METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4020" >
  <p>
    <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  </p>

  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
         
          <tr id="trdark"> 
            <td nowrap width="16%" > 
              <div align="right"><b>Cliente :</b></div>
            </td>
            <td nowrap width="20%" > 
              <div align="left">
                <input type="text" name="E01CUN2" size="10" maxlength="9" readonly value="<%= userPO.getHeader2().trim()%>">
              </div>
            </td>
            <td nowrap width="16%" > 
              <div align="right"><b>Nombre :</b> </div>
            </td>
            <td nowrap colspan="3" > 
              <div align="left"><font face="Arial"><font face="Arial"><font size="2">
                <input type="text" name="E01NA12" size="45" maxlength="45" readonly value="<%= userPO.getHeader3().trim()%>">
                </font></font></font></div>
            </td>
          </tr>
          
          <tr id="trdark"> 
            <td nowrap width="16%"> 
              <div align="right"><b>Cuenta :</b></div>
            </td>
            <td nowrap width="20%"> 
              <div align="left">
                <input type="text" name="E01ACC" size="13" maxlength="12" value="<%= userPO.getIdentifier().trim()%>" readonly>
              </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Moneda : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b>
                <input type="text" name="E01DEACCY" size="3" maxlength="3" value="<%= userPO.getCurrency().trim()%>" readonly>
                </b> </div>
            </td>
            <td nowrap width="16%"> 
              <div align="right"><b>Producto : </b></div>
            </td>
            <td nowrap width="16%"> 
              <div align="left"><b>
                <input type="text" name="E01PRO2" size="4" maxlength="4" value="<%= userPO.getHeader1().trim()%>" readonly>
                </b> </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
 
  <h4>Beneficiario</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
      
        <tr id="trclear"> 
            <td nowrap width="30%"> 
              <div align="right">N&uacute;mero de Cliente :</div>
            </td>       
            <td nowrap width="60%" colspan="3"> 
			<eibsinput:help readonly="true" name="lnCes" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" property="E01ADIBCU" 
					fn_param_one="E01ADIBCU" fn_param_two="E01ADINOM" fn_param_three="E01ADIIDN"  />
           </td> 
          </tr>
          <tr id="trdark"> 
            <td nowrap width="30%" > 
              <div align="right">Nombre :</div>
            </td>
           <td nowrap width="60%" colspan="3">
			 <eibsinput:text name="lnCes" property="E01ADINOM" size="35" maxlength="35" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" readonly="true"/>                
		   </td>
          </tr>
       
          <tr id="trclear"> 
            <td nowrap width="30%"> 
              <div align="right">Identificaci&oacute;n :</div>
            </td>
           <td nowrap width="60%" colspan="3">
			  <eibsinput:text name="lnCes" property="E01ADIIDN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>                
		   </td>
          </tr>
       
          <tr id="trdark"> 
            <td nowrap width="3%"> 
              <div align="right">Sexo :</div>
            </td>
                <td nowrap  width="31%" disabled > 
                  <p> 
                    <input type="radio" name="E01ADIBSX" value="F" <%if (lnCes.getE01ADIBSX().equals("F")) out.print("checked"); %>>
                    Femenino 
                    <input type="radio" name="E01ADIBSX" value="M" <%if (lnCes.getE01ADIBSX().equals("M")) out.print("checked"); %>>
                    Masculino </p> 
               </td>
          </tr>
        
          <tr id="trclear"> 
            <td nowrap width="3%"> 
              <div align="right">Tipo Relaci&oacute;n :</div>
            </td>
                <td nowrap  width="31%"> 
                  <select name="E01ADIPAR" disabled>
                    <option value=" " <% if (!(lnCes.getE01ADIPAR().equals("1") ||
                                               lnCes.getE01ADIPAR().equals("2") || 
                                               lnCes.getE01ADIPAR().equals("3") ||
                                               lnCes.getE01ADIPAR().equals("4") ||
                                               lnCes.getE01ADIPAR().equals("5") ||
                                               lnCes.getE01ADIPAR().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (lnCes.getE01ADIPAR().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (lnCes.getE01ADIPAR().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (lnCes.getE01ADIPAR().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (lnCes.getE01ADIPAR().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (lnCes.getE01ADIPAR().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="9" <% if (lnCes.getE01ADIPAR().equals("9")) out.print("selected"); %>>Otro</option>
                  </select> 
                </td>
          </tr>      
        </table>
      </td>
    </tr>
  </table> 
  
  <h4>Datos de Estudio</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
      
	       <tr id="trdark"> 
            <td nowrap width="30%"> 
              <div align="right">Instituci&oacute;n de Educaci&oacute;n :</div>
            </td>
            <td nowrap width="70%"> 
                <eibsinput:cnofc name="lnCes" flag="IN" property="E01ADIINS" fn_code="E01ADIINS" fn_description="D01ADIINS" />
               <eibsinput:text name="lnCes" property="D01ADIINS" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>" size="30" maxlength="30" readonly="true"/>
            </td>
          </tr>
  
          <tr id="trclear"> 
            <td nowrap width="30%" > 
              <div align="right">Carrera :</div>
            </td>
            <td nowrap width="70%"> 
               <eibsinput:text  name="lnCes" property="E01ADICAR" size="35" maxlength="35" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DESCRIPTION %>"readonly="true" />
           </td>
          </tr>
       
          <tr id="trdark"> 
            <td nowrap width="30%"> 
              <div align="right">Duraci&oacute;n de la Carrera :</div>
            </td>
            <td nowrap width="50%" align="left">
	  			<eibsinput:text name="lnCes" property="E01ADIDCA" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_REFERENCE %>" size="4" maxlength="2" onkeypress="enterInteger()" /> Semestres
             </td>  
          </tr>
       
          <tr id="trclear"> 
            <td nowrap width="3%"> 
              <div align="right">Semestres a Financiar :</div>
            </td>
            <td nowrap width="50%"align="left">
	  			<eibsinput:text name="lnCes" property="E01ADISFI" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_REFERENCE %>" size="4" maxlength="2" onkeypress="enterInteger()" />
             </td>  
          </tr>
        
          <tr id="trdark"> 
            <td nowrap width="3%"> 
              <div align="right">Ano a Financiar : </div>
            </td>
            <td nowrap width="50%"align="left">
	  			<eibsinput:text name="lnCes" property="E01ADIAFI" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_REFERENCE %>" size="4" maxlength="4" onkeypress="enterInteger()" /> 
             </td>  
          </tr>    
          <tr id="trclear"> 
            <td nowrap width="3%"> 
              <div align="right">Ano Academico a Financiar : </div>
            </td>
            <td nowrap width="50%"align="left">
	  			<eibsinput:text name="lnCes" property="E01ADIAAC" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_REFERENCE %>" size="4" maxlength="4" onkeypress="enterInteger()" /> 
             </td>
          </tr>  
          <tr id="trdark"> 
            <td nowrap width="3%"> 
              <div align="right">Valor Matricula : </div>
            </td>
             <td nowrap width="50%" align="left">
	  			<eibsinput:text name="lnCes" property="E01ADIMAT" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" onkeypress="enterRate()"/>
             </td>
          </tr>   
          <tr id="trclear"> 
            <td nowrap width="3%"> 
              <div align="right">Arancel : </div>
            </td>
             <td nowrap width="50%" align="left">
	  			<eibsinput:text name="lnCes" property="E01ADIARA" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" onkeypress="enterRate()"/>
             </td>
          </tr>   
          <tr id="trdark"> 
            <td nowrap width="3%"> 
              <div align="right">Gastos Asociados : </div>
            </td>
             <td nowrap width="50%" align="left">
	  			<eibsinput:text name="lnCes" property="E01ADIGAS" readonly="true" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" onkeypress="enterRate()"/>
             </td>
          </tr>                                     
        </table>
      </td>
    </tr>
  </table> 
</form>
</body>
</html>
