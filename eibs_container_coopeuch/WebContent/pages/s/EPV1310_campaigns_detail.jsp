<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV131001Message,datapro.eibs.beans.EPV131002Message"%>
<%@ page import="com.datapro.constants.EibsFields"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html> 
<head>
<title>Consulta de Campa&ntilde;as</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"	rel="stylesheet">

<jsp:useBean id="ERC" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "ERCSeleccion" class= "datapro.eibs.beans.EPV131001Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript">

</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Gesti&oacute;n de Cartolas<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="campaigns_detail.jsp, EPV1310"></h3>
<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1310">
<input type="hidden" name="SCREEN" value="300">
<input type="hidden" name="codigo_lista" value="<%=request.getAttribute("codigo_lista") %>" id="codigo_lista">  

<input type="hidden" name="E01CMPIDC" value="<%=request.getAttribute("E01CMPIDC") %>"> 
<input type="hidden" name="E01CCHREC" value="<%=request.getAttribute("E01CCHREC") %>"> 
<input type="hidden" name="E01FVIGDD" value="<%=request.getAttribute("E01FVIGDD") %>"> 
<input type="hidden" name="E01FVIGDM" value="<%=request.getAttribute("E01FVIGDM") %>"> 
<input type="hidden" name="E01FVIGDA" value="<%=request.getAttribute("E01FVIGDA") %>"> 
<input type="hidden" name="E01FVIGHD" value="<%=request.getAttribute("E01FVIGHD") %>"> 
<input type="hidden" name="E01FVIGHM" value="<%=request.getAttribute("E01FVIGHM") %>"> 
<input type="hidden" name="E01FVIGHA" value="<%=request.getAttribute("E01FVIGHA") %>"> 

 
   <table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">Id Carga : 
              </td>
             <td nowrap width="10%" align="left">
	  			<input type="text" name="cartola" value="<%=ERCSeleccion.getE01CMPIDC() %>" 
	  			size="23"  readonly>
             </td>  
             
               <td nowrap width="10%" align="right">Fecha Carga : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCAA" size="23" maxlength="20" value="<% out.print(ERCSeleccion.getE01CMPVAD()+"/"+ERCSeleccion.getE01CMPVAM()+"/"+ERCSeleccion.getE01CMPVAY());%>" readonly>
             </td>  
         </tr>
         
          <tr>
             <td nowrap width="10%" align="right">Estado : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCTA" size="23" maxlength="20" value="<%=ERCSeleccion.getE01CMPGST() %>" readonly>
             </td>
        <td nowrap width="10%" align="right">Usuario : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01BRMCAA" size="23" maxlength="20" value="<%=ERCSeleccion.getE01CMPVAU() %>" readonly>
             </td>
             </tr>
      
           <tr>
             <td nowrap width="10%" align="right">Registros Correctos : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha" size="23" maxlength="20" value="<%=ERCSeleccion.getE01CMPCAR() %>" readonly>
             </td>
        <td nowrap width="10%" align="right">Registros con Error : 
               </td>
             <td nowrap width="50%"align="left">
 <input type="text" name="E01fecha2" size="23" maxlength="20" value="<%=ERCSeleccion.getE01CMPERR() %>" readonly>
             </td>
             </tr>
        </table>
      </td>
    </tr>
  </table>
 
 

<%
	if (ERC.getNoResult()) {
%>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%"> 
			<a HREF="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1310?SCREEN=200&posicion=<%=request.getAttribute("E01CCHREC") %>&E01CMPIDC=<%=request.getAttribute("E01CMPIDC") %>&E01FVIGDD=<%=request.getAttribute("E01FVIGDD") %>&E01FVIGDM=<%=request.getAttribute("E01FVIGDM") %>&E01FVIGDA=<%=request.getAttribute("E01FVIGDA") %>&E01FVIGHD=<%=request.getAttribute("E01FVIGHD") %>&E01FVIGHM=<%=request.getAttribute("E01FVIGHM") %>&E01FVIGHA=<%=request.getAttribute("E01FVIGHA") %>"><b>Atras</b></a>
		</td>
	</tr>
</table>

<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>

<%
	} else {
%>
<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%"> 
			<a HREF="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1310?SCREEN=200&posicion=<%=request.getAttribute("E01CCHREC")%>&E01CMPIDC=<%=request.getAttribute("E01CMPIDC") %>&E01FVIGDD=<%=request.getAttribute("E01FVIGDD") %>&E01FVIGDM=<%=request.getAttribute("E01FVIGDM") %>&E01FVIGDA=<%=request.getAttribute("E01FVIGDA") %>&E01FVIGHD=<%=request.getAttribute("E01FVIGHD") %>&E01FVIGHM=<%=request.getAttribute("E01FVIGHM") %>&E01FVIGHA=<%=request.getAttribute("E01FVIGHA") %>"><b>Atras</b></a>
		</td>
	</tr>
</table>


<table class="tbenter" width=100% >
<tr><td height="20"></td></tr>

<tr>
<td>
<table id="headTable"  width="100%" align="left">
   <tr id="trdark">
 		<th align="center" nowrap  height="32">RUT</th>
		<th align="center" nowrap  height="32">Causa Error</th>
		<th align="center" nowrap  height="32">Tipo Campa&ntilde;a</th>
		<th align="center" nowrap  height="32">Causa de Error</th>
		<th align="center" nowrap  height="32">Tipo Evaluaci&oacute;n</th>
		<th align="center" nowrap  height="32">Causa de Error</th>
		<th align="center" nowrap  height="32">Vigencia</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">PV M&aacute;ximo</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">PMT Aprobado</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">Renta Base</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">Renta L&iacute;quida</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">Renta Depurada</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">Monto Ofertado</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">C&oacute;digo de Promoci&oacute;n</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">D&iacute;as Plazo</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">Codigo de Campa&ntilde;a</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">Descripci&oacute;n de Campa&ntilde;a</th>

		<th align="center" nowrap  height="32">Vigencia Desde</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

		<th align="center" nowrap  height="32">Vigencia Hasta</th>
		<th align="center" nowrap  height="32">Causa de Error</th>

   </tr>
		<%
			ERC.initRow();
			while (ERC.getNextRow()) {
				EPV131002Message conv = (EPV131002Message) ERC.getRecord();
		%>
		<tr>
			<td nowrap align="center" height="15"><%=conv.getE02RUT() %></td>
			<td nowrap align="center" height="15"><%=conv.getE02RUTE()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02TIPOCMP()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02TIPCMPE()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02VIGENCI()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02VIGENCE()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02PVMAX()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02PVMAXE()%></td>

			<td nowrap align="center" height="15"><%=conv.getE02PMTMAX () %></td>
			<td nowrap align="center" height="15"><%=conv.getE02PMTMAXE()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02MTOAPR()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02MTOAPRE()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02RTABAS()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02RTABASE()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02RTALIQ()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02RTALIQE()%></td>

			<td nowrap align="center" height="15"><%=conv.getE02RTADEP() %></td>
			<td nowrap align="center" height="15"><%=conv.getE02RTADEPE()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02MONTOF()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02MONTOFE()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02PROCOD()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02PROCODE()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02PCAPLZ()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02PCAPLZE()%></td>

			<td nowrap align="center" height="15"><%=conv.getE02CODCAM() %></td>
			<td nowrap align="center" height="15"><%=conv.getE02CODCAME()%></td>
			<td nowrap align="center" height="15"><%=conv.getE02DESCAM()%></td>
			<td nowrap align="center" height="15"><% out.print(conv.getE02FVIGDD()+"/"+conv.getE02FVIGDM()+"/"+conv.getE02FVIGDA());  %></td>
			<td nowrap align="center" height="15"><%=conv.getE02FVIGDE()%></td>
			<td nowrap align="center" height="15"><% out.print(conv.getE02FVIGHD()+"/"+conv.getE02FVIGHM()+"/"+conv.getE02FVIGHA());  %></td>
			<td nowrap align="center" height="15"><%=conv.getE02FVIGHE()%></td>
		</tr>
		<%}%>

	</table>
</td>
</tr>
	<tr>
	<td>
</td>
</tr>
</table>

	<table class="tbenter" width="500" align="left">
	<tr>
		<td width="50%" align="left">
		<%
			if (ERC.getShowPrev()) {
					int pos = ERC.getFirstRec() - 11;					
					
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1310?SCREEN=300&posicion="
									+pos+"&codigo_lista="
									+ request.getAttribute("codigo_lista")

									+"&E01CMPIDC="+request.getAttribute("E01CMPIDC")
									+"&E01CCHREC="+request.getAttribute("E01CCHREC")
									+"&E01FVIGDD="+request.getAttribute("E01FVIGDD")
									+"&E01FVIGDM="+request.getAttribute("E01FVIGDM")
									+"&E01FVIGDA="+request.getAttribute("E01FVIGDA")
									+"&E01FVIGHD="+request.getAttribute("E01FVIGHD")
									+"&E01FVIGHM="+request.getAttribute("E01FVIGHM")
									+"&E01FVIGHA="+request.getAttribute("E01FVIGHA")

									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (ERC.getShowNext()) {
					int pos = ERC.getLastRec();
					out.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.salesplatform.JSEPV1310?SCREEN=300&posicion="
									+pos+"&codigo_lista="
									+ request.getAttribute("codigo_lista")

									+"&E01CMPIDC="+request.getAttribute("E01CMPIDC")
									+"&E01CCHREC="+request.getAttribute("E01CCHREC")
									+"&E01FVIGDD="+request.getAttribute("E01FVIGDD")
									+"&E01FVIGDM="+request.getAttribute("E01FVIGDM")
									+"&E01FVIGDA="+request.getAttribute("E01FVIGDA")
									+"&E01FVIGHD="+request.getAttribute("E01FVIGHD")
									+"&E01FVIGHM="+request.getAttribute("E01FVIGHM")
									+"&E01FVIGHA="+request.getAttribute("E01FVIGHA")

									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/next_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>

</table>

<%
	}
%>
</form>
</body>
</html>
