<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Mantenimiento Productos de Garant�as</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">



<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>

</head>

<jsp:useBean id="prd" class="datapro.eibs.beans.ESD070001Message"  scope="session" />

<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     error.setERRNUM("0");
     out.println("</SCRIPT>");
     }
%>

<h3 align="center">Mantenimiento Producto Garant�as<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="products_collaterals.jsp, ESD0700"></h3>
<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSESD0700" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="1">
  <INPUT TYPE=HIDDEN NAME="E01APCACD" VALUE="<%= prd.getE01APCACD()%>">
  <table class="tableinfo">
    <tr > 
      <td> 
        <table cellspacing="0" cellpadding="2" width="100%"  class="tbhead"  align="center">
          <tr> 
            <td nowrap width="10%" align="right"> Banco: </td>
            <td nowrap width="12%" align="left"> 
              <input type="text"  name="E01APCBNK" size="3" maxlength="2" value="<%= prd.getE01APCBNK()%>" readonly>
            </td>
            <td nowrap width="6%" align="right"> Producto : </td>
            <td nowrap width="14%" align="left"> 
              <input type="text"  name="E01APCCDE" size="6" maxlength="4" value="<%= prd.getE01APCCDE()%>" readonly>
            </td>
            <td nowrap width="8%" align="right"> Tipo Producto : </td>
            <td nowrap width="50%"align="left"> 
              <input type="text"  name="E01APCTYP" size="6" maxlength="4" value="<%= prd.getE01APCTYP()%>" readonly>
              - 
              <input type="text"  name="E01DSCTYP" size="25" maxlength="25" value="<%= prd.getE01DSCTYP()%>" readonly>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <h4>Informaci�n General</h4>
<table class="tableinfo">
    <tr > 
      <td > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td > 
              <div align="right">Descripci�n :</div>
            </td>
            <td > 
              <input type="text"  name="E01APCDS1" size="50" maxlength="45" value="<%= prd.getE01APCDS1()%>">
            </td>
            <td > 
              <div align="right">Nombre de Mercadeo :</div>
            </td>
            <td > 
              <input type="text"  name="E01APCDS2" size="28" maxlength="25" value="<%= prd.getE01APCDS2()%>">
            </td>
          </tr>
          <tr id="trclear"> 
            <td > 
              <div align="right">C�digo de Moneda :</div>
            </td>
            <td > 
              <input type="text"  name="E01APCCCY" size="3" maxlength="3" value="<%= prd.getE01APCCCY()%>">
              <a href="javascript:GetCurrency('E01APCCCY','')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0" ></a> 
            </td>
            <td > 
              <div align="right">Ofrecimiento por :</div>
            </td>
            <td > 
            <SELECT name="E01APCFTT">
					<OPTION value="1"
						<%if (prd.getE01APCFTT().equals("1")) { out.print("selected"); }%>>Internet</OPTION>
					<OPTION value="I"
						<%if (prd.getE01APCFTT().equals("I")) { out.print("selected"); }%>>Internacional</OPTION>
					<OPTION value="L"
						<%if (prd.getE01APCFTT().equals("L")) { out.print("selected"); }%>>Local</OPTION>
					<OPTION value="3"
						<%if (prd.getE01APCFTT().equals("3")) { out.print("selected"); }%>>Plataforma</OPTION>
					<OPTION value="5"
						<%if (prd.getE01APCFTT().equals("5")) { out.print("selected"); }%>>Cualquier
					Medio</OPTION>
					<OPTION value="N"
						<%if (prd.getE01APCFTT().equals("N")) { out.print("selected"); }%>>No
					Ofrecer</OPTION>
				</SELECT><td >
            </td>
          </tr>
          <tr id="trdark"> 
            <td > 
              <div align="right"> Cuenta Contable :</div>
            </td>
            <td > 
              <input type="text"  name="E01APCGLN" size="17" maxlength="16" value="<%= prd.getE01APCGLN().trim()%>">
              <a href="javascript:GetLedger('E01APCGLN',document.forms[0].E01APCBNK.value,document.forms[0].E01APCCCY.value,document.forms[0].E01APCACD.value,'','<%= prd.getE01APCTYP().trim()%>')">
				<img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0" ></a> 
            </td>
            <td > 
              <div align="right">Tabla de Documentos :</div>
            </td>
            <td ><INPUT type="text" name="E01APCFTF" size="4" maxlength="2" value="<%= prd.getE01APCFTF().trim()%>">
            <a href="javascript:GetDocInv('E01APCFTF')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt=". . ." align="bottom" border="0"></a> 
            </td>
          </tr>
          <tr id="trclear"> 
            <td > 
              <div align="right"> Tipo de Cliente :</div>
            </td>
            <td > 
               <select name="E01APCFL4">
                <option value="" <%if (prd.getE01APCFL4().equals(""))   { out.print("selected"); }%>>Cualquiera</option>
                <option value="1" <%if (prd.getE01APCFL4().equals("1")) { out.print("selected"); }%>>Empresa</option>
                <option value="2" <%if (prd.getE01APCFL4().equals("2")) { out.print("selected"); }%>>Persona</option>
              </select>
             </td>
            <td > 
              <div align="right">Tipo de Garant�a: </div>
            </td>
            <td width="330">
              <select name="E01APCFRA">
                <option value="A" <%if (prd.getE01APCFRA().equals("A")) { out.print("selected"); }%>>Bienes Muebles</option>
                <option value="B" <%if (prd.getE01APCFRA().equals("B")) { out.print("selected"); }%>>Bienes Inmuebles</option>
                <option value="C" <%if (prd.getE01APCFRA().equals("C")) { out.print("selected"); }%>>Depositos Banco/Otros Bancos</option>
                <option value="D" <%if (prd.getE01APCFRA().equals("D")) { out.print("selected"); }%>>Prendaria no Agraria</option>
                <option value="E" <%if (prd.getE01APCFRA().equals("E")) { out.print("selected"); }%>>Prendaria Agricola</option>
                <option value="F" <%if (prd.getE01APCFRA().equals("F")) { out.print("selected"); }%>>Prendaria Maquinaria y Equipo</option>
                <option value="G" <%if (prd.getE01APCFRA().equals("G")) { out.print("selected"); }%>>Prendaria Ganadera</option>
                <option value="H" <%if (prd.getE01APCFRA().equals("H")) { out.print("selected"); }%>>Otras Garant�as</option>                
                <option value="I" <%if (prd.getE01APCFRA().equals("I")) { out.print("selected"); }%>>Avales</option>
              </select>
            </td>
          </tr>          
          <tr id="trdark"> 
            <td > 
              <div align="right">Meses a Reajustar :</div>
            </td>
            <td > 
              <input type="text"  name="E01APCTAR" size="4" maxlength="2" value="<%= prd.getE01APCTAR().trim()%>" onKeyPress="enterInteger()">
            </td>
            <td > 
              <div align="right">Porcentaje para Provisi�n :</div>
            </td>
            <td ><INPUT type="text" name="E01APAOIS" size="11" maxlength="9" value="<%= prd.getE01APAOIS().trim()%>"  onKeyPress="enterDecimal()">
            </td>
          </tr>
          <tr id="trclear"> 
            <td > 
              <div align="right">Porcentaje Aplicaci�n MRC :</div>
            </td>
            <td > 
				<INPUT type="text" name="E01APACNV" size="11" maxlength="9" value="<%= prd.getE01APACNV().trim()%>" onKeyPress="enterDecimal()">
             </td>
            <td > 
              <div align="right">Visualizar Sitio Privado : </div>
            </td>
            <td width="330">
	          	  <input type="radio" name="E01APAFG1" value="Y"  <%if (prd.getE01APAFG1().equals("Y")) out.print("checked"); %>>
	              Si 
	              <input type="radio" name="E01APAFG1" value="N"  <%if (prd.getE01APAFG1().equals("N")) out.print("checked"); %>>
	              No
            </td>
          </tr>          

        </table>
      </td>
    </tr>
  </table>

   <p>
  <div align="center"> 
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
 </form>
</body>
</html>
