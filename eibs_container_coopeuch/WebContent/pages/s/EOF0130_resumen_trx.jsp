<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page import = "datapro.eibs.master.Util" %>
<%@ page import = "java.lang.Object" %>
<HTML>
<HEAD>
<TITLE>
Resumen de transaccion
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "Ree" 		class="datapro.eibs.beans.EOF013001Message"  scope="session" />
<jsp:useBean id= "userPO"   class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error"    class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT Language="javascript">

	

</SCRIPT>
</HEAD>
<BODY>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
 	  error.setERRNUM("0");
      out.println("<SCRIPT Language=\"Javascript\">");
      out.println("       showErrors()");
      out.println("</SCRIPT>");
 }
%> 

<h3 align="center">Reemplazo de Cheques de Terceros<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="resumen_trx, EOF0130"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEOF0130">
   <table class="tableinfo">
	<tr > 
	    <td nowrap> 
		<table cellspacing="0" cellpadding="2" width="100%" border="0"	class="tbhead">
	       	<tr id="trclear">
				<td nowrap width="10%"></td >
			    <td nowrap width="10%"><h4>CHEQUE # 1</h4></td >
			    <td nowrap width="40%"><div align="right">Referencia :</div></td>
				<td nowrap width="10%"><%= Ree.getE01RMPRE1().trim()%></td >
			</tr>
			
			<tr id="trdark">
				<td></td >
			    <td></td >
			    <td><div align="right">Fecha :</div></td>
				<td><%= Ree.getE01CNTRDD().trim() + "/"+ Ree.getE01CNTRDM().trim() + "/" + Ree.getE01CNTRDY().trim() %></td >
			</tr>
			
			<tr id="trdark">
				<td></td >
			    <td></td >
			    <td><div align="right">Monto :</div></td>
			    <td>$*****<%= Ree.getE01MTOCH1().trim()%></td >
			</tr>
			
			<tr id="trdark"> 
				<td></td >
				<td><div align="right">Pagar a la orden de :</div></td>
				<td><%= Ree.getE01BENEF1().trim()%> </td>
				<td></td>
			</tr>
			
			<tr id="trdark">
			    <td></td >
				<td><div align="right">La suma de :</div></td>
				<td><%= Ree.getE01LETTE1().trim()%></td>
				<td></td>
			</tr>
			
			<tr id="trdark">
			    <td></td >
				<td><div align="right">Concepto :</div></td>
		        <td><%= Ree.getE01RMPTYP().trim()%></td>
		        <td></td>
		    </tr>
		    
		    <tr id="trdark"> 
		        <td></td >
				<td nowrap width="20%" ><div align="right">Banco Emisor :</div></td>
		        <%if(Ree.getE01BCOOP1().trim().equals("1")){ %>
		        	<td><%= Ree.getE01BCOE11().trim()%></td>
		        <%}else{ %>
		            <td><%= Ree.getE01BCOE12().trim()%></td>
		        <%} %>
		        <td></td>
		    </tr>

		    <tr id="trdark"> 
		        <td></td >
		        <td></td >
		        <td></td >
		        <td></td >
		    </tr>

	    	</table>
		</td>
	</tr>	    		
</table>

	<%if(Ree.getE01RMPTYP().trim().equals("RME")){%>
   <table class="tableinfo">
	<tr > 
	    <td nowrap> 
		<table cellspacing="0" cellpadding="2" width="100%" border="0"	class="tbhead">
	       	<tr id="trclear">
				<td nowrap width="10%"></td >
			    <td nowrap width="10%"><h4>CHEQUE # 2</h4></td >
			    <td nowrap width="40%"><div align="right">Referencia :</div></td>
				<td nowrap width="10%"><%= Ree.getE01RMPRE2().trim()%></td >
			</tr>
			
			<tr id="trdark">
				<td></td >
			    <td></td >
			    <td><div align="right">Fecha :</div></td>
				<td><%= Ree.getE01CNTRDD().trim() + "/"+ Ree.getE01CNTRDM().trim() + "/" + Ree.getE01CNTRDY().trim() %></td >
			</tr>
			
			<tr id="trdark">
				<td></td >
			    <td></td >
			    <td><div align="right">Monto :</div></td>
			    <td>$*****<%= Ree.getE01MTOCH2().trim()%></td >
			</tr>
			
			<tr id="trdark"> 
				<td></td >
				<td><div align="right">Pagar a la orden de :</div></td>
				<td><%= Ree.getE01BENEF2().trim()%> </td>
				<td></td>
			</tr>
			
			<tr id="trdark">
			    <td></td >
				<td><div align="right">La suma de :</div></td>
				<td><%= Ree.getE01LETTE2().trim()%></td>
				<td></td>
			</tr>
			
			<tr id="trdark">
			    <td></td >
				<td><div align="right">Concepto :</div></td>
		        <td><%= Ree.getE01RMPTYP().trim()%></td>
		        <td></td>
		    </tr>
		    
		    <tr id="trdark"> 
		        <td></td >
				<td nowrap width="20%" ><div align="right">Banco Emisor :</div></td>
		        <%if(Ree.getE01BCOOP1().trim().equals("1")){ %>
		        	<td><%= Ree.getE01BCOE21().trim()%></td>
		        <%}else{ %>
		            <td><%= Ree.getE01BCOE22().trim()%></td>
		        <%} %>
		        <td></td>
		    </tr>
		    <tr id="trdark"> 
		        <td></td >
		        <td></td >
		        <td></td >
		        <td></td >
		    </tr>
	    	</table>
		</td>
	</tr>	    		
</table>
<%}%>

<table class="tableinfo">
	<tr > 
	    <td nowrap> 
			<table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
				<tr id="trclear">
	            	<td nowrap width="10%"></td> 
			        <td><div align="left">Los cheques han sido procesados satisfactoriamente...</div></td>
		       	</tr>

 	       		<%if(Ree.getE01RMPTYP().trim().equals("RMA")){%>
		        <tr id="trclear"> 
		        	<td></td> 
			        <td><div align="left">Para terminar la transaccion, en caja de debe recaudar un monto en efectivo de $******<%= Ree.getE01MTOEFE().trim()%></div></td>
			    </tr>
	          	<%}%>
      		</table>  
       	</td>   
	</tr > 
</table>

</FORM>
</BODY>
</HTML>
