package datapro.eibs.salesplatform;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV131001Message;
import datapro.eibs.beans.EPV131002Message;
import datapro.eibs.beans.ERC200001Message;
import datapro.eibs.beans.ERC200002Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.ExcelColStyle;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.services.ExcelUtils;
import datapro.eibs.sockets.MessageField;

/**
 **/
public class JSEPV1310 extends JSEIBSServlet {
	
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int A_excel_generate=600;
	protected static final int A_reconciliation_automatic=801;
	protected static final int A_delete_statement=900;
	protected static final int A_reconciliation_manual=400;

	protected static final int A_FILTER_CAMPAIGNS = 100;
	protected static final int A_LIST_CAMPAIGNS = 200;
	protected static final int A_INQUIRY_CAMPAIGNS = 300;

	
    /**
     * Default constructor. 
     */
	@Override
	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {
	
		switch (screen) {
		
		case A_FILTER_CAMPAIGNS:
			procReqCampaigns(user, req, res, session);
			break;
			
		case A_LIST_CAMPAIGNS:
			procActionCampaignsList(user, req, res, session);
			break;
			
		case A_INQUIRY_CAMPAIGNS:
			procActionCampaignsDetail(user,req,res,session);
			break;			
			
		default:
			forward(SuperServlet.devPage, req, res);
			break;
		}
		
		
	}
	
	
	
	protected void procReqCampaigns(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) ses.getAttribute("userPO");
		ses.setAttribute("userPO", userPO);
		try {
			flexLog("About to call Page: EPV1310_campaigns_enter.jsp");
			forward("EPV1310_campaigns_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	
	
	
	protected void procActionCampaignsList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			mp = getMessageProcessor("EPV1310", req);
		

			EPV131001Message msgList = (EPV131001Message) mp.getMessageRecord("EPV131001", user.getH01USR(), "0001");
			
			if (req.getParameter("E01CMPIDC")!= null){
				String IdCampana = req.getParameter("E01CMPIDC");								
				msgList.setE01CMPIDC(IdCampana);
			} 
			
			if(req.getParameter("posicion")!=null)
				msgList.setE01CCHREC(req.getParameter("posicion"));
			else 
				msgList.setE01CCHREC("0");

			
			setMessageRecord(req, msgList);
			//Sends message
				
			String fecha1=req.getParameter("E01FVIGDD")+"/"+req.getParameter("E01FVIGDM")+"/"+req.getParameter("E01FVIGDA");
			String fecha2=req.getParameter("E01FVIGHD")+"/"+req.getParameter("E01FVIGHM")+"/"+req.getParameter("E01FVIGHA");
			
			req.setAttribute("fecha_inicial", fecha1);
			req.setAttribute("fecha_final", fecha2);
			
			
			//session.setAttribute("lista_entrada", msgList);

			mp.sendMessage(msgList);
						
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { // if there are errors go back to first page
				session.setAttribute("error", error);
				flexLog("About to call Page: EPV1310_campaigns_enter.jsp");
				forward("EPV1310_campaigns_enter.jsp", req, res);
			} else {
			
				
			JBObjList list = mp.receiveMessageRecordList("H01FLGMAS", "E01CCHREC");
			//if there are NO errors display list
		
			  session.setAttribute("ERClist", list);
		
	          session.setAttribute("IdCampana", req.getParameter("E01CMPIDC"));
	      	  req.setAttribute("E01CCHREC", msgList.getE01CCHREC());

	          forward("EPV1310_campaigns_list.jsp", req, res);
			}

 		} finally {
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionCampaignsDetail(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			mp = getMessageProcessor("EPV1310", req);
			EPV131002Message msgList = (EPV131002Message) mp.getMessageRecord("EPV131002", user.getH01USR(), "0001");
	
			if (req.getParameter("codigo_lista")!= null){
				
				JBObjList list = (JBObjList)session.getAttribute("ERClist");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				EPV131001Message listMessage = (EPV131001Message)list.get(index);
				
				msgList.setE02CMPIDC(listMessage.getE01CMPIDC());
				req.setAttribute("codigo_lista", index);
				session.setAttribute("ERCSeleccion", listMessage);
			}
			
			if(req.getParameter("posicion")!=null)
				msgList.setE02CCDREC(req.getParameter("posicion"));

			
			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { // if there are errors go back to first page
				session.setAttribute("error", error);
				flexLog("About to call Page: EPV1310_campaigns_list.jsp");
				forward("EPV1310_campaigns_list.jsp", req, res);
			} else {
				JBObjList list = mp.receiveMessageRecordList("H02FLGMAS", "E02CCDREC");
				
		          session.setAttribute("ERC", list);
			  	  req.setAttribute("E01CMPIDC", req.getParameter("E01CMPIDC"));
				  req.setAttribute("E01CCHREC", req.getParameter("E01CCHREC"));
				  req.setAttribute("E01FVIGDD", req.getParameter("E01FVIGDD"));
				  req.setAttribute("E01FVIGDM", req.getParameter("E01FVIGDM"));
				  req.setAttribute("E01FVIGDA", req.getParameter("E01FVIGDA"));
				  req.setAttribute("E01FVIGHD", req.getParameter("E01FVIGHD"));
				  req.setAttribute("E01FVIGHM", req.getParameter("E01FVIGHM"));
				  req.setAttribute("E01FVIGHA", req.getParameter("E01FVIGHA"));
				  
				  forwardOnSuccess("EPV1310_campaigns_detail.jsp", req, res);
			} 

		} finally {
			if (mp != null)
				mp.close();
		}
	}
}
