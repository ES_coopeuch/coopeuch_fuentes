<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.ECC150101Message,datapro.eibs.beans.ECC150102Message"%>
<%@ page import="com.datapro.constants.EibsFields"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html> 
<head>
<title>Preemisiones Tarjeta de Cr&eacute;dito</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="PreemDetalle" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<script type="text/javascript" src="<%=request.getContextPath()%>/jquery/jquery-1.7.2.js"> </script>

<script type="text/javascript"> 
  function goAction(op) 
   {
 	document.forms[0].SCREEN.value = op;
	document.forms[0].action = "<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC1501?SCREEN="+op+"&E02CCDIDN=<%=request.getAttribute("E02CCDIDN")%>";
 	document.forms[0].submit();
   }
</SCRIPT>  
</head>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>
<h3 align="center">Consulta Detalle de Preemisiones por RUT<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="ECC_list_rut_detail.jsp, ECC1501"></h3>
<hr size="4">
<form method="POST"	action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSECC1501">
<input type="hidden" name="SCREEN" value="2200">

   <table  class="tableinfo" width="100%">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" align="center" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right">Rut Socio : 
              </td>
             <td nowrap width="90%" align="left">
	  			<input type="text" name="cartola" value="<%=request.getAttribute("E02CCDIDN") %>" 
	  			size="23"  readonly>
             </td>  
             </tr>
        </table>
      </td>
    </tr>
  </table>
 
 

<%
	if (PreemDetalle.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>

<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="10%">
			<a href="javascript:goAction('2200')"><b>Exportar Excel</b></a> 
		</td>
	</tr>
</table>

<table class="tbenter" width=100% >
	<tr>
		<td height="20"></td>
	</tr>
	<tr>
		<td>
		<table id="headTable" width="100%" align="left">
			<tr id="trdark">
				<th align="center" nowrap width="10%">Id Interfaz</th>
				<th align="center" nowrap width="10%">Canal de Venta</th>
				<th align="center" nowrap width="15%">Solicitud</th>
				<th align="center" nowrap width="15%">N� Producto</th>
				<th align="center" nowrap width="10%">Cod Prod</th>
				<th align="center" nowrap width="15%">Cupo Nacional</th>
				<th align="center" nowrap width="15%">Cupo Internacional</th>
				<th align="center" nowrap width="15%">Oficina</th>
				<th align="center" nowrap width="10%">Vendedor</th>
				<th align="center" nowrap width="10%">Estado</th>
				<th align="center" nowrap width="20%">Causal Rechazo</th>
				<th align="center" nowrap width="111">Fecha Estado</th>
			</tr>
			<%
			PreemDetalle.initRow();
				int k = 0,inicial;
				boolean firstTime = true;
				String chk = "";
				while (PreemDetalle.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					ECC150102Message PDetalle = (ECC150102Message) PreemDetalle.getRecord();
		%>
			<tr>
				<td nowrap align="center" height="28"><%=PDetalle.getE02CCDIDC()%></td>
				<td nowrap align="right" height="28"><%=PDetalle.getE02CCDGCA()%></td>
				<td nowrap align="left" height="28"><%=PDetalle.getE02CCDNUM()%></td>
				<td nowrap align="center" height="28"><%=PDetalle.getE02CCDACC()%></td>
				<td nowrap align="center" height="28"><%=PDetalle.getE02CCDPRD()%></td>
				<td nowrap align="right" height="28"><%=Util.formatCCY(PDetalle.getE02CCDNAC())%></td>
				<td nowrap align="left" height="28"><%=Util.formatCCY(PDetalle.getE02CCDINT())%></td>
				<td nowrap align="right" height="28"><%=PDetalle.getE02CCDGBR()%></td>
				<td nowrap align="center" height="28"><%=PDetalle.getE02CCDIDV()%></td>
				<td nowrap align="left" height="28"><%=PDetalle.getE02CCDGST()%></td>
				<td nowrap align="left" height="28"><%=PDetalle.getE02CCDGER()%></td>
				
				<td nowrap align="center" height="28">
				
				
				<% out.print(PDetalle.getE02CCDUPD()+"/"+PDetalle.getE02CCDUPM()+"/"+PDetalle.getE02CCDUPY());%>
				</td>
			</tr>
			<%}%>
		</table>
		</td>
	</tr>
</table>
<%
	}
%>
</form>
</body>
</html>
