<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<%@page import="java.math.BigDecimal"%><html> 
<head>
<title>Plataforma de Venta</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="platfObj" class="datapro.eibs.beans.EPV101010Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />
<jsp:useBean id= "servlet" class= "java.lang.String"  scope="session" />
<jsp:useBean id= "pagOperation" class= "java.lang.String"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

//  Process according with user selection
 function goAction(op) {	
	document.forms[0].SCREEN.value = op;
	if (op=='601' || op=='600'){//simular y enviar
		
	}	
	
	if (document.forms[0].servlet.value=="4040" && op!='500'){
 		self.window.location.href = "<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD4040?SCREEN=300&tipoLlamada=TC&E01CUN=" + document.forms[0].customer_number.value;
 	}else{
		document.forms[0].submit();
	}
 }

 
 
 </script>
</head>

<%
	boolean readOnly=false;
	boolean maintenance=false;
%> 
          
<%
	// Determina si es solo lectura
	if (request.getParameter("readOnly") != null ){
		if (request.getParameter("readOnly").toLowerCase().equals("true")){
			readOnly=true;
		} else {
			readOnly=false;
		}
	}
%>
<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
	if (!userPO.getPurpose().equals("NEW")) {
		maintenance = true;
		out.println("<SCRIPT> initMenu(); </SCRIPT>");
	}
%>

<h3 align="center">
<% if (maintenance){ %>
	MANTENIMIENTO 
<%} else { %>INGRESO
	
<%} %>
SOLICITUD TARJETA DE CREDITO <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="EPV1010_salesplatform_cards_maintenance,JSEPV1010"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1010">
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="">
  <input type=HIDDEN name="customer_number"  value="<%= userPO.getCusNum()%>">
  <input type=HIDDEN name="E01PVMBNK"  value="<%= currUser.getE01UBK().trim()%>">  

<!--  Tipo de medio de evaluacion -->  
  <INPUT TYPE=HIDDEN NAME="E10TPRTPR" VALUE="<%=platfObj.getE10TPRTPR()%>">
<!--  va a motor Y / N-->  
  <INPUT TYPE=HIDDEN NAME="E10TPRFEV" VALUE="<%=platfObj.getE10TPRFEV()%>">
<!--  bussines object name-->  
  <INPUT TYPE=HIDDEN NAME="E10TPRBOB" VALUE="<%=platfObj.getE10TPRBOB()%>">
  <INPUT TYPE=HIDDEN NAME="E10TPRBOR" VALUE="<%=platfObj.getE10TPRBOR()%>">
  <INPUT TYPE=HIDDEN NAME="E10TPRBOA" VALUE="<%=platfObj.getE10TPRBOA()%>">   
  
  <input type="hidden" name="servlet" value="<%= servlet %>" size=15 maxlength=9 >
  <input type="hidden" name="pagOperation" value="<%= pagOperation %>" size=15 maxlength=9 >
  
 <% int row = 0;%>
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">

          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Cliente :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="platfObj" property="E10PVMCUN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Nombre :</td>
             <td nowrap align="left" width="40%">
	  			<eibsinput:text name="platfObj" property="E10CUSNA1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Solicitud :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="platfObj" property="E10PVMNUM" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT %>" readonly="true" />
             </td>
             <td nowrap align="right" width="20%"> Fecha Solicitud :</td>
             <td nowrap align="left" width="40%">
    	        <eibsinput:date name="platfObj" fn_year="E10PVMOPY" fn_month="E10PVMOPM" fn_day="E10PVMOPD" readonly="true" readonly="true"/>
             </td>
         </tr>
         <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
             <td nowrap align="right" width="20%"> Sucursal :</td>
             <td nowrap align="left" width="20%">
	  			<eibsinput:text name="platfObj" property="E10PVMBRN" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_BRANCH %>" readonly="true"/>
             </td>
             <td nowrap align="right" width="20%"> Ejecutivo :</td>
             <td nowrap align="left" width="40%">
	  			<eibsinput:text name="platfObj" property="E10PVMOFC" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_OFFICER %>" readonly="true"/>
             </td>
         </tr>         
        </table>
      </td>
    </tr>
  </table>

  <table class=tbenter>
   <tr > 
      <td nowrap> 
		  <h4>Valores Maximo Permitidos</h4>
      </td>
      <td nowrap align=right> 
   		<b>Estado :</b>
      </td>
      <td nowrap> 
   		<b><font color="#ff6600"><%= platfObj.getE10DSCSTS().trim()%></font></b>
      </td>
    </tr>
  </table>
  
<br/>

  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cupo M�ximo Permitido :</div>
            </td>
            <td nowrap width="30%">             
            	<eibsinput:text name="platfObj" property="E10PVMMTT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" readonly="true"/> 
			</td>
            <td nowrap width="20%"> 
            </td>
            <td nowrap width="30%">
            </td>
          </tr>
        </table>
        </td>
    </tr>
  </table> 
  
  <h4>Tarjeta de Credito</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%" >
            	<div align="right">Producto :</div>
            </td>
            <td nowrap width="30%" >
	            <eibsinput:help name="platfObj" property="E10TCPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_PRODUCT%>" fn_param_one="E10TCPROD" fn_param_two="94" fn_param_three="document.forms[0].E01PVMBNK.value" readonly="<%=readOnly%>"/>
            	<eibsinput:text name="platfObj" property="E10DSPROD" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>"  readonly="true"/>	
            </td>
            <td nowrap width="20%"> 
              <div align="right">Direccion :</div>
            </td>
            <td nowrap width="30%">
              <input type="text" name="E10TCCMLA" size="3" maxlength="2" value="<%= platfObj.getE10TCCMLA().trim()%>"  >
              <a href="javascript:GetMailing('E10TCCMLA',document.forms[0].E10PVMCUN.value)"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="Direcciones de Correo del Cliente" align="absmiddle" border="0"></a> 
            </td>
          </tr>                 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Local :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="platfObj" property="E10TCCUMB" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="<%=readOnly%>"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Extranjera :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="platfObj" property="E10TCCUME" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="<%=readOnly%>"/>
            </td> 
            </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Dia de Pago :</div>
            </td>
            <td nowrap width="30%">             
				<select name="E10TCDYPG" <%=readOnly?"disabled":""%>>
					<option></option>
					<option value="3" <% if (platfObj.getE10TCDYPG().equals("3")) out.print("selected");%>>3</option>
					<option value="23" <% if (platfObj.getE10TCDYPG().equals("23")) out.print("selected");%>>23</option>
				</select>
             </td>
            <td nowrap width="20%"> 
              <div align="right">Porcentaje de Pago :</div>
            </td>
            <td nowrap width="30%">            
				<select name="E10TCPRPG" <%=readOnly?"disabled":""%>>
					<option></option>
					<option value="5" <% if (platfObj.getBigDecimalE10TCPRPG().compareTo(new BigDecimal("5")) == 0) out.print("selected");%>>El 5%</option>
					<option value="100" <% if (platfObj.getBigDecimalE10TCPRPG().compareTo(new BigDecimal("100")) == 0) out.print("selected");%>>El 100%</option>
				</select>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Medio de Pago :</div>
            </td>
            <td nowrap width="30%">
				<select name="E10TCMEPG" <%=readOnly?"disabled":""%>>
					<option></option>
					<option value="1" <% if (platfObj.getE10TCMEPG().equals("1")) out.print("selected");%>>Con Cuenta</option>
					<option value="2" <% if (platfObj.getE10TCMEPG().equals("2")) out.print("selected");%>>Sin Cuenta</option>
				</select>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cuenta de Pago :</div>
            </td>
            <td nowrap width="30%">            
 				<eibsinput:help name="platfObj" property="E10TCACPG" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" fn_param_one="E10TCACPG" fn_param_two="document.forms[0].E01PVMBNK.value" fn_param_three="RT" readonly="<%=readOnly %>"/>
            </td>
          </tr>  
        </table>
      </td>
    </tr>
  </table>   

  <h4>Adicional 1</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Apellido Paterno :</div>
            </td>
            <td nowrap width="30%">             
            	<eibsinput:text name="platfObj" property="E10ADAPP1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly%>"/> 
			</td>
            <td nowrap width="20%"> 
            </td>
            <td nowrap width="30%">
            </td>
          </tr>
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Apellido Materno :</div>
            </td>
            <td nowrap width="30%">             
            	<eibsinput:text name="platfObj" property="E10ADAPM1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly%>"/> 
			</td>
            <td nowrap width="20%"> 
            </td>
            <td nowrap width="30%">
            </td>
          </tr>
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Nombres :</div>
            </td>
            <td nowrap width="30%">             
            	<eibsinput:text name="platfObj" property="E10ADNOM1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly%>"/> 
			</td>
            <td nowrap width="20%"> 
            </td>
            <td nowrap width="30%">
            </td>
          </tr>
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Rut :</div>
            </td>
            <td nowrap width="30%">             
            	<eibsinput:text name="platfObj" property="E10ADRUT1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>" readonly="<%=readOnly%>"/> 
			</td>
            <td nowrap width="20%"> 
              <div align="right">Parentesco :</div>
            </td>
            <td nowrap width="30%">
                  <select name="E10ADREL1" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(platfObj.getE10ADREL1().equals("1")||platfObj.getE10ADREL1().equals("2") || platfObj.getE10ADREL1().equals("3")||platfObj.getE10ADREL1().equals("4")||platfObj.getE10ADREL1().equals("5")||platfObj.getE10ADREL1().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (platfObj.getE10ADREL1().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (platfObj.getE10ADREL1().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (platfObj.getE10ADREL1().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (platfObj.getE10ADREL1().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (platfObj.getE10ADREL1().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (platfObj.getE10ADREL1().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
          </tr>
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Fecha Nacimiento :</div>
            </td>
            <td nowrap width="30%">             
    	        <eibsinput:date name="platfObj" fn_year="E10ADFNY1" fn_month="E10ADFNM1" fn_day="E10ADFND1" readonly="<%=readOnly%>"/>
			</td>
            <td nowrap width="20%"> 
              <div align="right">Sexo :</div>
            </td>
            <td nowrap width="30%">
				<select name="E10ADSEX1" <%=readOnly?"disabled":""%>>
					<option></option>
					<option value="M" <% if (platfObj.getE10ADSEX1().equals("M")) out.print("selected");%>>Masculino</option>
					<option value="F" <% if (platfObj.getE10ADSEX1().equals("F")) out.print("selected");%>>Femenino</option>
				</select>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Local :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="platfObj" property="E10ADCMB1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="<%=readOnly%>"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Extranjera :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="platfObj" property="E10ADCME1" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="<%=readOnly%>"/>
            </td>
          </tr>                 
         </table>
        </td>
    </tr>
  </table>    

  <h4>Adicional 2</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Apellido Paterno :</div>
            </td>
            <td nowrap width="30%">             
            	<eibsinput:text name="platfObj" property="E10ADAPP2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly%>"/> 
			</td>
            <td nowrap width="20%"> 
            </td>
            <td nowrap width="30%">
            </td>
          </tr>
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Apellido Materno :</div>
            </td>
            <td nowrap width="30%">             
            	<eibsinput:text name="platfObj" property="E10ADAPM2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly%>"/> 
			</td>
            <td nowrap width="20%"> 
            </td>
            <td nowrap width="30%">
            </td>
          </tr>
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Nombres :</div>
            </td>
            <td nowrap width="30%">             
            	<eibsinput:text name="platfObj" property="E10ADNOM2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="<%=readOnly%>"/> 
			</td>
            <td nowrap width="20%"> 
            </td>
            <td nowrap width="30%">
            </td>
          </tr>
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Rut :</div>
            </td>
            <td nowrap width="30%">             
            	<eibsinput:text name="platfObj" property="E10ADRUT2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION%>" readonly="<%=readOnly%>"/> 
			</td>
            <td nowrap width="20%"> 
              <div align="right">Parentesco :</div>
            </td>
            <td nowrap width="30%">
                  <select name="E10ADREL2" <%=readOnly?"disabled":""%>>
                    <option value=" " <% if (!(platfObj.getE10ADREL2().equals("1")||platfObj.getE10ADREL2().equals("2") || platfObj.getE10ADREL2().equals("3")||platfObj.getE10ADREL2().equals("4")||platfObj.getE10ADREL2().equals("5")||platfObj.getE10ADREL2().equals("9"))) out.print("selected"); %>></option>
                    <option value="1" <% if (platfObj.getE10ADREL2().equals("1")) out.print("selected"); %>>Esposo(a)</option>                   
                    <option value="2" <% if (platfObj.getE10ADREL2().equals("2")) out.print("selected"); %>>Hijo</option>
                    <option value="3" <% if (platfObj.getE10ADREL2().equals("3")) out.print("selected"); %>>Padre</option>
                    <option value="4" <% if (platfObj.getE10ADREL2().equals("4")) out.print("selected"); %>>Madre</option>
                    <option value="5" <% if (platfObj.getE10ADREL2().equals("5")) out.print("selected"); %>>Hermano(a)</option>
                    <option value="6" <% if (platfObj.getE10ADREL2().equals("9")) out.print("selected"); %>>Otro</option>
                  </select>
           </td>
          </tr>
 
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Fecha Nacimiento :</div>
            </td>
            <td nowrap width="30%">             
    	        <eibsinput:date name="platfObj" fn_year="E10ADFNY2" fn_month="E10ADFNM2" fn_day="E10ADFND2" readonly="<%=readOnly%>"/>
			</td>
            <td nowrap width="20%"> 
              <div align="right">Sexo :</div>
            </td>
            <td nowrap width="30%">
				<select name="E10ADSEX2" <%=readOnly?"disabled":""%>>
					<option></option>
					<option value="M" <% if (platfObj.getE10ADSEX2().equals("M")) out.print("selected");%>>Masculino</option>
					<option value="F" <% if (platfObj.getE10ADSEX2().equals("F")) out.print("selected");%>>Femenino</option>
				</select>
            </td>
          </tr>
          <tr id="<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>"> 
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Local :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="platfObj" property="E10ADCMB2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="<%=readOnly%>"/>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Cupo Moneda Extranjera :</div>
            </td>
            <td nowrap width="30%">
	            <eibsinput:text name="platfObj" property="E10ADCME2" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_DECIMAL%>"  readonly="<%=readOnly%>"/>
            </td>
          </tr>                 
         </table>
        </td>
    </tr>
  </table>    
  
  <%if (!readOnly){%>
  <div align="center">
     <input id="EIBSBTN" type=button name="Enviar" value="Enviar"    onclick="goAction('500');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                      
     <input id="EIBSBTN" type=button name="Cancel" value="Cancelar"  onclick="goAction('101');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </div>
  <%}%>                   
  </form>
</body>
</HTML>
