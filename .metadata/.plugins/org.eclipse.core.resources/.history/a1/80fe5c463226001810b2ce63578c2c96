package datapro.eibs.client;

/**
 * Servlet de personas relacionadas.
 * Creation date: (15/02/18)
 * @author: Jose Vivas
 */

import java.beans.Beans;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import org.apache.tools.ant.taskdefs.Sleep;

import datapro.eibs.beans.EGL003501Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESD008001Message;
import datapro.eibs.beans.ESD008002Message;
import datapro.eibs.beans.ESD059001Message;
import datapro.eibs.beans.ESD400001Message;
import datapro.eibs.beans.ESD403001Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.SuperServlet;
import datapro.eibs.sockets.MessageContext;
import datapro.eibs.sockets.MessageField;
import datapro.eibs.sockets.MessageRecord;

public class JSESD4030 extends JSEIBSServlet {
	
	// Action 
	protected static final int R_ENTITY_LIST = 500;
	protected static final int A_ENTITY_LIST_HELP = 505;
	protected static final int R_NEW = 300;
	protected static final int A_NEW = 900;
	protected static final int R_MAINTENANCE = 700;
	protected static final int A_MAINTENANCE = 600;
	protected static final int R_DESVINCULAR = 800;
	protected static final int A_MAINTENANCEDESVINCULAR = 1600;

	protected String LangPath = "S";
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		MessageContext mc = null;
		
		try{
			flexLog("Opennig Socket Connection");
			mc = new MessageContext(super.getMessageHandler("ESD4030", req));
		}catch(Exception e){
			e.printStackTrace();
			flexLog("Socket not Open(" + mc.toString() + "). Error: " + e);
			res.sendRedirect(super.srctx + LangPath + super.sckNotOpenPage);
		}
		finally {
			mc.close();
		}
		
		switch (screen) {
			case R_ENTITY_LIST :
			case A_ENTITY_LIST_HELP :
				procReqEntityList(user, req, res, session, screen);
				break;
			case R_NEW :
				procReqNew(user, req, res, session);
				break;
			case A_NEW :
				procActionNew(user, req, res, session);
				procReqEntityList(user, req, res, session, 500);
				break;
			case R_MAINTENANCE :
				procReqMaintenanceModif(mc, user, req, res, session);
				break;
			case A_MAINTENANCE :
				procActionMaintenance(user, req, res, session);
				procReqEntityList(user, req, res, session, 500);
				break;
			case R_DESVINCULAR :
				procReqMaintenanceDesvin(mc, user, req, res, session);
				procReqEntityList(user, req, res, session, 500);
				break;
			case A_MAINTENANCEDESVINCULAR :
				procActionDesvincular(user, req, res, session);
				procReqEntityList(user, req, res, session, 500);
				break;
			default :
				forward(SuperServlet.devPage, req, res);
				break;
			}
	}
	
	/**
	 * Listar todas las personas relacionadas al socio titular
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @param screen
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqEntityList(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		MessageProcessor mp = null;	
		
		try{
			
			mp = getMessageProcessor("ESD4030", req);
			
			ESD403001Message msgPersonalRelation = (ESD403001Message) mp.getMessageRecord("ESD403001");
			
			msgPersonalRelation.setH01USR(user.getH01USR());
			msgPersonalRelation.setH01PGM("ESD4030");
			msgPersonalRelation.setH01TIM(getTimeStamp());
			msgPersonalRelation.setH01SCR("01");
			msgPersonalRelation.setH01OPE("0015");
			msgPersonalRelation.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPersonalRelation.setL01FILLE5(userPO.getHeader2().toString());
			msgPersonalRelation.setL01FILLE4("R");
			
			mp.sendMessage(msgPersonalRelation);
			
			JBObjList list = mp.receiveMessageRecordList("H01MAS");
			if(mp.hasError(list)){
				session.setAttribute("error", mp.getError(list));
				forward("error_viewer.jsp", req, res);
			}else{
				session.setAttribute("userPO", userPO);
				session.setAttribute("ESD403001List", list);
				if(screen == A_ENTITY_LIST_HELP)
					forward("ESD4030_entity_help_helpmessage.jsp", req, res);
				else
					forward("ESD4030_entity_list.jsp", req, res);
			}
		}finally{
			if(mp != null) mp.close();
		}		
	}//fin metodo procReqEntityList
	
	
	/**
	 * Metodo de modificar data de una persona relacionada, envia la data a la vista 
	 * @param mc
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqMaintenanceModif(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session)
	throws ServletException, IOException {

		ESD403001Message msgPersonalRelation;
		ELEERRMessage msgError = null;
		MessageRecord newmessage = null;
		MessageProcessor mp = null;
		UserPos	userPO = null;
		boolean IsNotError = false;

		try{
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try{
			JBObjList list = (JBObjList) session.getAttribute("ESD403001List");

			msgPersonalRelation = new ESD403001Message();

			msgPersonalRelation.setH01USR(user.getH01USR());
			msgPersonalRelation.setH01PGM("ESD4030");
			msgPersonalRelation.setH01TIM(getTimeStamp());
			msgPersonalRelation.setH01SCR("01");
			msgPersonalRelation.setH01OPE("0015");
			msgPersonalRelation.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPersonalRelation.setL01FILLE5(userPO.getHeader2());
			msgPersonalRelation.setL01FILLE6("M");

			boolean bandera=false;
			list.initRow();
			while(list.getNextRow()){
				ESD403001Message aux = (ESD403001Message) list.getRecord();
				if(aux.getE01PRR().equals(req.getParameter("ROW"))){
					msgPersonalRelation.setE01PRR(req.getParameter("ROW"));
					msgPersonalRelation.setE01PRN(aux.getE01PRN());
					msgPersonalRelation.setE01PRD(aux.getE01PRD());
					msgPersonalRelation.setE01PRM(aux.getE01PRM());
					msgPersonalRelation.setE01PRA(aux.getE01PRA());
					msgPersonalRelation.setE01PRC(aux.getE01PRC());
					msgPersonalRelation.setE01PRF(aux.getE01PRF());
					msgPersonalRelation.setE01PRTV(aux.getE01PRTV());
					msgPersonalRelation.setE01PRP(aux.getE01PRP());
					bandera = true;
					break;
				}
				else{
					bandera=false;
				}
			}
			if(bandera == true){
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				session.setAttribute("entity", msgPersonalRelation);
				flexLog(LangPath + "ESD4030_entity_maintenance.jsp");
				forward("ESD4030_entity_maintenance.jsp", req, res);
			}else{
				flexLog("Error al modificar el dato " + LangPath + "ESD4030_entity_list.jsp");
				forward("ESD4030_entity_list.jsp", req, res);
			}
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}
	
	/**
	 * Metodo de envio de data para desvincular a una persona relacionada.
	 * @param mc
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqMaintenanceDesvin(MessageContext mc, ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session)
	throws ServletException, IOException {

		ESD403001Message msgPersonalRelation;
		ELEERRMessage msgError = null;
		MessageRecord newmessage = null;
		MessageProcessor mp = null;
		UserPos	userPO = null;
		boolean IsNotError = false;

		try{
			msgError = new datapro.eibs.beans.ELEERRMessage();
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}

		userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try{
			JBObjList list = (JBObjList) session.getAttribute("ESD403001List");

			msgPersonalRelation = new ESD403001Message();

			msgPersonalRelation.setH01USR(user.getH01USR());
			msgPersonalRelation.setH01PGM("ESD4030");
			msgPersonalRelation.setH01TIM(getTimeStamp());
			msgPersonalRelation.setH01SCR("01");
			msgPersonalRelation.setH01OPE("0002");
			msgPersonalRelation.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPersonalRelation.setL01FILLE5(userPO.getHeader2());
			msgPersonalRelation.setL01FILLE6("E");

			boolean bandera=false;
			list.initRow();
			while(list.getNextRow()){
				ESD403001Message aux = (ESD403001Message) list.getRecord();
				if(aux.getE01PRR().equals(req.getParameter("ROW"))){
					msgPersonalRelation.setE01PRR(req.getParameter("ROW"));
					msgPersonalRelation.setE01PRN(aux.getE01PRN());
					msgPersonalRelation.setE01PRD(aux.getE01PRD());
					msgPersonalRelation.setE01PRM(aux.getE01PRM());
					msgPersonalRelation.setE01PRA(aux.getE01PRA());
					msgPersonalRelation.setE01PRC(aux.getE01PRC());
					msgPersonalRelation.setE01PRF(aux.getE01PRF());
					msgPersonalRelation.setE01PRTV(aux.getE01PRTV());
					msgPersonalRelation.setE01PRP("N");
					bandera = true;
					break;
				}
				else{
					bandera=false;
				}
			}
			if(bandera == true){
				msgPersonalRelation.send();	
				msgPersonalRelation.destroy();
				
				session.setAttribute("error", msgError);
				session.setAttribute("userPO", userPO);
				session.setAttribute("entity", msgPersonalRelation);
				flexLog(LangPath + "ESD4030_entity_list.jsp");
			}else{
				flexLog("Error al desvincular el dato " + LangPath + "ESD4030_entity_list.jsp");
				forward("ESD4030_entity_list.jsp", req, res);
			}
		}catch (Exception e){
			e.printStackTrace();
			flexLog("Error: " + e);
			throw new RuntimeException("Socket Communication Error");
		}
	}
	
	/**
	 * Solicitar la data a registrar un nuevo socio titular
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqNew(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		ELEERRMessage msgError = null;
		MessageProcessor mp = null;
		
		try{
			try{
				msgError = new datapro.eibs.beans.ELEERRMessage();
			}catch(Exception e){
				flexLog("Error: " + e); 
			}
			
			JBObjList bl = (JBObjList) session.getAttribute("ESD403001Help");
			
			mp = getMessageProcessor("ESD4030", req);
			
			ESD403001Message msgPersonalRelation = (ESD403001Message) mp.getMessageRecord("ESD403001");
			msgPersonalRelation.setH01USR(user.getH01USR());
	  		msgPersonalRelation.setH01PGM("ESD4030");
	  		msgPersonalRelation.setH01TIM(getTimeStamp());
	  		msgPersonalRelation.setH01SCR("01");
	  		msgPersonalRelation.setH01OPE("0001");
	  		msgPersonalRelation.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
	  		msgPersonalRelation.setL01FILLE5(userPO.getHeader2());
	  		msgPersonalRelation.setL01FILLE6("I");
	  		
	  		session.setAttribute("userPO", userPO);
			session.setAttribute("entity", msgPersonalRelation);
			flexLog(LangPath + "ESD4030_entity_maintenance.jsp");
			forward("ESD4030_entity_maintenance.jsp", req, res);
			
		}finally{
			if(mp != null) mp.close();
		}
	}
	/**
	 * Registra al socio titular
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionNew(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		MessageContext mc = null;
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		MessageProcessor mp = null;

		try {
			mp = getMessageProcessor("ESD4030", req);
			JBObjList list = (JBObjList) session.getAttribute("ESD403001List");
			

			ESD403001Message msgPersonalRelation = (ESD403001Message) mp.getMessageRecord("ESD403001");
			msgPersonalRelation.setH01USR(user.getH01USR());
			msgPersonalRelation.setH01PGM("ESD4030");
			msgPersonalRelation.setH01TIM(getTimeStamp());
			msgPersonalRelation.setH01SCR("01");
			msgPersonalRelation.setH01OPE("0001");
			msgPersonalRelation.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPersonalRelation.setL01FILLE5(userPO.getHeader2().toString());
			msgPersonalRelation.setL01FILLE4("R");
			msgPersonalRelation.setL01FILLE6("I");
			
			boolean bandera = false;
			list.initRow();
			while(list.getNextRow()){
				ESD403001Message aux = (ESD403001Message) list.getRecord();
				if(aux.getE01PRR().equals(req.getParameter("E01PRR"))){
					bandera = true;
					break;
				}
				else {
					bandera = false;
				}
			}
			
			if(bandera == false){
				setMessageRecord(req, msgPersonalRelation);
				msgPersonalRelation.send();	
				msgPersonalRelation.destroy();
				try{
					mc = new MessageContext(super.getMessageHandler("ESD4030", req));
				}catch(Exception e){
					e.printStackTrace();
					flexLog("Socker not open("+ mc.toString() + "). Error: " + e);
					res.sendRedirect(super.srctx + super.sckNotOpenPage);
				}
				session.setAttribute("userPO", userPO);
				session.setAttribute("entity", msgPersonalRelation);
			}
			else{
				forward("ESD4030_entity_maintenance.jsp", req, res);	
			}				
		} finally {
			if (mp != null)	mp.close();
		}
	}//fin procActionNew
	
	/**
	 * Buscar el relacionado a modificar su data
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqMaintenance( ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		
		try{
			mp = getMessageProcessor("ESD4030", req);
			
			try{
				
				JBObjList list = (JBObjList) session.getAttribute("ESD403001List");
				
				msgError = new datapro.eibs.beans.ELEERRMessage();
				
				ESD403001Message msgPersonalRelation = new  ESD403001Message();
				msgPersonalRelation.setH01USR(user.getH01USR());
				msgPersonalRelation.setH01PGM("ESD4030");
				msgPersonalRelation.setH01TIM(getTimeStamp());
				msgPersonalRelation.setH01SCR("01");
				msgPersonalRelation.setH01OPE("0015");
				msgPersonalRelation.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
				msgPersonalRelation.setL01FILLE5(userPO.getHeader2().toString());
				msgPersonalRelation.setL01FILLE4("R");
				msgPersonalRelation.setL01FILLE6("M");
				
				boolean bandera = false;
				list.initRow();
				while(list.getNextRow()){
					ESD403001Message aux = (ESD403001Message) list.getRecord();
					if(aux.getE01PRR().equals(req.getParameter("ROW"))){
						msgPersonalRelation.setE01PRR(req.getParameter("ROW"));
						msgPersonalRelation.setE01PRN(aux.getE01PRN());
						msgPersonalRelation.setE01PRD(aux.getE01PRD());
						msgPersonalRelation.setE01PRM(aux.getE01PRM());
						msgPersonalRelation.setE01PRA(aux.getE01PRA());
						msgPersonalRelation.setE01PRC(aux.getE01PRC());
						msgPersonalRelation.setE01PRF(aux.getE01PRF());
						msgPersonalRelation.setE01PRTV(aux.getE01PRTV());
						msgPersonalRelation.setE01PRP(aux.getE01PRP());
						bandera = true;
						break;
					}
					else {
						bandera = false;
					}
				}
				
				if(bandera == true){
					session.setAttribute("error", msgError);
			  		session.setAttribute("userPO", userPO);
			  		session.setAttribute("entity", msgPersonalRelation);
			  		flexLog(LangPath + "ESD4030_entity_maintenance.jsp");
			  		forward("ESD4030_entity_maintenance.jsp", req, res);
				}else{
					flexLog("Error al modificar el dato " + LangPath + "ESD4030_entity_list.jsp");
					forward("ESD4030_entity_list.jsp", req, res);
				}
				procReqEntityList(user, req, res, session, 500);
			}catch(Exception e){
				e.printStackTrace();
		  		flexLog("Error: " + e);
		  		throw new RuntimeException("Socket Communication Error");
			}
		}finally{
			if (mp != null)	mp.close();
		}
		
	}//fin procReqMaintenance
	
	/**
	 * Registrar lo data de modificar
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionMaintenance(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		MessageProcessor mp = null;
		MessageContext mc = null;
		
		try{
			mp = getMessageProcessor("ESD4030", req);
			
				ESD403001Message msgPersonalRelation = (ESD403001Message) mp.getMessageRecord("ESD403001");
				msgPersonalRelation.setH01USR(user.getH01USR());
				msgPersonalRelation.setH01PGM("ESD4030");
				msgPersonalRelation.setH01TIM(getTimeStamp());
				msgPersonalRelation.setH01SCR("01");
				msgPersonalRelation.setH01OPE("0002");
				msgPersonalRelation.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
				msgPersonalRelation.setL01FILLE5(userPO.getHeader2().toString());
				msgPersonalRelation.setL01FILLE4("R");
			
			setMessageRecord(req, msgPersonalRelation);
			msgPersonalRelation.send();	
			msgPersonalRelation.destroy();
			
			try{
				mc = new MessageContext(super.getMessageHandler("ESD4030", req));
			}catch(Exception e){
				e.printStackTrace();
				flexLog("Socker not open("+ mc.toString() + "). Error: " + e);
				res.sendRedirect(super.srctx + super.sckNotOpenPage);
			}
			session.setAttribute("userPO", userPO);
			session.setAttribute("entity", msgPersonalRelation);
		}finally{
			if (mp != null)	mp.close();
		}
		
	} // fin procActionMaintenance

	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqDesvincular(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos	userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		MessageContext mc =  null;
		
		try{
			
			mp = getMessageProcessor("ESD4030", req);
			
			ESD403001Message msgPersonalRelation = (ESD403001Message) mp.getMessageRecord("ESD403001");
			JBObjList list = (JBObjList) session.getAttribute("ESD403001List");
			
			msgPersonalRelation.setH01USR(user.getH01USR());
			msgPersonalRelation.setH01PGM("ESD4030");
			msgPersonalRelation.setH01TIM(getTimeStamp());
			msgPersonalRelation.setH01SCR("01");
			msgPersonalRelation.setH01OPE("0015");
			msgPersonalRelation.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPersonalRelation.setL01FILLE5(userPO.getHeader2().toString());
			msgPersonalRelation.setL01FILLE4("R");
			msgPersonalRelation.setL01FILLE6("E");
			
			boolean bandera = false;
			list.initRow();
			while(list.getNextRow()){
				ESD403001Message aux = (ESD403001Message) list.getRecord();
				if(aux.getE01PRR().equals(req.getParameter("ROW"))){
					
					msgPersonalRelation.setE01PRR(req.getParameter("ROW"));
					msgPersonalRelation.setE01PRN(aux.getE01PRN());
					msgPersonalRelation.setE01PRD(aux.getE01PRD());
					msgPersonalRelation.setE01PRM(aux.getE01PRM());
					msgPersonalRelation.setE01PRA(aux.getE01PRA());
					msgPersonalRelation.setE01PRC(aux.getE01PRC());
					msgPersonalRelation.setE01PRF(aux.getE01PRF());
					msgPersonalRelation.setE01PRTV(aux.getE01PRTV());
					msgPersonalRelation.setE01PRP("N");
					bandera= true;
					break;
				}		
				else{
					bandera=false;
				}
			}
			try{
				
				if(bandera==true){
			  		session.setAttribute("userPO", userPO);
			  		session.setAttribute("entity", msgPersonalRelation);
			  		flexLog(LangPath + "ESD4030_entity_maintenance.jsp");
			  		forward("ESD4030_entity_maintenance.jsp", req, res);
				}
				else{
					flexLog("Error al modificar el dato " + LangPath + "ESD4030_entity_list.jsp");
					forward("ESD4030_entity_list.jsp", req, res);
				}
			}catch(Exception e){
				e.printStackTrace();
				flexLog("Socker not open("+ mc.toString() + "). Error: " + e);
				res.sendRedirect(super.srctx + super.sckNotOpenPage);
			}
	
		}finally{
			if (mp != null)	mp.close();
		}
		
	}//fin de procReqDesvincular
	
	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procActionDesvincular(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session) throws ServletException, IOException {
		
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		MessageProcessor mp = null;
		MessageContext mc = null;
		
		
		try{
			mp = getMessageProcessor("ESD4030", req);
			JBObjList list = (JBObjList) session.getAttribute("ESD403001List");
			
			ESD403001Message msgPersonalRelation = (ESD403001Message) mp.getMessageRecord("ESD403001");
			
			msgPersonalRelation.setH01USR(user.getH01USR());
			msgPersonalRelation.setH01PGM("ESD4030");
			msgPersonalRelation.setH01TIM(getTimeStamp());
			msgPersonalRelation.setH01SCR("01");
			msgPersonalRelation.setH01OPE("0002");
			msgPersonalRelation.setL01FILLE1(req.getParameter("CUSCUN")!=null ? req.getParameter("CUSCUN") : userPO.getCusNum());
			msgPersonalRelation.setL01FILLE5(userPO.getHeader2().toString());
			msgPersonalRelation.setL01FILLE4("R");
			msgPersonalRelation.setL01FILLE6("E");
			
			boolean bandera = false;
			list.initRow();
			while(list.getNextRow()){
				ESD403001Message aux = (ESD403001Message) list.getRecord();
				if(aux.getE01PRR().equals(req.getParameter("ROW"))){					
					msgPersonalRelation.setE01PRR(req.getParameter("ROW"));
					msgPersonalRelation.setE01PRN(aux.getE01PRN());
					msgPersonalRelation.setE01PRD(aux.getE01PRD());
					msgPersonalRelation.setE01PRM(aux.getE01PRM());
					msgPersonalRelation.setE01PRA(aux.getE01PRA());
					msgPersonalRelation.setE01PRC(aux.getE01PRC());
					msgPersonalRelation.setE01PRF(aux.getE01PRF());
					msgPersonalRelation.setE01PRTV(aux.getE01PRTV());
					msgPersonalRelation.setE01PRP(aux.getE01PRP());
					bandera= true;
					break;
				}		
				else{
					bandera=false;
				}
			}
				
			try{
				
				if(bandera== true){
					msgPersonalRelation.send();	
					msgPersonalRelation.destroy();
					try{
						mc = new MessageContext(super.getMessageHandler("ESD4030", req));
					}catch(Exception e){
						e.printStackTrace();
						flexLog("Socker not open("+ mc.toString() + "). Error: " + e);
						res.sendRedirect(super.srctx + super.sckNotOpenPage);
					}
					session.setAttribute("userPO", userPO);
					session.setAttribute("entity", msgPersonalRelation);
				}
				else{
					flexLog("Error al modificar el dato " + LangPath + "ESD4030_entity_list.jsp");
					forward("ESD4030_entity_list.jsp", req, res);
				}	
			}catch(Exception e){
				e.printStackTrace();
				flexLog("Socker not open("+ mc.toString() + "). Error: " + e);
				res.sendRedirect(super.srctx + super.sckNotOpenPage);
			}
		}finally{
			if (mp != null)	mp.close();
		}
		
	}//procActionDesvincular
	
	/**
	 * Metodo de error
	 * @param 
	 */
	protected void showERROR(ELEERRMessage m)
	{
		if (logType != NONE) {
			
			flexLog("ERROR received.");
			
			flexLog("ERROR number:" + m.getERRNUM());
			flexLog("ERR001 = " + m.getERNU01() + " desc: " + m.getERDS01() + " code : " + m.getERDF01());
			flexLog("ERR002 = " + m.getERNU02() + " desc: " + m.getERDS02() + " code : " + m.getERDF02());
			flexLog("ERR003 = " + m.getERNU03() + " desc: " + m.getERDS03() + " code : " + m.getERDF03());
			flexLog("ERR004 = " + m.getERNU04() + " desc: " + m.getERDS04() + " code : " + m.getERDF04());
			flexLog("ERR005 = " + m.getERNU05() + " desc: " + m.getERDS05() + " code : " + m.getERDF05());
			flexLog("ERR006 = " + m.getERNU06() + " desc: " + m.getERDS06() + " code : " + m.getERDF06());
			flexLog("ERR007 = " + m.getERNU07() + " desc: " + m.getERDS07() + " code : " + m.getERDF07());
			flexLog("ERR008 = " + m.getERNU08() + " desc: " + m.getERDS08() + " code : " + m.getERDF08());
			flexLog("ERR009 = " + m.getERNU09() + " desc: " + m.getERDS09() + " code : " + m.getERDF09());
			flexLog("ERR010 = " + m.getERNU10() + " desc: " + m.getERDS10() + " code : " + m.getERDF10());
			
		}
	}
}
