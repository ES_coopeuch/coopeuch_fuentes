<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="com.datapro.constants.Entities"%> 
<html>
<head>
<title>Prestamos</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id="datarec" class="datapro.eibs.beans.EDL114801Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id="userPO" class="datapro.eibs.beans.UserPos" scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBSBillsP.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </script>

<script type="text/javascript">

  	  function cerrarVentana(){
		window.open('','_parent','');
		window.close(); 
  	}
  	
//  Process according with user selection
 function goAction(op) {
	
   	switch (op){
	//Cancel
	case 1:  {
 		//document.forms[0].SCREEN.value = "101";
 		cerrarVentana(); 		
       	break;
		}
	}
	//document.forms[0].submit();
 }
 
 </script>
</head>

<body>
<%
	if (!error.getERRNUM().equals("0")) {
		error.setERRNUM("0");
		out.println("<SCRIPT Language=\"Javascript\">");
		out.println("       showErrors()");
		out.println("</SCRIPT>");
	}
%>

<h3 align="center">SOLICITUD DE CUPONERA DE PRESTAMOS <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="cuponera_maintenance.jsp, EDL1148"></h3>
<hr size="4">

<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDL1148" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="600">
  <input type=HIDDEN name="BANCO"  value="<%= currUser.getE01UBK().trim()%>">
  <input type=HIDDEN name="H01FLGWK1"  value="<%= datarec.getH01FLGWK1().trim()%>">

 <% int row = 0;%>
 
 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Cliente :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01DEACUN" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER%>" readonly="true"/>
   	        </td>
           <td width="15%"> 
              <div align="right">Nombre :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01CUSNA1" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="true"/>
   	        </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Cuenta :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01DEAACC" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_ACCOUNT%>" readonly="true"/>
   	        </td>
           <td width="15%"> 
              <div align="right">Estado :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01STATUS" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME%>" readonly="true"/>
   	        </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Fecha Apertura :</div>
            </td>
            <td width="35%">
    	        <eibsinput:date name="datarec" fn_year="E01DEAODY" fn_month="E01DEAODM" fn_day="E01DEAODD" readonly="true"/>
   	        </td>
           <td width="15%"> 
              <div align="right">Fecha vencimiento :</div>
            </td>
            <td width="35%">
    	        <eibsinput:date name="datarec" fn_year="E01DEAMDY" fn_month="E01DEAMDM" fn_day="E01DEAMDD" readonly="true"/>
   	        </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Monto Origen :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01DEAOAM" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
   	        </td>
           <td width="15%"> 
              <div align="right">Tasa :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01LNSRTE" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_RATE%>" readonly="true"/>
   	        </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Saldo Principal :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01DEAMEP" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
   	        </td>
            <td width="15%"> 
              <div align="right">Periodo Base :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01LNSBAS" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_INTEGER%>" size="4" readonly="true"/>
   	        </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Saldo Intereses :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01DEAMEI" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
   	        </td>
           <td width="15%"> 
              <div align="right">saldo de interes de Mora :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01DEAMEM" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
   	        </td>
          </tr>

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="15%"> 
              <div align="right">Total Deuda :</div>
            </td>
            <td width="35%">
	            <eibsinput:text property="E01TOTDUE" name="datarec" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT%>" readonly="true"/>
   	        </td>
           <td width="15%"> 
              <div align="right"></div>
            </td>
            <td width="35%">
   	        </td>
          </tr>
 
        </table>
      </td>
    </tr>
  </table>

  
<% if (datarec.getH01FLGWK1().trim().equals("Y")) {%> 
  
<br>
 
 <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">

          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="50%" nowrap> 
              <div  align="right">Se ha solicitado Previamente una cuponera del prestamo en :</div>
            </td>
            <td width="35%">
    	        <eibsinput:date name="datarec" fn_year="E01DLUFSY" fn_month="E01DLUFSM" fn_day="E01DLUFSD" readonly="true"/>
   	        </td>
          </tr>
          <tr id='<%= (row % 2 == 1) ? "trdark" : "trclear" %><%row++;%>'> 
            <td width="50%" nowrap> 
              <div  align="right">Esta pendiente de procesarlo lo desea Eliminar :</div>
            </td>
    		<td>
	   			<input type="radio" name="E01FLGSEL" value="Y" onClick="document.forms[0].E01FLGSEL.value='Y'"
	  			<%if(datarec.getE01FLGSEL().equals("Y")) out.print("checked");%>>
          			S&iacute; 
       			<input type="radio" name="E01DEAF01" value="N" onClick="document.forms[0].E01FLGSEL.value='N'"
	  			<%if(datarec.getE01FLGSEL().equals("N")) out.print("checked");%>>
           			No 
			</td>
          </tr>
 
        </table>
      </td>
    </tr>
  </table>
<% } %> 

  
   <div align="center"> 
  	<input id="EIBSBTN" type=submit name="Submit" value="Enviar">
   </div>     

  </form>
</body>
</HTML>
