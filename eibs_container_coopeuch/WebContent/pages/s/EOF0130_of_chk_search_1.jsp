
<%@ page import = "datapro.eibs.master.Util" %>  
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>

<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>

<html>
<head>
<title>Reemplazo de cheques</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "checkSel" class= "datapro.eibs.beans.ETL051001Message"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<SCRIPT Language="javascript">


function GoPage(){
	document.forms[0].SCREEN.value='350';
	document.forms[0].submit();
}	
	

</SCRIPT>

</head>
<body>
<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>
<h3 align="center">Reemplazo de Cheques <img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="of_chk_search_1, EOF0130"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSETL0510" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="500">
  <INPUT TYPE=HIDDEN NAME="OPTION" VALUE="<%= userPO.getHeader10()%>">
  <INPUT TYPE=HIDDEN NAME="E01SELSCH" VALUE="P"> <!-- Estado del Cheque -->

  <h4>Informaci&oacute;n B&aacute;sica de Seleccion</h4> 
  <table class="tableinfo">
    <tr > 
      <td nowrap> 
        <table cellpadding=2 cellspacing=0 width="100%" border="0">
          <tr id="trclear"> 
		        <td nowrap width="20%"> 
	            <div align="right">N&uacute;mero de Cheque :</div>
	            </td>
	            <td nowrap width="35%"> 
	              <input type="text" name="E01SELRE2" size="17" maxlength="16" value="<%= checkSel.getE01SELRE2()%>">
	              <a href="javascript:GoPage()"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" border="0" ></a> 
	            </td>
           </tr>
         </table>
      </td>
    </tr>
  </table>
  <div align="center"> 
    <input id="EIBSBTN" type=submit name="Submit" value="Enviar">
  </div>
  </form>
</body>
</html>
