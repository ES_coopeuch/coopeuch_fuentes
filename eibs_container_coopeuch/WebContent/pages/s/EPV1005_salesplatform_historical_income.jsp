<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV100505Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV100505List" class="datapro.eibs.beans.JBObjList"
	scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1"
	src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>

<script type="text/javascript">
function cerrarVentana(){
	window.open('','_parent','');
	window.close();
}
</script>

</head>

<body>
<% 

boolean warnig = false;
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");     
     out.println("       showErrors()");
	 out.println("    cerrarVentana();");     
     out.println("</SCRIPT>");
 }
%>

<h3 align="center"> Plataforma de Ventas - Rentas Historicas<img
	src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="salesplatform_historical_income.jsp,EPV1005"></h3>
<hr size="4">
 
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" class="tbhead">
          <tr>
             <td nowrap width="10%" align="right"> Numero: 
              </td>
             <td nowrap width="10%" align="left">
	  			<eibsinput:text name="userPO" property="cusNum" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_CUSTOMER %>" readonly="true"/>
             </td>
             <td nowrap width="10%" align="right">RUT:  
             </td>
             <td nowrap width="10%" align="left">            
	  			<eibsinput:text name="userPO" property="cusType" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_IDENTIFICATION %>" readonly="true"/>  			
             </td>
             <td nowrap width="10%" align="right"> Nombre: 
               </td>
             <td nowrap width="50%"align="left">
	  			<eibsinput:text name="userPO" property="cusName" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_NAME_FULL %>" readonly="true"/>
             </td>
         </tr>
        </table>
      </td>
    </tr>
  </table> 
<br/>
<%
	if (EPV100505List.getNoResult()) {
%>
<TABLE class="tbenter" width=100% height=50%>
	<TR>
		<TD>
		<div align="center">
		<font size="3"><b> 
			No hay resultados que correspondan a su criterio de b�squeda. 
		</b></font></div>
		</TD>
	</TR>
</TABLE>
<%
	} else {
%>

	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="20%">Solicitud</th>
			<th align="center" nowrap width="20%">Credito</th>
			<th align="center" nowrap width="15%">Renta Bruta</th>
			<th align="center" nowrap width="15%">Renta Liquida</th>
			<th align="center" nowrap width="15%">Renta Depurada</th>
			<th align="center" nowrap width="15%">Fecha Ingreso</th>
		</tr>
		<%
			EPV100505List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EPV100505List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EPV100505Message convObj = (EPV100505Message) EPV100505List.getRecord();
		%>
		<tr>		
			<td nowrap align="center"><%=Util.formatCell(convObj.getE05PVDNUM())%></td>
			<td nowrap align="center"><%=Util.formatCell(convObj.getE05PVDACC())%></td>
			<td nowrap align="right"><%=convObj.getE05PVMRBR()%></td>
			<td nowrap align="right"><%=convObj.getE05PVMICU()%></td>
			<td nowrap align="right"><%=convObj.getE05PVMRED()%></td>
			<td nowrap align="center"><%=Util.formatCell(convObj.getE05PVMOPD() + "/" + convObj.getE05PVMOPM() + "/"+ convObj.getE05PVMOPY())%> </td>
		</tr>
		<%
			}
		%>
	</table>
<%
	}
%>

       <div align="center"> 
           <input id="EIBSBTN" type="button" name="Cerrar" value="Cerrar" onclick="javascript:cerrarVentana();">
       </div>
</body>
</html>
