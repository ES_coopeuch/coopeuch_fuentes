package datapro.eibs.helps;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.EWD0130DSMessage;
import datapro.eibs.beans.EWD0900DSMessage;
import datapro.eibs.beans.JBList;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;

public class JSEWD0130D extends JSEIBSServlet {

	protected void processRequest(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session, int screen) throws ServletException,
			IOException {

		String argsp = req.getParameter("args") == null || req.getParameter("args").equals("") ? "0" : req.getParameter("args");
		int args; 
		try {
			args = Integer.parseInt(argsp);
		} catch (Exception e) {
			args = 0;
		}	
		
		MessageProcessor mp = null;
		try {
			mp = getMessageProcessor("ESS0030", req);
			EWD0130DSMessage msgHelp = (EWD0130DSMessage) mp.getMessageRecord("EWD0130DS", user.getH01USR(), "");
			msgHelp.setBNKUSR(user.getH01USR());
			msgHelp.setBNKNME(req.getParameter("NameSearch") == null ? "" : req.getParameter("NameSearch").toUpperCase());
			msgHelp.setBNKPVI(req.getParameter("PmtVia") == null ? "S" : req.getParameter("PmtVia").toUpperCase());
			
			mp.sendMessage(msgHelp);
			
			msgHelp = (EWD0130DSMessage) mp.receiveMessageRecord("EWD0130DS");
			
			StringBuffer myRow = null;
			JBList beanList = new JBList();
			
			while (!(msgHelp.getBNKPVI().equals("9"))) {
				myRow = new StringBuffer("<TR>");
				myRow.append("</TR>");
				if (args == 4) {
					myRow.append(
							"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getBNKID()
							+ "','"
							+ msgHelp.getBNKNME()
							+ "','"
							+ msgHelp.getBNKCTY()
							+ "','"
							+ msgHelp.getBNKCTR()
							+ "')\">"
							+ msgHelp.getBNKID()
							+ "</a></td>");
					myRow.append(
						"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getBNKID()
							+ "','"
							+ msgHelp.getBNKNME()
							+ "','"
							+ msgHelp.getBNKCTY()
							+ "','"
							+ msgHelp.getBNKCTR()
							+ "')\">"
							+ msgHelp.getBNKNME()
							+ "</a></td>");
					myRow.append(
						"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getBNKID()
							+ "','"
							+ msgHelp.getBNKNME()
							+ "','"
							+ msgHelp.getBNKCTY()
							+ "','"
							+ msgHelp.getBNKCTR()
							+ "')\">"
							+ msgHelp.getBNKCTR()
							+ "</a></td>");
					myRow.append(
						"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getBNKID()
							+ "','"
							+ msgHelp.getBNKNME()
							+ "','"
							+ msgHelp.getBNKCTY()
							+ "','"
							+ msgHelp.getBNKCTR()
							+ "')\">"
							+ msgHelp.getBNKSTA()
							+ "</a></td>");
					myRow.append(
						"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getBNKID()
							+ "','"
							+ msgHelp.getBNKNME()
							+ "','"
							+ msgHelp.getBNKCTY()
							+ "','"
							+ msgHelp.getBNKCTR()
							+ "')\">"
							+ msgHelp.getBNKCTY()
							+ "</a></td>");
				} else {
					myRow.append(
							"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getBNKID()
							+ "','"
							+ msgHelp.getBNKNME()
							+ "')\">"
							+ msgHelp.getBNKID()
							+ "</a></td>");
					myRow.append(
						"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getBNKID()
							+ "','"
							+ msgHelp.getBNKNME()
							+ "')\">"
							+ msgHelp.getBNKNME()
							+ "</a></td>");
					myRow.append(
						"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getBNKID()
							+ "','"
							+ msgHelp.getBNKNME()
							+ "')\">"
							+ msgHelp.getBNKCTR()
							+ "</a></td>");
					myRow.append(
						"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getBNKID()
							+ "','"
							+ msgHelp.getBNKNME()
							+ "')\">"
							+ msgHelp.getBNKSTA()
							+ "</a></td>");
					myRow.append(
						"<td nowrap><a href=\"javascript:enter('"
							+ msgHelp.getBNKID()
							+ "','"
							+ msgHelp.getBNKNME()
							+ "')\">"
							+ msgHelp.getBNKCTY()
							+ "</a></td>");
				}
				beanList.addRow("", myRow.toString());
				msgHelp = (EWD0130DSMessage) mp.receiveMessageRecord("EWD0130DS");
			}
			
			flexLog("Putting java beans into the session");
			session.setAttribute("bankIdHelp", beanList);
			
			req.setAttribute("NameSearch", req.getParameter("NameSearch"));
			
			forward("EWD0130D_bankid_help_helpmessage.jsp", req, res);
			
		} finally {
			if (mp != null)	mp.close();
		}
	}

}
