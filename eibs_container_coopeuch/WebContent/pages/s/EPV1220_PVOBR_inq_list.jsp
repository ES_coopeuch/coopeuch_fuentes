<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput"%>
<%@ page import="datapro.eibs.master.Util,datapro.eibs.beans.EPV122001Message"%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@page import="com.datapro.constants.EibsFields"%>
<html>
<head>
<title>Plataforma de Venta</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/pages/style.css"
	rel="stylesheet">

<jsp:useBean id="EPV122001List" class="datapro.eibs.beans.JBObjList" scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage" scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </script>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<script type="text/javascript">

function showInq(num) {
	page = webapp + "/servlet/datapro.eibs.salesplatform.JSEPV1220?SCREEN=203&COLUMNA=" + num;
	CenterWindow(page,600,500,2);
}

function closeHiddenDivNew(){
	setVisibility(document.getElementById("hiddenDivNew"), "hidden");
}

function showHiddenDivNew(evt) {
	evt = (evt) ? evt : ((window.event) ? window.event : "");
 	
	var hiddenDivNew = document.getElementById("hiddenDivNew");

	var y=evt.clientY + document.body.scrollTop;
	var x=evt.clientX + document.body.scrollLeft;
     
	cancelBub(evt);

	moveElement(hiddenDivNew, y, x);
	setVisibility(hiddenDivNew, "visible");
	 
}

document.onclick= closeHiddenDivNew;


</SCRIPT>  

</head>

<body>
<% 

 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Consulta Holguras por Sucursal <br> Plataforma de Venta<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="PVOBR_inq_list.jsp,EPV1220"></h3>
<hr size="4">
<form method="POST"
	action="<%=request.getContextPath()%>/servlet/datapro.eibs.salesplatform.JSEPV1220">
<input type="hidden" name="SCREEN" value="201"> 

<table class="tbenter" width="100%">
	<tr>
		<td align="center" class="tdbkg" width="20%"><a
			href="<%=request.getContextPath()%>/pages/background.jsp"><b>Salir</b></a>
		</td>
	</tr>
</table>

<%
	if (EPV122001List.getNoResult()) {
%>
<table class="tbenter" width=100% height=90%>
	<tr>
		<td>
		<div align="center">
			<font size="3">
				<b> No hay resultados que correspondan a su criterio de b�squeda. </b>
			</font>
		</div>
		</td>
	</tr>
</table>
<%
	} else {
%>

	<table id="headTable" width="100%">
		<tr id="trdark">
			<th align="center" nowrap width="15%">Sucursal</th>
			<th align="center" nowrap width="20%">Tipo Producto</th>
			<th align="center" nowrap width="10%">Tipo Limite</th>
			<th align="center" nowrap width="15%">Monto Limite</th>
    		<th align="center" nowrap width="10%">% Limite</th>		
    		<th align="center" nowrap width="15%">Holgura Aprobada</th> 
    		<th align="center" nowrap width="15%">Holgura Utilizada</th>      		   				
		</tr>
		<%
			EPV122001List.initRow();
				int k = 0;
				boolean firstTime = true;
				String chk = "";
				while (EPV122001List.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
					EPV122001Message pvobr = (EPV122001Message) EPV122001List
							.getRecord();
		%>
		<tr>											   
			<td nowrap align="center"><a href="javascript:showInq('<%= EPV122001List.getCurrentRow() %>');"><%=pvobr.getE01PVGBRN()%></a></td>	
			<td nowrap align="left"><a href="javascript:showInq('<%= EPV122001List.getCurrentRow() %>');"><%=pvobr.getE01TPRDES()%></a></td>	
			<td nowrap align="center"><a href="javascript:showInq('<%= EPV122001List.getCurrentRow() %>');"><%=pvobr.getE01PVGTYL()%></a></td>	
			<td nowrap align="right"><a href="javascript:showInq('<%= EPV122001List.getCurrentRow() %>');"><%=pvobr.getE01PVGAML()%></a></td>			
			<td nowrap align="center"><a href="javascript:showInq('<%= EPV122001List.getCurrentRow() %>');"><%=pvobr.getE01PVGPOL()%></a></td>			
			<td nowrap align="right"><a href="javascript:showInq('<%= EPV122001List.getCurrentRow() %>');"><%=pvobr.getE01PVGAMA()%></a></td>
			<td nowrap align="right"><a href="javascript:showInq('<%= EPV122001List.getCurrentRow() %>');"><%=pvobr.getE01PVGAMU()%></a></td>														
		</tr>
		<%
			}
		%>
	</table>


<table class="tbenter" width="98%" align="center">
	<tr>
		<td width="50%" align="left">
		<%
			if (EPV122001List.getShowPrev()) {
					int pos = EPV122001List.getFirstRec() - 51;
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1220?SCREEN=100&codNum="
									+ request.getAttribute("codNum")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
		<td width="50%" align="right">
		<%
			if (EPV122001List.getShowNext()) {
					int pos = EPV122001List.getLastRec();
					out
							.println("<A HREF=\""
									+ request.getContextPath()
									+ "/servlet/datapro.eibs.client.JSEPV1220?SCREEN=100&codNum="
									+ request.getAttribute("codNum")
									+ "\"><IMG border=\"0\" src=\""
									+ request.getContextPath()
									+ "/images/s/previous_records.gif\" ></A>");
				}
		%>
		</td>
	</tr>
</table>
<%
	}
%>
</form>
<SCRIPT language="JavaScript">
 	document.getElementById("hiddenDivNew").onclick=cancelBub;
	document.getElementById("eibsNew").onclick=showHiddenDivNew;  
</SCRIPT>
</body>
</html>

