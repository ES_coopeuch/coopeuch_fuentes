/**
 * ResumenAntecedentesComercialesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf170751.02 v1808105656
 */

package cl.Sinacofi.coopeuch.creditos.predictorriesgo;

public class ResumenAntecedentesComercialesResponse  implements java.io.Serializable {
    private java.lang.String codError;
    private java.lang.String desError;
    private cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResumenSinacofiType resumenSinacofiType;

    public ResumenAntecedentesComercialesResponse() {
    }

    public java.lang.String getCodError() {
        return codError;
    }

    public void setCodError(java.lang.String codError) {
        this.codError = codError;
    }

    public java.lang.String getDesError() {
        return desError;
    }

    public void setDesError(java.lang.String desError) {
        this.desError = desError;
    }

    public cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResumenSinacofiType getResumenSinacofiType() {
        return resumenSinacofiType;
    }

    public void setResumenSinacofiType(cl.Sinacofi.coopeuch.creditos.predictorriesgo.ResumenSinacofiType resumenSinacofiType) {
        this.resumenSinacofiType = resumenSinacofiType;
    }

    private transient java.lang.ThreadLocal __history;
    public boolean equals(java.lang.Object obj) {
        if (obj == null) { return false; }
        if (obj.getClass() != this.getClass()) { return false;}
        ResumenAntecedentesComercialesResponse other = (ResumenAntecedentesComercialesResponse) obj;
        boolean _equals;
        _equals = true
            && ((this.codError==null && other.getCodError()==null) || 
             (this.codError!=null &&
              this.codError.equals(other.getCodError())))
            && ((this.desError==null && other.getDesError()==null) || 
             (this.desError!=null &&
              this.desError.equals(other.getDesError())));
        if (!_equals) { return false; }
        if (__history == null) {
            synchronized (this) {
                if (__history == null) {
                    __history = new java.lang.ThreadLocal();
                }
            }
        }
        ResumenAntecedentesComercialesResponse history = (ResumenAntecedentesComercialesResponse) __history.get();
        if (history != null) { return (history == obj); }
        if (this == obj) return true;
        __history.set(obj);
        _equals = true
            && ((this.resumenSinacofiType==null && other.getResumenSinacofiType()==null) || 
             (this.resumenSinacofiType!=null &&
              this.resumenSinacofiType.equals(other.getResumenSinacofiType())));
        if (!_equals) {
            __history.set(null);
            return false;
        };
        __history.set(null);
        return true;
    }

    private transient java.lang.ThreadLocal __hashHistory;
    public int hashCode() {
        if (__hashHistory == null) {
            synchronized (this) {
                if (__hashHistory == null) {
                    __hashHistory = new java.lang.ThreadLocal();
                }
            }
        }
        ResumenAntecedentesComercialesResponse history = (ResumenAntecedentesComercialesResponse) __hashHistory.get();
        if (history != null) { return 0; }
        __hashHistory.set(this);
        int _hashCode = 1;
        if (getCodError() != null) {
            _hashCode += getCodError().hashCode();
        }
        if (getDesError() != null) {
            _hashCode += getDesError().hashCode();
        }
        if (getResumenSinacofiType() != null) {
            _hashCode += getResumenSinacofiType().hashCode();
        }
        __hashHistory.set(null);
        return _hashCode;
    }

}
