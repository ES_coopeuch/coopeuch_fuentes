package datapro.eibs.salesplatform;

/**
 * Curse
 * Creation date: (03/07/12)
 * @author: JMBE
 */ 
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.datapro.generics.Util;
import com.jspsmart.upload.SmartUpload;

import datapro.eibs.beans.ECC150101Message;
import datapro.eibs.beans.ECC150102Message;
import datapro.eibs.beans.ECO033001Message;
import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.EPV132001Message;
import datapro.eibs.beans.EPV132002Message;
import datapro.eibs.beans.EPV132003Message;
import datapro.eibs.beans.ESS0030DSMessage;
import datapro.eibs.beans.JBObjList;
import datapro.eibs.beans.UserPos;
import datapro.eibs.master.JSEIBSProp;
import datapro.eibs.master.JSEIBSServlet;
import datapro.eibs.master.MessageProcessor;
import datapro.eibs.master.ServiceLocator;
import datapro.eibs.services.FTPStdWrapper;
import datapro.eibs.services.FTPWrapper;

public class JSEPV1320 extends JSEIBSServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374590957161957090L;

	protected static final int R_PRO_LOAD_ENTER = 100;
	protected static final int R_PRO_CONSULTA_LIST = 200;
	protected static final int A_PRO_LOAD_ENTER = 300;
	protected static final int R_PRO_CONSULTA_LIST_DETAIL = 400;
	protected static final int R_PRO_CONSULTA_DETAIL = 500;
	
	private ServletConfig config = null;	

	/**
	 * Inicializamos e servlet
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.config = config;
	}
	
	protected void processRequest(ESS0030DSMessage user, HttpServletRequest req, HttpServletResponse res, HttpSession session, int screen) throws ServletException, IOException {
 		screen =  R_PRO_LOAD_ENTER;

		try {
			screen = Integer.parseInt(req.getParameter("SCREEN"));
		} catch (Exception e) {	
			//si da error viene del multipart/form-data
		}		
		
		switch (screen) {
		case R_PRO_LOAD_ENTER:
			procReqProEnter(user, req, res, session);
			break;
		case A_PRO_LOAD_ENTER:
			procActionProEnter(user, req, res, session);
			break; 
		case R_PRO_CONSULTA_LIST:
			procActionConsultaPromocionesList(user, req, res, session);
			break; 
		case R_PRO_CONSULTA_LIST_DETAIL:
			procActionConsultaPromocionesListDetail(user, req, res, session);
			break; 
		case R_PRO_CONSULTA_DETAIL:
			procReporteConsultaDetalleCargaPromo(user, req, res, session);
			break; 

			
		}		
	}
	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param ses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void procReqProEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession ses)
			throws ServletException, IOException {

		try {
			flexLog("About to call Page: EPV1320_promociones_load_enter.jsp");
			forward("EPV1320_promociones_load_enter.jsp", req, res);
		} catch (Exception e) {
			e.printStackTrace();
			flexLog("Exception calling page " + e);
		}
	}
	

	/**
	 * 
	 * @param user
	 * @param req
	 * @param res
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 * 
	 */
	protected void procActionProEnter(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {
	
		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");
		MessageProcessor mp = null;
		ELEERRMessage msgError = null;
		boolean ok = false;
		boolean okTam = true;
		String idPromocion = "";
		String TipoCarga = "P";  //Asume carga parcial
		double TamanoArchivo = 0;
		double NumeroReg = 0;

		if(req.getParameter("TipoCarga") != null) 
			TipoCarga=req.getParameter("TipoCarga");
		
		try {
			SmartUpload mySmartUpload = new SmartUpload();
			com.jspsmart.upload.File myFile = null;
			try {
				mySmartUpload.initialize(config, req, res);
				mySmartUpload.upload();
				myFile = mySmartUpload.getFiles().getFile(0);
				
				TamanoArchivo = myFile.getSize();
				
				if(!myFile.getFileExt().toLowerCase().trim().startsWith("txt"))
				{
		    		msgError = new ELEERRMessage();
		    		msgError.setERRNUM("1");
		    		msgError.setERNU01("01");		                
		    		msgError.setERDS01("El archivo debe tener extesion txt");
		    		okTam = false;
				}	
					
				if (TamanoArchivo > 0) 
				{
				    NumeroReg = TamanoArchivo/50;
				    
				    if (TipoCarga.equals("P") && (((int) NumeroReg) > 10) )
				    {	
						msgError = new ELEERRMessage();
						msgError.setERRNUM("1");
						msgError.setERNU01("01");		                
						msgError.setERDS01("El archivo no puede contener mas de 10 registros");
						okTam = false;
				   	}
				    	
				   	if(okTam) 
				   	{	
				    		//El archivo tiene datos buscamos el id asignado.					
				    		mp = getMessageProcessor("EPV1320", req);					
				    		EPV132001Message msg = (EPV132001Message) mp.getMessageRecord("EPV132001");
				    		//seteamos las propiedades
				    		msg.setH01USERID(user.getH01USR());
			    			msg.setE01PVHTPR(TipoCarga);
				    		msg.setH01OPECOD("0001");
				    		msg.setH01TIMSYS(getTimeStamp());					
					
				    		//Sending message
				    		mp.sendMessage(msg);
		
				    		//Receive error and data
				    		msgError = (ELEERRMessage) mp.receiveMessageRecord();
				    		msg = (EPV132001Message) mp.receiveMessageRecord();		
					
					
				    		//havent errors i get the field
				    		if (!mp.hasError(msgError)) {
				    			try {
				    				String fileName =  "PVPINP";//nombre del archivo	
				    				idPromocion = msg.getE01PVHIDC();
					
				    				byte[] bd = new byte[myFile.getSize()];
				    				for (int i = 0; i < myFile.getSize(); i++) 
				    				{
				    					bd[i] = myFile.getBinaryData(i);
				    				}
						
				    				InputStream  input = new ByteArrayInputStream(bd);													
				    				String userid = ServiceLocator.getSlInfo().getString("ftp.cnx.userid.eibs-server");
				    				String password = ServiceLocator.getSlInfo().getString("ftp.cnx.password.eibs-server");												
					
				    				FTPWrapper ftp = new FTPStdWrapper(JSEIBSProp.getHostIP(), userid, password, "");
						
				    				if (ftp.open()) 
				    				{
				    					ftp.setFileType(FTPWrapper.ASCII);
				    					ftp.upload(input,fileName); 
				    					ok=true;
				    				}
				    				else
				    				{	
				    					msgError = new ELEERRMessage();
				    					msgError.setERRNUM("1");
				    					msgError.setERNU01("01");		                
				    					msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
				    				}	

			    				} 
			    				catch (Exception e) {
			    					msgError = new ELEERRMessage();
			    					msgError.setERRNUM("1");
			    					msgError.setERNU01("01");		                
			    					msgError.setERDS01("NO EXISTE CONEXION AL SERVIDOR AS400 por FTP. Por Favor verifique");	
			    				}

			    			}	
					}	
				}
				else
				{
					//mandamos error en session 
					msgError = new ELEERRMessage();
					msgError.setERRNUM(new BigDecimal(1));
					msgError.setERDS01("Archivo Vacio...");							
				}
				
			}catch (Exception e) {
				String className = e.getClass().getName();
				String description = e.getMessage() == null ? "Exception General" : e.getMessage();	
				msgError = new ELEERRMessage();			
				msgError.setERRNUM("1");
	            msgError.setERNU01("01");
	            msgError.setERDS01(className);
	            msgError.setERNU02("02");
	            msgError.setERDS02(description.length() > 70 ? description.substring(0, 70) : description);
	            msgError.setERNU03("03");
	            msgError.setERDS03("Para mas informacion revizar los archivos de log.");
				e.printStackTrace();			
			} 	
			
			if (ok){									
				msgError = null;
				mp.close();
				mp = null;

				mp = getMessageProcessor("EPV1320", req);					
				EPV132001Message msg = (EPV132001Message) mp.getMessageRecord("EPV132001");
				msg.setH01USERID(user.getH01USR());
    			msg.setE01PVHTPR(TipoCarga);
				msg.setH01OPECOD("0002");
				msg.setH01TIMSYS(getTimeStamp());
				//Sets message with page fields
				msg.setE01PVHIDC(idPromocion);

				//Sending message
				mp.sendMessage(msg);

				//Receive error and data
				msgError = (ELEERRMessage) mp.receiveMessageRecord();
				msg = (EPV132001Message) mp.receiveMessageRecord();

				//Sets session with required data
				session.setAttribute("userPO", userPO);
				session.setAttribute("prom", msg);

				if (!mp.hasError(msgError)) {
					forward("EPV1320_promociones_load_procces.jsp", req, res);
				} else {
					//if there are errors go back to maintenance page and show errors
					session.setAttribute("error", msgError);
					forward("EPV1320_promociones_load_enter.jsp", req, res);
				}

			
			}else{
				session.setAttribute("error", msgError);				
				forward("EPV1320_promociones_load_enter.jsp", req, res);
			}

		} finally {
			if (mp != null)
				mp.close();
		}
	}

	
	protected void procActionConsultaPromocionesList(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			
			mp = getMessageProcessor("EPV1320", req);
	
			EPV132002Message msgList = (EPV132002Message) mp.getMessageRecord("EPV132002", user.getH01USR(), "0001");

			mp.sendMessage(msgList);
						
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { // if there are errors go back to first page
				session.setAttribute("error", error);
				
				flexLog("About to call Page: EPV1320_promociones_consulta_carga_list.jsp");
				forward("EPV1320_promociones_consulta_carga_list.jsp", req, res);
			} else {
			
				
     		JBObjList list = mp.receiveMessageRecordList("H02FLGMAS");
				
			session.setAttribute("ListPromo", list);
			forward("EPV1320_promociones_consulta_carga_list.jsp", req, res);
			
			}
			
		} finally { 
			if (mp != null)
				mp.close();
		}
	}

	protected void procActionConsultaPromocionesListDetail(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res, HttpSession session)
			throws ServletException, IOException {

		MessageProcessor mp = null;

		UserPos userPO = (datapro.eibs.beans.UserPos) session.getAttribute("userPO");

		try {
			mp = getMessageProcessor("EPV1320", req);
			
			EPV132003Message msgList = (EPV132003Message) mp.getMessageRecord("EPV132003", user.getH01USR(), "0001");
			
			if (req.getParameter("codigo_lista")!= null){
				
				JBObjList list = (JBObjList)session.getAttribute("ListPromo");
				int index = Util.parseInt(req.getParameter("codigo_lista"));
				EPV132002Message listMessage = (EPV132002Message)list.get(index);
				
				msgList.setE03PVDIDC(listMessage.getE02PVHIDC());
				req.setAttribute("codigo_lista", index);
		
				session.setAttribute("ProSeleccion", listMessage);
			}
			
			//Sends message
			mp.sendMessage(msgList);
			
			ELEERRMessage error = (ELEERRMessage)mp.receiveMessageRecord();	
	
			if (mp.hasError(error)) { 
				session.setAttribute("error", error);
				flexLog("About to call Page: ECC1501_ecc_preem_list.jsp");
				JBObjList ListPromo = (JBObjList)session.getAttribute("ListPromo");

				forward("EPV1320_promociones_consulta_carga_list.jsp", req, res);
			}
			else
			{
				JBObjList list = mp.receiveMessageRecordList("H03FLGMAS");

        	    session.setAttribute("PromListDetalle", list);
     			forwardOnSuccess("EPV1320_promociones_consulta_carga_list_detail.jsp", req, res);
			}  

		} finally {
			if (mp != null)
				mp.close();
		}
	}
	

	protected void procReporteConsultaDetalleCargaPromo(ESS0030DSMessage user,
			HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws ServletException,
			IOException {
		
		try {
				flexLog("About to call Page: " + JSEIBSProp.getReportePromo() + "/ReportServer/Pages/ReportViewer.aspx?/InformesEIBS/Reporte_SegmentoPromociones&idUsuario=" + user.getH01USR() + "&centroCosto=" + user.getE01CCN() + "&Rut=" + user.getE01IDN() +"&codOficina=" + user.getE01UBR() +"&idInterfaz=" + req.getParameter("Id_Interfaz"));
				res.sendRedirect(JSEIBSProp.getReportePromo() + "/ReportServer/Pages/ReportViewer.aspx?/InformesEIBS/Reporte_SegmentoPromociones&idUsuario=" + user.getH01USR() + "&centroCosto=" + user.getE01CCN() + "&Rut=" + user.getE01IDN() +"&codOficina=" + user.getE01UBR() +"&idInterfaz=" + req.getParameter("Id_Interfaz"));

		} finally {
		}
	}
	
	//END
}
	



