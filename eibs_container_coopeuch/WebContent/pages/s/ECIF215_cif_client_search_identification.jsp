<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Informacion Basica</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Page Designer V3.5.2 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>

<jsp:useBean id= "client" class= "datapro.eibs.beans.ECIF21501Message"  scope="session" />
<jsp:useBean id="error" class="datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id="currUser" class="datapro.eibs.beans.ESS0030DSMessage" scope="session" />

<script type="text/javascript">

	function validate() {
		if (document.getElementById("type").value == "RUT") {
			var rut = validateRut(document.getElementById("rut").value, <%=currUser.getE01INT()%>);
			if (rut == 0) {
				alert("!! Error: El Rut no es valido. !!");
				return;
			} else {
				document.getElementById("rut").value = rut;
			}	
		}
		document.forms[0].submit();
	}

</script>

</head>

<body>

<% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
 
    //out.println("<SCRIPT> initMenu(); </SCRIPT>");
 
%> 
<h3 align="center">Informacion Basica del Cliente<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" alt="cif_client_search_identification, ECIF215"></h3>
<hr size="4">
<form method="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSECIF215">
 <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="200">
 <input type="hidden" name="customer_name" value="">
 <input type="hidden" name="E01CUN" value="">
    
  <br>
  <table class="tableinfo">
    <tr> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="15%"> 
              <div align="right">Identificaci&oacute;n :</div>
            </td>
            <td nowrap width="18%"> 
              <input type="text" id="rut" name="E01RUT" size="28" maxlength="25" value="<%= client.getE01RUT().trim()%>" >
           		<a href="javascript: GetCustomerDescId('E01CUN', 'customer_name', 'E01RUT')"> <img
					src="<%=request.getContextPath()%>/images/1b.gif" alt="Ayuda"	
					border="0"></a>
            </td>
            <td nowrap width="11%"> 
              <div align="right">Tipo :</div>
            </td>
            <td nowrap width="20%"> 
              <input type="text" id="type" name="E01TID" size="5" maxlength="4" value="<%="RUT"%>" readonly="readonly">
              <%--  //client.getE01TID().trim()
              <a href="javascript:GetCodeAuxCNOFC('E01TID','34','<%=client.getH01SCR().trim()%>_2' )">
              <img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0"></a> 
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" >
              --%>
            </td>
          </tr>
 		 </table>
 	  </td>
    </tr>
  </table>
  
  <p align="center"> 
   <input id="EIBSBTN" type=button name="Submit" value="Enviar" onclick="validate()">
  </p>

</form>
</body>
</html>
