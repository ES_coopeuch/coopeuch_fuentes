<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/datapro-eibs-input.tld" prefix="eibsinput" %>
<%@ page import = "datapro.eibs.master.Util" %>
<%@page import="com.datapro.constants.EibsFields"%>
<%@page import="com.datapro.eibs.constants.HelpTypes"%>

<html>
<head>
<title>Informacion Basica</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">
<link href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">
 
<jsp:useBean id= "client" class= "datapro.eibs.beans.ESD008020Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />
<jsp:useBean id= "currUser" class= "datapro.eibs.beans.ESS0030DSMessage"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
<SCRIPT Language="Javascript">

<% if(!client.getH20WK1().equals("S")){ %>
	function setLayout() {
		setCenterSize(800,800);
	}
<% } else { %>  
	function setLayout(){}
<% } %>  

//  Process according with user selection
 function goAction(op) {
	
   	switch (op){
	// Validate & Write 
  	case 1:  {
    	document.forms[0].APPROVAL.value = 'N';
       	break;
        }
	// Validate and Approve
	case 2:  {
 		document.forms[0].APPROVAL.value = 'Y';
       	break;
		}
	}
	document.forms[0].submit();
 }

function protectFields(updtyp) {
		document.getElementById("E20SHN").disabled=true;  
		document.getElementById("E20NA1").disabled=true;  
} 
</SCRIPT>
</head>

<body bgcolor="#FFFFFF" onload="setLayout()">

 <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
 }
%>

<h3 align="center">Informaci&oacute;n Cliente Persona Natural<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="client_short_personal_basic, ESD0080"  ></h3>
<hr size="4">
 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/servlet/datapro.eibs.client.JSESD0080" >
  <INPUT TYPE=HIDDEN NAME="SCREEN" VALUE="8">
  <INPUT TYPE=HIDDEN NAME="APPROVAL" VALUE="N">
  <input type="hidden" name="H20WK1" value="<%= client.getH20WK1().trim()%>">
  <input type="hidden" name="E20LGT" value="<%= client.getE20LGT().trim()%>">
  <h4>Datos Persona</h4>
    <div align="left">
      
    <table class="tableinfo" >
      <tr > 
          <td nowrap > 
            <div align="center"> 
              
            <table cellspacing="0" cellpadding="2" width="100%" border="0">
              <tr id="trdark"> 
                <td nowrap width="25%"> 
                  <div align="right">Numero Cliente :</div>
                </td>
                <td nowrap colspan="4"> 
                  <input type="hidden" name="E20CUN" size="15" maxlength="10" value="<%= client.getE20CUN().trim()%>">
                  <%= client.getE20CUN().trim()%> </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="25%"> 
                  <div align="right">Primer Nombre :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" name="E20FNA" size="35" maxlength="30" value="<%= client.getE20FNA().trim()%>">
                  <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
                </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="25%" > 
                  <div align="right">Segundo Nombre :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" name="E20FN2" size="35" maxlength="30" value="<%= client.getE20FN2().trim()%>">
                </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="25%"> 
                  <div align="right">Primer Apellido :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" name="E20LN1" size="35" maxlength="30" value="<%= client.getE20LN1().trim()%>">
                  <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0"  > 
                </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="25%"> 
                  <div align="right">Segundo Apellido :</div>
                </td>
                <td nowrap colspan=3> 
                  <input type="text" name="E20LN2" size="35" maxlength="30" value="<%= client.getE20LN2().trim()%>">
	              <%if(!currUser.getE01INT().equals("11")){%>
	                <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
	              <% }%> 
                </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="25%"> 
                  <div align="right">Nombre de Casada :</div>
                </td>
                <td nowrap colspan=4> 
                  <input type="text" name="E20ACA" size="35" maxlength="30" value="<%= client.getE20ACA().trim()%>">
                </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="25%" > 
                  <div align="right">Nombre Legal :</div>
                </td>
                <td nowrap colspan=4 > 
                  <input type="text" name="E20NA1" size="65" maxlength="60" value="<%= client.getE20NA1().trim()%>">
                </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="25%" > 
                  <div align="right">Nombre Corto :</div>
                </td>
                <td nowrap colspan=4 > 
                  <input type="text" name="E20SHN" size="28" maxlength="25" value="<%= client.getE20SHN().trim()%>">
                  <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0"> 
                </td>
              </tr>
              <tr id="trdark"> 
                <td nowrap width="25%" > 
                  <div align="right">Sexo :</div>
                </td>
                <td nowrap  width="31%"> 
                  <p> 
                    <input type="radio" name="E20SEX" value="F" <%if (client.getE20SEX().equals("F")) out.print("checked"); %>>
                    Femenino 
                    <input type="radio" name="E20SEX" value="M" <%if (client.getE20SEX().equals("M")) out.print("checked"); %>>
                    Masculino <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0"> </p> 
                </td>
                <td nowrap width="21%" > 
                  <div align="right"> N&uacute;mero de Cargas :</div>
                </td>
                <td nowrap width="23%" > 
                  <input type="text" name="E20NSO" size="4" maxlength="2" value="<%= client.getE20NSO().trim()%>">
                </td>
              </tr>
              <tr id="trclear"> 
                <td nowrap width="25%" > 
                  <div align="right">Estado Civil :</div>
                </td>
                <td nowrap  width="31%"> 
                  <select name="E20MST">
                    <option value=" " <% if (!(client.getE20MST().equals("1")||client.getE20MST().equals("2") || client.getE20MST().equals("3")||client.getE20MST().equals("4")||client.getE20MST().equals("5"))) out.print("selected"); %>> 
                    </option>
                    <% if (client.getH20SCR().equals("07")) { %>
                    <option value="1" <% if (client.getE20MST().equals("1")) out.print("selected"); %>>Soltero(a)</option>
                    <option value="2" <% if (client.getE20MST().equals("2")) out.print("selected"); %>>Casado(a)</option>                   
                    <%} else if (client.getH20SCR().equals("18")) { %>
                    <option value="1" <% if (client.getE20MST().equals("1")) out.print("selected"); %>>Soltero(a)</option>
                    <option value="2" <% if (client.getE20MST().equals("2")) out.print("selected"); %>>Casado(a) - Separacion de Bienes</option>                   
                    <option value="3" <% if (client.getE20MST().equals("3")) out.print("selected"); %>>Casado(a) - Sociedad Conyugal</option>
                    <option value="4" <% if (client.getE20MST().equals("4")) out.print("selected"); %>>Casado(a) - Participacion</option>
                    <option value="7" <% if (client.getE20MST().equals("7")) out.print("selected"); %>>CONVIVIENTE CIVIL - SEPARACION DE BIENES</option>
                    <option value="8" <% if (client.getE20MST().equals("8")) out.print("selected"); %>>CONVIVIENTE CIVIL - COMUNIDAD DE BIENES</option>
                    <option value="5" <% if (client.getE20MST().equals("5")) out.print("selected"); %>>Viudo(a)</option>
                    <option value="6" <% if (client.getE20MST().equals("6")) out.print("selected"); %>>Separado(a)</option>
                    <option value="7" <% if (client.getE20MST().equals("9")) out.print("selected"); %>>Otro</option>
					<%} else { %>
		            <option value="1" <% if (client.getE20MST().equals("1")) out.print("selected"); %>>Soltero(a)</option>
                    <option value="2" <% if (client.getE20MST().equals("2")) out.print("selected"); %>>Casado(a)</option>                   
                    <option value="3" <% if (client.getE20MST().equals("3")) out.print("selected"); %>>Divorciado(a)</option>
                    <option value="4" <% if (client.getE20MST().equals("4")) out.print("selected"); %>>Viudo(a)</option>
                    <option value="5" <% if (client.getE20MST().equals("5")) out.print("selected"); %>>Unión Libre</option>
                    <option value="6" <% if (client.getE20MST().equals("6")) out.print("selected"); %>>Otro</option>
					<% } %>
                  </select>
                  <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0"> 
                </td>
                <td nowrap width="21%" > 
                  <div align="right">Nacionalidad :</div>
                </td>
                <td nowrap width="23%" > 
                  <input type="text" name="E20CCS" size="6" maxlength="4" value="<%= client.getE20CCS().trim()%>">
                  <a href="javascript:GetCodeCNOFC('E20CCS','03')"><img src="<%=request.getContextPath()%>/images/1b.gif" alt="ayuda" align="bottom" border="0" ></a> 
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" >                   
                </td>
              </tr>
            </table>
            
          </div>
          </td>
        </tr>
      </table>
   </div>
   
   <% System.out.println("Bloque Dirección H20SCR="+client.getH20SCR()); %>  
    <%if( client.getH20SCR().equals("03")){%>     
		<jsp:include page="ESD0080_address_template_venezuela.jsp" flush="true">				
			<jsp:param name="title" value="Dirección" />
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="false" />
		</jsp:include>
	<%} else if( client.getH20SCR().equals("06")){%> 	
		<jsp:include page="ESD0080_address_template_basic_guatemala.jsp" flush="true">
			<jsp:param name="title" value="Dirección" />
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="false" />
		</jsp:include>
	
	<%} else if( client.getH20SCR().equals("07")){%> 	
		<jsp:include page="ESD0080_address_template_basic_banesco_panama.jsp" flush="true">
			<jsp:param name="suffix" value="E20" />
			<jsp:param name="title" value="Dirección" />
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="false" />
			<jsp:param name="basic" value="true" />
		</jsp:include>
	
	<%} else if( client.getH20SCR().equals("18")){%> 	
		<jsp:include page="ESD0080_address_template_basic_chile.jsp" flush="true">
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="false" />
			<jsp:param name="title" value="Dirección Principal" />
			<jsp:param name="suffix" value="20" />
		</jsp:include>
	
	<%} else {%> 
		<jsp:include page="ESD0080_address_template_basic_generic.jsp" flush="true">
			<jsp:param name="messageName" value="client" />
			<jsp:param name="readOnly" value="false" />
			<jsp:param name="title" value="Dirección" />
			<jsp:param name="suffix" value="20" />
		</jsp:include>

	<%} %>   
    
  <h4>Informacion Adicional</h4>
  <table class="tableinfo">
    <tr > 
      <td nowrap > 
        <table cellspacing="0" cellpadding="2" width="100%" border="0" align="center">
          <tr id="trdark"> 
              <td nowrap> 
              <div align="right"> Numero Identificación :</div>
              </td>
              <td nowrap> 
                 <input type="text" name="E20IDN" size="26" maxlength="25" value="<%= client.getE20IDN().trim()%>" readonly>
              </td>
              <td nowrap> 
              <div align="right">Tipo Identificación :</div>
              </td>
              <td nowrap> 
                  <input type="text" name="E20TID" size="6" maxlength="4" value="<%= client.getE20TID().trim()%>" readonly>
              </td>
            </tr>

          <tr id="trclear"> 
              <td nowrap> 
              <div align="right">Fecha de Nacimiento :</div>
              </td>
              <td nowrap> 
                 <input type="text" name="E20BDD" size="3" maxlength="2" value="<%= client.getE20BDD().trim()%>">
                 <input type="text" name="E20BDM" size="3" maxlength="2" value="<%= client.getE20BDM().trim()%>">
                 <input type="text" name="E20BDY" size="5" maxlength="4" value="<%= client.getE20BDY().trim()%>">
	                <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" border="0" > 
              </td>
              <td nowrap> 
              <div align="right"> Cargo o Actividad :</div>
              </td>
              <td nowrap> 
               <eibsinput:cnofc name="client" flag="32" property="E20FC3"  required="true"/>
              </td>
            </tr>

          <tr id="trdark"> 
            <td nowrap width="27%"> 
              <div align="right">Sueldo L&iacute;quido :</div>
            </td>
            <td nowrap width="21%"> 
                <eibsinput:text name="client" property="E20AMT" eibsType="<%= EibsFields.EIBS_FIELD_TYPE_AMOUNT %>" required="true" />
            </td>
            <td nowrap width="29%"> 
              <div align="right">Tipo de Contrato :</div>
            </td>
            <td nowrap width="23%"> 
             <SELECT name="E20NEM">
                   <OPTION value=" " <% if (!(client.getE20NEM().equals("1") || client.getE20NEM().equals("2")||
                   							  client.getE20NEM().equals("3") || client.getE20NEM().equals("4")||
                   							  client.getE20NEM().equals("5") || client.getE20NEM().equals("6")||
                   							  client.getE20NEM().equals("7") || client.getE20NEM().equals("8")||
                   							  client.getE20NEM().equals("9"))) out.print("selected");%>></OPTION>
                   <OPTION value="1" <% if (client.getE20NEM().equals("1")) out.print("selected"); %>="">Contratado</OPTION>
                   <OPTION value="2" <% if (client.getE20NEM().equals("2")) out.print("selected"); %>="">Indefinido</OPTION>
                   <OPTION value="3" <% if (client.getE20NEM().equals("3")) out.print("selected"); %>="">Suplencia</OPTION>
                   <OPTION value="4" <% if (client.getE20NEM().equals("4")) out.print("selected"); %>="">Honorarios</OPTION>
                   <OPTION value="5" <% if (client.getE20NEM().equals("5")) out.print("selected"); %>="">Independiente</OPTION>
                   <OPTION value="6" <% if (client.getE20NEM().equals("6")) out.print("selected"); %>="">Pensionado</OPTION>
                   <OPTION value="7" <% if (client.getE20NEM().equals("7")) out.print("selected"); %>="">De Planta</OPTION>
                   <OPTION value="8" <% if (client.getE20NEM().equals("8")) out.print("selected"); %>="">Plazo Fijo</OPTION>
                   <OPTION value="9" <% if (client.getE20NEM().equals("9")) out.print("selected"); %>="">Sin Contrato</OPTION>
             </SELECT>
              <img src="<%=request.getContextPath()%>/images/Check.gif" alt="mandatory field" align="bottom" >              
             </td>
          </tr>

          <tr id="trclear"> 
            <td nowrap width="27%"> 
              <div align="right">Tel&eacute;fono Casa :</div>
            </td>
            <td nowrap width="21%"> 
              <input type="text" name="E20HPN" size="16" maxlength="15" value="<%= client.getE20HPN().trim()%>">
             </td>
            <td nowrap width="29%"> 
              <div align="right">Tel&eacute;fono Oficina :</div>
            </td>
            <td nowrap width="23%"> 
              <input type="text" name="E20PHN" size="16" maxlength="15" value="<%= client.getE20PHN().trim()%>">
            </td>
          </tr>
          <tr id="trdark">  
            <td nowrap width="27%" > 
              <div align="right">Tel&eacute;fono Fax :</div>
            </td>
            <td nowrap width="21%" > 
              <input type="text" name="E20FAX" size="16" maxlength="15" value="<%= client.getE20FAX().trim()%>">
            </td>
            <td nowrap width="29%" > 
              <div align="right">Tel&eacute;fono Celular :</div>
            </td>
            <td nowrap width="23%" > 
              <input type="text" name="E20PH1" size="16" maxlength="15" value="<%= client.getE20PH1().trim()%>">
            </td>
          </tr>
        </table>
        </td>
    </tr>
  </table>

	
	<br>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" bordercolor="#FFFFFF">
    	<tr bgcolor="#FFFFFF"> 
      		<td width="33%"> 
        		<div align="center"><input type="checkbox" name="H20WK2" value="1" >Aceptar con Avisos</div>
      		</td>
    	</tr>
  	</table>

<table width="100%">		
  	<tr>
		<td width="100%">
  		  <div align="center"> 
     		<input id="EIBSBTN" type="button" name="Submit" value="Enviar" onClick="javascript:goAction(1);">
     	  </div>	
  		</td>
  	</tr>	
</table>	
	<SCRIPT language="JavaScript">
		protectFields('<%= userPO.getPurpose().trim()%>');	
	</SCRIPT>

</form>
</body>
</html>

