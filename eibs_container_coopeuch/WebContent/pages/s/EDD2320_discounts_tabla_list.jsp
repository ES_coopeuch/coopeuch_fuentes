<%@ page import = "datapro.eibs.master.Util" %>
<html>
<head>
<title>Definici&oacute;n de Descuentos - Ingreso</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=ISO-8859-1">
<link Href="<%=request.getContextPath()%>/pages/style.css" rel="stylesheet">

<jsp:useBean id= "RTDisTabList" class= "datapro.eibs.beans.JBObjList"  scope="session" />
<jsp:useBean id="RTDis" class="datapro.eibs.beans.EDD232004Message"  scope="session" />
<jsp:useBean id= "error" class= "datapro.eibs.beans.ELEERRMessage"  scope="session" />
<jsp:useBean id= "userPO" class= "datapro.eibs.beans.UserPos"  scope="session" />

<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/eIBS.jsp"> </SCRIPT>
<script language="Javascript1.1" src="<%=request.getContextPath()%>/pages/s/javascripts/optMenu.jsp"> </SCRIPT>
 
 


<script language="JavaScript">



function goAction(op) {

	document.forms[0].opt.value = op;
	document.forms[0].submit();
  
}


function cancel() {
	document.forms[0].SCREEN.value = 3100;
	document.forms[0].submit();
}


function goDelete() {

	if(confirm("Esta seguro que desea borrar este codigo?")){
		document.forms[0].opt.value = 3;
		document.forms[0].submit();
	}
}

</SCRIPT>  

</head>

<BODY>
<h3 align="center">Lista de Tablas Relacionados a Descuentos<img src="<%=request.getContextPath()%>/images/e_ibs.gif" align="left" name="EIBS_GIF" ALT="discounts_tabla_list, EDD2320"></h3>
<hr size="4">
<FORM name="form1" METHOD="post" action="<%=request.getContextPath()%>/servlet/datapro.eibs.products.JSEDD2320" >
    <input type=HIDDEN name="SCREEN" value="5800">
    <input type=HIDDEN name="opt"> 
  
  <h4>Datos Descuento</h4>
  <table  class="tableinfo">
    <tr bordercolor="#FFFFFF"> 
      <td nowrap> 
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
          <tr id="trdark"> 
            <td nowrap width="20%"> 
              <div align="right">C&oacute;digo de Descuento :</div>
            </td>
            <td nowrap width="15%"> 
              <div align="left"> 
                <input type="text" name="E04CMRDCDC" size="5" maxlength="4" value="<%= RTDis.getE04CMRDCDC().trim()%>"  readonly>
              </div>
            </td>
            <td nowrap width="20%"> 
              <div align="right">Descripci&oacute;n  :</div>
            </td>
            <td nowrap> 
              <div align="left" width="45%"> 
                <input type="text" name="E04CMRDGDC" size="31" maxlength="30" value="<%= RTDis.getE04CMRDGDC().trim()%>" readonly >
              </div>
            </td>
          </tr>
        </table>
       </td>   
 	</tr>       
  </table>

    <%
	if ( RTDisTabList.getNoResult() ) {
 %>

  <TABLE class="tbenter" width="100%" >
    <TR>
      <TD > 
        <div align="center"> 
          <br>
          <p><b>No hay resultados para su b&uacute;squeda</b></p>
          <table class="tbenter" width=100% align=center>
            <tr> 
              <td class=TDBKG width="50%"> 
                <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
              </td>
              <td class=TDBKG width="50%"> 
                <div align="center"><a href="javascript:cancel()"><b>Volver</b></a></div>
              </td>
            </tr>
          </table>
          <p>&nbsp;</p>
          
        </div>

	  </TD>
	</TR>
    </TABLE>
	
  <%  
		}
	else {
%> <% 
 if ( !error.getERRNUM().equals("0")  ) {
     error.setERRNUM("0");
     out.println("<SCRIPT Language=\"Javascript\">");
     out.println("       showErrors()");
     out.println("</SCRIPT>");
     }

%> 
 
          
  <table class="tbenter" width=100% align=center height="8%">
    <tr> 
      <td class=TDBKG width="33%"> 
        <div align="center"><a href="javascript:goAction(1)"><b>Crear</b></a></div>
      </td>
	<td class=TDBKG width="34%"> 
        <div align="center"><a href="javascript:goDelete(3)"><b>Borrar</b></a></div>
      </td>      
	<td class=TDBKG width="33%"> 
        <div align="center"><a href="javascript:cancel()"><b>Volver</b></a></div>
      </td>      
    </tr>
  </table>
  <br>
 
 

  <table  id=cfTable class="tableinfo" height="62%">
    <tr height="5%"> 
      <td NOWRAP valign="top" width="100%"> 
        <table id="headTable" width="100%">
          <tr id="trdark"> 
            <th align=CENTER nowrap width="5%">&nbsp;</th>
            <th align=LEFT width="15%" nowrap >COD. TABLA</th>
            <th align=LEFT width="30%" >DESCRIPCION</th>
            <th align=LEFT width="50%" ></th>
          </tr>
 
           <%
                RTDisTabList.initRow();
				boolean firstTime = true;
				String chk = "";
        		while (RTDisTabList.getNextRow()) {
					if (firstTime) {
						firstTime = false;
						chk = "checked";
					} else {
						chk = "";
					}
                  	datapro.eibs.beans.EDD232004Message msgList = (datapro.eibs.beans.EDD232004Message) RTDisTabList.getRecord();
		 %>
          <tr id="dataTable<%= RTDisTabList.getCurrentRow() %>"> 
            <td NOWRAP  align=CENTER width="5%"><input type="radio" name="CURRCODE2" value="<%= RTDisTabList.getCurrentRow() %> "  <%=chk%> onClick="highlightRow('dataTable', this.value)"></td>
            <td NOWRAP  align=CENTER ><%= msgList.getE04CMRDTDC() %></td>
            <td NOWRAP  align="left" ><%= msgList.getE04CMRDTDC() %></td>
            <td NOWRAP  align=CENTER ></td>
          </tr>
          <%}%>
          </table>
      </td>
    </tr>
  </table>

  
   

<SCRIPT language="JavaScript">
	showChecked("CURRCODE2");
	function resizeDoc() {
	 	divResize();
	    adjustEquTables(document.getElementById('headTable'), document.getElementById('dataTable'), document.getElementById('dataDiv1'), 1, false);
	}
	resizeDoc();   			
	window.onresize=resizeDoc;        
</SCRIPT>

<%}%>

  </form>

</body>
</html>
